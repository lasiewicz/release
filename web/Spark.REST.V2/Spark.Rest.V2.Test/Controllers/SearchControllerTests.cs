﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Matchnet.Search.Interfaces;
using Matchnet.Session.ServiceAdapters;
using NUnit.Framework;
using Spark.REST.Configuration;
using Spark.REST.Controllers;
using Spark.REST.Entities.Search;
using Spark.Rest.Helpers;
using Spark.REST.Models;
using Spark.REST.Models.Search;
using Spark.Rest.Serialization;
using Spark.REST.DataAccess.Search;
using Spark.REST.V2.Test.Mocks;

#endregion

namespace Spark.REST.V2.Test.Controllers
{
    [TestFixture]
    public class SearchControllerTests
    {
        const string BaseUri = "http://api.local.spark.net"; //local
        //const string BaseUri = "https://api.spark.net"; //prod
        const int SiteId = 103;
        const int BrandId = 1003;
        //const int MemberId = 101805247; //dev
        //const int MemberId = 1127800169; //stage
        const int MemberId = 101805265; //QA test user on stage
        const int MemberId = 144032982; //prod

        SearchController _searchController;
        readonly string _preferencesUri = string.Format("/v2/brandId/{0}/match/preferences", SiteId);
        readonly string _searchUri = string.Format("/v2/brandId/{0}/search/results", SiteId);
        const string ProfileRelativeUri = "/v2/brandId/{0}/profile/attributeset/resultsetprofile/{1}";
        readonly IHttpContextHelperMock _httpContextHelperMock = new IHttpContextHelperMock();

        [TestFixtureSetUp]
        public void Setup()
        {
            _searchController = GetController<SearchController>(string.Format("{0}{1}", BaseUri, _preferencesUri));
            AttributeConfigReader.AttributeSetXmlPath = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + @"..\..\..\Spark.Rest.V2\Configuration\AttributeSetList.xml");
            AttributeConfigReader.PropertyWhitelistXmlPath = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + @"..\..\..\Spark.Rest.V2\Configuration\AttributeWhitelist.xml");
            var accountFile = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + @"..\..\..\Test References\MatchRatingSearchResults-Stage.csv");
            if (!File.Exists(accountFile)) return;
            var fileText = File.ReadAllText(accountFile);
            var accountArray = fileText.Split(',').ToArray();
            _viableAccounts = accountArray.Select(int.Parse).ToArray();
            _fileExists = true;
        }

        [Test]
        public void RetrieveAndVerifySearchResults()
        {
            try
            {
                var memberRequest = new MemberRequest {BrandId = BrandId};
                var preferencesResult = ((NewtonsoftJsonResult) _searchController.GetMatchPreferences(memberRequest)).Result;
                var searchRequest = new SearchResultsRequest(preferencesResult as SearchPreferences)
                {
                    PageSize = 1000, PageNumber = 1, SearchOrderByType = QuerySorting.MutualMatch, SearchType = SearchType.APISearch,
                    SearchEntryPointType = SearchEntryPoint.YourMatchesPage, Keywords = string.Empty, IncludeSpotlightProfile = false
                };

                _searchController = GetController<SearchController>(string.Format("{0}{1}", BaseUri, _searchUri));
                var searchResult = ((NewtonsoftJsonResult) _searchController.GetSearchResultsViaPostV2(searchRequest)).Result;
                Assert.IsNotNull(searchResult, "The call to GetSearchResultsViaPostV2 did not retrieve any results");

                var jsonItems = searchResult as SearchResultsV2;
                Assert.IsNotNull(jsonItems, "Casting search results to SearchResultsV2 resulted in null");
                var totalCount = 0;
                var matchCount = 0;
                var unmatchCount = 0;
                var matchSb = new StringBuilder("Matched Records\n");
                var unmatchSb = new StringBuilder("Unmatched Records\n");
                foreach (var item in jsonItems.Members)
                {
                    var targetMemberId = Convert.ToInt32(item["memberId"]);
                    var matchRating = Convert.ToInt32(item["matchRating"]);
                    var profileController = GetController<ProfileController>(string.Format("{0}{1}", BaseUri, string.Format(ProfileRelativeUri, SiteId, MemberId)));
                    var attributeSetRequest = new AttributeSetRequest {BrandId = BrandId, SiteId = SiteId, MemberId = MemberId, TargetMemberId = targetMemberId, AttributeSetName = "resultsetprofile"};
                    var profile = (Dictionary<string, object>) ((NewtonsoftJsonResult) profileController.GetAttributeSet(attributeSetRequest)).Result;
                    var targetMatchRating = Convert.ToInt32(profile["matchRating"]);
                    if (matchRating == targetMatchRating)
                    {
                        matchCount++;
                        matchSb.AppendLine(string.Format("{0} | {1} | {2}", targetMemberId, matchRating, targetMatchRating));
                    }
                    else
                    {
                        unmatchCount++;
                        unmatchSb.AppendLine(string.Format("{0} | {1} | {2}", targetMemberId, matchRating, targetMatchRating));
                    }
                    totalCount++;
                }
                Assert.AreEqual(totalCount, matchCount, string.Format("{0} Matches out of {1} total records {2} unmatchedItems.\n{3}\n{4}", matchCount, totalCount, unmatchCount, unmatchSb, matchSb));
            }
            catch (Exception exc)
            {
                var st = new StackTrace(exc, true);
                Console.WriteLine("Message: {0}\nLine Number:{1}\nStack Trace:\n{2}\n", exc.Message, st.GetFrame(0).GetFileLineNumber(), exc.StackTrace);
                Console.WriteLine("AttributeSetXmlPath:\n{0}\nPropertyWhitelistXmlPath:\n{1}", AttributeConfigReader.AttributeSetXmlPath, AttributeConfigReader.PropertyWhitelistXmlPath);
                Assert.AreEqual(0, 1);
            }
            
        }

        private T GetController<T>(string url) where T : SparkControllerBase, new()
        {
            var controller = new T();
            var context = GetMockHttpContext(string.Format("{0}{1}", BaseUri, url));
            _httpContextHelperMock.CurrentHttpContextBase = context;
            SessionSA.HttpContextHelper = _httpContextHelperMock;
            ErrorHelper.HttpContextHelper = _httpContextHelperMock;
            controller.ControllerContext = new ControllerContext(new RequestContext(context, new RouteData()), controller);
            return controller;
        }

        private static HttpContextBase GetMockHttpContext(string urlPath)
        {
            //const string queryString = "access_token=3-g2cdEnyLG7H0n2gmTzVp68atOOu3YucnvPJ4PBJoMA0"; //dev
            //const string queryString = "access_token=3-47SnXkDn3DuzQjYv2wc1CbKWUYxubR009eA9ftA1aU8"; //stage
            const string queryString = "access_token=1/FAePmJx8/SsogkbWJB5RF5tcXYFJMY1NRS0FzYVibfw="; //prod
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter) { StatusCode = (int)HttpStatusCode.OK };
            const string originIp = "64.16.85.39";
            var mockHttpContext = MockHttpContextFactory.GetMockHttpContext(BaseUri + urlPath, queryString, originIp, new HttpResponseWrapper(httpResponse));
            mockHttpContext.Items["tokenMemberId"] = MemberId;
            return mockHttpContext;
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
        }

        [Test]
        public void GetDailyMatchesTest()
        {
            try
            {
                List<Dictionary<string, object>> Run1Members = new List<Dictionary<string, object>>();
                List<Dictionary<string, object>> Run2Members = new List<Dictionary<string, object>>();
                List<Dictionary<string, object>> Run3Members = new List<Dictionary<string, object>>();
                List<Dictionary<string, object>> Run4Members = new List<Dictionary<string, object>>();


                SearchResultsRequest searchRequest = new SearchResultsRequest() { SearchType = SearchType.DailyMatches };
                var searchResults = ReadonlySearch.Instance.GetSearchResultsV2(searchRequest.Brand, MemberId,
                                                                          searchRequest.Gender,
                                                                          searchRequest.SeekingGender,
                                                                          searchRequest.MinAge, searchRequest.MaxAge,
                                                                          searchRequest.RegionId,
                                                                          searchRequest.MaxDistance,
                                                                          searchRequest.ShowOnlyJewishMembers,
                                                                          searchRequest.ShowOnlyMembersWithPhotos,
                                                                          searchRequest.PageSize,
                                                                          searchRequest.PageNumber,
                                                                          searchRequest.ListPosition,
                                                                          searchRequest.MinHeight,
                                                                          searchRequest.MaxHeight,
                                                                          searchRequest.SearchOrderByType,
                                                                          searchRequest.SearchType,
                                                                          searchRequest.SearchEntryPointType,
                                                                          searchRequest.Keywords,
                                                                          searchRequest.IncludeSpotlightProfile,
                                                                          searchRequest.UseDefaultPreferences,
                                                                          searchRequest.ShowOnlyRamah,
                                                                          searchRequest.IncludeBigData,
                                                                          searchRequest.AdvancedPreferences);

                Run1Members=searchResults.Members;
                Console.WriteLine("UseCase1 Results:");
                Run1Members.ForEach(delegate(Dictionary<string,Object> dictionary)
                {
                    Console.WriteLine(dictionary["memberId"]);
                });

                //Usecase 2:
                System.Threading.Thread.Sleep(2*60*1000);
                searchResults = ReadonlySearch.Instance.GetSearchResultsV2(searchRequest.Brand, MemberId,
                                                          searchRequest.Gender,
                                                          searchRequest.SeekingGender,
                                                          searchRequest.MinAge, searchRequest.MaxAge,
                                                          searchRequest.RegionId,
                                                          searchRequest.MaxDistance,
                                                          searchRequest.ShowOnlyJewishMembers,
                                                          searchRequest.ShowOnlyMembersWithPhotos,
                                                          searchRequest.PageSize,
                                                          searchRequest.PageNumber,
                                                          searchRequest.ListPosition,
                                                          searchRequest.MinHeight,
                                                          searchRequest.MaxHeight,
                                                          searchRequest.SearchOrderByType,
                                                          searchRequest.SearchType,
                                                          searchRequest.SearchEntryPointType,
                                                          searchRequest.Keywords,
                                                          searchRequest.IncludeSpotlightProfile,
                                                          searchRequest.UseDefaultPreferences,
                                                          searchRequest.ShowOnlyRamah,
                                                          searchRequest.IncludeBigData,
                                                          searchRequest.AdvancedPreferences);

                Run2Members=searchResults.Members;
                Console.WriteLine("UseCase2 Results:");
                Run2Members.ForEach(delegate(Dictionary<string, Object> dictionary)
                {
                    Console.WriteLine(dictionary["memberId"]);
                });

                //Run1 and Run2 members should be same
                var resultSet = Run1Members.Select(x => x["memberId"]).Except(Run2Members.Select(x => x["memberId"]));
                Assert.IsFalse(resultSet.Any(),"Use Case2 Result.");

                //Usecase 3:
                System.Threading.Thread.Sleep(5 * 60 * 1000);
                searchResults = ReadonlySearch.Instance.GetSearchResultsV2(searchRequest.Brand, MemberId,
                                                          searchRequest.Gender,
                                                          searchRequest.SeekingGender,
                                                          searchRequest.MinAge, searchRequest.MaxAge,
                                                          searchRequest.RegionId,
                                                          searchRequest.MaxDistance,
                                                          searchRequest.ShowOnlyJewishMembers,
                                                          searchRequest.ShowOnlyMembersWithPhotos,
                                                          searchRequest.PageSize,
                                                          searchRequest.PageNumber,
                                                          searchRequest.ListPosition,
                                                          searchRequest.MinHeight,
                                                          searchRequest.MaxHeight,
                                                          searchRequest.SearchOrderByType,
                                                          searchRequest.SearchType,
                                                          searchRequest.SearchEntryPointType,
                                                          searchRequest.Keywords,
                                                          searchRequest.IncludeSpotlightProfile,
                                                          searchRequest.UseDefaultPreferences,
                                                          searchRequest.ShowOnlyRamah,
                                                          searchRequest.IncludeBigData,
                                                          searchRequest.AdvancedPreferences);

                Run3Members = searchResults.Members;
                Console.WriteLine("UseCase3 Results:");
                Run3Members.ForEach(delegate(Dictionary<string, Object> dictionary)
                {
                    Console.WriteLine(dictionary["memberId"]);
                });
                //Run1 and Run2 members should always be diffrent as its scrubbed.
                resultSet = Run1Members.Select(x => x["memberId"]).Except(Run3Members.Select(x => x["memberId"]));
                Assert.AreEqual(resultSet.Count(), 5,"Use Case3 Result.");

                System.Threading.Thread.Sleep(22 * 60 * 1000);

                searchResults = ReadonlySearch.Instance.GetSearchResultsV2(searchRequest.Brand, MemberId,
                                          searchRequest.Gender,
                                          searchRequest.SeekingGender,
                                          searchRequest.MinAge, searchRequest.MaxAge,
                                          searchRequest.RegionId,
                                          searchRequest.MaxDistance,
                                          searchRequest.ShowOnlyJewishMembers,
                                          searchRequest.ShowOnlyMembersWithPhotos,
                                          searchRequest.PageSize,
                                          searchRequest.PageNumber,
                                          searchRequest.ListPosition,
                                          searchRequest.MinHeight,
                                          searchRequest.MaxHeight,
                                          searchRequest.SearchOrderByType,
                                          searchRequest.SearchType,
                                          searchRequest.SearchEntryPointType,
                                          searchRequest.Keywords,
                                          searchRequest.IncludeSpotlightProfile,
                                          searchRequest.UseDefaultPreferences,
                                          searchRequest.ShowOnlyRamah,
                                          searchRequest.IncludeBigData,
                                          searchRequest.AdvancedPreferences);

                Run4Members = searchResults.Members;
                Console.WriteLine("UseCase4 Results:");
                Run4Members.ForEach(delegate(Dictionary<string, Object> dictionary)
                {
                    Console.WriteLine(dictionary["memberId"]);
                });
                //Run1 and Run2 members could be same as scrubbed list is wiped out from couchbase.
                resultSet = Run1Members.Select(x => x["memberId"]).Except(Run4Members.Select(x => x["memberId"]));
                Assert.IsFalse(resultSet.Any(), "Use Case4 Result.");
            }
            catch (Exception exc)
            {
                var st = new StackTrace(exc, true);
                Console.WriteLine("Message: {0}\nLine Number:{1}\nStack Trace:\n{2}\n", exc.Message, st.GetFrame(0).GetFileLineNumber(), exc.StackTrace);
                Console.WriteLine("AttributeSetXmlPath:\n{0}\nPropertyWhitelistXmlPath:\n{1}", AttributeConfigReader.AttributeSetXmlPath, AttributeConfigReader.PropertyWhitelistXmlPath);
                Assert.AreEqual(0, 1);
            }

        }

    }
}
