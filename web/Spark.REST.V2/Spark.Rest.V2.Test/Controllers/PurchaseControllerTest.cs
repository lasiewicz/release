﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using NUnit.Framework;
using RestSharp.Deserializers;
using RestSharp.Serializers;
using Spark.Common;
using Spark.Common.Adapter;
using Spark.REST.Controllers;
using Spark.REST.Entities.Purchase;

namespace Spark.REST.V2.Test.Controllers
{
    [TestFixture]
    public class PurchaseControllerTest
    {
        private string encodedReceipt = "MIIk6QYJKoZIhvcNAQcCoIIk2jCCJNYCAQExCzAJBgUrDgMCGgUAMIIUmgYJKoZIhvcNAQcBoIIUiwSCFIcxghSDMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDgIBAQQDAgFSMAsCAQ8CAQEEAwIBADALAgEQAgEBBAMCAQAwCwIBGQIBAQQDAgEDMAwCAQoCAQEEBBYCNCswDQIBAwIBAQQFDAMxLjAwDQIBDQIBAQQFAgMBEdUwDQIBEwIBAQQFDAMxLjAwDgIBCQIBAQQGAgRQMjMxMBgCAQQCAQIEECf/yWoSHCy2LVkMfzuxa70wGQIBAgIBAQQRDA9jb20uc3BhcmsuSkRhdGUwGwIBAAIBAQQTDBFQcm9kdWN0aW9uU2FuZGJveDAcAgEFAgEBBBRIJKmSVrgH9JCvX367J4DPXdYBLjAeAgEMAgEBBBYWFDIwMTQtMDctMTZUMTg6MzQ6MDNaMB4CARICAQEEFhYUMjAxMy0wOC0wMVQwNzowMDowMFowQAIBBwIBAQQ4PdfoaiL2MpvjMZzqwF87+uL/9hYD9lSklDHTPMGvwbC/AaA5nOZDpT4Bx3cCJFNdxY0qYTynuB4wWgIBBgIBAQRSZa8PSBUFfEZgKb3MmlPJ+adJhiPMXpznuvmYdbNPgWjUvpu/V18KXJhspql+i0KrsXkCGsfXrtpFozugSROJmQFgXF8pDQBxgwDMGqi6LWoIETCCAYcCARECAQEEggF9MYIBeTALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6md7zAMBsCAganAgEBBBIMEDEwMDAwMDAxMTY2Mjg3MzQwGwICBqkCAQEEEgwQMTAwMDAwMDExNjYyODY5NTAfAgIGqAIBAQQWFhQyMDE0LTA3LTE2VDE4OjM0OjAzWjAfAgIGqgIBAQQWFhQyMDE0LTA3LTExVDIyOjQyOjI5WjAfAgIGrAIBAQQWFhQyMDE0LTA3LTExVDIyOjQ2OjI2WjAzAgIGpgIBAQQqDChjb20uc3BhcmsuSkRhdGUuSkRJXzFNX1NVQl9URVNUXzIwMTIwNDI0MIIBhwIBEQIBAQSCAX0xggF5MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMBICAgavAgEBBAkCBwONfqZ3vMEwGwICBqcCAQEEEgwQMTAwMDAwMDExNjYyODY5NTAbAgIGqQIBAQQSDBAxMDAwMDAwMTE2NjI4Njk1MB8CAgaoAgEBBBYWFDIwMTQtMDctMTZUMTg6MzQ6MDNaMB8CAgaqAgEBBBYWFDIwMTQtMDctMTFUMjI6NDA6MjdaMB8CAgasAgEBBBYWFDIwMTQtMDctMTFUMjI6NDM6MjZaMDMCAgamAgEBBCoMKGNvbS5zcGFyay5KRGF0ZS5KRElfMU1fU1VCX1RFU1RfMjAxMjA0MjQwggGHAgERAgEBBIIBfTGCAXkwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwEgICBq8CAQEECQIHA41+pne8xzAbAgIGpwIBAQQSDBAxMDAwMDAwMTE2NjI4Nzg0MBsCAgapAgEBBBIMEDEwMDAwMDAxMTY2Mjg2OTUwHwICBqgCAQEEFhYUMjAxNC0wNy0xNlQxODozNDowM1owHwICBqoCAQEEFhYUMjAxNC0wNy0xMVQyMjo0NTozMVowHwICBqwCAQEEFhYUMjAxNC0wNy0xMVQyMjo0OToyNlowMwICBqYCAQEEKgwoY29tLnNwYXJrLkpEYXRlLkpESV8xTV9TVUJfVEVTVF8yMDEyMDQyNDCCAYcCARECAQEEggF9MYIBeTALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6md7zLMBsCAganAgEBBBIMEDEwMDAwMDAxMTY2Mjg4MjcwGwICBqkCAQEEEgwQMTAwMDAwMDExNjYyODY5NTAfAgIGqAIBAQQWFhQyMDE0LTA3LTE2VDE4OjM0OjAzWjAfAgIGqgIBAQQWFhQyMDE0LTA3LTExVDIyOjQ5OjAwWjAfAgIGrAIBAQQWFhQyMDE0LTA3LTExVDIyOjUyOjI2WjAzAgIGpgIBAQQqDChjb20uc3BhcmsuSkRhdGUuSkRJXzFNX1NVQl9URVNUXzIwMTIwNDI0MIIBhwIBEQIBAQSCAX0xggF5MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMBICAgavAgEBBAkCBwONfqZ3vNIwGwICBqcCAQEEEgwQMTAwMDAwMDExNjYyODg2NjAbAgIGqQIBAQQSDBAxMDAwMDAwMTE2NjI4Njk1MB8CAgaoAgEBBBYWFDIwMTQtMDctMTZUMTg6MzQ6MDNaMB8CAgaqAgEBBBYWFDIwMTQtMDctMTFUMjI6NTI6MTBaMB8CAgasAgEBBBYWFDIwMTQtMDctMTFUMjI6NTU6MjZaMDMCAgamAgEBBCoMKGNvbS5zcGFyay5KRGF0ZS5KRElfMU1fU1VCX1RFU1RfMjAxMjA0MjQwggGHAgERAgEBBIIBfTGCAXkwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwEgICBq8CAQEECQIHA41+pne81zAbAgIGpwIBAQQSDBAxMDAwMDAwMTE2NjI4ODkzMBsCAgapAgEBBBIMEDEwMDAwMDAxMTY2Mjg2OTUwHwICBqgCAQEEFhYUMjAxNC0wNy0xNlQxODozNDowM1owHwICBqoCAQEEFhYUMjAxNC0wNy0xMVQyMjo1NDo1NlowHwICBqwCAQEEFhYUMjAxNC0wNy0xMVQyMjo1ODoyNlowMwICBqYCAQEEKgwoY29tLnNwYXJrLkpEYXRlLkpESV8xTV9TVUJfVEVTVF8yMDEyMDQyNDCCAYcCARECAQEEggF9MYIBeTALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6md7zZMBsCAganAgEBBBIMEDEwMDAwMDAxMTcwNTAxNTYwGwICBqkCAQEEEgwQMTAwMDAwMDExNjYyODY5NTAfAgIGqAIBAQQWFhQyMDE0LTA3LTE2VDE4OjM0OjAzWjAfAgIGqgIBAQQWFhQyMDE0LTA3LTE2VDE4OjE1OjQ0WjAfAgIGrAIBAQQWFhQyMDE0LTA3LTE2VDE4OjE4OjQ0WjAzAgIGpgIBAQQqDChjb20uc3BhcmsuSkRhdGUuSkRJXzFNX1NVQl9URVNUXzIwMTIwNDI0MIIBhwIBEQIBAQSCAX0xggF5MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMBICAgavAgEBBAkCBwONfqZ39iYwGwICBqcCAQEEEgwQMTAwMDAwMDExNzA1MDIyNzAbAgIGqQIBAQQSDBAxMDAwMDAwMTE2NjI4Njk1MB8CAgaoAgEBBBYWFDIwMTQtMDctMTZUMTg6MzQ6MDNaMB8CAgaqAgEBBBYWFDIwMTQtMDctMTZUMTg6MTc6NDdaMB8CAgasAgEBBBYWFDIwMTQtMDctMTZUMTg6MjE6NDRaMDMCAgamAgEBBCoMKGNvbS5zcGFyay5KRGF0ZS5KRElfMU1fU1VCX1RFU1RfMjAxMjA0MjQwggGHAgERAgEBBIIBfTGCAXkwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwEgICBq8CAQEECQIHA41+pnf2KzAbAgIGpwIBAQQSDBAxMDAwMDAwMTE3MDUxNTkyMBsCAgapAgEBBBIMEDEwMDAwMDAxMTY2Mjg2OTUwHwICBqgCAQEEFhYUMjAxNC0wNy0xNlQxODozNDowM1owHwICBqoCAQEEFhYUMjAxNC0wNy0xNlQxODoyMDo0N1owHwICBqwCAQEEFhYUMjAxNC0wNy0xNlQxODoyNDo0NFowMwICBqYCAQEEKgwoY29tLnNwYXJrLkpEYXRlLkpESV8xTV9TVUJfVEVTVF8yMDEyMDQyNDCCAYcCARECAQEEggF9MYIBeTALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6md/Y3MBsCAganAgEBBBIMEDEwMDAwMDAxMTcwNTE3OTgwGwICBqkCAQEEEgwQMTAwMDAwMDExNjYyODY5NTAfAgIGqAIBAQQWFhQyMDE0LTA3LTE2VDE4OjM0OjAzWjAfAgIGqgIBAQQWFhQyMDE0LTA3LTE2VDE4OjIzOjQ4WjAfAgIGrAIBAQQWFhQyMDE0LTA3LTE2VDE4OjI3OjQ0WjAzAgIGpgIBAQQqDChjb20uc3BhcmsuSkRhdGUuSkRJXzFNX1NVQl9URVNUXzIwMTIwNDI0MIIBhwIBEQIBAQSCAX0xggF5MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMBICAgavAgEBBAkCBwONfqZ39kowGwICBqcCAQEEEgwQMTAwMDAwMDExNzA1MTkwNTAbAgIGqQIBAQQSDBAxMDAwMDAwMTE2NjI4Njk1MB8CAgaoAgEBBBYWFDIwMTQtMDctMTZUMTg6MzQ6MDNaMB8CAgaqAgEBBBYWFDIwMTQtMDctMTZUMTg6MjY6NDhaMB8CAgasAgEBBBYWFDIwMTQtMDctMTZUMTg6MzA6NDRaMDMCAgamAgEBBCoMKGNvbS5zcGFyay5KRGF0ZS5KRElfMU1fU1VCX1RFU1RfMjAxMjA0MjQwggGHAgERAgEBBIIBfTGCAXkwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwEgICBq8CAQEECQIHA41+pnf2VjAbAgIGpwIBAQQSDBAxMDAwMDAwMTE3MDUyOTgzMBsCAgapAgEBBBIMEDEwMDAwMDAxMTY2Mjg2OTUwHwICBqgCAQEEFhYUMjAxNC0wNy0xNlQxODozNDowM1owHwICBqoCAQEEFhYUMjAxNC0wNy0xNlQxODoyOTo0OFowHwICBqwCAQEEFhYUMjAxNC0wNy0xNlQxODozMzo0NFowMwICBqYCAQEEKgwoY29tLnNwYXJrLkpEYXRlLkpESV8xTV9TVUJfVEVTVF8yMDEyMDQyNKCCDlUwggVrMIIEU6ADAgECAggYWUMhcnSc/DANBgkqhkiG9w0BAQUFADCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0xMDExMTEyMTU4MDFaFw0xNTExMTEyMTU4MDFaMHgxJjAkBgNVBAMMHU1hYyBBcHAgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC2k8K3DyRe7dI0SOiFBeMzlGZb6Cc3v3tDSev5yReXM3MySUrIb2gpFLiUpvRlSztH19EsZku4mNm89RJRy+YvqfSznxzoKPxSwIGiy1ZigFqika5OQMN9KC7X0+1N2a2K+/JnSOzreb0CbQRZGP+MN5+KN/Fi/7uiA1CHCtWS4IYRXiNG9eElYyuiaoyyELeRI02aP4NA8mQJWveNrlZc1PW0bgMbBF0sG68AmRfXpftJkc7ioRExXhkBwNrOUINeyOtJO0kaKurgn7/SRkmc2Kuhg2FsD8H8s62ZdSr8I5vvIgjre1kUEZ9zNC3muTmmO/fmPuzKpvurrybfj4iBAgMBAAGjggHYMIIB1DAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3ME0GA1UdHwRGMEQwQqBAoD6GPGh0dHA6Ly9kZXZlbG9wZXIuYXBwbGUuY29tL2NlcnRpZmljYXRpb25hdXRob3JpdHkvd3dkcmNhLmNybDAOBgNVHQ8BAf8EBAMCB4AwHQYDVR0OBBYEFHV2JKJrYgyXNKH6Tl4IDCK/c+++MIIBEQYDVR0gBIIBCDCCAQQwggEABgoqhkiG92NkBQYBMIHxMIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMCkGCCsGAQUFBwIBFh1odHRwOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAoDvxh7xptLeDfBn0n8QCZN8CyY4xc8scPtwmB4v9nvPtvkPWjWEt5PDcFnMB1jSjaRl3FL+5WMdSyYYAf2xsgJepmYXoePOaEqd+ODhk8wTLX/L2QfsHJcsCIXHzRD/Q4nth90Ljq793bN0sUJyAhMWlb1hZekYxQWi7EzVFQqSM+hHVSxbyMjXeH7zSmV3I5gIyWZDojcs53yHaw3b7ejYaFhqYTIUb5itFLS9ZGi3GmtZmkqPSNlJQgCBNM8iymtZTYrFgUvD1930QUOQSv71xvrSAx23Eb1s5NdHnt96BICeOOFyChzpzYMTW8RygqWZEfs4MKJsjf6zs5qA73TCCBCMwggMLoAMCAQICARkwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA4MDIxNDE4NTYzNVoXDTE2MDIxNDE4NTYzNVowgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKOFSmy1aqyCQ5SOmM7uxfuH8mkbw0U3rOfGOAYXdkXqUHI7Y5/lAtFVZYcC1+xG7BSoU+L/DehBqhV8mvexj/avoVEkkVCBmsqtsqMu2WY2hSFT2Miuy/axiV4AOsAX2XBWfODoWVN2rtCbauZ81RZJ/GXNG8V25nNYB2NqSHgW44j9grFU57Jdhav06DwY3Sk9UacbVgnJ0zTlX5ElgMhrgWDcHld0WNUEi6Ky3klIXh6MSdxmilsKP8Z35wugJZS3dCkTm59c3hTO/AO0iMpuUhXf1qarunFjVg0uat80YpyejDi+l5wGphZxWy8P3laLxiX27Pmd3vG2P+kmWrAgMBAAGjga4wgaswDgYDVR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFIgnFwmpthhgi+zruvZHWcVSVKO3MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMDYGA1UdHwQvMC0wK6ApoCeGJWh0dHA6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2Evcm9vdC5jcmwwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBANoyAJbFVJTTO4I3Zn0uaNXDxrjLJoxIkM8TJGpGjmPU8NATBt3YxME3FfIzEzkmLc4uVUDjCwOv+hLC5w0huNWAz6woL84ts06vhhkExulQ3UwpRxAj/Gy7G5hrSInhW53eRts1hTXvPtDiWEs49O11Wh9ccB1WORLl4Q0R5IklBr3VtBWOXtBZl5DpS4Hi3xivRHQeGaA6R8yRHTrrI1r+pS2X93u71odGQoXrUj0msmOotLHKj/TM4rPIR+C/mlmD+tqYUyqC9XxlLpXZM1317WXMMTfFWgToa+HniANKdZ6bKMtKQIhlQ3XdyzolI8WeV/guztKpkl5zLi8ldRUwggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIGFlDIXJ0nPwwCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQCGFJJyn9QPTDGDHOrIKEO+3kdACWUGxbE37AyDOiBRc46/POX2q9gqWKsLsSyeO8lsWtzysChD2ydzlJjuRnwINTzVUb/rG9I0tudd2YNU+NSeovjFpD0aIgld6Th27X09A91thLb7QZvt4pjSe/g6ylBZCIGXvy956m0HHUnvECccLhKi04RbriOpGWHB91OhY8m7AOpujHD7CqvhroTL7k1IUga9uxEwnI4IaY/4rm+lUP/40SIlDPiArz97qKJqmatq3oY5SMqIQQr+FfSNEZqRMi29nyt1h6Bi2G4IGqJWJehQRS8Sn2Up+MgLAe4B3SvxlUozJuQT4V4RL9Kx";
        private string receiptOriginalTransactionID = "1000000116628695";
        private PurchaseController controller = new PurchaseController();

        [TestFixtureSetUp]
        public void Setup()
        {
            
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            int memberId = 100004579;
            IOSInAppPurchaseRequest iosInAppPurchaseRequest = new IOSInAppPurchaseRequest();
            iosInAppPurchaseRequest.BrandId = 1003;

            Member member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
            string memberOrigTransID = member.GetAttributeText(iosInAppPurchaseRequest.Brand, PurchaseController.IOS_IAP_RECEIPT_ORIG_TRANS_ID_ATTR);
            if (!string.IsNullOrEmpty(memberOrigTransID))
            {
                //Reset member for test again
                member.SetAttributeText(iosInAppPurchaseRequest.Brand,
                    PurchaseController.IOS_IAP_RECEIPT_ORIG_TRANS_ID_ATTR, null, TextStatusType.Auto);
                MemberSA.Instance.SaveMember(member, iosInAppPurchaseRequest.Brand.Site.Community.CommunityID);
            }
        }

        /// <summary>
        ///A test for api call
        ///</summary>
        [Test]
        public void ValidateNoMemberReceiptTest()
        {
            string validateIosReceiptTransID = controller.ValidateIOSReceipt(encodedReceipt, null);
            Assert.IsNotNullOrEmpty(validateIosReceiptTransID);
        }

        [Test]
        public void ValidateWithMemberReceiptTest()
        {
            string validateIosReceiptTransID = controller.ValidateIOSReceipt(encodedReceipt, receiptOriginalTransactionID);
            Assert.IsNotNullOrEmpty(validateIosReceiptTransID);
            Assert.AreEqual(receiptOriginalTransactionID, validateIosReceiptTransID);
        }

        [Test]
        public void SendIOSInAppPurchaseTest()
        {
            int memberId = 100004579;
            IOSInAppPurchaseRequest iosInAppPurchaseRequest = new IOSInAppPurchaseRequest();
            iosInAppPurchaseRequest.BrandId = 1003;
            iosInAppPurchaseRequest.EncondedReceiptJSON = encodedReceipt;

            Member member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
            string memberOrigTransID = member.GetAttributeText(iosInAppPurchaseRequest.Brand, PurchaseController.IOS_IAP_RECEIPT_ORIG_TRANS_ID_ATTR);
            Assert.IsNullOrEmpty(memberOrigTransID);

            if (controller.TempData == null) controller.TempData = new TempDataDictionary();
            controller.TempData["tokenMemberId"] = memberId;

            ActionResult sendIosInAppPurchase = controller.SendIOSInAppPurchase(iosInAppPurchaseRequest);

            Assert.IsTrue(sendIosInAppPurchase is EmptyResult);
            member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
            string newMemberOrigTransID = member.GetAttributeText(iosInAppPurchaseRequest.Brand, PurchaseController.IOS_IAP_RECEIPT_ORIG_TRANS_ID_ATTR);
            Assert.IsNotNullOrEmpty(newMemberOrigTransID);
            Assert.AreEqual(receiptOriginalTransactionID, newMemberOrigTransID);
        }

        [Test]
        public void TruncateReceiptTest()
        {
            Console.WriteLine(string.Format("Length of receipt is {0}", encodedReceipt.Length));
            string compressedEncodedReceipt = encodedReceipt.Compress();
            Console.WriteLine(string.Format("Length of compressed receipt is {0}", compressedEncodedReceipt.Length));
            Assert.AreNotEqual(encodedReceipt, compressedEncodedReceipt);
            string decompressedEncodedReceipt = compressedEncodedReceipt.Decompress();
            Assert.AreEqual(encodedReceipt, decompressedEncodedReceipt);
        }

//        [Test]
//        public void ValidateTruncatedReceiptTest()
//        {
//            IOSReceipt iosReceipt = new IOSReceipt();
//            APIAdapter.RestStatus makeIosApiCall = controller.MakeIOSApiCall(encodedReceipt, out iosReceipt);
//
//            IOSReceipt truncatediosReceipt = new IOSReceipt();
//            truncatediosReceipt.environment = iosReceipt.environment;
//            truncatediosReceipt.latest_receipt_info = iosReceipt.latest_receipt_info.Skip(iosReceipt.latest_receipt_info.Count - 2).ToList();
//            truncatediosReceipt.receipt = new Receipt();
//            truncatediosReceipt.receipt.adam_id = iosReceipt.receipt.adam_id;
//            truncatediosReceipt.receipt.application_version = iosReceipt.receipt.application_version;
//            truncatediosReceipt.receipt.bundle_id = iosReceipt.receipt.bundle_id;
//            truncatediosReceipt.receipt.download_id = iosReceipt.receipt.download_id;
//            truncatediosReceipt.receipt.in_app = iosReceipt.receipt.in_app.Skip(iosReceipt.receipt.in_app.Count - 2).ToList();
//            truncatediosReceipt.receipt.original_application_version = iosReceipt.receipt.original_application_version;
//            truncatediosReceipt.receipt.original_purchase_date = iosReceipt.receipt.original_purchase_date;
//            truncatediosReceipt.receipt.original_purchase_date_ms = iosReceipt.receipt.original_purchase_date_ms;
//            truncatediosReceipt.receipt.original_purchase_date_pst = iosReceipt.receipt.original_purchase_date_pst;
//            truncatediosReceipt.receipt.receipt_type = iosReceipt.receipt.receipt_type;
//            truncatediosReceipt.receipt.request_date = iosReceipt.receipt.request_date;
//            truncatediosReceipt.receipt.request_date_ms = iosReceipt.receipt.request_date_ms;
//            truncatediosReceipt.receipt.request_date_pst = iosReceipt.receipt.request_date_pst;
//            truncatediosReceipt.status = iosReceipt.status;
//
//            JsonSerializer jsonSerializer = new JsonSerializer();
//            jsonSerializer.ContentType = "application/json";
//            string json = jsonSerializer.Serialize(iosReceipt.receipt);
//            string truncatedJson = jsonSerializer.Serialize(truncatediosReceipt.receipt);
//
//            Console.WriteLine(string.Format("Receipt Size:{0}, Value:{1}",json.Length, json));
//            Console.WriteLine(string.Format("Truncated Receipt Size:{0}, Value:{1}", truncatedJson.Length, truncatedJson));
//
//            string urlDecode = encodedReceipt.Replace(" ","+");
//            byte[] fromBase64String = Convert.FromBase64String(urlDecode);
//            string decodedJson = Encoding.UTF8.GetString(fromBase64String);
//            Console.WriteLine(string.Format("\n\nDecoded Receipt Size:{0}, Value:{1}", decodedJson.Length, decodedJson));
//
//
//            string base64String = Convert.ToBase64String(Encoding.UTF8.GetBytes(json)).Replace("+"," ");
//            string truncatedBase64String = Convert.ToBase64String(Encoding.UTF8.GetBytes(truncatedJson));
//
//            IOSReceipt iosReceipt2 = new IOSReceipt();
//            APIAdapter.RestStatus restStatus = controller.MakeIOSApiCall(base64String, out iosReceipt2);
//            Assert.AreEqual(0, iosReceipt2.status);
//
//
//            IOSReceipt iosReceipt3 = new IOSReceipt();
//            APIAdapter.RestStatus truncatedRestStatus = controller.MakeIOSApiCall(truncatedBase64String, out iosReceipt3);
//            Assert.AreEqual(0, iosReceipt3.status);
//        }

    }
}
