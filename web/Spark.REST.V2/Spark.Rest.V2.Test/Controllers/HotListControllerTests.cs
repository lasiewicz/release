﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Matchnet.Session.ServiceAdapters;
using NUnit.Framework;
using Spark.REST.Configuration;
using Spark.REST.Controllers;
using Spark.REST.Entities.HotList;
using Spark.Rest.Helpers;
using Spark.REST.Models.HotList;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Models.HotList;
using Spark.REST.V2.Test.Mocks;
using Spark.Rest.V2.Models.MingleMigration;

namespace Spark.REST.V2.Test.Controllers
{
    [TestFixture]
    public class HotListControllerTests
    {
        private HotListController _hotListController;
        private readonly IHttpContextHelperMock _httpContextHelperMock = new IHttpContextHelperMock();
        private HttpContextBase _httpContextMock;
        private const int MemberId = 101805265;

        private BrokeredHotListController _mingleListController;
        private HotlistChangeRequest mingleRequest;
        private const int LoadTestMemberId = 1127805223;
        private const int LoadTestTargetMemberId = 1127805226;
        private const int LoadTestCommunityId = 90110;

        [TestFixtureSetUp]
        public void Setup()
        {
            _hotListController = new HotListController();
            _mingleListController = new BrokeredHotListController();
            _httpContextMock = GetMockHttpContext();
            _httpContextHelperMock.CurrentHttpContextBase = _httpContextMock;
            SessionSA.HttpContextHelper = _httpContextHelperMock;
            ErrorHelper.HttpContextHelper = _httpContextHelperMock;
            AttributeConfigReader.AttributeSetXmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../Spark.Rest.V2/Configuration/AttributeSetList.xml");
            AttributeConfigReader.PropertyWhitelistXmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../Spark.Rest.V2/Configuration/AttributeWhitelist.xml");

            _hotListController.ControllerContext = new ControllerContext(new RequestContext(_httpContextMock, new RouteData()), _hotListController);
            _mingleListController.ControllerContext = new ControllerContext(new RequestContext(_httpContextMock, new RouteData()), _mingleListController);

        }
        [TestCaseSource("")]
        [TestFixtureTearDown]
        public void Teardown()
        {
        }

        //https://apps.api.spark.net/v2/brandId/1003/hotlist/ViewedCombined/pagesize/25/pagenumber/1?access_token=2/Ipr7HETKihXEWfrtKTXnwGCf7o41CCh5TGRanGxw528=
        private static HttpContextBase GetMockHttpContext()
        {
            const string urlHost = "http://api.local.spark.net";
            const string urlPath = "/v2/brandId/1003/hotlist/ViewedCombined/pagesize/25/pagenumber/1";
            const string queryString = "access_token=2/Ipr7HETKihXEWfrtKTXnwGCf7o41CCh5TGRanGxw528=";
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter) { StatusCode = (int)HttpStatusCode.OK };
            const string originIp = "192.168.1.141";
            var mockHttpContext = MockHttpContextFactory.GetMockHttpContext(urlHost + urlPath, queryString, originIp, new HttpResponseWrapper(httpResponse));
            mockHttpContext.Items["tokenMemberId"] = MemberId;
            return mockHttpContext;
        }

        [Test]
        public void TestCombinedHotLists()
        {
            var hotListRequest = new HotListRequest { BrandId = 1003, HotListCategory = "ViewedCombined", pageSize = 25, pageNumber = 1 };
            var hotListEntriesV2 = (NewtonsoftJsonResult)_hotListController.GetHotListEntriesV2(hotListRequest);
            var hotListResults = (List<HotListEntryV2>)hotListEntriesV2.Result;
            Assert.AreEqual(25, hotListResults.Count);
            Assert.Greater(hotListResults.First().ActionDate, hotListResults.Last().ActionDate);
        }

        [Test]
        public void TestAddOrRemoveFavorites()
        {
            //Running the test for 10 times
            for (int i = 0; i < 10; i++)
            {
                var hotlistChangeRequest = new HotlistChangeRequest
                {
                    BrandId = 1003,
                    HotListCategory = "Default",
                    MemberId = 101805165,
                    TargetMemberId = 23248572 + i,
                    Timestamp = DateTime.UtcNow
                };


                var actionResult = _hotListController.AddToHotlist(hotlistChangeRequest);
                Assert.IsInstanceOf<EmptyResult>(actionResult);
                Thread.Sleep(2000);
                
                actionResult = _hotListController.RemoveFromHotlist(hotlistChangeRequest);
                Assert.IsInstanceOf<EmptyResult>(actionResult);
                Thread.Sleep(2000);
            }
        }

        [Test, TestCaseSource("MingleHotList")]
        public void TestMingleAddToHotlist(int brandId, string hotListCategory, int memberId, int targetMemberId, DateTime timestamp)
        {
            mingleRequest = new HotlistChangeRequest
            {
                BrandId = brandId,
                HotListCategory = hotListCategory,
                MemberId = memberId,
                TargetMemberId = targetMemberId,
                Timestamp = timestamp
            };

            ActionResult actionResult;
            //actionResult = _mingleListController.AddToHotlist(mingleRequest);
            //Assert.IsInstanceOf<EmptyResult>(actionResult);
            //Console.WriteLine("AddToHotlist Completed for Category: " + mingleRequest.HotListCategory);
            //Thread.Sleep(200);

            actionResult = _mingleListController.RemoveFromHotlist(mingleRequest);
            Assert.IsInstanceOf<EmptyResult>(actionResult);
            Console.WriteLine("RemoveFromHotlist Completed for Category: " + mingleRequest.HotListCategory);
            Thread.Sleep(200);
        }

        [Test, TestCaseSource("MingleYNMHotList")]
        public void TestMingleYNMSaveData(int brandId, string hotListCategory, int memberId, int targetMemberId, DateTime timestamp, bool Yes, bool No, bool Maybe)
        {
            var mingleRequest = new MingleYNMVoteRequest()
            {
                BrandId = brandId,
                Yes = Yes,
                No = No,
                Maybe = Maybe,
                MemberId = memberId,
                AddMemberId = targetMemberId,
                Timestamp = timestamp
            };

            ActionResult actionResult;
            actionResult = _mingleListController.SaveYNMVote(mingleRequest);
            Assert.IsInstanceOf<EmptyResult>(actionResult);

            Thread.Sleep(200);

            //actionResult = _mingleListController.RemoveFromHotlist(mingleRequest);
            //Assert.IsInstanceOf<EmptyResult>(actionResult);

            //Thread.Sleep(200);
        }

        [Test, TestCaseSource("MingleMailMaskData")]
        public void TestUpdateMailMask(int brandId,int memberId, int targetMemberId, bool alertEmailSent, bool clickEmailSent)
        {
            var mingleListMailMaskMigrationRequest = new MingleListMailMaskMigrationRequest()
            {
                BrandId = brandId,
                AlertEmailSent = alertEmailSent,
                ClickEmailSent = clickEmailSent,
                MemberId = memberId,
                TargetMemberId = targetMemberId
            };

            ActionResult actionResult;
            actionResult = _mingleListController.UpdateMailMask(mingleListMailMaskMigrationRequest);

            Thread.Sleep(200);
        }


        static object[] MingleHotList =
        {
            //Dual entry
            new object[] { LoadTestCommunityId,"Default",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            new object[] { LoadTestCommunityId,"MembersYouEmailed",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            new object[] { LoadTestCommunityId,"MembersYouTeased",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            new object[] { LoadTestCommunityId,"MembersYouIMed",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            new object[] { LoadTestCommunityId,"MembersYouBlocked",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },


            //Single entry
            new object[] { LoadTestCommunityId,"MembersYouSentECards",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            new object[] { LoadTestCommunityId,"SparkSent",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            new object[] { LoadTestCommunityId,"SparkSent",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },

            ////Fail: Reciprocal should fail as its Dual entry
            new object[] { LoadTestCommunityId,"WhoAddedYouToTheirFavorites",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },
            new object[] { LoadTestCommunityId,"WhoEmailedYou",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },
            new object[] { LoadTestCommunityId,"WhoTeasedYou",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },
            new object[] { LoadTestCommunityId,"WhoIMedYou",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },
            new object[] { LoadTestCommunityId,"WhoBlockedYou",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },

            // //Success: Single entry
            new object[] { LoadTestCommunityId,"WhoSentYouECards",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },
            new object[] { LoadTestCommunityId,"SparkReceived",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },

            ////YNM Data
            new object[] { LoadTestCommunityId,"MembersYouViewed",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow},
            new object[] { LoadTestCommunityId,"WhoViewedYourProfile",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow }

            ////Dual entry
            //new object[] { LoadTestCommunityId,"Default",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"MembersYouEmailed",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"MembersYouTeased",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"MembersYouIMed",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },

            ////Single entry
            //new object[] { LoadTestCommunityId,"MembersYouSentECards",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"SparkSent",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },

            ////YNM entry
            //new object[] { LoadTestCommunityId,"MembersYouViewed",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"MembersClickedYes",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"MembersClickedNo",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"MembersClickedMaybe",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },


            ////Fail: Reciprocal should fail as its Dual entry
            //new object[] { LoadTestCommunityId,"WhoAddedYouToTheirFavorites",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"WhoEmailedYou",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"WhoTeasedYou",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"WhoIMedYou",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },

            // //Success: Single entry
            //new object[] { LoadTestCommunityId,"WhoSentYouECards",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"SparkReceived",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow },

            ////YNM entry
            //new object[] { LoadTestCommunityId,"WhoViewedYourProfile",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"MembersClickedYes",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"MembersClickedNo",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            //new object[] { LoadTestCommunityId,"MembersClickedMaybe",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow },
            
            
        };

        static object[] MingleYNMHotList =
        {
            //YNM entry
            
            new object[] { LoadTestCommunityId,"MembersClickedYes",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow, true, false, false},
            new object[] { LoadTestCommunityId,"MembersClickedNo",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow, false, true, false},
            new object[] { LoadTestCommunityId,"MembersClickedMaybe",LoadTestMemberId,LoadTestTargetMemberId,DateTime.UtcNow , false, false, true},


            
            new object[] { LoadTestCommunityId,"MembersClickedYes",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow, true, false, false },
            new object[] { LoadTestCommunityId,"MembersClickedNo",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow , false, true, false},
            new object[] { LoadTestCommunityId,"MembersClickedMaybe",LoadTestTargetMemberId, LoadTestMemberId,DateTime.UtcNow ,  false, false, true},

        };

        static object[] MingleMailMaskData =
        {
            new object[] { LoadTestCommunityId, LoadTestMemberId, LoadTestTargetMemberId, false, false},
            new object[] { LoadTestCommunityId, LoadTestMemberId, LoadTestTargetMemberId, true, false},
            new object[] { LoadTestCommunityId, LoadTestMemberId, LoadTestTargetMemberId, false, true}
        };

    }
}
