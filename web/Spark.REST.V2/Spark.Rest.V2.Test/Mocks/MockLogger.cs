using System;
using System.Collections.Generic;
using Spark.Logger;

namespace Spark.REST.V2.Test.Mocks
{
    public class MockLogger : ISparkLogger
    {
        public Stack<string> logDebugMessages = new Stack<string>();

        public bool IsDebugEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsErrorEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsFatalEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsInfoEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsWarnEnabled
        {
            get { throw new NotImplementedException(); }
        }


        public void LogDebugMessage(string message, Dictionary<string, string> customData, bool shouldLogExternally, string[] ignoreFormFields, string methodName, string classFile, int sourceLineNumber)
        {
            logDebugMessages.Push(message + " " + customData);
        }

        public void LogError(string message, Exception exception, Dictionary<string, string> customData, bool shouldLogExternally, string[] ignoreFormFields, string methodName, string classFile, int sourceLineNumber)
        {
            throw new NotImplementedException();
        }

        public void LogException(string message, Exception exception, Dictionary<string, string> customData, bool shouldLogExternally, string[] ignoreFormFields, string methodName, string classFile, int sourceLineNumber)
        {
            throw new NotImplementedException();
        }

        public void LogInfoMessage(string message, Dictionary<string, string> customData, bool shouldLogExternally, string[] ignoreFormFields, string methodName, string classFile, int sourceLineNumber)
        {
            throw new NotImplementedException();
        }

        public void LogWarningMessage(string message, Dictionary<string, string> customData, bool shouldLogExternally, string[] ignoreFormFields, string methodName, string classFile, int sourceLineNumber)
        {
            throw new NotImplementedException();
        }
    }
}