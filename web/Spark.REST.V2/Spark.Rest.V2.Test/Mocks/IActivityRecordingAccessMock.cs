﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spark.ActivityRecording.Processor.ValueObjects;
using Spark.Common.Adapter;
using Spark.REST.Entities.Purchase;
using Spark.Rest.V2.DataAccess;

namespace Spark.REST.V2.Test.Mocks
{
    public class IActivityRecordingAccessMock : IActivityRecordingAccess
    {

        public Stack<Hashtable> LoggedActivites = new Stack<Hashtable>();

        public void RecordActivity(int memberId, int targetMemberId, int actionType, int brandId, Dictionary<string, string> captionParameters, CallingSystem callingSystem)
        {
            throw new NotImplementedException();
        }

        public void RecordActivity(int memberId, int targetMemberId, int actionType, int brandId, string caption, CallingSystem callingSystem)
        {
            throw new NotImplementedException();
        }

        public void RecordActivityAsynch(int memberId, int targetMemberId, int actionType, int brandId, string caption)
        {
            throw new NotImplementedException();
        }

        public void RecordActivityAsynch(int memberId, int targetMemberId, int actionType, int brandId, Dictionary<string, string> captionParameters)
        {
            throw new NotImplementedException();
        }

        public void RecordAuthenticationAttemptActivity(string loginSessionId, string emailAddress, int memberId, int brandId, Matchnet.Member.ValueObjects.AuthenticationStatus authenticationStatus)
        {
            throw new NotImplementedException();
        }

        public void RecordLatestInAppReceiptActivity(int memberId, int brandId, IOSInAppInfo latestReceiptInfo)
        {
            Hashtable h = new Hashtable();
            h[CaptionConstants.IAP_CANCELLATION_DATE] = latestReceiptInfo.cancellation_date;
            h[CaptionConstants.IAP_EXPIRES_DATE] = latestReceiptInfo.expires_date;
            h[CaptionConstants.IAP_IS_TRIAL_PERIOD] = latestReceiptInfo.is_trial_period;
            h[CaptionConstants.IAP_ORIGINAL_PURCHASE_DATE] = latestReceiptInfo.original_purchase_date;
            h[CaptionConstants.IAP_ORIGINAL_TRANSACTION_ID] = latestReceiptInfo.original_transaction_id;
            h[CaptionConstants.IAP_PRODUCT_ID] = latestReceiptInfo.product_id;
            h[CaptionConstants.IAP_PURCHASE_DATE] = latestReceiptInfo.purchase_date;
            h[CaptionConstants.IAP_QUANTITY] = latestReceiptInfo.quantity;
            h[CaptionConstants.IAP_TRANSACTION_ID] = latestReceiptInfo.transaction_id;
            h[CaptionConstants.IAP_WEB_ORDER_LINE_ITEM_ID] = latestReceiptInfo.web_order_line_item_id;
            h[CaptionConstants.IAP_HAS_EXCEPTION] = Boolean.FalseString;
            h[CaptionConstants.IAP_EXCEPTION_MESSAGE] = string.Empty;
            LoggedActivites.Push(h);
        }

        public void RecordStatusForInAppReceiptActivity(int memberId, int brandId, IAPControllerStatus iapStatus, IOSInAppInfo latestReceiptInfo)
        {
            Hashtable h = new Hashtable();
            h[CaptionConstants.IAP_CONTROLLER_STATUS] = iapStatus;
            h[CaptionConstants.IAP_CANCELLATION_DATE] = latestReceiptInfo.cancellation_date;
            h[CaptionConstants.IAP_EXPIRES_DATE] = latestReceiptInfo.expires_date;
            h[CaptionConstants.IAP_IS_TRIAL_PERIOD] = latestReceiptInfo.is_trial_period;
            h[CaptionConstants.IAP_ORIGINAL_PURCHASE_DATE] = latestReceiptInfo.original_purchase_date;
            h[CaptionConstants.IAP_ORIGINAL_TRANSACTION_ID] = latestReceiptInfo.original_transaction_id;
            h[CaptionConstants.IAP_PRODUCT_ID] = latestReceiptInfo.product_id;
            h[CaptionConstants.IAP_PURCHASE_DATE] = latestReceiptInfo.purchase_date;
            h[CaptionConstants.IAP_QUANTITY] = latestReceiptInfo.quantity;
            h[CaptionConstants.IAP_TRANSACTION_ID] = latestReceiptInfo.transaction_id;
            h[CaptionConstants.IAP_WEB_ORDER_LINE_ITEM_ID] = latestReceiptInfo.web_order_line_item_id;
            LoggedActivites.Push(h);
        }

        public void RecordLoginFraudCallActivity(string loginSessionId, int brandId, string emailAddress, bool callSuccess, Spark.LoginFraud.FraudActionType loginFraudStatus, int loginFraudRejectionReason)
        {
            throw new NotImplementedException();
        }

        public void RecordPasswordResetAttemptActivity(string loginSessionId, string emailAddress, bool resetSuccessful, int memberId, int brandId)
        {
            throw new NotImplementedException();
        }

        public void RecordPasswordResetRequestActivity(string loginSessionId, string emailAddress, bool requestSuccessful, int memberId, int brandId)
        {
            throw new NotImplementedException();
        }

        public void RecordLatestInAppBillingActivity(int memberId, int brandId, Rest.V2.Entities.Purchase.AndroidSubscription androidSubscription)
        {
            throw new NotImplementedException();
        }

        public void RecordStatusForInAppBillingActivity(int memberId, int brandId, IAPControllerStatus iapStatus, Rest.V2.Entities.Purchase.AndroidSubscription androidSubscription)
        {
            throw new NotImplementedException();
        }


        public void RecordLatestInAppReceiptActivityException(int memberId, int brandId, Exception ex)
        {
            Hashtable h = new Hashtable();
            h[CaptionConstants.IAP_CANCELLATION_DATE] = string.Empty;
            h[CaptionConstants.IAP_EXPIRES_DATE] = string.Empty;
            h[CaptionConstants.IAP_IS_TRIAL_PERIOD] = string.Empty;
            h[CaptionConstants.IAP_ORIGINAL_PURCHASE_DATE] = string.Empty;
            h[CaptionConstants.IAP_ORIGINAL_TRANSACTION_ID] = string.Empty;
            h[CaptionConstants.IAP_PRODUCT_ID] = string.Empty;
            h[CaptionConstants.IAP_PURCHASE_DATE] = string.Empty;
            h[CaptionConstants.IAP_QUANTITY] = string.Empty;
            h[CaptionConstants.IAP_TRANSACTION_ID] = string.Empty;
            h[CaptionConstants.IAP_WEB_ORDER_LINE_ITEM_ID] = string.Empty;
            h[CaptionConstants.IAP_HAS_EXCEPTION] = Boolean.TrueString;
            h[CaptionConstants.IAP_EXCEPTION_MESSAGE] = ex.Message;
            LoggedActivites.Push(h);
        }

        public void RecordLatestInAppBillingActivityException(int memberId, int brandId, Exception ex)
        {
            throw new NotImplementedException();
        }
    }
}
