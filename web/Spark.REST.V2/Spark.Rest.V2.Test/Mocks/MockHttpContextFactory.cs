using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.SessionState;

namespace Spark.REST.V2.Test.Mocks
{
    public class MockHttpContextFactory
    {

        public static HttpContextBase GetMockHttpContext(string url, string queryString, string originIp, HttpResponseBase response)
        {
            var httpRequest = new MockHttpRequest(url, queryString);
            httpRequest.AddHeader("X-Spark-OriginIP", originIp);
            var httpContext = new MockHttpContext(httpRequest, response);
            httpRequest.RequestContext = new RequestContext(httpContext, new RouteData());

            var sessionContainer = new HttpSessionStateContainer("id", new SessionStateItemCollection(),
                new HttpStaticObjectsCollection(), 10, true,
                HttpCookieMode.AutoDetect,
                SessionStateMode.InProc, false);

            httpContext.Items["AspSession"] = typeof (HttpSessionState).GetConstructor(
                BindingFlags.NonPublic | BindingFlags.Instance,
                null, CallingConventions.Any,
                new[] {typeof (HttpSessionStateContainer)},
                null)
                .Invoke(new object[] {sessionContainer});

            return httpContext;
        }
    }
}