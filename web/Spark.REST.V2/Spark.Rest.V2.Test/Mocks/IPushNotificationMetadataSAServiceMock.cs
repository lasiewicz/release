﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.PushNotification;

namespace Spark.REST.V2.Test.Mocks
{
    public class IPushNotificationMetadataSAServiceMock : IPushNotificationMetadataSAService
    {
        public PushNotificationAppGroupModel NotificationAppGroupModel { get; set; }
        
        public PushNotificationAppGroupModel GetPushNotificationAppGroupModel(AppGroupID appId)
        {
            return NotificationAppGroupModel;
        }
    }
}
