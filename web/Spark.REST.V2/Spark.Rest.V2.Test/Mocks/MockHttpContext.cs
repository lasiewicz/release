﻿using System.Collections;
using System.Web;

namespace Spark.REST.V2.Test.Mocks
{
    public class MockHttpContext : HttpContextBase
    {
        private readonly HttpRequestBase _httpRequestBase;
        private readonly HttpResponseBase _httpResponseBase;
        private readonly IDictionary _items = new Hashtable();
        public MockHttpContext(HttpRequestBase httpRequestBase, HttpResponseBase httpResponseBase)
        {
            _httpRequestBase = httpRequestBase;
            _httpResponseBase = httpResponseBase;
        }

        public override HttpRequestBase Request
        {
            get { return _httpRequestBase; }
        }

        public override HttpResponseBase Response
        {
            get { return _httpResponseBase; }
        }

        public override IDictionary Items
        {
            get { return _items; }
        }
    }
}