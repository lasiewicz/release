using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet.ActivityRecording.ServiceAdapters;

namespace Spark.REST.V2.Test.Mocks
{
    public class MockActivityRecordingService : IActivityRecordingSA
    {

        public Stack<Hashtable> logApiRequests = new Stack<Hashtable>();

        #region IActivityRecordingSA Members
       
        public void LogApiRequest(string originIP, string clientIP, string url, int httpStatusCode, int httpSubStatusCode, string method, int appID, int memberID, long totalEllapsedRequestTime, int brandID, string host, string machineName, string actionName, string formDataXML, string requestBodyXML)
        {
            Hashtable h = new Hashtable();
            h["originIP"] = originIP;
            h["clientIP"] = clientIP;
            h["url"] = url;
            h["httpStatusCode"] = httpStatusCode;
            h["httpSubStatusCode"] = httpSubStatusCode;
            h["method"] = method;
            h["appID"] = appID;
            h["memberID"] = memberID;
            h["totalEllapsedRequestTime"] = totalEllapsedRequestTime;
            h["brandID"] = brandID;
            h["host"] = host;
            h["machineName"] = machineName;
            h["actionName"] = actionName;
            h["formDataXML"] = formDataXML;
            h["requestBodyXML"] = requestBodyXML;
            logApiRequests.Push(h);
        }

        public void RecordActivity(int memberID, int targetMemberID, int siteID, Matchnet.ActivityRecording.ValueObjects.Types.ActionType actionType, Matchnet.ActivityRecording.ValueObjects.Types.CallingSystem callingSystem, string activityCaption)
        {
            throw new NotImplementedException();
        }

        public void RecordRegistrationComplete(string regSessionID, int memberID, DateTime updateDate)
        {
            throw new NotImplementedException();
        }

        public void RecordRegistrationStart(string regSessionID, string regFormFactor, int regApplication, int siteID, int scenarioID, int ipAddress, DateTime insertDate)
        {
            throw new NotImplementedException();
        }

        public void RecordRegistrationStep(string regSessionID, int stepID, Dictionary<string, object> stepDetails, DateTime insertDate)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}