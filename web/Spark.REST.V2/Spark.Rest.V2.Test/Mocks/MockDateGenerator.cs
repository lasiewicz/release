using System;
using Spark.Rest.DataAccess.Purchase;

namespace Spark.REST.V2.Test.Mocks
{
    public class MockDateGenerator : IDateGenerator
    {
        public int DayOffSet { get; set; }

        public DateTime LocalNow
        {
            get { return DateTime.Now.AddDays(DayOffSet); }
        }

        public DateTime UTCNow
        {
            get { return DateTime.UtcNow.AddDays(DayOffSet); }
        }
    }
}