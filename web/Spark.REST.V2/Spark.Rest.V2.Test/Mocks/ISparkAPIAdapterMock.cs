﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Spark.Common.Adapter;
using Spark.Rest.DataAccess.Purchase;
using Spark.REST.Entities.Purchase;

namespace Spark.REST.V2.Test.Mocks
{
    public class ISparkAPIAdapterMock : ISparkAPIAdapter
    {
        public IOSReceipt ReturnIosReceipt { get; set; }

        public IOSReceipt GetIOSReceipt(Matchnet.Content.ValueObjects.BrandConfig.Brand brand, string encodedReceiptData, int memberId)
        {
            return ReturnIosReceipt;
        }

        public APIAdapter.RestStatus MakeIOSApiCallByClientId(string encodedReceiptData, out IOSReceipt iosReceipt, int clientId)
        {
            iosReceipt = ReturnIosReceipt;
            return new APIAdapter.RestStatus() { StatusCode = HttpStatusCode.OK, ValidResponse = true };
        }

        public APIAdapter.RestStatus MakeIOSApiCallBySiteId(string encodedReceiptData, out IOSReceipt iosReceipt, int siteId)
        {
            iosReceipt = ReturnIosReceipt;
            return new APIAdapter.RestStatus() { StatusCode = HttpStatusCode.OK, ValidResponse = true };
        }

        public APIAdapter.RestStatus MakeAndroidApiCallByClientId(string subscriptionToken, out Rest.V2.Entities.Purchase.AndroidSubscription androidSubscription, int clientId, GoogleApiCall googleApiCall)
        {
            throw new NotImplementedException();
        }

        public APIAdapter.RestStatus MakeAndroidApiCallBySiteId(string subscriptionToken, out Rest.V2.Entities.Purchase.AndroidSubscription androidSubscription, int siteId, GoogleApiCall googleApiCall)
        {
            throw new NotImplementedException();
        }
    }
}
