﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Matchnet.Helpers;

namespace Spark.REST.V2.Test.Mocks
{
    public class IHttpContextHelperMock : IHttpContextHelper
    {
        private HttpContextBase _mockHttpContextBase;

        public HttpContextBase CurrentHttpContextBase
        {
            get { return _mockHttpContextBase; }
            set { _mockHttpContextBase = value; }
        }
    }
}
