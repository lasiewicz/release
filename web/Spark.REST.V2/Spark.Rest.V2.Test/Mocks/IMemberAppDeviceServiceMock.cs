﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spark.Rest.V2.DataAccess.MemberDevices;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ValueObjects.MemberAppDevices;


namespace Spark.REST.V2.Test.Mocks
{
    public class IMemberAppDeviceServiceMock : IMemberAppDeviceService  
    {
        public MemberAppDeviceCollection DeviceCollection { get; set; }
        public MemberAppDevice AddedDevice { get; set; }
        public MemberAppDevice UpdatedDevice { get; set; }
        
        public void AddMemberAppDevice(int memberID, int appID, string deviceID, int brandID, string deviceData, long pushNotificationsFlags)
        {
            AddedDevice = new MemberAppDevice { MemberID = memberID, AppID = appID, DeviceID = deviceID, BrandID = brandID, DeviceData = deviceData, PushNotificationsFlags = pushNotificationsFlags };
        }

        public MemberAppDeviceCollection GetMemberAppDevices(int memberID)
        {
            return DeviceCollection; 
        }

        public void DeleteMemberAppDevice(int memberID, int appID, string deviceID)
        {
            return;
        }

        public void UpdateMemberAppDeviceNotificationsFlags(int memberID, int appID, string deviceID, long pushNotificationsFlags)
        {
            UpdatedDevice = new MemberAppDevice { MemberID = memberID, AppID = appID, DeviceID = deviceID, PushNotificationsFlags = pushNotificationsFlags };
        }
    }
}
