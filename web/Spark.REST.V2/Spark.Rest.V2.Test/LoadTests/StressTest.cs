﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using System.Web.SessionState;
using Matchnet.Search.Interfaces;
using Matchnet.Session.ServiceAdapters;
using NUnit.Framework;
using Spark.REST.Configuration;
using Spark.Rest.Helpers;
using Spark.REST.Models.Search;
using Spark.REST.V2.Test.Mocks;

namespace Spark.REST.V2.Test.LoadTests
{
    [TestFixture]
    public class StressTest
    {
        private Random _random = new Random();
        private IHttpContextHelperMock httpContextHelperMock = new IHttpContextHelperMock();
        private HttpContextBase mockHttpContextBase = null;
        private int memberId = 100046782;

        [TestFixtureSetUp]
        public void StartUp()
        {
            mockHttpContextBase = GetMockHttpContext();
            httpContextHelperMock.CurrentHttpContextBase = mockHttpContextBase;
            SessionSA.HttpContextHelper = httpContextHelperMock;
            ErrorHelper.HttpContextHelper = httpContextHelperMock;
            AttributeConfigReader.AttributeSetXmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../Spark.Rest.V2/Configuration/AttributeSetList.xml");
            AttributeConfigReader.PropertyWhitelistXmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../Spark.Rest.V2/Configuration/AttributeWhitelist.xml");

            var attributeSetDict = AttributeConfigReader.Instance.AttributeSetDictionary;
            var propertyWhitelist = AttributeConfigReader.PropertyWhitelist;
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            ErrorHelper.HttpContextHelper = null;
            SessionSA.HttpContextHelper = null;
        }

        [SetUp]
        public void setUp()
        {}

        [TearDown]
        public void tearDown()
        {}


        [Test]
        public void TestStressOnSearchController()
        {
            int brandId=1003;
            List<IStressTestable> runners = new List<IStressTestable>();
            bool useJdate = true;
            for (int i = 0; i < 1000; i++)
            {
                runners.Add(new DistributedCacheRunner(createSearchResultsRequest(brandId, i), mockHttpContextBase));
            }
            decimal avgSearchTime = 0l;
            using (StressTester tester = new StressTester(runners))
            {
                tester.RunConcurrently();
                while (!tester.IsAllDone())
                {
                    System.Threading.Thread.Sleep(500);
                }
                avgSearchTime=tester.AvgThreadTime();
            }

            int idx = 0;
            Console.WriteLine("\nAvg Search Time = " + avgSearchTime +" ms");
            foreach (IStressTestable testable in runners)
            {                                
                Assert.IsTrue(testable.RunSuceeded, "Failed at runner[" + idx + "], "+testable.ElapsedMillis + " elapsed millis.");
                Assert.Less(testable.ElapsedMillis, 60000, "Failed at runner["+idx+"]");
                idx++;
            }
        }

        private SearchResultsRequest createSearchResultsRequest(int brandId, int idx)
        {
            SearchResultsRequest searchPrefs = new SearchResultsRequest();
            searchPrefs.PageNumber = (idx % 2)+1;
            searchPrefs.PageSize = 15;
            searchPrefs.ShowOnlyMembersWithPhotos = (idx%2 == 0);
            searchPrefs.MaxDistance=((idx % 4) * 40);
            searchPrefs.SearchType=(idx % 2 == 0) ? SearchType.APISearch : SearchType.WebSearch;            
//            searchPrefs.SearchOrderByType= (idx % 2 == 0) ? QuerySorting.Proximity : QuerySorting.KeywordRelevance;
            searchPrefs.SearchOrderByType = QuerySorting.Proximity;
            searchPrefs.BrandId = brandId;
            searchPrefs.MinAge = ((idx % 3 + 1) * 5) + 10;
            searchPrefs.MaxAge = ((idx % 3 + 1) * 25) + 20;
            if (idx%2 == 0)
            {
                searchPrefs.Gender = "male";
                searchPrefs.SeekingGender = "female";
            }
            else
            {
                searchPrefs.SeekingGender = "male";
                searchPrefs.Gender = "female";                
            }

            int loc = (idx % 5);
            switch (loc)
            {
                case 0:
                    //beverly hills (zip=90211)
                    searchPrefs.RegionId = 3443821;
                    break;
                case 1:
                    //provo, ut (zip=84601)
                    searchPrefs.RegionId = 3479904;
                    break;
                case 2:
                    //paris, ontario
                    searchPrefs.RegionId = 2639301;
                    break;
                default:
                    //new york 3000 miles away (zip=10017)
                    searchPrefs.RegionId = 3466331;
                    break;
            }

//            int blockedIds = (idx % 5);
//            switch (blockedIds)
//            {
//                case 0:
//                    searchPrefs.Add("blockedmemberids", "140135545,118983987,140139505,40607559,133431636,119082848,139753450,119294684,118819917,115564075,132734885,103428161,126801553,114653949");
//                    break;
//                case 1:
//                    searchPrefs.Add("blockedmemberids", "119484824,119477759,119503394,119455296,126874963,126412708,126829225,119470445,119207058,126636670,119526935,126998623,119551985,48796521,126731107,118931349,119605262,115506774,119454474,119400362,119681468,119367221,119697683,126995557,126966439,119369433,119679182,109494815,119345774,119692067,110831390,119206320,126677941,126990805,119639546,119429048,119435987,126784252,119454545,119240508,126730684,119106327,119496699,119650121,119342391,118903334,119579921,119662796,119647589,119552021,119322572,126703129,119634959,119205849,119354219,126620995,127024804,126675493,126733249,119368826,126673381,126955018,126872929,119595278,119510522,119123010,119327915,126677716,126912547,126768610,126647269,119317014,119174388,119144730,126958210,126863047,126695899,127004041,119452986,119567108,119321184,126711286,119607101,119595590,126709012,119695223,126949201,119391030");
//                    break;
//                case 2:
//                    searchPrefs.Add("blockedmemberids", "114994142, 126956941, 118579617, 119335527, 115581807, 115435227, 106479126, 115408851, 49679916, 105679155, 30572128,106010805, 112611891, 53541306, 578453, 102444640, 114568138, 126707251, 108862841, 126777571, 126191869, 41827164, 30797871, 110572975, 101331909, 8428226, 7798235, 102998738, 115185314, 119672114, 119578988, 9417917, 40003042, 104864537, 45598897, 49023878, 51022114, 126695698, 30150119, 113113369, 49824581, 119317380, 116574453, 115181335, 114100327, 13058398, 126114760, 119476316, 110705727, 108958996, 126991414, 18528303, 119214570, 114362199, 6141899, 119243213, 114885357, 53935069, 53410959, 48105170, 114330116, 126627268, 114255162, 126592777, 114054561, 126782683,114411678, 119172704");
//                    break;
//                case 3:
//                    searchPrefs.Add("blockedmemberids", "54102155,8127862");
//                    break;
//                default:
//                    break;
//            }

//            int keywords = (idx % 5);
//            switch (keywords)
//            {
//                case 0:
//                    searchPrefs.Keywords="Blond, blue, slender";
//                    break;
//                case 1:
//                    searchPrefs.Keywords="good sense of humor";
//                    break;
//                case 2:
//                    searchPrefs.Keywords="A tttractive well educated women";
//                    break;
//                case 3:
//                    searchPrefs.Keywords="KOSHER";
//                    break;
//                case 4:
//                    searchPrefs.Keywords="the beach";
//                    break;
//                default:
////                    searchPrefs.Add("keywordsearch", "assertive");
//                    break;
//            }

            return searchPrefs;
        }

        private HttpContextBase GetMockHttpContext()
        {
            string urlHost = "http://api.local.spark.net";
            string urlPath = "/v2/brandId/1003/search/results";
            string queryString = "access_token=2%2FIAoxvbNtDPSLlTRQDYAo3mTKcloIe7j%2B47skHAhj7eg%3D";
            var stringWriter = new StringWriter();
            HttpResponse httpResponse = new HttpResponse(stringWriter);
            httpResponse.StatusCode = (int)HttpStatusCode.OK;
            string originIp = "192.168.1.141";
            var mockHttpContext = MockHttpContextFactory.GetMockHttpContext(urlHost + urlPath, queryString, originIp,
                new HttpResponseWrapper(httpResponse));
            mockHttpContext.Items["tokenMemberId"] = memberId;
            return mockHttpContext;
        }


    }
}
