﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Spark.REST.Controllers;
using Spark.REST.Entities.Search;
using Spark.REST.Models.Search;
using Spark.Rest.Serialization;
using Spark.REST.V2.Test.Mocks;

namespace Spark.REST.V2.Test.LoadTests
{
    public class DistributedCacheRunner : IStressTestable
    {
        SearchController searchController = null;
        private SearchResultsRequest _resultsRequest = null;
        private ReaderWriterLock _externalLock = null;
        private long _ellapsedMillis = 0;

        private bool _runSucceeded = false;
        private bool _isDone = false;

        public bool RunSuceeded
        {
            get { return _runSucceeded; }
        }

        public long ElapsedMillis
        {
            get { return _ellapsedMillis; }
        }

        public bool IsDone
        {
            get { return _isDone; }
        }

        public DistributedCacheRunner(SearchResultsRequest searchResultsRequest, HttpContextBase httpContext)
        {
            _resultsRequest = searchResultsRequest;
            _externalLock = new ReaderWriterLock();
            searchController = new SearchController();
            searchController.ControllerContext = new ControllerContext(new RequestContext(httpContext, new RouteData()), searchController);
        }

        public void Prepare(){}

        public void Run()
        {
            try
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                NewtonsoftJsonResult searchResultsViaPostV2 = (NewtonsoftJsonResult)searchController.GetSearchResultsViaPostV2(_resultsRequest);
                stopWatch.Stop();

                _ellapsedMillis = stopWatch.ElapsedMilliseconds;
                int results = (null == searchResultsViaPostV2.Result) ? 0 : ((SearchResultsV2)searchResultsViaPostV2.Result).Members.Count;
                Console.WriteLine("Thread:{0}, Results:{1}, Ellapsed Time:{2} ms",this.GetHashCode(),results,ElapsedMillis);
                this._runSucceeded = true;
            }
            catch (Exception e)
            {
                Console.WriteLine("\n" + e.Message);
                throw e;
            }
            finally
            {
                this._isDone = true;
            }
        }
    }
}
