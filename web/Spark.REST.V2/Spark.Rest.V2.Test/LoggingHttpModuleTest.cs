﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web;
using NUnit.Framework;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.V2.Test.Mocks;
 

namespace Spark.Rest.Modules
{
    [TestFixture]
    public class LoggingHttpModuleTest
    {


        [TestFixtureSetUp]
        public void SetUp()
        {
            LoggingHttpModule.LogApiRequest = new MockLogger();
            LoggingHttpModule.ActivityRecordingService = new MockActivityRecordingService();
        }
        
        //Use ClassCleanup to run code after all tests in a class have run
        [TestFixtureTearDown]
        public void TearDown()
        {
            LoggingHttpModule.LogApiRequest = null;
            LoggingHttpModule.ActivityRecordingService = null;
        }

        [Test]
        public void Log400Error()
        {
            string urlHost = "http://api.local.spark.net";
            string urlPath = "/v2/brandId/1003/profile/miniProfile/0";
            string queryString = "access_token=1%2FF6BgNEpHyz%2BAjgjTR9An8X7E8%2F16DA3FeqgVbYF%2BIe4%";
            var stringWriter = new StringWriter();
            HttpResponse httpResponse = new HttpResponse(stringWriter);
            httpResponse.StatusCode = (int)HttpStatusCode.BadRequest;
            if (HttpRuntime.UsingIntegratedPipeline)
            {
                httpResponse.SubStatusCode = (int) HttpSub400StatusCode.InvalidMemberId;
            }
            string originIp = "192.168.1.141";
            var mockHttpContext = MockHttpContextFactory.GetMockHttpContext(urlHost + urlPath, queryString, originIp, new HttpResponseWrapper(httpResponse));
            mockHttpContext.Items["_requestBody_"] = @"{'person': [{'@id': '1','name': 'Alan','url': 'http://www.google.com'},{'@id': '2','name': 'Louis','url': 'http://www.yahoo.com'}]}";
            MockHttpRequest mockHttpRequest = (MockHttpRequest)mockHttpContext.Request;
            mockHttpRequest.MockHttpMethod = HttpMethod.Post;
            mockHttpRequest.ContentType = "application/json";
            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("memberid","100004579");
            nameValueCollection.Add("username", "jasonfallon");
            nameValueCollection.Add("lookingFor","2,4,8,64");
            mockHttpRequest.MockForm = nameValueCollection;
            LoggingHttpModule.LogInfo(mockHttpContext);
            Thread.Sleep(1000);

            string infoMessage = ((MockLogger) LoggingHttpModule.LogApiRequest).logDebugMessages.Pop();
            Assert.IsNotNull(infoMessage);
            Assert.IsTrue(infoMessage.IndexOf(urlPath) > -1);
            Assert.IsTrue(infoMessage.IndexOf(queryString) > -1);
            Assert.IsTrue(infoMessage.IndexOf("OriginIP: "+originIp) > -1);
            Assert.IsTrue(infoMessage.IndexOf("Http Status Code: "+httpResponse.StatusCode) > -1);

            Hashtable h = ((MockActivityRecordingService) LoggingHttpModule.ActivityRecordingService).logApiRequests.Pop();
            Console.WriteLine("Form Data Xml = " + h["formDataXML"]);
            Console.WriteLine("Content Body Xml = " + h["requestBodyXML"]);
//            Assert.IsNotNull(h);
//            Assert.AreEqual(h["originIP"], originIp);
//            Assert.AreEqual(h["url"], urlPath+"?"+queryString);
//            Assert.AreEqual(h["httpStatusCode"], httpResponse.StatusCode);
        }

        [Test]
        public void LogBadXMLName()
        {

            string urlHost = "http://api.local.spark.net";
            string urlPath = "/v2/brandId/1003/search/results/";
            string queryString = "access_token=2/lDUSlCObT4ojiSwBM3/d7l16uI2NrVoMF5o9sQgak8c=";
            var stringWriter = new StringWriter();
            HttpResponse httpResponse = new HttpResponse(stringWriter);
            httpResponse.StatusCode = (int)HttpStatusCode.BadRequest;
            if (HttpRuntime.UsingIntegratedPipeline)
            {
                httpResponse.SubStatusCode = (int)HttpSub400StatusCode.InvalidMemberId;
            }
            string originIp = "192.168.1.141";
            var mockHttpContext = MockHttpContextFactory.GetMockHttpContext(urlHost + urlPath, queryString, originIp, new HttpResponseWrapper(httpResponse));
            mockHttpContext.Items["_requestBody_"] = @"{'person': [{'@id': '1','name': 'Alan','url': 'http://www.google.com'},{'@id': '2','name': 'Louis','url': 'http://www.yahoo.com'}]}";
            MockHttpRequest mockHttpRequest = (MockHttpRequest)mockHttpContext.Request;
            mockHttpRequest.MockHttpMethod = HttpMethod.Post;
            mockHttpRequest.ContentType = "application/json";
            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("memberid", "100004579");
            nameValueCollection.Add("POST /v2/brandId/1003/search/results/?access_token", "2/lDUSlCObT4ojiSwBM3/d7l16uI2NrVoMF5o9sQgak8c= HTTP/1.1 Host: apps.api.spark.net Content-Type: application/x-www-form-urlencoded Connection: keep-alive Accept: application/json; version=V2.1 User-Agent");
            mockHttpRequest.MockForm = nameValueCollection;
            LoggingHttpModule.LogInfo(mockHttpContext);
            Thread.Sleep(1000);

            
            //            Assert.IsNotNull(h);
            //            Assert.AreEqual(h["originIP"], originIp);
            //            Assert.AreEqual(h["url"], urlPath+"?"+queryString);
            //            Assert.AreEqual(h["httpStatusCode"], httpResponse.StatusCode);



            //Making sure this gets to the DB 
            //<POST /v2/brandId/1003/search/results/?access_token>
                //<![CDATA[2/lDUSlCObT4ojiSwBM3/d7l16uI2NrVoMF5o9sQgak8c= HTTP/1.1 Host: apps.api.spark.net Content-Type: application/x-www-form-urlencoded Connection: keep-alive Accept: application/json; version=V2.1 User-Agent]]>
            //</POST /v2/brandId/1003/search/results/?access_token>
            //MiscUtils.FireAndForget(o => ActivityRecordingSA.Instance.LogApiRequest(originIP, clientIP, url, httpStatusCode, httpSubStatusCode, method, appID, memberID, totalEllapsedRequestTime, brandID, host, machineName, actionName, formDataXML, requestBodyXML),
            //            "LoggingHttpModule", "Could not log api requests to api web log!");

        }
    }
}