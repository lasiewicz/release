﻿using NUnit.Framework;
using Spark.REST.DataAccess.Content;

namespace Spark.REST.V2.Test.DataAccess
{
    
    
    /// <summary>
    ///This is a test class for RegionDataAccessTest and is intended
    ///to contain all RegionDataAccessTest Unit Tests
    ///</summary>
    [TestFixture]
    public class RegionDataAccessTest
    {

        [Test]
        public void GetCountriesTest()
        {
            int languageId = 2; // English
			var actual = RegionDataAccess.Instance.GetCountries(languageId);
            Assert.IsTrue(actual.Count > 0);
        }

        [Test]
        public void GetChildRegionsTest()
        {
            int parentRegionId = 1538; // California
            int languageId = 2; // English
			var actual = RegionDataAccess.Instance.GetChildRegions(parentRegionId, languageId);
            Assert.IsTrue(actual.Count > 1500);
        }

        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void GetStatesTest()
        {
            int countryRegionId = 223; // USA
            int languageId = 2; // English
			var actual = RegionDataAccess.Instance.GetStates(countryRegionId, languageId);
            Assert.IsTrue(actual[9].Description == "California");
        }

        [Test]
        public void GetCityListTest()
        {
            int countryRegionId = 223; 
            int stateRegionId = 1538; 
            int languageId = 2; // 
			var actual = RegionDataAccess.Instance.GetCities(countryRegionId, stateRegionId, languageId);
            Assert.IsTrue(actual.Count > 0);
        }
    }
}
