﻿using System;
using System.Collections.Generic;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Member.ValueObjects.MemberAppDevices;
using NUnit.Framework;
using Spark.Rest.V2.DataAccess.MemberDevices;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.V2.Test.Mocks;

namespace Spark.REST.V2.Test.DataAccess
{
    [TestFixture]
    public class MemberDeviceAccessTest
    {
        private const int _appModelId = 1016;
        private const AppGroupID _appGroupID = AppGroupID.JDateAppGroup;

        [Test]
        public void TestHappyPathAddMemberDevice()
        {
            var memberDeviceAccess = new MemberDeviceAccess();
            var brand = GetBrand(1003, 103, 3);

            IPushNotificationMetadataSAServiceMock metadataMock = new IPushNotificationMetadataSAServiceMock();
            metadataMock.NotificationAppGroupModel = GetNotificationAppGroupModel(_appGroupID);

            IMemberAppDeviceServiceMock deviceServiceMock = new IMemberAppDeviceServiceMock();

            memberDeviceAccess.MemberAppDeviceService = deviceServiceMock;
            memberDeviceAccess.PushNotificationMetadataService = metadataMock;

            var computedHash = MemberAppDeviceHelper.GenerateMemberIdHash(1234);
            var returnedHash = memberDeviceAccess.AddAppDevice(1234, _appModelId, brand, "1234abcd", "devicetype", "appversion", "osversion");

            Assert.AreEqual(deviceServiceMock.AddedDevice.MemberID, 1234);
            Assert.AreEqual(deviceServiceMock.AddedDevice.AppID, _appModelId);
            Assert.AreEqual(deviceServiceMock.AddedDevice.BrandID, 1003);
            Assert.AreEqual(deviceServiceMock.AddedDevice.DeviceID, "1234abcd");
            Assert.AreEqual(computedHash, returnedHash);
        }


        [Test]
        [ExpectedException(typeof(SparkAPIReportableException))]
        public void TestDeleteMemberDeviceWithNonExistingDeviceID()
        {
            var memberDeviceAccess = new MemberDeviceAccess();
            memberDeviceAccess.DeleteAppDevice(1, 1, "test");
        }

        [Test]
        [ExpectedException(typeof(SparkAPIReportableArgumentException))]
        public void TestDeleteMemberDeviceWithInvalidMemberID()
        {
            var memberDeviceAccess = new MemberDeviceAccess();
            memberDeviceAccess.DeleteAppDevice(0, 111, "test");
        }

        [Test]
        [ExpectedException(typeof(SparkAPIReportableArgumentException))]
        public void TestDeleteMemberDeviceWithInvalidAppID()
        {
            var memberDeviceAccess = new MemberDeviceAccess();
            memberDeviceAccess.DeleteAppDevice(1, 0, "test");
        }

        [Test]
        [ExpectedException(typeof(SparkAPIReportableArgumentException))]
        public void TestDeleteMemberDeviceWithEmptyString()
        {
            var memberDeviceAccess = new MemberDeviceAccess();
            memberDeviceAccess.DeleteAppDevice(1, 1, string.Empty);
        }


        [Test]
        [ExpectedException(typeof(SparkAPIException))]
        public void TestAddMemberDeviceThrowExceptionIfNotificationAppModelNull()
        {
            var memberDeviceAccess = new MemberDeviceAccess();
            var brand = GetBrand(1003, 103, 3);

            IPushNotificationMetadataSAServiceMock metadataMock = new IPushNotificationMetadataSAServiceMock();
            metadataMock.NotificationAppGroupModel = null;

            IMemberAppDeviceServiceMock deviceServiceMock = new IMemberAppDeviceServiceMock();
            deviceServiceMock.DeviceCollection = new MemberAppDeviceCollection();
            deviceServiceMock.DeviceCollection.AppDevices.Add(new MemberAppDevice { MemberID = 1234, BrandID = 1003, AppID = _appModelId, DeviceID = "1234abcd" });

            memberDeviceAccess.MemberAppDeviceService = deviceServiceMock;
            memberDeviceAccess.PushNotificationMetadataService = metadataMock;

            memberDeviceAccess.AddAppDevice(1234, _appModelId, brand, "1234abcd", "devicetype", "appversion", "osversion");
        }

        [Test]
        public void TestAddMemberDeviceWorksIfDeviceAlreadyExists()
        {
            var memberDeviceAccess = new MemberDeviceAccess();
            var brand = GetBrand(1003, 103, 3);

            IPushNotificationMetadataSAServiceMock metadataMock = new IPushNotificationMetadataSAServiceMock();
            metadataMock.NotificationAppGroupModel = GetNotificationAppGroupModel(_appGroupID);

            IMemberAppDeviceServiceMock deviceServiceMock = new IMemberAppDeviceServiceMock();
            deviceServiceMock.DeviceCollection = new MemberAppDeviceCollection();
            deviceServiceMock.DeviceCollection.AppDevices.Add(new MemberAppDevice { MemberID = 1234, BrandID = 1003, AppID = _appModelId, DeviceID = "1234abcd" });

            memberDeviceAccess.MemberAppDeviceService = deviceServiceMock;
            memberDeviceAccess.PushNotificationMetadataService = metadataMock;

            var computedHash = MemberAppDeviceHelper.GenerateMemberIdHash(1234);
            var returnedHash = memberDeviceAccess.AddAppDevice(1234, _appModelId, brand, "1234abcd", "devicetype", "appversion", "osversion");
            Assert.AreEqual(computedHash, returnedHash);
        }

        [Test]
        public void TestUpdateDeviceThrowExceptionIfDeviceDoesNotExist()
        {
            var memberDeviceAccess = new MemberDeviceAccess();
            var brand = GetBrand(1003, 103, 3);

            IPushNotificationMetadataSAServiceMock metadataMock = new IPushNotificationMetadataSAServiceMock();
            metadataMock.NotificationAppGroupModel = GetNotificationAppGroupModel(_appGroupID);

            IMemberAppDeviceServiceMock deviceServiceMock = new IMemberAppDeviceServiceMock();
            deviceServiceMock.DeviceCollection = new MemberAppDeviceCollection();
            deviceServiceMock.DeviceCollection.AppDevices.Add(new MemberAppDevice { MemberID = 1234, AppID = _appModelId, DeviceID = "1234" });

            memberDeviceAccess.MemberAppDeviceService = deviceServiceMock;
            memberDeviceAccess.PushNotificationMetadataService = metadataMock;

            var correctExceptionCaught = false;
            var correctExceptionStatusCode = false;

            try
            {
                memberDeviceAccess.UpdateDeviceNotificationSettings(1234, 1016, "1234abcd", PushNotificationCategoryId.Promotional, true);
            }
            catch(SparkAPIReportableException exception)
            {
                correctExceptionCaught = true;
                correctExceptionStatusCode = (exception.SubStatusCode== HttpSub400StatusCode.DeviceNotRegisteredToMember);
            }

            Assert.IsTrue(correctExceptionCaught);
            Assert.IsTrue(correctExceptionStatusCode);            
        }

        [Test]
        public void TestUpdateDeviceEnablesCategoryNotification()
        {
            var memberDeviceAccess = new MemberDeviceAccess();
            var brand = GetBrand(1003, 103, 3);

            IPushNotificationMetadataSAServiceMock metadataMock = new IPushNotificationMetadataSAServiceMock();
            metadataMock.NotificationAppGroupModel = GetNotificationAppGroupModel(_appGroupID);


            var existingFlagValue = (long)Math.Pow(2, (double)PushNotificationTypeID.PromotionalAnnouncement);
            var accountCategoryNotificationValue = ((long)Math.Pow(2, (double)PushNotificationTypeID.PhotoApproved)) +
                ((long)Math.Pow(2, (double)PushNotificationTypeID.PhotoRejected));

            IMemberAppDeviceServiceMock deviceServiceMock = new IMemberAppDeviceServiceMock();
            deviceServiceMock.DeviceCollection = new MemberAppDeviceCollection();
            deviceServiceMock.DeviceCollection.AppDevices.Add(new MemberAppDevice { MemberID = 1234, AppID = _appModelId, DeviceID = "1234", PushNotificationsFlags = existingFlagValue });

            memberDeviceAccess.MemberAppDeviceService = deviceServiceMock;
            memberDeviceAccess.PushNotificationMetadataService = metadataMock;

            memberDeviceAccess.UpdateDeviceNotificationSettings(1234, 1016, "1234", PushNotificationCategoryId.Photos, true);
            Assert.AreEqual(deviceServiceMock.UpdatedDevice.PushNotificationsFlags, existingFlagValue + accountCategoryNotificationValue);
        }

        [Test]
        public void TestUpdateDeviceDisablesCategoryNotification()
        {
            var memberDeviceAccess = new MemberDeviceAccess();
            var brand = GetBrand(1003, 103, 3);

            IPushNotificationMetadataSAServiceMock metadataMock = new IPushNotificationMetadataSAServiceMock();
            metadataMock.NotificationAppGroupModel = GetNotificationAppGroupModel(_appGroupID);


            var existingFlagValue = ((long)Math.Pow(2, (double)PushNotificationTypeID.PhotoRejected)) +
                ((long)Math.Pow(2, (double)PushNotificationTypeID.PhotoApproved))
                + ((long)Math.Pow(2, (double)PushNotificationTypeID.PromotionalAnnouncement));
            var accountCategoryNotificationValue = ((long)Math.Pow(2, (double)PushNotificationTypeID.PhotoApproved)) +
                ((long)Math.Pow(2, (double)PushNotificationTypeID.PhotoRejected));

            IMemberAppDeviceServiceMock deviceServiceMock = new IMemberAppDeviceServiceMock();
            deviceServiceMock.DeviceCollection = new MemberAppDeviceCollection();
            deviceServiceMock.DeviceCollection.AppDevices.Add(new MemberAppDevice { MemberID = 1234, AppID = _appModelId, DeviceID = "1234", PushNotificationsFlags = existingFlagValue });

            memberDeviceAccess.MemberAppDeviceService = deviceServiceMock;
            memberDeviceAccess.PushNotificationMetadataService = metadataMock;

            memberDeviceAccess.UpdateDeviceNotificationSettings(1234, 1016, "1234", PushNotificationCategoryId.Photos, false);
            Assert.AreEqual(deviceServiceMock.UpdatedDevice.PushNotificationsFlags, existingFlagValue - accountCategoryNotificationValue);
        }

        [Test]
        public void TestUpdateDeviceDoesntFailIfCategoryNotificationAlreadyEnabled()
        {
            var memberDeviceAccess = new MemberDeviceAccess();
            var brand = GetBrand(1003, 103, 3);

            IPushNotificationMetadataSAServiceMock metadataMock = new IPushNotificationMetadataSAServiceMock();
            metadataMock.NotificationAppGroupModel = GetNotificationAppGroupModel(_appGroupID);


            var existingFlagValue = ((long)Math.Pow(2, (double)PushNotificationTypeID.PhotoApproved)) +
                ((long)Math.Pow(2, (double)PushNotificationTypeID.PhotoRejected));
            var accountCategoryNotificationValue = ((long)Math.Pow(2, (double)PushNotificationTypeID.PhotoApproved)) +
                ((long)Math.Pow(2, (double)PushNotificationTypeID.PhotoRejected));

            IMemberAppDeviceServiceMock deviceServiceMock = new IMemberAppDeviceServiceMock();
            deviceServiceMock.DeviceCollection = new MemberAppDeviceCollection();
            deviceServiceMock.DeviceCollection.AppDevices.Add(new MemberAppDevice { MemberID = 1234, AppID = _appModelId, DeviceID = "1234", PushNotificationsFlags = existingFlagValue });

            memberDeviceAccess.MemberAppDeviceService = deviceServiceMock;
            memberDeviceAccess.PushNotificationMetadataService = metadataMock;

            memberDeviceAccess.UpdateDeviceNotificationSettings(1234, 1016, "1234", PushNotificationCategoryId.Photos, true);
            Assert.AreEqual(deviceServiceMock.UpdatedDevice.PushNotificationsFlags, accountCategoryNotificationValue);
        }

        [Test]
        public void TestUpdateDeviceDoesntFailIfCategoryNotificationAlreadyDisabled()
        {
            var memberDeviceAccess = new MemberDeviceAccess();
            var brand = GetBrand(1003, 103, 3);

            IPushNotificationMetadataSAServiceMock metadataMock = new IPushNotificationMetadataSAServiceMock();
            metadataMock.NotificationAppGroupModel = GetNotificationAppGroupModel(_appGroupID);


            var existingFlagValue = 0;
            var accountCategoryNotificationValue = ((long)Math.Pow(2, (double)PushNotificationTypeID.PhotoApproved)) +
                ((long)Math.Pow(2, (double)PushNotificationTypeID.PhotoRejected));

            IMemberAppDeviceServiceMock deviceServiceMock = new IMemberAppDeviceServiceMock();
            deviceServiceMock.DeviceCollection = new MemberAppDeviceCollection();
            deviceServiceMock.DeviceCollection.AppDevices.Add(new MemberAppDevice { MemberID = 1234, AppID = _appModelId, DeviceID = "1234", PushNotificationsFlags = existingFlagValue });

            memberDeviceAccess.MemberAppDeviceService = deviceServiceMock;
            memberDeviceAccess.PushNotificationMetadataService = metadataMock;

            memberDeviceAccess.UpdateDeviceNotificationSettings(1234, 1016, "1234", PushNotificationCategoryId.Photos, false);
            Assert.AreEqual(deviceServiceMock.UpdatedDevice.PushNotificationsFlags, existingFlagValue);
        }

        private Brand GetBrand(int brandId, int siteId, int communityId)
        {
            var community = new Community(communityId, string.Empty);
            var site = new Site(siteId, community, string.Empty, 1, 1, string.Empty, 1, 1, 1, string.Empty, 1, DirectionType.ltr, 1, string.Empty, string.Empty, 1);
            return new Brand(brandId, site, string.Empty, 1, string.Empty, 1, 1, 1);
        }

        private PushNotificationAppGroupModel GetNotificationAppGroupModel(AppGroupID appModelId)
        {
            //dummied up for now
            var model = new PushNotificationAppGroupModel();
            model.AppGroupId = appModelId;
            model.NotificationAppGroupCategories = new List<PushNotificationAppGroupCategory>();

            var activityCategory = new PushNotificationAppGroupCategory();
            activityCategory.AppGroupId = appModelId;
            activityCategory.Category = new PushNotificationCategory();
            activityCategory.Category.Id = PushNotificationCategoryId.Activity;
            activityCategory.NotificationAppGroupCategoryTypes = new List<PushNotificationAppGroupCategoryType>();

            var messageReceived = new PushNotificationAppGroupCategoryType { AppGroupId = appModelId, Order = 1, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.MessageReceived } };
            var chatRequest = new PushNotificationAppGroupCategoryType { AppGroupId = appModelId, Order = 2, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.ChatRequest } };
            var mutualYes = new PushNotificationAppGroupCategoryType { AppGroupId = appModelId, Order = 3, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.MutualYes } };

            activityCategory.NotificationAppGroupCategoryTypes.Add(messageReceived);
            activityCategory.NotificationAppGroupCategoryTypes.Add(chatRequest);
            activityCategory.NotificationAppGroupCategoryTypes.Add(mutualYes);

            var accountCategory = new PushNotificationAppGroupCategory();
            accountCategory.AppGroupId = appModelId;
            accountCategory.Category = new PushNotificationCategory();
            accountCategory.Category.Id = PushNotificationCategoryId.Photos;
            accountCategory.NotificationAppGroupCategoryTypes = new List<PushNotificationAppGroupCategoryType>();

            var photoApproved = new PushNotificationAppGroupCategoryType { AppGroupId = appModelId, Order = 1, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.PhotoApproved } };
            var photoRejected = new PushNotificationAppGroupCategoryType { AppGroupId = appModelId, Order = 2, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.PhotoRejected } };

            accountCategory.NotificationAppGroupCategoryTypes.Add(photoApproved);
            accountCategory.NotificationAppGroupCategoryTypes.Add(photoRejected);

            var promotionalCategory = new PushNotificationAppGroupCategory();
            promotionalCategory.AppGroupId = appModelId;
            promotionalCategory.Category = new PushNotificationCategory();
            promotionalCategory.Category.Id = PushNotificationCategoryId.Promotional;
            promotionalCategory.NotificationAppGroupCategoryTypes = new List<PushNotificationAppGroupCategoryType>();

            var promotionalAnnouncement = new PushNotificationAppGroupCategoryType { AppGroupId = appModelId, Order = 2, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.PromotionalAnnouncement } };

            promotionalCategory.NotificationAppGroupCategoryTypes.Add(promotionalAnnouncement);

            model.NotificationAppGroupCategories.Add(activityCategory);
            model.NotificationAppGroupCategories.Add(accountCategory);
            model.NotificationAppGroupCategories.Add(promotionalCategory);

            return model;
        }
    }

 
}
