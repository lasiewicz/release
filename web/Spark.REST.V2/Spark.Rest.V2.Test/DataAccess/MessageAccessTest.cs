﻿using System.Collections.Generic;
using Matchnet.Email.ValueObjects;
using NUnit.Framework;
using Spark.REST.Models;
using Spark.Rest.V2.DataAccess.Mail;

namespace Spark.REST.V2.Test.DataAccess
{
    [TestFixture]
    public class MessageAccessTest
    {
        private BrandRequest brandRequest = null;
        private int memberId = 114402580;  // stage 3 memberid

        [TestFixtureSetUp]
        public void SetUp()
        {
            brandRequest = new BrandRequest();
            brandRequest.BrandId = 1003;
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            brandRequest = null;
        }

        [Test]
        public void GetUnreadMessageCountTest()
        {

            Dictionary<string, Dictionary<MailType, int>> unreadMessageCount = MessageAccess.Instance.GetUnreadMessageCount(memberId, brandRequest.Brand);
            Assert.NotNull(unreadMessageCount);
            Assert.Greater(unreadMessageCount["Inbox"][MailType.Email], 0);
        }
    }
}
