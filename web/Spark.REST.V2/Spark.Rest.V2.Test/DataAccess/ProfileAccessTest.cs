﻿using Matchnet.Content.ValueObjects.BrandConfig;
using NUnit.Framework;
using Spark.Rest.Helpers;

namespace Spark.REST.V2.Test.DataAccess
{
    [TestFixture]
    public class ProfileAccessTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
        

        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            

        }


        [Test]
        public void ClearCache()
        {
            CachingHelper.WebCachebucket.Clear();
            Assert.Pass();
        }


        public class Parameters
        {
            public static readonly int BrandId = 1003;
            public static readonly int MemberId = 16000206;
            public static Community community = new Community(3, "testing.jdate.com");
            public static Site usSite = new Site(103, community, "Jdate US", 0, 0, "en-US", 0, 0, 0, string.Empty, 0, DirectionType.ltr, 0, "defaultHost", string.Empty, 0);
            public static Site ilSite = new Site(103, community, "Jdate IL", 0, 0, "he-IL", 0, 0, 0, string.Empty, 0, DirectionType.rtl, 0, "defaultHost", string.Empty, 0);
            public static Brand usBrand = new Brand(1003, usSite, string.Empty, 0, string.Empty, 0, 100, 100);
            public static Brand ilBrand = new Brand(1003, ilSite, string.Empty, 0, string.Empty, 0, 100, 100);

            public static object[] MemberRequest = { new object[]{ BrandId, MemberId }};
            public static object[] GetDfpTagsRequestUs = { new object[] { usBrand, MemberId, "en-US" } };
            public static object[] GetDfpTagsRequestIl = { new object[] { ilBrand, MemberId, "he-IL" } };
        };

        }
}
