﻿#region

using System;
using System.Collections.Generic;
using Matchnet;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Spark.Rest.V2.DataAccess.Premium;

#endregion

namespace Spark.REST.V2.Test.DataAccess.Premium
{
    /// <summary>
    ///     todo: only using VS Unit Testing here. We want NUnit..
    /// </summary>
    [TestClass]
    public class PremiumAccessQuestionAnswerTest
    {
        private Answer _answer;
        private List<Answer> _answers;
        private Mock<IQuestionAnswerAdapter> _mockQuestionAnswer;
        private IPremiumAccessQuestionAnswer _premiumAccessQuestionAnswer;
        private Question _question;

        [TestInitialize]
        public void Initialize()
        {
            _mockQuestionAnswer = new Mock<IQuestionAnswerAdapter>();
            _premiumAccessQuestionAnswer = new PremiumAccessQuestionAnswer(_mockQuestionAnswer.Object);
            _answer = new Answer
            {
                InsertDate = DateTime.Now,
                AnswerStatus = QuestionAnswerEnums.AnswerStatusType.Approved,
                AdminMemberID = 27029711,
                AnswerID = 1000,
                AnswerType = QuestionAnswerEnums.AnswerType.FreeText,
                AnswerValue = "yo answer",
                AnswerValuePending = "yo answer pending?",
                GenderMask = 5,
                GlobalStatusMask = 6,
                HasApprovedPhotos = true,
                IsDirty = false,
                MemberID = 27029711,
                QuestionID = 2000,
                SelfSuspendedFlag = 0,
                UpdateDate = DateTime.Now
            };

            _answers = new List<Answer> {_answer};

            _question = new Question
            {
                InsertDate = DateTime.Now,
                Active = true,
                AdminMemberID = 27029711,
                Answers = _answers,
                QuestionID = 2000,
                CacheMode = CacheItemMode.Absolute,
                EndDate = DateTime.Now.AddDays(1),
                QuestionType = QuestionAnswerEnums.QuestionType.FreeText,
                QuestionTypeID = 1,
                SiteID = 103,
                StartDate = DateTime.Now.AddDays(-1),
                StockAnswers = null,
                Text = "um is this a question?",
                UpdateDate = DateTime.Now,
                Weight = 100
            };
        }

        [TestMethod]
        public void TestGetQuestion()
        {
            // Setup
            _mockQuestionAnswer.Setup(qa => qa.GetQuestion(2000)).Returns(_question);

            // Test
            var result = _premiumAccessQuestionAnswer.GetQuestion(2000);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(String.IsNullOrEmpty(result.Text));
            Assert.AreEqual(2000, result.QuestionID);
        }

        [TestMethod]
        public void TestGetQuestionWithBadId()
        {
            // Setup
            _mockQuestionAnswer.Setup(qa => qa.GetQuestion(2000)).Returns(_question);

            // Test
            var result = _premiumAccessQuestionAnswer.GetQuestion(4000);

            // Assert
            Assert.IsNull(result);
        }
    }
}