﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Web;
using NUnit.Framework;
using Spark.REST.DataAccess.Applications;
using Spark.Rest.Entities.Applications;
using Spark.Rest.Helpers;
using Spark.REST.Models;
using Spark.Rest.V2.BedrockPorts;
using Spark.Rest.V2.Entities.Applications;
using Spark.Rest.V2.Exceptions;
using Spark.REST.V2.Test.Mocks;

namespace Spark.REST.V2.Test.DataAccess
{
    
    
    /// <summary>
    ///This is a test class for ApplicationAccessTest and is intended
    ///to contain all ApplicationAccessTest Unit Tests
    ///</summary>
    [TestFixture]
    public class ApplicationAccessTest
    {
        /// <summary>
        ///A test for CreateToken
        ///</summary>
        [Test]
        public void CreateTokenTest()
        {
            int applicationId = 1054; // stage3
            int memberId = 101985158; 
            int cryptogrphicPaddingBytes = 0; // 0 for initial ,30 for refresh
            TimeSpan expiresIn = new TimeSpan(0, 5, 0);
            //TokenWithExpiration expected = null; // TODO: Initialize to an appropriate value
            TokenWithExpiration actual;
            actual = ApplicationAccess.CreateToken(applicationId, memberId, cryptogrphicPaddingBytes, expiresIn);
            string failReason;
            int failCode;
            bool validated = ApplicationAccess.ValidateAccessToken(memberId, actual.Token, out failReason, out failCode);
            Assert.AreEqual(validated, true);
        }

        [Test]
        public void DecryptTokenTest()
        {
            string ushorttoken = "1/sOKWaCQsafGQANQf/qqolHJNduwMKn4y9A99zulDBWc=";
            int memberId = 114402580;
//            string ushorttoken = "1/RXHkal/39zzeAPUn5VOG+PnUGSSo9s3iqILD/OACDzQ=";            
//            int memberId = 114402571; 
            string failReason;
            int failCode;
            bool validated = ApplicationAccess.ValidateAccessToken(memberId, ushorttoken, out failReason, out failCode);
            Assert.AreEqual(validated, true);
        }

        [Test]
        [ExpectedException(typeof(SparkAPIException))]
        public void ChangeEmailTestSameEmail()
        {

            BrandRequest brandRequest = new BrandRequest();
            brandRequest.BrandId = 1003;
            int memberId = 1127789085;
            string currentEmail = "thisIsATest1@spark.net";
            string newEmail = "new" + currentEmail;
            string ipAddress = "192.168.0.1";

            IHttpContextHelperMock httpContextHelperMock = new IHttpContextHelperMock();
            httpContextHelperMock.CurrentHttpContextBase = GetMockHttpContext();
            ErrorHelper.HttpContextHelper = httpContextHelperMock;

            EmailChangeResponse response = ApplicationAccess.AuthenticatedChangeEmail(memberId, currentEmail, currentEmail, brandRequest.Brand, ipAddress);
        }

        [Test]
        [ExpectedException(typeof(SparkAPIException))]
        public void ChangeEmailTestInvalidEmail()
        {

            BrandRequest brandRequest = new BrandRequest();
            brandRequest.BrandId = 1003;
            int memberId = 1127789085;
            string currentEmail = "thisIsATest1spark.net";
            string newEmail = "new" + currentEmail;
            string ipAddress = "192.168.0.1";

            IHttpContextHelperMock httpContextHelperMock = new IHttpContextHelperMock();
            httpContextHelperMock.CurrentHttpContextBase = GetMockHttpContext();
            ErrorHelper.HttpContextHelper = httpContextHelperMock;

            EmailChangeResponse response = ApplicationAccess.AuthenticatedChangeEmail(memberId, currentEmail, newEmail, brandRequest.Brand, ipAddress);
        }

        [Test]
        public void ChangeEmailTestValidEmail()
        {
            BrandRequest brandRequest = new BrandRequest();
            brandRequest.BrandId = 1003;
            int memberId = 1127789085;
            string currentEmail = "thisIsATest1@spark.net";
            string newEmail = "new" + currentEmail;
            string ipAddress = "192.168.0.1";

            IHttpContextHelperMock httpContextHelperMock = new IHttpContextHelperMock();
            httpContextHelperMock.CurrentHttpContextBase = GetMockHttpContext();
            ErrorHelper.HttpContextHelper = httpContextHelperMock;

            EmailChangeResponse response = ApplicationAccess.AuthenticatedChangeEmail(memberId, currentEmail, newEmail, brandRequest.Brand, ipAddress);
            Assert.AreEqual(EmailChangeStatus.Success, response.Status);

            Thread.Sleep(1000);
            // change email back to original so test can run again
            response = ApplicationAccess.AuthenticatedChangeEmail(memberId, newEmail, currentEmail, brandRequest.Brand, ipAddress);
            Assert.AreEqual(EmailChangeStatus.Success, response.Status);
        }

        [Test]
        public void ChangeEmailTestValidEmailTwice()
        {
            BrandRequest brandRequest = new BrandRequest();
            brandRequest.BrandId = 1003;
            int memberId = 1127789085;
            string currentEmail = "thisIsATest1@spark.net";
            string newEmail = "new" + currentEmail;
            string ipAddress = "192.168.0.1";

            IHttpContextHelperMock httpContextHelperMock = new IHttpContextHelperMock();
            httpContextHelperMock.CurrentHttpContextBase = GetMockHttpContext();
            ErrorHelper.HttpContextHelper = httpContextHelperMock;

            EmailChangeResponse response = ApplicationAccess.AuthenticatedChangeEmail(memberId, currentEmail, newEmail, brandRequest.Brand, ipAddress);
            Assert.AreEqual(EmailChangeStatus.Success, response.Status);

            Thread.Sleep(1000);

            //second attempt to change to same email should result in no change status
            response = ApplicationAccess.AuthenticatedChangeEmail(memberId, currentEmail, newEmail, brandRequest.Brand, ipAddress);
            Assert.AreEqual(EmailChangeStatus.NoChange, response.Status);

            // change email back to original so test can run again
            response = ApplicationAccess.AuthenticatedChangeEmail(memberId, newEmail, currentEmail, brandRequest.Brand, ipAddress);
            Assert.AreEqual(EmailChangeStatus.Success, response.Status);
        }


        [Test]
        public void TestEmailVerifyIsValid()
        {
            BrandRequest brandRequest = new BrandRequest();
            brandRequest.BrandId = 1003;
            EmailVerifyHelper emailVerifyHelper = new EmailVerifyHelper(brandRequest.Brand);
            //valid emails
            AssertValidEmail(emailVerifyHelper, "thisIsATest1@spark.net", true, "Should be valid: '{0}'!");
            AssertValidEmail(emailVerifyHelper, "newthisIsATest1@spark.net", true, "Should be valid: '{0}'!");
            AssertValidEmail(emailVerifyHelper, "thisIsATest1@la.spark.net", true, "Should be valid: '{0}'!");
            AssertValidEmail(emailVerifyHelper, "ta@ka.ta", true, "Should be valid: '{0}'!");
            //invalid emails
            AssertValidEmail(emailVerifyHelper, "thisIsATest1@spark-.net", false, "Should NOT be valid: '{0}'!");
            AssertValidEmail(emailVerifyHelper, ".thisIsATest1.@spark.net", false, "Should NOT be valid: '{0}'!");
            AssertValidEmail(emailVerifyHelper, "t@k.t", false, "Should NOT be valid: '{0}'!");
            AssertValidEmail(emailVerifyHelper, "ta@k.t", false, "Should NOT be valid: '{0}'!");
            AssertValidEmail(emailVerifyHelper, "ta@ka.t", false, "Should NOT be valid: '{0}'!");
            AssertValidEmail(emailVerifyHelper, "test@127.0.0.1", false, "Should NOT be valid: '{0}'!");
            AssertValidEmail(emailVerifyHelper, "ta@ka.ta", true, "Should be valid: '{0}'!");
        }

        [Test]
        public void ValidateAccessTokenV2Test()
        {
            BrandRequest brandRequest = new BrandRequest();
            brandRequest.BrandId = 1003;
            int memberId = 101805265;
            string shorttoken = "3-41qOY6Lq76TM3ztEErnIE_5EgdJMU6dB_eclppto6gQ";

            IHttpContextHelperMock httpContextHelperMock = new IHttpContextHelperMock();
            httpContextHelperMock.CurrentHttpContextBase = GetMockHttpContext();
            ErrorHelper.HttpContextHelper = httpContextHelperMock;

            ValidateTokenResponseV2 response = ApplicationAccess.ValidateAccessToken(brandRequest.Brand, shorttoken);
            Assert.AreSame(response.IsSelfSuspended, false);
        }

        private void AssertValidEmail(EmailVerifyHelper emailVerifyHelper, string currentEmail, bool expectedResult, string message)
        {
            bool b = emailVerifyHelper.IsEmailValid(currentEmail);
            Assert.AreEqual(expectedResult, b, message, currentEmail);
        }


        private HttpContextBase GetMockHttpContext()
        {
            string urlHost = "http://api.local.spark.net";
            string urlPath = "/v2/brandId/1003/profile/miniProfile/0";
            string queryString = "access_token=3-41qOY6Lq76TM3ztEErnIE_5EgdJMU6dB_eclppto6gQ";
            var stringWriter = new StringWriter();
            HttpResponse httpResponse = new HttpResponse(stringWriter);
            httpResponse.StatusCode = (int)HttpStatusCode.OK;
            string originIp = "192.168.3.165";
            var mockHttpContext = MockHttpContextFactory.GetMockHttpContext(urlHost + urlPath, queryString, originIp,
                new HttpResponseWrapper(httpResponse));
            return mockHttpContext;
        }

    }
}
