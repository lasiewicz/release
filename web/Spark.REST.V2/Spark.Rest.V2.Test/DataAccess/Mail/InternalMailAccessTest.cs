﻿#region

using System;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ServiceAdapters.Interfaces;
using Matchnet.Email.ValueObjects;
using Matchnet.EmailTracker.ValueObjects.ServiceDefinitions;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters.Interfaces;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using Spark.Common.Localization;
using Spark.Rest.V2.DataAccess.Mail;
using Spark.Rest.V2.DataAccess.Mail.Interfaces;
using Spark.Rest.V2.Entities.Mail;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

#endregion

namespace Spark.REST.V2.Test.DataAccess.Mail
{
    /// <summary>
    ///     Supports both MS and Nunit tests
    /// </summary>
    [TestClass]
    [TestFixture]
    public class InternalMailAccessTest
    {
        private IAllAccessAccess _allAccessAccess;
        private MessageSendResult _blockedMessageSendResult;
        private Brand _brand;

        private Member _fromMember;
        private int _fromMemberId;

        // Main Access Class Interface
        private IInternalMailAccess _internalMailAccess;

        private MessageSave _messageSave;
        private MessageSendResult _messageSendResult;

        // Mocks
        private Mock<IEmailBlockedSender> _mockEmailMessageAdapter;
        private Mock<IEmailTrackerService> _mockEmailTrackerAdapter;
        private Mock<IExternalMailSA> _mockExternalMailAdapter;
        private Mock<IMemberAllAccess> _mockGetMemberAllAccess;
        private Mock<IListAddMember> _mockListAdapter;
        private Mock<IAddQuotaItem> _mockQuotaAdapter;
        private Mock<IResourceProvider> _mockResourceProvider;
        private Mock<ISettingsSA> _mockSettingsAdapter;
        private Member _recipientMember;
        private int _recipientMemberId;

        private SendMailMessageRequest _sendMailMessageAllAccessRequest;
        private SendMailMessageRequest _sendMailMessageRequest;

        [TestInitialize]
        [TestFixtureSetUp] 
        public void Initialize()
        {
            _brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(1003);

            _fromMemberId = 27029711;
            _recipientMemberId = 116361935;

            _fromMember = new Member(new CachedMember(_fromMemberId));
            _recipientMember = new Member(new CachedMember(_recipientMemberId));

            _mockEmailMessageAdapter = new Mock<IEmailBlockedSender>();
            _mockExternalMailAdapter = new Mock<IExternalMailSA>();
            _mockGetMemberAllAccess = new Mock<IMemberAllAccess>();
            _mockListAdapter = new Mock<IListAddMember>();
            _mockQuotaAdapter = new Mock<IAddQuotaItem>();
            _mockEmailTrackerAdapter = new Mock<IEmailTrackerService>();
            _mockResourceProvider = new Mock<IResourceProvider>();
            _mockSettingsAdapter = new Mock<ISettingsSA>();

            _allAccessAccess = new AllAccessAccess(MemberSA.Instance,
                _mockEmailMessageAdapter.Object,
                RuntimeSettings.Instance);

            _internalMailAccess = new InternalMailAccess(_mockEmailMessageAdapter.Object,
                _mockExternalMailAdapter.Object,
                _mockGetMemberAllAccess.Object,
                _mockListAdapter.Object,
                _mockQuotaAdapter.Object,
                _mockEmailTrackerAdapter.Object,
                _mockResourceProvider.Object, 
                _mockSettingsAdapter.Object,
                _allAccessAccess);

            // this should be changed accordingly
            _messageSave = new MessageSave(27029711, 16000206, 3, MailType.Email, "subject", "body");

            _messageSendResult = new MessageSendResult(1000, 2000, 3000, 0, MailOption.None);
            _blockedMessageSendResult = new MessageSendResult(MessageSendError.Unspecified);

            _sendMailMessageRequest = new SendMailMessageRequest(_brand,
                _fromMemberId, _recipientMemberId, 0, false, false, "Email", "subject", "body", 0);

            _sendMailMessageAllAccessRequest = new SendMailMessageRequest(_brand,
                _fromMemberId, _recipientMemberId, 0, true, false, "Email", "subject", "body", 0);

            // setup some default mocks
            _mockExternalMailAdapter.SetReturnsDefault(true);
            _mockEmailTrackerAdapter.SetReturnsDefault(true);
            _mockEmailMessageAdapter.SetReturnsDefault(_messageSendResult);
            _mockResourceProvider.SetReturnsDefault("blah");

            _mockSettingsAdapter.Setup(
                x =>
                    x.GetSettingFromSingleton("VIP_EMAIL_ENABLED", _brand.Site.Community.CommunityID, _brand.Site.SiteID))
                .Returns("true");
        }

        /// <summary>
        ///     todo: member.GetUnifiedAccessPrivilege is actually calling the service so this is a functional test
        /// </summary>
        [TestMethod]
        [Test]
        public void TestGetAllAccessMember()
        {
            // Setup
            _mockGetMemberAllAccess.Setup(x => x.GetMember(_fromMemberId, MemberLoadFlags.None)).Returns(_fromMember);

            // Test
            var result = _allAccessAccess.GetAllAccessMember(_brand, _fromMemberId);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.AllAccessExpireDate > DateTime.UtcNow);
        }

        /// <summary>
        ///     Testing to see if it successfully sends one
        /// </summary>
        [TestMethod]
        [Test]
        public void TestSendBlockedMail()
        {
            // Setup
            _mockEmailMessageAdapter.Setup(
                x =>
                    x.CheckAndSendMessageForBlockedSenderInRecipientList(_brand.Site.Community.CommunityID,
                        It.IsAny<string>(), It.IsAny<string>(), _recipientMemberId, _fromMemberId, _brand.Site.SiteID,
                        _brand.Site.Community.CommunityID)).Returns(_messageSendResult);

            _mockGetMemberAllAccess.Setup(x => x.GetMember(_recipientMemberId, MemberLoadFlags.None))
                .Returns(_recipientMember);

            // Test
            MessageSendResult messageSendResult;
            var result = _internalMailAccess.SendBlockedEmail(_recipientMemberId, _brand, _messageSave, _fromMemberId,
                out messageSendResult);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [Test]
        public void TestSendDraftMessage()
        {
            // Setup
            var memberMailId = 2000;
            _mockEmailMessageAdapter.Setup(x => x.SaveMessage(_messageSave, out memberMailId)).Returns(true);
            _mockEmailMessageAdapter.Setup(
                x => x.MarkDraftSaved(_fromMemberId, _brand.Site.Community.CommunityID, true, It.IsAny<int>()));

            // Test
            int draftMailId;
            var result = _internalMailAccess.SendDraftMessage(_brand, _messageSave, _fromMemberId, out draftMailId);

            // Assert
            Assert.IsTrue(result);
            Assert.IsTrue(draftMailId > 0);
        }

        [TestMethod]
        [Test]
        public void TestSendExternalMailNotificationForInternalMailSent()
        {
            // Setup

            // Test
            _internalMailAccess.SendExternalMailNotificationForSent(_brand, false, false,
                _messageSendResult, false, _fromMemberId, _messageSave);

            // Assert
            // result is void, cannot assert
        }

        [TestMethod]
        [Test]
        public void AddToMembersYouEmailedHotListForInternalMailSent()
        {
            // Setup
            _mockListAdapter.SetReturnsDefault(new ListSaveResult
            {
                Counter = 1,
                MaxAllowed = 10,
                MaxAllowedDurationDays = 10,
                Status = ListActionStatus.Success
            });

            // Test
            _internalMailAccess.AddToMembersYouEmailedHotListForSent(_brand,
                _fromMemberId, _recipientMemberId, false, true, true, _messageSave);

            // Assert
            // result is void, cannot assert
        }

        /// <summary>
        ///     For testing a regular internal mail send
        ///     Valid Request
        ///     Non Reply
        ///     Non FraudHold
        ///     Non All Access
        ///     Non Draft
        /// </summary>
        [TestMethod]
        [Test]
        public void TestSendMailMessage()
        {
            // Setup
            _mockEmailMessageAdapter.Setup(x => x.SendMessage(_messageSave, true, 0, false)).Returns(_messageSendResult);
            _mockListAdapter.SetReturnsDefault(new ListSaveResult
            {
                Counter = 1,
                MaxAllowed = 10,
                MaxAllowedDurationDays = 10,
                Status = ListActionStatus.Success
            });
            // we want this to be not successful hence the blocked result
            _mockEmailMessageAdapter.Setup(
                x => x.CheckAndSendMessageForBlockedSenderInRecipientList(_brand.Site.Community.CommunityID,
                    It.IsAny<string>(), It.IsAny<string>(), _recipientMemberId, _fromMemberId, _brand.Site.SiteID,
                    _brand.Site.Community.CommunityID)).Returns(_blockedMessageSendResult);
            _mockGetMemberAllAccess.Setup(x => x.GetMember(_recipientMemberId, MemberLoadFlags.None))
                .Returns(_recipientMember);

            // Test
            // using the initialization values in the request which is a regular internal email for JD
            var result = _internalMailAccess.SendMailMessage(_sendMailMessageRequest);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSent);
        }

        /// <summary>
        ///     For testing an all access internal mail send
        ///     Valid Request
        ///     Non Reply
        ///     Non FraudHold
        ///     All Access
        ///     Non Draft
        /// </summary>
        [TestMethod]
        [Test]
        public void TestSendMailMessageAllAccess()
        {
            // Setup
            _mockEmailMessageAdapter.Setup(x => x.SendMessage(_messageSave, true, 0, false)).Returns(_messageSendResult);
            _mockListAdapter.SetReturnsDefault(new ListSaveResult
            {
                Counter = 1,
                MaxAllowed = 10,
                MaxAllowedDurationDays = 10,
                Status = ListActionStatus.Success
            });
            // we want this to be not successful hence the blocked result
            _mockEmailMessageAdapter.Setup(
                x => x.CheckAndSendMessageForBlockedSenderInRecipientList(_brand.Site.Community.CommunityID,
                    It.IsAny<string>(), It.IsAny<string>(), _recipientMemberId, _fromMemberId, _brand.Site.SiteID,
                    _brand.Site.Community.CommunityID)).Returns(_blockedMessageSendResult);
            _mockGetMemberAllAccess.Setup(x => x.GetMember(_recipientMemberId, MemberLoadFlags.None))
                .Returns(_recipientMember);

            // Test
            // using the initialization values in the request which is a regular internal email for JD
            var result = _internalMailAccess.SendMailMessage(_sendMailMessageAllAccessRequest);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSent);
        }

        /// <summary>
        ///     This currently fails due to GetMailMessage() that calls ProfileAccess which requires more DI refactoring...
        /// </summary>
        [TestMethod]
        [Test]
        public void TestGetMailFolder()
        {
            // Setup
            var request = new GetMessageFolderRequest(_brand, _fromMemberId, (int) SystemFolders.Inbox,
                2, 1);

            var messageList = new MessageList(1000, _fromMemberId, _recipientMemberId, false,
                _brand.Site.Community.CommunityID, (int) SystemFolders.Inbox, 2000, MessageStatus.New, MailType.Email,
                DateTime.Now, 500);

            var emailMessage = new EmailMessage(messageList);
            var message = new Message(2000, "subject", "body", DateTime.Now, DateTime.Now);
            emailMessage.Message = message;

            var emailMessageCollection = new EmailMessageCollection(_fromMemberId, _brand.Site.Community.CommunityID,
                (int) SystemFolders.Inbox);
            emailMessageCollection.Add(emailMessage);
            emailMessageCollection.Add(emailMessage);
            
            _mockEmailMessageAdapter.Setup(
                x => x.RetrieveMessages(_fromMemberId, _brand.Site.Community.CommunityID, (int) SystemFolders.Inbox))
                .Returns(emailMessageCollection);
            
            _mockEmailMessageAdapter.Setup(x => x.RetrieveMessagesFromLogicalFolder(_fromMemberId,
                _brand.Site.Community.CommunityID, (int) SystemFolders.Inbox, 1, 2, "InsertDate desc",
                _brand)).Returns(emailMessageCollection);

            // Test
            var result = _internalMailAccess.GetMessageFolder(request);

            // Assert
            Assert.IsTrue(result.IsRetrieved);
            Assert.IsTrue(result.MailFolder.MessageList.Count > 0);
        }
    }
}