﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Spark.REST.Configuration;
using Spark.Rest.V2.DataAccess.MingleMigration;

#endregion

namespace Spark.REST.V2.Test.DataAccess
{
    [TestFixture]
    public class MingleMigrationAccessTest
    {
        private int cmSiteId = 9081;

        [TestFixtureSetUp]
        public void SetUp()
        {
            AttributeConfigReader.AttributeSetXmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../Spark.Rest.V2/Configuration/AttributeSetList.xml");
            AttributeConfigReader.PropertyWhitelistXmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../Spark.Rest.V2/Configuration/AttributeWhitelist.xml");
            AttributeConfigReader.MigrationAttributeBlacklistXmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../Spark.Rest.V2/Configuration/MingleMigrationAttributeLastUpdateBlackList.xml");

            var attributeSetDict = AttributeConfigReader.Instance.AttributeSetDictionary;
            var propertyWhitelist = AttributeConfigReader.PropertyWhitelist;
            var migrationAttributeBlacklist = AttributeConfigReader.MigrationAttributeBlacklist;
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
        }

        [Test]
        public void TestLastUpdatedAddedForAttrMapping()
        {
            var attributeData = new Dictionary<string, string> {{"user_email", "blah@blah.com"}, {"has_marital_status", "married"}, {"has_gender", "male"}, {"has_birthdate", DateTime.Now.AddYears(-32).ToString()}, {"fun_facts_music", "jazz"}};
            var attributeMappingResult = MingleMigrationAccess.MapTextAttributes(attributeData, cmSiteId);
            Assert.IsTrue(attributeMappingResult.MappedAttributes.ContainsKey("LastUpdated"));

            var attributeIntData = new Dictionary<string, object>();
            attributeIntData.Add("extra_children_at_home", 2);
            attributeIntData.Add("extra_about_children", 4);
            attributeIntData.Add("extra_occupation", 6);
            attributeIntData.Add("has_children", true);
            var attributeMappingResult2 = MingleMigrationAccess.MapAttributes(attributeIntData, new Dictionary<string, List<int>>(), cmSiteId);
            Assert.IsTrue(attributeMappingResult2.MappedAttributes.ContainsKey("LastUpdated"));

            var attributeDataMultiValue = new Dictionary<string, List<int>>();
            attributeDataMultiValue.Add("extra_children_at_home", new[] {1, 2, 3}.ToList());
            attributeDataMultiValue.Add("extra_about_children", new[] {1, 2, 3}.ToList());
            attributeDataMultiValue.Add("extra_occupation", new[] {1, 2, 3}.ToList());
            var attributeMappingResult3 = MingleMigrationAccess.MapAttributes(new Dictionary<string, object>(), attributeDataMultiValue, cmSiteId);
            Assert.IsTrue(attributeMappingResult3.MappedAttributes.ContainsKey("LastUpdated"));

            var attributeMappingResult4 = MingleMigrationAccess.MapAttributes(attributeIntData, attributeDataMultiValue, cmSiteId);
            Assert.IsTrue(attributeMappingResult4.MappedAttributes.ContainsKey("LastUpdated"));
        }

        [Test]
        public void TestLastUpdatedNOTAddedForAttrMapping()
        {
            var now = DateTime.Now.ToString();
            var attributeData = new Dictionary<string, string>();
            attributeData.Add("reg_lpid", "2345");
            attributeData.Add("adid", "41243");
            attributeData.Add("HideMask", "234321");
            attributeData.Add("user_join_ip", "10.10.10.10");
            attributeData.Add("user_expires_date", now);
            attributeData.Add("HighlightedFlag", "Y");
            attributeData.Add("SubscriptionStatus", "N");
            attributeData.Add("SubscriptionLastInitialPurchaseDate", now);
            attributeData.Add("Refcd", "blah");
            attributeData.Add("ROLFScore", "B");
            attributeData.Add("ROLFScoreDescription", "Bad");
            attributeData.Add("IMPermissions", "None");
            attributeData.Add("UserAgent", "Nunit");
            attributeData.Add("UserCapabilitiesCaptured", "Y");
            attributeData.Add("UserFlashVersion", "11");
            attributeData.Add("TrackingRegApplication", "Nunit");
            attributeData.Add("TrackingRegOS", "Nunit");
            attributeData.Add("TrackingRegFormFactor", "Nunit");
            attributeData.Add("PledgeToCounterFraud", "Y");
            attributeData.Add("FacebookUserID", "10993asdf");
            attributeData.Add("TrackingRegDevice", "Nunit");
            attributeData.Add("reg_lptestid", "22288");
            attributeData.Add("SubscriptionFirstInitialPurchaseDate", now);
            attributeData.Add("user_last_ip", "127.0.0.1");
            attributeData.Add("mingleuserid", "18818929");
            attributeData.Add("user_flags", "blah");
            var attributeMappingResult = MingleMigrationAccess.MapTextAttributes(attributeData, cmSiteId);
            Assert.IsFalse(attributeMappingResult.MappedAttributes.ContainsKey("LastUpdated"));

            var attributeIntData = new Dictionary<string, object> {{"reg_lptestid", 2}, {"mingleuserid", 4}};
            var attributeMappingResult2 = MingleMigrationAccess.MapAttributes(attributeIntData, new Dictionary<string, List<int>>(), cmSiteId);
            Assert.IsFalse(attributeMappingResult2.MappedAttributes.ContainsKey("LastUpdated"));

            var attributeDataMultiValue = new Dictionary<string, List<int>> {{"Refcd", new[] {1, 2, 3}.ToList()}, {"user_flags", new[] {1, 2, 3}.ToList()}};
            var attributeMappingResult3 = MingleMigrationAccess.MapAttributes(new Dictionary<string, object>(), attributeDataMultiValue, cmSiteId);
            Assert.IsFalse(attributeMappingResult3.MappedAttributes.ContainsKey("LastUpdated"));

            var attributeMappingResult4 = MingleMigrationAccess.MapAttributes(attributeIntData, attributeDataMultiValue, cmSiteId);
            Assert.IsFalse(attributeMappingResult4.MappedAttributes.ContainsKey("LastUpdated"));
        }
    }
}
