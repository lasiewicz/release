﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ServiceAdapters;
using NUnit.Framework;
using Spark.REST.DataAccess;
using Spark.Rest.V2.Models.Profile;

namespace Spark.REST.V2.Test.DataAccess.Profile
{
    public static class SelfSuspendTestValues
    {
        public static readonly int BrandId = 1003;
        public static readonly int MemberId = 16000206;
        public static readonly int SuspendReasonId = 1; // found my soulmate on the site
        public static readonly int NotSuspendedReasonId = 0; // not suspended default value

        public static object[] SuspendTestParams =
        {
            new object[]{ BrandId, MemberId, SuspendReasonId }
        };

        public static object[] ReactivateTestParams =
        {
            new object[]{ BrandId, MemberId, NotSuspendedReasonId }
        };
    }
    
    [TestFixture]
    public class SelfSuspendTest
    {
        [Test, TestCaseSource(typeof(SelfSuspendTestValues), "SuspendTestParams")]
        public void SelfSuspendMemberProfileTest(int brandId, int memberId, int suspendReasonId)
        {
            // Arrange
            var request = new MemberSuspendRequest
            {
                SuspendReasonId = suspendReasonId
            };

            var brand = BrandConfigSA.Instance.GetBrandByID(brandId);

            // Act
            ProfileAccess.SuspendProfile(request, brand, memberId);
            
            // Assert
            Assert.AreEqual(suspendReasonId, ProfileAccess.GetSuspendedProfileReasonId(brand, memberId));
        }

        [Test, TestCaseSource(typeof(SelfSuspendTestValues), "ReactivateTestParams")]
        public void ReactivateSelfSuspendMemberProfileTest(int brandId, int memberId, int expectedReasonId)
        {
            // Arrange
            var brand = BrandConfigSA.Instance.GetBrandByID(brandId);

            // Act
            ProfileAccess.ReActivateSuspendedProfile(brand, memberId);

            // Assert
            Assert.AreEqual(expectedReasonId, ProfileAccess.GetSuspendedProfileReasonId(brand, memberId));
        }
    }
}
