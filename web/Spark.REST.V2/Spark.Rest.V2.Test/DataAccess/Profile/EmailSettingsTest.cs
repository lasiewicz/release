﻿using Matchnet.Content.ServiceAdapters;
using NUnit.Framework;
using Spark.REST.DataAccess;
using Spark.Rest.V2.DataAccess.Profile;
using Spark.Rest.V2.Models.Profile;

namespace Spark.REST.V2.Test.DataAccess.Profile
{
    public class EmailSettingsTestValues
    {
        public static readonly int BrandId = 1003;
        public static readonly int MemberId = 16000206;

        public static readonly int Daily = 1;
        public static readonly int None = 0;

        public static readonly EmailAlertSettings AllEmailAlertSettingsOff = new EmailAlertSettings();
        public static readonly EmailAlertSettings AllEmailAlertSettingsOn = new EmailAlertSettings
        {
            SecretAdmirerEmailFrequency = Daily,
            ShouldNotifyEcard = true,
            ShouldNotifyEmailAlert = true,
            ShouldNotifyMatches = true,
            ShouldNotifyProfileEssayRequests = true,
            ShouldNotifySecretAdmirerAlerts = true,
            ShouldNotifyWhenAddedToHotlist = true,
            ShouldNotifyWhenProfileViewed = true
        };

        public static readonly MessageSettings AllMessageSettingsOff = new MessageSettings();
        public static readonly MessageSettings AllMessageSettingsOn = new MessageSettings
        {
            ShouldIncludeOriginalInReplies = true,
            ShouldReplyOnMyBehalf = true,
            ShouldSaveSentCopies = true
        };

        public static readonly NewsLetterSettings AllNewsLetterSettingsOff = new NewsLetterSettings();
        public static readonly NewsLetterSettings AllNewsLetterSettingsOn = new NewsLetterSettings
        {
            ShouldSendDatingTips = true,
            ShouldSendEventsInvitations = true
        };
        
        public static object[] TestParams =
        {
            new object[] {new EmailSettings
            {
                EmailAlertSettings = AllEmailAlertSettingsOff,
                MessageSettings = AllMessageSettingsOff,
                NewsLetterSettings = AllNewsLetterSettingsOff
            }, None, BrandId, MemberId},
            
            new object[] {new EmailSettings
            {
                EmailAlertSettings = AllEmailAlertSettingsOn,
                MessageSettings = AllMessageSettingsOn,
                NewsLetterSettings = AllNewsLetterSettingsOn
            }, Daily, BrandId, MemberId},
        };

    }

    [TestFixture]
    public class EmailSettingsTest
    {
        [Test, TestCaseSource(typeof(EmailSettingsTestValues), "TestParams")]
        public void EmailSettingsUpdatesTest(EmailSettings expected, int frequency, int brandId, int memberId)
        {
            // Arrange
            var request = new EmailSettingsRequest
            {
                EmailAlertSettings = expected.EmailAlertSettings,
                MessageSettings = expected.MessageSettings,
                NewsLetterSettings = expected.NewsLetterSettings,
                BrandId = brandId,
                MemberId = memberId
            };

            var brand = BrandConfigSA.Instance.GetBrandByID(brandId);

            // Act
            ProfileAccess.UpdateEmailSettings(request, brand, memberId);
            var emailSettings = ProfileAccess.GetEmailSettings(memberId, brand);

            // Assert
            Assert.IsNotNull(emailSettings);

            var emailAlertSettings = emailSettings.EmailAlertSettings;
            var newsLetterSettings = emailSettings.NewsLetterSettings;
            var messageSettings = emailSettings.MessageSettings;

            // email alerts
            Assert.IsNotNull(emailAlertSettings);
            Assert.AreEqual(frequency, emailAlertSettings.SecretAdmirerEmailFrequency);
            Assert.AreEqual(expected.EmailAlertSettings.ShouldNotifyWhenAddedToHotlist, emailAlertSettings.ShouldNotifyWhenAddedToHotlist);
            Assert.AreEqual(expected.EmailAlertSettings.ShouldNotifyEmailAlert, emailAlertSettings.ShouldNotifyEmailAlert);
            Assert.AreEqual(expected.EmailAlertSettings.ShouldNotifyEcard, emailAlertSettings.ShouldNotifyEcard);
            Assert.AreEqual(expected.EmailAlertSettings.ShouldNotifySecretAdmirerAlerts, emailAlertSettings.ShouldNotifySecretAdmirerAlerts);
            Assert.AreEqual(expected.EmailAlertSettings.ShouldNotifyWhenProfileViewed, emailAlertSettings.ShouldNotifyWhenProfileViewed);
            Assert.AreEqual(expected.EmailAlertSettings.ShouldNotifyProfileEssayRequests, emailAlertSettings.ShouldNotifyProfileEssayRequests);

            // news letter
            Assert.IsNotNull(newsLetterSettings);
            Assert.AreEqual(expected.NewsLetterSettings.ShouldSendDatingTips, newsLetterSettings.ShouldSendDatingTips);
            Assert.AreEqual(expected.NewsLetterSettings.ShouldSendEventsInvitations, newsLetterSettings.ShouldSendEventsInvitations);

            // messages
            Assert.IsNotNull(messageSettings);
            Assert.AreEqual(expected.MessageSettings.ShouldIncludeOriginalInReplies, messageSettings.ShouldIncludeOriginalInReplies);
            Assert.AreEqual(expected.MessageSettings.ShouldReplyOnMyBehalf, messageSettings.ShouldReplyOnMyBehalf);
            Assert.AreEqual(expected.MessageSettings.ShouldSaveSentCopies, messageSettings.ShouldSaveSentCopies);
        }
    }
}
