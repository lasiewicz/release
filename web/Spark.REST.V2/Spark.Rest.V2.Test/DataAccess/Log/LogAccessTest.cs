﻿#region

using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spark.Rest.V2.DataAccess.Log;
using Spark.Rest.V2.DataAccess.Log.Interfaces;

#endregion

namespace Spark.REST.V2.Test.DataAccess.Log
{
    [TestClass]
    public class LogAccessTest
    {
        private readonly ILogAccess _logAccess = new LogAccess();

        [TestInitialize]
        public void Initialize()
        {
        }

        /// <summary>
        ///     Functional test, not using mocking
        /// </summary>
        [TestMethod]
        public void LogInfoMessage()
        {
            // Setup
            var customData = new Dictionary<string, string> {{"VS Unit Test", "true"}};

            // Execute
            var result = _logAccess.LogMessage(customData, LogAccess.MessageType.InfoFormat,
                "test info message", "2015-05-15", 1000, 27029711);

            // Assert
            Assert.IsTrue(result.LogMessageErrorType == LogAccess.LogMessageErrorType.None);
        }

        /// <summary>
        ///     Functional test, not using mocking
        /// </summary>
        [TestMethod]
        public void LogErrorMessage()
        {
            // Setup
            var customData = new Dictionary<string, string> {{"VS Unit Test", "true"}};

            // Execute
            var result = _logAccess.LogMessage(customData, LogAccess.MessageType.ErrorFormat
                , "test error message", "2015-05-15", 1000, 27029711);

            // Assert
            Assert.IsTrue(result.LogMessageErrorType == LogAccess.LogMessageErrorType.None);
        }
    }
}