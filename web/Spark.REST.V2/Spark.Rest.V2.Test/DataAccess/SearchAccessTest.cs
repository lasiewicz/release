﻿using Matchnet.Content.ServiceAdapters;
using NUnit.Framework;
using Spark.REST.DataAccess.Search;
using Spark.REST.Entities.Search;
using Spark.REST.Models.Search;

namespace Spark.REST.V2.Test.DataAccess
{
    
    
    /// <summary>
    ///This is a test class for SearchAccessTest and is intended
    ///to contain all SearchAccessTest Unit Tests
    ///</summary>
	[TestFixture]
	public class SearchAccessTest
	{
		/// <summary>
		///A test for GetSearchPreferences
		///</summary>
		[Test]
		public void GetSearchPreferencesTest()
		{
			var brand = BrandConfigSA.Instance.GetBrandByID(1003);

			int memberId = 411; 
			SearchPreferences expected = null; // TODO: Initialize to an appropriate value
			SearchPreferences actual;
			actual = SearchAccessSal.Instance.GetSearchPreferences(brand, memberId);
			Assert.AreEqual(expected, actual);
			Assert.Inconclusive("Verify the correctness of this test method.");
		}

		/// <summary>
		///A test for SaveSearchPreferences
		///</summary>
		[Test]
		public void SaveSearchPreferencesTest()
		{
			//const string brandUri = "jdate.com";
			const int memberId = 411;
			var brand = BrandConfigSA.Instance.GetBrandByID(1003);

			var expectedPrefs = new SearchPreferencesRequest
			                    	{
			                    		MemberId = memberId,
			                    		Gender = "Male",
			                    		SeekingGender = "Female",
			                    		MaxAge = 45,
			                    		MinAge = 35
			                    	};
			SearchAccessSal.Instance.SaveSearchPreferences(expectedPrefs, brand);

//			var searches = SAL.MemberSearchCollection.Load(expectedPrefs.MemberId, expectedPrefs.Brand);
			//Assert.IsNotNull(searches);
			//Assert.IsNotNull(searches.PrimarySearch);
			//var actualPrefs = searches.PrimarySearch;
			//Assert.AreEqual(expectedPrefs.MemberId, actualPrefs.MemberID, "memberId");
			//Assert.AreEqual(expectedPrefs.MinAge, actualPrefs.Age.MinValue, "min age");
			//Assert.AreEqual(expectedPrefs.MaxAge, actualPrefs.Age.MaxValue, "max age");
			//Assert.AreEqual(expectedPrefs.MaxDistance, actualPrefs.Distance, "distance");
			//Assert.AreEqual(expectedPrefs.ShowOnlyMembersWithPhotos, actualPrefs.HasPhoto, "show only photo members");
			//Assert.AreEqual(expectedPrefs.Gender, actualPrefs.Gender, "gender");
			//Assert.AreEqual(expectedPrefs.SeekingGender, actualPrefs.SeekingGender, "seeking gender");
			var searches = SearchAccessSal.Instance.GetSearchPreferences(expectedPrefs.Brand, expectedPrefs.MemberId);
		}
	}
}
