﻿using NUnit.Framework;
using Spark.Rest.V2.DataAccess.Profile;
using Spark.Rest.V2.Helpers.Mappers;
using System;

namespace Spark.REST.V2.Test.Mappers
{
    public class EmailAlertSettingsTestValues
    {
        public static readonly int Daily = 1;
        public static readonly int All = 1 | 2 | 4 | 8 | 32;
        public static readonly int None = 0 | 16; //ShouldNotifyWhenProfileViewed is reversed in bitmap
        public static readonly int Yes = 1;
        public static readonly int No = 0;

        public static readonly EmailAlertSettings AllSettingsOff = new EmailAlertSettings();

        public static readonly EmailAlertSettings AllSettingsOn = new EmailAlertSettings 
        {
            ShouldNotifyEcard = true,
            ShouldNotifyEmailAlert = true,
            ShouldNotifyMatches = true,
            ShouldNotifyProfileEssayRequests = true,
            ShouldNotifySecretAdmirerAlerts = true,
            ShouldNotifyWhenAddedToHotlist = true,
            ShouldNotifyWhenProfileViewed = true
        };

        public static object[] MaskToModelParams =
        {
            new object[] {None, Daily, No, AllSettingsOff},
            new object[] {All, 2, Yes, AllSettingsOn} 
        };

        public static object[] ModelToMaskParams =
        {
            new object[] {AllSettingsOff, Daily, None},
            new object[] {AllSettingsOn, Daily, All}
        };
    }

    [TestFixture]
    public class EmailAlertSettingsMapperTest
    {
        [Test, TestCaseSource(typeof(EmailAlertSettingsTestValues), "MaskToModelParams")]
        public void MapMaskToEmailAlertSettingsTest(int mask, int frequency, int shouldNotifyMatches, EmailAlertSettings expected)
        {
            // Arrange
            var mapper = AttributeToModelMapper.Instance;

            // Act
            var settings = mapper.MapToEmailAlertSettings(mask, frequency, shouldNotifyMatches);

            // Assert
            Assert.AreEqual(frequency, settings.SecretAdmirerEmailFrequency);
            Assert.AreEqual(expected.ShouldNotifyWhenAddedToHotlist, settings.ShouldNotifyWhenAddedToHotlist);
            Assert.AreEqual(expected.ShouldNotifyEmailAlert, settings.ShouldNotifyEmailAlert);
            Assert.AreEqual(expected.ShouldNotifyEcard, settings.ShouldNotifyEcard);
            Assert.AreEqual(expected.ShouldNotifySecretAdmirerAlerts, settings.ShouldNotifySecretAdmirerAlerts);
            Assert.AreEqual(expected.ShouldNotifyWhenProfileViewed, settings.ShouldNotifyWhenProfileViewed);
            Assert.AreEqual(expected.ShouldNotifyProfileEssayRequests, settings.ShouldNotifyProfileEssayRequests);
        }

        [Test, TestCaseSource(typeof(EmailAlertSettingsTestValues), "ModelToMaskParams")]
        public void MapMaskFromEmailAlertSettingsTest(EmailAlertSettings settings, int frequency, int expected)
        {
            // Arrange
            var mapper = ModelToAttributeMapper.Instance;

            // Act
            var mask = mapper.MapFromEmailAlertSettings(settings);

            // Assert
            Assert.AreEqual(expected, mask);
        }
    }
}
