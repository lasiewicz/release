﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Spark.Rest.V2.DataAccess.Profile;
using Spark.Rest.V2.Helpers.Mappers;

namespace Spark.REST.V2.Test.Mappers
{
    public class MessageSettingsTestValues
    {
        public static readonly int All = 1 | 2;
        public static readonly int None = 0;

        public static readonly MessageSettings AllSettingsOn = new MessageSettings
        {
            ShouldIncludeOriginalInReplies = true,
            ShouldReplyOnMyBehalf = true,
            ShouldSaveSentCopies = true
        };

        public static readonly MessageSettings AllSettingsOff = new MessageSettings();

        public static object[] MaskToModelParams =
        {
            new object[] { All, DateTime.Now, AllSettingsOn },
            new object[] { None, DateTime.MinValue, AllSettingsOff }
        };

        public static object[] ModelToMaskParams =
        {
            new object[] { AllSettingsOff, None },
            new object[] { AllSettingsOn, All }
        };
    }

    [TestFixture]
    public class MessageSettingsMapperTest
    {
        [Test, TestCaseSource(typeof(MessageSettingsTestValues), "MaskToModelParams")]
        public void MapMaskToNewLetterSettings(int mask, DateTime vacationStartDate,MessageSettings expected)
        {
            // Arrange
            var mapper = AttributeToModelMapper.Instance;

            // Act
            var settings = mapper.MapToMessageSettings(mask,vacationStartDate);

            // Assert
            Assert.AreEqual(expected.ShouldIncludeOriginalInReplies, settings.ShouldIncludeOriginalInReplies);
            Assert.AreEqual(expected.ShouldReplyOnMyBehalf, settings.ShouldReplyOnMyBehalf);
            Assert.AreEqual(expected.ShouldSaveSentCopies, settings.ShouldSaveSentCopies);
        }

        [Test, TestCaseSource(typeof(MessageSettingsTestValues), "ModelToMaskParams")]
        public void MapMaskFromNewsLetterSettings(MessageSettings settings, int expected)
        {
            // Arrange
            var mapper = ModelToAttributeMapper.Instance;

            // Act
            var mask = mapper.MapFromMessageSettings(settings);

            // Assert
            Assert.AreEqual(expected, mask);
        }
    }
}
