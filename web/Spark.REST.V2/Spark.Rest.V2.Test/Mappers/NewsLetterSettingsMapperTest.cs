﻿using NUnit.Framework;
using Spark.Rest.V2.DataAccess.Profile;
using Spark.Rest.V2.Helpers.Mappers;

namespace Spark.REST.V2.Test.Mappers
{
    public class NewsLetterSettingsTestValues
    {
        public static readonly int All = 1 | 2 | 4;
        public static readonly int None = 0;

        public static readonly NewsLetterSettings AllSettingsOn = new NewsLetterSettings
        {
            ShouldSendDatingTips = true,
            ShouldSendEventsInvitations = true
        };

        public static readonly NewsLetterSettings AllSettingsOff = new NewsLetterSettings();

        public static object[] MaskToModelParams =
        {
            new object[] { All, AllSettingsOn },
            new object[] { None, AllSettingsOff }
        };

        public static object[] ModelToMaskParams =
        {
            new object[] { AllSettingsOff, None },
            new object[] { AllSettingsOn, All }
        };
    }

    [TestFixture]
    public class NewsLetterSettingsMapperTest
    {
        [Test, TestCaseSource(typeof(NewsLetterSettingsTestValues), "MaskToModelParams")]
        public void MapMaskToNewLetterSettings(int mask, NewsLetterSettings expected)
        {
            // Arrange
            var mapper = AttributeToModelMapper.Instance;
   
            // Act
            var settings = mapper.MapToNewsLetterSettings(mask);

            // Assert
            Assert.AreEqual(expected.ShouldSendDatingTips, settings.ShouldSendDatingTips);
            Assert.AreEqual(expected.ShouldSendEventsInvitations, settings.ShouldSendEventsInvitations);
        }

        [Test, TestCaseSource(typeof(NewsLetterSettingsTestValues), "ModelToMaskParams")]
        public void MapMaskFromNewsLetterSettings(NewsLetterSettings settings, int expected)
        {
            // Arrange
            var mapper = ModelToAttributeMapper.Instance;

            // Act
            var mask = mapper.MapFromNewsLetterSettings(settings);

            // Assert
            Assert.AreEqual(expected, mask);
        }
    }
}
