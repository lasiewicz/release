﻿using Matchnet.Content.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;

namespace Spark.Common.Localization.Test
{
    
    
    /// <summary>
    ///This is a test class for ResourceProviderTest and is intended
    ///to contain all ResourceProviderTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ResourceProviderTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetResourceValue
        ///</summary>
        [TestMethod()]
        public void GetResourceValueTest()
        {
            ResourceProvider_Accessor target = new ResourceProvider_Accessor();
            int siteId = 103; 
            var cultureInfo = new CultureInfo("en-US");
            const string key = "ATTOPT_100_MORNING_PERSON";
            const string expected = "Morning Person";
			string actual = target.GetResourceValue(siteId, ResourceGroupEnum.Global, cultureInfo, key);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetResourceValue
        ///</summary>
        [TestMethod()]
        public void GetResourceValueTest1()
        {
            ResourceProvider_Accessor target = new ResourceProvider_Accessor();
            var brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string key = "ATTOPT_100_MORNING_PERSON";
            string expected = "Morning Person";
            string actual;
			actual = target.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global, key);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetResourceValue
        ///</summary>
        [TestMethod()]
        public void GetResourceValueTestDefaults()
        {
            string key = "ATTOPT_100_MORNING_PERSON";
            string expected = "Morning Person";
            string actual;
			actual = ResourceProvider.Instance.GetResourceValue(103, ResourceGroupEnum.Global, new CultureInfo("en-US"), key);
            Assert.AreEqual(expected, actual);
        }
    }
}
