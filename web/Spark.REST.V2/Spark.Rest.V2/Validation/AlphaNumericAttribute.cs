﻿using System.ComponentModel.DataAnnotations;

namespace Spark.Rest.V2.Validation
{
    /// <summary>
    /// A model validation attribute that checks if a string field is composed of only alpha numeric characters.
    /// </summary>
    sealed public class AlphaNumericAttribute : ValidationAttribute
    {
        private const string AlphaNumericRegex = "^[a-zA-Z0-9א-ת]*$";

        public override bool IsValid(object value)
        {
            return value != null && System.Text.RegularExpressions.Regex.IsMatch(value.ToString(), AlphaNumericRegex);
        }
    }
}