﻿using Matchnet;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Spark.Logger;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Configuration;
using Spark.Rest.V2.Controllers;
using Spark.Rest.V2.DataAccess.MembersOnline;
using Spark.Rest.V2.Entities.HotList;
using Spark.Rest.V2.Models.HotList;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.DataAccess;
using Spark.REST.Entities.HotList;
using Spark.REST.Models;
using Spark.REST.Models.HotList;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Spark.REST.Controllers
{
    /// <summary>
    /// HotListController
    /// </summary>
    public class HotListController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(HotListController));

        private static readonly List<HotListCategory> RelevantCategories = new List<HotListCategory>
        {
            HotListCategory.WhoViewedYourProfile,
            HotListCategory.ViewedCombined,
            HotListCategory.WhoAddedYouToTheirFavorites,
            HotListCategory.FavoritesCombined,
            HotListCategory.MutualYes
        };

        /// <summary>
        /// GetHotListCounts
        /// 
        /// This API will fetch the total count for the following categories
        /// 
        ///  HotListCategory.WhoViewedYourProfile,
        ///  HotListCategory.WhoAddedYouToTheirFavorites
        ///  HotListCategory.MembersYouTeased,
        ///  HotListCategory.MembersYouEmailed,
        ///  HotListCategory.MembersYouIMed,
        ///  HotListCategory.MembersYouViewed,
        ///  HotListCategory.MembersClickedYes,
        ///  HotListCategory.MembersClickedNo,
        ///  HotListCategory.MembersClickedMaybe,
        ///  HotListCategory.MutualYes,
        ///  HotListCategory.WhoTeasedYou,
        ///  HotListCategory.WhoEmailedYou,
        ///  HotListCategory.WhoIMedYou,
        ///  HotListCategory.IgnoreList
        /// 
        /// </summary>
        /// <example>
        /// REQUEST:
        /// 
        /// GET http://api.spark.net/v2/brandId/1003/hotlist/counts?access_token=2/lvF62cvir2+kAvNxRaD/nFw5zyRdzZzlE/zCrsj+E1Y= HTTP/1.1
        /// Content-Type: application/json
        /// Accept: application/json
        /// 
        /// RESPONSE:
        /// 
        /// HTTP/1.1 200 OK
        /// Cache-Control: private
        /// Content-Type: application/json; charset=utf-8
        /// Server: Microsoft-IIS/7.5
        /// X-AspNetMvc-Version: 4.0
        /// X-Runtime: 00:00:00.0434363
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Access-Control-Allow-Origin: *
        /// Access-Control-Allow-Headers: X-Requested-With, X-Requested-By, Content-Type, Accept, Origin
        /// Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS
        /// Date: Fri, 30 Jan 2015 21:25:04 GMT
        /// Content-Length: 361
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "hasNewMail": true,
        ///    "myFavoritesTotal": 20,
        ///    "viewedMyProfileTotal": 356,
        ///    "addedMeToTheirFavoritesTotal": 15,
        ///    "viewedMyProfileNew": 0,
        ///    "addedMeToTheirFavoritesNew": 1,
        ///    "MembersYouTeased": 27,
        ///    "MembersYouEmailed": 8,
        ///    "MembersYouIMed": 18,
        ///    "MembersYouViewed": 379,
        ///    "MembersClickedYes": 16,
        ///    "MembersClickedNo": 22,
        ///    "MembersClickedMaybe": 11,
        ///    "MutualYes": 1,
        ///    "MutualYesNew": 0,
        ///    "WhoEmailedYou": 5,
        ///    "WhoTeasedYou": 2,
        ///    "WhoIMedYou": 5,
        ///    "IgnoreList": 0 //your blocked from contacting you list
        ///  }
        /// }
        /// 
        /// </example>
        /// <param name="countRequest"></param>
        /// <returns></returns>
        /// <remarks> 
        /// My Favorites is not included because counts are not shown for that category
        /// </remarks>
        /// 
        [ApiMethod(ResponseType = typeof (HotListCounts))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [Stopwatch]
        public ActionResult GetHotListCounts(HotListCountRequest countRequest)
        {
            // My Favorites is not included because counts are not shown for that category
            var memberId = GetMemberId();
            var hotListTotalCountDictionary = HotListAccess.GetHotListTotalCounts(countRequest.Brand,
                memberId, new List<HotListCategory>(RelevantCategories)
                {
                    HotListCategory.Default,
                    HotListCategory.MembersYouTeased,
                    HotListCategory.MembersYouEmailed,
                    HotListCategory.MembersYouIMed,
                    HotListCategory.MembersYouViewed,
                    HotListCategory.MembersClickedYes,
                    HotListCategory.MembersClickedNo,
                    HotListCategory.MembersClickedMaybe,
                    HotListCategory.WhoTeasedYou,
                    HotListCategory.WhoEmailedYou,
                    HotListCategory.WhoIMedYou,
                    HotListCategory.IgnoreList
                });

            var hotListNewCountDictionary = HotListAccess.GetHotListNewCounts(countRequest.Brand, memberId, RelevantCategories);

            var hotListCounts = new HotListCounts
            {
                MyFavoritesTotal = hotListTotalCountDictionary[HotListCategory.Default],
                ViewedMyProfileTotal =
                    hotListTotalCountDictionary[HotListCategory.WhoViewedYourProfile],
                AddedMeToTheirFavoritesTotal =
                    hotListTotalCountDictionary[HotListCategory.WhoAddedYouToTheirFavorites],
                ViewedMyProfileNew =
                    hotListNewCountDictionary[HotListCategory.WhoViewedYourProfile],
                AddedMeToTheirFavoritesNew =
                    hotListNewCountDictionary[HotListCategory.WhoAddedYouToTheirFavorites],
                HasNewMail = HotListAccess.HasNewMail(countRequest.Brand, memberId),
                MembersYouEmailed = hotListTotalCountDictionary[HotListCategory.MembersYouEmailed],
                MembersYouTeased = hotListTotalCountDictionary[HotListCategory.MembersYouTeased],
                MembersYouIMed = hotListTotalCountDictionary[HotListCategory.MembersYouIMed],
                MembersYouViewed = hotListTotalCountDictionary[HotListCategory.MembersYouViewed],
                MembersClickedYes = hotListTotalCountDictionary[HotListCategory.MembersClickedYes],
                MembersClickedNo = hotListTotalCountDictionary[HotListCategory.MembersClickedNo],
                MembersClickedMaybe = hotListTotalCountDictionary[HotListCategory.MembersClickedMaybe],
                MutualYes = hotListTotalCountDictionary[HotListCategory.MutualYes],
                MutualYesNew = hotListNewCountDictionary[HotListCategory.MutualYes],
                FavoritesCombinedTotal = hotListTotalCountDictionary[HotListCategory.WhoAddedYouToTheirFavorites],
                TeasedCombinedTotal = hotListTotalCountDictionary[HotListCategory.WhoTeasedYou],
                ViewedCombinedTotal = hotListTotalCountDictionary[HotListCategory.WhoViewedYourProfile],
                WhoEmailedYou = hotListTotalCountDictionary[HotListCategory.WhoEmailedYou],
                WhoTeasedYou = hotListTotalCountDictionary[HotListCategory.WhoTeasedYou],
                WhoIMedYou = hotListTotalCountDictionary[HotListCategory.WhoIMedYou],
                IgnoreList = hotListTotalCountDictionary[HotListCategory.IgnoreList]
            };

            var ctx = System.Web.HttpContext.Current;
            MiscUtils.FireAndForget(o =>
            {
                //set HttpContext for thread
                System.Web.HttpContext.Current = ctx;
                try
                {
                    ProfileAccess.UpdateBrandLastLogonDate(countRequest.Brand, memberId, true);
                }
                finally
                {
                    //unset HttpContext for thread (precaution for GC)
                    System.Web.HttpContext.Current = null;
                }
            },
            "ProfileAccess UpdateProperties", "GetHotListCounts: Failed to update last logon date");

            return new NewtonsoftJsonResult {Result = hotListCounts};
        }

        /// <summary>
        /// Get the entries for a given hot list category, use categories below as a reference
        /// </summary>
        ///  <remarks>
        ///     Hot List Categories
        /// 
        ///     MembersYouViewedInPushFlirts = -38,
        ///     MembersWhoSentYouAllAccessEmail = -37,
        ///     MembersYouSentAllAccessEmail = -36,
        ///     ExcludeFromList = -35, (Block from their search - they should not see me in their searches)
        ///     ExcludeList = -34, (Block from my search - i don't want to see them in my searches) 
        ///     ExcludeListInternal = -33,
        ///     MembersYouOmnidated = -32,
        ///     WhoOmnidatedYou = -31,
        ///     MembersYouGamed = -30,
        ///     WhoGamedYou = -29,
        ///     MemberYouSentJmeterInvitationTo = -28,
        ///     WhoJmeteredYou = -27,
        ///     MemberYouJmetered = -26,
        ///     WhoSentYouECards = -25,
        ///     MembersYouSentECards = -24,
        ///     ReportAbuse = -23,
        ///     MembersClickedNo = -22,
        ///     MembersClickedMaybe = -21,
        ///     MembersClickedYes = -20,
        ///     MembersYouVoiceMailed = -19,
        ///     MembersYouListenedTo = -18,
        ///     WhoVoiceMailedYou = -17,
        ///     WhoListenedToYou = -16,
        ///     MutualYes = -15,
        ///     WhoMatchedYou = -14,
        ///     IgnoreList = -13, (block from contacting me)
        ///     Profile = -12,
        ///     WhosOnline = -11,
        ///     YourMatches = -10,
        ///     MembersYouViewed = -9,
        ///     MembersYouIMed = -8,
        ///     MembersYouEmailed = -7,
        ///     MembersYouTeased = -6,
        ///     WhoIMedYou = -5,
        ///     WhoEmailedYou = -4,
        ///     WhoTeasedYou = -3,
        ///     WhoAddedYouToTheirFavorites = -2,
        ///     WhoViewedYourProfile = -1,
        ///     Default = 0,
        ///     Custom Categories
        ///     FavoritesCombined = -100,
        ///     TeasedCombined = -101,
        ///     ViewedCombined = -102
        /// </remarks>
        /// <example>
        /// REQUEST:
        /// 
        /// GET http://api.local.spark.net/v2/brandId/1003/hotlist/-1/pagesize/1/pagenumber/1?access_token=2/lvF62cvir2+kAvNxRaD/nFw5zyRdzZzlE/zCrsj+E1Y= HTTP/1.1
        /// Content-Type: 'application/json'
        /// Accept: application/json
        /// 
        /// RESPONSE:
        /// 
        /// HTTP/1.1 200 OK
        /// Cache-Control: private
        /// Content-Type: application/json; charset=utf-8
        /// Server: Microsoft-IIS/7.5
        /// X-AspNetMvc-Version: 4.0
        /// X-Runtime: 00:00:00.0024531
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Access-Control-Allow-Origin: *
        /// Access-Control-Allow-Headers: X-Requested-With, X-Requested-By, Content-Type, Accept, Origin
        /// Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS
        /// Date: Fri, 30 Jan 2015 22:37:04 GMT
        /// Content-Length: 2312
        /// {
        /// 	"code": 200,
	    ///     "status": "OK",
        /// 	"data": [
        /// 		{
        /// 			"actionDate": "2015-01-27T02:39:00Z",
        /// 			"category": "WhoViewedYourProfile",
        /// 			"comment": "",
        /// 			"miniProfile": {
        /// 				"username": "116361935",
        /// 				"approvedPhotoCount": 0,
        /// 				"defaultPhoto": null,
        /// 				"thumbnailUrl": null,
        /// 				"photoUrl": null,
        /// 				"maritalStatus": "Single",
        /// 				"gender": "Female",
        /// 				"genderSeeking": "Male",
        /// 				"lookingFor": null,
        /// 				"age": 46,
        /// 				"location": "Beverly Hills, CA ",
        /// 				"height": 137,
        /// 				"lastLoggedIn": "2015-01-30T21:48:05Z",
        /// 				"lastUpdatedProfile": "2014-09-24T16:35:30.237Z",
        /// 				"isOnline": false,
        /// 				"subscriptionStatus": "",
        /// 				"zipCode": "90210",
        /// 				"registrationDate": "2012-08-31T16:47:12.377Z",
        /// 				"subscriptionStatusGam": "never_sub",
        /// 				"jDateEthnicity": "Mixed Ethnic",
        /// 				"isHighlighted": false,
        /// 				"isSpotlighted": false,
        /// 				"selfSuspendedFlag": false,
        /// 				"adminSuspendedFlag": false,
        /// 				"blockContactByTarget": false,
        /// 				"blockSearchByTarget": false,
        /// 				"removeFromSearchByTarget": false,
        /// 				"blockContact": false,
        /// 				"blockSearch": false,
        /// 				"removeFromSearch": false,
        /// 				"isViewerFavorite": true,
        /// 				"id": 116361935
        /// 			}
        /// 		}
        /// 	]
        /// }
        /// 
        /// 
        /// </example>
        /// <param name="hotListRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (List<HotListEntry>))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [Stopwatch]
        [DeprecatedUse]
        [Obsolete("This call is obsolete. Please use <a href='/v2/docs/hotlist/gethotlistentriesv2'>GetHotListEntriesV2</a> instead.")]
        public ActionResult GetHotListEntries(HotListRequest hotListRequest)
        {
            HotListCategory category;
            try
            {
                category = (HotListCategory) Enum.Parse(typeof (HotListCategory), hotListRequest.HotListCategory, true);
            }
            catch (ArgumentException)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.UnknownHotListCategory,
                    "Unknown hotlist category");
            }

            var memberId = GetMemberId();
            var hotListEntries = HotListAccess.GetHotlistMembers(memberId, category, hotListRequest.Brand,
                hotListRequest.pageSize, hotListRequest.pageNumber,
                true);

            if (RelevantCategories.Contains(category))
            {
                var ctx = System.Web.HttpContext.Current;
                // only update categories in list, don't try to update a category such as My Favorites
                MiscUtils.FireAndForget(o =>
                {
                    //set HttpContext for thread
                    System.Web.HttpContext.Current = ctx;
                    try
                    {
                        HotListAccess.UpdateViewedHotListTimeStamp(hotListRequest.Brand, memberId, category);
                    }
                    finally
                    {
                        //unset HttpContext for thread (precaution for GC)
                        System.Web.HttpContext.Current = null;
                    }
                },
                "GetHotListEntries", 
                string.Format("UpdateViewedHotListTimeStamp: Failed to update viewed hotlist time stap for brandId: {0}, memberId: {1}, category: {2}", 
                hotListRequest.BrandId, memberId, category.ToString()));
            }
            return new NewtonsoftJsonResult {Result = hotListEntries};
        }

        /// <summary>
        /// Get the entries for a given hot list category, use categories below as a reference
        /// 
        /// To use this API you need to set the Accept version header to 2.1. i.e.:
        /// 
        /// Accept: 'application/json;version=2.1'
        /// 
        ///  </summary>
        ///  <remarks>
        ///     Hot List Categories
        ///     
        ///     ViewedCombined = -102,
        ///     TeasedCombined = -101,
        ///     FavoritesCombined = -100,
        ///     MembersYouViewedInPushFlirts = -38,
        ///     MembersWhoSentYouAllAccessEmail = -37,
        ///     MembersYouSentAllAccessEmail = -36,
        ///     ExcludeFromList = -35, (Block from their search - they should not see me in their searches)
        ///     ExcludeList = -34, (Block from my search - i don't want to see them in my searches) 
        ///     ExcludeListInternal = -33,
        ///     MembersYouOmnidated = -32,
        ///     WhoOmnidatedYou = -31,
        ///     MembersYouGamed = -30,
        ///     WhoGamedYou = -29,
        ///     MemberYouSentJmeterInvitationTo = -28,
        ///     WhoJmeteredYou = -27,
        ///     MemberYouJmetered = -26,
        ///     WhoSentYouECards = -25,
        ///     MembersYouSentECards = -24,
        ///     ReportAbuse = -23,
        ///     MembersClickedNo = -22,
        ///     MembersClickedMaybe = -21,
        ///     MembersClickedYes = -20,
        ///     MembersYouVoiceMailed = -19,
        ///     MembersYouListenedTo = -18,
        ///     WhoVoiceMailedYou = -17,
        ///     WhoListenedToYou = -16,
        ///     MutualYes = -15,
        ///     WhoMatchedYou = -14,
        ///     IgnoreList = -13, (block from contacting me)
        ///     Profile = -12,
        ///     WhosOnline = -11,
        ///     YourMatches = -10,
        ///     MembersYouViewed = -9,
        ///     MembersYouIMed = -8,
        ///     MembersYouEmailed = -7,
        ///     MembersYouTeased = -6,
        ///     WhoIMedYou = -5,
        ///     WhoEmailedYou = -4,
        ///     WhoTeasedYou = -3,
        ///     WhoAddedYouToTheirFavorites = -2,
        ///     WhoViewedYourProfile = -1,
        ///     Default = 0, //this is your favorites list
        /// </remarks>
        /// <example>
        /// REQUEST:
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/hotlist/-1/pagesize/1/pagenumber/1?access_token= 
        /// HTTP/1.1
        /// Content-Type: 'application/json'
        /// Accept: 'application/json;version=2.1'
        /// 
        /// RESPONSE:
        /// 
        /// HTTP/1.1 200 OK
        /// Cache-Control: private
        /// Content-Type: application/json; charset=utf-8
        /// Server: Microsoft-IIS/7.5
        /// X-AspNetMvc-Version: 4.0
        /// X-Runtime: 00:00:00.0024531
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Access-Control-Allow-Origin: *
        /// Access-Control-Allow-Headers: X-Requested-With, X-Requested-By, Content-Type, Accept, Origin
        /// Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS
        /// Date: Fri, 30 Jan 2015 22:37:04 GMT
        /// Content-Length: 2312
        /// 
        /// {
        /// 	"code": 200,
        /// 	"status": "OK",
        /// 	"data": [
        ///     {
        ///      "actionDate": "2015-01-12T21:55:00.000Z",
        ///      "category": "WhoViewedYourProfile",
        ///      "comment": "",
        ///      "miniProfile": {
        ///        "memberId": 100066873,
        ///        "username": "100066873",
        ///        "adminSuspendedFlag": false,
        ///        "age": 38,
        ///        "approvedPhotoCount": 10,
        ///        "blockContact": false,
        ///        "blockContactByTarget": false,
        ///        "blockSearch": false,
        ///        "blockSearchByTarget": false,
        ///        "defaultPhoto": {
        ///          "memberPhotoId": 3118553,
        ///          "fullId": 120209577,
        ///          "thumbId": 120198742,
        ///          "fullPath": "http://dev01.JDate.com/Photo1000/2013/04/16/10/120209577.jpg",
        ///          "thumbPath": "http://dev01.JDate.com/Photo1000/2013/04/16/10/120198742.jpg",
        ///          "caption": "",
        ///          "listorder": 1,
        ///          "isCaptionApproved": false,
        ///          "isPhotoApproved": true,
        ///          "IsMain": true,
        ///          "IsApprovedForMain": true
        ///        },
        ///        "gender": "Female",
        ///        "heightCm": 183,
        ///        "isHighlighted": false,
        ///        "isOnline": false,
        ///        "isSpotlighted": false,
        ///        "isViewersFavorite": false,
        ///        "jDateEthnicity": "Another Ethnic",
        ///        "jDateReligion": "Orthodox (Frum)",
        ///        "lastLoggedIn": "2015-07-07T23:03:41.000Z",
        ///        "lastUpdated": "2013-09-19T22:12:06.923Z",
        ///        "location": "Beverly Hills, CA ",
        ///        "lookingFor": [
        ///          "A date",
        ///          "Friend",
        ///          "Marriage"
        ///        ],
        ///        "maritalStatus": "Separated",
        ///        "matchRating": 100,
        ///        "passedFraudCheck": true,
        ///        "PhotoUrl": "http://dev01.JDate.com/Photo1000/2013/04/16/10/120209577.jpg",
        ///        "primaryPhoto": {
        ///          "memberPhotoId": 3118553,
        ///          "fullId": 120209577,
        ///          "thumbId": 120198742,
        ///          "fullPath": "http://dev01.JDate.com/Photo1000/2013/04/16/10/120209577.jpg",
        ///          "thumbPath": "http://dev01.JDate.com/Photo1000/2013/04/16/10/120198742.jpg",
        ///          "caption": "",
        ///          "listorder": 1,
        ///          "isCaptionApproved": false,
        ///          "isPhotoApproved": true,
        ///          "IsMain": true,
        ///          "IsApprovedForMain": true
        ///        },
        ///        "regionId": 3443817,
        ///        "registrationDate": "2007-11-01T20:50:04.230Z",
        ///        "removeFromSearch": false,
        ///        "removeFromSearchByTarget": false,
        ///        "seekingGender": "Male",
        ///        "selfSuspendedFlag": false,
        ///        "ThumbnailUrl": "http://dev01.JDate.com/Photo1000/2013/04/16/10/120198742.jpg",
        ///        "zipcode": "90210"
        ///      }
        ///    },
        ///     ...more list items
        ///   ]
        /// }
        /// 
        /// </example>
        /// <param name="hotListRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (List<HotListEntryV2>))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [Stopwatch]
        public ActionResult GetHotListEntriesV2(HotListRequest hotListRequest)
        {
            HotListCategory category;
            try
            {
                category = (HotListCategory) Enum.Parse(typeof (HotListCategory), hotListRequest.HotListCategory, true);
            }
            catch (ArgumentException)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.UnknownHotListCategory, "Unknown hotlist category");
            }

            var memberId = GetMemberId();

            var hotListEntries = HotListAccess.GetHotlistMembersV2(memberId, category, hotListRequest.Brand,
                hotListRequest.pageSize, hotListRequest.pageNumber, true);
            
            if (RelevantCategories.Contains(category))
            {
                var ctx = System.Web.HttpContext.Current;
                MiscUtils.FireAndForget(o =>
                {
                    //set HttpContext for thread
                    System.Web.HttpContext.Current = ctx;
                    try
                    {
                        HotListAccess.UpdateViewedHotListTimeStamp(hotListRequest.Brand, memberId, category);
                    }
                    finally
                    {
                        //unset HttpContext for thread (precaution for GC)
                        System.Web.HttpContext.Current = null;
                    }
                }, "GetHotListEntries",
                string.Format("UpdateViewedHotListTimeStamp: Failed to update viewed hotlist time stap for brandId: {0}, memberId: {1}, category: {2}",
                hotListRequest.BrandId, memberId, category));
            }
            return new NewtonsoftJsonResult {Result = hotListEntries};
        }

        /// <summary>
        /// Get the entries for the given hot list categories.  This will return back a single merged list that's sorted by date just like the GetHotListEntriesV2 endpoint.  But with this you can specify multiple hotlist categories.
        ///  </summary>
        ///  <remarks>
        ///     Hot List Categories
        ///     
        ///     ViewedCombined = -102,
        ///     TeasedCombined = -101,
        ///     FavoritesCombined = -100,
        ///     MembersYouViewedInPushFlirts = -38,
        ///     MembersWhoSentYouAllAccessEmail = -37,
        ///     MembersYouSentAllAccessEmail = -36,
        ///     ExcludeFromList = -35, (Block from their search - they should not see me in their searches)
        ///     ExcludeList = -34, (Block from my search - i don't want to see them in my searches) 
        ///     ExcludeListInternal = -33,
        ///     MembersYouOmnidated = -32,
        ///     WhoOmnidatedYou = -31,
        ///     MembersYouGamed = -30,
        ///     WhoGamedYou = -29,
        ///     MemberYouSentJmeterInvitationTo = -28,
        ///     WhoJmeteredYou = -27,
        ///     MemberYouJmetered = -26,
        ///     WhoSentYouECards = -25,
        ///     MembersYouSentECards = -24,
        ///     ReportAbuse = -23,
        ///     MembersClickedNo = -22,
        ///     MembersClickedMaybe = -21,
        ///     MembersClickedYes = -20,
        ///     MembersYouVoiceMailed = -19,
        ///     MembersYouListenedTo = -18,
        ///     WhoVoiceMailedYou = -17,
        ///     WhoListenedToYou = -16,
        ///     MutualYes = -15,
        ///     WhoMatchedYou = -14,
        ///     IgnoreList = -13, (block from contacting me)
        ///     Profile = -12,
        ///     WhosOnline = -11,
        ///     YourMatches = -10,
        ///     MembersYouViewed = -9,
        ///     MembersYouIMed = -8,
        ///     MembersYouEmailed = -7,
        ///     MembersYouTeased = -6,
        ///     WhoIMedYou = -5,
        ///     WhoEmailedYou = -4,
        ///     WhoTeasedYou = -3,
        ///     WhoAddedYouToTheirFavorites = -2,
        ///     WhoViewedYourProfile = -1,
        ///     Default = 0, //this is your favorites list
        /// </remarks>
        /// <example>
        /// Note:
        /// The hotlist categories are passed as a querystring "Categories" with a comma delimited list of category ids
        /// 
        /// This example gets back a merged list items that contains your Favorites, MembersYouViewed, and MembersYouEmailed for page 1 with 10 items
        /// 
        /// REQUEST:
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/hotlistmultiple/pagesize/10/pagenumber/1?access_token=&amp;Categories=0,-9,-7
        /// 
        /// RESPONSE: 
        /// 
        /// {
        /// 	"code": 200,
        /// 	"status": "OK",
        /// 	"data": [
        ///     {
        ///      "actionDate": "2015-01-12T21:55:00.000Z",
        ///      "category": "MembersYouViewed",
        ///      "comment": "",
        ///      "miniProfile": {
        ///        "memberId": 100066873,
        ///        "username": "100066873",
        ///        "adminSuspendedFlag": false,
        ///        "age": 38,
        ///        "approvedPhotoCount": 10,
        ///        "blockContact": false,
        ///        "blockContactByTarget": false,
        ///        "blockSearch": false,
        ///        "blockSearchByTarget": false,
        ///        "defaultPhoto": {
        ///          "memberPhotoId": 3118553,
        ///          "fullId": 120209577,
        ///          "thumbId": 120198742,
        ///          "fullPath": "http://dev01.JDate.com/Photo1000/2013/04/16/10/120209577.jpg",
        ///          "thumbPath": "http://dev01.JDate.com/Photo1000/2013/04/16/10/120198742.jpg",
        ///          "caption": "",
        ///          "listorder": 1,
        ///          "isCaptionApproved": false,
        ///          "isPhotoApproved": true,
        ///          "IsMain": true,
        ///          "IsApprovedForMain": true
        ///        },
        ///        "gender": "Female",
        ///        "heightCm": 183,
        ///        "isHighlighted": false,
        ///        "isOnline": false,
        ///        "isSpotlighted": false,
        ///        "isViewersFavorite": false,
        ///        "jDateEthnicity": "Another Ethnic",
        ///        "jDateReligion": "Orthodox (Frum)",
        ///        "lastLoggedIn": "2015-07-07T23:03:41.000Z",
        ///        "lastUpdated": "2013-09-19T22:12:06.923Z",
        ///        "location": "Beverly Hills, CA ",
        ///        "lookingFor": [
        ///          "A date",
        ///          "Friend",
        ///          "Marriage"
        ///        ],
        ///        "maritalStatus": "Separated",
        ///        "matchRating": 100,
        ///        "passedFraudCheck": true,
        ///        "PhotoUrl": "http://dev01.JDate.com/Photo1000/2013/04/16/10/120209577.jpg",
        ///        "primaryPhoto": {
        ///          "memberPhotoId": 3118553,
        ///          "fullId": 120209577,
        ///          "thumbId": 120198742,
        ///          "fullPath": "http://dev01.JDate.com/Photo1000/2013/04/16/10/120209577.jpg",
        ///          "thumbPath": "http://dev01.JDate.com/Photo1000/2013/04/16/10/120198742.jpg",
        ///          "caption": "",
        ///          "listorder": 1,
        ///          "isCaptionApproved": false,
        ///          "isPhotoApproved": true,
        ///          "IsMain": true,
        ///          "IsApprovedForMain": true
        ///        },
        ///        "regionId": 3443817,
        ///        "registrationDate": "2007-11-01T20:50:04.230Z",
        ///        "removeFromSearch": false,
        ///        "removeFromSearchByTarget": false,
        ///        "seekingGender": "Male",
        ///        "selfSuspendedFlag": false,
        ///        "ThumbnailUrl": "http://dev01.JDate.com/Photo1000/2013/04/16/10/120198742.jpg",
        ///        "zipcode": "90210"
        ///      }
        ///    },
        ///     ...more list items
        ///   ]
        /// }
        /// 
        /// </example>
        [ApiMethod(ResponseType = typeof(List<HotListEntryV2>))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [Stopwatch]
        public ActionResult GetHotListEntriesMultiple(HotlistMultipleRequest hotListRequest)
        {
            if (string.IsNullOrEmpty(hotListRequest.Categories))
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.UnknownHotListCategory, "Unknown hotlist categories");
            }
            else if (hotListRequest.CategoryList.Count <= 0)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.UnsupportedHotListCategory, "Unsupported hotlist categories");
            }

            var memberId = GetMemberId();

            var hotListEntries = HotListAccess.GetHotlistMembersMultiple(memberId, hotListRequest.CategoryList, hotListRequest.Brand,
                hotListRequest.PageSize, hotListRequest.PageNumber, true);

            return new NewtonsoftJsonResult { Result = hotListEntries };
        }

        /// <summary>
        ///     Add a member to another's hotlist using the hotlist category
        /// </summary>
        /// <example>
        /// 
        /// POST http://api.local.spark.net/v2/brandId/1003/hotlist/-1/targetmemberid/200?access_token=2/lvF62cvir2+kAvNxRaD/nFw5zyRdzZzlE/zCrsj+E1Y= HTTP/1.1
        /// Content-Type: 'application/json'
        /// Accept: 'application/json'
        /// User-Agent: Fiddler
        /// Host: api.local.spark.net
        /// 
        /// HTTP/1.1 200 OK
        /// Cache-Control: private
        /// Content-Type: application/json; charset=utf-8
        /// Server: Microsoft-IIS/7.5
        /// X-AspNetMvc-Version: 4.0
        /// X-Runtime: 00:00:00.0009405
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Access-Control-Allow-Origin: *
        /// Access-Control-Allow-Headers: X-Requested-With, X-Requested-By, Content-Type, Accept, Origin
        /// Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS
        /// Date: Fri, 30 Jan 2015 23:59:07 GMT
        /// Content-Length: 36
        /// 
        /// {"code":200,"status":"OK","data":{}}
        /// 
        /// </example>
        ///  <remarks>
        ///     Hot List Categories
        /// 
        ///     MembersYouViewedInPushFlirts = -38,
        ///     MembersWhoSentYouAllAccessEmail = -37,
        ///     MembersYouSentAllAccessEmail = -36,
        ///     ExcludeFromList = -35, (Block from their search - they should not see me in their searches)
        ///     ExcludeList = -34, (Block from my search - i don't want to see them in my searches) 
        ///     ExcludeListInternal = -33,
        ///     MembersYouOmnidated = -32,
        ///     WhoOmnidatedYou = -31,
        ///     MembersYouGamed = -30,
        ///     WhoGamedYou = -29,
        ///     MemberYouSentJmeterInvitationTo = -28,
        ///     WhoJmeteredYou = -27,
        ///     MemberYouJmetered = -26,
        ///     WhoSentYouECards = -25,
        ///     MembersYouSentECards = -24,
        ///     ReportAbuse = -23,
        ///     MembersClickedNo = -22,
        ///     MembersClickedMaybe = -21,
        ///     MembersClickedYes = -20,
        ///     MembersYouVoiceMailed = -19,
        ///     MembersYouListenedTo = -18,
        ///     WhoVoiceMailedYou = -17,
        ///     WhoListenedToYou = -16,
        ///     MutualYes = -15,
        ///     WhoMatchedYou = -14,
        ///     IgnoreList = -13, (block from contacting me)
        ///     Profile = -12,
        ///     WhosOnline = -11,
        ///     YourMatches = -10,
        ///     MembersYouViewed = -9,
        ///     MembersYouIMed = -8,
        ///     MembersYouEmailed = -7,
        ///     MembersYouTeased = -6,
        ///     WhoIMedYou = -5,
        ///     WhoEmailedYou = -4,
        ///     WhoTeasedYou = -3,
        ///     WhoAddedYouToTheirFavorites = -2,
        ///     WhoViewedYourProfile = -1,
        ///     Default = 0,
        /// </remarks>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [Stopwatch]
        public ActionResult AddToHotlist(HotlistChangeRequest request)
        {
            if (request.Method != null && request.Method.ToLowerInvariant() == "delete")
                return RemoveFromHotlist(request);
            HotListCategory category;
            if(!Enum.TryParse( request.HotListCategory, true, out category))
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.UnknownHotListCategory, "Unknown hotlist category");
            var sendStatus = HotListAccess.AddToHotlist(request.Brand, category, GetMemberId(), request.TargetMemberId, request.Comment);
            return GetStatusCodeResult(sendStatus);
        }

        private ActionResult GetStatusCodeResult(ListActionStatus status)
        {
            HttpSub400StatusCode substatusCode;
            switch (status)
            {
                case ListActionStatus.Success:
                    return Ok();
                case ListActionStatus.ExceededMaxAllowed:
                    substatusCode = HttpSub400StatusCode.ExceededMaxAllowed;
                    break;
                case ListActionStatus.ExceededMaxAllowedForMember:
                    substatusCode = HttpSub400StatusCode.ExceededMaxAllowedForMember;
                    break;
                case ListActionStatus.NotCategoryOwner:
                    substatusCode = HttpSub400StatusCode.NotCategoryOwner;
                    break;
                case ListActionStatus.HasAlreadyTeasedMember:
                    substatusCode = HttpSub400StatusCode.HasAlreadyTeasedMember;
                    break;
                case ListActionStatus.HasAlreadyAddedMemberToList:
                    substatusCode = HttpSub400StatusCode.HasAlreadyAddedMemberToList;
                    break;
                default:
                    substatusCode = HttpSub400StatusCode.FailedAddToHotList;
                    break;
            }

            return new SparkHttpBadRequestResult((int)substatusCode , status.ToString());
        }


        /// <summary>
        ///     Allows the adding of notes 
        /// </summary>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireAccessTokenV2]
        [Stopwatch]
        public ActionResult AddHotListNote(HotlistChangeRequest request)
        {
            if (request.MemberId == 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "MemberId is required");

            if (request.TargetMemberId == 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "TargetMemberId is required");

            if (request.Comment == null || string.IsNullOrEmpty(request.Comment))
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "comment is required");

            if (request.HotListCategory == null || string.IsNullOrEmpty(request.HotListCategory))
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "HotList category is required");

            HotListCategory category;
            try
            {
                category = (HotListCategory)Enum.Parse(typeof(HotListCategory), request.HotListCategory, true);
            }
            catch (ArgumentException)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.UnknownHotListCategory, "Unknown hotlist category");
            }

            HotListAccess.AddHotlistNote(request.Brand, category, request.MemberId, request.TargetMemberId, request.Comment, request.Timestamp);

            Log.LogInfoMessage(string.Format("BackfillComment: MemberId: {0}, TargetMemberId: {1}, HotListCategory: {2}, Comment: {3}, TimeStamp: {4} ",
                               request.MemberId, request.TargetMemberId, request.HotListCategory, request.Comment, request.Timestamp), null);


            return Ok();
        }



        /// <summary>
        ///     Remove a member from another's hotlist
        /// </summary>
        ///  <remarks>
        ///     Hot List Categories
        /// 
        ///     MembersYouViewedInPushFlirts = -38,
        ///     MembersWhoSentYouAllAccessEmail = -37,
        ///     MembersYouSentAllAccessEmail = -36,
        ///     ExcludeFromList = -35, (Block from their search - they should not see me in their searches)
        ///     ExcludeList = -34, (Block from my search - i don't want to see them in my searches) 
        ///     ExcludeListInternal = -33,
        ///     MembersYouOmnidated = -32,
        ///     WhoOmnidatedYou = -31,
        ///     MembersYouGamed = -30,
        ///     WhoGamedYou = -29,
        ///     MemberYouSentJmeterInvitationTo = -28,
        ///     WhoJmeteredYou = -27,
        ///     MemberYouJmetered = -26,
        ///     WhoSentYouECards = -25,
        ///     MembersYouSentECards = -24,
        ///     ReportAbuse = -23,
        ///     MembersClickedNo = -22,
        ///     MembersClickedMaybe = -21,
        ///     MembersClickedYes = -20,
        ///     MembersYouVoiceMailed = -19,
        ///     MembersYouListenedTo = -18,
        ///     WhoVoiceMailedYou = -17,
        ///     WhoListenedToYou = -16,
        ///     MutualYes = -15,
        ///     WhoMatchedYou = -14,
        ///     IgnoreList = -13, (block from contacting me)
        ///     Profile = -12,
        ///     WhosOnline = -11,
        ///     YourMatches = -10,
        ///     MembersYouViewed = -9,
        ///     MembersYouIMed = -8,
        ///     MembersYouEmailed = -7,
        ///     MembersYouTeased = -6,
        ///     WhoIMedYou = -5,
        ///     WhoEmailedYou = -4,
        ///     WhoTeasedYou = -3,
        ///     WhoAddedYouToTheirFavorites = -2,
        ///     WhoViewedYourProfile = -1,
        ///     Default = 0,
        /// </remarks>
        /// <example>
        /// REQUEST:
        /// 
        /// DELETE http://api.local.spark.net/v2/brandId/1003/hotlist/-1/targetmemberid/200?access_token=2/lvF62cvir2+kAvNxRaD/nFw5zyRdzZzlE/zCrsj+E1Y= HTTP/1.1
        /// Content-Type: 'application/json'
        /// Accept: 'application/json'
        /// User-Agent: Fiddler
        /// Host: api.local.spark.net
        /// 
        /// RESPONSE:
        /// 
        /// HTTP/1.1 200 OK
        /// Cache-Control: private
        /// Content-Type: application/json; charset=utf-8
        /// Server: Microsoft-IIS/7.5
        /// X-AspNetMvc-Version: 4.0
        /// X-Runtime: 00:00:00.5553579
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Access-Control-Allow-Origin: *
        /// Access-Control-Allow-Headers: X-Requested-With, X-Requested-By, Content-Type, Accept, Origin
        /// Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS
        /// Date: Sat, 31 Jan 2015 00:25:45 GMT
        /// Content-Length: 36
        /// 
        /// {"code":200,"status":"OK","data":{}}
        /// 
        /// </example>
        /// <param name="hotlistChangeRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [Stopwatch]
        public ActionResult RemoveFromHotlist(HotlistChangeRequest hotlistChangeRequest)
        {
            HotListCategory category;

            if (hotlistChangeRequest.TargetMemberId==0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument,
                    "TargetMemberId is required");
            try
            {
                category =
                    (HotListCategory) Enum.Parse(typeof (HotListCategory), hotlistChangeRequest.HotListCategory, true);
            }
            catch (ArgumentException)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.UnknownHotListCategory,
                    "Unknown hotlist category");
            }
            try
            {
                HotListAccess.RemoveFromHotlist(hotlistChangeRequest.Brand, category, GetMemberId(),
                    hotlistChangeRequest.TargetMemberId);
            }
            catch (Exception exception)
            {
                Log.LogError(string.Format("HotListAccess.RemoveFromHotlist(brand(id): {0}, category: {1}, memberId: {2}, targetMemberId: {3}", hotlistChangeRequest.Brand, category, GetMemberId(),
                    hotlistChangeRequest.TargetMemberId), exception, null);

                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedRemoveFromHotList,
                    string.Format("RemoveFromHotlist: Error removing memberid {0} from category:{1}.", hotlistChangeRequest.TargetMemberId, category));
            }

            return Ok();
        }

        /// <summary>
        /// This endpoint is obsolete please refer to DeleteFromHotlist instead.
        /// </summary>
        /// <param name="hotlistChangeRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        [Obsolete("Use DeleteFromHotlist instead")]
        [DeprecatedUse]
        public ActionResult DeleteFavorite(FavoriteRequest hotlistChangeRequest)
        {
            try
            {
                HotListAccess.RemoveFromHotlist(hotlistChangeRequest.Brand, HotListCategory.Default, GetMemberId(),
                    hotlistChangeRequest.FavoriteMemberId);
            }
            catch (Exception exception)
            {
                Log.LogError(string.Format("HotListAccess.RemoveFromHotlist(brand(id): {0}, category: HotListCategory.Default, memberId: {1}, targetMemberId: {2}",
                  hotlistChangeRequest.Brand.BrandID, GetMemberId(),
                  hotlistChangeRequest.FavoriteMemberId), exception, null);

                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedRemoveFromHotList,
                    "Error removing from favorites list.");
            }

            return Ok();
        }

        /// <summary>
        ///     Check whether a member is on another member's hotlist.
        ///     Only allowed for ignoreList (blocked list) as of 8/29/2012
        /// </summary>
        /// <param name="onHotListRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (bool))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [Stopwatch]
        public ActionResult OnMemberHotList(OnMemberHotListRequest onHotListRequest)
        {
            if (onHotListRequest.TargetMemberId == 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument,
                    "TargetMemberId is required");
            
            HotListCategory category;
            try
            {
                category =
                    (HotListCategory) Enum.Parse(typeof (HotListCategory), onHotListRequest.HotListCategory, true);
            }
            catch (ArgumentException)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.UnknownHotListCategory,
                    "Unknown hotlist category");
            }
            if (category != HotListCategory.IgnoreList)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.UnsupportedHotListCategory,
                    "Unsupported hotlist category");
            }

            var onList = HotListAccess.MemberIsOnHotlist(onHotListRequest.Brand, category, GetMemberId(),
                onHotListRequest.TargetMemberId);

            return new NewtonsoftJsonResult {Result = onList};
        }

        /// <summary>
        ///     Saves YNM vote
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        [HttpPost]
        [Stopwatch]
        public ActionResult SaveYNMVote(YNMVoteRequest request)
        {
            var ctx = System.Web.HttpContext.Current;
            int memberId = GetMemberId();
            MiscUtils.FireAndForget(o =>
            {
                //set HttpContext for thread
                System.Web.HttpContext.Current = ctx;
                try
                {
                    HotListAccess.SaveYNMVote(request, memberId);
                    Log.LogInfoMessage(string.Format("Completed HotListController.SaveYNMVote(brand(id): {0}, category: HotListCategory.Default, memberId: {1}, targetMemberId: {2}",
                        request.Brand.BrandID, memberId, request.AddMemberId), null);
                }
                catch (Exception ex)
                {
                    Log.LogError(string.Format("Failed HotListController.SaveYNMVote(brand(id): {0}, category: HotListCategory.Default, memberId: {1}, targetMemberId: {2}",
                        request.Brand.BrandID, memberId, request.AddMemberId), ex, null);
                }
                finally
                {
                    //unset HttpContext for thread (precaution for GC)
                    System.Web.HttpContext.Current = null;
                }
            },
                "HotList SaveYNMVote", "Failed to SaveYNMVote");

            return Ok();
        }

        /// <summary>
        ///     Gets YNM vote
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (YNMVoteResponse))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [Stopwatch]
        [HttpGet]
        public ActionResult GetYNMVote(YNMVoteRequest request)
        {
            if (request.AddMemberId == 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument,
                    "AddMemberId is required");

            var clickMask = ListSA.Instance.GetClickMask(GetMemberId(), request.AddMemberId,
                request.Brand.Site.Community.CommunityID,
                request.Brand.Site.SiteID);

            var response = new YNMVoteResponse();

            // Mutual Yes
            if ((((clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes) &&
                 ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)))
            {
                response.Yes = true;
                response.MutualYes = true;
            }
            else if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)
            {
                response.Yes = true;
            }
            else if ((clickMask & ClickMask.MemberNo) == ClickMask.MemberNo)
            {
                response.No = true;
            }
            else if ((clickMask & ClickMask.MemberMaybe) == ClickMask.MemberMaybe)
            {
                response.Maybe = true;
            }

            return new NewtonsoftJsonResult {Result = response};
        }

        /// <summary>
        /// This gets contact history between two members, and is based on hotlist
        /// </summary>
        /// <remarks>
        /// The contact history includes actions based on the following hotlist categories and logical mutual categories, both of which are shown as "HistoryType" for each contact history item.
        /// 
        /// HOTLIST CATEGORIES
        /// 
        ///  Default //this is your favorites list
        ///  WhoAddedYouToTheirFavorites
        ///  
        ///  MembersYouViewed
        ///  WhoViewedYourProfile
        ///  
        ///  MembersYouEmailed
        ///  WhoEmailedYou
        ///  
        ///  MembersYouSentECards
        ///  WhoSentYouECards
        ///  
        ///  MembersYouTeased
        ///  WhoTeasedYou
        ///  
        ///  MembersYouIMed
        ///  WhoIMedYou
        ///  
        ///  //blocked history will only show your blocked actions
        ///  IgnoreList //you blocked target from contacting you
        ///  ExcludeList //you blocked target from showing up in your searches
        ///  ExcludeFromList //you blocked target from seeing you in their searches
        ///  
        ///  //ynm votes history will only show your ynm actions
        ///  MembersClickedNo
        ///  MembersClickedMaybe
        ///  MembersClickedYes
        ///  
        /// 
        /// LOGICAL MUTUAL HOTLIST CATEGORIES
        /// It also includes logical "Mutual" entries to indicate that you both have done the same thing with each other.
        /// 
        /// Friend //you both favorited each other
        /// ViewProfile
        /// Email
        /// ECard
        /// Tease
        /// IM
        /// YNM //you both said yes
        /// 
        /// </remarks>
        /// <example>
        /// Note:
        /// The response are hotlist entries and they have a category which can be used to display the appropriate copies
        /// The mini profiles returned are based on the resultsetprofilev2 attributeset
        /// The mutual entries will not have a valid date and will appear first in the list, they also have the isMutual property set to true
        /// 
        /// Example:
        /// This will get contact history between member 1116690655 (based on access token) and member 1116693286 (passed in as parameter)
        /// Since we will usually already have the mini profile data for the two members, we will pass in false for includeMiniProfile
        /// 
        /// GET http://api.stage3.spark.net/v2/brandid/1003/hotlist/contacthistory/targetmemberid/1116693286/includeMiniProfile/false?access_token=
        /// 
        /// RESPONSE: 
        /// 
        ///{
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "MemberID":  1116690655,
        ///    "TargetMemberID": 1116693286,
        ///    "MiniProfile": null,
        ///    "TargetMiniProfile": null,
        ///    "ContactHistoryList": [
        ///      {
        ///        "actionDate": "9999-12-31T23:59:59.999Z",
        ///        "historyType": "ViewProfile",
        ///        "isMutual": true
        ///      },
        ///      {
        ///        "actionDate": "9999-12-31T23:59:59.999Z",
        ///        "historyType": "IM",
        ///        "isMutual": true
        ///      },
        ///      {
        ///        "actionDate": "2015-02-27T02:41:00.000Z",
        ///        "historyType": "WhoViewedYourProfile",
        ///        "isMutual": false
        ///      },
        ///      {
        ///        "actionDate": "2014-10-09T22:27:00.000Z",
        ///        "historyType": "MembersClickedYes",
        ///        "isMutual": false
        ///      },
        ///      {
        ///        "actionDate": "2014-10-09T22:24:00.000Z",
        ///        "historyType": "MembersYouViewed",
        ///        "isMutual": false
        ///      },
        ///      {
        ///        "actionDate": "2014-10-09T01:04:00.000Z",
        ///        "historyType": "MembersYouEmailed",
        ///        "isMutual": false
        ///      },
        ///      {
        ///        "actionDate": "2014-01-28T21:38:00.000Z",
        ///        "historyType": "Default",
        ///        "isMutual": false
        ///      },
        ///      {
        ///        "actionDate": "2013-12-23T21:42:00.000Z",
        ///        "historyType": "WhoIMedYou",
        ///        "isMutual": false
        ///      },
        ///      {
        ///        "actionDate": "2013-12-23T21:39:00.000Z",
        ///        "historyType": "MembersYouIMed",
        ///        "isMutual": false
        ///      },
        ///      {
        ///        "actionDate": "2013-11-20T23:27:00.000Z",
        ///        "historyType": "WhoTeasedYou",
        ///        "isMutual": false
        ///      }
        ///    ]
        ///  }
        ///}
        /// 
        /// </example>
        /// <param name="TargetMemberID" required="true">The target member ID</param>
        /// <param name="includeMiniProfile" required="true">Specifies whether to return the mini profile data for both members</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(ContactHistoryResponse))]
        [RequireAccessTokenV2]
        [Stopwatch]
        public ActionResult GetContactHistory(ContactHistoryRequest request)
        {
            var memberId = GetMemberId();

            if (request.TargetMemberID <= 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument,
                    "TargetMemberId is required");

            var contactHistoryResponse = new ContactHistoryResponse();
            contactHistoryResponse.MemberID = memberId;
            contactHistoryResponse.TargetMemberID = request.TargetMemberID;
            contactHistoryResponse.ContactHistoryList = HotListAccess.GetContactHistory(memberId, request.TargetMemberID, request.Brand);
            if (request.IncludeMiniProfile)
            {
                contactHistoryResponse.MiniProfile = ProfileAccess.GetAttributeSetWithPhotoAttributes(request.Brand, memberId, request.TargetMemberID, "resultsetprofilev2").AttributeSet;
                contactHistoryResponse.TargetMiniProfile = ProfileAccess.GetAttributeSetWithPhotoAttributes(request.Brand, request.TargetMemberID, memberId, "resultsetprofilev2").AttributeSet;
            }


            return new NewtonsoftJsonResult { Result = contactHistoryResponse };
        }
    }
}