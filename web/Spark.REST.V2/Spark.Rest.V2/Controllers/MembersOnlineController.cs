﻿#region

using System;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.Session.ServiceAdapters;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.Entities.MembersOnline;
using Spark.REST.Models;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Controllers;
using Spark.Rest.V2.DataAccess.MembersOnline;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

#endregion

namespace Spark.REST.Controllers
{
    /// <summary>
    /// </summary>
    public class MembersOnlineController : SparkControllerBase
    {
        /// <summary>
        ///     For adding a member to MOL.
        ///     If the member has their hidden setting set to true, they're not added to MOL.
        /// </summary>
        /// <param name="request"></param>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        public void Add(MemberRequest request)
        {
            var memberId = GetMemberId();
            try
            {
                var appId = (int) HttpContext.Items["tokenAppId"];

                var ctx = System.Web.HttpContext.Current;
                MiscUtils.FireAndForget(o =>
                {
                    //set HttpContext for thread
                    System.Web.HttpContext.Current = ctx;
                    try
                    {
                        MembersOnlineAccess.Instance.Add(request.Brand, GetMemberId(), appId);
                    }
                    finally
                    {
                        //unset HttpContext for thread (precaution for GC)
                        System.Web.HttpContext.Current = null;
                    }
                },
                    "MOL add", "Failed to add to MOL");
            }
            catch (Exception ex)
            {
                throw new Exception(
                    "Error adding membersonline. CommunityId:" + request.Brand.Site.Community.CommunityID +
                    ", MemberId:" + memberId, ex);
            }
        }

        /// <summary>
        ///     For removing a member from MOL
        /// </summary>
        /// <param name="request"></param>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        public void Remove(MemberRequest request)
        {
            try
            {
                var ctx = System.Web.HttpContext.Current;
                MiscUtils.FireAndForget(o =>
                {
                    //set HttpContext for thread
                    System.Web.HttpContext.Current = ctx;
                    try
                    {
                        MembersOnlineAccess.Instance.Remove(request.Brand.Site.Community.CommunityID, GetMemberId());
                    }
                    finally
                    {
                        //unset HttpContext for thread (precaution for GC)
                        System.Web.HttpContext.Current = null;
                    }
                },
                    "MOL remove", "Failed to remove from MOL");
            }
            catch (Exception ex)
            {
                throw new Exception("Error deleting(removing) session.", ex);
            }
        }

        /// <summary>
        ///     Returns the total number of sessoin users which is used to show the total mol users instead.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (MembersOnlineCount))]
        public ActionResult GetMembersOnlineCount(BrandRequest request)
        {
            var count = MembersOnlineAccess.Instance.GetMOLCommunityCount(request.Brand, false);
            var countResult = new MembersOnlineCount {Count = count};
            return new NewtonsoftJsonResult {Result = countResult};
        }

        /// <summary>
        ///     Retrieves a list of online members matching the requesting member's default preferences.
        /// </summary>
        /// <param name="request">
        ///     Should be passed in as query string parameters. SortType:1=Photos,2=New,3=UserName,4=Age,5=Gender
        ///     PageSize:Optional PageNumber:Optional RegionId:Optional LanguageId:Optional SeekingGender:Optional[Male, Female]
        /// </param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (GetMembersOnlineResponse))]
        [RequireAccessTokenV2]
        [HttpGet]
        public ActionResult GetMembersOnlineList(GetMembersOnlineRequest request)
        {
            var response = new GetMembersOnlineResponse();

            if (request.PageSize > 50)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidPageSize,
                    "PageSize cannot be more than 50.");
            }

            if (request.MinAge > request.MaxAge)
            {
                // todo: Need a generic status code for bad param
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidPageSize,
                    "MinAge cannot be bigger than MaxAge.");
            }

            SortFieldType sortType;
            switch (request.SortType)
            {
                case 2:
                    sortType = SortFieldType.InsertDate;
                    break;
                case 3:
                    sortType = SortFieldType.UserName;
                    break;
                case 4:
                    sortType = SortFieldType.Age;
                    break;
                case 5:
                    sortType = SortFieldType.Gender;
                    break;
                default:
                    sortType = SortFieldType.HasPhoto;
                    break;
            }

            var genderMask = 0;
            if (request.SeekingGender != String.Empty)
            {
                if (request.SeekingGender.ToLower() != "male" && request.SeekingGender.ToLower() != "female")
                    // todo: Need a generic status code for bad param
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidPageSize,
                        "Must specify Male or Female for Seeking Gender.");

                var member = MemberSA.Instance.GetMember(GetMemberId(), MemberLoadFlags.None);
                var memberMaskValue = GenderUtils.GetGenderMaskSelf(member.GetAttributeInt(request.Brand, "GenderMask"));
                var seekingString = "Seeking" + request.SeekingGender;
                var seekingMaskValue = (int) Enum.Parse(typeof (GenderMask), seekingString, true);
                genderMask = memberMaskValue | seekingMaskValue;
                genderMask = GenderUtils.FlipMaskIfHeterosexual(genderMask);
            }

            response.Members = MembersOnlineAccess.Instance.GetMembersOnlineList(request.Brand, GetMemberId(),
                request.PageNumber,
                request.PageSize,
                sortType,
                request.MinAge,
                request.MaxAge,
                request.RegionId,
                request.LanguageId,
                genderMask);

            return new NewtonsoftJsonResult {Result = response};
        }
    }
}