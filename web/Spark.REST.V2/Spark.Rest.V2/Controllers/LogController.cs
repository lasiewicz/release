﻿#region

using System.Web.Mvc;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.Serialization;
using Spark.Rest.V2.DataAccess.Log;
using Spark.Rest.V2.DataAccess.Log.Interfaces;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.Controllers;

#endregion

// Keep the name space this way for Docs to work

namespace Spark.Rest.Controllers
{
    public class LogController : SparkControllerBase
    {
        private readonly ILogAccess _logAccess = new LogAccess();

        /// <summary>
        ///     Logs the information
        /// </summary>
        /// <example>
        ///     Request Url: POST /v2/log/info?access_token=[ACCESS_TOKEN]
        ///     Request Headers: Content-Type: application/json
        ///     Request Body:
        ///     {
        ///     "message":"info message",
        ///     "timestamp":"2015-08-21"
        ///     }
        /// </example>
        /// <param name="message">The message</param>
        /// <param name="timestamp">The timestamp</param>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        ///     Error Messages
        ///     Bad Request(400) - GenericFailedValidation(400098) - InvalidMessage
        ///     Bad Request(400) - GenericFailedValidation(400098) - InvalidTimestamp
        /// </responseAdditionalInfo>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        [HttpPost]
        public ActionResult LogInfo(string message, string timestamp)
        {
            var result = _logAccess.LogMessage(GetCustomDataForReportingErrors(), LogAccess.MessageType.InfoFormat,
                message, timestamp, GetAppId, GetMemberId());

            if (result.LogMessageErrorType == LogAccess.LogMessageErrorType.None)
                return Ok();

            return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.GenericFailedValidation,
                result.LogMessageErrorType.ToString());
        }

        /// <summary>
        ///     Logs the error
        /// </summary>
        /// <example>
        ///     Request Url: POST /v2/log/error?access_token=[ACCESS_TOKEN]
        ///     Request Headers: Content-Type: application/json
        ///     Request Body:
        ///     {
        ///     "message":"error message",
        ///     "timestamp":"2015-08-21"
        ///     }
        /// </example>
        /// <param name="message">The message</param>
        /// <param name="timestamp">The timestamp</param>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        ///     Error Messages
        ///     Bad Request(400) - GenericFailedValidation(400098) - InvalidMessage
        ///     Bad Request(400) - GenericFailedValidation(400098) - InvalidTimestamp
        /// </responseAdditionalInfo>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        [HttpPost]
        public ActionResult LogError(string message, string timestamp)
        {
            var result = _logAccess.LogMessage(GetCustomDataForReportingErrors(), LogAccess.MessageType.ErrorFormat,
                message, timestamp, GetAppId, GetMemberId());

            if (result.LogMessageErrorType == LogAccess.LogMessageErrorType.None)
                return Ok();

            return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.GenericFailedValidation,
                result.LogMessageErrorType.ToString());
        }
    }
}