﻿#region

using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Spark.Logger;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.Serialization;
using Spark.Rest.V2.DataAccess.Premium;
using Spark.Rest.V2.Entities.Premium;
using Spark.Rest.V2.Models.Premium;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.Controllers;
using Spark.REST.Models;

#endregion

namespace Spark.Rest.Controllers
{
    public partial class PremiumController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(PremiumController));

        /// <summary>
        /// Gets the spotlight settings for a member.  The spotlight settings specifies whether it's enabled and the target audience that should see their spotlighted profile
        /// </summary>
        /// <example>
        /// GET http://api.local.spark.net/v2/brandId/1003/premium/spotlightsettings?access_token=
        /// 
        /// RESPONSE
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": {
        ///        "isSpotlightEnabled": true,
        ///        "gender": "Female",
        ///        "seekingGender": "Male",
        ///        "minAge": 28,
        ///        "maxAge": 48,
        ///        "distance": 40,
        ///        "regionId": 3444725
        ///    }
        ///}
        /// </example>
        /// <param name="memberId">Not used, derived from access token</param>
        /// <param name="brandId">Not used, specified as part of URL path</param>
        /// <param name="siteId">Not used</param>
        /// <param name="Brand">Not used</param>
        [ApiMethod(ResponseType = typeof(SpotlightSettingsResponse))]
        [RequireAccessTokenV2]
        [HttpGet]
        public ActionResult GetSpotlightSettings(MemberRequest request)
        {
            var spotlightSettingsResponse = new SpotlightSettingsResponse();
            spotlightSettingsResponse.LoadSettings(PremiumAccess.Instance.GetSpotlightSettings(request.Brand, GetMemberId()));

            return new NewtonsoftJsonResult { Result = spotlightSettingsResponse };
        }

        /// <summary>
        /// Updates the spotlight settings for a member.  The spotlight settings specifies whether it's enabled and the target audience that should see their spotlighted profile
        /// </summary>
        /// <example>
        /// POST http://api.local.spark.net/v2/brandId/1003/premium/spotlightsettings?access_token=
        /// 
        /// {
        ///    "IsSpotlightEnabled": true,
        ///    "Gender": "Female",
        ///    "SeekingGender": "Male",
        ///    "MinAge": 20,
        ///    "MaxAge": 75,
        ///    "Distance": 50,
        ///    "RegionId": 3444725
        ///}
        ///
        /// 
        /// RESPONSE
        /// 
        /// {"code":200,"status":"OK","data":{}}
        /// 
        /// </example>
        /// <param name="IsSpotlightEnabled" required="true">Determines whether the user has their spotlight premium feature enabled</param>
        /// <param name="Gender" required="true">The gender of the target audience that should see the user spotlighted profile.  So specifying Female seeking male will only show your profile to people who are females and are seeking males</param>
        /// <param name="SeekingGender" required="true">The seeking gender of your target audience</param>
        /// <param name="MinAge" required="true">18-99</param>
        /// <param name="MaxAge" required="true">18-99</param>
        /// <param name="Distance" required="true">The radius in miles from the location</param>
        /// <param name="RegionId" required="true">RegionId representing the location</param>
        /// <param name="memberId">Not used, derived from access token</param>
        /// <param name="brandId">Not used, specified as part of URL path</param>
        /// <param name="siteId">Not used</param>
        /// <param name="Brand">Not used</param>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireAccessTokenV2]
        [HttpPost]
        public ActionResult UpdateSpotlightSettings(UpdateSpotlightSettingsRequest request)
        {
            HttpSub400StatusCode updateStatus = PremiumAccess.Instance.UpdateSpotlightSettings(request, GetMemberId());
            if (updateStatus != HttpSub400StatusCode.None)
            {
                return new SparkHttpBadRequestResult((int)updateStatus,
                        "Failed validation");
            }

            return Ok();
        }


    }

    /// <summary>
    ///     Question and Answer methods
    /// </summary>
    public partial class PremiumController
    {
        private IPremiumAccessQuestionAnswer _premiumAccessQuestionAnswer;

        /// <summary>
        /// </summary>
        public IPremiumAccessQuestionAnswer PremiumAccessQuestionAnswer
        {
            get
            {
                if (_premiumAccessQuestionAnswer != null)
                    return _premiumAccessQuestionAnswer;

                // make sure the 1.1 SA isn't referenced
                _premiumAccessQuestionAnswer = new PremiumAccessQuestionAnswer(QuestionAnswerSA.Instance);

                return _premiumAccessQuestionAnswer;
            }

            set { _premiumAccessQuestionAnswer = value; }
        }

        /// <summary>
        ///     For retrieving a specific question given its Id. sortOrder = 0 - by newest, sortOrder = 1 - by oldest
        /// </summary>
        /// <example>
        ///Request URL - /v2/brandId/1003/premium/questionanswer/question/2000?access_token=[ACCESS_TOKEN]&amp;pageSize=20&amp;pageNumber=1&amp;sortOrder=0
        ///Request Content Type - application/json
        /// 
        ///Response Body
        ///{
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "QuestionID": 2000,
        ///    "SiteID": 103,
        ///    "Text": "If you could change one thing about yourself, what would it be and why?",
        ///    "StartDate": "2014-09-09T00:00:00.000Z",
        ///    "EndDate": "2020-09-09T00:00:00.000Z",
        ///    "InsertDate": "2014-09-09T08:53:28.820Z",
        ///    "UpdateDate": "2014-10-08T13:35:47.650Z",
        ///    "AdminMemberID": -2147483648,
        ///    "Weight": 1,
        ///    "Active": true,
        ///    "QuestionType": 1,
        ///    "QuestionTypeID": 1,
        ///"Answers": [
        ///  {
        ///    "AnswerID": 201419,
        ///    "QuestionID": 825,
        ///    "MemberID": 101805229,
        ///    "AnswerValue": "fffef3e2",
        ///    "AnswerValuePending": "fefff32",
        ///    "AdminMemberID": 101805229,
        ///    "AnswerType": 1,
        ///    "AnswerStatus": 2,
        ///    "InsertDate": "2015-06-10T15:29:21.050Z",
        ///    "UpdateDate": "2015-06-12T14:35:31.230Z",
        ///    "IsDirty": false,
        ///    "GenderMask": 9,
        ///    "GlobalStatusMask": 0,
        ///    "SelfSuspendedFlag": 0,
        ///    "HasApprovedPhotos": false
        ///  }
        ///],
        ///    "StockAnswers": [],
        ///    "CacheMode": 0,
        ///    "CachePriority": 5,
        ///    "CacheTTLSeconds": 300
        ///  }
        ///}
        /// 
        /// When request question is not found.
        ///{
        ///    "code": 404,
        ///    "status": "NotFound",
        ///    "error": {
        ///        "code": 0,
        ///        "message": "Question not found."
        ///    }
        ///}
        /// </example>
        /// <param name="QuestionId" required="true">Unique Question Id</param>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        /// </responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof (QuestionResponse))]
        [RequireAccessTokenV2]
        [HttpGet]
        public ActionResult GetQuestion(BrandRequest brandRequest, int questionId, int pageSize, int pageNumber, AnswerSortOrder sortOrder = AnswerSortOrder.Newest)
        {
            #region Validation

            if (brandRequest.Brand == null)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingArgument, "brand cannot be null");
            }

            if (questionId <= 0)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingArgument,
                    "questionId cannot be less than 0");
            }

            if (pageNumber < 1)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericError, "Page number is 1-indexed, must be at least 1.");
            }

            if (pageSize > 50)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidPageSize,
                    "pageSize cannot be more than 50.");
            }

            if (pageSize <= 0)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument,
                    "pageSize cannot be less than 1");
            }

            #endregion

            var question = PremiumAccessQuestionAnswer.GetQuestion(questionId,pageSize,pageNumber, sortOrder);

            // no question was found given the Id, return Not Found.
            if (question == null)
            {
                return new SparkHttpStatusCodeResult((int) HttpStatusCode.NotFound, (int) HttpSub401StatusCode.None,
                    "Question not found.");
            }
            
            return new NewtonsoftJsonResult {Result = question, IgnoreNotImplementedException = true };
        }

        /// <summary>
        ///     For retrieving a list of questions given its brandId
        /// </summary>
        /// <example>
        ///Request URL - /v2/brandId/1003/premium/questionanswer/activequestions?access_token=[ACCESS_TOKEN]
        ///Request Content Type - application/json
        /// 
        ///Response Body
        /// 
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "Questions": [
        ///      {
        ///        "QuestionID": 838,
        ///        "SiteID": 103,
        ///        "Text": "Before going on a date with someone you met through an online dating service, where do you do additional \"recon\"?",
        ///        "StartDate": "2012-03-07T00:00:00.000Z",
        ///        "EndDate": "2020-01-01T00:00:00.000Z",
        ///        "InsertDate": "2012-03-07T17:15:38.513Z",
        ///        "UpdateDate": "2012-03-07T17:15:38.513Z",
        ///        "AdminMemberID": -2147483648,
        ///        "Weight": 30,
        ///        "Active": true,
        ///        "QuestionType": 1,
        ///        "QuestionTypeID": 2,
        ///        "Answers": [],
        ///        "StockAnswers": [],
        ///        "CacheMode": 0,
        ///        "CachePriority": 5,
        ///        "CacheTTLSeconds": 300
        ///      },
        ///      {
        ///        "QuestionID": 837,
        ///        "SiteID": 103,
        ///        "Text": "How many online dating services are you a member of or have you been a member of?",
        ///        "StartDate": "2012-03-07T00:00:00.000Z",
        ///        "EndDate": "2020-01-01T00:00:00.000Z",
        ///        "InsertDate": "2012-03-07T17:15:38.483Z",
        ///        "UpdateDate": "2012-03-07T17:15:38.483Z",
        ///        "AdminMemberID": -2147483648,
        ///        "Weight": 30,
        ///        "Active": true,
        ///        "QuestionType": 1,
        ///        "QuestionTypeID": 2,
        ///        "Answers": [],
        ///        "StockAnswers": [],
        ///        "CacheMode": 0,
        ///        "CachePriority": 5,
        ///        "CacheTTLSeconds": 300
        ///      }    
        ///    ],
        ///    "QuestionsAZ": [
        ///      {
        ///        "QuestionID": 838,
        ///        "SiteID": 103,
        ///        "Text": "Before going on a date with someone you met through an online dating service, where do you do additional \"recon\"?",
        ///        "StartDate": "2012-03-07T00:00:00.000Z",
        ///        "EndDate": "2020-01-01T00:00:00.000Z",
        ///        "InsertDate": "2012-03-07T17:15:38.513Z",
        ///        "UpdateDate": "2012-03-07T17:15:38.513Z",
        ///        "AdminMemberID": -2147483648,
        ///        "Weight": 30,
        ///        "Active": true,
        ///        "QuestionType": 1,
        ///        "QuestionTypeID": 2,
        ///        "Answers": [],
        ///        "StockAnswers": [],
        ///        "CacheMode": 0,
        ///        "CachePriority": 5,
        ///        "CacheTTLSeconds": 300
        ///      },
        ///      {
        ///        "QuestionID": 826,
        ///        "SiteID": 103,
        ///        "Text": "Do you think being described as \"quirky\" is a compliment or an insult?",
        ///        "StartDate": "2012-03-07T00:00:00.000Z",
        ///        "EndDate": "2020-01-01T00:00:00.000Z",
        ///        "InsertDate": "2012-03-07T17:15:38.410Z",
        ///        "UpdateDate": "2012-03-07T17:15:38.410Z",
        ///        "AdminMemberID": -2147483648,
        ///        "Weight": 30,
        ///        "Active": true,
        ///        "QuestionType": 1,
        ///        "QuestionTypeID": 1,
        ///        "Answers": [],
        ///        "StockAnswers": [],
        ///        "CacheMode": 0,
        ///        "CachePriority": 5,
        ///        "CacheTTLSeconds": 300
        ///      }    
        ///    ],
        ///    "SiteID": 103,
        ///    "CacheMode": 0,
        ///    "CachePriority": 2,
        ///    "CacheTTLSeconds": 300
        ///  }
        ///}
        /// </example>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        /// </responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof(QuestionList))]
        [RequireAccessTokenV2]
        [HttpGet]
        public ActionResult GetActiveQuestionList(BrandRequest brandRequest)
        {
            #region Validation

            if (brandRequest.Brand == null)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "brand cannot be null");
            }

            #endregion

            var questions = PremiumAccessQuestionAnswer.GetActiveQuestionList(brandRequest.Brand.Site.SiteID);

            return new NewtonsoftJsonResult { Result = questions, IgnoreNotImplementedException = true };
        }

        /// <summary>
        ///     For retrieving a paged list of questions given its brandId and paging arguments. "total" contains the total number of rows found.
        /// </summary>
        /// <example>
        ///Request URL - /v2/brandId/1003/premium/questionanswer/activequestionspaged?pageSize=[PAGE_SIZE]&amp;pageNumber=[PAGE_NUMBER]&amp;sort=[recent | az]&amp;access_token=[ACCESS_TOKEN]
        ///Request Content Type - application/json
        /// 
        ///Response Body
        /// 
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "questions": [
        ///      {
        ///        "TotalAnswers": 3,
        ///        "QuestionID": 838,
        ///        "SiteID": 103,
        ///        "Text": "Before going on a date with someone you met through an online dating service, where do you do additional \"recon\"?",
        ///        "StartDate": "2012-03-07T00:00:00.000Z",
        ///        "EndDate": "2020-01-01T00:00:00.000Z",
        ///        "InsertDate": "2012-03-07T17:15:38.513Z",
        ///        "UpdateDate": "2012-03-07T17:15:38.513Z",
        ///        "AdminMemberID": -2147483648,
        ///        "Weight": 30,
        ///        "Active": true,
        ///        "QuestionType": 1,
        ///        "QuestionTypeID": 2,
        ///        "Answers": [],
        ///        "StockAnswers": [],
        ///        "CacheMode": 0,
        ///        "CachePriority": 5,
        ///        "CacheTTLSeconds": 300
        ///      },
        ///      {
        ///        "TotalAnswers": 3, 
        ///        "QuestionID": 837,
        ///        "SiteID": 103,
        ///        "Text": "How many online dating services are you a member of or have you been a member of?",
        ///        "StartDate": "2012-03-07T00:00:00.000Z",
        ///        "EndDate": "2020-01-01T00:00:00.000Z",
        ///        "InsertDate": "2012-03-07T17:15:38.483Z",
        ///        "UpdateDate": "2012-03-07T17:15:38.483Z",
        ///        "AdminMemberID": -2147483648,
        ///        "Weight": 30,
        ///        "Active": true,
        ///        "QuestionType": 1,
        ///        "QuestionTypeID": 2,
        ///        "Answers": [],
        ///        "StockAnswers": [],
        ///        "CacheMode": 0,
        ///        "CachePriority": 5,
        ///        "CacheTTLSeconds": 300
        ///      }
        ///    ],
        ///    "total": 11
        ///  }
        ///}
        /// </example>
        /// <param name="pageSize">from 0 to 50</param>
        /// <param name="pageNumber">1-based page number</param>
        /// <param name="sort">QuestionListSort members: recent | az</param>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        /// </responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof(List<Question>))]
        [RequireAccessTokenV2]
        [HttpGet]
        public ActionResult GetActiveQuestionListPaged(BrandRequest brandRequest, int pageNumber, int pageSize, QuestionListSort sort)
        {
            #region Validation

            if (brandRequest.Brand == null)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "brand cannot be null");
            }

            if (pageNumber < 1)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericError, "Page number is 1-indexed, must be at least 1.");
            }

            if (pageSize > 50)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidPageSize,
                    "PageSize cannot be more than 50.");
            }

            #endregion

            var questions = PremiumAccessQuestionAnswer.GetActiveQuestionListPaged(
                brandRequest.Brand.Site.SiteID,
                pageNumber,
                pageSize,
                sort);

            return new NewtonsoftJsonResult { Result = new {questions = questions.Items, total = questions.Total}, IgnoreNotImplementedException = true};
        }

        /// <summary>
        ///     For retrieving a list of question Ids given its brandId
        /// </summary>
        /// <example>
        ///Request URL - /v2/brandId/1003/premium/questionanswer/activequestionids?access_token=[ACCESS_TOKEN]
        ///Request Content Type - application/json
        /// 
        ///Response Body
        /// 
        ///{
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "QuestionIDs": [
        ///      838,
        ///      837,
        ///      827,
        ///      826,
        ///      825,
        ///      146,
        ///      145,
        ///      144,
        ///      143,
        ///      142,
        ///      141
        ///    ],
        ///    "SiteID": 103,
        ///    "CacheMode": 0,
        ///    "CachePriority": 2,
        ///    "CacheTTLSeconds": 300
        ///  }
        ///}
        /// </example>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        /// </responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof(Question))]
        [RequireAccessTokenV2]
        [HttpGet]
        public ActionResult GetActiveQuestionIds(BrandRequest brandRequest)
        {
            #region Validation

            if (brandRequest.Brand == null)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "brand cannot be null");
            }

            #endregion

            var questions = PremiumAccessQuestionAnswer.GetActiveQuestionIds(brandRequest.Brand.Site.SiteID);

            return new NewtonsoftJsonResult { Result = questions };
        }

        /// <summary>
        ///     For retrieving member questions given its brandId and optional memberId. If memberId is not specified, it returns data for the logged in user.
        /// </summary>
        /// <example>
        ///Request URL - /v2/brandId/1003/premium/questionanswer/memberquestions?memberId=1234&amp;access_token=[ACCESS_TOKEN]
        ///
        ///Request Content Type - application/json
        /// 
        ///Response Body
        /// 
        ///{
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "Questions": [
        ///      {
        ///        "QuestionID": 825,
        ///        "SiteID": 103,
        ///        "Text": "What is your favorite thing to cook on a first date?",
        ///        "StartDate": "2012-03-07T00:00:00.000Z",
        ///        "EndDate": "2020-01-01T00:00:00.000Z",
        ///        "InsertDate": "2012-03-07T17:15:38.407Z",
        ///        "UpdateDate": "2012-03-07T17:15:38.407Z",
        ///        "AdminMemberID": -2147483648,
        ///        "Weight": 30,
        ///        "Active": true,
        ///        "QuestionType": 1,
        ///        "QuestionTypeID": 1,
        ///        "Answers": [
        ///          {
        ///            "AnswerID": 201419,
        ///            "QuestionID": 825,
        ///            "MemberID": 101805229,
        ///            "AnswerValue": "Salad.",
        ///            "AnswerValuePending": "Salad.",
        ///            "AdminMemberID": -2147483647,
        ///            "AnswerType": 1,
        ///            "AnswerStatus": 1,
        ///            "InsertDate": "2015-06-10T15:29:21.050Z",
        ///            "UpdateDate": "2015-06-10T15:29:21.050Z",
        ///            "IsDirty": false,
        ///            "GenderMask": 0,
        ///            "GlobalStatusMask": 0,
        ///            "SelfSuspendedFlag": 0,
        ///            "HasApprovedPhotos": false
        ///          }
        ///        ],
        ///        "StockAnswers": [],
        ///        "CacheMode": 0,
        ///        "CachePriority": 5,
        ///        "CacheTTLSeconds": 300
        ///      }
        ///    ],
        ///    "MemberID": 101805229,
        ///    "SiteID": 103,
        ///    "CacheMode": 0,
        ///    "CachePriority": 2,
        ///    "CacheTTLSeconds": 300
        ///  }
        ///}
        /// </example>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        /// </responseAdditionalInfo>

        [ApiMethod(ResponseType = typeof(MemberQuestionList))]
        [RequireAccessTokenV2]
        [HttpGet]
        public ActionResult GetMemberQuestions(MemberRequest brandRequest)
        {
            #region Validation

            if (brandRequest.Brand == null)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "brand cannot be null");
            }

            #endregion

            var memberId = brandRequest.MemberId > 0 ? brandRequest.MemberId : GetMemberId();

            var questions = PremiumAccessQuestionAnswer.GetMemberQuestions(memberId, brandRequest.Brand.Site.SiteID);

            return new NewtonsoftJsonResult { Result = questions, IgnoreNotImplementedException = true };
        }

        /// <summary>
        ///     For retrieving a list of answers since given brandId and daysBack
        /// </summary>
        /// <example>
        ///Request URL - /v2/brandId/1003/premium/questionanswer/recentanswers?daysBack=[DAYS_BACK]&amp;pageNumber=[PAGE_NUMBER]&amp;pageSize=[PAGE_SIZE]&amp;access_token=[ACCESS_TOKEN]
        ///Request Content Type - application/json
        /// 
        ///Response Body
        ///{
        ///  "code": 200,
        ///  "status": "OK",
        ///  "Data": {
        ///    "Items": [
        ///  {
        ///    "QuestionID": 838,
        ///    "SiteID": 103,
        ///    "Text": "Before going on a date with someone you met through an online dating service, where do you do additional \"recon\"?",
        ///    "StartDate": "2012-03-07T00:00:00.000Z",
        ///    "EndDate": "2020-01-01T00:00:00.000Z",
        ///    "InsertDate": "2012-03-07T17:15:38.513Z",
        ///    "UpdateDate": "2012-03-07T17:15:38.513Z",
        ///    "AdminMemberID": -2147483648,
        ///    "Weight": 30,
        ///    "Active": true,
        ///    "QuestionType": 1,
        ///    "QuestionTypeID": 2,
        ///    "Answers": [
        ///      {
        ///        "AnswerID": 2100987,
        ///        "QuestionID": 838,
        ///        "MemberID": 101805229,
        ///        "AnswerValue": "fff2f",
        ///        "AnswerValuePending": "",
        ///        "AdminMemberID": -2147483647,
        ///        "AnswerType": 1,
        ///        "AnswerStatus": 2,
        ///        "InsertDate": "2015-06-17T09:01:09.170Z",
        ///        "UpdateDate": "2015-08-14T15:55:24.840Z",
        ///        "IsDirty": false,
        ///        "GenderMask": 9,
        ///        "GlobalStatusMask": 0,
        ///        "SelfSuspendedFlag": 0,
        ///        "HasApprovedPhotos": false
        ///      }
        ///    ],
        ///    "StockAnswers": [
        ///      {
        ///        "StockAnswerID": 247,
        ///        "QuestionID": 838,
        ///        "AdminMemberID": -2147483648,
        ///        "SortOrder": 10,
        ///        "AnswerValue": "Google",
        ///        "InsertDate": "2012-03-07T17:15:38.520Z",
        ///        "UpdateDate": "2012-03-07T17:15:38.520Z"
        ///      },
        ///      {
        ///        "StockAnswerID": 248,
        ///        "QuestionID": 838,
        ///        "AdminMemberID": -2147483648,
        ///        "SortOrder": 20,
        ///        "AnswerValue": "Facebook",
        ///        "InsertDate": "2012-03-07T17:15:38.523Z",
        ///        "UpdateDate": "2012-03-07T17:15:38.523Z"
        ///      }
        ///    ],
        ///    "CacheMode": 0,
        ///    "CachePriority": 5,
        ///    "CacheTTLSeconds": 300
        ///  }
        ///]
        ///    "SiteID": 103,
        ///    "Total": 200
        ///  }
        ///}
        /// </example>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        /// </responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof(AnswerList))]
        [RequireAccessTokenV2]
        [HttpGet]
        public ActionResult GetRecentAnswerList(BrandRequest brandRequest, int daysBack,int pageNumber, int pageSize)
        {
            #region Validation

            if (brandRequest.Brand == null)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "brand cannot be null");
            }

            if (pageNumber < 1)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericError, "Page number is 1-indexed, must be at least 1.");
            }

            if (pageSize > 50)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidPageSize,
                    "PageSize cannot be more than 50.");
            }

            #endregion

            var questions = PremiumAccessQuestionAnswer.GetRecentAnswerList(brandRequest.Brand.Site.SiteID, daysBack, pageNumber, pageSize);

            return new NewtonsoftJsonResult { Result = questions, IgnoreNotImplementedException = true };
        }
        /// <summary>
        ///     Adds an answer if it does not exist and does nothing if not
        /// </summary>
        /// <example>
        ///Request URL - PUT - /v2/brandId/1003/premium/questionanswer?access_token=[ACCESS_TOKEN]
        ///Request Content Type - application/json
        ///
        /// {questionId : [QUESTION_NUMBER], answerValue :[ANSWER_VALUE_STRING]}
        ///  
        ///Response Body
        ///{
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {}
        ///}
        /// </example>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        /// </responseAdditionalInfo>

        [ApiMethod(ResponseType = typeof(void))]
        [RequireAccessTokenV2]
        [HttpPut]
        public ActionResult AddAnswer(AddAnswerRequest request)
        {
            #region Validation

            if (request.Brand == null)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "brand cannot be null");
            }
            
            if (request.QuestionId == 0)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "question ID shall be specified");
            }

            if (string.IsNullOrEmpty(request.AnswerValue))
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "answer cannot be empty or null");
            }

            #endregion

            var memberId = GetMemberId();
            var siteId = request.Brand.Site.SiteID;

            try
            {
                PremiumAccessQuestionAnswer.AddAnswer(memberId, siteId , request.QuestionId,
                    request.AnswerValue);
            }
            // TODO: The SM function that is called does not throw. Refactor it to throw.
            // At this time many "update" operations do not throw which prevents the consumer to control it.
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error adding answer. SiteId: {0}, MemberId: {1}", siteId, memberId), ex);
            }
            return Ok();
        }


        /// <summary>
        ///     For updating the member answer by given question id 
        /// </summary>
        /// <example>
        ///Request URL - POST - /v2/brandId/{brandId}/premium/questionanswer/memberquestions/{questionId}?access_token=[ACCESS_TOKEN]
        ///Request Content Type - application/json
        /// {
        ///     AnswerValue: "SomeValue"
        /// }
        /// 
        ///Response Body
        ///{
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {}
        ///}
        /// </example>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        /// </responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireAccessTokenV2]
        [HttpPost]
        public ActionResult UpdateMemberAnswer(UpdateAnswerRequest request)
        {
            #region Validation

            var questionId = request.QuestionId;
            if (request.Brand == null)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "brand cannot be null");
            }

            if (questionId == 0)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "question ID shall be specified");
            }

            if (string.IsNullOrEmpty(request.AnswerValue))
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "answer cannot be empty or null");
            }

            #endregion

            var memberId = GetMemberId();
            var siteId = request.Brand.Site.SiteID;

            try
            {
                PremiumAccessQuestionAnswer.UpdateAnswer(memberId, siteId, questionId,
                    request.AnswerValue);
            }
            // TODO: The SM function that is called does not throw. Refactor it to throw.
            // At this time many "update" operations do not throw which prevents the consumer to control it.
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error adding answer. SiteId: {0}, MemberId: {1}", siteId, memberId), ex);
            }
            return Ok();
        }




        /// <summary>
        ///     Updates an answer, designed to only be used by administrator, used by Admin Tools
        /// </summary>
        /// <example>
        ///Request URL - PUT - /v2/brandId/1003/premium/questionanswer/admin?client_secret=[CLIENT_SECRET]&amp;applicationId=[APPLICATION_ID]
        ///Request Content Type - application/json
        /// 
        ///Example:
        /// 
        ///{
        ///  "answerid" : 201419,
        ///  "questionid" : 825,
        ///  "answervalue" : "fffef3e2",
        ///  "answervaluepending" : "fefff32",
        ///  "answerstatus" : "Approved",
        ///  "adminmemberid" :101805229 ,
        ///  "memberid":101805228, 
        ///  "answertype":"FreeText"
        ///}        
        ///  
        ///Response Body
        ///{
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {}
        ///}
        /// </example>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        /// </responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/AdminTrustedClientIPWhitelist.xml")]
        [RequireClientCredentials]
        [HttpPut]
        public ActionResult AdminUpdateAnswer(AdminUpdateAnswerRequest request)
        {
            
            #region Validation

            if (request.Brand == null)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "brand cannot be null");
            }

            if (request.AnswerID <= 0)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode. MissingArgument, "answer ID shall be specified");
            }

            if (request.QuestionID <= 0)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "question ID shall be specified");
            }

            #endregion
            
            var siteId = request.Brand.Site.SiteID;

            try
            {
                PremiumAccessQuestionAnswer.AdminUpdateAnswer(request.ToAnswer(), siteId);
            }
            // TODO: The SM function that is called does not throw. Refactor it to throw.
            // At this time many "update" operations do not throw which prevents the consumer to control it.
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error admin updating answer. SiteId: {0}", siteId), ex);
            }
            return Ok();
        }
        /// <summary>
        ///     For removing the member answer by given question id 
        /// </summary>
        /// <example>
        ///Request URL - DELETE - /v2/brandId/{brandId}/premium/questionanswer/memberquestions/{questionId}?access_token=[ACCESS_TOKEN]
        ///Request Content Type - application/json
        /// 
        ///Response Body
        ///{
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {}
        ///}
        /// </example>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        /// </responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireAccessTokenV2]
        [HttpDelete]
        public ActionResult RemoveMemberAnswer(BrandRequest brandRequest,int questionId)
        {
            #region Validation

            if (brandRequest.Brand == null)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "brand cannot be null");
            }

            if (questionId <= 0)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "question ID shall be specified");
            }

            #endregion

            PremiumAccessQuestionAnswer.RemoveMemberAnswer(questionId, brandRequest.Brand.Site.SiteID, GetMemberId());

            return Ok();
        }
    }
}