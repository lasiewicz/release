﻿#region

using System;
using System.IO;
using System.Net;
using System.Web.Mvc;
using Spark.Rest.Helpers;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Spark.Logger;

#endregion

namespace Spark.REST.Controllers
{
    public class AdminController : SparkControllerBase
    {
        private const string MatchnetServerDown = "Matchnet Server Down";
        private const string MatchnetServerEnabled = "Matchnet Server Enabled";
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(AdminController));

        private bool IsApiHealthCheckFail
        {
            get
            {
                bool isApiHealthCheckFail = true;
                try
                {
                    string healthCheckFlag = RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.HEALTH_CHECK_FAIL);

                    if (String.IsNullOrEmpty(healthCheckFlag))
                    {
                        Log.LogError("MiddleTier is not responding. Configuration Service returned null for HEALTH_CHECK_FAIL Setting", new Exception(), ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally() );
                    }
                    else
                        isApiHealthCheckFail = Boolean.Parse(healthCheckFlag);
                }
                catch (Exception ex)
                {
                    Log.LogError("Error in getting HEALTH_CHECK_FAIL Settings.", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                }
                return isApiHealthCheckFail;
            }
        }

        [AllowAnonymous]
        [HandleError]
        public ActionResult GetHealthCheck()
        {
            var serverStatus = MatchnetServerDown;

            try
            {
                var filePath = Path.GetDirectoryName(Server.MapPath("/")) + @"\deploy.txt";
                var fileExists = System.IO.File.Exists(filePath);

                if (!fileExists && !IsApiHealthCheckFail)
                {
                    serverStatus = MatchnetServerEnabled;
                }
            }
            catch
            {
                serverStatus = MatchnetServerDown;
            }
            finally
            {
                Response.AddHeader("Health", serverStatus);
//                Response.Write(serverStatus);
//                Response.End();
            }

            return Content(serverStatus);
        }

        public ActionResult GetRoot()
        {
            Response.StatusCode = (int) HttpStatusCode.OK;
            return Content("<!--" + Environment.MachineName + " --><h2>Spark API services</h2>");
        }


        public ActionResult ClearCache(CachingHelper.CacheType cacheType)
        {
            var cachingHelper = new CachingHelper();
            var result = cachingHelper.ClearCache(cacheType);

            return Content(result);
        }
    }
}