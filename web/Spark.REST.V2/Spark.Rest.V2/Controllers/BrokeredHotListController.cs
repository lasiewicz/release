﻿#region

using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Matchnet;
using Matchnet.List.ValueObjects;
using Spark.Logger;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Configuration;
using Spark.Rest.V2.Models.HotList;
using Spark.Rest.V2.Models.MingleMigration;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.DataAccess;
using Spark.REST.Entities.HotList;
using Spark.REST.Models.HotList;
using Matchnet.Content.ValueObjects.BrandConfig;

#endregion

namespace Spark.REST.Controllers
{
    public class BrokeredHotListController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(BrokeredHotListController));

        private static readonly List<HotListCategory> RelevantCategories = new List<HotListCategory>
        {
            HotListCategory.WhoViewedYourProfile,
            HotListCategory.ViewedCombined,
            HotListCategory.WhoAddedYouToTheirFavorites,
            HotListCategory.FavoritesCombined
        };


        /// <summary>
        ///     Mingle List migration: Add a member to another's hotlist using the hotlist category
        /// </summary>
        /// <example>
        /// 
        /// POST http://api.local.spark.net/v2/brandId/1003/minglemigration/hotlist/{hotListCategory}/targetmemberid/145827?comment=daComment&memberId=4564646&applicationid=1004 HTTP/1.1
        /// Content-Type: 'application/json'
        /// Accept: 'application/json'
        /// User-Agent: Fiddler
        /// Host: api.local.spark.net
        /// 
        /// HTTP/1.1 200 OK
        /// Cache-Control: private
        /// Content-Type: application/json; charset=utf-8
        /// Server: Microsoft-IIS/7.5
        /// X-AspNetMvc-Version: 4.0
        /// X-Runtime: 00:00:00.0009405
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Access-Control-Allow-Origin: *
        /// Access-Control-Allow-Headers: X-Requested-With, X-Requested-By, Content-Type, Accept, Origin
        /// Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS
        /// Date: Fri, 30 Jan 2015 23:59:07 GMT
        /// Content-Length: 36
        /// 
        /// {"code":200,"status":"OK","data":{}}
        /// 
        /// </example>
        ///  <remarks>
        ///     Hot List Categories supported for migration:
        ///     MembersYouEmailed	-7
        ///     MembersYouSentECards	-24
        ///     MembersYouTeased	-6
        ///     MembersYouIMed	-8
        ///     MembersYouViewed	-9
        ///     MembersYouBlocked	-43
        ///     MembersYouBlocked	-43
        ///     MembersClickedYes	-20
        ///     MembersClickedMaybe	-21
        ///     MembersClickedNo	-22
        ///     Default	0
        ///     SparkSent	-40
        ///     MassSmileViewed	-45
        /// </remarks>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        [Stopwatch]
        public ActionResult AddToHotlist(HotlistChangeRequest request)
        {
            if (request.Method != null && request.Method.ToLowerInvariant() == "delete")
            {
                return RemoveFromHotlist(request);
            }

            if(request.MemberId==0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedAddToHotList, "MemberId is required");

            if (request.BrandId== 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedAddToHotList, "BrandId is required");
            
            HotListCategory category;
            try
            {
                category = (HotListCategory)Enum.Parse(typeof(HotListCategory), request.HotListCategory, true);
            }
            catch (ArgumentException)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.UnknownHotListCategory, "Unknown hotlist category");
            }
            
            
            var sendStatus = HotListAccess.AddToHotlist(request.Brand, category, request.MemberId, request.TargetMemberId, request.Comment, request.Timestamp);

            Log.LogInfoMessage(string.Format("AddToHotlist: MemberId: {0}, TargetMemberId: {1}, HotListCategory: {2}, Comment: {3}, TimeStamp: {4} ", 
                request.MemberId, request.TargetMemberId, request.HotListCategory, request.Comment, request.Timestamp), null);

            if (sendStatus == ListActionStatus.Success)
            {
                addToMissingTargetList(request.Brand, category, request.MemberId, request.TargetMemberId, request.Comment, request.Timestamp);
                return Ok();
            }

            Log.LogError(string.Format("Failed AddToHotlist: MemberId: {0}, TargetMemberId: {1}, HotListCategory: {2}, Comment: {3}, TimeStamp: {4} ",
                request.MemberId, request.TargetMemberId, request.HotListCategory, request.Comment, request.Timestamp), null,null);

            return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedAddToHotList, sendStatus.ToString());
        }

        /// <summary>
        ///     Mingle List migration:Remove a member from another's hotlist
        /// </summary>
        ///  <remarks>
        ///     Hot List Categories supported for migration:
        ///     MembersYouEmailed	-7
        ///     MembersYouSentECards	-24
        ///     MembersYouTeased	-6
        ///     MembersYouIMed	-8
        ///     MembersYouViewed	-9
        ///     MembersYouBlocked	-43
        ///     MembersYouBlocked	-43
        ///     MembersClickedYes	-20
        ///     MembersClickedMaybe	-21
        ///     MembersClickedNo	-22
        ///     Default	0
        ///     SparkSent	-40
        ///     MassSmileViewed	-45
        /// </remarks>
        /// <example>
        /// REQUEST:
        /// 
        /// DELETE http://api.local.spark.net/v2/brandId/90110/minglemigration/hotlist/{hotListCategory}/targetmemberid/1127805226?MemberId=1127805223&applicationId=1054 HTTP/1.1
        /// Content-Type: 'application/json'
        /// Accept: 'application/json'
        /// User-Agent: Fiddler
        /// Host: api.local.spark.net
        /// 
        /// RESPONSE:
        /// 
        /// HTTP/1.1 200 OK
        /// Cache-Control: private
        /// Content-Type: application/json; charset=utf-8
        /// Server: Microsoft-IIS/7.5
        /// X-AspNetMvc-Version: 4.0
        /// X-Runtime: 00:00:00.5553579
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Access-Control-Allow-Origin: *
        /// Access-Control-Allow-Headers: X-Requested-With, X-Requested-By, Content-Type, Accept, Origin
        /// Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS
        /// Date: Sat, 31 Jan 2015 00:25:45 GMT
        /// Content-Length: 36
        /// 
        /// {"code":200,"status":"OK","data":{}}
        /// 
        /// </example>
        /// <param name="hotlistChangeRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        [Stopwatch]
        public ActionResult RemoveFromHotlist(HotlistChangeRequest hotlistChangeRequest)
        {
            HotListCategory category;

            if (hotlistChangeRequest.MemberId == 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedAddToHotList, "MemberId is required");

            if (hotlistChangeRequest.BrandId == 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedAddToHotList, "BrandId is required");

            if (hotlistChangeRequest.TargetMemberId == 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "TargetMemberId is required");
            try
            {
                category = (HotListCategory)Enum.Parse(typeof(HotListCategory), hotlistChangeRequest.HotListCategory, true);
            }
            catch (ArgumentException)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.UnknownHotListCategory, "Unknown hotlist category");
            }
            try
            {
                HotListAccess.RemoveFromHotlist(hotlistChangeRequest.Brand, category, hotlistChangeRequest.MemberId, hotlistChangeRequest.TargetMemberId);

                Log.LogInfoMessage(string.Format("RemoveFromHotlist: MemberId: {0}, TargetMemberId: {1}, HotListCategory: {2}, Comment: {3}, TimeStamp: {4} ",
                hotlistChangeRequest.MemberId, hotlistChangeRequest.TargetMemberId, hotlistChangeRequest.HotListCategory, hotlistChangeRequest.Comment, hotlistChangeRequest.Timestamp), null);

            }
            catch (Exception exception)
            {
                Log.LogError(string.Format("Failed RemoveFromHotlist(brand(id): {0}, category: {1}, memberId: {2}, targetMemberId: {3}", hotlistChangeRequest.Brand, category, GetMemberId(),
                    hotlistChangeRequest.TargetMemberId), exception, null);

                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedRemoveFromHotList,
                    string.Format("RemoveFromHotlist: Error removing memberid {0} from category:{1}.", hotlistChangeRequest.TargetMemberId, category));
            }

            return Ok();
        }

        /// <summary>
        ///     Mingle list migration: Saves Yes/No/Maybe data to member's hotlist and target member's hotlist.
        /// </summary>
        ///  <remarks>
        /// </remarks>
        /// <example>
        /// REQUEST:
        /// 
        /// POST http://api.local.spark.net/v2/brandId/90110/minglemigration/hotlist/ynmvote?AddMemberId=111648507&Yes=false&No=false&Maybe=true&applicationId=1054&MemberId=111651723 HTTP/1.1
        /// Content-Type: 'application/json'
        /// Accept: 'application/json'
        /// User-Agent: Fiddler
        /// Host: api.local.spark.net
        /// 
        /// RESPONSE:
        /// 
        /// HTTP/1.1 200 OK
        /// Cache-Control: private
        /// Content-Type: application/json; charset=utf-8
        /// Server: Microsoft-IIS/7.5
        /// X-AspNetMvc-Version: 4.0
        /// X-Runtime: 00:00:00.5553579
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Access-Control-Allow-Origin: *
        /// Access-Control-Allow-Headers: X-Requested-With, X-Requested-By, Content-Type, Accept, Origin
        /// Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS
        /// Date: Sat, 31 Jan 2015 00:25:45 GMT
        /// Content-Length: 36
        /// 
        /// {"code":200,"status":"OK","data":{}}
        /// 
        /// </example>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        [Stopwatch]
        public ActionResult SaveYNMVote(MingleYNMVoteRequest request)
        {
            if(request.MemberId==0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "MemberId is required");

            if (request.AddMemberId == 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "AddMemberId (targetId) is required");
            
            HotListAccess.SaveYNMVote(request, request.MemberId);
            if (request.AlertEmailSent  || request.ClickEmailSent )
            {
                HotListAccess.SaveViralStatus(request.Brand.Site.Community.CommunityID, request.MemberId, request.AddMemberId, request.AlertEmailSent, request.ClickEmailSent);
            }

            Log.LogInfoMessage(string.Format("SaveYNMVote: MemberId: {0}, Yes: {1}, No: {2}, Maybe: {3}, TimeStamp: {4}, AddMemberId: {5}, AlertEmailSent: {6}, ClickEmailSent: {7}  ",
                request.MemberId, request.Yes, request.No, request.Maybe, request.Timestamp, request.AddMemberId, request.AlertEmailSent, request.ClickEmailSent), null);

            return Ok();
        }

        /// <summary>
        ///     Mingle List migration: Add a note to member's hotlist
        /// </summary>
        /// <example>
        /// 
        /// PUT http://api.stage3.spark.net/v2/brandId/90110/minglemigration/hotlist/0/addnote?TargetMemberId=1127805226&MemberId=1127805223&comment=This%20is%20a%20test&applicationId=1054 HTTP/1.1
        /// Content-Type: 'application/json'
        /// Accept: 'application/json'
        /// User-Agent: Fiddler
        /// Host: api.local.spark.net
        /// 
        /// HTTP/1.1 200 OK
        /// Cache-Control: private
        /// Content-Type: application/json; charset=utf-8
        /// Server: Microsoft-IIS/7.5
        /// X-AspNetMvc-Version: 4.0
        /// X-Runtime: 00:00:00.0009405
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Access-Control-Allow-Origin: *
        /// Access-Control-Allow-Headers: X-Requested-With, X-Requested-By, Content-Type, Accept, Origin
        /// Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS
        /// Date: Fri, 30 Jan 2015 23:59:07 GMT
        /// Content-Length: 36
        /// 
        /// {"code":200,"status":"OK","data":{}}
        /// 
        /// </example>
        ///  <remarks>
        ///     Hot List Categories supported for migration:
        ///     MembersYouEmailed	-7
        ///     MembersYouSentECards	-24
        ///     MembersYouTeased	-6
        ///     MembersYouIMed	-8
        ///     MembersYouViewed	-9
        ///     MembersYouBlocked	-43
        ///     MembersYouBlocked	-43
        ///     MembersClickedYes	-20
        ///     MembersClickedMaybe	-21
        ///     MembersClickedNo	-22
        ///     Default	0
        ///     SparkSent	-40
        ///     MassSmileViewed	-45
        /// </remarks>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        [Stopwatch]
        public ActionResult AddHotListNote(HotlistChangeRequest request)
        {
            if (request.MemberId == 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "MemberId is required");

            if (request.TargetMemberId == 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "TargetMemberId is required");

            if(request.Comment == null || string.IsNullOrEmpty(request.Comment))
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "comment is required");

            if(request.HotListCategory == null || string.IsNullOrEmpty(request.HotListCategory))
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "HotList category is required");

            HotListCategory category;
            try
            {
                category = (HotListCategory)Enum.Parse(typeof(HotListCategory), request.HotListCategory, true);
            }
            catch (ArgumentException)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.UnknownHotListCategory, "Unknown hotlist category");
            }

            var sendStatus=HotListAccess.AddHotlistNote(request.Brand, category, request.MemberId, request.TargetMemberId, request.Comment, request.Timestamp);

            Log.LogInfoMessage(string.Format("AddHotListNote: MemberId: {0}, TargetMemberId: {1}, HotListCategory: {2}, Comment: {3}, TimeStamp: {4} ", +
                request.MemberId, request.TargetMemberId, request.HotListCategory, request.Comment, request.Timestamp), null);

            if (sendStatus == ListActionStatus.Success)
            {
                return Ok();
            }

            Log.LogError(string.Format("Failed AddHotListNote: MemberId: {0}, TargetMemberId: {1}, HotListCategory: {2}, Comment: {3}, TimeStamp: {4} ",
                request.MemberId, request.TargetMemberId, request.HotListCategory, request.Comment, request.Timestamp), null, null);

            return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedAddToHotList, sendStatus.ToString());
        }

        /// <summary>
        /// Todo: This seems to be redundant as its same as AddToHotList
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        [Stopwatch]
        public ActionResult BackfillList(HotlistChangeRequest request)
        {
            if (request.MemberId == 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "MemberId is required");

            if (request.TargetMemberId == 0)
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "TargetMemberId is required");

            if (request.Comment == null || string.IsNullOrEmpty(request.Comment))
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, "comment is required");

            HotListCategory category;
            try
            {
                category = (HotListCategory)Enum.Parse(typeof(HotListCategory), request.HotListCategory, true);
            }
            catch (ArgumentException)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.UnknownHotListCategory, "Unknown hotlist category");
            }

            var sendStatus = HotListAccess.AddToHotlist(request.Brand, category, request.MemberId, request.TargetMemberId, request.Comment, request.Timestamp);

            Log.LogInfoMessage(string.Format("BackfillList: MemberId: {0}, TargetMemberId: {1}, HotListCategory: {2}, Comment: {3}, TimeStamp: {4} ",
                request.MemberId, request.TargetMemberId, request.HotListCategory, request.Comment, request.Timestamp), null);

            if (sendStatus == ListActionStatus.Success)
            {
                addToMissingTargetList(request.Brand, category, request.MemberId, request.TargetMemberId, request.Comment, request.Timestamp);
                return Ok();
            }

            Log.LogError(string.Format("Failed BackfillList: MemberId: {0}, TargetMemberId: {1}, HotListCategory: {2}, Comment: {3}, TimeStamp: {4} ",
                request.MemberId, request.TargetMemberId, request.HotListCategory, request.Comment, request.Timestamp), null, null);

            return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedAddToHotList, sendStatus.ToString());
        }

        /// <summary>
        ///     Mingle list migration: Saves hot list data of a member in a batch request.
        /// </summary>
        ///  <remarks>
        /// </remarks>
        /// <example>
        /// REQUEST:
        /// PUT http://api.local.spark.net/v2/brandId/90110/minglemigration/hotlistbatch?applicationId=1054 HTTP/1.1
        /// {"listData": [
        ///        {  "memberId":1127799236 , "hotListCategory":0, "targetMemberId":1127805169,"actionDate":"2015-10-21T20:00:38.567Z", "comment":"Doe"}, 
        ///        {  "memberId":1127799236 , "hotListCategory":0, "targetMemberId":1127805167,"actionDate":"2015-10-21T20:00:38.567Z", "comment":"Doe"}
        ///        ]
        /// }
        /// 
        /// RESPONSE:
        /// 
        /// {"code":200,"status":"OK","data":{}}
        /// 
        /// </example>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        [Stopwatch]
        public ActionResult TransferMemberListData(MingleListMigrationRequest request)
        {
            if (request.ListData != null && request.ListData.Count > 0)
            {
                foreach (var item in request.ListData)
                {
                    try
                    {
                        var actionDate = DateTime.UtcNow;
                        DateTime.TryParse(item.ActionDate, out actionDate);
                        var sendStatus = HotListAccess.AddToHotlist(request.Brand, (HotListCategory)item.HotListCategory, item.MemberId, item.TargetMemberId, item.Comment, actionDate);
                        if (sendStatus == ListActionStatus.Success)
                        {
                            addToMissingTargetList(request.Brand, (HotListCategory)item.HotListCategory, item.MemberId, item.TargetMemberId, item.Comment, actionDate);
                            Log.LogInfoMessage(string.Format("TransferMemberListData: MemberId: {0}, TargetMemberId: {1}, HotListCategory: {2}, Comment: {3}, TimeStamp: {4} ",
                                item.MemberId, item.TargetMemberId, item.HotListCategory, item.Comment, actionDate), null);
                        }
                        else
                        {
                            Log.LogWarningMessage(string.Format("Failed TransferMemberListData: MemberId: {0}, TargetMemberId: {1}, HotListCategory: {2}, Comment: {3}, TimeStamp: {4} ", 
                                item.MemberId, item.TargetMemberId, item.HotListCategory, item.Comment, DateTime.UtcNow), null);
                        }

                    }
                    catch (Exception ex)
                    {
                        Log.LogError(string.Format("Failed TransferMemberListData: MemberId: {0}, TargetMemberId: {1}, HotListCategory: {2}, Comment: {3}, TimeStamp: {4} ",
                            item.MemberId, item.TargetMemberId, item.HotListCategory, item.Comment, DateTime.UtcNow), ex, null);
                    }
                }
            }
            else
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "List data needs to be provided and valid.");
            }

            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";

            return Content(String.Empty);
        }

        /// <summary>
        ///     Mingle list migration: Saves Yes/No/Maybe data of a member in a batch request.
        /// </summary>
        ///  <remarks>
        /// </remarks>
        /// <example>
        /// REQUEST:
        /// 
        /// PUT http://api.local.spark.net/v2/brandId/90110/minglemigration/hotlist/ynmbatch?applicationId=1054 HTTP/1.1
        /// { "ynmData":[
        ///    {  "memberId":1127799236 ,"addMemberId":1127805169, "yes":true, "no":false, "maybe":false, "timestamp":"2015-10-21T20:00:38.567Z", "alertEmailSent": true, "clickEmailSent":false }, 
        ///    {  "memberId":1127799236 ,"addMemberId":1127805167, "yes":true, "no":false, "maybe":false, "timestamp":"2015-10-21T20:00:38.567Z", "alertEmailSent": false, "clickEmailSent":true }
        ///    ]
        /// }
        /// 
        /// RESPONSE:
        /// 
        /// {"code":200,"status":"OK","data":{}}
        /// 
        /// </example>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        [Stopwatch]
        public ActionResult TransferMemberYNMData(MingleYNMMigrationRequest request)
        {
            if (request.YNMData != null && request.YNMData.Count > 0)
            {
                foreach (var item in request.YNMData)
                {
                    try
                    {
                        HotListAccess.SaveYNMVote(request.Brand, item, item.MemberId);

                        if (item.AlertEmailSent == true || item.ClickEmailSent == true)
                        {
                            HotListAccess.SaveViralStatus(request.Brand.Site.Community.CommunityID, item.MemberId, item.AddMemberId, item.AlertEmailSent, item.ClickEmailSent);
                        }

                        Log.LogInfoMessage(string.Format("SaveYNMVote: MemberId: {0}, Yes: {1}, No: {2}, Maybe: {3}, TimeStamp: {4}, AddMemberId: {5}, AlertEmailSent: {6}, ClickEmailSent: {7} ",
                            item.MemberId, item.Yes, item.No, item.Maybe, item.Timestamp, item.AddMemberId, item.AlertEmailSent, item.ClickEmailSent), null);
                    }
                    catch (Exception ex)
                    {
                        Log.LogError(string.Format("Failed SaveYNMVote: MemberId: {0}, Yes: {1}, No: {2}, Maybe: {3}, TimeStamp: {4}, AddMemberId: {5}, AlertEmailSent: {6}, ClickEmailSent: {7} ",
                            item.MemberId, item.Yes, item.No, item.Maybe, item.Timestamp, item.AddMemberId, item.AlertEmailSent, item.ClickEmailSent), ex, null);
                    }
                }
            }
            else
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "YNM data needs to be provided and valid.");
            }

            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";

            return Content(String.Empty);
        }
        
        /// <summary>
        ///     Mingle List migration: update's mail mask to member's hotlist. If both the users say MutualYes, system will trigger email and mail mask value is updated in database.
        ///     MailMask Values:
        ///     0- Default Value
        ///     2- Alert Email Sent
        ///     4- Click Email Sent
        /// </summary>
        /// <example>
        /// 
        /// PUT http://api.local.spark.net/v2/brandId/90110/minglemigration/hotlist/updatemailmask?TargetMemberId=1127805169&alertEmailSent=false&clickEmailSent=false&MemberId=1127799236&applicationId=1054 HTTP/1.1
        /// Content-Type: 'application/json'
        /// Accept: 'application/json'
        /// User-Agent: Fiddler
        /// Host: api.local.spark.net
        /// 
        /// HTTP/1.1 200 OK
        /// Cache-Control: private
        /// Content-Type: application/json; charset=utf-8
        /// Server: Microsoft-IIS/7.5
        /// X-AspNetMvc-Version: 4.0
        /// X-Runtime: 00:00:00.0009405
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Access-Control-Allow-Origin: *
        /// Access-Control-Allow-Headers: X-Requested-With, X-Requested-By, Content-Type, Accept, Origin
        /// Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS
        /// Date: Fri, 30 Jan 2015 23:59:07 GMT
        /// Content-Length: 36
        /// 
        /// {"code":200,"status":"OK","data":{}}
        /// 
        /// </example>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireClientCredentials]
        [Stopwatch]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        public ActionResult UpdateMailMask(MingleListMailMaskMigrationRequest request)
        {
            if (request.AlertEmailSent == null || request.ClickEmailSent == null)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "AlertEmailSent and ClickEmail are required.");
            }

            HotListAccess.SaveViralStatus(request.Brand.Site.Community.CommunityID, request.MemberId, request.TargetMemberId, request.AlertEmailSent, request.ClickEmailSent);

            Log.LogInfoMessage(string.Format("UpdateMailMask: MemberId: {0}, TargetMemberId: {1}, AlertEmailSent: {2}, ClickEmailSent: {3}, BrandId: {4}", request.MemberId, request.TargetMemberId, request.AlertEmailSent, request.ClickEmailSent,request.BrandId), null);
            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";

            return Content(String.Empty);
        }


        [ApiMethod(ResponseType = typeof(HotListCounts))]
        [Stopwatch]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        public ActionResult GetHotListCounts(HotListCountRequest countRequest)
        {
            var memberId = GetMemberId();

            var hotListTotalCountDictionary = HotListAccess.GetHotListTotalCounts(countRequest.Brand,
                memberId, new List<HotListCategory>()
                {
                    HotListCategory.Default,
                    HotListCategory.MembersYouTeased,
                    HotListCategory.WhoTeasedYou,
                    HotListCategory.MembersYouEmailed,
                    HotListCategory.MembersYouIMed,
                    HotListCategory.MembersYouViewed,
                    HotListCategory.MembersClickedYes,
                    HotListCategory.MembersClickedNo,
                    HotListCategory.MembersClickedMaybe,
                    HotListCategory.MutualYes
                });

            var hotListNewCountDictionary = HotListAccess.GetHotListNewCounts(countRequest.Brand, memberId, RelevantCategories);

            var hotListCounts = new HotListCounts
            {
                MyFavoritesTotal = hotListTotalCountDictionary[HotListCategory.Default],
                ViewedMyProfileTotal =
                    hotListTotalCountDictionary[HotListCategory.WhoViewedYourProfile],
                AddedMeToTheirFavoritesTotal =
                    hotListTotalCountDictionary[HotListCategory.WhoAddedYouToTheirFavorites],
                ViewedMyProfileNew =
                    hotListNewCountDictionary[HotListCategory.WhoViewedYourProfile],
                AddedMeToTheirFavoritesNew =
                    hotListNewCountDictionary[HotListCategory.WhoAddedYouToTheirFavorites],
                HasNewMail = HotListAccess.HasNewMail(countRequest.Brand, memberId),
                MembersYouEmailed = hotListTotalCountDictionary[HotListCategory.MembersYouEmailed],
                MembersYouTeased = hotListTotalCountDictionary[HotListCategory.MembersYouTeased],
                MembersYouIMed = hotListTotalCountDictionary[HotListCategory.MembersYouIMed],
                MembersYouViewed = hotListTotalCountDictionary[HotListCategory.MembersYouViewed],
                MembersClickedYes = hotListTotalCountDictionary[HotListCategory.MembersClickedYes],
                MembersClickedNo = hotListTotalCountDictionary[HotListCategory.MembersClickedNo],
                MembersClickedMaybe = hotListTotalCountDictionary[HotListCategory.MembersClickedMaybe],
                MutualYes = hotListTotalCountDictionary[HotListCategory.MutualYes],
                FavoritesCombinedTotal = hotListTotalCountDictionary[HotListCategory.WhoAddedYouToTheirFavorites],
                TeasedCombinedTotal = hotListTotalCountDictionary[HotListCategory.WhoTeasedYou],
                ViewedCombinedTotal = hotListTotalCountDictionary[HotListCategory.WhoViewedYourProfile]
            };

            return new NewtonsoftJsonResult { Result = hotListCounts };
        }

        /// <summary>
        /// //TODO: Cleanup. Fix for Hotlist category Ecard reciprocal event for backfill and migration.
        /// </summary>
        /// <param name="category"></param>
        private void addToMissingTargetList(Brand brand, HotListCategory hotListCategory, int memberId, int targetMemberId, string comment, DateTime timeStamp)
        {
            //Temp fix for Hotlist category Ecard reciprocal event for backfill
            if (hotListCategory == HotListCategory.MembersYouSentECards)
            {
                HotListAccess.AddToHotlist(brand, HotListCategory.WhoSentYouECards, targetMemberId, memberId, comment, timeStamp);
            }
        }

    }
}