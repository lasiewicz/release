#region

using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;
using Spark.Common.Localization;
using Spark.Logger;
using Spark.Managers.Managers;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.REST.DataAccess.Applications;
using Spark.REST.DataAccess.Content;
using Spark.REST.DataAccess.Session;
using Spark.Rest.DataAccess.Subscription;
using Spark.REST.Entities;
using Spark.Rest.Entities.Applications;
using Spark.Rest.Helpers;
using Spark.REST.Entities.Member;
using Spark.REST.Helpers;
using Spark.REST.Models;
using Spark.Rest.Models.Content.Attributes;
using Spark.REST.Models.Content.RegistrationActivityRecording;
using Spark.REST.Models.Member;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Configuration;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

#endregion

namespace Spark.REST.Controllers
{
    /// <summary>
    /// The controller that defines the endpoints for the member
    /// registration process.
    /// </summary>
    public class RegistrationController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(RegistrationController));

        //todo: methods marked with obsolete should be completely deprecated once verified not to be in use on prod.
        /// <summary>
        /// Gets the new member identifier.
        /// </summary>
        /// <returns></returns>
        [Obsolete("Not used by new reg")]
        [ApiMethod(ResponseType = typeof (int))]
        [RequireClientCredentials]
        [DeprecatedUse]
        public ActionResult GetNewMemberID()
        {
            var newMemberID = MemberSA.Instance.GetNewMemberID();
            return new NewtonsoftJsonResult {Result = newMemberID};
        }

        /// <summary>
        /// Gets the authentication.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [Obsolete("Not used by new reg")]
        [ApiMethod(ResponseType = typeof (Authentication))]
        [DeprecatedUse]
        public ActionResult GetAuthentication(AuthenticationRequest request)
        {
            var authResult = MemberSA.Instance.Authenticate(request.Brand, request.Email, request.Password);

            if (authResult.Status == AuthenticationStatus.Authenticated)
            {
                var auth = new Authentication
                               {
                                   Id = authResult.MemberID,
                                   UserName = authResult.UserName,
                                   StatusCode = (int) authResult.Status,
                                   StatusMessage = authResult.Status.ToString()
                               };
                return new NewtonsoftJsonResult {Result = auth};
            }
            else
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedAuthentication,
                                                     authResult.Status.ToString());
            }
        }

        /// <summary>
        ///   After a member is initially registered and has completed the reg forms, use the saved attributedata
        ///   to check for fraud.
        /// 
        ///   Also fill out any default values here.
        /// </summary>
        /// <param name = "request"></param>
        /// <returns></returns>
        [Obsolete("Not used by new reg")]
        [ApiMethod(ResponseType = typeof (CompleteRegistrationResult))]
        [RequireAccessTokenV2]
        [DeprecatedUse]
        public ActionResult CompleteRegistration(MemberRequest request)
        {
            var status = MemberHelper.CompleteRegistration(request.Brand, GetMemberId(), "");
            if ("success".Equals(status.ToLower()))
            {
                var result = new CompleteRegistrationResult
                                 {
                                     Status = status
                                 };
                return new NewtonsoftJsonResult {Result = result};
            }
            else
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedRegistration, status);
            }
        }

        ///<summary>
        ///  Deprecate CompleteRegistration once all clients are using the new one and rename it back.
        ///</summary>
        ///<param name = "request"></param>
        ///<returns></returns>
        [Obsolete("Not used by new reg")]
        [ApiMethod(ResponseType = typeof (CompleteRegistrationResult))]
        [RequireAccessTokenV2]
        [HttpPost]
        [DeprecatedUse]
        public ActionResult CompleteRegistrationWithIovation(CompleteRegistrationRequest request)
        {
            var memberId = GetMemberId();
            Log.LogDebugMessage(string.Format("Request memberId:{0}, IovationBlackBox:{1}", memberId, request.IovationBlackBox), ErrorHelper.GetCustomData());
            var status = MemberHelper.CompleteRegistration(request.Brand, memberId, request.IovationBlackBox);

            if (!"success".Equals(status.ToLower())) 
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedRegistration, status);
            var result = new CompleteRegistrationResult {Status = status};
            return new NewtonsoftJsonResult {Result = result};
        }

        /// <summary>
        /// Full registration endpoint for the API
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">OAuthTokens cannobe null.</exception>
        [ApiMethod(ResponseType = typeof (ExtendedRegisterResult))]
        [RequireClientCredentials]
        public async Task<ActionResult> RegisterExtended(ExtendedRegisterRequest request)
        {
            var ipAddressString = request.IpAddress;
            if (ipAddressString == "::1")
            {
                //on some dev machines localhost is being reported with IP6 notation, 
                //so we need to fix it here or they will be IP blocked. Which is dumb. 
                ipAddressString = "127.0.0.1";
            }

            var ipAddress = Constants.NULL_INT;
            if (ipAddressString.Length > 0)
            {
                ipAddress = BitConverter.ToInt32(IPAddress.Parse(ipAddressString).GetAddressBytes(), 0);
            }

            var mappedAttributes = AttributeHelper.MapPublicAttributesToInternalFormat(request.AttributeData,
                                                                                       request.AttributeDataMultiValue);

            MemberRegisterResult memberRegisterResult;

            if (MemberHelper.RejectForBeingOnDne(request.EmailAddress, request.Brand.Site.SiteID))
            {
                memberRegisterResult = new MemberRegisterResult(RegisterStatusType.EmailAddressBlocked, 0, request.UserName);
            }
            else
            {
                //JS-1573 Taking out the "request.IovationBlackBox,"
                memberRegisterResult = MemberSA.Instance.RegisterExtended(request.Brand, 
                                                                          request.EmailAddress,
                                                                          request.UserName,
                                                                          request.Password,
                                                                          ipAddress,
                                                                          request.RegistrationSessionID,
                                                                          mappedAttributes);
            }

            // Failed
            if (memberRegisterResult.RegisterStatus != RegisterStatusType.Success)
            {
                if (memberRegisterResult.RegisterStatus == RegisterStatusType.FailedFraud)
                {
                    //log the action in the admin log
                    AdminSA.Instance.AdminActionLogInsert(memberRegisterResult.MemberID,
                                                          Constants.GROUP_PERSONALS
                                                          , (Int32) AdminAction.AdminSuspendMember
                                                          , -1
                                                          , AdminMemberIDType.AdminProfileMemberID
                                                          , AdminActionReasonID.ROLF);

                    //kill all traces of user session
                    MembersOnlineSA.Instance.Remove(request.Brand.Site.Community.CommunityID,
                                                    memberRegisterResult.MemberID);
                }

                var failedStatusRc = MemberHelper.GetRegisterResultResourceConstant(memberRegisterResult.RegisterStatus);
                var failedStatus = failedStatusRc != string.Empty
                                       ? ResourceProvider.Instance.GetResourceValue(request.Brand.Site.SiteID,
                                                                                    request.Brand.Site.CultureInfo,
                                                                                    ResourceGroupEnum.Global,
                                                                                    failedStatusRc)
                                       : "Failure";

//                var failedResult = new ExtendedRegisterResult
//                {
//                    RegisterStatus = failedStatus
//                };
//                return new NewtonsoftJsonResult { Result = failedResult };
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedRegistration, failedStatus);
            }

            // Success
            var registerResult = new ExtendedRegisterResult
                                     {
                                         MemberId = memberRegisterResult.MemberID,
                                         RegisterStatus = memberRegisterResult.RegisterStatus.ToString(),
                                         Username = memberRegisterResult.Username
                                     };

            RegistrationActivityRecordingDataAccess.Instance.RecordRegistrationComplete(request.RegistrationSessionID,
                                                                                        memberRegisterResult.MemberID,
                                                                                        DateTime.Now);

            if (!string.IsNullOrEmpty(request.RecaptureID))
            {
                RegistrationActivityRecordingDataAccess.Instance.DeleteRegistrationRecaptureRecord(request.Brand,
                                                                                                   request.EmailAddress,
                                                                                                   request.RecaptureID);
            }

            if (request.SessionTrackingID > 0)
            {
                SessionTrackingDataAccess.Instance.UpdateSessionTrackingForRegistration(request.RegistrationSessionID,
                                                                                        request.SessionTrackingID,
                                                                                        request.Brand.Site.SiteID,
                                                                                        memberRegisterResult.MemberID);
            }

            
            
            var enforceCanadianAutoOptOut = new SettingsManager().GetSettingBool(SettingConstants.AUTO_OPT_OUT_OF_ALL_EMAIL_FOR_CANADIAN_MEMBERS, request.Brand);
            var isMemberCanadian = false;

            if(enforceCanadianAutoOptOut)
            {
                if(mappedAttributes.ContainsKey("RegionId"))
                {
                    isMemberCanadian = RegionSA.Instance.IsRegionInCountry(Convert.ToInt32(mappedAttributes["RegionId"]),
                        request.Brand.Site.LanguageID, CountryConstants.CANADA);
                }
            }

            if (!(enforceCanadianAutoOptOut && isMemberCanadian))
            {
                ExternalMailSA.Instance.SendRegistrationVerification(memberRegisterResult.MemberID, request.Brand.BrandID,
                                                                     request.Brand.Site.SiteID);
            }

            //create oauth tokens if successful. this will be returned to client for cookie creation.
            try
            {
                int applicationId = Int32.Parse(HttpContext.Items["tokenAppId"].ToString());
                var settingsManager = new SettingsManager();
                var logonOnlySite = settingsManager.GetSettingBool(SettingConstants.LOGON_ONLY_SITE, request.Brand);

                var oauthTokens = await
                    ApplicationAccess.GetOrCreateAccessTokenUsingPasswordAsync(request.Brand,
                        applicationId,
                        request.EmailAddress,
                        request.Password,
                        false,
                        ApplicationResultType.TokenPairResponseV2,
                        "long",
                        logonOnlySite) as
                        TokenPairResponseV2;

                if (oauthTokens == null)
                    throw new Exception("OAuthTokens cannot be null.");

                registerResult.AccessToken = oauthTokens.AccessToken;
                registerResult.AccessExpiresTime = oauthTokens.AccessExpiresTime;
                registerResult.ExpiresIn = oauthTokens.ExpiresIn;
                registerResult.IsPayingMember = oauthTokens.IsPayingMember;
            }
            catch (Exception e)
            {
                Log.LogError("Could not create oauth tokens for MemberId:"+memberRegisterResult.MemberID, e, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(request.Brand));
            }
            return new NewtonsoftJsonResult {Result = registerResult};
        }

        /// <summary>
        /// Registers the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [Obsolete("Not used by new reg")]
        [ApiMethod(ResponseType = typeof (RegisterResult))]
        [RequireClientCredentials]
        [DeprecatedUse]
        public ActionResult Register(RegisterRequest request)
        {
            var ipAddressString = request.IpAddress;
            var ipAddress = Constants.NULL_INT;
            if (ipAddressString.Length > 0)
            {
                ipAddress = BitConverter.ToInt32(IPAddress.Parse(ipAddressString).GetAddressBytes(), 0);
            }

            MemberRegisterResult memberRegisterResult;

            if (MemberHelper.RejectForBeingOnDne(request.EmailAddress, request.Brand.Site.SiteID))
            {
                memberRegisterResult = new MemberRegisterResult(RegisterStatusType.EmailAddressBlocked, 0,
                                                                request.UserName);
            }
            else
            {
                memberRegisterResult = MemberSA.Instance.Register(request.Brand, request.EmailAddress,
                                                                  request.UserName,
                                                                  request.Password,
                                                                  ipAddress, request.Brand.Site.SiteID);
            }

            // Failed
            if (memberRegisterResult.RegisterStatus != RegisterStatusType.Success)
            {
                var registerStatus = ResourceProvider.Instance.GetResourceValue(request.Brand.Site.SiteID,
                                                                                request.Brand.Site.CultureInfo,
                                                                                ResourceGroupEnum.Global,
                                                                                MemberHelper.
                                                                                    GetRegisterResultResourceConstant(
                                                                                        memberRegisterResult.
                                                                                            RegisterStatus));
//                var failedResult = new RegisterResult
//                                       {
//                                           RegisterStatus =
//                                               registerStatus
//                                       };
//                return new NewtonsoftJsonResult {Result = failedResult};
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedRegistration, registerStatus);
            }

            // Success
            var registerResult = new RegisterResult
                                     {
                                         MemberId = memberRegisterResult.MemberID,
                                         RegisterStatus = memberRegisterResult.RegisterStatus.ToString(),
                                         Username = memberRegisterResult.Username
                                     };

            ExternalMailSA.Instance.SendRegistrationVerification(memberRegisterResult.MemberID, request.Brand.BrandID,
                                                                 request.Brand.Site.SiteID);

            return new NewtonsoftJsonResult {Result = registerResult};
        }

        /// <summary>
        /// Records the registration start.
        /// </summary>
        /// <param name="registrationStart">The registration start.</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (RegistrationRecordingResult))]
        [RequireClientCredentials]
        public ActionResult RecordRegistrationStart(RegistrationStart registrationStart)
        {
            var result = RegistrationActivityRecordingDataAccess.Instance.RecordRegistrationStart(registrationStart);
            return new NewtonsoftJsonResult {Result = result};
        }

        /// <summary>
        /// Records the registration complete.
        /// </summary>
        /// <param name="registrationComplete">The registration complete.</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (RegistrationRecordingResult))]
        [RequireClientCredentials]
        public ActionResult RecordRegistrationComplete(RegistrationComplete registrationComplete)
        {
            var result =
                RegistrationActivityRecordingDataAccess.Instance.RecordRegistrationComplete(registrationComplete);
            return new NewtonsoftJsonResult {Result = result};
        }

        /// <summary>
        /// Records the registration complete with recapture information.
        /// </summary>
        /// <param name="registrationComplete">The registration complete.</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (RegistrationRecordingResult))]
        [RequireClientCredentials]
        public ActionResult RecordRegistrationCompleteWithRecaptureInfo(
            RegistrationCompleteWithRecaptureInfo registrationComplete)
        {
            var result =
                RegistrationActivityRecordingDataAccess.Instance.RecordRegistrationCompleteWithRecaptureInfo(
                    registrationComplete);
            return new NewtonsoftJsonResult {Result = result};
        }

        /// <summary>
        /// Records the registration step.
        /// </summary>
        /// <param name="registrationStep">The registration step.</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (RegistrationRecordingResult))]
        [RequireClientCredentials]
        public ActionResult RecordRegistrationStep(RegistrationStep registrationStep)
        {
            var result = RegistrationActivityRecordingDataAccess.Instance.RecordRegistrationStep(registrationStep);
            return new NewtonsoftJsonResult {Result = result};
        }

        /// <summary>
        /// Records the registration step with recapture information.
        /// </summary>
        /// <param name="registrationStep">The registration step.</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (RegistrationRecordingResultWithRecaptureInfo))]
        [RequireClientCredentials]
        public ActionResult RecordRegistrationStepWithRecaptureInfo(RegistrationStepWithRecaptureInfo registrationStep)
        {
            var result =
                RegistrationActivityRecordingDataAccess.Instance.RecordRegistrationStepWithRecaptureInfo(
                    registrationStep);
            return new NewtonsoftJsonResult {Result = result};
        }

        /// <summary>
        /// Gets the registration recapture information.
        /// </summary>
        /// <param name="recaptureRequest">The recapture request.</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (RegistrationRecaptureInfo))]
        [RequireClientCredentials]
        public ActionResult GetRegistrationRecaptureInfo(RegistrationRecaptureRequest recaptureRequest)
        {
            var result = RegistrationActivityRecordingDataAccess.Instance.GetRegistrationRecaptureInfo(recaptureRequest);
            return new NewtonsoftJsonResult {Result = result};
        }

        /// <summary>
        /// This check also ensures MemberAccess(sub privilege status) gets updated immediately after a purchase.
        /// There's a known delay issue. Same check is done on the full site.
        /// </summary>
        /// <param name="memberRequest">The member request.</param>
        /// <returns></returns>
        [Obsolete("Not used by new reg")]
        [ApiMethod(ResponseType = typeof (AccessInfo))]
        [RequireAccessTokenV2]
        [DeprecatedUse]
        public ActionResult GetAccessInfo(MemberRequest memberRequest)
        {
            Brand brand;
            try
            {
                brand = memberRequest.Brand;
            }
            catch (Exception ex)
            {
                Log.LogError("Error getting brand.", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidBrandId, ex.ToString());
            }
            var isPayingMember = SubscriptionStatus.MemberIsSubscriber(brand, GetMemberId());
            var accessInfo = new AccessInfo
                                 {
                                     IsPayingMember = isPayingMember
                                 };

            return new NewtonsoftJsonResult {Result = accessInfo};
        }

        /// <summary>
        ///   Checks whether a given registration attribute value already exists.
        /// </summary>
        /// <param name = "attributeValidationRequest">Attribute name and value</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (AttributeValidationResult))]
        [RequireClientCredentials]
        public ActionResult ValidateRegistrationAttribute(AttributeValidationRequest attributeValidationRequest)
        {
            var result = new AttributeValidationResult {AttributeValueExists = false};
            if (!string.IsNullOrEmpty(attributeValidationRequest.AttributeName) &&
                !string.IsNullOrEmpty(attributeValidationRequest.AttributeValue))
            {
                switch (attributeValidationRequest.AttributeName.ToLower())
                {
                    //Email Address
                    // Checks if an email address exists - used for registration. 
                    // EmailAddress is shared across BH communities.
                    // For BH sites, checks if an email address exists in any one of the BH communities(shared logon communities).
                    // For Mingles sites, checks if an email exists in the given community.
                    case "emailaddress":
                        result.AttributeValueExists = MemberSA.Instance.CheckEmailAddressExists(attributeValidationRequest.AttributeValue, 
                                                                                                attributeValidationRequest.Brand.Site.Community.CommunityID);
                        break;
                    default:
                        result.AttributeValueExists = false;
                        break;
                }
            }
            return new NewtonsoftJsonResult {Result = result};
        }
    }
}