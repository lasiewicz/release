﻿#region

using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using Spark.Logger;
using Spark.Rest.Authorization;
using Spark.Rest.Helpers;
using Spark.Rest.V2.Configuration;
using Spark.Rest.V2.DataAccess.MembersOnline;

#endregion

namespace Spark.Rest.Controllers
{
    public class DocumentationController : Controller
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(DocumentationController));

        [HttpGet]
        [SparkOutputCache(Duration = 9999)]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/DocumentationTrustedClientIPWhitelist.xml")]
        public ViewResult GetEndPoints()
        {
            var controllerRoutes = DocumentationHelper.GetControllerRoutes();

            ViewBag.ControllerNames = (from key in controllerRoutes.Keys
                                       orderby key
                                       select key);

            ViewBag.ControllerRoutes = controllerRoutes;
            ViewBag.Environment = ConfigurationManager.AppSettings["Environment"] + ".htm";
            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            ViewBag.Major = version.Major;
            ViewBag.Minor = version.Minor;
            Log.LogDebugMessage(" DocumentationController:GetEndPoints() called", ErrorHelper.GetCustomData());
            return View();
        }

        [HttpGet]
        [SparkOutputCache(Duration = 9999, VaryByParam = "controllerName;methodName")]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/DocumentationTrustedClientIPWhitelist.xml")]
        public ViewResult GetControllerMethodDocument(string controllerName, string methodName)
        {
            var controllerRoutes = (from r in DocumentationHelper.GetControllerRoutes()
                                    where r.Key.ToLower() == controllerName.ToLower()
                                    select r).First().Value;

            var routeList = (from r in controllerRoutes
                         where r.Defaults["action"].ToString().ToLower() == methodName.ToLower()
                         select r).ToList();

            var route = routeList[0];

            StringBuilder routeUrl = new StringBuilder();
            if (routeList.Count > 1)
            {
                routeUrl.AppendLine(routeList.Count.ToString() + " routes available");
                foreach (Route r in routeList)
                {
                    routeUrl.AppendLine(r.Url);
                }
            }
            else
            {
                routeUrl.Append(route.Url);
            }

            ViewBag.RequiresAccessToken = DocumentationHelper.HasAttributeType(controllerName, methodName, typeof (RequireAccessTokenV2));
            ViewBag.RequiresPayingMember = DocumentationHelper.HasAttributeType(controllerName, methodName, typeof (RequirePayingMemberV2));
            ViewBag.RequireClientCredentials = DocumentationHelper.HasAttributeType(controllerName, methodName, typeof (RequireClientCredentials));
            ViewBag.UpdatesMembersOnLine = DocumentationHelper.HasAttributeType(controllerName, methodName, typeof (MembersOnlineActionFilter));
            ViewBag.RequireTrustedClientIP = DocumentationHelper.HasAttributeType(controllerName, methodName, typeof(RequireTrustedClientIP));
            ViewBag.ControllerName = route.Defaults["controller"];
            ViewBag.MethodName = route.Defaults["action"];
            var httpMethodConstraint = route.Constraints["httpmethod"] as HttpMethodConstraint;
            if (httpMethodConstraint != null) ViewBag.HttpMethod = string.Join(",", httpMethodConstraint.AllowedMethods.ToList());
            if (null != route.Constraints["acceptVersion"])
            {
                var acceptVersionConstraint = route.Constraints["acceptVersion"] as AcceptVersionConstraint;
                if (acceptVersionConstraint != null) ViewBag.AcceptVersion = acceptVersionConstraint.AllowedVersion;
            }
            ViewBag.RouteUrl = routeUrl.ToString().Replace("\n", "<br/>").Replace("[br]", "<br/>");
            ViewBag.Comments = DocumentationHelper.GetMethodComment(controllerName, methodName);
            ViewBag.Parameters = DocumentationHelper.GetParameters(controllerName, methodName, ViewBag.Comments.Params);
            ViewBag.Response = DocumentationHelper.GetResponse(controllerName, methodName);
            return View();
        }

        [HttpGet]
        [SparkOutputCache(Duration = 9999, VaryByParam = "responseName")]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/DocumentationTrustedClientIPWhitelist.xml")]
        public ViewResult GetResponse(string responseName)
        {
            ViewBag.Response = DocumentationHelper.GetResponse(responseName);

            return View();
        }
    }
}