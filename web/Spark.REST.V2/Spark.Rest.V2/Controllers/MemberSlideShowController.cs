#region

using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Spark.Logger;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.Serialization;
using Spark.Rest.V2.DataAccess.MembersOnline;
using Spark.Rest.V2.DataAccess.MemberSlideShow;
using Spark.Rest.V2.Entities.Content.RegionData;
using Spark.Rest.V2.Entities.MemberSlideShow;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.DataAccess;
using Spark.REST.DataAccess.Content;
using Spark.REST.Models.Search;

#endregion

namespace Spark.REST.Controllers
{
    ///<summary>
    ///</summary>
    public class MemberSlideShowController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MemberSlideShowController));

        ///<summary>
        ///  Returns the member ID of the next member in the requesting member's slideshow list.
        ///  Note on location; location = zip code, regionId = regionId; regionId will take precedence
        ///</summary>
        ///<param name="includeBigData">Not used</param>
        ///<param name="request"> </param>
        ///<returns> </returns>
        [ApiMethod(ResponseType = typeof (GetMemberSlideShowResponse))]
        [HttpGet]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetMemberSlideShow(SearchPreferencesRequest request)
        {
            IMember member;
            if (request.Gender == null && request.SeekingGender == null && request.Location == null && request.RegionId <= 0 &&
                request.MaxDistance == 0
                && request.MinAge == 0 && request.MaxAge == 0)
            {
                member = MemberSlideShowAccess.Instance.GetValidMember(GetMemberId(), request.Brand);
            }
            else
            {
                // this location validation block is repeated three times. doesn't need to be refactored. 
                // only the last getmemberslideshow with batch support should be used and two other methods 
                // should be deprecated.


                // location validation.
                // location = zip code, regionId = regionId; regionId will take precedence
                if (request.RegionId > 0)
                {
                    RegionHierarchy regionHierarchy = RegionDataAccess.Instance.GetRegionHierarchy(request.RegionId, 2);
                    if (regionHierarchy == null || regionHierarchy.Country == null)
                    {
                        return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidLocation,
                                                             "RegionID data is invalid. Input was: " + request.RegionId.ToString());
                    }

                    request.Location = request.RegionId.ToString();
                }
                else if (!string.IsNullOrEmpty(request.Location))
                {
                    if (!RegionDataAccess.Instance.IsUsorCanadianZipCode(request.Location))
                    {
                        return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidLocation,
                                                             "Location data is invalid. Input was: " + request.Location);
                    }
                    var cities = RegionDataAccess.Instance.GetCitiesByZip(RegionDataAccess.RegionUs, request.Location);
                    if (cities == null || cities.Count == 0)
                        cities = RegionDataAccess.Instance.GetCitiesByZip(RegionDataAccess.RegionidCanada,
                                                                          request.Location);
                    if (cities == null || cities.Count == 0)
                    {
                        return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidLocation,
                                                             "Location data is invalid. Input was: " + request.Location);
                    }
                    request.Location = cities[0].Id.ToString(CultureInfo.InvariantCulture);
                }
                member = MemberSlideShowAccess.Instance.GetValidMember(GetMemberId(), request.Brand, request);
            }

            if (member != null)
            {
                return new NewtonsoftJsonResult
                           {
                               Result = new GetMemberSlideShowResponse
                                            {
                                                MemberId = member.MemberID,
                                                MemberFound = true
                                            }
                           };
            }
            return new NewtonsoftJsonResult
                       {
                           Result = new GetMemberSlideShowResponse
                                        {
                                            MemberFound = false
                                        }
                       };
        }

        ///<summary>
        ///  Returns the miniprofile of the next member in the requesting member's slideshow list.
        ///</summary>
        ///<example>
        /// Note: 
        /// Only MemberId and BrandId are required. All other preference parameters are optional.
        /// Note on location; location = zip code, regionId = regionId; regionId will take precedence
        /// Attributeset: The search results returned are based on the ResultSetProfile attribute set.  
        /// Attributeset: If IncludeBigData is true, it will return ResultSetProfileBigV2 which has the AboutMe essay, possibly others down the road.
        /// 
        /// Example: Get next profile in slideshow using saved match preferences
        /// By default, match preferences are used to determine pool of available members for slideshow.
        /// Otherwise you can pass in parameters to specify the preferences.
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/memberslideshow/slideshow/miniprofile?access_token=
        /// 
        /// RESPONSE
        /// 
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": {
        ///        "MiniProfile": {
        ///            "memberId": 1127803736,
        ///            "username": "1127803736",
        ///            "adminSuspendedFlag": false,
        ///            "age": 35,
        ///            "approvedPhotoCount": 3,
        ///            "blockContact": false,
        ///            "blockContactByTarget": false,
        ///            "blockSearch": false,
        ///            "blockSearchByTarget": false,
        ///            "defaultPhoto": {
        ///                "memberPhotoId": 17008609,
        ///                "fullId": 222008996,
        ///                "thumbId": 222017680,
        ///                "fullPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/08/31/12/222008996.jpg",
        ///                "thumbPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/08/31/12/222017680.jpg",
        ///                "caption": "",
        ///                "listorder": 1,
        ///                "isCaptionApproved": false,
        ///                "isPhotoApproved": true,
        ///                "IsMain": true,
        ///                "IsApprovedForMain": true
        ///            },
        ///            "gender": "Female",
        ///            "heightCm": null,
        ///            "isHighlighted": false,
        ///            "isOnline": false,
        ///            "isSpotlighted": false,
        ///            "isViewersFavorite": false,
        ///            "jDateEthnicity": null,
        ///            "jDateReligion": "Reform",
        ///            "lastLoggedIn": "2015-09-17T18:49:39.000Z",
        ///            "lastUpdated": "2015-08-31T19:25:46.120Z",
        ///            "location": "Beverly Hills, CA ",
        ///            "lookingFor": [],
        ///            "maritalStatus": "Single",
        ///            "matchRating": 100,
        ///            "passedFraudCheck": true,
        ///            "PhotoUrl": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/08/31/12/222008996.jpg",
        ///            "primaryPhoto": {
        ///                "memberPhotoId": 17008609,
        ///                "fullId": 222008996,
        ///                "thumbId": 222017680,
        ///                "fullPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/08/31/12/222008996.jpg",
        ///                "thumbPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/08/31/12/222017680.jpg",
        ///                "caption": "",
        ///                "listorder": 1,
        ///                "isCaptionApproved": false,
        ///                "isPhotoApproved": true,
        ///                "IsMain": true,
        ///                "IsApprovedForMain": true
        ///            },
        ///            "regionId": 3443817,
        ///            "registrationDate": "2015-08-31T19:18:26.830Z",
        ///            "removeFromSearch": false,
        ///            "removeFromSearchByTarget": false,
        ///            "seekingGender": "Male",
        ///            "selfSuspendedFlag": false,
        ///            "ThumbnailUrl": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/08/31/12/222017680.jpg",
        ///            "zipcode": "90210"
        ///        },
        ///        "MemberId": 1127803736,
        ///        "MemberFound": true
        ///    }
        ///}
        /// 
        /// 
        /// Example: Get next profile and include About Me essay by passing in IncludeBigData param
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/memberslideshow/slideshow/miniprofile?includebigdata=true&amp;access_token=
        /// 
        /// 
        /// RESPONSE
        /// 
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": {
        ///        "MiniProfile": {
        ///            "memberId": 1127803736,
        ///            "username": "1127803736",
        ///            "aboutme" : "My about me essay",
        ///            "adminSuspendedFlag": false,
        ///            "age": 35,
        ///  ....
        /// 
        ///</example>
        ///<param name="includeBigData"> Default is false, indicates whether result attributset will include big data like About Me essay</param>
        ///<returns> </returns>
        [ApiMethod(ResponseType = typeof (GetMemberSlideShowMiniProfileResponse))]
        [HttpGet]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetMemberSlideShowMiniProfile(SearchPreferencesRequest request)
        {
            IMember member;
            if (request.Gender == null && request.SeekingGender == null && request.Location == null && request.RegionId <= 0 &&
                request.MaxDistance == 0
                && request.MinAge == 0 && request.MaxAge == 0)
            {
                member = MemberSlideShowAccess.Instance.GetValidMember(GetMemberId(), request.Brand);
            }
            else
            {
                // location validation.
                // location = zip code, regionId = regionId; regionId will take precedence
                if (request.RegionId > 0)
                {
                    RegionHierarchy regionHierarchy = RegionDataAccess.Instance.GetRegionHierarchy(request.RegionId, 2);
                    if (regionHierarchy == null || regionHierarchy.Country == null)
                    {
                        return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidLocation,
                                                             "RegionID data is invalid. Input was: " + request.RegionId.ToString());
                    }

                    request.Location = request.RegionId.ToString();
                }
                else if (!string.IsNullOrEmpty(request.Location))
                {
                    if (!RegionDataAccess.Instance.IsUsorCanadianZipCode(request.Location))
                    {
                        return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidLocation,
                                                             "Location data is invalid. Input was: " + request.Location);
                    }
                    var cities = RegionDataAccess.Instance.GetCitiesByZip(RegionDataAccess.RegionUs, request.Location);
                    if (cities == null || cities.Count == 0)
                        cities = RegionDataAccess.Instance.GetCitiesByZip(RegionDataAccess.RegionidCanada,
                                                                          request.Location);
                    if (cities == null || cities.Count == 0)
                    {
                        return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidLocation,
                                                             "Location data is invalid. Input was: " + request.Location);
                    }
                    request.Location = cities[0].Id.ToString(CultureInfo.InvariantCulture);
                }
                member = MemberSlideShowAccess.Instance.GetValidMember(GetMemberId(), request.Brand, request);
            }

            if (member != null)
            {
                var attributeSetName = request.IncludeBigData ? "resultsetprofilebigV2" : "resultsetprofile";
                return new NewtonsoftJsonResult
                           {
                               Result = new GetMemberSlideShowMiniProfileResponse
                                            {
                                                MemberId = member.MemberID,
                                                MiniProfile =
                                                    ProfileAccess.GetAttributeSetWithPhotoAttributes(request.Brand,
                                                                                             member.MemberID,
                                                                                             GetMemberId(), attributeSetName).AttributeSet
                                            }
                           };
            }
            return new NewtonsoftJsonResult
                       {
                           Result = new GetMemberSlideShowMiniProfileResponse
                                        {
                                            MemberFound = false
                                        }
                       };
        }

        /// <summary>
        ///   Returns a batch of memberslideshow results for the given member
        /// </summary>
        /// <example>
        /// Notes:
        /// A new result set will be retrieved under these conditions. 1. Cache ttl expiration - 10 minutes. 2. Reaching the end of the slideshow. Must perform a new search using IsNewPrefs=true. 3. First time retrieval.
        /// Note on location; location = zip code, regionId = regionId; regionId will take precedence
        /// Attributeset: The search results returned are based on the ResultSetProfile attribute set.  
        /// Attributeset: If IncludeBigData is true, it will return ResultSetProfileBigV2 which has the AboutMe essay, possibly others down the road.
        /// 
        /// Example: Get 10 slideshow members using default saved match preferences
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/memberslideshow/members?batchsize=10&amp;access_token=
        /// 
        /// {
        /// "code": 200,
        /// "status": "OK",
        /// "data": {
        ///    "SlideshowMembers": [{
        ///        "MiniProfile": {
        ///            "memberId": 1127779725,
        ///            "username": "1127779725",
        ///            "adminSuspendedFlag": false,
        ///            "age": 24,
        ///            "approvedPhotoCount": 1,
        ///            "blockContact": false,
        ///            "blockContactByTarget": false,
        ///            "blockSearch": false,
        ///            "blockSearchByTarget": false,
        ///            "defaultPhoto": {
        ///                "memberPhotoId": 17011535,
        ///                "fullId": 222017755,
        ///                "thumbId": 222011862,
        ///                "fullPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/09/15/16/222017755.jpg",
        ///                "thumbPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/09/15/16/222011862.jpg",
        ///                "caption": "",
        ///                "listorder": 1,
        ///                "isCaptionApproved": false,
        ///                "isPhotoApproved": true,
        ///                "IsMain": true,
        ///                "IsApprovedForMain": true
        ///            },
        ///            "gender": "Female",
        ///            "heightCm": 183,
        ///            "isHighlighted": false,
        ///            "isOnline": false,
        ///            "isSpotlighted": false,
        ///            "isViewersFavorite": false,
        ///            "jDateEthnicity": "Ashkenazi",
        ///            "jDateReligion": "Reform",
        ///            "lastLoggedIn": "2015-09-16T22:30:23.000Z",
        ///            "lastUpdated": "2015-03-24T17:34:19.910Z",
        ///            "location": "Beverly Hills, CA ",
        ///            "lookingFor": ["A date",
        ///            "Friend"],
        ///            "maritalStatus": "Separated",
        ///            "matchRating": 91,
        ///           "passedFraudCheck": false,
        ///            "PhotoUrl": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/09/15/16/222017755.jpg",
        ///            "primaryPhoto": {
        ///                "memberPhotoId": 17011535,
        ///                "fullId": 222017755,
        ///                "thumbId": 222011862,
        ///                "fullPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/09/15/16/222017755.jpg",
        ///                "thumbPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/09/15/16/222011862.jpg",
        ///                "caption": "",
        ///                "listorder": 1,
        ///                "isCaptionApproved": false,
        ///                "isPhotoApproved": true,
        ///                "IsMain": true,
        ///                "IsApprovedForMain": true
        ///            },
        ///            "regionId": 3443821,
        ///            "registrationDate": "2014-02-18T23:46:29.067Z",
        ///            "removeFromSearch": false,
        ///            "removeFromSearchByTarget": false,
        ///            "seekingGender": "Male",
        ///            "selfSuspendedFlag": false,
        ///            "ThumbnailUrl": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/09/15/16/222011862.jpg",
        ///            "zipcode": "90211"
        ///        },
        ///        "Photos": [{
        ///            "memberPhotoId": 17011535,
        ///            "fullId": 222017755,
        ///            "thumbId": 222011862,
        ///            "fullPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/09/15/16/222017755.jpg",
        ///            "thumbPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/09/15/16/222011862.jpg",
        ///            "caption": "",
        ///            "listorder": 1,
        ///            "isCaptionApproved": false,
        ///            "isPhotoApproved": true,
        ///            "IsMain": true,
        ///            "IsApprovedForMain": true
        ///        }]
        ///    },
        ///    {
        ///        "MiniProfile": {
        ///            "memberId": 114312863,
        ///            "username": "114312863",
        ///            "adminSuspendedFlag": false,
        ///            "age": 40,
        ///    .....
        /// 
        /// 
        /// Example: Get 10 slideshow members and include About Me essay by passing in IncludeBigData param
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/memberslideshow/members?batchsize=10&amp;includebigdata=true&amp;access_token=
        /// 
        /// 
        /// RESPONSE
        /// 
        /// {
        /// "code": 200,
        /// "status": "OK",
        /// "data": {
        ///    "SlideshowMembers": [{
        ///        "MiniProfile": {
        ///            "memberId": 1127779725,
        ///            "username": "1127779725",
        ///            "aboutme" : "my awesome about me essay",
        ///            "adminSuspendedFlag": false,
        ///            "age": 24,
        ///  ....
        ///  
        /// 
        /// 
        /// </example>
        /// <param name="batchsize">The number of slideshow members to get.  Default is 1.</param>
        /// <param name="request"> Only MemberId and BrandId are required. All other preference parameters are optional.Default values. Member's save search prefs values are used. batchsize=1 </param>
        /// <param name="includeBigData"> Default is false, indicates whether result attributset will include big data like About Me essay</param>
        /// <returns> </returns>
        [ApiMethod(ResponseType = typeof (GetMemberSlideShowMembersResponse))]
        [HttpGet]
        [RequireAccessTokenV2]
        public ActionResult GetMemberSlideShowMembers(SearchPreferencesRequest request)
        {
            var responseSlideshowMembers = new List<SlideshowMember>();

            // location validation.
            // location = zip code, regionId = regionId; regionId will take precedence
            if (request.RegionId > 0)
            {
                RegionHierarchy regionHierarchy = RegionDataAccess.Instance.GetRegionHierarchy(request.RegionId, 2);
                if (regionHierarchy == null || regionHierarchy.Country == null)
                {
                    return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidLocation,
                                                         "RegionID data is invalid. Input was: " + request.RegionId.ToString());
                }

                request.Location = request.RegionId.ToString();
            }
            else if (!string.IsNullOrEmpty(request.Location))
            {
                if (!RegionDataAccess.Instance.IsUsorCanadianZipCode(request.Location))
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidLocation,
                                                         "Location data is invalid. Input was: " + request.Location);
                }
                var cities = RegionDataAccess.Instance.GetCitiesByZip(RegionDataAccess.RegionUs, request.Location);
                if (cities == null || cities.Count == 0)
                    cities = RegionDataAccess.Instance.GetCitiesByZip(RegionDataAccess.RegionidCanada, request.Location);
                if (cities == null || cities.Count == 0)
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidLocation,
                                                         "Location data is invalid. Input was: " + request.Location);
                }
                request.Location = cities[0].Id.ToString(CultureInfo.InvariantCulture);
            }
            var members = MemberSlideShowAccess.Instance.GetValidMembers(GetMemberId(), request.Brand, request);

            if (members != null)
            {
                var attributeSetName = request.IncludeBigData ? "resultsetprofilebigV2" : "resultsetprofile";
                responseSlideshowMembers = members.Select(member => new SlideshowMember
                {
                    MiniProfile = ProfileAccess.GetAttributeSetWithPhotoAttributes
                        (request.Brand, member.MemberID, GetMemberId(), attributeSetName).AttributeSet,
                    Photos = PhotoAccess.GetPhotos(request.Brand, member.MemberID, GetMemberId())
                }).ToList();
            }

            return new NewtonsoftJsonResult
                       {
                           Result = new GetMemberSlideShowMembersResponse
                                        {
                                            SlideshowMembers = responseSlideshowMembers
                                        }
                       };
        }
    }
}