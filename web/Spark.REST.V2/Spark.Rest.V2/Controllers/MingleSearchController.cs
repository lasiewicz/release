﻿using System.Web.Mvc;
using Spark.MingleSearchEngine.ValueObjects;
using Spark.REST.Controllers;
using Spark.REST.Entities.Search;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.Models.Search;
using Spark.Rest.Serialization;
using Spark.Rest.V2.DataAccess.Search;

namespace Spark.Rest.Controllers
{
    public class MingleSearchController : SparkControllerBase
    {
        /// <summary>
        /// This method provides mingle search results for members.
        /// </summary>
        /// <param name="searchRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(SearchResultsV2))]
        [HttpPost]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MingleTrustedClientIPWhitelist.xml")]
        public ActionResult GetMingleSearchResultsViaPost(MingleSearchResultsRequest searchRequest)
        {
            int communityId = searchRequest.Brand.Site.Community.CommunityID;
            SearchParameterCollection buildSearchParameterCollection = MingleSearch.Instance.BuildSearchParameterCollection(communityId,
                                                                     searchRequest.SeekingGender, searchRequest.MinAge,
                                                                     searchRequest.MaxAge, searchRequest.MinHeight,
                                                                     searchRequest.MaxHeight,
                                                                     searchRequest.RadiansLatitude,
                                                                     searchRequest.RadiansLongitude,
                                                                     searchRequest.RadiusInMiles,
                                                                     searchRequest.Ethnicity, searchRequest.BodyType,
                                                                     searchRequest.Education, searchRequest.Religion,
                                                                     searchRequest.Smoke, searchRequest.Drink,
                                                                     searchRequest.MaritalStatus, searchRequest.HasPhoto,
                                                                     searchRequest.SortType, searchRequest.ResultType,
                                                                     searchRequest.MaxResults,
                                                                     searchRequest.HasEthnicity,
                                                                     searchRequest.HasSmoke,
                                                                     searchRequest.HasDrink,
                                                                     searchRequest.HasReligion,
                                                                     searchRequest.HasMaritalStatus,
                                                                     searchRequest.HasBodyType,
                                                                     searchRequest.HasChurchActivity,
                                                                     searchRequest.HasEducation,                                                                      
                                                                     searchRequest.WantsEthnicity,
                                                                     searchRequest.WantsSmoke,
                                                                     searchRequest.WantsDrink,
                                                                     searchRequest.WantsReligion,
                                                                     searchRequest.WantsMaritalStatus,
                                                                     searchRequest.WantsBodyType,
                                                                     searchRequest.WantsChurchActivity,
                                                                     searchRequest.WantsEducation,
                                                                     searchRequest.ImportanceEthnicity,
                                                                     searchRequest.ImportanceSmoke,
                                                                     searchRequest.ImportanceDrink,
                                                                     searchRequest.ImportanceReligion,
                                                                     searchRequest.ImportanceMaritalStatus,
                                                                     searchRequest.ImportanceBodyType,
                                                                     searchRequest.ImportanceChurchActivity,
                                                                     searchRequest.ImportanceEducation);
            var searchResults = MingleSearch.Instance.Search(buildSearchParameterCollection, communityId);
            return new NewtonsoftJsonResult { Result = searchResults };
        }

        /// <summary>
        /// This method updates mingle members in mingle search results.
        /// </summary>
        /// <param name="searchRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [HttpPost]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MingleTrustedClientIPWhitelist.xml")]
        public ActionResult UpdateMingleSearchMember(MingleSearchResultsUpdateRequest searchRequest)
        {
            int communityId = searchRequest.Brand.Site.Community.CommunityID;
            MingleSearchMemberUpdate mingleSearchMemberUpdate = new MingleSearchMemberUpdate();
            mingleSearchMemberUpdate.CommunityID = communityId;
            mingleSearchMemberUpdate.UpdateMode = searchRequest.UpdateMode;
            mingleSearchMemberUpdate.UpdateReason = searchRequest.UpdateReason;
            mingleSearchMemberUpdate.MemberID = searchRequest.MemberId;

            MingleSearchMember member = MingleSearchMember.GenerateSearchMember(searchRequest.MemberId, communityId,
                                                                                searchRequest.Gender,
                                                                                searchRequest.Birthdate,
                                                                                searchRequest.JoinDate,
                                                                                searchRequest.LastLogin,
                                                                                searchRequest.PhotoFlag,
                                                                                searchRequest.Education,
                                                                                searchRequest.Religion,
                                                                                searchRequest.Language,
                                                                                searchRequest.Ethnicity,
                                                                                searchRequest.Smoke, searchRequest.Drink,
                                                                                searchRequest.Height,
                                                                                searchRequest.MaritalStatus,
                                                                                searchRequest.BodyType,
                                                                                searchRequest.LocationBHRegionID,
                                                                                searchRequest.Longitude,
                                                                                searchRequest.Latitude,
                                                                                searchRequest.LastUpdateYMDT,
                                                                                searchRequest.Weight,
                                                                                searchRequest.Children,
                                                                                searchRequest.AboutChildren,
                                                                                searchRequest.ChildrenAtHome,
                                                                                searchRequest.ChurchActivity,
                                                                                searchRequest.ColorB,
                                                                                searchRequest.ColorR,
                                                                                searchRequest.ColorW,
                                                                                searchRequest.ColorY, searchRequest.Eyes,
                                                                                searchRequest.Hair,
                                                                                searchRequest.MinistryId,
                                                                                searchRequest.PremiumServicesFlags,
                                                                                searchRequest.WantsEthnicity,
                                                                                searchRequest.WantsSmoke,
                                                                                searchRequest.WantsDrink,
                                                                                searchRequest.WantsReligion,
                                                                                searchRequest.WantsMaritalStatus,
                                                                                searchRequest.WantsBodyType,
                                                                                searchRequest.WantsChurchActivity,
                                                                                searchRequest.WantsEducation,
                                                                                searchRequest.ImportanceEthnicity,
                                                                                searchRequest.ImportanceSmoke,
                                                                                searchRequest.ImportanceDrink,
                                                                                searchRequest.ImportanceReligion,
                                                                                searchRequest.ImportanceMaritalStatus,
                                                                                searchRequest.ImportanceBodyType,
                                                                                searchRequest.ImportanceChurchActivity,
                                                                                searchRequest.ImportanceEducation);

            member.UpdateMode = searchRequest.UpdateMode;
            member.UpdateReason = searchRequest.UpdateReason;
            MingleSearch.Instance.UpdateSearch(mingleSearchMemberUpdate, member);
            return Ok();
        }



    }
}
