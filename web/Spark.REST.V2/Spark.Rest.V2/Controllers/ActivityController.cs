﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.Content.ServiceAdapters;
using Spark.ActivityRecording.Processor.ValueObjects;
using Spark.REST.Controllers;
using Spark.RabbitMQ.Client;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.V2.DataAccess;
using Spark.Rest.V2.Models.Activity;
using Spark.Rest.V2.Models.MingleMigration;

namespace Spark.Rest.Controllers
{
    public class ActivityController : SparkControllerBase
    {

        int ParseActionType(string actionTypeStr)
        {
            int actionTypeInt;
            ActionType actionTypeEnum;

            if (int.TryParse(actionTypeStr, out actionTypeInt))
                return actionTypeInt;

            if (Enum.TryParse<ActionType>(actionTypeStr, out actionTypeEnum))
                return (int) actionTypeEnum;

            return 0;
        }

        /// <summary>This will record an activity for internal logging/reporting purposes.</summary>
        /// <example>
        /// POST http://api.local.spark.net/v2/brandId/1003/activity/record?applicationid=1000&amp;client_secret=
        /// 
        /// {
        ///     "memberId": 411,
        ///     "targetMemberId":  1116690655 ,
        ///     "actionType": "2",
        ///     "caption": "some caption here"
        /// }
        /// 
        /// 
        /// RESPONSE
        /// {"code":200,"status":"OK","data":""}
        /// 
        /// </example>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="actionType" required="true"></param>
        /// <param name="brandId">Not used, specified as part of URL path</param>
        /// <param name="caption">Activity description or sometimes will be used to store XML data for additional information</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireClientCredentials]
        public ActionResult RecordActivity(int memberId, int targetMemberId, string actionType, int brandId, string caption)
        {
            var actionTypeId = ParseActionType(actionType);
            ActivityRecordingAccess.Instance.RecordActivity(memberId, targetMemberId, actionTypeId, brandId, caption);
            
            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";
            return Content(String.Empty);
        }

        /// <summary>This will record an activity for internal logging/reporting purposes.</summary>
        /// <example>
        ///     POST http://api.local.spark.net/v2/brandId/1003/activitywithparameters/record?applicationid=1000&amp;client_secret=
        ///     {
        ///     "memberId": 411,
        ///     "targetMemberId":  1116690655 ,
        ///     "actionType": "2",
        ///     "callingSystem": "11",
        ///     "ParametersJsonString": "{\"param1\":\"value1\", \"param2\":\"value2\"}"
        ///     }
        ///     RESPONSE
        ///     {"code":200,"status":"OK","data":""}
        /// </example>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="actionType" required="true"></param>
        /// <param name="callingSystem">defaults to "Web"</param>
        /// <param name="parameters">Not used</param>
        /// <param name="brandId">Not used, specified as part of URL path</param>
        /// <param name="parametersJsonString">
        ///     A json dictionary with a string for both key and value, used to pass in caption
        ///     parameters to automatically build a specific XML caption.  The parameters are treated as child xml elements within
        ///     a root element that is defined within the API per action type.  Talk to API team for specifics.
        /// </param>
        /// <param name="siteId">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireClientCredentials]
        public ActionResult RecordActivityWithParameters(ActivityWithParametersModel request)
        {
            var actionTypeId = ParseActionType(request.ActionType);
            ActivityRecordingAccess.Instance.RecordActivity(request.MemberId, request.TargetMemberId,
                actionTypeId, request.BrandId, request.Parameters, request.CallingSystem);

            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";

            return Content(String.Empty);
        }
    }
}
