#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.EmailTracker.ServiceAdapters;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.RemotingClient;
using Spark.Common.AccessService;
using Spark.Common.Localization;
using Spark.Common.MemberLevelTracking;
using Spark.Logger;
using Spark.Managers.Managers;
using Spark.Rest.Authorization;
using Spark.REST.BedrockPorts;
using Spark.Rest.Configuration;
using Spark.REST.DataAccess;
using Spark.REST.Entities.Mail;
using Spark.REST.Entities.Profile;
using Spark.Rest.Helpers;
using Spark.REST.Models;
using Spark.REST.Models.Mail;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Configuration;
using Spark.Rest.V2.DataAccess.Mail;
using Spark.Rest.V2.DataAccess.Mail.Interfaces;
using Spark.Rest.V2.DataAccess.MembersOnline;
using Spark.Rest.V2.Entities.Mail;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Models.Mail;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using MailMessage = Spark.REST.Entities.Mail.MailMessage;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;
using Spark.Rest.V2.BedrockPorts;

#endregion

namespace Spark.REST.Controllers
{
    public class MailController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof (MailController));

        private IInternalMailAccess _internalMailAccess;
        private IAllAccessAccess _allAccessAccess;

        /// <summary>
        /// </summary>
        public IAllAccessAccess AllAccessAccess
        {
            get
            {
                if (_allAccessAccess != null)
                    return _allAccessAccess;

                _allAccessAccess = new AllAccessAccess(MemberSA.Instance,
                    EmailMessageSA.Instance,
                    RuntimeSettings.Instance);

                return _allAccessAccess;
            }
            set { _allAccessAccess = value; }
        }

        /// <summary>
        /// </summary>
        public IInternalMailAccess InternalMailAccess
        {
            get
            {
                if (_internalMailAccess != null)
                    return _internalMailAccess;

                _internalMailAccess = new InternalMailAccess(EmailMessageSA.Instance,
                    ExternalMailSA.Instance,
                    MemberSA.Instance,
                    ListSA.Instance,
                    QuotaSA.Instance,
                    EmailTrackerSA.Instance,
                    ResourceProvider.Instance,
                    RuntimeSettings.Instance,
                    AllAccessAccess);

                return _internalMailAccess;
            }

            set { _internalMailAccess = value; }
        }

        /// <summary>
        /// </summary>
        /// <param name="messageRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (MailMessage))]
        [RequirePayingMemberV2]
        [MembersOnlineActionFilter]
        [DeprecatedUse]
        public ActionResult GetMailMessage(MailMessageRequest messageRequest)
        {
            var mailMessage = GetMessage(messageRequest.Brand.BrandID, GetMemberId(), messageRequest.MessageListId);
            return new NewtonsoftJsonResult {Result = mailMessage};
        }

        public static MailMessage GetMessage(int brandId, int memberId, int messageListId)
        {
            var brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            var emailMessage = EmailMessageSA.Instance.RetrieveMessage(brand.Site.Community.CommunityID, memberId, messageListId, "insertDate desc", Direction.None, true, brand);
            if (emailMessage == null)
                throw new Exception(String.Format("Message not found. BrandId:{0} MemberId:{1} MessageListId:{2}", brandId, memberId, messageListId));

            var canReadReceipt = MessageAccess.Instance.HasReadReceiptAccessPrivilege(brand, memberId);
            var openDate = emailMessage.OpenDate == DateTime.MinValue ? emailMessage.OpenDate : emailMessage.OpenDate.ToUniversalTime();
            var message = new MailMessage
            {
                Id = emailMessage.MemberMailID,
                Subject = emailMessage.Message.MessageHeader,
                Body = emailMessage.Message.MessageBody,
                InsertDate = emailMessage.InsertDate.ToUniversalTime(),
                OpenDate = canReadReceipt ? (DateTime?)openDate : null,
                Sender = ProfileAccess.GetMiniProfile(brand, emailMessage.FromMemberID, emailMessage.ToMemberID),
                Recipient = ProfileAccess.GetMiniProfile(brand, emailMessage.ToMemberID, emailMessage.FromMemberID),
                MailType = Enum.GetName(typeof (MailType), emailMessage.MailTypeID),
                MemberFolderId = emailMessage.MemberFolderID,
                IsAllAccess = MessageAccess.Instance.IsAllAccessFolder(emailMessage.MemberFolderID)
            };

            return message;
        }

        /// <summary>
        /// This gets one specific mail message
        /// </summary>
        /// <example>
        /// 
        /// Note:
        /// The message will automatically be marked as read by default, you can use the markAsRead parameter to control the behavior.
        /// The markAsRead parameter as part of the URL path was added for backwards compatibility.  It is preferable to pass it in as a querystring.  e.g. &amp;markasread=false
        /// 
        /// </example>
        /// <param name="markAsRead">Determines whether to automatically mark the message as read, default is true.  Preferably pass in as querystring e.g. &amp;markasread=false</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (MailMessageV2))]
        [RequirePayingMemberV2]
        [MembersOnlineActionFilter]
        public ActionResult GetMailMessageV2(MailMessageRequest messageRequest)
        {
            var mailMessage = GetMessageWithMarkAsRead(messageRequest.Brand.BrandID, GetMemberId(), messageRequest.MessageListId, messageRequest.MarkAsRead);

            return new NewtonsoftJsonResult {Result = mailMessage};
        }

        //Do not need this anymore, will take advantage of multiple routes going to GetMailMessageV2 action
        //[ApiMethod(ResponseType = typeof (MailMessageV2))]
        //[RequirePayingMemberV2]
        //[MembersOnlineActionFilter]
        //public ActionResult GetMessageWithMarkAsRead(MailMessageRequest messageRequest)
        //{
        //    var mailMessage = GetMessageWithMarkAsRead(messageRequest.Brand.BrandID, GetMemberId(), messageRequest.MessageListId, messageRequest.MarkAsRead);

        //    return new NewtonsoftJsonResult {Result = mailMessage};
        //}

        private static MailMessageV2 GetMessageWithMarkAsRead(int brandId, int memberId, int messageListId, bool markAsRead)
        {
            var brand = BrandConfigSA.Instance.GetBrandByID(brandId);

            var emailMessage = EmailMessageSA.Instance.RetrieveMessage(brand.Site.Community.CommunityID, memberId, messageListId, "insertDate desc", Direction.None,
                                                                        markAsRead, brand);
            if (emailMessage == null) return null;
            var canReadReceipt = MessageAccess.Instance.HasReadReceiptAccessPrivilege(brand, memberId);
            var openDate = (emailMessage.OpenDate == DateTime.MinValue) ? emailMessage.OpenDate : emailMessage.OpenDate.ToUniversalTime();
            var message = new MailMessageV2
            {
                Id = emailMessage.MemberMailID, //TODO: This should be MessageListID, changing it now may break other calls, we may need a V3 for this object.
                MessageId = emailMessage.MailID,
                Subject = emailMessage.Message.MessageHeader,
                Body = emailMessage.Message.MessageBody,
                InsertDate = emailMessage.InsertDate.ToUniversalTime(),
                OpenDate = canReadReceipt ? (DateTime?)openDate : null,
                OpenDatePst = canReadReceipt ? (DateTime?)emailMessage.OpenDate : null,
                Sender = ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, emailMessage.FromMemberID, emailMessage.ToMemberID, "resultsetprofile").AttributeSet,
                Recipient = ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, emailMessage.ToMemberID, emailMessage.FromMemberID, "resultsetprofile").AttributeSet,
                MailType = Enum.GetName(typeof (MailType), emailMessage.MailTypeID),
                MemberFolderId = emailMessage.MemberFolderID,
                IsAllAccess = MessageAccess.Instance.IsAllAccessFolder(emailMessage.MemberFolderID)
            };
            return message;
        }

        [ApiMethod(ResponseType = typeof (MailFolder))]
        [RequirePayingMemberV2]
        [MembersOnlineActionFilter]
        [DeprecatedUse]
        [Obsolete("This call is obsolete. Please use <a href='/v2/docs/mail/getmailfolderv2'>GetMailFolderV2</a> instead.")]
        public ActionResult GetMailFolder(MessageFolderRequest messageFolderRequest)
        {
            var messageFolder = new MailFolder
            {
                MailboxOwnerMemberId = GetMemberId(),
                MessageList = new List<MailMessage>(),
            };
            PopulateMessageFolder(messageFolderRequest, messageFolder);
            return new NewtonsoftJsonResult {Result = messageFolder};
        }

        /// <summary>
        ///     For retrieving messages in a mail folder
        /// 
        ///     The member must be either a paying member or have at least one All Access message in their Inbox
        /// 
        ///     Features
        /// 
        ///     - Retrieves message in a mail folder
        ///     - If the member is a non sub and has at least one read or unread All Access email, returns All Access messages.
        /// </summary>
        /// <example>
        ///     Request URL - v2/brandid/1003/mail/folder/1/pagesize/2/page/1?access_token=[ACCESS_TOKEN]
        ///     Request Headers
        ///     Content Type - application/json, version=V2.1
        ///     
        ///     Request Body
        /// 
        ///     Response Body
        ///{
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "id": 0,
        ///    "groupId": 0,
        ///    "mailboxOwnerMemberId": 116361935,
        ///    "description": null,
        ///    "messageList": [
        ///      {
        ///        "id": 575221619,
        ///        "messageId": 833904146,
        ///        "sender": {
        ///          "username": "wonster",
        ///          "approvedPhotoCount": 2,
        ///          "defaultPhoto": {
        ///            "memberPhotoId": 0,
        ///            "fullId": 222013993,
        ///            "thumbId": 222011040,
        ///            "fullPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/05/07/19/222013993.jpg",
        ///            "thumbPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/05/07/19/222011040.jpg",
        ///            "caption": "",
        ///            "listorder": 0,
        ///            "isCaptionApproved": false,
        ///            "isPhotoApproved": true,
        ///            "IsMain": false,
        ///            "IsApprovedForMain": false
        ///          },
        ///          "thumbnailUrl": "http://s3.amazonaws.com/sparkmemberphotossv3/2012/09/21/17/222001724.jpg",
        ///          "photoUrl": "http://s3.amazonaws.com/sparkmemberphotossv3/2012/09/21/17/222001831.jpg",
        ///          "maritalStatus": "Single",
        ///          "gender": "Female",
        ///          "genderSeeking": "Male",
        ///          "lookingFor": null,
        ///          "age": 35,
        ///          "location": "Beverly Hills, CA ",
        ///          "height": 137,
        ///          "lastLoggedIn": "2015-06-25T01:09:32.290Z",
        ///          "lastUpdatedProfile": "2015-05-08T02:42:24.493Z",
        ///          "isOnline": false,
        ///          "subscriptionStatus": "Premium",
        ///          "zipCode": "90210",
        ///          "registrationDate": "2004-02-26T01:43:00.000Z",
        ///          "subscriptionStatusGam": "premium",
        ///          "jDateEthnicity": "Mixed Ethnic",
        ///          "jDateReligion": "Reform",
        ///          "isHighlighted": false,
        ///          "isSpotlighted": true,
        ///          "selfSuspendedFlag": false,
        ///          "adminSuspendedFlag": false,
        ///          "blockContactByTarget": false,
        ///          "blockSearchByTarget": false,
        ///          "removeFromSearchByTarget": false,
        ///          "blockContact": false,
        ///          "blockSearch": false,
        ///          "removeFromSearch": false,
        ///          "isViewerFavorite": true,
        ///          "id": 27029711,
        ///          "matchRating": 86
        ///        },
        ///        "recipient": {
        ///          "username": "116361935",
        ///          "approvedPhotoCount": 0,
        ///          "defaultPhoto": null,
        ///          "thumbnailUrl": null,
        ///          "photoUrl": null,
        ///          "maritalStatus": "Single",
        ///          "gender": "Female",
        ///          "genderSeeking": "Male",
        ///          "lookingFor": null,
        ///          "age": 46,
        ///          "location": "Beverly Hills, CA ",
        ///          "height": 137,
        ///          "lastLoggedIn": "2015-06-25T16:05:13.000Z",
        ///          "lastUpdatedProfile": "2015-04-18T00:32:25.233Z",
        ///          "isOnline": false,
        ///          "subscriptionStatus": "",
        ///          "zipCode": "90210",
        ///          "registrationDate": "2012-08-31T16:47:12.377Z",
        ///          "subscriptionStatusGam": "never_sub",
        ///          "jDateEthnicity": "Mixed Ethnic",
        ///          "jDateReligion": "Orthodox(Baal Teshuva)",
        ///          "isHighlighted": false,
        ///          "isSpotlighted": false,
        ///          "selfSuspendedFlag": false,
        ///          "adminSuspendedFlag": false,
        ///          "blockContactByTarget": false,
        ///          "blockSearchByTarget": false,
        ///          "removeFromSearchByTarget": false,
        ///          "blockContact": false,
        ///          "blockSearch": false,
        ///          "removeFromSearch": false,
        ///          "isViewerFavorite": true,
        ///          "id": 116361935,
        ///          "matchRating": 86
        ///        },
        ///        "subject": "subject ext mail test tue1",
        ///        "mailType": "Email",
        ///        "body": null,
        ///        "insertDate": "2015-06-23T16:16:57.787Z",
        ///        "openDate": "0001-01-01T00:00:00.000Z",
        ///        "messageStatus": 14,
        ///        "memberFolderId": 1,
        ///        "isAllAccess": false
        ///      },
        ///      {
        ///        "id": 575221382,
        ///        "messageId": 833904014,
        ///        "sender": {
        ///          "username": "wonster",
        ///          "approvedPhotoCount": 2,
        ///          "defaultPhoto": {
        ///            "memberPhotoId": 0,
        ///            "fullId": 222013993,
        ///            "thumbId": 222011040,
        ///            "fullPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/05/07/19/222013993.jpg",
        ///            "thumbPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2015/05/07/19/222011040.jpg",
        ///            "caption": "",
        ///            "listorder": 0,
        ///            "isCaptionApproved": false,
        ///            "isPhotoApproved": true,
        ///            "IsMain": false,
        ///            "IsApprovedForMain": false
        ///          },
        ///          "thumbnailUrl": "http://s3.amazonaws.com/sparkmemberphotossv3/2012/09/21/17/222001724.jpg",
        ///          "photoUrl": "http://s3.amazonaws.com/sparkmemberphotossv3/2012/09/21/17/222001831.jpg",
        ///          "maritalStatus": "Single",
        ///          "gender": "Female",
        ///          "genderSeeking": "Male",
        ///          "lookingFor": null,
        ///          "age": 35,
        ///          "location": "Beverly Hills, CA ",
        ///          "height": 137,
        ///          "lastLoggedIn": "2015-06-25T01:09:32.290Z",
        ///          "lastUpdatedProfile": "2015-05-08T02:42:24.493Z",
        ///          "isOnline": false,
        ///          "subscriptionStatus": "Premium",
        ///          "zipCode": "90210",
        ///          "registrationDate": "2004-02-26T01:43:00.000Z",
        ///          "subscriptionStatusGam": "premium",
        ///          "jDateEthnicity": "Mixed Ethnic",
        ///          "jDateReligion": "Reform",
        ///          "isHighlighted": false,
        ///          "isSpotlighted": true,
        ///          "selfSuspendedFlag": false,
        ///          "adminSuspendedFlag": false,
        ///          "blockContactByTarget": false,
        ///          "blockSearchByTarget": false,
        ///          "removeFromSearchByTarget": false,
        ///          "blockContact": false,
        ///          "blockSearch": false,
        ///          "removeFromSearch": false,
        ///          "isViewerFavorite": true,
        ///          "id": 27029711,
        ///          "matchRating": 86
        ///        },
        ///        "recipient": {
        ///          "username": "116361935",
        ///          "approvedPhotoCount": 0,
        ///          "defaultPhoto": null,
        ///          "thumbnailUrl": null,
        ///          "photoUrl": null,
        ///          "maritalStatus": "Single",
        ///          "gender": "Female",
        ///          "genderSeeking": "Male",
        ///          "lookingFor": null,
        ///          "age": 46,
        ///          "location": "Beverly Hills, CA ",
        ///          "height": 137,
        ///          "lastLoggedIn": "2015-06-25T16:05:13.000Z",
        ///          "lastUpdatedProfile": "2015-04-18T00:32:25.233Z",
        ///          "isOnline": false,
        ///          "subscriptionStatus": "",
        ///          "zipCode": "90210",
        ///          "registrationDate": "2012-08-31T16:47:12.377Z",
        ///          "subscriptionStatusGam": "never_sub",
        ///          "jDateEthnicity": "Mixed Ethnic",
        ///          "jDateReligion": "Orthodox(Baal Teshuva)",
        ///          "isHighlighted": false,
        ///          "isSpotlighted": false,
        ///          "selfSuspendedFlag": false,
        ///          "adminSuspendedFlag": false,
        ///          "blockContactByTarget": false,
        ///          "blockSearchByTarget": false,
        ///          "removeFromSearchByTarget": false,
        ///          "blockContact": false,
        ///          "blockSearch": false,
        ///          "removeFromSearch": false,
        ///          "isViewerFavorite": true,
        ///          "id": 116361935,
        ///          "matchRating": 86
        ///        },
        ///        "subject": "subject ext mail test mon",
        ///        "mailType": "Email",
        ///        "body": null,
        ///        "insertDate": "2015-06-22T18:21:02.293Z",
        ///        "openDate": "0001-01-01T00:00:00.000Z",
        ///        "messageStatus": 14,
        ///        "memberFolderId": 5,
        ///        "isAllAccess": false
        ///      }
        ///    ]
        ///  }
        ///}
        /// </example>
        /// <param name="FolderId" required="true" values="Inbox = 1, Draft = 2, Sent = 3, Trash = 4, VIPInbox = 5, VIPDraft = 6, VIPSent = 7, VIPTrash = 8">Request for 1,2,3, or 4 also includes the respective VIP(All Access) foldr messages</param>
        /// <param name="PageSize" required="true" values="1+"></param>
        /// <param name="PageNumber" required="true" values="1+"></param>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        ///     Refer to the Parameters tab for the Folder enumeration values
        /// 
        ///     Error Codes
        ///     40099 - MissingArgument(or Bad Argument)
        /// </responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof (MailFolderV2))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetMailFolderV2(MessageFolderRequest request)
        {
            // all business rules are pushed into the access classes. please keep it that way.
            var result = InternalMailAccess.GetMessageFolder(new GetMessageFolderRequest(request.Brand, GetMemberId(),
                request.FolderId, request.PageSize, request.PageNumber));

            if (result.IsRetrieved)
                return new NewtonsoftJsonResult {Result = result.MailFolder};

            return result.SparkHttpStatusCodeResult;
        }

        /// <summary>
        ///     Only used by the obsolete GetMailFolder method, this has been ported to InternalMailAccess to support V2
        /// </summary>
        /// <param name="messageFolderRequest"></param>
        /// <param name="messageFolder"></param>
        private void PopulateMessageFolder(MessageFolderRequest messageFolderRequest, object messageFolder)
        {
            if (messageFolderRequest.PageNumber < 1)
            {
                throw new Exception("Page number is 1-indexed, must be at least 1.");
            }
            var startRow = messageFolderRequest.PageSize*(messageFolderRequest.PageNumber - 1) + 1;
            ICollection matchnetMessages = null;
            try
            {
                // Call the new SA method to retrive system folders merged with VIP folders.
                if (messageFolderRequest.FolderId <= 4)
                {
                    matchnetMessages = EmailMessageSA.Instance.RetrieveMessagesFromLogicalFolder(GetMemberId(), messageFolderRequest.Brand.Site.Community.CommunityID,
                                                                                                 messageFolderRequest.FolderId, startRow, messageFolderRequest.PageSize, 
                                                                                                 "InsertDate desc", messageFolderRequest.Brand);
                }
                else
                {
                    matchnetMessages = EmailMessageSA.Instance.RetrieveMessages(GetMemberId(), messageFolderRequest.Brand.Site.Community.CommunityID, 
                                                                                messageFolderRequest.FolderId, startRow, messageFolderRequest.PageSize, 
                                                                                "InsertDate desc", messageFolderRequest.Brand);
                }
            }
            catch (ArgumentException ex)
            {
                // no messages for the given pagesize/page.  Any way to check for this condition ahead of time and avoid this exception?
                Log.LogDebugMessage(string.Format("ArgumentException getting mail folder. No messages for the given pagesize/page. Error:\n{0}", ex), GetCustomDataForReportingErrors());
            }
            if (matchnetMessages == null) return;
            var canReadReceipt = MessageAccess.Instance.HasReadReceiptAccessPrivilege(messageFolderRequest.Brand, GetMemberId());
            foreach (EmailMessage matchnetMessage in matchnetMessages)
            {
                if (matchnetMessage.MessageList == null || matchnetMessage.Message == null) continue;
                var messageListItem = matchnetMessage.MessageList;
                var message = matchnetMessage.Message;
                var mailFolderV2 = messageFolder as MailFolderV2;
                if (mailFolderV2 != null)
                {
                    Dictionary<string, object> sender;
                    try
                    {
                        sender = ProfileAccess.GetAttributeSetWithPhotoAttributes(messageFolderRequest.Brand,
                            matchnetMessage.FromMemberID,
                            GetMemberId(), "resultsetprofile").AttributeSet;
                    }
                    catch (Exception ex)
                    {
                        Log.LogError(string.Format("bad member with id {0}", matchnetMessage.FromMemberID), ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(messageFolderRequest.Brand));
                        continue;
                    }

                    var openDate = (matchnetMessage.OpenDate == DateTime.MinValue) ? matchnetMessage.OpenDate : matchnetMessage.OpenDate.ToUniversalTime();
                    var mailMessage = new MailMessageV2
                    {
                        Id = messageListItem.MessageListID,
                        MessageId = matchnetMessage.MailID,
                        Sender = sender,
                        Recipient = ProfileAccess.GetAttributeSetWithPhotoAttributes(messageFolderRequest.Brand, matchnetMessage.ToMemberID,
                            GetMemberId(), "resultsetprofile").AttributeSet,
                        Body = message.MessageBody,
                        Subject = matchnetMessage.Subject,
                        InsertDate = matchnetMessage.InsertDate.ToUniversalTime(),
                        OpenDate = canReadReceipt ? (DateTime?) openDate : null,
                        OpenDatePst = canReadReceipt ? (DateTime?)matchnetMessage.OpenDate : null,
                        MessageStatus = (int) matchnetMessage.StatusMask,
                        MailType = Enum.GetName(typeof (MailType), matchnetMessage.MailTypeID),
                        MemberFolderId = matchnetMessage.MemberFolderID
                    };
                    // Incase of an ecard, get the message body to build a url to Connect.
                    if (mailMessage.MailType == "ECard")
                    {
                        var messageWithBody = GetMessage(messageFolderRequest.Brand.BrandID,
                            GetMemberId(),
                            mailMessage.Id);

                        var navigateUrl = new StringBuilder(messageWithBody.Body);

                        navigateUrl.Append(messageWithBody.Body.IndexOf('?') > 0 ? "&" : "?");

                        navigateUrl.Append("MessageID=" + mailMessage.Id);
                        navigateUrl.Append("&toUserID=" + mailMessage.Recipient["memberId"]);

                        mailMessage.Body = "http://connect.jdate.com/" + navigateUrl;
                    }

                    mailFolderV2.MessageList.Add(mailMessage);
                }
                else
                {
                    if (matchnetMessage.FromMemberID <= 0) continue;
                    MiniProfile sender;
                    try
                    {
                        sender = ProfileAccess.GetMiniProfile(messageFolderRequest.Brand, matchnetMessage.FromMemberID, matchnetMessage.ToMemberID);
                    }
                    catch (Exception ex)
                    {
                        Log.LogError(string.Format("bad member with id {0}", matchnetMessage.FromMemberID), ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(messageFolderRequest.Brand));
                        continue;
                    }
                    var openDate = (matchnetMessage.OpenDate == DateTime.MinValue) ? matchnetMessage.OpenDate : matchnetMessage.OpenDate.ToUniversalTime();
                    var mailMessage = new MailMessage
                    {
                        Id = messageListItem.MessageListID,
                        MessageId = matchnetMessage.MailID,
                        Sender = sender,
                        Recipient = ProfileAccess.GetMiniProfile(messageFolderRequest.Brand, matchnetMessage.ToMemberID, matchnetMessage.FromMemberID),
                        Body = message.MessageBody,
                        Subject = matchnetMessage.Subject,
                        InsertDate = matchnetMessage.InsertDate.ToUniversalTime(),
                        OpenDate = canReadReceipt ? (DateTime?)openDate : null,
                        MessageStatus = (int) matchnetMessage.StatusMask,
                        MailType = Enum.GetName(typeof (MailType), matchnetMessage.MailTypeID),
                        MemberFolderId = matchnetMessage.MemberFolderID
                    };
                    // Incase of an ecard, get the message body to build a url to Connect.
                    if (mailMessage.MailType == "ECard")
                    {
                        var messageWithBody = GetMessage(messageFolderRequest.Brand.BrandID, GetMemberId(), mailMessage.Id);
                        var navigateUrl = new StringBuilder(messageWithBody.Body);
                        navigateUrl.Append(messageWithBody.Body.IndexOf('?') > 0 ? "&" : "?");
                        navigateUrl.Append("MessageID=" + mailMessage.Id);
                        navigateUrl.Append("&toUserID=" + mailMessage.Recipient.Id);
                        mailMessage.Body = "http://connect.jdate.com/" + navigateUrl;
                    }

                    ((MailFolder) messageFolder).MessageList.Add(mailMessage);
                }
            }
        }

        /// <summary>
        ///     For sending or replying to an internal mail message
        ///     
        ///     The sender must be either a paying member or qualify for All Access Free to Reply
        /// 
        ///     Features
        /// 
        ///     - Sending an internal Email
        ///     - Replying to an internal Email
        ///     - Saving a message as Draft
        ///     - Updating an existing Draft
        ///     - Sending a draft as an internal Email
        ///     - Sending a blocked email if the recipient if blocking the sender
        ///     - All Access if the member has the privilege
        ///     - All Access Free to Reply for the first All Access messsage
        ///     - Fraud Hold if the send is a First Time Subscriber
        ///     - Adds to Emailed Hot List and sends External Mail notification with exceptions
        /// </summary>
        /// <example>
        ///     
        /// Request URL - /v2/brandId/1003/mail/message?access_token=[ACCESS_TOKEN]
        /// Request Content Type - application/json
        /// 
        /// Example: Sending a new message
        /// 
        ///     Request Body
        ///     {
        ///    "RecipientMemberId": 116361935,
        ///    "Subject": "subject ext mail test tue1",
        ///    "Body": " body",
        ///    "MailType": "Email",
        ///    "IsDraft": false,
        ///    "IsAllAccess": false,
        ///    "OriginalMessageRepliedToId":0
        ///     }
        /// 
        ///     Response Body
        ///     {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data":
        ///      {
        ///        "success": true,
        ///        "messageId": 628419888,
        ///        "failReason": "",
        ///        "teasesLeft": 0
        ///      }
        ///     }
        ///     
        /// 
        /// Example: Replying to a message
        /// 
        ///     Request Body
        ///     {
        ///    "RecipientMemberId": 116361935,
        ///    "Subject": "subject ext mail test tue1",
        ///    "Body": " body",
        ///    "MailType": "Email",
        ///    "IsDraft": false,
        ///    "IsAllAccess": false,
        ///    "OriginalMessageRepliedToId":628419894
        ///     }
        /// 
        ///     Response Body
        ///     {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data":
        ///      {
        ///        "success": true,
        ///        "messageId": 628419897,
        ///        "failReason": "",
        ///        "teasesLeft": 0
        ///      }
        ///     }
        ///     
        /// Example: Saving a draft
        /// 
        ///     Request Body
        ///     {
        ///    "RecipientMemberId": 116361935,
        ///    "Subject": "subject ext mail test tue1",
        ///    "Body": " body",
        ///    "MailType": "Email",
        ///    "IsDraft": true
        ///     }
        /// 
        ///     Response Body
        ///     {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data":
        ///      {
        ///        "success": true,
        ///        "messageId": 628419889,
        ///        "failReason": "",
        ///        "teasesLeft": 0
        ///      }
        ///     }
        ///     
        /// 
        /// Example: Updating a draft
        /// 
        ///     Request Body
        ///     {
        ///    "RecipientMemberId": 116361935,
        ///    "Subject": "subject ext mail test tue1",
        ///    "Body": " body",
        ///    "MailType": "Email",
        ///    "IsDraft": true,
        ///    "DraftMessageId": 628419889
        ///     }
        /// 
        ///     Response Body
        ///     {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data":
        ///      {
        ///        "success": true,
        ///        "messageId": 628419889,
        ///        "failReason": "",
        ///        "teasesLeft": 0
        ///      }
        ///     }
        ///     
        /// Example: Sending a draft
        /// 
        ///     Request Body
        ///     {
        ///    "RecipientMemberId": 116361935,
        ///    "Subject": "subject ext mail test tue1",
        ///    "Body": " body",
        ///    "MailType": "Email",
        ///    "IsDraft": false,
        ///    "DraftMessageId": 628419889
        ///     }
        /// 
        ///     Response Body
        ///     {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data":
        ///      {
        ///        "success": true,
        ///        "messageId": 628419889,
        ///        "failReason": "",
        ///        "teasesLeft": 0
        ///      }
        ///     }
        /// </example>
        /// <param name="RecipientMemberId" required="true">Recipient's Member Id</param>
        /// <param name="Subject" required="true">Subject of the message. An empty(string) subject is acceptable</param>
        /// <param name="Body" required="true">Body of the message</param>
        /// <param name="MailType" required="true"
        ///     values="None,Email,Tease, MissedIM,SystemAnnouncement,DeclineEmail,IgnoreEmail,YesEmail,PrivatePhotoEmail,ECard,QandAComment, FreeTextComment,PhotoComment,InstantMessage,NoPhotoRequest">
        ///     Type
        ///     of the mail message
        /// </param>
        /// <param name="OriginalMessageRepliedToId">If the message is a reply, Id of the meesage being replied to, RecipientMessageListID</param>
        /// <param name="IsDraft"></param>
        /// <param name="IsAllAccess">Indicate if the message is an All Access message</param>
        /// <param name="DraftMessageId">Optional, this is the message Id of existing drafts and should be passed in when updating a draft (IsDraft is set to true) or sending a draft (IsDraft is set to false)</param>
        /// <paramsAdditionalInfo>
        /// The requesting member must be either a paying member or eligible for All Access Free to Reply.
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        ///     The MessageId field returns the SenderMessageListID instead of the RecipientMessageListID. The RecipientMessageListID is used for replying to a message but this value would need to be retrieved with another GET call.
        /// 
        ///     Error Codes
        ///     40018 - FailedSendMailMessage
        ///     40018 - ComposeLimitReached
        ///     40062 - BlockedSenderIdForSendMail
        ///     400126 - NoAllAccess
        ///     40099 - MissingArgument(or Bad Argument)
        /// </responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof (PostMessageResult))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult SendMailMessage(PostMessageRequest request)
        {
            // all business rules are pushed into the access classes. please keep it that way.
            var result = InternalMailAccess.SendMailMessage(new SendMailMessageRequest(
                request.Brand,
                GetMemberId(),
                request.RecipientMemberId,
                request.OriginalMessageRepliedToId,
                request.IsAllAccess,
                request.IsDraft,
                request.MailType,
                request.Subject,
                request.Body,
                request.DraftMessageId));

            if (result.IsSent)
            {
                return new NewtonsoftJsonResult {Result = result.PostMessageResult};
            }

            return result.SparkHttpStatusCodeResult;
        }

        [ApiMethod(ResponseType = typeof (PostMessageResult))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult SendTease(PostTeaseRequest teaseRequest)
        {
            int messageListId;
            int teasesLeft;
            var sendStatus = TeaseWrapper.SendTease(teaseRequest.Brand, GetMemberId(), teaseRequest.RecipientMemberId, teaseRequest.TeaseId, teaseRequest.TeaseCategoryId, 
                                                    teaseRequest.Body, out messageListId, out teasesLeft);

            PostMessageResult postResult;
            switch (sendStatus)
            {
                case ListActionStatus.Success:
                    postResult = new PostMessageResult
                    {
                        MessageId = messageListId,
                        Success = true,
                        FailureReason = null,
                        TeasesLeft = teasesLeft
                    };
                    break;
                case ListActionStatus.HasAlreadyTeasedMember:
                    postResult = new PostMessageResult
                    {
                        MessageId = 0,
                        Success = false,
                        FailureReason = ListActionStatus.HasAlreadyTeasedMember.ToString(),
                        TeasesLeft = 0
                    };
                    break;
                default:
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedSendTease, sendStatus.ToString());
            }

            return new NewtonsoftJsonResult {Result = postResult};
        }

        [ApiMethod(ResponseType = typeof (PostMessageResult))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult DeleteMailMessage(DeleteMessageRequest deleteMessageRequest)
        {
            var memberId = GetMemberId();
            var brand = deleteMessageRequest.Brand;
            var mailService = MessageAccess.Instance;

            var result = mailService.DeleteMailMessage(
                new DeleteMessage
                {
                    MessageId = deleteMessageRequest.MessageId,
                    CurrentFolderId = deleteMessageRequest.CurrentFolderId
                }, memberId, brand);

            return new NewtonsoftJsonResult {Result = result};
        }

        /// <summary>
        ///     Delete a list of mail messages.
        /// </summary>
        /// <example>
        ///     
        ///     Request URL - http://api.stage3.spark.net/v2/brandId/1003/mail/messages?access_token=[ACCESS_TOKEN]
        ///     Request Header - Accept: application/json
        ///     Request Header - Content-Type: application/json 
        ///     Request Http Verb - Use POST. DELETE is only supported for backwards compatibility.
        ///     Request Body
        ///{
        ///    "MessagesToDelete": [
        ///        {
        ///            "MessageId": 575208716,
        ///            "CurrentFolderId": 5
        ///        },
        ///        {
        ///            "MessageId": 628409274,
        ///            "CurrentFolderId": 5
        ///        }
        ///    ]
        ///}
        /// 
        ///     Response
        /// 
        ///{
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": [
        ///        {
        ///            "success": "true",
        ///            "messageId": 575208716,
        ///            "failReason": "",
        ///            "teasesLeft": 0
        ///        },
        ///        {
        ///            "success": true,
        ///            "messageId": 628409274,
        ///            "failReason": "",
        ///            "teasesLeft": 0
        ///        }
        ///    ]
        ///}
        /// </example>
        /// <param name="deleteMessagesRequest">The request to delete the messages.</param>
        /// <returns>The transaction results of the messages deleted.</returns>
        [ApiMethod(ResponseType = typeof (IEnumerable<PostMessageResult>))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult DeleteMailMessages(DeleteMessagesRequest deleteMessagesRequest)
        {
            var memberId = GetMemberId();
            var brand = deleteMessagesRequest.Brand;
            var mailService = MessageAccess.Instance;
            var result = deleteMessagesRequest.MessagesToDelete.Select(msg => mailService.DeleteMailMessage(msg, memberId, brand));

            return new NewtonsoftJsonResult {Result = result};
        }

        [ApiMethod(ResponseType = typeof (MessageAttachmentUploadResponse))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [ResponseContentType(ContentTypeToSet = "text/plain")]
        public ActionResult PostMessageAttachments(MessageAttachmentUploadRequest messageAttachmentUploadRequest, IList<HttpPostedFileBase> photoFiles)
        {
            if (!string.IsNullOrEmpty(messageAttachmentUploadRequest.RedirectUrl))
            {
                ControllerContext.RequestContext.HttpContext.Items["redirectUrl"] = messageAttachmentUploadRequest.RedirectUrl;
            }

            var photoCount = (null != photoFiles) ? photoFiles.Count : 0;
            if (photoCount <= 0)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingPhotoUploadData, "No files found");
            }

            var memberId = GetMemberId();
            var toMemberId = messageAttachmentUploadRequest.ToMemberId;
            const int messageId = Constants.NULL_INT;
            var attachmentType = AttachmentType.None;
            try
            {
                attachmentType = (AttachmentType) Enum.Parse(typeof (AttachmentType), messageAttachmentUploadRequest.AttachmentType.ToString());
            }
            catch (Exception e)
            {
                Log.LogError(string.Format("Could not parse attachment type:{0}", messageAttachmentUploadRequest.AttachmentType), e, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(messageAttachmentUploadRequest.Brand));
                attachmentType = AttachmentType.None;
            }

            var app = Application.FullSite;
            try
            {
                app = (Application) Enum.Parse(typeof (Application), messageAttachmentUploadRequest.AppCode.ToString());
            }
            catch (Exception e)
            {
                Log.LogError(string.Format("Could not parse app code:{0}", messageAttachmentUploadRequest.AppCode), e, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(messageAttachmentUploadRequest.Brand));
                app = Application.FullSite;
            }

            var communityId = messageAttachmentUploadRequest.Brand.Site.Community.CommunityID;
            var siteId = messageAttachmentUploadRequest.Brand.Site.SiteID;

//            var existingCount = PhotoAccess.GetPhotoCount(photoCaptionRequest.Brand, memberId);
//            if (photoFiles.Count + existingCount > 12)
//            {
//                return new SparkHttpStatusCodeResult((int)HttpSub400StatusCode.MaxNumberOfPhotosExceeded,
//                                                     "Limit of 12 photos");
//            }
            MessageAttachmentUploadResponse[] uploadResponses = null;
            try
            {
                uploadResponses = MessageAccess.Instance.MessageAttachmentUploadResponses(photoFiles, memberId, toMemberId, communityId, siteId, app, attachmentType, messageId);
            }
            catch (Exception e)
            {
                Log.LogError(string.Format("PostMessageAttachments error occured for brandId:{0}, fromMemberId:{1}, toMemberId:{2}, messageId:{3}", messageAttachmentUploadRequest.BrandId, memberId, toMemberId, messageId), e, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(messageAttachmentUploadRequest.Brand));
            }

            if (Log.IsInfoEnabled)
            {
                Log.LogInfoMessage(
                    string.Format("Processed {0} photos for fromMember:{1}, toMember:{2}, messageId:{3}, brandId:{4}",
                        photoCount, memberId, toMemberId, messageId, messageAttachmentUploadRequest.BrandId),
                    GetCustomDataForReportingErrors());
            }

            return new NewtonsoftJsonResult
            {
                Result = uploadResponses
            };
        }


        [ApiMethod(ResponseType = typeof (MessageAttachmentUploadResponse))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult PostMessageAttachmentJson(MessageAttachmentUploadRequest messageAttachmentUploadRequest)
        {
            if (String.IsNullOrEmpty(messageAttachmentUploadRequest.PhotoFile))
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingPhotoUploadData, "No file to upload");
            }

            var memberId = GetMemberId();
            var toMemberId = messageAttachmentUploadRequest.ToMemberId;
            const int messageId = Constants.NULL_INT;
            var attachmentType = AttachmentType.None;
            try
            {
                attachmentType = (AttachmentType) Enum.Parse(typeof (AttachmentType), messageAttachmentUploadRequest.AttachmentType.ToString());
            }
            catch (Exception e)
            {
                Log.LogError(string.Format("Could not parse attachment type:{0}", messageAttachmentUploadRequest.AttachmentType), e, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(messageAttachmentUploadRequest.Brand));
                attachmentType = AttachmentType.None;
            }

            Application app;
            try
            {
                app = (Application) Enum.Parse(typeof (Application), messageAttachmentUploadRequest.AppCode.ToString());
            }
            catch (Exception e)
            {
                Log.LogError(string.Format("Could not parse app code:{0}", messageAttachmentUploadRequest.AppCode), e, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(messageAttachmentUploadRequest.Brand));
                app = Application.FullSite;
            }

            var fileNameAndExtension = MessageAccess.Instance.GetFileNameAndExtension(messageAttachmentUploadRequest.FileName);

            using (var stream = new MemoryStream())
            {
                var buffer = Convert.FromBase64String(messageAttachmentUploadRequest.PhotoFile);
                stream.Write(buffer, 0, buffer.Length);
                if (stream.Length == 0)
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingPhotoUploadData,
                        "No file to upload");
                }

                var photoRules = new PhotoRulesManager();

                if (!photoRules.IsFileSizeAllowed((int) stream.Length))
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MaxPhotoSizeExceeded,
                        string.Format("Maximum file size exceeded {0}",
                            HttpUtility.HtmlEncode(
                                messageAttachmentUploadRequest.PhotoFile)));
                }

                if (!photoRules.IsValidImage(stream, (int) stream.Length))
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.UnsupportedPhotoFormat,
                        string.Format("The file {0} is not a valid image", HttpUtility.HtmlEncode(messageAttachmentUploadRequest.PhotoFile)));
                }


                MessageAttachmentSaveResult messageAttachmentSaveResult = null;
                try
                {
                    messageAttachmentSaveResult = EmailMessageSA.Instance.SaveMessageAttachment(memberId,
                        toMemberId,
                        stream.GetBuffer(),
                        fileNameAndExtension[1],
                        attachmentType,
                        messageId,
                        messageAttachmentUploadRequest.Brand.Site.Community.CommunityID,
                        messageAttachmentUploadRequest.Brand.Site.SiteID,
                        app);
                }
                catch (Exception ex)
                {
                    Log.LogError(string.Format("Exception saving file: {0} for fromMemberId: {1} to toMemberId:{2}", messageAttachmentUploadRequest.FileName, memberId, toMemberId), ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(messageAttachmentUploadRequest.Brand));
                }

                if (null == messageAttachmentSaveResult)
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedUploadPhoto, "Unknown server error saving photos");
                }

                if (messageAttachmentSaveResult.Status != MessageAttachmentSaveStatus.Success)
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedUploadPhoto, messageAttachmentSaveResult.Error.ToString());
                }

                if (Log.IsInfoEnabled)
                {
                    Log.LogInfoMessage(
                        string.Format("Processed photos for fromMember:{0}, toMember:{1}, messageId:{2}, brandId:{3}",
                            memberId, toMemberId, messageId, messageAttachmentUploadRequest.BrandId),
                        GetCustomDataForReportingErrors());
                }

                return new NewtonsoftJsonResult
                {
                    Result = new MessageAttachmentUploadResponse
                    {
                        SuccessCode = (int) messageAttachmentSaveResult.Status,
                        FailureReason = messageAttachmentSaveResult.Error.ToString(),
                        MessageAttachmentId = messageAttachmentSaveResult.MessageAttachmentID,
                        PreviewUrl = messageAttachmentSaveResult.PreviewCloudPath,
                        FileName = fileNameAndExtension[0],
                        OriginalExtension = fileNameAndExtension[1]
                    }
                };
            }
        }

        /// <summary>This call returns unread message counts for all messages for a logged in member.</summary>
        /// <param name="request">BrandRequest</param>
        /// <example>
        ///     <code>GET http://api.local.spark.net/v2/brandId/1003/mail/unreadcount?access_token=ACCESS_TOKEN
        ///  </code>
        /// </example>
        /// <remarks>
        ///     example return response
        ///     <code>
        ///  {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data":    {
        ///       "Sent":       {
        ///          "Email": 16,
        ///          "Tease": 3,
        ///          "MissedIM": 4
        ///       },
        ///       "Inbox":       {
        ///       "IgnoreEmail": 2,
        ///          "Email": 9,
        ///       "QandAComment": 2,
        ///       "PhotoComment": 2,
        ///          "MissedIM": 5,
        ///          "Tease": 3,
        ///       "FreeTextComment": 2
        ///       },
        ///       "VIPDraft": {"Email": 3},
        ///       "VIPSent": {"Email": 1}
        ///    }
        /// }
        ///  </code>
        /// </remarks>
        [ApiMethod(ResponseType = typeof (Dictionary<string, Dictionary<MailType, int>>))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetUnreadMessageCountForType(BrandRequest request)
        {
            var memberId = GetMemberId();
            try
            {
                var unreadMessageCount = MessageAccess.Instance.GetUnreadMessageCount(memberId, request.Brand);
                return new NewtonsoftJsonResult {Result = unreadMessageCount};
            }
            catch (SparkAPIReportableException reportableException)
            {
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkHttpBadRequestResult((int) reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkGenericHttpInternalServerErrorResult();
            }
        }

        /// <summary>
        /// This endpoint is used to get back a list of conversation (inbox, draft, and chat) messages between 2 members.
        /// </summary>
        /// <example>
        /// 
        /// Notes:
        /// This returns a list of messages based on the page number and size passed in.
        /// 
        /// MatchesFound gives you the total number of messages that exists between the 2 members, not how many was returned, so you can use this for pagination
        /// MessageList contains the actual list of messages
        /// The message type will specify what type of message it is(for full list of message types, visit /mail/sendmailmessage endpoint)
        /// The InstantMessage messages will include a "threadID" that can be used to get IM chat history for that particular conversation
        /// The sender and recipient properties of the messages will always be null, it is not yet used
        /// The senderId represents who sent this message
        /// The isAllAccess is a boolean to indicate whether the member's last message is all access or has unread all access
        /// The messageStatus is a bit mask value: 0 - Nothing, 1 - Read, 2 - Replied, 4 - Show, 8 - New, 16 - SavedToDraft, 32 - One Free Reply, 64 - Soft delete, 128 - Fraud Hold
        /// The optional markAsRead querystring parameter can be used to automatically mark messages returned as read.  Default is false. e.g. &amp;markasread=true
        /// 
        /// Example: Get Inbox conversation messages
        /// 
        /// This gets back the first 100 messages between you (based on access token) and the target member 1127807607.  It just so happens there's only one message, but it could return up to 100 if you had that many messages between the two.
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/mail/conversationInboxMessages/inbox/TargetMemberID/1127807607/Page/1/PageSize/100?access_token=
        /// 
        /// RESPONSE
        /// 
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "GroupId": 3,
        ///    "MemberId": 1116690655,
        ///    "TargetMemberId": 1127807607,
        ///    "MatchesFound": 1,
        ///    "MessageList": [
        ///      {
        ///        "messageId": 0, //this is always 0, please ignore
        ///        "sender": null,
        ///        "recipient": null,
        ///        "subject": "Email",
        ///        "mailType": "Email",
        ///        "body": "Meowww",
        ///        "insertDate": "2015-04-21T20:12:48.687Z",
        ///        "insertDatePst": "2015-04-21T13:12:48.687Z",
        ///        "openDate": null,
        ///        "messageStatus": 5,
        ///        "memberFolderId": 1,
        ///        "isAllAccess": false,
        ///        "senderId": 1127807607,
        ///        "threadId": "",
        ///        "id": 628414710 //this is the message list ID used to delete or update
        ///      },
        ///      {
        ///        "messageId": 0, //this is always 0, please ignore
        ///        "sender": null,
        ///        "recipient": null,
        ///        "subject": "IM Message",
        ///        "mailType": "InstantMessage",
        ///        "body": "Woof woof",
        ///        "insertDate": "2015-04-20T20:12:48.687Z",
        ///        "insertDatePst": "2015-04-20T13:12:48.687Z",
        ///        "openDate": null,
        ///        "messageStatus": 5,
        ///        "memberFolderId": 1,
        ///        "isAllAccess": false,
        ///        "senderId": 1127807607,
        ///        "threadId": "somethreadID", //this is the threadID that will be used to get all chat messages associated with this chat thread
        ///        "id": 628414710 //this is the message list ID used to delete or update
        ///      }
        ///    ]
        ///  }
        /// }
        /// 
        /// 
        /// Example: Same as above but marking those 100 messages as read at the same time
        /// 
        /// We will pass in the optional markAsRead querystring parameter.  The response will be the same as above.
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/mail/conversationInboxMessages/inbox/TargetMemberID/1127807607/Page/1/PageSize/100?markasread=true&amp;access_token=
        /// 
        /// 
        /// Example: Getting chat history for a specific chat conversation thread
        /// 
        /// We will pass in the optional threadID querystring parameter.  The response will be the same as above, but you'll only get chat messages that belongs to the threadID.
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/mail/conversationInboxMessages/chat/TargetMemberID/1127807607/Page/1/PageSize/100?threadId={threadIDValue}&amp;access_token=
        /// 
        /// 
        /// Example: Getting full chat history
        /// 
        /// Same as above, but we do not pass in any threadID.  The response will be the same as above, but you'll only get chat messages that belongs to the threadID.
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/mail/conversationInboxMessages/chat/TargetMemberID/1127807607/Page/1/PageSize/100?access_token=
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// Remarks:
        /// Special thanks to Ray Carpio on FNOLA!!
        /// 
        /// </example>
        /// <param name="targetMemberId">The target memberID that you had email messages with</param>
        /// <param name="pageNumber">The page number for the page of messages being requested</param>
        /// <param name="pageSize">The number of messages per page</param>
        /// <param name="memberId">Not used, derived from access token</param>
        /// <param name="ConversationType">The type of conversation.  Trash is not available here.</param>
        /// <param name="markasread">Querystring parameter to specify whether messages returned should automatically be marked as read.  Default is false. e.g. &amp;markasread=true</param>
        /// <param name="brandId">Not used, specified as part of URL path</param>
        /// <param name="siteId">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(MailConversationMessagesResponse))]
        [RequireAccessTokenV2]
        public ActionResult GetMailConversationMessages(MailConversationMessagesRequest mailConversationMessagesRequest)
        {
            MailConversationMessagesResponse response = new MailConversationMessagesResponse();
            response.MemberId = GetMemberId();
            response.TargetMemberId = mailConversationMessagesRequest.TargetMemberID;
            response.GroupId = mailConversationMessagesRequest.Brand.Site.Community.CommunityID;

            if (mailConversationMessagesRequest.ConversationType == ConversationType.None)
            {
                //inbox is default
                mailConversationMessagesRequest.ConversationType = ConversationType.Inbox;
            }

            //get conversation messages
            int totalMessages = 0;

            if (mailConversationMessagesRequest.ConversationType == ConversationType.Chat)
            {
                response.MessageList = MessageAccess.Instance.GetInstantMessageHistory(response.MemberId, response.TargetMemberId, mailConversationMessagesRequest.Brand, mailConversationMessagesRequest.PageNumber, mailConversationMessagesRequest.PageSize, mailConversationMessagesRequest.ThreadID, false, out totalMessages);
            }
            else
            {
                response.MessageList = MessageAccess.Instance.GetMailConversationMessages(response.MemberId, response.TargetMemberId, mailConversationMessagesRequest.Brand, mailConversationMessagesRequest.PageNumber, mailConversationMessagesRequest.PageSize, mailConversationMessagesRequest.ConversationType, mailConversationMessagesRequest.MarkAsRead, out totalMessages);
            }
            response.MatchesFound = totalMessages;

            return new NewtonsoftJsonResult { Result = response };
        }

        /// <summary>
        /// This endpoint is used to get back a list of members you've had messages (both emails and chat) with
        /// </summary>
        /// <example>
        /// 
        /// Notes:
        /// Endpoint returns a list of members, sorted by most recent first.
        /// Each member has an "All Access" property which indicates whether this member should be displayed in the All Access conversation section.  It is true if All Access is the last email in the conversation or there are unread all access emails
        /// 
        /// Each member has an "UnreadMessageCount" property to indicate how many total unread messages there are
        /// Each member has an "UnreadAllAccessCount" property to indicate how many unread all access emails there are
        /// The Member data is based on the ResultSetProfile attributeset.
        /// The MessageStatus here is the status of the last message between these 2 members, can be ignored here
        /// The MailType here is the type of message of the last message between these 2 members (for full list of message types, visit /mail/sendmailmessage endpoint)
        /// The Id represents this conversation member and is used with the removemailconversationinboxmember endpoint to remove a member from the conversation list
        /// The InsertDate is the last message date in UTC
        /// DraftCount is only currently available for the default Inbox conversation type
        /// 
        /// Example: Get inbox member list
        /// 
        /// You are identified based on your access token, so this will get back a list of all members you've had conversations with.
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/mail/conversationInboxMembers/inbox?access_token=
        /// 
        /// RESPONSE
        ///
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "GroupId": 3,
        ///    "MemberId": 1116690655,
        ///    "MatchesFound": 19, //total members found in the list
        ///    "MemberList": [
        ///      {
        ///        "Id": 628414710,
        ///        "MemberId": 1127807607,
        ///        "GroupId": 3,
        ///        "InsertDate": "2015-04-21T20:12:48.687Z",
        ///        "InsertDatePst": "2015-04-21T13:12:48.687Z",
        ///        "MessageStatus": 5,
        ///        "MailType": "Email",
        ///        "Body": null,
        ///        "IsBodyAllAccess": false, //this indicates whether the body message is an all access message
        ///        "AllAccess": false, //this indicates whether the conversation member is All Access
        ///        "AllAccessCount": 1, //this indicates how many received all access there are
        ///        "UnreadMessageCount": 0, //this indicates how many total unread messages there are
        ///        "UnreadAllAccessCount": 0, //this indicates how many unread all access messages there all
        ///        "DraftCount": 0, //this indicates how many draft messages you have with this member
        ///        "Member": {
        ///          "memberId": 1127807607,
        ///          "username": "1127807607",
        ///          "adminSuspendedFlag": false,
        ///          "age": 18,
        ///          "approvedPhotoCount": 0,
        ///          "blockContact": false,
        ///          "blockContactByTarget": false,
        ///          "blockSearch": false,
        ///          "blockSearchByTarget": false,
        ///          "defaultPhoto": null,
        ///          "gender": "Male",
        ///          "heightCm": null,
        ///          "isHighlighted": false,
        ///          "isOnline": false,
        ///          "isSpotlighted": false,
        ///          "isViewersFavorite": false,
        ///          "jDateEthnicity": null,
        ///          "jDateReligion": null,
        ///          "lastLoggedIn": "0001-01-01T00:00:00.000Z",
        ///          "lastUpdated": "2015-04-21T20:07:14.053Z",
        ///          "location": "Tel Aviv - Yaffo, 06 ",
        ///          "lookingFor": [],
        ///          "maritalStatus": "Single",
        ///          "matchRating": 86,
        ///          "passedFraudCheck": true,
        ///          "PhotoUrl": null,
        ///          "primaryPhoto": null,
        ///          "regionId": 9801455,
        ///          "registrationDate": "0001-01-01T00:00:00.000Z",
        ///          "removeFromSearch": false,
        ///          "removeFromSearchByTarget": false,
        ///          "seekingGender": "Female",
        ///          "selfSuspendedFlag": false,
        ///          "ThumbnailUrl": null,
        ///          "zipcode": null
        ///        }
        ///      },
        ///      ....more members here...
        ///      ]
        ///    }
        ///  }
        ///  
        /// 
        /// 
        /// Example: Get inbox member list with pagination
        /// 
        /// Same as above, but we'll use the optional route that also takes the page and page size parameters as part of the path.
        /// Here we'll grab page 1 with 10 members
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/mail/conversationInboxMembers/inbox/Page/1/PageSize/10?access_token=
        /// 
        /// 
        /// Example: Include body of most recent message for each member
        /// 
        /// Add the "bodylength" parameter as a querystring.  Here we will specify it as 50 to include the first 50 characters of the message.
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/mail/conversationInboxMembers/inbox?bodylength=50&amp;access_token=
        /// 
        /// RESPONSE
        ///
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "GroupId": 3,
        ///    "MemberId": 1116690655,
        ///    "MatchesFound": 19, //total members found in the list
        ///    "MemberList": [
        ///      {
        ///        "Id": 628414710,
        ///        "MemberId": 1127807607,
        ///        "GroupId": 3,
        ///        "InsertDate": "2015-04-21T20:12:48.687Z",
        ///        "InsertDatePst": "2015-04-21T13:12:48.687Z",
        ///        "MessageStatus": 5,
        ///        "MailType": "Email",
        ///        "Body": "I am taking a break and will respond when I return.",
        ///        "IsBodyAllAccess": false, //this indicates whether the body message is an all access message
        ///        "AllAccess": false, //this indicates whether the conversation member is All Access
        ///        "AllAccessCount": 1, //this indicates how many received all access there are
        ///        "UnreadMessageCount": 0, //this indicates how many total unread messages there are
        ///        "UnreadAllAccessCount": 0, //this indicates how many unread all access messages there all
        ///        "DraftCount": 0, //this indicates how many draft messages you have with this member
        ///        "Member": {
        ///          "memberId": 1127807607,
        ///          "username": "1127807607",
        ///          "adminSuspendedFlag": false,
        ///  ....
        ///  
        /// 
        /// 
        /// 
        /// 
        /// 
        /// Remarks:
        /// Special thanks to David Bremer on FNOLA!!
        ///  
        ///  
        /// </example>
        /// <param name="ConversationType">The conversation type to filter appropriately</param>
        /// <param name="pageNumber">The page number for the page of messages being requested</param>
        /// <param name="pageSize">The number of messages per page</param>
        /// <param name="bodylength">Querystring parameter, use it to specify how many characters of the most recent message body text you want to be included in the result.  Default is 0 so no message body is returned.</param>
        /// <param name="brandId">Not used, specified as part of URL path</param>
        /// <param name="siteId">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(MailConversationMembersResponse))]
        [RequireAccessTokenV2]
        public ActionResult GetMailConversationMembers(MailConversationMembersRequest mailConversationMembersRequest)
        {
            var response = new MailConversationMembersResponse
            {
                MemberId = GetMemberId(),
                GroupId = mailConversationMembersRequest.Brand.Site.Community.CommunityID
            };

            if (mailConversationMembersRequest.ConversationType == ConversationType.None)
            {
                //inbox is default
                mailConversationMembersRequest.ConversationType = ConversationType.Inbox;
            }

            //get conversation members
            int totalMembers;

            if (mailConversationMembersRequest.ConversationType == ConversationType.Chat)
            {
                response.MemberList = MessageAccess.Instance.GetInstantMessageHistoryMembers(response.MemberId, mailConversationMembersRequest.Brand, mailConversationMembersRequest.PageNumber, mailConversationMembersRequest.PageSize, false, mailConversationMembersRequest.BodyLength, out totalMembers);
            }
            else
            {
                response.MemberList = MessageAccess.Instance.GetMailConversationMembers(response.MemberId, mailConversationMembersRequest.Brand, mailConversationMembersRequest.ConversationType, mailConversationMembersRequest.PageNumber, mailConversationMembersRequest.PageSize, mailConversationMembersRequest.BodyLength, out totalMembers);
            }
            response.MatchesFound = totalMembers;

            return new NewtonsoftJsonResult { Result = response };
        }

        /// <summary>
        /// Removes or re-adds a member from your Email Inbox members conversation list
        /// Note: Member will show back up in your conversation list if you send or get a new message from that member
        /// </summary>
        /// <example>
        /// 
        /// NOTES:
        /// The "messageId" is the ID that is returned as part of the GetMailConversationMembers endpoint
        /// 
        /// This will remove member based on message Id 628404777 from your conversation member list
        /// Note: Notice we're passing in undo as "false", which is to remove.  You can change that to true to re-add the member.
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/mail/removeConversationInboxMember/messageId/628404777/undo/false?access_token=
        /// 
        /// RESPONSE
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "success": true,
        ///    "messageId": 628404777,
        ///    "failReason": "",
        ///  }
        /// }
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// Remarks:
        /// Special thanks to Fawad Akbar of FNOLA!!
        /// 
        /// </example>
        /// <param name="MessageId">This is the id included in the results of the GetMailConversationMembers endpoint, pass that in to remove that member from the list</param>
        /// <param name="Undo">False means you're removing the member, True allows you to undo that and re-adds the member</param>
        /// <param name="memberId">Not used, derived from access token</param>
        /// <param name="brandId">Not used, specified as part of URL path</param>
        /// <param name="siteId">Not used</param>
        /// <param name="Brand">Not used</param>
        [ApiMethod(ResponseType = typeof(MessageMemberRemoveResponse))]
        [RequireAccessTokenV2]
        public ActionResult RemoveMailConversationMember(MessageMemberRemoveRequest removeRequest)
        {
            bool removed;

            removed = EmailMessageSA.Instance.MarkMailConversationMemberRemoved(GetMemberId(),
                                                                removeRequest.Brand.Site.Community.CommunityID,
                                                                !removeRequest.Undo,
                                                                new ArrayList { removeRequest.MessageId },
                                                                ConversationType.Inbox,
                                                                null);
            var result = new MessageMemberRemoveResponse
            {
                MessageId = removeRequest.MessageId,
                Success = removed,
                FailureReason = removed ? "" : "Conversation based on MessageId provided not found" //currently, this is the only reason for false from SA
            };

            return new NewtonsoftJsonResult { Result = result };    
        }

        /// <summary>
        /// This will delete a conversation between 2 members, including all messages (emails and chat).
        /// This will normally be done from the Trash filter.  To remove items and send them to Trash, see the RemoveMailConversationMember endpoint instead.
        /// </summary>
        /// <example>
        /// 
        /// NOTES:
        /// The "messageId" is the ID that is returned as part of the GetMailConversationMembers endpoint
        /// To delete, both the target member ID and the messageId (this is the ID that you have representing the member from the GetMailConversationMembers endpoint for trash filter).
        /// The conversation is currently restricted to only be allowed to be deleted after it has been moved to the trash filter.
        /// 
        /// 
        /// Example: Delete all conversations (email and chat) between member (based on access token) and target memberID 100004579
        /// 
        /// 
        /// GET http://api.stage3.spark.net/v2/brandid/1003/mail/deleteConversationInboxMember/targetMemberId/100004579/messageId/608487639?access_token=
        /// 
        /// SUCCESS RESPONSE
        /// {
        ///    code: 200
        ///    status: "OK"
        ///    data: {
        ///     Success: true
        ///     MessageId: 608487639
        ///     FailureReason: ""
        ///    }
        /// }
        ///
        /// 
        /// 
        /// FAIL RESPONSE
        /// {
        ///    code: 200
        ///    status: "OK"
        ///    data: {
        ///     Success: false
        ///     MessageId: 608487639
        ///     FailureReason: "MessageMemberNotFoundInTrash"
        ///    }
        /// }
        /// 
        /// </example>
        /// <param name="MessageId">This is the id included in the results of the GetMailConversationMembers endpoint, pass that in to delete that member from the list</param>
        /// <param name="TargetMemberId">This is target memberId who the user has a conversation with that will be deleted</param>
        /// <param name="memberId">Not used, derived from access token</param>
        /// <param name="brandId">Not used, specified as part of URL path</param>
        /// <param name="siteId">Not used</param>
        /// <param name="Brand">Not used</param>
        [ApiMethod(ResponseType = typeof(MessageMemberDeleteResponse))]
        [RequireAccessTokenV2]
        public ActionResult DeleteMailConversationMember(MessageMemberDeleteRequest deleteRequest)
        {
            //for now, we only delete a message if it is in Trash
            MessageGeneralResult messageDeleteResult = MessageAccess.Instance.DeleteMailConversation(GetMemberId(), deleteRequest.TargetMemberId, deleteRequest.Brand, deleteRequest.MessageId, ConversationType.Trash);

            var result = new MessageMemberDeleteResponse
            {
                MessageId = messageDeleteResult.MessageListID,
                Success = messageDeleteResult.Status == MessageGeneralResultStatus.Success,
                FailureReason = messageDeleteResult.Error.ToString()
            };

            return new NewtonsoftJsonResult { Result = result };  
        }

        /// <summary>
        /// This provides support for marking 1 or more messages as having been read.
        /// This is useful for apps pulling messages that have not been marked automatically, perhaps for pre-loading in the client,
        /// so this endpoint can be used to mark those messages as read once they are displayed to the user.
        /// </summary>
        /// <example>
        /// 
        /// Example: Update 4 messages as having been read
        /// 
        /// POST http://api.stage3.spark.net/v2/brandId/1003/mail/message/read?access_token
        /// 
        /// {
        ///     "MessageListIDs": [1010982, 2028282, 202893, 202828]
        /// }
        /// 
        /// 
        /// RESPONSE
        /// 
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "UpdateStatus": "true"
        ///  }
        /// }
        /// 
        /// </example>
        /// <param name="MessageListIDs">An array or list of one or more message ID (internally, it's the messageListID)</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(bool))]
        [RequireAccessTokenV2]
        public ActionResult MarkReadMailMessages(MarkReadMailMessagesRequest request)
        {
            int memberID = GetMemberId();
            bool result = MessageAccess.Instance.MarkRead(memberID, request.Brand.Site.Community.CommunityID, request.MessageListIDs);
            return new NewtonsoftJsonResult
            {
                Result = new { UpdateStatus = result.ToString().ToLower() }
            };
        }

        /// <summary>
        /// Add or remove email address for the Do Not Email list
        /// </summary>
        /// <example>
        /// 
        /// Note:
        /// The RemoveDne parameter is optional and defaults to false.
        /// This endpoint is made for public consumption by anybody who wants to opt-out, there are no security on this endpoint - no IP restrictions, tokens, or secret
        /// 
        /// Example: Add email to DNE
        /// POST http://api.stage3.spark.net/v2/brandId/1003/mail/dne
        /// 
        /// {
        ///    "EmailAddress": "rcarpio@spark.net"
        /// }
        /// 
        /// RESPONSE
        /// 
        /// {"code":200,"status":"OK","data":{}}
        /// 
        /// 
        /// 
        /// Example: Remove email from DNE
        /// POST http://api.stage3.spark.net/v2/brandId/1003/mail/dne
        /// 
        /// {
        ///    "EmailAddress": "dbremer10@spark.net",
        ///    "RemoveDne": true
        /// }
        /// 
        /// RESPONSE
        /// 
        /// {"code":200,"status":"OK","data":{}}
        /// 
        /// 
        /// </example>
        /// <param name="EmailAddress" required="true">The email address to update to DNE</param>
        /// <param name="RemoveDne">Default is false. A boolean to indicate whether to remove email address from DNE.</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [HttpPost]
        public ActionResult UpdateDNE(MailDneRequest mailDneRequest)
        {
            //validate email
            EmailVerifyHelper emailVerifyHelper = new EmailVerifyHelper(mailDneRequest.Brand);
            if (emailVerifyHelper.IsValidDNEmail(mailDneRequest.EmailAddress))
            {
                DoNotEmailEntry entry = DoNotEmailSA.Instance.GetEntryBySiteAndEmailAddress(mailDneRequest.Brand.Site.SiteID,
                                                                                            mailDneRequest.EmailAddress.ToLower());

                if (mailDneRequest.RemoveDne)
                {
                    //for remove, we will always call MT, no need to check if they're already on dne because cache doesn't look to be synced between all instances of external mail service

                    //remove from dne
                    bool success = DoNotEmailSA.Instance.RemoveEmailAddress(mailDneRequest.Brand.Site.SiteID, mailDneRequest.EmailAddress.ToLower());
                    if (!success)
                    {
                        return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, string.Format("Error removing to DNE list, email: {0}", mailDneRequest.EmailAddress));
                    }
                }
                else
                {
                    //already on dne
                    if (entry != null)
                    {
                        return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.EmailAlreadyExists, string.Format("Email already on DNE list, email: {0}", mailDneRequest.EmailAddress));
                    }

                    //add to dne
                    bool success = false;
                    int memberID = MemberSA.Instance.GetMemberIDByEmail(mailDneRequest.EmailAddress);
                    if (memberID > 0)
                    {
                        success = DoNotEmailSA.Instance.AddEmailAddress(mailDneRequest.Brand.BrandID, mailDneRequest.EmailAddress.ToLower(), memberID);
                    }
                    else
                    {
                        success = DoNotEmailSA.Instance.AddEmailAddress(mailDneRequest.Brand.BrandID, mailDneRequest.EmailAddress.ToLower());
                    }
 
                    if (!success)
                    {
                        return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericFailedValidation, string.Format("Error adding to DNE list, email: {0}, memberId: {1}", mailDneRequest.EmailAddress, memberID));
                    }
                }

                return Ok();
            }

            return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidEmail, string.Format("Invalid email address, email: {0}", mailDneRequest.EmailAddress));
        }
    }
}