﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Spark.Logger;
using Spark.Rest.Configuration;
using Spark.Rest.Helpers;
using Spark.REST.DataAccess.Session;
using Spark.REST.Entities.Session;
using Spark.REST.Models.Session;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

#endregion

namespace Spark.REST.Controllers
{
    public class SessionController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(SessionController));

        //This method should be deprecated after the 10/3 release
        [ApiMethod(ResponseType = typeof (Session))]
        public ActionResult Create(SessionCreateRequest request)
        {
            try
            {
                var member = MemberSA.Instance.GetMember(request.MemberID, MemberLoadFlags.None);
                if (member == null)
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberId,
                                                         "Could not retrieve Member: " + request.MemberID);
                }

                var brand = BrandConfigSA.Instance.GetBrandByID(request.BrandId);
                var genderMask = member.GetAttributeInt(brand, "GenderMask", Constants.NULL_INT);

                // EventLog.WriteEntry("REST", "Web RESTCreateSession called. " + request.BrandId, EventLogEntryType.Information);
                var userSession = SessionSA.Instance.RESTCreateSession(request.BrandId, request.MemberID,
                                                                       member.EmailAddress, genderMask);
                if (userSession == null)
                {
                    Log.LogWarningMessage("Got a null session from SessionSA.Instance.RESTCreateSession", ErrorHelper.GetCustomData());
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedCreateSession,
                                                         "Got a null session from SessionSA.Instance.RESTCreateSession");
                }

                var session = new Session
                                  {
                                      Key = userSession.Key.ToString(),
                                  };

                return new NewtonsoftJsonResult {Result = session};
            }
            catch (Exception ex)
            {
                Log.LogError("Error creating session.", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedCreateSession,
                                                     "Error creating session: " + ex);
            }
        }

        [ApiMethod(ResponseType = typeof (Session))]
        [HttpPost]
        public ActionResult CreateWithSessionId(SessionCreateRequestWithSessionId request)
        {
            try
            {
                var member = MemberSA.Instance.GetMember(request.MemberID, MemberLoadFlags.None);
                if (member == null)
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberId,
                                                         "Could not retrieve Member: " + request.MemberID);
                }

                var brand = BrandConfigSA.Instance.GetBrandByID(request.BrandId);
                var genderMask = member.GetAttributeInt(brand, "GenderMask", Constants.NULL_INT);

                // EventLog.WriteEntry("REST", "Web RESTCreateSession called. " + request.BrandId, EventLogEntryType.Information);
                var userSession = SessionSA.Instance.RESTCreateSession(request.BrandId, request.MemberID,
                                                                       member.EmailAddress, genderMask,
                                                                       request.SessionId);
                if (userSession == null)
                {
                    Log.LogWarningMessage("Got a null session from SessionSA.Instance.RESTCreateSession", ErrorHelper.GetCustomData());
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedCreateSession,
                                                         "Got a null session from SessionSA.Instance.RESTCreateSession");
                }

                var session = new Session
                                  {
                                      Key = userSession.Key.ToString(),
                                  };

                return new NewtonsoftJsonResult {Result = session};
            }
            catch (Exception ex)
            {
                Log.LogError("Error creating session.", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedCreateSession,
                                                     "Error creating session: " + ex);
            }
        }


        [ApiMethod(ResponseType = typeof (SessionMemberResponse))]
        public ActionResult GetMemberForSession(SessionMemberRequest request)
        {
            try
            {
                var memberID = SessionSA.Instance.GetMemberID(request.SessionId);
                var response = new SessionMemberResponse {MemberId = memberID};

                return new NewtonsoftJsonResult {Result = response};
            }
            catch (Exception ex)
            {
                Log.LogError("Error getting member from session.", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedGetMemberFromSession,
                                                     "Error getting member from session: " + ex);
            }
        }

        [ApiMethod]
        public ActionResult Get(SessionRequest request)
        {
            try
            {
                // EventLog.WriteEntry("REST", "Web RESTGetSession called. " + request.Key, EventLogEntryType.Information);
                var userSession = SessionSA.Instance.RESTGetSession(request.Key);

                if (userSession == null)
                {
                    return null;
                }

                var session = new Session
                                  {
                                      Key = userSession.Key.ToString(),
                                  };

                var properties = new Dictionary<String, Object>();
                foreach (String key in userSession.Properties.Keys)
                {
                    var sessionProperty = userSession.Properties[key] as SessionProperty;

                    if (sessionProperty == null)
                        return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidSessionProperty,
                                                             "Session property cannot be null. Key:" + key);


                    properties.Add(key, sessionProperty.Val);
                }
                session.Properties = properties;

                return new NewtonsoftJsonResult {Result = session};
            }
            catch (Exception ex)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedGetSession,
                                                     "Error getting session: " + ex);
            }
        }

        /// <summary>
        ///   Keeps the session alive.The only way a session is removed is through call back expiration.
        /// </summary>
        /// <param name = "request"></param>
        [ApiMethod]
        [HttpPost]
        public ActionResult Save(SessionRequest request)
        {
            try
            {
                //EventLog.WriteEntry("REST", "Web RESTSaveSession called. " + request.Properties.Count, EventLogEntryType.Information);

                var userSession = new UserSession(new Guid(request.Key));
                //TODO reference tracker, caching...


                foreach (var property in request.Properties.Where(property => property.Value != null))
                {
                    userSession.Add(property.Key, property.Value.ToString(), SessionPropertyLifetime.Persistent);
                }

                userSession.CacheTTLSeconds = 3600;

                SessionSA.Instance.RESTSaveSession(userSession);
            }
            catch (Exception ex)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedSaveSession,
                                                     "Error saving session: " + ex);
            }
            return new EmptyResult();
        }

        [ApiMethod]
        [HttpPost]
        public ActionResult CreateSessionTracking(CreateSessionTrackingRequest createSessionTrackingRequest)
        {
            var response = SessionTrackingDataAccess.Instance.CreateSessionTracking(createSessionTrackingRequest);
            return new NewtonsoftJsonResult {Result = response};
        }
    }
}