﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Spark.Common.FraudService;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.REST.Controllers;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Helpers;
using Spark.Rest.V2.Models;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

namespace Spark.Rest.V2.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class FraudController : SparkControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fraudRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(FraudDetailedResponse))]
        [RequireClientCredentials]
        public ActionResult GetMemberFraudInfo(FraudInfoRequest fraudRequest)
        {
            FraudDetailedResponse fraudInfo;
            try
            {
                fraudInfo = FraudHelper.GetMemberFraudInfo(fraudRequest.MemberId, fraudRequest.RiskInquiryTypeID,
                    fraudRequest.SiteId, fraudRequest.bindingID);
            }
            catch (Exception ex)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedFraudInfoGet,"No Fraud info found for memberId");
            }

            return new NewtonsoftJsonResult { Result = fraudInfo };
        }
    }
}
