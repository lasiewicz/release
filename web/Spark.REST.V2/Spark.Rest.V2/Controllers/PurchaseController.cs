﻿#region

using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Spark.Common.Adapter;
using Spark.Logger;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.DataAccess.Purchase;
using Spark.REST.Entities.Purchase;
using Spark.Rest.Helpers;
using Spark.Rest.Serialization;
using Spark.Rest.V2.DataAccess;
using Spark.Rest.V2.Entities.Purchase;
using Spark.Rest.V2.Exceptions;

#endregion

namespace Spark.REST.Controllers
{
    /// <summary>
    /// </summary>
    public class PurchaseController : SparkControllerBase
    {
        public const int RENEWAL_PROCESSOR_APP = 1077;
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(PurchaseController));
        private IActivityRecordingAccess _activityRecordingAccess;
        private IPurchaseAccess _purchaseAccess;

        /// <summary>
        ///     Gets or sets the purchase access.
        /// </summary>
        /// <value>
        ///     The purchase access.
        /// </value>
        public IPurchaseAccess PurchaseAccess
        {
            get { return _purchaseAccess ?? (_purchaseAccess = Rest.DataAccess.Purchase.PurchaseAccess.Singleton); }
            set { _purchaseAccess = value; }
        }

        /// <summary>
        ///     Gets or sets the activity recording access.
        /// </summary>
        /// <value>
        ///     The activity recording access.
        /// </value>
        public IActivityRecordingAccess ActivityRecordingAccess
        {
            get { return _activityRecordingAccess ?? (_activityRecordingAccess = Rest.V2.DataAccess.ActivityRecordingAccess.Instance); }
            set { _activityRecordingAccess = value; }
        }

        /// <summary>
        /// Call that returns the products we are promoting for IAP.
        /// </summary>
        /// <param name="request"></param>
        /// <example>
        ///     POST http://api.local.spark.net/v2/brandId/1003/purchase/iap/products?access_token=ACCESS_TOKEN
        ///     {"requestData":""}
        /// </example>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(List<IOSPurchaseProduct>))]
        [HttpGet]
        [RequireAccessTokenV2]
        public ActionResult GetIOSInAppProducts(IOSPurchaseProductsRequest request)
        {
            var products = new List<IOSPurchaseProduct> {new IOSPurchaseProduct("com.spark.JDate.JD_1M_SUB_20141002", "39.99", "1", "1 Month Sub")};
            return new NewtonsoftJsonResult {Result = products};
        }

        /// <summary>
        /// This call will send an encoded ios receipt to Apple's api site and return receipt details in json
        /// </summary>
        /// <param name="request">IOSInAppValidateReceiptRequest</param>
        /// <example>
        ///     POST http://api.local.spark.net/v2/brandId/0/purchase/iap/receiptdetails?applicationId=1078&amp;
        ///     client_secret=grb312kMJnrK7cI4MWA111ul74MCpFqwbl%2BplYcChio%3D
        /// {
        ///     "encodedReceiptJSON":[Encoded IOS Receipt Data]
        /// }
        /// </example>
        /// <remarks>
        ///     example return response
        ///     <code>
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data":{
        ///     "status": 0,
        ///     "environment": "Sandbox",
        ///     "receipt":{
        ///        "receipt_type": "ProductionSandbox",
        ///        "bundle_id": "com.spark.JDate",
        ///        "application_version": "2.0",
        ///        "request_date": "2014-11-05 05:25:37 Etc/GMT",
        ///        "request_date_ms": "1415165137621",
        ///        "request_date_pst": "2014-11-04 21:25:37 America/Los_Angeles",
        ///        "original_purchase_date": "2013-08-01 07:00:00 Etc/GMT",
        ///        "original_purchase_date_ms": "1375340400000",
        ///        "original_purchase_date_pst": "2013-08-01 00:00:00 America/Los_Angeles",
        ///        "original_application_version": "1.0",
        ///        "in_app":[{
        ///              "ExpiresDateUTC": "0001-01-01T00:00:00Z",
        ///              "PurchaseDateUTC": "0001-01-01T00:00:00Z",
        ///              "CancellationDateUTC": "0001-01-01T00:00:00Z",
        ///              "quantity": "1",
        ///              "product_id": "com.spark.JDate.JDI_1M_SUB_TEST_20120424",
        ///              "transaction_id": "1000000117052983",
        ///              "original_transaction_id": "1000000116628695",
        ///              "purchase_date": "2014-09-18 19:29:56 Etc/GMT",
        ///              "purchase_date_ms": "1411068596000",
        ///              "purchase_date_pst": "2014-09-18 12:29:56 America/Los_Angeles",
        ///              "original_purchase_date": "2014-07-16 18:29:48 Etc/GMT",
        ///              "original_purchase_date_ms": "1405535388000",
        ///              "original_purchase_date_pst": "2014-07-16 11:29:48 America/Los_Angeles",
        ///              "expires_date": "2014-07-16 18:33:44 Etc/GMT",
        ///              "expires_date_ms": "1405535624000",
        ///              "expires_date_pst": "2014-07-16 11:33:44 America/Los_Angeles",
        ///              "cancellation_date": null,
        ///              "cancellation_date_ms": null,
        ///              "cancellation_date_pst": null,
        ///              "web_order_line_item_id": "1000000028407382",
        ///              "is_trial_period": "false"
        ///           }]
        ///     },
        ///     "latest_receipt_info":[{
        ///           "quantity": "1",
        ///           "product_id": "com.spark.JDate.JD_3M_SUB_TEST_20140814",
        ///           "transaction_id": "1000000124387691",
        ///           "original_transaction_id": "1000000116628695",
        ///           "purchase_date": "2014-11-05 05:25:37 Etc/GMT",
        ///           "purchase_date_ms": "1415165137610",
        ///           "purchase_date_pst": "2014-11-04 21:25:37 America/Los_Angeles",
        ///           "original_purchase_date": "2014-09-18 20:41:04 Etc/GMT",
        ///           "original_purchase_date_ms": "1411072864000",
        ///           "original_purchase_date_pst": "2014-09-18 13:41:04 America/Los_Angeles",
        ///           "expires_date": "2014-09-18 20:59:55 Etc/GMT",
        ///           "expires_date_ms": "1411073995000",
        ///           "expires_date_pst": "2014-09-18 13:59:55 America/Los_Angeles",
        ///           "web_order_line_item_id": "1000000028621835",
        ///           "is_trial_period": "false"
        ///        }],
        ///     "latest_receipt":[LATEST ENCODED IOS RECEIPT DATA]
        /// }
        /// </code>
        /// </remarks>
        [ApiMethod(ResponseType = typeof(IOSReceipt))]
        [HttpPost]
        [RequireClientCredentials]
//        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/UPSTrustedClientIPWhitelist.xml")]
        public ActionResult GetIOSReceiptDetails(IOSInAppValidateReceiptRequest request)
        {
            Exception thrownException = null;
            try
            {
                APIAdapter.RestStatus restStatus;
                var iosReceiptDetails = PurchaseAccess.GetIOSReceiptDetails(request.EncodedReceiptJSON,
                    out restStatus, request.Brand.Site.SiteID);
                return new NewtonsoftJsonResult {Result = iosReceiptDetails};
            }
            catch (SparkAPIReportableException reportableException)
            {
                thrownException = reportableException;
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors());
                return new SparkHttpBadRequestResult((int) reportableException.SubStatusCode,
                    reportableException.Message);
            }
            catch (Exception exception)
            {
                thrownException = exception;
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors());
                return new SparkGenericHttpInternalServerErrorResult();
            }
            finally
            {
                if (null != thrownException)
                {
                    ActivityRecordingAccess.RecordLatestInAppReceiptActivityException(GetMemberId(),request.BrandId,thrownException);
                }
            }
        }
       
        /// <summary>This call determines the renewal action for an in app purchase subscription.</summary>
        /// <param name="request">IOSInAppPurchaseRequest</param>  
        /// <example>
        ///     POST http://api.local.spark.net/v2/brandId/0/purchase/iap/renew?applicationId=1077&amp;
        ///     client_secret=8LOctpPHWPvJPhAC8tdAz6gUdWchsBrZFnWAvTbd33Y&#61;
        /// {
        ///     "memberId":[member id],
        ///     "siteId":[site id]
        /// }
        /// </example>
        /// <remarks>
        ///     returns value from Enum
        ///     <code>IAPControllerStatus
        ///     {
        ///         None = 0,
        ///         PurchaseOrder = 1,
        ///         RenewOrder = 2,
        ///         TerminateRenewal = 3,
        ///         VoidPreviousOrder = 4
        ///     }</code>
        /// </remarks>
        [ApiMethod(ResponseType = typeof(NewtonsoftJsonResult))]
        [HttpPost]
        [RequireClientCredentials]
//        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/UPSTrustedClientIPWhitelist.xml")]
        public ActionResult RenewIOSInAppPurchase(IOSInAppRenewRequest request)
        {
            //if not renewal processor app, return status of None
            if (GetAppId != RENEWAL_PROCESSOR_APP)
            {
                return new NewtonsoftJsonResult
                {
                    Result = new {Status = IAPControllerStatus.None, RenewalExternalPackageID = request.RenewalSku}
                };
            }

            Exception thrownException = null;
            //TODO: Flag added for UPS Testing.  Remove before launch to prod. -arod
            try
            {
                if (request.IapStatus > -1)
                {
                    var iapStatus = (IAPControllerStatus) request.IapStatus;
                    return new NewtonsoftJsonResult { Result = new {Status=iapStatus, RenewalExternalPackageID=request.RenewalSku}};
                }
            }
            catch (Exception e)
            {
                Log.LogError(e.Message, e, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
            }
            
            var memberId = (request.MemberId <= 0) ? GetMemberId() : request.MemberId;
            try
            {
                var iosValidateResponse = PurchaseAccess.ValidateIOSReceipt(request.Brand, request.EncodedReceiptJSON, memberId);
                var iapStatus = iosValidateResponse.Status;
                if (!string.IsNullOrEmpty(request.RenewalSku) && request.RenewalSku.ToLower() == iosValidateResponse.LastInAppReceipt.product_id.ToLower())
                {
                    if (iapStatus == IAPControllerStatus.PurchaseOrder)
                    {
                        iapStatus = IAPControllerStatus.RenewOrder;
                    }
                }
                return new NewtonsoftJsonResult() { Result = new { Status = iapStatus, RenewalExternalPackageID = iosValidateResponse.LastInAppReceipt.product_id, OriginalPurchaseDate = iosValidateResponse.LastInAppReceipt.original_purchase_date } };
            }
            catch (SparkAPIReportableException reportableException)
            {
                thrownException = reportableException;
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkHttpBadRequestResult((int) reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                thrownException = exception;
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkGenericHttpInternalServerErrorResult();
            }
            finally
            {
                if (null != thrownException)
                {
                    ActivityRecordingAccess.RecordLatestInAppReceiptActivityException(memberId, request.Brand.BrandID, thrownException);
                }
            }
        }

        /// <summary>This call processes an ios in app purchase.</summary>
        /// <param name="request">IOSInAppPurchaseRequest</param>
        /// <param name="RegionId" required="false" values="">Use only for CM members</param>
        /// <example>
        ///     <code>http://api.local.spark.net/v2/brandId/1003//purchase/iap/process?access_token=ACCESS_TOKEN
        /// {
        ///     "encondedReceiptJSON":[encoded ios receipt data],
        ///     "orderAttributeXML":[order attributes]
        /// }
        /// </code>
        /// </example>
        /// <remarks>
        ///     returns value from Enum
        ///     <code>IAPControllerStatus
        ///     {
        ///         None = 0,
        ///         PurchaseOrder = 1,
        ///         RenewOrder = 2,
        ///         TerminateRenewal = 3,
        ///         VoidPreviousOrder = 4
        ///     }</code>
        /// </remarks>
        [ApiMethod(ResponseType = typeof(IAPControllerStatus))]
        [HttpPost]
        [RequireAccessTokenV2]
        public ActionResult ProcessIOSInAppPurchase(IOSInAppPurchaseRequest request)
        {
            Exception thrownException = null;
            var memberId = GetMemberId();
            try
            {
                var iapControllerStatus = PurchaseAccess.ProcessIOSOrder
                    (request.Brand, request.EncodedReceiptJSON, memberId, request.OrderAttributeXML, request.RegionId);
                return new NewtonsoftJsonResult {Result = iapControllerStatus};
            }
            catch (SparkAPIReportableException reportableException)
            {
                thrownException = reportableException;
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkHttpBadRequestResult((int) reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                thrownException = exception;
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkGenericHttpInternalServerErrorResult();
            }
            finally
            {
                if (null != thrownException)
                {
                    ActivityRecordingAccess.RecordLatestInAppReceiptActivityException(memberId, request.BrandId, thrownException);
                }
            }
        }

        /// <summary>
        /// This call will send subscription token to Google's api site and return subscription details in json
        /// </summary>
        /// <param name="request">AndroidInAppValidateReceiptRequest</param>
        /// <example>
        ///     POST http://api.local.spark.net/v2/brandId/0/purchase/iab/receiptdetails?applicationId=1078&amp;
        ///     client_secret=grb312kMJnrK7cI4MWA111ul74MCpFqwbl%2BplYcChio%3D
        /// {
        ///     "subscriptionToken":[PackageName|SubscriptionId|TokenId],
        ///     "siteId":[site id]
        /// }
        /// </example>
        /// <remarks>
        ///     example return response
        ///     <code>
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data":    {
        ///       "Status": 0,
        ///       "ErrorReason": null,
        ///       "Kind": "androidpublisher#subscriptionPurchase",
        ///       "IsAutoRenew": false,
        ///       "ExpirationDate": "2015-01-04T16:09:23.6Z",
        ///       "StartDate": "2014-12-04T16:09:50.028Z",
        ///       "PackageName": "com.spark.christianmingle",
        ///       "SubscriptionId": "test_cm_1m_basic_32.99",
        ///       "TokenId": "nkcgoegnnedniofanifpllnk.AO-J1OwtfgBiS7RAh9iy9P8DVRTfUmNkH14bYEEpxvK79LxqC0PyGzqFTKaqQCD-AbSi0Ibc6HtR7IFDQanxBS5t236q4zfdG7z_fbqvMcsYgLufg_QA0Osu1erXav5m6gmUw8NGYAl_2Z83IHIXxSWQK4v-NlRWDw"
        ///    }
        /// }
        /// </code>
        /// </remarks>
        [ApiMethod(ResponseType = typeof(AndroidSubscription))]
        [HttpPost]
        [RequireClientCredentials]
        //        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/UPSTrustedClientIPWhitelist.xml")]
        public ActionResult GetAndroidReceiptDetails(AndroidInAppValidateReceiptRequest request)
        {
            Exception thrownException = null;
            try
            {
                APIAdapter.RestStatus restStatus;
                var androidSubscription = PurchaseAccess.GetAndroidSubscriptionDetails(request.SubscriptionToken, out restStatus, request.Brand.Site.SiteID);
                return new NewtonsoftJsonResult {Result = androidSubscription};
            }
            catch (SparkAPIReportableException reportableException)
            {
                thrownException = reportableException;
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkHttpBadRequestResult((int)reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                thrownException = exception;
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkGenericHttpInternalServerErrorResult();
            }
            finally
            {
                if (null != thrownException)
                {
                    ActivityRecordingAccess.RecordLatestInAppBillingActivityException(GetMemberId(), request.BrandId, thrownException);
                }
            }
        }

        /// <summary>This call determines the renewal action for an android subscription.</summary>
        /// <param name="request">AndroidInAppRenewRequest</param>  
        /// <example>
        ///     POST http://api.local.spark.net/v2/brandId/0/purchase/iab/renew?applicationId=1077&amp;
        ///     client_secret=8LOctpPHWPvJPhAC8tdAz6gUdWchsBrZFnWAvTbd33Y&#61;
        /// {
        ///     "memberId":[member id],
        ///     "siteId":[site id]
        /// }
        /// </example>
        /// <remarks>
        ///     returns value from Enum
        ///     <code>IAPControllerStatus
        ///     {
        ///         None = 0,
        ///         PurchaseOrder = 1,
        ///         RenewOrder = 2,
        ///         TerminateRenewal = 3,
        ///         VoidPreviousOrder = 4
        ///     }</code>
        /// </remarks>
        [ApiMethod(ResponseType = typeof(IAPControllerStatus))]
        [HttpPost]
        [RequireClientCredentials]
        //        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/UPSTrustedClientIPWhitelist.xml")]
        public ActionResult RenewAndroidInAppBilling(AndroidInAppRenewRequest request)
        {
            Exception thrownException = null;
            //TODO: Flag added for UPS Testing.  Remove before launch to prod. -arod
            try
            {
                if (request.IapStatus > -1)
                {
                    var iapStatus = (IAPControllerStatus) request.IapStatus;
                    return new NewtonsoftJsonResult {Result = iapStatus};
                }
            }
            catch (Exception e)
            {
                Log.LogError(e.Message, e, GetCustomDataForReportingErrors(), false);
            }


            var memberId = (request.MemberId <= 0) ? GetMemberId() : request.MemberId;
            try
            {
                var androidValidateResponse = PurchaseAccess.ValidateAndroidSubscription(request.Brand, request.SubscriptionToken, memberId);
                var iapStatus = androidValidateResponse.Status;
                //renewal processor should get back renewal status instead of purchase
                if (GetAppId == RENEWAL_PROCESSOR_APP)
                {
                    if (androidValidateResponse.Status == IAPControllerStatus.PurchaseOrder)
                    {
                        iapStatus = IAPControllerStatus.RenewOrder;
                    }
                }
                return new NewtonsoftJsonResult {Result = iapStatus};
            }
            catch (SparkAPIReportableException reportableException)
            {
                thrownException = reportableException;
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkHttpBadRequestResult((int)reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                thrownException = exception;
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkGenericHttpInternalServerErrorResult();
            }
            finally
            {
                if (null != thrownException)
                {
                    ActivityRecordingAccess.RecordLatestInAppBillingActivityException(memberId, request.Brand.BrandID, thrownException);
                }
            }
        }

        /// <summary>This call processes an android subscription.</summary>
        /// <param name="request">AndroidInAppBillingRequest</param>
        /// <param name="RegionId" required="false" values="">Use only for CM members</param>
        /// <example>
        ///     <code>http://api.local.spark.net/v2/brandId/1003/purchase/iab/process?access_token=ACCESS_TOKEN
        /// {
        ///     "subscriptionToken":[PackageName|SubscriptionId|TokenId],
        ///     "orderAttributeXML":[order attributes]
        /// }
        /// </code>
        /// </example>
        /// <remarks>
        ///     returns value from Enum
        ///     <code>IAPControllerStatus
        ///     {
        ///         None = 0,
        ///         PurchaseOrder = 1,
        ///         RenewOrder = 2,
        ///         TerminateRenewal = 3,
        ///         VoidPreviousOrder = 4
        ///     }</code>
        /// </remarks>
        [ApiMethod(ResponseType = typeof(IAPControllerStatus))]
        [HttpPost]
        [RequireAccessTokenV2]
        public ActionResult ProcessAndroidInAppBilling(AndroidInAppBillingRequest request)
        {
            Exception thrownException = null;
            var memberId = GetMemberId();
            try
            {
                var iapControllerStatus = PurchaseAccess.ProcessAndroidOrder(request.Brand, request.SubscriptionToken, memberId, request.OrderAttributeXML, request.RegionId);
                return new NewtonsoftJsonResult {Result = iapControllerStatus};
            }
            catch (SparkAPIReportableException reportableException)
            {
                thrownException = reportableException;
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkHttpBadRequestResult((int)reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                thrownException = exception;
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkGenericHttpInternalServerErrorResult();
            }
            finally
            {
                if (null != thrownException)
                {
                    ActivityRecordingAccess.RecordLatestInAppBillingActivityException(memberId, request.BrandId, thrownException);
                }
            }
        }

        /// <summary>
        ///     This call will send subscription token to Google's api site, terminate auto renewal for the subscription with
        ///     Google, and return subscription details in json
        /// </summary>
        /// <param name="request">AndroidInAppValidateReceiptRequest</param>
        /// <example>
        ///     POST http://api.local.spark.net/v2/brandId/0/purchase/iab/receiptterminate?applicationId=1078&amp;
        ///     client_secret=grb312kMJnrK7cI4MWA111ul74MCpFqwbl%2BplYcChio%3D
        /// {
        ///     "subscriptionToken":[PackageName|SubscriptionId|TokenId],
        ///     "siteId":[site id]
        /// }
        /// </example>
        /// <remarks>
        ///     example return response
        ///     <code>
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data":    {
        ///       "Status": 0,
        ///       "ErrorReason": null,
        ///       "Kind": "androidpublisher#subscriptionPurchase",
        ///       "IsAutoRenew": false,
        ///       "ExpirationDate": "2015-01-04T16:09:23.6Z",
        ///       "StartDate": "2014-12-04T16:09:50.028Z",
        ///       "PackageName": "com.spark.christianmingle",
        ///       "SubscriptionId": "test_cm_1m_basic_32.99",
        ///       "TokenId": "nkcgoegnnedniofanifpllnk.AO-J1OwtfgBiS7RAh9iy9P8DVRTfUmNkH14bYEEpxvK79LxqC0PyGzqFTKaqQCD-AbSi0Ibc6HtR7IFDQanxBS5t236q4zfdG7z_fbqvMcsYgLufg_QA0Osu1erXav5m6gmUw8NGYAl_2Z83IHIXxSWQK4v-NlRWDw"
        ///    }
        /// }
        /// </code>
        /// </remarks>
        [ApiMethod(ResponseType = typeof(AndroidSubscription))]
        [HttpPost]
        [RequireClientCredentials]
        //        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/UPSTrustedClientIPWhitelist.xml")]
        public ActionResult TerminateAndroidSubscriptionRenewal(AndroidInAppValidateReceiptRequest request)
        {
            Exception thrownException = null;
            try
            {
                APIAdapter.RestStatus restStatus;
                var androidSubscription = PurchaseAccess.TerminateAndroidSubscriptionRenewal(request.SubscriptionToken, out restStatus, request.Brand.Site.SiteID);
                return new NewtonsoftJsonResult {Result = androidSubscription};
            }
            catch (SparkAPIReportableException reportableException)
            {
                thrownException = reportableException;
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkHttpBadRequestResult((int)reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                thrownException = exception;
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkGenericHttpInternalServerErrorResult();
            }
            finally
            {
                if (null != thrownException)
                {
                    ActivityRecordingAccess.RecordLatestInAppBillingActivityException(GetMemberId(), request.BrandId, thrownException);
                }
            }
        }

        /// <summary>
        ///     This call will send subscription token to Google's api site, void the subscription with Google, and return
        ///     subscription details in json
        /// </summary>
        /// <param name="request">AndroidInAppValidateReceiptRequest</param>
        /// <example>
        ///     POST http://api.local.spark.net/v2/brandId/0/purchase/iab/receiptvoid?applicationId=1078&amp;
        ///     client_secret=grb312kMJnrK7cI4MWA111ul74MCpFqwbl%2BplYcChio%3D
        /// {
        ///     "subscriptionToken":[PackageName|SubscriptionId|TokenId],
        ///     "siteId":[site id]
        /// }
        /// </example>
        /// <remarks>
        ///     example return response
        ///     <code>
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data":    {
        ///       "Status": 0,
        ///       "ErrorReason": null,
        ///       "Kind": "androidpublisher#subscriptionPurchase",
        ///       "IsAutoRenew": false,
        ///       "ExpirationDate": "2015-01-04T16:09:23.6Z",
        ///       "StartDate": "2014-12-04T16:09:50.028Z",
        ///       "PackageName": "com.spark.christianmingle",
        ///       "SubscriptionId": "test_cm_1m_basic_32.99",
        ///       "TokenId": "nkcgoegnnedniofanifpllnk.AO-J1OwtfgBiS7RAh9iy9P8DVRTfUmNkH14bYEEpxvK79LxqC0PyGzqFTKaqQCD-AbSi0Ibc6HtR7IFDQanxBS5t236q4zfdG7z_fbqvMcsYgLufg_QA0Osu1erXav5m6gmUw8NGYAl_2Z83IHIXxSWQK4v-NlRWDw"
        ///    }
        /// }
        /// </code>
        /// </remarks>
        [ApiMethod(ResponseType = typeof(AndroidSubscription))]
        [HttpPost]
        [RequireClientCredentials]
        //        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/UPSTrustedClientIPWhitelist.xml")]
        public ActionResult VoidAndroidSubscription(AndroidInAppValidateReceiptRequest request)
        {
            Exception thrownException = null;
            try
            {
                APIAdapter.RestStatus restStatus;
                var androidSubscription = PurchaseAccess.VoidAndroidSubscription(request.SubscriptionToken, out restStatus, request.Brand.Site.SiteID);
                return new NewtonsoftJsonResult {Result = androidSubscription};
            }
            catch (SparkAPIReportableException reportableException)
            {
                thrownException = reportableException;
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkHttpBadRequestResult((int)reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                thrownException = exception;
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkGenericHttpInternalServerErrorResult();
            }
            finally
            {
                if (null != thrownException)
                {
                    ActivityRecordingAccess.RecordLatestInAppBillingActivityException(GetMemberId(), request.BrandId, thrownException);
                }
            }
        }
    }
}