﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Matchnet.UserNotifications.ValueObjects;
using Spark.Rest.Authorization;
using Spark.Rest.BedrockPorts;
using Spark.Rest.Configuration;
using Spark.REST.Models.UserNotifications;
using Spark.Rest.Serialization;
using Spark.Rest.V2.DataAccess.MembersOnline;

namespace Spark.REST.Controllers
{
    public class UserNotificationController : SparkControllerBase
    {
        /// <summary>This call returns user notifications for logged in members.</summary>
        /// <example>GET http://api.local.spark.net/v2/brandId/1003/startNum/1/pageSize/25/usernotifications?applicationId=998&amp;access_token=ACCESS_TOKEN
        /// </example>
        /// <remarks>There may be a delay in recently added user notifications due to the levels of caching in the service layers.</remarks>
        /// <param name="request">UserNotificationsRequest for notifications params</param>
        /// <returns>list of user notifications for member</returns>
        /// <exception>In the case of communication with the distributed caching cluster being down, timeout exceptions could be thrown.</exception>
        [ApiMethod(ResponseType = typeof(NewtonsoftJsonResult))]
        [HttpGet]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetUserNotifications(UserNotificationsRequest request)
        {
            int memberId = GetMemberId();
            List<UserNotificationViewObject> userNotificationViewObjects = UserNotificationAccess.GetUserNotifications(memberId, request.Brand, request.StartNum, request.PageSize,
                request.RefreshDataPointCache);

            return new NewtonsoftJsonResult(){Result=userNotificationViewObjects};
        }

    }
}