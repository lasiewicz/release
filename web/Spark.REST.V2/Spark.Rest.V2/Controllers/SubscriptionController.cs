﻿#region

using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Spark.Common;
using Spark.Common.AccessService;
using Spark.Common.Adapter;
using Spark.Logger;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.DataAccess.Purchase;
using Spark.Rest.Helpers;
using Spark.Rest.Serialization;
using Spark.Rest.V2.DataAccess.MembersOnline;
using Spark.Rest.V2.DataAccess.Subscription;
using Spark.Rest.V2.DataAccess.Ups;
using Spark.Rest.V2.Entities.Subscription;
using Spark.Rest.V2.Models.Subscription;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.DataAccess.Subscription;
using Spark.REST.Entities.Subscription;
using Spark.REST.Helpers;
using Spark.REST.Models;
using Spark.REST.Models.Subscription;

#endregion

namespace Spark.REST.Controllers
{
    public class SubscriptionController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(SubscriptionController));
        /// <summary>
        /// Gets encrypted JSON data that can be used to redirect the user to the Payment UI for purchasing or changing billing information. 
        /// For puchasing, the API automatically determines the highest priority promo (and thus the plans) available to the user.
        /// The specific type of data returned will be based on the PaymentUiPostDataType parameter: subscription (default), upsale, premium services (a la carte), and billing information
        /// </summary>
        /// <example>
        /// 
        /// Example: FWS Subscribe (non-paying member wants to subscribe or paying member wants to upgrade subscription)
        /// This gets PUI data to Subscribe on FWS, it only differs from MOS based on the "PromoType" so that we can qualify the user for the appropriate promo.  
        /// MOS may also have different URLs for the various URL properties so that PUI can pass through or redirect appropriately afterwards.
        /// 
        /// POST http://api.spark.net/v2/brandId/1003/subscription/paymentuiredirectdata?access_token=
        /// 
        /// {
        ///    "CancelUrl": "http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?memberID=411",
        ///    "ReturnUrl": "http://www.jdate.com",
        ///    "ConfirmationUrl": "http://www.jdate.com/Applications/Subscription/SubscriptionConfirmation.aspx",
        ///    "DestinationUrl": "http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?memberID=411",
        ///    "PrtId": "10042",
        ///    "SrId": "411",
        ///    "clientip": "100.100.100.100",
        ///    "OmnitureVariablesJson": "{\"evar10\":\"value1\",\"evar11\":\"value2\"}",
        ///    "MemberLevelTrackingLastApplication": "1",
        ///    "MemberLevelTrackingIsMobileDevice": "false",
        ///    "MemberLevelTrackingIsTablet": "false",
        ///    "PaymentUiPostDataType": "1",
        ///    "PromoType": "0", //4: mobile, 0: member (used by FWS)
        ///    "PaymentType": "1" //1: credit card, 2: check
        ///}
        ///
        /// RESPONSE
        /// 
        /// {
        /// "code":200,
        /// "status":"OK",
        /// "data":{
        ///     "Json":"T/IHoEfVFW/aoJH8Urrk+TXjETByGRxgx8bt5zrMfyW3eF4gm2QKFlZeFm16ycV9sArq0PA3X7/peDFaDfI3JF+H2bmz9IBhFgDLpYEhWkeXxNCk1WmHfWvg/1lpqMaGV68KSkENyk6W56CTjwdH07KWdHuV9rgI/zbB2U+7SeA=YYck1rtkrun7t5rSV6+0pqgyaH1ESLfa4cM36WswJiirHxXpbXmCB3aKYKEoy6krLoni4RgglYQUm1vdiIsthAupz7puzaeQ3nXOhKxNgkVL2bWRAM3pzOU1ctX0spkHcQtwrcDZDGZXFf7Q4/wgLu8v9lEjm4PlDjfZRWKpzGM=nLZBFNiloab/Ma0/eNxCBDqImw1Skaqw5oY1vBbIjEf7acSGPYxEb8WJ1i+1u7ssB/pSBHIO9iOZq/Uq+82SWAtnkLxFH8o8csYKu7AtIZ8m2VfVpVuB/SVLKPC8Sru+IkdZNgZe+I1vZwKASRtGWeQ0xNvIFqi7jCpH3NNTW0s=f62zZRbx7NPy6JWNlEq2bZxHefR2D+2D3XvLgm9Vpyou1/2IuomxKHUxbyjDVx7WczPSQQKLMTfB+45L6UtduQwNUzfEK9j4jaQ5Kv//L0I5op2Q6tACK2CGh3C4NjsmnM3cLw7vUk7591hhFUmALEYuMZcnj2fXJFn7EEriRxU=FEVz1jmeRALp8sgfvV6yRiiAL0O7BL1w0nfZQPFTYB/ywy1/8KwhzPLCpkifeDHmFVfJOdODsGuR1CbOD8Tc2WnxkgqvvyvQf2YHNNdPtdAjTBil9Fh6vBmhZcYfLOBl94sLT+F28FSow1Y4SycDzaJzG8CmIApR0b4ydITbno4=kvTBEkqoPhrfTP2SFs178WRvuvB4woFz7hTdFtaVK1cZ10lrhJAnCirA/B5r1le64MkrHVc2FwcwTfU0NLK7Q2qwjpp21z0apvKfFfgB6TpbrKUFgCIjb5WHEj2mxvurQ9JJjJftBNWNT32O2XKxxvEnlXmT08QyKIjJdL87lNg=eocryWVlnNlUTGAwzeknZCYHdDHYkPx0xb3BkGrJq2sw9/njeD7CqozG4267xY06sLOy7ak4wwZ0t/4Xtvij5Zmncyl9nwJU09qz35nWDBFXxsJLO+qFJb2ZswJeTUujcGepGMyGBMse2BXUPqAIVCPb9G7dYacpKWCZ5rhVEBg=k6LaetgO4/L6pUdjIBLUD3YCa1KChUvvnldNEe7buM2aQptDHzSjIpoImq1i2ZLPC/SspeyUVoaPP7CnEkY786fFEyBbw43aqcOvkqBv/RHlr7yhCdTR6N8Mkm1inDXquyoHpwxeFjY+dtk6V931hWnuctUMNY5Lf+Hwl+qGeJA=nW0CW13YnOo1vEHq3ovDyw4ZyhXwBqgTmI8XIahxPoEVNFGVoBHxq3oTfZiZDV5jjHlL7naGVQ3cz8suUWcep5IRpa6bqFfekzOVNYg2aYiQnF3YjCgy89f/U2/eFIcdvZxRrZ280iIlBTBn5cNBua2YYGA/T+HEObHOhfqz7fU=AjvBJQYEeyMhJtK3eZiFBQ46PnQLdqB3hX4jipCVEmmeiFBur1eegtet3YQJWRKPrm5qWOsw9GFi8UvCR9ecK1Gj1SSvs4rJtpqYKlbBg02OFSQNAk/qZ3fOiO/gqorOaLpw491Qg/Hts7OKIfwQ4CoFsz0IdjNSSDSsFJRxDVc=RHRjL0155s6quCAJGMn8M6m4Hq+c0xqvnzUb1lhwPCVswLSIMbC1p51/6vaUWYYrdKcc5FYQCAmUxpZDv02otX3lrs2wKbbVt00xT4KL4RxGIaEfJa6bhR79SM1pCR13N+QlmF1WbIKQEIRfgY+RJIZ28v1nfZF1g1Q7kejiWJ8=Z+lbvqjM/9EqTshm+0svoxgPkvAK/bDa8PCflQwWGuIgECrJgpNxTbNzNMtdy9NWKIrsRlQWOOK6Y60CoaVbkmR9tIXyjf3wBijh9GG5TbWr3m88vrkyao0c9YnDJnoKgtcOpbcX6wmEGEVQvAeymxxHBr2sTCxKGBZp8Byq1Ko=dpTykOGYS8EUyHzC30wOYdxF+cte4KweWBMW+EG6BpB1kyT1UghkId7d9loX2RgJxzmNO7jJy+eaUeSmDiNQb4Qv4OT6WJa22BZTUIrUMeYH0mQx32UbDKUTWb7SBvqgqt1oSkxzvY4KaZmfy6boPywiiF3IUkP637v4Rad1AS8=pOZoOD5ZvOT4qkvaAzdO2zBXaydrKAFTnj0FPkVmxB9/A6KTw8wc8osn1l/SGs9ZbvEf4exen5kJUaqhWARAFkVzSeNtSRLt8BHBEzeaYpgc2uFIGpZxXofGxfeFniEWfd9mgmjQAmIrgUwEiSwQeqfZCNWnVN4aYvU/RBhvFzA=W7/8enPeZCa2l1XB2KrN0W7bFFRgr/X73ENpZyYqDhrptIJ89yGa1vr2jzFEjbwhyO/AQ/DB97uXGgBAVMsOdLWG6y5E0Rgkm0PKeGCqMidr0ggNeRX1K6mqmYq8z3CcXXZ5jgCGUnN0Q2QhThmCWJ+0BWRDNZOarCcpyJSXlOg=au8O8rWFgSFa1AY95+Jxpd4GgcZ4ow2phRhQ+xLvRIyMTiWIpJwxoK5y+02e/uEfVCaYfSMDOnqu3at5i/Nt5SVLVjdQlQQFTuigF+oMpwXGCStaH85dtyU9FOYE6cy3gmFtl0mqV3UKRj/Ng7nO+tN4QJ7ERfDIiMJBKbW1BRw=tFC4kqU4lDtjOx4J4MgqfKWmJhQgkNVa9yNYFRtv8mnmrq7wgoYMqpw5iXqi6L/kKtBAY6t3wOGWtoq/gD77FGOV0IqfrU+3L00m9vLpwJhXSXmsiG5bAahkZP7mJWX7hIIJZMh2FAZ+gT9Oyfy1ZpM6kcPQWn/nurqGqkwggZY=j1drTYPLZdzjDbQdoBN53owTEZhvOTl2wFC+zsogjGovRDqjvha/ebw5tn1ADWkgwIMSAePkUrMSmGaiMsP9yXj7vHbIsbCUQShvtlj/5+JltSbotsF0epNX0/vGvas2u9YVnyYbBcgdw1pnHZS3AgCAyv9d86gvjh/k/DYKRsQ=u5H62pCo8w2VmFLPF5d35IXNbTf5macXKCSIBex/cHWj7t+aopVijpLsdGd5WGy01XnNbjRy0ORQ4sYq8YbzHc7gQELfSvAT2ZSW7IUw5EkLI1XN1pPxpC71EHjHCvy3BRc9BpO/Bws3cByyGNCBErFuYUWqfa83nQT9S/XGDpE=U+Dqidq70v71FWv5bLny3tJnEoNV5bVCV7XIZtId8sViPoJKxp1HBIJwEp2j1Bu3EhDJm9cGu7ElvpoDCgqnn09WNoD8PG9SwgQwVO+5LVOqucwfn11s3t4DKugoKtbHnoLk3ejH+TQO6GLxR1CXZctR8BubGO5KYu7eVvHrZxw=TjyryhPCq3U/BKLByTYJ7G2pj3zXz8aUrAsLaFbw2cbLcknxHT/BrimgGG7NyUQJjkeWhDQkZLKENc4xls4gS0Nnb4cRllBT6CZt5qsN5jSqmfNjb3kSkTl8ASp73TX1F7Q8e/T7Kzy0JHdDNLXOg/BVk68DiifOlQjSk5VPMSI=cXIBElfFvrU7WswF+Ub87k/1N9NY+QKGms2JBa79Ef5LgODwJLUd3zLZfvPla3GOVZIgI2eKCpY6JMQsCRF+MCC7OnDP2gbb7NsSwiI+Ul3zwkNur4Di3WfAJnl9/ROWZtDiFOLMK3mb65AQyrVP4t0/qP3v1kXfZxbUUS0twpA=WmpKg4Laajiqj7xa+wJPWL3vIwRrUyTwdNXekykZ76Ru+dD2MyxxgDdmY7OW97P5VoZ5OU3JZ6na/gDcX3V8O1A1EqrX4OyzODVRJn3NI7AhdD2lzi/+4sLN64GoFAgRHRG9wDyOdVJbhWxrl/+BkDsPK6rVvSvVAgb8ixv+Xxs=VVD2SE0uu+Pnqwh0ks2A4vABFjqFEd0C8uuWHQ3eBMwtv5q+ULKOZwYQjnkbzxE4v48fOMJyKcs/IS48vxmwmQfr41sjl+ZxCPkKH0rmJcnF26iNZg/bRmrEV4ItBwgPF64RgW5wRFhJwUvSj84gIEieoli+eYaElf5YfftIINk=RSGLBQocOD2s3X/rY2RUjnCatDvzjuN75p37CSqjmY3iKwaqvFwM+6MVrwuhw7IsiZnoJ/b8zKJSCMcYQEzTkX/jA6xYRALbbBqSCui5ulHafDw2hb5yC0XoEkM+Ig0RUvcSrN79vM1HaRNzfJ9dUOw65Ms/6GbYO/TDDeNmhfQ=hvM1zBQFRgEDfkLLKnl799hAUREJHwcsFkUS5U77ZqgAHxRy+nZ4mydh/nfKiTXwGqqg30PK4z/bf3Ll2pOlIz/cVqTR8aP54OBFSyFuXzGgHM1ekx2ob4BgNfHTzRonHpL7KTkM2AXOhjfdbEHQiARKvTgHzi9hnNfKxOFvaSo=h78eZBuDw2PZuMVU8m6tatfTwhxbPRvDSrRPFlhbMHV/osMkAUjVouKJb356zNw5VHRyxIvsMDFbiuS0ww5NHqHmNpCKlqj3Q3SW4wA3DSV2Uhko4ibycLBt1oOjmIdgGfXZxZdUl0BV68MZxvR/uZCUaZLxkfuGYtJlQF2FvPk=gJumo6FQ09q3ln08PLJ92CHnaaTkygnKjj08qS6YPdbFJW2NfK2xNFWu28MJkrSZ4V5/tCnv8vehqYTljcxfDxzGZOqK3EEaDvcJ5joucMWNEhA+slHfqGcBweBxOEoW9T/SMY/YyBhxLISU6WmXZiTsOZZgtcB+BpWZSddsW3o=e+L83nT7Q93CtmruPLll2PjfF19LLJeimEbjC2SzPgM/OuilOaYYHw7weOzhRR9p21mWdOJ4DGcWDwL6bbZ9wUefvcl8f6+/cHgw17dLg39feasLa5sHxN1y3zUUL8Oy2LQteGd1xgMyPooe/EHuuafoka2m1PqYCTiMZ7zOXgw=NSO9Vs+0kTT6ZLgR5QfREOxdYQ+/pMir+61mzTv9KpKQY1g9PUqTBz7Hoo0Apy2Zf/Z0zO9wr114Hp7P2OlW7UE63d2qIKwuklC/VFaFPsIVWAgNkAoJz5kO1mT5KTksuSNskFf8t74hABM0OUl5/WV6NRkxBj1eQyR9V7qNQZc=xkbDzM/LvzuUiDOum60JeY/CDxgtYf7Ti5ghw2BuX+s/la62XNIRfvcVqNk0EwQbhUaR5wURmVMI+bxoFZYB0aouQEL4Kp6N3WcoaJxZ8eWRvTW412bC5Gz2keV4zoV/sGgQxRBel1KAEHq8nA+tUBYpqgewufD3UOP+LYoBcjw=R9Mm7BAian/XZA8jkabCLHppOkk27eEgsRZMJvc8/agP1lzbSZxB2B9zu2iGP/CgY7K9cnSLs0w148k8bnJioJK/N79FkaJOYxeXrwv1AVtbm+4j4B2vVG7Khqci/+CeoPEIayeuG4xUVG0MIHSiPJG+7mn4dXj2RzUdGqB7Lss=tau9Y32wLLP6MQhq4nU166qFFh1J4Q9UomQoqbVo+gVp8jTtU4HrmH0DxWr4QcDP2Ekkf/tczTSVlXixiJhfsfYS2I7K4W8vDxEwEl1b+M6gwSC9E+pNldStvsELJPrW8bNL/a+hq00cC1J0qUGY05+RyHDcbY39GoVCd/cW1iQ=WGOcwtfjbBW7ZqJsR1eaKfZGUrdInb6FoisWhkyS2SNVN9Qiekg2g4DIXmNn2/4q0N/FmUyoiQ7lI0Ayvlg4nMdnf7PK0DXxx2QvnMFR2ERAqUmTXKYehYtDSUYsTJfJWBF22qkHHAWTL5V2j1JEFtAt+WScijqvliGbPUXDp78=e+TpdisRtiVTPtuvqI9BnkG4p9nvObhcb4qO682qS4NgN9UEb7AGyVWc9kNb+PQJ1LEdoCbb0PmAlgYXbJqHZCeZuzZ35kNx/9MF0hCkTsDR/Gju7foYyGDKV6nASmWmHHR66fKB7/NPrFwljoNxI0uPSAY+jqdLTeBrTqu8+SM=lmY4b4w30KNT8xlDGNpdo7e5vb8unR0rq6H5+t9kjUXCRLxLDw8kjLv5A9fMvLRuemzBpHMnGmDxgfZmzIRnelpSMrCSP5rea9VYy4YNVIJo+0sebzak/E1JaHbJAi6C786HIU1WhcfIsyxyj6NuEtFpzpkkL82AvrgGzbok220=SyrQ1zFv7ise+FYoyWYFuJJifRSm4lxaQNIBS6yEJckX6qDU6Z/osRwp+OXHhfj31GapT5dlX+LrOvhH74eYrRehjMqKSWCV9R072RB4lmb13hE0e0WTJzrlkWWgGpps1SN2X3uOCGKYQWR5omKMVeHnvXIVFYBPDFdy/on6QmQ=qxNnsAQWrUiUgg9ujT6s48raaHEtJWo9cGNzYxnpCvEkMQORZj2Xbnn/i0+D32LSUnJyQgRqnGJFTl0Ug4UZXjISfZR81QWkYE8LiDjPAk2yPrtHT7KiDgEYdfgrrlaTdLyB4z+sA8r6OET/tOXAHwCVgiwkcGErOe8pelPCOOA=Xhmlz3SoorIz/u6jmM1I5A8Iq1j6Y6ld9AfUHhamcXR4ty9o0jQxQ8QocfNxiw8Zyu10v8zVMrCJlgbVb5eMd3fKdXkYp6yO2gk+EXQTwustvK8X+1c9KMDgw2T7uZkrJAvgk/mNK6ofpjcZ83U79iuJZ6GzzBpj4Oq9ezn8QM8=uJSFWFOG1ARuohBFSBRGFo7hJmaDdGRsXILxuiavlbwg1FwMXDCKJJI8xkrMG5UXirznUXYe++Tg/IUuhnUFKrTDy0tKDA3WsaDx/Gb/YqE+q4Hg8c3EvqiRwxyVh4Qrl5Nm7G5g0Cm90rMyDF5XZLaO2YRwquZuUAkV7Zjc9Wg=rJfVHVxoDvBb8N4NyuIwXhbQKMY5gb+ghSSNZ6mQH7tatpdUlfYwXCa10e30EmUK3XEZdmVfTNHFC+NYwz5xGA+uyDoVxMYPLPHqCNVjV+Osb4JEPUPwrnJPQ/hcE0PZFWABlL45xWnFyWf91795cevRj8N/CNyul7RW8GSkswM=IDlCVUmFhB6bQ2IYGBKLVn4lLualQjGqIOfRvK1CsB3oaP8s21MpNAByDj4e68zhiuOZ7SBPF8fi2uLniGHoNuzAfcWJU9zm8JwiZx+eiVMHZUrJ1H+2aEjLqN4UwEHfuwUrP116Dd57N1ZHyVu7OoMSxN4liiVHtGSzZsgFacs=JO4X20j6RnU4dlen4Wu0ADz8U7ZuQygBUIXDKtgS4h66hO6BpyIfQ0NE3ictDsIK/Lr9ayW5rzIfnkdrnRgCvti8PBEVJdnd/3lBH2lEQmu8Ws0EbRZ17+PuQFDXJSK55CjULoObEdRHP4nANBX5t16OnUDZyKLbGmzJF+A7VqU=mxv6z2CNLx8tnKlyb5/9Stt9EShIH6cUMFgN5FZh+zwrX0oYKVschGBvHU4ik2/08ZsrZxVh1WBK4bO9j0u7Vj7RWapqi80Nx+Hx2MMzdNudZS2JkPbyLcCthtSDZCBdBUwxcDquliDQ09vRyYYKuZY28edOjqWkp/tuuYalJ2Y=UukWnHoHEZuCW9vp+yoj9Vjbo7NuvrYbK13bOPM2Dlsc7qms4tF8u3J7kr2YTHMSQWSdn5XAxD8zk6vcF8shk+xu6GemhGi4gxkVxpe0eHmoNkXmrS1ZAbzhY/JjEJbaFTxDb1ET7yYd8P7yHUJ2Fy11No7IuVsSoU1I1vVZWfg=AxTNvPYxIV/V4gNruoZE0zANuOkKTJPyjG78oWFhoZkmSFvZCokRpyZtzS6Ex3K2ELZ+LwZg5oVzp81YCwdnJxtB1Zjf2LVQkWqpO8+2dDYGGNDv2AlFwFyQfFq7WZFdpxwdF6S8972yvAo4kNm8BhtelPhWQ1cMb0YIQ9MrXVA=OzhxDeLcrgRjMWk3mCTGbBGITluDYQgaibsjVeZl1hQ6S1AHAHSf3IZWfkkTTKMhVDe41rE5IVwLhzLM+pnO6Xp7DHZefJbd/r7Gd9tF1r1qs3GfS1AVwzKM+o604MFli9tkKKHncTqhM8br39FKX1AHCp3In9jMYtZGGxxBT/c=YdsOI6dMUHzMXr9YgWMRhYy4gLOWLz00zPMSd8aVXR++jN/YNs4aX+jF0uk2UNmF1lwYKNw1EF2toYubz/TF0z5uc15N4TFAsILWnr4omZyC5R7jOcVSlTgHKG3fYtnPewt3c46v/8WdmWJzo6aZDasmBFi4sgG15gboYx2fL9g=JCqftPiyCuCT9vGREUsyGqbcwKo8CjjrGlniu+07FBnpUzBO0/2uDis/HcVAvJtmPgsPZ7OCfqn+mx+d0rNX9+MnYsdEMiGFqU2yl7BxHyh7s+cZP1tRiskz1CuQxwAe90KGwy5Yb4jwXSQe2+JZcXWLXLi47oNUz1jXG5InM6Q=H3ISFCJuaHj4QAdyWZFbBQkdzD0BH0XPR7yDbEx3hfGkG2L8rJiWg1hB4N6cSSACScHyvxo84BhgkDFU/IIBsrSppznTt2Hd9vbw53KXfJfYIHFYXhhlB7lU/Pkt8VPpQlcW6YdpztjGjJoJHR8QtX48VQozcEWRQ4edyOqKRDU=nsdZViduOL8XR0d4QdyrRsHyzP9uXa5ToWymdZDfg80Q0DoaEkMz2PoWJu6eNXIsZEUbGlVh/qqF+Hfai4+Vk/qbz17IpyjTD3CnbCXSi/YKxsPb5wpzF2AbVzksJtTEKXs39ZtWyDOyOYw9YPUgVzuvDckRId+Th+/Ci5wJxzM=Y2MhLHUJFG9/wlRDgb3OT+IDYDiMg3U0aeFj3RLY4iGqI0vljjr6UdO94ozvCkBi/sjfQKamXvCE7Ru820Qf63dAxrgRDrB3TBVFRrfK/kDuqxkmgDeJB2xuG+fvLl8qJbWXHSYNWJz16uBcqWTH3BR5yrEVd7rkEANmZoBp1zw=YHjNrlxwMBMetLKahzfCCkqyLF5SrkXfx+er3gCFWoIS2McRl5IxxdPHnku7wpwzqQXryJKu+lF8nzr4BDDO5H3G62Y1qiO6ZCEpuSIctLvgnlMdWdMqyNtEIwt0HA5ON2Fq2dTutoZJ/KeO/cT3DZgMQ3nsiSZDKzv02KjFF0A=HN0Qg0HOAb9rGEwYx0kOIXtPHB6vvgOkrPeXgERxnc1UzvkQW7F+Da7KEdpCvaGCL0ZC/4yeI14FvHPrwD/XfH+sm8kVWZZmYSivfv1RpQZXq6BgN6YuyvSuCvCHxz7Q4P7r2P5o881YGAgIpBIgr2W6a7v9weRmNvXZZxFEMGA=F2ineK5rCsxGaKNWt/6a/MyMzPFQDyUnV7Vbbj72goubFK1ZFRU3o9Q4s2sYqrxMsBXpqGfAJTa2sBi/uTAh2lvrqXzgb291VTeJIauXYWHr+TiQfV4ZW+rf0QDR86SLLrrXtad1NOHv9RB9ja3BtHrMWWZmg6FZymE73q/T/9o=", 
        ///     "PaymentUiUrl":"https://secure.spark.net/jdatecom",
        ///     "GlobalLogId":1146715248
        ///  }
        /// }
        /// 
        /// 
        /// 
        /// Example: No packages (plans) available
        /// You\'ll get back a response of 400 and substatus code of 40102 if there are no packages found for subscription, upsale, or premium a la cartes
        /// 
        /// {
        ///     "code":400,
        ///     "status":"BadRequest",
        ///     "error":
        ///     {
        ///         "code":40102,
        ///         "message":"No subscription packages available. MemberID: 100067359, SiteID: 103"
        ///     }
        /// }
        /// 
        /// 
        /// 
        /// Example: IP blocked
        /// This endpoint participates in checking whether the Client IP provided has been blocked.  If so, it will return an unauthorized.
        /// 
        /// {
        ///     "code":401,
        ///     "status":"Unauthorized",
        ///     "error":
        ///     {
        ///         "code":41011,
        ///         "message":"IP Blocked. 100.100.100.100"
        ///     }
        /// }
        /// 
        /// 
        /// 
        /// Example: FWS Subscribe with specific Promo (non-paying member wants to subscribe or paying member wants to upgrade subscription)
        /// Here we'll pass in a specific PromoID that should be used to get the plans, but if it's an invalid promo, it will automatically look for next available promo.
        /// 
        /// POST http://api.spark.net/v2/brandId/1003/subscription/paymentuiredirectdata?access_token=
        /// 
        /// {
        ///    "CancelUrl": "http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?memberID=411",
        ///    "ReturnUrl": "http://www.jdate.com",
        ///    "ConfirmationUrl": "http://www.jdate.com/Applications/Subscription/SubscriptionConfirmation.aspx",
        ///    "DestinationUrl": "http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?memberID=411",
        ///    "PrtId": "10042",
        ///    "SrId": "411",
        ///    "clientip": "100.100.100.100",
        ///    "OmnitureVariablesJson": "{\"evar10\":\"value1\",\"evar11\":\"value2\"}",
        ///    "MemberLevelTrackingLastApplication": "1",
        ///    "MemberLevelTrackingIsMobileDevice": "true",
        ///    "MemberLevelTrackingIsTablet": "true",
        ///    "PaymentUiPostDataType": "1",
        ///    "PromoType": "0", //4: mobile, 0: member (used by FWS)
        ///    "PromoID": 28282
        ///}
        ///
        /// RESPONSE
        /// 
        /// {
        /// "code":200,
        /// "status":"OK",
        /// "data":{
        ///     "Json":"T/IHoEfVFW/aoJH8Urrk+TXjETByGRxgx8bt5zrMfyW3eF4gm2QKFlZeFm16ycV9sArq0PA3X7/peDFaDfI3JF+H2bmz9IBhFgDLpYEhWkeXxNCk1WmHfWvg/1lpqMaGV68KSkENyk6W56CTjwdH07KWdHuV9rgI/zbB2U+7SeA=YYck1rtkrun7t5rSV6+0pqgyaH1ESLfa4cM36WswJiirHxXpbXmCB3aKYKEoy6krLoni4RgglYQUm1vdiIsthAupz7puzaeQ3nXOhKxNgkVL2bWRAM3pzOU1ctX0spkHcQtwrcDZDGZXFf7Q4/wgLu8v9lEjm4PlDjfZRWKpzGM=nLZBFNiloab/Ma0/eNxCBDqImw1Skaqw5oY1vBbIjEf7acSGPYxEb8WJ1i+1u7ssB/pSBHIO9iOZq/Uq+82SWAtnkLxFH8o8csYKu7AtIZ8m2VfVpVuB/SVLKPC8Sru+IkdZNgZe+I1vZwKASRtGWeQ0xNvIFqi7jCpH3NNTW0s=f62zZRbx7NPy6JWNlEq2bZxHefR2D+2D3XvLgm9Vpyou1/2IuomxKHUxbyjDVx7WczPSQQKLMTfB+45L6UtduQwNUzfEK9j4jaQ5Kv//L0I5op2Q6tACK2CGh3C4NjsmnM3cLw7vUk7591hhFUmALEYuMZcnj2fXJFn7EEriRxU=FEVz1jmeRALp8sgfvV6yRiiAL0O7BL1w0nfZQPFTYB/ywy1/8KwhzPLCpkifeDHmFVfJOdODsGuR1CbOD8Tc2WnxkgqvvyvQf2YHNNdPtdAjTBil9Fh6vBmhZcYfLOBl94sLT+F28FSow1Y4SycDzaJzG8CmIApR0b4ydITbno4=kvTBEkqoPhrfTP2SFs178WRvuvB4woFz7hTdFtaVK1cZ10lrhJAnCirA/B5r1le64MkrHVc2FwcwTfU0NLK7Q2qwjpp21z0apvKfFfgB6TpbrKUFgCIjb5WHEj2mxvurQ9JJjJftBNWNT32O2XKxxvEnlXmT08QyKIjJdL87lNg=eocryWVlnNlUTGAwzeknZCYHdDHYkPx0xb3BkGrJq2sw9/njeD7CqozG4267xY06sLOy7ak4wwZ0t/4Xtvij5Zmncyl9nwJU09qz35nWDBFXxsJLO+qFJb2ZswJeTUujcGepGMyGBMse2BXUPqAIVCPb9G7dYacpKWCZ5rhVEBg=k6LaetgO4/L6pUdjIBLUD3YCa1KChUvvnldNEe7buM2aQptDHzSjIpoImq1i2ZLPC/SspeyUVoaPP7CnEkY786fFEyBbw43aqcOvkqBv/RHlr7yhCdTR6N8Mkm1inDXquyoHpwxeFjY+dtk6V931hWnuctUMNY5Lf+Hwl+qGeJA=nW0CW13YnOo1vEHq3ovDyw4ZyhXwBqgTmI8XIahxPoEVNFGVoBHxq3oTfZiZDV5jjHlL7naGVQ3cz8suUWcep5IRpa6bqFfekzOVNYg2aYiQnF3YjCgy89f/U2/eFIcdvZxRrZ280iIlBTBn5cNBua2YYGA/T+HEObHOhfqz7fU=AjvBJQYEeyMhJtK3eZiFBQ46PnQLdqB3hX4jipCVEmmeiFBur1eegtet3YQJWRKPrm5qWOsw9GFi8UvCR9ecK1Gj1SSvs4rJtpqYKlbBg02OFSQNAk/qZ3fOiO/gqorOaLpw491Qg/Hts7OKIfwQ4CoFsz0IdjNSSDSsFJRxDVc=RHRjL0155s6quCAJGMn8M6m4Hq+c0xqvnzUb1lhwPCVswLSIMbC1p51/6vaUWYYrdKcc5FYQCAmUxpZDv02otX3lrs2wKbbVt00xT4KL4RxGIaEfJa6bhR79SM1pCR13N+QlmF1WbIKQEIRfgY+RJIZ28v1nfZF1g1Q7kejiWJ8=Z+lbvqjM/9EqTshm+0svoxgPkvAK/bDa8PCflQwWGuIgECrJgpNxTbNzNMtdy9NWKIrsRlQWOOK6Y60CoaVbkmR9tIXyjf3wBijh9GG5TbWr3m88vrkyao0c9YnDJnoKgtcOpbcX6wmEGEVQvAeymxxHBr2sTCxKGBZp8Byq1Ko=dpTykOGYS8EUyHzC30wOYdxF+cte4KweWBMW+EG6BpB1kyT1UghkId7d9loX2RgJxzmNO7jJy+eaUeSmDiNQb4Qv4OT6WJa22BZTUIrUMeYH0mQx32UbDKUTWb7SBvqgqt1oSkxzvY4KaZmfy6boPywiiF3IUkP637v4Rad1AS8=pOZoOD5ZvOT4qkvaAzdO2zBXaydrKAFTnj0FPkVmxB9/A6KTw8wc8osn1l/SGs9ZbvEf4exen5kJUaqhWARAFkVzSeNtSRLt8BHBEzeaYpgc2uFIGpZxXofGxfeFniEWfd9mgmjQAmIrgUwEiSwQeqfZCNWnVN4aYvU/RBhvFzA=W7/8enPeZCa2l1XB2KrN0W7bFFRgr/X73ENpZyYqDhrptIJ89yGa1vr2jzFEjbwhyO/AQ/DB97uXGgBAVMsOdLWG6y5E0Rgkm0PKeGCqMidr0ggNeRX1K6mqmYq8z3CcXXZ5jgCGUnN0Q2QhThmCWJ+0BWRDNZOarCcpyJSXlOg=au8O8rWFgSFa1AY95+Jxpd4GgcZ4ow2phRhQ+xLvRIyMTiWIpJwxoK5y+02e/uEfVCaYfSMDOnqu3at5i/Nt5SVLVjdQlQQFTuigF+oMpwXGCStaH85dtyU9FOYE6cy3gmFtl0mqV3UKRj/Ng7nO+tN4QJ7ERfDIiMJBKbW1BRw=tFC4kqU4lDtjOx4J4MgqfKWmJhQgkNVa9yNYFRtv8mnmrq7wgoYMqpw5iXqi6L/kKtBAY6t3wOGWtoq/gD77FGOV0IqfrU+3L00m9vLpwJhXSXmsiG5bAahkZP7mJWX7hIIJZMh2FAZ+gT9Oyfy1ZpM6kcPQWn/nurqGqkwggZY=j1drTYPLZdzjDbQdoBN53owTEZhvOTl2wFC+zsogjGovRDqjvha/ebw5tn1ADWkgwIMSAePkUrMSmGaiMsP9yXj7vHbIsbCUQShvtlj/5+JltSbotsF0epNX0/vGvas2u9YVnyYbBcgdw1pnHZS3AgCAyv9d86gvjh/k/DYKRsQ=u5H62pCo8w2VmFLPF5d35IXNbTf5macXKCSIBex/cHWj7t+aopVijpLsdGd5WGy01XnNbjRy0ORQ4sYq8YbzHc7gQELfSvAT2ZSW7IUw5EkLI1XN1pPxpC71EHjHCvy3BRc9BpO/Bws3cByyGNCBErFuYUWqfa83nQT9S/XGDpE=U+Dqidq70v71FWv5bLny3tJnEoNV5bVCV7XIZtId8sViPoJKxp1HBIJwEp2j1Bu3EhDJm9cGu7ElvpoDCgqnn09WNoD8PG9SwgQwVO+5LVOqucwfn11s3t4DKugoKtbHnoLk3ejH+TQO6GLxR1CXZctR8BubGO5KYu7eVvHrZxw=TjyryhPCq3U/BKLByTYJ7G2pj3zXz8aUrAsLaFbw2cbLcknxHT/BrimgGG7NyUQJjkeWhDQkZLKENc4xls4gS0Nnb4cRllBT6CZt5qsN5jSqmfNjb3kSkTl8ASp73TX1F7Q8e/T7Kzy0JHdDNLXOg/BVk68DiifOlQjSk5VPMSI=cXIBElfFvrU7WswF+Ub87k/1N9NY+QKGms2JBa79Ef5LgODwJLUd3zLZfvPla3GOVZIgI2eKCpY6JMQsCRF+MCC7OnDP2gbb7NsSwiI+Ul3zwkNur4Di3WfAJnl9/ROWZtDiFOLMK3mb65AQyrVP4t0/qP3v1kXfZxbUUS0twpA=WmpKg4Laajiqj7xa+wJPWL3vIwRrUyTwdNXekykZ76Ru+dD2MyxxgDdmY7OW97P5VoZ5OU3JZ6na/gDcX3V8O1A1EqrX4OyzODVRJn3NI7AhdD2lzi/+4sLN64GoFAgRHRG9wDyOdVJbhWxrl/+BkDsPK6rVvSvVAgb8ixv+Xxs=VVD2SE0uu+Pnqwh0ks2A4vABFjqFEd0C8uuWHQ3eBMwtv5q+ULKOZwYQjnkbzxE4v48fOMJyKcs/IS48vxmwmQfr41sjl+ZxCPkKH0rmJcnF26iNZg/bRmrEV4ItBwgPF64RgW5wRFhJwUvSj84gIEieoli+eYaElf5YfftIINk=RSGLBQocOD2s3X/rY2RUjnCatDvzjuN75p37CSqjmY3iKwaqvFwM+6MVrwuhw7IsiZnoJ/b8zKJSCMcYQEzTkX/jA6xYRALbbBqSCui5ulHafDw2hb5yC0XoEkM+Ig0RUvcSrN79vM1HaRNzfJ9dUOw65Ms/6GbYO/TDDeNmhfQ=hvM1zBQFRgEDfkLLKnl799hAUREJHwcsFkUS5U77ZqgAHxRy+nZ4mydh/nfKiTXwGqqg30PK4z/bf3Ll2pOlIz/cVqTR8aP54OBFSyFuXzGgHM1ekx2ob4BgNfHTzRonHpL7KTkM2AXOhjfdbEHQiARKvTgHzi9hnNfKxOFvaSo=h78eZBuDw2PZuMVU8m6tatfTwhxbPRvDSrRPFlhbMHV/osMkAUjVouKJb356zNw5VHRyxIvsMDFbiuS0ww5NHqHmNpCKlqj3Q3SW4wA3DSV2Uhko4ibycLBt1oOjmIdgGfXZxZdUl0BV68MZxvR/uZCUaZLxkfuGYtJlQF2FvPk=gJumo6FQ09q3ln08PLJ92CHnaaTkygnKjj08qS6YPdbFJW2NfK2xNFWu28MJkrSZ4V5/tCnv8vehqYTljcxfDxzGZOqK3EEaDvcJ5joucMWNEhA+slHfqGcBweBxOEoW9T/SMY/YyBhxLISU6WmXZiTsOZZgtcB+BpWZSddsW3o=e+L83nT7Q93CtmruPLll2PjfF19LLJeimEbjC2SzPgM/OuilOaYYHw7weOzhRR9p21mWdOJ4DGcWDwL6bbZ9wUefvcl8f6+/cHgw17dLg39feasLa5sHxN1y3zUUL8Oy2LQteGd1xgMyPooe/EHuuafoka2m1PqYCTiMZ7zOXgw=NSO9Vs+0kTT6ZLgR5QfREOxdYQ+/pMir+61mzTv9KpKQY1g9PUqTBz7Hoo0Apy2Zf/Z0zO9wr114Hp7P2OlW7UE63d2qIKwuklC/VFaFPsIVWAgNkAoJz5kO1mT5KTksuSNskFf8t74hABM0OUl5/WV6NRkxBj1eQyR9V7qNQZc=xkbDzM/LvzuUiDOum60JeY/CDxgtYf7Ti5ghw2BuX+s/la62XNIRfvcVqNk0EwQbhUaR5wURmVMI+bxoFZYB0aouQEL4Kp6N3WcoaJxZ8eWRvTW412bC5Gz2keV4zoV/sGgQxRBel1KAEHq8nA+tUBYpqgewufD3UOP+LYoBcjw=R9Mm7BAian/XZA8jkabCLHppOkk27eEgsRZMJvc8/agP1lzbSZxB2B9zu2iGP/CgY7K9cnSLs0w148k8bnJioJK/N79FkaJOYxeXrwv1AVtbm+4j4B2vVG7Khqci/+CeoPEIayeuG4xUVG0MIHSiPJG+7mn4dXj2RzUdGqB7Lss=tau9Y32wLLP6MQhq4nU166qFFh1J4Q9UomQoqbVo+gVp8jTtU4HrmH0DxWr4QcDP2Ekkf/tczTSVlXixiJhfsfYS2I7K4W8vDxEwEl1b+M6gwSC9E+pNldStvsELJPrW8bNL/a+hq00cC1J0qUGY05+RyHDcbY39GoVCd/cW1iQ=WGOcwtfjbBW7ZqJsR1eaKfZGUrdInb6FoisWhkyS2SNVN9Qiekg2g4DIXmNn2/4q0N/FmUyoiQ7lI0Ayvlg4nMdnf7PK0DXxx2QvnMFR2ERAqUmTXKYehYtDSUYsTJfJWBF22qkHHAWTL5V2j1JEFtAt+WScijqvliGbPUXDp78=e+TpdisRtiVTPtuvqI9BnkG4p9nvObhcb4qO682qS4NgN9UEb7AGyVWc9kNb+PQJ1LEdoCbb0PmAlgYXbJqHZCeZuzZ35kNx/9MF0hCkTsDR/Gju7foYyGDKV6nASmWmHHR66fKB7/NPrFwljoNxI0uPSAY+jqdLTeBrTqu8+SM=lmY4b4w30KNT8xlDGNpdo7e5vb8unR0rq6H5+t9kjUXCRLxLDw8kjLv5A9fMvLRuemzBpHMnGmDxgfZmzIRnelpSMrCSP5rea9VYy4YNVIJo+0sebzak/E1JaHbJAi6C786HIU1WhcfIsyxyj6NuEtFpzpkkL82AvrgGzbok220=SyrQ1zFv7ise+FYoyWYFuJJifRSm4lxaQNIBS6yEJckX6qDU6Z/osRwp+OXHhfj31GapT5dlX+LrOvhH74eYrRehjMqKSWCV9R072RB4lmb13hE0e0WTJzrlkWWgGpps1SN2X3uOCGKYQWR5omKMVeHnvXIVFYBPDFdy/on6QmQ=qxNnsAQWrUiUgg9ujT6s48raaHEtJWo9cGNzYxnpCvEkMQORZj2Xbnn/i0+D32LSUnJyQgRqnGJFTl0Ug4UZXjISfZR81QWkYE8LiDjPAk2yPrtHT7KiDgEYdfgrrlaTdLyB4z+sA8r6OET/tOXAHwCVgiwkcGErOe8pelPCOOA=Xhmlz3SoorIz/u6jmM1I5A8Iq1j6Y6ld9AfUHhamcXR4ty9o0jQxQ8QocfNxiw8Zyu10v8zVMrCJlgbVb5eMd3fKdXkYp6yO2gk+EXQTwustvK8X+1c9KMDgw2T7uZkrJAvgk/mNK6ofpjcZ83U79iuJZ6GzzBpj4Oq9ezn8QM8=uJSFWFOG1ARuohBFSBRGFo7hJmaDdGRsXILxuiavlbwg1FwMXDCKJJI8xkrMG5UXirznUXYe++Tg/IUuhnUFKrTDy0tKDA3WsaDx/Gb/YqE+q4Hg8c3EvqiRwxyVh4Qrl5Nm7G5g0Cm90rMyDF5XZLaO2YRwquZuUAkV7Zjc9Wg=rJfVHVxoDvBb8N4NyuIwXhbQKMY5gb+ghSSNZ6mQH7tatpdUlfYwXCa10e30EmUK3XEZdmVfTNHFC+NYwz5xGA+uyDoVxMYPLPHqCNVjV+Osb4JEPUPwrnJPQ/hcE0PZFWABlL45xWnFyWf91795cevRj8N/CNyul7RW8GSkswM=IDlCVUmFhB6bQ2IYGBKLVn4lLualQjGqIOfRvK1CsB3oaP8s21MpNAByDj4e68zhiuOZ7SBPF8fi2uLniGHoNuzAfcWJU9zm8JwiZx+eiVMHZUrJ1H+2aEjLqN4UwEHfuwUrP116Dd57N1ZHyVu7OoMSxN4liiVHtGSzZsgFacs=JO4X20j6RnU4dlen4Wu0ADz8U7ZuQygBUIXDKtgS4h66hO6BpyIfQ0NE3ictDsIK/Lr9ayW5rzIfnkdrnRgCvti8PBEVJdnd/3lBH2lEQmu8Ws0EbRZ17+PuQFDXJSK55CjULoObEdRHP4nANBX5t16OnUDZyKLbGmzJF+A7VqU=mxv6z2CNLx8tnKlyb5/9Stt9EShIH6cUMFgN5FZh+zwrX0oYKVschGBvHU4ik2/08ZsrZxVh1WBK4bO9j0u7Vj7RWapqi80Nx+Hx2MMzdNudZS2JkPbyLcCthtSDZCBdBUwxcDquliDQ09vRyYYKuZY28edOjqWkp/tuuYalJ2Y=UukWnHoHEZuCW9vp+yoj9Vjbo7NuvrYbK13bOPM2Dlsc7qms4tF8u3J7kr2YTHMSQWSdn5XAxD8zk6vcF8shk+xu6GemhGi4gxkVxpe0eHmoNkXmrS1ZAbzhY/JjEJbaFTxDb1ET7yYd8P7yHUJ2Fy11No7IuVsSoU1I1vVZWfg=AxTNvPYxIV/V4gNruoZE0zANuOkKTJPyjG78oWFhoZkmSFvZCokRpyZtzS6Ex3K2ELZ+LwZg5oVzp81YCwdnJxtB1Zjf2LVQkWqpO8+2dDYGGNDv2AlFwFyQfFq7WZFdpxwdF6S8972yvAo4kNm8BhtelPhWQ1cMb0YIQ9MrXVA=OzhxDeLcrgRjMWk3mCTGbBGITluDYQgaibsjVeZl1hQ6S1AHAHSf3IZWfkkTTKMhVDe41rE5IVwLhzLM+pnO6Xp7DHZefJbd/r7Gd9tF1r1qs3GfS1AVwzKM+o604MFli9tkKKHncTqhM8br39FKX1AHCp3In9jMYtZGGxxBT/c=YdsOI6dMUHzMXr9YgWMRhYy4gLOWLz00zPMSd8aVXR++jN/YNs4aX+jF0uk2UNmF1lwYKNw1EF2toYubz/TF0z5uc15N4TFAsILWnr4omZyC5R7jOcVSlTgHKG3fYtnPewt3c46v/8WdmWJzo6aZDasmBFi4sgG15gboYx2fL9g=JCqftPiyCuCT9vGREUsyGqbcwKo8CjjrGlniu+07FBnpUzBO0/2uDis/HcVAvJtmPgsPZ7OCfqn+mx+d0rNX9+MnYsdEMiGFqU2yl7BxHyh7s+cZP1tRiskz1CuQxwAe90KGwy5Yb4jwXSQe2+JZcXWLXLi47oNUz1jXG5InM6Q=H3ISFCJuaHj4QAdyWZFbBQkdzD0BH0XPR7yDbEx3hfGkG2L8rJiWg1hB4N6cSSACScHyvxo84BhgkDFU/IIBsrSppznTt2Hd9vbw53KXfJfYIHFYXhhlB7lU/Pkt8VPpQlcW6YdpztjGjJoJHR8QtX48VQozcEWRQ4edyOqKRDU=nsdZViduOL8XR0d4QdyrRsHyzP9uXa5ToWymdZDfg80Q0DoaEkMz2PoWJu6eNXIsZEUbGlVh/qqF+Hfai4+Vk/qbz17IpyjTD3CnbCXSi/YKxsPb5wpzF2AbVzksJtTEKXs39ZtWyDOyOYw9YPUgVzuvDckRId+Th+/Ci5wJxzM=Y2MhLHUJFG9/wlRDgb3OT+IDYDiMg3U0aeFj3RLY4iGqI0vljjr6UdO94ozvCkBi/sjfQKamXvCE7Ru820Qf63dAxrgRDrB3TBVFRrfK/kDuqxkmgDeJB2xuG+fvLl8qJbWXHSYNWJz16uBcqWTH3BR5yrEVd7rkEANmZoBp1zw=YHjNrlxwMBMetLKahzfCCkqyLF5SrkXfx+er3gCFWoIS2McRl5IxxdPHnku7wpwzqQXryJKu+lF8nzr4BDDO5H3G62Y1qiO6ZCEpuSIctLvgnlMdWdMqyNtEIwt0HA5ON2Fq2dTutoZJ/KeO/cT3DZgMQ3nsiSZDKzv02KjFF0A=HN0Qg0HOAb9rGEwYx0kOIXtPHB6vvgOkrPeXgERxnc1UzvkQW7F+Da7KEdpCvaGCL0ZC/4yeI14FvHPrwD/XfH+sm8kVWZZmYSivfv1RpQZXq6BgN6YuyvSuCvCHxz7Q4P7r2P5o881YGAgIpBIgr2W6a7v9weRmNvXZZxFEMGA=F2ineK5rCsxGaKNWt/6a/MyMzPFQDyUnV7Vbbj72goubFK1ZFRU3o9Q4s2sYqrxMsBXpqGfAJTa2sBi/uTAh2lvrqXzgb291VTeJIauXYWHr+TiQfV4ZW+rf0QDR86SLLrrXtad1NOHv9RB9ja3BtHrMWWZmg6FZymE73q/T/9o=", 
        ///     "PaymentUiUrl":"https://secure.spark.net/jdatecom",
        ///     "GlobalLogId":1146715248
        ///  }
        /// }
        /// 
        /// 
        /// 
        /// Example: Trigger Upsale All Access Email (member is already an All Access Subscriber)
        /// This example, the member is already an all access subscriber, and we want to upsale them more all access email.  
        /// This will get fixed price All Access Email plans.
        /// The key here is to specify All Access Email as the PremiumType we want.
        /// 
        /// {
        ///    "CancelUrl": "http://www.jdate.com/Applications/MemberServices/MemberServices.aspx",
        ///    "ReturnUrl": "http://www.jdate.com",
        ///    "ConfirmationUrl": "http://www.jdate.com/Applications/Subscription/SubscriptionConfirmation.aspx",
        ///    "DestinationUrl": "http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?memberID=411",
        ///    "PrtId": "10042",
        ///    "SrId": "411",
        ///    "clientip": "100.100.100.100",
        ///    "OmnitureVariablesJson": "{\"evar10\":\"value1\",\"evar11\":\"value2\"}",
        ///    "MemberLevelTrackingLastApplication": "1",
        ///    "MemberLevelTrackingIsMobileDevice": "false",
        ///    "MemberLevelTrackingIsTablet": "false",
        ///    "PaymentUiPostDataType": "2", //upsale
        ///    "PromoType": "0", //4: mobile, 0: member (used by FWS)
        ///    "PaymentType": "1", //1: credit card, 2: check
        ///    "PremiumType": "64", //all access email only
        ///    "TemplateID": "110"
        ///}
        ///
        /// RESPONSE
        /// 
        /// {
        /// "code":200,
        /// "status":"OK",
        /// "data":{
        ///     "Json":"T/IHoEfVFW/aoJH8Urrk+TXjETByGRxgx8bt5zrMfyW3eF4gm2QKFlZeFm16ycV9sArq0PA3X7/peDFaDfI3JF+H2bmz9IBhFgDLpYEhWkeXxNCk1WmHfWvg/1lpqMaGV68KSkENyk6W56CTjwdH07KWdHuV9rgI/zbB2U+7SeA=YYck1rtkrun7t5rSV6+0pqgyaH1ESLfa4cM36WswJiirHxXpbXmCB3aKYKEoy6krLoni4RgglYQUm1vdiIsthAupz7puzaeQ3nXOhKxNgkVL2bWRAM3pzOU1ctX0spkHcQtwrcDZDGZXFf7Q4/wgLu8v9lEjm4PlDjfZRWKpzGM=nLZBFNiloab/Ma0/eNxCBDqImw1Skaqw5oY1vBbIjEf7acSGPYxEb8WJ1i+1u7ssB/pSBHIO9iOZq/Uq+82SWAtnkLxFH8o8csYKu7AtIZ8m2VfVpVuB/SVLKPC8Sru+IkdZNgZe+I1vZwKASRtGWeQ0xNvIFqi7jCpH3NNTW0s=f62zZRbx7NPy6JWNlEq2bZxHefR2D+2D3XvLgm9Vpyou1/2IuomxKHUxbyjDVx7WczPSQQKLMTfB+45L6UtduQwNUzfEK9j4jaQ5Kv//L0I5op2Q6tACK2CGh3C4NjsmnM3cLw7vUk7591hhFUmALEYuMZcnj2fXJFn7EEriRxU=FEVz1jmeRALp8sgfvV6yRiiAL0O7BL1w0nfZQPFTYB/ywy1/8KwhzPLCpkifeDHmFVfJOdODsGuR1CbOD8Tc2WnxkgqvvyvQf2YHNNdPtdAjTBil9Fh6vBmhZcYfLOBl94sLT+F28FSow1Y4SycDzaJzG8CmIApR0b4ydITbno4=kvTBEkqoPhrfTP2SFs178WRvuvB4woFz7hTdFtaVK1cZ10lrhJAnCirA/B5r1le64MkrHVc2FwcwTfU0NLK7Q2qwjpp21z0apvKfFfgB6TpbrKUFgCIjb5WHEj2mxvurQ9JJjJftBNWNT32O2XKxxvEnlXmT08QyKIjJdL87lNg=eocryWVlnNlUTGAwzeknZCYHdDHYkPx0xb3BkGrJq2sw9/njeD7CqozG4267xY06sLOy7ak4wwZ0t/4Xtvij5Zmncyl9nwJU09qz35nWDBFXxsJLO+qFJb2ZswJeTUujcGepGMyGBMse2BXUPqAIVCPb9G7dYacpKWCZ5rhVEBg=k6LaetgO4/L6pUdjIBLUD3YCa1KChUvvnldNEe7buM2aQptDHzSjIpoImq1i2ZLPC/SspeyUVoaPP7CnEkY786fFEyBbw43aqcOvkqBv/RHlr7yhCdTR6N8Mkm1inDXquyoHpwxeFjY+dtk6V931hWnuctUMNY5Lf+Hwl+qGeJA=nW0CW13YnOo1vEHq3ovDyw4ZyhXwBqgTmI8XIahxPoEVNFGVoBHxq3oTfZiZDV5jjHlL7naGVQ3cz8suUWcep5IRpa6bqFfekzOVNYg2aYiQnF3YjCgy89f/U2/eFIcdvZxRrZ280iIlBTBn5cNBua2YYGA/T+HEObHOhfqz7fU=AjvBJQYEeyMhJtK3eZiFBQ46PnQLdqB3hX4jipCVEmmeiFBur1eegtet3YQJWRKPrm5qWOsw9GFi8UvCR9ecK1Gj1SSvs4rJtpqYKlbBg02OFSQNAk/qZ3fOiO/gqorOaLpw491Qg/Hts7OKIfwQ4CoFsz0IdjNSSDSsFJRxDVc=RHRjL0155s6quCAJGMn8M6m4Hq+c0xqvnzUb1lhwPCVswLSIMbC1p51/6vaUWYYrdKcc5FYQCAmUxpZDv02otX3lrs2wKbbVt00xT4KL4RxGIaEfJa6bhR79SM1pCR13N+QlmF1WbIKQEIRfgY+RJIZ28v1nfZF1g1Q7kejiWJ8=Z+lbvqjM/9EqTshm+0svoxgPkvAK/bDa8PCflQwWGuIgECrJgpNxTbNzNMtdy9NWKIrsRlQWOOK6Y60CoaVbkmR9tIXyjf3wBijh9GG5TbWr3m88vrkyao0c9YnDJnoKgtcOpbcX6wmEGEVQvAeymxxHBr2sTCxKGBZp8Byq1Ko=dpTykOGYS8EUyHzC30wOYdxF+cte4KweWBMW+EG6BpB1kyT1UghkId7d9loX2RgJxzmNO7jJy+eaUeSmDiNQb4Qv4OT6WJa22BZTUIrUMeYH0mQx32UbDKUTWb7SBvqgqt1oSkxzvY4KaZmfy6boPywiiF3IUkP637v4Rad1AS8=pOZoOD5ZvOT4qkvaAzdO2zBXaydrKAFTnj0FPkVmxB9/A6KTw8wc8osn1l/SGs9ZbvEf4exen5kJUaqhWARAFkVzSeNtSRLt8BHBEzeaYpgc2uFIGpZxXofGxfeFniEWfd9mgmjQAmIrgUwEiSwQeqfZCNWnVN4aYvU/RBhvFzA=W7/8enPeZCa2l1XB2KrN0W7bFFRgr/X73ENpZyYqDhrptIJ89yGa1vr2jzFEjbwhyO/AQ/DB97uXGgBAVMsOdLWG6y5E0Rgkm0PKeGCqMidr0ggNeRX1K6mqmYq8z3CcXXZ5jgCGUnN0Q2QhThmCWJ+0BWRDNZOarCcpyJSXlOg=au8O8rWFgSFa1AY95+Jxpd4GgcZ4ow2phRhQ+xLvRIyMTiWIpJwxoK5y+02e/uEfVCaYfSMDOnqu3at5i/Nt5SVLVjdQlQQFTuigF+oMpwXGCStaH85dtyU9FOYE6cy3gmFtl0mqV3UKRj/Ng7nO+tN4QJ7ERfDIiMJBKbW1BRw=tFC4kqU4lDtjOx4J4MgqfKWmJhQgkNVa9yNYFRtv8mnmrq7wgoYMqpw5iXqi6L/kKtBAY6t3wOGWtoq/gD77FGOV0IqfrU+3L00m9vLpwJhXSXmsiG5bAahkZP7mJWX7hIIJZMh2FAZ+gT9Oyfy1ZpM6kcPQWn/nurqGqkwggZY=j1drTYPLZdzjDbQdoBN53owTEZhvOTl2wFC+zsogjGovRDqjvha/ebw5tn1ADWkgwIMSAePkUrMSmGaiMsP9yXj7vHbIsbCUQShvtlj/5+JltSbotsF0epNX0/vGvas2u9YVnyYbBcgdw1pnHZS3AgCAyv9d86gvjh/k/DYKRsQ=u5H62pCo8w2VmFLPF5d35IXNbTf5macXKCSIBex/cHWj7t+aopVijpLsdGd5WGy01XnNbjRy0ORQ4sYq8YbzHc7gQELfSvAT2ZSW7IUw5EkLI1XN1pPxpC71EHjHCvy3BRc9BpO/Bws3cByyGNCBErFuYUWqfa83nQT9S/XGDpE=U+Dqidq70v71FWv5bLny3tJnEoNV5bVCV7XIZtId8sViPoJKxp1HBIJwEp2j1Bu3EhDJm9cGu7ElvpoDCgqnn09WNoD8PG9SwgQwVO+5LVOqucwfn11s3t4DKugoKtbHnoLk3ejH+TQO6GLxR1CXZctR8BubGO5KYu7eVvHrZxw=TjyryhPCq3U/BKLByTYJ7G2pj3zXz8aUrAsLaFbw2cbLcknxHT/BrimgGG7NyUQJjkeWhDQkZLKENc4xls4gS0Nnb4cRllBT6CZt5qsN5jSqmfNjb3kSkTl8ASp73TX1F7Q8e/T7Kzy0JHdDNLXOg/BVk68DiifOlQjSk5VPMSI=cXIBElfFvrU7WswF+Ub87k/1N9NY+QKGms2JBa79Ef5LgODwJLUd3zLZfvPla3GOVZIgI2eKCpY6JMQsCRF+MCC7OnDP2gbb7NsSwiI+Ul3zwkNur4Di3WfAJnl9/ROWZtDiFOLMK3mb65AQyrVP4t0/qP3v1kXfZxbUUS0twpA=WmpKg4Laajiqj7xa+wJPWL3vIwRrUyTwdNXekykZ76Ru+dD2MyxxgDdmY7OW97P5VoZ5OU3JZ6na/gDcX3V8O1A1EqrX4OyzODVRJn3NI7AhdD2lzi/+4sLN64GoFAgRHRG9wDyOdVJbhWxrl/+BkDsPK6rVvSvVAgb8ixv+Xxs=VVD2SE0uu+Pnqwh0ks2A4vABFjqFEd0C8uuWHQ3eBMwtv5q+ULKOZwYQjnkbzxE4v48fOMJyKcs/IS48vxmwmQfr41sjl+ZxCPkKH0rmJcnF26iNZg/bRmrEV4ItBwgPF64RgW5wRFhJwUvSj84gIEieoli+eYaElf5YfftIINk=RSGLBQocOD2s3X/rY2RUjnCatDvzjuN75p37CSqjmY3iKwaqvFwM+6MVrwuhw7IsiZnoJ/b8zKJSCMcYQEzTkX/jA6xYRALbbBqSCui5ulHafDw2hb5yC0XoEkM+Ig0RUvcSrN79vM1HaRNzfJ9dUOw65Ms/6GbYO/TDDeNmhfQ=hvM1zBQFRgEDfkLLKnl799hAUREJHwcsFkUS5U77ZqgAHxRy+nZ4mydh/nfKiTXwGqqg30PK4z/bf3Ll2pOlIz/cVqTR8aP54OBFSyFuXzGgHM1ekx2ob4BgNfHTzRonHpL7KTkM2AXOhjfdbEHQiARKvTgHzi9hnNfKxOFvaSo=h78eZBuDw2PZuMVU8m6tatfTwhxbPRvDSrRPFlhbMHV/osMkAUjVouKJb356zNw5VHRyxIvsMDFbiuS0ww5NHqHmNpCKlqj3Q3SW4wA3DSV2Uhko4ibycLBt1oOjmIdgGfXZxZdUl0BV68MZxvR/uZCUaZLxkfuGYtJlQF2FvPk=gJumo6FQ09q3ln08PLJ92CHnaaTkygnKjj08qS6YPdbFJW2NfK2xNFWu28MJkrSZ4V5/tCnv8vehqYTljcxfDxzGZOqK3EEaDvcJ5joucMWNEhA+slHfqGcBweBxOEoW9T/SMY/YyBhxLISU6WmXZiTsOZZgtcB+BpWZSddsW3o=e+L83nT7Q93CtmruPLll2PjfF19LLJeimEbjC2SzPgM/OuilOaYYHw7weOzhRR9p21mWdOJ4DGcWDwL6bbZ9wUefvcl8f6+/cHgw17dLg39feasLa5sHxN1y3zUUL8Oy2LQteGd1xgMyPooe/EHuuafoka2m1PqYCTiMZ7zOXgw=NSO9Vs+0kTT6ZLgR5QfREOxdYQ+/pMir+61mzTv9KpKQY1g9PUqTBz7Hoo0Apy2Zf/Z0zO9wr114Hp7P2OlW7UE63d2qIKwuklC/VFaFPsIVWAgNkAoJz5kO1mT5KTksuSNskFf8t74hABM0OUl5/WV6NRkxBj1eQyR9V7qNQZc=xkbDzM/LvzuUiDOum60JeY/CDxgtYf7Ti5ghw2BuX+s/la62XNIRfvcVqNk0EwQbhUaR5wURmVMI+bxoFZYB0aouQEL4Kp6N3WcoaJxZ8eWRvTW412bC5Gz2keV4zoV/sGgQxRBel1KAEHq8nA+tUBYpqgewufD3UOP+LYoBcjw=R9Mm7BAian/XZA8jkabCLHppOkk27eEgsRZMJvc8/agP1lzbSZxB2B9zu2iGP/CgY7K9cnSLs0w148k8bnJioJK/N79FkaJOYxeXrwv1AVtbm+4j4B2vVG7Khqci/+CeoPEIayeuG4xUVG0MIHSiPJG+7mn4dXj2RzUdGqB7Lss=tau9Y32wLLP6MQhq4nU166qFFh1J4Q9UomQoqbVo+gVp8jTtU4HrmH0DxWr4QcDP2Ekkf/tczTSVlXixiJhfsfYS2I7K4W8vDxEwEl1b+M6gwSC9E+pNldStvsELJPrW8bNL/a+hq00cC1J0qUGY05+RyHDcbY39GoVCd/cW1iQ=WGOcwtfjbBW7ZqJsR1eaKfZGUrdInb6FoisWhkyS2SNVN9Qiekg2g4DIXmNn2/4q0N/FmUyoiQ7lI0Ayvlg4nMdnf7PK0DXxx2QvnMFR2ERAqUmTXKYehYtDSUYsTJfJWBF22qkHHAWTL5V2j1JEFtAt+WScijqvliGbPUXDp78=e+TpdisRtiVTPtuvqI9BnkG4p9nvObhcb4qO682qS4NgN9UEb7AGyVWc9kNb+PQJ1LEdoCbb0PmAlgYXbJqHZCeZuzZ35kNx/9MF0hCkTsDR/Gju7foYyGDKV6nASmWmHHR66fKB7/NPrFwljoNxI0uPSAY+jqdLTeBrTqu8+SM=lmY4b4w30KNT8xlDGNpdo7e5vb8unR0rq6H5+t9kjUXCRLxLDw8kjLv5A9fMvLRuemzBpHMnGmDxgfZmzIRnelpSMrCSP5rea9VYy4YNVIJo+0sebzak/E1JaHbJAi6C786HIU1WhcfIsyxyj6NuEtFpzpkkL82AvrgGzbok220=SyrQ1zFv7ise+FYoyWYFuJJifRSm4lxaQNIBS6yEJckX6qDU6Z/osRwp+OXHhfj31GapT5dlX+LrOvhH74eYrRehjMqKSWCV9R072RB4lmb13hE0e0WTJzrlkWWgGpps1SN2X3uOCGKYQWR5omKMVeHnvXIVFYBPDFdy/on6QmQ=qxNnsAQWrUiUgg9ujT6s48raaHEtJWo9cGNzYxnpCvEkMQORZj2Xbnn/i0+D32LSUnJyQgRqnGJFTl0Ug4UZXjISfZR81QWkYE8LiDjPAk2yPrtHT7KiDgEYdfgrrlaTdLyB4z+sA8r6OET/tOXAHwCVgiwkcGErOe8pelPCOOA=Xhmlz3SoorIz/u6jmM1I5A8Iq1j6Y6ld9AfUHhamcXR4ty9o0jQxQ8QocfNxiw8Zyu10v8zVMrCJlgbVb5eMd3fKdXkYp6yO2gk+EXQTwustvK8X+1c9KMDgw2T7uZkrJAvgk/mNK6ofpjcZ83U79iuJZ6GzzBpj4Oq9ezn8QM8=uJSFWFOG1ARuohBFSBRGFo7hJmaDdGRsXILxuiavlbwg1FwMXDCKJJI8xkrMG5UXirznUXYe++Tg/IUuhnUFKrTDy0tKDA3WsaDx/Gb/YqE+q4Hg8c3EvqiRwxyVh4Qrl5Nm7G5g0Cm90rMyDF5XZLaO2YRwquZuUAkV7Zjc9Wg=rJfVHVxoDvBb8N4NyuIwXhbQKMY5gb+ghSSNZ6mQH7tatpdUlfYwXCa10e30EmUK3XEZdmVfTNHFC+NYwz5xGA+uyDoVxMYPLPHqCNVjV+Osb4JEPUPwrnJPQ/hcE0PZFWABlL45xWnFyWf91795cevRj8N/CNyul7RW8GSkswM=IDlCVUmFhB6bQ2IYGBKLVn4lLualQjGqIOfRvK1CsB3oaP8s21MpNAByDj4e68zhiuOZ7SBPF8fi2uLniGHoNuzAfcWJU9zm8JwiZx+eiVMHZUrJ1H+2aEjLqN4UwEHfuwUrP116Dd57N1ZHyVu7OoMSxN4liiVHtGSzZsgFacs=JO4X20j6RnU4dlen4Wu0ADz8U7ZuQygBUIXDKtgS4h66hO6BpyIfQ0NE3ictDsIK/Lr9ayW5rzIfnkdrnRgCvti8PBEVJdnd/3lBH2lEQmu8Ws0EbRZ17+PuQFDXJSK55CjULoObEdRHP4nANBX5t16OnUDZyKLbGmzJF+A7VqU=mxv6z2CNLx8tnKlyb5/9Stt9EShIH6cUMFgN5FZh+zwrX0oYKVschGBvHU4ik2/08ZsrZxVh1WBK4bO9j0u7Vj7RWapqi80Nx+Hx2MMzdNudZS2JkPbyLcCthtSDZCBdBUwxcDquliDQ09vRyYYKuZY28edOjqWkp/tuuYalJ2Y=UukWnHoHEZuCW9vp+yoj9Vjbo7NuvrYbK13bOPM2Dlsc7qms4tF8u3J7kr2YTHMSQWSdn5XAxD8zk6vcF8shk+xu6GemhGi4gxkVxpe0eHmoNkXmrS1ZAbzhY/JjEJbaFTxDb1ET7yYd8P7yHUJ2Fy11No7IuVsSoU1I1vVZWfg=AxTNvPYxIV/V4gNruoZE0zANuOkKTJPyjG78oWFhoZkmSFvZCokRpyZtzS6Ex3K2ELZ+LwZg5oVzp81YCwdnJxtB1Zjf2LVQkWqpO8+2dDYGGNDv2AlFwFyQfFq7WZFdpxwdF6S8972yvAo4kNm8BhtelPhWQ1cMb0YIQ9MrXVA=OzhxDeLcrgRjMWk3mCTGbBGITluDYQgaibsjVeZl1hQ6S1AHAHSf3IZWfkkTTKMhVDe41rE5IVwLhzLM+pnO6Xp7DHZefJbd/r7Gd9tF1r1qs3GfS1AVwzKM+o604MFli9tkKKHncTqhM8br39FKX1AHCp3In9jMYtZGGxxBT/c=YdsOI6dMUHzMXr9YgWMRhYy4gLOWLz00zPMSd8aVXR++jN/YNs4aX+jF0uk2UNmF1lwYKNw1EF2toYubz/TF0z5uc15N4TFAsILWnr4omZyC5R7jOcVSlTgHKG3fYtnPewt3c46v/8WdmWJzo6aZDasmBFi4sgG15gboYx2fL9g=JCqftPiyCuCT9vGREUsyGqbcwKo8CjjrGlniu+07FBnpUzBO0/2uDis/HcVAvJtmPgsPZ7OCfqn+mx+d0rNX9+MnYsdEMiGFqU2yl7BxHyh7s+cZP1tRiskz1CuQxwAe90KGwy5Yb4jwXSQe2+JZcXWLXLi47oNUz1jXG5InM6Q=H3ISFCJuaHj4QAdyWZFbBQkdzD0BH0XPR7yDbEx3hfGkG2L8rJiWg1hB4N6cSSACScHyvxo84BhgkDFU/IIBsrSppznTt2Hd9vbw53KXfJfYIHFYXhhlB7lU/Pkt8VPpQlcW6YdpztjGjJoJHR8QtX48VQozcEWRQ4edyOqKRDU=nsdZViduOL8XR0d4QdyrRsHyzP9uXa5ToWymdZDfg80Q0DoaEkMz2PoWJu6eNXIsZEUbGlVh/qqF+Hfai4+Vk/qbz17IpyjTD3CnbCXSi/YKxsPb5wpzF2AbVzksJtTEKXs39ZtWyDOyOYw9YPUgVzuvDckRId+Th+/Ci5wJxzM=Y2MhLHUJFG9/wlRDgb3OT+IDYDiMg3U0aeFj3RLY4iGqI0vljjr6UdO94ozvCkBi/sjfQKamXvCE7Ru820Qf63dAxrgRDrB3TBVFRrfK/kDuqxkmgDeJB2xuG+fvLl8qJbWXHSYNWJz16uBcqWTH3BR5yrEVd7rkEANmZoBp1zw=YHjNrlxwMBMetLKahzfCCkqyLF5SrkXfx+er3gCFWoIS2McRl5IxxdPHnku7wpwzqQXryJKu+lF8nzr4BDDO5H3G62Y1qiO6ZCEpuSIctLvgnlMdWdMqyNtEIwt0HA5ON2Fq2dTutoZJ/KeO/cT3DZgMQ3nsiSZDKzv02KjFF0A=HN0Qg0HOAb9rGEwYx0kOIXtPHB6vvgOkrPeXgERxnc1UzvkQW7F+Da7KEdpCvaGCL0ZC/4yeI14FvHPrwD/XfH+sm8kVWZZmYSivfv1RpQZXq6BgN6YuyvSuCvCHxz7Q4P7r2P5o881YGAgIpBIgr2W6a7v9weRmNvXZZxFEMGA=F2ineK5rCsxGaKNWt/6a/MyMzPFQDyUnV7Vbbj72goubFK1ZFRU3o9Q4s2sYqrxMsBXpqGfAJTa2sBi/uTAh2lvrqXzgb291VTeJIauXYWHr+TiQfV4ZW+rf0QDR86SLLrrXtad1NOHv9RB9ja3BtHrMWWZmg6FZymE73q/T/9o=", 
        ///     "PaymentUiUrl":"https://secure.spark.net/jdatecom",
        ///     "GlobalLogId":1146715248
        ///  }
        /// }
        /// 
        /// 
        /// 
        /// Example: Trigger Upsale All Access premium feature (member is already a subscriber)
        /// This example, the member is a subscriber but does not have All Access, so we will upsale them all access which will include all access emails too. 
        /// This will get variable priced all access premium feature.
        /// If it\'s from confirmation page, we should also pass in their orderID of what they just bought
        /// The key here is to specify All Access as the PremiumType we want.
        /// 
        /// {
        ///    "CancelUrl": "http://www.jdate.com/Applications/MemberServices/MemberServices.aspx",
        ///    "ReturnUrl": "http://www.jdate.com",
        ///    "ConfirmationUrl": "http://www.jdate.com/Applications/Subscription/SubscriptionConfirmation.aspx",
        ///    "DestinationUrl": "http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?memberID=411",
        ///    "PrtId": "10042",
        ///    "SrId": "411",
        ///    "clientip": "100.100.100.100",
        ///    "OmnitureVariablesJson": "{\"evar10\":\"value1\",\"evar11\":\"value2\"}",
        ///    "MemberLevelTrackingLastApplication": "1",
        ///    "MemberLevelTrackingIsMobileDevice": "false",
        ///    "MemberLevelTrackingIsTablet": "false",
        ///    "PaymentUiPostDataType": "2", //upsale
        ///    "PromoType": "0", //4: mobile, 0: member (used by FWS)
        ///    "PaymentType": "1", //1: credit card, 2: check
        ///    "PremiumType": "32" //all access only
        ///}
        ///
        /// RESPONSE
        /// 
        /// {
        /// "code":200,
        /// "status":"OK",
        /// "data":{
        ///     "Json":"T/IHoEfVFW/aoJH8Urrk+TXjETByGRxgx8bt5zrMfyW3eF4gm2QKFlZeFm16ycV9sArq0PA3X7/peDFaDfI3JF+H2bmz9IBhFgDLpYEhWkeXxNCk1WmHfWvg/1lpqMaGV68KSkENyk6W56CTjwdH07KWdHuV9rgI/zbB2U+7SeA=YYck1rtkrun7t5rSV6+0pqgyaH1ESLfa4cM36WswJiirHxXpbXmCB3aKYKEoy6krLoni4RgglYQUm1vdiIsthAupz7puzaeQ3nXOhKxNgkVL2bWRAM3pzOU1ctX0spkHcQtwrcDZDGZXFf7Q4/wgLu8v9lEjm4PlDjfZRWKpzGM=nLZBFNiloab/Ma0/eNxCBDqImw1Skaqw5oY1vBbIjEf7acSGPYxEb8WJ1i+1u7ssB/pSBHIO9iOZq/Uq+82SWAtnkLxFH8o8csYKu7AtIZ8m2VfVpVuB/SVLKPC8Sru+IkdZNgZe+I1vZwKASRtGWeQ0xNvIFqi7jCpH3NNTW0s=f62zZRbx7NPy6JWNlEq2bZxHefR2D+2D3XvLgm9Vpyou1/2IuomxKHUxbyjDVx7WczPSQQKLMTfB+45L6UtduQwNUzfEK9j4jaQ5Kv//L0I5op2Q6tACK2CGh3C4NjsmnM3cLw7vUk7591hhFUmALEYuMZcnj2fXJFn7EEriRxU=FEVz1jmeRALp8sgfvV6yRiiAL0O7BL1w0nfZQPFTYB/ywy1/8KwhzPLCpkifeDHmFVfJOdODsGuR1CbOD8Tc2WnxkgqvvyvQf2YHNNdPtdAjTBil9Fh6vBmhZcYfLOBl94sLT+F28FSow1Y4SycDzaJzG8CmIApR0b4ydITbno4=kvTBEkqoPhrfTP2SFs178WRvuvB4woFz7hTdFtaVK1cZ10lrhJAnCirA/B5r1le64MkrHVc2FwcwTfU0NLK7Q2qwjpp21z0apvKfFfgB6TpbrKUFgCIjb5WHEj2mxvurQ9JJjJftBNWNT32O2XKxxvEnlXmT08QyKIjJdL87lNg=eocryWVlnNlUTGAwzeknZCYHdDHYkPx0xb3BkGrJq2sw9/njeD7CqozG4267xY06sLOy7ak4wwZ0t/4Xtvij5Zmncyl9nwJU09qz35nWDBFXxsJLO+qFJb2ZswJeTUujcGepGMyGBMse2BXUPqAIVCPb9G7dYacpKWCZ5rhVEBg=k6LaetgO4/L6pUdjIBLUD3YCa1KChUvvnldNEe7buM2aQptDHzSjIpoImq1i2ZLPC/SspeyUVoaPP7CnEkY786fFEyBbw43aqcOvkqBv/RHlr7yhCdTR6N8Mkm1inDXquyoHpwxeFjY+dtk6V931hWnuctUMNY5Lf+Hwl+qGeJA=nW0CW13YnOo1vEHq3ovDyw4ZyhXwBqgTmI8XIahxPoEVNFGVoBHxq3oTfZiZDV5jjHlL7naGVQ3cz8suUWcep5IRpa6bqFfekzOVNYg2aYiQnF3YjCgy89f/U2/eFIcdvZxRrZ280iIlBTBn5cNBua2YYGA/T+HEObHOhfqz7fU=AjvBJQYEeyMhJtK3eZiFBQ46PnQLdqB3hX4jipCVEmmeiFBur1eegtet3YQJWRKPrm5qWOsw9GFi8UvCR9ecK1Gj1SSvs4rJtpqYKlbBg02OFSQNAk/qZ3fOiO/gqorOaLpw491Qg/Hts7OKIfwQ4CoFsz0IdjNSSDSsFJRxDVc=RHRjL0155s6quCAJGMn8M6m4Hq+c0xqvnzUb1lhwPCVswLSIMbC1p51/6vaUWYYrdKcc5FYQCAmUxpZDv02otX3lrs2wKbbVt00xT4KL4RxGIaEfJa6bhR79SM1pCR13N+QlmF1WbIKQEIRfgY+RJIZ28v1nfZF1g1Q7kejiWJ8=Z+lbvqjM/9EqTshm+0svoxgPkvAK/bDa8PCflQwWGuIgECrJgpNxTbNzNMtdy9NWKIrsRlQWOOK6Y60CoaVbkmR9tIXyjf3wBijh9GG5TbWr3m88vrkyao0c9YnDJnoKgtcOpbcX6wmEGEVQvAeymxxHBr2sTCxKGBZp8Byq1Ko=dpTykOGYS8EUyHzC30wOYdxF+cte4KweWBMW+EG6BpB1kyT1UghkId7d9loX2RgJxzmNO7jJy+eaUeSmDiNQb4Qv4OT6WJa22BZTUIrUMeYH0mQx32UbDKUTWb7SBvqgqt1oSkxzvY4KaZmfy6boPywiiF3IUkP637v4Rad1AS8=pOZoOD5ZvOT4qkvaAzdO2zBXaydrKAFTnj0FPkVmxB9/A6KTw8wc8osn1l/SGs9ZbvEf4exen5kJUaqhWARAFkVzSeNtSRLt8BHBEzeaYpgc2uFIGpZxXofGxfeFniEWfd9mgmjQAmIrgUwEiSwQeqfZCNWnVN4aYvU/RBhvFzA=W7/8enPeZCa2l1XB2KrN0W7bFFRgr/X73ENpZyYqDhrptIJ89yGa1vr2jzFEjbwhyO/AQ/DB97uXGgBAVMsOdLWG6y5E0Rgkm0PKeGCqMidr0ggNeRX1K6mqmYq8z3CcXXZ5jgCGUnN0Q2QhThmCWJ+0BWRDNZOarCcpyJSXlOg=au8O8rWFgSFa1AY95+Jxpd4GgcZ4ow2phRhQ+xLvRIyMTiWIpJwxoK5y+02e/uEfVCaYfSMDOnqu3at5i/Nt5SVLVjdQlQQFTuigF+oMpwXGCStaH85dtyU9FOYE6cy3gmFtl0mqV3UKRj/Ng7nO+tN4QJ7ERfDIiMJBKbW1BRw=tFC4kqU4lDtjOx4J4MgqfKWmJhQgkNVa9yNYFRtv8mnmrq7wgoYMqpw5iXqi6L/kKtBAY6t3wOGWtoq/gD77FGOV0IqfrU+3L00m9vLpwJhXSXmsiG5bAahkZP7mJWX7hIIJZMh2FAZ+gT9Oyfy1ZpM6kcPQWn/nurqGqkwggZY=j1drTYPLZdzjDbQdoBN53owTEZhvOTl2wFC+zsogjGovRDqjvha/ebw5tn1ADWkgwIMSAePkUrMSmGaiMsP9yXj7vHbIsbCUQShvtlj/5+JltSbotsF0epNX0/vGvas2u9YVnyYbBcgdw1pnHZS3AgCAyv9d86gvjh/k/DYKRsQ=u5H62pCo8w2VmFLPF5d35IXNbTf5macXKCSIBex/cHWj7t+aopVijpLsdGd5WGy01XnNbjRy0ORQ4sYq8YbzHc7gQELfSvAT2ZSW7IUw5EkLI1XN1pPxpC71EHjHCvy3BRc9BpO/Bws3cByyGNCBErFuYUWqfa83nQT9S/XGDpE=U+Dqidq70v71FWv5bLny3tJnEoNV5bVCV7XIZtId8sViPoJKxp1HBIJwEp2j1Bu3EhDJm9cGu7ElvpoDCgqnn09WNoD8PG9SwgQwVO+5LVOqucwfn11s3t4DKugoKtbHnoLk3ejH+TQO6GLxR1CXZctR8BubGO5KYu7eVvHrZxw=TjyryhPCq3U/BKLByTYJ7G2pj3zXz8aUrAsLaFbw2cbLcknxHT/BrimgGG7NyUQJjkeWhDQkZLKENc4xls4gS0Nnb4cRllBT6CZt5qsN5jSqmfNjb3kSkTl8ASp73TX1F7Q8e/T7Kzy0JHdDNLXOg/BVk68DiifOlQjSk5VPMSI=cXIBElfFvrU7WswF+Ub87k/1N9NY+QKGms2JBa79Ef5LgODwJLUd3zLZfvPla3GOVZIgI2eKCpY6JMQsCRF+MCC7OnDP2gbb7NsSwiI+Ul3zwkNur4Di3WfAJnl9/ROWZtDiFOLMK3mb65AQyrVP4t0/qP3v1kXfZxbUUS0twpA=WmpKg4Laajiqj7xa+wJPWL3vIwRrUyTwdNXekykZ76Ru+dD2MyxxgDdmY7OW97P5VoZ5OU3JZ6na/gDcX3V8O1A1EqrX4OyzODVRJn3NI7AhdD2lzi/+4sLN64GoFAgRHRG9wDyOdVJbhWxrl/+BkDsPK6rVvSvVAgb8ixv+Xxs=VVD2SE0uu+Pnqwh0ks2A4vABFjqFEd0C8uuWHQ3eBMwtv5q+ULKOZwYQjnkbzxE4v48fOMJyKcs/IS48vxmwmQfr41sjl+ZxCPkKH0rmJcnF26iNZg/bRmrEV4ItBwgPF64RgW5wRFhJwUvSj84gIEieoli+eYaElf5YfftIINk=RSGLBQocOD2s3X/rY2RUjnCatDvzjuN75p37CSqjmY3iKwaqvFwM+6MVrwuhw7IsiZnoJ/b8zKJSCMcYQEzTkX/jA6xYRALbbBqSCui5ulHafDw2hb5yC0XoEkM+Ig0RUvcSrN79vM1HaRNzfJ9dUOw65Ms/6GbYO/TDDeNmhfQ=hvM1zBQFRgEDfkLLKnl799hAUREJHwcsFkUS5U77ZqgAHxRy+nZ4mydh/nfKiTXwGqqg30PK4z/bf3Ll2pOlIz/cVqTR8aP54OBFSyFuXzGgHM1ekx2ob4BgNfHTzRonHpL7KTkM2AXOhjfdbEHQiARKvTgHzi9hnNfKxOFvaSo=h78eZBuDw2PZuMVU8m6tatfTwhxbPRvDSrRPFlhbMHV/osMkAUjVouKJb356zNw5VHRyxIvsMDFbiuS0ww5NHqHmNpCKlqj3Q3SW4wA3DSV2Uhko4ibycLBt1oOjmIdgGfXZxZdUl0BV68MZxvR/uZCUaZLxkfuGYtJlQF2FvPk=gJumo6FQ09q3ln08PLJ92CHnaaTkygnKjj08qS6YPdbFJW2NfK2xNFWu28MJkrSZ4V5/tCnv8vehqYTljcxfDxzGZOqK3EEaDvcJ5joucMWNEhA+slHfqGcBweBxOEoW9T/SMY/YyBhxLISU6WmXZiTsOZZgtcB+BpWZSddsW3o=e+L83nT7Q93CtmruPLll2PjfF19LLJeimEbjC2SzPgM/OuilOaYYHw7weOzhRR9p21mWdOJ4DGcWDwL6bbZ9wUefvcl8f6+/cHgw17dLg39feasLa5sHxN1y3zUUL8Oy2LQteGd1xgMyPooe/EHuuafoka2m1PqYCTiMZ7zOXgw=NSO9Vs+0kTT6ZLgR5QfREOxdYQ+/pMir+61mzTv9KpKQY1g9PUqTBz7Hoo0Apy2Zf/Z0zO9wr114Hp7P2OlW7UE63d2qIKwuklC/VFaFPsIVWAgNkAoJz5kO1mT5KTksuSNskFf8t74hABM0OUl5/WV6NRkxBj1eQyR9V7qNQZc=xkbDzM/LvzuUiDOum60JeY/CDxgtYf7Ti5ghw2BuX+s/la62XNIRfvcVqNk0EwQbhUaR5wURmVMI+bxoFZYB0aouQEL4Kp6N3WcoaJxZ8eWRvTW412bC5Gz2keV4zoV/sGgQxRBel1KAEHq8nA+tUBYpqgewufD3UOP+LYoBcjw=R9Mm7BAian/XZA8jkabCLHppOkk27eEgsRZMJvc8/agP1lzbSZxB2B9zu2iGP/CgY7K9cnSLs0w148k8bnJioJK/N79FkaJOYxeXrwv1AVtbm+4j4B2vVG7Khqci/+CeoPEIayeuG4xUVG0MIHSiPJG+7mn4dXj2RzUdGqB7Lss=tau9Y32wLLP6MQhq4nU166qFFh1J4Q9UomQoqbVo+gVp8jTtU4HrmH0DxWr4QcDP2Ekkf/tczTSVlXixiJhfsfYS2I7K4W8vDxEwEl1b+M6gwSC9E+pNldStvsELJPrW8bNL/a+hq00cC1J0qUGY05+RyHDcbY39GoVCd/cW1iQ=WGOcwtfjbBW7ZqJsR1eaKfZGUrdInb6FoisWhkyS2SNVN9Qiekg2g4DIXmNn2/4q0N/FmUyoiQ7lI0Ayvlg4nMdnf7PK0DXxx2QvnMFR2ERAqUmTXKYehYtDSUYsTJfJWBF22qkHHAWTL5V2j1JEFtAt+WScijqvliGbPUXDp78=e+TpdisRtiVTPtuvqI9BnkG4p9nvObhcb4qO682qS4NgN9UEb7AGyVWc9kNb+PQJ1LEdoCbb0PmAlgYXbJqHZCeZuzZ35kNx/9MF0hCkTsDR/Gju7foYyGDKV6nASmWmHHR66fKB7/NPrFwljoNxI0uPSAY+jqdLTeBrTqu8+SM=lmY4b4w30KNT8xlDGNpdo7e5vb8unR0rq6H5+t9kjUXCRLxLDw8kjLv5A9fMvLRuemzBpHMnGmDxgfZmzIRnelpSMrCSP5rea9VYy4YNVIJo+0sebzak/E1JaHbJAi6C786HIU1WhcfIsyxyj6NuEtFpzpkkL82AvrgGzbok220=SyrQ1zFv7ise+FYoyWYFuJJifRSm4lxaQNIBS6yEJckX6qDU6Z/osRwp+OXHhfj31GapT5dlX+LrOvhH74eYrRehjMqKSWCV9R072RB4lmb13hE0e0WTJzrlkWWgGpps1SN2X3uOCGKYQWR5omKMVeHnvXIVFYBPDFdy/on6QmQ=qxNnsAQWrUiUgg9ujT6s48raaHEtJWo9cGNzYxnpCvEkMQORZj2Xbnn/i0+D32LSUnJyQgRqnGJFTl0Ug4UZXjISfZR81QWkYE8LiDjPAk2yPrtHT7KiDgEYdfgrrlaTdLyB4z+sA8r6OET/tOXAHwCVgiwkcGErOe8pelPCOOA=Xhmlz3SoorIz/u6jmM1I5A8Iq1j6Y6ld9AfUHhamcXR4ty9o0jQxQ8QocfNxiw8Zyu10v8zVMrCJlgbVb5eMd3fKdXkYp6yO2gk+EXQTwustvK8X+1c9KMDgw2T7uZkrJAvgk/mNK6ofpjcZ83U79iuJZ6GzzBpj4Oq9ezn8QM8=uJSFWFOG1ARuohBFSBRGFo7hJmaDdGRsXILxuiavlbwg1FwMXDCKJJI8xkrMG5UXirznUXYe++Tg/IUuhnUFKrTDy0tKDA3WsaDx/Gb/YqE+q4Hg8c3EvqiRwxyVh4Qrl5Nm7G5g0Cm90rMyDF5XZLaO2YRwquZuUAkV7Zjc9Wg=rJfVHVxoDvBb8N4NyuIwXhbQKMY5gb+ghSSNZ6mQH7tatpdUlfYwXCa10e30EmUK3XEZdmVfTNHFC+NYwz5xGA+uyDoVxMYPLPHqCNVjV+Osb4JEPUPwrnJPQ/hcE0PZFWABlL45xWnFyWf91795cevRj8N/CNyul7RW8GSkswM=IDlCVUmFhB6bQ2IYGBKLVn4lLualQjGqIOfRvK1CsB3oaP8s21MpNAByDj4e68zhiuOZ7SBPF8fi2uLniGHoNuzAfcWJU9zm8JwiZx+eiVMHZUrJ1H+2aEjLqN4UwEHfuwUrP116Dd57N1ZHyVu7OoMSxN4liiVHtGSzZsgFacs=JO4X20j6RnU4dlen4Wu0ADz8U7ZuQygBUIXDKtgS4h66hO6BpyIfQ0NE3ictDsIK/Lr9ayW5rzIfnkdrnRgCvti8PBEVJdnd/3lBH2lEQmu8Ws0EbRZ17+PuQFDXJSK55CjULoObEdRHP4nANBX5t16OnUDZyKLbGmzJF+A7VqU=mxv6z2CNLx8tnKlyb5/9Stt9EShIH6cUMFgN5FZh+zwrX0oYKVschGBvHU4ik2/08ZsrZxVh1WBK4bO9j0u7Vj7RWapqi80Nx+Hx2MMzdNudZS2JkPbyLcCthtSDZCBdBUwxcDquliDQ09vRyYYKuZY28edOjqWkp/tuuYalJ2Y=UukWnHoHEZuCW9vp+yoj9Vjbo7NuvrYbK13bOPM2Dlsc7qms4tF8u3J7kr2YTHMSQWSdn5XAxD8zk6vcF8shk+xu6GemhGi4gxkVxpe0eHmoNkXmrS1ZAbzhY/JjEJbaFTxDb1ET7yYd8P7yHUJ2Fy11No7IuVsSoU1I1vVZWfg=AxTNvPYxIV/V4gNruoZE0zANuOkKTJPyjG78oWFhoZkmSFvZCokRpyZtzS6Ex3K2ELZ+LwZg5oVzp81YCwdnJxtB1Zjf2LVQkWqpO8+2dDYGGNDv2AlFwFyQfFq7WZFdpxwdF6S8972yvAo4kNm8BhtelPhWQ1cMb0YIQ9MrXVA=OzhxDeLcrgRjMWk3mCTGbBGITluDYQgaibsjVeZl1hQ6S1AHAHSf3IZWfkkTTKMhVDe41rE5IVwLhzLM+pnO6Xp7DHZefJbd/r7Gd9tF1r1qs3GfS1AVwzKM+o604MFli9tkKKHncTqhM8br39FKX1AHCp3In9jMYtZGGxxBT/c=YdsOI6dMUHzMXr9YgWMRhYy4gLOWLz00zPMSd8aVXR++jN/YNs4aX+jF0uk2UNmF1lwYKNw1EF2toYubz/TF0z5uc15N4TFAsILWnr4omZyC5R7jOcVSlTgHKG3fYtnPewt3c46v/8WdmWJzo6aZDasmBFi4sgG15gboYx2fL9g=JCqftPiyCuCT9vGREUsyGqbcwKo8CjjrGlniu+07FBnpUzBO0/2uDis/HcVAvJtmPgsPZ7OCfqn+mx+d0rNX9+MnYsdEMiGFqU2yl7BxHyh7s+cZP1tRiskz1CuQxwAe90KGwy5Yb4jwXSQe2+JZcXWLXLi47oNUz1jXG5InM6Q=H3ISFCJuaHj4QAdyWZFbBQkdzD0BH0XPR7yDbEx3hfGkG2L8rJiWg1hB4N6cSSACScHyvxo84BhgkDFU/IIBsrSppznTt2Hd9vbw53KXfJfYIHFYXhhlB7lU/Pkt8VPpQlcW6YdpztjGjJoJHR8QtX48VQozcEWRQ4edyOqKRDU=nsdZViduOL8XR0d4QdyrRsHyzP9uXa5ToWymdZDfg80Q0DoaEkMz2PoWJu6eNXIsZEUbGlVh/qqF+Hfai4+Vk/qbz17IpyjTD3CnbCXSi/YKxsPb5wpzF2AbVzksJtTEKXs39ZtWyDOyOYw9YPUgVzuvDckRId+Th+/Ci5wJxzM=Y2MhLHUJFG9/wlRDgb3OT+IDYDiMg3U0aeFj3RLY4iGqI0vljjr6UdO94ozvCkBi/sjfQKamXvCE7Ru820Qf63dAxrgRDrB3TBVFRrfK/kDuqxkmgDeJB2xuG+fvLl8qJbWXHSYNWJz16uBcqWTH3BR5yrEVd7rkEANmZoBp1zw=YHjNrlxwMBMetLKahzfCCkqyLF5SrkXfx+er3gCFWoIS2McRl5IxxdPHnku7wpwzqQXryJKu+lF8nzr4BDDO5H3G62Y1qiO6ZCEpuSIctLvgnlMdWdMqyNtEIwt0HA5ON2Fq2dTutoZJ/KeO/cT3DZgMQ3nsiSZDKzv02KjFF0A=HN0Qg0HOAb9rGEwYx0kOIXtPHB6vvgOkrPeXgERxnc1UzvkQW7F+Da7KEdpCvaGCL0ZC/4yeI14FvHPrwD/XfH+sm8kVWZZmYSivfv1RpQZXq6BgN6YuyvSuCvCHxz7Q4P7r2P5o881YGAgIpBIgr2W6a7v9weRmNvXZZxFEMGA=F2ineK5rCsxGaKNWt/6a/MyMzPFQDyUnV7Vbbj72goubFK1ZFRU3o9Q4s2sYqrxMsBXpqGfAJTa2sBi/uTAh2lvrqXzgb291VTeJIauXYWHr+TiQfV4ZW+rf0QDR86SLLrrXtad1NOHv9RB9ja3BtHrMWWZmg6FZymE73q/T/9o=", 
        ///     "PaymentUiUrl":"https://secure.spark.net/jdatecom",
        ///     "GlobalLogId":1146715248
        ///  }
        /// }
        /// 
        /// 
        /// Example: Trigger Upsale from confirmation after they just made a subscription purchase
        /// This example, the member just made a subscription purchase and so we want to present them with an upsale of available premium features 
        /// This will get variable priced all access premium feature.
        /// If it\'s from confirmation page, we should also pass in their orderID of what they just bought
        /// The key here is to specify All Access as the PremiumType we want.
        /// 
        /// {
        ///    "CancelUrl": "http://www.jdate.com/Applications/MemberServices/MemberServices.aspx",
        ///    "ReturnUrl": "http://www.jdate.com",
        ///    "ConfirmationUrl": "http://www.jdate.com/Applications/Subscription/SubscriptionConfirmation.aspx",
        ///    "DestinationUrl": "http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?memberID=411",
        ///    "PrtId": "10042",
        ///    "SrId": "411",
        ///    "clientip": "100.100.100.100",
        ///    "OmnitureVariablesJson": "{\"evar10\":\"value1\",\"evar11\":\"value2\"}",
        ///    "MemberLevelTrackingLastApplication": "1",
        ///    "MemberLevelTrackingIsMobileDevice": "false",
        ///    "MemberLevelTrackingIsTablet": "false",
        ///    "PaymentUiPostDataType": "2",
        ///    "PromoType": "0", //4: mobile, 0: member (used by FWS)
        ///    "PaymentType": "1", //1: credit card, 2: check
        ///    "TemplateID": "400",
        ///    "OrderID": "28227272"
        ///}
        ///
        /// RESPONSE
        /// 
        /// {
        /// "code":200,
        /// "status":"OK",
        /// "data":{
        ///     "Json":"T/IHoEfVFW/aoJH8Urrk+TXjETByGRxgx8bt5zrMfyW3eF4gm2QKFlZeFm16ycV9sArq0PA3X7/peDFaDfI3JF+H2bmz9IBhFgDLpYEhWkeXxNCk1WmHfWvg/1lpqMaGV68KSkENyk6W56CTjwdH07KWdHuV9rgI/zbB2U+7SeA=YYck1rtkrun7t5rSV6+0pqgyaH1ESLfa4cM36WswJiirHxXpbXmCB3aKYKEoy6krLoni4RgglYQUm1vdiIsthAupz7puzaeQ3nXOhKxNgkVL2bWRAM3pzOU1ctX0spkHcQtwrcDZDGZXFf7Q4/wgLu8v9lEjm4PlDjfZRWKpzGM=nLZBFNiloab/Ma0/eNxCBDqImw1Skaqw5oY1vBbIjEf7acSGPYxEb8WJ1i+1u7ssB/pSBHIO9iOZq/Uq+82SWAtnkLxFH8o8csYKu7AtIZ8m2VfVpVuB/SVLKPC8Sru+IkdZNgZe+I1vZwKASRtGWeQ0xNvIFqi7jCpH3NNTW0s=f62zZRbx7NPy6JWNlEq2bZxHefR2D+2D3XvLgm9Vpyou1/2IuomxKHUxbyjDVx7WczPSQQKLMTfB+45L6UtduQwNUzfEK9j4jaQ5Kv//L0I5op2Q6tACK2CGh3C4NjsmnM3cLw7vUk7591hhFUmALEYuMZcnj2fXJFn7EEriRxU=FEVz1jmeRALp8sgfvV6yRiiAL0O7BL1w0nfZQPFTYB/ywy1/8KwhzPLCpkifeDHmFVfJOdODsGuR1CbOD8Tc2WnxkgqvvyvQf2YHNNdPtdAjTBil9Fh6vBmhZcYfLOBl94sLT+F28FSow1Y4SycDzaJzG8CmIApR0b4ydITbno4=kvTBEkqoPhrfTP2SFs178WRvuvB4woFz7hTdFtaVK1cZ10lrhJAnCirA/B5r1le64MkrHVc2FwcwTfU0NLK7Q2qwjpp21z0apvKfFfgB6TpbrKUFgCIjb5WHEj2mxvurQ9JJjJftBNWNT32O2XKxxvEnlXmT08QyKIjJdL87lNg=eocryWVlnNlUTGAwzeknZCYHdDHYkPx0xb3BkGrJq2sw9/njeD7CqozG4267xY06sLOy7ak4wwZ0t/4Xtvij5Zmncyl9nwJU09qz35nWDBFXxsJLO+qFJb2ZswJeTUujcGepGMyGBMse2BXUPqAIVCPb9G7dYacpKWCZ5rhVEBg=k6LaetgO4/L6pUdjIBLUD3YCa1KChUvvnldNEe7buM2aQptDHzSjIpoImq1i2ZLPC/SspeyUVoaPP7CnEkY786fFEyBbw43aqcOvkqBv/RHlr7yhCdTR6N8Mkm1inDXquyoHpwxeFjY+dtk6V931hWnuctUMNY5Lf+Hwl+qGeJA=nW0CW13YnOo1vEHq3ovDyw4ZyhXwBqgTmI8XIahxPoEVNFGVoBHxq3oTfZiZDV5jjHlL7naGVQ3cz8suUWcep5IRpa6bqFfekzOVNYg2aYiQnF3YjCgy89f/U2/eFIcdvZxRrZ280iIlBTBn5cNBua2YYGA/T+HEObHOhfqz7fU=AjvBJQYEeyMhJtK3eZiFBQ46PnQLdqB3hX4jipCVEmmeiFBur1eegtet3YQJWRKPrm5qWOsw9GFi8UvCR9ecK1Gj1SSvs4rJtpqYKlbBg02OFSQNAk/qZ3fOiO/gqorOaLpw491Qg/Hts7OKIfwQ4CoFsz0IdjNSSDSsFJRxDVc=RHRjL0155s6quCAJGMn8M6m4Hq+c0xqvnzUb1lhwPCVswLSIMbC1p51/6vaUWYYrdKcc5FYQCAmUxpZDv02otX3lrs2wKbbVt00xT4KL4RxGIaEfJa6bhR79SM1pCR13N+QlmF1WbIKQEIRfgY+RJIZ28v1nfZF1g1Q7kejiWJ8=Z+lbvqjM/9EqTshm+0svoxgPkvAK/bDa8PCflQwWGuIgECrJgpNxTbNzNMtdy9NWKIrsRlQWOOK6Y60CoaVbkmR9tIXyjf3wBijh9GG5TbWr3m88vrkyao0c9YnDJnoKgtcOpbcX6wmEGEVQvAeymxxHBr2sTCxKGBZp8Byq1Ko=dpTykOGYS8EUyHzC30wOYdxF+cte4KweWBMW+EG6BpB1kyT1UghkId7d9loX2RgJxzmNO7jJy+eaUeSmDiNQb4Qv4OT6WJa22BZTUIrUMeYH0mQx32UbDKUTWb7SBvqgqt1oSkxzvY4KaZmfy6boPywiiF3IUkP637v4Rad1AS8=pOZoOD5ZvOT4qkvaAzdO2zBXaydrKAFTnj0FPkVmxB9/A6KTw8wc8osn1l/SGs9ZbvEf4exen5kJUaqhWARAFkVzSeNtSRLt8BHBEzeaYpgc2uFIGpZxXofGxfeFniEWfd9mgmjQAmIrgUwEiSwQeqfZCNWnVN4aYvU/RBhvFzA=W7/8enPeZCa2l1XB2KrN0W7bFFRgr/X73ENpZyYqDhrptIJ89yGa1vr2jzFEjbwhyO/AQ/DB97uXGgBAVMsOdLWG6y5E0Rgkm0PKeGCqMidr0ggNeRX1K6mqmYq8z3CcXXZ5jgCGUnN0Q2QhThmCWJ+0BWRDNZOarCcpyJSXlOg=au8O8rWFgSFa1AY95+Jxpd4GgcZ4ow2phRhQ+xLvRIyMTiWIpJwxoK5y+02e/uEfVCaYfSMDOnqu3at5i/Nt5SVLVjdQlQQFTuigF+oMpwXGCStaH85dtyU9FOYE6cy3gmFtl0mqV3UKRj/Ng7nO+tN4QJ7ERfDIiMJBKbW1BRw=tFC4kqU4lDtjOx4J4MgqfKWmJhQgkNVa9yNYFRtv8mnmrq7wgoYMqpw5iXqi6L/kKtBAY6t3wOGWtoq/gD77FGOV0IqfrU+3L00m9vLpwJhXSXmsiG5bAahkZP7mJWX7hIIJZMh2FAZ+gT9Oyfy1ZpM6kcPQWn/nurqGqkwggZY=j1drTYPLZdzjDbQdoBN53owTEZhvOTl2wFC+zsogjGovRDqjvha/ebw5tn1ADWkgwIMSAePkUrMSmGaiMsP9yXj7vHbIsbCUQShvtlj/5+JltSbotsF0epNX0/vGvas2u9YVnyYbBcgdw1pnHZS3AgCAyv9d86gvjh/k/DYKRsQ=u5H62pCo8w2VmFLPF5d35IXNbTf5macXKCSIBex/cHWj7t+aopVijpLsdGd5WGy01XnNbjRy0ORQ4sYq8YbzHc7gQELfSvAT2ZSW7IUw5EkLI1XN1pPxpC71EHjHCvy3BRc9BpO/Bws3cByyGNCBErFuYUWqfa83nQT9S/XGDpE=U+Dqidq70v71FWv5bLny3tJnEoNV5bVCV7XIZtId8sViPoJKxp1HBIJwEp2j1Bu3EhDJm9cGu7ElvpoDCgqnn09WNoD8PG9SwgQwVO+5LVOqucwfn11s3t4DKugoKtbHnoLk3ejH+TQO6GLxR1CXZctR8BubGO5KYu7eVvHrZxw=TjyryhPCq3U/BKLByTYJ7G2pj3zXz8aUrAsLaFbw2cbLcknxHT/BrimgGG7NyUQJjkeWhDQkZLKENc4xls4gS0Nnb4cRllBT6CZt5qsN5jSqmfNjb3kSkTl8ASp73TX1F7Q8e/T7Kzy0JHdDNLXOg/BVk68DiifOlQjSk5VPMSI=cXIBElfFvrU7WswF+Ub87k/1N9NY+QKGms2JBa79Ef5LgODwJLUd3zLZfvPla3GOVZIgI2eKCpY6JMQsCRF+MCC7OnDP2gbb7NsSwiI+Ul3zwkNur4Di3WfAJnl9/ROWZtDiFOLMK3mb65AQyrVP4t0/qP3v1kXfZxbUUS0twpA=WmpKg4Laajiqj7xa+wJPWL3vIwRrUyTwdNXekykZ76Ru+dD2MyxxgDdmY7OW97P5VoZ5OU3JZ6na/gDcX3V8O1A1EqrX4OyzODVRJn3NI7AhdD2lzi/+4sLN64GoFAgRHRG9wDyOdVJbhWxrl/+BkDsPK6rVvSvVAgb8ixv+Xxs=VVD2SE0uu+Pnqwh0ks2A4vABFjqFEd0C8uuWHQ3eBMwtv5q+ULKOZwYQjnkbzxE4v48fOMJyKcs/IS48vxmwmQfr41sjl+ZxCPkKH0rmJcnF26iNZg/bRmrEV4ItBwgPF64RgW5wRFhJwUvSj84gIEieoli+eYaElf5YfftIINk=RSGLBQocOD2s3X/rY2RUjnCatDvzjuN75p37CSqjmY3iKwaqvFwM+6MVrwuhw7IsiZnoJ/b8zKJSCMcYQEzTkX/jA6xYRALbbBqSCui5ulHafDw2hb5yC0XoEkM+Ig0RUvcSrN79vM1HaRNzfJ9dUOw65Ms/6GbYO/TDDeNmhfQ=hvM1zBQFRgEDfkLLKnl799hAUREJHwcsFkUS5U77ZqgAHxRy+nZ4mydh/nfKiTXwGqqg30PK4z/bf3Ll2pOlIz/cVqTR8aP54OBFSyFuXzGgHM1ekx2ob4BgNfHTzRonHpL7KTkM2AXOhjfdbEHQiARKvTgHzi9hnNfKxOFvaSo=h78eZBuDw2PZuMVU8m6tatfTwhxbPRvDSrRPFlhbMHV/osMkAUjVouKJb356zNw5VHRyxIvsMDFbiuS0ww5NHqHmNpCKlqj3Q3SW4wA3DSV2Uhko4ibycLBt1oOjmIdgGfXZxZdUl0BV68MZxvR/uZCUaZLxkfuGYtJlQF2FvPk=gJumo6FQ09q3ln08PLJ92CHnaaTkygnKjj08qS6YPdbFJW2NfK2xNFWu28MJkrSZ4V5/tCnv8vehqYTljcxfDxzGZOqK3EEaDvcJ5joucMWNEhA+slHfqGcBweBxOEoW9T/SMY/YyBhxLISU6WmXZiTsOZZgtcB+BpWZSddsW3o=e+L83nT7Q93CtmruPLll2PjfF19LLJeimEbjC2SzPgM/OuilOaYYHw7weOzhRR9p21mWdOJ4DGcWDwL6bbZ9wUefvcl8f6+/cHgw17dLg39feasLa5sHxN1y3zUUL8Oy2LQteGd1xgMyPooe/EHuuafoka2m1PqYCTiMZ7zOXgw=NSO9Vs+0kTT6ZLgR5QfREOxdYQ+/pMir+61mzTv9KpKQY1g9PUqTBz7Hoo0Apy2Zf/Z0zO9wr114Hp7P2OlW7UE63d2qIKwuklC/VFaFPsIVWAgNkAoJz5kO1mT5KTksuSNskFf8t74hABM0OUl5/WV6NRkxBj1eQyR9V7qNQZc=xkbDzM/LvzuUiDOum60JeY/CDxgtYf7Ti5ghw2BuX+s/la62XNIRfvcVqNk0EwQbhUaR5wURmVMI+bxoFZYB0aouQEL4Kp6N3WcoaJxZ8eWRvTW412bC5Gz2keV4zoV/sGgQxRBel1KAEHq8nA+tUBYpqgewufD3UOP+LYoBcjw=R9Mm7BAian/XZA8jkabCLHppOkk27eEgsRZMJvc8/agP1lzbSZxB2B9zu2iGP/CgY7K9cnSLs0w148k8bnJioJK/N79FkaJOYxeXrwv1AVtbm+4j4B2vVG7Khqci/+CeoPEIayeuG4xUVG0MIHSiPJG+7mn4dXj2RzUdGqB7Lss=tau9Y32wLLP6MQhq4nU166qFFh1J4Q9UomQoqbVo+gVp8jTtU4HrmH0DxWr4QcDP2Ekkf/tczTSVlXixiJhfsfYS2I7K4W8vDxEwEl1b+M6gwSC9E+pNldStvsELJPrW8bNL/a+hq00cC1J0qUGY05+RyHDcbY39GoVCd/cW1iQ=WGOcwtfjbBW7ZqJsR1eaKfZGUrdInb6FoisWhkyS2SNVN9Qiekg2g4DIXmNn2/4q0N/FmUyoiQ7lI0Ayvlg4nMdnf7PK0DXxx2QvnMFR2ERAqUmTXKYehYtDSUYsTJfJWBF22qkHHAWTL5V2j1JEFtAt+WScijqvliGbPUXDp78=e+TpdisRtiVTPtuvqI9BnkG4p9nvObhcb4qO682qS4NgN9UEb7AGyVWc9kNb+PQJ1LEdoCbb0PmAlgYXbJqHZCeZuzZ35kNx/9MF0hCkTsDR/Gju7foYyGDKV6nASmWmHHR66fKB7/NPrFwljoNxI0uPSAY+jqdLTeBrTqu8+SM=lmY4b4w30KNT8xlDGNpdo7e5vb8unR0rq6H5+t9kjUXCRLxLDw8kjLv5A9fMvLRuemzBpHMnGmDxgfZmzIRnelpSMrCSP5rea9VYy4YNVIJo+0sebzak/E1JaHbJAi6C786HIU1WhcfIsyxyj6NuEtFpzpkkL82AvrgGzbok220=SyrQ1zFv7ise+FYoyWYFuJJifRSm4lxaQNIBS6yEJckX6qDU6Z/osRwp+OXHhfj31GapT5dlX+LrOvhH74eYrRehjMqKSWCV9R072RB4lmb13hE0e0WTJzrlkWWgGpps1SN2X3uOCGKYQWR5omKMVeHnvXIVFYBPDFdy/on6QmQ=qxNnsAQWrUiUgg9ujT6s48raaHEtJWo9cGNzYxnpCvEkMQORZj2Xbnn/i0+D32LSUnJyQgRqnGJFTl0Ug4UZXjISfZR81QWkYE8LiDjPAk2yPrtHT7KiDgEYdfgrrlaTdLyB4z+sA8r6OET/tOXAHwCVgiwkcGErOe8pelPCOOA=Xhmlz3SoorIz/u6jmM1I5A8Iq1j6Y6ld9AfUHhamcXR4ty9o0jQxQ8QocfNxiw8Zyu10v8zVMrCJlgbVb5eMd3fKdXkYp6yO2gk+EXQTwustvK8X+1c9KMDgw2T7uZkrJAvgk/mNK6ofpjcZ83U79iuJZ6GzzBpj4Oq9ezn8QM8=uJSFWFOG1ARuohBFSBRGFo7hJmaDdGRsXILxuiavlbwg1FwMXDCKJJI8xkrMG5UXirznUXYe++Tg/IUuhnUFKrTDy0tKDA3WsaDx/Gb/YqE+q4Hg8c3EvqiRwxyVh4Qrl5Nm7G5g0Cm90rMyDF5XZLaO2YRwquZuUAkV7Zjc9Wg=rJfVHVxoDvBb8N4NyuIwXhbQKMY5gb+ghSSNZ6mQH7tatpdUlfYwXCa10e30EmUK3XEZdmVfTNHFC+NYwz5xGA+uyDoVxMYPLPHqCNVjV+Osb4JEPUPwrnJPQ/hcE0PZFWABlL45xWnFyWf91795cevRj8N/CNyul7RW8GSkswM=IDlCVUmFhB6bQ2IYGBKLVn4lLualQjGqIOfRvK1CsB3oaP8s21MpNAByDj4e68zhiuOZ7SBPF8fi2uLniGHoNuzAfcWJU9zm8JwiZx+eiVMHZUrJ1H+2aEjLqN4UwEHfuwUrP116Dd57N1ZHyVu7OoMSxN4liiVHtGSzZsgFacs=JO4X20j6RnU4dlen4Wu0ADz8U7ZuQygBUIXDKtgS4h66hO6BpyIfQ0NE3ictDsIK/Lr9ayW5rzIfnkdrnRgCvti8PBEVJdnd/3lBH2lEQmu8Ws0EbRZ17+PuQFDXJSK55CjULoObEdRHP4nANBX5t16OnUDZyKLbGmzJF+A7VqU=mxv6z2CNLx8tnKlyb5/9Stt9EShIH6cUMFgN5FZh+zwrX0oYKVschGBvHU4ik2/08ZsrZxVh1WBK4bO9j0u7Vj7RWapqi80Nx+Hx2MMzdNudZS2JkPbyLcCthtSDZCBdBUwxcDquliDQ09vRyYYKuZY28edOjqWkp/tuuYalJ2Y=UukWnHoHEZuCW9vp+yoj9Vjbo7NuvrYbK13bOPM2Dlsc7qms4tF8u3J7kr2YTHMSQWSdn5XAxD8zk6vcF8shk+xu6GemhGi4gxkVxpe0eHmoNkXmrS1ZAbzhY/JjEJbaFTxDb1ET7yYd8P7yHUJ2Fy11No7IuVsSoU1I1vVZWfg=AxTNvPYxIV/V4gNruoZE0zANuOkKTJPyjG78oWFhoZkmSFvZCokRpyZtzS6Ex3K2ELZ+LwZg5oVzp81YCwdnJxtB1Zjf2LVQkWqpO8+2dDYGGNDv2AlFwFyQfFq7WZFdpxwdF6S8972yvAo4kNm8BhtelPhWQ1cMb0YIQ9MrXVA=OzhxDeLcrgRjMWk3mCTGbBGITluDYQgaibsjVeZl1hQ6S1AHAHSf3IZWfkkTTKMhVDe41rE5IVwLhzLM+pnO6Xp7DHZefJbd/r7Gd9tF1r1qs3GfS1AVwzKM+o604MFli9tkKKHncTqhM8br39FKX1AHCp3In9jMYtZGGxxBT/c=YdsOI6dMUHzMXr9YgWMRhYy4gLOWLz00zPMSd8aVXR++jN/YNs4aX+jF0uk2UNmF1lwYKNw1EF2toYubz/TF0z5uc15N4TFAsILWnr4omZyC5R7jOcVSlTgHKG3fYtnPewt3c46v/8WdmWJzo6aZDasmBFi4sgG15gboYx2fL9g=JCqftPiyCuCT9vGREUsyGqbcwKo8CjjrGlniu+07FBnpUzBO0/2uDis/HcVAvJtmPgsPZ7OCfqn+mx+d0rNX9+MnYsdEMiGFqU2yl7BxHyh7s+cZP1tRiskz1CuQxwAe90KGwy5Yb4jwXSQe2+JZcXWLXLi47oNUz1jXG5InM6Q=H3ISFCJuaHj4QAdyWZFbBQkdzD0BH0XPR7yDbEx3hfGkG2L8rJiWg1hB4N6cSSACScHyvxo84BhgkDFU/IIBsrSppznTt2Hd9vbw53KXfJfYIHFYXhhlB7lU/Pkt8VPpQlcW6YdpztjGjJoJHR8QtX48VQozcEWRQ4edyOqKRDU=nsdZViduOL8XR0d4QdyrRsHyzP9uXa5ToWymdZDfg80Q0DoaEkMz2PoWJu6eNXIsZEUbGlVh/qqF+Hfai4+Vk/qbz17IpyjTD3CnbCXSi/YKxsPb5wpzF2AbVzksJtTEKXs39ZtWyDOyOYw9YPUgVzuvDckRId+Th+/Ci5wJxzM=Y2MhLHUJFG9/wlRDgb3OT+IDYDiMg3U0aeFj3RLY4iGqI0vljjr6UdO94ozvCkBi/sjfQKamXvCE7Ru820Qf63dAxrgRDrB3TBVFRrfK/kDuqxkmgDeJB2xuG+fvLl8qJbWXHSYNWJz16uBcqWTH3BR5yrEVd7rkEANmZoBp1zw=YHjNrlxwMBMetLKahzfCCkqyLF5SrkXfx+er3gCFWoIS2McRl5IxxdPHnku7wpwzqQXryJKu+lF8nzr4BDDO5H3G62Y1qiO6ZCEpuSIctLvgnlMdWdMqyNtEIwt0HA5ON2Fq2dTutoZJ/KeO/cT3DZgMQ3nsiSZDKzv02KjFF0A=HN0Qg0HOAb9rGEwYx0kOIXtPHB6vvgOkrPeXgERxnc1UzvkQW7F+Da7KEdpCvaGCL0ZC/4yeI14FvHPrwD/XfH+sm8kVWZZmYSivfv1RpQZXq6BgN6YuyvSuCvCHxz7Q4P7r2P5o881YGAgIpBIgr2W6a7v9weRmNvXZZxFEMGA=F2ineK5rCsxGaKNWt/6a/MyMzPFQDyUnV7Vbbj72goubFK1ZFRU3o9Q4s2sYqrxMsBXpqGfAJTa2sBi/uTAh2lvrqXzgb291VTeJIauXYWHr+TiQfV4ZW+rf0QDR86SLLrrXtad1NOHv9RB9ja3BtHrMWWZmg6FZymE73q/T/9o=", 
        ///     "PaymentUiUrl":"https://secure.spark.net/jdatecom",
        ///     "GlobalLogId":1146715248
        ///  }
        /// }
        /// 
        /// 
        /// Example: Present all available premium a la cartes (member is already a subscriber)
        /// This example, the member is a subscriber and clicks on some link so that they can look at all available premium a la cartes they can add
        /// 
        /// {
        ///    "CancelUrl": "http://www.jdate.com/Applications/MemberServices/MemberServices.aspx",
        ///    "ReturnUrl": "http://www.jdate.com",
        ///    "ConfirmationUrl": "http://www.jdate.com/Applications/Subscription/SubscriptionConfirmation.aspx",
        ///    "DestinationUrl": "http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?memberID=411",
        ///    "PrtId": "10042",
        ///    "SrId": "411",
        ///    "clientip": "100.100.100.100",
        ///    "OmnitureVariablesJson": "{\"evar10\":\"value1\",\"evar11\":\"value2\"}",
        ///    "MemberLevelTrackingLastApplication": "1",
        ///    "MemberLevelTrackingIsMobileDevice": "false",
        ///    "MemberLevelTrackingIsTablet": "false",
        ///    "PaymentUiPostDataType": "3", //a la carte
        ///    "PromoType": "0", //4: mobile, 0: member (used by FWS)
        ///    "PaymentType": "1", //1: credit card, 2: check
        ///    "TemplateID": "120",
        ///}
        ///
        /// RESPONSE
        /// 
        /// {
        /// "code":200,
        /// "status":"OK",
        /// "data":{
        ///     "Json":"T/IHoEfVFW/aoJH8Urrk+TXjETByGRxgx8bt5zrMfyW3eF4gm2QKFlZeFm16ycV9sArq0PA3X7/peDFaDfI3JF+H2bmz9IBhFgDLpYEhWkeXxNCk1WmHfWvg/1lpqMaGV68KSkENyk6W56CTjwdH07KWdHuV9rgI/zbB2U+7SeA=YYck1rtkrun7t5rSV6+0pqgyaH1ESLfa4cM36WswJiirHxXpbXmCB3aKYKEoy6krLoni4RgglYQUm1vdiIsthAupz7puzaeQ3nXOhKxNgkVL2bWRAM3pzOU1ctX0spkHcQtwrcDZDGZXFf7Q4/wgLu8v9lEjm4PlDjfZRWKpzGM=nLZBFNiloab/Ma0/eNxCBDqImw1Skaqw5oY1vBbIjEf7acSGPYxEb8WJ1i+1u7ssB/pSBHIO9iOZq/Uq+82SWAtnkLxFH8o8csYKu7AtIZ8m2VfVpVuB/SVLKPC8Sru+IkdZNgZe+I1vZwKASRtGWeQ0xNvIFqi7jCpH3NNTW0s=f62zZRbx7NPy6JWNlEq2bZxHefR2D+2D3XvLgm9Vpyou1/2IuomxKHUxbyjDVx7WczPSQQKLMTfB+45L6UtduQwNUzfEK9j4jaQ5Kv//L0I5op2Q6tACK2CGh3C4NjsmnM3cLw7vUk7591hhFUmALEYuMZcnj2fXJFn7EEriRxU=FEVz1jmeRALp8sgfvV6yRiiAL0O7BL1w0nfZQPFTYB/ywy1/8KwhzPLCpkifeDHmFVfJOdODsGuR1CbOD8Tc2WnxkgqvvyvQf2YHNNdPtdAjTBil9Fh6vBmhZcYfLOBl94sLT+F28FSow1Y4SycDzaJzG8CmIApR0b4ydITbno4=kvTBEkqoPhrfTP2SFs178WRvuvB4woFz7hTdFtaVK1cZ10lrhJAnCirA/B5r1le64MkrHVc2FwcwTfU0NLK7Q2qwjpp21z0apvKfFfgB6TpbrKUFgCIjb5WHEj2mxvurQ9JJjJftBNWNT32O2XKxxvEnlXmT08QyKIjJdL87lNg=eocryWVlnNlUTGAwzeknZCYHdDHYkPx0xb3BkGrJq2sw9/njeD7CqozG4267xY06sLOy7ak4wwZ0t/4Xtvij5Zmncyl9nwJU09qz35nWDBFXxsJLO+qFJb2ZswJeTUujcGepGMyGBMse2BXUPqAIVCPb9G7dYacpKWCZ5rhVEBg=k6LaetgO4/L6pUdjIBLUD3YCa1KChUvvnldNEe7buM2aQptDHzSjIpoImq1i2ZLPC/SspeyUVoaPP7CnEkY786fFEyBbw43aqcOvkqBv/RHlr7yhCdTR6N8Mkm1inDXquyoHpwxeFjY+dtk6V931hWnuctUMNY5Lf+Hwl+qGeJA=nW0CW13YnOo1vEHq3ovDyw4ZyhXwBqgTmI8XIahxPoEVNFGVoBHxq3oTfZiZDV5jjHlL7naGVQ3cz8suUWcep5IRpa6bqFfekzOVNYg2aYiQnF3YjCgy89f/U2/eFIcdvZxRrZ280iIlBTBn5cNBua2YYGA/T+HEObHOhfqz7fU=AjvBJQYEeyMhJtK3eZiFBQ46PnQLdqB3hX4jipCVEmmeiFBur1eegtet3YQJWRKPrm5qWOsw9GFi8UvCR9ecK1Gj1SSvs4rJtpqYKlbBg02OFSQNAk/qZ3fOiO/gqorOaLpw491Qg/Hts7OKIfwQ4CoFsz0IdjNSSDSsFJRxDVc=RHRjL0155s6quCAJGMn8M6m4Hq+c0xqvnzUb1lhwPCVswLSIMbC1p51/6vaUWYYrdKcc5FYQCAmUxpZDv02otX3lrs2wKbbVt00xT4KL4RxGIaEfJa6bhR79SM1pCR13N+QlmF1WbIKQEIRfgY+RJIZ28v1nfZF1g1Q7kejiWJ8=Z+lbvqjM/9EqTshm+0svoxgPkvAK/bDa8PCflQwWGuIgECrJgpNxTbNzNMtdy9NWKIrsRlQWOOK6Y60CoaVbkmR9tIXyjf3wBijh9GG5TbWr3m88vrkyao0c9YnDJnoKgtcOpbcX6wmEGEVQvAeymxxHBr2sTCxKGBZp8Byq1Ko=dpTykOGYS8EUyHzC30wOYdxF+cte4KweWBMW+EG6BpB1kyT1UghkId7d9loX2RgJxzmNO7jJy+eaUeSmDiNQb4Qv4OT6WJa22BZTUIrUMeYH0mQx32UbDKUTWb7SBvqgqt1oSkxzvY4KaZmfy6boPywiiF3IUkP637v4Rad1AS8=pOZoOD5ZvOT4qkvaAzdO2zBXaydrKAFTnj0FPkVmxB9/A6KTw8wc8osn1l/SGs9ZbvEf4exen5kJUaqhWARAFkVzSeNtSRLt8BHBEzeaYpgc2uFIGpZxXofGxfeFniEWfd9mgmjQAmIrgUwEiSwQeqfZCNWnVN4aYvU/RBhvFzA=W7/8enPeZCa2l1XB2KrN0W7bFFRgr/X73ENpZyYqDhrptIJ89yGa1vr2jzFEjbwhyO/AQ/DB97uXGgBAVMsOdLWG6y5E0Rgkm0PKeGCqMidr0ggNeRX1K6mqmYq8z3CcXXZ5jgCGUnN0Q2QhThmCWJ+0BWRDNZOarCcpyJSXlOg=au8O8rWFgSFa1AY95+Jxpd4GgcZ4ow2phRhQ+xLvRIyMTiWIpJwxoK5y+02e/uEfVCaYfSMDOnqu3at5i/Nt5SVLVjdQlQQFTuigF+oMpwXGCStaH85dtyU9FOYE6cy3gmFtl0mqV3UKRj/Ng7nO+tN4QJ7ERfDIiMJBKbW1BRw=tFC4kqU4lDtjOx4J4MgqfKWmJhQgkNVa9yNYFRtv8mnmrq7wgoYMqpw5iXqi6L/kKtBAY6t3wOGWtoq/gD77FGOV0IqfrU+3L00m9vLpwJhXSXmsiG5bAahkZP7mJWX7hIIJZMh2FAZ+gT9Oyfy1ZpM6kcPQWn/nurqGqkwggZY=j1drTYPLZdzjDbQdoBN53owTEZhvOTl2wFC+zsogjGovRDqjvha/ebw5tn1ADWkgwIMSAePkUrMSmGaiMsP9yXj7vHbIsbCUQShvtlj/5+JltSbotsF0epNX0/vGvas2u9YVnyYbBcgdw1pnHZS3AgCAyv9d86gvjh/k/DYKRsQ=u5H62pCo8w2VmFLPF5d35IXNbTf5macXKCSIBex/cHWj7t+aopVijpLsdGd5WGy01XnNbjRy0ORQ4sYq8YbzHc7gQELfSvAT2ZSW7IUw5EkLI1XN1pPxpC71EHjHCvy3BRc9BpO/Bws3cByyGNCBErFuYUWqfa83nQT9S/XGDpE=U+Dqidq70v71FWv5bLny3tJnEoNV5bVCV7XIZtId8sViPoJKxp1HBIJwEp2j1Bu3EhDJm9cGu7ElvpoDCgqnn09WNoD8PG9SwgQwVO+5LVOqucwfn11s3t4DKugoKtbHnoLk3ejH+TQO6GLxR1CXZctR8BubGO5KYu7eVvHrZxw=TjyryhPCq3U/BKLByTYJ7G2pj3zXz8aUrAsLaFbw2cbLcknxHT/BrimgGG7NyUQJjkeWhDQkZLKENc4xls4gS0Nnb4cRllBT6CZt5qsN5jSqmfNjb3kSkTl8ASp73TX1F7Q8e/T7Kzy0JHdDNLXOg/BVk68DiifOlQjSk5VPMSI=cXIBElfFvrU7WswF+Ub87k/1N9NY+QKGms2JBa79Ef5LgODwJLUd3zLZfvPla3GOVZIgI2eKCpY6JMQsCRF+MCC7OnDP2gbb7NsSwiI+Ul3zwkNur4Di3WfAJnl9/ROWZtDiFOLMK3mb65AQyrVP4t0/qP3v1kXfZxbUUS0twpA=WmpKg4Laajiqj7xa+wJPWL3vIwRrUyTwdNXekykZ76Ru+dD2MyxxgDdmY7OW97P5VoZ5OU3JZ6na/gDcX3V8O1A1EqrX4OyzODVRJn3NI7AhdD2lzi/+4sLN64GoFAgRHRG9wDyOdVJbhWxrl/+BkDsPK6rVvSvVAgb8ixv+Xxs=VVD2SE0uu+Pnqwh0ks2A4vABFjqFEd0C8uuWHQ3eBMwtv5q+ULKOZwYQjnkbzxE4v48fOMJyKcs/IS48vxmwmQfr41sjl+ZxCPkKH0rmJcnF26iNZg/bRmrEV4ItBwgPF64RgW5wRFhJwUvSj84gIEieoli+eYaElf5YfftIINk=RSGLBQocOD2s3X/rY2RUjnCatDvzjuN75p37CSqjmY3iKwaqvFwM+6MVrwuhw7IsiZnoJ/b8zKJSCMcYQEzTkX/jA6xYRALbbBqSCui5ulHafDw2hb5yC0XoEkM+Ig0RUvcSrN79vM1HaRNzfJ9dUOw65Ms/6GbYO/TDDeNmhfQ=hvM1zBQFRgEDfkLLKnl799hAUREJHwcsFkUS5U77ZqgAHxRy+nZ4mydh/nfKiTXwGqqg30PK4z/bf3Ll2pOlIz/cVqTR8aP54OBFSyFuXzGgHM1ekx2ob4BgNfHTzRonHpL7KTkM2AXOhjfdbEHQiARKvTgHzi9hnNfKxOFvaSo=h78eZBuDw2PZuMVU8m6tatfTwhxbPRvDSrRPFlhbMHV/osMkAUjVouKJb356zNw5VHRyxIvsMDFbiuS0ww5NHqHmNpCKlqj3Q3SW4wA3DSV2Uhko4ibycLBt1oOjmIdgGfXZxZdUl0BV68MZxvR/uZCUaZLxkfuGYtJlQF2FvPk=gJumo6FQ09q3ln08PLJ92CHnaaTkygnKjj08qS6YPdbFJW2NfK2xNFWu28MJkrSZ4V5/tCnv8vehqYTljcxfDxzGZOqK3EEaDvcJ5joucMWNEhA+slHfqGcBweBxOEoW9T/SMY/YyBhxLISU6WmXZiTsOZZgtcB+BpWZSddsW3o=e+L83nT7Q93CtmruPLll2PjfF19LLJeimEbjC2SzPgM/OuilOaYYHw7weOzhRR9p21mWdOJ4DGcWDwL6bbZ9wUefvcl8f6+/cHgw17dLg39feasLa5sHxN1y3zUUL8Oy2LQteGd1xgMyPooe/EHuuafoka2m1PqYCTiMZ7zOXgw=NSO9Vs+0kTT6ZLgR5QfREOxdYQ+/pMir+61mzTv9KpKQY1g9PUqTBz7Hoo0Apy2Zf/Z0zO9wr114Hp7P2OlW7UE63d2qIKwuklC/VFaFPsIVWAgNkAoJz5kO1mT5KTksuSNskFf8t74hABM0OUl5/WV6NRkxBj1eQyR9V7qNQZc=xkbDzM/LvzuUiDOum60JeY/CDxgtYf7Ti5ghw2BuX+s/la62XNIRfvcVqNk0EwQbhUaR5wURmVMI+bxoFZYB0aouQEL4Kp6N3WcoaJxZ8eWRvTW412bC5Gz2keV4zoV/sGgQxRBel1KAEHq8nA+tUBYpqgewufD3UOP+LYoBcjw=R9Mm7BAian/XZA8jkabCLHppOkk27eEgsRZMJvc8/agP1lzbSZxB2B9zu2iGP/CgY7K9cnSLs0w148k8bnJioJK/N79FkaJOYxeXrwv1AVtbm+4j4B2vVG7Khqci/+CeoPEIayeuG4xUVG0MIHSiPJG+7mn4dXj2RzUdGqB7Lss=tau9Y32wLLP6MQhq4nU166qFFh1J4Q9UomQoqbVo+gVp8jTtU4HrmH0DxWr4QcDP2Ekkf/tczTSVlXixiJhfsfYS2I7K4W8vDxEwEl1b+M6gwSC9E+pNldStvsELJPrW8bNL/a+hq00cC1J0qUGY05+RyHDcbY39GoVCd/cW1iQ=WGOcwtfjbBW7ZqJsR1eaKfZGUrdInb6FoisWhkyS2SNVN9Qiekg2g4DIXmNn2/4q0N/FmUyoiQ7lI0Ayvlg4nMdnf7PK0DXxx2QvnMFR2ERAqUmTXKYehYtDSUYsTJfJWBF22qkHHAWTL5V2j1JEFtAt+WScijqvliGbPUXDp78=e+TpdisRtiVTPtuvqI9BnkG4p9nvObhcb4qO682qS4NgN9UEb7AGyVWc9kNb+PQJ1LEdoCbb0PmAlgYXbJqHZCeZuzZ35kNx/9MF0hCkTsDR/Gju7foYyGDKV6nASmWmHHR66fKB7/NPrFwljoNxI0uPSAY+jqdLTeBrTqu8+SM=lmY4b4w30KNT8xlDGNpdo7e5vb8unR0rq6H5+t9kjUXCRLxLDw8kjLv5A9fMvLRuemzBpHMnGmDxgfZmzIRnelpSMrCSP5rea9VYy4YNVIJo+0sebzak/E1JaHbJAi6C786HIU1WhcfIsyxyj6NuEtFpzpkkL82AvrgGzbok220=SyrQ1zFv7ise+FYoyWYFuJJifRSm4lxaQNIBS6yEJckX6qDU6Z/osRwp+OXHhfj31GapT5dlX+LrOvhH74eYrRehjMqKSWCV9R072RB4lmb13hE0e0WTJzrlkWWgGpps1SN2X3uOCGKYQWR5omKMVeHnvXIVFYBPDFdy/on6QmQ=qxNnsAQWrUiUgg9ujT6s48raaHEtJWo9cGNzYxnpCvEkMQORZj2Xbnn/i0+D32LSUnJyQgRqnGJFTl0Ug4UZXjISfZR81QWkYE8LiDjPAk2yPrtHT7KiDgEYdfgrrlaTdLyB4z+sA8r6OET/tOXAHwCVgiwkcGErOe8pelPCOOA=Xhmlz3SoorIz/u6jmM1I5A8Iq1j6Y6ld9AfUHhamcXR4ty9o0jQxQ8QocfNxiw8Zyu10v8zVMrCJlgbVb5eMd3fKdXkYp6yO2gk+EXQTwustvK8X+1c9KMDgw2T7uZkrJAvgk/mNK6ofpjcZ83U79iuJZ6GzzBpj4Oq9ezn8QM8=uJSFWFOG1ARuohBFSBRGFo7hJmaDdGRsXILxuiavlbwg1FwMXDCKJJI8xkrMG5UXirznUXYe++Tg/IUuhnUFKrTDy0tKDA3WsaDx/Gb/YqE+q4Hg8c3EvqiRwxyVh4Qrl5Nm7G5g0Cm90rMyDF5XZLaO2YRwquZuUAkV7Zjc9Wg=rJfVHVxoDvBb8N4NyuIwXhbQKMY5gb+ghSSNZ6mQH7tatpdUlfYwXCa10e30EmUK3XEZdmVfTNHFC+NYwz5xGA+uyDoVxMYPLPHqCNVjV+Osb4JEPUPwrnJPQ/hcE0PZFWABlL45xWnFyWf91795cevRj8N/CNyul7RW8GSkswM=IDlCVUmFhB6bQ2IYGBKLVn4lLualQjGqIOfRvK1CsB3oaP8s21MpNAByDj4e68zhiuOZ7SBPF8fi2uLniGHoNuzAfcWJU9zm8JwiZx+eiVMHZUrJ1H+2aEjLqN4UwEHfuwUrP116Dd57N1ZHyVu7OoMSxN4liiVHtGSzZsgFacs=JO4X20j6RnU4dlen4Wu0ADz8U7ZuQygBUIXDKtgS4h66hO6BpyIfQ0NE3ictDsIK/Lr9ayW5rzIfnkdrnRgCvti8PBEVJdnd/3lBH2lEQmu8Ws0EbRZ17+PuQFDXJSK55CjULoObEdRHP4nANBX5t16OnUDZyKLbGmzJF+A7VqU=mxv6z2CNLx8tnKlyb5/9Stt9EShIH6cUMFgN5FZh+zwrX0oYKVschGBvHU4ik2/08ZsrZxVh1WBK4bO9j0u7Vj7RWapqi80Nx+Hx2MMzdNudZS2JkPbyLcCthtSDZCBdBUwxcDquliDQ09vRyYYKuZY28edOjqWkp/tuuYalJ2Y=UukWnHoHEZuCW9vp+yoj9Vjbo7NuvrYbK13bOPM2Dlsc7qms4tF8u3J7kr2YTHMSQWSdn5XAxD8zk6vcF8shk+xu6GemhGi4gxkVxpe0eHmoNkXmrS1ZAbzhY/JjEJbaFTxDb1ET7yYd8P7yHUJ2Fy11No7IuVsSoU1I1vVZWfg=AxTNvPYxIV/V4gNruoZE0zANuOkKTJPyjG78oWFhoZkmSFvZCokRpyZtzS6Ex3K2ELZ+LwZg5oVzp81YCwdnJxtB1Zjf2LVQkWqpO8+2dDYGGNDv2AlFwFyQfFq7WZFdpxwdF6S8972yvAo4kNm8BhtelPhWQ1cMb0YIQ9MrXVA=OzhxDeLcrgRjMWk3mCTGbBGITluDYQgaibsjVeZl1hQ6S1AHAHSf3IZWfkkTTKMhVDe41rE5IVwLhzLM+pnO6Xp7DHZefJbd/r7Gd9tF1r1qs3GfS1AVwzKM+o604MFli9tkKKHncTqhM8br39FKX1AHCp3In9jMYtZGGxxBT/c=YdsOI6dMUHzMXr9YgWMRhYy4gLOWLz00zPMSd8aVXR++jN/YNs4aX+jF0uk2UNmF1lwYKNw1EF2toYubz/TF0z5uc15N4TFAsILWnr4omZyC5R7jOcVSlTgHKG3fYtnPewt3c46v/8WdmWJzo6aZDasmBFi4sgG15gboYx2fL9g=JCqftPiyCuCT9vGREUsyGqbcwKo8CjjrGlniu+07FBnpUzBO0/2uDis/HcVAvJtmPgsPZ7OCfqn+mx+d0rNX9+MnYsdEMiGFqU2yl7BxHyh7s+cZP1tRiskz1CuQxwAe90KGwy5Yb4jwXSQe2+JZcXWLXLi47oNUz1jXG5InM6Q=H3ISFCJuaHj4QAdyWZFbBQkdzD0BH0XPR7yDbEx3hfGkG2L8rJiWg1hB4N6cSSACScHyvxo84BhgkDFU/IIBsrSppznTt2Hd9vbw53KXfJfYIHFYXhhlB7lU/Pkt8VPpQlcW6YdpztjGjJoJHR8QtX48VQozcEWRQ4edyOqKRDU=nsdZViduOL8XR0d4QdyrRsHyzP9uXa5ToWymdZDfg80Q0DoaEkMz2PoWJu6eNXIsZEUbGlVh/qqF+Hfai4+Vk/qbz17IpyjTD3CnbCXSi/YKxsPb5wpzF2AbVzksJtTEKXs39ZtWyDOyOYw9YPUgVzuvDckRId+Th+/Ci5wJxzM=Y2MhLHUJFG9/wlRDgb3OT+IDYDiMg3U0aeFj3RLY4iGqI0vljjr6UdO94ozvCkBi/sjfQKamXvCE7Ru820Qf63dAxrgRDrB3TBVFRrfK/kDuqxkmgDeJB2xuG+fvLl8qJbWXHSYNWJz16uBcqWTH3BR5yrEVd7rkEANmZoBp1zw=YHjNrlxwMBMetLKahzfCCkqyLF5SrkXfx+er3gCFWoIS2McRl5IxxdPHnku7wpwzqQXryJKu+lF8nzr4BDDO5H3G62Y1qiO6ZCEpuSIctLvgnlMdWdMqyNtEIwt0HA5ON2Fq2dTutoZJ/KeO/cT3DZgMQ3nsiSZDKzv02KjFF0A=HN0Qg0HOAb9rGEwYx0kOIXtPHB6vvgOkrPeXgERxnc1UzvkQW7F+Da7KEdpCvaGCL0ZC/4yeI14FvHPrwD/XfH+sm8kVWZZmYSivfv1RpQZXq6BgN6YuyvSuCvCHxz7Q4P7r2P5o881YGAgIpBIgr2W6a7v9weRmNvXZZxFEMGA=F2ineK5rCsxGaKNWt/6a/MyMzPFQDyUnV7Vbbj72goubFK1ZFRU3o9Q4s2sYqrxMsBXpqGfAJTa2sBi/uTAh2lvrqXzgb291VTeJIauXYWHr+TiQfV4ZW+rf0QDR86SLLrrXtad1NOHv9RB9ja3BtHrMWWZmg6FZymE73q/T/9o=", 
        ///     "PaymentUiUrl":"https://secure.spark.net/jdatecom",
        ///     "GlobalLogId":1146715248
        ///  }
        /// }
        /// 
        /// 
        /// 
        /// Example: Billing Information
        /// This example gets PUI data so the user can change billing information.
        /// 
        /// POST http://api.stage3.spark.net/v2/brandId/1003/subscription/paymentuiredirectdata?access_token=
        /// 
        /// {
        ///    "CancelUrl": "http://stage3.jdate.com/Applications/MemberServices/MemberServices.aspx?NavPoint=sub",
        ///    "ReturnUrl": "http://stage3.jdate.com",
        ///    "clientip": "100.100.100.100",
        ///    "MemberLevelTrackingLastApplication": "1",
        ///    "MemberLevelTrackingIsMobileDevice": "false",
        ///    "MemberLevelTrackingIsTablet": "false",
        ///    "PaymentUiPostDataType": "4"
        /// }
        /// 
        /// 
        /// RESPONSE
        /// 
        /// {
        ///     "code":200,
        ///     "status":"OK",
        ///     "data":{
        ///         "Json":"VmFiNxsKqlpyCb3BP1+X/TxncgeBuyETdf7AhYjZn0gnUvob5m40L09KIZ0BijreT2o1KR4yB+JwWZiW5OYl8mHm/0DfXPrgN+/9r98wrxW6MILaQnHzy1uvfAx7AdCdocYQ8nyA6XPnVKRKw9lu+TwEXJvv75cBaERK8ico6fM=r+qczJwyNfJoyNxyuxqou4jB5wu+XCkkHjFWWnX3HNQIZTZ8GAu43TNRmR9Ozes477TMoAlYEiu/6nXaix0DnOzUnTPWBSepYryyEeqtkNuyE2tTf0E4ZLDHSQxzYTdFiHQQXhTojMXdJYUkevMQt88kmN5ASFufmad+hiKuDDo=SleZoRXFCo5ktBsbRykhM2W6yIFg9TplpM5jxotng4iq72vBBTZFqYl2xk1BSprQlMrQ+mDzioMmCgfHkFNCuP81UDbaYzA9Ciig8OQMaWHJRpnfIMaV/x0ZDdLJwSevdw4eT2ylj/EFXa2XhJ9jSKxobwOWuVCgZlc1HfSdGsA=hakOyEPQx/Sl98si5c23cB0hhHFZVwK248bNZMmA6mLbU9/WVEtgmoPieegujFCOE5uX8wIXlcCe+hpP9/kkKCckDPt/vsRfiPgtQdT2IYqAH+fnJlA7yn3/3YDMVXr9a8dWA3WTK8150gEBouuWuD139U1tgICfxhEQovyNbmw=s2pDgykA1CvKHZSj7agECASCUKEczmyEJKU/SoFN3r8krPq0W9MlBgiRRcqI5IwSwHzZFIsuhHljPrglc2LJq6E6UK+W27UXbzhxsuoMk0oMEWhOT4VSwNMyaS56gjZVrovKtsvfpfQcpm7SS9gQycx6PebHDv1PGhJjaCbnu+Q=acn5+8wBIe0xwh8CxS9WSsxB0TP0fWHPi8uf8sbtzzvOG+wAauo8rVszqRtU7OcPqj5qF/t9B90eD+HOJ1kRR2S2VuSNlhBsVE+bQUzKvUC+n7p7h+ZA55Slobms37q5WIYbVq3ZiHiq8nhEU1Tmtga1ANf6qCxqciC8dYlb3BM=KFogsm+8FWe6HD0rONNG9XmPhRW+NXisYIfjItsg6XVKYFL/tEFYJjircTmcN+HoCtuvSpbhx2yJ2hlSIkSsgam903QdFwOT1bju3dYdKAJl1Z2kbiiJouaySwdUqTwd5Nrvahg3VvVdrkhCfWnGqX7cb5Jn3+f8Ul5JA1y2Ni4=JAR0slEVL7xWZwoP7A2mpWCEIURpPM3WdkRJGPps0WR1D7ozfg08aGFxFL09f65ZBIsa3kqYu9HEqn6CJIWDw9WeSOVemIw2MJ2agCz3Q4W9ZhfIdE4DRYM8f3+zy9U9tS1awjGr32S2jZj/WocB6+jDuws7wgHHFRU4EWz7ck4=Xoxd0CLV7lkQ8pB+G6fRK3Imddi9iUyOHl4us8d+VdCjtbgtcFPBRqa0lvUnPyw6YvfcL666bxfATds6a39+yoFqxV+fa73aEeJ8OpqSpjWtrlBObKTVVzWDPvXvkjrKLLM6AJggQ/YemubJ4JTgHODi7JIYSu9yoruwiAoCUvU=r3PJgAsPmaWZdIg6pCtsFaAPL8oQ6/PYMpx571bYmfxP7zTbpHmzHr2GUprVM4Rc/We7OREDc3NUNyu65LcDdYRjZn7iHz5AQwsmPVS8pgEClwu/cHMFFvpkqGTnkzeALBoPnxvnXoaKFTaHzd75hC+Mo6jbPjUlXsLMc0S2iq8=v373eqGHa17SKg4yhElb+GyEH3FQ+zoadWK4w9lyOOd0HYpLQcP1MufhZjNSte726Jqd0d7WbnBqBE9NNruY+ugGTQELS0fDLaSyoD8B6Trb+HhYOe+WuolvjFjaqj1J4UYWMyvchKXcb2PA3qDXhDRtFLAF0EK/nwVs9dOXi0A=Fw69BE6/eFXmOn5CKUrmWyA/lZlq2/NyShJDKLPPnbfMRBubFgKm+p/544T1v6ZdwK1Hok6AU7zB9MuK06sJDktv8EXd6hr7vDqjVlskMQyLec5/yevi5299LcEBedwlC5HiJ8grmhy6tV3scAD5saLi19WaQLeDsBl0Cw+nQDw=Gkc9u/v4KUBVTVtkOBtkJzp1Aklc5bAbplfuxezuVAtnHYI0RKf31Tuyv7/w6nsqnRLWcCrOLtEhOVvC35pMSdUJCpAQXZhXwrTqhnNbciIDw64l5aDEe+9Lo3iGSs7dojCt2ZXhEu8MbeIc49zspB9OGvnixXX17+kfoLmfOhE=NP5FO9SkCRrc6iXgpymndDb/XmcnE7ZrW8WqNlFUS+UdnZ/fSP3Fh3LT8UaaywnZ7FJoU9gS85bsbXluJGiX5i0Qr9c37IhAo4gUWZxQwayqUMXfUFKpJ7oZNRSBylNKO1K4NjBhYX1B7a+iwCq7JxnhJUUS4ME063bnyG86cIo=b+jTt6n8lwVvpt2AA533Wpq69/NjqDIG/TAQAzqveyQUMdNA/iY+8hCCQTzucDue+AkkFk8umeK7B4v8qWNbETKcqyntY5GsKQ/NyI755/cp2xJsrshilEyRjRwqB89RnS3LfwmnSaeWqT/LIGvNdPVt4mj8xKBHfUotycWdyH0=IXeOXL7GwpvSAN7IpNFJxzjhPqItJVrWBdscyBdYU4CCCfI0DxKHydim0jG1goFVHNcPeA/lRu/4jvUOcCkj2kETWofPl0s7eoaniW/z+mNlhCYZeCBxdF9hCQIVudYvZxWeZ0KE5afWBkUKmjk/fX3iwj3pMpIOwFI1O0HAjIk=Rb2oTcBClO0q56syQuWcHBg77dJxcBgefg47LoLpJflFbkdfFNq1K5wMsgjH0mXeyDu9BwyS899pcmJ50ogo7MDtx/tguG1MLOxl1/95OSxcQTOaXe9Z4wuewLnFeIzLbf290IKVqXnVdUOl14hzx6TRlnDvhrWpqfzCxVIJAOE=",
        ///         "PaymentUiUrl":"stage3-secure.jdate.com/jdatecom",
        ///         "GlobalLogId":1100013030}
        ///     }
        /// }
        /// 
        /// 
        /// Example: No billing information
        /// You only have billing information with us if you made a free trial or purchased a subscription.  Trying to request billing information otherwise will result
        /// in a response of 400 and a substatus code of 40084
        /// 
        /// {
        ///     "code":400,
        ///     "status":"BadRequest",
        ///     "error":
        ///     {
        ///         "code":40084,
        ///         "message":"Error attempting to access billing information with no past subscription. MemberID: 100067359, SiteID: 103"
        ///     }
        /// }
        /// 
        /// 
        /// </example>
        /// <param name="CancelUrl" required="true">The full URL to redirect the user to if they cancel; usually the URL of page they were looking at prior to subscribing</param>
        /// <param name="ReturnUrl" required="true">The full URL to redirect the user to for various links back to your site; usually the URL back to your site root or homepage</param>
        /// <param name="ConfirmationUrl" required="true">The full URL to redirect the user to after purchasing to show them the confirmation page</param>
        /// <param name="DestinationUrl" required="true">The full or relative URL to redirect the user to after they've seen the confirmation page and hit ok; usually the URL of page they were looking at prior to subscribing</param>
        /// <param name="GiftDestinationUrl" required="false">The full or relative URL to redirect the user for BOGO promos so they can manage their gift; if not specified and the promo is a BOGO, will default to the DestinationUrl</param>
        /// <param name="PrtId" required="true">The purchase reason ID for tracking</param>
        /// <param name="SrId">The subscription reason ID, currently is the memberID of the profile or message that caused you to want to subscribe</param>
        /// <param name="clientIp">The IP of user</param>
        /// <param name="PrtTitle">Not used</param>
        /// <param name="OmnitureVariablesJson">JSON array of additional omniture variable/values pair</param>
        /// <param name="AnalyticVariablesJson">JSON array of additional analytics variable/values pair (initially used for Tealium)</param>
        /// <param name="MemberLevelTrackingLastApplication">Tracking support - last application ID</param>
        /// <param name="MemberLevelTrackingIsMobileDevice" values="true/false as string">Tracking support - are they using a mobile device</param>
        /// <param name="MemberLevelTrackingIsTablet" values="true/false as string">Tracking support - are they using a tablet</param>
        /// <param name="OrderID">Optional OrderID when requested redirect data for Upsale</param>
        /// <param name="PaymentUiPostDataType">The type of data being sent to payment UI; defaults to subscription if no values are specified</param>
        /// <param name="PromoType">The type of of promo to qualify the user for; defaults to mobile promos if no values are specified</param>
        /// <param name="PaymentType">The type of payment to filter promos; default is Credit Card if no values are specified</param>
        /// <param name="TemplateID">This is the PUI templateID.  
        /// Used for PaymentUiPostDataType premium services. Default is 121.  
        /// Used for PaymentUiPostDataType billing information.  Default is 110.
        /// Used for PaymentUiPostDataType upsale. Default is 400 for variable, 410 for fixed pricing
        /// The other paymentUIPostDataType uses template ID that comes from the promo.</param>
        /// <param name="PromoID">Used to pass in a promoID that should be used, if valid.  Only available when PaymentUiPostDataType is for Subscription.</param>
        /// <param name="PremiumType">Used to restrict plans for Upsale or Premium services a la carte.  By default, all premiums will be included.</param>
        /// <param name="MemberID">Not used</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <paramsAdditionalInfo></paramsAdditionalInfo>
        /// <responseAdditionalInfo>JSON Object consisting of member specific promo needed to post to UPS when redirecting to Payment UI.</responseAdditionalInfo>
        /// <returns>JSON Object consisting of member specific promo needed to post to UPS when redirecting to Payment UI.</returns>
        [ApiMethod(ResponseType = typeof (PaymentUiPostData))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetPaymentUiRedirectData(PaymentUiPostDataRequest request)
        {
            if (request.MemberId <= 0)
            {
                request.MemberId = GetMemberId();
            }

            if (!IPBlockerServiceAdapter.Instance.CheckIPAccess(request.ClientIp, request.Brand.BrandID, IPBlockerAccessType.Subscription))
            {
                return new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.IPBlocked, string.Format("IP Blocked. IP: {0}, MemberID: {1}, SiteID: {2}", request.ClientIp, request.MemberId, request.Brand.Site.SiteID));
            }

            var data = UnifiedPaymentSystemHelper.Instance.GetPaymentUiPostData(request, false);

            return new NewtonsoftJsonResult {Result = data};
        }

        /// <summary>
        /// Admin version
        /// 
        /// Gets JSON Object consisting of an encrypted string that has member specific promo and other relevant data needed to post to UPS when redirecting to Payment UI. The API automatically determines the highest priority promo available to the user.
        /// There are 3 types of redirect data supported, based on PaymentUiPostDataType parameter: subscription (default), upsale and premium services (a la carte)
        /// </summary>
        /// <example>
        /// POST http://api.spark.net/v2/brandId/1003/subscription/adminpaymentuiredirectdata?applicationId=1016&amp;client_secret=
        /// 
        /// {
        ///    "CancelUrl": "http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?memberID=411",
        ///    "ReturnUrl": "http://www.jdate.com",
        ///    "ConfirmationUrl": "http://www.jdate.com/Applications/Subscription/SubscriptionConfirmation.aspx",
        ///    "DestinationUrl": "http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?memberID=411",
        ///    "PrtId": "10042",
        ///    "SrId": "411",
        ///    "clientip": "100.100.100.100",
        ///    "OmnitureVariablesJson": "{\"evar10\":\"value1\",\"evar11\":\"value2\"}",
        ///    "MemberLevelTrackingLastApplication": "1000",
        ///    "MemberLevelTrackingIsMobileDevice": "true",
        ///    "MemberLevelTrackingIsTablet": "true",
        ///    "PaymentUiPostDataType": "1",
        ///    "PromoType": "0" //0: member promo, 1: admin promo, 5: supervisor promo,
        ///    "PaymentType": "1" //1: credit card, 2: check
        ///    "AdminMemberID": 411,
        ///    "AdminDomainName": "matchnet\\dbremer",
        ///    "MemberID":411
        /// }
        ///
        /// RESPONSE
        /// 
        /// {
        /// "code":200,
        /// "status":"OK",
        /// "data":{
        ///     "Json":"T/IHoEfVFW/aoJH8Urrk+TXjETByGRxgx8bt5zrMfyW3eF4gm2QKFlZeFm16ycV9sArq0PA3X7/peDFaDfI3JF+H2bmz9IBhFgDLpYEhWkeXxNCk1WmHfWvg/1lpqMaGV68KSkENyk6W56CTjwdH07KWdHuV9rgI/zbB2U+7SeA=YYck1rtkrun7t5rSV6+0pqgyaH1ESLfa4cM36WswJiirHxXpbXmCB3aKYKEoy6krLoni4RgglYQUm1vdiIsthAupz7puzaeQ3nXOhKxNgkVL2bWRAM3pzOU1ctX0spkHcQtwrcDZDGZXFf7Q4/wgLu8v9lEjm4PlDjfZRWKpzGM=nLZBFNiloab/Ma0/eNxCBDqImw1Skaqw5oY1vBbIjEf7acSGPYxEb8WJ1i+1u7ssB/pSBHIO9iOZq/Uq+82SWAtnkLxFH8o8csYKu7AtIZ8m2VfVpVuB/SVLKPC8Sru+IkdZNgZe+I1vZwKASRtGWeQ0xNvIFqi7jCpH3NNTW0s=f62zZRbx7NPy6JWNlEq2bZxHefR2D+2D3XvLgm9Vpyou1/2IuomxKHUxbyjDVx7WczPSQQKLMTfB+45L6UtduQwNUzfEK9j4jaQ5Kv//L0I5op2Q6tACK2CGh3C4NjsmnM3cLw7vUk7591hhFUmALEYuMZcnj2fXJFn7EEriRxU=FEVz1jmeRALp8sgfvV6yRiiAL0O7BL1w0nfZQPFTYB/ywy1/8KwhzPLCpkifeDHmFVfJOdODsGuR1CbOD8Tc2WnxkgqvvyvQf2YHNNdPtdAjTBil9Fh6vBmhZcYfLOBl94sLT+F28FSow1Y4SycDzaJzG8CmIApR0b4ydITbno4=kvTBEkqoPhrfTP2SFs178WRvuvB4woFz7hTdFtaVK1cZ10lrhJAnCirA/B5r1le64MkrHVc2FwcwTfU0NLK7Q2qwjpp21z0apvKfFfgB6TpbrKUFgCIjb5WHEj2mxvurQ9JJjJftBNWNT32O2XKxxvEnlXmT08QyKIjJdL87lNg=eocryWVlnNlUTGAwzeknZCYHdDHYkPx0xb3BkGrJq2sw9/njeD7CqozG4267xY06sLOy7ak4wwZ0t/4Xtvij5Zmncyl9nwJU09qz35nWDBFXxsJLO+qFJb2ZswJeTUujcGepGMyGBMse2BXUPqAIVCPb9G7dYacpKWCZ5rhVEBg=k6LaetgO4/L6pUdjIBLUD3YCa1KChUvvnldNEe7buM2aQptDHzSjIpoImq1i2ZLPC/SspeyUVoaPP7CnEkY786fFEyBbw43aqcOvkqBv/RHlr7yhCdTR6N8Mkm1inDXquyoHpwxeFjY+dtk6V931hWnuctUMNY5Lf+Hwl+qGeJA=nW0CW13YnOo1vEHq3ovDyw4ZyhXwBqgTmI8XIahxPoEVNFGVoBHxq3oTfZiZDV5jjHlL7naGVQ3cz8suUWcep5IRpa6bqFfekzOVNYg2aYiQnF3YjCgy89f/U2/eFIcdvZxRrZ280iIlBTBn5cNBua2YYGA/T+HEObHOhfqz7fU=AjvBJQYEeyMhJtK3eZiFBQ46PnQLdqB3hX4jipCVEmmeiFBur1eegtet3YQJWRKPrm5qWOsw9GFi8UvCR9ecK1Gj1SSvs4rJtpqYKlbBg02OFSQNAk/qZ3fOiO/gqorOaLpw491Qg/Hts7OKIfwQ4CoFsz0IdjNSSDSsFJRxDVc=RHRjL0155s6quCAJGMn8M6m4Hq+c0xqvnzUb1lhwPCVswLSIMbC1p51/6vaUWYYrdKcc5FYQCAmUxpZDv02otX3lrs2wKbbVt00xT4KL4RxGIaEfJa6bhR79SM1pCR13N+QlmF1WbIKQEIRfgY+RJIZ28v1nfZF1g1Q7kejiWJ8=Z+lbvqjM/9EqTshm+0svoxgPkvAK/bDa8PCflQwWGuIgECrJgpNxTbNzNMtdy9NWKIrsRlQWOOK6Y60CoaVbkmR9tIXyjf3wBijh9GG5TbWr3m88vrkyao0c9YnDJnoKgtcOpbcX6wmEGEVQvAeymxxHBr2sTCxKGBZp8Byq1Ko=dpTykOGYS8EUyHzC30wOYdxF+cte4KweWBMW+EG6BpB1kyT1UghkId7d9loX2RgJxzmNO7jJy+eaUeSmDiNQb4Qv4OT6WJa22BZTUIrUMeYH0mQx32UbDKUTWb7SBvqgqt1oSkxzvY4KaZmfy6boPywiiF3IUkP637v4Rad1AS8=pOZoOD5ZvOT4qkvaAzdO2zBXaydrKAFTnj0FPkVmxB9/A6KTw8wc8osn1l/SGs9ZbvEf4exen5kJUaqhWARAFkVzSeNtSRLt8BHBEzeaYpgc2uFIGpZxXofGxfeFniEWfd9mgmjQAmIrgUwEiSwQeqfZCNWnVN4aYvU/RBhvFzA=W7/8enPeZCa2l1XB2KrN0W7bFFRgr/X73ENpZyYqDhrptIJ89yGa1vr2jzFEjbwhyO/AQ/DB97uXGgBAVMsOdLWG6y5E0Rgkm0PKeGCqMidr0ggNeRX1K6mqmYq8z3CcXXZ5jgCGUnN0Q2QhThmCWJ+0BWRDNZOarCcpyJSXlOg=au8O8rWFgSFa1AY95+Jxpd4GgcZ4ow2phRhQ+xLvRIyMTiWIpJwxoK5y+02e/uEfVCaYfSMDOnqu3at5i/Nt5SVLVjdQlQQFTuigF+oMpwXGCStaH85dtyU9FOYE6cy3gmFtl0mqV3UKRj/Ng7nO+tN4QJ7ERfDIiMJBKbW1BRw=tFC4kqU4lDtjOx4J4MgqfKWmJhQgkNVa9yNYFRtv8mnmrq7wgoYMqpw5iXqi6L/kKtBAY6t3wOGWtoq/gD77FGOV0IqfrU+3L00m9vLpwJhXSXmsiG5bAahkZP7mJWX7hIIJZMh2FAZ+gT9Oyfy1ZpM6kcPQWn/nurqGqkwggZY=j1drTYPLZdzjDbQdoBN53owTEZhvOTl2wFC+zsogjGovRDqjvha/ebw5tn1ADWkgwIMSAePkUrMSmGaiMsP9yXj7vHbIsbCUQShvtlj/5+JltSbotsF0epNX0/vGvas2u9YVnyYbBcgdw1pnHZS3AgCAyv9d86gvjh/k/DYKRsQ=u5H62pCo8w2VmFLPF5d35IXNbTf5macXKCSIBex/cHWj7t+aopVijpLsdGd5WGy01XnNbjRy0ORQ4sYq8YbzHc7gQELfSvAT2ZSW7IUw5EkLI1XN1pPxpC71EHjHCvy3BRc9BpO/Bws3cByyGNCBErFuYUWqfa83nQT9S/XGDpE=U+Dqidq70v71FWv5bLny3tJnEoNV5bVCV7XIZtId8sViPoJKxp1HBIJwEp2j1Bu3EhDJm9cGu7ElvpoDCgqnn09WNoD8PG9SwgQwVO+5LVOqucwfn11s3t4DKugoKtbHnoLk3ejH+TQO6GLxR1CXZctR8BubGO5KYu7eVvHrZxw=TjyryhPCq3U/BKLByTYJ7G2pj3zXz8aUrAsLaFbw2cbLcknxHT/BrimgGG7NyUQJjkeWhDQkZLKENc4xls4gS0Nnb4cRllBT6CZt5qsN5jSqmfNjb3kSkTl8ASp73TX1F7Q8e/T7Kzy0JHdDNLXOg/BVk68DiifOlQjSk5VPMSI=cXIBElfFvrU7WswF+Ub87k/1N9NY+QKGms2JBa79Ef5LgODwJLUd3zLZfvPla3GOVZIgI2eKCpY6JMQsCRF+MCC7OnDP2gbb7NsSwiI+Ul3zwkNur4Di3WfAJnl9/ROWZtDiFOLMK3mb65AQyrVP4t0/qP3v1kXfZxbUUS0twpA=WmpKg4Laajiqj7xa+wJPWL3vIwRrUyTwdNXekykZ76Ru+dD2MyxxgDdmY7OW97P5VoZ5OU3JZ6na/gDcX3V8O1A1EqrX4OyzODVRJn3NI7AhdD2lzi/+4sLN64GoFAgRHRG9wDyOdVJbhWxrl/+BkDsPK6rVvSvVAgb8ixv+Xxs=VVD2SE0uu+Pnqwh0ks2A4vABFjqFEd0C8uuWHQ3eBMwtv5q+ULKOZwYQjnkbzxE4v48fOMJyKcs/IS48vxmwmQfr41sjl+ZxCPkKH0rmJcnF26iNZg/bRmrEV4ItBwgPF64RgW5wRFhJwUvSj84gIEieoli+eYaElf5YfftIINk=RSGLBQocOD2s3X/rY2RUjnCatDvzjuN75p37CSqjmY3iKwaqvFwM+6MVrwuhw7IsiZnoJ/b8zKJSCMcYQEzTkX/jA6xYRALbbBqSCui5ulHafDw2hb5yC0XoEkM+Ig0RUvcSrN79vM1HaRNzfJ9dUOw65Ms/6GbYO/TDDeNmhfQ=hvM1zBQFRgEDfkLLKnl799hAUREJHwcsFkUS5U77ZqgAHxRy+nZ4mydh/nfKiTXwGqqg30PK4z/bf3Ll2pOlIz/cVqTR8aP54OBFSyFuXzGgHM1ekx2ob4BgNfHTzRonHpL7KTkM2AXOhjfdbEHQiARKvTgHzi9hnNfKxOFvaSo=h78eZBuDw2PZuMVU8m6tatfTwhxbPRvDSrRPFlhbMHV/osMkAUjVouKJb356zNw5VHRyxIvsMDFbiuS0ww5NHqHmNpCKlqj3Q3SW4wA3DSV2Uhko4ibycLBt1oOjmIdgGfXZxZdUl0BV68MZxvR/uZCUaZLxkfuGYtJlQF2FvPk=gJumo6FQ09q3ln08PLJ92CHnaaTkygnKjj08qS6YPdbFJW2NfK2xNFWu28MJkrSZ4V5/tCnv8vehqYTljcxfDxzGZOqK3EEaDvcJ5joucMWNEhA+slHfqGcBweBxOEoW9T/SMY/YyBhxLISU6WmXZiTsOZZgtcB+BpWZSddsW3o=e+L83nT7Q93CtmruPLll2PjfF19LLJeimEbjC2SzPgM/OuilOaYYHw7weOzhRR9p21mWdOJ4DGcWDwL6bbZ9wUefvcl8f6+/cHgw17dLg39feasLa5sHxN1y3zUUL8Oy2LQteGd1xgMyPooe/EHuuafoka2m1PqYCTiMZ7zOXgw=NSO9Vs+0kTT6ZLgR5QfREOxdYQ+/pMir+61mzTv9KpKQY1g9PUqTBz7Hoo0Apy2Zf/Z0zO9wr114Hp7P2OlW7UE63d2qIKwuklC/VFaFPsIVWAgNkAoJz5kO1mT5KTksuSNskFf8t74hABM0OUl5/WV6NRkxBj1eQyR9V7qNQZc=xkbDzM/LvzuUiDOum60JeY/CDxgtYf7Ti5ghw2BuX+s/la62XNIRfvcVqNk0EwQbhUaR5wURmVMI+bxoFZYB0aouQEL4Kp6N3WcoaJxZ8eWRvTW412bC5Gz2keV4zoV/sGgQxRBel1KAEHq8nA+tUBYpqgewufD3UOP+LYoBcjw=R9Mm7BAian/XZA8jkabCLHppOkk27eEgsRZMJvc8/agP1lzbSZxB2B9zu2iGP/CgY7K9cnSLs0w148k8bnJioJK/N79FkaJOYxeXrwv1AVtbm+4j4B2vVG7Khqci/+CeoPEIayeuG4xUVG0MIHSiPJG+7mn4dXj2RzUdGqB7Lss=tau9Y32wLLP6MQhq4nU166qFFh1J4Q9UomQoqbVo+gVp8jTtU4HrmH0DxWr4QcDP2Ekkf/tczTSVlXixiJhfsfYS2I7K4W8vDxEwEl1b+M6gwSC9E+pNldStvsELJPrW8bNL/a+hq00cC1J0qUGY05+RyHDcbY39GoVCd/cW1iQ=WGOcwtfjbBW7ZqJsR1eaKfZGUrdInb6FoisWhkyS2SNVN9Qiekg2g4DIXmNn2/4q0N/FmUyoiQ7lI0Ayvlg4nMdnf7PK0DXxx2QvnMFR2ERAqUmTXKYehYtDSUYsTJfJWBF22qkHHAWTL5V2j1JEFtAt+WScijqvliGbPUXDp78=e+TpdisRtiVTPtuvqI9BnkG4p9nvObhcb4qO682qS4NgN9UEb7AGyVWc9kNb+PQJ1LEdoCbb0PmAlgYXbJqHZCeZuzZ35kNx/9MF0hCkTsDR/Gju7foYyGDKV6nASmWmHHR66fKB7/NPrFwljoNxI0uPSAY+jqdLTeBrTqu8+SM=lmY4b4w30KNT8xlDGNpdo7e5vb8unR0rq6H5+t9kjUXCRLxLDw8kjLv5A9fMvLRuemzBpHMnGmDxgfZmzIRnelpSMrCSP5rea9VYy4YNVIJo+0sebzak/E1JaHbJAi6C786HIU1WhcfIsyxyj6NuEtFpzpkkL82AvrgGzbok220=SyrQ1zFv7ise+FYoyWYFuJJifRSm4lxaQNIBS6yEJckX6qDU6Z/osRwp+OXHhfj31GapT5dlX+LrOvhH74eYrRehjMqKSWCV9R072RB4lmb13hE0e0WTJzrlkWWgGpps1SN2X3uOCGKYQWR5omKMVeHnvXIVFYBPDFdy/on6QmQ=qxNnsAQWrUiUgg9ujT6s48raaHEtJWo9cGNzYxnpCvEkMQORZj2Xbnn/i0+D32LSUnJyQgRqnGJFTl0Ug4UZXjISfZR81QWkYE8LiDjPAk2yPrtHT7KiDgEYdfgrrlaTdLyB4z+sA8r6OET/tOXAHwCVgiwkcGErOe8pelPCOOA=Xhmlz3SoorIz/u6jmM1I5A8Iq1j6Y6ld9AfUHhamcXR4ty9o0jQxQ8QocfNxiw8Zyu10v8zVMrCJlgbVb5eMd3fKdXkYp6yO2gk+EXQTwustvK8X+1c9KMDgw2T7uZkrJAvgk/mNK6ofpjcZ83U79iuJZ6GzzBpj4Oq9ezn8QM8=uJSFWFOG1ARuohBFSBRGFo7hJmaDdGRsXILxuiavlbwg1FwMXDCKJJI8xkrMG5UXirznUXYe++Tg/IUuhnUFKrTDy0tKDA3WsaDx/Gb/YqE+q4Hg8c3EvqiRwxyVh4Qrl5Nm7G5g0Cm90rMyDF5XZLaO2YRwquZuUAkV7Zjc9Wg=rJfVHVxoDvBb8N4NyuIwXhbQKMY5gb+ghSSNZ6mQH7tatpdUlfYwXCa10e30EmUK3XEZdmVfTNHFC+NYwz5xGA+uyDoVxMYPLPHqCNVjV+Osb4JEPUPwrnJPQ/hcE0PZFWABlL45xWnFyWf91795cevRj8N/CNyul7RW8GSkswM=IDlCVUmFhB6bQ2IYGBKLVn4lLualQjGqIOfRvK1CsB3oaP8s21MpNAByDj4e68zhiuOZ7SBPF8fi2uLniGHoNuzAfcWJU9zm8JwiZx+eiVMHZUrJ1H+2aEjLqN4UwEHfuwUrP116Dd57N1ZHyVu7OoMSxN4liiVHtGSzZsgFacs=JO4X20j6RnU4dlen4Wu0ADz8U7ZuQygBUIXDKtgS4h66hO6BpyIfQ0NE3ictDsIK/Lr9ayW5rzIfnkdrnRgCvti8PBEVJdnd/3lBH2lEQmu8Ws0EbRZ17+PuQFDXJSK55CjULoObEdRHP4nANBX5t16OnUDZyKLbGmzJF+A7VqU=mxv6z2CNLx8tnKlyb5/9Stt9EShIH6cUMFgN5FZh+zwrX0oYKVschGBvHU4ik2/08ZsrZxVh1WBK4bO9j0u7Vj7RWapqi80Nx+Hx2MMzdNudZS2JkPbyLcCthtSDZCBdBUwxcDquliDQ09vRyYYKuZY28edOjqWkp/tuuYalJ2Y=UukWnHoHEZuCW9vp+yoj9Vjbo7NuvrYbK13bOPM2Dlsc7qms4tF8u3J7kr2YTHMSQWSdn5XAxD8zk6vcF8shk+xu6GemhGi4gxkVxpe0eHmoNkXmrS1ZAbzhY/JjEJbaFTxDb1ET7yYd8P7yHUJ2Fy11No7IuVsSoU1I1vVZWfg=AxTNvPYxIV/V4gNruoZE0zANuOkKTJPyjG78oWFhoZkmSFvZCokRpyZtzS6Ex3K2ELZ+LwZg5oVzp81YCwdnJxtB1Zjf2LVQkWqpO8+2dDYGGNDv2AlFwFyQfFq7WZFdpxwdF6S8972yvAo4kNm8BhtelPhWQ1cMb0YIQ9MrXVA=OzhxDeLcrgRjMWk3mCTGbBGITluDYQgaibsjVeZl1hQ6S1AHAHSf3IZWfkkTTKMhVDe41rE5IVwLhzLM+pnO6Xp7DHZefJbd/r7Gd9tF1r1qs3GfS1AVwzKM+o604MFli9tkKKHncTqhM8br39FKX1AHCp3In9jMYtZGGxxBT/c=YdsOI6dMUHzMXr9YgWMRhYy4gLOWLz00zPMSd8aVXR++jN/YNs4aX+jF0uk2UNmF1lwYKNw1EF2toYubz/TF0z5uc15N4TFAsILWnr4omZyC5R7jOcVSlTgHKG3fYtnPewt3c46v/8WdmWJzo6aZDasmBFi4sgG15gboYx2fL9g=JCqftPiyCuCT9vGREUsyGqbcwKo8CjjrGlniu+07FBnpUzBO0/2uDis/HcVAvJtmPgsPZ7OCfqn+mx+d0rNX9+MnYsdEMiGFqU2yl7BxHyh7s+cZP1tRiskz1CuQxwAe90KGwy5Yb4jwXSQe2+JZcXWLXLi47oNUz1jXG5InM6Q=H3ISFCJuaHj4QAdyWZFbBQkdzD0BH0XPR7yDbEx3hfGkG2L8rJiWg1hB4N6cSSACScHyvxo84BhgkDFU/IIBsrSppznTt2Hd9vbw53KXfJfYIHFYXhhlB7lU/Pkt8VPpQlcW6YdpztjGjJoJHR8QtX48VQozcEWRQ4edyOqKRDU=nsdZViduOL8XR0d4QdyrRsHyzP9uXa5ToWymdZDfg80Q0DoaEkMz2PoWJu6eNXIsZEUbGlVh/qqF+Hfai4+Vk/qbz17IpyjTD3CnbCXSi/YKxsPb5wpzF2AbVzksJtTEKXs39ZtWyDOyOYw9YPUgVzuvDckRId+Th+/Ci5wJxzM=Y2MhLHUJFG9/wlRDgb3OT+IDYDiMg3U0aeFj3RLY4iGqI0vljjr6UdO94ozvCkBi/sjfQKamXvCE7Ru820Qf63dAxrgRDrB3TBVFRrfK/kDuqxkmgDeJB2xuG+fvLl8qJbWXHSYNWJz16uBcqWTH3BR5yrEVd7rkEANmZoBp1zw=YHjNrlxwMBMetLKahzfCCkqyLF5SrkXfx+er3gCFWoIS2McRl5IxxdPHnku7wpwzqQXryJKu+lF8nzr4BDDO5H3G62Y1qiO6ZCEpuSIctLvgnlMdWdMqyNtEIwt0HA5ON2Fq2dTutoZJ/KeO/cT3DZgMQ3nsiSZDKzv02KjFF0A=HN0Qg0HOAb9rGEwYx0kOIXtPHB6vvgOkrPeXgERxnc1UzvkQW7F+Da7KEdpCvaGCL0ZC/4yeI14FvHPrwD/XfH+sm8kVWZZmYSivfv1RpQZXq6BgN6YuyvSuCvCHxz7Q4P7r2P5o881YGAgIpBIgr2W6a7v9weRmNvXZZxFEMGA=F2ineK5rCsxGaKNWt/6a/MyMzPFQDyUnV7Vbbj72goubFK1ZFRU3o9Q4s2sYqrxMsBXpqGfAJTa2sBi/uTAh2lvrqXzgb291VTeJIauXYWHr+TiQfV4ZW+rf0QDR86SLLrrXtad1NOHv9RB9ja3BtHrMWWZmg6FZymE73q/T/9o=", 
        ///     "PaymentUiUrl":"https://secure.spark.net/jdatecom",
        ///     "GlobalLogId":1146715248
        ///  }
        /// }
        /// 
        /// </example>
        /// <param name="CancelUrl" required="true">The full URL to redirect the user to if they cancel; usually the URL of page they were looking at prior to subscribing</param>
        /// <param name="ReturnUrl" required="true">The full URL to redirect the user to for various links back to your site; usually the URL back to your site root or homepage</param>
        /// <param name="ConfirmationUrl" required="true">The full URL to redirect the user to after purchasing to show them the confirmation page</param>
        /// <param name="DestinationUrl" required="true">The full or relative URL to redirect the user to after they've seen the confirmation page and hit ok; usually the URL of page they were looking at prior to subscribing</param>
        /// <param name="GiftDestinationUrl" required="false">The full or relative URL to redirect the user for BOGO promos so they can manage their gift; if not specified and the promo is a BOGO, will default to the DestinationUrl</param>
        /// <param name="PrtId" required="true">The purchase reason ID for tracking</param>
        /// <param name="SrId">The subscription reason ID, currently is the memberID of the profile or message that caused you to want to subscribe</param>
        /// <param name="clientIp">The IP of user</param>
        /// <param name="PrtTitle">Not used</param>
        /// <param name="OmnitureVariablesJson">JSON array of additional omniture variable/values pair</param>
        /// <param name="MemberLevelTrackingLastApplication">Tracking support - last application ID</param>
        /// <param name="MemberLevelTrackingIsMobileDevice" values="true/false as string">Tracking support - are they using a mobile device</param>
        /// <param name="MemberLevelTrackingIsTablet" values="true/false as string">Tracking support - are they using a tablet</param>
        /// <param name="OrderID">Optional OrderID when requested redirect data for Upsale</param>
        /// <param name="PaymentUiPostDataType">The type of data being sent to payment UI; defaults to subscription if no values are specified</param>
        /// <param name="PromoType">The type of promo to qualify the user for; defaults to mobile promos if no values are specified</param>
        /// <param name="PaymentType">The type of payment to filter promos; default is Credit Card if no values are specified</param>
        /// <param name="TemplateID">This is the PUI templateID.  
        /// Used for PaymentUiPostDataType premium services. Default is 121.  
        /// Used for PaymentUiPostDataType billing information.  Default is 110.
        /// Used for PaymentUiPostDataType upsale. Default is 400 for variable, 410 for fixed pricing
        /// The other paymentUIPostDataType uses template ID that comes from the promo.</param>
        /// <param name="PromoID">Used to pass in a promoID that should be used, if valid.  Only available when PaymentUiPostDataType is for Subscription.</param>
        /// <param name="PremiumType">Used to restrict plans for Upsale or Premium services a la carte.  By default, all premiums will be included.</param>
        /// <param name="MemberID">The memberID that the admin is purchasing on behalf of</param>
        /// <param name="AdminMemberID">The memberID representing the admin</param>
        /// <param name="AdminDomainName">The admin admindomain name (e.g. matchnet\sburton)</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <paramsAdditionalInfo></paramsAdditionalInfo>
        /// <responseAdditionalInfo>JSON Object consisting of member specific promo needed to post to UPS when redirecting to Payment UI.</responseAdditionalInfo>
        /// <returns>JSON Object consisting of member specific promo needed to post to UPS when redirecting to Payment UI.</returns>
        [ApiMethod(ResponseType = typeof(AdminPaymentUiPostDataRequest))]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/AdminTrustedClientIPWhitelist.xml")]
        [RequireClientCredentials]
        public ActionResult AdminGetPaymentUiRedirectData(AdminPaymentUiPostDataRequest request)
        {
            if (!IPBlockerServiceAdapter.Instance.CheckIPAccess(request.ClientIp, request.Brand.BrandID, IPBlockerAccessType.Subscription))
            {
                return new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.IPBlocked, string.Format("IP Blocked. IP: {0}, MemberID: {1}, SiteID: {2}, AdminID: {3}, Admin: {4}", request.ClientIp, request.MemberId, request.Brand.Site.SiteID, request.AdminMemberID, request.AdminDomainName));
            }

            var data = UnifiedPaymentSystemHelper.Instance.GetPaymentUiPostData(request, true);

            return new NewtonsoftJsonResult { Result = data };
        }

        /// <summary>
        /// Used for subscription confirmation; gets the order information based on an OrderID
        /// </summary>
        /// <example>
        /// GET http://api.spark.net/v2/brandId/1003/subscription/transactionInfo/1140366213?access_token=
        /// 
        /// RESPONSE
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": {
        ///        "username": "DavidIsAwesome",
        ///        "confirmationNumber": "1140366213",
        ///        "creditCardType": "Visa",
        ///        "maskedCreditCardNumber": "4468",
        ///        "transactionDate": "2014-10-03T22:25:40.027Z",
        ///        "subscriptionStatusDetailed": "",
        ///        "subscriptionStatus": "Member",
        ///        "planDescription": "418;1Month;1;39.99",
        ///        "promoId": 0
        ///    }
        ///}
        /// 
        /// </example>
        /// <param name="OrderID">Not used, pass it in with the URL as a GET call</param>
        /// <param name="MemberID">Not used</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <paramsAdditionalInfo></paramsAdditionalInfo>
        /// <responseAdditionalInfo>JSON Object consisting of the order transaction info</responseAdditionalInfo>
        /// <returns>JSON Object consisting of the order transaction info</returns>
        [ApiMethod(ResponseType = typeof (TransactionInfo))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetPaymentTransactionInfo(TransactionInfoRequest request)
        {
            var memberId = GetMemberId();
            var transactionInfo = UnifiedPaymentSystemHelper.Instance.GetTransactionInfo(request.Brand, memberId,
                request.OrderId);
            UnifiedPaymentSystemHelper.Instance.ExpireCachedMemberAccess(memberId);
            return new NewtonsoftJsonResult {Result = transactionInfo};
        }

        /// <summary>
        /// Updates the lifetime access for a member
        /// </summary>
        /// <example>
        /// PUT http://api.spark.net/v2/brandId/1003/subscription/lifetimeaccess?applicationid=APPID&amp;client_secret=
        /// 
        /// {
        ///     "memberId": 411,
        ///     "adminmemberId": 411
        /// }
        /// 
        /// RESPONSE
        /// 
        /// {"code":200,"status":"OK","data":""}
        /// 
        /// </example>
        /// <param name="AdminMemberID" required="true">Admin memberID</param>
        /// <param name="MemberID" required="true">MemberID</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <paramsAdditionalInfo></paramsAdditionalInfo>
        /// <responseAdditionalInfo>JSON with just an ok status if all went well</responseAdditionalInfo>
        /// <returns>JSON with just an ok status if all went well</returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireClientCredentials]
        [HttpPut]
        public ActionResult UpdateLifeTimeAccess(UpdateLifeTimeAccessRequest request)
        {
            if (request.MemberId == 0 || request.MemberId == Constants.NULL_INT)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingMemberId, "Member Id required.");
            }

            if (request.AdminMemberId == 0 || request.AdminMemberId == Constants.NULL_INT)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingMemberId,
                    "Admin Member Id required.");
            }

            var proxy = Common.Adapter.AccessServiceWebAdapter.GetProxyInstanceForBedrock();

            try
            {
                Log.LogInfoMessage(string.Format("Adding life time access for memberId:{0} adminMemberId:{1} siteId:{2}",
                    request.MemberId, request.AdminMemberId, request.Brand.Site.SiteID), ErrorHelper.GetCustomData());
                proxy.InsertLifetimeMember(request.MemberId, request.Brand.Site.SiteID, request.AdminMemberId);
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Error adding life time access. memberId:{0} adminMemberId:{1} siteId:{2}",
                    request.MemberId, request.AdminMemberId, request.Brand.Site.SiteID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedLifeTimeAccess,
                    "Error adding life time access");
            }
            finally
            {
                proxy.Close();
            }

            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";
            return Content(String.Empty);
        }

        /// <summary>
        /// Gets the lifetime information for a member
        /// </summary>
        /// <example>
        /// GET http://api.spark.net/v2/brandId/1003/subscription/lifetimeaccess/411?applicationid=APPID&amp;client_secret=
        /// 
        /// RESPONSE
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": {
        ///        "memberId": 411,
        ///        "renewalDate": "2014-10-03T22:25:40.027Z"
        ///    }
        /// }
        /// 
        /// </example>
        /// <param name="AdminMemberId">Not used</param>
        /// <param name="MemberID">Not used</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <paramsAdditionalInfo></paramsAdditionalInfo>
        /// <responseAdditionalInfo>JSON Object with the MemberId and RenewalDate, if member is found</responseAdditionalInfo>
        /// <returns>JSON Object with the MemberId and RenewalDate, if member is found</returns>
        [ApiMethod(ResponseType = typeof (LifeTimeAccessResponse))]
        [RequireClientCredentials]
        [HttpGet]
        public ActionResult GetLifeTimeAccess(UpdateLifeTimeAccessRequest request)
        {
            // use the bedrock method to use the database setting
            var proxy = Common.Adapter.AccessServiceWebAdapter.GetProxyInstanceForBedrock();
            LifeTimeAccessResponse lifeTimeAccessResponse = null;

            try
            {
                var lifeTimeMember = proxy.GetLifetimeMember(request.MemberId, request.Brand.Site.SiteID);

                if (lifeTimeMember != null)
                {
                    lifeTimeAccessResponse = new LifeTimeAccessResponse
                    {
                        MemberId = lifeTimeMember.MemberID,
                        RenewalDate = lifeTimeMember.RenewalDate,
                    };
                }
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Error retrieving life time access. memberId:{0} adminMemberId:{1} siteId:{2}",
                    request.MemberId, request.AdminMemberId, request.Brand.Site.SiteID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedLifeTimeAccess,
                    "Error retrieving life time access");
            }
            finally
            {
                proxy.Close();
            }

            if (lifeTimeAccessResponse != null)
            {
                return new NewtonsoftJsonResult {Result = lifeTimeAccessResponse};
            }
            else
            {
                return new NewtonsoftJsonResult {Result = null};
            }
        }

        /// <summary>
        ///     Returns a list of UPS Access privileges the requesting member has
        /// </summary>
        /// <example>
        /// GET http://api.spark.net/v2/brandId/1003/subscription/accessprivileges?access_token=
        /// 
        /// RESPONSE
        /// 
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": [{
        ///        "name": "BasicSubscription",
        ///        "endDateUtc": "2014-09-18T15:11:47.953Z",
        ///        "remainingCount": null
        ///    },
        ///    {
        ///        "name": "HighlightedProfile",
        ///        "endDateUtc": "2014-05-29T21:59:37.73Z",
        ///        "remainingCount": null
        ///    },
        ///    {
        ///        "name": "SpotlightMember",
        ///        "endDateUtc": "2014-05-29T21:59:37.73Z",
        ///        "remainingCount": null
        ///    },
        ///    {
        ///        "name": "AllAccess",
        ///        "endDateUtc": "2014-09-18T15:11:22.167Z",
        ///        "remainingCount": null
        ///    },
        ///    {
        ///        "name": "AllAccessEmails",
        ///        "endDateUtc": "0001-01-01T00:00:00Z",
        ///        "remainingCount": 20
        ///    }]
        ///}
        /// 
        /// </example>
        /// <param name="MemberID">Not used, member is derived from the access_token</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <paramsAdditionalInfo></paramsAdditionalInfo>
        /// <responseAdditionalInfo>JSON Object with all privileges for a member</responseAdditionalInfo>
        /// <returns>JSON Object with all privileges for a member</returns>
        [ApiMethod(ResponseType = typeof (List<AccessPrivilegeResponse>))]
        [RequireAccessTokenV2]
        [HttpGet]
        public ActionResult GetAccessPrivileges(MemberRequest request)
        {
            var member = MemberSA.Instance.GetMember(GetMemberId());
            var brand = request.Brand;
            var values = Enum.GetValues(typeof (PrivilegeType));
            var accessPrivileges = new List<AccessPrivilegeResponse>();
            foreach (PrivilegeType value in values)
            {
                var accessPrivilege = member.GetUnifiedAccessPrivilege(value,
                    brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (accessPrivilege == null) continue;
                //JS-1329
                Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDateUTC: {2}", member.MemberID, value.ToString(), accessPrivilege.EndDateUTC), ErrorHelper.GetCustomData());
                var accessPrivilegeResponse = new AccessPrivilegeResponse
                {
                    Name = value.ToString(),
                    EndDateUtc = accessPrivilege.EndDateUTC,
                    RemainingCount =
                        // let's not return the nasty null_int, just return null.
                        (accessPrivilege.RemainingCount != Constants.NULL_INT)
                            ? (int?) accessPrivilege.RemainingCount
                            : null
                };
                accessPrivileges.Add(accessPrivilegeResponse);
            }
            return new NewtonsoftJsonResult {Result = accessPrivileges};
        }

        /// <summary>
        ///     This call returns true for each mobile app store member is subscribed to
        /// </summary>
        /// <example>
        ///     GET
        ///     http://api.local.spark.net/v2/brandId/1003/subscription/ismobileappsubscriber/27029711/?client_secret=[CLIENT_SECRET]%26applicationId=[APP_ID]
        ///
        ///     RESPONSE
        ///     {
        ///         "code": 200,
        ///         "status": "OK",
        ///         "data": [
        ///             {
        ///                 "mobileStore": "IAP",
        ///                 "isSubscriber": false
        ///             },
        ///             {
        ///                 "mobileStore": "IAB",
        ///                 "isSubscriber": false
        ///             }
        ///         ]
        ///     }
        /// </example>
        /// <param name="MemberID" required="true">Specify in URL</param>
        /// <param name="SiteID">Derived from URL</param>
        /// <param name="BrandID">Derived from URL</param>
        /// <param name="Brand">Derived from URL</param>
        [ApiMethod(ResponseType = typeof (List<IsMobileAppSubscriberResponse>))]
        [HttpGet]
        [RequireClientCredentials]
        public ActionResult GetIsMobileAppSubscriber(MemberRequest request)
        {
            if (request.MemberId < 0)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingMemberId, "Member Id required.");
            }

            // With CM IAP, IAB members, they do not have any actual member data except for the IAP, IAB member attributes
            // This is causing normal Member synchronization from working. Forceload instead from the service.
            var member = MemberSA.Instance.GetMember(request.MemberId, MemberLoadFlags.IngoreSACache);

            var response = new List<IsMobileAppSubscriberResponse>
            {
                new IsMobileAppSubscriberResponse
                {
                    MobileStore = "IAP",
                    IsSubscriber =
                        MemberHelper.IsMobileAppPayingMember(request.Brand, member,
                            PurchaseAccess.IOS_IAP_RECEIPT_ORIG_TRANS_ID_ATTR)
                }
                ,
                new IsMobileAppSubscriberResponse
                {
                    MobileStore = "IAB",
                    IsSubscriber =
                        MemberHelper.IsMobileAppPayingMember(request.Brand, member,
                            PurchaseAccess.ANDROID_IAB_SUBSCRIPTION_TOKENID_ATTR)
                }
            };

            return new NewtonsoftJsonResult {Result = response};
        }

        /// <summary>
        /// Update the member's subscription plan.
        /// </summary>
        /// <example>
        /// 
        /// PUT 
        /// http://api.local.spark.net/v2/brandId/{brandId}/subscription/plan?access_token=2/FbQPcqu9EadodF6evcPR/nfhql3/QkcNmAqjIACQQa0=
        /// 
        /// 
        /// * Sample to enable a premium feature and turn of another
        /// 
        /// {
        ///   "BaseSubscriptionPlan":       
        ///   {
        ///      "IsEnabled": true
        ///   },
        ///   "PremiumOptions":       [
        ///      {
        ///         "PremiumSubscriptionType": 2,
        ///         "IsEnabled": false
        ///      },
        ///      {
        ///         "PremiumSubscriptionType": 1,
        ///         "IsEnabled": true
        ///      }
        ///   ]
        /// }
        /// 
        /// * Sample to disable the main subscription and self suspend
        /// 
        /// {
        ///   "TerminationReasonId": 2,
        ///   "IsSelfSuspended": true,
        ///   "BaseSubscriptionPlan":       
        ///   {
        ///      "IsEnabled": false
        ///   }
        /// }
        /// 
        /// 
        /// 
        /// PremiumSubscriptionType:  
        ///     0   None
        ///     1   HighlightedProfile
        ///     2   SpotlightMember
        ///     4   JMeter
        ///     32  AllAccess
        ///     128 ReadReceipt
        /// 
        /// TerminationReasonId: should be a value from [mnSystem].[dbo].[TransactionReason]
        /// Some of these are meant for automatic terminations, should not be shown on UI apps to the user
        /// 
        ///     TransactionReasonID	Description
        ///     1	Bounced Email
        ///     2	I found someone on JDate
        ///     3	Found my soul mate on my own
        ///     4	Too many people are contacting me
        ///     5	Giving up - could not find a soul mate
        ///     6	Not enough members contacted me
        ///     7	Not enough members replied to my emails / IMs
        ///     8	I did not like the matches I received
        ///     9	JDate is too expensive
        ///     10	Just testing the service
        ///     11	Do not have time to date right now
        ///     12	Unable to upload a photo
        ///     13	The site is too difficult to use
        ///     14	Taking a break / vacation
        ///     15	Not enough members in my area
        ///     16	Going to try another service
        ///     17	Other
        ///     18	Complimentary
        ///     19	Customer Service Issues
        ///     20	Admin Suspended
        ///     21	Self Suspended
        ///     22	Check Period Ended
        ///     23	Unable to renew past grace period
        ///     24	Credit Given
        ///     25	Met someone on the site
        ///     26	I found someone on another dating site
        ///     27	I had a negative experience with another member
        ///     28	Not enough contacts/replies
        ///     29	No members interest me
        ///     30	Tried it but it is not for me 
        ///     31	Taking a break
        ///     32	I am no longer interested in paying for online dating
        ///     33	Online dating is too time-cons
        ///     34	Other sites are cheaper
        ///     35	Prefer to use another dating s
        ///     36	Poor customer care
        ///     37	I would rather not say
        ///     38	Paper Check
        ///     39	Free Trial
        ///     40	JCupid Import
        ///     41	Cupidon Import
        ///     42	Direct Debit Period Ended
        ///     43	YNM Purchase as a result of another member
        ///     44	Batch Renewal
        ///     45	Transaction approved in error
        ///     46	I found someone offline
        ///     47	AMEX - Renewal Failure
        ///     48	Soft Decline - Recycle Failed
        ///     49	Tried it but it is not for me
        ///     50	I am no longer interested in paying for online dating
        ///     51	I would rather not say
        ///     52	I found someone on another dating site
        ///     53	I had a negative experience with another member
        ///     54	Self Re Opened
        ///     55	GreatShape
        ///     56	24hrs Free SMS Verification for both Cupid and JDIL
        ///     57	Cupid WAP
        ///     58	Time Credit for Spark.com launch
        ///     79	I was not meeting the right people on JDate
        ///     80	I am dating someone I didn not meet on JDate
        /// 
        /// Response will return the transaction details for each renewal:
        /// 
        /// {
        ///   "code": 200,
        ///   "status": "OK",
        ///   "data": [   {
        ///      "RenewalSubscriptionId": 41467,
        ///      "RenewalResponse":       {
        ///         "version": "1.0",
        ///         "response": "SUCCESS",
        ///         "message": "EnableALaCarteAutoRenewal",
        ///         "InternalResponse":          {
        ///            "responsecode": "0",
        ///            "responsemessage": "Accept transaction - Success",
        ///            "userInterfaceActionTypeID": -2147483647,
        ///            "userInterfaceResponseCode": null
        ///         },
        ///         "TransactionID": 1103052099
        ///      },
        ///      "PremiumType": 2
        ///   }]
        /// }
        /// 
        /// 
        /// </example>
        /// <param name="request">The subscription plan change request</param>
        /// <returns>A dictionary that lists the changes commited by the request</returns>
        [ApiMethod(ResponseType = typeof(List<SubscriptionPlanChangeConfirmation>))]
        [RequireAccessTokenV2]
        [HttpPut]
        [MembersOnlineActionFilter]
        public ActionResult UpdateSubscriptionPlan(SubscriptionPlanChangeRequest request)
        {
            try
            {
                var plan = new SubscriptionPlanAccess(GetMemberId(), request.Brand);
                var updateResults = plan.Update(request);

                return new NewtonsoftJsonResult { Result = updateResults };
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Error updating subscription plan. memberId:{0}; siteId:{1}; request{2}",
                    GetMemberId(), request.BrandId, Newtonsoft.Json.JsonConvert.SerializeObject(request)), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(request.Brand));

                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedUpdatingSubscriptionPlan,
                    ex.Message);
            }
        }

        /// <summary>
        /// Get the member's subscription plan for either basic subscription or Free Trial
        /// </summary>
        /// <example>
        /// 
        /// Note: Bundled subscriptions will show the total price in the renewal rate for the subscription and premium options that belongs to the subscription.
        /// 
        /// GET 
        /// http://api.stage3.spark.net/v2/brandId/1003/subscription/plan?access_token=
        /// 
        /// Example response for member with bundled subscription and one a la carte package
        /// {
        ///        "code": 200,
        ///        "status": "OK",
        ///        "data": {
        ///            "IsSelfSuspended": false,
        ///            "IsFreeTrial": false,
        ///            "PackageTypeId": 1,
        ///            "BaseSubscriptionPlan": {
        ///                "IsEnabled": true,
        ///                "RenewalRate": 64.98,
        ///                "CurrencyType": 1,
        ///                "EndDate": "0001-01-01T00:00:00.000Z",
        ///                "RenewalDurationType": 5,
        ///                "RenewalDuration": 1,
        ///                "InitialCost": 39.99,
        ///                "InitialDurationType": 5,
        ///                "InitialDuration": 1
        ///            },
        ///            "SubscriptionEndDate": "2017-06-04T18:14:55.047Z",
        ///            "PremiumOptions": [{
        ///                "PremiumSubscriptionType": 32,
        ///                "PremiumSubscriptionTypeList": [32,
        ///                64],
        ///                "IsBase": true, //this indicates this premium is part of subscription bundle
        ///                "PackageID": 4018,
        ///                "PremiumSubscriptionTypeDescription": "AllAccess, AllAccessEmail",
        ///                "IsEnabled": true,
        ///                "RenewalRate": 64.98,
        ///                "CurrencyType": 1,
        ///                "EndDate": "0001-01-01T00:00:00.000Z",
        ///                "RenewalDurationType": 5,
        ///                "RenewalDuration": 0,
        ///                "InitialCost": 0.0,
        ///                "InitialDurationType": 0,
        ///                "InitialDuration": 0
        ///            },
        ///            {
        ///                "PremiumSubscriptionType": 128,
        ///                "PremiumSubscriptionTypeList": [128],
        ///                "IsBase": false, //this indicates it is an a la carte
        ///                "PackageID": 4101,
        ///                "PremiumSubscriptionTypeDescription": "ReadReceipt",
        ///                "IsEnabled": true,
        ///                "RenewalRate": 0.0,
        ///                "CurrencyType": 1,
        ///                "EndDate": "0001-01-01T00:00:00.000Z",
        ///                "RenewalDurationType": 5,
        ///                "RenewalDuration": 0,
        ///                "InitialCost": 0.0,
        ///                "InitialDurationType": 0,
        ///                "InitialDuration": 0
        ///            }],
        ///            "FreeTrialSubscriptionPlan": null
        ///        }
        /// }
        /// 
        /// 
        /// Example response for member with free trial
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data":
        ///    {
        ///        "IsSelfSuspended": false,
        ///        "IsFreeTrial": true,
        ///        "PackageTypeId": 1,
        ///        "BaseSubscriptionPlan": null,
        ///        "PremiumOptions": null,
        ///        "FreeTrialSubscriptionPlan":
        ///        {
        ///            "IsEnabled": true,
        ///            "RenewalRate": 39.99,
        ///            "CurrencyType": 1,
        ///            "RenewalDurationType": 0,
        ///            "RenewalDuration": 1,
        ///            "InitialCost": 39.99,
        ///            "InitialDurationType": 5,
        ///            "InitialDuration": 1
        ///        }
        ///    }
        /// }
        /// 
        /// CurrencyType: 
        /// None = 0, 
        /// USDollar = 1, 
        /// Euro = 2, 
        /// CanadianDollar = 3, 
        /// Pound = 4, 
        /// AustralianDollar = 5, 
        /// Shekels = 6, 
        /// VerifiedByVisa = 7
        /// 
        /// DurationType: 
        /// None = 0, 
        /// Minute = 1, 
        /// Hour = 2, 
        /// Day = 3, 
        /// Week = 4, 
        /// Month = 5, 
        /// Year = 6
        /// 
        /// PremiumSubscriptionType:  
        /// None = 0, 
        /// HighlightedProfile = 1, 
        /// SpotlightMember = 2, 
        /// JMeter = 4, 
        /// AllAccess = 32, 
        /// ReadReceipt = 128
        /// 
        /// PackageType:
        /// None = 0,
        /// Basic = 1,
        /// Bundled = 2,
        /// ALaCarte = 3,
        /// Discount = 4,
        /// RemainingCredit = 5,
        /// OneOff = 6,
        /// ServiceFee = 7,
        /// FreeTrial = 8,
        /// BOGOEligible = 9,
        /// Gift = 10,
        /// Event = 11
        /// 
        /// 
        /// </example>
        /// <param name="request">The member request.</param>
        /// <returns>The member's subscription plan status.</returns>
        /// <responseAdditionalInfo>
        /// IsEnabled for Free Trial means it has not been cancelled by the member 
        /// IsEnabled for Basic Subscription indicates if renewal is enabled
        /// </responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof(SubscriptionPlanStatus))]
        [RequireAccessTokenV2]
        [HttpGet]
        [MembersOnlineActionFilter]
        public ActionResult GetSubscriptionPlanStatus(MemberRequest request)
        {
            try
            {
                var planAccess = new SubscriptionPlanAccess(GetMemberId(), request.Brand);
                //var status = planAccess.GetPlanStatus();
                var status = planAccess.GetPackageStatus();

                return new NewtonsoftJsonResult { Result = status };
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Error getting subscription plan status. memberId:{0}; siteId:{1}; request{2}",
                    GetMemberId(), request.BrandId, Newtonsoft.Json.JsonConvert.SerializeObject(request)), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(request.Brand));

                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedGettingSubscriptionPlan,
                    ex.Message);
            }
        }

        /// <summary>
        /// Disable the member's Free Trial subscription plan status.
        /// </summary>
        /// <example>
        /// 
        /// PUT 
        /// http://api.local.spark.net/v2/brandId/{brandId}/subscription/freetrial/disable?access_token=2/FbQPcqu9EadodF6evcPR/nfhql3/QkcNmAqjIACQQa0=
        /// 
        /// 
        /// Response:
        /// 
        /// {
        ///   "code": 200,
        ///   "status": "OK",
        ///   "data": {}
        /// }
        /// 
        /// 
        /// </example>
        /// <param name="request">The member request.</param>
        /// <returns>The member's subscription plan status.</returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireAccessTokenV2]
        [HttpPut]
        [MembersOnlineActionFilter]
        public ActionResult DisableFreeTrial(MemberRequest request)
        {
            try
            {
                var plan = new SubscriptionPlanAccess(GetMemberId(), request.Brand);

                if (plan.DisableFreeTrial())
                {
                    return Ok();
                }

                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedDisablingFreeTrial,
                    "Unkown error disabling free trial.");
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Error disabling free trial status. memberId:{0}; siteId:{1}; request{2}",
                    GetMemberId(), request.BrandId, Newtonsoft.Json.JsonConvert.SerializeObject(request)), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(request.Brand));

                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedDisablingFreeTrial,
                    ex.Message);
            }
        }

        /// <summary>
        ///     Retrieves Account Transaction History. 
        /// </summary>
        /// <example>
        ///Request URL - GET - /v2/brandId/1003/subscription/history?access_token=[ACCESS_TOKEN]
        ///Request Content Type - application/json
        /// 
        ///Response Body
        ///{
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": [
        ///    {
        ///      "Description": null,
        ///      "Date": "2015-04-02T19:03:01.997Z",
        ///      "TransactionItemType": 4,
        ///      "TransactionType": 0,
        ///      "InitialCost": 0,
        ///      "InitialDuration": 0,
        ///      "InitialDurationType": 0,
        ///      "RenewalRate": 39.99,
        ///      "RenewalDuration": 1,
        ///      "RenewalDurationType": 5,
        ///      "CurrencyType": 0,
        ///      "PackageId": 35889,
        ///      "PromoId": 8397,
        ///      "PromoDescription": "JON Free Trial Promotion III",
        ///      "PromoPageTitle": "JON Free Trial III",
        ///      "Status": 2,
        ///      "PaymentType": 1,
        ///      "LastFourDigitsAccount": "1000",
        ///      "ConfirmationNumber": 1103066649,
        ///      "AuthorizationId": 236711246,
        ///      "ChargeId": 0
        ///    }
        ///  ]
        ///}    
        /// Fields definition
        ///     
        ///    public class TransactionHistoryItemPlain
        ///    {
        ///        /// &lt;summary&gt;
        ///        /// Billing Description
        ///        /// &lt;/summary&gt;
        ///        public string Description { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// String representation of time when the transaction was created, in UTC
        ///        /// &lt;/summary&gt;
        ///        public string Date { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Transaction Type:
        ///        ///     None = 0,
        ///        ///     OrderInfo = 1,
        ///        ///     AccessTransaction = 2,
        ///        ///     RenewalTransaction = 3,
        ///        ///     FreeTrialTransaction = 4
        ///        /// &lt;/summary&gt;
        ///        public int TransactionItemType { get; set; }
        ///        /// &lt;summary&gt;
        ///        ///     None = 0,
        ///        ///     InitialSubscriptionPurchase = 1,
        ///        ///     Renewal = 2,
        ///        ///     Credit = 3,
        ///        ///     Void = 4,
        ///        ///     AutoRenewalTerminate = 5,
        ///        ///     Authorization = 6,
        ///        ///     PaymentProfileAuthorization = 10,
        ///        ///     PlanChange = 11,
        ///        ///     TrialPayAdjustment = 12,
        ///        ///     AutoRenewalReopen = 18,
        ///        ///     AdditionalSubscriptionPurchase = 23,     upgrade before
        ///        ///     AdditionalNonSubscriptionPurchase = 24,  downgrade before
        ///        ///     VirtualTerminalPurchase = 34,
        ///        ///     VirtualTerminalCredit = 35,
        ///        ///     VirtualTerminalVoid = 36,
        ///        ///     BatchRenewal = 37,
        ///        ///     LifetimeMemberAdjustment = 38,
        ///        ///     AutoRenewalTerminateALaCarte = 39,
        ///        ///     AutoRenewalReopenALaCarte = 40,
        ///        ///     FreezeSubscriptionAccount = 46,
        ///        ///     UnfreezeSubscriptionAccount = 47,
        ///        ///     ManualPayment = 48,		
        ///        ///     TrialTakenInitialSubscription = 1001,
        ///        ///     ChargeBack = 1004,
        ///        ///     CheckReversal = 1005,
        ///        ///     Adjustment = 1007,
        ///        ///     GiftPurchase = 1013,
        ///        ///     GiftRedeem = 1014,
        ///        ///     RenewalRecyclingTimeAdjustment = 1015
        ///        /// &lt;/summary&gt;
        ///        public int TransactionType { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Initial cost of the subscription
        ///        /// &lt;/summary&gt;
        ///        public decimal InitialCost { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Initial duration of the subscription
        ///        /// &lt;/summary&gt;
        ///        public int InitialDuration { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Initial duration type of the subscription:
        ///        ///     None = 0,
        ///        ///     Minutes = 1,
        ///        ///     Hour = 2,
        ///        ///     Day = 3,
        ///        ///     Week = 4,
        ///        ///     Month = 5,
        ///        ///     Year = 6
        ///        /// &lt;/summary&gt;
        ///        public int InitialDurationType { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Cost to renew subscription
        ///        /// &lt;/summary&gt;
        ///        public decimal RenewalRate { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Duration of renewal
        ///        /// &lt;/summary&gt;
        ///        public int RenewalDuration { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Duration type of renewal:
        ///        ///     None = 0,
        ///        ///     Minutes = 1,
        ///        ///     Hour = 2,
        ///        ///     Day = 3,
        ///        ///     Week = 4,
        ///        ///     Month = 5,
        ///        ///     Year = 6
        ///        /// &lt;/summary&gt;
        ///        public int RenewalDurationType { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Localization of the currency
        ///        ///     None = 0,
        ///        ///     USD = 1,
        ///        ///     EUR = 2,
        ///        ///     CAD = 3,
        ///        ///     GBP = 4,
        ///        ///     AUD = 5,
        ///        ///     ILS = 6,
        ///        ///     VerifiedByVisa = 7 
        ///        /// &lt;/summary&gt;
        ///        public int CurrencyType { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Package ID, Aka Subscription ID, Plan ID
        ///        /// &lt;/summary&gt;
        ///        public int PackageId { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Promotion ID
        ///        /// &lt;/summary&gt;
        ///        public int PromoId { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Description of the promotion
        ///        /// &lt;/summary&gt;
        ///        public string PromoDescription { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Promo Page Title
        ///        /// &lt;/summary&gt;
        ///        public string PromoPageTitle { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Billing Outcome
        ///        ///     None = 0,
        ///        ///     Pending = 1,
        ///        ///     Successful = 2,
        ///        ///     Failed = 3
        ///        /// &lt;/summary&gt;
        ///        public int Status { get; set; }
        ///        /// &lt;summary&gt;
        ///        ///     None = 0,
        ///        ///     CreditCard = 1,
        ///        ///     Check = 2,
        ///        ///     DirectDebit = 4,
        ///        ///     PayPalLitle = 8,
        ///        ///     SMS = 16,
        ///        ///     PayPalDirect = 32,
        ///        ///     Manual = 64,
        ///        ///     ElectronicFundsTransfer = 128,
        ///        ///     PaymentReceived = 256,
        ///        ///     DebitCard = 512,
        ///        ///     InApplicationPurchase = 1024
        ///        /// &lt;/summary&gt;
        ///        public int PaymentType { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// 
        ///        /// &lt;/summary&gt;
        ///        public string LastFourDigitsAccount { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// Order ID, Confirmation
        ///        /// &lt;/summary&gt;
        ///        public int ConfirmationNumber { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// 
        ///        /// &lt;/summary&gt;
        ///        public int AuthorizationId { get; set; }
        ///        /// &lt;summary&gt;
        ///        /// 
        ///        /// &lt;/summary&gt;
        ///        public int ChargeId { get; set; }
        ///    }
        ///}
        /// </example>
        /// <paramsAdditionalInfo>
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo>
        /// </responseAdditionalInfo>

        [ApiMethod(ResponseType = typeof(TransactionHistoryItemPlain))]
        [RequireAccessTokenV2]
        [HttpGet]
        public ActionResult GetTransactionHistory(BrandRequest request)
        {
            var result = TransactionHistoryServiceAdapter.Instance.GetAccountHistory(request.Brand, GetMemberId(), 5000, 1);
            return new NewtonsoftJsonResult() {Result = result};
        }

    }
}
