﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Spark.Logger;
using Spark.Managers.Managers;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.Entities.Profile;
using Spark.Rest.Helpers;
using Spark.Rest.Serialization;
using Spark.Rest.V2.DataAccess.MembersOnline;
using Spark.Rest.V2.Models.Photos;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.DataAccess;
using Spark.REST.Models;
using Spark.REST.Models.Photos;
using Photo = Spark.REST.Entities.Profile.Photo;

#endregion

namespace Spark.REST.Controllers
{
    public class PhotoController : SparkControllerBase
    {
        private const int UploadSortPosition = 12;
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(PhotoController));

        public ActionResult GetPhotoUploadPage(MemberRequest memberRequest)
        {
            return View("GetPhotoUploadPage", memberRequest);
        }

        [ApiMethod(ResponseType = typeof (List<Photo>))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetPhotos(MemberAndTargetMemberRequest memberRequest)
        {
            var photos = PhotoAccess.GetPhotos(memberRequest.Brand, memberRequest.TargetMemberId, GetMemberId());
            return new NewtonsoftJsonResult {Result = photos};
        }

        /// <summary>
        /// </summary>
        /// <param name="deleteRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult DeletePhoto(DeletePhotoRequest deleteRequest)
        {
            var memberId = GetMemberId();
            Log.LogInfoMessage(string.Format("Delete photo called for memberId {0}, memberPhotoId {1}", memberId,
                deleteRequest.MemberPhotoId), ErrorHelper.GetCustomData());

            if (deleteRequest.MemberPhotoId <= 0)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberPhotoId,
                    String.Format(
                        "Value must be greater than 0. MemberPhotoId:{0} MemberId:{1}",
                        deleteRequest.MemberPhotoId, memberId));
            }

            var photos = PhotoAccess.GetPhotos(deleteRequest.Brand, memberId, memberId);

            var listOrder =
                (from photo in photos where photo.MemberPhotoId == deleteRequest.MemberPhotoId select photo.ListOrder).
                    FirstOrDefault();

            var photoUpdates = new List<PhotoUpdate>(1);

            var photoUpdate = new PhotoUpdate(null,
                true,
                deleteRequest.MemberPhotoId,
                Constants.NULL_INT,
                null,
                Constants.NULL_INT,
                null,
                listOrder,
                false,
                false,
                Constants.NULL_INT,
                Constants.NULL_INT);

            if (photoUpdate.Delete != true)
                Log.LogError("Photo update's delete flag is false.", new Exception(), ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(deleteRequest.Brand));

            photoUpdates.Add(photoUpdate);

            var success = false;
            try
            {
                success = MemberSA.Instance.SavePhotos(deleteRequest.BrandId, deleteRequest.Brand.Site.SiteID,
                    deleteRequest.Brand.Site.Community.CommunityID, memberId,
                    photoUpdates.ToArray());
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Exception saving file for member: {0}", memberId), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(deleteRequest.Brand));
            }

            if (!success)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedDeletePhoto,
                    "Unknown server error removing photo");
            }

            return new NewtonsoftJsonResult
            {Result = String.Format("Successfully removed photoId: {0}", deleteRequest.MemberPhotoId)};
        }

        /// <summary>
        /// Photo upload as standard http form files
        /// </summary>
        /// <example>
        /// Note: 
        /// Supports multiple photo files
        /// 
        /// </example>
        /// <param name="photoMetadataRequest"></param>
        /// <param name="photoFiles"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult PostPhotos(PhotoMetadataRequest photoMetadataRequest, IList<HttpPostedFileBase> photoFiles)
        {
            var memberId = GetMemberId();
            if (photoFiles == null || photoFiles.Count <= 0)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingPhotoUploadData, "No files found");
            }

            var existingCount = PhotoAccess.GetPhotoCount(photoMetadataRequest.Brand, memberId);
            if (photoFiles.Count + existingCount > 12)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MaxNumberOfPhotosExceeded,
                    "Limit of 12 photos");
            }

            var photoUpdates = new List<PhotoUpdate>(photoFiles.Count);
            byte sort = UploadSortPosition;
            foreach (var photo in photoFiles)
            {
                if (photo == null || photo.ContentLength <= 0) continue;

                var fileName = Path.GetFileName(photo.FileName);
                if (fileName == null) continue;

                var photoRules = new PhotoRulesManager();

                if (!photoRules.IsFileSizeAllowed(photo.ContentLength))
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MaxPhotoSizeExceeded,
                        string.Format("Maximum file size exceeded {0}",
                            HttpUtility.HtmlEncode(photo.FileName)));
                }

                if (!photoRules.IsValidImage(photo.InputStream, photo.ContentLength))
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.UnsupportedPhotoFormat,
                        string.Format("The file {0} is not a valid image",
                            HttpUtility.HtmlEncode(photo.FileName)));
                }

                byte[] newFileBytes = null;

                using (var fileStream = photo.InputStream)
                {
                    try
                    {
                        fileStream.Position = 0;
                        newFileBytes = new byte[photo.ContentLength];
                        fileStream.Read(newFileBytes, 0, photo.ContentLength);
                        photo.InputStream.Flush();
                    }
                    catch (Exception ex)
                    {
                        Log.LogError(string.Format("Exception reading file for member: {0} filename: {1}", memberId, fileName), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(photoMetadataRequest.Brand));
                    }
                }

                if (newFileBytes == null || newFileBytes.Length <= 0) continue;
                var photoUpdate = new PhotoUpdate(newFileBytes,
                    false,
                    Constants.NULL_INT,
                    Constants.NULL_INT,
                    null,
                    Constants.NULL_INT,
                    null,
                    sort--,
                    false,
                    false,
                    Constants.NULL_INT,
                    Constants.NULL_INT,
                    photoMetadataRequest.Caption,
                    false);
                photoUpdate.IsMain = photoMetadataRequest.IsMain;
                photoUpdates.Add(photoUpdate);
            }

            var success = false;
            try
            {
                success = MemberSA.Instance.SavePhotos(photoMetadataRequest.BrandId,
                    photoMetadataRequest.Brand.Site.SiteID,
                    photoMetadataRequest.Brand.Site.Community.CommunityID, memberId,
                    photoUpdates.ToArray());
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Exception saving file for member: {0}", memberId), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(photoMetadataRequest.Brand));
            }

            if (!success)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedUploadPhoto,
                    "Unknown server error saving photos");
            }
            return new NewtonsoftJsonResult
            {Result = String.Format("Successfully uploaded {0} photo(s)", photoUpdates.Count)};
        }

        /// <summary>
        /// Photo upload as json where the photo is a base64 string
        /// </summary>
        /// <example>
        /// Note: 
        /// Only 1 photo upload at a time
        /// 
        /// 
        /// </example>
        /// <param name="photoFile">Photo file data as base64 string</param>
        /// <param name="isMain">Indicates whether the photo should be considered as a main photo</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (PhotoUploadResponse))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult PostPhotosJson(FileRequest photoUploadRequest)
        {
            var memberId = GetMemberId();
            if (String.IsNullOrEmpty(photoUploadRequest.PhotoFile))
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingPhotoUploadData,
                    "No file to upload");
            }

            //this json endpoint currently only supports uploading one photo at a time
            var photoCount = 1;
            var existingCount = PhotoAccess.GetPhotoCount(photoUploadRequest.Brand, memberId);
            if (photoCount + existingCount > 12)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MaxNumberOfPhotosExceeded,
                    "Limit of 12 photos");
            }

            using (var stream = new MemoryStream())
            {
                var buffer = Convert.FromBase64String(photoUploadRequest.PhotoFile);
                stream.Write(buffer, 0, buffer.Length);
                if (stream.Length == 0)
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingPhotoUploadData,
                        "No file to upload");
                }

                var photoRules = new PhotoRulesManager();

                if (!photoRules.IsFileSizeAllowed((int) stream.Length))
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MaxPhotoSizeExceeded,
                        string.Format("Maximum file size exceeded {0}",
                            HttpUtility.HtmlEncode(
                                photoUploadRequest.PhotoFile)));
                }

                if (!photoRules.IsValidImage(stream, (int) stream.Length))
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.UnsupportedPhotoFormat,
                        string.Format("The file {0} is not a valid image",
                            HttpUtility.HtmlEncode(
                                photoUploadRequest.PhotoFile)));
                }

                var photoUpdate = new PhotoUpdate(buffer,
                    false,
                    Constants.NULL_INT,
                    Constants.NULL_INT,
                    null,
                    Constants.NULL_INT,
                    null,
                    UploadSortPosition,
                    false,
                    false,
                    Constants.NULL_INT,
                    Constants.NULL_INT,
                    photoUploadRequest.Caption,
                    false);
                var photoUpdates = new List<PhotoUpdate> {photoUpdate};

                var success = false;
                try
                {
                    success = MemberSA.Instance.SavePhotos(photoUploadRequest.BrandId,
                        photoUploadRequest.Brand.Site.SiteID,
                        photoUploadRequest.Brand.Site.Community.CommunityID, memberId,
                        photoUpdates.ToArray());
                }
                catch (Exception ex)
                {
                    Log.LogError(string.Format("Exception saving file for member: {0}", memberId), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(photoUploadRequest.Brand));
                }

                if (!success)
                {
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedUploadPhoto,
                        "Unknown server error saving photos");
                }
                return new NewtonsoftJsonResult
                {
                    Result = new PhotoUploadResponse {Success = true, FailureReason = String.Empty, PhotoId = 0}
                };
            }
        }

        /// <summary>
        /// Photo upload by specifying just the URL where the photo source resides
        /// </summary>
        /// <example>
        /// Note:
        /// Supports uploading multiple photos at a time
        /// 
        /// 
        /// Example: Upload 2 photos
        /// 
        /// POST http://api.stage3.spark.net/v2/brandId/1003/profile/photos/uploadurl?access_token=
        /// 
        /// {
        ///    "Photos":[
        ///    {
        ///      "caption": "cool deal 2",
        ///      "isMain": "true",
        ///      "photoUploadUrl": "http://s3.amazonaws.com/sparkmemberphotossv3/2014/04/04/11/222003586.jpg"
        ///    },
        ///    {
        ///      "caption": "cool deal",
        ///      "isMain": "true",
        ///      "photoUploadUrl": "http://50.57.22.103/wp-content/uploads/2013/05/free.jpg"
        ///    }
        ///    ]
        /// }
        /// 
        /// RESPONSE
        /// 
        /// {"code":200,"status":"OK","data":"Successfully uploaded 2 photo(s)"}
        /// 
        /// 
        /// </example>
        /// <param name="Photos" required="true">A list or array of photo metadata</param>
        /// <param name="MemberID">Not used</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public async Task<ActionResult> PostPhotosUrl(PhotoUploadUrlRequest photoUploadRequest)
        {
            var memberId = GetMemberId();

            if (photoUploadRequest.Photos.Count <= 0)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingPhotoUploadData, "No files found");
            }

            var existingCount = PhotoAccess.GetPhotoCount(photoUploadRequest.Brand, memberId);
            if (photoUploadRequest.Photos.Count + existingCount > 12)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MaxNumberOfPhotosExceeded,
                    "Limit of 12 photos");
            }

            //get photos from url
            var photoUpdates = new List<PhotoUpdate>();
            var httpClient = new HttpClient();
            var taskList = new List<Task<byte[]>>();
            byte sort = UploadSortPosition;
            foreach (var photoMeta in photoUploadRequest.Photos)
            {
                if (!string.IsNullOrEmpty(photoMeta.PhotoUploadUrl))
                {
                    Log.LogInfoMessage(string.Format("Retrieving photos via URL. MemberID:{0}, BrandID:{1}, Photo URL:{2}", photoUploadRequest.MemberId, photoUploadRequest.BrandId, photoMeta.PhotoUploadUrl), ErrorHelper.GetCustomData());
                    Task<byte[]> photoTask = httpClient.GetByteArrayAsync(photoMeta.PhotoUploadUrl);
                    taskList.Add(photoTask);
                }
            }

            //wait for all downloads to be done
            await Task.WhenAll(taskList);

            if (taskList.Count != photoUploadRequest.Photos.Count)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingPhotoUploadData, "There was an issue downloading one or more of the photos");
            }

            var i = 0;
            foreach (var photoMeta in photoUploadRequest.Photos)
            {
                var photoBytes = taskList[i].Result;
                if (photoBytes.Length > 0)
                {
                    byte photoSort = sort--;
                    var photoUpdate = new PhotoUpdate(photoBytes,
                                        false,
                                        Constants.NULL_INT,
                                        Constants.NULL_INT,
                                        null,
                                        Constants.NULL_INT,
                                        null,
                                        photoSort,
                                        false,
                                        false,
                                        Constants.NULL_INT,
                                        Constants.NULL_INT,
                                        photoMeta.Caption,
                                        false);
                    photoUpdate.IsMain = photoMeta.IsMain;
                    photoUpdates.Add(photoUpdate);
                }
                i++;
            }

            Log.LogInfoMessage(string.Format("Retrieved photos via URL.  Photos Requested: {0}, Photos Downloaded: {1}", photoUploadRequest.Photos.Count, photoUpdates.Count), ErrorHelper.GetCustomData());

            if (photoUpdates.Count > 0)
            {
                //save photos
                var success = false;
                try
                {
                    success = MemberSA.Instance.SavePhotos(photoUploadRequest.BrandId,
                        photoUploadRequest.Brand.Site.SiteID,
                        photoUploadRequest.Brand.Site.Community.CommunityID, memberId,
                        photoUpdates.ToArray());
                }
                catch (Exception ex)
                {
                    Log.LogError(string.Format("Exception saving file for member: {0}", memberId), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(photoUploadRequest.Brand));
                }

                if (!success)
                {
                    return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedUploadPhoto,
                        "Unknown server error saving photos");
                }
            }

            return new NewtonsoftJsonResult { Result = String.Format("Successfully uploaded {0} photo(s)", photoUpdates.Count) };
        }

        /// <summary>
        ///     Also supports masking a photo as main and changing the list order.
        ///     todo:This should be renamed to UpdatePhotoMetadata
        /// </summary>
        /// <param name="photoMetadataRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (PhotoUploadResponse))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult UpdatePhotoCaption(PhotoMetadataRequest photoMetadataRequest)
        {
            var memberId = GetMemberId();
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            var photoCommunity = member.GetPhotos(photoMetadataRequest.Brand.Site.Community.CommunityID);
            var requestedPhoto = photoCommunity.Find(photoMetadataRequest.MemberPhotoId);
            var photoUpdates = new List<PhotoUpdate>();

            // can't update if there's no matching photo in the backend!
            if (requestedPhoto == null)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberPhotoId,
                    string.Format("no member photo found with specified ID:{0}",
                        photoMetadataRequest.MemberPhotoId));
            }

            // listorder change is not requested, let's assign the existing value.
            var listOrder = (photoMetadataRequest.ListOrder <= 0)
                ? requestedPhoto.ListOrder // use the old value
                : photoMetadataRequest.ListOrder; // use the new value

            // caption change is not requested, let's assign the existing value
            var caption = (String.IsNullOrEmpty(photoMetadataRequest.Caption))
                ? requestedPhoto.Caption
                : photoMetadataRequest.Caption;

            // unmarks old main as not main if the new one is main eligible, make sure there's only one main being saved.
            if (photoMetadataRequest.IsMain && requestedPhoto.IsApprovedForMain)
            {
                // this explicit value will be used for updating the request photo's list order to be the first one.
                listOrder = 1;

                // this loop only ends up updating one photo (to be former main), not the requested photo.
                for (byte i = 0; i < photoCommunity.Count; i++)
                {
                    var currentPhoto = photoCommunity[i];

                    // skip if current photo is not marked as main. we're only interested in the current main photo.
                    if (!(currentPhoto.IsMain && currentPhoto.ListOrder == 1)) continue;
                    // requested photo is already main, skip.
                    if (currentPhoto.MemberPhotoID == requestedPhoto.MemberPhotoID) continue;

                    // let's bump up the list order of the current main photo
                    var newListOrder = (byte) (currentPhoto.ListOrder + 1);

                    // we're changing listOrder and isMain only
                    photoUpdates.Add(new PhotoUpdate(null, false, currentPhoto.MemberPhotoID,
                        currentPhoto.FileID,
                        currentPhoto.FileWebPath,
                        currentPhoto.ThumbFileID,
                        currentPhoto.ThumbFileWebPath,
                        newListOrder,
                        currentPhoto.IsApproved,
                        currentPhoto.IsPrivate,
                        currentPhoto.AlbumID,
                        currentPhoto.AdminMemberID,
                        currentPhoto.Caption,
                        currentPhoto.IsCaptionApproved,
                        currentPhoto.FileCloudPath,
                        currentPhoto.ThumbFileCloudPath,
                        currentPhoto.IsApprovedForMain,
                        false, // setting to to NOT main explicitly 
                        true));
                    Log.LogDebugMessage(string.Format("Unmarking photo {0} as main. List order {1}", currentPhoto.MemberPhotoID,
                        currentPhoto.ListOrder + 1), ErrorHelper.GetCustomData());
                }
            }
            else if (photoMetadataRequest.IsMain && !requestedPhoto.IsApprovedForMain)
            {
                // cannot mark a photo as main when it is not eligible.
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedPhotoSave,
                    string.Format("Cannot mark photo as main, it is not eligible. id{0}",
                        photoMetadataRequest.MemberPhotoId));
            }

            // this updates metadata for the request photo.
            photoUpdates.Add(new PhotoUpdate(null, false, photoMetadataRequest.MemberPhotoId, requestedPhoto.FileID,
                requestedPhoto.FileWebPath,
                requestedPhoto.ThumbFileID,
                requestedPhoto.ThumbFileWebPath,
                (byte) listOrder,
                requestedPhoto.IsApproved,
                requestedPhoto.IsPrivate,
                requestedPhoto.AlbumID,
                requestedPhoto.AdminMemberID,
                caption,
                // only if the requested photo's caption is the same as the current photo, consider approved.
                (requestedPhoto.Caption == photoMetadataRequest.Caption) && requestedPhoto.IsCaptionApproved,
                requestedPhoto.FileCloudPath,
                requestedPhoto.ThumbFileCloudPath,
                requestedPhoto.IsApprovedForMain,
                photoMetadataRequest.IsMain,
                true));

            Log.LogDebugMessage(string.Format("Updating photo metadata memberPhotoId:{0} isMain:{1}", photoMetadataRequest.MemberPhotoId,
                photoMetadataRequest.IsMain), ErrorHelper.GetCustomData());

            var success = false;

            try
            {
                success = MemberSA.Instance.SavePhotos(photoMetadataRequest.BrandId,
                    photoMetadataRequest.Brand.Site.SiteID,
                    photoMetadataRequest.Brand.Site.Community.CommunityID,
                    memberId,
                    photoUpdates.ToArray());
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Exception saving file for member: {0}", memberId), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(photoMetadataRequest.Brand));
            }

            if (!success)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedPhotoSave,
                    string.Format("Server error saving member photo id:{0} isMain:{1} listOrder:{2} caption:{3}",
                        photoMetadataRequest.MemberPhotoId, photoMetadataRequest.IsMain,
                        photoMetadataRequest.ListOrder, photoMetadataRequest.Caption));
            }

            return new NewtonsoftJsonResult
            {
                Result = new PhotoUploadResponse
                {
                    Success = true,
                    FailureReason = String.Empty,
                    PhotoId = photoMetadataRequest.MemberPhotoId
                }
            };
        }
    }
}