﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.SystemEvents;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Spark.Logger;
using Spark.PushNotifications.Processor.ServiceAdapter;
using Spark.Rest.Authorization;
using Spark.Rest.BedrockPorts;
using Spark.Rest.Configuration;
using Spark.Rest.Helpers;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Configuration;
using Spark.Rest.V2.DataAccess.Mail;
using Spark.Rest.V2.DataAccess.MembersOnline;
using Spark.Rest.V2.DataAccess.Mingle;
using Spark.Rest.V2.Entities.InstantMessenger;
using Spark.Rest.V2.Entities.Mail;
using Spark.Rest.V2.Models.InstantMessenger;
using Spark.Rest.V2.Serialization;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.Controllers;

#endregion

namespace Spark.Rest.Controllers
{
    /// <summary>
    /// </summary>
    public class InstantMessengerController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(InstantMessengerController));
        private static TimeZoneInfo zonePST = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");

        /// <summary>
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Stopwatch]
        [ApiMethod(ResponseType = typeof (InstantMessengerInvite))]
        [RequireAccessTokenV2]
        public ActionResult AddInvite(InstantMessengerInviteAddRequest request)
        {
            var member = MemberSA.Instance.GetMember(GetMemberId(), MemberLoadFlags.None);

            if (!member.IsPayingMember(request.Brand.Site.SiteID))
            {
                return new SparkHttpForbiddenResult((int) HttpSub403StatusCode.SubscriptionRequired, String.Empty);
            }

            InstantMessengerSA.Instance.InitiateConversation(GetMemberId(), request.RecipientMemberId,
                request.Brand.Site.Community.CommunityID);

            ListSA.Instance.AddListMember(HotListCategory.MembersYouIMed,
                request.Brand.Site.Community.CommunityID,
                request.Brand.Site.SiteID,
                GetMemberId(),
                request.RecipientMemberId,
                string.Empty,
                Constants.NULL_INT,
                true,
                false);

            // Send a push notification to the recipient
            PushNotificationsProcessorAdapter.Instance.SendNotificationEvent(new IMRequestEvent()
            {
                BrandId = request.Brand.BrandID,
                EventCategory = SystemEventCategory.MemberGenerated,
                EventType = SystemEventType.ChatRequest,
                MemberId = GetMemberId(),
                TargetMemberId = request.RecipientMemberId,
                NotificationTypeId = PushNotificationTypeID.ChatRequest,
                AppGroupId = AppGroupID.JDateAppGroup
            });

            var canMemberReply = InstantMessengerSA.Instance.CanMemberReply(GetMemberId(), request.RecipientMemberId,
                request.Brand);

            ConversationToken conversationToken = null;

            if (canMemberReply)
            {
                // used to track conversation invites securely for All Access
                conversationToken = InstantMessengerSA.Instance.CreateConverationToken(GetMemberId(),
                    request.RecipientMemberId,
                    request.Brand.Site.Community.CommunityID);
                Log.LogDebugMessage(string.Format("ConversationToken generated. Id:{0} SenderId:{1} RecipientId:{2}",
                    conversationToken.ConversationTokenId, conversationToken.SenderMemberId,
                    conversationToken.RecipientMemberId), ErrorHelper.GetCustomData());
            }

            var invite = new InstantMessengerInvite
            {
                SenderMemberId = GetMemberId(),
                InviteDate = DateTime.Now,
                ConversationKey = string.Empty,
                CanMemberReply = canMemberReply,
                ConversationTokenId =
                    (conversationToken != null) ? conversationToken.ConversationTokenId : null
            };

            return new NewtonsoftJsonResult {Result = invite};
        }

        /// <summary>
        ///     Get a list of pending conversation invites for the requesting member.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Stopwatch]
        [ApiMethod(ResponseType = typeof (List<InstantMessengerInvite>))]
        [RequireAccessTokenV2]
        public ActionResult GetInvites(InstantMessengerInviteRequest request)
        {
            var instantMessengerInvites = new List<InstantMessengerInvite>();

            var communityId = request.Brand.Site.Community.CommunityID;
            var recipientMemberId = GetMemberId();
            var invites = InstantMessengerSA.Instance.GetInvites(recipientMemberId, communityId);

            if (invites == null) return new NewtonsoftJsonResult {Result = instantMessengerInvites};

            foreach (ConversationInvite invite in invites)
            {
                //check if this invite has never been rendered before
                //todo:if (invite.IsRead) continue; expose this property for consistent results.
                //mark invite as being Read
                invite.IsRead = true;
                InstantMessengerSA.Instance.Save(invite);

                //// dont allow a popup to display for a member who is currently being ignored
                //if (!g.List.IsHotListed(HotListCategory.IgnoreList, g.Brand.Site.Community.CommunityID, senderMemberID))

                var senderMemberId = invite.Message.Sender.MemberID;
                var canMemberReply = InstantMessengerSA.Instance.CanMemberReply(senderMemberId, recipientMemberId,
                    request.Brand);

                ConversationToken conversationToken = null;

                if (canMemberReply)
                {
                    // used to track conversation invites securely for All Access
                    conversationToken = GetConversationToken(senderMemberId, recipientMemberId, request.BrandId,
                        communityId);
                }

                instantMessengerInvites.Add(new InstantMessengerInvite
                {
                    SenderMemberId = senderMemberId,
                    InviteDate = invite.InviteDate,
                    ConversationKey = invite.ConversationKey.GUID,
                    CanMemberReply = canMemberReply,
                    ConversationTokenId =
                        (conversationToken != null) ? conversationToken.ConversationTokenId : null
                });
            }

            Log.LogDebugMessage(string.Format("Returning invitations for {0}, total {1}", recipientMemberId, instantMessengerInvites.Count), ErrorHelper.GetCustomData());
            return new NewtonsoftJsonResult {Result = instantMessengerInvites};
        }

        private ConversationToken GetConversationToken(int senderMemberId, int recipientMemberId, int brandId,
            int communityId)
        {
            var membaseId = ConversationToken.GenerateMembaseId(senderMemberId, recipientMemberId, communityId);
            Log.LogInfoMessage(string.Format("Generated membaseid:{0} from senderId:{1}, recipientId:{2}, and communityId{3}", membaseId,
                senderMemberId, recipientMemberId, communityId), ErrorHelper.GetCustomData());
            var conversationToken = InstantMessengerSA.Instance.ReadConversationToken(senderMemberId, recipientMemberId,
                communityId);
            if (conversationToken != null)
            {
                Log.LogDebugMessage(string.Format("ConversationToken retrieved. Id:{0}, SenderId:{1}, RecipientId:{2}, BrandId:{3}",
                    conversationToken.ConversationTokenId, conversationToken.SenderMemberId,
                    conversationToken.RecipientMemberId, brandId), ErrorHelper.GetCustomData());
            }
            else
            {
                Log.LogDebugMessage(string.Format("Null conversationtoken retrieved. SenderId:{0}, RecipientId:{1}, BrandId:{2}",
                    senderMemberId, recipientMemberId, brandId), ErrorHelper.GetCustomData());
            }
            return conversationToken;
        }

        /// <summary>
        ///     Removes invitation from the invitation collection so that it won't bother user again.
        ///     This is for both declining and joining.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Stopwatch]
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        public ActionResult RemoveInvite(InstantMessengerRemoveRequest request)
        {
            var invite = new ConversationInvite
            {
                ConversationKey = ConversationKey.New(request.ConversationKey),
                CommunityID = request.Brand.Site.Community.CommunityID,
                RecipientID = GetMemberId()
            };

            var ctx = System.Web.HttpContext.Current;
            MiscUtils.FireAndForget(o =>
            {
                //set HttpContext for thread
                System.Web.HttpContext.Current = ctx;
                try
                {
                    InstantMessengerSA.Instance.DeclineInvitation(invite);
                }
                finally
                {
                    //unset HttpContext for thread (precaution for GC)
                    System.Web.HttpContext.Current = null;
                }
            },
                "IM RemoveInvite", "Failed to RemoveInvite");
          
            return Ok();
        }

        /// <summary>
        ///     Sends a missed IM message both internally and externally to the target member.
        ///     If the quota has met for the sender, a quota warning message is sent to the sender and nothing is sent to the
        ///     target member.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Stopwatch]
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        public ActionResult SendMissedIm(InstantMessengerSendMissedIMRequest request)
        {
            var member = MemberSA.Instance.GetMember(GetMemberId(), MemberLoadFlags.None);

            if (!member.IsPayingMember(request.Brand.Site.SiteID))
            {
                return new SparkHttpForbiddenResult((int) HttpSub403StatusCode.SubscriptionRequired, String.Empty);
            }

            InstantMessengerSA.Instance.SendMissedIM(GetMemberId(), request.RecipientMemberId, request.Messages, request.Brand);

            var list = ListSA.Instance.GetList(request.RecipientMemberId);

            if (!list.IsHotListed(HotListCategory.IgnoreList, request.Brand.Site.Community.CommunityID, GetMemberId()))
            {
                UserNotificationAccess.AddUserNotificationForIM(request.RecipientMemberId, GetMemberId(), request.Brand);
            }

            return Ok();
        }

        /// <summary>
        ///     Returns true if the recipient is a paying member or if the sender has an All Access subscription
        ///     Deprecated: Look to use CanIM instead.
        /// </summary>
        /// <param name="request">SenderMemberId</param>
        /// <returns></returns>
        [Stopwatch]
        [ApiMethod(ResponseType = typeof (bool))]
        [HttpGet]
        [RequireAccessTokenV2]
        [DeprecatedUse]
        public ActionResult CanMemberReply(InstantMessengerCanMemberReplyRequest request)
        {
            return InstantMessengerSA.Instance.CanMemberReply(request.SenderMemberId, GetMemberId(), request.Brand)
                ? new NewtonsoftJsonResult {Result = true}
                : new NewtonsoftJsonResult {Result = false};
        }

        /// <summary>
        /// Checks all conditions to determine whether an IM can occur between a sender and target member.
        /// Returns true/false as well as a reason why.
        /// 
        /// It is important to correctly specify the "sender" vs "target" because we have rules, such as IM can occur if the sender if an All Access subscriber.
        /// The sender is the one who initiated the IM.
        /// 
        /// The CanInviteCheckOnly is optional and is false by default, indicates whether we only want to check if the sender can perform an invite.  This will basically ignore the fact that the target member may be a non-subscriber.
        /// 
        /// Potential response reasons:
        /// 
        /// SenderInvalid,
        /// TargetInvalid,
        /// SenderIsAllAccess,
        /// BothAreSubscribers,
        /// SenderBlockedTarget,
        /// TargetBlockedSender,
        /// SenderIsNonSubscriber,
        /// TargetIsNonSubscriber,
        /// SenderSuspended,
        /// TargetSuspended,
        /// SenderSelfSuspended,
        /// TargetSelfSuspended,
        /// TargetFraudCheckPending,
        /// SenderFraudCheckPending,
        /// SenderHidden,
        /// TargetHidden,
        /// SenderIsSubscriberCanInvite
        /// 
        /// </summary>
        /// <example>
        /// E.g. Checks if IM can occur between 1127797859 (sender) and 411 (target)
        /// 
        /// GET http://api-temp.stgv3.spark.net/v2/brandId/1003/instantmessenger/canim/1127797859/411/?applicationId=&amp;client_secret=
        /// 
        /// RESPONSE (allowed example)
        /// 
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "CanIM": true,
        ///    "Reason": "BothAreSubscribers"
        ///  }
        ///}
        /// 
        /// RESPONSE (not allowed example)
        /// 
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "CanIM": false,
        ///    "Reason": "SenderIsNonSubscriber"
        ///  }
        ///}
        ///
        /// E.g. Checks if 1127797859 (sender) can initiate and send an IM invite to 411 (target)
        /// 
        /// GET http://api-temp.stgv3.spark.net/v2/brandId/1003/instantmessenger/canim/1127797859/411/true?applicationId=&amp;client_secret=
        /// 
        /// RESPONSE (allowed example)
        /// 
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "CanIM": true,
        ///    "Reason": "SenderIsSubscriberCanInvite"
        ///  }
        ///}
        /// 
        /// 
        /// 
        /// </example>
        /// <param name="SenderMemberId">MemberID of the person who initiated IM</param>
        /// <param name="TargetMemberId">MemberID of the person who received IM invite</param>
        /// <param name="CanInviteCheckOnly">Optional. Default is false, indicates whether we only want to check if the sender can perform an invite.  This will basically ignore the fact that the target member may be a non-subscriber.</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(InstantMessageCanIMResponse))]
        [HttpGet]
        [RequireClientCredentials]
        public ActionResult CanIM(InstantMessageCanIMRequest request)
        {
            InstantMessageCanIMResponse response = new InstantMessageCanIMResponse();

            CanMemberIMResponse saResponse = InstantMessengerSA.Instance.CanMemberIM(request.SenderMemberId, request.TargetMemberId, request.Brand, request.CanInviteCheckOnly);
            response.CanIM = saResponse.CanIM;
            response.Reason = saResponse.Reason.ToString();

            Log.LogDebugMessage(string.Format("CanIM results. SenderId:{0}, TargetId:{1}, BrandId:{2}, CanIM:{3}, Reason:{4}, CanInviteCheckOnly:{5}",
                    request.SenderMemberId, request.TargetMemberId, request.BrandId, response.CanIM, response.Reason, request.CanInviteCheckOnly), ErrorHelper.GetCustomData());

            return new NewtonsoftJsonResult { Result = response };
        }

        /// <summary>
        ///     Once the IM server receives a JID containing a token, it makes a call to this method using client credentials which
        ///     returns a boolean to validate the conversation
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Stopwatch]
        [ApiMethod(ResponseType = typeof (bool))]
        [HttpGet]
        [RequireClientCredentials]
        public ActionResult ValidateConversationToken(InstantMessengerValidateConversationTokenRequest request)
        {
            var conversationToken = GetConversationToken(request.SenderMemberId, request.RecipientMemberId,
                request.CommunityId, request.BrandId);
            if (conversationToken == null)
                return new NewtonsoftJsonResult {Result = false};

            if (string.IsNullOrEmpty(conversationToken.ConversationTokenId) ||
                conversationToken.RecipientMemberId == Constants.NULL_INT ||
                conversationToken.SenderMemberId == Constants.NULL_INT)
                return new NewtonsoftJsonResult {Result = false};

            return conversationToken.ConversationTokenId == request.ConversationTokenId
                ? new NewtonsoftJsonResult {Result = true}
                : new NewtonsoftJsonResult {Result = false};
        }

        /// <summary>
        /// Saves IM message to support IM History
        /// </summary>
        /// <example>
        /// Note: 1017 is the ejabberID applicationID
        /// 
        /// POST http://api-temp.stgv3.spark.net/v2/brandId/1003/application/1017/instantmessenger/saveim?client_secret=
        /// 
        /// {
        ///     "MemberId":  1116690655,
        ///     "ToMemberId":   1116693472 ,
        ///     "MessageSubject": "IM Message",
        ///     "MessageBody": "Where's Ray Carpio",
        ///     "MessageDateTime": "2015-4-19 16:36:00.000",
        ///     "ThreadId": "mosautoray1"
        /// }    
        ///
        /// RESPONSE
        /// 
        /// {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": {
        ///         "SendStatus": "Success",
        ///         "ThreadId": "mosautoray1"
        ///     }
        /// }
        /// 
        /// </example>
        /// <param name="toMemberId">The target memberID; recipient</param>
        /// <param name="memberId">The sender memberID</param>
        /// <param name="threadId">This is the conversation threadID.  Optional for integrated IM endpoint, the API will auto-generate this based on a sliding X minutes gap rule.</param>
        /// <param name="messageSubject">Optional subject.  API will use IM Message, if not provided.</param>
        /// <param name="messageBody">The message, it should already be encoded</param>
        /// <param name="messageDateTime">The UTC datetime when the message was sent</param>
        /// <param name="messageStyles">Integrated IM only. The styles information for the message like font color, size, type</param>
        /// <param name="roomId">Integrated IM only.</param>
        [Stopwatch]
        [ApiMethod(ResponseType = typeof (bool))]
        [RequireClientCredentials]
        [HttpPost]
        public ActionResult PostIMMessage(InstantMessageSaveRequest instantMessageSaveRequest)
        {
            var threadId = instantMessageSaveRequest.ThreadId;

            //Log.DebugFormat("PostIMMessage Request encoding: {0}, header conent type: {1}", Request.ContentEncoding, Request.Headers["Content-Type"]);

            if ((SettingsService.GetSettingFromSingleton("ENABLE_SAVE_IM_HISTORY", instantMessageSaveRequest.Brand.Site.Community.CommunityID, instantMessageSaveRequest.Brand.Site.SiteID, instantMessageSaveRequest.Brand.BrandID)).ToLower() == "true")
            {
                MessageSendResult sendResult = null;
                var fromMemberId = instantMessageSaveRequest.MemberId;
                var toMemberId = instantMessageSaveRequest.ToMemberId;
                const MailType mailType = MailType.InstantMessage;
                var messageSubject = instantMessageSaveRequest.MessageSubject;
                var messageBody = HttpUtility.HtmlDecode(instantMessageSaveRequest.MessageBody);

                var messageDateTimePST = DateTime.Now;
                if (instantMessageSaveRequest.MessageDateTime != DateTime.MinValue)
                {
                    messageDateTimePST = TimeZoneInfo.ConvertTimeFromUtc(instantMessageSaveRequest.MessageDateTime, zonePST);
                }

                //temp logging for debugging
                //Log.DebugFormat("PostIMMessage for brandID:{0}, toMemberID:{1}, fromMemberID:{2}, threadId:{3}, datetime:{4}, subject:{5}, body:{6}, bodyDecoded:{7}",
                //            instantMessageSaveRequest.BrandId, instantMessageSaveRequest.ToMemberId, instantMessageSaveRequest.MemberId, threadId, instantMessageSaveRequest.MessageDateTime, instantMessageSaveRequest.MessageSubject, instantMessageSaveRequest.MessageBody, messageBody);
                try
                {
                    var brand = instantMessageSaveRequest.Brand;
                    sendResult = MessageAccess.Instance.SendMessage(brand, fromMemberId, toMemberId, mailType, threadId,
                        messageSubject, messageBody, messageDateTimePST);
                }
                catch (Exception e)
                {
                    Log.LogError(string.Format("PostIMMessage failed for brandID:{0}, toMemberID:{1}, fromMemberID:{2}, threadId:{3}",
                            instantMessageSaveRequest.BrandId, toMemberId, fromMemberId, threadId), e, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(instantMessageSaveRequest.Brand));
                }

                if (null == sendResult) sendResult = new MessageSendResult(new MessageSendError());
                return new NewtonsoftJsonResult
                {
                    Result = new { SendStatus = sendResult.Status.ToString(), ThreadId = threadId }
                };
            }
            else
            {
                Log.LogDebugMessage("PostIMMessage - ENABLE_SAVE_IM_HISTORY setting is disabled", ErrorHelper.GetCustomData());
                return new NewtonsoftJsonResult
                {
                    Result = new { SendStatus = MessageSendStatus.Success.ToString(), ThreadId = threadId }
                };
            }
        }

        /// <summary>
        /// Saves IM message to support IM History, this is the integrated IM with Utah version.
        /// </summary>
        /// <example>
        /// POST http://api-temp.stgv3.spark.net/v2/brandId/1003/instantmessenger/saveintegratedim?applicationId=&amp;client_secret=
        /// 
        /// {
        ///     "MemberId":  1116690655,
        ///     "ToMemberId":   1116693472 ,
        ///     "MessageBody": "Where's Ray Carpio",
        ///     "MessageDateTime": "2015-4-19 16:36:00.000",
        ///     "RoomId": 100,
        ///     "MessageStyles": "fc-1 ff-1 fz-5"
        /// }    
        ///
        /// RESPONSE
        /// 
        /// {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": {
        ///         "SendStatus": "Success",
        ///         "ThreadId": ""
        ///     }
        /// }
        /// </example>
        /// <param name="toMemberId">The target memberID; recipient</param>
        /// <param name="memberId">The sender memberID</param>
        /// <param name="threadId">This is the conversation threadID.  Optional for integrated IM endpoint, the API will auto-generate this based on a sliding X minutes gap rule.</param>
        /// <param name="messageSubject">Optional subject.  API will use IM Message, if not provided.</param>
        /// <param name="messageBody">The message, it should already be encoded</param>
        /// <param name="messageDateTime">The UTC datetime when the message was sent</param>
        /// <param name="messageStyles">Integrated IM only. The styles information for the message like font color, size, type</param>
        /// <param name="roomId">Integrated IM only.</param>
        /// <returns></returns>
        [Stopwatch]
        [ApiMethod(ResponseType = typeof(bool))]
        [RequireClientCredentials]
        [HttpPost]
        public ActionResult PostIntegratedIMMessage(InstantMessageSaveRequest instantMessageSaveRequest)
        {
            //Log.DebugFormat("PostIMMessage Request encoding: {0}, header conent type: {1}", Request.ContentEncoding, Request.Headers["Content-Type"]);

            if ((SettingsService.GetSettingFromSingleton("ENABLE_SAVE_IM_HISTORY", instantMessageSaveRequest.Brand.Site.Community.CommunityID, instantMessageSaveRequest.Brand.Site.SiteID, instantMessageSaveRequest.Brand.BrandID)).ToLower() == "true")
            {
                MessageSendResult sendResult = null;
                var fromMemberId = instantMessageSaveRequest.MemberId;
                var toMemberId = instantMessageSaveRequest.ToMemberId;
                var mailType = MailType.InstantMessage;
                var roomId = instantMessageSaveRequest.RoomId;
                var styles = instantMessageSaveRequest.MessageStyles;
                var messageSubject = instantMessageSaveRequest.MessageSubject;
                if (string.IsNullOrEmpty(messageSubject))
                {
                    messageSubject = "IM Message";
                }
                var messageBody = HttpUtility.HtmlDecode(instantMessageSaveRequest.MessageBody);

                //date is passed in as UTC, convert to pst because all email messages in our db are pst
                var messageDateTimePST = DateTime.Now;
                if (instantMessageSaveRequest.MessageDateTime != DateTime.MinValue)
                {
                    messageDateTimePST = TimeZoneInfo.ConvertTimeFromUtc(instantMessageSaveRequest.MessageDateTime, zonePST);
                }

                //convert photo to sparktag so our existing IM history works without additional changes
                //Photo from consolidated IM: <sparkimgtag image="http://s3.amazonaws.com/sparkmessageattachmentssv3/attachments/c09000f9e3a86b668c4389aaf564be07/preview/222010905.jpg" attachmentid="1519">
                //Photo from previous IM: <sparktag type="photo" alt="david1" src="&#x2F;attachments&#x2F;d82741f62d61a8a233ae7248a3a08aa3&#x2F;preview&#x2F;222007711.jpg" id="1394" data-originalExt="jpg"/>
                if (messageBody.IndexOf("sparkimgtag") > 0)
                {
                    messageBody = messageBody.Replace("<sparkimgtag", "<sparktag type=\"photo\" data-originalExt=\"jpg\"");
                    messageBody = messageBody.Replace("sparkimgtag", "sparktag");
                    messageBody = messageBody.Replace("attachmentid=", "id=");
                    messageBody = messageBody.Replace("image=", "src=");
                    messageBody = messageBody.Replace(ConfigurationManager.AppSettings["MessageAttachmentURLPath"], "");
                }

                //add misc roomid and styles info (font, background, etc) as part of message
                if (!string.IsNullOrEmpty(instantMessageSaveRequest.MessageStyles))
                {
                    var sparkMiscTag = "<sparktag type=\"messageMisc\"" + " roomid=\"" + instantMessageSaveRequest.RoomId + "\"" + " class=\"" + instantMessageSaveRequest.MessageStyles + "\"/>";
                    messageBody += sparkMiscTag;
                }

                //create threadID (conversation thread) based on sliding x minutes gap rule
                int gapMinutes = 20;
                string threadId = "";
                DateTime lastMessageDate = DateTime.MinValue;
                LastConversation lastConversation = MessageAccess.Instance.GetInstantMessageLastConversation(fromMemberId, toMemberId, instantMessageSaveRequest.Brand.Site.Community.CommunityID);
                if (lastConversation != null)
                {
                    lastMessageDate = lastConversation.InsertDate;
                    if (DateTime.Now > lastConversation.InsertDate && lastConversation.InsertDate > DateTime.MinValue)
                    {
                        if (DateTime.Now.Subtract(lastConversation.InsertDate).Minutes <= gapMinutes)
                        {
                            threadId = lastConversation.ThreadID;
                        }
                    }
                    else
                    {
                        Log.LogWarningMessage(string.Format("PostIntegratedIMMesage - Last conversation insert date is greater than now, how is that possible. brandID:{0}, toMemberID:{1}, fromMemberID:{2}. Date Now: {3}, Date Last Conversation: {4}, Last MessageListID: {5}", instantMessageSaveRequest.BrandId, toMemberId, fromMemberId, DateTime.Now, lastConversation.InsertDate, threadId), ErrorHelper.GetCustomData());
                    }
                }

                if (string.IsNullOrEmpty(threadId))
                {
                    threadId = Guid.NewGuid().ToString();
                    Log.LogInfoMessage(string.Format("PostIntegratedIMMessage - Creating new threadID. MemberID: {0}, ToMemberID: {1}, CommunityID: {2}, DateTime now: {3}, Last Message DateTime: {4}, ThreadID: {5}", fromMemberId, toMemberId, instantMessageSaveRequest.Brand.Site.Community.CommunityID, DateTime.Now, lastMessageDate, threadId), ErrorHelper.GetCustomData());

                    //clear cache for mail conversation member list
                    MessageAccess.Instance.RemoveMailConversationMembersCache(fromMemberId, instantMessageSaveRequest.Brand.Site.Community.CommunityID, ConversationType.Inbox);

                }

                //temp logging for debugging
                //Log.DebugFormat("PostIntegratedIMMessage for brandID:{0}, toMemberID:{1}, fromMemberID:{2}, threadId:{3}, datetime:{4}, subject:{5}, body:{6}, bodyDecoded:{7}, styles:{8}, roomId:{9}, newConversation:{10}",
                //            instantMessageSaveRequest.BrandId, instantMessageSaveRequest.ToMemberId, instantMessageSaveRequest.MemberId, threadId, instantMessageSaveRequest.MessageDateTime, instantMessageSaveRequest.MessageSubject, instantMessageSaveRequest.MessageBody, messageBody, styles, roomId, newConversation);
                
                //save message
                var brand = instantMessageSaveRequest.Brand;
                try
                {
                    sendResult = MessageAccess.Instance.SendMessage(brand, fromMemberId, toMemberId, mailType, threadId,
                        messageSubject, messageBody, messageDateTimePST);
                }
                catch (Exception e)
                {
                    Log.LogError(
                        string.Format(
                            "PostIntegratedIMMessage failed for brandID:{0}, toMemberID:{1}, fromMemberID:{2}, threadId:{3}",
                            instantMessageSaveRequest.BrandId, toMemberId, fromMemberId, threadId), e, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                }

                if (null == sendResult) sendResult = new MessageSendResult(new MessageSendError());

                return new NewtonsoftJsonResult
                {
                    Result = new { SendStatus = sendResult.Status.ToString(), ThreadId = threadId }
                };
            }
            else
            {
                Log.LogDebugMessage("PostIntegratedIMMessage - ENABLE_SAVE_IM_HISTORY setting is disabled", ErrorHelper.GetCustomData());
                return new NewtonsoftJsonResult
                {
                    Result = new { SendStatus = MessageSendStatus.Success.ToString(), ThreadId = "" }
                };
            }
        }

        /// <summary>
        /// Gets IM History messages between 2 members
        /// Note: For new implementations, consider swapping over to the GetMailConversationMessage endpoint which also supports getting IM messages by threadID
        /// </summary>
        /// <example>
        /// 
        /// E.g. This will get IM messages between me (based on access token) and memberID 1116693286, the first page with 3 results
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/instantmessenger/getInstantMessageHistory/targetMemberID/1116693286/page/1/pagesize/3?access_token=
        /// 
        /// Note: Optionally ignore cache using http://api.stage3.spark.net/v2/brandId/1003/instantmessenger/getInstantMessageHistory/targetMemberID/1116693286/page/1/pagesize/3/true?access_token=
        /// Note: InsertDate are in UTC as of 5/27/2015, added InsertDatePst for those that still need it
        /// 
        /// RESPONSE
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "GroupId": 3,
        ///    "MemberId": 1116690655,
        ///    "TargetMemberId": 1116693286,
        ///    "MatchesFound": 44, //the total available result count, so you can control your pagination accordingly
        ///    "MessageList": [
        ///      {
        ///        "id": 562638181,
        ///        "sender": null,
        ///        "recipient": null,
        ///        "subject": "IM Message",
        ///        "mailType": "InstantMessage",
        ///        "body": "cool, encoding all text when rebuilding message!",
        ///        "insertDate": "2015-01-12T13:57:33.577Z",
        ///        "openDate": null,
        ///        "messageStatus": 5,
        ///        "memberFolderId": 3,
        ///        "isAllAccess": false,
        ///        "senderID": 1116690655
        ///      },
        ///      {
        ///        "id": 628407555,
        ///        "sender": null,
        ///        "recipient": null,
        ///        "subject": "IM Message",
        ///        "mailType": "InstantMessage",
        ///        "body": "hello right back",
        ///        "insertDate": "2015-01-12T13:57:08.813Z",
        ///        "openDate": null,
        ///        "messageStatus": 5,
        ///        "memberFolderId": 3,
        ///        "isAllAccess": false,
        ///        "senderID": 1116693286
        ///      },
        ///      {
        ///        "id": 562638175,
        ///        "sender": null,
        ///        "recipient": null,
        ///        "subject": "IM Message",
        ///        "mailType": "InstantMessage",
        ///        "body": "hello bob",
        ///        "insertDate": "2015-01-12T13:56:54.583Z",
        ///        "openDate": null,
        ///        "messageStatus": 5,
        ///        "memberFolderId": 3,
        ///        "isAllAccess": false,
        ///        "senderID": 1116690655
        ///      }
        ///    ]
        ///  }
        ///}
        /// </example>
        /// <param name="targetMemberId">The target memberID that you had instant messages with</param>
        /// <param name="pageNumber">The page number for the page of messages being requested</param>
        /// <param name="pageSize">The number of messages per page</param>
        /// <param name="memberId">Not used, derived from access token</param>
        /// <param name="brandId">Not used, specified as part of URL path</param>
        /// <param name="siteId">Not used</param>
        /// <param name="Brand">Not used</param>
        [ApiMethod(ResponseType = typeof(InstantMessageHistoryResponse))]
        [RequirePayingMemberV2]
        [MembersOnlineActionFilter]
        public ActionResult GetInstantMessageHistory(InstantMessageHistoryRequest instantMessageHistoryRequest)
        {
            InstantMessageHistoryResponse response = new InstantMessageHistoryResponse();
            response.MemberId = GetMemberId();
            response.TargetMemberId = instantMessageHistoryRequest.TargetMemberID;
            response.GroupId = instantMessageHistoryRequest.Brand.Site.Community.CommunityID;

            //get IM history messages
            int totalMessages = 0;
            response.MessageList = MessageAccess.Instance.GetInstantMessageHistory(response.MemberId, response.TargetMemberId, instantMessageHistoryRequest.Brand, instantMessageHistoryRequest.PageNumber, instantMessageHistoryRequest.PageSize, "", instantMessageHistoryRequest.IgnoreCache, out totalMessages);
            response.MatchesFound = totalMessages;

            return new NewtonsoftJsonResult { Result = response };
        }

        /// <summary>
        /// Gets a list of members that you've had IM conversations with
        /// Note: For new implementations, consider swapping over to the GetMailConversationMember endpoint
        /// </summary>
        /// <param name="instantMessageHistoryMembersRequest">The instant message history members request.</param>
        /// <returns></returns>
        /// <example>
        /// GET http://api.stage3.spark.net/v2/brandId/1003/instantmessenger/getInstantMessageHistoryMembers?access_token=
        /// Note: Optionally ignore cache using  http://api.stage3.spark.net/v2/brandId/1003/instantmessenger/getInstantMessageHistoryMembers/true?access_token=
        /// Note: Last message InsertDates are in UTC as of 5/27/2015, added InsertDatePst for those that still need it
        /// 
        /// RESPONSE
        /// {
        /// "code": 200,
        /// "status": "OK",
        /// "data": {
        /// "GroupId": 3,
        /// "MemberId": 1116690655,
        /// "MatchesFound": 4,
        /// "MemberList": [
        /// {
        /// "Id": 562639843,
        /// "MemberId": 1116693286,
        /// "Member": {
        /// "memberId": 1116693286,
        /// "username": "1116693286",
        /// "primaryPhoto": {
        /// "memberPhotoId": 17001569,
        /// "fullId": 222003306,
        /// "thumbId": 222003574,
        /// "fullPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2014/04/04/11/222003306.jpg",
        /// "thumbPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2014/04/04/11/222003574.jpg",
        /// "caption": "",
        /// "listorder": 1,
        /// "isCaptionApproved": false,
        /// "isPhotoApproved": true,
        /// "IsMain": true,
        /// "IsApprovedForMain": true
        /// },
        /// "maritalStatus": "Divorced",
        /// "gender": "Male",
        /// "seekingGender": "Female",
        /// "lookingFor": [],
        /// "age": 28,
        /// "location": "Ashington, ",
        /// "lastLoggedIn": "2015-02-05T01:54:46.873Z",
        /// "lastUpdated": "2013-08-23T23:50:47.46Z",
        /// "isOnline": false,
        /// "jDateEthnicity": "Ashkenazi",
        /// "registrationDate": "2013-09-10T16:49:25.813Z",
        /// "passedFraudCheck": true,
        /// "approvedPhotoCount": 5,
        /// "zipcode": null,
        /// "regionId": 9799184,
        /// "heightCm": 137,
        /// "selfSuspendedFlag": false,
        /// "adminSuspendedFlag": false,
        /// "blockContactByTarget": false,
        /// "blockSearchByTarget": false,
        /// "removeFromSearchByTarget": false,
        /// "blockContact": false,
        /// "blockSearch": false,
        /// "removeFromSearch": false,
        /// "isHighlighted": false,
        /// "isViewersFavorite": true,
        /// "isSpotlighted": false,
        /// "DefaultPhoto": {
        /// "memberPhotoId": 0,
        /// "fullId": 222003306,
        /// "thumbId": 222003574,
        /// "fullPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2014/04/04/11/222003306.jpg",
        /// "thumbPath": "http://s3.amazonaws.com/sparkmemberphotossv3/2014/04/04/11/222003574.jpg",
        /// "caption": "",
        /// "listorder": 0,
        /// "isCaptionApproved": false,
        /// "isPhotoApproved": true,
        /// "IsMain": false,
        /// "IsApprovedForMain": false
        /// },
        /// "PhotoUrl": "http://s3.amazonaws.com/sparkmemberphotossv3/2014/04/04/11/222003306.jpg",
        /// "ThumbnailUrl": "http://s3.amazonaws.com/sparkmemberphotossv3/2014/04/04/11/222003574.jpg"
        /// },
        /// "GroupId": 3,
        /// "InsertDate": "2015-02-03T15:46:48.737Z",
        /// "MessageStatus": 5,
        /// "MailType": "InstantMessage"
        /// },
        /// ....
        /// ]
        /// }
        /// }
        /// </example>
        [ApiMethod(ResponseType = typeof(InstantMessageHistoryMembersResponse))]
        [RequirePayingMemberV2]
        [MembersOnlineActionFilter]
        public ActionResult GetInstantMessageHistoryMembers(InstantMessageHistoryMembersRequest instantMessageHistoryMembersRequest)
        {
            var response = new InstantMessageHistoryMembersResponse
            {
                MemberId = GetMemberId(),
                GroupId = instantMessageHistoryMembersRequest.Brand.Site.Community.CommunityID
            };

            //get IM history members you've had conversations with
            int totalMembers;
            response.MemberList = MessageAccess.Instance.GetInstantMessageHistoryMembers(response.MemberId, instantMessageHistoryMembersRequest.Brand, 0, 0, instantMessageHistoryMembersRequest.IgnoreCache, 0, out totalMembers);
            response.MatchesFound = totalMembers;

            return new NewtonsoftJsonResult { Result = response };
        }

        /// <summary>
        /// Removes or re-adds a member from your IM history members conversation list
        /// Note: Member will show back up in your conversation list if you start a new IM conversation with member
        /// </summary>
        /// <example>
        /// 
        /// This will remove member based on message Id 628404777 from your IM conversation member list
        /// Note: Notice we're passing in undo as "false", which is to remove.  You can change that to true to re-add the member.
        /// 
        /// GET http://api.stage3.spark.net/v2/brandId/1003/instantmessenger/removeInstantMessageMember/messageId/628404777/undo/false?access_token=
        /// 
        /// RESPONSE
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "success": true,
        ///    "messageId": 628404777,
        ///    "failReason": "",
        ///  }
        ///}
        /// 
        /// 
        /// </example>
        /// <param name="MessageId">This is the id included in the results of the getmailconversationinboxmembers endpoint, pass that in to remove that member from the list</param>
        /// <param name="Undo">False means you're removing the member, True allows you to undo that and re-adds the member</param>
        /// <param name="memberId">Not used, derived from access token</param>
        /// <param name="brandId">Not used, specified as part of URL path</param>
        /// <param name="siteId">Not used</param>
        /// <param name="Brand">Not used</param>
        [ApiMethod(ResponseType = typeof(MessageMemberRemoveResponse))]
        [RequirePayingMemberV2]
        [MembersOnlineActionFilter]
        public ActionResult RemoveInstantMessageMember(InstantMessageMemberRemoveRequest removeRequest)
        {
            bool removed;

            removed = EmailMessageSA.Instance.MarkInstantMessageMemberRemoved(GetMemberId(),
                                                                removeRequest.Brand.Site.Community.CommunityID,
                                                                !removeRequest.Undo,
                                                                new ArrayList { removeRequest.MessageId },
                                                                null);

            var result = new MessageMemberRemoveResponse
            {
                MessageId = removeRequest.MessageId,
                Success = removed,
                FailureReason = removed ? "" : "MessageId not found"
            };

            return new NewtonsoftJsonResult { Result = result };
        }

        /// <summary>
        /// Pass-through endpoint to Utah. This API call can be used to create a chat conversation between two members
        /// https://confluence.matchnet.com/display/SOFTENG/createConversation
        /// 
        /// </summary>
        /// <example>
        /// 
        /// POST http://api.stage3.spark.net/v2/brandid/1003/chat/createConversation?access_token=
        /// 
        /// {
        ///     "to_memberid":  1116693286
        /// }
        /// 
        /// RESPONSE
        /// Look at Utah doc: https://confluence.matchnet.com/display/SOFTENG/createConversation
        /// 
        /// </example>
        /// <param name="to_memberid">This is the BH member id of the profile you want to start chatting with</param>
        /// <param name="access_token">Provide this in the URL for authorization</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(string))]
        [RequireAccessTokenV2]
        public ActionResult CreateConversation(InstantMessengerCreateConversationRequest request)
        {
            string mingleJsonResult = MingleApiAdapter.Instance.CreateConversation(request);

            return new JsonStringResult(mingleJsonResult);
        }

        /// <summary>
        /// Pass-through endpoint to Utah. This API call can be used to log that an invite was sent.  It populates lists and push notifications.
        /// https://confluence.matchnet.com/display/SOFTENG/recordInviteSent
        /// </summary>
        /// <example>
        /// 
        /// GET http://api.stage3.spark.net/v2/brandid/1003/chat/recordInviteSent/10101/1116693286?access_token=
        /// 
        /// 
        /// RESPONSE
        /// Look at Utah doc: https://confluence.matchnet.com/display/SOFTENG/recordInviteSent
        /// 
        /// </example>
        /// <param name="to_memberid">This is the BH member id of the profile you want to start chatting with</param>
        /// <param name="access_token">Provide this in the URL for authorization</param>
        /// <param name="roomid">The chat room id</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(string))]
        [RequireAccessTokenV2]
        public ActionResult RecordInviteSent(InstantMessengerRecordInviteSentRequest request)
        {
            string mingleJsonResult = MingleApiAdapter.Instance.RecordInviteSent(request);

            return new JsonStringResult(mingleJsonResult);
        }

        /// <summary>
        /// Pass-through endpoint to Utah. This API call can be used to create a chat conversation between two members
        /// https://confluence.matchnet.com/display/SOFTENG/validateInvite
        /// </summary>
        /// <example>
        /// 
        /// GET http://api.stage3.spark.net/v2/brandid/1003/chat/validateInvite/10101/1116693286/1116690655/?access_token=
        /// 
        /// 
        /// RESPONSE
        /// Look at Utah doc: https://confluence.matchnet.com/display/SOFTENG/validateInvite
        /// 
        /// </example>
        /// <param name="to_memberid">This is the BH member id of the profile you want to start chatting with</param>
        /// <param name="from_memberid">This is the BH member id of member that initiated the chat</param>
        /// <param name="access_token">Provide this in the URL for authorization</param>
        /// <param name="roomid">The chat room id</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(string))]
        [RequireAccessTokenV2]
        public ActionResult ValidateInvite(InstantMessengerValidateInviteRequest request)
        {
            string mingleJsonResult = MingleApiAdapter.Instance.ValidateInvite(request);

            return new JsonStringResult(mingleJsonResult);
        }

        /// <summary>
        /// Pass-through endpoint to Utah. This API call can be used to pull information about the other member in the chat.  It also records that a member joined a chat.
        /// https://confluence.matchnet.com/display/SOFTENG/getIMInfo
        /// </summary>
        ///  <example>
        ///  
        /// The imid in the URL is the roomID.
        /// /brandid/{brandid}/chat/getIMInfo?access_token={token}&amp;imid={roomid}
        /// 
        /// GET http://api.stage3.spark.net/v2/brandid/1003/chat/getIMInfo?access_token=&amp;imid=10101
        /// 
        /// 
        /// RESPONSE
        /// Look at Utah doc: https://confluence.matchnet.com/display/SOFTENG/getIMInfo
        /// 
        /// </example>
        /// <param name="access_token">Provide this in the URL for authorization</param>
        /// <param name="imid">The chat room id, provide this in the URL with key: imid</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(string))]
        [RequireAccessTokenV2]
        public ActionResult GetIMInfo(InstantMessengerGetImInfoRequest request)
        {
            string mingleJsonResult = MingleApiAdapter.Instance.GetIMInfo(request);

            return new JsonStringResult(mingleJsonResult);
        }
    }
}