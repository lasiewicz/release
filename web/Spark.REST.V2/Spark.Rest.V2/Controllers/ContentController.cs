﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Registration;
using Matchnet.Email.ServiceAdapters;
using Spark.Common.Localization;
using Spark.Logger;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.DataAccess.Content;
using Spark.Rest.Entities.Content.Promotion;
using Spark.Rest.Helpers;
using Spark.Rest.Models.Content.Promotion;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Configuration;
using Spark.Rest.V2.Entities.Content.AttributeData;
using Spark.Rest.V2.Entities.Content.RegionData;
using Spark.Rest.V2.Models.Content.Region;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.DataAccess.Content;
using Spark.REST.Entities.Content;
using Spark.REST.Entities.Content.AttributeData;
using Spark.REST.Entities.Content.BrandConfig;
using Spark.REST.Entities.Content.RegionData;
using Spark.REST.Entities.Mail;
using Spark.REST.Models.Content;
using Spark.REST.Models.Content.AttributeMetaData;
using Spark.REST.Models.Content.BrandConfig;
using Spark.REST.Models.Content.Pixels;
using Spark.REST.Models.Content.Promotion;
using Spark.REST.Models.Content.Region;
using Spark.REST.Models.Mail;
using Scenario = Spark.REST.Entities.Content.Registration.Scenario;

#endregion

namespace Spark.REST.Controllers
{
    public class ContentController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(ContentController));

        /// <summary>
        /// Logs the information.
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="message">The message.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <returns></returns>
        [Obsolete]
        [ApiMethod(ResponseType = typeof (void))]
        [HttpPut]
        public ActionResult LogInfo(string client, string message, string timestamp)
        {
            var canLog = (SettingsService.SettingExistsFromSingleton("API_LOG_EXTERNAL_CLIENTS", 0, 0)) && Boolean.TrueString.ToLower()
                .Equals(SettingsService.GetSettingFromSingleton("API_LOG_EXTERNAL_CLIENTS"));
            if (canLog)
            {
                if (!string.IsNullOrEmpty(message))
                {
                    Log.LogInfoMessage(string.Format("Client: {0}, Timestamp: {1}, Info: {2}", client, timestamp, message), GetCustomDataForReportingErrors());
                }
            }
            return Ok();
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="message">The message.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <returns></returns>
        [Obsolete]
        [ApiMethod(ResponseType = typeof (void))]
        [HttpPut]
        public ActionResult LogError(string client, string message, string timestamp)
        {
            var canLog = (SettingsService.SettingExistsFromSingleton("API_LOG_EXTERNAL_CLIENTS", 0, 0)) && Boolean.TrueString.ToLower()
                .Equals(SettingsService.GetSettingFromSingleton("API_LOG_EXTERNAL_CLIENTS"));
            if (!canLog) return Ok();
            if (!string.IsNullOrEmpty(message))
            {
                Log.LogError(string.Format("Client: {0}, Timestamp: {1}, Error: {2}", client, timestamp, message), new Exception(),GetCustomDataForReportingErrors(),ErrorHelper.ShouldLogExternally());
            }
            return Ok();
        }

        /// <summary>
        /// Gets the brand by domain.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [Obsolete("to be removed.  GetBrand will be used instead")]
        [OutputCache(Duration = 10000, Location = OutputCacheLocation.Any)]
        public ActionResult GetBrandByDomain(BrandRequest request)
        {
            try
            {
                var brandByUri = BrandConfigSA.Instance.GetBrandByURI(request.BrandUri);

                var brand = new Brand
                {
                    Id = brandByUri.BrandID,
                    Site = new Site
                    {
                        Id = brandByUri.Site.SiteID,
                        Name = brandByUri.Site.Name,
                        Community = new Community
                        {
                            Id = brandByUri.Site.Community.CommunityID,
                            Name = brandByUri.Site.Community.Name
                        }
                    },
                    Uri = brandByUri.Uri
                };

                return new NewtonsoftJsonResult {Result = brand};
            }
            catch (Exception ex)
            {
                Log.LogError("Error getting brand.", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidBrandId, ex.ToString());
            }
        }

        /// <summary>
        /// Gets the brand.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OutputCache(Duration = 10000, Location = OutputCacheLocation.Any, VaryByParam = "brandId")]
        public ActionResult GetBrand(Models.BrandRequest request)
        {
            try
            {
                var brand = new Brand
                {
                    Id = request.Brand.BrandID,
                    Site = new Site
                    {
                        Id = request.Brand.Site.SiteID,
                        Name = request.Brand.Site.Name,
                        Community = new Community
                        {
                            Id = request.Brand.Site.Community.CommunityID,
                            Name = request.Brand.Site.Community.Name
                        }
                    },
                    Uri = request.Brand.Uri
                };

                return new NewtonsoftJsonResult {Result = brand};
            }
            catch (Exception ex)
            {
                Log.LogError("Error getting brand.", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidBrandId, ex.ToString());
            }
        }

        /// <summary>Gets a list of options for a given attribute and brand</summary>
        /// <example>
        ///     Get a list of options for religion attribute
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/attributes/options/religion
        ///     RESPONSE
        ///     Note: Some response data removed to keep example short
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": [{
        ///     "AttributeOptionId": 74282,
        ///     "Description": "",
        ///     "ListOrder": 1,
        ///     "Value": 0
        ///     },
        ///     {
        ///     "AttributeOptionId": 75708,
        ///     "Description": "Anglican",
        ///     "ListOrder": 3,
        ///     "Value": 2
        ///     },
        ///     {
        ///     "AttributeOptionId": 74284,
        ///     "Description": "Atheist",
        ///     "ListOrder": 4,
        ///     "Value": 131072
        ///     },
        ///     {
        ///     "AttributeOptionId": 75710,
        ///     "Description": "Baptist",
        ///     "ListOrder": 5,
        ///     "Value": 4
        ///     },
        ///     ...
        ///     ]
        ///     }
        /// </example>
        /// <param name="attributeName" required="true">The attribute name; passed in as part of URL</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        [ApiMethod(ResponseType = typeof (List<AttributeOption>))]
        [OutputCache(Duration = 10000, Location = OutputCacheLocation.Any, VaryByParam = "attributeName;brandId")]
        public ActionResult GetAttributeOptions(AttributeOptionsRequest request)
        {
            var attributeOptions = AttributeDataAccess.Instance.GetAttributeOptions(request.BrandId, request.AttributeName);
            return new NewtonsoftJsonResult {Result = attributeOptions};
        }

        /// <summary>Gets attribute options for each of the attributes specified</summary>
        /// <example>
        ///     Get attribute options for marital status and smokinghabits
        ///     POST http://api.local.spark.net/v2/brandId/1003/content/attributes/optionscollection
        ///     {
        ///     "attributeNamesJson": "[\"MaritalStatus\", \"smokinghabits\"]"
        ///     }
        ///     RESPONSE
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": [{
        ///     "AttributeName": "MaritalStatus",
        ///     "AttributeOptionList": [{
        ///     "AttributeOptionId": 75376,
        ///     "Description": "",
        ///     "ListOrder": 1,
        ///     "Value": -2147483647
        ///     },
        ///     {
        ///     "AttributeOptionId": 75378,
        ///     "Description": "Single",
        ///     "ListOrder": 2,
        ///     "Value": 2
        ///     },
        ///     {
        ///     "AttributeOptionId": 75380,
        ///     "Description": "Divorced",
        ///     "ListOrder": 3,
        ///     "Value": 8
        ///     },
        ///     {
        ///     "AttributeOptionId": 75382,
        ///     "Description": "Separated",
        ///     "ListOrder": 4,
        ///     "Value": 16
        ///     },
        ///     {
        ///     "AttributeOptionId": 75384,
        ///     "Description": "Widowed",
        ///     "ListOrder": 5,
        ///     "Value": 32
        ///     }]
        ///     },
        ///     {
        ///     "AttributeName": "smokinghabits",
        ///     "AttributeOptionList": [{
        ///     "AttributeOptionId": 74260,
        ///     "Description": "",
        ///     "ListOrder": 1,
        ///     "Value": 0
        ///     },
        ///     {
        ///     "AttributeOptionId": 74261,
        ///     "Description": "Non-Smoker",
        ///     "ListOrder": 2,
        ///     "Value": 2
        ///     },
        ///     {
        ///     "AttributeOptionId": 74262,
        ///     "Description": "Occasionally",
        ///     "ListOrder": 3,
        ///     "Value": 4
        ///     },
        ///     {
        ///     "AttributeOptionId": 74263,
        ///     "Description": "Regularly",
        ///     "ListOrder": 4,
        ///     "Value": 8
        ///     },
        ///     {
        ///     "AttributeOptionId": 105452,
        ///     "Description": "Trying to quit",
        ///     "ListOrder": 5,
        ///     "Value": 16
        ///     }]
        ///     }]
        ///     }
        /// </example>
        /// <param name="attributeNames">List of attribute names</param>
        /// <param name="attributeNamesJson">
        ///     JSON array of attribute names; Use of this will overwrite values in AttributeNames
        ///     param
        /// </param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <returns></returns>
        [HttpPost]
        [ApiMethod(ResponseType = typeof (List<AttributeOptions>))]
        // using both attributeNames and attributeNamesJson ensures either one will be used for caching.
        [SparkOutputCache(Duration = 3600, VaryByParam = "attributeNames;attributeNamesJson")]
        public ActionResult GetAttributeOptionsCollection(Models.BrandRequest request, AttributeNames attributeNames,
            string attributeNamesJson)
        {
            // when the json param has a value, attempt to deserialize with populate attriuteNames
            if (!string.IsNullOrEmpty(attributeNamesJson))
            {
                var ser = new JavaScriptSerializer();
                try
                {
                    attributeNames = ser.Deserialize<AttributeNames>(attributeNamesJson);
                }
                catch (Exception)
                {
                    // could easily blow up with bad json data
                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.AttributeMappingFailure,
                        string.Format("error deserializing {0}", attributeNamesJson));
                }
            }
            var attributeOptionsCollection = AttributeDataAccess.Instance.GetAttributeOptionsCollection(
                request.BrandId, attributeNames);
            return new NewtonsoftJsonResult {Result = attributeOptionsCollection};
        }

        /// <summary>Gets the metadata for a given attribute name; use to get the attribute id and data type</summary>
        /// <example>
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/attributes/metadata/smokinghabits
        ///     RESPONSE
        ///     {"code":200,"status":"OK","data":{"Id":92,"DataType":"Number"}}
        /// </example>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (AttributeMetadata))]
        [OutputCache(Duration = 10000, Location = OutputCacheLocation.Any, VaryByParam = "attributeName;brandId")]
        public ActionResult GetAttributeMetadata(AttributeMetadataRequest request)
        {
            return new NewtonsoftJsonResult
            {
                Result = AttributeDataAccess.Instance.GetAttributeMetadata(request.AttributeName)
            };
        }

        /// <summary>
        ///     Gets a runtime "boolean" setting, only use if the setting value can be interpreted and returned as true/false.
        ///     Note: To get back any setting as just a "string", use the getruntimesettingsvalue api
        /// </summary>
        /// <example>
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/runtimesetting/ENABLE_TABBED_VIEW_PROFILE
        ///     {"code":200,"status":"OK","data":{"name":"ENABLE_TABBED_VIEW_PROFILE","isEnabled":true}}
        /// </example>
        /// <param name="SettingName" required="true">The name of the setting</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        [ApiMethod(ResponseType = typeof (Setting))]
        public ActionResult GetRuntimeSetting(SettingRequest request)
        {
            var exists = SettingsService.SettingExistsFromSingleton(request.SettingName,
                request.Brand.Site.Community.CommunityID,
                request.Brand.Site.SiteID);
            Setting setting;
            if (exists)
            {
                var isEnabled =
                    (Convert.ToBoolean(SettingsService.GetSettingFromSingleton(request.SettingName,
                        request.Brand.Site.Community.CommunityID,
                        request.Brand.Site.SiteID,
                        request.Brand.BrandID)));
                setting = new Setting {IsEnabled = isEnabled, Name = request.SettingName};
            }
            else
            {
                Log.LogWarningMessage(string.Format("setting {0} was not found", request.SettingName), GetCustomDataForReportingErrors());
                setting = new Setting {IsEnabled = false, Name = request.SettingName};
            }
            return new NewtonsoftJsonResult {Result = setting};
        }

        /// <summary>
        ///     Gets a runtime setting value (as a string) for the given setting name.  The full hierarchy (brand, site,
        ///     community, global) is taken into consideration so if the setting has a "brand" level value, that will always be
        ///     returned.  We store all setting values as strings, so a setting value could be used to represent anything, and so
        ///     it's up to the user of the setting value to convert/use it appropriately.
        /// </summary>
        /// <example>
        ///     Gets the setting value for "ENABLE_SPLASH_REDIRECT" for brand 1003, this setting happens to store strings as "true"
        ///     or "false"
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/runtimesettingvalue/ENABLE_TABBED_VIEW_PROFILE
        ///     RESPONSE
        ///     {"code":200,"status":"OK","data":{"name":"ENABLE_TABBED_VIEW_PROFILE","value":"true"}}
        /// </example>
        /// <param name="SettingName" required="true">The name of the setting</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        [ApiMethod(ResponseType = typeof (SettingV2))]
        public ActionResult GetRuntimeSettingValue(SettingRequest request)
        {
            var exists = SettingsService.SettingExistsFromSingleton(request.SettingName,
                request.Brand.Site.Community.CommunityID,
                request.Brand.Site.SiteID);

            SettingV2 setting;
            if (exists)
            {
                var value = SettingsService.GetSettingFromSingleton(request.SettingName,
                    request.Brand.Site.Community.CommunityID,
                    request.Brand.Site.SiteID, request.Brand.BrandID);
                setting = new SettingV2 {Value = value, Name = request.SettingName};
            }
            else
            {
                Log.LogWarningMessage(string.Format("setting {0} was not found", request.SettingName), GetCustomDataForReportingErrors());
                setting = new SettingV2 { Name = request.SettingName };
            }
            return new NewtonsoftJsonResult {Result = setting};
        }

        /// <summary>
        ///     This should only be called if there's no PromotionID(PRM) found.
        ///     This returns matching affiliate promotion values for a given referring url.
        ///     This is a POST method since we don't want to pass in special URL chars in a GET request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>AffiliateTracking data</returns>
        [ApiMethod(ResponseType = typeof (AffiliateTracking))]
        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Any, VaryByParam = "brandId")]
        public ActionResult GetAffiliateTracking(AffiliateTrackingRequest request)
        {
            var affiliateTracking = new AffiliateTracking
            {
                PromotionId = 0,
                Luggage = string.Empty
            };

            var referralUri = new Uri(request.UrlReferrer); // i.g.http://www.google.com/?blah=blah
            var referralHost = referralUri.Host; // i.g.http://www.google.com
            var referralUrl = referralUri.AbsoluteUri; // i.g.http://www.google.com/?blah=blah
            var referralUrlQueryString = (!String.IsNullOrEmpty(referralUri.Query))
                ? referralUri.Query
                : string.Empty; // i.g.?blah=blah

            var prmList = PromotionSA.Instance.GetPRMURLMapperList();

            if (prmList == null)
                return new NewtonsoftJsonResult {Result = affiliateTracking};

            var prmUrlMapper = prmList.GetPRMByReferralURL(referralHost);

            if (prmUrlMapper == null)
                return new NewtonsoftJsonResult {Result = affiliateTracking};

            affiliateTracking.PromotionId = prmUrlMapper.PromotionID;

            if (String.IsNullOrEmpty(prmUrlMapper.SourceURL))
            {
                affiliateTracking.Luggage = (referralUrl.Length > 150)
                    ? referralUrl.Substring(0, 150)
                    : referralUrl;
            }
            else
            {
                affiliateTracking.Luggage = (referralUrlQueryString.Length > 150)
                    ? referralUrlQueryString.Substring(0, 150)
                    : referralUrlQueryString;
            }

            return Json(affiliateTracking);
        }

        /// <summary>Gets a list of teases for a given category</summary>
        /// <example>
        ///     Get teases for the romantic category
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/teases/category/26
        ///     RESPONSE
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": [{
        ///     "ID": 130,
        ///     "Description": "How is it that you haven't been snatched up yet?"
        ///     },
        ///     {
        ///     "ID": 132,
        ///     "Description": "You look and sound adorable. I'd love to learn more."
        ///     },
        ///     {
        ///     "ID": 134,
        ///     "Description": "I'd travel a million miles to see your smile."
        ///     },
        ///     {
        ///     "ID": 136,
        ///     "Description": "Candlelight, sunlight, or moonlight?"
        ///     },
        ///     {
        ///     "ID": 138,
        ///     "Description": "I'd like a shot at sweeping you off your feet."
        ///     },
        ///     {
        ///     "ID": 140,
        ///     "Description": "Judging your book by its cover, I'd love to curl up and read the rest."
        ///     },
        ///     {
        ///     "ID": 142,
        ///     "Description": "I'd carve your name in every tree from Los Angeles to New York."
        ///     }]
        ///     }
        /// </example>
        /// <param name="teaseCategoryId" required="true">The tease category ID; passed in as part of URL</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (List<Tease>))]
        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Any, VaryByParam = "brandId")]
        public ActionResult GetTeases(TeaseCollectionRequest teaseCollectionRequest)
        {
            var teases = new List<Tease>();

            var teaseCollection = TeaseSA.Instance.GetTeaseCollection(teaseCollectionRequest.TeaseCategoryId,
                teaseCollectionRequest.Brand.Site.SiteID);

            foreach (Matchnet.Email.ValueObjects.Tease tease in teaseCollection)
            {
                teases.Add(new Tease
                {
                    Id = tease.TeaseID,
                    Description =
                        ResourceProvider.Instance.GetResourceValue(teaseCollectionRequest.Brand.Site.SiteID,
                            teaseCollectionRequest.Brand.Site.CultureInfo,
                            ResourceGroupEnum.Global,
                            tease.ResourceKey)
                });
            }

            return new NewtonsoftJsonResult {Result = teases};
        }

        /// <summary>Gets a list of tease categories for a given brand</summary>
        /// <example>
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/teases/categories
        ///     RESPONSE
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": [{
        ///     "Id": 26,
        ///     "Description": "Romantic",
        ///     "Teases": null
        ///     },
        ///     {
        ///     "Id": 46,
        ///     "Description": "Humorous",
        ///     "Teases": null
        ///     },
        ///     {
        ///     "Id": 66,
        ///     "Description": "Casual",
        ///     "Teases": null
        ///     },
        ///     {
        ///     "Id": 106,
        ///     "Description": "Whose Line is it Anyway?",
        ///     "Teases": null
        ///     },
        ///     {
        ///     "Id": 126,
        ///     "Description": "Holiday / Seasonal",
        ///     "Teases": null
        ///     },
        ///     {
        ///     "Id": 166,
        ///     "Description": "Juda'isms",
        ///     "Teases": null
        ///     }]
        ///     }
        /// </example>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        [ApiMethod(ResponseType = typeof (List<TeaseCategory>))]
        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Any, VaryByParam = "brandId")]
        public ActionResult GetTeaseCategories(Models.BrandRequest brandRequest)
        {
            var teaseCategoryCollection =
                TeaseSA.Instance.GetTeaseCategoryCollection(brandRequest.Brand.Site.SiteID);

            var categories = (from Matchnet.Email.ValueObjects.TeaseCategory teaseCategory in teaseCategoryCollection
                select new TeaseCategory
                {
                    Id = teaseCategory.TeaseCategoryID, Description = ResourceProvider.Instance.GetResourceValue(brandRequest.Brand.Site.SiteID, brandRequest.Brand.Site.CultureInfo, ResourceGroupEnum.Global, teaseCategory.ResourceKey)
                }).ToList();

            return new NewtonsoftJsonResult {Result = categories};
        }

        /// <summary>Gets a list of all tease categories and the teases for each of those categories</summary>
        /// <example>
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/teases/all
        ///     RESPONSE
        ///     Note: Some results removed to keep response example short
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": [{
        ///     "Id": 26,
        ///     "Description": "Romantic",
        ///     "Teases": [{
        ///     "ID": 130,
        ///     "Description": "How is it that you haven't been snatched up yet?"
        ///     },
        ///     {
        ///     "ID": 142,
        ///     "Description": "I&apos;d carve your name in every tree from Los Angeles to New York."
        ///     }]
        ///     },
        ///     {
        ///     "Id": 46,
        ///     "Description": "Humorous",
        ///     "Teases": [{
        ///     "ID": 168,
        ///     "Description": "I&apos;d like the opportunity to embarrass myself during an attempt to impress you."
        ///     },
        ///     {
        ///     "ID": 174,
        ///     "Description": "Hi, I&apos;m Mr./Mrs. Right. Someone said you were looking for me."
        ///     },
        ///     {
        ///     "ID": 176,
        ///     "Description": "I&apos;d like to share some uncomfortable silences with you."
        ///     }]
        ///     }
        ///     ...
        ///     ]
        ///     }
        /// </example>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        [ApiMethod(ResponseType = typeof (List<TeaseCategory>))]
        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Any, VaryByParam = "brandId")]
        public ActionResult GetAllTeases(Models.BrandRequest request)
        {
            var groupId = request.Brand.Site.SiteID;

            var teaseCategoryCollection =
                TeaseSA.Instance.GetTeaseCategoryCollection(groupId);

            var categories = (from Matchnet.Email.ValueObjects.TeaseCategory teaseCategory in teaseCategoryCollection
                let teaseCollection =
                    TeaseSA.Instance.GetTeaseCollection(teaseCategory.TeaseCategoryID, groupId)
                let teases = (from Matchnet.Email.ValueObjects.Tease tease in teaseCollection
                    select new Tease
                    {
                        Id = tease.TeaseID,
                        Description =
                            ResourceProvider.Instance.GetResourceValue(
                                request.Brand.Site.SiteID, request.Brand.Site.CultureInfo,
                                ResourceGroupEnum.Global, tease.ResourceKey)
                    }).ToList()
                select new TeaseCategory
                {
                    Id = teaseCategory.TeaseCategoryID,
                    Description =
                        ResourceProvider.Instance.GetResourceValue(request.Brand.Site.SiteID,
                            request.Brand.Site.CultureInfo, ResourceGroupEnum.Global,
                            teaseCategory.ResourceKey),
                    Teases = teases
                }).ToList();

            return new NewtonsoftJsonResult {Result = categories};
        }

        /// <summary>
        /// Gets the registration scenarios.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error getting scenarios.</exception>
        [ApiMethod(ResponseType = typeof (List<Scenario>))]
        [RequireClientCredentials]
        public ActionResult GetRegistrationScenarios(Models.BrandRequest request)
        {
            try
            {
                var scenarios = RegistrationMetadataAccess.Instance.GetRegistrationScenarios(request.Brand);
                return new NewtonsoftJsonResult {Result = scenarios};
            }
            catch (Exception ex)
            {
                Log.LogError("Error getting scenarios.", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                throw new Exception("Error getting scenarios.", ex);
            }
        }

        /// <summary>
        /// Gets the random name of the scenario.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error getting random scenario name.</exception>
        [ApiMethod(ResponseType = typeof (string))]
        [RequireClientCredentials]
        public ActionResult GetRandomScenarioName(Models.BrandRequest request)
        {
            try
            {
                var randomScenarioName = RegistrationMetadataAccess.Instance.GetRandomScenarioName(request.Brand);
                return new NewtonsoftJsonResult {Result = randomScenarioName};
            }
            catch (Exception ex)
            {
                Log.LogError("Error getting random scenario name.", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                throw new Exception("Error getting random scenario name.", ex);
            }
        }

        /// <summary>
        /// Gets the random type of the scenario name by device.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="deviceType">Type of the device.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error getting random scenario name.</exception>
        [ApiMethod(ResponseType = typeof (string))]
        public ActionResult GetRandomScenarioNameByDeviceType(Models.BrandRequest request, DeviceType deviceType)
        {
            try
            {
                var randomScenarioName = RegistrationMetadataAccess.Instance.GetRandomScenarioName(request.Brand,
                    deviceType);
                return new NewtonsoftJsonResult {Result = randomScenarioName};
            }
            catch (Exception ex)
            {
                Log.LogError("Error getting random scenario name.", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                throw new Exception("Error getting random scenario name.", ex);
            }
        }

        /// <summary>
        /// Retrieves the pixels.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error retrieving pixels.</exception>
        [ApiMethod(ResponseType = typeof (PixelResult))]
        [RequireClientCredentials]
        public ActionResult RetrievePixels(PixelRequest request)
        {
            try
            {
                var pixelRepsonse = PixelAccess.Instance.RetrievePixels(request);
                return new NewtonsoftJsonResult {Result = pixelRepsonse};
            }
            catch (Exception ex)
            {
                Log.LogError("Error retrieving pixels.", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                throw new Exception("Error retrieving pixels.", ex);
            }
        }

        /// <summary>
        /// Maps the URL to promo.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error mapping URL to promo.</exception>
        [ApiMethod(ResponseType = typeof (PromotionMappingResult))]
        public ActionResult MapURLToPromo(PromotionMappingRequest request)
        {
            try
            {
                var mappingResponse = PromotionDataAccess.Instance.MapURLToPromo(request);
                return new NewtonsoftJsonResult {Result = mappingResponse};
            }
            catch (Exception ex)
            {
                Log.LogError("Error mapping URL to promo.", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                throw new Exception("Error mapping URL to promo.", ex);
            }
        }

        #region Region

        /// <summary>Gets a list of cities based on a zip code</summary>
        /// <example>
        ///     Gets a list of cities for the 90210 zip code
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/region/citiesbyzipcode/90210
        ///     RESPONSE
        ///     {"code":200,"status":"OK","data":[{"Id":3443817,"Description":"Beverly Hills","Depth":0}]}
        /// </example>
        /// <param name="zipCode" required="true">Zip code is passed in as part of URL</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (List<Region>))]
        [SparkOutputCache(Duration = 10000, VaryByParam = "zipCode")]
        public ActionResult GetCitiesByZipCodes(string zipCode)
        {
            return new NewtonsoftJsonResult
            {
                Result = RegionDataAccess.Instance.GetCitiesByZip(RegionDataAccess.RegionUs, zipCode)
            };
        }

        /// <summary>Gets a list of countries based on languageID</summary>
        /// <example>
        ///     Get a list of countries with descriptions in english
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/region/countries/2
        ///     RESPONSE
        ///     Note: Results shortened for example
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": [{
        ///     "Id": 1,
        ///     "Description": "Afghanistan",
        ///     "Depth": 0
        ///     },
        ///     {
        ///     "Id": 2,
        ///     "Description": "Albania",
        ///     "Depth": 0
        ///     },
        ///     {
        ///     "Id": 3,
        ///     "Description": "Algeria",
        ///     "Depth": 0
        ///     },
        ///     {
        ///     "Id": 5,
        ///     "Description": "Andorra",
        ///     "Depth": 0
        ///     },
        ///     {
        ///     "Id": 6,
        ///     "Description": "Angola",
        ///     "Depth": 0
        ///     },
        ///     {
        ///     "Id": 7,
        ///     "Description": "Anguilla",
        ///     "Depth": 0
        ///     },
        ///     ....
        ///     }
        /// </example>
        /// <param name="languageId" required="true" values="2: english[br]8: french[br]262144: Hebrew">
        ///     Determines the language
        ///     used for description returned; languageId is passed in as part of URL
        /// </param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (List<Country>))]
        [SparkOutputCache(Duration = 10000, VaryByParam = "languageId")]
        public ActionResult GetCountries(CountriesRequest request)
        {
            return new NewtonsoftJsonResult {Result = RegionDataAccess.Instance.GetCountries(request.LanguageId)};
        }

        /// <summary>
        ///     Gets a list of states based on languageId and a country regionid
        /// </summary>
        /// <example>
        ///     Get a list of states in english for the country of Israel
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/region/states/2/105
        ///     RESPONSE
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": [{
        ///     "Id": 3484025,
        ///     "Description": "Darom",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 3484027,
        ///     "Description": "Eilat / Arava",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 3484014,
        ///     "Description": "Hasharon&amp; Shfelat Ha Hof",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 3484017,
        ///     "Description": "Jerusalem &amp; surroundings",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 3484012,
        ///     "Description": "Merkaz",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 3484024,
        ///     "Description": "North",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 3484016,
        ///     "Description": "Yehuda &amp; Shomron",
        ///     "Depth": 2
        ///     }]
        ///     }
        /// </example>
        /// <param name="languageId" required="true" values="2: english[br]8: french[br]262144: Hebrew">
        ///     The languageID determines
        ///     the language used for description returned; passed in as part of URL
        /// </param>
        /// <param name="countryRegionId" required="true">The regionID for a country; passed in as part of URL</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (List<State>))]
        [SparkOutputCache(Duration = 10000, VaryByParam = "countryRegionId;languageId")]
        public ActionResult GetStates(StatesRequest request)
        {
            return new NewtonsoftJsonResult
            {
                Result = RegionDataAccess.Instance.GetStates(request.CountryRegionId, request.LanguageId)
            };
        }

        /// <summary>Gets a list of cities based on languageId, country, and state</summary>
        /// <example>
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/region/cities/2/105/3484017
        ///     RESPONSE
        ///     Note: Some results removed for example
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": [{
        ///     "Id": 9801876,
        ///     "Description": "Abu Gosh",
        ///     "Depth": 3
        ///     },
        ///     {
        ///     "Id": 9800652,
        ///     "Description": "Adderet",
        ///     "Depth": 3
        ///     },
        ///     {
        ///     "Id": 9800653,
        ///     "Description": "Agur",
        ///     "Depth": 3
        ///     },
        ///     {
        ///     "Id": 9801877,
        ///     "Description": "Aminadav",
        ///     "Depth": 3
        ///     },
        ///     {
        ///     "Id": 9800654,
        ///     "Description": "Aviezer",
        ///     "Depth": 3
        ///     },
        ///     {
        ///     "Id": 9800630,
        ///     "Description": "Jerusalem",
        ///     "Depth": 3
        ///     },
        ///     {
        ///     "Id": 9800686,
        ///     "Description": "Zeharia",
        ///     "Depth": 3
        ///     },
        ///     ...
        ///     ]
        ///     }
        /// </example>
        /// <param name="languageId" required="true" values="2: english[br]8: french[br]262144: Hebrew">
        ///     The languageID determines
        ///     the language used for description returned; passed in as part of URL
        /// </param>
        /// <param name="countryRegionId" required="true">The regionID for a country; passed in as part of URL</param>
        /// <param name="stateRegionId" required="true">The regionID for a state; passed in as part of URL</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        [ApiMethod(ResponseType = typeof (List<City>))]
        [SparkOutputCache(Duration = 10000, VaryByParam = "countryRegionId;stateRegionId;languageId")]
        public ActionResult GetCities(CitiesRequest request)
        {
            return new NewtonsoftJsonResult
            {
                Result = RegionDataAccess.Instance.GetCities(request.CountryRegionId, request.StateRegionId, request.LanguageId)
            };
        }

        /// <summary>Gets child regions for a specified parent region</summary>
        /// <example>
        ///     Gets the states for the country of canada, return description in english
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/region/childregions/2/38
        ///     RESPONSE
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": [{
        ///     "Id": 341,
        ///     "Description": "Alberta",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 342,
        ///     "Description": "British Columbia",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 343,
        ///     "Description": "Manitoba",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 344,
        ///     "Description": "New Brunswick",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 345,
        ///     "Description": "Newfoundland",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 346,
        ///     "Description": "Northwest Territories",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 347,
        ///     "Description": "Nova Scotia",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 348,
        ///     "Description": "Nunavut",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 349,
        ///     "Description": "Ontario",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 350,
        ///     "Description": "Prince Edward Island",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 351,
        ///     "Description": "Quebec",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 352,
        ///     "Description": "Saskatchewan",
        ///     "Depth": 2
        ///     },
        ///     {
        ///     "Id": 353,
        ///     "Description": "Yukon Territory",
        ///     "Depth": 2
        ///     }]
        ///     }
        /// </example>
        /// <param name="languageId" required="true" values="2: english[br]8: french[br]262144: Hebrew">
        ///     The languageID determines
        ///     the language used for description returned; passed in as part of URL
        /// </param>
        /// <param name="parentRegionId" required="true">
        ///     The parent regionID used to grab all its child regions; passed in as part
        ///     of the URL
        /// </param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (List<ChildRegion>))]
        [SparkOutputCache(Duration = 10000, VaryByParam = "parentRegionId;languageId")]
        public ActionResult GetChildRegions(ChildRegionsRequest request)
        {
            return new NewtonsoftJsonResult
            {
                Result = RegionDataAccess.Instance.GetChildRegions(request.ParentRegionId, request.LanguageId)
            };
        }

        /// <summary>
        ///     Retrieves the parent regions for a given region;
        ///     E.g. Pass in a region representing zip code, and you'll get back zip code, city, state, country
        /// </summary>
        /// <example>
        ///     Get region hierarchy for regionID 3443817 (i.e. 90210 zip code) in english
        ///     GET http://api.local.spark.net/v2/brandId/1003/content/region/hierarchy/2/3443817
        ///     RESPONSE
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": {
        ///     "PostalCode": {
        ///     "Id": 3443817,
        ///     "Description": "90210",
        ///     "Depth": 4
        ///     },
        ///     "City": {
        ///     "Id": 3403764,
        ///     "Description": "Beverly Hills",
        ///     "Depth": 3
        ///     },
        ///     "State": {
        ///     "Abbreviation": "CA ",
        ///     "Id": 1538,
        ///     "Description": "California",
        ///     "Depth": 2
        ///     },
        ///     "Country": {
        ///     "Abbreviation": "US ",
        ///     "Id": 223,
        ///     "Description": "USA",
        ///     "Depth": 1
        ///     }
        ///     }
        ///     }
        /// </example>
        /// <param name="languageId" required="true" values="2: english[br]8: french[br]262144: Hebrew">
        ///     The languageID determines
        ///     the language used for description returned; passed in as part of URL
        /// </param>
        /// <param name="regionId" required="true">
        ///     The region ID to retrieve the hierarchy for; usually would represent the
        ///     regionID of a zip code
        /// </param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (RegionHierarchy))]
        [SparkOutputCache(Duration = 10000, VaryByParam = "languageId;regionId")]
        public ActionResult GetRegionHierarchy(RegionHierarchyRequest request)
        {
            return new NewtonsoftJsonResult
            {
                Result = RegionDataAccess.Instance.GetRegionHierarchy(request.RegionId, request.LanguageId)
            };
        }

        #endregion
    }
}