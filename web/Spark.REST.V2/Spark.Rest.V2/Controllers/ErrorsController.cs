﻿#region

using System;
using System.Net;
using System.Web.Mvc;
using Spark.Rest.Modules;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

#endregion

namespace Spark.REST.Controllers
{
    public class ErrorsController : SparkControllerBase
    {
        // remove all methods since the error module is handling all errors
        // leaving the file in tact for keeping history just in case
    }
}