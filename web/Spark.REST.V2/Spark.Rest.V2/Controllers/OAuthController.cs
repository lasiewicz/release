﻿#region

using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Spark.Logger;
using Spark.Managers.Managers;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.Entities.Applications;
using Spark.Rest.Helpers;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Configuration;
using Spark.Rest.V2.Entities.Applications;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Extensions;
using Spark.Rest.V2.Models.Applications;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.DataAccess;
using Spark.REST.DataAccess.Applications;
using Spark.REST.Models.Applications;

#endregion

namespace Spark.REST.Controllers
{
    /// <summary>
    ///     OAuthController is used to get access tokens
    /// </summary>
    public class OAuthController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof (OAuthController));

        [ApiMethod(ResponseType = typeof (int))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MingleTrustedClientIPWhitelist.xml")]
        public ActionResult ValidateAccessToken(string accessToken)
        {
            string failReason;
            int failCode;
            int memberId;

            try
            {
                var isTokenValid = ApplicationAccess.ValidateAccessToken(HttpUtility.UrlDecode(accessToken),
                    out failReason, out failCode, out memberId);
                if (!isTokenValid)
                {
                    return new SparkHttpUnauthorizedResult(failCode, String.Format("Authorization failed - {0}", failReason));
                }
            }
            catch (Exception ex)
            {
                Log.LogError("Authorization failed",ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpUnauthorizedResult((int) HttpSub400StatusCode.FailedAuthentication, "General Error validating token.");
            }

            return new NewtonsoftJsonResult {Result = new ValidateTokenResponse {MemberId = memberId}};
        }

        /// <summary>
        ///     This new version includes the app Id and the expiration date for the access token in the response.
        ///     Created to support backwards compatibility for the initial version above.
        /// </summary>
        /// <example>
        /// 
        ///     Return properties associated for a valid access token
        ///     GET /v2/brandId/1003/oauth2/accesstoken/validate/?accesstoken=ACCESS_TOKEN&amp;applicationId=APP_ID
        /// 
        ///     REQUEST HEADERS
        ///     Accept: application/json; version=V2.2
        /// 
        ///     RESPONSE
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": {
        ///     "isValid": true,
        ///     "memberId": 27029711,
        ///     "failReason": "",
        ///     "failCode": 0,
        ///     "tokenAccessExpiration": "2015-03-26T06:03:07.452Z",
        ///     "tokenAppId": 1054
        ///     }
        ///     }
        /// </example>
        /// <param name="accessToken" required="true" values="1+">Valid access token</param>
        /// <param name="applicationId" required="true" values="1+">Valid application Id</param>
        /// <paramsAdditionalInfo></paramsAdditionalInfo>
        /// <responseAdditionalInfo>A JSON with decrypted properties stored in the token</responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof(ValidateTokenResponseV2))]
        [RequireClientCredentials]
        [HttpGet]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MingleTrustedClientIPWhitelist.xml")]
        public ActionResult ValidateAccessTokenV2(ValidateAccessTokenRequest request)
        {
            try
            {
                var result = ApplicationAccess.ValidateAccessToken(request.Brand, HttpUtility.UrlDecode(request.AccessToken));

                if (!result.IsValid)
                {
                    // Using null safe result properties
                    return new SparkHttpUnauthorizedResult(result.FailCode, String.Format("Authorization failed - {0}", result.FailReason));
                }

                return new NewtonsoftJsonResult { Result = result };
            }
            catch (Exception ex)
            {
                Log.LogError("Authorization failed", ex, GetCustomDataForReportingErrors(),
                    ErrorHelper.ShouldLogExternally());
                return new SparkHttpUnauthorizedResult((int) HttpSub400StatusCode.FailedAuthentication,
                    "General Error validating token.");
            }
        }

        /// <summary>
        ///     Used by SUA clients to authenticate an access token and call post logon process handled in MemberSA
        ///     In bedrock, there's a bunch of attribute updates that happen right after logon.
        ///     This is following that pattern.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (AuthenticateAccessTokenResponse))]
        [RequireAccessTokenV2]
        [DeprecatedUse]
        public ActionResult AuthenticateAccessToken(AuthenticateAccessTokenRequest request)
        {
            int memberId;

            try
            {
                var accessToken = HttpContext.Request.QueryString["access_token"];
                string failReason;
                int failCode;
                var isTokenValid = ApplicationAccess.ValidateAccessToken(HttpUtility.UrlDecode(accessToken),
                    out failReason, out failCode, out memberId);
                if (!isTokenValid)
                {
                    return new NewtonsoftJsonResult
                    {
                        // Do not return HTTP UNAUTH in this case. 
                        // Any client with a valid client secret and Id is AUTH to call.
                        Result = new AuthenticateAccessTokenResponse
                        {
                            Authenticated = false,
                            MemberId = memberId,
                            FailCode = failCode,
                            FailReason = failReason
                        }
                    };
                }

                var validMember = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);

                MemberSA.Instance.PostLogonUpdate(request.Brand, validMember, true);
                bool isParticipatingInRedesign = (null != validMember && validMember.GetAttributeBool(request.Brand, "REDRedesignBetaParticipatingFlag"));
                bool isOfferedRedesign = (null != validMember && validMember.GetAttributeBool(request.Brand, "REDRedesignBetaOfferedFlag"));
                return new NewtonsoftJsonResult
                {
                    Result =
                        new AuthenticateAccessTokenResponse
                        {
                            Authenticated = true,
                            MemberId = memberId,
                            FailReason = string.Empty,
                            FailCode = Constants.NULL_INT,
                            REDRedesignBetaParticipatingFlag = isParticipatingInRedesign,
                            REDRedesignBetaOfferedFlag = isOfferedRedesign
                        }
                };
            }
            catch (Exception ex)
            {
                Log.LogError("Authorization failed", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpUnauthorizedResult((int)HttpSub400StatusCode.FailedAuthentication, "General Error authenticating token.");
            }
        }

        /// <summary>
        ///     given a member's login credentials, returns an access/refresh token pair, and also runs login fraud if enabled
        /// </summary>
        /// <param name="request"> </param>
        /// <returns> </returns>
        [ApiMethod(ResponseType = typeof (TokenPairResponse))]
        [RequireClientCredentials]
        [Stopwatch]
        public async Task<ActionResult> CreateAccessTokenPasswordWithLoginFraud(PasswordAccessTokenRequest request)
        {
            try
            {
                var settingsManager = new SettingsManager();
                var logonOnlySite = settingsManager.GetSettingBool(SettingConstants.LOGON_ONLY_SITE, request.Brand);

                var accessToken = await ApplicationAccess.GetOrCreateAccessTokenUsingPasswordAsync(request.Brand,
                                                                          request.ApplicationId,
                                                                          request.Email,
                                                                          request.Password,
                                                                          request.PasswordIsEncrypted ?? false,
                                                                          ApplicationResultType.TokenPairResponseV2,
                                                                          request.Scope,
                                                                          logonOnlySite,
                                                                          request.IpAddress,
                                                                          request.UserAgent,
                                                                          request.HttpHeaders, 
                                                                          request.LoginSessionId) as TokenPairResponseV2;

                // throw a hard exception as this should not happen
                if (accessToken == null)
                    throw new Exception(String.Format("accesstoken cannot be null. email:{0} Ip:{1}", request.Email, request.IpAddress));

                if (accessToken.ResponseCode != (int) HttpStatusCode.OK)
                    return new SparkHttpStatusCodeResult(accessToken.ResponseCode, accessToken.ResponseSubCode, accessToken.Error);
                try
                {
                    ProfileAccess.UpdateBrandLastLogonDate(request.Brand, accessToken.MemberId, true);
                    ProfileAccess.UpdateRedRedesignAttributes(request.Brand, accessToken, request.IsRedRedesignBeta, request.IsRedRedesignBetaOffered);
                }
                catch (Exception ex)
                {
                    Log.LogError("Failed to update last logon date or beta redesign attrs", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally());
                }

                //add member id to context for logging
                HttpContext.Items["tokenMemberId"] = accessToken.MemberId;
                return new NewtonsoftJsonResult {Result = accessToken};
            }
            catch (Exception e)
            {
                Log.LogError("Create Access tokens failed.", e, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidBrandId, e.ToString());
                //		        throw e;
            }
        }

        /// <summary>
        ///     given a member's login credentials, returns an access/refresh token pair
        /// </summary>
        /// <param name="request"> </param>
        /// <returns> </returns>
        [ApiMethod(ResponseType = typeof (TokenPairResponse))]
        [RequireClientCredentials]
        [Stopwatch]
        public async Task<ActionResult> CreateAccessTokenPassword(PasswordAccessTokenRequest request)
        {
            try
            {
                var settingsManager = new SettingsManager();
                var logonOnlySite = settingsManager.GetSettingBool(SettingConstants.LOGON_ONLY_SITE,
                    request.Brand);

                var accessToken = await
                    ApplicationAccess.GetOrCreateAccessTokenUsingPasswordAsync(request.Brand,
                        request.ApplicationId,
                        request.Email,
                        request.Password,
                        request.PasswordIsEncrypted ??
                        false,
                        ApplicationResultType.TokenPairResponseV2,
                        request.Scope,
                        logonOnlySite) as
                        TokenPairResponseV2;

                // throw a hard exception as this should not happen
                if (accessToken == null)
                    throw new Exception(String.Format("accesstoken cannot be null. email:{0} Ip:{1}", request.Email, request.IpAddress));

                if (accessToken.ResponseCode != (int) HttpStatusCode.OK)
                    return new SparkHttpStatusCodeResult(accessToken.ResponseCode, accessToken.ResponseSubCode, accessToken.Error);

                try
                {
                    if (!logonOnlySite)
                    {
                        // 2/10/2014 - Is this call necessary? Even w/o BrandLastLogon is getting updated. Commenting out for now.

                        //if only a logonsite, no need to update profile properties because a member record doesn't exist
                        //ProfileAccess.UpdateProperties(request.Brand, accessToken.MemberId,
                        //                               new Dictionary<string, object>{{"lastLoggedIn", DateTime.Now}},
                        //                               null);
                    }
                }
                catch (Exception ex)
                {
                    Log.LogError("Failed to update last logon date", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally());
                }
                //add member id to context for logging
                HttpContext.Items["tokenMemberId"] = accessToken.MemberId;

                return new NewtonsoftJsonResult {Result = accessToken};
            }
            catch (Exception e)
            {
                Log.LogError("Create Access tokens failed.", e, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidBrandId, e.ToString());
                //		        throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <example>
        /// Post
        /// v2/brandId/1003/oauth2/accesstoken/application/1000/createcredentials?client_secret=[CLIENT_SECRET]
        /// {
        ///     "EmailAddress":"[EMAILADDRESS]",
        ///     "Password": [PASSWORD],
        ///     "UserName":"[USERNAME]",
        ///     "Scope":"long"
        /// }
        /// Response
        ///{
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data":
        ///    {
        ///        "UserName": [USERNAME],
        ///        "MemberId": 144667559,
        ///        "AccessToken": [ACCESS_TOKEN],
        ///        "ExpiresIn": 2591999,
        ///        "AccessExpiresTime": "2015-05-09T17:26:23.458Z",
        ///        "RefreshToken": [REFRESH_TOKEN],
        ///        "RefreshTokenExpiresTime": "2015-05-09T17:26:23.459Z",
        ///        "Error": null,
        ///        "IsPayingMember": false,
        ///        "IsSelfSuspended": false,
        ///        "IsIAPPayingMember": false,
        ///        "IsIABPayingMember": false
        ///    }
        ///}
        /// </example>
        /// <param name="EmailAddress" required="true"></param>
        /// <param name="Password" required="true"></param>
        /// <param name="UserName" required="true"></param>
        /// <param name="Scope" required="false" values="short, long">Acess Token life time. Defaults to short</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MingleTrustedClientIPWhitelist.xml")]
        public ActionResult CreateLogonCredentials(CreateCredentialsRequest request)
        {
            try
            {
                var response = ApplicationAccess.CreateLogonCredentialsAndReturnTokens(request.Brand,
                    request.ApplicationId,
                    request.EmailAddress,
                    request.Password,
                    request.UserName,
                    request.Scope);

                if (response.ResponseCode == (int) HttpStatusCode.OK)
                {
                    return new NewtonsoftJsonResult {Result = response};
                }

                return new SparkHttpStatusCodeResult(response.ResponseCode, response.ResponseSubCode, response.Error);
            }
            catch (Exception e)
            {
                Log.LogError("Create Logon Credentials failed.", e, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidBrandId, e.ToString());
                //		        throw e;
            }
        }

        /// <summary>
        ///     The long-lived refresh token can be used to get new short-lived access tokens
        /// </summary>
        /// <param name="accessTokenRequest"> </param>
        /// <returns> </returns>
        [ApiMethod(ResponseType = typeof (TokenPairResponse))]
        [RequireClientCredentials]
        [DeprecatedUse]
        public ActionResult CreateAccessTokenUsingRefreshToken(RefreshAccessTokenRequest accessTokenRequest)
        {
            try
            {
                var accessToken =
                    ApplicationAccess.GetOrCreateAccessTokenRefresh(accessTokenRequest.Brand,
                        accessTokenRequest.ApplicationId,
                        accessTokenRequest.MemberId,
                        accessTokenRequest.RefreshToken,
                        ApplicationResultType.TokenPairResponseV2) as
                        TokenPairResponseV2;

                if (accessToken.ResponseCode == (int) HttpStatusCode.OK)
                {
                    try
                    {
                        ProfileAccess.UpdateBrandLastLogonDate(accessTokenRequest.Brand, accessTokenRequest.MemberId, true);
                    }
                    catch (Exception ex)
                    {
                        Log.LogError("Failed to update last logon date", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(accessTokenRequest.Brand));
                    }
                    //add member id to context for logging
                    HttpContext.Items["tokenMemberId"] = accessToken.MemberId;

                    return new NewtonsoftJsonResult {Result = accessToken};
                }
                return new SparkHttpStatusCodeResult(accessToken.ResponseCode, accessToken.ResponseSubCode,
                    accessToken.Error);
            }
            catch (Exception e)
            {
                Log.LogError("Invalid Brand Id", e, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidBrandId, e.ToString());
                //                throw e;
            }
        }

        [ApiMethod(ResponseType = typeof (void))]
        [HttpPost]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MingleTrustedClientIPWhitelist.xml")]
        public ActionResult HardResetPassword(PasswordHardResetRequest request)
        {
            var success = ApplicationAccess.HardResetPassword(request.MemberId, request.Brand);

            if (!success)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.ValidMemberIdRequired,
                    "Error performing hard reset on member password.");
            }

            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";
            return Content(String.Empty);
        }

        /// <summary>
        ///     Call for member password reset. Requires member email.
        /// </summary>
        /// <param name="request"> </param>
        /// <returns> </returns>
        [ApiMethod(ResponseType = typeof (PasswordResetResponse))]
        [HttpPost]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/VisitorTrustedClientIPWhitelist.xml")]
        public ActionResult ResetPassword(PasswordResetRequest request)
        {
            var emailAddress = request.Email;
            try
            {
                Log.LogDebugMessage(string.Format(
                    "Password reset request received. Email:{0} SuaUrl:{1} TemplateId:{2} ApplicationId:{3}",
                    emailAddress, request.SuaUrl, request.TemplateId, request.ApplicationId), GetCustomDataForReportingErrors());

                var passwordResetResponse = ApplicationAccess.ResetPassword(request.Brand, emailAddress,
                                                                            request.SuaUrl,
                                                                            request.TemplateId,
                                                                            request.ApplicationId, 
                                                                            request.IpAddress, 
                                                                            request.UserAgent,
                                                                            request.HttpHeaders, 
                                                                            request.LoginSessionId);

                if (passwordResetResponse.EmailSent)
                {
                    Log.LogDebugMessage("Password reset request processed.", GetCustomDataForReportingErrors());
                    return new NewtonsoftJsonResult {Result = passwordResetResponse};
                }

                Log.LogWarningMessage(string.Format("Password request request error. Email:{0}", emailAddress),GetCustomDataForReportingErrors());
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingEmail,
                    string.Format("Could not reset password for brand:{0} and email:{1}", request.Brand.BrandID,
                        emailAddress));
            }
            catch (Exception e)
            {
                Log.LogError("Invalid Brand Id", e, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally());
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidBrandId, e.ToString());
            }
        }

        [ApiMethod(ResponseType = typeof (AuthenticatedPasswordChangeResponse))]
        [RequireAccessTokenV2]
        [HttpPost]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/VisitorTrustedClientIPWhitelist.xml")]
        public ActionResult AuthenticatedChangePassword(AuthenticatedPasswordChangeRequest request)
        {
            Log.LogInfoMessage(string.Format("Authenticated change password request. MemberId: {0} BrandId: {1} ApplicationId: {2}",
                request.MemberId, request.Brand.BrandID, request.ApplicationId), GetCustomDataForReportingErrors());

            var accessToken = HttpContext.Request.QueryString["access_token"];

            var response = ApplicationAccess.AuthenticatedChangePassword(request.MemberId,
                request.CurrentPassword,
                request.NewPassword,
                accessToken,
                request.Brand,
                request.ApplicationId,
                request.IpAddress,
                request.UserAgent,
                request.HttpHeaders);

            Log.LogInfoMessage(string.Format("Authenticated change password response. MemberId: {0} ResponseStatus: {1}",
                request.MemberId, response.Status), GetCustomDataForReportingErrors());

            return new NewtonsoftJsonResult {Result = response};
        }

        [ApiMethod(ResponseType = typeof (PasswordChangeResponse))]
        [HttpPost]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/VisitorTrustedClientIPWhitelist.xml")]
        public ActionResult ChangePassword(PassswordChangeRequest request)
        {
            if (string.IsNullOrEmpty(request.ResetToken))
            {
                Log.LogWarningMessage(string.Format("Invalid password reset token. ResetToken:{0} AppId:{1}", request.ResetToken,
                    request.ApplicationId), GetCustomDataForReportingErrors());
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidPasswordResetToken,
                    string.Format("Empty password reset token brand:{0}",
                        request.BrandId));
            }

            var memberId =
                ApplicationAccess.ValidateResetPasswordToken(HttpUtility.UrlDecode(request.ResetToken));

            if (memberId <= 0)
            {
                Log.LogWarningMessage(string.Format("Bad member Id returned. ResetToken:{0} AppId:{1}", request.ResetToken,
                    request.ApplicationId), GetCustomDataForReportingErrors());
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidPasswordResetToken,
                    string.Format("Bad password reset token brand:{0} resettoken:{1}",
                        request.BrandId, request.ResetToken));
            }
            //add member id to context for logging
            HttpContext.Items["tokenMemberId"] = memberId;

            var passwordChanged = ApplicationAccess.ChangePassword(memberId,
                request.Brand,
                request.Password,
                request.ApplicationId,
                request.IpAddress,
                request.UserAgent,
                request.HttpHeaders, 
                request.LoginSessionId);

            Log.LogDebugMessage(string.Format("Password changed. ResetToken:{0} AppId:{1}", request.ResetToken, request.ApplicationId), GetCustomDataForReportingErrors());

            return passwordChanged
                ? new NewtonsoftJsonResult {Result = new PasswordChangeResponse {PasswordChanged = true}}
                // why would it ever go to this case? there's no error being raised in the method.
                : new NewtonsoftJsonResult {Result = new PasswordChangeResponse {PasswordChanged = false}};
        }

        /// <summary>
        /// This call enables an authenticated user to change email address for the site.
        /// This call will change the email address of a user so any new logins for this user will have to use the new email address.
        /// It will also send an email to the user informing of the change.
        /// </summary>
        /// <returns>
        /// EmailChangeResponse
        /// </returns>
        /// <example>
        /// 
        /// Example Request:
        /// 
        /// POST http://api.local.spark.net/v2/brandId/1003/oauth2/accesstoken/authenticatedchangeemail?access_token=[ACCESS_TOKEN]
        /// {
        ///     "CurrentEmail":"newthisIsATest1@spark.net",
        ///     "NewEmail":"thisIsATest1@spark.net",
        ///     "IpAddress":"192.168.4.121"
        /// }
        /// 
        /// Example Response:
        /// {
        ///     "code": 200
        ///     "status": "OK",
        ///     "data":  {
        ///         "EmailSent": true,
        ///         "EnableEmailVerification": false,
        ///         "Status": 1
        ///     }
        /// }
        /// 
        /// 
        /// Example Validation Error Response:
        /// This endpoint still returns status 200 OK for validation error. Check the status in data.
        /// 
        /// 
        ///NoChange = 0,
        ///Success = 1,
        ///NewEmailNotValid = 2,
        ///OriginalEmailNotValid = 3,
        ///InvalidAccessToken = 4,
        ///GeneralFailure = 5,
        ///EmailAddressAlreadyExists = 6,
        ///CurrentEmailIncorrect = 7
        /// 
        /// {"code":200,"status":"OK","data":{"EmailSent":false,"EnableEmailVerification":false,"Status":3}}
        /// </example>
        /// <param name="NewEmail" required="true">The new email that you want to change it to</param>
        /// <param name="CurrentEmail" required="true">The current email</param>
        /// <param name="IpAddress" required="true"></param>
        [ApiMethod(ResponseType = typeof(EmailChangeResponse))]
        [RequireAccessTokenV2]
        [HttpPost]
        public ActionResult AuthenticatedChangeEmail(EmailChangeRequest request)
        {
            int memberId = GetMemberId();
            try
            {
                Log.LogInfoMessage(string.Format(
                        "Authenticated change email request. MemberId: {0} OldEmail: '{1}' NewEmail: '{2}' BrandId: {3} ApplicationId: {4}",
                        memberId, request.CurrentEmail, request.NewEmail, request.BrandId, GetAppId),
                    GetCustomDataForReportingErrors());

                var response = ApplicationAccess.AuthenticatedChangeEmail(memberId,
                    request.CurrentEmail,
                    request.NewEmail,
                    request.Brand,
                    request.IpAddress);

                Log.LogInfoMessage(string.Format(
                        "Authenticated change email response. MemberId: {0} OldEmail: '{1}' NewEmail: '{2}' BrandId: {3} ApplicationId: {4} ResponseStatus: {5}",
                        memberId, request.CurrentEmail, request.NewEmail, request.BrandId, GetAppId, response.Status),
                    GetCustomDataForReportingErrors());

                return new NewtonsoftJsonResult {Result = response};
            }
            catch (SparkAPIReportableException reportableException)
            {
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkHttpBadRequestResult((int) reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkGenericHttpInternalServerErrorResult();
            }
        }

        /// <summary>
        /// This call allows an authenticated user to validate their password without refreshing the access token
        /// or update the last logon date.
        /// </summary>
        /// <example>
        /// 
        /// Request:
        /// 
        /// POST http://api.local.spark/v2/brandId/{brandId}/oauth2/password/authenticate?access_token=[ACCESS_TOKEN]
        /// 
        /// {
        ///     "email" : "test@spark.net",
        ///     "password" : "1111"
        /// }
        /// 
        /// Response:
        /// 
        /// {
        ///     "code": 200
        ///     "status": "OK",
        ///     "data":  { true }
        /// }
        /// 
        /// </example>
        /// <param name="request">The password authentication request.</param>
        /// <returns>True if the passowrd is authenticated.</returns>
        [ApiMethod(ResponseType = typeof (bool))]
        [RequireAccessTokenV2]
        [HttpPost]
        public ActionResult AuthenticatePassword(PasswordAuthenticationRequest request)
        {
            try
            {
                if (!request.Email.IsValidEmailAddress())
                {
                    Log.LogInfoMessage(string.Format("Invalid email address passed in request:{0}",
                        Newtonsoft.Json.JsonConvert.SerializeObject(request)),
                        new Dictionary<string, string>());

                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedAuthentication,
                        "Invalid or missing email address");
                }

                if (string.IsNullOrWhiteSpace(request.Password))
                {
                    Log.LogInfoMessage(string.Format("Invalid or missing password in request:{0}",
                        Newtonsoft.Json.JsonConvert.SerializeObject(request)),
                        new Dictionary<string, string>());

                    return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedAuthentication,
                        "Invalid or missing password");
                }

                if (MemberSA.Instance.GetMemberIDByEmail(request.Email, request.Brand.Site.Community.CommunityID) != GetMemberId())
                {
                    Log.LogInfoMessage(string.Format("Email address doesn't match with the access token in request: {0}",
                        Newtonsoft.Json.JsonConvert.SerializeObject(request)),
                        new Dictionary<string, string>());

                    return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedAuthentication,
                        "Email address doesn't match with the access token");
                }

                var authenticated = ApplicationAccess.AuthenticatePassword(GetMemberId(), request.Brand, request.Email,
                    request.Password);

                return new NewtonsoftJsonResult {Result = authenticated};
            }
            catch (Exception ex)
            {

                Log.LogError(string.Format("Error authenticating password for memberId:{0}; siteId:{1}; request{2};",
                    GetMemberId(), request.BrandId, Newtonsoft.Json.JsonConvert.SerializeObject(request)), ex,
                    new Dictionary<string, string>());

                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedAuthentication, ex.Message);
            }
        }

    }
}