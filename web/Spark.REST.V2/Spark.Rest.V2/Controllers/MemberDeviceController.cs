﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Spark.Logger;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.Serialization;
using Spark.Rest.V2.DataAccess.Interfaces;
using Spark.Rest.V2.DataAccess.MemberDevices;
using Spark.Rest.V2.Entities.MemberDevices;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Extensions;
using Spark.Rest.V2.Models.MemberDevices;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

namespace Spark.REST.Controllers
{
    public class MemberDeviceController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MemberDeviceController));

        private IMemberDeviceAccess _memberDeviceAccessService;

        public IMemberDeviceAccess MemberDeviceAccessService
        {
            get
            {
                return _memberDeviceAccessService ?? new MemberDeviceAccess();
            }
            set
            {
                _memberDeviceAccessService = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(string))]
        [RequireAccessTokenV2]
        [HttpPost]
        public ActionResult Add(AddMemberDeviceRequest request)
        {
            var memberId = GetMemberId();
            string hash;
            
            try
            {
                hash = MemberDeviceAccessService.AddAppDevice(memberId, GetAppId, request.Brand, request.DeviceId,
                    request.DeviceType, request.AppVersion, request.OSVersion);
            }
            catch(SparkAPIReportableException reportableException)
            {
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors());
                return new SparkHttpBadRequestResult((int)reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors());
                return new SparkGenericHttpInternalServerErrorResult();
            }

            return new NewtonsoftJsonResult { Result = new AddMemberDeviceResult { Alias = hash } };
        }


        /// <summary>
        /// Removes member App Device mapping
        /// </summary>
        /// 
        /// <param name="request">
        ///     DeviceId needs to be provided to process the call
        /// </param>
        /// <example>Example Request:
        /// DELETE  http://api.spark.net/v2/brandId/1003/memberdevice?access_token=[ACCESS_TOKEN]
        /// 
        /// Example Response:
        /// {
        ///     "code": 200
        ///     "status": "OK",
        ///     "data":  {}
        /// }
        /// </example>
        /// <returns>ActionResult</returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireAccessTokenV2]
        public ActionResult Delete(DeleteMemberDeviceRequest request)
        {
            var memberId = GetMemberId();

            if (string.IsNullOrEmpty(request.DeviceId) )
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument, "deviceId value can not be null or empty");

            try
            {
                MemberDeviceAccessService.DeleteAppDevice(memberId, GetAppId, request.DeviceId);
            }
            catch (SparkAPIReportableException reportableException)
            {
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors());
            }
            catch (Exception exception)
            {
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors());
                return new SparkGenericHttpInternalServerErrorResult();
            }

            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";
            return Content(String.Empty);
        }




        /// <summary>
        /// CategoryId values
        ///      Activity = 1, Photos = 2, Promotional = 3
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireAccessTokenV2]
        [HttpPut]
        public ActionResult UpdateDeviceNotificationSetting(UpdateDeviceNotificationSettingsRequest request)
        {
            var memberId = GetMemberId();

            try
            {
                MemberDeviceAccessService.UpdateDeviceNotificationSettings(memberId, GetAppId, request.DeviceId,
                    request.NotificationCategoryId, request.Enabled);
            }
            catch (SparkAPIReportableException reportableException)
            {
                Log.LogError(reportableException.Message, reportableException, GetCustomDataForReportingErrors());
                return new SparkHttpBadRequestResult((int)reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                Log.LogError(exception.Message, exception, GetCustomDataForReportingErrors());
                return new SparkGenericHttpInternalServerErrorResult();
            }
                        
            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";
            return Content(String.Empty);
        }

        /// <summary>
        /// Update when the last login by the member was made through a device app.
        /// This is typically triggered when the app is brought to the foreground.
        /// </summary>
        /// <example>
        /// 
        /// POST http://api.spark.net/v2/brandId/1003/memberdevice/autologin?access_token=[ACCESS_TOKEN]
        /// 
        /// {
        ///     "applicationId" : 2012
        /// }
        /// 
        /// 
        /// * US JDate IOS applicationId = 2012
        /// 
        /// </example>
        /// <param name="applicationId" required="true">The appID that determines what specific application the user logged into</param>
        /// <param name="email">This is no longer needed, the API will determine this from member data based on the user access token</param>
        /// <returns>OK response</returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        [HttpPost]
        public ActionResult UpdateAutoLoginDate(DeviceAutoLoginRequest request)
        {
            try
            {
                if (request.ApplicationId < 1)
                {
                    Log.LogInfoMessage(string.Format("Invalid Application ID passed in request:{0}",
                        Newtonsoft.Json.JsonConvert.SerializeObject(request)),
                        new Dictionary<string, string>());

                    return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedUpdateLastLogin, "Invalid Application ID");
                }
                
                var ipAddress = Request.UserHostAddress;
                var headers = Request.GetRequestHeadersAsString();
                var userAgent = Request.UserAgent;

                MemberDeviceAccessService.UpdateLastLoginDate(GetMemberId(), request.Brand, ipAddress,
                    userAgent, headers, request.ApplicationId);
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Error updatintg last login date for memberId:{0}; siteId:{1}; request{2};",
                    GetMemberId(), request.BrandId, Newtonsoft.Json.JsonConvert.SerializeObject(request)), ex, new Dictionary<string, string>());

                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedUpdateLastLogin,
                    ex.Message);
            }

            return Ok();
        }

        /// <summary>
        /// Sends a push notification message to a device
        /// </summary>
        /// <example>
        /// 
        /// Note: 
        /// Deep link formats
        /// MessageReceived     spark-jdate://message/{messagelistid}/?sub={issub}
        /// ChatRequest         spark-jdate://chat/{frommemberid}/?sub={issub}
        /// MutualYes           spark-jdate://mutualyes/{frommemberid}/?sub={issub}
        /// PhotoApproved       spark-jdate://photo/approved/?sub={issub}
        /// PhotoRejected       spark-jdate://photo/declined/?sub={issub}
        /// ChatRequestV2       spark-jdate://chat/{frommemberid}/{imid}?sub={issub}
        /// ChatRequestV3       spark-jdate://chat/{frommemberid}/{imid}/{resourceid}?sub={issub}
        /// 
        /// Example: Default message
        /// This sends an IM invite push notification (chat request for consolidated IM) from the member identified by the access token to member  1116690655
        /// 
        /// POST REQUEST http://api.stage3.spark.net/v2/brandId/1003/memberdevice/sendpush?access_token=
        /// 
        /// {
        ///     "RecipientMemberId":   1116690655,
        ///     "PushNotificationType": 8,
        ///     "SystemEventCategory": 0,
        ///     "IMID": 1282828,
        ///     "AppGroupID": 1
        /// }
        /// 
        /// 
        /// SUCCESSFUL RESPONSE
        /// 
        /// {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": {}
        /// }
        /// 
        /// 
        /// Example: Custom Message
        /// This sends an IM invite push notification (chat request for consolidated IM) from the member identified by the access token to member  1116690655, along with a custom message that will be sent and displayed.
        /// 
        /// POST REQUEST http://api.stage3.spark.net/v2/brandId/1003/memberdevice/sendpush?access_token=
        /// 
        /// {
        ///     "RecipientMemberId":   1116690655,
        ///     "PushNotificationType": 8,
        ///     "SystemEventCategory": 0,
        ///     "IMID": 1282828,
        ///     "AppGroupID": 1,
        ///     "Message": "It would be my honor to chat with you"
        /// }
        /// 
        /// 
        /// SUCCESSFUL RESPONSE
        /// 
        /// {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": {}
        /// }
        /// 
        /// 
        /// Note: Failed responses have status code of 400 for various validation errors.  The specific validation issue will be in the error property with a sub status code.
        /// 
        /// FAILED RESPONSE - INVALID NOTIFICATION TYPE 
        /// 
        /// {
        ///     "code": 400,
        ///     "status": "BadRequest",
        ///     "error": {
        ///         "code": 40100,
        ///         "message": "Push Notification Type not supported"
        ///     }
        ///}
        /// 
        /// </example>
        /// <param name="senderMemberId">Not used</param>
        /// <param name="RecipientMemberId" required="true">The memberID of the member that will get a push notification</param>
        /// <param name="MessageListId">Optional, only required for MessageReceived pushes. This is the inbox mail ID.</param>
        /// <param name="AppGroupID" required="true">Only JDate group available for now, list will automatically update once we support other groups</param>
        /// <param name="SystemEventCategory" required="true">This is for logging purposes to know whether the notification is a result of a user action or something internal</param>
        /// <param name="PushNotificationType" required="true">This is the type of push notification to send.  ChatRequest is for LA specific IM, ChatRequestV2 is for new consolidated IM</param>
        /// <param name="IMID">Optional, only required for ChatRequestV2 and ChatRequestV3 pushes. This is the IM ID, which should represent the Room ID and will be passed as part of deep link</param>
        /// <param name="ResourceID">Optional, only used for ChatRequestV3 pushes. This will will be passed as part of deep link</param>
        /// <param name="Message">Optional custom message that will be pushed and displayed.  If not provided, default messages will be used.</param>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireAccessTokenV2]
        [HttpPost]
        public ActionResult SendPushNotification(SendPushRequest sendPushRequest)
        {
            //note: exceptions, including sparkAPIReportableExceptions are handled by our error module, no custom exception handling needed here
            PushNotificationAccess.Instance.SendPushNotification(GetMemberId(), sendPushRequest);
            return Ok();

        }

        /// <summary>
        /// Sends a push notification message to a device.  It's the same as the SendPushNotification endpoint, but this one uses client credentials instead of the access token.
        /// </summary>
        /// <example>
        /// 
        /// Note: 
        /// Deep link formats
        /// MessageReceived     spark-jdate://message/{messagelistid}/?sub={issub}
        /// ChatRequest         spark-jdate://chat/{frommemberid}/?sub={issub}
        /// MutualYes           spark-jdate://mutualyes/{frommemberid}/?sub={issub}
        /// PhotoApproved       spark-jdate://photo/approved/?sub={issub}
        /// PhotoRejected       spark-jdate://photo/declined/?sub={issub}
        /// ChatRequestV2       spark-jdate://chat/{frommemberid}/{imid}?sub={issub}
        /// ChatRequestV3       spark-jdate://chat/{frommemberid}/{imid}/{resourceid}?sub={issub}
        /// 
        /// 
        /// Example: Default message
        /// This sends an IM invite push notification (chat request for consolidated IM) from the member identified by the access token to member  1116690655
        /// 
        /// POST REQUEST http://api.stage3.spark.net/v2/brandId/1003/memberdevice/sendpush?applicationId=&amp;client_secret=
        /// 
        /// {
        ///     "SenderMemberId": 1116693286,
        ///     "RecipientMemberId":   1116690655,
        ///     "PushNotificationType": 8,
        ///     "SystemEventCategory": 0,
        ///     "IMID": 1282828,
        ///     "AppGroupID": 1
        /// }
        /// 
        /// 
        /// SUCCESSFUL RESPONSE
        /// 
        /// {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": {}
        /// }
        /// 
        /// 
        /// Example: Custom Message
        /// This sends an IM invite push notification (chat request for consolidated IM) from the member identified by the access token to member  1116690655, along with a custom message that will be sent and displayed.
        /// 
        /// POST REQUEST http://api.stage3.spark.net/v2/brandId/1003/memberdevice/sendpush?access_token=
        /// 
        /// {
        ///     "SenderMemberId": 1116693286,
        ///     "RecipientMemberId":   1116690655,
        ///     "PushNotificationType": 8,
        ///     "SystemEventCategory": 0,
        ///     "IMID": 1282828,
        ///     "AppGroupID": 1,
        ///     "Message": "It would be my honor to chat with you"
        /// }
        /// 
        /// 
        /// SUCCESSFUL RESPONSE
        /// 
        /// {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": {}
        /// }
        /// 
        /// Note: Failed responses have status code of 400 for various validation errors.  The specific validation issue will be in the error property with a sub status code.
        /// 
        /// FAILED RESPONSE - INVALID NOTIFICATION TYPE 
        /// 
        /// {
        ///     "code": 400,
        ///     "status": "BadRequest",
        ///     "error": {
        ///         "code": 40100,
        ///         "message": "Push Notification Type not supported"
        ///     }
        ///}
        /// 
        /// </example>
        /// <param name="senderMemberId" required="true">The memberID of the member that initiated the push notification</param>
        /// <param name="RecipientMemberId" required="true">The memberID of the member that will get a push notification</param>
        /// <param name="MessageListId">Optional, only required for MessageReceived pushes. This is the inbox mail ID.</param>
        /// <param name="AppGroupID" required="true">Only JDate group available for now, list will automatically update once we support other groups</param>
        /// <param name="SystemEventCategory" required="true">This is for logging purposes to know whether the notification is a result of a user action or something internal</param>
        /// <param name="PushNotificationType" required="true">This is the type of push notification to send.  ChatRequest is for LA specific IM, ChatRequestV2 is for new consolidated IM</param>
        /// <param name="IMID">Optional, only required for ChatRequestV2 and ChatRequestV3 pushes. This is the IM ID, which should represent the Room ID and will be passed as part of deep link</param>
        /// <param name="ResourceID">Optional, only used for ChatRequestV3 pushes. This will will be passed as part of deep link</param>
        /// <param name="Message">Optional custom message that will be pushed and displayed.  If not provided, default messages will be used.</param>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireClientCredentials]
        [HttpPost]
        public ActionResult SendPushNotificationClient(SendPushRequest sendPushRequest)
        {
            //note: exceptions, including sparkAPIReportableExceptions are handled by our error module, no custom exception handling needed here
            PushNotificationAccess.Instance.SendPushNotification(sendPushRequest.SenderMemberId, sendPushRequest);
            return Ok();

        }
    }
}
