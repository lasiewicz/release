#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ServiceAdapters;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Spark.Logger;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.Entities.Applications;
using Spark.Rest.Helpers;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Configuration;
using Spark.Rest.V2.DataAccess;
using Spark.Rest.V2.DataAccess.MembersOnline;
using Spark.Rest.V2.DataAccess.Profile;
using Spark.Rest.V2.Entities.Member;
using Spark.Rest.V2.Entities.Profile;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Extensions;
using Spark.Rest.V2.Models;
using Spark.Rest.V2.Models.Applications;
using Spark.Rest.V2.Models.Profile;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.Configuration;
using Spark.REST.DataAccess;
using Spark.REST.DataAccess.Applications;
using Spark.REST.DataAccess.Content;
using Spark.REST.Entities.Content.AttributeData;
using Spark.REST.Entities.Member;
using Spark.REST.Entities.Profile;
using Spark.REST.Helpers;
using Spark.REST.Models;

#endregion

namespace Spark.REST.Controllers
{
    /// <summary>
    /// </summary>
    public class ProfileController : SparkControllerBase
    {
        private const string ReportAbuseAttributeName = "ReportAbuseReason";
        private const string ReportAbuseCountAttributeName = "ReportAbuseCount";
        private const string ContactUsListAttributeName = "ContactUsList";
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(ProfileController));


        /// <summary>
        ///     This call will send an email to our system for Customer Service to process.
        ///     An access token is not required due to the fact that the endpoint will be
        ///     available to non-subscribers.
        /// </summary>
        /// <returns>
        ///     ActionResult
        /// </returns>
        /// <example>
        ///     Example
        ///     
        ///     POST http://api.local.spark.net/v2/brandId/1003/sendcontactusemail?applicationId=999&amp;client_secret=[CLIENT_SECRET]
        ///     {
        ///             "appDeviceInfo": "Chrome, Windows 8.0",
        ///             "attributeOptionId":"75824",
        ///             "browser": "Chrome 12.012901823901",
        ///             "comment": "I've fallen and I can't get up!",
        ///             "memberId" : "91283901",
        ///             "remoteIp": "127.0.0.1",
        ///             "senderEmailAddress": "cstest@spark.net"
        ///     }
        ///     
        ///     Response
        ///     {
        ///         "code": 200
        ///         "status": "OK",
        ///         "data":  {}
        ///     }
        /// </example>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireClientCredentials]
        [HttpPost]
        public ActionResult SendContactUsEmail(ContactUsRequest request)
        {
            try
            {
                if (request.AttributeOptionId == 0) return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.GenericInvalidID, "attributeOptionId is required");
                AttributeOption attribute;
                var badRequestResult = FillAndValidateAttributes(request.BrandId, request.AttributeOptionId, ContactUsListAttributeName, request.Comment, out attribute);
                if (badRequestResult != null) return badRequestResult;
                Member member = null;
                var memberId = request.MemberId;
                if (memberId == 0 && HttpContext.Items.Contains("tokenMemberId"))
                    memberId = (int) HttpContext.Items["tokenMemberId"];
                if (memberId != 0)
                {
                    member = MemberSA.Instance.GetMember(request.MemberId, MemberLoadFlags.None);
                    if (!member.IsSiteMember(request.Brand.Site.SiteID)) return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidMemberId, "Member doesn't exist.");
                }

                string userName = null;
                string smemberId = null;
                string contactUsEmail = null;
                if (member != null)
                {
                    userName = member.GetUserName(request.Brand);
                    smemberId = memberId.ToString(CultureInfo.InvariantCulture);
                }
                try
                {
                    Application app=ApplicationAccess.GetApplication(Int32.Parse(Request.QueryString["applicationId"]));
                    contactUsEmail = app.ContactUsEmail;
                }
                catch (Exception ex)
                {
                    Log.LogError("Error in retrieving Application: ", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                }
                Log.LogInfoMessage(string.Format("About to send ContactUs email to customer service [brandId : {0}] [comment: {1}] [contactus email: {2}]", request.Brand.BrandID, request.Comment, contactUsEmail), ErrorHelper.GetCustomData());
                ExternalMailSA.Instance.SendContactUs(request.BrandId, userName, request.SenderEmailAddress, smemberId, request.Browser, request.RemoteIp, attribute.Description, request.Comment, request.AppDeviceInfo, contactUsEmail);
            }
            catch (SparkAPIReportableException reportableException)
            {
                return new SparkHttpBadRequestResult((int) reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                Log.LogError("SendContactUsEmail error!", exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkGenericHttpInternalServerErrorResult();
            }
            return Ok();
        }

        /// <summary>
        ///     This call will send an email to our system for Customer Service to process.
        ///     An access token is not required due to the fact that the endpoint will be
        ///     available to non-subscribers.
        ///     
        ///     Note: Same as the SendContactUsEmail endpoint, but this one takes a user access token
        /// </summary>
        /// <returns>
        ///     ActionResult
        /// </returns>
        /// <example>
        /// 
        ///     Example
        ///     
        ///     POST http://api.local.spark.net/v2/brandId/1003/contactus?access_token=
        ///     {
        ///             "appDeviceInfo": "Chrome, Windows 8.0",
        ///             "attributeOptionId":"75824",
        ///             "browser": "Chrome 12.012901823901",
        ///             "comment": "I've fallen and I can't get up!",
        ///             "memberId" : "91283901",
        ///             "senderEmailAddress": "cstest@spark.net"
        ///     }
        ///     
        /// 
        ///     RESPONSE
        ///     {
        ///         "code": 200
        ///         "status": "OK",
        ///         "data":  {}
        ///     }
        /// </example>
        /// <param name="remoteIp">RemoteIp is optional, it will be automatically retrieved within api</param>
        [ApiMethod(ResponseType = typeof(void))]
        [RequireAccessTokenV2]
        [HttpPost]
        public ActionResult SendContactUsEmailByToken(ContactUsRequest request)
        {
            try
            {
                if (request.AttributeOptionId == 0) return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.GenericInvalidID, "attributeOptionId is required");
                AttributeOption attribute;
                var badRequestResult = FillAndValidateAttributes(request.BrandId, request.AttributeOptionId, ContactUsListAttributeName, request.Comment, out attribute);
                if (badRequestResult != null) return badRequestResult;
                Member member = null;
                var memberId = request.MemberId;
                if (memberId <= 0 && HttpContext.Items.Contains("tokenMemberId"))
                    memberId = (int)HttpContext.Items["tokenMemberId"];
                if (memberId >= 0)
                {
                    member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
                    if (!member.IsSiteMember(request.Brand.Site.SiteID)) return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidMemberId, "Member doesn't exist.");
                }
                else
                {
                    return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidMemberId, "Member doesn't exist.");
                }

                string userName = null;
                string smemberId = null;
                string contactUsEmail = null;
                if (member != null)
                {
                    userName = member.GetUserName(request.Brand);
                    smemberId = memberId.ToString(CultureInfo.InvariantCulture);
                }
                try
                {
                    Application app = ApplicationAccess.GetApplication(Int32.Parse(Request.QueryString["applicationId"]));
                    contactUsEmail = app.ContactUsEmail;
                }
                catch (Exception ex)
                {
                    Log.LogError("Error in retrieving Application: ", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                }

                if (string.IsNullOrEmpty(request.RemoteIp))
                {
                    request.RemoteIp = MemberHelper.GetClientIP(this.Request);
                }

                Log.LogInfoMessage(string.Format("About to send ContactUs email to customer service [brandId : {0}] [comment: {1}] [contactus email: {2}] [ip: {3}]", request.Brand.BrandID, request.Comment, contactUsEmail, request.RemoteIp), ErrorHelper.GetCustomData());
                ExternalMailSA.Instance.SendContactUs(request.BrandId, userName, request.SenderEmailAddress, smemberId, request.Browser, request.RemoteIp, attribute.Description, request.Comment, request.AppDeviceInfo, contactUsEmail);
            }
            catch (SparkAPIReportableException reportableException)
            {
                return new SparkHttpBadRequestResult((int)reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                Log.LogError("SendContactUsEmail error!", exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkGenericHttpInternalServerErrorResult();
            }
            return Ok();
        }

        private static SparkHttpBadRequestResult FillAndValidateAttributes(int brandId, int attributeOptionId, string attributeName, string comment,
            out AttributeOption attribute, ICollection<int> attributesThatRequireComment = null)
        {
            if (attributeOptionId == 0)
            {
                attribute = null;
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingArgument, "attributeOptionId is required.");
            }

            var attributes = AttributeDataAccess.Instance.GetAttributeOptions(brandId, attributeName);
            attribute = attributes.SingleOrDefault(a => a.AttributeOptionId == attributeOptionId);
            if (attribute == null)
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.AttributeMappingFailure,
                    string.Format("Invalid AttributeOptionId, attributes must belong to the {0} category.", attributeName));

            if (attributesThatRequireComment == null || !attributesThatRequireComment.Any()) return null;
            var isCommentRequired = attributesThatRequireComment.Contains(attributeOptionId);
            if (isCommentRequired && string.IsNullOrEmpty(comment))
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingArgument, "Comment is required for this AttributeOptionID");
            return null;
        }

        /// <summary>
        ///     This call enables the reporting of abuse by members. Internally the API will also send an email to our system for
        ///     CS to process.
        ///     The following rules apply:
        ///     - When member has already attempted to report the target member we will still return 200 with a message indicating
        ///     that member has been reported already .
        ///     - The following attributes required a comment: Harrassment, IllegalActivity, Inappropriate IM/Chat content and
        ///     Other
        ///     - This API should be use in conjunction  with the GetAttributeOptions API filtering by ReportAbuseReason,
        ///     v2/brandId/{0}/content/attributes/options/ReportAbuseReason?access_token=...
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>
        ///     ActionResult
        /// </returns>
        /// <example>
        ///     Example Request:
        ///     POST http://api.local.spark.net/v2/brandId/1003/member/10000/reportabuse?access_token=[ACCESS_TOKEN]
        ///     {
        ///         {
        ///             "AttributeOptionId":109942,
        ///             "Comment": "User was mean and didn't want to chat with me",
        ///             "AddToIgnoreList":true,
        ///             "MemberMailId":0
        ///         }
        ///     }
        ///     Example Response:
        ///     {
        ///         "code": 200
        ///         "status": "OK",
        ///         "data":  {}
        ///     }
        /// </example>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        [HttpPost]
        public ActionResult ReportAbuse(ReportMemberAbuseRequest request)
        {
            try
            {
                // Harrassment, IllegalActivity, Inappropriate IM/Chat content, Other 
                int[] attributesThatRequireComment = {109942, 109943, 109947, 109945};
                AttributeOption attribute;

                var badRequestResult = FillAndValidateAttributes(request.BrandId, request.AttributeOptionId, ReportAbuseAttributeName,
                    request.Comment, out attribute, attributesThatRequireComment);
                if (badRequestResult != null) return badRequestResult;
                var isMemberAlredyReported = HotListAccess.MemberIsOnHotlist(request.Brand, HotListCategory.ReportAbuse, GetMemberId(), request.MemberId);

                if (!isMemberAlredyReported)
                {
                    try
                    {
                        var targetMember = MemberSA.Instance.GetMember(request.MemberId, MemberLoadFlags.None);

                        if (targetMember != null && !string.IsNullOrEmpty(targetMember.EmailAddress))
                        {
                            try
                            {
                                HotListAccess.AddToHotlist(request.Brand, HotListCategory.ReportAbuse, GetMemberId(), request.MemberId, request.Comment);

                                if (request.AddToIgnoreList)
                                {
                                    HotListAccess.AddToHotlist(request.Brand, HotListCategory.IgnoreList, GetMemberId(), request.MemberId, request.Comment);
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.LogError(string.Format("Failed to add to hotList: {0} ", GetMemberId()), ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                            }

                            var abuseCounter = 0;

                            try
                            {
                                abuseCounter = targetMember.GetAttributeInt(request.Brand, ReportAbuseCountAttributeName, 0);
                                abuseCounter++;
                                targetMember.SetAttributeInt(request.Brand, ReportAbuseCountAttributeName, abuseCounter);
                                MemberSA.Instance.SaveMember(targetMember);
                            }
                            catch (Exception ex)
                            {
                                Log.LogError(string.Format("Failed to update the reportAbuse counter for targetMemberId: {1}", targetMember.MemberID), ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                            }

                            try
                            {
                                var email = ComposeAbuseMessage(GetMemberId(), targetMember, request.Brand, abuseCounter, request.Comment, request.MemberMailId, attribute.Description);

                                Log.LogInfoMessage(string.Format("About to send reportAbuse [brandId : {0}] [content: {1}]", request.Brand.BrandID, email), ErrorHelper.GetCustomData());

                                ExternalMailSA.Instance.SendReportAbuseEmail(request.Brand.BrandID, email);
                            }
                            catch (Exception ex)
                            {
                                Log.LogError("Failed to send email reportAbuse error!", ex, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                            }
                        }
                        else
                        {
                            return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberId, "Member doesn't exist.");
                        }
                    }
                    catch (Exception exception)
                    {
                        Log.LogError("ReportAbuse error!", exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                    }
                }
                else
                {
                    return new NewtonsoftJsonResult {Result = "Member has been reported already"};
                }
            }
            catch (SparkAPIReportableException reportableException)
            {
                return new SparkHttpBadRequestResult((int) reportableException.SubStatusCode, reportableException.Message);
            }
            catch (Exception exception)
            {
                Log.LogError("ReportAbuse error!", exception, GetCustomDataForReportingErrors(), ErrorHelper.ShouldLogExternally(request.Brand));
                return new SparkGenericHttpInternalServerErrorResult();
            }

            return Ok();
        }


        /// <summary>
        ///     Build the message that the CS reps will see to determine if action should be taken.
        /// </summary>
        /// <param name="memberId">The member identifier.</param>
        /// <param name="targetMember">The target member.</param>
        /// <param name="brand">The brand.</param>
        /// <param name="reportCount">The report count.</param>
        /// <param name="comment">The comment.</param>
        /// <param name="memberMailId">The member mail identifier.</param>
        /// <param name="reason">The reason.</param>
        /// <returns>
        ///     A string of the content regarding this report.
        /// </returns>
        private static string ComposeAbuseMessage(int memberId, IMemberDTO targetMember, Brand brand, int reportCount, string comment, int memberMailId, string reason)
        {
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            var message = new StringBuilder();

            message.AppendFormat("MemberID: {0} ({1}){2}", member.MemberID, member.EmailAddress, Environment.NewLine);
            message.AppendFormat("Reported MemberID: {0}{1}", targetMember.MemberID, Environment.NewLine);
            message.AppendFormat("Site: {0}{1}", brand.Site.Name, Environment.NewLine);
            message.AppendFormat("Time: {0}{1}", DateTime.UtcNow.ToString(CultureInfo.InvariantCulture), Environment.NewLine);
            message.AppendFormat("Times Reported: {0}{1}", reportCount, Environment.NewLine);
            message.AppendFormat("Reason: {0}{1}", reason, Environment.NewLine);

            var environment = "production";

            try
            {
                environment = RuntimeSettings.GetSetting("ENVIRONMENT_TYPE", brand.Site.Community.CommunityID, brand.Site.SiteID);
            }
            catch
            {
                //This setting does not exist in production and is risky to add it, so we will just ignore the error and default to production.
                Log.LogWarningMessage("Runtime setting 'ENVIRONMENT_TYPE' not found", ErrorHelper.GetCustomData());
            }

            message.AppendFormat("Environment (API): {0}{1}", environment, Environment.NewLine);

            if (comment != String.Empty)
            {
                message.AppendLine();
                message.AppendFormat("Explanation: {0}{1}", comment, Environment.NewLine);
            }

            if (memberMailId == 0) return message.ToString();
            var email = EmailMessageSA.Instance.RetrieveMessage(member.MemberID, brand.Site.Community.CommunityID, memberMailId);

            if (email == null) return message.ToString();
            message.AppendLine();
            message.AppendFormat("Email Subject: {0}{1}", email.Subject, Environment.NewLine);
            message.AppendFormat("Email Contents: {0}{1}", email.Content, Environment.NewLine);

            return message.ToString();
        }

        /// <summary>
        ///     Gets the mini profile.
        /// </summary>
        /// <param name="memberRequest">The member request.</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (MiniProfile))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [DeprecatedUse]
        [Obsolete("This call is obsolete. Please use <a href='/v2/docs/profile/getminiprofilev2'>GetMiniProfileV2</a> instead.")]
        public ActionResult GetMiniProfile(MemberAndTargetMemberRequest memberRequest)
        {
            if (memberRequest.TargetMemberId == 0)
                memberRequest.TargetMemberId = GetMemberId();
            var miniProfile = ProfileAccess.GetMiniProfile(memberRequest.Brand, memberRequest.TargetMemberId, GetMemberId());
            return new NewtonsoftJsonResult {Result = miniProfile};
        }

        /// <summary>
        ///     Gets the mini profile of a target member
        /// </summary>
        /// <example>
        ///     E.g. Get miniprofile
        ///     GET http://api-temp.stgv3.spark.net/v2/brandId/1003/profile/miniprofile/1127797859?access_token=
        ///     
        ///     Example Response:
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": 
        ///             {
        ///                 "memberId": 1127797859,
        ///                 "username": "1127797859",
        ///                 "primaryPhoto": null,
        ///                 "maritalStaktus": "Separated",
        ///                 "gender": "Male",
        ///                 "seekingGender": "Female",
        ///                 "lookingFor": [],
        ///                 "age": 23,
        ///                 "location": "Beverly Hills, CA ",
        ///                 "lastLoggedIn": "2015-04-09T20:31:43.813Z",
        ///                 "lastUpdated": "2015-03-19T21:12:50.773Z",
        ///                 "isOnline": false,
        ///                 "jDateEthnicity": "Another Ethnic",
        ///                 "jDateReligion": 131072,
        ///                 "isPayingMember": true,
        ///                 "registrationDate": "2015-03-23T16:23:46.163Z",
        ///                 "passedFraudCheck": true,
        ///                 "approvedPhotoCount": 0,
        ///                 "subscriptionStatus": "",
        ///                 "subscriptionStatusGam": "never_sub",
        ///                 "zipcode": "90210",
        ///                 "regionId": 3443817,
        ///                 "heightCm": 137,
        ///                 "selfSuspendedFlag": false,
        ///                 "adminSuspendedFlag": false,
        ///                 "blockContactByTarget": false,
        ///                 "blockSearchByTarget": false,
        ///                 "removeFromSearchByTarget": false,
        ///                 "blockContact": false,
        ///                 "blockSearch": false,
        ///                 "removeFromSearch": false,
        ///                 "isHighlighted": false,
        ///                 "isSpotlighted": false,
        ///                 "matchRating": 91,
        ///                 "isIAPPayingMember": false,
        ///                 "isIABPayingMember": false,
        ///                 "isViewersFavorite": false
        ///             }
        ///     }
        /// </example>
        /// <param name="memberRequest"></param>
        /// <returns>ActionResult</returns>
        [ApiMethod(ResponseType = typeof (Dictionary<string, object>))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetMiniProfileV2(MemberAndTargetMemberRequest memberRequest)
        {
            if (memberRequest.TargetMemberId == 0) memberRequest.TargetMemberId = GetMemberId();
            var miniProfile = ProfileAccess.GetAttributeSetWithPhotoAttributes(memberRequest.Brand, memberRequest.TargetMemberId, GetMemberId(), "miniprofile");
            return new NewtonsoftJsonResult {Result = miniProfile};
        }

        /// <summary>
        ///     Gets the member status.
        /// </summary>
        /// <param name="memberRequest">The member request.</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (MemberStatus))]
        [RequireClientCredentials]
        public ActionResult GetMemberStatus(MemberRequest memberRequest)
        {
            var status = ProfileAccess.GetMemberSubscriptionStatus(memberRequest.Brand, memberRequest.MemberId);
            return new NewtonsoftJsonResult {Result = new MemberStatus {SubscriptionStatus = status}};
        }

        /// <summary>
        ///     Gets the member basic logon information.
        /// </summary>
        /// <param name="memberRequest">The member request.</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (MemberBasicLogonInfo))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MingleTrustedClientIPWhitelist.xml")]
        public ActionResult GetMemberBasicLogonInfo(MemberRequest memberRequest)
        {
            var basicInfo = ProfileAccess.GetMemberBasicLogonInfo(memberRequest.MemberId, memberRequest.Brand);
            if (basicInfo == null)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberId, "No info found for memberId");
            }

            return new NewtonsoftJsonResult {Result = basicInfo};
        }
        /// <summary>
        ///     Updates the profile enabled status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MingleTrustedClientIPWhitelist.xml")]
        public ActionResult UpdateProfileEnabledStatus(UpdateProfileEnabledStatusRequest request)
        {
            var successfulUpdate = ProfileAccess.UpdateProfileEnabledStatus(request.MemberId, request.AdminId, request.ReasonId, request.Enabled, request.Brand);
            if (!successfulUpdate)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedUpdateProfileEnabledStatus, "Error updating member's profile enabled status.");
            }

            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";
            return Content(String.Empty);
        }

        /// <summary>
        ///     This method returns a collection of attributes for a target member, which are grouped in the attributeSetList.xml file
        ///     Supported attribute types are:
        ///     
        ///     miniprofile
        ///     fullprofile
        ///     visitorprofile
        ///     resultsetprofile
        ///     secretadmirerprofile
        ///     targetingprofile_us
        ///     targetingprofile_il
        ///     miniprofileV2
        ///     fullprofileV2
        ///     visitorprofileV2
        ///     resultsetprofileV2
        ///     resultsetprofileBigV2
        ///     secretadmirerprofileV2
        ///     AdminAttributes
        ///     tealium
        ///     tealiumtargetingprofile_us
        ///     tealiumtargetingprofile_il
        /// </summary>
        /// <param name="attributeSetRequest">The attribute set request.</param>
        /// <returns></returns>
        /// <remarks>
        ///     targetingprofile_il supports a referrealUri param, this will allow the population of the promoid
        /// </remarks>
        /// <example>
        /// 
        /// Example: Get Tealium attributeset
        ///     
        /// GET http://api.stage3.spark.net/v2/brandId/1003/profile/attributeset/miniprofile/1127797859?access_token=
        ///     
        /// RESPONSE
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": {
        ///        "memberId": 100067359,
        ///        "age": 60,
        ///        "approvedPhotoCount": 5,
        ///        "auto_renew": "on",
        ///        "bannerId": 0,
        ///        "channel": "",
        ///        "countryAbbrev": "US ",
        ///        "dayssincereg": 2745,
        ///        "daystorenew":["46","46-60"],
        ///        "dma": "Los Angeles",
        ///        "environment": "Dev",
        ///        "gender": "Female",
        ///        "jDateReligion":"Reform",
        ///        "lapsedays": "-55",
        ///        "luggage": "",
        ///        "promotionId": 49097,
        ///        "subchannel": "",
        ///        "subscriptionStatusTealium": 9
        ///    }
        /// }
        /// 
        /// 
        /// 
        /// Example: Get miniprofile
        ///     
        /// GET http://api.stage3.spark.net/v2/brandId/1003/profile/attributeset/miniprofile/1127797859?access_token=
        /// 
        /// RESPONSE
        ///     {
        ///         "code": 200,
        ///         "status": "OK",
        ///         "data": 
        ///         {
        ///             "memberId": 1127797859,
        ///             "username": "1127797859",
        ///             "primaryPhoto": null,
        ///             "maritalStatus": "Separated",
        ///             "gender": "Male",
        ///             "seekingGender": "Female",
        ///             "lookingFor": [],
        ///             "age": 23,
        ///             "location": "Beverly Hills, CA ",
        ///             "lastLoggedIn": "2015-04-09T20:31:43.813Z",
        ///             "lastUpdated": "2015-03-19T21:12:50.773Z",
        ///             "isOnline": false,
        ///             "jDateEthnicity": "Another Ethnic",
        ///             "jDateReligion": 131072,
        ///             "isPayingMember": true,
        ///             "registrationDate": "2015-03-23T16:23:46.163Z",
        ///             "passedFraudCheck": true,
        ///             "approvedPhotoCount": 0,
        ///             "subscriptionStatus": "",
        ///             "subscriptionStatusGam": "never_sub",
        ///             "zipcode": "90210",
        ///             "regionId": 3443817,
        ///             "heightCm": 137,
        ///             "selfSuspendedFlag": false,
        ///             "adminSuspendedFlag": false,
        ///             "blockContactByTarget": false,
        ///             "blockSearchByTarget": false,
        ///             "removeFromSearchByTarget": false,
        ///             "blockContact": false,
        ///             "blockSearch": false,
        ///             "removeFromSearch": false,
        ///             "isHighlighted": false,
        ///             "isSpotlighted": false,
        ///             "matchRating": 91,
        ///             "isIAPPayingMember": false,
        ///             "isIABPayingMember": false,
        ///             "isViewersFavorite": false
        ///         }
        ///     }
        ///     
        /// 
        /// 
        /// Example: Get FullProfileV2
        ///     
        /// GET http://api.stage3.spark.net/v2/brandId/1003/profile/attributeset/fullprofilev2/411?access_token=
        /// 
        /// RESPONSE
        /// {
        ///  "code": 200,
        ///  "status": "OK",
        ///  "data": {
        ///    "memberId": 411,
        ///    "username": "david411",
        ///    "aboutMe": "archaeology, a tennis fanatic? We all have something that makes us unique; this is your chance to tell",
        ///    "activityLevel": "Never Active",
        ///    "adminSuspendedFlag": false,
        ///    "age": 36,
        ///    "approvedPhotoCount": 0,
        ///    "birthdate": "0001-01-01T00:00:00.000Z",
        ///    "blockContact": false,
        ///    "blockContactByTarget": false,
        ///    "blockSearch": false,
        ///    "blockSearchByTarget": false,
        ///    "bodyType": [
        ///      "Petite"
        ///    ],
        ///    "canBuyPremiumService": false,
        ///    "canBuyUpgradeSubscription": false,
        ///    "children": "None",
        ///    "coolestPlacesVisitedEssay": "",
        ///    "custody": "I have no children",
        ///    "drinkingHabits": "Socially",
        ///    "educationLevel": "Some College",
        ///    "eyeColor": "Blue",
        ///    "favoriteBooksMoviesEtcEssay": "",
        ///    "favoriteFoods": [
        ///      "Cajun/Southern",
        ///      "Carribean/Cuban",
        ///      "Barbecue",
        ///      "California-Fusion"
        ///    ],
        ///    "favoriteMusic": [
        ///      "Classic Rock n' Roll",
        ///      "Classical",
        ///      "Dance/Electronica",
        ///      "Country"
        ///    ],
        ///    "forFunILikeToEssay": "",
        ///    "gender": "Male",
        ///    "grewUpIn": "asdf",
        ///    "hairColor": "Other",
        ///    "heightCm": 183,
        ///    "historyOfMyLifeEssay": "hellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohello",
        ///    "idealRelationshipEssay": "",
        ///    "incomeLevel": "Will tell you later",
        ///    "inMyFreeTimeILikeTo": [
        ///      "Board Games/Backgammon/Chess",
        ///      "Card Games/Bridge/Canasta",
        ///      "Camping",
        ///      "Collecting"
        ///    ],
        ///    "isAdmin": false,
        ///    "isHighlighted": false,
        ///    "isIABPayingMember": false,
        ///    "isIAPPayingMember": false,
        ///    "isOnline": false,
        ///    "isPayingMember": false,
        ///    "isSpotlighted": false,
        ///    "isViewersFavorite": false,
        ///    "jDateEthnicity": "Another Ethnic",
        ///    "jDateReligion": "Culturally Jewish but not practicing",
        ///    "keepKosher": "Not at all",
        ///    "languagesSpoken": [
        ///      "Other",
        ///      "English",
        ///      "Vietnamese",
        ///      "Norwegian",
        ///      "French",
        ///      "Finnish",
        ///      "Hindi",
        ///      "Polish",
        ///      "Spanish",
        ///      "Greek",
        ///      "Tagalog",
        ///      "Urdu",
        ///      "Japanese",
        ///      "Portuguese",
        ///      "German",
        ///      "Malay",
        ///      "Hebrew",
        ///      "Persian/Farsi",
        ///      "Swedish",
        ///      "Thai",
        ///      "Korean",
        ///      "Yiddish",
        ///      "Romanian",
        ///      "Italian",
        ///      "Russian"
        ///    ],
        ///    "lastLoggedIn": "2012-09-12T21:01:42.000Z",
        ///    "lastUpdated": "2012-08-16T23:09:56.020Z",
        ///    "learnFromThePastEssay": "",
        ///    "lifeAndAbmitionEssay": "hellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohello",
        ///    "likeGoingOutTo": [
        ///      "Art Galleries",
        ///      "Bars/Nightclubs",
        ///      "Bookstores",
        ///      "Antique Stores/Flea Markets/Garage Sales",
        ///      "Beach"
        ///    ],
        ///    "likeToRead": [
        ///      "Trade Journals",
        ///      "Non-Fiction",
        ///      "Poetry"
        ///    ],
        ///    "location": "Achisemech, 06 ",
        ///    "lookingFor": [
        ///      "A long-term relationship"
        ///    ],
        ///    "maritalStatus": "Single",
        ///    "matchDrinkingHabits": [
        ///      "Never"
        ///    ],
        ///    "matchEducationLevel": [
        ///      "Elementary"
        ///    ],
        ///    "matchJDateReligion": [
        ///      "Orthodox (Baal Teshuva)"
        ///    ],
        ///    "matchMaritalStatus": [],
        ///    "matchMaxAge": 30,
        ///    "matchMinAge": 18,
        ///    "matchRating": 95,
        ///    "matchSmokingHabits": [
        ///      "Non-Smoker"
        ///    ],
        ///    "messageMeIfYouEssay": "",
        ///    "occupation": "Entrepreneurial/Start-up",
        ///    "occupationDescription": "asdf",
        ///    "onFriSatITypicallyEssay": "",
        ///    "onOurFirstDateRemindMeToEssay": "",
        ///    "passedFraudCheck": true,
        ///    "perfectFirstDateEssay": "",
        ///    "perfectMatchEssay": "asdfOccasionally Frequently  Smoking habits: Non-Smoker Occasionally Regularly",
        ///    "personalityTraits": [
        ///      "Compulsive",
        ///      "Conservative/Clean Cut",
        ///      "Easygoing/Flexible/Open-Minded",
        ///      "Earthy"
        ///    ],
        ///    "pets": [
        ///      "Cat",
        ///      "Dog"
        ///    ],
        ///    "photos": null,
        ///    "photoCount": 0,
        ///    "physicalActivity": [
        ///      "Biking",
        ///      "Boating/Sailing/Rafting",
        ///      "Bowling"
        ///    ],
        ///    "planOnChildren": "Yes",
        ///    "politicalOrientation": "Unspecified",
        ///    "regionId": 9801351,
        ///    "registrationDate": "2007-12-24T19:00:01.030Z",
        ///    "relocation": "Yes",
        ///    "removeFromSearch": false,
        ///    "removeFromSearchByTarget": false,
        ///    "seekingGender": "Female",
        ///    "selfSuspendedFlag": false,
        ///    "smokingHabits": "Non-Smoker",
        ///    "studiedOrInterestedIn": "asdf",
        ///    "subscriptionStatus": "",
        ///    "subscriptionStatusGam": "ex_sub",
        ///    "synagogueAttendance": "Never",
        ///    "thingsCantLiveWithoutEssay": "",
        ///    "weightGrams": 60782,
        ///    "zipcode": null,
        ///    "zodiac": "Virgo"
        ///  }
        ///}
        ///
        /// 
        /// </example>
        [ApiMethod(ResponseType = typeof (Dictionary<string, object>))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [Stopwatch]
        public ActionResult GetAttributeSet(AttributeSetRequest attributeSetRequest)
        {
            if (attributeSetRequest.TargetMemberId == 0)
                attributeSetRequest.TargetMemberId = GetMemberId();

            var attributeSetResult = ProfileAccess.GetAttributeSetWithPhotoAttributes(attributeSetRequest.Brand, attributeSetRequest.TargetMemberId, GetMemberId(), 
                                                                                      attributeSetRequest.AttributeSetName, false, attributeSetRequest.ReferralUri);

            // todo: it makes sense to tell the caller why the attributeset came back null.
            // BUT we do not want to break existing calls that expect an empty attributeset. 
            // it will have to be a coordinated effort. just logging when it does happen
            if (attributeSetResult.AttributeSetNotFoundReason != AttributeSetNotFoundReasonType.None)
            {
                Log.LogWarningMessage(
                    string.Format("Profile view not allowed. MemberId:{0}  TargetMemberId:{1}", GetMemberId(),
                        attributeSetRequest.TargetMemberId), GetCustomDataForReportingErrors());
            }

            return new NewtonsoftJsonResult {Result = attributeSetResult.AttributeSet};
        }

        /// <summary>
        ///     Similar as GetAttributeSet, but this supports getting a collection of attributes for a batch of members
        ///     Supported attribute types are:
        ///     
        ///     miniprofile
        ///     fullprofile
        ///     visitorprofile
        ///     resultsetprofile
        ///     secretadmirerprofile
        ///     targetingprofile_us
        ///     targetingprofile_il
        ///     miniprofileV2
        ///     fullprofileV2
        ///     visitorprofileV2
        ///     resultsetprofileV2
        ///     resultsetprofileBigV2
        ///     secretadmirerprofileV2
        ///     AdminAttributes
        ///     tealium
        ///     tealiumtargetingprofile_us
        ///     tealiumtargetingprofile_il
        /// </summary>
        /// <param name="attributeSetBatchRequest">The attribute set batch request.</param>
        /// <returns></returns>
        /// <example>
        ///     This example will request mini profiles for the following members: 1127797859,411
        ///     GET:
        ///     http://api.stage3.spark.net/v2/brandId/1003/profile/attributesetbatch/miniprofile?targetmembers=1127797859,411
        ///     &amp;access_token=
        ///     Returns a dictionary - key: memberid, value: collection of profile attributes
        ///     RESPONSE:
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": {
        ///     "100067359": {
        ///     "memberId": 100067359,
        ///     "username": "camel0010",
        ///     "primaryPhoto": {
        ///     "memberPhotoId": 3118277,
        ///     "fullId": 120208947,
        ///     "thumbId": 120130796,
        ///     "fullPath": "http://dev01.JDate.com/Photo1000/2012/11/01/17/120208947.jpg",
        ///     "thumbPath": "http://dev01.JDate.com/Photo1000/2012/11/01/17/120130796.jpg",
        ///     "caption": "",
        ///     "listorder": 1,
        ///     "isCaptionApproved": false,
        ///     "isPhotoApproved": true,
        ///     "IsMain": true,
        ///     "IsApprovedForMain": true
        ///     },
        ///     "maritalStatus": "Widowed",
        ///     "gender": "Female",
        ///     "seekingGender": "Male",
        ///     "lookingFor": [
        ///     "Friend"
        ///     ],
        ///     "age": 50,
        ///     "location": "Seattle, WA ",
        ///     "lastLoggedIn": "2015-04-09T22:07:09.000Z",
        ///     "lastUpdated": "2015-03-31T21:30:52.010Z",
        ///     "isOnline": false,
        ///     "jDateEthnicity": "",
        ///     "jDateReligion": "Reform",
        ///     "isPayingMember": false,
        ///     "registrationDate": "2008-03-17T18:37:25.247Z",
        ///     "passedFraudCheck": true,
        ///     "approvedPhotoCount": 5,
        ///     "subscriptionStatus": "",
        ///     "subscriptionStatusGam": "ex_sub",
        ///     "zipcode": "98101",
        ///     "regionId": 3481241,
        ///     "heightCm": 152,
        ///     "selfSuspendedFlag": false,
        ///     "adminSuspendedFlag": false,
        ///     "blockContactByTarget": false,
        ///     "blockSearchByTarget": false,
        ///     "removeFromSearchByTarget": false,
        ///     "blockContact": false,
        ///     "blockSearch": false,
        ///     "removeFromSearch": false,
        ///     "isHighlighted": false,
        ///     "isSpotlighted": false,
        ///     "matchRating": 100,
        ///     "isIAPPayingMember": false,
        ///     "isIABPayingMember": false,
        ///     "isViewersFavorite": false,
        ///     "DefaultPhoto": {
        ///     "memberPhotoId": 0,
        ///     "fullId": 120208947,
        ///     "thumbId": 120130796,
        ///     "fullPath": "http://dev01.JDate.com/Photo1000/2012/11/01/17/120208947.jpg",
        ///     "thumbPath": "http://dev01.JDate.com/Photo1000/2012/11/01/17/120130796.jpg",
        ///     "caption": "",
        ///     "listorder": 0,
        ///     "isCaptionApproved": false,
        ///     "isPhotoApproved": true,
        ///     "IsMain": true,
        ///     "IsApprovedForMain": true
        ///     },
        ///     "PhotoUrl": "http://dev01.JDate.com/Photo1000/2012/11/01/17/120208947.jpg",
        ///     "ThumbnailUrl": "http://dev01.JDate.com/Photo1000/2012/11/01/17/120130796.jpg"
        ///     },
        ///     "411": {
        ///     "memberId": 411,
        ///     "username": "411",
        ///     "primaryPhoto": null,
        ///     "maritalStatus": null,
        ///     "gender": null,
        ///     "seekingGender": null,
        ///     "lookingFor": [],
        ///     "age": null,
        ///     "location": ", ",
        ///     "lastLoggedIn": "0001-01-01T00:00:00.000Z",
        ///     "lastUpdated": "0001-01-01T00:00:00.000Z",
        ///     "isOnline": false,
        ///     "jDateEthnicity": null,
        ///     "jDateReligion": "Not sure if I'm willing to convert",
        ///     "isPayingMember": false,
        ///     "registrationDate": "0001-01-01T00:00:00.000Z",
        ///     "passedFraudCheck": true,
        ///     "approvedPhotoCount": 0,
        ///     "subscriptionStatus": "",
        ///     "subscriptionStatusGam": "never_sub",
        ///     "zipcode": null,
        ///     "regionId": null,
        ///     "heightCm": 193,
        ///     "selfSuspendedFlag": false,
        ///     "adminSuspendedFlag": false,
        ///     "blockContactByTarget": false,
        ///     "blockSearchByTarget": false,
        ///     "removeFromSearchByTarget": false,
        ///     "blockContact": false,
        ///     "blockSearch": false,
        ///     "removeFromSearch": false,
        ///     "isHighlighted": false,
        ///     "isSpotlighted": false,
        ///     "matchRating": 95,
        ///     "isIAPPayingMember": false,
        ///     "isIABPayingMember": false,
        ///     "isViewersFavorite": false
        ///     }
        ///     }
        ///     }
        /// </example>
        [ApiMethod(ResponseType = typeof (Dictionary<int, Dictionary<string, object>>))]
        [RequireAccessTokenV2]
        [Stopwatch]
        public ActionResult GetAttributeSetBatch(AttributeSetBatchRequest attributeSetBatchRequest)
        {
            var attributeSetList = new Dictionary<int, Dictionary<string, object>>();

            if (attributeSetBatchRequest.TargetMemberList != null)
            {
                foreach (var targetMemberId in attributeSetBatchRequest.TargetMemberList)
                {
                    if (targetMemberId <= 0) continue;
                    var attributeSetResult = ProfileAccess.GetAttributeSetWithPhotoAttributes(attributeSetBatchRequest.Brand, targetMemberId, GetMemberId(), 
                                                                                             attributeSetBatchRequest.AttributeSetName, false, attributeSetBatchRequest.ReferralUri);

                    // todo: it makes sense to tell the caller why the attributeset came back null.
                    // BUT we do not want to break existing calls that expect an empty attributeset. 
                    // it will have to be a coordinated effort. just logging when it does happen
                    if (attributeSetResult.AttributeSetNotFoundReason != AttributeSetNotFoundReasonType.None)
                    {
                        //return new SparkHttpForbiddenResult((int)HttpSub403StatusCode.ProfileViewNotAllowed,
                        //    attributeSetResult.AttributeSetNotFoundReason.ToString());
                        Log.LogWarningMessage(string.Format("Profile view not allowed. MemberId:{0}  TargetMemberId:{1}, Reason:{2}",
                            GetMemberId(), targetMemberId, attributeSetResult.AttributeSetNotFoundReason), GetCustomDataForReportingErrors());
                    }
                    else
                    {
                        attributeSetList[targetMemberId] = attributeSetResult.AttributeSet;
                    }
                }
            }
            else
            {
                Log.LogInfoMessage("No target member ids were passed in.", GetCustomDataForReportingErrors());
            }


            return new NewtonsoftJsonResult {Result = attributeSetList};
        }

        /// <summary>
        ///     Similar as GetAttributeSet, but this supports getting a collection of attributes for a batch of members.
        ///     Identical to GetAttributeSetBatch but this supports App/Secret auth instead of access token.
        ///     Supported attribute types are:
        ///     
        ///     miniprofile
        ///     fullprofile
        ///     visitorprofile
        ///     resultsetprofile
        ///     secretadmirerprofile
        ///     targetingprofile_us
        ///     targetingprofile_il
        ///     miniprofileV2
        ///     fullprofileV2
        ///     visitorprofileV2
        ///     resultsetprofileV2
        ///     resultsetprofileBigV2
        ///     secretadmirerprofileV2
        ///     AdminAttributes
        ///     tealium
        ///     tealiumtargetingprofile_us
        ///     tealiumtargetingprofile_il
        /// </summary>
        /// <param name="attributeSetBatchRequest">The attribute set batch request.</param>
        /// <returns></returns>
        /// <example>
        ///     This example will request mini profiles for the following members: 1127797859,411
        ///     GET:
        ///     http://api.stage3.spark.net/v2/brandId/1003/profile/attributesetbatchclient/miniprofile?targetmembers=1127797859,411
        ///     &amp;applicationId=&amp;client_secret=
        ///     Returns a dictionary - key: memberid, value: collection of profile attributes
        ///     RESPONSE:
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": {
        ///     "100067359": {
        ///     "memberId": 100067359,
        ///     "username": "camel0010",
        ///     "primaryPhoto": {
        ///     "memberPhotoId": 3118277,
        ///     "fullId": 120208947,
        ///     "thumbId": 120130796,
        ///     "fullPath": "http://dev01.JDate.com/Photo1000/2012/11/01/17/120208947.jpg",
        ///     "thumbPath": "http://dev01.JDate.com/Photo1000/2012/11/01/17/120130796.jpg",
        ///     "caption": "",
        ///     "listorder": 1,
        ///     "isCaptionApproved": false,
        ///     "isPhotoApproved": true,
        ///     "IsMain": true,
        ///     "IsApprovedForMain": true
        ///     },
        ///     "maritalStatus": "Widowed",
        ///     "gender": "Female",
        ///     "seekingGender": "Male",
        ///     "lookingFor": [
        ///     "Friend"
        ///     ],
        ///     "age": 50,
        ///     "location": "Seattle, WA ",
        ///     "lastLoggedIn": "2015-04-09T22:07:09.000Z",
        ///     "lastUpdated": "2015-03-31T21:30:52.010Z",
        ///     "isOnline": false,
        ///     "jDateEthnicity": "",
        ///     "jDateReligion": "Reform",
        ///     "isPayingMember": false,
        ///     "registrationDate": "2008-03-17T18:37:25.247Z",
        ///     "passedFraudCheck": true,
        ///     "approvedPhotoCount": 5,
        ///     "subscriptionStatus": "",
        ///     "subscriptionStatusGam": "ex_sub",
        ///     "zipcode": "98101",
        ///     "regionId": 3481241,
        ///     "heightCm": 152,
        ///     "selfSuspendedFlag": false,
        ///     "adminSuspendedFlag": false,
        ///     "blockContactByTarget": false,
        ///     "blockSearchByTarget": false,
        ///     "removeFromSearchByTarget": false,
        ///     "blockContact": false,
        ///     "blockSearch": false,
        ///     "removeFromSearch": false,
        ///     "isHighlighted": false,
        ///     "isSpotlighted": false,
        ///     "matchRating": 100,
        ///     "isIAPPayingMember": false,
        ///     "isIABPayingMember": false,
        ///     "isViewersFavorite": false,
        ///     "DefaultPhoto": {
        ///     "memberPhotoId": 0,
        ///     "fullId": 120208947,
        ///     "thumbId": 120130796,
        ///     "fullPath": "http://dev01.JDate.com/Photo1000/2012/11/01/17/120208947.jpg",
        ///     "thumbPath": "http://dev01.JDate.com/Photo1000/2012/11/01/17/120130796.jpg",
        ///     "caption": "",
        ///     "listorder": 0,
        ///     "isCaptionApproved": false,
        ///     "isPhotoApproved": true,
        ///     "IsMain": true,
        ///     "IsApprovedForMain": true
        ///     },
        ///     "PhotoUrl": "http://dev01.JDate.com/Photo1000/2012/11/01/17/120208947.jpg",
        ///     "ThumbnailUrl": "http://dev01.JDate.com/Photo1000/2012/11/01/17/120130796.jpg"
        ///     },
        ///     "411": {
        ///     "memberId": 411,
        ///     "username": "411",
        ///     "primaryPhoto": null,
        ///     "maritalStatus": null,
        ///     "gender": null,
        ///     "seekingGender": null,
        ///     "lookingFor": [],
        ///     "age": null,
        ///     "location": ", ",
        ///     "lastLoggedIn": "0001-01-01T00:00:00.000Z",
        ///     "lastUpdated": "0001-01-01T00:00:00.000Z",
        ///     "isOnline": false,
        ///     "jDateEthnicity": null,
        ///     "jDateReligion": "Not sure if I'm willing to convert",
        ///     "isPayingMember": false,
        ///     "registrationDate": "0001-01-01T00:00:00.000Z",
        ///     "passedFraudCheck": true,
        ///     "approvedPhotoCount": 0,
        ///     "subscriptionStatus": "",
        ///     "subscriptionStatusGam": "never_sub",
        ///     "zipcode": null,
        ///     "regionId": null,
        ///     "heightCm": 193,
        ///     "selfSuspendedFlag": false,
        ///     "adminSuspendedFlag": false,
        ///     "blockContactByTarget": false,
        ///     "blockSearchByTarget": false,
        ///     "removeFromSearchByTarget": false,
        ///     "blockContact": false,
        ///     "blockSearch": false,
        ///     "removeFromSearch": false,
        ///     "isHighlighted": false,
        ///     "isSpotlighted": false,
        ///     "matchRating": 95,
        ///     "isIAPPayingMember": false,
        ///     "isIABPayingMember": false,
        ///     "isViewersFavorite": false
        ///     }
        ///     }
        ///     }
        /// </example>
        [ApiMethod(ResponseType = typeof (Dictionary<int, Dictionary<string, object>>))]
        [RequireClientCredentials]
        [Stopwatch]
        public ActionResult GetAttributeSetBatchClient(AttributeSetBatchRequest attributeSetBatchRequest)
        {
            var attributeSetList = new Dictionary<int, Dictionary<string, object>>();

            if (attributeSetBatchRequest.TargetMemberList != null)
            {
                foreach (var targetMemberId in attributeSetBatchRequest.TargetMemberList)
                {
                    if (targetMemberId <= 0) continue;
                    var attributeSetResult =
                        ProfileAccess.GetAttributeSetWithPhotoAttributes(attributeSetBatchRequest.Brand, targetMemberId, Constants.NULL_INT, 
                                                                         attributeSetBatchRequest.AttributeSetName, false, attributeSetBatchRequest.ReferralUri);

                    // todo: it makes sense to tell the caller why the attributeset came back null.
                    // BUT we do not want to break existing calls that expect an empty attributeset. 
                    // it will have to be a coordinated effort. just logging when it does happen
                    if (attributeSetResult.AttributeSetNotFoundReason != AttributeSetNotFoundReasonType.None)
                    {
                        //return new SparkHttpForbiddenResult((int)HttpSub403StatusCode.ProfileViewNotAllowed,
                        //    attributeSetResult.AttributeSetNotFoundReason.ToString());
                        Log.LogWarningMessage(string.Format("Profile view not allowed. MemberId:{0}  TargetMemberId:{1}, Reason:{2}", GetMemberId(), targetMemberId, 
                                       attributeSetResult.AttributeSetNotFoundReason), GetCustomDataForReportingErrors());
                    }
                    else
                    {
                        attributeSetList[targetMemberId] = attributeSetResult.AttributeSet;
                    }
                }
            }
            else
            {
                Log.LogInfoMessage("No target member ids were passed in.", GetCustomDataForReportingErrors());
            }


            return new NewtonsoftJsonResult {Result = attributeSetList};
        }

        /// <summary>
        /// </summary>
        /// <param name="attributeSetRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (Dictionary<string, object>))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/VisitorTrustedClientIPWhitelist.xml")]
        [Stopwatch]
        public ActionResult GetAttributeSetForVisitor(AttributeSetRequest attributeSetRequest)
        {
            var attributeResult = ProfileAccess.GetAttributeSetWithPhotoAttributes(attributeSetRequest.Brand, attributeSetRequest.TargetMemberId, GetMemberId(), 
                                                                                   "visitorprofile", true);
            return new NewtonsoftJsonResult {Result = attributeResult.AttributeSet};
        }


        /// <summary>
        /// </summary>
        /// <param name="memberRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [HttpPut]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [DeprecatedUse]
        public ActionResult UpdateAttributeData(AttributeRequest memberRequest)
        {
            return UpdateMemberAttributeData(memberRequest);
        }

        /// <summary>
        ///     This is the latest version of the call to update member attributes
        /// </summary>
        /// <example>
        /// 
        /// Example: Updating attributes with a single value
        /// 
        /// Here we will use the AttributesJson parameter, which is a string representing a JSON object of attributes
        /// 
        /// POST: http://api.stage3.spark.net/v2/brandId/1003/profile/attributes?access_token=
        /// Header: Accept:V2.1
        /// 
        /// {
        ///     "AttributesJson": "{\"RedRedesignBetaParticipatingFlag\": false}",
        ///     "AttributesMultiValueJson": "{}"
        /// }
        /// 
        /// or multiple attributes at a time
        /// 
        /// {
        ///     "AttributesJson": "{\"ProfileQuote\": \"I am the king of the mountain\", \"RedRedesignBetaParticipatingFlag\": false}",
        ///     "AttributesMultiValueJson": "{}"
        /// }
        /// 
        /// 
        /// RESPONSE
        /// 
        /// {"code":200,"status":"OK","data":""}
        /// 
        /// 
        /// 
        /// 
        /// Example: Updating the pledge fraud attribute
        /// 
        /// POST: http://api.stage3.spark.net/v2/brandId/1003/profile/attributes?access_token=
        /// {
        ///    "AttributesJson": "{\"pledgeToCounterFroud\": true}",
        ///    "AttributesMultiValueJson": "{}"
        /// }
        /// 
        /// 
        /// RESPONSE
        /// 
        /// {"code":200,"status":"OK","data":""}
        /// 
        /// 
        /// 
        /// 
        /// 
        /// </example>
        /// <param name="AttributesJson">A JSON string of attributes to update, use this for attributes with a single value</param>
        /// <returns>AttributeRequest</returns>
        /// <exception cref="SparkHttpBadRequestResult">HttpSub400StatusCode.MissingAttributesToUpdate</exception>
        /// <exception cref="SparkHttpUnauthorizedResult">HttpSub401StatusCode.InvalidAttributeToUpdate</exception>
        [ApiMethod(ResponseType = typeof (void))]
        [HttpPut]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult UpdateAttributeDataV2(AttributeRequest memberRequest)
        {
            return UpdateMemberAttributeData(memberRequest);
        }

        private ActionResult UpdateMemberAttributeData(AttributeRequest memberRequest)
        {
            var attributeWhitelistDict = AttributeConfigReader.Instance.AttributeWhitelistDictionary;

            if (memberRequest.Attributes.Count + memberRequest.AttributesMultiValue.Count == 0)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingAttributesToUpdate, "No attributes to update");
            }
            var keys = new List<string>(memberRequest.Attributes.Keys);
            keys.AddRange(memberRequest.AttributesMultiValue.Keys);

            foreach (var key in keys.Where(key => !attributeWhitelistDict.ContainsKey(key.ToLower()) || !attributeWhitelistDict[key.ToLower()].IsWriteable))
            {
                return new SparkHttpUnauthorizedResult((int) HttpSub401StatusCode.InvalidAttributeToUpdate, String.Format("write access to '{0}' is denied.", key));
            }

            ProfileAccess.UpdateProperties(memberRequest.Brand, GetMemberId(), memberRequest.Attributes, memberRequest.AttributesMultiValue);

            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";
            return Content(String.Empty);
        }

        /// <summary>
        /// This endpoint provides the method to update a user's profile display settings.
        /// </summary>
        /// <param name="request">The <see cref="DisplaySettingsRequest"/> request.</param>
        /// <returns>ActionResult</returns>
        /// <example>
        /// Example Request:
        /// POST http://api-temp.stgv3.spark.net/v2/brandId/1003/profile/displaysettings?access_token=[ACCESS_TOKEN]
        /// {
        ///     {
        ///         "Online" : true,
        ///         "ShowProfileInSearch" : true,
        ///         "ShowPhotosToNonMembers": false,
        ///         "ShowWhenViewOrHotlist": false
        ///     }
        /// }
        /// Example Response:
        /// {
        ///     "code": 200
        ///     "status": "OK",
        ///     "data":  {}
        /// }
        /// </example>
        [ApiMethod(ResponseType = typeof (void))]
        [HttpPut]
        [RequireAccessTokenV2]
        public ActionResult UpdateDisplaySettings(DisplaySettingsRequest request)
        {
            var member = MemberSA.Instance.GetMember(GetMemberId(), MemberLoadFlags.None);
            var brand = request.Brand;
            
            // Remove from MOL if doesn't want to show online.
            if (!request.Online)
            {
                MembersOnlineAccess.Instance.Remove(brand.Site.Community.CommunityID, member.MemberID);
            }
            member.SetAttributeInt(brand, "HideMask", (int)request.DisplaySettings.HideMask);
            MemberSA.Instance.SaveMember(member);

            return Ok();
        }

        /// <summary>
        ///     Gets display settings for member.
        /// </summary>
        /// <example>
        ///     GET http://api-temp.stgv3.spark.net/v2/brandid/1003/profile/displaysettings?access_token=[ACCESS_TOKEN]
        ///     RESPONSE     
        ///     {
        ///         "code": 200,
        ///         "status": "OK",
        ///         "data": 
        ///         {
        ///             "Online" : true,
        ///             "ShowProfileInSearch" : true,
        ///             "ShowPhotosToNonMembers": false,
        ///             "ShowWhenViewOrHotlist": false
        ///         }
        ///     }
        /// </example>
        /// <param name="request">The <see cref="MemberRequest"/> for display settings</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (DisplaySettings))]
        [HttpGet]
        [RequireAccessTokenV2]
        public ActionResult GetDisplaySettings(MemberRequest request)
        {
            var displaySettings = ProfileAccess.GetDisplaySettings(GetMemberId(), request.Brand);
            return new NewtonsoftJsonResult {Result = displaySettings};
        }

        /// <summary>
        ///     This is the same as the lookup by Username feature
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (Dictionary<string, object>))]
        [HttpGet]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult LookupAttributeSetByMemberUsername(LookupByMemberUsernameRequest request)
        {
            if (String.IsNullOrEmpty(request.TargetMemberUsername))
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberId, "Requesting Username cannot be empty.");

            var memberId = MemberSA.Instance.GetMemberID(request.TargetMemberUsername, request.Brand.Site.Community.CommunityID);

            if (memberId >= 0)
                return LookupAttributeSetByMemberId(new LookupByMemberIdRequest
                {
                    BrandId = request.BrandId,
                    AttributeSetName = request.AttributeSetName,
                    Brand = request.Brand,
                    MemberId = GetMemberId(),
                    TargetMemberId = memberId
                });
            if (!int.TryParse(request.TargetMemberUsername, out memberId))
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberId, "Requesting member not available on the site .");

            return LookupAttributeSetByMemberId(new LookupByMemberIdRequest
            {
                BrandId = request.BrandId,
                AttributeSetName = request.AttributeSetName,
                Brand = request.Brand,
                MemberId = GetMemberId(),
                TargetMemberId = memberId
            });
        }

        /// <summary>
        ///     This is the same as the lookup by member Id feature
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (Dictionary<string, object>))]
        [HttpGet]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult LookupAttributeSetByMemberId(LookupByMemberIdRequest request)
        {
            if (request.TargetMemberId < 0)
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberId, "Requesting MemberId cannot be less than 0.");

            if (string.IsNullOrEmpty(request.AttributeSetName))
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberId, "Requesting Attributeset cannot be empty.");

            var attributeSet = ProfileAccess.LookupAttributeSetByMemberId(GetMemberId(), request.TargetMemberId, request.AttributeSetName, request.Brand);
            if (attributeSet == null)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberId, "Requesting member not available on the site .");
            }

            return new NewtonsoftJsonResult {Result = attributeSet};
        }

        /// <summary>
        ///     This method is used to update the member's email settings.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <example>
        /// 
        /// 1/6/2015 Note on "newsLetterSettings"
        /// For newsLetterSettings, "shouldSendDatingTips" combines both newsletter and offer opt-ins and will take precedence.
        /// This was left in for backwards compatibility.  Consider not passing this in and instead use "shouldSendNewsletters" and "shouldSendOffers" instead.
        /// The example below has been updated to only use these two instead shouldSendDatingTips.
        /// 
        /// Example: Updating mail settings
        /// 
        /// PUT http://api.local.spark.net/v2/brandId/1003/profile/emailsettings?access_token=
        /// {
        ///     "emailAlertSettings": {
        ///         "secretAdmirerEmailFrequency" : 1,
        ///         "shouldNotifySecretAdmirerAlerts" : "true",
        ///         "shouldNotifyMatches" : "true",
        ///         "shouldNotifyWhenAddedToHotlist" : "true",
        ///         "shouldNotifyEmailAlert" : "true",
        ///         "shouldNotifyEcard" : "true",
        ///         "shouldNotifyWhenProfileViewed" : "true",
        ///         "shouldNotifyProfileEssayRequests" : "true"
        ///     },
        ///     "newsLetterSettings": {
        ///         "shouldSendEventsInvitations" : "true",
        ///         "shouldSendNewsletters" : "true",
        ///         "shouldSendOffers" : "true"
        ///     },
        ///     "messageSettings": {
        ///         "shouldIncludeOriginalInReplies" : "true",
        ///         "shouldSaveSentCopies" : "true",
        ///         "shouldReplyOnMyBehalf" : "true"
        ///     }
        /// }
        ///     
        /// RESPONSE
        /// {"code":200,"status":"OK","data":{}}
        ///     
        /// </example>
        [ApiMethod(ResponseType = typeof (void))]
        [HttpPut]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult UpdateEmailSettings(EmailSettingsRequest request)
        {
            ProfileAccess.UpdateEmailSettings(request, request.Brand, GetMemberId());

            return Ok();
        }

        /// <summary>
        ///     This method is used to get the member's email settings.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>
        ///     The member's email settings.
        /// </returns>
        /// <example>
        /// 
        /// 1/6/2015 Note on "newsLetterSettings"
        /// For newsLetterSettings, "shouldSendDatingTips" should no longer be relied on, instead use "shouldSendNewsletters" and "shouldSendOffers".
        /// 
        /// 
        /// Example: Get Email settings
        /// 
        ///     GET http://api.local.spark.net/v2/brandId/1003/profile/emailsettings?access_token=
        ///     
        ///     RESPONSE
        ///     {
        ///         "code": 200,
        ///         "status": "OK",
        ///         "data": {
        ///             "emailAlertSettings": {
        ///                 "secretAdmirerEmailFrequency" : 1,
        ///                 "shouldNotifySecretAdmirerAlerts" : "true",
        ///                 "shouldNotifyMatches" : "true",
        ///                 "shouldNotifyWhenAddedToHotlist" : "true",
        ///                 "shouldNotifyEmailAlert" : "true",
        ///                 "shouldNotifyEcard" : "true",
        ///                 "shouldNotifyWhenProfileViewed" : "true",
        ///                 "shouldNotifyProfileEssayRequests" : "true"
        ///             },
        ///             "newsLetterSettings": {
        ///                 "shouldSendEventsInvitations" : "true",
        ///                 "shouldSendDatingTips" : "true", //this should be ignored, use the separate newsletter and offers instead
        ///                 "shouldSendNewsletters" : "true",
        ///                 "shouldSendOffers" : "true"
        ///             },
        ///             "messageSettings": {
        ///                 "shouldIncludeOriginalInReplies" : "true",
        ///                 "shouldSaveSentCopies" : "true",
        ///                 "shouldReplyOnMyBehalf" : "true"
        ///             }
        ///         }
        ///     }
        /// </example>
        [ApiMethod(ResponseType = typeof (EmailAlertSettings))]
        [HttpGet]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetEmailSettings(MemberRequest request)
        {
            var emailSettings = ProfileAccess.GetEmailSettings(GetMemberId(), request.Brand);

            return new NewtonsoftJsonResult {Result = emailSettings};
        }

        /// <summary>
        ///     Allow a member to self suspend their profile.
        /// </summary>
        /// <example>
        ///     PUT http://api.local.spark.net/v2/brandId/1003/profile/selfsuspend?access_token=[accesstoken]
        ///     {
        ///     "suspendReasonId" : 1
        ///     }
        ///     0   not suspended (default)
        ///     1	found my soulmate on the site
        ///     2	found my soulmate on my own
        ///     4	too many responses
        ///     8	too few responses
        ///     16	taking a break - vacation
        ///     32	technical issues (logging in, etc.)
        ///     64	site performance (photos, essays, etc.)
        ///     128	Suspend other
        /// </example>
        /// <param name="request">Request to self suspend a member's profile.</param>
        /// <returns>Ok code in response</returns>
        [ApiMethod(ResponseType = typeof (void))]
        [HttpPut]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult SelfSuspend(MemberSuspendRequest request)
        {
            ProfileAccess.SuspendProfile(request, request.Brand, GetMemberId());

            return Ok();
        }

        /// <summary>
        ///     Get the reason for self suspending a member's profile.
        /// </summary>
        /// <example>
        ///     GET http://api.local.spark.net/v2/brandId/1003/profile/selfsuspendreason?access_token=[accesstoken]
        ///     0   not suspended (default)
        ///     1	found my soulmate on the site
        ///     2	found my soulmate on my own
        ///     4	too many responses
        ///     8	too few responses
        ///     16	taking a break - vacation
        ///     32	technical issues (logging in, etc.)
        ///     64	site performance (photos, essays, etc.)
        ///     128	Suspend other
        /// </example>
        /// <param name="request">The request to get the member's reason for self suspending their profile.</param>
        /// <returns>The reason for self suspending a member's profile.</returns>
        [ApiMethod(ResponseType = typeof (Int32))]
        [HttpGet]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetSelfSuspendedReason(MemberRequest request)
        {
            return new NewtonsoftJsonResult
            {
                Result = ProfileAccess.GetSuspendedProfileReasonId(request.Brand, GetMemberId())
            };
        }

        /// <summary>
        ///     Reactivate a self suspended profile
        /// </summary>
        /// <example>
        ///     PUT http://api.local.spark.net/v2/brandId/1003/profile/selfsuspendreason?access_token=[accesstoken]
        /// </example>
        /// <param name="request">The request to reactivate a self suspended profile.</param>
        /// <returns>Ok code in response.</returns>
        [ApiMethod(ResponseType = typeof (void))]
        [HttpPut]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult ReActivateSelfSuspended(MemberRequest request)
        {
            ProfileAccess.ReActivateSuspendedProfile(request.Brand, GetMemberId());

            return Ok();
        }

        /// <summary>
        ///     Update the member's username.
        /// </summary>
        /// <example>
        ///     PUT http://api.local.spark.net/v2/brandId/1003/profile/username?access_token=[accesstoken]
        ///     { "userName" : "superduperman" }
        ///     * Note: changes to the member's username require approvals before they are seen in the UI.
        /// </example>
        /// <param name="request">The username update request.</param>
        /// <returns>OK code in response.</returns>
        [ApiMethod(ResponseType = typeof (void))]
        [HttpPut]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult UpdateUsername(UpdateUsernameRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.UserName) || !request.UserName.IsMinLength(4) || !request.UserName.IsAlphaNumeric())
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidUserName, "Username must be at least 4 characters long and alpha numeric.");
            ProfileAccess.UpdateUsername(request.UserName, request.Brand, GetMemberId());

            return Ok();
        }

        /// <summary>
        ///     Used by EJabberD for integrated chat with Utah.  Returns true if member is valid and is not admin or self
        ///     suspended.
        /// </summary>
        /// <example>
        ///     GET http://api-temp.stgv3.spark.net/v2/brandId/1003/profile/memberexists/1127797859?applicationId=&amp;
        ///     client_secret=
        ///     RESPONSE (valid memberid will return true or false)
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": {
        ///     "exists": true
        ///     }
        ///     }
        ///     RESPONSE (invalid memberid)
        ///     {
        ///     "code": 400,
        ///     "status": "BadRequest",
        ///     "error": {
        ///     "code": 40002,
        ///     "message": "Member ID not found"
        ///     }
        ///     }
        /// </example>
        /// <param name="request">SenderMemberId</param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (MemberExistsResponse))]
        [HttpGet]
        [RequireClientCredentials]
        public ActionResult MemberExists(MemberRequest request)
        {
            var member = MemberSA.Instance.GetMember(request.MemberId, MemberLoadFlags.None);
            if (member == null || member.MemberID <= 0 || string.IsNullOrEmpty(member.EmailAddress)) 
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberId, "Member ID not found");
            var memberExistsResponse = new MemberExistsResponse();

            if (MemberHelper.IsMemberSelfSuspended(request.Brand, member))
            {
                memberExistsResponse.Exists = false;
            }
            else if (MemberHelper.IsMemberAdminSuspended(request.Brand, member))
            {
                memberExistsResponse.Exists = false;
            }
            else
            {
                memberExistsResponse.Exists = true;
            }

            return new NewtonsoftJsonResult {Result = memberExistsResponse};
        }

        /// <summary>
        ///     Used by EJabberD to support rostering functionality
        ///     Returns an array of key/values, the key is memberID-siteID, value is username
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <example>
        ///     This gets the favorites list for member 1127797859
        ///     GET http://api-temp.stgv3.spark.net/v2/brandId/1003/profile/imfavorites/1127797859?applicationId=&amp;
        ///     client_secret=
        ///     RESPONSE
        ///     {
        ///     "code": 200,
        ///     "status": "OK",
        ///     "data": {
        ///     "100067248-103": "100067248",
        ///     "16000206-103": "superman001",
        ///     "988269718-103": "helloworld00"
        ///     }
        ///     }
        /// </example>
        [ApiMethod(ResponseType = typeof (Dictionary<string, string>))]
        [HttpGet]
        [RequireClientCredentials]
        public ActionResult IMFavoritesProfileRostering(MemberRequest request)
        {
            var member = MemberSA.Instance.GetMember(request.MemberId, MemberLoadFlags.None);
            if (member == null || member.MemberID <= 0 || string.IsNullOrEmpty(member.EmailAddress))
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidMemberId, "Member ID not found");
            var imFavoritesList = new Dictionary<string, string>();
            var memberIDs = HotListAccess.GetHotlistMemberIDs(member.MemberID, HotListCategory.Default,
                request.Brand, 1000, 1);
            if (memberIDs == null) return new NewtonsoftJsonResult {Result = imFavoritesList};
            foreach (int i in memberIDs)
            {
                var m = MemberSA.Instance.GetMember(i, MemberLoadFlags.None);
                if (m != null)
                {
                    imFavoritesList.Add(i + "-" + request.Brand.Site.SiteID, m.GetUserName(request.Brand));
                }
            }

            return new NewtonsoftJsonResult {Result = imFavoritesList};
        }
    }
}