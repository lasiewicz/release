﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Member.ValueObjects;
using Newtonsoft.Json;
using Spark.Common.FraudService;
using Spark.Common.Localization;
using Spark.Logger;
using Spark.MingleMigration.ValueObjects;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.Rest.Serialization;
using Spark.Rest.V2.DataAccess.MingleMigration;
using Spark.Rest.V2.Entities;
using Spark.Rest.V2.Entities.MingleMigration;
using Spark.Rest.V2.Helpers;
using Spark.Rest.V2.Models.Member;
using Spark.Rest.V2.Models.MingleMigration;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.Controllers;
using Spark.REST.Helpers;

namespace Spark.Rest.Controllers
{
    public class MigrationController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MigrationController));
        private static readonly ISparkLogger timingLog = SparkLoggerManager.Singleton.GetSparkLogger("RollingTimingLogFileAppender");

        [ApiMethod(ResponseType = typeof(SaveLogonCredentialsResult))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        public ActionResult SaveLogonCredentials(SaveLogonCredentialsRequest request)
        {
            var sessionID = Guid.NewGuid().ToString();

            string emailAddress = request.EmailAddress ?? string.Empty;
            string userName = request.UserName ?? string.Empty;
            string passwordHash = request.P1 ?? string.Empty;
            string passwordSalt = request.P2 ?? string.Empty;

            Log.LogDebugMessage(string.Format("Entering MigrationController.SaveLogonCredentials. MemberID: {0} EmailAddress: {1} Username: {2}",
                    request.MemberId.ToString(), emailAddress, userName),GetCustomDataForReportingErrors());

            var saveResult = MingleMigrationAccess.SaveLogonCredentials(request.MemberId, emailAddress,
                                                                                  userName, passwordHash,
                                                                                  passwordSalt, request.Brand);

            if (saveResult.RegisterStatus != RegisterStatusType.Success)
            {
                var status = ResourceProvider.Instance.GetResourceValue(request.Brand.Site.SiteID,
                                                                                request.Brand.Site.CultureInfo,
                                                                                ResourceGroupEnum.Global,
                                                                                MemberHelper.GetRegisterResultResourceConstant(saveResult.RegisterStatus));

                Log.LogDebugMessage("MigrationController.SaveLogonCredentials - failed to save credentials (" + saveResult.RegisterStatus + "): " + sessionID, GetCustomDataForReportingErrors());

                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedSaveLogonCredentials, status);
            }

            Log.LogDebugMessage(string.Format("Entering MigrationController.SaveLogonCredentials Success: MemberID: {0} EmailAddress: {1}",
                request.MemberId.ToString(), emailAddress), GetCustomDataForReportingErrors());

            // Success
            var sucessfulResult = new SaveLogonCredentialsResult
            {
                Status = saveResult.RegisterStatus.ToString(),
                Username = saveResult.Username
            };

            return new NewtonsoftJsonResult { Result = sucessfulResult };
        }

        /// <summary>
        /// This call is used by the Mingle sites to migrate their registrations to the LA system.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof(MingleRegistrationResult))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        public ActionResult Register(MingleRegistrationRequest request)
        {
            var registrationSessionId = Guid.NewGuid().ToString();
            Stopwatch outerStopwatch = new Stopwatch();
            Dictionary<string, string> customDataForReportingErrors = GetCustomDataForReportingErrors();
            Log.LogDebugMessage("MigrationController.Register - Entered: " + registrationSessionId, customDataForReportingErrors);
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.Register, Section:Register(brandId:{0}, regSessionId:'{1}')", request.BrandId, registrationSessionId), customDataForReportingErrors);
            outerStopwatch.Start();
            
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            //first, verfiy all required parameters are supplied, including that necessary entries are in the appropriate collections
            {
                timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.Register, Section:MingleMigrationHelper.ValidateRequest(brandId:{0}, regSessionId:'{1}')", request.BrandId, registrationSessionId), customDataForReportingErrors);
                var validationErrorString = MingleMigrationHelper.ValidateRequest(request);
                stopwatch.Stop();
                long elapsedMilliseconds1 = stopwatch.ElapsedMilliseconds;
                timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.Register, Section:MingleMigrationHelper.ValidateRequest(brandId:{0}, regSessionId:'{1}'), ElapsedTime:{2}ms", request.BrandId, registrationSessionId, elapsedMilliseconds1), customDataForReportingErrors);
                if (!string.IsNullOrEmpty(validationErrorString))
                {
                    Log.LogDebugMessage("MigrationController.Register - missing required parameters(" + validationErrorString + "): " + registrationSessionId, customDataForReportingErrors);
                    return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MingleRegistrationRequestMissingRequiredParameters, validationErrorString);
                }
            }

            stopwatch.Restart();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.Register, Section:BitConverter.ToInt32(IPAddress.Parse(request.IpAddress), brandId:{0}, regSessionId:'{1}'", request.BrandId, registrationSessionId), customDataForReportingErrors);
            var ipAddress = Constants.NULL_INT;
            if (request.IpAddress.Length > 0)
            {
                ipAddress = BitConverter.ToInt32(IPAddress.Parse(request.IpAddress).GetAddressBytes(), 0);
            }
            stopwatch.Stop();
            long elapsedMilliseconds2 = stopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.Register, Section:BitConverter.ToInt32(IPAddress.Parse(request.IpAddress), brandId:{0}, regSessionId:'{1}', ElapsedTime:{2}ms", request.BrandId, registrationSessionId, elapsedMilliseconds2), customDataForReportingErrors);

            stopwatch.Restart();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.Register, Section:MingleMigrationAccess.ValidateSearchPreferences(), brandId:{0}, regSessionId:'{1}'", request.BrandId, registrationSessionId), customDataForReportingErrors);
            string searchPreferenceValidationError;
            var validSearchPreferences = MingleMigrationAccess.ValidateSearchPreferences(request.SearchPreferences,
                                                            request.SearchPreferencesMultiValue,
                                                            request.Brand.Site.SiteID,
                                                            out searchPreferenceValidationError);
            stopwatch.Stop();
            long elapsedMilliseconds3 = stopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.Register, Section:MingleMigrationAccess.ValidateSearchPreferences(), brandId:{0}, regSessionId:'{1}', ElapsedTime:{2}ms", request.BrandId, registrationSessionId, elapsedMilliseconds3), customDataForReportingErrors);
            
            if (!validSearchPreferences)
            {
                Log.LogDebugMessage("MigrationController.Register - failed search preference parameter validation (" + searchPreferenceValidationError + "): " + registrationSessionId, customDataForReportingErrors);
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.SearchPreferenceMappingFailure, searchPreferenceValidationError);
            }

            Log.LogDebugMessage("MigrationController.Register - search preferences validated: " + registrationSessionId, customDataForReportingErrors);

            stopwatch.Restart();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.Register, Section:MingleMigrationAccess.MapAttributes(), brandId:{0}, regSessionId:'{1}'", request.BrandId, registrationSessionId), customDataForReportingErrors);
            var attributeMappingResult = MingleMigrationAccess.MapAttributes(request.AttributeData,
                                                                             request.AttributeDataMultiValue,
                                                                             request.Brand.Site.SiteID);
            stopwatch.Stop();
            long elapsedMilliseconds4 = stopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.Register, Section:MingleMigrationAccess.MapAttributes(), brandId:{0}, regSessionId:'{1}', ElapsedTime:{2}ms", request.BrandId, registrationSessionId, elapsedMilliseconds4), customDataForReportingErrors);

            if (!attributeMappingResult.Success)
            {
                stopwatch.Restart();
                timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.Register, Section:attributeMappingResult.Errors, brandId:{0}, regSessionId:'{1}'", request.BrandId, registrationSessionId), customDataForReportingErrors);
                var errorMessage = new StringBuilder();
                foreach(var error in attributeMappingResult.Errors)
                {
                    errorMessage.Append(string.Format("AttributeName: {0} ErrorType: {1} Message: {2};", 
                        error.AttributeName, error.ErrorType.ToString(), error.ErrorMessage));
                }
                stopwatch.Stop();
                long elapsedMilliseconds5 = stopwatch.ElapsedMilliseconds;
                timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.Register, Section:attributeMappingResult.Errors, brandId:{0}, regSessionId:'{1}', ElapsedTime:{2}ms", request.BrandId, registrationSessionId, elapsedMilliseconds5), customDataForReportingErrors);

                Log.LogDebugMessage("MigrationController.Register - failed attribute mapping (" + errorMessage.ToString() + "): " + registrationSessionId, customDataForReportingErrors);
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.AttributeMappingFailure, errorMessage.ToString());
            }

            Log.LogDebugMessage("MigrationController.Register - attributes mapped: " + registrationSessionId, customDataForReportingErrors);

            stopwatch.Restart();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.Register, Section:MingleMigrationHelper.GetSearchPreferencesFromRequest(), brandId:{0}, regSessionId:'{1}'", request.BrandId, registrationSessionId), customDataForReportingErrors);
            var memberSearch = MingleMigrationHelper.GetSearchPreferencesFromRequest(attributeMappingResult.MappedAttributes,
                                                                      request.SearchPreferences,
                                                                      request.SearchPreferencesMultiValue,
                                                                      request.Brand, 
                                                                      request.MemberId);
            stopwatch.Stop();
            long elapsedMilliseconds6 = stopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.Register, Section:MingleMigrationHelper.GetSearchPreferencesFromRequest(), brandId:{0}, regSessionId:'{1}', ElapsedTime:{2}ms", request.BrandId, registrationSessionId, elapsedMilliseconds6), customDataForReportingErrors);

            stopwatch.Restart();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.Register, Section:MingleMigrationAccess.RegisterMember(), brandId:{0}, regSessionId:'{1}'", request.BrandId, registrationSessionId), customDataForReportingErrors);
            string registrationErrorMessage;
            var memberRegistered = MingleMigrationAccess.RegisterMember(request.MemberId,
                                                                            request.Brand, 
                                                                            registrationSessionId,
                                                                            ipAddress,
                                                                            attributeMappingResult.MappedAttributes,
                                                                            memberSearch,
                                                                            out registrationErrorMessage);
            stopwatch.Stop();
            long elapsedMilliseconds7 = stopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.Register, Section:MingleMigrationAccess.RegisterMember(), brandId:{0}, regSessionId:'{1}', ElapsedTime:{2}ms", request.BrandId, registrationSessionId, elapsedMilliseconds7), customDataForReportingErrors);

            // Failed
            if (!memberRegistered)
            {
                Log.LogDebugMessage("MigrationController.Register - failed registration: " + registrationSessionId, customDataForReportingErrors);

                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedRegistration, registrationErrorMessage);
            }

            Log.LogDebugMessage("MigrationController.Register - successful registration (" + request.MemberId + "): " + registrationSessionId, customDataForReportingErrors);
            outerStopwatch.Stop();
            long elapsedMilliseconds8 = outerStopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.Register, Section:Register(brandId:{0}, regSessionId:'{1}'), ElapsedTime:{2}ms", request.BrandId, registrationSessionId, elapsedMilliseconds8), customDataForReportingErrors);
            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";
            return Content(String.Empty);
        }

        /// <summary>
        /// This call is used by the Mingle sites to migrate their profile edits to the LA system.
        /// </summary>
        /// <example>
        /// REQUEST
        /// POST http://api.local.spark.net/v2/brandId/90810/minglemigration/updateattributes?applicationId=498419&amp;client_secret=QwYGlTfpjZT1LrnDvWi2ZE3c6dMvVLm6AmxQr0J 
        /// {
        /// "MemberId":142027703,
        /// "AttributeDataJson":"",
        /// "AttributeDataMultiValueJson":"",
        /// "ApprovedTextAttributeDataJson":"",
        /// "SearchPreferencesJson":"[{\"Name\":\"wants_distance\",\"Weight\":0,\"Value\":50},
        /// {\"Name\":\"wants_max_age\",\"Weight\":0,\"Value\":53},
        /// {\"Name\":\"wants_max_height\",\"Weight\":0,\"Value\":83},
        /// {\"Name\":\"wants_min_age\",\"Weight\":0,\"Value\":43},
        /// {\"Name\":\"wants_min_height\",\"Weight\":0,\"Value\":72}]",
        /// "SearchPreferencesMultiValueJson":"[{\"Name\":\"wants_body_type\",\"Weight\":0,\"Values\":[0,2,3]},
        /// {\"Name\":\"wants_church_activity\",\"Weight\":0,\"Values\":[-1]},
        /// {\"Name\":\"wants_drink\",\"Weight\":0,\"Values\":[1]},
        /// {\"Name\":\"wants_education\",\"Weight\":0,\"Values\":[4,6,8]},
        /// {\"Name\":\"wants_ethnicity\",\"Weight\":0,\"Values\":[2]},
        /// {\"Name\":\"wants_marital_status\",\"Weight\":0,\"Values\":[-1]},
        /// {\"Name\":\"wants_religion\",\"Weight\":0,\"Values\":[7]},
        /// {\"Name\":\"wants_smoke\",\"Weight\":0,\"Values\":[0]}]"
        /// }
        /// 
        /// RESPONSE
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": ""
        /// }
        /// </example>
        /// <param name="MemberId">LA Member Id</param>
        /// <param name="AttributeDataJson">Stringified json for attribute name/value updates</param>
        /// <param name="AttributeDataMultiValueJson">Stringified json for attribute name/value list updates</param>
        /// <param name="ApprovedTextAttributeDataJson">Stringified json for text attribute name/value updates</param>
        /// <param name="SearchPreferencesJson">Stringified json for text search preference name/weight/value updates</param>
        [ApiMethod(ResponseType = typeof(NewtonsoftJsonResult))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        public ActionResult UpdateMemberAttributeData(MingleUpdateRequest request)
        {
            var sessionID = Guid.NewGuid().ToString();
            Dictionary<string, string> customDataForReportingErrors = GetCustomDataForReportingErrors();

            Log.LogDebugMessage(
                string.Format("MigrationController.UpdateMemberAttributeData called. MemberID: {0} SessionID: {1} BrandID: {2} AttributeCount: {3} MultiValueAttributeCount: {4}  SearchPreferenceCount: {5} MultiValuedSearchPreferenceCount: {6}",
                    request.MemberId,
                    sessionID,
                    request.Brand.BrandID,
                    request.AttributeData == null ? 0 : request.AttributeData.Count,
                    request.AttributeDataMultiValue == null ? 0 : request.AttributeDataMultiValue.Count,
                    request.SearchPreferences == null ? 0 : request.SearchPreferences.Count,
                    request.SearchPreferencesMultiValue == null ? 0 : request.SearchPreferencesMultiValue.Count
                    ), customDataForReportingErrors);

            Stopwatch outerStopwatch = new Stopwatch();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.UpdateMemberAttributeData, Section:UpdateMemberAttributeData(brandId:{0}, memberId:{1}, sessionId:'{2}')", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
            outerStopwatch.Start();

            Stopwatch stopwatch = new Stopwatch();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.UpdateMemberAttributeData, Section:MingleMigrationAccess.ValidateSearchPreferences(), brandId:{0}, memberId:{1}, sessionId:'{2}'", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
            stopwatch.Start();
            string searchPreferenceValidationError;
            var validSearchPreferences = MingleMigrationAccess.ValidateSearchPreferences(request.SearchPreferences,
                                                            request.SearchPreferencesMultiValue,
                                                            request.Brand.Site.SiteID,
                                                            out searchPreferenceValidationError);
            stopwatch.Stop();
            long elapsedMilliseconds1 = stopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.UpdateMemberAttributeData, Section:MingleMigrationAccess.ValidateSearchPreferences(), brandId:{0}, memberId:{1}, sessionId:'{2}', ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds1), customDataForReportingErrors);

            if (!validSearchPreferences)
            {
                Log.LogDebugMessage("MigrationController.UpdateMemberAttributeData - failed search preference parameter validation (" + searchPreferenceValidationError + "): " + sessionID, customDataForReportingErrors);
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.SearchPreferenceMappingFailure, searchPreferenceValidationError);
            }

            Log.LogDebugMessage("MigrationController.UpdateMemberAttributeData - search preferences validated: " + sessionID, customDataForReportingErrors);

            stopwatch.Restart();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.UpdateMemberAttributeData, Section:MingleMigrationAccess.MapAttributes(), brandId:{0}, memberId:{1}, sessionId:'{2}'", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
            var attributeMappingResult = MingleMigrationAccess.MapAttributes(request.AttributeData,
                                                                             request.AttributeDataMultiValue,
                                                                             request.Brand.Site.SiteID);
            stopwatch.Stop();
            long elapsedMilliseconds2 = stopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.UpdateMemberAttributeData, Section:MingleMigrationAccess.MapAttributes(), brandId:{0}, memberId:{1}, sessionId:'{2}', ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds2), customDataForReportingErrors);

            if (!attributeMappingResult.Success)
            {
                stopwatch.Restart();
                timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.UpdateMemberAttributeData, Section:attributeMappingResult.Errors, brandId:{0}, memberId:{1}, sessionId:'{2}'", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
                var errorMessage = new StringBuilder();
                foreach (var error in attributeMappingResult.Errors)
                {
                    errorMessage.Append(error.ErrorMessage + ";");
                }
                stopwatch.Stop();
                long elapsedMilliseconds3 = stopwatch.ElapsedMilliseconds;
                timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.UpdateMemberAttributeData, Section:attributeMappingResult.Errors, brandId:{0}, memberId:{1}, sessionId:'{2}', ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds3), customDataForReportingErrors);

                Log.LogDebugMessage("MigrationController.UpdateMemberAttributeData - failed attribute mapping (" + errorMessage.ToString() + "): " + sessionID, customDataForReportingErrors);
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.AttributeMappingFailure, errorMessage.ToString());
            }

            Log.LogDebugMessage("MigrationController.UpdateMemberAttributeData - attributes mapped: " + sessionID, customDataForReportingErrors);
            
            //TODO: ?????
            if(attributeMappingResult.MappedAttributes.ContainsKey(AttributeConstants.GENDERMASK))
            {

            }


            AttributeMappingResult textAttributeMappingResult = null;

            if (request.ApprovedTextAttributeData != null && request.ApprovedTextAttributeData.Count >=0)
            {
                stopwatch.Restart();
                timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.UpdateMemberAttributeData, Section:MingleMigrationAccess.MapTextAttributes(), brandId:{0}, memberId:{1}, sessionId:'{2}'", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
                textAttributeMappingResult = MingleMigrationAccess.MapTextAttributes(request.ApprovedTextAttributeData,
                                                                             request.Brand.Site.SiteID);
                stopwatch.Stop();
                long elapsedMilliseconds4 = stopwatch.ElapsedMilliseconds;
                timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.UpdateMemberAttributeData, Section:MingleMigrationAccess.MapTextAttributes(), brandId:{0}, memberId:{1}, sessionId:'{2}', ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds4), customDataForReportingErrors);

                if (!textAttributeMappingResult.Success)
                {
                    stopwatch.Restart();
                    timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.UpdateMemberAttributeData, Section:textAttributeMappingResult.Errors, brandId:{0}, memberId:{1}, sessionId:'{2}'", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
                    var errorMessage = new StringBuilder();
                    foreach (var error in textAttributeMappingResult.Errors)
                    {
                        errorMessage.Append(error.ErrorMessage + ";");
                    }
                    stopwatch.Stop();
                    long elapsedMilliseconds5 = stopwatch.ElapsedMilliseconds;
                    timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.UpdateMemberAttributeData, Section:textAttributeMappingResult.Errors, brandId:{0}, memberId:{1}, sessionId:'{2}', ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds5), customDataForReportingErrors);

                    Log.LogDebugMessage("MigrationController.UpdateMemberAttributeData - failed approved text attribute mapping (" + errorMessage.ToString() + "): " + sessionID, customDataForReportingErrors);
                    return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.AttributeMappingFailure, errorMessage.ToString());
                }
                else
                {
                    Log.LogDebugMessage("MigrationController.UpdateMemberAttributeData - approved text attributes mapped: " + sessionID, customDataForReportingErrors);
                }
            }

            var mappedTextAttributes = (textAttributeMappingResult == null)
                                           ? null
                                           : textAttributeMappingResult.MappedAttributes;

            stopwatch.Restart();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.UpdateMemberAttributeData, Section:MingleMigrationAccess.SaveMember(), brandId:{0}, memberId:{1}, sessionId:'{2}'", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
            var saveResult = MingleMigrationAccess.SaveMember(request.MemberId, request.Brand, attributeMappingResult.MappedAttributes,
                                                               mappedTextAttributes,
                                                               request.SearchPreferences,
                                                               request.SearchPreferencesMultiValue, false, request.P1);
            stopwatch.Stop();
            long elapsedMilliseconds6 = stopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.UpdateMemberAttributeData, Section:MingleMigrationAccess.SaveMember(), brandId:{0}, memberId:{1}, sessionId:'{2}', ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds6), customDataForReportingErrors);

            if (saveResult.SaveStatus != MingleMemberSaveStatusType.Success)
            {
                Log.LogDebugMessage("MigrationController.UpdateMemberAttributeData - failed to save member: " + sessionID, customDataForReportingErrors);
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedMingleMemberUpdate, saveResult.ErrorMessage);
            }

            Log.LogDebugMessage("MigrationController.UpdateMemberAttributeData - member saved: " + sessionID, customDataForReportingErrors);

            outerStopwatch.Stop();
            long elapsedMilliseconds7 = outerStopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.UpdateMemberAttributeData, Section:UpdateMemberAttributeData(brandId:{0}, memberId:{1}, sessionId:'{2}'), ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds7), customDataForReportingErrors);
            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";
            return Content(String.Empty);
        }

        [ApiMethod(ResponseType = typeof(void))]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        public ActionResult TransferMemberAttributeData(MingleFullProfileTransferRequest request)
        {
            var sessionID = Guid.NewGuid().ToString();
            Dictionary<string, string> customDataForReportingErrors = GetCustomDataForReportingErrors();
            Stopwatch outerStopwatch = new Stopwatch();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.TransferMemberAttributeData, Section:TransferMemberAttributeData(brandId:{0}, memberId:{1}, sessionId:'{2}')", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
            outerStopwatch.Start();

            Stopwatch stopwatch = new Stopwatch();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.TransferMemberAttributeData, Section:MingleMigrationAccess.ValidateSearchPreferences(), brandId:{0}, memberId:{1}, sessionId:'{2}'", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
            stopwatch.Start();
            string searchPreferenceValidationError;
            var validSearchPreferences = MingleMigrationAccess.ValidateSearchPreferences(request.SearchPreferences,
                                                            request.SearchPreferencesMultiValue,
                                                            request.Brand.Site.SiteID,
                                                            out searchPreferenceValidationError);
            stopwatch.Stop();
            long elapsedMilliseconds1 = stopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.TransferMemberAttributeData, Section:MingleMigrationAccess.ValidateSearchPreferences(), brandId:{0}, memberId:{1}, sessionId:'{2}', ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds1), customDataForReportingErrors);

            if (!validSearchPreferences)
            {
                Log.LogDebugMessage("MigrationController.TransferMemberAttributeData - failed search preference parameter validation (" + searchPreferenceValidationError + "): " + sessionID, customDataForReportingErrors);
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.SearchPreferenceMappingFailure, searchPreferenceValidationError);
            }

            Log.LogDebugMessage("MigrationController.TransferMemberAttributeData - search preferences validated: " + sessionID, customDataForReportingErrors);

            stopwatch.Restart();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.TransferMemberAttributeData, Section:MingleMigrationAccess.MapAttributes(), brandId:{0}, memberId:{1}, sessionId:'{2}'", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
            var attributeMappingResult = MingleMigrationAccess.MapAttributes(request.AttributeData,
                                                                             request.AttributeDataMultiValue,
                                                                             request.Brand.Site.SiteID);
            stopwatch.Stop();
            long elapsedMilliseconds2 = stopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.TransferMemberAttributeData, Section:MingleMigrationAccess.MapAttributes(), brandId:{0}, memberId:{1}, sessionId:'{2}', ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds2), customDataForReportingErrors);

            if (!attributeMappingResult.Success)
            {
                stopwatch.Restart();
                timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.TransferMemberAttributeData, Section:attributeMappingResult.Errors, brandId:{0}, memberId:{1}, sessionId:'{2}'", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
                var errorMessage = new StringBuilder();
                foreach (var error in attributeMappingResult.Errors)
                {
                    errorMessage.Append(error.ErrorMessage + ";");
                }
                stopwatch.Stop();
                long elapsedMilliseconds3 = stopwatch.ElapsedMilliseconds;
                timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.TransferMemberAttributeData, Section: attributeMappingResult.Errors, brandId:{0}, memberId:{1}, sessionId:'{2}', ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds3), customDataForReportingErrors);

                Log.LogDebugMessage("MigrationController.TransferMemberAttributeData - failed attribute mapping (" + errorMessage.ToString() + "): " + sessionID, customDataForReportingErrors);
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.AttributeMappingFailure, errorMessage.ToString());
            }

            Log.LogDebugMessage("MigrationController.TransferMemberAttributeData - attributes mapped: " + sessionID, customDataForReportingErrors);

            AttributeMappingResult textAttributeMappingResult = null;

            if (request.ApprovedTextAttributeData != null && request.ApprovedTextAttributeData.Count >=0)
            {
                stopwatch.Restart();
                timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.TransferMemberAttributeData, Section:MingleMigrationAccess.MapTextAttributes(), brandId:{0}, memberId:{1}, sessionId:'{2}'", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
                textAttributeMappingResult = MingleMigrationAccess.MapTextAttributes(request.ApprovedTextAttributeData,
                                                                             request.Brand.Site.SiteID);
                stopwatch.Stop();
                long elapsedMilliseconds4 = stopwatch.ElapsedMilliseconds;
                timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.TransferMemberAttributeData, Section:MingleMigrationAccess.MapTextAttributes(), brandId:{0}, memberId:{1}, sessionId:'{2}', ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds4), customDataForReportingErrors);

                if (!textAttributeMappingResult.Success)
                {
                    stopwatch.Restart();
                    timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.TransferMemberAttributeData, Section: textAttributeMappingResult.Errors, brandId:{0}, memberId:{1}, sessionId:'{2}'", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
                    var errorMessage = new StringBuilder();
                    foreach (var error in textAttributeMappingResult.Errors)
                    {
                        errorMessage.Append(error.ErrorMessage + ";");
                    }
                    stopwatch.Stop();
                    long elapsedMilliseconds5 = stopwatch.ElapsedMilliseconds;
                    timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.TransferMemberAttributeData, Section:textAttributeMappingResult.Errors, brandId:{0}, memberId:{1}, sessionId:'{2}', ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds5), customDataForReportingErrors);

                    Log.LogDebugMessage("MigrationController.TransferMemberAttributeData - failed approved text attribute mapping (" + errorMessage.ToString() + "): " + sessionID, customDataForReportingErrors);
                    return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.AttributeMappingFailure, errorMessage.ToString());
                }
                else
                {
                    Log.LogDebugMessage("MigrationController.TransferMemberAttributeData - approved text attributes mapped: " + sessionID, customDataForReportingErrors);
                }
            }

            var mappedTextAttributes = (textAttributeMappingResult == null)
                                           ? null
                                           : textAttributeMappingResult.MappedAttributes;

            stopwatch.Restart();
            timingLog.LogInfoMessage(string.Format("START: Method: MigrationController.TransferMemberAttributeData, Section:MingleMigrationAccess.SaveMember(), brandId:{0}, memberId:{1}, sessionId:'{2}'", request.BrandId, request.MemberId, sessionID), customDataForReportingErrors);
            var saveResult = MingleMigrationAccess.SaveMember(request.MemberId, request.Brand, attributeMappingResult.MappedAttributes,
                                                               mappedTextAttributes,
                                                               request.SearchPreferences,
                                                               request.SearchPreferencesMultiValue, true);
            stopwatch.Stop();
            long elapsedMilliseconds6 = stopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.TransferMemberAttributeData, Section:MingleMigrationAccess.SaveMember(), brandId:{0}, memberId:{1}, sessionId:'{2}', ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds6), customDataForReportingErrors);

            if (saveResult.SaveStatus != MingleMemberSaveStatusType.Success)
            {
                Log.LogDebugMessage("MigrationController.TransferMemberAttributeData - failed to save member (" + saveResult.ErrorMessage + "): " + sessionID, customDataForReportingErrors);
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedMingleMemberUpdate, saveResult.ErrorMessage);
            }

            outerStopwatch.Stop();
            long elapsedMilliseconds7 = outerStopwatch.ElapsedMilliseconds;
            timingLog.LogInfoMessage(string.Format("END: Method: MigrationController.TransferMemberAttributeData, Section:TransferMemberAttributeData(brandId:{0}, memberId:{1}, sessionId:'{2}'), ElapsedTime:{3}ms", request.BrandId, request.MemberId, sessionID, elapsedMilliseconds7), customDataForReportingErrors);

            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";
            return Content(String.Empty);
        }

     

		[Obsolete]
		[ApiMethod(ResponseType = typeof(AttributeMappingResult))]
		[RequireClientCredentials]
		[RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
		public ActionResult MapToMingleAttributes(MingleRegistrationRequest request)
		{
			var mappedAttributes = MingleMigrationAccess.MapBhToMingleAttributes(request);

			var result = new MingleMappedAttributes
			{
				AttributeData = mappedAttributes.MappedAttributes,
				AttributeDataJson = JsonConvert.SerializeObject(mappedAttributes.MappedAttributes)
			};

			// serialize the dictionaries
			//result.AttributeDataJson = JsonConvert.SerializeObject(result.AttributeData);
			//result.AttributeDataMultiValueJson = JsonConvert.SerializeObject(result.AttributeDataMultiValue);

			return new NewtonsoftJsonResult() {Result = result};
		}

		///  <summary>
		/// This call is used by the Mingle sites to migrate their registrations to the LA system.
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		/// REQUEST
		/// POST http://api.local.spark.net/v2/brandId/90810/minglemigration/DoRegistrationRiskInquiry?applicationId=498419&amp;client_secret=QwYGlTfpjZT1LrnDvWi2ZE3c6dMvVLm6AmxQr0J 
		/// {
		/// "MemberId": 145206339,
		/// "IpAddress": "192.168.3.168",
		/// "EmailAddress": "bbw0617151@matchnet.com",
		/// "RegistrationSessionId": "92dd2a4c9d9f441a9b0f5821a3b3e5dc",
		/// "AttributeDataJson": "{
		/// \"AboutMe\": \"hey hows it going\",
		/// \"UserName\": \"testUsername\",
		/// \"Birthdate\":\"4\/5\/1993\",
		/// \"MaritalStatus\" : \"Separated\",
		/// \"OccupationDescription\" :\"engineer\",
		/// \"EducationLevel\" :\"Bachelor's Degree\",
		/// \"Religion\": \"Atheist\",
		/// \"Ethnicity\":  \"African Descent\/Black\",
		/// \"PromotionID\": 55020,
		/// \"SiteFirstName\" : \"Brad\",
		/// \"SiteLastName\" : \"Essary\",
		/// \"EyeColor\" : \"Black\/Ebony\",
		/// \"HairColor\" : \"GreenBrown\",
		/// \"Height\" :142,
		/// \"Gender\": \"Male\",
		/// \"AddressCity\": \"Los Angeles\",
		/// \"AddressCountry\": \"USA\",
		/// \"AddressPostalCode\": 90034,
		/// \"AddressState\": \"California\"
		/// }"
		/// }
		/// 
		/// 
		/// RESPONSE
		/// {
		///   "code": 200,
		///   "status": "OK",
		///   "data":    {
		///      "InternalResponse":       {
		///         "responsecode": "0",
		///         "responsemessage": "Accept transaction - Success",
		///        "userInterfaceActionTypeID": 1,
		///         "userInterfaceResponseCode": "1"
		///      },
		///      "AutomateRiskInquiryStatus": 2,
		///      "RiskInquiryScore": "28"
		///   }
		///}
		///
		/// 
		[ApiMethod(ResponseType = typeof (FraudResponse))]
		[RequireClientCredentials]
		[RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
		public ActionResult DoRegistrationRiskInquiry(MingleRegistrationRiskInquiryRequest request)
		{
			var validationErrorString = MingleMigrationHelper.ValidateRiskInquiryRequest(request);
			Dictionary<string, string> customDataForReportingErrors = GetCustomDataForReportingErrors();
			var fraudResponse = new FraudResponse();

			if (!string.IsNullOrEmpty(validationErrorString))
			{
				Log.LogDebugMessage(
					"MigrationController.Register - missing required parameters(" + validationErrorString + "): " +
					new Guid(), customDataForReportingErrors);
				return
					new SparkHttpBadRequestResult(
						(int) HttpSub400StatusCode.MingleRegistrationRequestMissingRequiredParameters,
						validationErrorString);
			}
			else
			{
			   fraudResponse = FraudHelper.DoKountFraudCheck(request.MemberId, request.Brand, request.EmailAddress,request.AttributeData, request.IpAddress, request.RegistrationSessionId);
				return new NewtonsoftJsonResult() { Result =fraudResponse };
			   //TODO: define sub status codes for service call failure
			}
		}

		/// <summary>
		///     For uploading a photo for a mingle member
		/// </summary>
		/// <example>
		///     Request URL - /v2/brandId/1003/minglemigration/photo/upload?client_secret=[CLIENT_SECRET]&amp;
		///     applicationId=[APPLICATION_ID]
		///     Request Header Content-Type:application/json
		///     Request Header Accept:application/json
		///     Request Body
		///     {
		///     "MemberId": "27029711",
		///     "Caption": "Blahhh",
		///     "ListOrder": 10,
		///     "SiteId": 103,
		///     "FileData": "/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKD"
		///     }
		///     Response Body
		///     {
		///     "code": 200,
		///     "status": "OK",
		///     "data": {
		///     "MemberPhotoId": 17008351,
		///     "CloudPath": "/2015/04/28/10/222013885.jpg",
		///     "FilePath": "/Photo7100/2015/04/28/10/222013885.jpg",
		///     "Height": 260,
		///     "Width": 161
		///     }
		///     }
		/// </example>
		/// <param name="MemberId" required="true">The BH MemberId of the member who owns the photo</param>
		/// <param name="ListOrder" required="true">
		///     Integer value representing the list order of the photo. This is not zero based
		///     numbering.
		/// </param>
		/// <param name="Caption" required="true">The photo's caption, pass in empty string if no value</param>
		/// <param name="FileData"> A base64 encoded string of the raw byte array of the photo file</param>
		/// <param name="uploadRequest"></param>
		/// <paramsAdditionalInfo>
		/// </paramsAdditionalInfo>
		/// <responseAdditionalInfo>
		/// </responseAdditionalInfo>
		[ApiMethod(ResponseType = typeof(MinglePhotoUploadResult))]
		[RequireClientCredentials]
		[RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        public ActionResult UploadPhoto(MinglePhotoUploadRequest uploadRequest)
        {
            var result = MingleMigrationPhotoAccess.Instance.UploadPhoto(uploadRequest.MemberId,
                uploadRequest.Brand, uploadRequest.FileData,
                uploadRequest.Caption,
                uploadRequest.ListOrder);

            if (!result.IsSuccessful)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedUploadPhoto,
                    "Failed to upload photo.");
            }

            return new NewtonsoftJsonResult { Result = result.MinglePhotoUploadResult };
        }

		/// <summary>
		///     For reordering photos for a Mingle member
		/// </summary>
		/// <example>
		///     Request URL - /v2/brandId/1003/minglemigration/photo/reorder?applicationId=[APP_ID]&amp;
		///     client_secret=[CLIENT_SECRET]
		///     Request Header - Content-Type:application/json
		///     Request Header - Accept:application/json
		///     Request Body
		///     {
		///     "MemberId": 27029711,
		///     "PhotoOrders": [
		///     {
		///     "MemberPhotoId": 17001584,
		///     "ListOrder": 7
		///     }]
		///     }
		///     Response Body
		///     {
		///     "code": 200,
		///     "status": "OK",
		///     "data": ""
		///     }
		/// </example>
		/// <param name="MemberId" required="true">The BH MemberId of the member who owns the photo</param>
		/// <paramsAdditionalInfo>
		///     PhotoOrders Properties
		///     MemberPhotoId - Unique Id of the member photo
		///     ListOrder - New order value for the photo
		/// </paramsAdditionalInfo>
		/// <returns></returns>
		[ApiMethod(ResponseType = typeof(void))]
		[RequireClientCredentials]
		[RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
		public ActionResult ReorderPhotos(MinglePhotoReorderRequest reorderRequest)
		{
			var reorderSuccess = MingleMigrationPhotoAccess.Instance.ReorderPhotos(reorderRequest.MemberId,
				reorderRequest.Brand,
				reorderRequest.PhotoOrders);

			if (!reorderSuccess)
			{
				return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedMigrationPhotoReorder,
					"Failed to reorder photo.");
			}

			Response.StatusCode = 200;
			Response.StatusDescription = "Ok";
			return Content(String.Empty);
		}

		/// <summary>
		///     For updating a photo caption for a Mingle member
		/// </summary>
		/// <example>
		/// Request Url - /v2/brandId/1003/minglemigration/photo/updatecaption?applicationId=[APP_ID]&amp;client_secret=[CLIENT_SECRET]
		/// Request Header Content-Type: application/json
		/// Request Body
		/// {
		///     "MemberId":27029711,
		///     "MemberPhotoId":17000927,
		///     "Caption":"new caption!!!"
		/// }
		/// 
		/// Response Body
		/// {
		///     "code": 200,
		///     "status": "OK",
		///     "data": ""
		/// }
		/// </example>
		/// <param name="MemberId" required="true">The BH MemberId of the member who owns the photo</param>
		/// <param name="MemberPhotoID" required="true">The unique ID of the photo being deleted</param>
		/// <param name="Caption" required="true">The photo's caption, pass in empty string if no value</param>
		/// <returns></returns>
		[ApiMethod(ResponseType = typeof(void))]
		[RequireClientCredentials]
		[RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
        public ActionResult UpdatePhotoCaption(MinglePhotoUpdateCaptionRequest captionRequest)
        {
            var updateSuccess = MingleMigrationPhotoAccess.Instance.UpdatePhotoCaption(captionRequest.MemberId,
                captionRequest.Brand,
                captionRequest.MemberPhotoId,
                captionRequest.Caption);

            if (!updateSuccess)
            {
                return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedMigrationPhotoCaptionUpdate, "Failed to update photo caption.");
            }

            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";
            return Content(String.Empty);
        }

		/// <summary>
		///     For deleting a photo for a mingle member
		/// </summary>
		/// <example>
		/// Request URL - /v2/brandId/1003/minglemigration/photo/delete?applicationId=1000&amp;client_secret=3vG2V0pPomCHEebGJLBIjAqMrAWWFF0BjC2p6fOUKQU=
		/// Request Header Content-Type: application/json
		/// Request Body
		/// {
		///     "MemberId":27029711,
		///     "MemberPhotoId":17000884
		/// }
		/// 
		/// Reponse Body
		/// {
		///     "code": 200,
		///     "status": "OK",
		///     "data": ""
		/// }
		/// </example>
		/// <param name="MemberId" required="true">The BH MemberId of the member who owns the photo</param>
		/// <param name="MemberPhotoID" required="true">The unique ID of the photo being deleted</param>
		/// <responseAdditionalInfo>
		///If the deletion is successful, a standard http response will be returned with a status code of 200 and a status description of "Ok".
		///If the deletion fails for any reason, a JSON-serialized SparkHttpStatusCodeResult object will be returned. The SubStatusCode property will be an enumeration (listed below) and the StatusDescription will contain a textual description of why the upload failed. 
		/// </responseAdditionalInfo>
		[ApiMethod(ResponseType = typeof(void))]
		[RequireClientCredentials]
		[RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
		public ActionResult DeletePhoto(MinglePhotoDeleteRequest deleteRequest)
		{
			var deleteSuccess = MingleMigrationPhotoAccess.Instance.DeletePhoto(deleteRequest.MemberId, deleteRequest.Brand, deleteRequest.MemberPhotoId);

			if (!deleteSuccess)
			{
				return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedDeletePhoto, "Failed to delete photo.");
			}

			Response.StatusCode = 200;
			Response.StatusDescription = "Ok";
			return Content(String.Empty);
		}

		/// <summary>
		///     For transferring the metadata for a mingle member's photo
		/// </summary>
		/// <example>
		///Request URL - /v2/brandId/1003/minglemigration/photo/transfermetadata?client_secret=[CLIENT_SECRET]&amp;applicationId=[APPLICATION_ID]
		///Request Content Type - application/json
		/// 
		///Request Body
		///       {
		///    "MemberId": 1234,
		///    "ListOrder": 1,
		///    "InsertDate": "1/2/2014 5:00:00",
		///    "Caption": "Caption",
		///    "ApprovedForMain": 1,
		///    "IsMain": 1,
		///    "Files": [
		///        {
		///            "FileType": 1,
		///            "FileRoot": 1000,
		///            "FilePath": "/Photo1000/2014/03/19/12/120210324.jpg",
		///            "CloudPath": "/erfw/2014/03/19/12/120210324.jpg",
		///            "Height": 200,
		///            "Width": 200,
		///            "Size": 420000
		///        },
		///        {
		///            "FileType": 2,
		///            "FileRoot": 3000,
		///            "FilePath": "/Photo3000/2014/03/19/12/120210335.jpg",
		///            "CloudPath": "/erfw/2014/03/19/12/120210335.jpg",
		///            "Height": 200,
		///            "Width": 200,
		///            "Size": 420000
		///        }
		///    ]
		///}
		///Response Body
		///{
		///     "code": 200,
		///     "status": "OK",
		///     "data": {
		///         "MemberPhotoId": 3118820
		///     }
		/// }
		/// </example>
		/// <param name="MemberId" required="true">The BH MemberId of the member who owns the photo</param>
		/// <param name="ListOrder" required="true">Integer. The list order of the photo</param>
		/// <param name="Caption" required="true">The photo's caption, pass in empty string if no value</param>
		/// <param name="InsertDate" required="true">The date the photo was uploaded</param>
		/// <param name="ApprovedForMain" required="true">If true, indicates that this photo is approved to be a main photo</param>
		/// <param name="IsMain" required="true">If true, indicates that this photo is currently the main photo</param>
		/// <param name="Files" required="true" values="collection of PhotoTransferFile">JSON array of metadata for the various files for the photo in a collection</param>
		/// <param name="FilesJson">If this value is passed in, Files is populated with that value instead of the value it's assigned</param>
		/// <paramsAdditionalInfo>
		///
		///PhotoTransferFile Properties
		///     Height - The height of the photo in pixels
		///     Width - The width of the photo in pixels
		///     CloudPath - The relative path to the photo as stored in the cloud.
		///     FilePath - Optional(default is empty string). String. The relative path to the photo including the fileroot
		///     FileRoot - Optional(default is 0). Integer. Randomly selected filerootid.
		///     Size - The size of the file in bytes
		///     FileType - An enumeration that will represent the type of file - thumbnail, mobile optimized, etc. We will need to agree on what the possible file types are
		/// 
		///FileType Enum
		///     RegularFWS = 1,
		///     Thumbnail = 2,
		///     Original = 3,
		///     iPhone4S = 4,
		///     MinglePreview = 5
		/// 
		/// </paramsAdditionalInfo>
		/// <responseAdditionalInfo>
		///     This is the unique key to the photo in the BH system, and will be used in subsequent calls to
		///     approve or delete the photo
		/// </responseAdditionalInfo>
		[ApiMethod(ResponseType = typeof(MinglePhotoTransferResult))]
		[RequireClientCredentials]
		[RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
		public ActionResult TransferPhotoMetadata(MinglePhotoTransferRequest transferRequest)
		{
			var memberPhotoId = MingleMigrationPhotoAccess.Instance.SavePhotoMetadata(transferRequest.MemberId,
				transferRequest.Brand,
				transferRequest.ListOrder,
				transferRequest.Caption,
				transferRequest.InsertDate,
				transferRequest.ApprovedForMain,
				transferRequest.IsMain,
				transferRequest.Files);

			if (memberPhotoId <= 0)
			{
				return new SparkHttpBadRequestResult((int)HttpSub400StatusCode.FailedPhotoSave,
					"Failed to save photo metadata.");
			}

			return new NewtonsoftJsonResult { Result = new MinglePhotoTransferResult { MemberPhotoId = memberPhotoId } };
		}

		/// <summary>
		///     Batch support for TransferPhotoMetadata. Refer the doc for TransferPhotoMetadata for request and response details.
		/// </summary>
		///<example>
		///Request URL - /v2/brandId/1003/minglemigration/photo/transfermetadatacollection?client_secret=[CLIENT_SECRET]&amp;applicationId=[APPLICATION_ID]
		///Request Content Type - application/json
		/// 
		///Request Body
		///[
		///    {
		///        "MemberId": 1234,
		///        "ListOrder": 1,
		///        "InsertDate": "1/2/2014 5:00:00",
		///        "Caption": "Caption",
		///        "ApprovedForMain": 1,
		///        "IsMain": 1,
		///        "Files": [
		///            {
		///                "FileType": 1,
		///                "FileRoot": 1000,
		///                "FilePath": "/Photo1000/2014/03/19/12/120210324.jpg",
		///                "CloudPath": "/erfw/2014/03/19/12/120210324.jpg",
		///                "Height": 200,
		///                "Width": 200,
		///                "Size": 420000
		///            },
		///            {
		///                "FileType": 2,
		///                "FileRoot": 3000,
		///                "FilePath": "/Photo3000/2014/03/19/12/120210335.jpg",
		///                "CloudPath": "/erfw/2014/03/19/12/120210335.jpg",
		///                "Height": 200,
		///                "Width": 200,
		///                "Size": 420000
		///            }
		///        ]
		///    },
		///    {
		///        "MemberId": 1234,
		///        "ListOrder": 2,
		///        "InsertDate": "1/2/2014 5:00:00",
		///        "Caption": "Caption",
		///        "ApprovedForMain": 1,
		///        "IsMain": 0,
		///        "Files": [
		///            {
		///                "FileType": 1,
		///                "FileRoot": 1000,
		///                "FilePath": "/Photo1000/2014/03/19/12/120210324.jpg",
		///                "CloudPath": "/erfw/2014/03/19/12/120210324.jpg",
		///                "Height": 200,
		///                "Width": 200,
		///                "Size": 420000
		///            },
		///            {
		///                "FileType": 2,
		///                "FileRoot": 3000,
		///                "FilePath": "/Photo3000/2014/03/19/12/120210335.jpg",
		///                "CloudPath": "/erfw/2014/03/19/12/120210335.jpg",
		///                "Height": 200,
		///                "Width": 200,
		///                "Size": 420000
		///            }
		///        ]
		///    }
		///]
		/// 
		///Response Body
		///{
		///    "code": 200,
		///    "status": "OK",
		///    "data": [
		///        {
		///            "MemberId": 27029711,
		///            "MemberPhotoId": 17001584,
		///            "ListOrder": 6
		///        },
		///        {
		///            "MemberId": 27029711,
		///            "MemberPhotoId": 17008363,
		///            "ListOrder": 5
		///        }
		///    ]
		///}
		/// </example>
		/// <param name="request"></param>
		/// <returns></returns>
		[ApiMethod(ResponseType = typeof(ConcurrentBag<MinglePhotoTransferCollectionResult>))]
		[RequireClientCredentials]
		[RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/MigrationTrustedClientIPWhitelist.xml")]
		public ActionResult TransferPhotoMetadataCollection(List<MinglePhotoTransferRequest> request)
		{
			var result = new List<MinglePhotoTransferCollectionResult>();
            var ctx = System.Web.HttpContext.Current;
			Parallel.ForEach(request, ParallelTasksHelper.GetMaxDegreeOfParallelism(), memberPhoto =>
			{
                //set HttpContext for thread
                System.Web.HttpContext.Current = ctx;
			    try
			    {
			        var memberPhotoId = MingleMigrationPhotoAccess.Instance.SavePhotoMetadata(memberPhoto.MemberId,
			            memberPhoto.Brand,
			            memberPhoto.ListOrder,
			            memberPhoto.Caption,
			            memberPhoto.InsertDate,
			            memberPhoto.ApprovedForMain,
			            memberPhoto.IsMain,
			            memberPhoto.Files);

			        result.Add(new MinglePhotoTransferCollectionResult
			        {
			            MemberPhotoId = memberPhotoId,
			            MemberId = memberPhoto.MemberId,
			            ListOrder = memberPhoto.ListOrder
			        });
			    }
			    finally
			    {
			        //unset HttpContext for thread (precaution for GC)
			        System.Web.HttpContext.Current = null;
			    }
			});

			return new NewtonsoftJsonResult { Result = result };
		}
	}
}
