﻿#region

using System.Web.Mvc;
using Spark.Logger;
using Spark.Rest.Authorization;
using Spark.REST.DataAccess.Applications;
using Spark.Rest.Entities.Applications;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

#endregion

namespace Spark.REST.Controllers
{
    public class ApplicationController : SparkControllerBase
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(ApplicationController));

        [RequireClientCredentials]
        public ActionResult GetApplication(int applicationId)
        {
            var application = ApplicationAccess.GetApplication(applicationId);
            if (application == null)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidApplicationId,
                                                     string.Format("Application not found for appId:{0}", applicationId));
            }
            return new NewtonsoftJsonResult {Result = application};
        }

        [RequireClientCredentials]
        public ActionResult CreateApplication(Application application)
        {
            var hostName = GetHostName();
            var appResource = ApplicationAccess.CreateApplication(application, hostName);
            return new NewtonsoftJsonResult {Result = new {resource = appResource}};
        }

        [RequireClientCredentials]
        public ActionResult UpdateApplication(Application application)
        {
            if (!ApplicationAccess.UpdateApplication(application.ApplicationId, application))
            {
//				return new NewtonsoftJsonResult { Result = new { error = "Application not found" } };
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidApplicationId,
                                                     string.Format("Application not found for appId:{0}",
                                                                   application.ApplicationId));
            }
            return new NewtonsoftJsonResult {Result = new {result = "Application updated"}};
        }

        [RequireClientCredentials]
        public ActionResult ResetClientSecret(int applicationId)
        {
            var hostName = GetHostName();
            var newResource = ApplicationAccess.ResetClientSecret(applicationId, hostName);
            if (newResource == null)
            {
//				return new NewtonsoftJsonResult { Result = new { error = "Application not found" } };
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidApplicationId,
                                                     string.Format("Application not found for appId:{0}", applicationId));
            }
            return new NewtonsoftJsonResult {Result = new {resource = newResource}};
        }

        [RequireClientCredentials]
        public ActionResult DeleteApplication(int applicationId)
        {
            if (!ApplicationAccess.DeleteApplication(applicationId))
            {
//				return new NewtonsoftJsonResult { Result = new { error = "Application not found" } };
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidApplicationId,
                                                     string.Format("Application not found for appId:{0}", applicationId));
            }
            return new NewtonsoftJsonResult {Result = new {result = "Application deleted"}};
        }

        private string GetHostName()
        {
            var hostName = string.Empty;
            if (HttpContext.Request.Url != null)
            {
                hostName = HttpContext.Request.Url.Host;
            }
            return hostName;
        }
    }
}