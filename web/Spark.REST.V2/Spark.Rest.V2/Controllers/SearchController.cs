#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Mvc;
using Spark.Logger;
using Spark.Rest.Authorization;
using Spark.Rest.Configuration;
using Spark.REST.DataAccess.Search;
using Spark.REST.Entities.Search;
using Spark.REST.Models;
using Spark.Rest.Models.Search;
using Spark.REST.Models.Search;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Configuration;
using Spark.Rest.V2.DataAccess.MembersOnline;
using Spark.Rest.V2.Models.Search;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

#endregion

namespace Spark.REST.Controllers
{
    public class SearchController : SparkControllerBase
    {
        private static readonly ISearchAccess SearchAccess = SearchAccessSal.Instance;
        private static readonly ISparkLogger log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(SearchController));
        private static readonly ISparkLogger timingLog = SparkLoggerManager.Singleton.GetSparkLogger("RollingTimingLogFileAppender");

        /// <summary></summary>
        /// <param name="searchRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (SearchResults))]
        [HttpPost]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [Obsolete("This call is obsolete. Please use <a href='/v2/docs/search/GetSearchResultsViaPostV2'>GetSearchResultsViaPostV2</a> instead.")]
        [DeprecatedUse]
        public ActionResult GetSearchResultsViaPost(SearchResultsRequest searchRequest)
        {
            Stopwatch stopwatch=new Stopwatch();
            stopwatch.Start();     
            var searchResults = ReadonlySearch.Instance.GetSearchResults(searchRequest.Brand, GetMemberId(),
                                                                         searchRequest.Gender,
                                                                         searchRequest.SeekingGender,
                                                                         searchRequest.MinAge, searchRequest.MaxAge,
                                                                         searchRequest.MaxDistance,
                                                                         searchRequest.ShowOnlyJewishMembers,
                                                                         searchRequest.ShowOnlyMembersWithPhotos,
                                                                         searchRequest.PageSize,
                                                                         searchRequest.PageNumber,
                                                                         0,
                                                                         searchRequest.MinHeight,
                                                                         searchRequest.MaxHeight);
            stopwatch.Stop();
            
            return new NewtonsoftJsonResult {Result = searchResults};
        }

        /// <summary>The active method for performing a member search.  Search params are all passed in by the client, thus the post method.</summary>
        /// <example> 
        /// 
        /// Notes:
        /// Any parameters not specified will automatically default to your saved preferences
        /// Default sort: JoinDate
        /// Default search type: WebSearch
        /// Default entry point: None
        /// Attributeset: The search results returned are based on the ResultSetProfile attribute set.  
        /// Attributeset: If IncludeBigData is true, it will return ResultSetProfileBigV2 which has the AboutMe essay, possibly others down the road.
        /// 
        /// Example: Bare minimum recommended (and have it automatically use your default saved preferences)
        /// POST http://api.stage3.spark.net/v2/brandId/1003/search/results?access_token=ACCESS_TOKEN
        /// {
        ///    "pageSize": 10,
        ///    "pageNumber": 1,
        ///    "searchType": "4",
        ///    "searchOrderByType": "2",
        ///    "searchEntryPointType": "1",
        ///    "useDefaultPreferences" : "true"
        ///}
        ///
        /// Example: Using ListPosition as offset instead of Page Number
        /// Instead of passing in a page number, this uses ListPosition to say starting at position 1, get 10 items.  This will in essence give you the first 10 items.
        /// To get the next 10, we'd use ListPosition 11.
        /// 
        /// POST http://api.stage3.spark.net/v2/brandId/1003/search/results?access_token=ACCESS_TOKEN
        /// {
        ///    "pageSize": 10,
        ///    "listPosition": 1,
        ///    "searchType": "4",
        ///    "searchOrderByType": "2",
        ///    "searchEntryPointType": "1",
        ///    "useDefaultPreferences" : "true"
        ///}
        /// 
        /// 
        /// Example: Performing basic search with specific religion preferences
        /// 
        /// POST http://api.stage3.spark.net/v2/brandId/1003/search/results?access_token=ACCESS_TOKEN
        /// {
        ///    "pageSize": 1,
        ///    "pageNumber": 1,
        ///    "showOnlyJewishMembers": "false",
        ///    "gender": "male",
        ///    "seekingGender": "female",
        ///    "minAge": 40,
        ///    "maxAge": 45,
        ///    "regionId": 9805721,
        ///    "maxDistance": 5,
        ///    "showOnlyMembersWithPhotos": "false",
        ///    "searchType": "4",
        ///    "searchOrderByType": "2",
        ///    "searchEntryPointType": "1",
        ///    "keywords": "delightful",
        ///    "includeSpotlightProfile": "true",
        ///    "AdvancedPreferencesJson": "{\"JDateReligion\":[\"2\",\"256\",\"8\",\"16\",\"256\",\"512\",\"16384\"]}"
        ///}
        ///
        ///     Performing Daily Matches which uses default preferences 
        /// 
        /// {
        ///     "searchType":14 //SearchType.DailyMatches
        ///}
        /// RESPONSE
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": {
        ///        "Members": [{
        ///            "memberId": 148545631,
        ///            "username": "lovinglife4494E9",
        ///            "primaryPhoto": {
        ///                "memberPhotoId": 24372123,
        ///                "fullId": 174194345,
        ///                "thumbId": 174000753,
        ///                "fullPath": "http://s3.amazonaws.com/sparkmemberphotos/2014/07/05/18/174194345.jpg",
        ///                "thumbPath": "http://s3.amazonaws.com/sparkmemberphotos/2014/07/05/18/174000753.jpg",
        ///                "caption": "",
        ///                "listorder": 1,
        ///                "isCaptionApproved": false,
        ///                "isPhotoApproved": true,
        ///                "IsMain": true,
        ///                "IsApprovedForMain": true
        ///            },
        ///            "maritalStatus": "Divorced",
        ///            "gender": "Female",
        ///            "seekingGender": "Male",
        ///            "lookingFor": ["Marriage",
        ///            "A long-term relationship"],
        ///            "age": 44,
        ///            "location": "Los Angeles, CA ",
        ///            "lastLoggedIn": "2014-10-29T15:45:32Z",
        ///            "lastUpdated": "2014-10-29T15:46:36.48Z",
        ///            "isOnline": false,
        ///            "jDateEthnicity": "Ashkenazi",
        ///            "registrationDate": "2014-07-03T01:59:19.387Z",
        ///            "passedFraudCheck": true,
        ///            "approvedPhotoCount": 7,
        ///            "zipcode": "90064",
        ///            "regionId": 9805721,
        ///            "heightCm": 160,
        ///            "selfSuspendedFlag": false,
        ///            "adminSuspendedFlag": false,
        ///            "blockContactByTarget": false,
        ///            "blockSearchByTarget": false,
        ///            "removeFromSearchByTarget": false,
        ///            "blockContact": false,
        ///            "blockSearch": false,
        ///            "removeFromSearch": false,
        ///            "isHighlighted": false,
        ///            "isSpotlighted": false,
        ///            "DefaultPhoto": {
        ///                "memberPhotoId": 0,
        ///                "fullId": 174194345,
        ///                "thumbId": 174000753,
        ///                "fullPath": "http://s3.amazonaws.com/sparkmemberphotos/2014/07/05/18/174194345.jpg",
        ///                "thumbPath": "http://s3.amazonaws.com/sparkmemberphotos/2014/07/05/18/174000753.jpg",
        ///                "caption": "",
        ///                "listorder": 0,
        ///                "isCaptionApproved": false,
        ///                "isPhotoApproved": true,
        ///                "IsMain": false,
        ///                "IsApprovedForMain": false
        ///            },
        ///            "PhotoUrl": "http://s3.amazonaws.com/sparkmemberphotos/2014/07/05/18/174194345.jpg",
        ///            "ThumbnailUrl": "http://s3.amazonaws.com/sparkmemberphotos/2014/07/05/18/174000753.jpg",
        ///            "matchRating": 95
        ///        }],
        ///        "Spotlight": {
        ///             "memberId": 100067359,
        ///             "username": "camel0010",
        ///             ...
        ///        }
        ///        "MatchesFound": 212
        ///    }
        ///}
        ///</example>
        /// <param name="PageSize" required="true" values="1+">The number of results per page. Not required for Daily Matches</param>
        /// <param name="PageNumber" required="true" values="1+">The page number.  You may use ListPosition instead of page number, but one of these are required. Not required for Daily Matches</param>
        /// <param name="ListPosition" values="1+">The list position to start getting back results, so list position 1 means start getting items at result item 1.  This can be used instead of page number.</param>
        /// <param name="Keywords" values="string of up to 200 characters"></param>
        /// <param name="SearchType">This specifies the type of search, which may kick off certain logical rules; Matches and APISearch includes match rating in the results</param>
        /// <param name="SearchOrderByType">This is the sorting</param>
        /// <param name="SearchEntryPointType">This indicates where the searching originated for reporting purposes</param>
        /// <param name="Gender" values="male, female"></param>
        /// <param name="SeekingGender" values="male, female"></param>
        /// <param name="MinAge" values="18 to 99">Minimum age</param>
        /// <param name="MaxAge" values="18 to 99">Maximum age</param>
        /// <param name="Location">Not used</param>
        /// <param name="RegionID">The regionID representing the location</param>
        /// <param name="MaxDistance">Distance in miles</param>
        /// <param name="IsNewPrefs">Not used</param>
        /// <param name="BatchSize">Not used</param>
        /// <param name="MinHeight">Minimum height in centimeters</param>
        /// <param name="MaxHeight">Maximum height in centimeters</param>
        /// <param name="ShowOnlyRamah" values="true, false"></param>
        /// <param name="ShowOnlyMembersWithPhotos" values="true, false"></param>
        /// <param name="AdvancedPreferences">Do not use for now</param>
        /// <param name="AdvancedPreferencesJSON" values="MaritalStatus[br]JdateReligion[br]JDateEthnicity[br]SmokingHabits[br]DrinkingHabits[br]BodyType[br]KeepKosher[br]EducationLevel[br]SynagogueAttendance[br]ChildrenCount[br]MoreChildrenFlag[br]Custody[br]ActivityLevel[br]LanguageMask[br]RelocateFlag">JSON serialized key value(array list of option values) pairs</param>
        /// <param name="IncludeSpotlightProfile">Determines whether to include a spotlighted profile (if any) as part of response</param>
        /// <param name="UseDefaultPreferences">Determines whether to automatically use your default saved match preferences for any parameters not specified</param>
        /// <param name="IncludeBigData">Default is false.  When set to true, the attributeset will include big data (i.e. About me essay)</param>
        /// <param name="MemberID">Not used</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <paramsAdditionalInfo>For enums, you can pass in either the integer value or the name value</paramsAdditionalInfo>
        /// <responseAdditionalInfo>A JSON with an array of member results and a total results available number</responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof (SearchResultsV2))]
        [HttpPost]
        [RequireAccessTokenV2]
        [Stopwatch]
        public ActionResult GetSearchResultsViaPostV2(SearchResultsRequest searchRequest)
        {
            var stopwatch = new Stopwatch();
            var memberId = GetMemberId();
            var customDataForReportingErrors = GetCustomDataForReportingErrors();
            timingLog.LogInfoMessage(string.Format("START: Section:GetSearchResultsViaPostV2(brandId:{0}, memberId:{1})", searchRequest.BrandId, memberId), customDataForReportingErrors);
            stopwatch.Start();
            var searchResults = ReadonlySearch.Instance.GetSearchResultsV2(searchRequest.Brand, memberId,
                                                                           searchRequest.Gender,
                                                                           searchRequest.SeekingGender,
                                                                           searchRequest.MinAge, searchRequest.MaxAge, 
                                                                           searchRequest.RegionId,
                                                                           searchRequest.MaxDistance,
                                                                           searchRequest.ShowOnlyJewishMembers,
                                                                           searchRequest.ShowOnlyMembersWithPhotos,
                                                                           searchRequest.PageSize,
                                                                           searchRequest.PageNumber,
                                                                           searchRequest.ListPosition,
                                                                           searchRequest.MinHeight,
                                                                           searchRequest.MaxHeight, 
                                                                           searchRequest.SearchOrderByType, 
                                                                           searchRequest.SearchType,
                                                                           searchRequest.SearchEntryPointType,
                                                                           searchRequest.Keywords,
                                                                           searchRequest.IncludeSpotlightProfile,
                                                                           searchRequest.UseDefaultPreferences,
                                                                           searchRequest.ShowOnlyRamah,
																		   searchRequest.IncludeBigData,
                                                                           searchRequest.AdvancedPreferences);
            stopwatch.Stop();
            timingLog.LogInfoMessage(string.Format("END: Section:GetSearchResultsViaPostV2(brandId:{0}, memberId:{1}), ElapsedTime:{2}ms", searchRequest.BrandId, memberId, stopwatch.ElapsedMilliseconds), customDataForReportingErrors);
            return new NewtonsoftJsonResult { Result = searchResults };
        }

        /// <summary>not currently used</summary>
        /// <param name = "searchRequest"></param>
        /// <returns></returns>
        [Obsolete("This call is obsolete. Please use <a href='/v2/docs/search/getmatchresultsv2'>GetMatchResultsV2</a> instead.")]
        [ApiMethod(ResponseType = typeof(SearchResults))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        [DeprecatedUse]
        public ActionResult GetMatchResults(SearchResultsRequest searchRequest)
        {
            var searchResults = SearchAccess.GetSearchResults(searchRequest.Brand, GetMemberId(), searchRequest.PageSize,
                                                              searchRequest.PageNumber);
            return new NewtonsoftJsonResult {Result = searchResults};
        }

        /// <summary>not currently used</summary>
        /// <param name = "searchRequest"></param>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (SearchResultsV2))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetMatchResultsV2(SearchResultsRequest searchRequest)
        {
            var searchResults = SearchAccess.GetSearchResultsV2(searchRequest.Brand, GetMemberId(),
                                                                searchRequest.PageSize, searchRequest.PageNumber);
            return new NewtonsoftJsonResult {Result = searchResults};
        }

        /// <summary>Gets the match preferences for a member. In the response, AdvancedPreferences returns a dictionary of mask type search preference names and values</summary>
        /// <example>
        /// GET http://api.spark.net/v2/brandId/1003/match/preferences?access_token=[ACCESS_TOKEN]
        /// 
        /// RESPONSE
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": {
        ///        "memberId": 113915549,
        ///        "gender": "Male",
        ///        "seekingGender": "Female",
        ///        "minAge": 29,
        ///        "maxAge": 35,
        ///        "location": null,
        ///        "regionId": "3404470",
        ///        "maxDistance": 25,
        ///        "showOnlyMembersWithPhotos": true,
        ///        "MinHeight": -2147483647,
        ///        "MaxHeight": -2147483647,
        ///        "AdvancedSearchPreferences": {
        ///            "relationshipstatus": -2147483647,
        ///            "smokinghabits": -2147483647,
        ///            "bodytype": -2147483647,
        ///            "custody": -2147483647,
        ///            "churchactivity": -2147483647,
        ///            "jdatereligion": 2047,
        ///            "maritalstatus": 2,
        ///            "childrencount": -2147483647,
        ///            "relationshipmask": -2147483647,
        ///            "jdateethnicity": -2147483647,
        ///            "churchimportance": -2147483647,
        ///            "servedmission": -2147483647,
        ///            "keepkosher": -2147483647,
        ///            "educationlevel": -2147483647,
        ///            "ethnicity": -2147483647,
        ///            "gendermask": 6,
        ///            "religion": -2147483647,
        ///            "synagogueattendance": -2147483647,
        ///            "sexualidentitytype": -2147483647,
        ///            "templestatus": -2147483647,
        ///            "morechildrenflag": -2147483647,
        ///            "languagemask": -2147483647,
        ///            "zodiac": -2147483647,
        ///            "relocateflag": -2147483647,
        ///            "activitylevel": -2147483647,
        ///            "christianreligion": -2147483647,
        ///            "drinkinghabits": -2147483647
        ///        }
        ///    }
        ///}
        /// </example>
        /// <param name="MemberID">Not used</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <paramsAdditionalInfo></paramsAdditionalInfo>
        /// <responseAdditionalInfo></responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof(Entities.Search.SearchPreferences))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult GetMatchPreferences(MemberRequest memberRequest)
        {
            var searchPreferences = SearchAccess.GetSearchPreferences(memberRequest.Brand, GetMemberId());
            return new NewtonsoftJsonResult {Result = searchPreferences};
        }

        /// <summary>This is used to update a member's match search preferences.</summary>
        /// <example>
        /// 
        /// PUT http://api.spark.net/v2/brandId/1003/match/preferences?access_token=
        /// {
        ///    "showOnlymembersWithPhotos": "false",
        ///    "gender": "male",
        ///   "seekingGender": "female",
        ///    "minAge": 40,
        ///    "maxAge": 45,
        ///    "Location": "9805721",
        ///    "regionId": 9805721,
        ///    "maxDistance": 5,
        ///    "showOnlyMembersWithPhotos": "false",
        ///    "minHeight": 55,
        ///    "maxHeight": 1000,
        ///    "AdvancedPreferencesJson": "{\"JDateReligion\":[\"2\",\"256\",\"8\",\"16\",\"256\",\"512\",\"16384\"]}"
        ///}
        /// 
        /// RESPONSE
        /// {"code":200,"status":"OK","data":""}
        /// 
        /// </example>
        /// <param name="Gender" values="male, female"></param>
        /// <param name="SeekingGender" values="male, female"></param>
        /// <param name="MinAge" values="18 to 99">Minimum age</param>
        /// <param name="MaxAge" values="18 to 99">Maximum age</param>
        /// <param name="Location">Same as regionID but passed in as a string, using Location or RegionID is ok but if both are used, the RegionID parameter will take precedence</param>
        /// <param name="RegionID">The regionID representing the location</param>
        /// <param name="MaxDistance">Distance in miles</param>
        /// <param name="IsNewPrefs">Not used</param>
        /// <param name="BatchSize">Not used</param>
        /// <param name="MinHeight">Minimum height in centimeters</param>
        /// <param name="MaxHeight">Maximum height in centimeters</param>
        /// <param name="AdvancedPreferences">Do not use for now</param>
        /// <param name="AdvancedPreferencesJSON" values="MaritalStatus[br]JdateReligion[br]JDateEthnicity[br]SmokingHabits[br]DrinkingHabits[br]BodyType[br]KeepKosher[br]EducationLevel[br]SynagogueAttendance[br]ChildrenCount[br]MoreChildrenFlag[br]Custody[br]ActivityLevel[br]LanguageMask[br]RelocateFlag">JSON serialized key value(array list of option values) pairs</param>
        /// <param name="MemberID">Not used</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        ///<param name="includeBigData">Not used</param>
        /// <paramsAdditionalInfo>
        /// searchPreferencesRequest: This is the same type of request as the one used in GetSearchResultsViaPostV2.
        /// All params are optional.
        /// </paramsAdditionalInfo>
        /// <responseAdditionalInfo></responseAdditionalInfo>
        /// <returns></returns>
        [ApiMethod(ResponseType = typeof (void))]
        [RequireAccessTokenV2]
        [MembersOnlineActionFilter]
        public ActionResult PutMatchPreferences(SearchPreferencesRequest searchPreferencesRequest)
        {
            if (searchPreferencesRequest.MemberId <= 0)
                searchPreferencesRequest.MemberId = GetMemberId();

            var success = SearchAccess.SaveSearchPreferences(searchPreferencesRequest, searchPreferencesRequest.Brand);
            if (!success)
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedSaveMatchPreferences,
                    "Could not save match prefs.");

            Response.StatusCode = 200;
            Response.StatusDescription = "Ok";
            return Content(String.Empty);
        }

        /// <summary>Getting member results for Secret Admirer</summary>
        /// <example>
        /// POST http://api.spark.net/v2/brandId/1003/search/secretadmirer?access_token=ACCESS_TOKEN
        /// {
        ///    "minage": 18,
        ///    "maxage": 45,
        ///    "pagesize": 1,
        ///    "pagenumber": 1,
        ///    "showonlyjewishmembers": false
        ///}
        ///
        /// Response
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": {
        ///        "Members": [{
        ///            "memberId": 31181547,
        ///            "username": "jewgal84",
        ///            "gender": "Female",
        ///            "seekingGender": "Male",
        ///            "age": 29,
        ///            "primaryPhoto": {
        ///                "memberPhotoId": 24288631,
        ///                "fullId": 173410285,
        ///                "thumbId": 173036948,
        ///                "fullPath": "http://s3.amazonaws.com/sparkmemberphotos/2014/04/12/18/173410285.jpg",
        ///                "thumbPath": "http://s3.amazonaws.com/sparkmemberphotos/2014/04/12/18/173036948.jpg",
        ///                "caption": "",
        ///                "listorder": 1,
        ///                "isCaptionApproved": false,
        ///                "isPhotoApproved": true,
        ///                "IsMain": true,
        ///                "IsApprovedForMain": true
        ///            },
        ///            "lastLoggedIn": "2014-10-30T01:07:57Z",
        ///            "lastUpdated": "2014-09-13T05:35:41.42Z",
        ///            "isOnline": false,
        ///            "location": "Los Angeles, CA ",
        ///            "zipcode": "90035"
        ///        }],
        ///        "MatchesFound": 10
        ///    }
        ///}
        /// </example>
        /// <param name="PageSize" required="true" values="1+"></param>
        /// <param name="PageNumber" required="true" values="1+">The page number.  You may use ListPosition instead of page number, but one of these are required.</param>
        /// <param name="ListPosition" values="1+">The list position to start getting back results, so list position 1 means start getting items at result item 1.  This can be used instead of page number.</param>
        /// <param name="MinAge" values="18 to 99">Minimum age</param>
        /// <param name="MaxAge" values="18 to 99">Maximum age</param>
        /// <param name="MemberID">Not used</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <paramsAdditionalInfo></paramsAdditionalInfo>
        /// <responseAdditionalInfo>A JSON with an array of member results and a total results available number</responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof (SearchResultsV2))]
        [HttpPost]
        [RequireAccessTokenV2]
        public ActionResult GetSecretAdmirerResults(SecretAdmirerRequest secretAdmirerRequest)
        {
            const string attributeSetType = "SecretAdmirerProfile";

            var searchResults = ReadonlySearch.Instance.GetSecretAdmirerSearchResults(secretAdmirerRequest.Brand,
                GetMemberId(),
                secretAdmirerRequest.MinAge,
                secretAdmirerRequest.MaxAge,
                secretAdmirerRequest.PageSize,
                secretAdmirerRequest.PageNumber,
                secretAdmirerRequest.ListPosition,
                attributeSetType);
            return new NewtonsoftJsonResult {Result = searchResults};
        }

        /// <summary>This method provides visitor search results.</summary>
        /// <example>
        /// POST http://api.spark.net/v2/brandId/1003/visitorsearch/results?applicationid=APPID&amp;client_secret=
        /// 
        /// {
        ///        "pageSize": 1,
        ///        "pageNumber": 1,
        ///        "showOnlyJewishMembers": "false",
        ///        "gendermask": 6,
        ///        "minAge": 40,
        ///        "maxAge": 45,
        ///        "regionId": 9805721,
        ///        "maxDistance": 5,
        ///        "showOnlyMembersWithPhotos": "false"
        ///    }
        ///    
        /// RESPONSE
        /// {
        ///    "code": 200,
        ///    "status": "OK",
        ///    "data": {
        ///        "Members": [{
        ///            "memberId": 142528199,
        ///            "username": "HappyMimi",
        ///            "primaryPhoto": {
        ///                "memberPhotoId": 24659164,
        ///                "fullId": 175381820,
        ///                "thumbId": 175381823,
        ///                "fullPath": "http://s3.amazonaws.com/sparkmemberphotos/2014/10/29/13/175381820.jpg",
        ///                "thumbPath": "http://s3.amazonaws.com/sparkmemberphotos/2014/10/29/13/175381823.jpg",
        ///                "caption": "Somewhere in Echo Park (10/17)",
        ///                "listorder": 1,
        ///                "isCaptionApproved": true,
        ///                "isPhotoApproved": true,
        ///                "IsMain": true,
        ///                "IsApprovedForMain": true
        ///            },
        ///            "maritalStatus": "Separated",
        ///            "gender": "Female",
        ///            "seekingGender": "Male",
        ///            "lookingFor": ["A date"],
        ///            "age": 43,
        ///            "location": "Santa Monica, CA ",
        ///            "lastLoggedIn": "2014-10-30T01:23:04Z",
        ///            "lastUpdated": "2014-10-29T21:20:55.378578Z",
        ///            "isOnline": true,
        ///            "jDateEthnicity": null,
        ///            "registrationDate": "2014-10-29T04:33:57.66Z",
        ///            "passedFraudCheck": true,
        ///            "approvedPhotoCount": 10,
        ///            "zipcode": "90403",
        ///            "regionId": 3445894,
        ///            "heightCm": 157,
        ///            "selfSuspendedFlag": false,
        ///            "adminSuspendedFlag": false,
        ///            "blockContactByTarget": false,
        ///            "blockSearchByTarget": false,
        ///            "removeFromSearchByTarget": false,
        ///            "blockContact": false,
        ///            "blockSearch": false,
        ///            "removeFromSearch": false,
        ///            "isHighlighted": false,
        ///            "isSpotlighted": false,
        ///            "DefaultPhoto": {
        ///                "memberPhotoId": 0,
        ///                "fullId": 175381820,
        ///                "thumbId": 175381823,
        ///                "fullPath": "http://s3.amazonaws.com/sparkmemberphotos/2014/10/29/13/175381820.jpg",
        ///                "thumbPath": "http://s3.amazonaws.com/sparkmemberphotos/2014/10/29/13/175381823.jpg",
        ///                "caption": "Somewhere in Echo Park (10/17)",
        ///                "listorder": 0,
        ///                "isCaptionApproved": true,
        ///                "isPhotoApproved": true,
        ///                "IsMain": false,
        ///                "IsApprovedForMain": false
        ///            },
        ///            "PhotoUrl": "http://s3.amazonaws.com/sparkmemberphotos/2014/10/29/13/175381820.jpg",
        ///            "ThumbnailUrl": "http://s3.amazonaws.com/sparkmemberphotos/2014/10/29/13/175381823.jpg"
        ///        }],
        ///        "MatchesFound": 360
        ///    }
        ///}
        ///</example>
        /// <param name="PageSize" required="true" values="1+"></param>
        /// <param name="PageNumber" required="true" values="1+">The page number.  You may use ListPosition instead of page number, but one of these are required.</param>
        /// <param name="ListPosition" values="1+">The list position to start getting back results, so list position 1 means start getting items at result item 1.  This can be used instead of page number.</param>
        /// <param name="Keywords" values="string of up to 200 characters"></param>
        /// <param name="GenderMask" required="true" values="9: male seeking female[br]5: male seeking male[br]6: female seeking male[br]10: female seeking female">Mask value of gender and seeking gender</param>
        /// <param name="MinAge" values="18 to 99">Minimum age</param>
        /// <param name="MaxAge" values="18 to 99">Maximum age</param>
        /// <param name="RegionID">The regionID representing the location</param>
        /// <param name="MaxDistance" required="true">Distance in miles</param>
        /// <param name="MemberID">Not used</param>
        /// <param name="SiteID">Not used</param>
        /// <param name="BrandID">Not used</param>
        /// <param name="Brand">Not used</param>
        /// <paramsAdditionalInfo></paramsAdditionalInfo>
        /// <responseAdditionalInfo>A JSON with an array of member results and a total results available number</responseAdditionalInfo>
        [ApiMethod(ResponseType = typeof (SearchResults))]
        [HttpPost]
        [RequireClientCredentials]
        [RequireTrustedClientIP(TrustedIpList = "~/Authorization/XML/VisitorTrustedClientIPWhitelist.xml")]
        public ActionResult GetVisitorSearchResultsViaPost(VisitorSearchResultsRequest searchRequest)
        {
            var searchResults = ReadonlySearch.Instance.GetVisitorSearchResults(searchRequest.Brand,
                                                                                searchRequest.GenderMask,
                                                                                searchRequest.MinAge,
                                                                                searchRequest.MaxAge,
                                                                                searchRequest.RegionID,
                                                                                searchRequest.MaxDistance,
                                                                                searchRequest.ShowOnlyJewishMembers,
                                                                                searchRequest.ShowOnlyMembersWithPhotos,
                                                                                searchRequest.PageSize,
                                                                                searchRequest.PageNumber,
                                                                                searchRequest.ListPosition);
            return new NewtonsoftJsonResult {Result = searchResults};
        }
    }
}