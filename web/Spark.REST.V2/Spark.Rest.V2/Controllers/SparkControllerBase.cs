#region

using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Configuration;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Rest.Helpers;
using Spark.Rest.Serialization;
using System;
using Spark.Rest.V2.Serialization;

#endregion

namespace Spark.REST.Controllers
{
    public class SparkControllerBase : Controller
    {
        private ISettingsSA _settingsService;

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    _settingsService = RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        protected int GetAppId
        {
            get
            {
                if (HttpContext.Items.Contains("tokenAppId"))
                    return (int)HttpContext.Items["tokenAppId"];
                return 0;
            }
        }

        public EmptyResult Ok()
        {
            return new EmptyResult();
        }

        public HttpException NotFound()
        {
            return new HttpException(404, "Resource not found");
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            if (filterContext.HttpContext.Response.StatusCode == (int) HttpStatusCode.OK)
            {
                object result = filterContext.Result;
                bool ignoreNotImplementedException = false;
                if (result is NewtonsoftJsonResult)
                {
                    ignoreNotImplementedException = ((NewtonsoftJsonResult) result).IgnoreNotImplementedException;
                    result = ((NewtonsoftJsonResult) result).Result;
                }
                else if (result is JsonResult)
                {
                    result = ((JsonResult) result).Data;
                }
                else if (result is ContentResult)
                {
                    result = ((ContentResult) result).Content;
                }

                //don't wrap status code results in success response
                //don't wrap json string results in success response, it should be raw json
                if (!(result is HttpStatusCodeResult)
                    && !(filterContext.Result is JsonStringResult)) 
                {
                    filterContext.Result = new NewtonsoftJsonResult
                    {
                        Result =
                            new
                            {
                                code = (int) HttpStatusCode.OK,
                                status = HttpStatusCode.OK.ToString(),
                                data = result
                            },
                        IgnoreNotImplementedException =  ignoreNotImplementedException
                    };
                }
            }
        }

        protected int GetMemberId()
        {
            var memberId = 0;
            //if (memberId > 0) return memberId; // this can never happen
            if (HttpContext.Items.Contains("tokenMemberId"))
                memberId = (int) HttpContext.Items["tokenMemberId"];

            return memberId;
        }

        public Dictionary<string, string> GetCustomDataForReportingErrors()
        {
            return ErrorHelper.GetCustomData();
        }
    }
}