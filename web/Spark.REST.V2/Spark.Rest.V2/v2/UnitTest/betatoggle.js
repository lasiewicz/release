﻿function ajaxHelper(requestUrl, httpVerbType, requestData, customHeaders, expectedStatusCode) {
    return $.ajax({
        url: requestUrl,
        data: JSON.stringify(requestData),
        type: httpVerbType,
        headers: customHeaders,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        timeOut: 15000 // tried 1 sec, too short for some tests
    }).complete(function (e) {
        var statusCodeToMatch = "200"; // most tests are looking for 200 OK
        if (expectedStatusCode != null && !isNaN(parseInt(expectedStatusCode))) statusCodeToMatch = expectedStatusCode;
        if (e.status != statusCodeToMatch) {
            $("#confirm").text("Error: Received Status code: " + e.status + ", Error Code:" + e.responseJSON.error.code + ", Error Message:'" + e.responseJSON.error.message + "'");
        }
    });
};

function adjustBetaRedesignSetting(isBeta, brandId) {
    var accessToken;
    var memberId;
    var loginUrl = _apiUrl + "/brandId/" + _brandId + "/oauth2/accesstokenwithloginfraud/application/" + _appId + "?client_secret=" + _clientSecret;
    var customHeaders = { Accept: "application/json; version=V2.1" };
    var requestData = { "email": $("#emailAddr").val(), "password": $("#pass").val(), "ipaddress": "192.168.1.103" };
    $("#confirm").html("<img src='http://www.spark.com/img/Community/Spark/ajax-loader.gif'/>");
    ajaxHelper(loginUrl, 'POST', requestData, customHeaders).success(function (data) {
        accessToken = data.data.AccessToken;
        memberId = data.data.MemberId;
        var updateUrl = _apiUrl + "/brandId/" + brandId + "/profile/attributes/" + "?access_token=" + accessToken;
        var attributesMultiValueJson = {};
        var attributesJson = { REDRedesignBetaParticipatingFlag: isBeta, REDRedesignBetaOfferedFlag: true };
        attributesMultiValueJson = JSON.stringify(attributesMultiValueJson);
        attributesJson = JSON.stringify(attributesJson);
        var requestData2 = {
            AttributesMultiValueJson: attributesMultiValueJson,
            AttributesJson: attributesJson
        };
        ajaxHelper(updateUrl, 'PUT', requestData2, customHeaders).success(function (data2) {
            if (data2.status == 'OK') $("#confirm").text("IsBeta has been set to " + isBeta + " for memberId " + memberId);
        });
    });
}

$(document).ready(function () {
    $("#submitbeta").click(function () {
        var betaVal = ($("#betaval").val() == "True");
        $("#confirm").text("");
        adjustBetaRedesignSetting(betaVal, _brandId);
    });
});
