﻿var _statusCode;
var _mainApiUrl = _apiUrl + "/brandId/" + _brandId;



module("GetAccessToken");

asyncTest("[post CreateAccessTokenPassword] for MemberA", function () {
    var url = _apiUrl + "/brandId/" + _brandId + "/oauth2/accesstokenwithloginfraud/application/" + _appId + "?client_secret=" + _clientSecret;
    var requestData = { "email": _emailAddress, "password": _password, "ipaddress": "192.168.1.105" };
    ajaxHelper(url, 'POST', requestData, null).success(function (data) {
        sessionStorage.setItem("AccessToken", data.data.AccessToken);
        ok((data.data.AccessToken != null), "retrieved an access token for " + _emailAddress + " access token:" + sessionStorage.getItem("AccessToken"));
    });
});

module("MIGRATION HotList");

asyncTest("[MIGRATION - Add to DEFAULT hotlist]", 1, function () {
    var url = _mainApiUrl +"/minglemigration/hotlist/0";
    var requestData = { "memberId": 2288145827, "targetMemberId": 500000, "method": "add", "comment": "This is a comment", "timestamp": "2015-04-23T18:25:43.511Z", "brandId": _cmBrandId, "client_secret": _cmClientSecret, "applicationId": _cmAppId};
    ajaxHelper(url, 'POST', requestData, null);
});


//{
//    module("HotList");

//    asyncTest("[get hotlist counts for MemberA]", 2, function () {
//        var url = _mainApiUrl + "/hotlist/counts?access_token=" + sessionStorage.getItem("AccessToken");
//        var customHeader = { 'accept': 'application/json; version=V2.1' };
//        ajaxHelper(url, 'GET', null, customHeader).success(function (data) {
//            ok(data.data.TeasedCombinedTotal > 0, "retrieved TeasedCombinedTotal counts:" + data.data.TeasedCombinedTotal);
//        });
//    });

//    asyncTest("[get hotlists for MemberA]", 2, function () {
//        var url = _mainApiUrl + "/hotlist/default/pagesize/5/pagenumber/1?access_token=" + sessionStorage.getItem("AccessToken");
//        var customHeader = { 'accept': 'application/json; version=V2.1' };
//        ajaxHelper(url, 'GET', null, customHeader).success(function (data) {
//            ok(data.data.length > 0, "retrieved hotlist. count:" + data.data.length);
//        });
//    });

//    asyncTest("[get FavoritesCombined custom hotlist for MemberA]", 2, function () {
//        var url = _mainApiUrl + "/hotlist/FavoritesCombined/pagesize/10/pagenumber/2?access_token=" + sessionStorage.getItem("AccessToken");
//        var customHeader = { 'accept': 'application/json; version=V2.1' };
//        ajaxHelper(url, 'GET', null, customHeader).success(function (data) {
//            ok(data.data.length > 0, "retrieved hotlist. count:" + data.data.length);
//        });
//    });

//    asyncTest("[post delete to hotlist]", 1, function () {
//        var url = _mainApiUrl + "/hotlist/default/targetmemberid/" + _targetMemberId + "?access_token=" + sessionStorage.getItem("AccessToken");
//        var requestData = { "comment": "qunit test comment", "method": "delete" };
//        ajaxHelper(url, 'POST', requestData, null);
//    });

//    asyncTest("[post add to hotlist]", 1, function () {
//        var url = _mainApiUrl + "/hotlist/default/targetmemberid/" + _targetMemberId + "?access_token=" + sessionStorage.getItem("AccessToken");
//        var requestData = { "comment": "qunit test comment", "method": "add" };
//        ajaxHelper(url, 'POST', requestData, null);
//    });
//}
//{
//    module("HotList - YNM");

//    asyncTest("[post SaveYNMVote] vote yes on MemberB for MemberA", function () {
//        var url = _apiUrl + "/brandId/" + _brandId + "/hotlist/ynmvote/?access_token=" + sessionStorage.getItem("AccessToken");
//        var requestData = { "Yes": true, "AddMemberId": _targetMemberId };
//        ajaxHelper(url, 'POST', requestData, null).success(function (data) {
//            ok(true, "voted yes on MemberB for MemberA.");
//        });
//    });

//    asyncTest("[get GetYNMVote] retrieve ynm vote on MemberB for MemberA", function () {
//        var url = _apiUrl + "/brandId/" + _brandId + "/hotlist/ynmvote/targetMemberId/" + _targetMemberId + "/?access_token=" + sessionStorage.getItem("AccessToken");
//        ajaxHelper(url, 'GET', null, null).success(function (data) {
//            ok((data.data.Yes = true), "retrieved ynm vote on MemberB for MemberA. yes:" + data.data.Yes + " no:" + data.data.No + " maybe:" + data.data.Maybe);
//        });
//    });

//}
////#region List


function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    return $('<div/>').text(value).html();
}

// todo: better to use always, done, and error. 
// success, complete and fail are essentially deprecated according to jquery docs.
function ajaxHelper(requestUrl, httpVerbType, requestData, customHeaders, doNotTriggerStart, expectedStatusCode) {
    return $.ajax({
        url: requestUrl,
        data: JSON.stringify(requestData),
        type: httpVerbType,
        headers: customHeaders,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        timeOut: 5000 // tried 1 sec, too short for some tests
    }).complete(function (e, textStatus) {
        var statusCodeToMatch = "200"; // most tests are looking for 200 OK
        if (expectedStatusCode != null && !isNaN(parseInt(expectedStatusCode))) statusCodeToMatch = expectedStatusCode;
        ok(e.status == statusCodeToMatch, "received status code: " + e.status + " " + textStatus);
    }).always(function () {
        if (!doNotTriggerStart) start();
    });
};