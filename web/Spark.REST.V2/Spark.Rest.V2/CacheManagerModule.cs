using System;
using System.Web;
using Spark.Logger;

namespace Spark.REST
{
    /// <summary>
    /// This is soley to support Bedorck MT services to expire cached items.
    /// </summary>
    public class CacheManagerModule : IHttpModule
	{

        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(CacheManagerModule));

        #region IHttpModule Members

        void IHttpModule.Dispose()
        {
            
        }

        void IHttpModule.Init(HttpApplication context)
        {
            context.BeginRequest += ApplicationBeginRequest;
        }

        private static void ApplicationBeginRequest(object sender, EventArgs e)
        {
            HttpContext context = ((HttpApplication)sender).Context;

            if (context.Request.Path.ToLower() != "/cachemanager.aspx") return;

            var key = context.Request.QueryString["DeleteKey"];

            if (key == null) return;

            Matchnet.Caching.Cache.Instance.Remove(key);

            //This comment gets too noisy in the log.
            //Log.DebugFormat("Cache Removed: {0}", key);

            context.ApplicationInstance.CompleteRequest();

            context.Response.End();

        }

        #endregion
    }
}