﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Text;
using System.Web;

namespace Spark.Rest.V2.Extensions
{
    public static class HttpRequestExtensions
    {
        /// <summary>
        /// Get all the request headers concatinated into a string.
        /// </summary>
        /// <param name="request">The HttpRequestMessage</param>
        /// <returns>All the request headers concatinated into a string.</returns>
        public static string GetRequestHeadersAsString(this HttpRequestBase request)
        {
            var builder = new StringBuilder();

            foreach (var key in request.Headers.Keys)
            {
                builder.AppendFormat("{0}={1};", key, request.Headers.Get(key.ToString()));
            }

            return builder.ToString();
        }
    }
}