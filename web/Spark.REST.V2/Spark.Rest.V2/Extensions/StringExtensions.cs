﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Extensions
{
    public static class StringExtensions
    {
        private const string AlphaNumericRegex = "^[a-zA-Z0-9א-ת]*$";
        private const string EmailRegex = "^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$";

        /// <summary>
        /// Check if a string is alpha numeric
        /// </summary>
        /// <param name="value">The string value</param>
        /// <returns>True if the string is alpha numeric</returns>
        public static bool IsAlphaNumeric(this string value)
        {
            return !string.IsNullOrWhiteSpace(value) && System.Text.RegularExpressions.Regex.IsMatch(value, AlphaNumericRegex);
        }

        /// <summary>
        /// Check if a string is of a minimum lentgh
        /// </summary>
        /// <param name="value">The string to check</param>
        /// <param name="minLength">The minimum length</param>
        /// <returns>True if a string is of a minimum lentgh</returns>
        public static bool IsMinLength(this string value, int minLength)
        {
            return !string.IsNullOrWhiteSpace(value) && value.Length >= minLength;
        }

        /// <summary>
        /// Check if a string is a valid email address.
        /// </summary>
        /// <param name="value">The string to check.</param>
        /// <returns>True if a string is a valid email address.</returns>
        public static bool IsValidEmailAddress(this string value)
        {
            return !string.IsNullOrWhiteSpace(value) && System.Text.RegularExpressions.Regex.IsMatch(value, EmailRegex);
        }
    }
}