﻿using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;
using Spark.Common.AccessService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Helpers
{
    internal static class PremiumHelper
    {
        [Flags]
        public enum MatchMeterFlags : int
        {
            DEFAULT_NoChange = 0,
            EnableApp = 1,
            ShowBestMatches = 2,
            PayFor1On1 = 4,
            PayForBestMatches = 8,
            PremiumFor1On1 = 16,
            PremiumForBestMatches = 32
        }

        public static bool IsJMeterEnabledAsPremium(Brand brand)
        {
            int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", brand.Site.Community.CommunityID,
                                                                  brand.Site.SiteID, brand.BrandID));

            bool isEnabled = (mmsett & ((int)MatchMeterFlags.EnableApp)) == ((int)MatchMeterFlags.EnableApp)
                            &&
                            ((mmsett & ((int)MatchMeterFlags.PremiumForBestMatches)) == ((int)MatchMeterFlags.PremiumForBestMatches)
                                || (mmsett & ((int)MatchMeterFlags.PremiumFor1On1)) == ((int)MatchMeterFlags.PremiumFor1On1));

            return isEnabled;
        }

        public static bool IsReadRecieptPremiumFeatureEnabled(Brand brand)
        {
            return Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_READ_RECEIPTS_PREMIUM_SERVICE_FEATURE", brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID));
        }

        public static bool IsColorCodeEnabled(Brand brand)
        {
            return Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_COLORCODE", brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID));
        }

        public static bool IsVIPEnabledSite(Brand brand)
        {
            return Convert.ToBoolean(RuntimeSettings.GetSetting("VIP_EMAIL_ENABLED", brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID));
        }

        public static bool IsVIPSendEnabledSite(Brand brand)
        {
            return Convert.ToBoolean(RuntimeSettings.GetSetting("VIP_EMAIL_SEND_ENABLED", brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID));
        }

        internal static bool HasAnyPremiumServices(Brand brand, Member member)
        {
            try
            {
                ArrayList arrPremiumServices = PremiumServiceSA.Instance.GetMemberActivePremiumServices(member, brand);
                if (arrPremiumServices.Count > 1)
                {
                    return true;
                }
                else if (arrPremiumServices.Count == 1 && arrPremiumServices[0] != Spark.Common.AccessService.PrivilegeType.AllAccessEmails.ToString())
                {
                    //all access email by itself should not be considered having premium, since you can't use it without all access privilege
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        internal static bool HasPremiumServiceLeftToPurchase(Brand brand, Member member)
        {
            bool memberHasPremiuimServiceToPurchase = false;

            if (member != null)
            {
                bool hasHighlightedProfile = false;
                bool hadSpotlightMember = false;
                bool hasJMeter = false;
                bool hasReadReceipts = false;
                bool hasAllAccess = false;

                var apHighlightedProfile = member.GetUnifiedAccessPrivilege(PrivilegeType.HighlightedProfile, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (apHighlightedProfile != null && apHighlightedProfile.EndDatePST > DateTime.Now)
                {
                    hasHighlightedProfile = true;
                }

                var apSpotlightMember = member.GetUnifiedAccessPrivilege(PrivilegeType.SpotlightMember, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (apSpotlightMember != null && apSpotlightMember.EndDatePST > DateTime.Now)
                {
                    hadSpotlightMember = true;
                }

                if (IsJMeterEnabledAsPremium(brand))
                {
                    var apJMeter = member.GetUnifiedAccessPrivilege(PrivilegeType.JMeter, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                    if (apJMeter != null && apJMeter.EndDatePST > DateTime.Now)
                    {
                        hasJMeter = true;
                    }
                }
                else
                {
                    hasJMeter = true; //free or not enabled
                }

                if (IsReadRecieptPremiumFeatureEnabled(brand))
                {
                    var apReadReceipt = member.GetUnifiedAccessPrivilege(PrivilegeType.ReadReceipt, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                    if (apReadReceipt != null && apReadReceipt.EndDatePST > DateTime.Now)
                    {
                        hasReadReceipts = true;
                    }
                }
                else
                {
                    hasReadReceipts = true; //free
                }

                if (IsVIPSendEnabledSite(brand))
                {
                    var apAllAccess = member.GetUnifiedAccessPrivilege(PrivilegeType.AllAccess, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                    if (apAllAccess != null && apAllAccess.EndDatePST > DateTime.Now)
                    {
                        hasAllAccess = true;
                    }
                }
                else
                {
                    hasAllAccess = true; //not enabled
                }

                if (!hasHighlightedProfile || !hadSpotlightMember || !hasJMeter || !hasAllAccess || !hasReadReceipts)
                {
                    memberHasPremiuimServiceToPurchase = true;
                }
            }

            return memberHasPremiuimServiceToPurchase;
        }
    }
}