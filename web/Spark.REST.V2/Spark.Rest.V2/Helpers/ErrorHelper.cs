﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Logger;
using Spark.Rest.Serialization;
using Matchnet.Helpers;

namespace Spark.Rest.Helpers
{
    internal static class ErrorHelper
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(ErrorHelper));        
        private static ISettingsSA _settingsService = null;
        private static IHttpContextHelper _httpContextHelper = null;

        public static ISettingsSA SettingsService
        {
            get { return _settingsService ?? RuntimeSettings.Instance; }
            set { _settingsService = value; }
        }

        public static IHttpContextHelper HttpContextHelper
        {
            get { return _httpContextHelper ?? (_httpContextHelper = new HttpContextHelper()); }
            set { _httpContextHelper = value; }
        }

        internal static void OnException(ExceptionContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            if (context.ExceptionHandled)
            {
                return;
            }

            var guidId = Convert.ToString(Guid.NewGuid());
            LogException(context.Exception, guidId);
            var returnMessage = "Sorry, an error occurred while processing your request.";
            string exceptionMessage = context.Exception.ToString();
#if DEBUG
            returnMessage = exceptionMessage;
#endif
            context.Result = new NewtonsoftJsonResult
                                 {
                                     Result = new JsonError
                                                  {
                                                      Message = returnMessage,
                                                      UniqueId = guidId
                                                  }
                                 };
            context.ExceptionHandled = true;
            context.RequestContext.HttpContext.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
        }

        public static void LogException(Exception exception, string guidId)
        {
            Log.LogError(String.Format("GuidId: {0} Exception Message: {1}", guidId, exception),exception,null);
        }

        public static Dictionary<string, string> GetCustomData()
        {
            try
            {
                return GetCustomData(HttpContextHelper.CurrentHttpContextBase);
            }
            catch(Exception ex)
            {
                Log.LogError("GetCustomData error", ex, null);
            }

            return null;
        }
     
        public static Dictionary<string, string> GetCustomData(HttpContextBase contextBase)
        {
            var appID = contextBase.Items["tokenAppId"] ?? 0;
            var memberID = contextBase.Items["tokenMemberId"] ?? 0;
            var environment = ConfigurationManager.AppSettings["Environment"] ?? "dev";
            var brandID = 0;
            var path = String.Empty;
            if (null != contextBase.Request.RequestContext)
            {
                var routeData = contextBase.Request.RequestContext.RouteData;

                if (routeData != null && routeData.Values["brandId"] != null)
                {
                    brandID = Convert.ToInt32(routeData.Values["brandId"]);
                }

                if (contextBase.Request.RequestContext.HttpContext != null &&
                    contextBase.Request.RequestContext.HttpContext.Request != null &&
                    contextBase.Request.RequestContext.HttpContext.Request.Path != null)
                    path = contextBase.Request.RequestContext.HttpContext.Request.Path;
            }
            var emailAddress = contextBase.Request["Email"] != null
                ? Convert.ToString(contextBase.Request["Email"])
                : String.Empty;
            if (String.IsNullOrEmpty(emailAddress))
            {
                emailAddress = (contextBase.Request.Params != null && contextBase.Request.Params["Email"] != null)
                    ? Convert.ToString(contextBase.Request.Params["Email"])
                    : String.Empty;
            }

            //Must pass custom data in
            var customData = new Dictionary<string, string>
            {
                {"AppID", appID.ToString()},
                {"MemberID", memberID.ToString()},
                {"BrandID", brandID.ToString(CultureInfo.InvariantCulture)},
                {"Email", emailAddress.ToString(CultureInfo.InvariantCulture)},
                {"Environment", environment},
                {"Path", path}
            };

            return customData;
        }

        public static bool ShouldLogExternally()
        {
            return SettingsService.SettingExistsFromSingleton("ENABLE_RAYGUNIO", 0, 0) &&
                   Convert.ToBoolean(SettingsService.GetSettingFromSingleton("ENABLE_RAYGUNIO"));
        }

        public static bool ShouldLogExternally(Brand b)
        {
            int communityId = b.Site.Community.CommunityID;
            int siteId = b.Site.SiteID;
            int brandId = b.BrandID;
            return SettingsService.SettingExistsFromSingleton("ENABLE_RAYGUNIO", communityId, siteId) &&
                   Convert.ToBoolean(SettingsService.GetSettingFromSingleton("ENABLE_RAYGUNIO", communityId, siteId, brandId));

        }
    }

    /// <summary>
    /// </summary>
    [Serializable]
    public class JsonError
    {
        public string Message { get; set; }
        public string UniqueId { get; set; }
    }
}
