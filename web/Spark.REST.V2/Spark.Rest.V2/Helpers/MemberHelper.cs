﻿#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.MatchTest.ValueObjects.MatchMeter;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Enumerations;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Search.Interfaces;
using Spark.CloudStorage;
using Spark.Common.AccessService;
using Spark.Common.RenewalService;
using Spark.Logger;
using Spark.Rest.BedrockPorts;
using Spark.Rest.Helpers;
using Spark.Rest.V2.BedrockPorts;
using Spark.Rest.V2.DataAccess.Premium;
using Spark.Rest.V2.Entities.Premium;

#endregion

namespace Spark.REST.Helpers
{
#pragma warning disable 1591
    public enum PhotoType
    {
        Full = 1,
        Thumbnail
    }
#pragma warning restore 1591

    internal static class MemberHelper
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MemberHelper));
        private static readonly Regex IpAddressRegex = new Regex(@"\A(?:\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b)(:[1-9][0-9]{0,4})?\z", RegexOptions.Compiled);

        internal static string CompleteRegistration(Brand brand, int memberId, string iovationBlackBox)
        {
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);

            var passed = PassedFraud(member, brand, iovationBlackBox);

            var status = !passed ? "Fradulent" : "Success";

            Log.LogDebugMessage(string.Format("Registration fraud check result:{0} for memberId: {1}", status, memberId), ErrorHelper.GetCustomData());

            if (!passed)
            {
                //suspend user
                var globalStatusMask = (GlobalStatusMask) member.GetAttributeInt(brand, "GlobalStatusMask");
                globalStatusMask = globalStatusMask | GlobalStatusMask.AdminSuspended;
                member.SetAttributeInt(brand, "GlobalStatusMask", (Int32) globalStatusMask);
                member.SetAttributeInt(brand, "LastSuspendedAdminReasonID", (Int32) AdminActionReasonID.ROLF);

                MemberSA.Instance.SaveMember(member);

                //log the action in the admin log
                AdminSA.Instance.AdminActionLogInsert(member.MemberID,
                    Constants.GROUP_PERSONALS
                    , (Int32) AdminAction.AdminSuspendMember
                    , -1
                    , AdminMemberIDType.AdminProfileMemberID
                    , AdminActionReasonID.ROLF);

                //kill all traces of user session
                MembersOnlineSA.Instance.Remove(brand.Site.Community.CommunityID, member.MemberID);

                // Members are now deleted from cache if they log off to allow for synchronization
                // with outside partners such as Mingle that use the SparkWS.
                MemberSA.Instance.ExpireCachedMember(member.MemberID);

                Log.LogDebugMessage(string.Format("Registration Completed admin action log and killing session for memberId: {0}",memberId), ErrorHelper.GetCustomData());
            }

            //if member did not previously belong to a community make it so now
            var brandLogonCount = member.GetAttributeInt(brand, "BrandLogonCount", 0);
            if (brandLogonCount == 0)
            {
                var now = DateTime.Now;
                member.SetAttributeDate(brand, "BrandInsertDate", now);
                //this attribute is immutable so it is safe to attempt updates all day
                member.SetAttributeDate(brand, "BrandLastLogonDate", now);
                member.SetAttributeInt(brand, "LastBrandID", brand.BrandID);
                member.SetAttributeInt(brand, "BrandLogonCount", brandLogonCount + 1);
            }

            // TODO Refactor this call into a DAL method.
            member.SetAttributeDate(brand, "LastUpdated", DateTime.Now);

            MemberSA.Instance.SaveMember(member);

            Log.LogDebugMessage(string.Format("Registration Completed saving member memberId: {0}", memberId), ErrorHelper.GetCustomData());

            return status;
        }

        internal static string GetRegisterResultResourceConstant(RegisterStatusType statusType)
        {
            string errorResourceConstant;

            switch (statusType)
            {
                case RegisterStatusType.AlreadyRegistered:
                    errorResourceConstant = "ALREADY_REGISTERED";
                    break;

                case RegisterStatusType.EmailAddressBlocked:
                    errorResourceConstant = "EMAIL_ADDRESS_ON_DNE";
                    break;

                case RegisterStatusType.EmailAddressNotAllowed:
                    errorResourceConstant = "EMAIL_ADDRESS_NOT_ALLOWED";
                    break;

                case RegisterStatusType.InvalidEmail:
                    errorResourceConstant = "INVALIDEMAIL";
                    break;

                case RegisterStatusType.IPBlocked:
                case RegisterStatusType.InvalidAttributes:
                case RegisterStatusType.FailedFraud:
                case RegisterStatusType.PasswordLengthExceeded:
                    errorResourceConstant = string.Empty;
                    break;

                default:
                    throw new ArgumentOutOfRangeException("statusType",
                        "Unknown RegisterStatusType value: " + statusType);
            }

            return errorResourceConstant;
        }

        internal static bool RejectForBeingOnDne(string emailAddress, int siteId)
        {
            var reject = false;

            var entry = DoNotEmailSA.Instance.GetEntryBySiteAndEmailAddress(siteId, emailAddress);
            if (entry != null)
            {
                reject = true;
            }

            return reject;
        }

        // Ported from full site
        internal static bool PassedFraud(Member member, Brand brand, string iovationBlackBox)
        {
            var rolfClient = new MingleROLFClient(brand, member.MemberID, member.EmailAddress)
            {
                IOBlackBoxValue = iovationBlackBox,
                RegIP = member.GetAttributeInt(brand, "RegistrationIP"),
                GenderMask = member.GetAttributeInt(brand, "GenderMask"),
                FirstName = member.GetAttributeText(brand, "SiteFirstName"),
                LastName = member.GetAttributeText(brand, "SiteLastName"),
                MaritalStatus =
                    GetMaritalStatusDescription(member.GetAttributeInt(brand, "MaritalStatus")),
                Ethnicity =
                    GetDescriptions("JDateEthnicity",
                        member.GetAttributeInt(brand, "JDateEthnicity"),
                        brand),
                Religion =
                    GetDescription("JDateReligion", member.GetAttributeInt(brand, "JDateReligion"),
                        brand)
            };
            // TODO need to support other mos sites.
            //if (brand.Site.Community.CommunityID == (int)WebConstants.Communities.JDate)
            //{
            //}
            //else
            //{
            //    rolfClient.Ethnicity = Option.GetDescriptions("Ethnicity", member.GetAttributeInt(brand, "Ethnicity"), g);
            //    rolfClient.Religion = Option.GetDescription("Religion", member.GetAttributeInt(brand, "Religion"), g);
            //}

            if (
                Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_COLORCODE", brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID)))
            {
                var memberColorCodeVal = member.GetAttributeText(brand, "ColorCodeQuizAnswers");
                rolfClient.ColorCode = memberColorCodeVal == "0" ? string.Empty : memberColorCodeVal;
            }

            var region = RegionSA.Instance.RetrievePopulatedHierarchy(member.GetAttributeInt(brand, "RegionID"),
                brand.Site.LanguageID);
            var postalRegion = RegionSA.Instance.RetrieveRegionByID(region.PostalCodeRegionID, brand.Site.LanguageID);
            var countryRegion = RegionSA.Instance.RetrieveRegionByID(region.CountryRegionID, brand.Site.LanguageID);

            rolfClient.PostalCode = postalRegion.Description;
            rolfClient.Country = countryRegion.Description;

            rolfClient.LGID = member.GetAttributeText(brand, "Luggage", "");
            rolfClient.PRM = (member.GetAttributeInt(brand, "PromotionID", Constants.NULL_INT) == Constants.NULL_INT)
                ? ""
                : Convert.ToString(member.GetAttributeInt(brand, "PromotionID"));

            var response = rolfClient.GetROLFResponse();

            member.SetAttributeInt(brand, "ROLFScore", response.Code);
            member.SetAttributeText(brand, "ROLFScoreDescription", response.Description, TextStatusType.Auto);

            MemberSA.Instance.SaveMember(member);

            return response.Passed;
        }

        private static string GetMaritalStatusDescription(int optionID)
        {
            if (optionID == Constants.NULL_INT) return string.Empty;

            var options = AttributeOptionSA.Instance.GetAttributeOptionCollection(
                "MaritalStatus", 8383);

            if ((from AttributeOption o in options where o.AttributeOptionID == optionID select o).ToList().Count == 0)
                return string.Empty;

            var description =
                (from AttributeOption o in options where o.AttributeOptionID == optionID select o).First().Description;

            return description;
        }

        public static List<string> GetDescriptions(string attributeName, int attributeMaskValue, Brand brand)
        {
            var descriptions = new List<string>();

            var attributeOptionCollection =
                AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return descriptions;

            foreach (AttributeOption attributeOption in attributeOptionCollection)
            {
                if (attributeOption.MaskContains(attributeMaskValue))
                {
                    descriptions.Add(attributeOption.Description);
                }
            }

            return descriptions;
        }

        public static string GetDescription(string attributeName, int attributeValue, Brand brand)
        {
            var attributeOptionCollection =
                AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            var attributeOption = attributeOptionCollection[attributeValue];

            if (attributeOption != null)
                return attributeOption.Description;

            return string.Empty;
        }

        /// <summary>
        /// todo: refactor with memberphotohelper.cs getphotourl method
        /// </summary>
        /// <param name="photo"></param>
        /// <param name="photoType"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public static string GetPhotoUrl(Photo photo, PhotoType photoType, Brand brand)
        {
            if (photo == null)
                throw new ArgumentNullException("photo");

            var memberPhotoUrl = string.Empty;

            var showPhotosFromCloud =
                Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD"));

            if (showPhotosFromCloud)
            {
                switch (photoType)
                {
                    case PhotoType.Full:
                        if (photo.FileCloudPath != null)
                            memberPhotoUrl = photo.FileCloudPath;
                        break;
                    case PhotoType.Thumbnail:
                        if (photo.ThumbFileCloudPath != null)
                            memberPhotoUrl = photo.ThumbFileCloudPath;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("photoType", photoType, null);
                }
            }
            else
            {
                switch (photoType)
                {
                    case PhotoType.Full:
                        if (photo.FileWebPath != null)
                            memberPhotoUrl = photo.FileWebPath;
                        break;
                    case PhotoType.Thumbnail:
                        if (photo.ThumbFileWebPath != null)
                            memberPhotoUrl = photo.ThumbFileWebPath;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("photoType", photoType, null);
                }
            }

            // We're expected to get some empty cloud paths until the s3 is completely done (even after the switch is on)
            if (string.IsNullOrEmpty(memberPhotoUrl))
            {
                // Return an empty url, each caller handles it.
                return memberPhotoUrl;
            }

            if (showPhotosFromCloud)
            {
                var cloudClient = new Client(brand.Site.Community.CommunityID, brand.Site.SiteID,
                    RuntimeSettings.Instance);

                memberPhotoUrl = cloudClient.GetFullCloudImagePath(memberPhotoUrl, FileType.MemberPhoto, false);
            }
            else
            {
                memberPhotoUrl = PrependFullUrl(brand, memberPhotoUrl);
            }

            return memberPhotoUrl;
        }

        /// <summary>
        /// This checks if photos are available at all - checks for guests, suspension, etc
        /// </summary>
        public static bool IsPhotosAvailable(int viewerId, Member member, Brand brand)
        {
            bool avail = true;

            if (avail)
            {
                //suspended members have photos hidden (moved to another bucket )and are not available, resulting in broken images
                int globalStatusMask = member.GetAttributeInt(brand, "GlobalStatusMask");
                if ((globalStatusMask & CachedMember.ATTRIBUTEOPTION_SUSPEND_ADMIN) == CachedMember.ATTRIBUTEOPTION_SUSPEND_ADMIN)
                {
                    avail = false;
                }
            }

            if (avail)
            {
                //self-suspended members have photos (moved to another bucket )and are not available, resulting in broken images
                int selfSuspendedFlag = member.GetAttributeInt(brand, "SelfSuspendedFlag");
                if (selfSuspendedFlag == 1)
                {
                    avail = false;
                }
            }

            if (avail)
            {
                avail = IsPhotosAvailableToGuests(viewerId, member, brand);
            }

            return avail;
        }

        /// <summary>
        /// Determines if only public photos should return.
        /// </summary>
        public static bool PublicPhotosOnly(Brand brand, int viewerId, Member member)
        {
            // if the site doesn't support it we can return right away with false here
            if (!brand.GetStatusMaskValue(Matchnet.Content.ValueObjects.BrandConfig.StatusType.PrivatePhotos))
                return false;

            // at this point, we just have to make sure the viewing member is not a guest
            if (viewerId <= 0)
                return true;

            return false;
        }

        public static bool IsPrivatePhoto(Matchnet.Member.ValueObjects.Photos.Photo photo, Brand brand)
        {
            return (photo.IsPrivate && brand.GetStatusMaskValue(Matchnet.Content.ValueObjects.BrandConfig.StatusType.PrivatePhotos));
        }

        /// <summary>
        /// This checks if photos are available to guests
        /// </summary>
        public static bool IsPhotosAvailableToGuests(int viewerId, Member member, Brand brand)
        {
            if (viewerId <= 0)
                return member.GetAttributeBool(brand, "DisplayPhotosToGuests");
            else
            {
                return true;
            }
        }

        private static string PrependFullUrl(Brand brand, string relativePath)
        {
            return "http://" + brand.Site.DefaultHost + "." + brand.Uri + relativePath;
        }

        public static bool IsMemberAdminSuspended(Brand brand, Member member)
        {
            //FIX for GlobalStatusMask:  If GlobalStatusMask is null int, assume member is NOT suspended. - 10/15/14 arod
            int globalStatusMaskAttrInt = member.GetAttributeInt(brand, AttributeConstants.GLOBALSTATUSMASK);
            var globalStatusMask = (GlobalStatusMask) globalStatusMaskAttrInt;
            return (globalStatusMaskAttrInt != Constants.NULL_INT) && ((globalStatusMask & GlobalStatusMask.AdminSuspended) == GlobalStatusMask.AdminSuspended);
        }

        public static bool IsMemberSelfSuspended(Brand brand, Member member)
        {
            return member.GetAttributeInt(brand, "SelfSuspendedFlag", 0) == 1;
        }

        public static bool IsMemberOfCurrentCommunity(Brand brand, Member member)
        {
            var communityIdList = member.GetCommunityIDList();
            return communityIdList.Any(communityId => communityId == brand.Site.Community.CommunityID);
        }

        /// <summary>
        ///     Returns true requesting member is blocked by requested member
        ///     There are 3 types of blocking a member can perform.
        ///     1. HotListCategory.ExcludeFromList - remove from my search
        ///     2. HotListCategory.IgnoreList - block from contacting me
        ///     3. HotListCategory.ExcludeList - block from searching me
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="hotListCategory"></param>
        /// <returns></returns>
        public static bool IsRequestedMemberBlocked(Brand brand, int memberId, int targetMemberId,
            Matchnet.List.ValueObjects.HotListCategory hotListCategory)
        {
            // you cannot block yourself
            if (memberId == targetMemberId) return false;

            // visitor, ignore 
            if (memberId == Constants.NULL_INT || targetMemberId == Constants.NULL_INT) return false;

            // is the feature on?
            var isProfileBlockingEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("PROFILE_BLOCK_ENABLED", brand.Site.Community.CommunityID));
            if (!isProfileBlockingEnabled) return false;

            return ListSA.Instance.GetList(targetMemberId).IsHotListed(hotListCategory, brand.Site.Community.CommunityID, memberId);
        }

        public static bool IsHighlighted(Brand brand, Member member)
        {
            var isHighlighted = false;
            if (member != null)
            {
                //check privilege and highlight setting
                var priv = member.GetUnifiedAccessPrivilege(PrivilegeType.HighlightedProfile, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (priv != null && priv.EndDateUTC >= DateTime.Now.ToUniversalTime())
                {
                    if (member.GetAttributeInt(brand, "HighlightedFlag", 0) == 1)
                        isHighlighted = true;
                    //JS-1329
                    Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDateUTC: {2}", member.MemberID, PrivilegeType.HighlightedProfile.ToString(), priv.EndDateUTC), ErrorHelper.GetCustomData());
                }
            }
            return isHighlighted;
        }

        public static bool IsSpotlighted(Brand brand, Member member)
        {
            var isSpotlighted = false;
            if (member != null)
            {
                //check privilege and spotlight setting
                var priv = member.GetUnifiedAccessPrivilege(Common.AccessService.PrivilegeType.SpotlightMember, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (priv != null && priv.EndDateUTC >= DateTime.Now.ToUniversalTime())
                {
                    SpotlightSettings settings = PremiumAccess.Instance.GetSpotlightSettings(brand, member.MemberID);
                    isSpotlighted = settings.EnableFlag;
                    //JS-1329
                    Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDateUTC: {2}", member.MemberID, PrivilegeType.SpotlightMember.ToString(), priv.EndDateUTC), ErrorHelper.GetCustomData());
                }
            }
            return isSpotlighted;
        }

        public static int GetMatchRating(Brand brand, int memberId, int targetMemberId)
        {
            return Matchnet.Search.ServiceAdapters.MatchRatingUtil.CalculateMatchPercentage(memberId, targetMemberId, brand.Site.Community.CommunityID, brand.Site.SiteID, SearchType.None).MatchScore;
        }

        public static DateTime GetBirthdate(Brand brand, Member member, int viewerMemberId, bool restrictToOwnProfile)
        {
            if (restrictToOwnProfile && (member.MemberID == viewerMemberId))
            {
                return member.GetAttributeDate(brand, "Birthdate");
            }
            return DateTime.MinValue;
        }

        public static bool IsValidBirthdate(DateTime birthdate)
        {
            if (isOverEighteen(birthdate) && isUnderNinetyNine(birthdate))
                return true;
            else
                return false;
        }

        public static bool isOverEighteen(DateTime birthdate)
        {
            int yearsLived = DateTime.Now.Year - birthdate.Year;
            if (yearsLived < 18)
            {
                return false;
            }
            else if (yearsLived == 18)
            {
                // if birth month is later in the year than now, then not 18 yet
                if (birthdate.Month > DateTime.Now.Month)
                {
                    return false;
                }
                else if (birthdate.Month == DateTime.Now.Month)
                {
                    // if birth day is later in the month than now, then not 18 yet
                    if (birthdate.Day > DateTime.Now.Day)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool isUnderNinetyNine(DateTime birthdate)
        {
            int yearsLived = DateTime.Now.Year - birthdate.Year;
            if (yearsLived > 99)
            {
                return false;
            }
            else if (yearsLived == 99)
            {
                // if birth month is earlier in the year than now, then already 99
                if (birthdate.Month < DateTime.Now.Month)
                {
                    return false;
                }
                else if (birthdate.Month == DateTime.Now.Month)
                {
                    // if birth day is earlier in the month than now, then already 99
                    if (birthdate.Day <= DateTime.Now.Day)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool IsMobileAppPayingMember(Brand brand, Member member, string access)
        {
            var isSub = false;

            var accessPrivilege = member.GetUnifiedAccessPrivilege(Common.AccessService.PrivilegeType.BasicSubscription, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

            if (accessPrivilege == null) return false;

            if (accessPrivilege.EndDatePST > DateTime.Now)
                isSub = true;
            //JS-1329
            Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDateUTC: {2}", member.MemberID, PrivilegeType.BasicSubscription.ToString(), accessPrivilege.EndDateUTC), ErrorHelper.GetCustomData());

            bool isIap;
            // wrap get attribute in try/catch block to deal with attribute group problems 
            try
            {
                isIap = !string.IsNullOrEmpty(member.GetAttributeText(brand, access));
            }
            catch (Exception e)
            {
                //TODO: Log this exception somewhere
                isIap = false;
            }

            return isIap && isSub;
        }


        public static bool IsMemberSubscribed(Brand brand, Member member)
        {
            int SubscribedMember = 1023;
            int memberPrivilege = GetMemberPrivilegeAtt(brand, member);
            if (SubscribedMember == (SubscribedMember & memberPrivilege))
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        private static string GetGamSubscriber(Brand brand, Member member)
        {
            var returnValue = string.Empty;

            if (IsMemberSubscribed(brand, member))
                returnValue = "subscriber";
            else
                returnValue = "user_state";

            return returnValue;

        }



        public static IEnumerable<string> GetGamLastLogonDate(Brand brand, Member member)
        {
            var returnValues = new List<string>();

            var daysSinceLastLogin = (DateTime.Now - member.GetAttributeDate(brand, "BrandLastLogonDateOldValue", DateTime.Now)).Days;
            returnValues.Add(daysSinceLastLogin.ToString());

            if (daysSinceLastLogin >= 0 && daysSinceLastLogin <= 30)
                returnValues.Add("0-30");
            else if (daysSinceLastLogin >= 31 && daysSinceLastLogin <= 60)
                returnValues.Add("31-60");
            else if (daysSinceLastLogin >= 61 && daysSinceLastLogin <= 90)
                returnValues.Add("61-90");
            else if (daysSinceLastLogin >= 91 && daysSinceLastLogin <= 180)
                returnValues.Add("91-180");
            else if (daysSinceLastLogin >= 181)
                returnValues.Add("181_plus");

            return returnValues;
        }



        public static object GetGamPrm(Brand brand, Member member, string referalUri)
        {
            var prmList = PromotionSA.Instance.GetPRMURLMapperList();
            var returnValue = "0";

            if (prmList != null && !string.IsNullOrEmpty(referalUri))
            {
                try
                {
                    var uri = new Uri(referalUri);

                    var prmUrlMapper = prmList.GetPRMByReferralURL(uri.Host);

                    if (prmUrlMapper != null)
                    {
                        returnValue = prmUrlMapper.PromotionID.ToString();
                    }
                }
                catch (Exception ex)
                {
                    Log.LogError("GetGamPrm: Invalid referalUri passed, defaulting to 0", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                }
            }

            return returnValue;
        }


        public static IEnumerable<string> GetGamDaysUntilRenew(Brand brand, Member member)
        {
            var results = new List<string>();

            var subscriptionExpirationDate = DateTime.MinValue;
            var basicSubscriptionPrivilege = member.GetUnifiedAccessPrivilege(Common.AccessService.PrivilegeType.BasicSubscription, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

            if (basicSubscriptionPrivilege != null)
            {
                subscriptionExpirationDate = basicSubscriptionPrivilege.EndDatePST;
                //JS-1329
                Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", member.MemberID, PrivilegeType.BasicSubscription.ToString(), basicSubscriptionPrivilege.EndDatePST), ErrorHelper.GetCustomData());
            }

            if (subscriptionExpirationDate != DateTime.MinValue && subscriptionExpirationDate > DateTime.Now)
            {
                int daysTilRenew = (subscriptionExpirationDate - DateTime.Now.Date).Days;

                if (daysTilRenew <= 30)
                {
                    results.Add(daysTilRenew.ToString());
                }
                else if (daysTilRenew >= 31 && daysTilRenew <= 45)
                {
                    results.Add("31-45");
                }
                else if (daysTilRenew >= 46 && daysTilRenew <= 60)
                {
                    results.Add("46-60");
                }
                else if (daysTilRenew >= 61 && daysTilRenew <= 90)
                {
                    results.Add("61-90");
                }
                else if (daysTilRenew >= 91 && daysTilRenew <= 120)
                {
                    results.Add("91-120");
                }
                else if (daysTilRenew >= 121 && daysTilRenew <= 180)
                {
                    results.Add("121-180");
                }
                else if (daysTilRenew >= 181)
                {
                    results.Add("181+");
                }
            }

            return results;
        }

        public static IEnumerable<string> GetGamDaysToRenew(Brand brand, Member member)
        {
            var results = new List<string>();

            var subscriptionExpirationDate = DateTime.MinValue;
            var basicSubscriptionPrivilege = member.GetUnifiedAccessPrivilege(Common.AccessService.PrivilegeType.BasicSubscription, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

            if (basicSubscriptionPrivilege != null)
            {
                subscriptionExpirationDate = basicSubscriptionPrivilege.EndDatePST;
                //JS-1329
                Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", member.MemberID, PrivilegeType.BasicSubscription.ToString(), basicSubscriptionPrivilege.EndDatePST), ErrorHelper.GetCustomData());
            }

            if (subscriptionExpirationDate != DateTime.MinValue && subscriptionExpirationDate > DateTime.Now)
            {
                int daysTilRenew = (subscriptionExpirationDate - DateTime.Now.Date).Days;
                results.Add(daysTilRenew.ToString());

                if (daysTilRenew <= 30)
                {
                    results.Add("0-30");
                }
                else if (daysTilRenew >= 31 && daysTilRenew <= 45)
                {
                    results.Add("31-45");
                }
                else if (daysTilRenew >= 46 && daysTilRenew <= 60)
                {
                    results.Add("46-60");
                }
                else if (daysTilRenew >= 61 && daysTilRenew <= 90)
                {
                    results.Add("61-90");
                }
                else if (daysTilRenew >= 91 && daysTilRenew <= 120)
                {
                    results.Add("91-120");
                }
                else if (daysTilRenew >= 121 && daysTilRenew <= 180)
                {
                    results.Add("121-180");
                }
                else if (daysTilRenew >= 181)
                {
                    results.Add("181+");
                }
            }

            return results;
        }

        public static string GetGamAutoRenewStatus(Brand brand, Member member)
        {
            var returnValue = "off";
            RenewalSubscription renewalInfo = null;

            try
            {
                renewalInfo = new RenewalServiceClient().GetCurrentRenewalSubscription(member.MemberID, brand.Site.SiteID);
            }
            catch (Exception ex)
            {
                Log.LogError("GetAutoRenewStatus Error!", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            if (renewalInfo != null && renewalInfo.RenewalSubscriptionID > 0 && renewalInfo.IsRenewalEnabled)
                returnValue = "on";

            return returnValue;
        }

        private static int GetAge(DateTime birthDate)
        {
            if (birthDate == DateTime.MinValue) return 0;
            return (int) ((double) DateTime.Now.Subtract(birthDate).Days/365.25);
        }

        public static object GetGamPromoId(Brand brand, Member member)
        {
            var returnValue = "0";
            try
            {
                Promo promo = PromoEngineSA.Instance.GetMemberPromo(member.MemberID, brand);
                if (promo != null)
                    returnValue = promo.PromoID.ToString();
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Failed to get promoId for member: {0}", member.MemberID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            return returnValue;
        }




        public static string GetSubStatus(Brand brand, Member member)
        {
            //////We dont have a particular sub scription status for IAP Subs. We put in the 100 value which corresponds to gam_value of "sub_iap" in GAM.xml
            return DfpAttributeHelper.GetAdAttributes("SubscriptionStatus", member.IsPayingMember(brand.Site.SiteID) ? "100" : GetMemberSubcriptionStatus(member, brand).ToString());
        }



        public static int GetMemberSubcriptionStatus(Member member, Brand brand)
        {
            int memberSubscriptionStatus = Constants.NULL_INT;

            try
            {
                bool evaluateSubscriptionStatusAgain = false;

                memberSubscriptionStatus = member.GetAttributeInt(brand, "SubscriptionStatus", 0);
                var enumSubscriptionStatus = (Matchnet.Purchase.ValueObjects.SubscriptionStatus) memberSubscriptionStatus;

                //get basic sub expiration date
                DateTime dtSubendDate = DateTime.MinValue;
                Spark.Common.AccessService.AccessPrivilege ap = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.BasicSubscription, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (ap != null)
                {
                    dtSubendDate = ap.EndDatePST;
                    //JS-1329
                    Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", member.MemberID, PrivilegeType.BasicSubscription.ToString(), ap.EndDatePST), ErrorHelper.GetCustomData());
                }

                if (enumSubscriptionStatus == Matchnet.Purchase.ValueObjects.SubscriptionStatus.PremiumSubscriber)
                {
                    if (dtSubendDate != DateTime.MinValue)
                    {
                        // Member is currently identified as a premium subscriber
                        //if ((PremiumServiceSA.Instance.GetMemberActivePremiumServices(memberID, brand).Count < 1)
                        //    || dtSubendDate < DateTime.Now)
                        if ((PremiumServiceSA.Instance.GetMemberActivePremiumServices(member, brand).Count < 1) || dtSubendDate < DateTime.Now)
                        {
                            // Member does not have an active premium service or the member does not have
                            // subscription privilege.  
                            // Reevaluate the subscription status of this member
                            evaluateSubscriptionStatusAgain = true;
                        }
                    }
                }
                else if (enumSubscriptionStatus != Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber && enumSubscriptionStatus != Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberFreeTrial && enumSubscriptionStatus != Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed)
                {
                    if (dtSubendDate != DateTime.MinValue)
                    {
                        // Member is currently identified as a subscriber
                        if (dtSubendDate < DateTime.Now)
                        {
                            // Member does not have subscription privilege.  
                            // Reevaluate the subscription status of this member
                            evaluateSubscriptionStatusAgain = true;
                        }
                    }
                }

                if (evaluateSubscriptionStatusAgain)
                {
                    MemberSaveResult result = null;

                    memberSubscriptionStatus = Convert.ToInt16(Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetMemberSubStatus(member.MemberID, brand.Site.SiteID, dtSubendDate, false).Status);
                    member.SetAttributeInt(brand, "SubscriptionStatus", memberSubscriptionStatus);
                    result = MemberSA.Instance.SaveMember(member);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Error in FrameworkGlobals.GetMemberSubcriptionStatus(), " + ex.ToString());
                Log.LogError("Error in FrameworkGlobals.GetMemberSubcriptionStatus()", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            return memberSubscriptionStatus;
        }




        private static int GetMemberPrivilegeAtt(Brand brand, Member member)
        {
            int memberPrivilege = 0;
            int NonPermitMember = 0;
            int PermitMember = 1; // 1 bit
            int SubscribedMemberMask = 1023; //10 bits 111111111, subscribe member should have privilege to everything

            var helper = new MemberPhotoHelper(RuntimeSettings.Instance);

            //grab configuration is photo approved access is for this site
            string strConfigSetting = RuntimeSettings.GetSetting("PHOTO_FEATURE_ACCESS", brand.Site.Community.CommunityID, brand.Site.SiteID);
            if (strConfigSetting.Trim().ToLower() == "true")
            {
                //check if a member has an approved photo
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                if (null != helper.GetDefaultPhoto(member, brand))
                {
                    memberPrivilege = memberPrivilege | PermitMember;
                }
            }

            //grab configuration to see if paid features is accessible for promotion
            strConfigSetting = RuntimeSettings.GetSetting("FTPlay_FEATURE_ACCESS", brand.Site.Community.CommunityID, brand.Site.SiteID);
            if (strConfigSetting.Trim().ToLower() == "true")
            {
                memberPrivilege = memberPrivilege | PermitMember;
            }

            //check if a member currently has a subscription 
            if (member.IsPayingMember(brand.Site.SiteID))
            {
                memberPrivilege = memberPrivilege | SubscribedMemberMask;
            }

            return memberPrivilege;
        }



        public static object GetGamSite(Brand brand)
        {
            return DfpAttributeHelper.GetAdAttributes("Site", brand.Site.Name);
        }

        public static string GetGamReligion(Brand brand, Member member)
        {
            const string attributeName = "Religion";

            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection != null)
            {
                var attributeOptionValue = member.GetAttributeInt(brand, attributeName);

                var attributeOption = attributeOptionCollection[attributeOptionValue];

                return attributeOption != null ? DfpAttributeHelper.GetAdAttributes(attributeName, attributeOption.Description) : "no_value";
            }

            return string.Empty;
        }


        public static IEnumerable<string> GetGamBrandInsertDate(Brand brand, Member member)
        {
            var results = new List<string>();
            var regDate = Convert.ToDateTime(member.GetAttributeDate(brand, "BrandInsertDate"));
            var days = (regDate != DateTime.MinValue) ? DateTime.Now.Subtract(regDate).Days : 0;

            results.Add(days.ToString());

            if (days >= 0 && days <= 30)
                results.Add("0-30");
            else if (days >= 31 && days <= 60)
                results.Add("31-60");
            else if (days >= 61 && days <= 90)
                results.Add("61-90");
            else if (days >= 91 && days <= 180)
                results.Add("91-180");
            else if (days >= 181)
                results.Add("181_plus");

            return results;
        }

        public static int GetDaysSinceRegistration(Brand brand, Member member)
        {
            var regDate = Convert.ToDateTime(member.GetAttributeDate(brand, "BrandInsertDate"));
            var days = (regDate != DateTime.MinValue) ? DateTime.Now.Subtract(regDate).Days : 0;

            return days;
        }

        public static string GetGamRegisteredDevice(Brand brand, Member member)
        {
            var regDevice = String.Empty;
            if (member != null)
            {
                DeviceOS deviceOS = DeviceOS.Unknown;

                try
                {
                    deviceOS = MemberSA.Instance.GetRegisteredDevice(member, brand);
                }
                catch (Exception ex)
                {
                    Log.LogError(string.Format("Failed to get registered device for BrandId: {0} Member: {1}", brand.BrandID, member.MemberID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                }

                switch (deviceOS)
                {
                    case DeviceOS.Android_Phone:
                        regDevice = "android_ph";
                        break;
                    case DeviceOS.Android_Tablet:
                        regDevice = "android_tb";
                        break;
                    case DeviceOS.Apple_Desktop:
                        regDevice = "apple_dt";
                        break;
                    case DeviceOS.Blackberry:
                        regDevice = "blackb";
                        break;
                    case DeviceOS.Blackberry_Tablet:
                        regDevice = "blackb_tb";
                        break;
                    case DeviceOS.IPad:
                        regDevice = "ipad";
                        break;
                    case DeviceOS.IPhone:
                        regDevice = "iphone";
                        break;
                    case DeviceOS.Linux_Desktop:
                        regDevice = "linux_dt";
                        break;
                    case DeviceOS.Linux_Phone:
                        regDevice = "linux_ph";
                        break;
                    case DeviceOS.Palm_Phone:
                        regDevice = "palm_ph";
                        break;
                    case DeviceOS.Palm_Tablet:
                        regDevice = "palm_tb";
                        break;
                    case DeviceOS.Symbian_Phone:
                        regDevice = "symbian_ph";
                        break;
                    case DeviceOS.Windows_Desktop:
                        regDevice = "windows_dt";
                        break;
                    case DeviceOS.Windows_Phone:
                        regDevice = "windows_ph";
                        break;
                    case DeviceOS.Windows_Tablet:
                        regDevice = "windows_tb";
                        break;
                    default:
                        regDevice = "unknown";
                        break;
                }
            }

            return regDevice;
        }

        public static IEnumerable<string> GetGamSubscriptionExpirationStatus(Brand brand, Member member)
        {
            var results = new List<string>();

            DateTime subscriptionExpirationDate = DateTime.MinValue;

            var basicSubscriptionPrivilege = member.GetUnifiedAccessPrivilege(Common.AccessService.PrivilegeType.BasicSubscription, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

            if (basicSubscriptionPrivilege != null)
            {
                subscriptionExpirationDate = basicSubscriptionPrivilege.EndDatePST;
                //JS-1329
                Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", member.MemberID, PrivilegeType.BasicSubscription.ToString(), basicSubscriptionPrivilege.EndDatePST), ErrorHelper.GetCustomData());
            }

            if (subscriptionExpirationDate != DateTime.MinValue && subscriptionExpirationDate < DateTime.Now)
            {
                int daysSinceSubEnd = (DateTime.Now - subscriptionExpirationDate).Days;

                results.Add(daysSinceSubEnd.ToString());
                if (daysSinceSubEnd >= 0 && daysSinceSubEnd <= 30)
                    results.Add("0-30");
                else if (daysSinceSubEnd >= 31 && daysSinceSubEnd <= 60)
                    results.Add("31-60");
                else if (daysSinceSubEnd >= 61 && daysSinceSubEnd <= 90)
                    results.Add("61-90");
                else if (daysSinceSubEnd >= 91 && daysSinceSubEnd <= 180)
                    results.Add("91-180");
                else if (daysSinceSubEnd >= 181)
                    results.Add("181_plus");
            }

            return results;
        }

        public static string GetSubscriptionLapseDays(Brand brand, Member member)
        {
            DateTime subscriptionExpirationDate = DateTime.MinValue;

            var basicSubscriptionPrivilege = member.GetUnifiedAccessPrivilege(Common.AccessService.PrivilegeType.BasicSubscription, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

            if (basicSubscriptionPrivilege != null)
            {
                subscriptionExpirationDate = basicSubscriptionPrivilege.EndDatePST;
                Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", member.MemberID, PrivilegeType.BasicSubscription.ToString(), basicSubscriptionPrivilege.EndDatePST), ErrorHelper.GetCustomData());
            }

            if (subscriptionExpirationDate != DateTime.MinValue)
            {
                return (DateTime.Now - subscriptionExpirationDate).Days.ToString();
            }

            return "";
        }

        public static object GetGamProfession(Brand brand, Member member)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection("IndustryType", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            var attributeOptionValue = member.GetAttributeInt(brand, "IndustryType");

            AttributeOption attributeOption = null;
            if (attributeOptionCollection != null)
            {
                attributeOption = attributeOptionCollection[attributeOptionValue];
            }

            return attributeOption != null ? DfpAttributeHelper.GetAdAttributes("IndustryType", attributeOption.Description) : "no_value";
        }


        public static string GetDescription(Brand brand, string attributeName, int attributeValue)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            AttributeOption attributeOption = attributeOptionCollection[attributeValue];

            if (attributeOption != null)
                return attributeOption.Description.ToString();

            return string.Empty;
        }



        public static object GetGamEthnicity(Brand brand, Member member)
        {
            var r = new RegionInfo(brand.Site.CultureInfo.LCID);
            var ethnicityValue = 0;
            var actualAttributeName = r.Name == "IL" ? "Ethnicity" : "JDateEthnicity";

            ethnicityValue = member.GetAttributeInt(brand, actualAttributeName);

            return DfpAttributeHelper.GetAdAttributes("Ethnicity", GetDescription(brand, actualAttributeName, ethnicityValue));
        }

        public static object GetGamFeatures(Brand brand, Member member)
        {
            var r = new RegionInfo(brand.Site.CultureInfo.LCID);
            var ethnicityValue = string.Empty;

            var list = r.Name == "IL" ? new List<string>
            {
                GetAllAccessStatus(brand, member), GetQuizStatus(brand, member), GetHighlightStatus(brand, member), GetSpotlightStatus(brand, member), GetInviteStatus(brand, member), GetMatchTestStatus(brand, member), GetPhotosStatus(brand, member), GetMobileStatus(brand, member)
            } : new List<string>
            {
//us
                GetAllAccessStatus(brand, member), GetColorCodeColor(brand, member), GetMatchTestStatus(brand, member), GetCustodyStatus(brand, member), GetMobileStatus(brand, member), GetNewsletterStatus(brand, member), GetPhotosStatus(brand, member), GetInviteStatus(brand, member), GetUAPStatus(brand, member)
            };
            list.RemoveAll(string.IsNullOrWhiteSpace);

            return list;
        }

        public static object GetGamFreeTime(Brand brand, Member member)
        {
            return GetDescriptions("LeisureActivity", member.GetAttributeInt(brand, "LeisureActivity"), brand).Select(description => DfpAttributeHelper.GetAdAttributes("LeisureActivity", description)).ToList();
        }

        public static object GetGamGender(Brand brand, Member member)
        {
            var results = new List<string>();

            var genderMask = Convert.ToString(member.GetAttributeInt(brand, "Gendermask"));
            switch (genderMask)
            {
                case "6":
                    results.Add("fm");
                    results.Add("f");
                    break;
                case "10":
                    results.Add("ff");
                    results.Add("f");
                    break;
                case "5":
                    results.Add("mm");
                    results.Add("m");
                    break;
                case "9":
                    results.Add("mf");
                    results.Add("m");
                    break;
            }

            return results;
        }

        public static object GetGamIncome(Brand brand, Member member)
        {
            var attributeValue = member.GetAttributeInt(brand, "IncomeLevel");
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection("IncomeLevel", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            AttributeOption attributeOption = attributeOptionCollection[attributeValue];

            if (attributeOption == null)
            {
                return "undefined";
            }

            return DfpAttributeHelper.GetAdAttributes("IncomeLevel", attributeOption.Description);
        }

        public static object GetGamLastMemId(Member member)
        {
            return (member.MemberID%10).ToString();
        }

        public static object GetGamMaritalStatus(Brand brand, Member member)
        {
            var attributeValue = member.GetAttributeInt(brand, "MaritalStatus");
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection("MaritalStatus", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            AttributeOption attributeOption = attributeOptionCollection[attributeValue];

            if (attributeOption == null)
            {
                return "undefined";
            }
            return DfpAttributeHelper.GetAdAttributes("MaritalStatus", attributeOption.Description);
        }



        private static string GetHighlightStatus(Brand brand, Member member)
        {
            var returnValue = string.Empty;
            AccessPrivilege apHighlightedProfile = null;

            try
            {
                apHighlightedProfile = member.GetUnifiedAccessPrivilege(Common.AccessService.PrivilegeType.HighlightedProfile, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Failed to get HighlightedProfile for BrandId: {0} SiteId: {1} CommunityId: {2}", brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            if (apHighlightedProfile != null && apHighlightedProfile.EndDatePST > DateTime.Now)
            {
                returnValue = "highlight_taker";
                //JS-1329
                Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", member.MemberID, PrivilegeType.HighlightedProfile.ToString(), apHighlightedProfile.EndDatePST), ErrorHelper.GetCustomData());
            }

            return returnValue;
        }

        private static string GetSpotlightStatus(Brand brand, Member member)
        {
            var returnValue = string.Empty;
            AccessPrivilege apSpotlightMember = null;

            try
            {
                apSpotlightMember = member.GetUnifiedAccessPrivilege(Common.AccessService.PrivilegeType.SpotlightMember, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (apSpotlightMember != null && apSpotlightMember.EndDatePST > DateTime.Now)
                {
                    returnValue = "spotlight_taker";
                    //JS-1329
                    Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", member.MemberID, PrivilegeType.SpotlightMember.ToString(), apSpotlightMember.EndDatePST), ErrorHelper.GetCustomData());
                }
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Failed to get spotlight status for BrandId: {0} SiteId: {1} CommunityId: {2}", brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            return returnValue;
        }

        private static string GetNewsletterStatus(Brand brand, Member member)
        {
            var returnValue = string.Empty;
            var newsletterMask = member.GetAttributeInt(brand, "NewsletterMask", 0);

            if ((newsletterMask & (int) NewsEventOfferMask.News) == (int) NewsEventOfferMask.News || (newsletterMask & (int) NewsEventOfferMask.Offers) == (int) NewsEventOfferMask.Offers)
            {
                returnValue = "newslet_y";
            }
            else
            {
                returnValue = "newslet_n";
            }

            return returnValue;
        }

        private static string GetMobileStatus(Brand brand, Member member)
        {
            var returnValue = string.Empty;
            int messmoCapable = member.GetAttributeInt(brand, "MessmoCapable", 0);

            if (messmoCapable == (int) DfpAttributeHelper.MessmoCapableStatus.Capable || messmoCapable == (int) DfpAttributeHelper.MessmoCapableStatus.Pending)
                returnValue = "mobile_yes";
            else
                returnValue = "mobile_no";

            return returnValue;
        }

        private static string GetCustodyStatus(Brand brand, Member member)
        {
            var returnValue = string.Empty;
            var custody = member.GetAttributeInt(brand, "Custody");

            if (custody == Constants.NULL_INT)
                returnValue = "kids_blank";

            switch ((DfpAttributeHelper.Custody) custody)
            {
                case DfpAttributeHelper.Custody.Blank:
                    returnValue = "kids_blank";
                    break;
                case DfpAttributeHelper.Custody.NoKids:
                    returnValue = "kids_no";
                    break;
                default:
                    returnValue = "kids_yes";
                    break;
            }

            return returnValue;
        }


        private static string GetMatchTestStatus(Brand brand, Member member)
        {
            var returnValue = "jm_notaker";
            var iscurrentMemberSubscribed = DfpAttributeHelper.SubscribedMemberMask == (DfpAttributeHelper.SubscribedMemberMask & GetMemberPrivilegeAtt(brand, member));

            try
            {
                MatchTestStatus mts = MatchMeterSA.Instance.GetMatchTestStatus(member.MemberID, iscurrentMemberSubscribed, brand);

                if (mts == MatchTestStatus.Completed || mts == MatchTestStatus.MinimumCompleted)
                {
                    returnValue = "jm_taker";
                }
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Failed to GetMatchTest status for BrandId: {0} and MemberId: {1}", brand.BrandID, member.MemberID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            return returnValue;
        }

        private static string GetInviteStatus(Brand brand, Member member)
        {
            string returnValue = "invite_no";
            int NewsletterMask = member.GetAttributeInt(brand, "NewsletterMask");

            if ((NewsletterMask & (int) NewsEventOfferMask.Events) == (int) NewsEventOfferMask.Events)
            {
                returnValue = "invite_yes";
            }

            return returnValue;
        }

        private static string GetAllAccessStatus(Brand brand, Member member)
        {
            var returnValue = "allacc_no";

            AccessPrivilege privdate = null;
            try
            {
                privdate = member.GetUnifiedAccessPrivilege(PrivilegeType.AllAccess, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Error getting unified access privilege brandId: {0} siteId: {1} communityId: {2} ", brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            if (privdate != null && privdate.EndDatePST > DateTime.UtcNow)
            {
                returnValue = "allacc_yes";
                //JS-1329
                Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", member.MemberID, PrivilegeType.AllAccess.ToString(), privdate.EndDatePST), ErrorHelper.GetCustomData());
            }

            return returnValue;
        }

        private static string GetQuizStatus(Brand brand, Member member)
        {
            var colorCode = member.GetAttributeText(brand, "ColorCodePrimaryColor");

            return colorCode != "none" ? "cc_taker" : "cc_notaker";
        }

        private static string GetColorCodeColor(Brand brand, Member member)
        {
            var returnValue = string.Empty;
            var colorCode = member.GetAttributeText(brand, "ColorCodePrimaryColor");

            switch (colorCode)
            {
                case "red":
                    returnValue = "cc_red";
                    break;
                case "blue":
                    returnValue = "cc_blue";
                    break;
                case "yellow":
                    returnValue = "cc_yellow";
                    break;
                case "white":
                    returnValue = "cc_white";
                    break;
                default:
                    break;
            }

            return returnValue;
        }

        private static string GetPhotosStatus(Brand brand, Member member)
        {
            string returnValue = string.Empty;

            var photos = member.GetPhotos(brand.Site.Community.CommunityID);

            if (photos == null || photos.Count == 0)
                returnValue = "no_photos";
            else
                returnValue = "photos";

            return returnValue;
        }

        private static string GetUAPStatus(Brand brand, Member member)
        {
            var returnValue = string.Empty;

            AccessPrivilege apReadReceipts = null;

            try
            {
                apReadReceipts = member.GetUnifiedAccessPrivilege(PrivilegeType.ReadReceipt, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Failed to get UAPStatus for BrandId: {0} SiteId: {1} CommunityId: {2}", brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            if (apReadReceipts != null && apReadReceipts.EndDatePST > DateTime.Now)
            {
                returnValue = "read_yes";
                //JS-1329
                Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", member.MemberID, PrivilegeType.ReadReceipt.ToString(), apReadReceipts.EndDatePST), ErrorHelper.GetCustomData());
            }
            else
            {
                returnValue = "read_no";
            }

            return returnValue;
        }



        internal static object GetGamSincePurchase(Brand brand, Member member)
        {
            var returnValue = new List<string>();

            var subscriptionLastInitialPurchaseDate = member.GetAttributeDate(brand, "SubscriptionLastInitialPurchaseDate", DateTime.MinValue);
            int daysSinceLastInitialPurchase = (DateTime.Now - subscriptionLastInitialPurchaseDate).Days;
            if (subscriptionLastInitialPurchaseDate == DateTime.MinValue)
            {
                daysSinceLastInitialPurchase = -1;
            }

            returnValue.Add(daysSinceLastInitialPurchase.ToString());

            if (daysSinceLastInitialPurchase >= 0 && daysSinceLastInitialPurchase <= 30)
                returnValue.Add("0-30");
            else if (daysSinceLastInitialPurchase >= 31 && daysSinceLastInitialPurchase <= 60)
                returnValue.Add("31-60");
            else if (daysSinceLastInitialPurchase >= 61 && daysSinceLastInitialPurchase <= 90)
                returnValue.Add("61-90");
            else if (daysSinceLastInitialPurchase >= 91 && daysSinceLastInitialPurchase <= 180)
                returnValue.Add("91-180");
            else if (daysSinceLastInitialPurchase >= 181)
                returnValue.Add("181_plus");

            return returnValue;
        }

        public static string GetClientIP(HttpRequestBase request)
        {
            var strIp = request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (strIp != null && strIp.Trim().Length > 0)
            {
                string[] addresses = strIp.Split(new char[] {',', ' '}, StringSplitOptions.RemoveEmptyEntries);
                if (addresses.Length > 0)
                {
                    // first one = client IP per http://en.wikipedia.org/wiki/X-Forwarded-For
                    strIp = addresses[0];
                }
            }

            if (!IsValidIpAddress(strIp))
            {
                strIp = String.Empty;
            }

            // if that's empty, get their ip via server vars
            if (strIp == null || strIp.Trim().Length == 0)
            {
                strIp = request.ServerVariables["client-ip"];
            }

            if (!IsValidIpAddress(strIp))
            {
                strIp = String.Empty;
            }

            if (strIp == null || strIp.Trim().Length == 0)
            {
                strIp = request.ServerVariables["REMOTE_ADDR"];
            }

            if (!IsValidIpAddress(strIp))
            {
                strIp = String.Empty;
            }

            // if that's still empty, get their ip via .net's built-in method
            if (strIp == null || strIp.Trim().Length == 0)
            {
                strIp = request.UserHostAddress;
            }

            return strIp;
        }

        public static bool IsValidIpAddress(string strIp)
        {
            if (strIp != null)
            {
                return IpAddressRegex.IsMatch(strIp.Trim());
            }
            return false;
        }
    }
}
