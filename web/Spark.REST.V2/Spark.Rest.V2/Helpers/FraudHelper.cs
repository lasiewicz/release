﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Spark.Common.Adapter;
using Spark.Common.Fraud;
using Spark.Common.FraudService;
using Spark.Logger;
using Spark.Rest.Helpers;

namespace Spark.Rest.V2.Helpers
{
    internal static class FraudHelper
    {
         private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof (FraudHelper));

        internal static FraudDetailedResponse GetMemberFraudInfo(int memberId, int riskInquiryTypeId, int siteId, string bindingId)
        {
            var fraudInfo = new FraudDetailedResponse();
            try
            {
                var fraudServiceAdapter = new FraudServiceAdapter();
                fraudInfo = fraudServiceAdapter.GetProxyInstance()
                    .GetMemberFraudDetailedResponse(memberId, riskInquiryTypeId, siteId, bindingId);
            }
            catch (Exception ex)
            {
                Log.LogError("Error in Retrieving Fraud Detailed response", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
            }
            return fraudInfo;
        }

        public static FraudResponse DoKountFraudCheck(int memberId, Brand brand, string emailAddress,
   Dictionary<string, object> attributeValues, string ipAddress, string sessionID)
        {
            var registrationRiskData = new RegistrationRiskInquiryData();
            var riskInquiryResponse = new FraudResponse();
            // Make sure Kount should not break any registrations
            // bool _passedKountFraudCheck = true;
            try
            {
                //first, read the values into a case insensitive dictionary so that we don't miss values because of casing issues
                var caseNeutralAttributeValues = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
                foreach (var attribute in attributeValues)
                {
                    caseNeutralAttributeValues.Add(attribute.Key, attribute.Value);
                }

                #region UDFS
                //now read out the necessary values
                //1.       USERNAME
                if (caseNeutralAttributeValues.ContainsKey("USerName") && caseNeutralAttributeValues["UserName"] is string)
                {
                    registrationRiskData.UserName = (caseNeutralAttributeValues["UserName"]).ToString();
                }

                //2.       AGE
                registrationRiskData.DateOfBirth = Constants.NULL_STRING;
                if (caseNeutralAttributeValues.ContainsKey("Birthdate") && caseNeutralAttributeValues["Birthdate"] is string)
                {
                    registrationRiskData.DateOfBirth = (caseNeutralAttributeValues["Birthdate"]).ToString();
                    //TODO: check on age
                    registrationRiskData.Age = CalculateAge(registrationRiskData.DateOfBirth);
                }

                //3.       MARITALSTATUS
                if (caseNeutralAttributeValues.ContainsKey("MaritalStatus") && caseNeutralAttributeValues["MaritalStatus"] is string)
                {
                    registrationRiskData.MaritalStatus = caseNeutralAttributeValues["MaritalStatus"].ToString();
                }
                //4.       OCCUPATION
                if (caseNeutralAttributeValues.ContainsKey("OccupationDescription") && caseNeutralAttributeValues["OccupationDescription"] is string)
                {
                    registrationRiskData.Occupation = caseNeutralAttributeValues["OccupationDescription"].ToString();
                }


                //5.       EDUCATION
                if (caseNeutralAttributeValues.ContainsKey("EducationLevel") && caseNeutralAttributeValues["EducationLevel"] is string)
                {
                    registrationRiskData.Education = caseNeutralAttributeValues["EducationLevel"].ToString();

                }

                //6.       RELIGION
                var religionInt = Constants.NULL_INT;
                if (caseNeutralAttributeValues.ContainsKey("Religion") && caseNeutralAttributeValues["Religion"] is string)
                {
                    registrationRiskData.Religion = caseNeutralAttributeValues["Religion"].ToString();
                }

                //7.	ETHNICITY
                if (caseNeutralAttributeValues.ContainsKey("Ethnicity") && caseNeutralAttributeValues["Ethnicity"] is string)
                {
                    registrationRiskData.Ethnicity = caseNeutralAttributeValues["Ethnicity"].ToString();
                }
                //8.	ABOUTME
                if (caseNeutralAttributeValues.ContainsKey("AboutMe") && caseNeutralAttributeValues["AboutMe"] is string)
                {
                    // Allows only upto 255 Chars Max
                    var aboutMe = caseNeutralAttributeValues["AboutMe"].ToString();
                    registrationRiskData.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);
                }

                //9.	CAMPAIGNSRC : Depricated

                //10.	CAMPAIGNCAT : Renamed to PromotionID
                if (caseNeutralAttributeValues.ContainsKey("PromotionID") && caseNeutralAttributeValues["PromotionID"] is int)
                {
                    registrationRiskData.PromotionID = (Conversion.CInt(caseNeutralAttributeValues["PromotionID"])).ToString();
                }

                var firstName = caseNeutralAttributeValues.ContainsKey("SiteFirstName") ? caseNeutralAttributeValues["SiteFirstName"] : string.Empty;
                var lastName = caseNeutralAttributeValues.ContainsKey("SiteLastName") ? caseNeutralAttributeValues["SiteLastName"] : string.Empty;
                //TODO: check right pattern
                registrationRiskData.Name = firstName + " " + lastName;

                //11.   EYES
                if (caseNeutralAttributeValues.ContainsKey("EyeColor") && caseNeutralAttributeValues["EyeColor"] is string)
                {
                    registrationRiskData.Eyes = (caseNeutralAttributeValues["EyeColor"]).ToString();
                }

                //12.   HAIR
                if (caseNeutralAttributeValues.ContainsKey("HairColor") && caseNeutralAttributeValues["HairColor"] is string)
                {
                    registrationRiskData.Hair = (caseNeutralAttributeValues["HairColor"]).ToString();
                }

                //13.   HEIGHT
                if (caseNeutralAttributeValues.ContainsKey("Height") && caseNeutralAttributeValues["Height"] is int)
                {
                    registrationRiskData.Height = (Conversion.CInt(caseNeutralAttributeValues["Height"])).ToString();
                }


                #endregion UDF

                //Compile other KOUNT fields
                registrationRiskData.Email = emailAddress;
                registrationRiskData.CustomerID = memberId.ToString();
                registrationRiskData.SessionID = sessionID.Replace("-", "");
                registrationRiskData.DateOfRegistration = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);

                if (caseNeutralAttributeValues.ContainsKey("Gender") && caseNeutralAttributeValues["Gender"] is string)
                {
                    registrationRiskData.Gender = caseNeutralAttributeValues["Gender"].ToString();
                }
                if (caseNeutralAttributeValues.ContainsKey("AddressCity") && caseNeutralAttributeValues["AddressCity"] is string)
                {
                    registrationRiskData.AddressCity = caseNeutralAttributeValues["AddressCity"].ToString();
                }
                if (caseNeutralAttributeValues.ContainsKey("AddressCountry") && caseNeutralAttributeValues["AddressCountry"] is string)
                {
                    registrationRiskData.AddressCountry = caseNeutralAttributeValues["AddressCountry"].ToString();
                }
                if (caseNeutralAttributeValues.ContainsKey("AddressPostalCode") && caseNeutralAttributeValues["AddressPostalCode"] is int)
                {
                    registrationRiskData.AddressPostalCode = caseNeutralAttributeValues["AddressPostalCode"].ToString();
                }
                if (caseNeutralAttributeValues.ContainsKey("AddressState") && caseNeutralAttributeValues["AddressState"] is string)
                {
                    registrationRiskData.AddressState = caseNeutralAttributeValues["AddressState"].ToString();
                }

                registrationRiskData.SiteID = brand.Site.SiteID.ToString();
                registrationRiskData.CallingSystemTypeID = brand.Site.SiteID.ToString();

                Log.LogInfoMessage(string.Format("IPAddress: {0}", ipAddress), ErrorHelper.GetCustomData());
                //registrationRiskData.IPAddress = new IPAddress(BitConverter.GetBytes(ipAddress)).ToString();
                registrationRiskData.IPAddress = ipAddress;

                var objJSONRiskInquiryRequestData = new JSONRegistrationRiskInquiryRequestData();
                objJSONRiskInquiryRequestData.registrationRiskInquiryRequestData = registrationRiskData.SerializeRiskInquiryData;

                //logging risk data
                Log.LogInfoMessage(string.Format("registrationRiskInquiryRequestData: {0}", objJSONRiskInquiryRequestData.registrationRiskInquiryRequestData), ErrorHelper.GetCustomData());
                Log.LogInfoMessage("Invoke fraud DoRegistrationRiskInquiry", ErrorHelper.GetCustomData());

                var fraudServiceAdapter = new FraudServiceAdapter();
                riskInquiryResponse = fraudServiceAdapter.GetProxyInstance().DoRegistrationRiskInquiry(objJSONRiskInquiryRequestData.registrationRiskInquiryRequestData);

                var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);

                if (riskInquiryResponse != null)
                {
                    Log.LogInfoMessage(string.Format("Memberid:{0}, KountScore: {1}", memberId, riskInquiryResponse.RiskInquiryScore), ErrorHelper.GetCustomData());

                    if (riskInquiryResponse.AutomateRiskInquiryStatus == Spark.Common.FraudService.AutomatedRiskInquiryStatus.A
                        || riskInquiryResponse.AutomateRiskInquiryStatus == Spark.Common.FraudService.AutomatedRiskInquiryStatus.R)
                    {
                        //Acceptable RISK
                        try
                        {
                            if (member != null)
                            {
                                member.SetAttributeInt(brand, "KOUNTScore", Convert.ToInt32(riskInquiryResponse.RiskInquiryScore));
                                member.SetAttributeInt(brand, "KOUNTAutomatedRiskInquiryStatusID", Convert.ToInt32(riskInquiryResponse.AutomateRiskInquiryStatus));
                                MemberSaveResult msr = MemberSA.Instance.SaveMember(member);
                                Log.LogInfoMessage(string.Format("Member fraud response Attribute Savestatus: {0}", msr.SaveStatus), ErrorHelper.GetCustomData());
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.LogError(string.Format("Failed to set member attribute memberid: {0}", memberId), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                        }
                    }
                    else if (riskInquiryResponse.AutomateRiskInquiryStatus == Spark.Common.FraudService.AutomatedRiskInquiryStatus.D)
                    {
                        // Declined
                        if (member != null)
                        {
                            member.SetAttributeInt(brand, "KOUNTScore", Convert.ToInt32(riskInquiryResponse.RiskInquiryScore));
                            member.SetAttributeInt(brand, "KOUNTAutomatedRiskInquiryStatusID", Convert.ToInt32(riskInquiryResponse.AutomateRiskInquiryStatus));
                            MemberSaveResult msr = MemberSA.Instance.SaveMember(member);
                            Log.LogInfoMessage(string.Format("Member fraud response Attribute Savestatus: {0}", msr.SaveStatus), ErrorHelper.GetCustomData());
                        }
                    }
                    else if (riskInquiryResponse.AutomateRiskInquiryStatus == Spark.Common.FraudService.AutomatedRiskInquiryStatus.E
                             || riskInquiryResponse.AutomateRiskInquiryStatus == Spark.Common.FraudService.AutomatedRiskInquiryStatus.None)
                    {
                        // In this case still allow the Registration to process 
                        Log.LogInfoMessage("The fraud response has Error code. Allow the registration process to continue.", ErrorHelper.GetCustomData());
                    }
                }
                else
                {
                    Log.LogInfoMessage("The fraud response was NULL. Allow the registration process to continue.", ErrorHelper.GetCustomData());
                }

            }
            catch (Exception ex)
            {
                Log.LogError("Error in KOUNT call", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            return riskInquiryResponse;
        }



        private static string CalculateAge(string birthDate)
        {
            var dtBirthDate = Convert.ToDateTime(birthDate);
            DateTime dtToday = DateTime.Now;

            // get the difference in year
            int years = dtToday.Year - dtBirthDate.Year;
            // subtract another year if we're before the
            // birth day in the current year
            if (dtToday.Month < dtBirthDate.Month || (dtToday.Month == dtBirthDate.Month && dtToday.Day < dtBirthDate.Day))
                years = years - 1;

            return years.ToString();
        }
    }
}