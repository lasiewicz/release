﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects;
using Spark.LoginFraud;
using Spark.Managers.Managers;
using Spark.Rest.V2.DataAccess;

namespace Spark.Rest.V2.Helpers
{
    public class LoginFraudHelper
    {
        private Brand _brand;
        private LoginFraudClient _loginFraudClient;
        
        public LoginFraudHelper(Brand brand)
        {
            _brand = brand;
            _loginFraudClient = new LoginFraudClient(brand, brand.Site.SiteID);
        }
        
        public bool IsLoginFraudEnabled(string ipAddress)
        {
            return new SettingsManager().GetSettingBool(SettingConstants.MINGLE_LOGIN_FRAUD_ENABLED, _brand) && !string.IsNullOrEmpty(ipAddress);
        }

        public async Task<FraudCheckResponse> CheckFraudAsync(string email, string ipAddress, string userAgent,
            string httpHeaders, int applicationId, int brandId, string loginSessionId)
        {
            if (!IsLoginFraudEnabled(ipAddress)) return null;

            var fraudActionType = FraudActionType.NotFraud;
            var rejectedReasonId = 0;

            var fraudCheckResponse =await _loginFraudClient.CheckFraudAsync(email, ipAddress, userAgent ?? string.Empty,
                        httpHeaders ?? string.Empty, applicationId);

            if (fraudCheckResponse.ValidResult)
            {
                fraudActionType = fraudCheckResponse.ActionId;
                rejectedReasonId = fraudCheckResponse.StepId;
            }

            var ctx = System.Web.HttpContext.Current;
            Matchnet.MiscUtils.FireAndForget(
                o =>
                {
                    //set HttpContext for thread
                    System.Web.HttpContext.Current = ctx;
                    try
                    {
                        ActivityRecordingAccess.Instance.RecordLoginFraudCallActivity(loginSessionId, brandId, email,
                            fraudCheckResponse.ValidResult, fraudActionType,
                            rejectedReasonId);
                    }
                    finally
                    {
                        //unset HttpContext for thread (precaution for GC)
                        System.Web.HttpContext.Current = null;
                    }
                },
                "APIActivityRecording",
                "Failed to record activity");

            return fraudCheckResponse;
        }

        public UpdateLogResponse UpdateLog(string emailAddress, int logId, string ipAddress, int memberId, AuthenticationStatus status)
        {
            if (IsLoginFraudEnabled(ipAddress))
            {
                return _loginFraudClient.UpdateLog(logId, emailAddress, memberId, AuthenticationStatusToLoginStatus(status));
            }
            return null;
        }

        public LogActionResponse LogAction(string emailAddress, LogActionType logActionType, string ipAddress, string userAgent,
                                           string httpHeaders, int memberId, int applicationId, bool throttled, bool loginSuccessful)
        {
            if (IsLoginFraudEnabled(ipAddress))
            {
                return _loginFraudClient.LogAction(emailAddress, logActionType, ipAddress, userAgent ?? string.Empty,
                                                   httpHeaders ?? string.Empty, memberId, applicationId, throttled,
                                                   loginSuccessful);
            }
            return null;
        }

        public EmailUpdateResponse ChangeEmail(int memberId, string newEmailAddress, string ipAddress)
        {
            if (IsLoginFraudEnabled(ipAddress))
            {
                return _loginFraudClient.UpdateEmailAddress(memberId, newEmailAddress);
            }
            return null;
        }

        internal static LoginStatus AuthenticationStatusToLoginStatus(AuthenticationStatus authStatus)
        {
            var loginStatus = LoginStatus.Success;

            switch (authStatus)
            {
                case AuthenticationStatus.InvalidPassword:
                    loginStatus = LoginStatus.FailedBadPassword;
                    break;
                case AuthenticationStatus.InvalidEmailAddress:
                    loginStatus = LoginStatus.FailedBadEmail;
                    break;
                case AuthenticationStatus.AdminSuspended:
                    loginStatus = LoginStatus.FailedSuspended;
                    break;
            }

            return loginStatus;
        }
            
    }
}