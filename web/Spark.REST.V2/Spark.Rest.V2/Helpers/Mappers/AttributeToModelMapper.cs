﻿using Matchnet.ExternalMail.ValueObjects;
using Matchnet.Member.ValueObjects;
using Spark.Rest.V2.DataAccess.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Helpers.Mappers
{
    public class AttributeToModelMapper
    {
        public static readonly AttributeToModelMapper Instance = new AttributeToModelMapper();

        private AttributeToModelMapper() { /* hide singleton ctor */  }

        public EmailAlertSettings MapToEmailAlertSettings(int mask, int secretAdmirerEmailFrequency, int shouldNotifyMatches)
        {
            var result = new EmailAlertSettings
            {
                SecretAdmirerEmailFrequency = secretAdmirerEmailFrequency,

                ShouldNotifyMatches = shouldNotifyMatches == 1,

                ShouldNotifyWhenAddedToHotlist = (mask & (int)EmailAlertMask.HotListedAlert) == (int)EmailAlertMask.HotListedAlert,

                ShouldNotifyEmailAlert = (mask & (int)EmailAlertMask.NewEmailAlert) == (int)EmailAlertMask.NewEmailAlert,

                ShouldNotifyEcard = (mask & (int)EmailAlertMask.ECardAlert) == (int)EmailAlertMask.ECardAlert,

                ShouldNotifySecretAdmirerAlerts = (mask & (int)EmailAlertMask.GotClickAlert) == (int)EmailAlertMask.GotClickAlert,

                ShouldNotifyWhenProfileViewed = (mask & (int)EmailAlertMask.ProfileViewedAlertOptOut) != (int)EmailAlertMask.ProfileViewedAlertOptOut,

                ShouldNotifyProfileEssayRequests = (mask & (int)EmailAlertMask.NoEssayRequestEmail) == (int)EmailAlertMask.NoEssayRequestEmail
            };

            return result;
        }

        public NewsLetterSettings MapToNewsLetterSettings(int mask)
        {
            var result = new NewsLetterSettings
            {
                ShouldSendEventsInvitations = (mask & (int)NewsEventOfferMask.Events) == (int)NewsEventOfferMask.Events,

                //combining newsletters and offers
                ShouldSendDatingTips = (mask & (int)NewsEventOfferMask.News) == (int)NewsEventOfferMask.News ||
                                       (mask & (int)NewsEventOfferMask.Offers) == (int)NewsEventOfferMask.Offers,

                //separate newsletters and offers
                ShouldSendNewsletters = (mask & (int)NewsEventOfferMask.News) == (int)NewsEventOfferMask.News,
                ShouldSendOffers = (mask & (int)NewsEventOfferMask.Offers) == (int)NewsEventOfferMask.Offers

            };

            return result;
        }

        public MessageSettings MapToMessageSettings(int mask, DateTime vacationStartDate)
        {
            var result = new MessageSettings
            {
                ShouldIncludeOriginalInReplies = (mask & (int)AttributeOptionMailboxPreference.IncludeOriginalMessagesInReplies) == (int)AttributeOptionMailboxPreference.IncludeOriginalMessagesInReplies,

                ShouldSaveSentCopies = (mask & (int)AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) == (int)AttributeOptionMailboxPreference.SaveCopiesOfSentMessages,

                ShouldReplyOnMyBehalf = vacationStartDate != DateTime.MinValue
            };

            return result;
        }

    }
}