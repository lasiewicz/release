﻿using Spark.Rest.V2.DataAccess.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.Member.ValueObjects;

namespace Spark.Rest.V2.Helpers.Mappers
{
    public class ModelToAttributeMapper
    {
        public static readonly ModelToAttributeMapper Instance = new ModelToAttributeMapper();

        private ModelToAttributeMapper() { /* hide singleton ctor */ }

        public int MapFromEmailAlertSettings(EmailAlertSettings settings)
        {
            var result = 0;

            if (settings.ShouldNotifyWhenAddedToHotlist)
                result = (result | (int)EmailAlertMask.HotListedAlert);
            if (settings.ShouldNotifyEmailAlert)
                result = (result | (int)EmailAlertMask.NewEmailAlert);
            if (settings.ShouldNotifyEcard)
                result = (result | (int)EmailAlertMask.ECardAlert);
            if (settings.ShouldNotifySecretAdmirerAlerts)
                result = (result | (int)EmailAlertMask.GotClickAlert);
            if (!settings.ShouldNotifyWhenProfileViewed)
                result = (result | (int)EmailAlertMask.ProfileViewedAlertOptOut);
            if (settings.ShouldNotifyProfileEssayRequests)
                result = (result | (int)EmailAlertMask.NoEssayRequestEmail);

            return result;
        }

        public int MapFromNewsLetterSettings(NewsLetterSettings settings)
        {
            var result = 0;
            if (settings.ShouldSendEventsInvitations)
                result = (result | (int)NewsEventOfferMask.Events);

            //combining newsletters and offers
            if (settings.ShouldSendDatingTips.HasValue)
            {
                if (settings.ShouldSendDatingTips.Value)
                {
                    result = (result | (int)NewsEventOfferMask.News);
                    result = (result | (int)NewsEventOfferMask.Offers);
                }
            }
            else
            {
                if (settings.ShouldSendNewsletters)
                {
                    result = (result | (int)NewsEventOfferMask.News);
                }

                if (settings.ShouldSendOffers)
                {
                    result = (result | (int)NewsEventOfferMask.Offers);
                }
            }

            return result;
        }

        public int MapFromMessageSettings(MessageSettings settings)
        {
            var result = 0;            

            if (settings.ShouldIncludeOriginalInReplies)
                result |= (int)AttributeOptionMailboxPreference.IncludeOriginalMessagesInReplies;

            if (settings.ShouldSaveSentCopies)
                result |= (int)AttributeOptionMailboxPreference.SaveCopiesOfSentMessages;

            return result;
        }
    }
}