﻿using System;
using System.Collections.Generic;
using System.Linq;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Common.Localization;
using Spark.REST.Configuration;
using Spark.REST.DataAccess.Attributes;
using Spark.Rest.V2.BedrockPorts;
using Matchnet.Content.ValueObjects.Region;

namespace Spark.REST.Helpers
{
    internal static class AttributeHelper
    {
        internal static Dictionary<string, object> MapPublicAttributesToInternalFormat(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
		{
            if (attributeData == null)
                attributeData = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            if (attributeDataMultiValue == null)
                attributeDataMultiValue = new Dictionary<string, List<int>>(StringComparer.OrdinalIgnoreCase);

            if (attributeData.Count == 0 && attributeDataMultiValue.Count == 0)
            {
                throw new ArgumentException("attributeData/attributeDataMultiValue");
            }

            var translatedAttributes = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            foreach (var attribute in attributeData)
            {
                PseudoAttributeTranslator.PseudoAttributeTranslatorDelegate valueTranslator;

                var value = PseudoAttributeTranslator.PseudoAttributeMethodMap.TryGetValue(attribute.Key.ToLower(), out valueTranslator) ? valueTranslator(attribute.Value) : attribute.Value;

                ProfileProperty property;
                if (!AttributeConfigReader.Instance.AttributeWhitelistDictionary.TryGetValue(attribute.Key.ToLower(), out property))
                {
                    throw new Exception(String.Format("property {0} must have either an assoicated attribute or a pseudoAttribute method", attribute.Key));
                }

                translatedAttributes.Add(property.AttributeName, value);
            }

            foreach (var multiValueAttribute in attributeDataMultiValue)
            {
                ProfileProperty property;
                if (!AttributeConfigReader.Instance.AttributeWhitelistDictionary.TryGetValue(multiValueAttribute.Key.ToLower(), out property))
                {
                    throw new Exception(String.Format("property {0} must have either an assoicated attribute or a pseudoAttribute method", multiValueAttribute.Key));
                }

                var totalValue = multiValueAttribute.Value.Sum();

                translatedAttributes.Add(property.AttributeName, totalValue);
            }

		    return translatedAttributes;
		}
        
        internal static string GetGenderDescription(Brand brand, int genderMask, bool isSeeking)
		{
			if (genderMask == Matchnet.Constants.NULL_INT)
			{
				return null;
			}
			var genderValues = Enum.GetValues(typeof(Matchnet.GenderMask));
			var genderNames = Enum.GetNames(typeof(Matchnet.GenderMask));
			var i = 0;
			foreach (int gender in genderValues)
			{
				if ((genderMask & gender) == gender &&
					((genderNames[i].Contains("Seeking") && isSeeking) ||
					  (!genderNames[i].Contains("Seeking") && !isSeeking)))
				{
                    return genderNames[i].Replace("Seeking", "");
                    /*todo - this block returns localized gender string, but existing clients
                     are testing on old literal values "female" and "male"
                     need to inroduce a new property like "GenderText" for localized.*/
                    //var genderDescription = genderNames[i].Replace("Seeking", "");
                    //genderDescription = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global,
                    //                                           genderDescription);
				    //return genderDescription;
				}
				i++;
			}
			return String.Empty;
		}

        // the (untested) code below would return localized version of male/female, i.e. Man/Woman.  
        // This can't be changed yet without breaking the client that depend on the values "Male" and "Female"
        // Checking this in commented so we can use it later - BG
        //
        //internal static string GetGenderDescription(Brand brand, int genderMask, bool isSeeking)
        //{
        //    if (genderMask == Matchnet.Constants.NULL_INT)
        //    {
        //        return null;
        //    }
        //    var genderValues = Enum.GetValues(typeof(Matchnet.GenderMask));
        //    var genderNames = Enum.GetNames(typeof(Matchnet.GenderMask));
        //    var i = 0;
        //    foreach (int gender in genderValues)
        //    {
        //        if ((genderMask & gender) == gender)
        //        {
        //            if (!isSeeking && !genderNames[i].Contains("Seeking")) // looking for gender and found a match
        //            {
        //                var foundGender = genderNames[i];
        //                var key = foundGender.ToLowerInvariant() == "male" ? "SEEKING_MALE" : "SEEKING_FEMALE";
        //                return ResourceProvider.Instance.GetResourceValue(brand.Site, ResourceGroupEnum.Global, key);
        //            }
        //            if (isSeeking && genderNames[i].Contains("Seeking")) // looking for seeking gender and found a match
        //            {
        //                var foundGender = genderNames[i];
        //                var key = foundGender.ToLowerInvariant() == "seekingmale" ? "SEEKING_MALE" : "SEEKING_FEMALE";
        //                return ResourceProvider.Instance.GetResourceValue(brand.Site, ResourceGroupEnum.Global, key);
        //            }
        //        }
        //        i++;
        //    }
        //    return String.Empty;
        //}


		/// <summary>
		/// returns a bitmask value of all genders, or if isSeeking is set, returns a bitmask of a all "seeking" genders
		/// </summary>
		/// <param name="isSeeking"></param>
		/// <returns></returns>
		internal static int GetGenderBitmask(bool isSeeking)
		{
			var genderValues = Enum.GetValues(typeof(Matchnet.GenderMask));
			var genderNames = Enum.GetNames(typeof(Matchnet.GenderMask));
			var i = 0;
			var genderMask = 0;
			foreach (int gender in genderValues)
			{
				if ( (genderNames[i].Contains("Seeking") && isSeeking) || // if a "seeking" value and called wants "seeking" bits
					(!genderNames[i].Contains("Seeking") && !isSeeking)) // if "gender" value and caller wants "gender" bits
				{
					genderMask |= gender;
				}
				i++;
			}
			return genderMask;
		}
		
		
		internal static List<string> GetMaskDescriptions(string pAttributeName, int pMaskValue, Brand brand)
        {
            if (pMaskValue <= 0)
            {
                return new List<string>();
            }

            var attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(pAttributeName, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            if (attributeOptionCollection == null)
                return new List<string>();

            return (from AttributeOption attributeOption in attributeOptionCollection
                    where attributeOption.Value != 0 && attributeOption.MaskContains(pMaskValue)
                    let resourceValue = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global, attributeOption.ResourceKey)
                    select resourceValue ?? attributeOption.ResourceKey).ToList();
        }

        internal static string GetFirstDescription(List<string> items)
        {
            return items.Count > 0 ? items[0] : String.Empty;
        }

        internal static string GetLocation(Brand brand, Int32 regionId, bool fullCountry)
        {
            // REMOVING SP-895 CODE TO STOP JDIOS APP FROM CRASHING. RE-INSERT SP-895 CODE WHEN JDIOS APP IS FIXED TO NOT CRASH. -11.17.2015 arod
            var regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionId, brand.Site.LanguageID);
            var city = regionLanguage.CityName;
            var state = regionLanguage.StateAbbreviation;
            if (state == String.Empty)
                state = regionLanguage.StateDescription;
            //Log.DebugFormat("Location retrieved for regionId:{0}, brandId:{1} languageId:{2}", regionId, brand.BrandID, brand.Site.LanguageID);
            return String.Format("{0}, {1}", city, state);
        }

        internal static bool isDefaultRegion(Brand brand, RegionLanguage regionLanguage)
        {
            if (regionLanguage.CountryRegionID == brand.Site.DefaultRegionID)
            {
                return true;
            }
            return false;
        }

        internal static string GetZipCode(Brand brand, Int32 regionId)
        {
            var regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionId, brand.Site.LanguageID);
            return regionLanguage.PostalCode;
        }
        
        internal static int? GetAge(DateTime birthDate)
        {
            if (birthDate == DateTime.MinValue) return null;

            int age = DateTime.Today.Year - birthDate.Year;
            if (birthDate > DateTime.Today.AddYears(-age))
                age--;
            return (int?) age;
        }

		/// <summary>
		/// Spark has legacy code that uses Int32.MinValue + 1 to represent null ints.  Use this method to convert to actual nullable ints
		/// </summary>
		/// <param name="intValue">the value</param>
		/// <returns>nullable version of the given int</returns>
		internal static int? GetNullableInt(int intValue)
		{
			return GetNullableInt(intValue, false);
		}

		/// <summary>
		/// Spark has legacy code that uses Int32.MinValue + 1 to represent null ints.  Use this method to convert to actual nullable ints
		/// </summary>
		/// <param name="intValue">the value</param>
		/// <param name="zeroMeansNull">setting this true will turn 0 value into null</param>
		/// <returns>nullable version of the given int</returns>
		internal static int? GetNullableInt(int intValue, bool zeroMeansNull)
    	{
    		int? nullableInt;
			if (intValue < Int32.MinValue + 2 || (intValue == 0 && zeroMeansNull)) // Int32.MinValue + 2 <- catching minValue and MinValue + 1 just in case
			{
				nullableInt = null;
			}
			else
			{
				nullableInt = intValue;
			}
    		return nullableInt;
    	}

        internal static object GetGamEducationDescription(Brand brand, int attributeValue, bool p)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection("EducationLevel", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection != null)
            {
                var attributeOption = attributeOptionCollection[attributeValue];

                return attributeOption != null ? DfpAttributeHelper.GetAdAttributes("EducationLevel", attributeOption.Description) : "no_value";
            }

            return string.Empty;
        }
    }
}
