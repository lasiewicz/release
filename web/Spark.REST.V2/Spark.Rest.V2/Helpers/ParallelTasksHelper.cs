﻿using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Spark.Rest.V2.Helpers
{
    internal static class ParallelTasksHelper
    {
        private static Int32 MaxNumberOfThreads
        {
            get
            {
                Int32 maxNumberOfThreads = -1; //If it is -1, there is no limit on the number of concurrently running operations
                try
                {
                    maxNumberOfThreads = Int32.Parse(RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.MAX_NUMBER_OF_THREADS));
                }
                catch (Exception ex)
                {

                }
                return maxNumberOfThreads;
            }
        }

        internal static ParallelOptions GetMaxDegreeOfParallelism()
        {
            ParallelOptions options = new ParallelOptions();
            options.MaxDegreeOfParallelism = MaxNumberOfThreads;
            return options;
        }
    }
}