﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Purchase.ValueObjects;
using Spark.Common.UPS;

namespace Spark.REST.Helpers
{
    internal static class SubscriptionHelper
    {
        public static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static string GetCurrencyAbbreviation(CurrencyType currencyType)
        {
            switch (currencyType)
            {
                case CurrencyType.USDollar:
                    return "USD";
                case CurrencyType.Euro:
                    return "EUR";
                case CurrencyType.CanadianDollar:
                    return "CAD";
                case CurrencyType.Pound:
                    return "GBP";
                case CurrencyType.AustralianDollar:
                    return "AUD";
                case CurrencyType.Shekels:
                    return "NIS";
                case CurrencyType.VerifiedByVisa:
                    return "VBV";
                default:
                    return "None";
            }
        }

        public static bool DoesPackageExistAtRowCol(int row, int col, PaymentJsonPackages packages)
        {
            return packages.Any(package => Convert.ToInt32(package.Items["ColumnGroup"]) == col && Convert.ToInt32(package.Items["ListOrder"]) == row);
        }

        public static DateTime ConvertEpochMillisToDateTime(long timeMillis)
        {
            double expiryMillisDouble = Convert.ToDouble(timeMillis);
            DateTime expiryDateTime = UnixEpoch + TimeSpan.FromMilliseconds(expiryMillisDouble);
            return expiryDateTime;
        }
    }
}