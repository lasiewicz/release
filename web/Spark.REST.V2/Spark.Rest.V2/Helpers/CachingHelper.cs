﻿using System;
using Matchnet;
using Matchnet.Caching.CacheBuster;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Spark.Caching;
using Spark.Logger;
using Spark.REST.Helpers;

namespace Spark.Rest.Helpers
{
    public class CachingHelper
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(CachingHelper));
        private const string ScenarioCacheCleared = "Scenario Cache Cleared";
        private const string ScenarioCacheNotCleared = "Scenario Cache Not Cleared";
        private static ICaching _webCachebucket = null;

        public static ICaching WebCachebucket
        {
            get
            {
                if (null == _webCachebucket)
                {
                    _webCachebucket = MembaseCaching.GetSingleton(RuntimeSettings.GetMembaseConfigByBucket("webcache"));
                }
                return _webCachebucket;
            }
            set { _webCachebucket = value; }
        }


        public enum CacheType
        {
            RegistrationScenarios = 0
        }

        public string ClearCache(CacheType cacheType)
        {
            var result = string.Empty;

            switch(cacheType)
            {
                case CacheType.RegistrationScenarios:
                    result = ClearRegistrationScenariosCache();
                    break;
            }

            return result;
        }


        private string ClearRegistrationScenariosCache()
        {
            string result = ScenarioCacheCleared;
            
            try
            {
                string scenariosCacheKey = RegistrationMetadataSA.Instance.GetScenarioCacheKey();
                string adminScenariosCacheKey = RegistrationMetadataSA.Instance.GetAdminScenariosCacheKey();

                var cacheBuster = new CacheBuster();
                var scenariosCacheBusted = cacheBuster.BustCache(scenariosCacheKey);
                var adminScenariosCacheBusted = cacheBuster.BustCache(adminScenariosCacheKey);

                if(!scenariosCacheBusted || !adminScenariosCacheBusted)
                {
                    result = ScenarioCacheNotCleared;
                }
            }
            catch (Exception ex)
            {
                Log.LogError("Scenario cache not busted", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                result = ScenarioCacheNotCleared;
            }

            return result;
        }
    }
}