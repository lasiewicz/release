﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Caching;
using System.Runtime.Serialization;
using System.Text;
using System.Web.Routing;
using System.Xml.Linq;
using Spark.Logger;
using Spark.Rest.Configuration;
using Spark.Rest.Entities.Documentation;
using Spark.REST.Models;

#endregion

namespace Spark.Rest.Helpers
{
    internal static class DocumentationHelper
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(DocumentationHelper));

        internal static Dictionary<string, List<Route>> GetControllerRoutes()
        {
            var cache = MemoryCache.Default;
            var controllerRoutes = cache["ControllerRoutes"] as Dictionary<string, List<Route>>;

            if (controllerRoutes != null)
                return controllerRoutes;

            controllerRoutes = new Dictionary<string, List<Route>>();

            // if your routes aren't showing up, check if the Controller class is in Spark.REST.Controllers namespace not V2.
            var routes = (from route in RouteTable.Routes
                where ((Route) route).Defaults != null
                select (Route) route).ToList();

            var controllerNames = (from route in routes
                orderby route.Defaults["controller"].ToString() ascending
                select route.Defaults["controller"].ToString()).Distinct(
                    StringComparer.CurrentCultureIgnoreCase);
            foreach (var name in controllerNames)
            {
                try
                {
                    var name1 = name;

                    //get routes for controller
                    var filteredRoutes = 
                        (from route in routes
                            where (route.Defaults["controller"].ToString().ToLower() == name1.ToLower()) &&
                                  HasAttributeType(name1, route.Defaults["action"].ToString(),
                                      typeof (ApiMethod))
                            select route).ToList();

                    if (filteredRoutes.Count > 0)
                    {
                        controllerRoutes.Add(char.ToUpper(name[0]) + name.Substring(1), filteredRoutes);
                    }
                }
                catch (Exception exception)
                {
                    // there are times when a merge happens where a route looks for a missing controller or method. use this try catch to supress
                    Log.LogError(string.Format("Error finding routes for {0}", name), exception, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                }
            }

            //cache.Add("ControllerRoutes", controllerRoutes, DateTime.Now.AddYears(1));

            return controllerRoutes;
        }

        internal static bool HasAttributeType(string controllerName, string methodName, Type attributeType)
        {
            try
            {
                if (string.IsNullOrEmpty(controllerName)) throw new Exception("Missing controllername parameter.");
                if (string.IsNullOrEmpty(methodName))
                    throw new Exception(string.Format("Missing methodName parameter. Controller:{0}", controllerName));
                if (attributeType == null)
                    throw new Exception(string.Format("Missing attributeType parameter. Controller:{0}", controllerName));

                Type controllerType = null;

                try
                {
                    controllerType = Assembly.GetExecutingAssembly().GetType("Spark.REST.Controllers."
                                                                                 + controllerName + "Controller",
                                                                                 true,
                                                                                 true);
                }
                catch(Exception ex)
                {
                    controllerType = Assembly.GetExecutingAssembly().GetType("Spark.REST.V2.Controllers."
                                                                                 + controllerName + "Controller",
                                                                                 true,
                                                                                 true);
                }

                var attributes =
                    controllerType.GetMethod(methodName,
                                             BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance |
                                             BindingFlags.IgnoreCase).GetCustomAttributes(true);

                if (attributes.Any(attribute => attribute.GetType() == attributeType))
                {
                    return true;
                }
            }
            catch (Exception exception)
            {
                const string formatError = "Error looking up method attributes. Probable mismtach in routes and controller. controller:{0}, method:{1}, attributeType:{2}";
                Log.LogError(string.Format(formatError, controllerName, methodName, attributeType), exception, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                //throw new Exception(string.Format(formatError, controllerName, methodName, attributeType), exception);
            }

            return false;
        }

        internal static string GetAttributePropertyValue(string controllerName, string methodName, Type attributeType,
                                                         string propertyName)
        {
            try
            {
                Type controllerType = null;

                try
                {
                    controllerType = Assembly.GetExecutingAssembly().GetType("Spark.REST.Controllers."
                                                                              + controllerName + "Controller",
                                                                              true,
                                                                              true);

                }
                catch (Exception ex)
                {
                    controllerType = Assembly.GetExecutingAssembly().GetType("Spark.REST.V2.Controllers."
                                                                                 + controllerName + "Controller",
                                                                                 true,
                                                                                 true);
                }

                var attributes =
                    controllerType.GetMethod(methodName,
                                             BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance |
                                             BindingFlags.IgnoreCase).GetCustomAttributes(true);

                foreach (var attribute in attributes)
                {
                    if (attribute.GetType() != attributeType) continue;
                    var property = attribute.GetType().GetProperty(propertyName);
                    if (property == null)
                        throw new Exception("Property," + propertyName + " is not defined in attribute," +
                                            attributeType);
                    var value = property.GetValue(attribute, null);
                    // there are response types referenced from other assemblies like Spark.Common
                    // that requires using the fully qualified name to resolve
                    return attributeType.Name == "ApiMethod" ? ((Type)(value)).AssemblyQualifiedName : Convert.ToString(value);
                }

                return string.Empty;
            }
            catch (Exception exception)
            {
                throw new Exception("HasAttributeType controllerName:" + controllerName + ", methodName:" + methodName
                                    + ", attributeType:" + attributeType + " Check in Controller and Global routes",
                                    exception);
            }
        }

        internal static Dictionary<string, object> GetParameters(string controllerName, string methodName, Dictionary<string, XmlCommentParams> commentParams)
        {
            try
            {
                Dictionary<string, object> result = null;
                Type controllerType = null;

                try
                {
                    controllerType = Assembly.GetExecutingAssembly().GetType("Spark.REST.Controllers."
                                                                              + controllerName + "Controller",
                                                                              true,
                                                                              true);
                }
                catch (Exception ex)
                {
                    controllerType = Assembly.GetExecutingAssembly().GetType("Spark.REST.V2.Controllers."
                                                                                 + controllerName + "Controller",
                                                                                 true,
                                                                                 true);
                }

                var methodInfo = controllerType.GetMethod(methodName,
                                                          BindingFlags.Public | BindingFlags.Static |
                                                          BindingFlags.Instance | BindingFlags.IgnoreCase);

                var parameters = methodInfo.GetParameters();

                if (parameters.Length > 0)
                {
                    result = GetParamTypeProperties(parameters, commentParams);
                }
                else
                {
                    result = new Dictionary<string, object>();
                }

                return result;
            }
            catch (Exception exception)
            {
                throw new Exception("GetParameters controllerName:" + controllerName + ", methodName:" + methodName,
                                    exception);
            }
        }

        internal static Dictionary<string, object> GetTypeProperties(Type type, Dictionary<string, XmlCommentParams> commentParams)
        {
            var parameterDict = new Dictionary<string, object>();

            var fieldInfos = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (var fieldInfo in fieldInfos)
            {
                if (fieldInfo.CanRead && fieldInfo.CanWrite &&
                    !HasAttribute(fieldInfo, typeof(IgnoreDataMemberAttribute)))
                {
                    AddParamTypeInfo(fieldInfo.PropertyType, fieldInfo.Name, parameterDict, commentParams);

                }
            }

            return parameterDict;
        }

        internal static Dictionary<string, object> GetParamTypeProperties(ParameterInfo[] methodParams, Dictionary<string, XmlCommentParams> commentParams)
        {
            var parameterDict = new Dictionary<string, object>();

            foreach (ParameterInfo pInfo in methodParams)
            {
                //only use certain types to check for properties as params
                if (pInfo.ParameterType.IsSubclassOf(typeof(RequestBase)))
                {
                    var fieldInfos = pInfo.ParameterType.GetProperties(BindingFlags.Instance | BindingFlags.Public);

                    foreach (var fieldInfo in fieldInfos)
                    {
                        if (fieldInfo.CanRead && fieldInfo.CanWrite &&
                            !HasAttribute(fieldInfo, typeof(IgnoreDataMemberAttribute)))
                        {
                            AddParamTypeInfo(fieldInfo.PropertyType, fieldInfo.Name, parameterDict, commentParams);

                        }
                    }
                }
                else
                {
                    AddParamTypeInfo(pInfo.ParameterType, pInfo.Name, parameterDict, commentParams);
                }
            }

            return parameterDict;
        }

        internal static void AddParamTypeInfo(Type type, string paramName, Dictionary<string, object> parameterDict, Dictionary<string, XmlCommentParams> commentParams)
        {
            try
            {
                var friendlyTypeName = GetFriendlyTypeName(type);
                parameterDict[paramName] = friendlyTypeName;

                if (commentParams != null)
                {
                    if (type.IsEnum)
                    {
                        if (!commentParams.ContainsKey(paramName.ToLower()))
                        {
                            commentParams[paramName.ToLower()] = new XmlCommentParams();
                        }

                        if (string.IsNullOrWhiteSpace(commentParams[paramName.ToLower()].Values))
                        {
                            StringBuilder sbValues = new StringBuilder();
                            foreach (var e in Enum.GetValues(type))
                            {
                                sbValues.AppendLine(((int)e).ToString() + ": " + e.ToString());
                            }
                            commentParams[paramName.ToLower()].Values = sbValues.ToString().Replace("  ", "&nbsp;&nbsp;").Replace("\n", "<br/>");
                        }
                    }
                    else if (type.Equals(typeof(Boolean)))
                    {
                        if (!commentParams.ContainsKey(paramName.ToLower()))
                        {
                            commentParams[paramName.ToLower()] = new XmlCommentParams();
                        }

                        if (string.IsNullOrWhiteSpace(commentParams[paramName.ToLower()].Values))
                        {
                            commentParams[paramName.ToLower()].Values = "true, false";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new Exception(
                    "Error adding to parameters dictionary for type:" + type + ", field:" + paramName,
                    exception);
            }
        }

        internal static bool HasAttribute(Type fieldType, Type attributeType)
        {
            var attributes = fieldType.GetCustomAttributes(true);
            foreach (var attribute in attributes)
            {
                if (attribute.GetType() == attributeType)
                {
                    return true;
                }
            }
            return false;
        }

        internal static bool HasAttribute(PropertyInfo propertyInfo, Type attributeType)
        {
            var attributes = propertyInfo.GetCustomAttributes(true);
            foreach (var attribute in attributes)
            {
                if (attribute.GetType() == attributeType)
                {
                    return true;
                }
            }
            return false;
        }

        internal static Response GetResponse(string responseName)
        {
            var cache = MemoryCache.Default;

            var response = cache[responseName.ToLower()] as Response;

            if (response == null)
                throw new Exception("Response info not found in cache. Name:" + responseName);

            return response;
        }

        internal static Response GetResponse(Type type)
        {
            var response = new Response
            {
                Type = type
            };

            string friendlyName;

            if (response.Type.IsGenericType && response.Type.FullName.Contains("System.Collections"))
            {
                response.IsCollection = true;
                response.CollectionOfType = response.Type.GetGenericArguments()[0];
                response.KeyValuePairs = GetTypeProperties(response.CollectionOfType, null);
            }
            else
            {
                response.IsCollection = false;
                response.KeyValuePairs = GetTypeProperties(response.Type, null);
            }

            response.FriendlyName = GetFriendlyTypeName(type);
            response.CachedName = response.FriendlyName.Replace("&gt;", "").Replace("&lt;", "").Replace(".", "").Replace(",", "").Replace(" ", "");

            var cache = MemoryCache.Default;
            cache.Remove(response.CachedName);
            cache.Add(response.CachedName.ToLower(), response, DateTime.Now.AddYears(1));

            return response;
        }

        internal static Response GetResponse(string controllerName, string methodName)
        {
            try
            {
                return
                    GetResponse(Type.GetType(GetAttributePropertyValue(controllerName, methodName, typeof(ApiMethod),
                                                                       "ResponseType")));
            }
            catch (Exception exception)
            {
                throw new Exception("GetResponse controllerName:" + controllerName + ", methodName:" + methodName,
                                    exception);
            }
        }

        internal static string GetFriendlyTypeName(Type type)
        {
            if (type.IsGenericParameter)
            {
                return type.Name;
            }

            if (!type.IsGenericType)
            {
                return type.Name;
            }

            var builder = new System.Text.StringBuilder();
            var name = type.Name;
            var index = name.IndexOf("`");
            builder.Append(name.Substring(0, index));
            builder.Append("&lt;");
            var first = true;
            foreach (var arg in type.GetGenericArguments())
            {
                if (!first)
                {
                    builder.Append(", ");
                }
                builder.Append(GetFriendlyTypeName(arg));
                first = false;
            }
            builder.Append("&gt;");
            return builder.ToString();
        }

        internal static XmlComments GetControllerComment(string controllerName)
        {
            XmlComments xmlComments = null;
            var xmlCommentData = GetXmlCommentData();
            var members = xmlCommentData.Root.Element("members");
            foreach (var xe in members.Elements())
            {
                var name = xe.Attribute("name").Value.ToLower();
                if (name.StartsWith("t:"))
                {
                    if (name.Contains(controllerName.ToLower()))
                    {
                        var xElements = xe.Descendants();
                        xmlComments = GetXmlComments(xElements);
                        break;
                    }
                }
            }
            return xmlComments;
        }

        internal static XmlComments GetMethodComment(string controllerName, string methodName)
        {
            XmlComments xmlComments = new XmlComments();
            var xmlCommentData = GetXmlCommentData();
            var members = xmlCommentData.Root.Element("members");
            foreach (var xe in members.Elements())
            {
                var memberName = xe.Attribute("name").Value.ToLower();
                if (memberName.StartsWith("m:"))
                {
                    //if (memberName.Contains(controllerName.ToLower()) && memberName.Contains(methodName.ToLower()))
                    if (memberName.Contains("controller") && memberName.Contains(controllerName.ToLower()) && memberName.Contains("." + methodName.ToLower() + "("))
                    {
                        var xElements = xe.Descendants();
                        xmlComments = GetXmlComments(xElements);
                        Type obsoleteAttrType = typeof (ObsoleteAttribute);
                        if (HasAttributeType(controllerName, methodName, obsoleteAttrType))
                        {
                            string obsoleteWorkAround = GetAttributePropertyValue(controllerName, methodName,
                                obsoleteAttrType, "Message");
                            if (string.IsNullOrEmpty(obsoleteWorkAround))
                            {
                                obsoleteWorkAround = "This call is obsolete.";
                            }
                            if (null == xmlComments) xmlComments = new XmlComments();
                            xmlComments.Obsolete = obsoleteWorkAround;
                        }
                        break;
                    }
                }
            }
            return xmlComments;
        }

        internal static XmlComments GetXmlComments(IEnumerable<XElement> xElements)
        {
            XmlComments xmlComments = null;
            foreach (var xElement in xElements)
            {
                var xName = xElement.Name.LocalName.ToLower();
                switch (xName)
                {
                    case "summary":
                        if (null == xmlComments) xmlComments = new XmlComments();
                        xmlComments.Summary = xElement.Value.Replace("  ","&nbsp;&nbsp;").Replace("\n","<br/>").Replace("[br]", "<br/>");
                        break;
                    case "returns":
                        if (null == xmlComments) xmlComments = new XmlComments();
                        xmlComments.Returns = xElement.Value.Replace("  ", "&nbsp;&nbsp;").Replace("\n", "<br/>").Replace("[br]", "<br/>");
                        break;
                    case "remarks":
                        if (null == xmlComments) xmlComments = new XmlComments();
                        xmlComments.Remarks = xElement.Value.Replace("  ", "&nbsp;&nbsp;").Replace("\n", "<br/>").Replace("[br]", "<br/>");
                        break;
                    case "example":
                        if (null == xmlComments) xmlComments = new XmlComments();
                        xmlComments.Example = xElement.Value.Replace("  ", "&nbsp;&nbsp;").Replace("\n", "<br/>").Replace("[br]", "<br/>");
                        break;
                    case "value":
                        if (null == xmlComments) xmlComments = new XmlComments();
                        xmlComments.Value = xElement.Value.Replace("  ", "&nbsp;&nbsp;").Replace("\n", "<br/>").Replace("[br]", "<br/>");
                        break;
                    case "exception":
                        if (null == xmlComments) xmlComments = new XmlComments();
                        var exString = string.Empty;
                        if (null != xElement.Attribute("cref"))
                        {
                            exString = xElement.Attribute("cref").Value.Replace("T:", "");
                            if (!string.IsNullOrEmpty(xElement.Value))
                            {
                                exString += ": ";
                            }
                        }
                        exString += xElement.Value;
                        xmlComments.AddException(exString);
                        break;
                    case "param":
                        if (null == xmlComments) xmlComments = new XmlComments();
                        if (null != xElement.Attribute("name"))
                        {
                            var paramName = xElement.Attribute("name").Value;
                            XmlCommentParams param = new XmlCommentParams();
                            if (null != xElement.Attribute("values"))
                            {
                                param.Values = xElement.Attribute("values").Value.Replace("  ", "&nbsp;&nbsp;").Replace("\n", "<br/>").Replace("[br]", "<br/>");
                            }
                            if (null != xElement.Attribute("required"))
                            {
                                param.Required = bool.Parse(xElement.Attribute("required").Value);
                            }
                            if (!string.IsNullOrWhiteSpace(xElement.Value))
                            {
                                param.Description = xElement.Value.Replace("  ", "&nbsp;&nbsp;").Replace("\n", "<br/>").Replace("[br]", "<br/>");
                            }

                            xmlComments.AddParam(paramName.ToLower(), param);
                        }

                        break;
                    case "paramsadditionalinfo":
                        if (null == xmlComments) xmlComments = new XmlComments();
                        xmlComments.ParamsAdditionalInfo = xElement.Value.Replace("  ","&nbsp;&nbsp;").Replace("\n","<br/>").Replace("[br]", "<br/>");
                        break;
                    case "responseadditionalinfo":
                        if (null == xmlComments) xmlComments = new XmlComments();
                        xmlComments.ResponseAdditionalInfo = xElement.Value.Replace("  ", "&nbsp;&nbsp;").Replace("\n", "<br/>").Replace("[br]", "<br/>");
                        break;
                    case "permission":
                        if (null == xmlComments) xmlComments = new XmlComments();
                        var permString = string.Empty;
                        if (null != xElement.Attribute("cref"))
                        {
                            permString = xElement.Attribute("cref").Value.Replace("T:", "");
                            if (!string.IsNullOrEmpty(xElement.Value))
                            {
                                permString += ": ";
                            }
                        }
                        permString += xElement.Value;
                        xmlComments.AddPermission(permString);
                        break;
                    default:
                        break;
                }
            }
            return xmlComments;
        }

        internal static XDocument GetXmlCommentData()
        {
            var cache = MemoryCache.Default;
            var xmlComments = cache["xmlComments"] as XDocument;
            if (null == xmlComments)
            {
                var xmlFileName = Assembly.GetExecutingAssembly().EscapedCodeBase.ToLower().Replace(".dll", ".xml");
                xmlComments = XDocument.Load(xmlFileName);
                cache["xmlComments"] = xmlComments;
            }
            return xmlComments;
        }
    }

    public class XmlComments
    {
        private List<string> _exceptions = null;
        private List<string> _includes = null;
        private List<string> _paramRefs = null;
        private Dictionary<string, XmlCommentParams> _params = new Dictionary<string, XmlCommentParams>();
        private List<string> _permissions = null;

        public string Summary { get; set; }
        public string Returns { get; set; }
        public string Remarks { get; set; }
        public string Example { get; set; }
        public string Value { get; set; }
        public string Obsolete { get; set; }
        public string ParamsAdditionalInfo { get; set; }
        public string ResponseAdditionalInfo { get; set; }

        public List<string> Includes
        {
            get { return _includes; }
        }

        public List<string> Exceptions
        {
            get { return _exceptions; }
        }

        public List<string> ParamRefs
        {
            get { return _paramRefs; }
        }

        public List<string> Permissions
        {
            get { return _permissions; }
        }

        public Dictionary<string, XmlCommentParams> Params
        {
            get { return _params; }
        }

        public void AddIncludes(string _include)
        {
            if (null == _includes) _includes = new List<string>();
            _includes.Add(_include);
        }

        public void AddException(string _exception)
        {
            if (null == _exceptions) _exceptions = new List<string>();
            _exceptions.Add(_exception);
        }

        public void AddParamRef(string _paramRef)
        {
            if (null == _paramRefs) _paramRefs = new List<string>();
            _paramRefs.Add(_paramRef);
        }

        public void AddPermission(string _permission)
        {
            if (null == _permissions) _permissions = new List<string>();
            _permissions.Add(_permission);
        }

        public void AddParam(string key, XmlCommentParams param)
        {
            _params[key] = param;
        }
    }

    public class XmlCommentParams
    {
        public string Values { get; set; }
        public string Description { get; set; }
        public bool Required { get; set; }

        public XmlCommentParams()
        {
            Values = string.Empty;
            Description = string.Empty;
            Required = false;
        }
    }
}