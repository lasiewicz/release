﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.MingleMigration.ServiceAdapters;
using Spark.MingleMigration.ValueObjects;
using Spark.Rest.V2.DataAccess.MingleMigration;
using Spark.Rest.V2.Entities.MingleMigration;
using Spark.Rest.V2.Models.Member;
using Spark.Rest.V2.Models.MingleMigration;
using Spark.SearchPreferences;

namespace Spark.Rest.V2.Helpers
{
    public static class MingleMigrationHelper
    {
        internal static void TranslateIncomingAttributesBeforeMapping(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue,
            out Dictionary<string, object> translatedAttributeData, out Dictionary<string, List<int>> translatedAttributeDataMultiValue)
        {
            if (attributeDataMultiValue == null)
            {
                attributeDataMultiValue = new Dictionary<string, List<int>>();
            }

            translatedAttributeData = new Dictionary<string, object>(attributeData);
            translatedAttributeDataMultiValue = new Dictionary<string, List<int>>(attributeDataMultiValue);

            MingleAttributeTranslator.MingleAttributeTranslatorDelegate valueTranslator;

            foreach (var attribute in attributeData)
            {
                if (MingleAttributeTranslator.MingleAttributeMethodMap.TryGetValue(attribute.Key.ToLower(), out valueTranslator))
                {
                    valueTranslator(translatedAttributeData, translatedAttributeDataMultiValue);
                }
            }

            foreach (var multiAttribute in attributeDataMultiValue)
            {
                if (MingleAttributeTranslator.MingleAttributeMethodMap.TryGetValue(multiAttribute.Key.ToLower(), out valueTranslator))
                {
                    valueTranslator(translatedAttributeData, translatedAttributeDataMultiValue);
                }
            }

            if (translatedAttributeData.ContainsKey(MingleAttributeConstants.USERDELETEDFLAG))
            {
                //remove the original flag value because we've shifted it into other attributes
                translatedAttributeData.Remove(MingleAttributeConstants.USERDELETEDFLAG);
            }
        }

        internal static MemberSearch GetSearchPreferencesFromRequest(Dictionary<string, object> attributeData,
            List<SearchPreference> searchPreferences,
            List<MultiValuedSearchPreference> searchPreferencesMultiValue,
            Brand brand, int memberId)
        {
            var searchPreferenceMapper = AttributeMapperSA.Instance.GetMingleSearchPreferenceMapper();
            var attributeMapper = AttributeMapperSA.Instance.GetMingleAttributeMapper();

            var genderMask = GetIntValueFromMappedAttributes(attributeData, AttributeConstants.GENDERMASK);
            var regionID = GetIntValueFromMappedAttributes(attributeData, AttributeConstants.REGIONID);
            var minAgePreference = MingleSearchPreferenceTranslator.GetPreferenceValue(searchPreferences, MingleSearchPreferenceConstants.MINAGE);
            var maxAgePreference = MingleSearchPreferenceTranslator.GetPreferenceValue(searchPreferences, MingleSearchPreferenceConstants.MAXAGE);
            var distancePreference = MingleSearchPreferenceTranslator.GetPreferenceValue(searchPreferences, MingleSearchPreferenceConstants.DISTANCE);

            var searchPreferenceCollection = new SearchPreferenceCollection();
            searchPreferenceCollection.Add(new SearchPreference { Name = SearchPreferenceConstants.SearchType, Weight = 0, Value = SearchTypeID.Region.ToString("d") });
            searchPreferenceCollection.Add(new SearchPreference { Name = SearchPreferenceConstants.OrderBy, Weight = 0, Value = QuerySorting.Popularity.ToString("d") });
            searchPreferenceCollection.Add(new SearchPreference { Name = SearchPreferenceConstants.GenderMask, Weight = 0, Value = genderMask.ToString() });
            searchPreferenceCollection.Add(new SearchPreference { Name = SearchPreferenceConstants.RegionID, Weight = 0, Value = regionID.ToString() });
            searchPreferenceCollection.Add(new SearchPreference { Name = SearchPreferenceConstants.Distance, Weight = 0, Value = distancePreference.Value });
            searchPreferenceCollection.Add(new SearchPreference { Name = SearchPreferenceConstants.MinAge, Weight = 0, Value = minAgePreference.Value });
            searchPreferenceCollection.Add(new SearchPreference { Name = SearchPreferenceConstants.MaxAge, Weight = 0, Value = maxAgePreference.Value });
            var allSearchPreferenceConstants = SearchPreferenceConstants.GetAllSearchPreferenceConstants();

            foreach (var preference in searchPreferences)
            {
                if (Convert.ToInt32(preference.Value) != -1)
                {
                    var mappedPreference = searchPreferenceMapper.GetSearchPreference(preference.Name, brand.Site.SiteID);
                    if (!allSearchPreferenceConstants.Contains(mappedPreference.BHPreferenceName))
                    {
                        searchPreferenceCollection.Add(new SearchPreference
                        {
                            Name = mappedPreference.BHPreferenceName,
                            Weight = preference.Weight,
                            Value = preference.Value
                        });
                    }
                }
            }

            foreach (var preference in searchPreferencesMultiValue)
            {
                if (!(preference.Values.Count == 1 && preference.Values.First() == -1))
                {

                    var mappedPreference = searchPreferenceMapper.GetSearchPreference(preference.Name, brand.Site.SiteID);
                    if (!allSearchPreferenceConstants.Contains(mappedPreference.BHPreferenceName))
                    {
                        var mappedOptions =
                            attributeMapper.GetMingleOptionsForAttributeAndGroup(
                                mappedPreference.RelatedMingleAttributeName,
                                brand.Site.SiteID);

                        int totalPreferenceValue = 0;
                        foreach (var suppliedOption in preference.Values)
                        {
                            var mappedOption =
                                (from o in mappedOptions where o.Value == suppliedOption select o).First();
                            totalPreferenceValue = totalPreferenceValue + mappedOption.BHValue;
                        }

                        searchPreferenceCollection.Add(new SearchPreference
                        {
                            Name = mappedPreference.BHPreferenceName,
                            Weight = preference.Weight,
                            Value = totalPreferenceValue.ToString()
                        });
                    }
                }
            }

            return new MemberSearch(memberId, brand.Site.Community.CommunityID, MemberSearchCollection.DEFAULTSEARCHNAME, true, searchPreferenceCollection);

        }

        internal static int GetIntValueFromMappedAttributes(Dictionary<string, object> attributeData, string key)
        {
            int value = 0;

            if (attributeData == null || !attributeData.ContainsKey(key) || !int.TryParse(attributeData[key].ToString(), out value))
            {
                throw new ArgumentException(String.Format("Attribute data does not contain {0} in the correct format", key));
            }

            return value;
        }

        internal static string ValidateRequest(MingleRegistrationRequest request)
        {
            var errorString = new StringBuilder();

            if (request.MemberId <= 0)
            {
                errorString.Append("MemberId must be supplied" + Environment.NewLine);
            }
            if (string.IsNullOrEmpty(request.IpAddress))
            {
                errorString.Append("IpAddress must be supplied" + Environment.NewLine);
            }
            if (request.AttributeData == null || request.AttributeData.Count == 0)
            {
                errorString.Append("AtributeData can not be empty" + Environment.NewLine);
            }
            else
            {
                if (!request.AttributeData.ContainsKey(MingleAttributeConstants.REGIONID))
                {
                    errorString.Append("AtributeData must contain " + MingleAttributeConstants.REGIONID + Environment.NewLine);
                }
                if (!request.AttributeData.ContainsKey(MingleAttributeConstants.SOURCE))
                {
                    errorString.Append("AtributeData must contain " + MingleAttributeConstants.SOURCE + Environment.NewLine);
                }
                if (!request.AttributeData.ContainsKey(MingleAttributeConstants.BIRTHDATE))
                {
                    errorString.Append("AtributeData must contain " + MingleAttributeConstants.BIRTHDATE + Environment.NewLine);
                }
                if (!request.AttributeData.ContainsKey(MingleAttributeConstants.MINGLEUSERID))
                {
                    errorString.Append("AtributeData must contain " + MingleAttributeConstants.MINGLEUSERID + Environment.NewLine);
                }
            }
            if (request.AttributeDataMultiValue == null || request.AttributeDataMultiValue.Count == 0)
            {
                errorString.Append("AttributeDataMultiValue can not be empty" + Environment.NewLine);
            }
            else
            {
                if (!request.AttributeDataMultiValue.ContainsKey(MingleAttributeConstants.GENDERMASK))
                {
                    errorString.Append("AttributeDataMultiValue must contain " + MingleAttributeConstants.GENDERMASK + Environment.NewLine);
                }
            }
            if (request.SearchPreferences == null || request.SearchPreferences.Count == 0)
            {
                errorString.Append("SearchPreferences can not be empty" + Environment.NewLine);
            }
            else
            {
                if (!request.SearchPreferences.Any(sp => sp.Name == MingleSearchPreferenceConstants.DISTANCE))
                {
                    errorString.Append("SearchPreferences must contain " + MingleSearchPreferenceConstants.DISTANCE + Environment.NewLine);
                }
                if (!request.SearchPreferences.Any(sp => sp.Name == MingleSearchPreferenceConstants.MINAGE))
                {
                    errorString.Append("SearchPreferences must contain " + MingleSearchPreferenceConstants.MINAGE + Environment.NewLine);
                }
                if (!request.SearchPreferences.Any(sp => sp.Name == MingleSearchPreferenceConstants.MAXAGE))
                {
                    errorString.Append("SearchPreferences must contain " + MingleSearchPreferenceConstants.MAXAGE + Environment.NewLine);
                }
            }

            return errorString.ToString();
        }


        internal static string ValidateRiskInquiryRequest(MingleRegistrationRiskInquiryRequest request)
        {
            var errorString = new StringBuilder();

            if (request.MemberId <= 0)
            {
                errorString.Append("MemberId must be supplied" + Environment.NewLine);
            }
            if (string.IsNullOrEmpty(request.IpAddress))
            {
                errorString.Append("IpAddress must be supplied" + Environment.NewLine);
            }
            if (string.IsNullOrEmpty(request.RegistrationSessionId))
            {
                errorString.Append("SessionID must be supplied" + Environment.NewLine);
            }
            if (string.IsNullOrEmpty(request.EmailAddress))
            {
                errorString.Append("EmailAddress must be supplied" + Environment.NewLine);
            }
            if (request.Brand == null)
            {
                errorString.Append("Brand must be supplied" + Environment.NewLine);
            }
            if (request.AttributeData == null || request.AttributeData.Count == 0)
            {
                errorString.Append("AtributeData can not be empty" + Environment.NewLine);
            }
            //else
            //{
            //    if (!request.AttributeData.ContainsKey(MingleAttributeConstants.REGIONID))
            //    {
            //        errorString.Append("AtributeData must contain " + MingleAttributeConstants.REGIONID + Environment.NewLine);
            //    }
            //    if (!request.AttributeData.ContainsKey(MingleAttributeConstants.SOURCE))
            //    {
            //        errorString.Append("AtributeData must contain " + MingleAttributeConstants.SOURCE + Environment.NewLine);
            //    }
            //    if (!request.AttributeData.ContainsKey(MingleAttributeConstants.BIRTHDATE))
            //    {
            //        errorString.Append("AtributeData must contain " + MingleAttributeConstants.BIRTHDATE + Environment.NewLine);
            //    }
            //    if (!request.AttributeData.ContainsKey(MingleAttributeConstants.MINGLEUSERID))
            //    {
            //        errorString.Append("AtributeData must contain " + MingleAttributeConstants.MINGLEUSERID + Environment.NewLine);
            //    }
            //}
           
            return errorString.ToString();
        }

    }
}