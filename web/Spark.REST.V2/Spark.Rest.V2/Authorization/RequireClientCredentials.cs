﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.DataAccess.Applications;

namespace Spark.Rest.Authorization
{
	public class RequireClientCredentials : FilterAttribute, IAuthorizationFilter
	{
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(RequireClientCredentials));

		private static readonly bool OAuthEnabled;

		static RequireClientCredentials()
		{
			var oauthEnabled = ConfigurationManager.AppSettings["EnforceOAuth"];
			if (!String.IsNullOrEmpty(oauthEnabled))
			{
				OAuthEnabled = Boolean.Parse(oauthEnabled);
			}
		}

		public void OnAuthorization(AuthorizationContext authorizationContext)
		{
			if (!OAuthEnabled)
				return;

		    var httpContextBase = authorizationContext.HttpContext;
		    if (!authorizationContext.RouteData.Values.ContainsKey("applicationId") && httpContextBase.Request.QueryString["applicationId"] == null)
            {
                Log.LogInfoMessage(string.Format("Missing application Id in request that requires client credentials {0}", httpContextBase.Request.Url), ErrorHelper.GetCustomData(httpContextBase));
                authorizationContext.Result = new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.InvalidApplicationId, "An application id (your client_id) is required for secured endpoints");
                return;
            }

		    int applicationId;
            if (!Int32.TryParse((string)authorizationContext.RouteData.Values["applicationId"], out applicationId) || applicationId < 1)
            {
                if (!Int32.TryParse(httpContextBase.Request.QueryString["applicationId"], out applicationId) || applicationId < 1)
                {
                    Log.LogInfoMessage(string.Format("No valid application Id in request that requires client credentials {0}",httpContextBase.Request.Url), ErrorHelper.GetCustomData(httpContextBase));
                    authorizationContext.Result = new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.InvalidApplicationId, "A valid application ID (client_id) is required for secured endpoints");
                }
            }

            //add appid for logging module
            authorizationContext.RequestContext.HttpContext.Items["tokenAppId"] = applicationId;

            // Verify only if it's passed in. It can be optional to support Implicit Grant Flow.
		    var clientSecret = httpContextBase.Request.QueryString["client_secret"];
		    if (clientSecret != null) clientSecret = clientSecret.Replace(" ", "+");

		    string failReason;
		    int failCode;
			if (!ApplicationAccess.ValidateClientCredentials(applicationId, clientSecret, out failReason, out failCode))
			{
                Log.LogInfoMessage(string.Format("Could not validate client credential require for request {0}.  Reason: {1}", httpContextBase.Request.Url, failReason), ErrorHelper.GetCustomData(httpContextBase));
                authorizationContext.Result = new SparkHttpUnauthorizedResult(failCode, String.Format("Authorization failed - {0}", failReason));
				return;
			}
		}
	}
}
