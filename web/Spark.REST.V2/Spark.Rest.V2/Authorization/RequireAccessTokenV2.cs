﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.REST.DataAccess.Applications;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;


namespace Spark.Rest.Authorization
{
	public class RequireAccessTokenV2 : FilterAttribute, IAuthorizationFilter
	{
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(RequireAccessTokenV2));

		private static readonly bool OAuthEnabled;

		static RequireAccessTokenV2()
		{
			var oauthEnabled = ConfigurationManager.AppSettings["EnforceOAuth"];
			if (!String.IsNullOrEmpty(oauthEnabled))
			{
				OAuthEnabled = Boolean.Parse(oauthEnabled);
			}
            if (!OAuthEnabled)
            {
                Log.LogWarningMessage("OAuth is not enabled", ErrorHelper.GetCustomData());
            }
		}

        private void SetAccessTokenValuesInHttpContext(string accessToken, AuthorizationContext authorizationContext)
        {
            DateTime tokenAccessExpiration;
            int tokenMemberId;
            int tokenAdminMemberId;
            int tokenAppId;
            string failReason;
            int failCode;
            ApplicationAccess.DecryptAccessToken(accessToken, out tokenMemberId, out tokenAppId,
                                                 out tokenAccessExpiration,
                                                 out failReason, out failCode, out tokenAdminMemberId);
            if (tokenMemberId > 0)
            {
                authorizationContext.RequestContext.HttpContext.Items["tokenMemberId"] = tokenMemberId;
            }

            if (tokenAdminMemberId > 0)
            {
                authorizationContext.RequestContext.HttpContext.Items["tokenAdminMemberId"] = tokenAdminMemberId;
            }

            if (tokenAppId > 0)
            {
                authorizationContext.RequestContext.HttpContext.Items["tokenAppId"] = tokenAppId;
            }

            authorizationContext.RequestContext.HttpContext.Items["tokenAccessExpiration"] = tokenAccessExpiration;
        }

		public virtual void OnAuthorization(AuthorizationContext authorizationContext)
		{
			if (!OAuthEnabled)
				return;

		    var httpContextBase = authorizationContext.HttpContext;
		    var accessToken = httpContextBase.Request.QueryString["access_token"];
			if (String.IsNullOrEmpty(accessToken))
			{
                Log.LogInfoMessage(string.Format("No access token for request {0}", httpContextBase.Request.Url), ErrorHelper.GetCustomData(httpContextBase));
                authorizationContext.Result = new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.MissingAccessToken, "An access token is required for access to the resource");
				return;
			}

            //too noisy
            //Log.DebugFormat("access token received in request: {0}", accessToken);
            SetAccessTokenValuesInHttpContext(accessToken, authorizationContext);
			string failReason;
		    int failCode;
		    if (!ApplicationAccess.ValidateAccessToken(accessToken, out failReason, out failCode))
			{
                Log.LogInfoMessage(string.Format("Could not validate access token for request {0}.  {1}", httpContextBase.Request.Url, failReason), ErrorHelper.GetCustomData(httpContextBase));
                authorizationContext.Result = new SparkHttpUnauthorizedResult(failCode,String.Format("Authorization failed - {0}", failReason));
				return;
			}
		}
	}
}