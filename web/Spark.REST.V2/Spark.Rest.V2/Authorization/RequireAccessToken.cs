﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.REST.DataAccess.Applications;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

namespace Spark.Rest.Authorization
{
	public class RequireAccessToken : FilterAttribute, IAuthorizationFilter
	{
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(RequireAccessToken));

		private static readonly bool OAuthEnabled;

		static RequireAccessToken()
		{
			var oauthEnabled = ConfigurationManager.AppSettings["EnforceOAuth"];
			if (!String.IsNullOrEmpty(oauthEnabled))
			{
				OAuthEnabled = Boolean.Parse(oauthEnabled);
			}
            if (!OAuthEnabled)
            {
                Log.LogWarningMessage("OAuth is not enabled", ErrorHelper.GetCustomData());
            }
		}


		public virtual void OnAuthorization(AuthorizationContext authorizationContext)
		{
			if (!OAuthEnabled)
				return;

		    var httpContextBase = authorizationContext.HttpContext;
		    var accessToken = httpContextBase.Request.QueryString["access_token"];
			if (String.IsNullOrEmpty(accessToken))
			{
                Log.LogInfoMessage(string.Format("No access token for request {0}", httpContextBase.Request.Url), ErrorHelper.GetCustomData(httpContextBase));
                authorizationContext.Result = new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.MissingAccessToken, "An access token is required for access to the resource");
				return;
			}

            Log.LogDebugMessage(string.Format("access token received in request: {0}", accessToken), ErrorHelper.GetCustomData(httpContextBase));

			if (!authorizationContext.RouteData.Values.ContainsKey("memberId"))
			{
                Log.LogInfoMessage(string.Format("No member ID in request {0}", httpContextBase.Request.Url), ErrorHelper.GetCustomData(httpContextBase));
                authorizationContext.Result = new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.InvalidMemberId, "A valid member ID is required for secured endpoints");
			}


			int memberId;
			if (!Int32.TryParse((string)authorizationContext.RouteData.Values["memberId"], out memberId) || memberId < 1)
			{
                Log.LogInfoMessage(string.Format("No member ID in endpoint for request {0}", httpContextBase.Request.Url), ErrorHelper.GetCustomData(httpContextBase));
                authorizationContext.Result = new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.InvalidMemberId, "A valid member ID is required for secured endpoints");
			}

			string failReason;
		    int failCode;
		    if (!ApplicationAccess.ValidateAccessToken(memberId, accessToken, out failReason, out failCode))
			{
                Log.LogInfoMessage(string.Format("Could not validate access token for request {0}.  {1}", httpContextBase.Request.Url, failReason), ErrorHelper.GetCustomData(httpContextBase));
                authorizationContext.Result = new SparkHttpUnauthorizedResult(failCode,String.Format("Authorization failed - {0}", failReason));
				return;
			}
		}
	}
}