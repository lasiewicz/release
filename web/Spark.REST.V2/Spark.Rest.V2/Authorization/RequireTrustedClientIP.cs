﻿using System.Web.Mvc;
using Matchnet.Caching.CacheBuster;
using Matchnet.Caching.CacheBuster.HTTPContextWrappers;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

namespace Spark.Rest.Authorization
{
    public class RequireTrustedClientIP : FilterAttribute, IAuthorizationFilter
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof (RequireTrustedClientIP));
        public string TrustedIpList { get; set; }
        static RequireTrustedClientIP(){}

        public virtual void OnAuthorization(AuthorizationContext authorizationContext)
        {
            CurrentRequest currentRequest = new CurrentRequest();
            CurrentServer currentServer = new CurrentServer();
            var ipConverter = new IPConverter();

            string ipAddress = currentRequest.ClientIP;
            int ipAddressAsNumber = ipConverter.ToNumber(ipAddress);

            if (ipAddressAsNumber == 0)
            {
                authorizationContext.Result = new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.MissingClientIP, "Cannot retieve client ip.");
                return;
            }

            var reader = new IPRangeWhitelistReader();
            string authorizationTrustedclientipwhitelistXml=TrustedIpList;
            if (string.IsNullOrEmpty(authorizationTrustedclientipwhitelistXml))
            {
                authorizationTrustedclientipwhitelistXml = "~/Authorization/XML/VisitorTrustedClientIPWhitelist.xml";
            }

            var configPath = currentServer.MapPath(authorizationTrustedclientipwhitelistXml);
            var whitelist = reader.GetWhitelistFromConfigFile(configPath);
            bool isTrusted = false;
            foreach (var ipRange in whitelist.IPRanges)
            {
                var beginningAddressAsNumber = ipConverter.ToNumber(ipRange.BeginningAddress);
                var endingAddressAsNumber = ipConverter.ToNumber(ipRange.EndingAddress);

                if (beginningAddressAsNumber != 0 && endingAddressAsNumber != 0 &&
                    ipAddressAsNumber >= beginningAddressAsNumber &&
                    ipAddressAsNumber <= endingAddressAsNumber)
                {
                    isTrusted = true;
                }
            }
            if (!isTrusted)
            {
                Log.LogInfoMessage(string.Format("Client ip: {0} not trusted for request {1}.", ipAddress, authorizationContext.HttpContext.Request.Url), ErrorHelper.GetCustomData());
                authorizationContext.Result = new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.InvalidClientIP, "Client IP is not trusted.");
                return;                
            }
        }

    }
}