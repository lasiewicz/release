﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

namespace Spark.Rest.Authorization
{
	public class RequirePayingMemberV2 : RequireAccessTokenV2
	{
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(RequirePayingMemberV2));

        protected static Dictionary<int, Brand> BrandLookup = new Dictionary<int, Brand>();

        public override void OnAuthorization(AuthorizationContext authorizationContext)
        {
            // endpoints requiring a subscription will also always need an access token
            base.OnAuthorization(authorizationContext); // check for access token


            int brandId = 0;
            if (!Int32.TryParse((string) authorizationContext.RouteData.Values["brandId"], out brandId) || brandId < 1)
            {
                brandId = 1003;
                // default to jdate to support old style URLs with no brand ID. TODO: remove this after clients are using only new URLs in production
                //Log.Info("A valid brand ID is required for subscribing member endpoints");
                //authorizationContext.Result = new HttpUnauthorizedResult("A valid brand ID is required for subscribing member endpoints");
            }

            // load the brand into a dictionary based on URL
            Brand brand;
            if (!BrandLookup.TryGetValue(brandId, out brand))
            {
                {
                    brand = BrandConfigSA.Instance.GetBrandByID(brandId);
                    if (brand == null)
                    {
                        authorizationContext.Result = new SparkHttpBadRequestResult((int) HttpSub400StatusCode.InvalidBrandId, String.Format("No brand found for host: {0}", brandId));
                        return;
                    }
                    BrandLookup.Add(brandId, brand);
                }
            }

            var tokenMemberId = 0;
            var httpContextBase = authorizationContext.RequestContext.HttpContext;
            if (httpContextBase.Items.Contains("tokenMemberId"))
            {
                tokenMemberId = (int) httpContextBase.Items["tokenMemberId"];
            }
            var isPayingMember = DataAccess.Subscription.SubscriptionStatus.MemberIsSubscriber(brand, tokenMemberId);
#if DEBUG
            if (httpContextBase.Request.IsLocal)
            {
                isPayingMember = true;
            }
#endif
            if (isPayingMember) return;
            Log.LogWarningMessage(string.Format("A non-subscriber tried to access a subscription-only endpoint.  Member ID {0}", tokenMemberId), ErrorHelper.GetCustomData(httpContextBase));
            authorizationContext.Result = new SparkHttpUnauthorizedResult((int) HttpSub401StatusCode.SubscriberIdRequired,
                                                String.Format("Access to this endpoint is restricted to subscriber members.  Member {0} does not have a subscription", tokenMemberId));
        }
	}
}