﻿using System;
using System.Web.Mvc;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Spark.Logger;
using Spark.Rest.V2.Serialization;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

namespace Spark.Rest.Authorization
{
    public class ServiceUnavailableCheck : FilterAttribute, IAuthorizationFilter
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(ServiceUnavailableCheck));

        private bool IsServiceUnavailable
        {
            get
            {
                bool isServiceUnavailable = true;
                try
                {
                    string apiMaintenanceFlag = RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.API_MAINTENANCE);

                    if (String.IsNullOrEmpty(apiMaintenanceFlag))
                    {
                        Log.LogException("MiddleTier is not responding. Configuration Service returned null for API_MAINTENANCE Setting", null);
                    }
                    else
                        isServiceUnavailable = Boolean.Parse(apiMaintenanceFlag);
                }
                catch (Exception ex)
                {
                    Log.LogException("Error in getting API_MAINTENANCE Settings.", ex);
                }
                return isServiceUnavailable;
            }
        }

        public virtual void OnAuthorization(AuthorizationContext authorizationContext)
        {
            //skip service availability check for Health check or any other methods decorated with AllowAnonymous
            bool skipAuthorization = authorizationContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true)
                                 || authorizationContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true);

            if (skipAuthorization)
            {
                return;
            }

            if (IsServiceUnavailable)
            {
                Log.LogException("Service Unavailable: Either MiddleTier is not responding or API scheduled for maintenance", null);
                authorizationContext.Result =  new SparkHttpServiceUnavailableResult((int)HttpSub503StatusCode.ServerMaintenanceError, "Server Maintenance");
                return;
            }
        }
    }
}