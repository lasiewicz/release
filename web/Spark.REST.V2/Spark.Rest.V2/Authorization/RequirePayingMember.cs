﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

namespace Spark.Rest.Authorization
{
	public class RequirePayingMember : RequireAccessToken
	{
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(RequirePayingMember));

        protected static Dictionary<int, Brand> BrandLookup = new Dictionary<int, Brand>();

		public override void OnAuthorization(AuthorizationContext authorizationContext)
		{
            // endpoints requiring a subscription will also always need an access token
            base.OnAuthorization(authorizationContext); // check for access token

			int memberId;
            var httpContextBase = authorizationContext.HttpContext;
            if (!Int32.TryParse((string)authorizationContext.RouteData.Values["memberId"], out memberId) || memberId < 1)
			{
                Log.LogInfoMessage("A valid member ID is required for subscribing member endpoints", ErrorHelper.GetCustomData(httpContextBase));
                authorizationContext.Result = new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.InvalidMemberId, "A valid member ID is required for secured endpoints");
                return;
			}

		    var accessToken = httpContextBase.Request.QueryString["access_token"];
            if (String.IsNullOrEmpty(accessToken))
            {
                Log.LogInfoMessage(string.Format("No access token for request {0}", httpContextBase.Request.Url), ErrorHelper.GetCustomData(httpContextBase));
                authorizationContext.Result = new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.MissingAccessToken, "An access token is required for access to the resource");
                return;
            }

            int brandId = 0;
            if (!Int32.TryParse((string)authorizationContext.RouteData.Values["brandId"], out brandId) || brandId < 1)
            {
                brandId = 1003; // default to jdate to support old style URLs with no brand ID. TODO: remove this after clients are using only new URLs in production
                //Log.Info("A valid brand ID is required for subscribing member endpoints");
                //authorizationContext.Result = new HttpUnauthorizedResult("A valid brand ID is required for subscribing member endpoints");
            }

            // load the brand into a dictionary based on URL
		    Brand brand;
            if (!BrandLookup.TryGetValue(brandId, out brand))
            {
				{
                    brand = BrandConfigSA.Instance.GetBrandByID(brandId);
					if (brand == null)
					{
                        authorizationContext.Result=new SparkHttpBadRequestResult((int)HttpSub400StatusCode.InvalidBrandId, String.Format("No brand found for host: {0}", brandId));
					    return;
					}
                    BrandLookup.Add(brandId, brand);
				}
			}

		    var isPayingMember = DataAccess.Subscription.SubscriptionStatus.MemberIsSubscriber(brand, memberId);
		    if (isPayingMember) return;
		    Log.LogWarningMessage(string.Format("A non-subscriber tried to access a subscription-only endpoint.  Member ID {0}", memberId), ErrorHelper.GetCustomData(httpContextBase));
		    authorizationContext.Result = new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.SubscriberIdRequired, String.Format("Access to this endpoint is restricted to subscriber members.  Member {0} does not have a subscription", memberId));
		}
	}
}