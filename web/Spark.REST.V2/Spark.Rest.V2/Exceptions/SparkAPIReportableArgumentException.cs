﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

namespace Spark.Rest.V2.Exceptions
{

    public class SparkAPIReportableArgumentException : SparkAPIReportableException
    {
        public SparkAPIReportableArgumentException(string message)
            : base(HttpSub400StatusCode.MissingArgument, message)
        {

        }
    }
}