﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

namespace Spark.Rest.V2.Exceptions
{
    public class SparkAPIException: Exception
    {
        public SparkAPIException(string message) : base(message) { }
    }
}