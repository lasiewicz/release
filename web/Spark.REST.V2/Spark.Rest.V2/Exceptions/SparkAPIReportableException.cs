﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

namespace Spark.Rest.V2.Exceptions
{
    public class SparkAPIReportableException: SparkAPIException
    {
        public HttpSub400StatusCode SubStatusCode { get; private set; }

        public SparkAPIReportableException(HttpSub400StatusCode subStatusCode, string message)
            : base(message)
        {
            SubStatusCode = subStatusCode;
        }
    }
}