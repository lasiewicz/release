using System;
using System.Linq;
using Spark.REST.Entities;

namespace Spark.REST.Repositories
{
	public interface IRepository<T>
		where T : IEntity
	{
		T Get(int id);
		IQueryable<T> GetAll();

		int Count();
		bool Exists(int id);

		void Save(T entity);
		void Delete(T entity);
	}
}