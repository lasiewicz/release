using System;
using System.Collections.Generic;
using System.Linq;
using Spark.REST.Entities;

namespace Spark.REST.Repositories
{
	public class InMemoryRepository<T> : IRepository<T>
		where T : class, IEntity
	{
		private readonly Dictionary<int, T> _items = new Dictionary<int, T>();

		public T Get(int id)
		{
			T item;

			if (!_items.TryGetValue(id, out item))
				return null;

			return item;
		}

		public IQueryable<T> GetAll()
		{
			return _items.Values.AsQueryable();
		}

		public int Count()
		{
			return _items.Count;
		}

		public bool Exists(int id)
		{
			return _items.ContainsKey(id);
		}

		public void Save(T entity)
		{
			if (entity.Id == 0)
				entity.Id = _items.Count + 1;

			_items[entity.Id] = entity;
		}

		public void Delete(T entity)
		{
			if (entity.Id != 0)
				_items.Remove(entity.Id);
		}
	}
}