#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;

#endregion

namespace Spark.REST.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class BrandRequest : RequestBase
    {
        protected static readonly Dictionary<string, Brand> BrandLookup = new Dictionary<string, Brand>();
        protected static readonly Dictionary<int, Brand> BrandLookupById = new Dictionary<int, Brand>();
        protected static readonly Dictionary<int, Brand> BrandLookupBySiteId = new Dictionary<int, Brand>();
        private static readonly object LockObj = new object();
        private Brand _brand;
        private int _brandId;
        private int _siteId;
        private string Host { get; set; }

        /// <summary>
        /// Gets or sets the brand identifier.
        /// </summary>
        /// <value>
        /// The brand identifier.
        /// </value>
        [DataMember(Name = "brandId")]
        public int BrandId
        {
            get { return _brandId; }
            set { _brandId = value; }
        }

        /// <summary>
        /// Gets or sets the site identifier.
        /// </summary>
        /// <value>
        /// The site identifier.
        /// </value>
        [DataMember(Name = "siteId")]
        public int SiteId
        {
            get { return _siteId; }
            set { _siteId = value; }
        }

        /// <summary>
        /// Gets or sets the brand.
        /// </summary>
        /// <value>
        /// The brand.
        /// </value>
        /// <exception cref="System.Exception">
        /// </exception>
        public Brand Brand
        {
            [DebuggerStepThrough]
            get
            {
                if (_brand != null) return _brand;

                if (_brandId != 0)
                {
                    lock (LockObj)
                    {
                        if (BrandLookupById.TryGetValue(_brandId, out _brand)) return _brand;
                    }

                    _brand = BrandConfigSA.Instance.GetBrandByID(_brandId);
                    if (_brand == null)
                    {
                        throw new Exception(String.Format("no brand found with ID: {0}", _brandId));
                    }

                    lock (LockObj)
                    {
                        BrandLookupById[_brandId] = _brand;
                    }
                }
                else
                {
                    if (_siteId != 0)
                    {
                        lock (LockObj)
                        {
                            if (BrandLookupBySiteId.TryGetValue(_siteId, out _brand)) return _brand;
                        }

                        var brandsBySite = BrandConfigSA.Instance.GetBrandsBySite(_siteId);
                        if (null != brandsBySite && brandsBySite.Count > 0)
                        {
                            var enumerator = brandsBySite.GetEnumerator();
                            var moveNext = enumerator.MoveNext();
                            _brand = enumerator.Current as Brand;
                        }

                        if (_brand == null)
                        {
                            throw new Exception(String.Format("no brand found for siteID: {0}", _siteId));
                        }

                        lock (LockObj)
                        {
                            BrandLookupBySiteId[_siteId] = _brand;
                        }
                    }
                    else
                    {
                        Host = "jdate.com";
                        // for backward compatibility only - defaulting to jdate for old style URLs without a brand ID
                        lock (LockObj)
                        {
                            if (BrandLookup.TryGetValue(Host, out _brand)) return _brand;
                        }

                        _brand = BrandConfigSA.Instance.GetBrandByURI(Host);
                        if (_brand == null)
                        {
                            throw new Exception(String.Format("no brand found for host: {0}", Host));
                        }

                        lock (LockObj)
                        {
                            BrandLookup[Host] = _brand;
                        }
                    }
                }
                return _brand;
            }
            set { _brand = value; }
        }
    }
}