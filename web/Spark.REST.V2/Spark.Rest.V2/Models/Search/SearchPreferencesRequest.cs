#region

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Matchnet;

#endregion

namespace Spark.REST.Models.Search
{
    /// <summary>
    ///     For search related methods
    /// </summary>
    public class SearchPreferencesRequest : MemberRequest
    {
        private int _regionId = Constants.NULL_INT;

        /// <summary>
        ///     For internally storing deserialized preferences
        /// </summary>
        private Dictionary<string, List<int>> _advancedPreferences;

        /// <summary>
        /// </summary>
        public SearchPreferencesRequest()
        {
            BatchSize = 1;
        }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        [DataMember(Name = "gender")]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the seeking gender.
        /// </summary>
        /// <value>
        /// The seeking gender.
        /// </value>
        [DataMember(Name = "seekingGender")]
        public string SeekingGender { get; set; }

        /// <summary>
        /// Gets or sets the minimum age.
        /// </summary>
        /// <value>
        /// The minimum age.
        /// </value>
        [DataMember(Name = "minAge")]
        public int MinAge { get; set; }

        /// <summary>
        /// Gets or sets the maximum age.
        /// </summary>
        /// <value>
        /// The maximum age.
        /// </value>
        [DataMember(Name = "maxAge")]
        public int MaxAge { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        [DataMember(Name = "location")]
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the region identifier.
        /// </summary>
        /// <value>
        /// The region identifier.
        /// </value>
        [DataMember(Name = "regionId")]
        public int RegionId
        {
            get { return _regionId; }
            set { _regionId = value; }
        }

        /// <summary>
        /// Gets or sets the maximum distance.
        /// </summary>
        /// <value>
        /// The maximum distance.
        /// </value>
        [DataMember(Name = "maxDistance")]
        public int MaxDistance { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [show only members with photos].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show only members with photos]; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "showOnlyMembersWithPhotos")]
        public bool? ShowOnlyMembersWithPhotos { get; set; }

        /// <summary>
        ///     To indicate if this is a new search based on changed prefs values. Used for MemberSlideShow.
        /// </summary>
        [DataMember(Name = "IsNewPrefs")]
        public bool IsNewPrefs { get; set; }

        /// <summary>
        /// Gets or sets the size of the batch.
        /// </summary>
        /// <value>
        /// The size of the batch.
        /// </value>
        [DataMember(Name = "BatchSize")]
        public short BatchSize { get; set; }

        /// <summary>
        /// Gets or sets the minimum height.
        /// </summary>
        /// <value>
        /// The minimum height.
        /// </value>
        [DataMember(Name = "MinHeight")]
        public int MinHeight { get; set; }

        /// <summary>
        /// Gets or sets the maximum height.
        /// </summary>
        /// <value>
        /// The maximum height.
        /// </value>
        [DataMember(Name = "MaxHeight")]
        public int MaxHeight { get; set; }

        [DataMember(Name = "includeBigData")]
        public bool IncludeBigData { get; set; }

        /// <summary>
        ///     Accepts a dictionary of advanced search preferences(options) that are mask valued.
        /// </summary>
        [DataMember(Name = "AdvancedPreferencesJson")]
        public string AdvancedPreferencesJson { get; set; }

        /// <summary>
        ///     For initializaing, if necessary, and retrieving the internal variable
        /// </summary>
        [DataMember(Name = "AdvancedPreferences")]
        public Dictionary<string, List<int>> AdvancedPreferences
        {
            get
            {
                if (_advancedPreferences != null) return _advancedPreferences;
                if (String.IsNullOrEmpty(AdvancedPreferencesJson)) return _advancedPreferences;
                var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                _advancedPreferences = ser.Deserialize<Dictionary<string, List<int>>>(AdvancedPreferencesJson);
                return _advancedPreferences;
            }
            set { _advancedPreferences = value; }
        }
    }
}
