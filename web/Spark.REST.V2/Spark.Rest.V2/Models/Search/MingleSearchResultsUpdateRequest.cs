﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Newtonsoft.Json.Linq;
using Spark.MingleSearchEngine.ValueObjects;
using Spark.REST.Models;

namespace Spark.Rest.Models.Search
{
    public class MingleSearchResultsUpdateRequest : MemberRequest
    {

        public UpdateModeEnum _updateMode = UpdateModeEnum.none;
        public UpdateModeEnum UpdateMode
        {
            get
            {
                if (_updateMode ==  UpdateModeEnum.none)
                {
                    if (null != JSONObject)
                    {
                        string value = ((JObject)JSONObject).GetValue("updateMode").Value<string>();
                        _updateMode = (UpdateModeEnum)Enum.Parse(typeof(UpdateModeEnum), value);
                    }
                }
                return _updateMode;
            }
        }

        private UpdateReasonEnum _updateReason = UpdateReasonEnum.none;
        public UpdateReasonEnum UpdateReason
        {
            get
            {
                if (_updateReason == UpdateReasonEnum.none)
                {
                    if (null != JSONObject)
                    {
                        string value = ((JObject)JSONObject).GetValue("updateReason").Value<string>();
                        _updateReason = (UpdateReasonEnum)Enum.Parse(typeof(UpdateReasonEnum), value);
                    }
                }
                return _updateReason;
            }
        }

        [DataMember(Name = "updateReason")]
        public string UpdateReasonStr { get; set; }

        [DataMember(Name = "updateMode")]
        public string UpdateModeStr { get; set; }

        [DataMember(Name = "gender")]
        public int Gender { get; set; }

        [DataMember(Name = "birthDate")]
        public DateTime Birthdate { get; set; }

        [DataMember(Name = "joinDate")]
        public DateTime JoinDate { get; set; }

        [DataMember(Name = "lastLogin")]
        public DateTime LastLogin { get; set; }

        [DataMember(Name = "photo")]
        public int PhotoFlag { get; set; }

        [DataMember(Name = "education")]
        public int Education { get; set; }

        [DataMember(Name = "religion")]
        public int Religion { get; set; }

        [DataMember(Name = "language")]
        public int Language { get; set; }

        [DataMember(Name = "ethnicity")]
        public int Ethnicity { get; set; }

        [DataMember(Name = "smoke")]
        public int Smoke { get; set; }

        [DataMember(Name = "drink")]
        public int Drink { get; set; }

        [DataMember(Name = "height")]
        public int Height { get; set; }

        [DataMember(Name = "maritalStatus")]
        public int MaritalStatus { get; set; }

        [DataMember(Name = "bodyType")]
        public int BodyType { get; set; }

        [DataMember(Name = "longitude")]
        public string LongitudeStr { get; set; }

        [DataMember(Name = "latitude")]
        public string LatitudeStr { get; set; }

        private double _lat;
        public double Latitude
        {
            get
            {
                if (_lat <= 0.0)
                {
                    if (null != JSONObject)
                    {
                        _lat = ((JObject)JSONObject).GetValue("latitude").Value<double>();
                    }
                }
                return _lat;
            }
        }

        private double _lng;
        public double Longitude
        {
            get
            {
                if (_lng <= 0.0)
                {
                    if (null != JSONObject)
                    {
                        _lng = ((JObject)JSONObject).GetValue("longitude").Value<double>();
                    }
                }
                return _lng;
            }
        }

        [DataMember(Name = "locationBHRegionID")]
        public int LocationBHRegionID { get; set; }

        [DataMember(Name = "lastUpdateYMDT")]
        public DateTime LastUpdateYMDT { get; set; }

        [DataMember(Name = "weight")]
        public int Weight { get; set; }

        [DataMember(Name = "children")]
        public int Children { get; set; }

        [DataMember(Name = "aboutChildren")]
        public int AboutChildren { get; set; }

        [DataMember(Name = "childrenAtHome")]
        public int ChildrenAtHome { get; set; }

        [DataMember(Name = "colorB")]
        public string ColorBStr { get; set; }

        private decimal _colorB = decimal.Zero;
        public decimal ColorB
        {
            get
            {
                if (_colorB == decimal.Zero)
                {
                    if (null != JSONObject)
                    {
                        _colorB = ((JObject) JSONObject).GetValue("colorB").Value<decimal>();
                    }
                }
                return _colorB;
            }
        }

        [DataMember(Name = "colorR")]
        public string ColorRStr { get; set; }

        private decimal _colorR = decimal.Zero;
        public decimal ColorR
        {
            get
            {
                if (_colorR == decimal.Zero)
                {
                    if (null != JSONObject)
                    {
                        _colorR = ((JObject)JSONObject).GetValue("colorR").Value<decimal>();
                    }
                }
                return _colorR;
            }
        }

        [DataMember(Name = "colorW")]
        public string ColorWStr { get; set; }

        private decimal _colorW = decimal.Zero;
        public decimal ColorW
        {
            get
            {
                if (_colorW == decimal.Zero)
                {
                    if (null != JSONObject)
                    {
                        _colorW = ((JObject)JSONObject).GetValue("colorW").Value<decimal>();
                    }
                }
                return _colorW;
            }
        }

        [DataMember(Name = "colorY")]
        public string ColorYStr { get; set; }

        private decimal _colorY = decimal.Zero;
        public decimal ColorY
        {
            get
            {
                if (_colorY == decimal.Zero)
                {
                    if (null != JSONObject)
                    {
                        _colorY = ((JObject)JSONObject).GetValue("colorY").Value<decimal>();
                    }
                }
                return _colorY;
            }
        }

        [DataMember(Name = "premiumServicesFlags")]
        public int PremiumServicesFlags { get; set; }

        [DataMember(Name = "churchActivity")]
        public int ChurchActivity { get; set; }

        [DataMember(Name = "ministryId")]
        public int MinistryId { get; set; }

        [DataMember(Name = "eyes")]
        public int Eyes { get; set; }

        [DataMember(Name = "hair")]
        public int Hair { get; set; }

        [DataMember(Name = "intention")]
        public int Intention { get; set; }

        [DataMember(Name = "wantsethnicity")]
        public int WantsEthnicity { get; set; }

        [DataMember(Name = "wantssmoke")]
        public int WantsSmoke { get; set; }

        [DataMember(Name = "wantsdrink")]
        public int WantsDrink { get; set; }

        [DataMember(Name = "wantsreligion")]
        public int WantsReligion { get; set; }

        [DataMember(Name = "wantsmaritalstatus")]
        public int WantsMaritalStatus { get; set; }

        [DataMember(Name = "wantsbodytype")]
        public int WantsBodyType { get; set; }

        [DataMember(Name = "wantschurchactivity")]
        public int WantsChurchActivity { get; set; }

        [DataMember(Name = "wantseducation")]
        public int WantsEducation { get; set; }

        [DataMember(Name = "importanceethnicity")]
        public int ImportanceEthnicity { get; set; }

        [DataMember(Name = "importancesmoke")]
        public int ImportanceSmoke { get; set; }

        [DataMember(Name = "importancedrink")]
        public int ImportanceDrink { get; set; }

        [DataMember(Name = "importancereligion")]
        public int ImportanceReligion { get; set; }

        [DataMember(Name = "importancemaritalstatus")]
        public int ImportanceMaritalStatus { get; set; }

        [DataMember(Name = "importancebodytype")]
        public int ImportanceBodyType { get; set; }

        [DataMember(Name = "importancechurchactivity")]
        public int ImportanceChurchActivity { get; set; }

        [DataMember(Name = "importanceeducation")]
        public int ImportanceEducation { get; set; }

    }
}