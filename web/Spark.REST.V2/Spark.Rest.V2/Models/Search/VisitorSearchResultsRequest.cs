﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;
using Spark.REST.Models.Search;

namespace Spark.Rest.Models.Search
{
    public class VisitorSearchResultsRequest : BrandRequest
    {
        [DataMember(Name = "pageSize")]
        public int PageSize { get; set; }

        [DataMember(Name = "pageNumber")]
        public int PageNumber { get; set; }

        [DataMember(Name = "listPosition")]
        public int ListPosition { get; set; }

        [DataMember(Name = "showOnlyJewishMembers")]
        public bool ShowOnlyJewishMembers { get; set; }

        [DataMember(Name = "genderMask")]
        public int GenderMask { get; set; }

        [DataMember(Name = "regionId")]
        public int RegionID { get; set; }

        [DataMember(Name = "minAge")]
        public int MinAge { get; set; }

        [DataMember(Name = "maxAge")]
        public int MaxAge { get; set; }

        [DataMember(Name = "maxDistance")]
        public int MaxDistance { get; set; }

        [DataMember(Name = "showOnlyMembersWithPhotos")]
        public bool ShowOnlyMembersWithPhotos { get; set; }
        
    }
}