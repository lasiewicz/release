#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Matchnet.Search.Interfaces;
using Newtonsoft.Json.Linq;

#endregion

namespace Spark.REST.Models.Search
{
    public class SearchResultsRequest : SearchPreferencesRequest
    {
        public SearchResultsRequest() { }

        public SearchResultsRequest(Entities.Search.SearchPreferences preferences)
        {
            AdvancedPreferences = ConvertAdvancedSearchPreferences(preferences.AdvancedSearchPreferences);
            Gender = preferences.Gender;
            Location = preferences.Location;
            MaxAge = preferences.MaxAge;
            MaxDistance = preferences.MaxDistance;
            MaxHeight = preferences.MaxHeight;
            MinAge = preferences.MinAge;
            MinHeight = preferences.MinHeight;
            MemberId = preferences.MemberId;
            RegionId = Convert.ToInt32(preferences.RegionId);
            SeekingGender = preferences.SeekingGender;
            ShowOnlyMembersWithPhotos = preferences.ShowOnlyMembersWithPhotos;
        }

        private SearchEntryPoint _searchEntryPointType = SearchEntryPoint.None;
        private QuerySorting _searchOrderByType = QuerySorting.None;
        private SearchType _searchType = SearchType.WebSearch;

        [DataMember(Name = "pageSize")]
        public int PageSize { get; set; }

        [DataMember(Name = "pageNumber")]
        public int PageNumber { get; set; }

        [DataMember(Name = "listPosition")]
        public int ListPosition { get; set; }

        [DataMember(Name = "showOnlyJewishMembers")]
        public bool ShowOnlyJewishMembers { get; set; }

        [DataMember(Name = "keywords")]
        public string Keywords { get; set; }

        [DataMember(Name="includeSpotlightProfile")]
        public bool IncludeSpotlightProfile { get; set; }

        [DataMember(Name = "useDefaultPreferences")]
        public bool UseDefaultPreferences { get; set; }

        [DataMember(Name = "showOnlyRamah")]
        public bool? ShowOnlyRamah { get; set; }

        public QuerySorting SearchOrderByType
        {
            get
            {
                if (_searchOrderByType != QuerySorting.JoinDate || null == JSONObject) return _searchOrderByType;
                var jToken = ((JObject) JSONObject).GetValue("searchOrderByType");
                if (null == jToken) return _searchOrderByType;
                var value = jToken.Value<string>();
                if (!string.IsNullOrEmpty(value))
                {
                    _searchOrderByType = (QuerySorting) Enum.Parse(typeof (QuerySorting), value);
                }
                return _searchOrderByType;
            }
            set { _searchOrderByType = value; }
        }

        public SearchType SearchType
        {
            get
            {
                if (_searchType != SearchType.WebSearch || null == JSONObject) return _searchType;
                var jToken = ((JObject) JSONObject).GetValue("searchType");
                if (null == jToken) return _searchType;
                var value = jToken.Value<string>();
                if (!string.IsNullOrEmpty(value))
                {
                    _searchType = (SearchType) Enum.Parse(typeof (SearchType), value);
                }
                return _searchType;
            }
            set { _searchType = value; }
        }

        public SearchEntryPoint SearchEntryPointType
        {
            get
            {
                if (_searchEntryPointType != SearchEntryPoint.None || null == JSONObject) return _searchEntryPointType;
                var jToken = ((JObject) JSONObject).GetValue("searchEntryPointType");
                if (null == jToken) return _searchEntryPointType;
                var value = jToken.Value<string>();
                if (!string.IsNullOrEmpty(value))
                {
                    _searchEntryPointType = (SearchEntryPoint) Enum.Parse(typeof (SearchEntryPoint), value);
                }
                return _searchEntryPointType;
            }
            set { _searchEntryPointType = value; }
        }

        private Dictionary<string, List<int>> ConvertAdvancedSearchPreferences(Dictionary<string, object> preferences)
        {
            if (preferences == null || !preferences.Any()) return null;
            var advancedPreferences = new Dictionary<string, List<int>>(preferences.Count);
            foreach (var item in preferences)
            {
                var list = new List<int> {Convert.ToInt32(item.Value)};
                advancedPreferences.Add(item.Key, list);
            }
            return advancedPreferences;
        }
    }
}
