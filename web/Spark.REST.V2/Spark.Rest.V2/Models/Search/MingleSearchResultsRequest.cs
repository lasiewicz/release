﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;
using Spark.MingleSearchEngine.ValueObjects;
using Spark.REST.Models;

namespace Spark.Rest.Models.Search
{
    public class MingleSearchResultsRequest : MemberRequest
    {
        private int _seekingGender = int.MinValue;
        private int _minAge = int.MinValue;
        private int _maxAge = int.MinValue;
        private int _minHeight = int.MinValue;
        private int _maxHeight = int.MinValue;
        private int _radiusInMiles = int.MinValue;
        private int _ethnicity = int.MinValue;
        private int _bodyType = int.MinValue;
        private int _education = int.MinValue;
        private int _religion = int.MinValue;
        private int _smoke = int.MinValue;
        private int _drink = int.MinValue;
        private int _maritalStatus = int.MinValue;
        private int _maxResults = int.MinValue;
        private int _hasEthnicity = int.MinValue;
        private int _hasSmoke = int.MinValue;
        private int _hasDrink = int.MinValue;
        private int _hasReligion = int.MinValue;
        private int _hasMaritalStatus = int.MinValue;
        private int _hasBodyType = int.MinValue;
        private int _hasChurchActivity = int.MinValue;
        private int _hasEducation = int.MinValue;
        private int _wantsEthnicity = int.MinValue;
        private int _wantsSmoke = int.MinValue;
        private int _wantsDrink = int.MinValue;
        private int _wantsReligion = int.MinValue;
        private int _wantsMaritalStatus = int.MinValue;
        private int _wantsBodyType = int.MinValue;
        private int _wantsChurchActivity = int.MinValue;
        private int _wantsEducation = int.MinValue;
        private int _importanceEthnicity = int.MinValue;
        private int _importanceSmoke = int.MinValue;
        private int _importanceDrink = int.MinValue;
        private int _importanceReligion = int.MinValue;
        private int _importanceMaritalStatus = int.MinValue;
        private int _importanceBodyType = int.MinValue;
        private int _importanceChurchActivity = int.MinValue;
        private int _importanceEducation = int.MinValue;


        [DataMember(Name = "seekingGender")]
        public int SeekingGender
        {
            get { return _seekingGender; }
            set { _seekingGender = value; }
        }

        [DataMember(Name = "minAge")]
        public int MinAge
        {
            get { return _minAge; }
            set { _minAge = value; }
        }

        [DataMember(Name = "maxAge")]
        public int MaxAge
        {
            get { return _maxAge; }
            set { _maxAge = value; }
        }

        [DataMember(Name = "minHeight")]
        public int MinHeight
        {
            get { return _minHeight; }
            set { _minHeight = value; }
        }

        [DataMember(Name = "maxHeight")]
        public int MaxHeight
        {
            get { return _maxHeight; }
            set { _maxHeight = value; }
        }

        [DataMember(Name = "radiansLat")]
        public string RadiansLatitudeStr { get; set; }

        [DataMember(Name = "radiansLng")]
        public string RadiansLongitudeStr { get; set; }

        [DataMember(Name = "radiusInMiles")]
        public int RadiusInMiles
        {
            get { return _radiusInMiles; }
            set { _radiusInMiles = value; }
        }

        [DataMember(Name = "ethnicity")]
        public int Ethnicity
        {
            get { return _ethnicity; }
            set { _ethnicity = value; }
        }

        [DataMember(Name = "bodyType")]
        public int BodyType
        {
            get { return _bodyType; }
            set { _bodyType = value; }
        }

        [DataMember(Name = "education")]
        public int Education
        {
            get { return _education; }
            set { _education = value; }
        }
        
        [DataMember(Name = "religion")]
        public int Religion
        {
            get { return _religion; }
            set { _religion = value; }
        }

        [DataMember(Name = "smoke")]
        public int Smoke
        {
            get { return _smoke; }
            set { _smoke = value; }
        }

        [DataMember(Name = "drink")]
        public int Drink
        {
            get { return _drink; }
            set { _drink = value; }
        }
   
        [DataMember(Name = "maritalStatus")]
        public int MaritalStatus
        {
            get { return _maritalStatus; }
            set { _maritalStatus = value; }
        }

        [DataMember(Name = "hasPhoto")]
        public bool HasPhoto { get; set; }

        [DataMember(Name = "sortType")]
        public string SortTypeStr { get; set; }

        [DataMember(Name = "resultType")]
        public string ResultTypeStr { get; set; }

        [DataMember(Name = "maxResults")]
        public int MaxResults
        {
            get { return _maxResults; }
            set { _maxResults = value; }
        }

        [DataMember(Name = "wantsethnicity")]
        public int WantsEthnicity
        {
            get { return _wantsEthnicity; }
            set { _wantsEthnicity = value; }
        }

        [DataMember(Name = "wantssmoke")]
        public int WantsSmoke
        {
            get { return _wantsSmoke; }
            set { _wantsSmoke = value; }
        }

        [DataMember(Name = "wantsdrink")]
        public int WantsDrink
        {
            get { return _wantsDrink; }
            set { _wantsDrink = value; }
        }

        [DataMember(Name = "wantsreligion")]
        public int WantsReligion
        {
            get { return _wantsReligion; }
            set { _wantsReligion = value; }
        }

        [DataMember(Name = "wantsmaritalstatus")]
        public int WantsMaritalStatus
        {
            get { return _wantsMaritalStatus; }
            set { _wantsMaritalStatus = value; }
        }

        [DataMember(Name = "wantsbodytype")]
        public int WantsBodyType
        {
            get { return _wantsBodyType; }
            set { _wantsBodyType = value; }
        }

        [DataMember(Name = "wantschurchactivity")]
        public int WantsChurchActivity
        {
            get { return _wantsChurchActivity; }
            set { _wantsChurchActivity = value; }
        }

        [DataMember(Name = "wantseducation")]
        public int WantsEducation
        {
            get { return _wantsEducation; }
            set { _wantsEducation = value; }
        }

        [DataMember(Name = "importanceethnicity")]
        public int ImportanceEthnicity
        {
            get { return _importanceEthnicity; }
            set { _importanceEthnicity = value; }
        }

        [DataMember(Name = "importancesmoke")]
        public int ImportanceSmoke
        {
            get { return _importanceSmoke; }
            set { _importanceSmoke = value; }
        }

        [DataMember(Name = "importancedrink")]
        public int ImportanceDrink
        {
            get { return _importanceDrink; }
            set { _importanceDrink = value; }
        }

        [DataMember(Name = "importancereligion")]
        public int ImportanceReligion
        {
            get { return _importanceReligion; }
            set { _importanceReligion = value; }
        }

        [DataMember(Name = "importancemaritalstatus")]
        public int ImportanceMaritalStatus
        {
            get { return _importanceMaritalStatus; }
            set { _importanceMaritalStatus = value; }
        }

        [DataMember(Name = "importancebodytype")]
        public int ImportanceBodyType
        {
            get { return _importanceBodyType; }
            set { _importanceBodyType = value; }
        }

        [DataMember(Name = "importancechurchactivity")]
        public int ImportanceChurchActivity
        {
            get { return _importanceChurchActivity; }
            set { _importanceChurchActivity = value; }
        }

        [DataMember(Name = "importanceeducation")]
        public int ImportanceEducation
        {
            get { return _importanceEducation; }
            set { _importanceEducation = value; }
        }

        [DataMember(Name = "hasethnicity")]
        public int HasEthnicity
        {
            get { return _hasEthnicity; }
            set { _hasEthnicity = value; }
        }

        [DataMember(Name = "hassmoke")]
        public int HasSmoke
        {
            get { return _hasSmoke; }
            set { _hasSmoke = value; }
        }

        [DataMember(Name = "hasdrink")]
        public int HasDrink
        {
            get { return _hasDrink; }
            set { _hasDrink = value; }
        }

        [DataMember(Name = "hasreligion")]
        public int HasReligion
        {
            get { return _hasReligion; }
            set { _hasReligion = value; }
        }

        [DataMember(Name = "hasmaritalstatus")]
        public int HasMaritalStatus
        {
            get { return _hasMaritalStatus; }
            set { _hasMaritalStatus = value; }
        }

        [DataMember(Name = "hasbodytype")]
        public int HasBodyType
        {
            get { return _hasBodyType; }
            set { _hasBodyType = value; }
        }

        [DataMember(Name = "haschurchactivity")]
        public int HasChurchActivity
        {
            get { return _hasChurchActivity; }
            set { _hasChurchActivity = value; }
        }

        [DataMember(Name = "haseducation")]
        public int HasEducation
        {
            get { return _hasEducation; }
            set { _hasEducation = value; }
        }

        private double _lat = double.MinValue;
        public double RadiansLatitude
        {
            get
            {
                if (_lat <= 0.0)
                {
                    if (null != JSONObject)
                    {
                        _lat = ((JObject)JSONObject).GetValue("radiansLat").Value<double>();
                    }
                }
                return _lat;
            }
        }

        private double _lng = double.MinValue;
        public double RadiansLongitude
        {
            get
            {
                if (_lng <= 0.0)
                {
                    if (null != JSONObject)
                    {
                        _lng = ((JObject)JSONObject).GetValue("radiansLng").Value<double>();
                    }
                }
                return _lng;
            }
        }

        private SearchSorting _sort=SearchSorting.ColorCode;
        public SearchSorting SortType
        {
            get
            {
                if (_sort == SearchSorting.ColorCode)
                {
                    if (null != JSONObject)
                    {
                        string value = ((JObject) JSONObject).GetValue("sortType").Value<string>();
                        _sort = (SearchSorting)Enum.Parse(typeof(SearchSorting),value);
                    }
                }
                return _sort;
            }
        }

        private SearchResultType _resultType = SearchResultType.TerseResult;
        public SearchResultType ResultType
        {
            get
            {
                if (_resultType == SearchResultType.TerseResult)
                {
                    if (null != JSONObject)
                    {
                        string value = ((JObject)JSONObject).GetValue("resultType").Value<string>();
                        _resultType = (SearchResultType)Enum.Parse(typeof(SearchResultType), value);
                    }
                }
                return _resultType;
            }
        }

    }
}