﻿using System.Runtime.Serialization;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Search
{
    public class SecretAdmirerRequest : BrandRequest
    {
        [DataMember(Name = "minAge")]
        public int MinAge { get; set; }

        [DataMember(Name = "maxAge")]
        public int MaxAge { get; set; }

        [DataMember(Name = "pageSize")]
        public int PageSize { get; set; }

        [DataMember(Name = "pageNumber")]
        public int PageNumber { get; set; }

        [DataMember(Name = "listPosition")]
        public int ListPosition { get; set; }

        [DataMember(Name = "showOnlyJewishMembers")]
        public bool ShowOnlyJewishMembers { get; set; }

    }
}