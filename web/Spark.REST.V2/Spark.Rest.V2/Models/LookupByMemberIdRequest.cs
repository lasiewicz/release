namespace Spark.REST.Models
{
	public class LookupByMemberIdRequest : MemberRequest
	{
		public int TargetMemberId { get; set; }

        public string AttributeSetName { get; set; }
	}
}
