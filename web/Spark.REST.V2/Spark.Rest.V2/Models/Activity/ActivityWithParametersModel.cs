﻿#region

using System;
using System.Collections.Generic;
using Spark.ActivityRecording.Processor.ValueObjects;
using Spark.REST.Models;

#endregion

namespace Spark.Rest.V2.Models.Activity
{
    public class ActivityWithParametersModel : BrandRequest
    {
        // Set the default value of "Web" here. Also for backwards compatibility.
        private CallingSystem _callingSystem = CallingSystem.Web;
        private Dictionary<string, string> _parameters { get; set; }
        public int MemberId { get; set; }
        public int TargetMemberId { get; set; }

        /// <summary>
        ///     Need matching values in DB for this enum, check!
        /// </summary>
        public string ActionType { get; set; }

        /// <summary>
        ///     Need matching values in DB for this enum, check!
        /// </summary>
        public CallingSystem CallingSystem
        {
            get { return _callingSystem; }
            set { _callingSystem = value; }
        }

        public string ParametersJsonString { get; set; }

        public Dictionary<string, string> Parameters
        {
            get
            {
                if (_parameters == null)
                {
                    if (!String.IsNullOrEmpty(ParametersJsonString))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _parameters = ser.Deserialize<Dictionary<string, string>>(ParametersJsonString);
                    }

                    _parameters = _parameters ?? new Dictionary<string, string>();
                }
                return _parameters;
            }

            set { _parameters = value; }
        }
    }
}