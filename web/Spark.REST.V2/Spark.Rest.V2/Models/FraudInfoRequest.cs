﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models
{
    public class FraudInfoRequest : MemberRequest
    {
        [DataMember(Name = "riskInquiryType")]
        public int RiskInquiryTypeID { get; set; }

        [DataMember(Name = "BindingID")]
        public string bindingID { get; set; }
    }
}