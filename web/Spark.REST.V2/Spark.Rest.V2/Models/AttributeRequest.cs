using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;

namespace Spark.REST.Models
{
    /// <summary>
    /// 
    /// </summary>
	public class AttributeRequest : MemberRequest
	{
        /// <summary>
        /// Gets or sets the attributes json.
        /// </summary>
        /// <value>
        /// The attributes json.
        /// </value>
        [DataMember(Name = "attributesJson")]
        public string AttributesJson { get; set; } // JSON dictionary


	    private Dictionary<string, object> _attributes;
        /// <summary>
        /// Gets or sets the attributes.
        /// </summary>
        /// <value>
        /// The attributes.
        /// </value>
        public Dictionary<string, object> Attributes
        {
            get
            {
                if (_attributes != null) return _attributes;
                if (!String.IsNullOrEmpty(AttributesJson))
                {
                    var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                    _attributes = ser.Deserialize<Dictionary<string, object>>(AttributesJson);
                }
                else
                {
                    var jObject = JSONObject as JObject;
                    if (jObject != null)
                    {
                        var list = jObject.SelectToken("attributesJson").ToList();
                        foreach (var jp in list.Cast<JProperty>())
                        {
                            if(null == _attributes) _attributes = new Dictionary<string, object>();
                            switch (jp.Value.Type)
                            {
                                case JTokenType.Integer:
                                    _attributes.Add(jp.Name, jp.Value.ToObject<int>());
                                    break;
                                case JTokenType.Date:
                                    _attributes.Add(jp.Name, jp.Value.ToObject<DateTime>());
                                    break;
                                case JTokenType.Boolean:
                                    _attributes.Add(jp.Name, jp.Value.ToObject<bool>());
                                    break;
                                default:
                                    _attributes.Add(jp.Name, jp.Value.ToString());
                                    break;
                            }
                        }
                    }
                }
                _attributes =_attributes ?? new Dictionary<string, object>();
                return _attributes;
            }
            set { _attributes = value; }
        }


        /// <summary>
        /// Gets or sets the attributes multi value json.
        /// </summary>
        /// <value>
        /// The attributes multi value json.
        /// </value>
        [DataMember(Name = "attributesMultiValueJson")]
        public string AttributesMultiValueJson { get; set; } // JSON dictionary

	    private Dictionary<string, List<int>> _attributesMultiValue;
        /// <summary>
        /// Gets or sets the attributes multi value.
        /// </summary>
        /// <value>
        /// The attributes multi value.
        /// </value>
	    public Dictionary<string, List<int>> AttributesMultiValue
	    {
	        get
	        {
	            if (_attributesMultiValue != null) return _attributesMultiValue;
	            if (!String.IsNullOrEmpty(AttributesMultiValueJson))
	            {
	                var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
	                _attributesMultiValue = ser.Deserialize<Dictionary<string, List<int>>>(AttributesMultiValueJson);
	            }
	            else
	            {
	                var jObject = JSONObject as JObject;
	                if (jObject != null)
	                {
	                    var list = jObject.SelectToken("attributesMultiValueJson").ToList();
	                    foreach (var jp in list.Cast<JProperty>())
	                    {
	                        if (null == _attributesMultiValue) _attributesMultiValue = new Dictionary<string, List<int>>();
	                        _attributesMultiValue.Add(jp.Name, jp.Value.Select(m => m.Value<int>()).ToList());
	                    }
	                }
	            }
	            _attributesMultiValue = _attributesMultiValue ?? new Dictionary<string, List<int>>();
	            return _attributesMultiValue;
	        }
            set { _attributesMultiValue = value; }
	    }
    }
}