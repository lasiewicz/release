﻿using Newtonsoft.Json;
using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Models
{
    public class ProfileBatchRequest : MemberRequest
    {
        private List<int> _TargetMemberList;

        /// <summary>
        /// Comma delimited list of member IDs
        /// </summary>
        public string TargetMembers { get; set; }

        [IgnoreDataMember]
        public List<int> TargetMemberList
        {
            get
            {
                if (_TargetMemberList == null)
                {
                    _TargetMemberList = new List<int>();
                    if (!string.IsNullOrEmpty(TargetMembers))
                    {
                        string[] memberIds = TargetMembers.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in memberIds)
                        {
                            int memberId = 0;
                            int.TryParse(s, out memberId);
                            if (memberId > 0)
                            {
                                _TargetMemberList.Add(memberId);
                            }
                        }
                    }
                }

                return _TargetMemberList;
            }
            set
            {
                _TargetMemberList = value;
            }
        }
    }
}