﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Models
{
    public class AttributeSetBatchRequest : ProfileBatchRequest
    {
        [DataMember(Name = "attributeSetName")]
        public string AttributeSetName { get; set; }

        [DataMember(Name = "referralUri")]
        public string ReferralUri { get; set; }
    }
}