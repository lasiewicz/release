using System.Runtime.Serialization;

namespace Spark.REST.Models
{
    /// <summary>
    /// 
    /// </summary>
	public class AttributeSetRequest : MemberAndTargetMemberRequest
	{
        /// <summary>
        /// Gets or sets the name of the attribute set.
        /// </summary>
        /// <value>
        /// The name of the attribute set.
        /// </value>
		[DataMember(Name = "attributeSetName")]
		public string AttributeSetName { get; set; }

        /// <summary>
        /// Gets or sets the referral URI.
        /// </summary>
        /// <value>
        /// The referral URI.
        /// </value>
        [DataMember(Name = "referralUri")]
        public string ReferralUri { get; set; }


	}
}
