﻿using System.Runtime.Serialization;

namespace Spark.REST.Models
{
	public class FileRequest : MemberRequest
	{
		/// <summary>
		/// Base 64 encoded file
		/// </summary>
		[DataMember(Name = "photoFile")]
		public string PhotoFile { get; set; }

		[DataMember(Name = "fileName")]
		public string FileName { get; set; }

		[DataMember(Name = "caption")]
		public string Caption { get; set; }

        [DataMember(Name="comment")]
        public bool IsMain { get; set; }

	}
}
