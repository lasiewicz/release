﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.MemberDevices
{
    public class DeviceAutoLoginRequest : MemberRequest
    {
        /// <summary>
        /// application ID identifies the app making the request
        /// </summary>
        [DataMember(Name = "applicationId")]
        public int ApplicationId { get; set; }

        /// <summary>
        /// member's email address
        /// </summary>
        [DataMember(Name = "email")]
        public string Email { get; set; }
    }
}