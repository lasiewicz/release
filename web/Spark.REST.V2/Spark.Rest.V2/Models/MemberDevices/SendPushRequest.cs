﻿using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.SystemEvents;
using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.MemberDevices
{
    public class SendPushRequest : MemberRequest
    {
        public int SenderMemberId { get; set; }
        public int RecipientMemberId { get; set; }
        public int MessageListId { get; set; }
        public PushNotificationTypeID PushNotificationType { get; set; }
        public SystemEventCategory SystemEventCategory { get; set; }
        public AppGroupID AppGroupID { get; set; }
        public int IMID { get; set; }
        public string ResourceID { get; set; }
        public string Message { get; set; }
    }
}