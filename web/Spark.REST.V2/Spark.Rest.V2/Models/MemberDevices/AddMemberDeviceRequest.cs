﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.MemberDevices
{
    public class AddMemberDeviceRequest : BrandRequest
    {
        public string DeviceId { get; set; }
        public string DeviceType { get; set; }
        public string OSVersion { get; set; }
        public string AppVersion { get; set; }
    }
}