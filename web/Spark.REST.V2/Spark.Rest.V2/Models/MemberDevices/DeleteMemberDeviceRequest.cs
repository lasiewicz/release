﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.MemberDevices
{
    public class DeleteMemberDeviceRequest : MemberRequest
    {
        public string DeviceId { get; set; }
    }
}