﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.REST.Models;
using Matchnet.Content.ValueObjects.PushNotification;

namespace Spark.Rest.V2.Models.MemberDevices
{
    public class UpdateDeviceNotificationSettingsRequest: BrandRequest
    {
        public string DeviceId { get; set; }
        public PushNotificationCategoryId NotificationCategoryId { get; set; }
        public bool Enabled { get; set; }
    }
}