using System;

namespace Spark.REST.Models
{
	public class GetRequest : RequestBase
	{
		public int Id { get; set; }
	}
}