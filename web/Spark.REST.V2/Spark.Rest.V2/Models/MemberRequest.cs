using System.Runtime.Serialization;

namespace Spark.REST.Models
{
	///<summary>
	/// Member request.  Conatins member and brand
	///</summary>
	public class MemberRequest : BrandRequest
	{
		///<summary>
		/// Spark member ID
		///</summary>
		[DataMember(Name = "memberId")]
		public int MemberId { get; set; }

        [DataMember(Name = "isRedRedesignBeta")]
        public string IsRedRedesignBeta { get; set; }

        [DataMember(Name = "isRedRedesignBetaOffered")]
        public string IsRedRedesignBetaOffered { get; set; }            
	}
}