﻿#region

using Newtonsoft.Json;
using Spark.REST.Models;
using Spark.Rest.V2.BedrockPorts;
using Spark.Rest.V2.DataAccess.Profile;

#endregion

namespace Spark.Rest.V2.Models.Profile
{
    /// <summary>
    /// This request wraps the <see cref="DisplaySettings"/> object because its 
    /// presentation to clients is inverted from the actual implementation in <see cref="AttributeOptionHideMask"/>
    /// </summary>
    public class DisplaySettingsRequest : MemberRequest
    {
        /// <summary>
        ///  Show/Hide When You're Online'
        /// 
        /// Select true if you'd like members to know when you're logged into the site. 
        /// You'll show up in the Members Online section, and the "I'm online" button will flash in your profile.
        /// </summary>
        [JsonProperty(PropertyName = "online")]
        public bool Online { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show or hide the profile in searches.
        /// </summary>
        /// <value>
        /// <c>true</c> if show, otherwise, <c>false</c>.
        /// </value>
        [JsonProperty(PropertyName = "showProfileInSearch")]
        public bool ShowProfileInSearch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the member wants photos shown to non-members.
        /// </summary>
        /// <value>
        /// <c>true</c> if the member wants photos hidden from non-members; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty(PropertyName = "showPhotosToNonMembers")]
        public bool ShowPhotosToNonMembers { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the member wants let other members
        /// know when they view or hotlist another member.
        /// </summary>
        /// <value>
        /// <c>true</c> if the member wants to show when they view or hotlist; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty(PropertyName = "showWhenViewOrHotlist")]
        public bool ShowWhenViewOrHotlist { get; set; }

        /// <summary>
        /// Gets the display settings.
        /// </summary>
        /// <value>
        /// The display settings.
        /// </value>
        public DisplaySettings DisplaySettings
        {
            get
            {
                return new DisplaySettings {Online = Online, ShowProfileInSearch = ShowProfileInSearch, ShowPhotosToNonMembers = ShowPhotosToNonMembers, ShowWhenViewOrHotlist = ShowWhenViewOrHotlist};
            }
        }
    }
}