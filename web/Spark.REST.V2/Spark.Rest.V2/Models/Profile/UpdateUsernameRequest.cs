﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Profile
{
    /// <summary>
    /// Update the member's username.
    /// </summary>
    public class UpdateUsernameRequest : MemberRequest
    {
        /// <summary>
        /// The new username to set.
        /// </summary>
        public string UserName { get; set; }
    }
}