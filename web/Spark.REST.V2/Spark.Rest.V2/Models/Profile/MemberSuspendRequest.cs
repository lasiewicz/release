﻿using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Profile
{
    /// <summary>
    /// A request to suspend the member's status
    /// </summary>
    public class MemberSuspendRequest : MemberRequest
    {
        /// <summary>
        /// Why is the user suspending the account?
        /// AttributeValue	Description
        /// 1	found my soulmate on the site
        /// 2	found my soulmate on my own
        /// 4	too many responses
        /// 8	too few responses
        /// 16	taking a break - vacation
        /// 32	technical issues (logging in, etc.)
        /// 64	site performance (photos, essays, etc.)
        /// 128	Suspend other
        /// </summary>
        public int SuspendReasonId { get; set; }
    }
}