﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Profile
{
    public class UpdateProfileEnabledStatusRequest : MemberRequest
    {
        [DataMember(Name = "Enabled")]
        public bool Enabled { get; set; }

        [DataMember(Name = "AdminId")]
        public int AdminId { get; set; }

        [DataMember(Name = "ReasonId")]
        public int ReasonId { get; set; }
    }
}