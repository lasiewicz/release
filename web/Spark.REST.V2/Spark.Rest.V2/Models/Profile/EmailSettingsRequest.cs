﻿using Spark.Rest.V2.DataAccess.Profile;
using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.Profile
{
    public class EmailSettingsRequest : MemberRequest
    {
        /// <summary>
        /// Emails Settings and Alerts
        /// </summary>
        public EmailAlertSettings EmailAlertSettings { get; set; }

        /// <summary>
        /// Message Settings
        /// </summary>
        public MessageSettings MessageSettings { get; set; }

        /// <summary>
        /// Newsletters, Events and Offers 
        /// </summary>
        public NewsLetterSettings NewsLetterSettings { get; set; }
    }
}