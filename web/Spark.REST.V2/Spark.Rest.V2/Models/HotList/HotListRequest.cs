namespace Spark.REST.Models.HotList
{
    public class HotListRequest : BrandRequest
    {
        public int MemberId { get; set; }
        public string HotListCategory { get; set; }
		public int pageSize { get; set; }
		public int pageNumber { get; set; }
    }
}