namespace Spark.REST.Models.HotList
{
    public class OnMemberHotListRequest : BrandRequest
    {
        public int MemberId { get; set; }
		public int TargetMemberId { get; set; }
		public string HotListCategory { get; set; }
    }
}