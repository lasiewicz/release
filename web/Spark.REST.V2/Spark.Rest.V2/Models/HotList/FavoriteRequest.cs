﻿using System.Runtime.Serialization;

namespace Spark.REST.Models.HotList
{
    [DataContract(Name = "FavoriteRequest")]
    public class FavoriteRequest : MemberRequest
    {
		[DataMember(Name = "favoriteMemberId")]
        public int FavoriteMemberId { get; set; }
    }
}