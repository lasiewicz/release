﻿#region

using System;
using Spark.REST.Models;

#endregion

namespace Spark.Rest.V2.Models.HotList
{
    ///<summary>
    ///</summary>
    public class YNMVoteRequest : MemberRequest
    {
        /// <summary>
        /// </summary>
        public bool Yes { get; set; }

        /// <summary>
        /// </summary>
        public bool No { get; set; }

        /// <summary>
        /// </summary>
        public bool Maybe { get; set; }

        /// <summary>
        ///   Member Id that will be added to the requesting member's list.
        /// </summary>
        public int AddMemberId { get; set; }

        /// <summary>
        /// YN votes on the UT side go through a queue server. 
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}