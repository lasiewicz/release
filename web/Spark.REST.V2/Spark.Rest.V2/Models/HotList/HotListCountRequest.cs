using System.Collections.Generic;
using Matchnet.List.ValueObjects;

namespace Spark.REST.Models.HotList
{
    public class HotListCountRequest : BrandRequest
    {
        public int MemberId { get; set; }
    }
}