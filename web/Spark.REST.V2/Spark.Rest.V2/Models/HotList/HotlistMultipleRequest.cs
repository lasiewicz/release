﻿using Matchnet.List.ValueObjects;
using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Models.HotList
{
    public class HotlistMultipleRequest : BrandRequest
    {
        private List<HotListCategory> _CategoryList;

        public int MemberId { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }

        /// <summary>
        /// Comma delimited list of hotlist categories
        /// </summary>
        public string Categories { get; set; }

        [IgnoreDataMember]
        public List<HotListCategory> CategoryList
        {
            get
            {
                if (_CategoryList == null)
                {
                    _CategoryList = new List<HotListCategory>();
                    if (!string.IsNullOrEmpty(Categories))
                    {
                        string[] categoryIds = Categories.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in categoryIds)
                        {
                            HotListCategory category;
                            if (Enum.TryParse<HotListCategory>(s, true, out category))
                            {
                                if (!_CategoryList.Contains(category))
                                {
                                    _CategoryList.Add(category);
                                }
                            }
                        }
                    }
                }

                return _CategoryList;
            }
            set
            {
                _CategoryList = value;
            }
        }
    }
}