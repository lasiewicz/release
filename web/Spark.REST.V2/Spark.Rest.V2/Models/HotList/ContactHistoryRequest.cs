﻿using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.HotList
{
    public class ContactHistoryRequest : BrandRequest
    {
        public int TargetMemberID { get; set; }
        public bool IncludeMiniProfile { get; set; }
    }
}