﻿#region

using System;
using System.Runtime.Serialization;

#endregion

namespace Spark.REST.Models.HotList
{
    /// <summary>
    ///     for adding/deleting one member from/to another's hotlist
    /// </summary>
    [DataContract(Name = "HotlistChangeRequest")]
    public class HotlistChangeRequest : MemberRequest
    {
        public HotlistChangeRequest()
        {
            Comment = string.Empty;
        }

        /// <summary>
        ///     member to add or remove
        /// </summary>
        [DataMember(Name = "targetMemberId")]
        public int TargetMemberId { get; set; }

        /// <summary>
        ///     category- default, ignorelist, etc.
        /// </summary>
        [DataMember(Name = "hotListCategory")]
        public string HotListCategory { get; set; }

        /// <summary>
        ///     For tunneling DELETEs.  Needed for IE which doesnt allow cross domain DELETE method
        /// </summary>
        [DataMember(Name = "method")]
        public string Method { get; set; }

        [DataMember(Name = "comment")]
        public string Comment { get; set; }

        [DataMember(Name = "timestamp")]
        public DateTime Timestamp { get; set; }
    }
}