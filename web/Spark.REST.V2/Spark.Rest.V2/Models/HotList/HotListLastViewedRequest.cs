﻿using System;
using System.Runtime.Serialization;

namespace Spark.REST.Models.HotList
{
	public class HotListLastViewedRequest : BrandRequest
	{
		[DataMember(Name = "memberId")]
		public int MemberId { get; set; }

		[DataMember(Name = "hotListCategory")]
		public string HotListCategory { get; set; }
	}
}