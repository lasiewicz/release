﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.REST.Models.HotList
{
    public class BrokeredHotlistChangeRequest : HotlistChangeRequest
    {
        public int MemberId {get;set;}

        public DateTime Timestamp { get; set; }
    }
}