namespace Spark.REST.Models
{
    public class LookupByMemberUsernameRequest : MemberRequest
	{
        public string TargetMemberUsername { get; set; }

        public string AttributeSetName { get; set; }
	}
}
