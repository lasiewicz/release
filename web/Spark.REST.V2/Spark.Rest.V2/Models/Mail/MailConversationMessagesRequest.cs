﻿using Matchnet.Email.ValueObjects;
using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.Mail
{
    public class MailConversationMessagesRequest : BrandRequest
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TargetMemberID { get; set; }
        public bool MarkAsRead { get; set; }
        public string ThreadID { get; set; }

        private ConversationType _conversationType = ConversationType.Inbox;
        public ConversationType ConversationType
        {
            get { return _conversationType; }
            set { _conversationType = value; }
        }
    }
}