namespace Spark.REST.Models.Mail
{
    public class TeaseCollectionRequest : BrandRequest
    {
        public int TeaseCategoryId { get; set; }
    }
}