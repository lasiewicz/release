﻿using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.Mail
{
    public class MarkReadMailMessagesRequest : BrandRequest
    {
        public List<int> MessageListIDs { get; set; }
    }
}