﻿#region

using System.Collections.Generic;
using Spark.REST.Models;

#endregion

namespace Spark.Rest.V2.Models.Mail
{
    /// <summary>
    ///     Encapsulates a request to delete multiple messages.
    /// </summary>
    public class DeleteMessagesRequest : MemberRequest
    {
        /// <summary>
        ///     The list of messages to delete.
        /// </summary>
        public IEnumerable<DeleteMessage> MessagesToDelete { get; set; }
    }
}