﻿using Spark.Rest.V2.Entities.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.Mail
{
    public class MailConversationMembersResponse
    {
        public int GroupId { get; set; }
        public int MemberId { get; set; }
        public int MatchesFound { get; set; }
        public List<MailMember> MemberList { get; set; }
    }
}