﻿using Matchnet.Email.ValueObjects;
using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.Mail
{
    public class MailConversationMembersRequest : BrandRequest
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        private ConversationType _conversationType = ConversationType.Inbox;
        public int BodyLength { get; set; }

        public ConversationType ConversationType
        {
            get { return _conversationType; }
            set { _conversationType = value; }
        }
    }
}