﻿using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.Mail
{
    public class MessageMemberDeleteRequest : MemberRequest
    {
        public int MessageId { get; set; }
        public int TargetMemberId { get; set; }
    }
}