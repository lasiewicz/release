﻿using System;

namespace Spark.REST.Models.Mail
{
    public class DeleteMessageRequest : MemberRequest
    {
        public int MessageId { get; set; }
        public int CurrentFolderId { get; set; }
    }
}