﻿namespace Spark.Rest.V2.Models.Mail
{
    /// <summary>
    ///     Identifies a mail message to delete.
    /// </summary>
    public class DeleteMessage
    {
        /// <summary>
        ///     The message identifier.
        /// </summary>
        public int MessageId { get; set; }

        /// <summary>
        ///     The message's current folder identifier.
        /// </summary>
        public int CurrentFolderId { get; set; }
    }
}