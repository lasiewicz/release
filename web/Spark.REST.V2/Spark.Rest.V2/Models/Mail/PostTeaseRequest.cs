using System.Runtime.Serialization;
using Spark.REST.Entities.Mail;

namespace Spark.REST.Models.Mail
{
	public class PostTeaseRequest : MemberRequest
	{
		[DataMember(Name = "recipientMemberId")]
		public int RecipientMemberId { get; set; }

		[DataMember(Name = "teaseId")]
		public int TeaseId { get; set; }

		[DataMember(Name = "teaseCategoryId")]
		public int TeaseCategoryId { get; set; }

		[DataMember(Name = "body")]
		public string Body { get; set; }

		//[DataMember(Name = "mailType")]
		//public string MailType { get; set; }

		//[DataMember(Name = "originalMessageRepliedToId")]
		//public int OriginalMessageRepliedToId { get; set; }
	}
}