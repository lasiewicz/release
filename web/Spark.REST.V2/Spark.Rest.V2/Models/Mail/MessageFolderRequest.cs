namespace Spark.REST.Models.Mail
{
    /// <summary>
    /// 
    /// </summary>
    public class MessageFolderRequest : BrandRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public int FolderId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageNumber { get; set; }
    }
}