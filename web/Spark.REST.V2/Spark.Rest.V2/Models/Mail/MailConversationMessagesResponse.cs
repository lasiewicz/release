﻿using Spark.REST.Entities.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.Mail
{
    public class MailConversationMessagesResponse
    {
        public int GroupId { get; set; }
        public int MemberId { get; set; }
        public int TargetMemberId { get; set; }
        public int MatchesFound { get; set; }
        public List<MailMessageV2> MessageList { get; set; }
    }
}