using System.Runtime.Serialization;
namespace Spark.REST.Models.Mail
{
    public class MailMessageRequest : BrandRequest
    {
        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }
        [DataMember(Name = "messageListId")]
        public int MessageListId { get; set; }
        [DataMember(Name = "markAsRead")]
        public bool MarkAsRead { get; set; }

        public MailMessageRequest()
        {
            //default is true to keep it backwards compatible with current behavior
            MarkAsRead = true;
        }
    }
}