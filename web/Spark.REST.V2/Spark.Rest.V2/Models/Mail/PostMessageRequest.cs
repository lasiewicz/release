#region

using System;
using System.Runtime.Serialization;

#endregion

namespace Spark.REST.Models.Mail
{
    public class PostMessageRequest : BrandRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public PostMessageRequest()
        {
            RecipientMemberId = 0;
            Body = String.Empty;
            Subject = String.Empty;
            OriginalMessageRepliedToId = 0;
            IsDraft = false;
            IsAllAccess = false;
            DraftMessageId = 0;
        }

        [DataMember(Name = "recipientMemberId")]
        public int RecipientMemberId { get; set; }

        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }

        [DataMember(Name = "subject")]
        public string Subject { get; set; }

        [DataMember(Name = "body")]
        public string Body { get; set; }

        [DataMember(Name = "mailType")]
        public string MailType { get; set; }

        [DataMember(Name = "originalMessageRepliedToId")]
        public int OriginalMessageRepliedToId { get; set; }

        [DataMember(Name = "isDraft")]
        public bool IsDraft { get; set; }

        /// <summary>
        ///     Optional parameter to support sending All Access mail
        /// </summary>
        [DataMember(Name = "isAllAccess")]
        public bool IsAllAccess { get; set; }

        [DataMember(Name = "draftMessageId")]
        public int DraftMessageId { get; set; }
    }
}