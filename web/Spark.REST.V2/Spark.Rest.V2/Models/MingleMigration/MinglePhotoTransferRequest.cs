﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ValueObjects.Photos;
using Spark.MingleMigration.ValueObjects;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.MingleMigration
{
    public class MinglePhotoTransferRequest: MemberRequest
    {
        private List<PhotoTransferFile> _files;
        
        public string FilesJson { get; set; }
        
        public int ListOrder { get; set; }
        public string Caption { get; set; }
        public DateTime InsertDate { get; set; }
        public bool ApprovedForMain { get; set; }
        public bool IsMain { get; set; }
        
        public List<PhotoTransferFile> Files
        {
            get
            {
                if (_files == null)
                {
                    if (!String.IsNullOrEmpty(FilesJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _files = ser.Deserialize<List<PhotoTransferFile>>(FilesJson);
                    }
                    _files = _files ?? new List<PhotoTransferFile>();
                }
                return _files;
            }

            set { _files = value; }
        }
    }
}