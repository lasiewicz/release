﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Newtonsoft.Json.Linq;
using Spark.MingleMigration.ValueObjects;
using Spark.REST.Models;
using System.Web.Script.Serialization;
using Spark.Rest.V2.Models.HotList;

namespace Spark.Rest.V2.Models.MingleMigration
{
    public class MingleListMigrationRequest: MemberRequest
    {
        [DataMember(Name = "listDataJson")]
        public List<MingleList> ListData { get; set; }
    }

    public class MingleListMailMaskMigrationRequest : MemberRequest
    {
        [DataMember(Name = "alertEmailSent")]
        public bool AlertEmailSent { get; set; }

        [DataMember(Name = "clickEmailSent")]
        public bool ClickEmailSent { get; set; }

        [DataMember(Name = "targetMemberId")]
        public int TargetMemberId { get; set; }

    }

    public class MingleYNMMigrationRequest : MemberRequest
    {
        [DataMember(Name = "ynmDataJson")]
        public List<MingleYNMVoteRequest> YNMData { get; set; }
    }

    ///<summary>
    ///</summary>
    public class MingleYNMVoteRequest : YNMVoteRequest
    {
        [DataMember(Name = "alertEmailSent")]
        public bool AlertEmailSent { get; set; }

        [DataMember(Name = "clickEmailSent")]
        public bool ClickEmailSent { get; set; }
    }
}