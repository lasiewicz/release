﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using Spark.MingleMigration.ValueObjects;
using Spark.REST.Models;
using Spark.Rest.V2.Entities.MingleMigration;
using Spark.SearchPreferences;

namespace Spark.Rest.V2.Models.Member
{
    [DataContract(Name = "MingleRegistrationRequest")]
    public class MingleRegistrationRequest : BrandRequest
    {
        private Dictionary<string, List<int>> _attributeDataMultiValue;
        private Dictionary<string, object> _attributeData;
        private List<SearchPreference> _searchPreferences;
        private List<MultiValuedSearchPreference> _searchPreferencesMultiValue;

        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
        [DataMember(Name = "ipAddress")]
        public string IpAddress { get; set; }

        [DataMember(Name = "AttributeDataJson")]
        public string AttributeDataJson { get; set; } // JSON dictionary

        [DataMember(Name = "AttributeDataMultiValueJson")]
        public string AttributeDataMultiValueJson { get; set; } // JSON dictionary

        [DataMember(Name = "SearchPreferencesJson")]
        public string SearchPreferencesJson { get; set; } // JSON dictionary

        [DataMember(Name = "SearchPreferencesMultiValueJson")]
        public string SearchPreferencesMultiValueJson { get; set; } // JSON dictionary

        public Dictionary<string, object> AttributeData
        {
            get
            {
                if (_attributeData == null)
                {
                    if (!String.IsNullOrEmpty(AttributeDataJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _attributeData = ser.Deserialize<Dictionary<string, object>>(AttributeDataJson);
                    }
                    else if (null != JSONObject && JSONObject is JObject)
                    {
                        var list = ((JObject)JSONObject).SelectToken("AttributeDataJson").ToList();
                        foreach (JProperty jp in list)
                        {
                            if (null == _attributeData) _attributeData = new Dictionary<string, object>();
                            if (jp.Value.Type == JTokenType.Integer)
                            {
                                _attributeData.Add(jp.Name, jp.Value.ToObject<int>());
                            }
                            else if (jp.Value.Type == JTokenType.Date)
                            {
                                _attributeData.Add(jp.Name, jp.Value.ToObject<DateTime>());
                            }
                            else
                            {
                                _attributeData.Add(jp.Name, jp.Value.ToString());
                            }
                        }
                    }
                    _attributeData = _attributeData ?? new Dictionary<string, object>();
                }
                return _attributeData;
            }

            set { _attributeData = value; }
        }

        public List<SearchPreference> SearchPreferences
        {
            get
            {
                if (_searchPreferences == null)
                {
                    if (!String.IsNullOrEmpty(SearchPreferencesJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _searchPreferences = ser.Deserialize<List<SearchPreference>>(SearchPreferencesJson);
                    }
                    else if (null != JSONObject && JSONObject is JObject)
                    {
                        var list = ((JObject)JSONObject).SelectToken("SearchPreferencesJson").ToList();
                        foreach (JProperty jp in list)
                        {
                            if (null == _searchPreferences) _searchPreferences = new List<SearchPreference>();
                            if (jp.Value.Type == JTokenType.Integer)
                            {
                                _searchPreferences.Add(jp.Value.ToObject<SearchPreference>());
                            }
                        }
                    }
                    _searchPreferences = _searchPreferences ?? new List<SearchPreference>();
                }
                return _searchPreferences;
            }

            set { _searchPreferences = value; }
        }

        public Dictionary<string, List<int>> AttributeDataMultiValue
        {
            get
            {
                if (_attributeDataMultiValue == null)
                {
                    if (!String.IsNullOrEmpty(AttributeDataMultiValueJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _attributeDataMultiValue = ser.Deserialize<Dictionary<string, List<int>>>(AttributeDataMultiValueJson);
                    }
                    else if (null != JSONObject && JSONObject is JObject)
                    {
                        var list = ((JObject)JSONObject).SelectToken("AttributeDataMultiValueJson").ToList();
                        foreach (JProperty jp in list)
                        {
                            if (null == _attributeDataMultiValue) _attributeDataMultiValue = new Dictionary<string, List<int>>();
                            _attributeDataMultiValue.Add(jp.Name, jp.Value.Select(m => m.Value<int>()).ToList<int>());
                        }
                    }
                    _attributeDataMultiValue = _attributeDataMultiValue ?? new Dictionary<string, List<int>>();
                }
                return _attributeDataMultiValue;
            }
            set { _attributeDataMultiValue = value; }
        }

        public List<MultiValuedSearchPreference> SearchPreferencesMultiValue
        {
            get
            {
                if (_searchPreferencesMultiValue == null)
                {
                    if (!String.IsNullOrEmpty(SearchPreferencesMultiValueJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _searchPreferencesMultiValue = ser.Deserialize<List<MultiValuedSearchPreference>>(SearchPreferencesMultiValueJson);
                    }
                    else if (null != JSONObject && JSONObject is JObject)
                    {
                        var list = ((JObject)JSONObject).SelectToken("SearchPreferencesMultiValueJson").ToList();
                        foreach (JProperty jp in list)
                        {
                            if (null == _searchPreferencesMultiValue) _searchPreferencesMultiValue = new List<MultiValuedSearchPreference>();
                            _searchPreferencesMultiValue.Add(jp.Value.ToObject<MultiValuedSearchPreference>());
                        }
                    }
                    _searchPreferencesMultiValue = _searchPreferencesMultiValue ?? new List<MultiValuedSearchPreference>();
                }
                return _searchPreferencesMultiValue;
            }
            set { _searchPreferencesMultiValue = value; }
        }
    }


}