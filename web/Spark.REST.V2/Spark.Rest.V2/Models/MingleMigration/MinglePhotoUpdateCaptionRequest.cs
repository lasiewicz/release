﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.MingleMigration
{
    public class MinglePhotoUpdateCaptionRequest : MemberRequest
    {
        public int MemberPhotoId { get; set; }
        public string Caption { get; set; }
    }
}