﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.MingleMigration
{
    public class SaveLogonCredentialsRequest: MemberRequest
    {
        [DataMember(Name = "userName")]
        public string UserName { get; set; }
        [DataMember(Name = "emailAddress")]
        public string EmailAddress { get; set; }
        [DataMember(Name = "P1")]
        public string P1 { get; set; }
        [DataMember(Name = "P2")]
        public string P2 { get; set; }
    }
}