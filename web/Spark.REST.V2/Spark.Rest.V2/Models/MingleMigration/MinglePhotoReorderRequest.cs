﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.MingleMigration
{
    public class MinglePhotoReorderRequest : MemberRequest
    {
        private List<PhotoOrder> _PhotoOrders;
        public string PhotoOrdersJson { get; set; }


        public List<PhotoOrder> PhotoOrders
        {
            get
            {
                if (_PhotoOrders == null)
                {
                    if (!String.IsNullOrEmpty(PhotoOrdersJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _PhotoOrders = ser.Deserialize<List<PhotoOrder>>(PhotoOrdersJson);
                    }
                    _PhotoOrders = _PhotoOrders ?? new List<PhotoOrder>();
                }
                return _PhotoOrders;
            }

            set { _PhotoOrders = value; }
        }

    }

    public class PhotoOrder
    {
        public int MemberPhotoId { get; set; }
        public int ListOrder { get; set; }
    }
}