﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.MingleMigration
{
    public class MinglePhotoApprovalRequest: MemberRequest  
    {
        public int MemberPhotoId { get; set; }
        public int CropX { get; set; }
        public int CropY { get; set; }
        public int CropWidth { get; set; }
        public int CropHeight { get; set; }
        public int ThumbX { get; set; }
        public int ThumbY { get; set; }
        public int ThumbWidth { get; set; }
        public int ThumbHeight { get; set; }
        public int Rotation { get; set; }
        public int AdminMemberId { get; set; }
        public bool CaptionOnly { get; set; }
        public bool CaptionDelete { get; set; }
        public bool ApprovedForMain { get; set; }
    }
}