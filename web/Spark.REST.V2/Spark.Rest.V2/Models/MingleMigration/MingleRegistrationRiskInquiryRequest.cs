﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Newtonsoft.Json.Linq;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.MingleMigration
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "MingleRegistrationRiskInquiryRequest")]
    public class MingleRegistrationRiskInquiryRequest : BrandRequest
    {
        private Dictionary<string, object> _attributeData;

        /// <summary>
        /// Required field
        /// </summary>
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }

        /// <summary>
        /// Required field
        /// </summary>
        [DataMember(Name = "SessionID")]
        public string RegistrationSessionId { get; set; }

        /// <summary>
        /// Required field
        /// </summary>
        [DataMember(Name = "ipAddress")]
        public string IpAddress { get; set; }

        /// <summary>
        /// Required field
        /// </summary>
        [DataMember(Name = "emailAddress")]
        public string EmailAddress { get; set; }

        

        [DataMember(Name = "AttributeDataJson")]
        public string AttributeDataJson { get; set; } // JSON dictionary

        public Dictionary<string, object> AttributeData
        {
            get
            {
                if (_attributeData == null)
                {
                    if (!String.IsNullOrEmpty(AttributeDataJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _attributeData = ser.Deserialize<Dictionary<string, object>>(AttributeDataJson);
                    }
                    else if (null != JSONObject && JSONObject is JObject)
                    {
                        var list = ((JObject)JSONObject).SelectToken("AttributeDataJson").ToList();
                        foreach (JProperty jp in list)
                        {
                            if (null == _attributeData) _attributeData = new Dictionary<string, object>();
                            if (jp.Value.Type == JTokenType.Integer)
                            {
                                _attributeData.Add(jp.Name, jp.Value.ToObject<int>());
                            }
                            else if (jp.Value.Type == JTokenType.Date)
                            {
                                _attributeData.Add(jp.Name, jp.Value.ToObject<DateTime>());
                            }
                            else
                            {
                                _attributeData.Add(jp.Name, jp.Value.ToString());
                            }
                        }
                    }
                    _attributeData = _attributeData ?? new Dictionary<string, object>();
                }
                return _attributeData;
            }

            set { _attributeData = value; }
        }
    }
}