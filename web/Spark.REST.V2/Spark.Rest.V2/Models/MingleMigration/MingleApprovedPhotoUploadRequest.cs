﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.MingleMigration
{
    public class MingleApprovedPhotoUploadRequest : MemberRequest
    {
        public string FileData { get; set; }
        public string Caption { get; set; }
        public int ListOrder { get; set; }
        public string CloudPath {get;set;}
        public int Height {get;set;}
        public int Width {get;set;}
        public int Size {get;set;}
        public bool ApprovedForMain {get;set;}
        public bool IsMain { get; set; }
        public string OriginalCloudPath { get; set; }
        public int OriginalHeight { get; set; }
        public int OriginalWidth { get; set; }
        public int OriginalSize { get; set; }
    }
}