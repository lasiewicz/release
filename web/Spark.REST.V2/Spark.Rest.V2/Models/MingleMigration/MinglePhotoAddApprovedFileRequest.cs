﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.REST.Models;
using Matchnet.Member.ValueObjects.Photos;

namespace Spark.Rest.V2.Models.MingleMigration
{
    public class MinglePhotoAddApprovedFileRequest : MemberRequest
    {
        public int MemberPhotoId { get; set; }
        public PhotoFileType FileType { get; set; }
        public string FileData { get; set; }
        public string CloudPath { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Size { get; set; }
    }
}