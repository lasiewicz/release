﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.MingleMigration
{
    public class MinglePhotoDeleteRequest: MemberRequest    
    {
        public int MemberPhotoId { get; set; }
    }
}