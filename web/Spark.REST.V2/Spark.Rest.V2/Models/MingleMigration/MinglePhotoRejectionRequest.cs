﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.MingleMigration
{
    public class MinglePhotoRejectionRequest : MinglePhotoDeleteRequest
    {
        public int AdminMemberId { get; set; }
        public int RejectionReasonId { get; set; }
    }
}