﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Spark.REST.Models.Session
{
    public class SessionRequest : RequestBase
    {
        public String Key { get; set; }

        [DataMember(Name = "propertiesJson")]
        public string PropertiesJson { get; set; }

        // JSON 

        private Dictionary<String, Object> _properties;

        public Dictionary<String, Object> Properties
        {
            get
            {
                if (_properties == null)
                {
                    if (!String.IsNullOrEmpty(PropertiesJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _properties = ser.Deserialize<Dictionary<string, object>>(PropertiesJson);
                    }
                    _properties = _properties ?? new Dictionary<string, object>();
                }
                return _properties;
            }
        }
    }
}