﻿#region

using System.Runtime.Serialization;

#endregion

namespace Spark.REST.Models.Session
{
    [DataContract(Name = "CreateSessionTrackingRequest")]
    public class CreateSessionTrackingRequest : BrandRequest
    {
        [DataMember(Name = "SessionID")]
        public string SessionID { get; set; }

        [DataMember(Name = "PromotionID")]
        public int PromotionID { get; set; }

        [DataMember(Name = "LuggageID")]
        public string LuggageID { get; set; }

        [DataMember(Name = "BannerID")]
        public int BannerID { get; set; }

        [DataMember(Name = "ClientIP")]
        public string ClientIP { get; set; }

        [DataMember(Name = "ReferrerURL")]
        public string ReferrerURL { get; set; }

        [DataMember(Name = "LandingPageID")]
        public string LandingPageID { get; set; }

        [DataMember(Name = "LandingPageTestID")]
        public string LandingPageTestID { get; set; }
    }
}