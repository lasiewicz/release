﻿using System.Runtime.Serialization;
using Spark.REST.Models;

namespace Spark.REST.Models.Session
{
    [DataContract(Name = "SessionMemberRequest")]
    public class SessionMemberRequest : RequestBase
    {
        [DataMember(Name = "SessionId")]
        public string SessionId { get; set; }
    }
}
