﻿using System;
using System.Runtime.Serialization;

namespace Spark.REST.Models.Session
{
    [DataContract(Name = "SessionCreateRequest")]
    public class SessionCreateRequest: RequestBase
    {
        [DataMember(Name = "BrandId")]
        public Int32 BrandId { get; set; }

        [DataMember(Name = "MemberID")]
        public Int32 MemberID { get; set; }
    }
}
