﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.REST.Models.Session
{
    [DataContract(Name = "SessionCreateRequestWithSessionId")]
    public class SessionCreateRequestWithSessionId : SessionCreateRequest
    {
        [DataMember(Name = "SessionId")]
        public string SessionId { get; set; }
    }
}