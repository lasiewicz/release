using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.REST.Models
{
    public class BrandRequest : RequestBase
    {

		protected static Dictionary<string, Brand> BrandLookup = new Dictionary<string, Brand>();


		[DataMember(Name = "brandUri")]
        public string BrandUri { get; set; }

    	private Brand _brand;

		public Brand Brand
		{
            [DebuggerStepThrough]
            get
			{
				if (_brand == null)
				{
					if (BrandUri == null)
					{
						throw new Exception(String.Format("BrandUri has no value, cannot retrieve brand"));
					}
					if (!BrandLookup.TryGetValue(BrandUri, out _brand))
					{
						_brand = BrandConfigSA.Instance.GetBrandByURI(BrandUri);
						if (_brand == null)
						{
							throw new Exception(String.Format("no brand found for brand URL: {0}", BrandUri));
						}
						BrandLookup.Add(BrandUri, _brand);
					}
				}
				return _brand;
			}
		}

        public int BrandId
        {
            get
            {
                return Brand.BrandID;
            }
        }

    }
}