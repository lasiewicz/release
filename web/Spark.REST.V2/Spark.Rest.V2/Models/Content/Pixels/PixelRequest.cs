﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;

namespace Spark.REST.Models.Content.Pixels
{
    [DataContract(Name = "PixelRequest")]
    public class PixelRequest : BrandRequest
    {
        [DataMember(Name = "PageName")]
        public string PageName { get; set; }

        [DataMember(Name = "PromotionID")]
        public int PromotionID { get; set; }

        [DataMember(Name = "LanguageID")]
        public int LanguageID { get; set; }

        [DataMember(Name = "ReferralURL")]
        public string ReferralURL { get; set; }

        [DataMember(Name = "LuggageID")]
        public string LuggageID { get; set; }

        [DataMember(Name = "MemberID")]
        public int MemberID { get; set; }

        [DataMember(Name = "RegionID")]
        public int RegionID { get; set; }

        [DataMember(Name = "GenderMask")]
        public int GenderMask { get; set; }

        [DataMember(Name = "Birthdate")]
        public DateTime Birthdate { get; set; }

        [DataMember(Name = "SubscriptionAmount")]
        public string SubscriptionAmount { get; set; }

        [DataMember(Name = "SubscriptionDuration")]
        public string SubscriptionDuration { get; set; }

        public PixelRequest()
        {
            MemberID = 0;
            RegionID = 0;
            GenderMask = 0;
            PromotionID = 0;
            LanguageID = 0;
            Birthdate = DateTime.MinValue;
        }
    }
}