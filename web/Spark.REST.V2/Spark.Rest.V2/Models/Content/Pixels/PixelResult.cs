﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.REST.Models.Content.Pixels
{
    [DataContract(Name = "PixelResult")]
    public class PixelResult
    {
        [DataMember(Name = "RenderedPixels")]
        public List<string> RenderedPixels { get; set; }
    }
}