﻿using Spark.REST.Models;

namespace Spark.Rest.Models.Content.Promotion
{
    public class AffiliateTrackingRequest : RequestBase
    {
        /// <summary>
        /// This is the absolute url of the referrer, i.g.http://www.google.com/?prm=343
        /// </summary>
        public string UrlReferrer { get; set; }
    }
}