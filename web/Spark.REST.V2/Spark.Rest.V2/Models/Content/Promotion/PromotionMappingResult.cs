﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.REST.Models.Content.Promotion
{
    [DataContract(Name = "PromotionMappingResult")]
    public class PromotionMappingResult
    {
        [DataMember(Name = "PromotionID")]
        public int PromotionID { get; set; }

        [DataMember(Name = "LuggageID")]
        public string LuggageID { get; set; } 
    }
}