﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;

namespace Spark.REST.Models.Content.Promotion
{
    [DataContract(Name = "PromotionMappingRequest")]
    public class PromotionMappingRequest : BrandRequest
    {
        [DataMember(Name = "ReferralURL")]
        public string ReferralURL { get; set; }

        [DataMember(Name = "ReferralHost")]
        public string ReferralHost { get; set; }

        [DataMember(Name = "ReferralURLQueryString")]
        public string ReferralURLQueryString { get; set; }
    }
}