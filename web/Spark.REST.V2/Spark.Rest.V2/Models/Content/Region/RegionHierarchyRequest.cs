﻿using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Models.Content.Region
{
    [DataContract(Name = "RegionHierarchyRequest")]
    public class RegionHierarchyRequest : BrandRequest
    {
        private int _languageId;
        private int _regionId;

        [DataMember(Name = "languageId")]
        public int LanguageId
        {
            get { return _languageId; }
            set { _languageId = value; }
        }

        [DataMember(Name = "regionId")]
        public int RegionId
        {
            get { return _regionId; }
            set { _regionId = value; }
        }
    }
}