﻿using System.Runtime.Serialization;

namespace Spark.REST.Models.Content.Region
{
    [DataContract(Name = "CitiesRequest")]
    public class CitiesRequest : StatesRequest
    {
        private int _stateRegionId;

        [DataMember(Name = "StateRegionId")]
        public int StateRegionId
        {
            get { return _stateRegionId; }
            set { _stateRegionId = value; }
        }
    }
}