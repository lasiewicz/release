﻿using System.Runtime.Serialization;

namespace Spark.REST.Models.Content.Region
{
    [DataContract(Name = "ChildRegionsRequest")]
    public class ChildRegionsRequest : CountriesRequest
    {
        private int _parentRegionId;

        [DataMember(Name = "ParentRegionId")]
        public int ParentRegionId
        {
            get { return _parentRegionId; }
            set { _parentRegionId = value; }
        }
    }
}