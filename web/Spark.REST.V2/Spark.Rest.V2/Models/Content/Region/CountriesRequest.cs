﻿using System.Runtime.Serialization;

namespace Spark.REST.Models.Content.Region
{
    [DataContract(Name = "CountriesRequest")]
    public class CountriesRequest : BrandRequest
    {
        private int _languageId;

        [DataMember(Name = "LanguageId")]
        public int LanguageId
        {
            get { return _languageId; }
            set { _languageId = value; }
        }
    }
}