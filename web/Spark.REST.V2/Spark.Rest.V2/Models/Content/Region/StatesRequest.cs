﻿using System.Runtime.Serialization;

namespace Spark.REST.Models.Content.Region
{
    [DataContract(Name = "StatesRequest")]
    public class StatesRequest : CountriesRequest
    {
        private int _countryRegionId;

        [DataMember(Name = "CountryRegionId")]
        public int CountryRegionId
        {
            get { return _countryRegionId; }
            set { _countryRegionId = value; }
        }
    }
}