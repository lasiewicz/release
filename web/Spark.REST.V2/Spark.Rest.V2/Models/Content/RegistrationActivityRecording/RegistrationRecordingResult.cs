﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.REST.Models.Content.RegistrationActivityRecording
{
    [DataContract(Name = "RegistrationRecordingResult", Namespace = "")]
    public class RegistrationRecordingResult
    {

        [DataMember(Name = "Recorded")]
        public bool Recorded { get; set; }
    }
}