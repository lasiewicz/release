﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.REST.Models.Content.RegistrationActivityRecording
{
    [DataContract(Name = "RegistrationStep", Namespace = "")]
    public class RegistrationStep
    {
        private Dictionary<string, object> _stepDetails;
        
        [DataMember(Name = "RegistrationSessionID")]
        public string RegistrationSessionID { get; set; }

        [DataMember(Name = "StepID")]
        public int StepID { get; set; }

        [DataMember(Name = "TimeStamp")]
        public DateTime TimeStamp { get; set; }

        public Dictionary<string, object> StepDetails
        {
            get
            {
                if (_stepDetails == null)
                {
                    if (!String.IsNullOrEmpty(StepDetailsJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _stepDetails = ser.Deserialize<Dictionary<string, object>>(StepDetailsJson);
                    }
                    _stepDetails = _stepDetails ?? new Dictionary<string, object>();
                }
                return _stepDetails;
            }
        }

        [DataMember(Name = "StepDetailsJson")]
        public string StepDetailsJson { get; set; } // JSON dictionary
    }
}