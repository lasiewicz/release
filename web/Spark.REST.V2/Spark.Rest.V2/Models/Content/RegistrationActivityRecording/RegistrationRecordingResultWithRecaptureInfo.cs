﻿using System.Runtime.Serialization;

namespace Spark.REST.Models.Content.RegistrationActivityRecording
{
    [DataContract(Name = "RegistrationRecordingResultWithRecaptureInfo", Namespace = "")]
    public class RegistrationRecordingResultWithRecaptureInfo : RegistrationRecordingResult
    {
        [DataMember(Name = "RecaptureID")]
        public string RecaptureID { get; set; }
    }
}