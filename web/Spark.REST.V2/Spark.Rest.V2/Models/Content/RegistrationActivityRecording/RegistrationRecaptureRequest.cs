﻿using System.Runtime.Serialization;

namespace Spark.REST.Models.Content.RegistrationActivityRecording
{
    [DataContract(Name = "RegistrationRecaptureRequest", Namespace = "")]
    public class RegistrationRecaptureRequest
    {
        [DataMember(Name = "RecaptureID")]
        public string RecaptureID { get; set; }
    }
}