﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Spark.REST.Models.Content.RegistrationActivityRecording
{
    [DataContract(Name = "RegistrationStepWithRecaptureInfo", Namespace = "")]
    public class RegistrationStepWithRecaptureInfo: RegistrationStep
    {
        private Dictionary<string, object> _allRegFields;

        [DataMember(Name = "RecaptureID")]
        public string RecaptureID { get; set; }

        [DataMember(Name = "BrandID")]
        public int BrandID { get; set; }

        [DataMember(Name = "EmailAddress")]
        public string EmailAddress { get; set; }

        public Dictionary<string, object> AllRegFields
        {
            get
            {
                if (_allRegFields == null)
                {
                    if (!String.IsNullOrEmpty(AllRegFieldsJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        _allRegFields = ser.Deserialize<Dictionary<string, object>>(AllRegFieldsJson);
                    }
                    _allRegFields = _allRegFields ?? new Dictionary<string, object>();
                }
                return _allRegFields;
            }
        }

        [DataMember(Name = "AllRegFieldsJson")]
        public string AllRegFieldsJson { get; set; } // JSON dictionary
    }
}