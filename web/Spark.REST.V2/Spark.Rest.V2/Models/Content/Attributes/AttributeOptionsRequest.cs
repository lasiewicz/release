using System.Runtime.Serialization;

namespace Spark.REST.Models.Content.AttributeMetaData
{
    [DataContract(Name = "AttributeOptionsRequest")]
    public class AttributeOptionsRequest : BrandRequest
    {
        private string _attributeName;

        [DataMember(Name = "AttributeName")]
        public string AttributeName
        {
            get { return _attributeName; }
            set { _attributeName = value; }
        }
    }
}