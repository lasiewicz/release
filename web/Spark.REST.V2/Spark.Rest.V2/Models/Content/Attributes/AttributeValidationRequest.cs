﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.Models.Content.Attributes
{
    [DataContract(Name = "AttributeValidationRequest", Namespace = "")]
    public class AttributeValidationRequest : BrandRequest
    {
        [DataMember(Name = "AttributeName")]
        public string AttributeName { get; set; }

        [DataMember(Name = "AttributeValue")]
        public string AttributeValue { get; set; }
    }
}