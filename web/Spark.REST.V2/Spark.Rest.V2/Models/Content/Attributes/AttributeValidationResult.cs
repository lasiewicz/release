﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.Models.Content.Attributes
{
    [DataContract(Name = "AttributeValidationResult", Namespace = "")]
    public class AttributeValidationResult
    {
        [DataMember(Name = "AttributeValueExists")]
        public bool AttributeValueExists { get; set; }
    }
}