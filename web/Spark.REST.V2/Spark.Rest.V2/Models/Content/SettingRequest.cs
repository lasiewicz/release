namespace Spark.REST.Models.Content
{
    public class SettingRequest : BrandRequest
    {
        public string SettingName { get; set; }
    }
}