﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.Subscription
{
    public class AdminPaymentUiPostDataRequest : Spark.REST.Models.Subscription.PaymentUiPostDataRequest
    {
        public int AdminMemberID { get; set; }
        public string AdminDomainName { get; set; }
    }
}