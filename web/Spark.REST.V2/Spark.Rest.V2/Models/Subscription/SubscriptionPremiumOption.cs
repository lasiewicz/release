﻿
using Matchnet.Purchase.ValueObjects;
using System.Collections.Generic;
using System.Text;

namespace Spark.Rest.V2.Models.Subscription
{
    public class SubscriptionPremiumOption : SubscriptionBase
    {
        /// <summary>
        /// Type of premium option to subscribe to:
        /// 
        /// None = 0,
        /// HighlightedProfile = 1,
        /// SpotlightMember = 2,
        /// JMeter = 4,
        /// ColorCode = 8,
        /// ServiceFee = 16,
        /// AllAccess = 32,
        /// AllAccessEmail = 64,
        /// ReadReceipt = 128
        /// 
        /// </summary>
        public PremiumType PremiumSubscriptionType { get; set; }
        public List<PremiumType> PremiumSubscriptionTypeList { get; set; }
        public bool IsBase { get; set; }
        public int PackageID { get; set; }

        public string PremiumSubscriptionTypeDescription
        {
            get {
                StringBuilder desc = new StringBuilder();
                int count = 1;
                foreach (PremiumType pt in PremiumSubscriptionTypeList)
                {
                    if (count == 1)
                    {
                        desc.Append(pt.ToString());
                    }
                    else
                    {
                        desc.Append(", " + pt.ToString());
                    }
                    count++;
                }

                return desc.ToString();
            }
        }
    }
}