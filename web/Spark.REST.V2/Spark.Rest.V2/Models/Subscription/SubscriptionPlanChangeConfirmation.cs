﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Purchase.ValueObjects;
using Spark.Common.RenewalService;

namespace Spark.Rest.V2.Models.Subscription
{
    /// <summary>
    /// Encapsulates a change confirmation in the member's subscription
    /// </summary>
    public class SubscriptionPlanChangeConfirmation
    {
        /// <summary>
        /// The subscription package identifier.
        /// </summary>
        public int RenewalSubscriptionId { get; set; }

        /// <summary>
        /// The renewal response from SUA
        /// </summary>
        public RenewalResponse RenewalResponse { get; set; }

        /// <summary>
        /// The plan type.
        /// </summary>
        public PremiumType PremiumType { get; set; }
    }
}