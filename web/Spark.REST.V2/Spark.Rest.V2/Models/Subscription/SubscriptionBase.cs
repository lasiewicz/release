﻿
using System;
using Matchnet;
using Matchnet.Purchase.ValueObjects;

namespace Spark.Rest.V2.Models.Subscription
{
    /// <summary>
    /// The base class for the Subscription object types
    /// Contains the required properties for all subscription types
    /// </summary>
    public class SubscriptionBase
    {
        /// <summary>
        /// Is the premium option enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// How much will the premium service cost per the duration.
        /// </summary>
        /// <value>
        /// The renewal rate.
        /// </value>
        public decimal RenewalRate { get; set; }

        /// <summary>
        /// The cost currency
        /// </summary>
        /// <value>
        /// The type of the currency.
        /// </value>
        public CurrencyType CurrencyType { get; set; }

        /// <summary>
        /// Gets or sets the renewal date.
        /// </summary>
        /// <value>
        /// The renewal date.
        /// </value>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// How often will the charge occur.
        /// </summary>
        /// <value>
        /// The type of the renewal duration.
        /// </value>
        public DurationType RenewalDurationType { get; set; }


        /// <summary>
        /// Gets or sets the duration of the renewal.
        /// </summary>
        /// <value>
        /// The duration of the renewal.
        /// </value>
        public int RenewalDuration { get; set; }

        /// <summary>
        /// Gets or sets the initial cost.
        /// </summary>
        /// <value>
        /// The initial cost.
        /// </value>
        public decimal InitialCost { get; set; }

        /// <summary>
        /// Gets or sets the initial type of the duration.
        /// </summary>
        /// <value>
        /// The initial type of the duration.
        /// </value>
        public DurationType InitialDurationType { get; set; }

        /// <summary>
        /// Gets or sets the initial duration.
        /// </summary>
        /// <value>
        /// The initial duration.
        /// </value>
        public int InitialDuration { get; set; }
    }
}