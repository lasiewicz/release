﻿using System.Runtime.Serialization;

namespace Spark.REST.Models.Subscription
{
	public class TransactionInfoRequest : MemberRequest
	{
		[DataMember(Name = "orderId")]
		public int OrderId { get; set; }
	}
}