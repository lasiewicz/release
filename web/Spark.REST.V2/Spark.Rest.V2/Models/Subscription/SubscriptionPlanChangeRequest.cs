﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Subscription
{
    /// <summary>
    /// This class represents a request to change the member's subscription plans.
    /// </summary>
    public class SubscriptionPlanChangeRequest : MemberRequest
    {
        /// <summary>
        /// Why are you terminating the subscription?
        /// </summary>
        public int TerminationReasonId { get; set; }

        /// <summary>
        /// Do you also want to self-suspend and hide your profile?
        /// </summary>
        public bool IsSelfSuspended { get; set; }

        /// <summary>
        /// The base subscription plan options.
        /// </summary>
        public SubscriptionBase BaseSubscriptionPlan { get; set; }

        /// <summary>
        /// Add/remove premium options.
        /// </summary>
        public List<SubscriptionPremiumOption> PremiumOptions { get; set; }
    }
}