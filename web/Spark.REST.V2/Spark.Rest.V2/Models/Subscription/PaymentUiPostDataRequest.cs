﻿#region

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Matchnet.PromoEngine.ValueObjects;
using Spark.REST.DataAccess.Subscription;
using Matchnet.Purchase.ValueObjects;

#endregion

namespace Spark.REST.Models.Subscription
{
    public class PaymentUiPostDataRequest : Spark.REST.Models.MemberRequest
    {
        private Dictionary<String, String> omnitureVariables;
        private Dictionary<String, String> analyticVariables;

        public PaymentUiPostDataRequest()
        {
            CancelUrl = "";
            ReturnUrl = "";
            ConfirmationUrl = "";
            DestinationUrl = "";
            GiftDestinationUrl = "";
            PrtId = "";
            SrId = "";
            EID = "";
            PromoType = Matchnet.PromoEngine.ValueObjects.PromoType.Mobile;
            PaymentType = PaymentType.CreditCard;
            OrderID = "";
            PaymentUiPostDataType = UnifiedPaymentSystemHelper.PaymentUiPostDataType.Subscription;
            TemplateId = 0;
            PromoID = 0;
            PremiumType = Matchnet.Purchase.ValueObjects.PremiumType.None;
        }

        public string CancelUrl { get; set; }
        public string ReturnUrl { get; set; }
        public string ConfirmationUrl { get; set; }
        public string DestinationUrl { get; set; }
        public string GiftDestinationUrl { get; set; }

        /// <summary>
        ///     Purchase Reason Type Id. Semicolon separated for multiple values.
        /// </summary>
        public string PrtId { get; set; }

        /// <summary>
        ///     Subscription Reason (?) Id. Semicolon separated for multiple values.
        /// </summary>
        public string SrId { get; set; }

        /// <summary>
        /// The EID this is an optional param. Its used for tracking. 
        /// </summary>
        public string EID { get; set; }


        public string ClientIp { get; set; }
        public string PrtTitle { get; set; }
        public string OmnitureVariablesJson { get; set; }

        public Dictionary<String, String> OmnitureVariables
        {
            get
            {
                if (omnitureVariables == null)
                {
                    if (!String.IsNullOrEmpty(OmnitureVariablesJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        omnitureVariables = ser.Deserialize<Dictionary<string, string>>(OmnitureVariablesJson);
                    }
                    omnitureVariables = omnitureVariables ?? new Dictionary<string, string>();
                }
                return omnitureVariables;
            }
        }

        public string AnalyticVariablesJson { get; set; }

        public Dictionary<String, String> AnalyticVariables
        {
            get
            {
                if (analyticVariables == null)
                {
                    if (!String.IsNullOrEmpty(AnalyticVariablesJson))
                    {
                        var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                        analyticVariables = ser.Deserialize<Dictionary<string, string>>(AnalyticVariablesJson);
                    }
                    analyticVariables = analyticVariables ?? new Dictionary<string, string>();
                }
                return analyticVariables;
            }
        }

        public string MemberLevelTrackingLastApplication { get; set; }
        public string MemberLevelTrackingIsMobileDevice { get; set; }
        public string MemberLevelTrackingIsTablet { get; set; }
        public string OrderID { get; set; }
        public UnifiedPaymentSystemHelper.PaymentUiPostDataType PaymentUiPostDataType { get; set; }
        public PromoType PromoType { get; set; }
        public PaymentType PaymentType { get; set; }
        public int PromoID { get; set; }

        /// <summary>
        /// The premium type to restrict a specific premium type for an upsale or a la carte
        /// </summary>
        public PremiumType PremiumType { get; set; }

        /// <summary>
        ///     Added to support passing in a unique template Id for Premium Services
        /// </summary>
        public int TemplateId { get; set; }

    }
}