﻿#region

using System;
using System.Collections.Generic;
using Spark.Common.AuthorizationService;

#endregion

namespace Spark.Rest.V2.Models.Subscription
{
    /// <summary>
    ///     Represents the member's current subscription plan status, including all premium services.
    /// </summary>
    public class SubscriptionPlanStatus
    {
        /// <summary>
        ///     Do you also want to self-suspend and hide your profile?
        /// </summary>
        public bool IsSelfSuspended { get; set; }

        /// <summary>
        ///     Is this subscription in the free trial period?
        /// </summary>
        public bool IsFreeTrial { get; set; }

        /// <summary>
        /// Package Type ID
        /// 0 - None
        /// 1 - Basic - Basic subscription only
        /// 2 - Bundled - Basic subscription with premium services
        /// 3 - AlaCarte - Premium service only
        /// 4 - Discount
        /// 5 - RemainingCredit - Remaining credit package for the site
        /// 6 - OneOff - For example, color code package
        /// 7 - ServiceFee - Any additional service fee package that may be included with the purchase
        /// 8 - FreeTrial
        /// 9 - BOGOEligible
        /// 10 - Gift
        /// </summary>
        public int PackageTypeId { get; set; }

        /// <summary>
        ///     The base subscription plan options.
        /// </summary>
        public SubscriptionBase BaseSubscriptionPlan { get; set; }

        /// <summary>
        /// Gets or sets the member's subscription end date as UTC.
        /// </summary>
        /// <value>
        /// The subscription end date.
        /// </value>
        public DateTime SubscriptionEndDate { get; set; }

        /// <summary>
        ///     Add/remove premium options.
        /// </summary>
        public List<SubscriptionPremiumOption> PremiumOptions { get; set; }

        /// <summary>
        ///     Free Trial is different than a basic subscription plan, expose a new property for its plan
        /// </summary>
        public SubscriptionBase FreeTrialSubscriptionPlan { get; set; }
    }
}