﻿using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Subscription
{
    public class UpdateLifeTimeAccessRequest : MemberRequest
    {
        public int AdminMemberId { get; set; }
    }
}
