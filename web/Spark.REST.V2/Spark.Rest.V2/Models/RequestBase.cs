#region

using System.Runtime.Serialization;

#endregion

//using Siesta;

namespace Spark.REST.Models
{
    public abstract class RequestBase
    {
        //public bool Indent { get; set; }

        //public ModelFormatting Formatting
        //{
        //    get { return Indent ? ModelFormatting.HumanReadable : ModelFormatting.Normal; }
        //}
        [IgnoreDataMember]
        public object JSONObject { get; set; }
    }
}