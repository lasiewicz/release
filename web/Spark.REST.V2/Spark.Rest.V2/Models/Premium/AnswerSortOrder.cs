﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.PromoEngine.ValueObjects;

namespace Spark.Rest.V2.Models.Premium
{
    public enum AnswerSortOrder
    {
        Newest,
        Oldest
    }

    public class AnswerSortOrderComparers
    {

        public static Comparison<Answer> Get(AnswerSortOrder sortOrder)
        {
            switch (sortOrder)
            {
                case AnswerSortOrder.Newest:
                    return new Comparison<Answer>(Newest);
                case AnswerSortOrder.Oldest:
                    return new Comparison<Answer>(Oldest);
            }
            return null;
        }

        public static Func<Answer,Answer,int> Newest{ get { return (answer1, answer2) => -answer1.UpdateDate.CompareTo(answer2.UpdateDate); } }
        public static Func<Answer, Answer, int> Oldest { get { return (answer1, answer2) => answer1.UpdateDate.CompareTo(answer2.UpdateDate); } }

    }

}