﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Premium
{
    /// <summary>
    /// Request for Addming Answers for the member.
    /// </summary>
    public class UpdateAnswerRequest: BrandRequest
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "questionId")]
        public int QuestionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "answerValue")]
        public string AnswerValue { get; set; }
    }
}