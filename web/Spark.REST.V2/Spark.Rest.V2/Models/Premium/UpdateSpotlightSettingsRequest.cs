﻿using Spark.REST.Models;
using Spark.SAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.Premium
{
    public class UpdateSpotlightSettingsRequest : MemberRequest
    {
        public bool IsSpotlightEnabled { get; set; }
        public Gender Gender { get; set; }
        public Gender SeekingGender { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int Distance { get; set; }
        public int RegionId { get; set; }
    }
}