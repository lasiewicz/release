﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Premium
{
    public class AdminUpdateAnswerRequest : BrandRequest
    {
        [IgnoreDataMember]
        private Answer _answer = new Answer();

        public Answer ToAnswer()
        {
            return _answer;
        }

        [DataMember(Name = "answerid")]
        public int AnswerID
        {
            get { return _answer.AnswerID; }
            set { _answer.AnswerID = value; }
        }

        [DataMember(Name = "questionid")]
        public int QuestionID
        {
            get { return _answer.QuestionID; }
            set { _answer.QuestionID = value; }
        }

        [DataMember(Name = "memberid")]
        public int MemberID
        {
            get { return _answer.MemberID; }
            set { _answer.MemberID = value; }
        }

        [DataMember(Name = "answervalue")]
        public string AnswerValue
        {
            get { return _answer.AnswerValue; }
            set { _answer.AnswerValue = value; }
        }

        [DataMember(Name = "answervaluepending")]
        public string AnswerValuePending
        {
            get { return _answer.AnswerValuePending; }
            set { _answer.AnswerValuePending = value; }
        }

        [DataMember(Name = "adminmemberid")]
        public int AdminMemberID
        {
            get { return _answer.AdminMemberID; }
            set { _answer.AdminMemberID = value; }
        }

        [DataMember(Name = "answertype")]
        public QuestionAnswerEnums.AnswerType AnswerType
        {
            get { return _answer.AnswerType; }
            set { _answer.AnswerType = value; }
        }

        [DataMember(Name = "answerstatus")]
        public QuestionAnswerEnums.AnswerStatusType AnswerStatus
        {
            get { return _answer.AnswerStatus; }
            set { _answer.AnswerStatus = value; }
        }

        [DataMember(Name = "isdirty")]
        public bool IsDirty
        {
            get { return _answer.IsDirty; }
            set { _answer.IsDirty = value; }
        }

        [DataMember(Name = "gendermask")]
        public int GenderMask
        {
            get { return _answer.GenderMask; }
            set { _answer.GenderMask = value; }
        }

        [DataMember(Name = "globalstatusmask")]
        public int GlobalStatusMask
        {
            get { return _answer.GlobalStatusMask; }
            set { _answer.GlobalStatusMask = value; }
        }

        [DataMember(Name = "selfsuspendedflag")]
        public int SelfSuspendedFlag
        {
            get { return _answer.SelfSuspendedFlag; }
            set { _answer.SelfSuspendedFlag = value; }
        }

        [DataMember(Name = "hasapprovedphotos")]
        public bool HasApprovedPhotos
        {
            get { return _answer.HasApprovedPhotos; }
            set { _answer.HasApprovedPhotos = value; }
        }
    }
}