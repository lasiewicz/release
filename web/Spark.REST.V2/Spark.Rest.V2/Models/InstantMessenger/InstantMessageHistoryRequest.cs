﻿using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.InstantMessenger
{
    public class InstantMessageHistoryRequest : BrandRequest
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TargetMemberID { get; set; }
        public bool IgnoreCache { get; set; }
    }
}