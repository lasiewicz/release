﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.InstantMessenger
{
    /// <summary>
    /// 
    /// </summary>
    public class InstantMessengerInviteAddRequest : MemberRequest
    {
        /// <summary>
        /// Member receiving the IM request
        /// </summary>
        public int RecipientMemberId { get; set; }
    }
}