#region

using System.Collections.Generic;
using Spark.REST.Models;

#endregion

namespace Spark.Rest.V2.Models.InstantMessenger
{
    /// <summary>
    /// </summary>
    public class InstantMessengerSendMissedIMRequest : MemberRequest
    {
        /// <summary>
        /// Member receiving the missed IM email.
        /// </summary>
        public int RecipientMemberId { get; set; }

        /// <summary>
        /// Undelivered messages.
        /// </summary>
        public List<string> Messages { get; set; }
    }
}