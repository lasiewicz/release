﻿using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.InstantMessenger
{
    public class InstantMessageMemberRemoveRequest : MemberRequest
    {
        private bool _Undo = false;

        public int MessageId { get; set; }
        public bool Undo 
        {
            get { return _Undo; }
            set { _Undo = value; }
        }

    }
}