﻿#region

using Matchnet;
using Spark.REST.Models;

#endregion

namespace Spark.Rest.V2.Models.InstantMessenger
{
    /// <summary>
    /// 
    /// </summary>
    public class InstantMessengerValidateConversationTokenRequest : BrandRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public InstantMessengerValidateConversationTokenRequest()
        {
            ConversationTokenId = Constants.NULL_STRING;
        }

        /// <summary>
        /// 
        /// </summary>
        public string ConversationTokenId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int SenderMemberId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int RecipientMemberId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CommunityId { get; set; }
    }
}