﻿using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models.InstantMessenger
{
    public class InstantMessengerRecordInviteSentRequest : BrandRequest
    {
        public int roomid { get; set; }
        public int to_memberid { get; set; }
        public string access_token { get; set; }
    }
}