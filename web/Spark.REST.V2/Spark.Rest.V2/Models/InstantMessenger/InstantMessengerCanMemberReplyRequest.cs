﻿#region

using Spark.REST.Models;

#endregion

namespace Spark.Rest.V2.Models.InstantMessenger
{
    /// <summary>
    ///     Determines if the recipient is allowed to reply to sender's IM request
    ///     The recipient's member Id is part of the MemberRequest
    /// </summary>
    public class InstantMessengerCanMemberReplyRequest : MemberRequest
    {
        /// <summary>
        /// </summary>
        public int SenderMemberId { get; set; }
    }
}