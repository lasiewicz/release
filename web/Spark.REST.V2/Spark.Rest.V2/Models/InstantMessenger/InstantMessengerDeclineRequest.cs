﻿using Spark.REST.Models;

namespace Spark.Rest.V2.Models.InstantMessenger
{
    /// <summary>
    /// 
    /// </summary>
    public class InstantMessengerRemoveRequest : MemberRequest
    {
        ///<summary>
        ///</summary>
        public string ConversationKey { get; set; }
    }
}