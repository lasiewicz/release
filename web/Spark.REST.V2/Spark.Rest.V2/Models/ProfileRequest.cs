namespace Spark.REST.Models
{
	public class MemberAndTargetMemberRequest : MemberRequest
	{
		public int TargetMemberId { get; set; }
	}
}
