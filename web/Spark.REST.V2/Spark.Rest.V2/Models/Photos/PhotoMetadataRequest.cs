﻿#region

using System.Runtime.Serialization;
using Matchnet;

#endregion

namespace Spark.REST.Models.Photos
{
    /// <summary>
    /// </summary>
    public class PhotoMetadataRequest : MemberRequest
    {
        /// <summary>
        /// </summary>
        public PhotoMetadataRequest()
        {
            IsMain = false;
            ListOrder = Constants.NULL_INT;
        }

        /// <summary>
        ///     ID of a member's photo
        /// </summary>
        [DataMember(Name = "memberPhotoId")]
        public int MemberPhotoId { get; set; }

        /// <summary>
        ///     For setting photo caption
        /// </summary>
        [DataMember(Name = "caption")]
        public string Caption { get; set; }

        /// <summary>
        ///     For indicating main, default, or primary
        /// </summary>
        [DataMember(Name = "isMain")]
        public bool IsMain { get; set; }

        /// <summary>
        ///     For setting photo list order
        /// </summary>
        [DataMember(Name = "listOrder")]
        public int ListOrder { get; set; }

        /// <summary>
        ///     URL of photo where we should upload from
        /// </summary>
        [DataMember(Name = "photoUploadUrl")]
        public string PhotoUploadUrl { get; set; }
    }
}