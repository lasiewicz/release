﻿using Matchnet;
using Spark.REST.Models;
using Spark.REST.Models.Photos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Models.Photos
{
    public class PhotoUploadUrlRequest : MemberRequest
    {
        public PhotoUploadUrlRequest()
        {
            Photos = new List<PhotoMetadataRequest>();
        }

        public List<PhotoMetadataRequest> Photos { get; set; }
    }
}