using System.Runtime.Serialization;

namespace Spark.REST.Models
{
	///<summary>
	///</summary>
	public class DeletePhotoRequest : MemberRequest
	{
		///<summary>
		///</summary>
		[DataMember(Name = "memberPhotoId")]
		public int MemberPhotoId { get; set; }
	}
}
