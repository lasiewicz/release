﻿using System.Runtime.Serialization;
using Spark.REST.Models;

namespace Spark.REST.Models.Photos
{
	public class PhotoCaptionRequest : MemberRequest
	{
		/// <summary>
		/// ID of a member's photo
		/// </summary>
		[DataMember(Name = "memberPhotoId")]
		public int MemberPhotoId { get; set; }

		/// <summary>
		/// caption for that photo
		/// </summary>
		[DataMember(Name = "caption")]
		public string Caption { get; set; }
	}
}
