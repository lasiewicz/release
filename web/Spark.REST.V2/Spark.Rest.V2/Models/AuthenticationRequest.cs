using System;

namespace Spark.REST.Models
{
	public class AuthenticationRequest : BrandRequest
	{
		public string Email { get; set; }
		public string Password { get; set; }
	}
}