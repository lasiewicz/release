﻿#region

using System.Runtime.Serialization;
using Spark.REST.Models;

#endregion

namespace Spark.Rest.V2.Controllers
{
    /// <summary>
    /// </summary>
    public class GetMembersOnlineRequest : MemberRequest
    {
        private int _pageNumber = 1;
        private int _pageSize = 12;
        private int _sortType = 1;

        /// <summary>
        /// </summary>
        public GetMembersOnlineRequest()
        {
            MaxAge = 0;
            MinAge = 0;
            RegionId = 0;
            LanguageId = 0;
            SeekingGender = string.Empty;
        }

        /// <summary>
        ///   Optional. Defaults to 12.
        /// </summary>
        [DataMember(Name = "pageSize")]
        public int PageSize
        {
            get { return _pageSize; }
            set { _pageSize = value; }
        }

        /// <summary>
        ///   Optional. Defaults to HasPhotos.
        /// </summary>
        [DataMember(Name = "sortType")]
        public int SortType
        {
            get { return _sortType; }
            set { _sortType = value; }
        }

        /// <summary>
        ///   Optional. Defaults to 1.
        /// </summary>
        [DataMember(Name = "pageNumber")]
        public int PageNumber
        {
            get { return _pageNumber; }
            set { _pageNumber = value; }
        }

        /// Optional. Defaults to brand default.
        [DataMember(Name = "minAge")]
        public int MinAge { get; set; }

        /// Optional. Defaults to brand default.
        [DataMember(Name = "MaxAge")]
        public int MaxAge { get; set; }

        /// Optional. Defaults to brand default.
        [DataMember(Name = "RegionId")]
        public int RegionId { get; set; }

        /// Optional. Defaults to brand default.
        [DataMember(Name = "LanguageId")]
        public int LanguageId { get; set; }

        /// Optional. Defaults to everyone
        [DataMember(Name = "SeekingGender")]
        public string SeekingGender { get; set; }
    }
}