﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Models
{
    [Obsolete("Use Matchnet.BaseClassess.PagedResult instead.",false)]
    public class PagedResult<TData>
    {
        public PagedResult()
        {
            
        }

        public PagedResult(List<TData> items, int total)
        {
            Items = items;
            Total = total;
        } 
        public List<TData> Items { get; set; }
        public int Total { get; set; }
    }
}