﻿using System.Runtime.Serialization;

namespace Spark.REST.Models.Member
{
    ///<summary>
    /// Request for new member registration
    ///</summary>
    public class RegisterRequest : BrandRequest
    {
        [DataMember(Name = "userName")]
        public string UserName { get; set; }
        [DataMember(Name = "emailAddress")]
        public string EmailAddress { get; set; }
        [DataMember(Name = "password")]
        public string Password { get; set; }
        [DataMember(Name = "ipAddress")]
        public string IpAddress { get; set; }
    }
}