﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;

namespace Spark.REST.Models.Member
{
    /// <summary>
    /// Register Request object for API endpoint registrations
    /// </summary>
    [DataContract(Name = "ExtendedRegisterRequest")]
    public class ExtendedRegisterRequest : RegisterRequest
    {
        private Dictionary<string, List<int>> _attributeDataMultiValue;
        private Dictionary<string, object> _attributeData;

        /// <summary>
        /// Gets or sets the registration session identifier.
        /// </summary>
        /// <value>
        /// The registration session identifier.
        /// </value>
        [DataMember(Name = "RegistrationSessionID")]
        public string RegistrationSessionID { get; set; }

        /// <summary>
        /// Gets or sets the recapture identifier.
        /// </summary>
        /// <value>
        /// The recapture identifier.
        /// </value>
        [DataMember(Name = "RecaptureID")]
        public string RecaptureID { get; set; }

        /// <summary>
        /// Gets or sets the session tracking identifier.
        /// </summary>
        /// <value>
        /// The session tracking identifier.
        /// </value>
        [DataMember(Name = "SessionTrackingID")]
        public int SessionTrackingID { get; set; }

        //JS-1573 Taking out the "Iovation"
        ///// <summary>
        ///// Gets or sets the iovation black box.
        ///// </summary>
        ///// <value>
        ///// The iovation black box.
        ///// </value>
        //[DataMember(Name = "IovationBlackBox")]
        //public string IovationBlackBox { get; set; }

        /// <summary>
        /// Gets or sets the attribute data json.
        /// </summary>
        /// <value>
        /// The attribute data json.
        /// </value>
        [DataMember(Name = "AttributeDataJson")]
        public string AttributeDataJson { get; set; } // JSON dictionary
        
        //[DataMember(Name = "AttributeData")]
        /// <summary>
        /// Gets the attribute data.
        /// </summary>
        /// <value>
        /// The attribute data.
        /// </value>
        public Dictionary<string, object> AttributeData
        {
            get
            {
                if (_attributeData != null) return _attributeData;
                if (!String.IsNullOrEmpty(AttributeDataJson))
                {
                    var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                    _attributeData = ser.Deserialize<Dictionary<string, object>>(AttributeDataJson);
                }
                else
                {
                    var jObject = JSONObject as JObject;
                    if (jObject != null)
                    {
                        var list = jObject.SelectToken("attributeDataJson").ToList();
                        foreach (var jp in list.Cast<JProperty>())
                        {
                            if (null == _attributeData) _attributeData = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
                            switch (jp.Value.Type)
                            {
                                case JTokenType.Integer:
                                    _attributeData.Add(jp.Name, jp.Value.ToObject<int>());
                                    break;
                                case JTokenType.Date:
                                    _attributeData.Add(jp.Name, jp.Value.ToObject<DateTime>());
                                    break;
                                default:
                                    _attributeData.Add(jp.Name, jp.Value.ToString());
                                    break;
                            }
                        }
                    }
                }
                _attributeData = _attributeData ?? new Dictionary<string, object>();
                return _attributeData;
            }
        }

        /// <summary>
        /// Gets or sets the attribute data multi value json.
        /// </summary>
        /// <value>
        /// The attribute data multi value json.
        /// </value>
        [DataMember(Name = "attributesMultiValueJson")]
        public string AttributeDataMultiValueJson { get; set; } // JSON dictionary
        
        /// <summary>
        /// Gets the attribute data multi value.
        /// </summary>
        /// <value>
        /// The attribute data multi value.
        /// </value>
        public Dictionary<string, List<int>> AttributeDataMultiValue
        {
            get
            {
                if (_attributeDataMultiValue != null) return _attributeDataMultiValue;
                if (!String.IsNullOrEmpty(AttributeDataMultiValueJson))
                {
                    var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                    _attributeDataMultiValue = ser.Deserialize<Dictionary<string, List<int>>>(AttributeDataMultiValueJson);
                }
                else
                {
                    var jObject = JSONObject as JObject;
                    if (jObject != null)
                    {
                        var list = jObject.SelectToken("attributesMultiValueJson").ToList();
                        foreach (var jp in list.Cast<JProperty>())
                        {
                            if (null == _attributeDataMultiValue) _attributeDataMultiValue = new Dictionary<string, List<int>>(StringComparer.OrdinalIgnoreCase);
                            _attributeDataMultiValue.Add(jp.Name, jp.Value.Select(m => m.Value<int>()).ToList());
                        }
                    }
                }
                _attributeDataMultiValue = _attributeDataMultiValue ?? new Dictionary<string, List<int>>();
                return _attributeDataMultiValue;
            }
        }
    }
}