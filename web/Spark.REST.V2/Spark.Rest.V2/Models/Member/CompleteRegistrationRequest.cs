using System.Runtime.Serialization;

namespace Spark.REST.Models
{
	///<summary>
	///</summary>
    public class CompleteRegistrationRequest : MemberRequest
	{
		///<summary>
		///</summary>
		[DataMember(Name = "IovationBlackBox")]
        public string IovationBlackBox { get; set; }
	}
}