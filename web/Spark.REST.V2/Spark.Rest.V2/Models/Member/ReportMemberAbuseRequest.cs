using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Spark.REST.Models
{
    [DataContract(Name = "ReportMemberAbuseRequest")]
    public class ReportMemberAbuseRequest : MemberRequest
	{
        [DataMember(Name = "AttributeOptionId")]
        public int AttributeOptionId { get; set; }

        [DataMember(Name = "Comment")]
        [StringLength(700)]  
        public string Comment { get; set; }

        [DataMember(Name = "AddToIgnoreList")]
        public bool AddToIgnoreList { get; set; }

        [DataMember(Name = "MemberMailId")]
        public int MemberMailId { get; set; }



	}
}