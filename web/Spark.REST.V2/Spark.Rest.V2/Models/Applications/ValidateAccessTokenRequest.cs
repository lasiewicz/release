using System.Runtime.Serialization;

namespace Spark.REST.Models.Applications
{
    [DataContract(Name = "ValidateAccessTokenRequest")]
    public class ValidateAccessTokenRequest : BrandRequest
    {
        [DataMember(Name = "accessToken")]
        public string AccessToken { get; set; }
    }
}
