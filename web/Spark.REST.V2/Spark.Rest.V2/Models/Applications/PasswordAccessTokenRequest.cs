using System.Runtime.Serialization;

namespace Spark.REST.Models.Applications
{
    [DataContract(Name = "PasswordAccessTokenRequest")]
    public class PasswordAccessTokenRequest : MemberRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public PasswordAccessTokenRequest()
        {
            Scope = "short";
        }
        /// <summary>
        /// application ID identifies the app making the request
        /// </summary>
        [DataMember(Name = "applicationId")]
        public int ApplicationId { get; set; }

        /// <summary>
        /// member's email address
        /// </summary>
        [DataMember(Name = "email")]
        public string Email { get; set; }

        /// <summary>
        /// The password can either be unencrypted (default) or encrypted at this time.  PasswordIsEncrypted should be set to true if the password is encrypted
        /// </summary>
        [DataMember(Name = "password")]
        public string Password { get; set; }

        /// <summary>
        /// Using encrypted is preferred where possible
        /// </summary>
        [DataMember(Name = "PasswordIsEncrypted")]
        public bool? PasswordIsEncrypted { get; set; }

        /// <summary>
        /// Currently only support specifying the life time of access token. possible values are "short" and "long"
        /// </summary>
        [DataMember(Name = "Scope")]
        public string Scope { get; set; }

        /// <summary>
        /// The original IP of the client making the request
        /// </summary>
        [DataMember(Name = "IpAddress")]
        public string IpAddress { get; set; }

        /// <summary>
        /// The original http headers of the client making the request
        /// </summary>
        [DataMember(Name = "HttpHeaders")]
        public string HttpHeaders { get; set; }

        /// <summary>
        /// The original user agent of the client making the request
        /// </summary>
        [DataMember(Name = "UserAgent")]
        public string UserAgent { get; set; }

        /// <summary>
        /// The session if of the login session
        /// </summary>
        [DataMember(Name = "LoginSessionId")]
        public string LoginSessionId { get; set; }
    }
}
