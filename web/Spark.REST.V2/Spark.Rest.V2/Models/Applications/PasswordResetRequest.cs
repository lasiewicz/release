﻿using System.Runtime.Serialization;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Applications
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "PasswordResetRequest")]
    public class PasswordResetRequest : BrandRequest
    {
        /// <summary>
        /// application ID identifies the app making the request
        /// </summary>
        [DataMember(Name = "applicationId")]
        public int ApplicationId { get; set; }

        /// <summary>
        /// member's email address
        /// </summary>
        [DataMember(Name = "email")]
        public string Email { get; set; }

        /// <summary>
        /// SUA Url
        /// </summary>
        [DataMember(Name = "SuaUrl")]
        public string SuaUrl { get; set; }

        /// <summary>
        /// SUA template Id
        /// </summary>
        [DataMember(Name = "TemplateId")]
        public string TemplateId { get; set; }

        /// <summary>
        /// The original IP of the client making the request
        /// </summary>
        [DataMember(Name = "IpAddress")]
        public string IpAddress { get; set; }

        /// <summary>
        /// The original http headers of the client making the request
        /// </summary>
        [DataMember(Name = "HttpHeaders")]
        public string HttpHeaders { get; set; }

        /// <summary>
        /// The original user agent of the client making the request
        /// </summary>
        [DataMember(Name = "UserAgent")]
        public string UserAgent { get; set; }

        /// <summary>
        /// The session if of the login session
        /// </summary>
        [DataMember(Name = "LoginSessionId")]
        public string LoginSessionId { get; set; }
    }
}