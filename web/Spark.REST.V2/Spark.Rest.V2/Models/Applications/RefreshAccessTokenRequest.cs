using System.Runtime.Serialization;

namespace Spark.REST.Models.Applications
{
    [DataContract(Name = "RefreshAccessTokenRequest")]
    public class RefreshAccessTokenRequest : BrandRequest
    {
        [DataMember(Name = "applicationId")]
        public int ApplicationId { get; set; }
        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }
        [DataMember(Name = "refreshToken")]
        public string RefreshToken { get; set; }
    }
}
