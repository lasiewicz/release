﻿using System.Runtime.Serialization;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Applications
{
    [DataContract(Name = "EmailChangeRequest")]
    public class EmailChangeRequest : BrandRequest
    {
        /// <summary>
        ///  The new email
        /// </summary>
        [DataMember(Name = "NewEmail")]
        public string NewEmail { get; set; }

        /// <summary>
        ///  The original email
        /// </summary>
        [DataMember(Name = "CurrentEmail")]
        public string CurrentEmail { get; set; }

        /// <summary>
        /// The original IP of the client making the request
        /// </summary>
        [DataMember(Name = "IpAddress")]
        public string IpAddress { get; set; }
    }
}