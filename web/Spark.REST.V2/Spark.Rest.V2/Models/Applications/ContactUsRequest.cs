﻿using System.Runtime.Serialization;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Applications
{
    /// <summary>
    /// This is the request object for sending an email to customer service
    /// using the form on the ContactUs page
    /// </summary>
    public class ContactUsRequest : MemberRequest
    {

        /// <summary>
        /// Gets or sets the application and/or device information. (i.e. App version, device, OS version)
        /// </summary>
        /// <value>
        /// The application and/or device information
        /// </value>
        [DataMember(Name = "appDeviceInfo")]
        public string AppDeviceInfo { get; set; }

        /// <summary>
        /// Gets or sets the attribute option identifier.
        /// Also referred to as ReasonID.
        /// </summary>
        /// <value>
        /// The attribute option identifier.
        /// </value>
        [DataMember(Name = "attributeOptionId")]
        public int AttributeOptionId { get; set; }

        /// <summary>
        /// Gets or sets the comment. This is the text entered by the sender.
        /// </summary>
        /// <value>
        /// The comment.
        /// </value>
        [DataMember(Name = "comment")]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the browser info
        /// </summary>
        /// <value>
        /// The browser information.
        /// </value>
        [DataMember(Name = "browser")]
        public string Browser { get; set; }

        /// <summary>
        /// Gets or sets the IP address of the client.
        /// </summary>
        /// <value>
        /// The IP address.
        /// </value>
        [DataMember(Name = "remoteIp")]
        public string RemoteIp { get; set; }

        /// <summary>
        /// Gets or sets the sender email address.
        /// </summary>
        /// <value>
        /// The sender email address.
        /// </value>
        [DataMember(Name = "senderEmailAddress")]
        public string SenderEmailAddress { get; set; }
    }
}