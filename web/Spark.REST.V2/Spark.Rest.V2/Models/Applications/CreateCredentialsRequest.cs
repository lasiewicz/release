﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Applications
{
    public class CreateCredentialsRequest : MemberRequest
    {
        public CreateCredentialsRequest()
        {
            // for backwards compatibility as this property was added later on
            Scope = "short";
        }

        [DataMember(Name = "Password")]
        public string Password { get; set; }

        [DataMember(Name = "EmailAddress")]
        public string EmailAddress { get; set; }

        [DataMember(Name = "UserName")]
        public string UserName { get; set; }

        [DataMember(Name = "ApplicationId")]
        public int ApplicationId { get; set; }

        /// <summary>
        ///     Long, Short
        /// </summary>
        [DataMember(Name = "Scope")]
        public string Scope { get; set; }
    }
}