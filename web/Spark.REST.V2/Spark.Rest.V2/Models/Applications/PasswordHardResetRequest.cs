﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Applications
{
    [DataContract(Name = "PasswordHardResetRequest")]
    public class PasswordHardResetRequest : BrandRequest
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
    }
}