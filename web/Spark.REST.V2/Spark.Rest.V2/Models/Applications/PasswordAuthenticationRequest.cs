﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Models.Applications
{
    /// <summary>
    /// A request to authenticate a member's password without updating the access tokens or perform other post-login actions.
    /// </summary>
    public class PasswordAuthenticationRequest : MemberRequest
    {
        /// <summary>
        /// The member's email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// The password must be unencrypted.
        /// </summary>
        public string Password { get; set; }
    }
}