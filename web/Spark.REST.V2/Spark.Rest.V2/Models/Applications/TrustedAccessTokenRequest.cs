using System.Runtime.Serialization;

namespace Spark.REST.Models.Applications
{
    [DataContract(Name = "TrustedAccessTokenRequest")]
    public class TrustedAccessTokenRequest : BrandRequest
    {
        [DataMember(Name = "applicationId")]
        public int ApplicationId { get; set; }
        [DataMember(Name = "applicationSecret")]
        public string ApplicationSecret { get; set; }
        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }
    }
}
