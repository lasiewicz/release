﻿#region

using System.Runtime.Serialization;
using Spark.REST.Models;

#endregion

namespace Spark.Rest.V2.Models.Applications
{
    [DataContract(Name = "AuthenticateAccessTokenRequest")]
    public class AuthenticateAccessTokenRequest : BrandRequest
    {
    }
}