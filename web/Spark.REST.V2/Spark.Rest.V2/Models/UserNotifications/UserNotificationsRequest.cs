﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.REST.Models.UserNotifications
{
    public class UserNotificationsRequest : BrandRequest
    {
        [DataMember(Name = "startNum")]
        public int StartNum { get; set; }

        [DataMember(Name = "pageSize")]
        public int PageSize { get; set; }

        [DataMember(Name = "refreshDataPointCache")]
        public bool RefreshDataPointCache { get; set; }

    }
}