﻿#region

using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Spark.Logger;
using Spark.Rest.V2.Configuration;
using Spark.Rest.Authorization;
using Spark.Rest.Helpers;

#endregion

namespace Spark.Rest
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MvcApplication));

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            ModelBinders.Binders.DefaultBinder = new JsonNetModelBinder(ModelBinders.Binders.DefaultBinder);

            RegisterRoutes(RouteTable.Routes);
            //register the error routes after all other routes. must be registered last.
            RegisterErrorRoutes(RouteTable.Routes);
            RegisterGlobalFilters(GlobalFilters.Filters);

            //this line increases concurrent connections to an IP end point.
            //Read more here: http://blogs.msdn.com/b/tmarq/archive/2007/07/21/asp-net-thread-usage-on-iis-7-0-and-6-0.aspx
            System.Net.ServicePointManager.DefaultConnectionLimit = int.MaxValue;
        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ServiceUnavailableCheck());
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var lastError = Server.GetLastError();
            if (null != lastError)
            {
                var ex = lastError.GetBaseException();
                Log.LogError("Application Error!", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
            }
        }

        public static void RegisterRoutesV2(RouteCollection routes)
        {
            routes.MapRoute(
                "post-terminateandroidsubscriptionrenewal", "{0}/brandId/{brandId}/purchase/iab/receiptterminate",
                new {controller = "Purchase", action = "TerminateAndroidSubscriptionRenewal"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-voidandroidsubscription", "{0}/brandId/{brandId}/purchase/iab/receiptvoid",
                new {controller = "Purchase", action = "VoidAndroidSubscription"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-getandroidreceiptdetails", "{0}/brandId/{brandId}/purchase/iab/receiptdetails",
                new {controller = "Purchase", action = "GetAndroidReceiptDetails"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-renewandroidinapppurchase", "{0}/brandId/{brandId}/purchase/iab/renew",
                new {controller = "Purchase", action = "RenewAndroidInAppBilling"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-processandroidinapppurchase", "{0}/brandId/{brandId}/purchase/iab/process",
                new {controller = "Purchase", action = "ProcessAndroidInAppBilling"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-getiosreceiptdetails", "{0}/brandId/{brandId}/purchase/iap/receiptdetails",
                new {controller = "Purchase", action = "GetIOSReceiptDetails"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-renewiosinapppurchase", "{0}/brandId/{brandId}/purchase/iap/renew",
                new {controller = "Purchase", action = "RenewIOSInAppPurchase"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-processiosinapppurchase", "{0}/brandId/{brandId}/purchase/iap/process",
                new {controller = "Purchase", action = "ProcessIOSInAppPurchase"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "get-iosinapppurchaseproducts", "{0}/brandId/{brandId}/purchase/iap/products",
                new {controller = "Purchase", action = "GetIOSInAppProducts"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-usernotifications",
                "{0}/brandId/{brandId}/startNum/{startNum}/pageSize/{pageSize}/usernotifications",
                new {controller = "UserNotification", action = "GetUserNotifications"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-visitorsearchresultspost", "{0}/brandId/{brandId}/visitorsearch/results",
                new {controller = "Search", action = "GetVisitorSearchResultsViaPost"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "get-minglesearchresultspost", "{0}/brandId/{brandId}/minglesearch/results",
                new {controller = "MingleSearch", action = "GetMingleSearchResultsViaPost"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "update-minglesearchresultspost", "{0}/brandId/{brandId}/minglesearch/update",
                new {controller = "MingleSearch", action = "UpdateMingleSearchMember"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "get-secretadmirerresults", "{0}/brandId/{brandId}/search/secretadmirer",
                new {controller = "Search", action = "GetSecretAdmirerResults"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "get-mmemberbasiclogoninfo", "{0}/brandId/{brandId}/basiclogoninfo/{memberId}",
                new {controller = "Profile", action = "GetMemberBasicLogonInfo"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-miniprofileV2.1", "{0}/brandId/{brandId}/profile/miniProfile/{targetMemberId}",
                new {controller = "Profile", action = "GetMiniProfileV2", targetMemberId = UrlParameter.Optional},
                new {httpMethod = new HttpMethodConstraint("GET"), acceptVersion = new AcceptVersionConstraint("V2.1")});

            routes.MapRoute(
                "get-miniprofileV2", "{0}/brandId/{brandId}/profile/miniProfile/{targetMemberId}",
                new {controller = "Profile", action = "GetMiniProfile", targetMemberId = UrlParameter.Optional},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-photouploadpage", "{0}/brandId/{brandId}/profile/photos/upload/{memberId}",
                new {controller = "Photo", action = "getPhotoUploadPage"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "post-photoswebform", "{0}/brandId/{brandId}/profile/photos/uploadform",
                new {controller = "Photo", action = "PostPhotos"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-photosjson", "{0}/brandId/{brandId}/profile/photos/uploadjson",
                new {controller = "Photo", action = "PostPhotosJson"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-photosurl", "{0}/brandId/{brandId}/profile/photos/uploadurl",
                new { controller = "Photo", action = "PostPhotosUrl" },
                new { httpMethod = new HttpMethodConstraint("Post") });

            routes.MapRoute(
                "delete-photo", "{0}/brandId/{brandId}/profile/photos/delete/{memberPhotoId}",
                new {controller = "Photo", action = "DeletePhoto"},
                new {httpMethod = new HttpMethodConstraint("Delete")});

            routes.MapRoute(
                "update-photocaption", "{0}/brandId/{brandId}/profile/photos/{memberPhotoId}",
                new {controller = "Photo", action = "UpdatePhotoCaption"},
                new {httpMethod = new HttpMethodConstraint("Put")});

            routes.MapRoute(
                "get-photosV2", "{0}/brandId/{brandId}/profile/photos/{targetMemberId}",
                new {controller = "Photo", action = "GetPhotos"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-unreadmessagecount", "{0}/brandId/{brandId}/mail/unreadcount",
                new {controller = "Mail", action = "GetUnreadMessageCountForType"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-mailFolderV2.1",
                "{0}/brandId/{brandId}/mail/folder/{folderId}/pagesize/{pageSize}/page/{pageNumber}",
                new {controller = "Mail", action = "GetMailFolderV2"},
                new {httpMethod = new HttpMethodConstraint("GET"), acceptVersion = new AcceptVersionConstraint("V2.1")});

            routes.MapRoute(
                "get-mailFolderV2",
                "{0}/brandId/{brandId}/mail/folder/{folderId}/pagesize/{pageSize}/page/{pageNumber}",
                new {controller = "Mail", action = "GetMailFolder"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-mailConversationMessages",
                "{0}/brandId/{brandId}/mail/conversationInboxMessages/{ConversationType}/targetMemberID/{TargetMemberID}/page/{PageNumber}/pagesize/{PageSize}",
                new { controller = "Mail", action = "GetMailConversationMessages"},
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-mailConversationMembersPage",
                "{0}/brandId/{brandId}/mail/conversationInboxMembers/{ConversationType}/page/{PageNumber}/pagesize/{PageSize}",
                new { controller = "Mail", action = "GetMailConversationMembers" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-mailConversationMembers",
                "{0}/brandId/{brandId}/mail/conversationInboxMembers/{ConversationType}",
                new { controller = "Mail", action = "GetMailConversationMembers" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-removeMailConversationMember",
                "{0}/brandId/{brandId}/mail/removeConversationInboxMember/messageId/{MessageId}/undo/{Undo}",
                new { controller = "Mail", action = "RemoveMailConversationMember" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-deleteMailConversationMember",
                "{0}/brandId/{brandId}/mail/deleteConversationInboxMember/targetMemberId/{targetMemberId}/messageId/{MessageId}",
                new { controller = "Mail", action = "DeleteMailConversationMember" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-GetMessageWithMarkAsReadV2",
                "{0}/brandId/{brandId}/mail/message/{messageListId}/markAsRead/{markAsRead}",
                new { controller = "Mail", action = "GetMailMessageV2" },
                new {httpMethod = new HttpMethodConstraint("GET"), acceptVersion = new AcceptVersionConstraint("V2.1")});

            routes.MapRoute(
                "get-mailMessageV2.1", 
                "{0}/brandId/{brandId}/mail/message/{messageListId}",
                new {controller = "Mail", action = "GetMailMessageV2"},
                new {httpMethod = new HttpMethodConstraint("GET"), acceptVersion = new AcceptVersionConstraint("V2.1")});

            routes.MapRoute(
                "get-mailMessageV2", "{0}/brandId/{brandId}/mail/message/{messageListId}",
                new {controller = "Mail", action = "GetMailMessage"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "delete-mailMessageV2",
                "{0}/brandId/{brandId}/mail/message/folder/{currentFolderId}/message/{messageId}",
                new {controller = "Mail", action = "DeleteMailMessage"},
                new {httpMethod = new HttpMethodConstraint("Delete")});

            routes.MapRoute(
                "delete-mailMessages",
                "{0}/brandId/{brandId}/mail/messages",
                new {controller = "Mail", action = "DeleteMailMessages"},
                new {httpMethod = new HttpMethodConstraint("Delete")});

            routes.MapRoute(
                "post-mailMessageV2", "{0}/brandId/{brandId}/mail/message",
                new {controller = "Mail", action = "SendMailMessage"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-markReadMailMessages", "{0}/brandId/{brandId}/mail/message/read",
                new { controller = "Mail", action = "MarkReadMailMessages" },
                new { httpMethod = new HttpMethodConstraint("Post") });

            // send a flirt/smile to another user
            routes.MapRoute(
                "post-teaseV2", "{0}/brandId/{brandId}/mail/tease",
                new {controller = "Mail", action = "SendTease"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-attachmentwebform", "{0}/brandId/{brandId}/mail/uploadattachmentsform",
                new {controller = "Mail", action = "PostMessageAttachments"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-attachmentjson", "{0}/brandId/{brandId}/mail/uploadattachmentjson",
                new {controller = "Mail", action = "PostMessageAttachmentJson"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-dne", "{0}/brandId/{brandId}/mail/dne",
                new { controller = "Mail", action = "UpdateDNE" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "get-brandV2", "{0}/brandId/{brandId}/content/brand",
                new {controller = "Content", action = "GetBrand"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-citiesbyzipcodeV2", "{0}/brandId/{brandId}/content/region/citiesbyzipcode/{zipCode}",
                new {controller = "Content", action = "GetCitiesByZipCodes"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-getcountries", "{0}/brandId/{brandId}/content/region/countries/{languageId}",
                new {controller = "Content", action = "GetCountries"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-getstates", "{0}/brandId/{brandId}/content/region/states/{languageId}/{countryRegionId}",
                new {controller = "Content", action = "GetStates"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-getcities",
                "{0}/brandId/{brandId}/content/region/cities/{languageId}/{countryRegionId}/{stateRegionId}",
                new {controller = "Content", action = "GetCities"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-getchildregions", "{0}/brandId/{brandId}/content/region/childregions/{languageId}/{parentRegionId}",
                new {controller = "Content", action = "GetChildRegions"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-getregionhierarchy", "{0}/brandId/{brandId}/content/region/hierarchy/{languageId}/{regionId}",
                new {controller = "Content", action = "GetRegionHierarchy"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-attributeoptionsV2", "{0}/brandId/{brandId}/content/attributes/options/{attributeName}",
                new {controller = "Content", action = "GetAttributeOptions"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "post-attributeoptionscollectionV2", "{0}/brandId/{brandId}/content/attributes/optionscollection",
                new {controller = "Content", action = "GetAttributeOptionsCollection"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "get-attributemetadataV2", "{0}/brandId/{brandId}/content/attributes/metadata/{attributeName}",
                new {controller = "Content", action = "GetAttributeMetadata"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-teasesV2", "{0}/brandId/{brandId}/content/teases/category/{teaseCategoryID}",
                new {controller = "Content", action = "GetTeases"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-teasecategoriesV2", "{0}/brandId/{brandId}/content/teases/categories",
                new {controller = "Content", action = "GetTeaseCategories"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-teasesallV2", "{0}/brandId/{brandId}/content/teases/all",
                new {controller = "Content", action = "GetAllTeases"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-hotlistV2.1",
                "{0}/brandId/{brandId}/hotlist/{hotListCategory}/pagesize/{pageSize}/pagenumber/{pageNumber}",
                new {controller = "Hotlist", action = "GetHotListEntriesV2"},
                new {httpMethod = new HttpMethodConstraint("Get"), acceptVersion = new AcceptVersionConstraint("V2.1")});
				
			routes.MapRoute(
                "get-hotlistMultiple",
                "{0}/brandId/{brandId}/hotlistmultiple/pagesize/{pageSize}/pagenumber/{pageNumber}",
                new { controller = "Hotlist", action = "GetHotListEntriesMultiple" },
                new { httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-hotlistV2",
                "{0}/brandId/{brandId}/hotlist/{hotListCategory}/pagesize/{pageSize}/pagenumber/{pageNumber}",
                new {controller = "Hotlist", action = "GetHotListEntries"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-memberonhotlist",
                "{0}/brandId/{brandId}/memberonhotlist/{hotListCategory}/{targetMemberId}",
                new {controller = "Hotlist", action = "OnMemberHotList"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "delete-favoriteV2", "{0}/brandId/{brandId}/hotlist/favoritemember/{favoriteMemberId}",
                new {controller = "Hotlist", action = "DeleteFavorite"},
                new {httpMethod = new HttpMethodConstraint("DELETE")});

            routes.MapRoute(
                "post-hotlist", "{0}/brandId/{brandId}/hotlist/{hotListCategory}/targetmemberid/{targetMemberId}",
                new {controller = "Hotlist", action = "AddToHotlist"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "delete-hotlist",
                "{0}/brandId/{brandId}/hotlist/{hotListCategory}/targetmemberid/{targetMemberId}",
                new {controller = "Hotlist", action = "RemoveFromHotlist"},
                new {httpMethod = new HttpMethodConstraint("DELETE")});

            routes.MapRoute(
                "get-hotlistcountsV2", "{0}/brandId/{brandId}/hotlist/counts",
                new {controller = "Hotlist", action = "GetHotListCounts"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-contacthistory",
                "{0}/brandId/{brandId}/hotlist/contacthistory/targetmemberid/{TargetMemberID}/includeMiniProfile/{IncludeMiniProfile}",
                new { controller = "Hotlist", action = "GetContactHistory" },
                new { httpMethod = new HttpMethodConstraint("Get") });

            routes.MapRoute(
                "get-searchresultspostV2.1", "{0}/brandId/{brandId}/search/results",
                new {controller = "Search", action = "GetSearchResultsViaPostV2"},
                new {httpMethod = new HttpMethodConstraint("Post"), acceptVersion = new AcceptVersionConstraint("V2.1")});

            routes.MapRoute(
                "get-searchresultspostV2", "{0}/brandId/{brandId}/search/results",
                new {controller = "Search", action = "GetSearchResultsViaPost"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "get-matchpreferencesV2", "{0}/brandId/{brandId}/match/preferences",
                new {controller = "Search", action = "GetMatchPreferences"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "put-matchpreferencesV2", "{0}/brandId/{brandId}/match/preferences",
                new {controller = "Search", action = "PutMatchPreferences"},
                new {httpMethod = new HttpMethodConstraint("Put")});

            routes.MapRoute(
                "get-matchresultsV2.1",
                "{0}/brandId/{brandId}/match/results/pagesize/{pageSize}/page/{pageNumber}",
                new {controller = "Search", action = "GetMatchResultsV2"},
                new {httpMethod = new HttpMethodConstraint("Get"), acceptVersion = new AcceptVersionConstraint("V2.1")});

            routes.MapRoute(
                "get-matchresultsV2", "{0}/brandId/{brandId}/match/results/pagesize/{pageSize}/page/{pageNumber}",
                new {controller = "Search", action = "GetMatchResults"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-membersonlineV2", "{0}/brandId/{brandId}/membersonline/count",
                new {controller = "Membersonline", action = "GetMembersOnlineCount"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-membersonlinelist", "{0}/brandId/{brandId}/membersonline/list",
                new {controller = "Membersonline", action = "GetMembersOnlineList", list = UrlParameter.Optional},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "post-membersonlineV2", "{0}/brandId/{brandId}/addmembersonline",
                new {controller = "MembersOnline", action = "Add"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "put-membersonlineV2", "{0}/brandId/{brandId}/removemembersonline",
                new {controller = "MembersOnline", action = "Remove"},
                new {httpMethod = new HttpMethodConstraint("Delete")});

            routes.MapRoute(
                "put-attributesV2.1", "{0}/brandId/{brandId}/profile/attributes",
                new {controller = "Profile", action = "UpdateAttributeDataV2"},
                new {httpMethod = new HttpMethodConstraint("Put"), acceptVersion = new AcceptVersionConstraint("V2.1")});

            routes.MapRoute(
                "put-attributesV2", "{0}/brandId/{brandId}/profile/attributes",
                new {controller = "Profile", action = "UpdateAttributeData"},
                new {httpMethod = new HttpMethodConstraint("Put")});

            routes.MapRoute(
                "UpdateProfileEnabledStatus",
                "{0}/brandId/{brandId}/profile/ProfileEnabledStatus",
                new {controller = "Profile", action = "UpdateProfileEnabledStatus"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "get-attributesetviewerV2",
                "{0}/brandId/{brandId}/profile/attributeset/{attributeSetName}/{targetMemberId}",
                new {controller = "Profile", action = "GetAttributeSet", targetMemberId = UrlParameter.Optional},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-attributeSetBatch",
                "{0}/brandId/{brandId}/profile/attributesetbatch/{attributeSetName}",
                new {controller = "Profile", action = "GetAttributeSetBatch"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-attributeSetBatchClient",
                "{0}/brandId/{brandId}/profile/attributesetbatchclient/{attributeSetName}",
                new {controller = "Profile", action = "GetAttributeSetBatchClient"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-LookupAttributeSetByMemberId",
                "{0}/brandId/{brandId}/profile/lookupByMemberId/{attributeSetName}/{targetMemberId}",
                new {controller = "Profile", action = "LookupAttributeSetByMemberId"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-LookupAttributeSetByMemberUsername",
                "{0}/brandId/{brandId}/profile/lookupByMemberUsername/{attributeSetName}/{targetMemberUsername}",
                new {controller = "Profile", action = "LookupAttributeSetByMemberUsername"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-attributesetvisitorV2",
                "{0}/brandId/{brandId}/visitorprofile/{targetMemberId}",
                new {controller = "Profile", action = "GetAttributeSetForVisitor"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-memberStatus", "{0}/brandId/{brandId}/application/{applicationId}/member/{memberId}/status",
                new {controller = "Profile", action = "GetMemberStatus"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-runtimesettingvalue", "{0}/brandId/{brandId}/content/runtimesettingvalue/{settingName}",
                new {controller = "Content", action = "GetRuntimeSettingValue"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-runtimesettingV2", "{0}/brandId/{brandId}/content/runtimesetting/{settingName}",
                new {controller = "Content", action = "GetRuntimeSetting"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-affiliatetrackingV2", "{0}/brandId/{brandId}/content/affiliatetracking",
                new {controller = "Content", action = "GetAffiliateTracking"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "get-PaymentUiRedirectDataV2", "{0}/brandId/{brandId}/subscription/PaymentUiRedirectData",
                new {controller = "Subscription", action = "GetPaymentUiRedirectData"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "get-AdminPaymentUiRedirectData", "{0}/brandId/{brandId}/subscription/AdminPaymentUiRedirectData",
                new {controller = "Subscription", action = "AdminGetPaymentUiRedirectData"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "get-PaymentTransactionInfoV2", "{0}/brandId/{brandId}/subscription/transactionInfo/{orderId}",
                new {controller = "Subscription", action = "GetPaymentTransactionInfo"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "put-LifeTimeAccess", "{0}/brandId/{brandId}/subscription/lifetimeaccess/",
                new {controller = "Subscription", action = "UpdateLifeTimeAccess"},
                new {httpMethod = new HttpMethodConstraint("Put")});

            routes.MapRoute(
                "get-LifeTimeAccess", "{0}/brandId/{brandId}/subscription/lifetimeaccess/{memberId}",
                new {controller = "Subscription", action = "GetLifeTimeAccess"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-AccessPrivileges", "{0}/brandId/{brandId}/subscription/accessprivileges",
                new {controller = "Subscription", action = "GetAccessPrivileges"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-IsMobileAppSubscriber", "{0}/brandId/{brandId}/subscription/ismobileappsubscriber/{memberId}",
                new {controller = "Subscription", action = "GetIsMobileAppSubscriber"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            /////////////////////////////////////////////////////////////////////////////////////////
            // subscription plans updates
            routes.MapRoute(
                "put-UpdateSubscriptionPlan", "{0}/brandId/{brandId}/subscription/plan",
                new {controller = "Subscription", action = "UpdateSubscriptionPlan"},
                new {httpMethod = new HttpMethodConstraint("Put")});

            routes.MapRoute(
                "put-GetSubscriptionPlanStatus", "{0}/brandId/{brandId}/subscription/plan",
                new {controller = "Subscription", action = "GetSubscriptionPlanStatus"},
                new {httpMethod = new HttpMethodConstraint("Get")});
            /////////////////////////////////////////////////////////////////////////////////////////
            // Free trial cancellation
            routes.MapRoute(
                "put-DisableFreeTrial", "{0}/brandId/{brandId}/subscription/freetrial/disable",
                new {controller = "Subscription", action = "DisableFreeTrial"},
                new {httpMethod = new HttpMethodConstraint("Put")});

            routes.MapRoute(
                "get-SubscriptionHistory", "{0}/brandId/{brandId}/subscription/history",
                new { controller = "Subscription", action = "GetTransactionHistory" },
                new { httpMethod = new HttpMethodConstraint("Get") });
            /////////////////////////////////////////////////////////////////////////////////////////

            routes.MapRoute(
                "post-applicationV2", "{0}/brandId/{brandId}/apps/application",
                new {controller = "Application", action = "CreateApplication"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "get-applicationV2", "{0}/brandId/{brandId}/apps/application/{applicationId}",
                new {controller = "Application", action = "GetApplication"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "put-applicationV2", "{0}/brandId/{brandId}/apps/application/{applicationId}",
                new {controller = "Application", action = "UpdateApplication"},
                new {httpMethod = new HttpMethodConstraint("Put")});

            routes.MapRoute(
                "put-resetsecretV2", "{0}/brandId/{brandId}/apps/application/{applicationId}/clientsecret",
                new {controller = "Application", action = "ResetClientSecret"},
                new {httpMethod = new HttpMethodConstraint("Put")});

            routes.MapRoute(
                "delete-applicationV2", "{0}/brandId/{brandId}/apps/application/{applicationId}",
                new {controller = "Application", action = "DeleteApplication"},
                new {httpMethod = new HttpMethodConstraint("Delete")});

            //	obsolete.  Is this still used by MOS?
            routes.MapRoute(
                "get-authenticationV2", "{0}/brandId/{brandId}/authenticate/{email}/{password}",
                new {controller = "registration", action = "GetAuthentication"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "post-memberregisterV2", "{0}/brandId/{brandId}/member/register",
                new {controller = "registration", action = "register"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-memberreportabuse", "{0}/brandId/{brandId}/member/{memberId}/reportabuse",
                new {controller = "Profile", action = "ReportAbuse"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-membersendcontactusemail", "{0}/brandId/{brandId}/sendcontactusemail",
                new { controller = "Profile", action = "SendContactUsEmail" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "post-membersendcontactusemailclient", "{0}/brandId/{brandId}/contactusclient",
                new { controller = "Profile", action = "SendContactUsEmail" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "post-membersendcontactusemailbytoken", "{0}/brandId/{brandId}/contactus",
                new { controller = "Profile", action = "SendContactUsEmailByToken" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "post-memberregisterextendedV2", "{0}/brandId/{brandId}/member/registerextended",
                new {controller = "Registration", action = "RegisterExtended"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-recordactivity", "{0}/brandId/{brandId}/activity/record",
                new {controller = "Activity", action = "RecordActivity"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-recordactivitywithparameters", "{0}/brandId/{brandId}/activitywithparameters/record",
                new {controller = "Activity", action = "RecordActivityWithParameters"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-minglemigrationregister", "{0}/brandId/{brandId}/minglemigration/register",
                new {controller = "Migration", action = "Register"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-minglemigrationupdateattributes", "{0}/brandId/{brandId}/minglemigration/updateattributes",
                new {controller = "Migration", action = "UpdateMemberAttributeData"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-minglemigrationtransferattributes", "{0}/brandId/{brandId}/minglemigration/transferattributes",
                new {controller = "Migration", action = "TransferMemberAttributeData"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-minglemigrationsavecredentials", "{0}/brandId/{brandId}/minglemigration/savecredentials",
                new {controller = "Migration", action = "SaveLogonCredentials"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-minglemigrationuploadphoto", "{0}/brandId/{brandId}/minglemigration/photo/upload",
                new {controller = "Migration", action = "UploadPhoto"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-migration-saveynmvote", "{0}/brandId/{brandId}/minglemigration/hotlist/ynmvote",
                new { controller = "BrokeredHotList", action = "SaveYNMVote" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "put-migration-addhotlistnote",
                "{0}/brandId/{brandId}/minglemigration/hotlist/{hotListCategory}/addnote",
                new { controller = "BrokeredHotList", action = "AddHotListNote" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
               "post-minglemigrationregriskiquiry", "{0}/brandId/{brandId}/minglemigration/doregistrationriskinquiry",
               new { controller = "Migration", action = "DoRegistrationRiskInquiry" },
               new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "put-hotlist-addhotlistnote",
                "{0}/brandId/{brandId}/hotlist/{hotListCategory}/addnote",
                new { controller = "Hotlist", action = "AddHotListNote" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "put-migration-transferlistdata", "{0}/brandId/{brandId}/minglemigration/hotlistbatch",
                new { controller = "BrokeredHotList", action = "TransferMemberListData" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "put-migration-transferynmdata", "{0}/brandId/{brandId}/minglemigration/hotlist/ynmbatch",
                new { controller = "BrokeredHotList", action = "TransferMemberYNMData" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "put-migration-updatemailmask", "{0}/brandId/{brandId}/minglemigration/hotlist/updatemailmask",
                new { controller = "BrokeredHotList", action = "UpdateMailMask" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "post-migration-hotlist", "{0}/brandId/{brandId}/minglemigration/hotlist/{hotListCategory}",
                new { controller = "BrokeredHotList", action = "AddToHotlist" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "delete-migration-hotlist",
                "{0}/brandId/{brandId}/minglemigration/hotlist/{hotListCategory}/targetmemberid/{targetMemberId}",
                new { controller = "BrokeredHotList", action = "RemoveFromHotlist" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Remove following three commented routes after Mingle Photo Migration is completed
            // Leaving them in here and marked Obsolete in the Controller

            // Unintentionally merged to Release, breaks /docs, commented out for now.
            //routes.MapRoute(
            //     "post-minglemigrationuploadapprovedphoto", "{0}/brandId/{brandId}/minglemigration/photo/uploadapproved",
            //     new { controller = "Migration", action = "UploadApprovedPhoto" },
            //     new { httpMethod = new HttpMethodConstraint("POST") });

            //routes.MapRoute(
            //     "post-minglemigrationaddapprovedfile", "{0}/brandId/{brandId}/minglemigration/photo/addapprovedfile",
            //     new { controller = "Migration", action = "AddApprrovedPhotoFile" },
            //     new { httpMethod = new HttpMethodConstraint("POST") });

            //routes.MapRoute(
            //    "post-minglemigrationapprovephoto", "{0}/brandId/{brandId}/minglemigration/photo/approve",
            //    new {controller = "Migration", action = "ApprovePhoto"},
            //    new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-minglemigrationdeletephoto", "{0}/brandId/{brandId}/minglemigration/photo/delete",
                new {controller = "Migration", action = "DeletePhoto"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            //routes.MapRoute(
            //    "post-minglemigrationrejectphoto", "{0}/brandId/{brandId}/minglemigration/photo/reject",
            //    new {controller = "Migration", action = "RejectPhoto"},
            //    new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-minglemigrationreorderPhotos", "{0}/brandId/{brandId}/minglemigration/photo/reorder",
                new {controller = "Migration", action = "ReorderPhotos"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            // map attributes from BH back to mingle
            routes.MapRoute(
                "post-minglemigrationmaptomingleattributes", "{0}/brandId/{brandId}/minglemigration/attributes/map",
                new {controller = "Migration", action = "MapToMingleAttributes"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-minglemigrationtransferephoto", "{0}/brandId/{brandId}/minglemigration/photo/transfermetadata",
                new {controller = "Migration", action = "TransferPhotoMetadata"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-MingleMigrationTransferPhotoMetadataCollection",
                "{0}/brandId/{brandId}/minglemigration/photo/transferphotometadatacollection",
                new {controller = "Migration", action = "TransferPhotoMetadataCollection"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-minglemigrationupdatephotocaption", "{0}/brandId/{brandId}/minglemigration/photo/updatecaption",
                new {controller = "Migration", action = "UpdatePhotoCaption"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-memberdeviceAutoLogin", "{0}/brandId/{brandId}/memberdevice/autologin",
                new {controller = "MemberDevice", action = "UpdateAutoLoginDate"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-memberdeviceadd", "{0}/brandId/{brandId}/memberdevice",
                new {controller = "MemberDevice", action = "Add"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "delete-memberdevice", "{0}/brandId/{brandId}/memberdevice",
                new {controller = "MemberDevice", action = "Delete"},
                new {httpMethod = new HttpMethodConstraint("Delete")});

            routes.MapRoute(
                "post-memberdeviceupdatenotificationsetting", "{0}/brandId/{brandId}/memberdevice/notificationsettings",
                new {controller = "MemberDevice", action = "UpdateDeviceNotificationSetting"},
                new {httpMethod = new HttpMethodConstraint("PUT")});

            routes.MapRoute(
                "post-sendpush", "{0}/brandId/{brandId}/memberdevice/sendpush",
                new { controller = "MemberDevice", action = "SendPushNotification" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "post-sendpushclient", "{0}/brandId/{brandId}/memberdevice/sendpushclient",
                new { controller = "MemberDevice", action = "SendPushNotificationClient" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "post-memberregisterrecordstartV2", "{0}/brandId/{brandId}/member/recordregistrationstart",
                new {controller = "Registration", action = "RecordRegistrationStart"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-memberregisterrecordcompleteV2", "{0}/brandId/{brandId}/member/recordregistrationcomplete",
                new {controller = "Registration", action = "RecordRegistrationComplete"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-memberregisterrecordcompletewithrecaptureinfoV2",
                "{0}/brandId/{brandId}/member/recordregistrationcompletewithrecaptureinfo",
                new {controller = "Registration", action = "RecordRegistrationCompleteWithRecaptureInfo"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-memberregisterrecordstepV2", "{0}/brandId/{brandId}/member/recordregistrationstep",
                new {controller = "Registration", action = "RecordRegistrationStep"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-memberregisterrecordstepwithrecaptureinfoV2",
                "{0}/brandId/{brandId}/member/recordregistrationstepwithrecaptureinfo",
                new {controller = "Registration", action = "RecordRegistrationStepWithRecaptureInfo"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "get-getregistrationrecaptureinfoV2", "{0}/brandId/{brandId}/member/getregistrationrecaptureinfo",
                new {controller = "Registration", action = "GetRegistrationRecaptureInfo"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "post-completeregistrationV2", "{0}/brandId/{brandId}/completeregistration",
                new {controller = "registration", action = "completeregistration"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "post-completeregistrationwithiovationV2",
                "{0}/brandId/{brandId}/completeregistrationwithiovation",
                new {controller = "registration", action = "completeregistrationwithiovation"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "get-memberaccessinfoV2", "{0}/brandId/{brandId}/accessinfo",
                new {controller = "registration", action = "GetAccessInfo"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-newmemberid", "{0}/brandId/{brandId}/member/GetNewMemberID",
                new {controller = "registration", action = "GetNewMemberID"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "create-accesstokenpasswordV2", "{0}/brandId/{brandId}/oauth2/accesstoken/application/{applicationId}",
                new {controller = "OAuth", action = "CreateAccessTokenPassword"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "create-accesstokenpasswordwithloginfraudV2",
                "{0}/brandId/{brandId}/oauth2/accesstokenwithloginfraud/application/{applicationId}",
                new {controller = "OAuth", action = "CreateAccessTokenPasswordWithLoginFraud"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "create-accesstokenrefreshtokenV2",
                "{0}/brandId/{brandId}/oauth2/accesstoken/application/{applicationId}/refreshtoken",
                new {controller = "OAuth", action = "CreateAccessTokenUsingRefreshToken"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "hardresetpassword",
                "{0}/brandId/{brandId}/oauth2/accesstoken/application/{applicationId}/hardresetpassword",
                new {controller = "OAuth", action = "HardResetPassword"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "resetpassword",
                "{0}/brandId/{brandId}/oauth2/accesstoken/application/{applicationId}/resetpassword",
                new {controller = "OAuth", action = "ResetPassword"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "changepassword",
                "{0}/brandId/{brandId}/oauth2/accesstoken/application/{applicationId}/changepassword",
                new {controller = "OAuth", action = "ChangePassword"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "authenticatedchangepassword",
                "{0}/brandId/{brandId}/oauth2/accesstoken/application/{applicationId}/authenticatedchangepassword",
                new {controller = "OAuth", action = "AuthenticatedChangePassword"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "authenticatedchangeemail",
                "{0}/brandId/{brandId}/oauth2/accesstoken/authenticatedchangeemail",
                new {controller = "OAuth", action = "AuthenticatedChangeEmail"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "createlogoncredentials",
                "{0}/brandId/{brandId}/oauth2/accesstoken/application/{applicationId}/createcredentials",
                new {controller = "OAuth", action = "CreateLogonCredentials"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "validateaccesstokenV2.2", "{0}/brandId/{brandId}/oauth2/accesstoken/validate/",
                new {controller = "OAuth", action = "ValidateAccessTokenV2"},
                new {httpMethod = new HttpMethodConstraint("Get"), acceptVersion = new AcceptVersionConstraint("V2.2")});

            routes.MapRoute(
                "validateaccesstoken", "{0}/brandId/{brandId}/oauth2/accesstoken/validate/",
                new {controller = "OAuth", action = "ValidateAccessToken"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "authenticateaccesstoken",
                "{0}/brandId/{brandId}/oauth2/accesstoken/authenticate/",
                new {controller = "OAuth", action = "AuthenticateAccessToken"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-registrationmetadataV2", "{0}/brandId/{brandId}/content/registration/metadata",
                new {controller = "Content", action = "GetRegistrationScenarios"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-registrationrandomscenarioV2", "{0}/brandId/{brandId}/content/registration/randomscenarioname",
                new {controller = "Content", action = "GetRandomScenarioName"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-registrationrandomscenariobydevicetypeV2",
                "{0}/brandId/{brandId}/content/registration/{deviceType}/randomscenarioname",
                new {controller = "Content", action = "GetRandomScenarioNameByDeviceType"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-pixelsV2", "{0}/brandId/{brandId}/content/pixels",
                new {controller = "Content", action = "RetrievePixels"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-mapurltopromoV2", "{0}/brandId/{brandId}/content/promotion/MapURLToPromo",
                new {controller = "Content", action = "MapURLToPromo"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "create-sessiontrackingV2", "{0}/brandId/{brandId}/session/createsessiontracking",
                new {controller = "Session", action = "CreateSessionTracking"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "create-sessionV2", "{0}/brandId/{brandId}/session/create",
                new {controller = "Session", action = "Create"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "create-sessionwithsessionidV2", "{0}/brandId/{brandId}/session/createwithsessionid",
                new {controller = "Session", action = "CreateWithSessionId"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "get-memberforsessionV2", "{0}/brandId/{brandId}/session/getmember",
                new {controller = "Session", action = "GetMemberForSession"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-instantmessengerinvitations", "{0}/brandId/{brandId}/instantmessenger/invites",
                new {controller = "InstantMessenger", action = "GetInvites"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "delete-instantmessengerinvite", "{0}/brandId/{brandId}/instantmessenger/invite",
                new {controller = "InstantMessenger", action = "RemoveInvite"},
                new {httpMethod = new HttpMethodConstraint("DELETE")});

            routes.MapRoute(
                "post-instantmessengerinvite", "{0}/brandId/{brandId}/instantmessenger/invite",
                new {controller = "InstantMessenger", action = "AddInvite"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-sendmissedim", "{0}/brandId/{brandId}/instantmessenger/missedIM",
                new {controller = "InstantMessenger", action = "SendMissedIM"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "get-canmembereply", "{0}/brandId/{brandId}/instantmessenger/canmemberreply/{senderMemberId}",
                new {controller = "InstantMessenger", action = "CanMemberReply"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-canim",
                "{0}/brandId/{brandId}/instantmessenger/canim/{SenderMemberId}/{TargetMemberId}/{CanInviteCheckOnly}",
                new {controller = "InstantMessenger", action = "CanIM", CanInviteCheckOnly = UrlParameter.Optional},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-validateconversationtoken",
                "{0}/brandId/{brandId}/instantmessenger/validateconversationtoken/{senderMemberId}/{recipientMemberId}/{conversationTokenId}",
                new {controller = "InstantMessenger", action = "ValidateConversationToken"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "post-saveimmessage", "{0}/brandId/{brandId}/application/{applicationId}/instantmessenger/saveIM",
                new {controller = "InstantMessenger", action = "PostIMMessage"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-saveintegratedimmessage", "{0}/brandId/{brandId}/instantmessenger/saveintegratedIM",
                new {controller = "InstantMessenger", action = "PostIntegratedIMMessage"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "get-instantmessagehistory",
                "{0}/brandId/{brandId}/instantmessenger/getInstantMessageHistory/targetMemberID/{TargetMemberID}/page/{PageNumber}/pagesize/{PageSize}/{IgnoreCache}",
                new
                {
                    controller = "InstantMessenger",
                    action = "GetInstantMessageHistory",
                    IgnoreCache = UrlParameter.Optional
                },
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-instantmessagehistorymembers",
                "{0}/brandId/{brandId}/instantmessenger/getInstantMessageHistoryMembers/{IgnoreCache}",
                new
                {
                    controller = "InstantMessenger",
                    action = "GetInstantMessageHistoryMembers",
                    IgnoreCache = UrlParameter.Optional
                },
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-removeinstantmessagemember",
                "{0}/brandId/{brandId}/instantmessenger/removeInstantMessageMember/messageId/{MessageId}/undo/{Undo}",
                new {controller = "InstantMessenger", action = "RemoveInstantMessageMember"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-chatcreateconversation",
                "{0}/brandId/{brandId}/chat/createConversation",
                new { controller = "InstantMessenger", action = "CreateConversation" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "get-chatrecordinvitesent",
                "{0}/brandId/{brandId}/chat/recordInviteSent/{roomid}/{to_memberid}",
                new { controller = "InstantMessenger", action = "RecordInviteSent" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-chatvalidateinvite",
                "{0}/brandId/{brandId}/chat/validateInvite/{roomid}/{to_memberid}/{from_memberid}",
                new { controller = "InstantMessenger", action = "ValidateInvite" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-chatgetiminfo",
                "{0}/brandId/{brandId}/chat/getIMInfo",
                new { controller = "InstantMessenger", action = "GetIMInfo" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "post-saveynmvote", "{0}/brandId/{brandId}/hotlist/ynmvote",
                new {controller = "HotList", action = "SaveYNMVote"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "get-getynmvote", "{0}/brandId/{brandId}/hotlist/ynmvote/targetMemberId/{addMemberId}",
                new {controller = "HotList", action = "GetYNMVote"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-getdisplaysettings", "{0}/brandId/{brandId}/profile/displaysettings",
                new {controller = "Profile", action = "GetDisplaySettings"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "put-updatedisplaysettings", "{0}/brandId/{brandId}/profile/displaysettings",
                new {controller = "Profile", action = "UpdateDisplaySettings"},
                new {httpMethod = new HttpMethodConstraint("PUT")});

            routes.MapRoute(
                "get-getemailsettings", "{0}/brandId/{brandId}/profile/emailsettings",
                new {controller = "Profile", action = "GetEmailSettings"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "put-updateemailsettings", "{0}/brandId/{brandId}/profile/emailsettings",
                new {controller = "Profile", action = "UpdateEmailSettings"},
                new {httpMethod = new HttpMethodConstraint("PUT")});

            routes.MapRoute(
                "get-getselfsuspendreason", "{0}/brandId/{brandId}/profile/selfsuspendreason",
                new {controller = "Profile", action = "GetSelfSuspendedReason"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "put-selfsuspend", "{0}/brandId/{brandId}/profile/selfsuspend",
                new {controller = "Profile", action = "SelfSuspend"},
                new {httpMethod = new HttpMethodConstraint("PUT")});

            routes.MapRoute(
                "put-reactivateselfsuspend", "{0}/brandId/{brandId}/profile/reactivateselfsuspend",
                new {controller = "Profile", action = "ReActivateSelfSuspended"},
                new {httpMethod = new HttpMethodConstraint("PUT")});

            routes.MapRoute(
                "put-updateusername", "{0}/brandId/{brandId}/profile/username",
                new {controller = "Profile", action = "UpdateUsername"},
                new {httpMethod = new HttpMethodConstraint("PUT")});

            routes.MapRoute(
                "get-memberexists", "{0}/brandId/{brandId}/profile/memberexists/{memberId}",
                new {controller = "Profile", action = "MemberExists"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-imfavoritesprofilerostering", "{0}/brandId/{brandId}/profile/imfavorites/{memberId}",
                new {controller = "Profile", action = "IMFavoritesProfileRostering"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-getmemberslideshow", "{0}/brandId/{brandId}/memberslideshow/slideshow",
                new {controller = "MemberSlideShow", action = "GetMemberSlideShow"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-getmemberslideshowminiprofile", "{0}/brandId/{brandId}/memberslideshow/slideshow/miniprofile",
                new {controller = "MemberSlideShow", action = "GetMemberSlideShowMiniProfile"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-GetMemberSlideShowMembers", "{0}/brandId/{brandId}/memberslideshow/members",
                new {controller = "MemberSlideShow", action = "GetMemberSlideShowMembers"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-spotlightSettings", "{0}/brandId/{brandId}/premium/spotlightsettings",
                new {controller = "Premium", action = "GetSpotlightSettings"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "update-spotlightSettings", "{0}/brandId/{brandId}/premium/spotlightsettings",
                new {controller = "Premium", action = "UpdateSpotlightSettings"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "get-QuestionAnswer-Question", "{0}/brandId/{brandId}/premium/questionanswer/question/{questionId}",
                new {controller = "Premium", action = "GetQuestion"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-QuestionAnswer-ActiveQuestions", "{0}/brandId/{brandId}/premium/questionanswer/activequestions",
                new { controller = "Premium", action = "GetActiveQuestionList" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-QuestionAnswer-ActiveQuestionsPaged", "{0}/brandId/{brandId}/premium/questionanswer/activequestionspaged",
                new { controller = "Premium", action = "GetActiveQuestionListPaged" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-QuestionAnswer-MemberQuestions", "{0}/brandId/{brandId}/premium/questionanswer/memberquestions/{memberId}",
                new { controller = "Premium", action = "GetMemberQuestions" },
                new { httpMethod = new HttpMethodConstraint("GET") });
            routes.MapRoute(
                "get-QuestionAnswer-CurrentMemberQuestions", "{0}/brandId/{brandId}/premium/questionanswer/memberquestions/",
                new { controller = "Premium", action = "GetMemberQuestions" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-QuestionAnswer-ActiveQuestionIds", "{0}/brandId/{brandId}/premium/questionanswer/activequestionids",
                new { controller = "Premium", action = "GetActiveQuestionIds" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-QuestionAnswer-RecentAnswers", "{0}/brandId/{brandId}/premium/questionanswer/recentanswers",
                new { controller = "Premium", action = "GetRecentAnswerList" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-QuestionAnswer-AddAnswer", "{0}/brandId/{brandId}/premium/questionanswer",
                new { controller = "Premium", action = "AddAnswer" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "get-QuestionAnswer-RemoveMemberAnswer", "{0}/brandId/{brandId}/premium/questionanswer/memberquestions/{questionId}",
                new { controller = "Premium", action = "RemoveMemberAnswer" },
                new { httpMethod = new HttpMethodConstraint("Delete") });

            routes.MapRoute(
                "get-QuestionAnswer-UpdateMemberAnswer", "{0}/brandId/{brandId}/premium/questionanswer/memberquestions/{questionId}",
                new { controller = "Premium", action = "UpdateMemberAnswer" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "get-QuestionAnswer-AdminUpdateAnswer", "{0}/brandId/{brandId}/premium/questionanswer/admin",
                new { controller = "Premium", action = "AdminUpdateAnswer" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "get-ControllerNamesAndMethods", "{0}/docs",
                new {controller = "Documentation", action = "GetEndPoints"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-GetResponse", "{0}/docs/responses/{responseName}",
                new {controller = "Documentation", action = "GetResponse"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-GetControllerMethodDocument", "{0}/docs/{controllerName}/{methodName}",
                new {controller = "Documentation", action = "GetControllerMethodDocument"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-registrationattributevalidationV2", "{0}/brandId/{brandId}/member/validateregistrationattribute",
                new {controller = "Registration", action = "ValidateRegistrationAttribute"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-healthcheck", "{0}/admin/healthcheck",
                new {controller = "admin", action = "gethealthcheck"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-bustcache", "{0}/admin/ClearCache/{cacheType}",
                new {controller = "admin", action = "ClearCache"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "put-loginfo-obsolete", "{0}/loginfo",
                new {controller = "Content", action = "LogInfo"},
                new {httpMethod = new HttpMethodConstraint("PUT")});

            routes.MapRoute(
                "put-logerror-obsolete", "{0}/logerror",
                new {controller = "Content", action = "LogError"},
                new {httpMethod = new HttpMethodConstraint("PUT")});

            routes.MapRoute(
                "post-loginfo", "{0}/log/info",
                new { controller = "Log", action = "LogInfo" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "post-logerror", "{0}/log/error",
                new { controller = "Log", action = "LogError" },
                new { httpMethod = new HttpMethodConstraint("POST") });
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "get-miniprofile", "profile/miniProfile/{targetMemberId}",
                new {controller = "Profile", action = "GetMiniProfile"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-photos", "profile/photos/{targetMemberId}",
                new {controller = "Photo", action = "GetPhotos"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-mailFolder", "mail/folder/{folderId}/pagesize/{pageSize}/page/{pageNumber}",
                new {controller = "Mail", action = "GetMailFolder"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-mailMessage", "mail/message/{messageListId}",
                new {controller = "Mail", action = "GetMailMessage"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "delete-mailMessage", "mail/message/folder/{currentFolderId}/message/{messageId}",
                new {controller = "Mail", action = "DeleteMailMessage"},
                new {httpMethod = new HttpMethodConstraint("Delete")});

            routes.MapRoute(
                "post-mailMessage", "mail/message",
                new {controller = "Mail", action = "SendMailMessage"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            // send a flirt/smile to another user
            routes.MapRoute(
                "post-tease", "mail/tease",
                new {controller = "Mail", action = "SendTease"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "get-brand", "content/brand/{brandUri}",
                new {controller = "Content", action = "GetBrandByDomain"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-citiesbyzipcode", "content/region/citiesbyzipcode/{zipCode}",
                new {controller = "Content", action = "GetCitiesByZipCodes"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-attributeoptions", "content/attributes/options/{attributeName}",
                new {controller = "Content", action = "GetAttributeOptions"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-attributemetadata", "content/attributes/metadata/{attributeName}",
                new {controller = "Content", action = "GetAttributeMetadata"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            // deprecated
            //routes.MapRoute(
            //    "create-session", "session/create",
            //    new { controller = "Session", action = "Create" },
            //    new { httpMethod = new HttpMethodConstraint("POST") });

            //routes.MapRoute(
            //    "save-session", "session/save",
            //    new { controller = "Session", action = "Save" },
            //    new { httpMethod = new HttpMethodConstraint("POST") });

            //routes.MapRoute(
            //    "get-session", "session/{key}",
            //    new { controller = "Session", action = "Get" },
            //    new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-teases", "content/teases/category/{teaseCategoryID}",
                new {controller = "Content", action = "GetTeases"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-teasecategories", "content/teases/categories",
                new {controller = "Content", action = "GetTeaseCategories"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-teasesall", "content/teases/all",
                new {controller = "Content", action = "GetAllTeases"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-hotlist", "hotlist/{hotListCategory}/pagesize/{pageSize}/pagenumber/{pageNumber}",
                new {controller = "Hotlist", action = "GetHotListEntries"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            //routes.MapRoute(
            //    "post-favorite", "hotlist/favorite",
            //    new {controller = "Hotlist", action = "PostFavorite"},
            //    new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "delete-favorite", "hotlist/favoritemember/{favoriteMemberId}",
                new {controller = "Hotlist", action = "DeleteFavorite"},
                new {httpMethod = new HttpMethodConstraint("DELETE")});

            routes.MapRoute(
                "get-hotlistcounts", "hotlist/counts",
                new {controller = "Hotlist", action = "GetHotListCounts"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-searchresultspost", "search/results",
                new {controller = "Search", action = "GetSearchResultsViaPost"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "get-matchpreferences", "match/preferences",
                new {controller = "Search", action = "GetMatchPreferences"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "put-matchpreferences", "match/preferences",
                new {controller = "Search", action = "PutMatchPreferences"},
                new {httpMethod = new HttpMethodConstraint("Put")});

            routes.MapRoute(
                "get-matchresults", "match/results/pagesize/{pageSize}/page/{pageNumber}",
                new {controller = "Search", action = "GetMatchResults"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-membersonline", "membersonline/count",
                new {controller = "Membersonline", action = "GetMembersOnlineCount"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "post-membersonline", "addmembersonline",
                new {controller = "MembersOnline", action = "Add"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            //routes.MapRoute(
            //    "put-membersonline", "removemembersonline",
            //    new {controller = "MembersOnline", action = "Delete"},
            //    new {httpMethod = new HttpMethodConstraint("Delete")});

            //routes.MapRoute(
            //    "get-healthcheck", "admin/healthcheck",
            //    new { controller = "admin", action = "gethealthcheck" },
            //    new { httpMethod = new HttpMethodConstraint("Get") });

            routes.MapRoute(
                "put-attributes", "profile/attributes",
                new {controller = "Profile", action = "UpdateAttributeData"},
                new {httpMethod = new HttpMethodConstraint("Put")});

            //routes.MapRoute(
            //    "get-attributes", "profile/attributes/member/{memberId}",
            //    new { controller = "Profile", action = "GetAttributeData" },
            //    new { httpMethod = new HttpMethodConstraint("Post") });

            routes.MapRoute(
                "get-attributesetviewer", "profile/attributeset/{attributeSetName}/{targetMemberId}",
                new {controller = "Profile", action = "GetAttributeSet", targetMemberId = UrlParameter.Optional},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-attributeset", "profile/attributeset/{attributeSetName}/{targetMemberId}",
                new {controller = "Profile", action = "GetAttributeSet", targetMemberId = UrlParameter.Optional},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-runtimesetting", "content/runtimesetting/{settingName}",
                new {controller = "Content", action = "GetRuntimeSetting"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-affiliatetracking", "content/affiliatetracking",
                new {controller = "Content", action = "GetAffiliateTracking"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "get-PaymentTransactionInfo", "subscription/transactionInfo/{orderId}",
                new {controller = "Subscription", action = "GetPaymentTransactionInfo"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-application", "apps/application/{applicationId}",
                new {controller = "Application", action = "GetApplication"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "put-application", "apps/application/{applicationId}",
                new {controller = "Application", action = "UpdateApplication"},
                new {httpMethod = new HttpMethodConstraint("Put")});

            routes.MapRoute(
                "put-resetsecret", "apps/application/{applicationId}/clientsecret",
                new {controller = "Application", action = "ResetClientSecret"},
                new {httpMethod = new HttpMethodConstraint("Put")});

            routes.MapRoute(
                "delete-application", "apps/application/{applicationId}",
                new {controller = "Application", action = "DeleteApplication"},
                new {httpMethod = new HttpMethodConstraint("Delete")});

            routes.MapRoute(
                "get-authentication", "authenticate/{email}/{password}",
                new {controller = "registration", action = "GetAuthentication"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "post-memberregister", "member/register",
                new {controller = "registration", action = "register"},
                new {httpMethod = new HttpMethodConstraint("POST")});

            routes.MapRoute(
                "post-completeregistration", "completeregistration",
                new {controller = "registration", action = "completeregistration"},
                new {httpMethod = new HttpMethodConstraint("GET")});

            routes.MapRoute(
                "get-memberaccessinfo", "accessinfo",
                new {controller = "registration", action = "GetAccessInfo"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "create-accesstokenpassword", "oauth2/accesstoken/application/{applicationId}",
                // /applicationId/{applicationId}/email/{email}/password/{password}
                new {controller = "OAuth", action = "CreateAccessTokenPassword"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "create-accesstokenrefreshtoken", "oauth2/accesstoken/application/{applicationId}/refreshtoken",
                //"applicationId/{applicationId}/memberId/{memberId}/refreshtoken",
                new {controller = "OAuth", action = "CreateAccessTokenUsingRefreshToken"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
                "post-oauth2authenticatepassword", "{0}/brandId/{brandId}/oauth2/password/authenticate",
                new {controller = "OAuth", action = "AuthenticatePassword"},
                new {httpMethod = new HttpMethodConstraint("Post")});

            routes.MapRoute(
              "get-MemberFraudInfo", "{0}/brandId/{brandId}/fraud/memberfraudinfo",
              new { controller = "Fraud", action = "GetMemberFraudInfo" },
              new { httpMethod = new HttpMethodConstraint("Post") });


            RegisterRoutesV2(routes);
        }

        public static void RegisterErrorRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "get-http500", "{0}/errors/exception",
                new {controller = "errors", action = "http500"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-http404", "{0}/errors/notfound",
                new {controller = "errors", action = "http404"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-http403", "{0}/errors/forbidden",
                new {controller = "errors", action = "http403"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-http401", "{0}/errors/unauthorized",
                new {controller = "errors", action = "http401"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "get-http400", "{0}/errors/badrequest",
                new {controller = "errors", action = "http400"},
                new {httpMethod = new HttpMethodConstraint("Get")});

            routes.MapRoute(
                "Catch-all", "{*url}",
                new {controller = "errors", action = "http404"},
                new {httpMethod = new HttpMethodConstraint("Get")});
        }
    }
}