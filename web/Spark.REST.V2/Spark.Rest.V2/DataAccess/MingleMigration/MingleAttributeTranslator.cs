﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Matchnet;
using Matchnet.Member.ValueObjects;

namespace Spark.Rest.V2.DataAccess.MingleMigration
{
    internal static class MingleAttributeTranslator
    {
        internal delegate void MingleAttributeTranslatorDelegate(Dictionary<string, object> attributeData,
            Dictionary<string, List<int>> attributeDataMultiValue);

        internal static readonly Dictionary<string, MingleAttributeTranslatorDelegate> MingleAttributeMethodMap = new Dictionary
            <string, MingleAttributeTranslatorDelegate>
		                                                                                               	{
																											{MingleAttributeConstants.GENDERMASK.ToLower(), TranslateGenderMask}, 
                                                                                                            {MingleAttributeConstants.HEIGHT.ToLower(), TranslateHeight},
                                                                                                            {MingleAttributeConstants.USERJOINIP.ToLower(), TranslateUserJoinIP},
                                                                                                            {MingleAttributeConstants.USERLASTIP.ToLower(), TranslateUserLastIP},
                                                                                                            {MingleAttributeConstants.USERDELETEDFLAG.ToLower(), TranslateUserDeletedFlag},
                                                                                                            {MingleAttributeConstants.USERCAPABILITIESCAPTURED.ToLower(), TranslateUserCapabilitiesCaptured},
                                                                                                            {MingleAttributeConstants.SUBSCRIPTIONFIRSTINITIALPURCHASEDATE.ToLower(), TranslateSubscriptionFirstInitialPurchaseDate},
                                                                                                            {MingleAttributeConstants.SUBSCRIPTIONLASTINITIALPURCHASEDATE.ToLower(), TranslateSubscriptionLastInitialPurchaseDate},
                                                                                                            {MingleAttributeConstants.USERLASTLOGIN.ToLower(), TranslateUserLastLogin},
                                                                                                            {MingleAttributeConstants.USERJOINDATE.ToLower(), TranslateUserJoinDate},
                                                                                                            {MingleAttributeConstants.USEREXPIRESDATE.ToLower(), TranslateUserExpiresDate}, 
                                                                                                            {MingleAttributeConstants.TRACKINGREGFORMFACTOR.ToLower(), TranslateTrackingRegFormFactor} 
                                                                                                        };

        internal static void TranslateGenderMask(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            if (attributeDataMultiValue == null || !attributeDataMultiValue.ContainsKey(MingleAttributeConstants.GENDERMASK) || attributeDataMultiValue[MingleAttributeConstants.GENDERMASK].Count != 1)
            {
                throw new ArgumentException(String.Format("Attribute data does not contain gendermask in the correct format"));
            }
            
            try
            {
                var explodedGenderMask = MingleAttributeLookups.GendermaskLookup[Convert.ToInt32(attributeDataMultiValue[MingleAttributeConstants.GENDERMASK][0])];
                attributeDataMultiValue[MingleAttributeConstants.GENDERMASK] = explodedGenderMask;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for gender: {0}", attributeDataMultiValue[MingleAttributeConstants.GENDERMASK][0].ToString()), ex);
            }
        }

        internal static void TranslateTrackingRegFormFactor(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            var valueFormat= "<DeviceProperties><IsMobileDevice>{0}</IsMobileDevice><IsTablet>{1}</IsTablet></DeviceProperties>";
            
            if (attributeData == null || !attributeData.ContainsKey(MingleAttributeConstants.TRACKINGREGFORMFACTOR) )
            {
                throw new ArgumentException(String.Format("Attribute data does not contain TrackingRegFormFactor in the correct format"));
            }

            try
            {
                switch (attributeData[MingleAttributeConstants.TRACKINGREGFORMFACTOR].ToString().ToLower())
                {
                    case "desktop":
                        attributeData[MingleAttributeConstants.TRACKINGREGFORMFACTOR] = string.Format(valueFormat, "false", "false");
                        break;
                    case "phone":
                        attributeData[MingleAttributeConstants.TRACKINGREGFORMFACTOR] = string.Format(valueFormat, "true", "false");
                        break;
                    case "tablet":
                        attributeData[MingleAttributeConstants.TRACKINGREGFORMFACTOR] = string.Format(valueFormat, "false", "true");
                        break;
                    case "unknown":
                        attributeData[MingleAttributeConstants.TRACKINGREGFORMFACTOR] = string.Format(valueFormat, "false", "false");
                        break;
                    default: 
                        attributeData[MingleAttributeConstants.TRACKINGREGFORMFACTOR] = string.Format(valueFormat, "false", "false");
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for TrackingRegFormFactor: {0}", attributeData[MingleAttributeConstants.TRACKINGREGFORMFACTOR].ToString()), ex);
            }
        }
        
        internal static void TranslateUserDeletedFlag(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            if (attributeDataMultiValue == null || !attributeData.ContainsKey(MingleAttributeConstants.USERDELETEDFLAG) )
            {
                throw new ArgumentException(String.Format("Attribute data does not contain user_deleted in the correct format"));
            }

            try
            {
                switch(Convert.ToInt32(attributeData[MingleAttributeConstants.USERDELETEDFLAG]))
                {
                    case 1:
                        attributeDataMultiValue.Add(MingleAttributeConstants.HIDEMASK, new List<int> {1,2,4,8});
                        attributeData.Add(MingleAttributeConstants.SELFSUSPENDEDFLAG, false);
                        attributeDataMultiValue.Add(MingleAttributeConstants.GLOBALSTATUSMASK, new List<int> { Convert.ToInt32(GlobalStatusMask.Default) });
                        break;
                    case 2:
                        attributeData.Add(MingleAttributeConstants.SELFSUSPENDEDFLAG, true);
                        attributeDataMultiValue.Add(MingleAttributeConstants.HIDEMASK, new List<int> { 0 });
                        attributeDataMultiValue.Add(MingleAttributeConstants.GLOBALSTATUSMASK, new List<int> { Convert.ToInt32(GlobalStatusMask.Default) });
                        break;
                    case 3:
                        attributeDataMultiValue.Add(MingleAttributeConstants.GLOBALSTATUSMASK, new List<int> {Convert.ToInt32(GlobalStatusMask.AdminSuspended)});
                        attributeDataMultiValue.Add(MingleAttributeConstants.HIDEMASK, new List<int> { 0 });
                        attributeData.Add(MingleAttributeConstants.SELFSUSPENDEDFLAG, false);
                        break;
                    case 0:
                        attributeDataMultiValue.Add(MingleAttributeConstants.HIDEMASK, new List<int> { 0 });
                        attributeDataMultiValue.Add(MingleAttributeConstants.GLOBALSTATUSMASK, new List<int> { Convert.ToInt32(GlobalStatusMask.Default) });
                        attributeData.Add(MingleAttributeConstants.SELFSUSPENDEDFLAG, false);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for user_deleted: {0}", attributeData[MingleAttributeConstants.USERDELETEDFLAG].ToString()), ex);
            }
        }

        internal static void TranslateHeight(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            if(!attributeData.ContainsKey(MingleAttributeConstants.HEIGHT))
            {
                throw new ArgumentException(String.Format("Attribute data does not contain height in the correct format"));
            }

            try
            {
                var valueInInches = Convert.ToInt32(attributeData[MingleAttributeConstants.HEIGHT]);
                attributeData[MingleAttributeConstants.HEIGHT] = Convert.ToInt32(Math.Truncate(Convert.ToDouble(valueInInches) * 2.54));
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for height: {0}", attributeData[MingleAttributeConstants.HEIGHT]), ex);
            }
        }

        internal static void TranslateUserJoinIP(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            if (!attributeData.ContainsKey(MingleAttributeConstants.USERJOINIP) || string.IsNullOrEmpty(attributeData[MingleAttributeConstants.USERJOINIP].ToString()))
            {
                throw new ArgumentException(String.Format("Attribute data does not contain user_join_ip in the correct format"));
            }

            try
            {
                attributeData[MingleAttributeConstants.USERJOINIP] = BitConverter.ToInt32(IPAddress.Parse(attributeData[MingleAttributeConstants.USERJOINIP].ToString()).GetAddressBytes(), 0);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for user_join_ip: {0}", attributeData[MingleAttributeConstants.USERJOINIP]), ex);
            }
        }

        internal static void TranslateUserLastIP(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            if (!attributeData.ContainsKey(MingleAttributeConstants.USERLASTIP) || string.IsNullOrEmpty(attributeData[MingleAttributeConstants.USERLASTIP].ToString()))
            {
                throw new ArgumentException(String.Format("Attribute data does not contain user_last_ip in the correct format"));
            }

            try
            {
                attributeData[MingleAttributeConstants.USERLASTIP] = BitConverter.ToInt32(IPAddress.Parse(attributeData[MingleAttributeConstants.USERLASTIP].ToString()).GetAddressBytes(), 0);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for user_last_ip: {0}", attributeData[MingleAttributeConstants.USERLASTIP]), ex);
            }
        }

        internal static int GetRegionID(Dictionary<string, object> attributeData)
        {
            if (!attributeData.ContainsKey(MingleAttributeConstants.REGIONID))
            {
                throw new ArgumentException(String.Format("Attribute data does not contain RegionID in the correct format"));
            }

            try
            {
                return Convert.ToInt32(attributeData[MingleAttributeConstants.REGIONID]);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for RegionID: {0}", attributeData[MingleAttributeConstants.REGIONID]), ex);
            }
        }

        internal static void TranslateUserCapabilitiesCaptured(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            if (!attributeData.ContainsKey(MingleAttributeConstants.USERCAPABILITIESCAPTURED))
            {
                throw new ArgumentException(String.Format("Attribute data does not contain UserCapabilitiesCaptured in the correct format"));
            }

            try
            {
                attributeData[MingleAttributeConstants.USERCAPABILITIESCAPTURED] = Convert.ToDateTime(attributeData[MingleAttributeConstants.USERCAPABILITIESCAPTURED]).AddHours(-1);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for UserCapabilitiesCaptured: {0}", attributeData[MingleAttributeConstants.USERCAPABILITIESCAPTURED]), ex);
            }
        }

        internal static void TranslateSubscriptionFirstInitialPurchaseDate(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            if (!attributeData.ContainsKey(MingleAttributeConstants.SUBSCRIPTIONFIRSTINITIALPURCHASEDATE))
            {
                throw new ArgumentException(String.Format("Attribute data does not contain SubscriptionFirstInitialPurchaseDate in the correct format"));
            }

            try
            {
                attributeData[MingleAttributeConstants.SUBSCRIPTIONFIRSTINITIALPURCHASEDATE] = Convert.ToDateTime(attributeData[MingleAttributeConstants.SUBSCRIPTIONFIRSTINITIALPURCHASEDATE]).AddHours(-1);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for SubscriptionFirstInitialPurchaseDate: {0}", attributeData[MingleAttributeConstants.SUBSCRIPTIONFIRSTINITIALPURCHASEDATE]), ex);
            }
        }

        internal static void TranslateSubscriptionLastInitialPurchaseDate(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            if (!attributeData.ContainsKey(MingleAttributeConstants.SUBSCRIPTIONLASTINITIALPURCHASEDATE))
            {
                throw new ArgumentException(String.Format("Attribute data does not contain SubscriptionLastInitialPurchaseDate in the correct format"));
            }

            try
            {
                attributeData[MingleAttributeConstants.SUBSCRIPTIONLASTINITIALPURCHASEDATE] = Convert.ToDateTime(attributeData[MingleAttributeConstants.SUBSCRIPTIONLASTINITIALPURCHASEDATE]).AddHours(-1);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for SubscriptionLastInitialPurchaseDate: {0}", attributeData[MingleAttributeConstants.SUBSCRIPTIONLASTINITIALPURCHASEDATE]), ex);
            }
        }

        internal static void TranslateUserLastLogin(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            if (!attributeData.ContainsKey(MingleAttributeConstants.USERLASTLOGIN))
            {
                throw new ArgumentException(String.Format("Attribute data does not contain UserLastLogin in the correct format"));
            }

            try
            {
                attributeData[MingleAttributeConstants.USERLASTLOGIN] = Convert.ToDateTime(attributeData[MingleAttributeConstants.USERLASTLOGIN]).AddHours(-1);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for UserLastLogin: {0}", attributeData[MingleAttributeConstants.USERLASTLOGIN]), ex);
            }
        }

        internal static void TranslateUserJoinDate(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            if (!attributeData.ContainsKey(MingleAttributeConstants.USERJOINDATE))
            {
                throw new ArgumentException(String.Format("Attribute data does not contain UserJoinDate in the correct format"));
            }

            try
            {
                attributeData[MingleAttributeConstants.USERJOINDATE] = Convert.ToDateTime(attributeData[MingleAttributeConstants.USERJOINDATE]).AddHours(-1);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for UserJoinDate: {0}", attributeData[MingleAttributeConstants.USERJOINDATE]), ex);
            }
        }

        internal static void TranslateUserExpiresDate(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            if (!attributeData.ContainsKey(MingleAttributeConstants.USEREXPIRESDATE))
            {
                throw new ArgumentException(String.Format("Attribute data does not contain UserExpiresDate in the correct format"));
            }

            try
            {
                attributeData[MingleAttributeConstants.USEREXPIRESDATE] = Convert.ToDateTime(attributeData[MingleAttributeConstants.USEREXPIRESDATE]).AddHours(-1);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for UserExpiresDate: {0}", attributeData[MingleAttributeConstants.USEREXPIRESDATE]), ex);
            }
        }
    }
}