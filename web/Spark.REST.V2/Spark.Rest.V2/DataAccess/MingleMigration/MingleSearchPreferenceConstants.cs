﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.DataAccess.MingleMigration
{
    internal static class MingleSearchPreferenceConstants
    {
        internal const string MINAGE = "wants_min_age";
        internal const string MAXAGE = "wants_max_age";
        internal const string DISTANCE = "wants_distance";
    }
}