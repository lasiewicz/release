﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.SearchPreferences;

namespace Spark.Rest.V2.DataAccess.MingleMigration
{
    internal static class MingleSearchPreferenceTranslator
    {
        internal static SearchPreference GetPreferenceValue(List<SearchPreference> searchPreferences, string preferenceKey)
        {
            if (searchPreferences == null || !searchPreferences.Any(sp => sp.Name == preferenceKey))
            {
                throw new ArgumentException(String.Format("Preferences do not contain " + preferenceKey));
            }

            return searchPreferences.First(sp => sp.Name == preferenceKey);
        }
    }
}