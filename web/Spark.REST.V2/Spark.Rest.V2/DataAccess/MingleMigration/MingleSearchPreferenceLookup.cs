﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.DataAccess.MingleMigration
{
    internal static class MingleSearchPreferenceLookup
    {
        internal static readonly Dictionary<string, string> Lookup = new Dictionary<string, string>
	                                                                        {
                                                                                {"wants_body_type", "BodyType"}, 
                                                                                {"wants_church_activity", "ChurchActivity"}, 
                                                                                {"wants_distance", "Distance"}, 
                                                                                {"wants_drink", "DrinkingHabits"}, 
                                                                                {"wants_education", "EducationLevel"}, 
                                                                                {"wants_ethnicity", "Ethnicity"}, 
                                                                                {"wants_marital_status", "MaritalStatus"}, 
                                                                                {"wants_max_age", "MaxAge"}, 
                                                                                {"wants_max_height", "MaxHeight"}, 
                                                                                {"wants_min_age", "MinAge"}, 
                                                                                {"wants_min_height", "MinHeight"},
                                                                                {"wants_religion", "Religion"},
                                                                                {"wants_smoke", "SmokingHabits"},
                                                                                {"wants_temple_status", "TempleStatus"},
                                                                                {"wants_mission", "Mission"}
	                                                                        };
        public static void test()
        {
            
        }
    }
}