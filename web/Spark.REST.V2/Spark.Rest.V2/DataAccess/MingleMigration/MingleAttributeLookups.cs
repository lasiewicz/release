﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.DataAccess.MingleMigration
{
    internal static class MingleAttributeLookups
    {
        internal static readonly Dictionary<int, List<int>> GendermaskLookup = new Dictionary<int, List<int>>
	                                                                        {
                                                                                {0,new List<int>{1,8}},
                                                                                {1,new List<int>{2,4}}
	                                                                        };
    }
}