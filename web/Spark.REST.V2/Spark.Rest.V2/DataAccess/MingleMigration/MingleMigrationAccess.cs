﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Spark.Logger;
using Spark.MingleMigration.ServiceAdapters;
using Spark.MingleMigration.ValueObjects;
using Spark.Rest.Helpers;
using Spark.Rest.V2.Helpers;
using Spark.Rest.V2.Models.Member;
using Spark.REST.Configuration;
using Spark.REST.Helpers;
using Spark.SearchPreferences;

namespace Spark.Rest.V2.DataAccess.MingleMigration
{
    public class MingleMigrationAccess
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MingleMigrationAccess));

        internal static bool ValidateSearchPreferences(List<SearchPreference> searchPreferences, List<MultiValuedSearchPreference> multiValuedSearchPreferences,
            int siteID, out string errorMessage)
        {
            errorMessage = string.Empty;
            var attributeMapper = AttributeMapperSA.Instance.GetMingleAttributeMapper();
            var searchPreferenceMapper = AttributeMapperSA.Instance.GetMingleSearchPreferenceMapper();

            var validationResult = AttributeMapperSA.Instance.ValidateSearchPreferences(searchPreferences,
                                                                 multiValuedSearchPreferences, searchPreferenceMapper,
                                                                 attributeMapper, siteID);

            if (!validationResult.Success)
            {
                var errorMessageBuilder = new StringBuilder();
                foreach (var error in validationResult.Errors)
                {
                    errorMessageBuilder.Append(string.Format("PreferenceName: {0} ErrorType: {1} Message: {2};",
                        error.PreferenceName, error.ErrorType.ToString(), error.ErrorMessage));
                }
                errorMessage = errorMessageBuilder.ToString();
                return false;
            }

            return true;
        }
        
        internal static  AttributeMappingResult MapTextAttributes(Dictionary<string, string> attributeData, int siteID)
        {            
            var attributeMapper = AttributeMapperSA.Instance.GetMingleAttributeMapper();
            var attributeMappingResult = AttributeMapperSA.Instance.MapTextAttributes(attributeData,
                                                                                         siteID,
                                                                                         AttributeMetadataSA.Instance,
                                                                                         attributeMapper);
            AddLastUpdatedToResult(attributeData.Keys.ToList(), attributeMappingResult);
            return attributeMappingResult;
        }

        internal static AttributeMappingResult MapAttributes(Dictionary<string, object> attributeData,
            Dictionary<string, List<int>> attributeDataMultiValue, int siteID)
        {
            var attributeMapper = AttributeMapperSA.Instance.GetMingleAttributeMapper();

            Dictionary<string, object> translatedAttributeData;
            Dictionary<string, List<int>> translatedAttributeDataMultiValue;
            MingleMigrationHelper.TranslateIncomingAttributesBeforeMapping(attributeData, attributeDataMultiValue,
                out translatedAttributeData, out translatedAttributeDataMultiValue);

            var attributeMappingResult = AttributeMapperSA.Instance.MapAttributes(translatedAttributeData,
                                                                                         translatedAttributeDataMultiValue,
                                                                                         siteID,
                                                                                         AttributeMetadataSA.Instance,
                                                                                         attributeMapper);

            AddLastUpdatedToResult(attributeData.Keys.ToList(), attributeMappingResult);
            AddLastUpdatedToResult(attributeDataMultiValue.Keys.ToList(), attributeMappingResult);

            return attributeMappingResult;
        }

        private static void AddLastUpdatedToResult(IEnumerable<string> mingleAttributeNames, AttributeMappingResult attributeMappingResult)
        {
            //if attribute not in black list, add LastUpdated attribute to result
            if (!attributeMappingResult.MappedAttributes.ContainsKey(MingleAttributeConstants.LAST_UPDATE_ATTR_NAME))
            {
                var mingleAttributeBlacklistlist = AttributeConfigReader.Instance.MingleAttributeBlacklistlistDictionaryByAttribute;
                if (mingleAttributeNames.Any(name => !mingleAttributeBlacklistlist.ContainsKey(name.ToLower())))
                {
                    if (!attributeMappingResult.MappedAttributes.ContainsKey(MingleAttributeConstants.LAST_UPDATE_ATTR_NAME))
                    {
                        attributeMappingResult.MappedAttributes.Add(MingleAttributeConstants.LAST_UPDATE_ATTR_NAME,
                            DateTime.Now);
                    }
                }
            }
        }

        internal static MemberRegisterResult SaveLogonCredentials(int memberId, string emailAddress, string username, string passwordHash, string passwordSalt, Brand brand)
        {
            return MingleMemberSA.Instance.SaveLogonCredentials(memberId, emailAddress, username, passwordHash, passwordSalt,
                                                         brand);
            
        }

        internal static bool RegisterMember(int memberId, Brand brand, string registrationSessionId, 
            int ipAddress, Dictionary<string, object> mappedAttributes, MemberSearch memberSearch, out string errorMessage)
        {
            bool memberRegistered;
            errorMessage = string.Empty;
            
            try
            {
                Log.LogDebugMessage("MigrationAccess.RegisterMember - about to call CreateMember: " + registrationSessionId, ErrorHelper.GetCustomData());
                
                memberRegistered = MingleMemberSA.Instance.CreateMember(memberId, 
                                                                        brand, 
                                                                        registrationSessionId,
                                                                        ipAddress, 
                                                                        mappedAttributes);

                Log.LogDebugMessage("MigrationAccess.RegisterMember - called CreateMember (" + memberRegistered + "): " + registrationSessionId, ErrorHelper.GetCustomData());

                // Failed
               if (memberRegistered)
               {
                   MemberSearchPreferencesSA.Instance.SaveMemberSearchCollection(new MemberSearchCollection {memberSearch}, 
                       memberId, 
                       brand.Site.Community.CommunityID);
                   
                   Log.LogDebugMessage("MigrationAccess.RegisterMember - saved member search : " + registrationSessionId, ErrorHelper.GetCustomData());
                }
            }
            catch (Exception ex)
            {
                Log.LogError("Error registering member: " + memberId, ex, ErrorHelper.GetCustomData());
                errorMessage = ex.Message;
                memberRegistered = false;
            }
            

            return memberRegistered;
        }

        internal static MingleMemberSaveResult SaveMember(int memberID,
            Brand brand,
            Dictionary<string, object> mappedAttributes,
            Dictionary<string, object> approvedTextAttributes,
            List<SearchPreference> searchPreferences,
            List<MultiValuedSearchPreference> searchPreferencesMultiValue,
            bool createMemberRecord,
            string password = "")
        {
            var approvedTextAttributesConvertedToStrings = new Dictionary<string, string>();
            if (approvedTextAttributes != null)
            {
                foreach (var attribute in approvedTextAttributes)
                {
                    // Bug Fix: add LastUpdated attribute to mapped attr dictionary, NOT approved text attr dictionary
                    if (attribute.Key == MingleAttributeConstants.LAST_UPDATE_ATTR_NAME)
                    {
                        if (null == mappedAttributes) mappedAttributes = new Dictionary<string, object>();
                        mappedAttributes[attribute.Key] = attribute.Value;
                    }
                    else
                    {
                        approvedTextAttributesConvertedToStrings.Add(attribute.Key, attribute.Value.ToString());
                    }
                }
            }

            return MingleMemberSA.Instance.SaveMember(mappedAttributes, approvedTextAttributesConvertedToStrings, searchPreferences, searchPreferencesMultiValue, memberID, brand, createMemberRecord, password);
        }

        internal static AttributeMappingResult MapBhToMingleAttributes(MingleRegistrationRequest request)
        {
            var mapper = AttributeMapperSA.Instance.GetMingleAttributeMapper();

            var internalApiAttributes = AttributeHelper.MapPublicAttributesToInternalFormat(request.AttributeData,
                                                                                       FixMultiValueKeyNames(request.AttributeDataMultiValue));

            var mappedAttributes = AttributeMapperSA.Instance.ReverseMapAttributes(
                internalApiAttributes, 
                request.Brand.Site.SiteID, 
                AttributeMetadataSA.Instance,
                mapper);
            
            //return new MingleMappedAttributes()
            //{
            //    AttributeData = mappedAttributes.MappedAttributes,
            //    AttributeDataMultiValue = new Dictionary<string, List<int>>()
            //};

            return mappedAttributes;
        }

        // because dictionaries don't serialize properly, some key come back in the 
        // form "keyname[0]" and this need to be cleaned up before mapping to internal names
        internal static Dictionary<string, List<int>> FixMultiValueKeyNames(Dictionary<string, List<int>> attributeDataMultiValue)
        {
            return attributeDataMultiValue.Keys.ToDictionary(key => key.Replace("[0]", string.Empty), key => attributeDataMultiValue[key]);
        }
    }
}