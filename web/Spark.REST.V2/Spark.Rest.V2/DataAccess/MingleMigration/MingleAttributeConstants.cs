﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.DataAccess.MingleMigration
{
    internal static class MingleAttributeConstants
    {
        internal const string GENDERMASK = "has_gender";
        internal const string HEIGHT = "has_height";
        internal const string REGIONID = "has_location_region";
        internal const string SOURCE = "source";
        internal const string BIRTHDATE = "has_birthdate";
        internal const string MINGLEUSERID = "mingleuserid";
        internal const string USERJOINIP = "user_join_ip";
        internal const string USERLASTIP = "user_last_ip";
        internal const string USERDELETEDFLAG = "user_deleted";
        internal const string GLOBALSTATUSMASK = "GlobalStatusMask";
        internal const string HIDEMASK = "HideMask";
        internal const string SELFSUSPENDEDFLAG = "SelfSuspendedFlag";
        internal const string USERCAPABILITIESCAPTURED = "UserCapabilitiesCaptured";
        internal const string SUBSCRIPTIONFIRSTINITIALPURCHASEDATE = "SubscriptionFirstInitialPurchaseDate";
        internal const string SUBSCRIPTIONLASTINITIALPURCHASEDATE = "SubscriptionLastInitialPurchaseDate";
        internal const string USERLASTLOGIN = "user_last_login";
        internal const string USERJOINDATE = "user_join_date";
        internal const string USEREXPIRESDATE = "user_expires_date";
        internal const string TRACKINGREGFORMFACTOR = "TrackingRegFormFactor";
        internal const string LAST_UPDATE_ATTR_NAME = "LastUpdated";
    }
}