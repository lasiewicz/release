﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.File.ServiceAdapters;
using Matchnet.File.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Spark.Logger;
using Spark.MingleMigration.ValueObjects;
using Spark.Rest.Helpers;
using Spark.Rest.V2.Entities.MingleMigration;
using Spark.Rest.V2.Models.MingleMigration;

#endregion

namespace Spark.Rest.V2.DataAccess.MingleMigration
{
    public class MingleMigrationPhotoAccess
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MingleMigrationPhotoAccess));

        public static readonly MingleMigrationPhotoAccess Instance = new MingleMigrationPhotoAccess();

        private MingleMigrationPhotoAccess()
        {
        }

        /// <summary>
        ///     For generating a new file Id and insert a new file record into mnFile.
        ///     This was being repeated in many instances, refactored.
        /// </summary>
        private static int InsertNewFile(int fileRootId, PhotoFileType fileType, int memberId, int brandId)
        {
            // generate a new DB Id
            var newFileId = KeySA.Instance.GetKey("FileID");

            // insert a new file record into mnFile for main photo
            FileSA.Instance.InsertFile(newFileId, fileRootId, "jpg", DateTime.Now);

            Log.LogDebugMessage(string.Format(
                "Saving migration photo metadata, saved new file. MemberId: {0} BrandId: {1} FileID: {2} FileType: {3}",
                memberId, brandId, newFileId, fileType), ErrorHelper.GetCustomData());

            return newFileId;
        }

        /// <summary>
        ///     Internal method for saving a Mingle Member Photo to LA System
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="brand"></param>
        /// <param name="listOrder"></param>
        /// <param name="caption"></param>
        /// <param name="insertDate"></param>
        /// <param name="approvedForMain"></param>
        /// <param name="isMain"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        public int SavePhotoMetadata(int memberId, Brand brand, int listOrder, string caption, DateTime insertDate,
            bool approvedForMain, bool isMain, List<PhotoTransferFile> files)
        {
            Log.LogDebugMessage(string.Format(
                "Saving migration photo metadata. MemberId: {0} BrandId: {1} listOrder: {2} Caption: {3} InsertDate: {4} ApprovedForMain: {5} IsMain: {6} NumberPhotoFiles: {6}",
                memberId, brand.BrandID, listOrder, caption, insertDate, approvedForMain, isMain), ErrorHelper.GetCustomData());

            var memberPhotoId = 0;

            try
            {
                // need at least one photo file to work with
                if (files == null || files.Count == 0)
                {
                    throw new ArgumentException("PhotoFiles are null or empty", "files");
                }

                // look for the full size photo in the list
                var mainPhotoFile =
                    (from file in files where file.FileType == PhotoFileType.RegularFWS select file).FirstOrDefault();

                if (mainPhotoFile == null) throw new Exception("FWS photo missing");

                var thumbPhotoFile =
                    (from file in files where file.FileType == PhotoFileType.Thumbnail select file).FirstOrDefault();

                if (thumbPhotoFile == null) throw new Exception("Thumbnail photo missing");

                // select files other than FWS and Thumbnail
                // RegularFWS = 1,Thumbnail = 2, Original = 3, iPhone4S = 4, MinglePreview = 5,
                var dynamicFiles = (from file in files
                    where file.FileType != PhotoFileType.RegularFWS && file.FileType != PhotoFileType.Thumbnail
                    select file).ToList();

                var mainPhotoFileId = InsertNewFile(mainPhotoFile.FileRoot, mainPhotoFile.FileType, memberId,
                    brand.BrandID);

                var thumbPhotoFileId = InsertNewFile(thumbPhotoFile.FileRoot, thumbPhotoFile.FileType, memberId,
                    brand.BrandID);

                // save to mnMember by creating new PhotoFiles
                var photoFiles = new List<PhotoFile>();

                foreach (var dynamicFile in dynamicFiles)
                {
                    var fileId = InsertNewFile(thumbPhotoFile.FileRoot, thumbPhotoFile.FileType, memberId, brand.BrandID);

                    var photoFile = new PhotoFile(dynamicFile.FileType, dynamicFile.Height, dynamicFile.Width,
                        dynamicFile.Size, fileId, dynamicFile.FilePath, dynamicFile.CloudPath);

                    photoFiles.Add(photoFile);
                }

                var photoUpdate = new PhotoUpdate(null,
                    false,
                    Constants.NULL_INT,
                    mainPhotoFileId,
                    mainPhotoFile.FilePath,
                    thumbPhotoFileId,
                    thumbPhotoFile.FilePath,
                    (byte) listOrder,
                    true,
                    false,
                    Constants.NULL_INT,
                    Constants.NULL_INT,
                    caption,
                    true,
                    mainPhotoFile.CloudPath,
                    thumbPhotoFile.CloudPath,
                    approvedForMain,
                    isMain,
                    true,
                    mainPhotoFile.Height,
                    mainPhotoFile.Width,
                    mainPhotoFile.Size,
                    thumbPhotoFile.Height,
                    thumbPhotoFile.Width,
                    thumbPhotoFile.Size) {Files = photoFiles};

                var success = MemberSA.Instance.SavePhotos(brand.BrandID, brand.Site.SiteID,
                    brand.Site.Community.CommunityID, memberId,
                    new[] {photoUpdate});

                // something went wrong in the SavePhotos call which does a lot...
                if (!success)
                {
                    throw new Exception("Error saving photos to Member.");
                }

                Log.LogDebugMessage(string.Format(
                    "Saving migration photo metadata, saved photo. MemberId: {0} BrandId: {1} listOrder: {2}", memberId,
                    brand.BrandID, listOrder), ErrorHelper.GetCustomData());

                // following code is for verifying if the prior saves went through without issues.

                var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
                var photos = member.GetPhotos(brand.Site.Community.CommunityID);

                if (photos == null || photos.Count == 0)
                {
                    Log.LogDebugMessage(string.Format(
                        "Error saving migration photo metadata, no photos returned. MemberId: {0} List Order: {1} BrandId: {2}",
                        memberId, listOrder, brand.BrandID), ErrorHelper.GetCustomData());
                    throw new Exception("Could not retrieve photos for member.");
                }

                Photo uploadedPhoto = null;

                foreach (Photo photo in photos)
                {
                    if (photo.ListOrder != listOrder) continue;
                    uploadedPhoto = photo;
                    break;
                }

                if (uploadedPhoto == null)
                {
                    Log.LogDebugMessage(string.Format(
                        "Error saving migration photo metadata, could not find uploaded photo. MemberId: {0} List Order: {1} BrandId: {2}",
                        memberId, listOrder, brand.BrandID), ErrorHelper.GetCustomData());
                    throw new Exception("Could not find uploaded photos for member.");
                }

                memberPhotoId = uploadedPhoto.MemberPhotoID;
            }
            catch (Exception ex)
            {
                Log.LogError(
                    string.Format(
                        "Exception saving migration photo metadata, could not find uploaded photo. MemberId: {0} List Order: {1} BrandId: {2}",
                        memberId, listOrder, brand.BrandID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            return memberPhotoId;
        }

        public bool DeletePhoto(int memberId, Brand brand, int memberPhotoId)
        {
            var success = true;
            Log.LogDebugMessage(string.Format("Deleting migration photo. MemberId: {0} MemberPhotoId: {1}", memberId, memberPhotoId), ErrorHelper.GetCustomData());

            try
            {
                var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
                var photo = member.GetPhotos(brand.Site.Community.CommunityID).Find(memberPhotoId);

                if (photo == null)
                {
                    throw new Exception("Migrated photo not found");
                }

                var photoUpdate = new PhotoUpdate(null, true, memberPhotoId, Constants.NULL_INT, string.Empty,
                    Constants.NULL_INT, string.Empty, 1, false, false, Constants.NULL_INT,
                    Constants.NULL_INT, string.Empty, false, string.Empty, string.Empty, false, false, false);

                var saved = MemberSA.Instance.SavePhotos(brand.BrandID, brand.Site.SiteID,
                    brand.Site.Community.CommunityID, memberId,
                    new[] {photoUpdate});

                if (!saved)
                {
                    Log.LogDebugMessage(string.Format("Error deleting migration photo. MemberId: {0} MemberPhotoId: {1}", memberId,
                        memberPhotoId), ErrorHelper.GetCustomData());
                }
            }
            catch (Exception ex)
            {
                Log.LogError(
                    string.Format(
                        "Exception encountered deleting migration photo, MemberId: {0} MemberPhotoId: {1} BrandId: {2}",
                        memberId, memberPhotoId, brand.BrandID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                success = false;
            }

            return success;
        }

        public MinglePhotoUploadResult AddApprovedPhotoFile(int memberId, Brand brand, string encodedPhotoData,
         int memberPhotoId,
         string cloudPath, int height, int width, int size, PhotoFileType fileType, out bool success)
        {
            // manually removed in Release
            throw new NotImplementedException();
        }

        public MinglePhotoUploadResult UploadApprovedPhoto(int memberId, Brand brand, string encodedPhotoData,
            string caption, int listOrder,
            string cloudPath, int height, int width, int size, bool approvedForMain, bool isMain,
            string originalCloudPath, int originalHeight, int originalWidth, int originalSize, out bool success)
        {
            // manually removed in Release
            throw new NotImplementedException();
        }

        public UploadPhotoResult UploadPhoto(int memberId, Brand brand, string encodedPhotoData,
            string caption, int listOrder)
        {
            var result = new UploadPhotoResult();

            try
            {
                Log.LogDebugMessage(string.Format("Uploading migration photo. MemberId: {0} Order: {1} BrandId: {2}", memberId, listOrder,
                brand.BrandID), ErrorHelper.GetCustomData());

                var photoData = Convert.FromBase64String(encodedPhotoData);

                var photoUpdate = new PhotoUpdate(photoData, false, Constants.NULL_INT, Constants.NULL_INT, string.Empty,
                    Constants.NULL_INT, string.Empty, (byte) listOrder, false, false, Constants.NULL_INT,
                    Constants.NULL_INT, caption, true, string.Empty, string.Empty, true,
                    true, false);

                var saved = MemberSA.Instance.SavePhotos(brand.BrandID, brand.Site.SiteID,
                    brand.Site.Community.CommunityID, memberId,
                    new[] {photoUpdate});

                if (!saved)
                {
                    Log.LogDebugMessage(string.Format("Error uploading migration photo. MemberId: {0} List Order: {1} BrandId: {2}",
                        memberId, listOrder, brand.BrandID), ErrorHelper.GetCustomData());
                    return result;
                }

                var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
                var photos = member.GetPhotos(brand.Site.Community.CommunityID);

                if (photos == null || photos.Count == 0)
                {
                    Log.LogDebugMessage(string.Format(
                        "Error uploading migration photo, no photos returned. MemberId: {0} List Order: {1} BrandId: {2}",
                        memberId, listOrder, brand.BrandID), ErrorHelper.GetCustomData());
                    return result;
                }

                Photo uploadedPhoto = null;

                foreach (Photo photo in photos)
                {
                    if (photo.ListOrder == listOrder)
                    {
                        uploadedPhoto = photo;
                        break;
                    }
                }

                if (uploadedPhoto == null)
                {
                    Log.LogDebugMessage(string.Format(
                        "Error uploading migration photo, uploaded photo not found, invalid list order? MemberId: {0} List Order: {1} BrandId: {2} Total Number of Photos: {3}",
                        memberId, listOrder, brand.BrandID, photos.Count), ErrorHelper.GetCustomData());
                    return result;
                }

                result.MinglePhotoUploadResult.CloudPath = uploadedPhoto.FileCloudPath;
                result.MinglePhotoUploadResult.MemberPhotoId = uploadedPhoto.MemberPhotoID;
                result.MinglePhotoUploadResult.FilePath = uploadedPhoto.FileWebPath;
                result.MinglePhotoUploadResult.Height = uploadedPhoto.FileHeight;
                result.MinglePhotoUploadResult.Width = uploadedPhoto.FileWidth;

                result.IsSuccessful = true;
            }
            catch (Exception ex)
            {
                Log.LogError(
                    string.Format(
                        "Exception encountered uploading migration photo, MemberId: {0} List Order: {1} BrandId: {2}",
                        memberId, listOrder, brand.BrandID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            return result;
        }

        public bool UpdatePhotoCaption(int memberId, Brand brand, int memberPhotoId, string caption)
        {
            var success = true;

            try
            {
                Log.LogDebugMessage(string.Format("Reordering migration photos. MemberId: {0} BrandId: {1}", memberId, brand.BrandID), ErrorHelper.GetCustomData());

                var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
                var photo = member.GetPhotos(brand.Site.Community.CommunityID).Find(memberPhotoId);

                if (photo == null)
                {
                    throw new Exception("Migrated photo not found");
                }

                var updatedPhotos = new List<PhotoUpdate>();
                var photoUpdate = new PhotoUpdate(null,
                    false,
                    photo.MemberPhotoID,
                    photo.FileID,
                    photo.FileWebPath,
                    photo.ThumbFileID,
                    photo.ThumbFileWebPath,
                    photo.ListOrder,
                    photo.IsApproved,
                    false,
                    0,
                    0,
                    caption,
                    true,
                    photo.FileCloudPath,
                    photo.ThumbFileCloudPath,
                    photo.IsApprovedForMain,
                    photo.IsMain,
                    true);
                updatedPhotos.Add(photoUpdate);

                var saved = MemberSA.Instance.SavePhotos(brand.BrandID,
                    brand.Site.SiteID,
                    brand.Site.Community.CommunityID,
                    memberId,
                    updatedPhotos.ToArray());

                if (!saved)
                {
                    success = false;
                    Log.LogDebugMessage(string.Format("Error reordering photos. MemberId: {0} BrandId: {1}", memberId, brand.BrandID), ErrorHelper.GetCustomData());
                }
            }
            catch (Exception ex)
            {
                Log.LogError(
                    string.Format("Exception reordering photos, MemberId: {0} BrandId: {1}", memberId, brand.BrandID),
                    ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                success = false;
            }

            return success;
        }

        public bool ReorderPhotos(int memberId, Brand brand, List<PhotoOrder> photoOrders)
        {
            var success = true;

            try
            {
                Log.LogDebugMessage(string.Format("Reordering migration photos. MemberId: {0} BrandId: {1}", memberId, brand.BrandID), ErrorHelper.GetCustomData());

                var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
                var photos = member.GetPhotos(brand.Site.Community.CommunityID);

                if (photos == null || photos.Count == 0)
                {
                    throw new Exception("No photos found for member.");
                }

                var photoUpdates = new PhotoUpdate[photoOrders.Count];
                var index = 0;
                var newMainMemberPhotoId = 0;
                var updatedPhotos = new List<PhotoUpdate>();

                var newPossibleMainPhoto =
                    (from po in photoOrders where po.ListOrder == 1 select po).FirstOrDefault();
                if (newPossibleMainPhoto != null)
                {
                    var photo = photos.Find(newPossibleMainPhoto.MemberPhotoId);
                    if (photo != null && photo.IsApproved && photo.IsApprovedForMain)
                    {
                        newMainMemberPhotoId = newPossibleMainPhoto.MemberPhotoId;
                    }
                }

                foreach (var photoOrder in photoOrders)
                {
                    var photo = photos.Find(photoOrder.MemberPhotoId);
                    if (photo == null)
                    {
                        throw new Exception(string.Format("Photo not found: {0}.", photoOrder.MemberPhotoId));
                    }

                    if (newMainMemberPhotoId > 0)
                    {
                        photo.IsMain = (photo.MemberPhotoID == newMainMemberPhotoId);
                    }

                    var photoUpdate = new PhotoUpdate(null,
                        false,
                        photo.MemberPhotoID,
                        photo.FileID,
                        photo.FileWebPath,
                        photo.ThumbFileID,
                        photo.ThumbFileWebPath,
                        (byte) photoOrder.ListOrder,
                        photo.IsApproved,
                        false,
                        0,
                        0,
                        photo.Caption,
                        photo.IsCaptionApproved,
                        photo.FileCloudPath,
                        photo.ThumbFileCloudPath,
                        photo.IsApprovedForMain,
                        photo.IsMain,
                        true);

                    updatedPhotos.Add(photoUpdate);
                    //photoUpdates[index] = photoUpdate;
                    index++;
                }

                if (newMainMemberPhotoId > 0)
                {
                    foreach (Photo photo in photos)
                    {
                        var listOrder = photo.ListOrder == 1 ? 2 : photo.ListOrder;

                        if (photo.IsMain && photo.MemberPhotoID != newMainMemberPhotoId)
                        {
                            var photoUpdate = new PhotoUpdate(null,
                                false,
                                photo.MemberPhotoID,
                                photo.FileID,
                                photo.FileWebPath,
                                photo.ThumbFileID,
                                photo.ThumbFileWebPath,
                                (byte) listOrder,
                                photo.IsApproved,
                                false,
                                0,
                                0,
                                photo.Caption,
                                photo.IsCaptionApproved,
                                photo.FileCloudPath,
                                photo.ThumbFileCloudPath,
                                photo.IsApprovedForMain,
                                false,
                                true);

                            updatedPhotos.Add(photoUpdate);
                        }
                    }
                }

                var saved = MemberSA.Instance.SavePhotos(brand.BrandID,
                    brand.Site.SiteID,
                    brand.Site.Community.CommunityID,
                    memberId,
                    updatedPhotos.ToArray());

                if (!saved)
                {
                    success = false;
                    Log.LogDebugMessage(string.Format("Error reordering photos. MemberId: {0} BrandId: {1}", memberId, brand.BrandID), ErrorHelper.GetCustomData());
                }
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Exception reordering photos, MemberId: {0} BrandId: {1}", memberId, brand.BrandID),
                    ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                success = false;
            }

            return success;
        }
    }
}