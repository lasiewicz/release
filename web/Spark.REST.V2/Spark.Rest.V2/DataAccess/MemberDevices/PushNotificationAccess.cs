﻿using Matchnet.Content.ValueObjects.SystemEvents;
using Matchnet.Content.ValueObjects.SystemEvents.Interfaces;
using Spark.Logger;
using Spark.PushNotifications.Processor.ServiceAdapter;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Models.MemberDevices;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spark.Rest.V2.DataAccess.MemberDevices
{
    public class PushNotificationAccess
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(PushNotificationAccess));
        public static readonly PushNotificationAccess Instance = new PushNotificationAccess();

        private PushNotificationAccess()
        {
        }

        public void SendPushNotification(int memberID, SendPushRequest sendPushRequest)
        {
            //need to create event specific implementations because they can have differing logic or overrides
            if (memberID <= 0)
                throw new SparkAPIReportableException(HttpSub400StatusCode.InvalidMemberId, "Invalid Member Id");

            IPushNotificationEvent pushEvent = null;
            switch (sendPushRequest.PushNotificationType)
            {
                case Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.ChatRequest:
                    pushEvent = new IMRequestEvent()
                    {
                        NotificationTypeId = Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.ChatRequest,
                        EventType = SystemEventType.ChatRequest
                    };
                    break;
                case Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.ChatRequestV2:
                    pushEvent = new IMRequestEvent()
                    {
                        NotificationTypeId = Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.ChatRequestV2,
                        EventType = SystemEventType.ChatRequest,
                        RoomID = sendPushRequest.IMID
                    };
                    break;
                case Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.ChatRequestV3:
                    pushEvent = new IMRequestEvent()
                    {
                        NotificationTypeId = Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.ChatRequestV3,
                        EventType = SystemEventType.ChatRequest,
                        RoomID = sendPushRequest.IMID,
                        ResourceID = sendPushRequest.ResourceID
                    };
                    break;
                case Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.MessageReceived:
                    if (sendPushRequest.MessageListId <= 0)
                    {
                        throw new SparkAPIReportableException(HttpSub400StatusCode.MissingArgument, "Message Received pushes requires the message list ID (mail ID)");
                    }

                    pushEvent = new EmailEvent()
                    {
                        NotificationTypeId = Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.MessageReceived,
                        EventType = SystemEventType.MessageReceived,
                        MessageListID = sendPushRequest.MessageListId
                    };
                    break;
                case Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.MutualYes:
                    pushEvent = new MutualYesEvent()
                    {
                        NotificationTypeId = Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.MutualYes,
                        EventType = SystemEventType.BothSaidYes
                    };
                    break;
                case Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.PhotoApproved:
                    pushEvent = new PhotoApprovalEvent()
                    {
                        NotificationTypeId = Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.PhotoApproved,
                        EventType = SystemEventType.PhotoApproved
                    };
                    break;
                case Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.PhotoRejected:
                    pushEvent = new PhotoRejectionEvent()
                    {
                        NotificationTypeId = Matchnet.Content.ValueObjects.PushNotification.PushNotificationTypeID.PhotoRejected,
                        EventType = SystemEventType.PhotoRejected
                    };
                    break;
            }

            if (pushEvent == null)
            {
                Log.LogInfoMessage(string.Format("Push Notification type not supported. BrandID: {0}, MemberID: {1}, TargetMemberID: {2}, NotificationType: {3}, AppGroup: {4}, SystemEventCategory: {5}",
                    sendPushRequest.Brand.BrandID, memberID, sendPushRequest.RecipientMemberId, sendPushRequest.PushNotificationType, sendPushRequest.AppGroupID, sendPushRequest.SystemEventCategory), null);

                throw new SparkAPIReportableException(HttpSub400StatusCode.InvalidPushNotificationType, "Push Notification Type not supported");
            }

            pushEvent.EventCategory = sendPushRequest.SystemEventCategory;
            pushEvent.MemberId = memberID;
            pushEvent.AppGroupId = sendPushRequest.AppGroupID;
            pushEvent.BrandId = sendPushRequest.Brand.BrandID;
            pushEvent.TargetMemberId = sendPushRequest.RecipientMemberId;

            (pushEvent as SystemEvent).Message = sendPushRequest.Message;

            PushNotificationsProcessorAdapter.Instance.SendNotificationEvent(pushEvent);

        }

    }
}