﻿using System;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ValueObjects.MemberAppDevices;
using Spark.Logger;
using Spark.LoginFraud;
using Spark.Rest.V2.DataAccess.Interfaces;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Helpers;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

namespace Spark.Rest.V2.DataAccess.MemberDevices
{
    public class MemberDeviceAccess : IMemberDeviceAccess
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MemberDeviceAccess));
        private IMemberAppDeviceService _MemberAppDeviceService;
        private IPushNotificationMetadataSAService _pushNotificationMetadataSAService;

        public IMemberAppDeviceService MemberAppDeviceService
        {
            get
            {
                return _MemberAppDeviceService ?? MemberSA.Instance;
            }
            set
            {
                _MemberAppDeviceService = value;
            }
        }

        public IPushNotificationMetadataSAService PushNotificationMetadataService
        {
            get
            {
                return _pushNotificationMetadataSAService ?? PushNotificationMetadataSA.Instance;
            }
            set
            {
                _pushNotificationMetadataSAService = value;
            }
        }

        public string AddAppDevice(Int32 memberId, Int32 appId, Brand brand, string deviceId, 
            string deviceType = "", string appVersion = "", string OSVersion = "")
        {
            
            if (memberId < 1)
            {
                throw new SparkAPIReportableArgumentException("memberID value must be greater than zero");
            }

            if (appId < 1)
            {
                throw new SparkAPIReportableArgumentException("appid value must be greater than zero");
            }

            if(string.IsNullOrEmpty(deviceId))
            {
                throw new SparkAPIReportableArgumentException("deviceId value can not be null or empty");
            }

            if(brand == null || brand.BrandID <1)
            {
                throw new SparkAPIReportableArgumentException("brand value can not be null or empty");
            }

            Log.LogDebugMessage(string.Format("Entered AddAppDevice. MemberId: {0} GetAppId: {1} BrandId: {2} DeviceId: {3} DeviceType: {4} AppVersion: {5} OSVersion: {6}",
                memberId, appId, brand.BrandID, deviceId, deviceId, deviceType, appVersion, OSVersion), 
                null);

            //TODO - get appGroupID based on appId
            var appGroupId = AppGroupID.JDateAppGroup;
            var notificationAppGroupModel = PushNotificationMetadataService.GetPushNotificationAppGroupModel(appGroupId);

            if (notificationAppGroupModel == null)
            {
               throw new SparkAPIException("No PushNotificationAppGroupModel found for GetAppId:" + appId.ToString() + ", AppGroupId:" + appGroupId.ToString());
            }
            
            var memberAppDevices = MemberAppDeviceService.GetMemberAppDevices(memberId);
            
            if (memberAppDevices!= null && memberAppDevices.ContainsDevice(deviceId, appId))
            {
                //device is already registered to member, so just log a warning and return the hash
                Log.LogWarningMessage(string.Format("Device: {0} already registered for app: {1} and member: {2}", deviceId, appId, memberId), null);
                return MemberAppDeviceHelper.GenerateMemberIdHash(memberId);
            }
            
            var deviceData = MemberAppDeviceHelper.SerializeDeviceData(deviceType, appVersion, OSVersion);
            
            long notificationFlags = PushNotificationsHelper.GetDefaultEnabledNotificationsValue(notificationAppGroupModel);

            MemberAppDeviceService.AddMemberAppDevice(memberId, appId, deviceId,
                brand.BrandID, deviceData, notificationFlags);

            Log.LogDebugMessage(string.Format("Completed AddAppDevice. MemberId: {0} GetAppId: {1} BrandId: {2} DeviceId: {3}",
                    memberId, appId, brand.BrandID, deviceId, deviceId),
                    null);

            return MemberAppDeviceHelper.GenerateMemberIdHash(memberId);
        }


        public void DeleteAppDevice(Int32 memberId, Int32 appId, string deviceId)
        {
            if (memberId < 1)
            {
                throw new SparkAPIReportableArgumentException("memberID value must be greater than zero");
            }

            if (appId < 1)
            {
                throw new SparkAPIReportableArgumentException("appid value must be greater than zero");
            }

            if (string.IsNullOrEmpty(deviceId))
            {
                throw new SparkAPIReportableArgumentException("deviceId value can not be null or empty");
            }

            var memberAppDevices = MemberAppDeviceService.GetMemberAppDevices(memberId);


            if (memberAppDevices == null || !memberAppDevices.ContainsDevice(deviceId, appId))
            {
                throw new SparkAPIReportableException(HttpSub400StatusCode.DeviceNotRegisteredToMember,
                   string.Format("Device: {0} is not registered to this member: {1}", deviceId, memberId));
            }
            
            
            MemberAppDeviceService.DeleteMemberAppDevice(memberId, appId, deviceId);
            
        }

        public void UpdateDeviceNotificationSettings(int memberId, int appId, string deviceId, PushNotificationCategoryId notificationCategoryId, bool enabled)
        {
            if (memberId < 1)
            {
                throw new SparkAPIReportableArgumentException("memberID value must be greater than zero");
            }

            if (appId < 1)
            {
                throw new SparkAPIReportableArgumentException("appId value must be greater than zero");
            }
            
            if (string.IsNullOrEmpty(deviceId))
            {
                throw new SparkAPIReportableArgumentException("deviceId value can not be null or empty");
            }

            Log.LogDebugMessage(string.Format("Entered UpdateDeviceNotificationSettings. MemberId: {0} GetAppId: {1} DeviceId: {2} PushNotificationCategoryId: {3} Enabled: {4}",
                memberId, appId, deviceId, deviceId, notificationCategoryId, enabled),
                null);

            //TODO - get appGroupID based on appId
            var appGroupId = AppGroupID.JDateAppGroup;
            var notificationAppGroupModel = PushNotificationMetadataService.GetPushNotificationAppGroupModel(appGroupId);

            if (notificationAppGroupModel == null)
            {
                throw new SparkAPIException("No PushNotificationAppGroupModel found for GetAppId:" + appId.ToString() + ", AppGroupId:" + appGroupId.ToString());
            }

            var memberAppDevices = MemberAppDeviceService.GetMemberAppDevices(memberId);

            if (memberAppDevices == null || !memberAppDevices.ContainsDevice(deviceId, appId))
            {
                throw new SparkAPIReportableException(HttpSub400StatusCode.DeviceNotRegisteredToMember,
                   string.Format("Device: {0} is not registered to this member: {1}", deviceId, memberId));
            }

            var memberAppDevice = memberAppDevices.GetDevice(deviceId, appId);
            
            var notificationTypesForCategory = PushNotificationsHelper.GetNotificationTypesFromModelForCategory(notificationAppGroupModel, notificationCategoryId);

            if(notificationTypesForCategory == null || notificationTypesForCategory.Count ==0)
            {
                throw new SparkAPIException("No notification types found for category: " + notificationCategoryId);
            }

            var newNotificationFlagsValue = memberAppDevice.PushNotificationsFlags;

            foreach(var notificationType in notificationTypesForCategory)
            {
                long notificationFlagValue = (long)Math.Pow(2, (double)notificationType);

                bool alreadyContainsValue = (newNotificationFlagsValue & notificationFlagValue) == notificationFlagValue;
                if(alreadyContainsValue && !enabled)
                {
                    newNotificationFlagsValue = newNotificationFlagsValue - notificationFlagValue;
                }
                if(!alreadyContainsValue && enabled)
                {
                    newNotificationFlagsValue = newNotificationFlagsValue + notificationFlagValue;
                }
            }

            MemberAppDeviceService.UpdateMemberAppDeviceNotificationsFlags(memberId, appId, deviceId, newNotificationFlagsValue);
            
            Log.LogDebugMessage(string.Format("Leaving UpdateDeviceNotificationSettings. MemberId: {0} GetAppId: {1} DeviceId: {2} PushNotificationCategoryId: {3} Enabled: {4}",
                memberId, appId, deviceId, deviceId, notificationCategoryId, enabled),
                null);
        }

        public void UpdateLastLoginDate(int memberId, Brand brand, string ipAddress, string userAgent, string headers, int applicationId)
        {
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            if (member == null || member.MemberID <= 0)
            {
                throw new SparkAPIReportableException(HttpSub400StatusCode.InvalidMemberId, "memberID value must be greater than zero");
            }

            member.SetAttributeDate(brand, "BrandLastLogonDate", DateTime.Now);
            MemberSA.Instance.SaveMember(member);

            var loginFraudClient = new LoginFraudHelper(brand);
            var response = loginFraudClient.LogAction(
                member.EmailAddress, 
                LogActionType.AutoLogin, 
                ipAddress, 
                userAgent ?? string.Empty,
                headers ?? string.Empty, 
                memberId, 
                applicationId, 
                false,
                true);

            if (!response.Success)
            {
                throw new Exception(String.Format("Error updating fraud client: {0}", response.ErrorMessage));
            }
        }
    }
}