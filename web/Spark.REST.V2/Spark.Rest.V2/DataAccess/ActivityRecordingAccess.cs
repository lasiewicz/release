﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Spark.ActivityRecording.Processor.ServiceAdapter;
using Spark.ActivityRecording.Processor.ValueObjects;
using Spark.Common.Adapter;
using Spark.Logger;
using Spark.LoginFraud;
using Spark.Rest.V2.Entities.Purchase;
using Spark.REST.Entities.Purchase;

namespace Spark.Rest.V2.DataAccess
{
    public interface IActivityRecordingAccess
    {
        void RecordActivity(int memberId, int targetMemberId, int actionType, int brandId, string caption, CallingSystem callingSystem = CallingSystem.Web);
        void RecordActivity(int memberId, int targetMemberId, int actionType, int brandId, Dictionary<string, string> captionParametersm, CallingSystem callingSystem = CallingSystem.Web);
        void RecordActivityAsynch(int memberId, int targetMemberId, int actionType, int brandId, Dictionary<string, string> captionParameters);
        void RecordActivityAsynch(int memberId, int targetMemberId, int actionType, int brandId, string caption);
        void RecordLoginFraudCallActivity(string loginSessionId, int brandId, string emailAddress, bool callSuccess, FraudActionType loginFraudStatus, int loginFraudRejectionReason);        
        void RecordAuthenticationAttemptActivity(string loginSessionId, string emailAddress, int memberId, int brandId, AuthenticationStatus authenticationStatus);
        void RecordPasswordResetRequestActivity(string loginSessionId, string emailAddress, bool requestSuccessful, int memberId, int brandId);       
        void RecordPasswordResetAttemptActivity(string loginSessionId, string emailAddress, bool resetSuccessful, int memberId, int brandId);
        void RecordLatestInAppReceiptActivity(int memberId, int brandId, IOSInAppInfo latestReceiptInfo);
        void RecordStatusForInAppReceiptActivity(int memberId, int brandId, IAPControllerStatus iapStatus, IOSInAppInfo latestReceiptInfo);
        void RecordLatestInAppBillingActivity(int memberId, int brandId, AndroidSubscription androidSubscription);
        void RecordStatusForInAppBillingActivity(int memberId, int brandId, IAPControllerStatus iapStatus, AndroidSubscription androidSubscription);
        void RecordLatestInAppReceiptActivityException(int memberId, int brandId, Exception ex);
        void RecordLatestInAppBillingActivityException(int memberId, int brandId, Exception ex);
    }

    public class ActivityRecordingAccess : IActivityRecordingAccess
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(ActivityRecordingAccess));
        
        public static readonly ActivityRecordingAccess Instance = new ActivityRecordingAccess();

        /// <summary>
        ///     Accepts a string for caption
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="actionType"></param>
        /// <param name="brandId"></param>
        /// <param name="caption"></param>
        /// <param name="callingSystem"></param>
        public void RecordActivity(int memberId, int targetMemberId, int actionType, int brandId, string caption
            , CallingSystem callingSystem = CallingSystem.Web)
        {
            var brand = BrandConfigSA.Instance.GetBrandByID(brandId);

            ActivityRecordingProcessorAdapter.Instance.RecordActivity(memberId, targetMemberId, actionType,
                brand.Site.SiteID, caption, callingSystem);
        }
      
        /// <summary>
        ///     Accepts a dictionary for caption 
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="callingSystem"></param>
        /// <param name="actionType"></param>
        /// <param name="brandId"></param>
        /// <param name="captionParameters"></param>
        public void RecordActivity(int memberId, int targetMemberId, int actionType,
            int brandId, Dictionary<string, string> captionParameters, CallingSystem callingSystem = CallingSystem.Web)
        {
            var brand = BrandConfigSA.Instance.GetBrandByID(brandId);

            ActivityRecordingProcessorAdapter.Instance.RecordActivity(memberId, targetMemberId, actionType,
                brand.Site.SiteID, callingSystem, captionParameters);
        }

        public void RecordActivityAsynch(int memberId, int targetMemberId, int actionType, int brandId, Dictionary<string, string> captionParameters)
        {
            var ctx = System.Web.HttpContext.Current;
            Matchnet.MiscUtils.FireAndForget(o =>
            {
                //set HttpContext for thread
                System.Web.HttpContext.Current = ctx;
                try
                {
                    RecordActivity(memberId, targetMemberId, actionType, brandId, captionParameters);
                }
                finally
                {
                    //unset HttpContext for thread (precaution for GC)
                    System.Web.HttpContext.Current = null;                    
                }
            },
                "APIActivityRecording",
                "Failed to record activity");
        }

        public void RecordActivityAsynch(int memberId, int targetMemberId, int actionType, int brandId, string caption)
        {
            var ctx = System.Web.HttpContext.Current;
            Matchnet.MiscUtils.FireAndForget(o =>
            {
                //set HttpContext for thread
                System.Web.HttpContext.Current = ctx;
                try
                {
                    RecordActivity(memberId, targetMemberId, actionType, brandId, caption);
                }
                finally
                {
                    //unset HttpContext for thread (precaution for GC)
                    System.Web.HttpContext.Current = null;
                }
            },
                "APIActivityRecording",
                "Failed to record activity");
        }

        public void RecordLoginFraudCallActivity(string loginSessionId, int brandId, string emailAddress, bool callSuccess, FraudActionType loginFraudStatus, int loginFraudRejectionReason)
        {
            if (!string.IsNullOrEmpty(loginSessionId))
            {
                var captionParameters = new Dictionary<string, string>
                                            {
                                                {CaptionConstants.LOGINSESSIONID, loginSessionId},
                                                {CaptionConstants.EMAILADDRESS, emailAddress},
                                                {CaptionConstants.LOGINFRAUDCALLSUCCESS, callSuccess.ToString()},
                                                {CaptionConstants.LOGINFRAUDRESULT, loginFraudStatus.ToString()},
                                                {CaptionConstants.LOGINFRAUDFREJECTEDREASON, loginFraudRejectionReason.ToString()}
                                            };
                RecordActivityAsynch(Constants.NULL_INT, Constants.NULL_INT, (int) ActionType.LoginFraudCheck, brandId,
                               captionParameters);
            }
        }

        public void RecordAuthenticationAttemptActivity(string loginSessionId, string emailAddress,  int memberId, int brandId, AuthenticationStatus authenticationStatus)
        {
            if (!string.IsNullOrEmpty(loginSessionId))
            {
                var captionParameters = new Dictionary<string, string>
                                            {
                                                {CaptionConstants.LOGINSESSIONID, loginSessionId},
                                                {CaptionConstants.EMAILADDRESS, emailAddress},
                                                {CaptionConstants.LOGINAUTHENTICATIONRESULT,authenticationStatus.ToString() }
                                            };
                RecordActivityAsynch(memberId, Constants.NULL_INT, (int) ActionType.LoginAuthentication, brandId, captionParameters);
            }
        }

        public void RecordPasswordResetRequestActivity(string loginSessionId, string emailAddress, bool requestSuccessful, int memberId, int brandId)
        {
            if (!string.IsNullOrEmpty(loginSessionId))
            {
                var captionParameters = new Dictionary<string, string>
                                            {
                                                {CaptionConstants.LOGINSESSIONID, loginSessionId},
                                                {CaptionConstants.EMAILADDRESS, emailAddress},
                                                {CaptionConstants.PASSWORDRESETREQUESTSUCCESSFUL, requestSuccessful.ToString()},
                                            };
                RecordActivityAsynch(memberId, Constants.NULL_INT, (int) ActionType.LoginPasswordResetRequestSubmitted, brandId,
                               captionParameters);
            }
        }

        public void RecordPasswordResetAttemptActivity(string loginSessionId, string emailAddress, bool resetSuccessful, int memberId, int brandId)
        {
            if (!string.IsNullOrEmpty(loginSessionId))
            {
                var captionParameters = new Dictionary<string, string>
                                            {
                                                {CaptionConstants.LOGINSESSIONID, loginSessionId},
                                                {CaptionConstants.EMAILADDRESS, emailAddress},
                                                {CaptionConstants.PASSWORDRESETREQUESTSUCCESSFUL, resetSuccessful.ToString()},
                                            };
                RecordActivityAsynch(memberId, Constants.NULL_INT, (int) ActionType.LoginAttemptedPasswordReset, brandId, captionParameters);
            }
        }

        public void RecordLatestInAppReceiptActivity(int memberId, int brandId, IOSInAppInfo latestReceiptInfo)
        {
            if (null != latestReceiptInfo)
            {
                var captionParameters = new Dictionary<string, string>
                                            {
                                                {CaptionConstants.IAP_CANCELLATION_DATE, latestReceiptInfo.cancellation_date},
                                                {CaptionConstants.IAP_EXPIRES_DATE, latestReceiptInfo.expires_date},
                                                {CaptionConstants.IAP_IS_TRIAL_PERIOD, latestReceiptInfo.is_trial_period},
                                                {CaptionConstants.IAP_ORIGINAL_PURCHASE_DATE, latestReceiptInfo.original_purchase_date},
                                                {CaptionConstants.IAP_ORIGINAL_TRANSACTION_ID, latestReceiptInfo.original_transaction_id},
                                                {CaptionConstants.IAP_PRODUCT_ID, latestReceiptInfo.product_id},
                                                {CaptionConstants.IAP_PURCHASE_DATE, latestReceiptInfo.purchase_date},
                                                {CaptionConstants.IAP_QUANTITY, latestReceiptInfo.quantity},
                                                {CaptionConstants.IAP_TRANSACTION_ID, latestReceiptInfo.transaction_id},
                                                {CaptionConstants.IAP_WEB_ORDER_LINE_ITEM_ID, latestReceiptInfo.web_order_line_item_id},
                                                {CaptionConstants.IAP_HAS_EXCEPTION, Boolean.FalseString},
                                                {CaptionConstants.IAP_EXCEPTION_MESSAGE, string.Empty}
                                            };
                RecordActivityAsynch(memberId, Constants.NULL_INT, (int) ActionType.IOSInAppPurchaseReceipt, brandId, captionParameters);
            }
        }

        public void RecordStatusForInAppReceiptActivity(int memberId, int brandId, IAPControllerStatus iapStatus, IOSInAppInfo latestReceiptInfo)
        {
            if (null != latestReceiptInfo)
            {
                var captionParameters = new Dictionary<string, string>
                                            {
                                                {CaptionConstants.IAP_CONTROLLER_STATUS, iapStatus.ToString()},
                                                {CaptionConstants.IAP_CANCELLATION_DATE, latestReceiptInfo.cancellation_date},
                                                {CaptionConstants.IAP_EXPIRES_DATE, latestReceiptInfo.expires_date},
                                                {CaptionConstants.IAP_IS_TRIAL_PERIOD, latestReceiptInfo.is_trial_period},
                                                {CaptionConstants.IAP_ORIGINAL_PURCHASE_DATE, latestReceiptInfo.original_purchase_date},
                                                {CaptionConstants.IAP_ORIGINAL_TRANSACTION_ID, latestReceiptInfo.original_transaction_id},
                                                {CaptionConstants.IAP_PRODUCT_ID, latestReceiptInfo.product_id},
                                                {CaptionConstants.IAP_PURCHASE_DATE, latestReceiptInfo.purchase_date},
                                                {CaptionConstants.IAP_QUANTITY, latestReceiptInfo.quantity},
                                                {CaptionConstants.IAP_TRANSACTION_ID, latestReceiptInfo.transaction_id},
                                                {CaptionConstants.IAP_WEB_ORDER_LINE_ITEM_ID, latestReceiptInfo.web_order_line_item_id}
                                            };
                RecordActivityAsynch(memberId, Constants.NULL_INT, (int) ActionType.IOSInAppPurchaseStatus, brandId, captionParameters);
            }            
        }

        public void RecordLatestInAppBillingActivity(int memberId, int brandId, AndroidSubscription androidSubscription)
        {
            if (null != androidSubscription)
            {
            var captionParameters = new Dictionary<string, string>
                                            {
                                                {CaptionConstants.IAB_SUBSCRIPTION_KIND, androidSubscription.Kind},
                                                {CaptionConstants.IAB_SUBSCRIPTION_IS_AUTORENEW, androidSubscription.IsAutoRenew.ToString()},
                                                {CaptionConstants.IAB_SUBSCRIPTION_EXPIRATION_DATE, androidSubscription.ExpirationDate.ToString()},
                                                {CaptionConstants.IAB_SUBSCRIPTION_START_DATE, androidSubscription.StartDate.ToString()},
                                                {CaptionConstants.IAB_SUBSCRIPTION_HAS_EXCEPTION, Boolean.FalseString},
                                                {CaptionConstants.IAB_SUBSCRIPTION_EXCEPTION_MESSAGE, string.Empty}
                                            };
            RecordActivityAsynch(memberId, Constants.NULL_INT, (int) ActionType.AndroidInAppBillingReceipt, brandId, captionParameters);
        }            
        }            

        public void RecordStatusForInAppBillingActivity(int memberId, int brandId, IAPControllerStatus iapStatus, AndroidSubscription androidSubscription)
        {
            if (null != androidSubscription)
            {
            var captionParameters = new Dictionary<string, string>
                                            {
                                                {CaptionConstants.IAP_CONTROLLER_STATUS, iapStatus.ToString()},
                                                {CaptionConstants.IAB_SUBSCRIPTION_KIND, androidSubscription.Kind},
                                                {CaptionConstants.IAB_SUBSCRIPTION_IS_AUTORENEW, androidSubscription.IsAutoRenew.ToString()},
                                                {CaptionConstants.IAB_SUBSCRIPTION_EXPIRATION_DATE, androidSubscription.ExpirationDate.ToString()},
                                                {CaptionConstants.IAB_SUBSCRIPTION_START_DATE, androidSubscription.StartDate.ToString()}
                                            };
            RecordActivityAsynch(memberId, Constants.NULL_INT, (int) ActionType.AndroidInAppBillingStatus, brandId, captionParameters);
        }
    }

        public void RecordLatestInAppReceiptActivityException(int memberId, int brandId, Exception ex)
        {
            if (null != ex)
            {
                XCData cdata = new XCData(ex.Message);
                var captionParameters = new Dictionary<string, string>
                {
                    {CaptionConstants.IAP_CANCELLATION_DATE, string.Empty},
                    {CaptionConstants.IAP_EXPIRES_DATE, string.Empty},
                    {CaptionConstants.IAP_IS_TRIAL_PERIOD, string.Empty},
                    {CaptionConstants.IAP_ORIGINAL_PURCHASE_DATE, string.Empty},
                    {CaptionConstants.IAP_ORIGINAL_TRANSACTION_ID, string.Empty},
                    {CaptionConstants.IAP_PRODUCT_ID, string.Empty},
                    {CaptionConstants.IAP_PURCHASE_DATE, string.Empty},
                    {CaptionConstants.IAP_QUANTITY, string.Empty},
                    {CaptionConstants.IAP_TRANSACTION_ID, string.Empty},
                    {CaptionConstants.IAP_WEB_ORDER_LINE_ITEM_ID, string.Empty},
                    {CaptionConstants.IAP_HAS_EXCEPTION, Boolean.TrueString},
                    {CaptionConstants.IAP_EXCEPTION_MESSAGE, cdata.ToString()}
                };
                RecordActivityAsynch(memberId, Constants.NULL_INT, (int) ActionType.IOSInAppPurchaseReceipt, brandId, captionParameters);
            }
        }

        public void RecordLatestInAppBillingActivityException(int memberId, int brandId, Exception ex)
        {
            if (null != ex)
            {
                XCData cdata = new XCData(ex.Message);
                var captionParameters = new Dictionary<string, string>
                {
                    {CaptionConstants.IAB_SUBSCRIPTION_KIND, string.Empty},
                    {CaptionConstants.IAB_SUBSCRIPTION_IS_AUTORENEW, string.Empty},
                    {CaptionConstants.IAB_SUBSCRIPTION_EXPIRATION_DATE, string.Empty},
                    {CaptionConstants.IAB_SUBSCRIPTION_START_DATE, string.Empty},
                    {CaptionConstants.IAB_SUBSCRIPTION_HAS_EXCEPTION, Boolean.TrueString},
                    {CaptionConstants.IAB_SUBSCRIPTION_EXCEPTION_MESSAGE, cdata.ToString()}
                };
                RecordActivityAsynch(memberId, Constants.NULL_INT, (int) ActionType.AndroidInAppBillingReceipt, brandId, captionParameters);
            }
        }
    }
}