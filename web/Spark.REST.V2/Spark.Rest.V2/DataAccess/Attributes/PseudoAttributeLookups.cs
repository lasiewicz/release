﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet;

namespace Spark.REST.DataAccess.Attributes
{
    internal static class PseudoAttributeLookups
    {
        internal static readonly Dictionary<string, int> FutureChildrenLookup = new Dictionary<string, int>
		                                                                       	{
		                                                                       		{"no", 0},
		                                                                       		{"yes", 1},
		                                                                       		{"not sure", 2},
		                                                                       		{"", Constants.NULL_INT}
		                                                                       	};

        internal static readonly Dictionary<int, int> GendermaskLookup = new Dictionary<int, int>
	                                                                        {
                                                                                {1,9},
                                                                                {2,6},
                                                                                {3,5},
                                                                                {4,10}
	                                                                        };
    }
}