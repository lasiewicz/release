﻿using System;
using System.Collections.Generic;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Spark.REST.Helpers;

namespace Spark.REST.DataAccess.Attributes
{
	internal static class PseudoAttributeUpdaters
	{

		internal delegate void PseudoAttributeDelegate(Brand brand, Member member, object attributeValue);

		internal static readonly Dictionary<string, PseudoAttributeDelegate> PseudoAttributeMethodMap = new Dictionary
			<string, PseudoAttributeDelegate>
		                                                                                               	{
		                                                                                               		{"gender", ProcessGender },
		                                                                                               		{"seekinggender", ProcessSeekingGender},
																											{"planonchildren", SetPlanOnChildren},
                                                                                                            {"gendermask", ProcessGendermask},
                                                                                                            {"birthdate", UpdateBirthdate},
                                                                                                            {"pledgetocounterfroud", UpdatePledge}
		                                                                                               	};

		



        private static void ProcessGendermask(Brand brand, Member member, object attributeValue)
        {
            int genderMask = 0;
            try
            {
                genderMask = PseudoAttributeLookups.GendermaskLookup[Convert.ToInt32(attributeValue)];
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for gender: {0}", attributeValue), ex);
            }

            member.SetAttributeInt(brand, "GenderMask", genderMask);
            MemberSA.Instance.SaveMember(member); 
        }

		private static void ProcessGenderOrSeekingGender(Brand brand, Member member, object attributeValue, bool isSeeking)
		{
			GenderMask gender;
			var genderString = isSeeking ? "Seeking" + (string)attributeValue : (string)attributeValue;
			try
			{
				gender = (GenderMask)Enum.Parse(typeof(GenderMask), genderString, true);
			}
			catch (Exception ex)
			{
				throw new ArgumentException(String.Format("Invalid value for gender: {0}", attributeValue), ex);
			}

			var genderMask = member.GetAttributeInt(brand, "GenderMask");
			if (genderMask == Constants.NULL_INT) // if no value (null_int) start with 0 value
			{
				genderMask = 0;
			}
			var genderBits = AttributeHelper.GetGenderBitmask(isSeeking);
			genderMask &= (~genderBits); // clear existing gender (or seeking gender) bits
			genderMask |= (int)gender; // set new gender (or seeking gender) bit
			member.SetAttributeInt(brand, "GenderMask", genderMask);
			MemberSA.Instance.SaveMember(member); // do this intermediate save now, so the old gendermask is not reloaded after a change
		}

		internal static void ProcessGender(Brand brand, Member member, object attributeValue)
		{
			ProcessGenderOrSeekingGender(brand, member, attributeValue, false);
		}

		internal static void ProcessSeekingGender(Brand brand, Member member, object attributeValue)
		{
			ProcessGenderOrSeekingGender(brand, member, attributeValue, true);
		}

		internal static void SetPlanOnChildren(Brand brand, Member member, object attributeValue)
		{
			int planOnChildrenValue;

            if (attributeValue is int)
            {
                planOnChildrenValue = Convert.ToInt32(attributeValue);
            }
            else
            {
                var planOnChildrenText = attributeValue == null ? "" : ((string) attributeValue).ToLower();
                if (!PseudoAttributeLookups.FutureChildrenLookup.TryGetValue(planOnChildrenText, out planOnChildrenValue))
                {
                    planOnChildrenValue = Constants.NULL_INT;
                }
            }
		    member.SetAttributeInt(brand, "MoreChildrenFlag", planOnChildrenValue);
		}

        internal static void UpdateBirthdate(Brand brand, Member member, object attributeValue)
        {
            DateTime dateTimeAttribute;
            if (DateTime.TryParse(Convert.ToString(attributeValue), out dateTimeAttribute))
            {
                if (MemberHelper.IsValidBirthdate(dateTimeAttribute))
                {
                    member.SetAttributeDate(brand, "Birthdate", dateTimeAttribute);
                }
                else
                {
                    throw new ArgumentException(
                    String.Format("Invalid value for Birthdate.  Must be between ages of 18 and 99. Input was: {0}",
                        attributeValue));
                }
            }
            else
            {
                throw new ArgumentException(
                    String.Format("Invalid value for Birthdate.  Expected a datetime, input was: {0}",
                        attributeValue));
            }
        }

        internal static void UpdatePledge(Brand brand, Member member, object attributeValue)
        {
            if (!(attributeValue is Boolean))
            {
                throw new ArgumentException(string.Format("Invalid value for {0}.  Expected a boolean, input was: {1}", "PledgeToCounterFroud", attributeValue));
            }

            member.SetAttributeInt(brand, "PledgeToCounterFroud", Convert.ToInt32(attributeValue));
            member.SetAttributeDate(brand, "PledgeToCounterFroudAcceptedDate", DateTime.Now);
        }

	}
}
