﻿#region

using System;
using System.Collections.Generic;
using Matchnet;

#endregion

namespace Spark.REST.DataAccess.Attributes
{
    internal static class PseudoAttributeTranslator
    {
        internal static readonly Dictionary<string, PseudoAttributeTranslatorDelegate> PseudoAttributeMethodMap = new Dictionary
            <string, PseudoAttributeTranslatorDelegate>
        {
            {"planonchildren", TranslatePlanOnChildren},
            {"gendermask", TranslateGenderMask}
        };

        internal static object TranslateGenderMask(object attributeValue)
        {
            int genderMask;
            try
            {
                genderMask = PseudoAttributeLookups.GendermaskLookup[Convert.ToInt32(attributeValue)];
            }
            catch (Exception ex)
            {
                throw new ArgumentException(String.Format("Invalid value for gender: {0}", attributeValue), ex);
            }

            return genderMask;
        }

        internal static object TranslatePlanOnChildren(object attributeValue)
        {
            int planOnChildrenValue;
            var planOnChildrenText = attributeValue == null ? "" : ((string) attributeValue).ToLower();
            if (!PseudoAttributeLookups.FutureChildrenLookup.TryGetValue(planOnChildrenText, out planOnChildrenValue))
            {
                planOnChildrenValue = Constants.NULL_INT;
            }

            return planOnChildrenValue;
        }

        internal delegate object PseudoAttributeTranslatorDelegate(object attributeValue);
    }
}