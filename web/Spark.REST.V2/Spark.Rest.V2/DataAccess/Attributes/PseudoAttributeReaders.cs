﻿#region

using System.Collections.Generic;
using Matchnet.Content.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Spark.Common.Localization;
using Spark.Rest.DataAccess.Purchase;
using Spark.REST.DataAccess.Subscription;
using Spark.REST.Entities.Profile;
using Spark.REST.Helpers;
using Spark.Rest.V2.BedrockPorts;
using Brand = Matchnet.Content.ValueObjects.BrandConfig.Brand;
using Spark.Rest.V2.Helpers;

#endregion

namespace Spark.REST.DataAccess.Attributes
{
    /// <summary>
    /// </summary>
    public class PseudoAttributeReaders
    {
        // TODO: forcing to lowercase all names, http://stackoverflow.com/questions/12486544/ignoring-case-in-dictionary-keys
        internal static Dictionary<string, PseudoAttributeDelegate> PseudoAttributeMethodMap = new Dictionary <string, PseudoAttributeDelegate>
        {
            { "adminsuspendedflag", GetAdminSuspendedFlag },
            { "age", GetAge },
            { "approvedphotocount", GetApprovedPhotoCount },
            { "auto_renew", GetGamAutoRenew },
            { "birthdate", GetBirthdate },
            { "blockcontact", GetBlockContact },
            { "blockcontactbytarget", GetBlockContactByTarget },
            { "blocksearch", GetBlockSearch },
            { "blocksearchbytarget", GetBlockSearchByTarget },
            { "canbuypremiumservice", CanBuyPremiumService },
            { "canbuyupgradesubscription", CanBuyUpgradeSubscription },
            { "channel", GetMarketingChannel },
            { "country", GetCountry },
            { "countryabbrev", GetCountryAbbrev },
            { "dayssincereg", GetDaysSinceRegistration },
            { "daystorenew", GetGamDaysToRenew }, 
            { "defaultphoto", GetDefaultPhoto }, //defaultphoto is the standard from version 2 of the attribute on
            { "dma", GetDMADescription },
            { "environment", GetEnvironment },
            { "education", GetGamEducation },
            { "ethnicity", GetGamEthnicity },
            { "features", GetGamFeatures },
            { "firstname", GetFirstName },
            { "free_time", GetGamFreeTime },
            { "gam_gender", GetGamGender },
            { "gender", ProcessGender},
            { "income", GetGamIncome },
            { "ishighlighted", IsHighlighted },
            { "isiabpayingmember", IsIABPayingMember },
            { "isiappayingmember", IsIAPPayingMember },
            { "isonline", IsOnline },
            { "ispayingmember", IsPayingMember },
            { "isspotlighted", IsSpotlighted },
            { "isviewersfavorite", IsViewersFavorite },
            { "lapsedays", GetSubscriptionLapseDays },
            { "lastmemid", GetGamLastMemId },
            { "lastname", GetLastName },
            { "location", GetLocation },
            { "locationfull", GetLocationFull },
            { "marital_st", GetGamMaritalStatus },
            { "matchrating", GetMatchRating },
            { "memberid", GetMemberId },
            { "photocount", GetPhotoCount },
            { "photos", GetPhotos },
            { "photourl", GetPhotoUrl },
            { "planonchildren", GetPlanOnChildren },
            { "primaryphoto", GetDefaultPhoto },  //primaryphoto is included for backwards compatibility
            { "prm", GetGamPrm },
            { "profession", GetGamProfession },
            { "promo_id", GetGamPromoId },
            { "reg_device", GetGamRegisteredDevice },
            { "religion", GetGamReligion },
            { "removefromsearch", GetRemoveFromSearch },
            { "removefromsearchbytarget", GetRemoveFromSearchByTarget },
            { "seekinggender", ProcessSeekingGender },
            { "since_logi", GetGamSinceLogin },
            { "since_purc", GetGamSincePurchase },
            { "since_reg", GetGamSinceReg },
            { "since_subx", GetGamSinceSubx },
            { "site", GetGamSite }, 
            { "subchannel", GetMarketingSubChannel }, 
            { "subscriptionstatus", GetSubscriptionStatus },
            { "subscriptionstatusgam", GetSubscriptionStatusGam },
            { "subscriptionstatustealium", GetSubscriptionStatusTealium },
            { "sub_status", GetGamSubStatus },
            { "thumbnailurl", GetThumbnailUrl },
            { "til_renew", GetGamTilRenew }, 
            { "username", GetUsername },
            { "user_state", GetGamUserState },
            { "zipcode", GetZipCode }
        };

        private static object GetMarketingChannel(Brand brand, Member member, int viewerid)
        {
            return "";
        }

        private static object GetMarketingSubChannel(Brand brand, Member member, int viewerid)
        {
            return "";
        }

        private static object GetGamMaritalStatus(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamMaritalStatus(brand, member);
        }

        private static object GetGamLastMemId(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamLastMemId(member);
        }

        private static object GetGamIncome(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamIncome(brand, member);
        }

        private static object GetGamGender(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamGender(brand, member);
        }

        private static object GetGamFreeTime(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamFreeTime(brand, member);
        }

        private static object GetGamFeatures(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamFeatures(brand, member);
        }

        private static object GetGamEthnicity(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamEthnicity(brand, member);
        }

        private static object GetGamSubStatus(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetSubStatus(brand, member);
        }

        private static object GetGamSite(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamSite(brand);
        }

        private static object GetGamSinceSubx(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamSubscriptionExpirationStatus(brand, member);
        }

        private static object GetSubscriptionLapseDays(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetSubscriptionLapseDays(brand, member);
        }

        private static object GetGamSinceReg(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamBrandInsertDate(brand, member);
        }

        private static object GetDaysSinceRegistration(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetDaysSinceRegistration(brand, member);
        }

        private static object GetGamReligion(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamReligion(brand, member);
        }

        private static object GetGamRegisteredDevice(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamRegisteredDevice(brand, member);
        }

        private static object GetGamProfession(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamProfession(brand, member);
        }

        private static object GetGamUserState(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.IsMemberSubscribed(brand, member) ? "subscriber" : "user_state";
        }

        private static object GetGamSincePurchase(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamSincePurchase(brand, member);
        }

        private static object GetGamSinceLogin(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamLastLogonDate(brand, member);
        }

        private static object GetGamPromoId(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamPromoId(brand, member);
        }

        private static object GetGamPrm(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamPrm(brand, member, string.Empty); 
        }

        private static object GetGamAutoRenew(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamAutoRenewStatus(brand, member);
        }

        private static object GetGamTilRenew(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamDaysUntilRenew(brand, member);
        }

        private static object GetGamDaysToRenew(Brand brand, Member member, int viewerid)
        {
            return MemberHelper.GetGamDaysToRenew(brand, member);
        }

        internal static object GetGamEducation(Brand brand, Member member, int viewerid)
        {
            var attributeValue = member.GetAttributeInt(brand, "EducationLevel");
            return AttributeHelper.GetGamEducationDescription(brand, attributeValue, false);
        }

        internal static object GetUsername(Brand brand, Member member, int viewerId)
        {
            return member.GetUserName(brand);
        }

        internal static object ProcessGender(Brand brand, Member member, int viewerId)
        {
            var genderMask = member.GetAttributeInt(brand, "GenderMask");
            return AttributeHelper.GetGenderDescription(brand, genderMask, false);
        }

        internal static object ProcessSeekingGender(Brand brand, Member member, int viewerId)
        {
            var genderMask = member.GetAttributeInt(brand, "GenderMask");
            return AttributeHelper.GetGenderDescription(brand, genderMask, true);
        }

        internal static object GetMemberId(Brand brand, Member member, int viewerId)
        {
            return member.MemberID;
        }

        internal static object GetDefaultPhoto(Brand brand, Member member, int viewerId)
        {
            var photo = PhotoAccess.GetDefaultPhoto(brand, viewerId, member);
            return photo;
        }

        internal static object GetPhotos(Brand brand, Member member, int viewerId)
        {
            if (!MemberHelper.IsPhotosAvailable(viewerId, member, brand)) return null;
            var photos = PhotoAccess.GetPhotos(brand, member.MemberID, viewerId);
            return photos.Count <= 0 ? null : photos;
        }

        internal static object GetPhotoUrl(Brand brand, Member member, int viewerId)
        {
            var photo = (Photo)GetDefaultPhoto(brand, member, viewerId);
            return (photo == null) ? null : photo.FullPath;
        }

        internal static object GetThumbnailUrl(Brand brand, Member member, int viewerId)
            // todo: optimize to cache photos to share with GetPhotoUrl?
        {
            var photo = (Photo)GetDefaultPhoto(brand, member, viewerId);
            return (photo == null) ? null : photo.ThumbPath;
        }

        internal static object GetAge(Brand brand, Member member, int viewerId)
        {
            return AttributeHelper.GetAge(member.GetAttributeDate(brand, "Birthdate"));
        }

        internal static object GetLocation(Brand brand, Member member, int viewerId)
        {
            return AttributeHelper.GetLocation(brand, member.GetAttributeInt(brand, "RegionID"), false);
        }

        internal static object GetLocationFull(Brand brand, Member member, int viewerId)
        {
            return AttributeHelper.GetLocation(brand, member.GetAttributeInt(brand, "RegionID"), true);
        }

        internal static object GetZipCode(Brand brand, Member member, int viewerId)
        {
            var region = RegionSA.Instance.RetrievePopulatedHierarchy(member.GetAttributeInt(brand, "RegionID"),
                brand.Site.LanguageID);
            var postalRegion = RegionSA.Instance.RetrieveRegionByID(region.PostalCodeRegionID, brand.Site.LanguageID);
            return postalRegion.Description;
        }

        internal static object GetCountry(Brand brand, Member member, int viewerId)
        {
            // todo: cache region to share with get zipcode?
            var region = RegionSA.Instance.RetrievePopulatedHierarchy(member.GetAttributeInt(brand, "RegionID"),
                brand.Site.LanguageID);
            var countryRegion = RegionSA.Instance.RetrieveRegionByID(region.CountryRegionID, brand.Site.LanguageID);

            return countryRegion.Description;
        }

        internal static object GetCountryAbbrev(Brand brand, Member member, int viewerId)
        {
            // todo: cache region to share with get zipcode?
            var region = RegionSA.Instance.RetrievePopulatedHierarchy(member.GetAttributeInt(brand, "RegionID"),
                brand.Site.LanguageID);
            var countryRegion = RegionSA.Instance.RetrieveRegionByID(region.CountryRegionID, brand.Site.LanguageID);

            return countryRegion.Abbreviation;
        }

        internal static object GetDMADescription(Brand brand, Member member, int viewerId)
        {
            //get dmacode, unfortunately, this doesn't yet return back description, but all of these data will be cached
            var dma = RegionSA.Instance.GetDMAByZipRegionID(member.GetAttributeInt(brand, "RegionID"));
            

            if (dma != null)
            {
                var fulldma = RegionSA.Instance.GetDMAByDMAID(dma.DMAID);
                if (fulldma != null)
                {
                    return fulldma.DMADescription;
                }
            }

            return null; 
        }

        internal static object GetEnvironment(Brand brand, Member member, int viewerId)
        {
            string env = System.Configuration.ConfigurationManager.AppSettings["Environment"];

            if (!string.IsNullOrEmpty(env))
            {
                env = env.Trim().ToLower();
                switch (env)
                {
                    case "dev":
                        return "Dev";
                    case "preprod":
                        return "PreProd";
                    case "prod":
                        return "Prod";
                }
            }

            return "Stage";
        }

        internal static object IsOnline(Brand brand, Member member, int viewerId)
        {
            var hideIsOnline = (member.GetAttributeInt(brand, "HideMask") &
                                (int) AttributeOptionHideMask.HideMembersOnline) == (int) AttributeOptionHideMask.HideMembersOnline;
            if (hideIsOnline) return false;

            var isMemberOnline = MembersOnlineSA.Instance.IsMemberOnline(brand.Site.Community.CommunityID, member.MemberID);
            return isMemberOnline;
        }

        internal static object IsPayingMember(Brand brand, Member member, int viewerId)
        {
            return member.IsPayingMember(brand.Site.SiteID);
        }

        internal static object GetPhotoCount(Brand brand, Member member, int viewerId)
        {
            var photos = PhotoAccess.GetPhotos(brand, member.MemberID, viewerId);
            return photos == null ? 0 : photos.Count;
        }

        internal static object IsViewersFavorite(Brand brand, Member member, int viewerId)
        {
            return viewerId > 0 && HotListAccess.MemberIsOnHotlist(brand, HotListCategory.Default, viewerId, member.MemberID);
        }

        internal static object GetPlanOnChildren(Brand brand, Member member, int viewerId)
        {
            var moreChildrenFlag = member.GetAttributeInt(brand, "MoreChildrenFlag");
            string planOnChildrenText;
            switch (moreChildrenFlag)
            {
                case 0:
                    planOnChildrenText = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID,
                        brand.Site.CultureInfo,
                        ResourceGroupEnum.Global, "NO");
                    break;
                case 1:
                    planOnChildrenText = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID,
                        brand.Site.CultureInfo,
                        ResourceGroupEnum.Global, "YES");
                    break;
                case 2:
                    planOnChildrenText = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID,
                        brand.Site.CultureInfo,
                        ResourceGroupEnum.Global, "NOT_SURE");
                    break;
                default: // including System.Int32.MinValue + 1 (not answered value)
                    planOnChildrenText = null;
                    break;
            }
            return planOnChildrenText;
        }

        internal static object GetApprovedPhotoCount(Brand brand, Member member, int viewerId)
        {
            var approvedPhotoCount = 0;
            var photos = member.GetPhotos(brand.Site.Community.CommunityID);
            if (photos == null) return approvedPhotoCount;
            for (byte i = 0; i < photos.Count; i++)
            {
                var photo = photos[i];
                if (photo != null && photo.IsApproved)
                {
                    approvedPhotoCount++;
                }
            }
            return approvedPhotoCount;
        }

        internal static object GetSubscriptionStatus(Brand brand, Member member, int viewerId)
        {
            var memberSubscriptionStatus = UnifiedPaymentSystemHelper.GetMemberSubcriptionStatus(member.MemberID, brand);
            var enumSubscriptionStatus = (SubscriptionStatus) memberSubscriptionStatus;
            var subscriptionStatus =
                UnifiedPaymentSystemHelper.Instance.ConvertSubscriptionStatusToString(enumSubscriptionStatus, true);
            return subscriptionStatus;
        }

        internal static object GetSubscriptionStatusTealium(Brand brand, Member member, int viewerId)
        {
            var subStatusID = UnifiedPaymentSystemHelper.GetMemberSubcriptionStatus(member.MemberID, brand);
            return subStatusID >= 0 ? subStatusID : 0;
        }

        internal static object GetSubscriptionStatusGam(Brand brand, Member member, int viewerId)
        {
            return UnifiedPaymentSystemHelper.Instance.GetSubscriptionStatusGam(brand, member);
        }

        internal static object GetAdminSuspendedFlag(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.IsMemberAdminSuspended(brand, member);
        }

        internal static object GetBlockContactByTarget(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.IsRequestedMemberBlocked(brand, viewerId, member.MemberID, HotListCategory.IgnoreList);
        }

        internal static object GetBlockSearchByTarget(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.IsRequestedMemberBlocked(brand, viewerId, member.MemberID,
                HotListCategory.ExcludeList);
        }

        internal static object GetRemoveFromSearchByTarget(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.IsRequestedMemberBlocked(brand, viewerId, member.MemberID,
                HotListCategory.ExcludeFromList);
        }

        internal static object GetBlockContact(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.IsRequestedMemberBlocked(brand, member.MemberID, viewerId, HotListCategory.IgnoreList);
        }

        internal static object GetBlockSearch(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.IsRequestedMemberBlocked(brand, member.MemberID, viewerId,
                HotListCategory.ExcludeList);
        }

        internal static object GetRemoveFromSearch(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.IsRequestedMemberBlocked(brand, member.MemberID, viewerId,
                HotListCategory.ExcludeFromList);
        }

        internal static object IsHighlighted(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.IsHighlighted(brand, member);
        }

        internal static object IsSpotlighted(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.IsSpotlighted(brand, member);
        }

        internal static object GetMatchRating(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.GetMatchRating(brand, viewerId, member.MemberID);
        }

        internal static object GetBirthdate(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.GetBirthdate(brand, member, viewerId, true);
        }

        internal static object IsIAPPayingMember(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.IsMobileAppPayingMember(brand, member, PurchaseAccess.IOS_IAP_RECEIPT_ORIG_TRANS_ID_ATTR);
        }

        internal static object IsIABPayingMember(Brand brand, Member member, int viewerId)
        {
            return MemberHelper.IsMobileAppPayingMember(brand, member, PurchaseAccess.ANDROID_IAB_SUBSCRIPTION_TOKENID_ATTR);
        }

        internal static object CanBuyUpgradeSubscription(Brand brand, Member member, int viewerId)
        {
            bool canUpgrade = false;

            if (!MemberHelper.IsMobileAppPayingMember(brand, member, PurchaseAccess.IOS_IAP_RECEIPT_ORIG_TRANS_ID_ATTR)
                && !MemberHelper.IsMobileAppPayingMember(brand, member, PurchaseAccess.ANDROID_IAB_SUBSCRIPTION_TOKENID_ATTR))
            {
                if (member.IsPayingMember(brand.Site.SiteID))
                {
                    if (!(PremiumHelper.HasAnyPremiumServices(brand, member)))
                    {
                        canUpgrade = true;
                    }
                }
                else
                {
                    //if not a subscriber, there is no upsale, they need to buy something with basic subscription first
                    canUpgrade = false;
                }
            }

            return canUpgrade;
        }

        internal static object CanBuyPremiumService(Brand brand, Member member, int viewerId)
        {
            if (member.IsPayingMember(brand.Site.SiteID))
            {
                return PremiumHelper.HasPremiumServiceLeftToPurchase(brand, member);
            }

            //not a subscriber, need basic subscription before buying premium services
            return false;
        }

        internal static object GetFirstName(Brand brand, Member member, int viewerId)
        {
            if (member.MemberID == viewerId)
            {
                return member.GetAttributeText(brand, "SiteFirstName");
            }

            return string.Empty;
        }

        internal static object GetLastName(Brand brand, Member member, int viewerId)
        {
            if (member.MemberID == viewerId)
            {
                return member.GetAttributeText(brand, "SiteLastName");
            }

            return string.Empty;
        }

        #region Nested type: PseudoAttributeDelegate

        //Maybe a better way to pass params would be using a dictionary with a paramstype enum instead of an object[] and use a factory approach
        //internal delegate object PseudoAttributeDelegate (Brand brand, Member member, params object[] args); 
        //vs
        //internal delegate object PseudoAttributeDelegate (Brand brand, Member member, Dictionary<AttributeSetType, object>);

        internal delegate object PseudoAttributeDelegate (Brand brand, Member member, int viewerId);


        #endregion
    }
}