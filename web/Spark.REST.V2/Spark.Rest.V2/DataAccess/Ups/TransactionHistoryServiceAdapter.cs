﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestSharp;
using RestSharp.Extensions;
using Spark.Common.Adapter;
using Spark.Common.AuthorizationService;
using Spark.Common.CatalogService;
using Spark.Common.DiscountService;
using Spark.Common.PurchaseService;
using Spark.Rest.V2.Helpers;
using Brand = Matchnet.Content.ValueObjects.BrandConfig.Brand;
using TransactionType = Spark.Common.PurchaseService.TransactionType;

namespace Spark.Rest.V2.DataAccess.Ups
{
    public class TransactionHistoryServiceAdapter
    {
        private readonly static TransactionHistoryServiceAdapter _instance = new TransactionHistoryServiceAdapter();

        public static TransactionHistoryServiceAdapter Instance
        {
            get { return _instance; }
        }

        #region Access Service Settings
        public static string GetOrderHistoryServiceUrl(Brand brand)
        {
            //return "http://lastgsvc303.sparkstage.com:2789/OrderHistoryJSONService.svc";
            try
            {
                return RuntimeSettings.Instance.GetSettingFromSingleton(
                    "UPS_ORDERHISTORY_JSON_SERVICE_URL", 
                    brand.Site.Community.CommunityID, 
                    brand.Site.SiteID, 
                    brand.BrandID);
            }
            catch (Exception ex)
            {
                //probably missing setting
                return "http://172.16.100.171:2789/OrderHistoryJSONService.svc";
            }
        }
        #endregion

        int GetFreeTrialPaymentType(TransactionHistoryItem historyItem)
        {
            if (historyItem.FreeTrialTransaction != null &&
                historyItem.FreeTrialTransaction.PaymentProfile != null)
            {
                Matchnet.Purchase.ValueObjects.PaymentType paymentType;
                if(PaymentType.TryParse(historyItem.FreeTrialTransaction.PaymentProfile.PaymentType, out paymentType))
                    return (int) paymentType;
                return 0;
            }
            return 0;
        }

        string GetLastFourAccountNumber(TransactionHistoryItem historyItem)
        {
            if (historyItem.FreeTrialTransaction != null &&
                historyItem.FreeTrialTransaction.PaymentProfile != null)
                return historyItem.FreeTrialTransaction.PaymentProfile.LastFourDigitsCreditCardNumber;
            return null;            
        }

        private void AddAccountHistoryItemForOrderInfo(
            List<TransactionHistoryItemPlain> items,
            TransactionHistoryItem historyItem, 
            Brand brand, CatalogServiceClient catalogServiceProxy)
        {
            if (historyItem.OrderInfo == null)
                return;

            //var package = catalogServiceProxy.GetPackageDetails(historyItem.OrderInfo.PrimaryPackageID);
            
            var plan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(historyItem.OrderInfo.PrimaryPackageID,
                    brand.BrandID);
                

            var promo =
                Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoByID(
                    historyItem.OrderInfo.PromoID,
                    brand.BrandID, false);

            var item = new TransactionHistoryItemPlain();

            item.CurrencyType = historyItem.OrderInfo.CurrencyID;
            item.ConfirmationNumber = historyItem.OrderInfo.OrderID;
            item.InitialCost = historyItem.OrderInfo.TotalAmount;
            item.InitialDuration = historyItem.OrderInfo.Duration;
            item.InitialDurationType = historyItem.OrderInfo.DurationTypeID;
            item.Date = historyItem.InsertDateInUTC;
            item.Status = historyItem.OrderInfo.OrderStatusID;
            item.PaymentType = historyItem.OrderInfo.PaymentType;
            item.LastFourDigitsAccount = historyItem.OrderInfo.LastFourAccountNumber;
            item.TransactionItemType = historyItem.TransactionHistoryType;
            item.PackageId = historyItem.OrderInfo.PrimaryPackageID;

            if (historyItem.OrderInfo.OrderUserPayment != null)
            {
                foreach (var orderUserPayment in historyItem.OrderInfo.OrderUserPayment)
                {
                    switch (orderUserPayment.ChargeTypeID)
                    {
                        case 1:
                            item.ChargeId = orderUserPayment.ChargeID;
                            break;
                        case 2:
                            item.AuthorizationId = orderUserPayment.ChargeID;
                            break;
                    }
                }
            }

            if (historyItem.OrderInfo.OrderDetail != null)
            {
                var orderDetail = historyItem.OrderInfo.OrderDetail.FirstOrDefault( x=> x.OrderTypeID >= 0);
                if (orderDetail != null)
                {
                    item.TransactionType = orderDetail.OrderTypeID;
                    item.Description = orderDetail.ItemDescription;
                }

            }

            if (plan != null)
            {
                item.RenewalRate = plan.RenewCost;
                item.RenewalDuration = plan.RenewDuration;
                item.RenewalDurationType = (int) plan.RenewDurationType;
            }

            if (promo != null)
            {
                item.PromoId = promo.PromoID;
                item.PromoDescription = promo.Description;
                item.PromoPageTitle = promo.PageTitle;
            }

            lock (items)
            {

                items.Add(item);

            }
        }
        private void AddAccountHistoryItemForFreeTrial(
            List<TransactionHistoryItemPlain> items,
            TransactionHistoryItem historyItem,
            Brand brand, CatalogServiceClient catalogServiceProxy)
        {

            if (historyItem.FreeTrialTransaction == null || historyItem.FreeTrialTransaction.AuthorizationSubscription == null)
                return;

            var primaryPackageId = historyItem.FreeTrialTransaction.AuthorizationSubscription.PrimaryPackageID;

            var package = catalogServiceProxy.GetPackageDetails(primaryPackageId);
            
            var plan = (historyItem.OrderInfo != null)
                ? Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(primaryPackageId,
                    brand.BrandID)
                : null;


            var promo =
                Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoByID(
                    historyItem.FreeTrialTransaction.AuthorizationSubscription.PromoID,
                    brand.BrandID, false);

            var item = new TransactionHistoryItemPlain();

            item.Date = historyItem.InsertDateInUTC;
            item.PackageId = primaryPackageId;
            item.AuthorizationId = historyItem.FreeTrialTransaction.AuthorizationSubscription.AuthorizationChargeID;
            item.PaymentType = this.GetFreeTrialPaymentType(historyItem);
            item.LastFourDigitsAccount = this.GetLastFourAccountNumber(historyItem);
            item.TransactionItemType = historyItem.TransactionHistoryType;
            
            if (package != null && package.Items != null)
            {

                var packageItem = package.Items.FirstOrDefault();
                if (packageItem != null)
                {
                    item.RenewalRate = packageItem.RenewalAmount;
                    item.RenewalDuration = packageItem.RenewalDuration;
                    item.RenewalDurationType = (int)packageItem.RenewalDurationType;
                }
            }
            
            if (promo != null)
            {
                item.PromoId = promo.PromoID;
                item.PromoDescription = promo.Description;
                item.PromoPageTitle = promo.PageTitle;
            }

            if (historyItem.FreeTrialTransaction.AuthorizationTransaction != null)
            {
                item.Status = historyItem.FreeTrialTransaction.AuthorizationTransaction.CaptureStatusID;
                
            }

            if (historyItem.FreeTrialTransaction.AccessTransaction != null)
            {
                item.ConfirmationNumber = historyItem.FreeTrialTransaction.AccessTransaction.AccessTransactionID;
            }

            lock (items)
            {

                items.Add(item);

            }
        }

        Gift GetGift(int accessTransactionId)
        {
            try
            {
                var gift =
                    Spark.Common.Adapter.DiscountServiceWebAdapter.GetProxyInstanceForBedrock()
                        .GetGiftByAccessTransactionID(accessTransactionId);
                return gift;

            }
            catch (Exception ex)
            {
                // TODO: log
                return null;
            }
            
        }

        private void AddAccountHistoryItemForAccessOnly(
            List<TransactionHistoryItemPlain> items,
            TransactionHistoryItem historyItem,
            Brand brand, CatalogServiceClient catalogServiceProxy)

        {
            if (historyItem.AccessTransaction == null)
                return;

            var item = new TransactionHistoryItemPlain();

            item.Date = historyItem.InsertDateInUTC;
            item.ConfirmationNumber = historyItem.AccessTransaction.AccessTransactionID;
            item.TransactionType = historyItem.AccessTransaction.UnifiedTransactionTypeID;
            // item.InitialCost; // is 0

            if (historyItem.AccessTransaction.UnifiedTransactionTypeID ==
                (int) Spark.Common.AccessService.TransactionType.GiftRedeem)
            {
                var gift = GetGift(historyItem.AccessTransaction.AccessTransactionID);
                if (gift != null && gift.GiftRedemption != null)
                {
                    item.LastFourDigitsAccount = gift.GiftRedemption.LastFourAccountNumber;
                    // NOT USED: item.CallingSystemId = gift.GiftRedemption.CallingSystemID;
                    item.PaymentType = (int) gift.GiftRedemption.PaymentType;
                    item.PromoId = gift.GiftRedemption.PromoID;
                    item.PackageId = gift.GiftRedemption.PackageID;
                }
            }
            else
            {
                item.PromoId = Constants.NULL_INT;
                item.PackageId = Constants.NULL_INT;
            }

            item.Status = (int) OrderStatusType.Successful;

            if (historyItem.AccessTransaction.AccessTransactionDetails != null)
            {
                var durationType = 0;
                var duration = 0;
                foreach (var detail in historyItem.AccessTransaction.AccessTransactionDetails)
                {
                    if (durationType == 0 && detail.Duration != Constants.NULL_INT)
                    {
                        duration = detail.Duration;
                        durationType = detail.DurationTypeID;
                        if (durationType != 0)
                            break;
                    }                        

                }

                item.InitialDuration = duration;
                item.InitialDurationType = durationType;
            }
            
            var promo =
              Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoByID(
                  item.PromoId,
                  brand.BrandID, false);

            if (promo != null)
            {
                item.PromoDescription = promo.Description;
                item.PromoPageTitle = promo.PageTitle;
            }

            item.TransactionItemType = historyItem.TransactionHistoryType;
            
            lock (items)
            {
                items.Add(item);
            }

        }

        private void AddAccountHistoryItemForRenewal(
            List<TransactionHistoryItemPlain> items,
            TransactionHistoryItem historyItem,
            List<TransactionHistoryItem> historyItems,
            Brand brand, CatalogServiceClient catalogServiceProxy)
        {

            if (historyItem.RenewalTransaction == null)
                return;

            var item = new TransactionHistoryItemPlain();

            var renewalTran = historyItem.RenewalTransaction;
            
            item.Date = historyItem.RenewalTransaction.InsertDateUTC;
            item.ConfirmationNumber = historyItem.RenewalTransaction.RenewalTransactionID;
            item.TransactionType = historyItem.RenewalTransaction.TransactionTypeID;
            item.InitialCost = 0.0m;
            //this._callingSystemID = renewalTran.CallingSystemID;
            item.LastFourDigitsAccount = "";
            item.PaymentType = 0;
            item.Status = historyItem.RenewalTransaction.RenewalStatusID;
            //this._adminID = renewalTran.AdminID;
            item.PromoId = Constants.NULL_INT;
            item.PackageId = Constants.NULL_INT;
            item.InitialDuration = 0; 
            item.InitialDurationType = 0;
            item.TransactionItemType = historyItem.TransactionHistoryType;
            //if (this._transactionType != TransactionType.FreezeSubscriptionAccount
            //   && this._transactionType != TransactionType.UnfreezeSubscriptionAccount)
            //{
            //    this._duration = 0;
            //    this._durationType = DurationType.Month;
            //}
            switch ((TransactionType) renewalTran.TransactionTypeID)
            {
                case TransactionType.AutoRenewalReopen:
                case TransactionType.AutoRenewalTerminate:
                    if (renewalTran.RenewalTransactionDetails != null && renewalTran.RenewalTransactionDetails.Count > 0)
                        item.PackageId = renewalTran.RenewalTransactionDetails[0].PackageID;
                    break;
                case TransactionType.AutoRenewalReopenALaCarte:
                case TransactionType.AutoRenewalTerminateALaCarte:
                    break;
                case TransactionType.FreezeSubscriptionAccount:
                {
                    //item.RenewalRate = -Math.Abs(renewalTran.DeferredAmount);
                    if (renewalTran.PrimaryPackageID > 0)
                    {
                        item.PackageId = renewalTran.PrimaryPackageID;
                    }

                    // Frozen time is saved in minutes  
                    decimal days = renewalTran.Duration/1440;
                    item.RenewalDuration = -Math.Abs(Convert.ToInt32(Math.Ceiling(days)));
                    item.RenewalDurationType = (int) Matchnet.DurationType.Day;

                    break;
                }
                case TransactionType.UnfreezeSubscriptionAccount:
                {
                    //item.RenewalRate = Math.Abs(renewalTran.DeferredAmount);
                    if (renewalTran.PrimaryPackageID > 0)
                    {
                        item.PackageId = renewalTran.PrimaryPackageID;
                    }

                    // Frozen time is saved in minutes  
                    decimal days = renewalTran.Duration/1440;
                    item.RenewalDuration = Math.Abs(Convert.ToInt32(Math.Ceiling(days)));
                    item.RenewalDurationType = (int) Matchnet.DurationType.Day;

                    break;
                }
            }
            if (item.PackageId > 0)
            {
                var plan = PlanSA.Instance.GetPlan(item.PackageId, brand.BrandID);

                if (plan != null)
                {
                    item.InitialCost = plan.InitialCost;
                    item.InitialDuration = plan.InitialDuration;
                    item.InitialDurationType = (int) plan.InitialDurationType;
                    item.RenewalRate = plan.RenewCost;
                    item.RenewalDuration = plan.RenewDuration;
                    item.RenewalDurationType = (int) plan.RenewDurationType;
                }
            }
            //item.TransactionType = orderDetail.OrderTypeID;
            var query = ( from mainItem in historyItems
                          where mainItem.OrderInfo != null 
                            && mainItem.OrderInfo.OrderDetail != null 
                            && mainItem.OrderInfo.OrderDetail.Any(x => x.OrderTypeID == (int)TransactionType.InitialSubscriptionPurchase)
                            && mainItem.OrderInfo.OrderStatusID == 2 /*Success*/
                            && mainItem.OrderInfo.PrimaryPackageID == item.PackageId
                          orderby mainItem.InsertDateInUTC descending 
                          select mainItem.OrderInfo.PromoID).Take(1);

            foreach (var promoId in query)
                item.PromoId = promoId;

            if (item.PromoId > 0)
            {
                var promo =
                    Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoByID(
                        item.PromoId,
                        brand.BrandID, false);

                if (promo != null)
                {
                    item.PromoDescription = promo.Description;
                    item.PromoPageTitle = promo.PageTitle;
                }
            }

            lock (items)
            {
                items.Add(item);
            }
           
        }

        public List<TransactionHistoryItemPlain> GetAccountHistory(Brand brand, int memberId, int pageSize, int pageNumber)
        {
            var res = GetAccountHistoryRaw(brand, memberId, pageSize, pageNumber);
            var ret = new List<TransactionHistoryItemPlain>();
            var catalogServiceProxy = CatalogServiceWebAdapter.GetProxyInstance();
            try
            {
                var ctx = System.Web.HttpContext.Current;
                Parallel.ForEach(res, ParallelTasksHelper.GetMaxDegreeOfParallelism(), (historyItem) =>
                {
                    //set HttpContext for thread
                    System.Web.HttpContext.Current = ctx;

                    try
                    {
                        switch (historyItem.TransactionHistoryType)
                        {
                            case 1:
                                AddAccountHistoryItemForOrderInfo(ret, historyItem, brand, catalogServiceProxy);
                                break;
                            case 2:
                                AddAccountHistoryItemForAccessOnly(ret, historyItem, brand, catalogServiceProxy);
                                break;
                            case 3:
                                AddAccountHistoryItemForRenewal(ret, historyItem, res, brand, catalogServiceProxy);
                                break;
                            case 4:
                                AddAccountHistoryItemForFreeTrial(ret, historyItem, brand, catalogServiceProxy);
                                break;
                        }
                    }
                    finally
                    {
                        //unset HttpContext for thread (precaution for GC)
                        System.Web.HttpContext.Current = null;                        
                    }
                    return;
                });
            }
            finally
            {
                Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();
            }
            ret.Sort((x, y) => -x.Date.CompareTo(y.Date));
            return ret;
        }

        List<TransactionHistoryItem> GetAccountHistoryRaw(Brand brand, int memberId, int pageSize, int pageNumber)
        {
            var restClient = new RestClient(GetOrderHistoryServiceUrl(brand));
            var restRequest = new RestRequest("GetMemberTransactionHistoryByMemberID", Method.GET);
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddParameter("CallingSystemID", brand.Site.SiteID, ParameterType.QueryString);
            restRequest.AddParameter("MemberID", memberId, ParameterType.QueryString);
            restRequest.AddParameter("PageNumber", pageNumber, ParameterType.QueryString);
            restRequest.AddParameter("PageSize", pageSize, ParameterType.QueryString);

            var response = restClient.Execute<TransactionHistoryWrapper>(restRequest);
            if (response.Data != null)
                return response.Data.AllTransactionHistoryItems;
            //if (response.Content != null)
            //{
            //    var responseObj = JsonConvert.DeserializeObject<TransactionHistoryWrapper>(response.Content,
            //        new JsonSerializerSettings
            //        {
            //            //Error = (object sender, ErrorEventArgs errorArgs) =>
            //            //    {
            //            //        var currentError = errorArgs.ErrorContext.Error.Message;
            //            //        errorArgs.ErrorContext.Handled = true;
            //            //    }
            //        });
            //    if(responseObj != null)
            //        return responseObj.AllTransactionHistoryItems;
            //}
            return null;
        }

        string GetAccountHistoryRaw2(Brand brand, int memberId, int pageSize, int pageNumber)
        {
            var restClient = new RestClient(GetOrderHistoryServiceUrl(brand));
            var restRequest = new RestRequest("GetMemberTransactionHistoryByMemberID", Method.GET);
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddParameter("CallingSystemID", brand.Site.SiteID, ParameterType.QueryString);
            restRequest.AddParameter("MemberID", memberId, ParameterType.QueryString);
            restRequest.AddParameter("PageNumber", pageNumber, ParameterType.QueryString);
            restRequest.AddParameter("PageSize", pageSize, ParameterType.QueryString);

            var response = restClient.Execute(restRequest);

            return response.Content;
        }



    }
}