﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.RabbitMQ.Client;

namespace Spark.Rest.V2.DataAccess.Ups
{
    public class TransactionHistoryItemPlain
    {
        /// <summary>
        /// Billing Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// String representation of time when the transaction was created, in UTC
        /// </summary>
        public string Date { get; set; }
        /// <summary>
        /// Transaction Type:
        ///     None = 0,
        ///     OrderInfo = 1,
        ///     AccessTransaction = 2,
        ///     RenewalTransaction = 3,
        ///     FreeTrialTransaction = 4
        /// </summary>
        public int TransactionItemType { get; set; }

        /// <summary>
        ///     None = 0,
        ///     InitialSubscriptionPurchase = 1,
        ///     Renewal = 2,
        ///     Credit = 3,
        ///     Void = 4,
        ///     AutoRenewalTerminate = 5,
        ///     Authorization = 6,
        ///     PaymentProfileAuthorization = 10,
        ///     PlanChange = 11,
        ///     TrialPayAdjustment = 12,
        ///     AutoRenewalReopen = 18,
        ///     AdditionalSubscriptionPurchase = 23,     upgrade before
        ///     AdditionalNonSubscriptionPurchase = 24,  downgrade before
        ///     VirtualTerminalPurchase = 34,
        ///     VirtualTerminalCredit = 35,
        ///     VirtualTerminalVoid = 36,
        ///     BatchRenewal = 37,
        ///     LifetimeMemberAdjustment = 38,
        ///     AutoRenewalTerminateALaCarte = 39,
        ///     AutoRenewalReopenALaCarte = 40,
        ///     FreezeSubscriptionAccount = 46,
        ///     UnfreezeSubscriptionAccount = 47,
        ///     ManualPayment = 48,		
        ///     TrialTakenInitialSubscription = 1001,
        ///     ChargeBack = 1004,
        ///     CheckReversal = 1005,
        ///     Adjustment = 1007,
        ///     GiftPurchase = 1013,
        ///     GiftRedeem = 1014,
        ///     RenewalRecyclingTimeAdjustment = 1015
        /// </summary>
        public int TransactionType { get; set; }

        /// <summary>
        /// Initial cost of the subscription
        /// </summary>
        public decimal InitialCost { get; set; }
        /// <summary>
        /// Initial duration of the subscription
        /// </summary>
        public int InitialDuration { get; set; }
        /// <summary>
        /// Initial duration type of the subscription:
        ///     None = 0,
        ///     Minutes = 1,
        ///     Hour = 2,
        ///     Day = 3,
        ///     Week = 4,
        ///     Month = 5,
        ///     Year = 6
        /// </summary>
        public int InitialDurationType { get; set; }
        /// <summary>
        /// Cost to renew subscription
        /// </summary>
        public decimal RenewalRate { get; set; }
        /// <summary>
        /// Duration of renewal
        /// </summary>
        public int RenewalDuration { get; set; }
        /// <summary>
        /// Duration type of renewal:
        ///     None = 0,
        ///     Minutes = 1,
        ///     Hour = 2,
        ///     Day = 3,
        ///     Week = 4,
        ///     Month = 5,
        ///     Year = 6
        /// </summary>
        public int RenewalDurationType { get; set; }
        /// <summary>
        /// Localization of the currency
        ///     None = 0,
        ///     USD = 1,
        ///     EUR = 2,
        ///     CAD = 3,
        ///     GBP = 4,
        ///     AUD = 5,
        ///     ILS = 6,
        ///     VerifiedByVisa = 7 
        /// </summary>
        public int CurrencyType { get; set; }
        /// <summary>
        /// Package ID, Aka Subscription ID, Plan ID
        /// </summary>
        public int PackageId { get; set; }
        /// <summary>
        /// Promotion ID
        /// </summary>
        public int PromoId { get; set; }
        /// <summary>
        /// Description of the promotion
        /// </summary>
        public string PromoDescription { get; set; }
        /// <summary>
        /// Promo Page Title
        /// </summary>
        public string PromoPageTitle { get; set; }
        /// <summary>
        /// Billing Outcome
        ///     None = 0,
        ///     Pending = 1,
        ///     Successful = 2,
        ///     Failed = 3
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        ///     None = 0,
        ///     CreditCard = 1,
        ///     Check = 2,
        ///     DirectDebit = 4,
        ///     PayPalLitle = 8,
        ///     SMS = 16,
        ///     PayPalDirect = 32,
        ///     Manual = 64,
        ///     ElectronicFundsTransfer = 128,
        ///     PaymentReceived = 256,
        ///     DebitCard = 512,
        ///     InApplicationPurchase = 1024
        /// </summary>
        public int PaymentType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LastFourDigitsAccount { get; set; }
        /// <summary>
        /// Order ID, Confirmation
        /// </summary>
        public long ConfirmationNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AuthorizationId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ChargeId { get; set; }
    }
}