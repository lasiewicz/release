﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.DataAccess.Ups
{
    public class TransactionHistoryWrapper
    {
        public List<TransactionHistoryItem> AllTransactionHistoryItems { get; set; }
    }
    public class TransactionHistoryItem
    {
        public string InsertDateInUTC { get; set; } // => Date
        public int TransactionHistoryType { get; set; } //  => TransactionItemType
        public OrderInfo OrderInfo { get; set; }
        public FreeTrialTransaction FreeTrialTransaction { get; set; }
        public AccessTransaction AccessTransaction { get; set; }
        public RenewalTransaction RenewalTransaction { get; set; }
    }

    public class RenewalTransaction
    {
        public long RenewalTransactionID { get; set; }
        public int TransactionTypeID { get; set; }
        public string InsertDateUTC { get; set; }
        public int RenewalStatusID { get; set; }
        public List<RenewalTransactionDetails> RenewalTransactionDetails { get; set; }
        public string DeferredAmount { get; set; }
        public int PrimaryPackageID { get; set; }
        public int Duration { get; set; }

    }

    public class RenewalTransactionDetails
    {
        public int PackageID { get; set; }
        public long RenewalTransactionID { get; set; }
    }

    public class FreeTrialTransaction
    {
        public AuthorizationSubscription AuthorizationSubscription { get; set; }
        public AuthorizationTransaction AuthorizationTransaction { get; set; }
        public AccessTransaction AccessTransaction { get; set; }
        public PaymentProfile PaymentProfile { get; set; }

    }
    public class OrderUserPayment
    {
        public int OrderID { get; set; } // => AuthID
        public int ChargeID { get; set; } // => ChargeID
        public int ChargeTypeID { get; set; } // helper field
    }

    public class AccessTransaction
    {
        public int AccessTransactionID { get; set; } // => Confirmation in free trial and 
        
        // Below - Only for Access Transaction (2)
        public int UnifiedTransactionTypeID { get; set; } // => Transaction Type


        public List<AccessTransactionDetails> AccessTransactionDetails { get; set; }


    }

    public class AccessTransactionDetails
    {
        public int Duration { get; set; }
        public int DurationTypeID { get; set; }
        public int UnifiedPrivilegeTypeID { get; set; }
        public int Count { get; set; }
    }

    public class AuthorizationTransaction
    {
        public int CaptureStatusID { get; set; } // => Status (description)
    }
    public class AuthorizationSubscription
    {
        
        public int PrimaryPackageID { get; set; } // => Used for Renewal data
        public int PromoID { get; set; }
        public int AuthorizationChargeID { get; set; }
    }

    public enum OrderStatusType
    {
        None = 0,
        Pending = 1,
        Successful = 2,
        Failed = 3
    }

    public enum TransactionHistoryType
    {
        None = 0,
        OrderInfo = 1,
        AccessTransaction = 2,
        RenewalTransaction = 3,
        FreeTrialTransaction = 4
    }

    public enum PaymentType
    {
        None = 0,
        CreditCard = 1,
        Check = 2,
        DirectDebit = 4,
        PayPalLitle = 8,
        SMS = 16,
        PayPalDirect = 32,
        Manual = 64,
        ElectronicFundsTransfer = 128,
        PaymentReceived = 256,
        DebitCard = 512,
        InApplicationPurchase = 1024
    }
    // => fileter by OrderInfo || FreeTrialTransaciton


    public class PaymentProfile
    {
        public string PaymentType { get; set; } // => PaymentType
        public string LastFourDigitsCreditCardNumber { get; set; }
    }

    public class OrderInfo
    {
        public int OrderID { get; set; } // => Confirmation Number
        
        public decimal TotalAmount { get; set; } // => InitialCost
        public int Duration { get; set; } // => InitialDuration
        public int DurationTypeID { get; set; } // => InitialDurationType
        public int CurrencyID { get; set; }
        public List<OrderDetail> OrderDetail { get; set; }
        public List<OrderUserPayment> OrderUserPayment { get; set; }
        public int PrimaryPackageID { get; set; }// => Used for Renewal data
        public int PromoID { get; set; } // => PromoId
        public int OrderStatusID { get; set; } // => Status (description)
        public int PaymentType { get; set; } // => PaymentType
        public string LastFourAccountNumber { get; set; }
    }

    public enum DurationType
    {
        None = 0,
        Minutes = 1,
        Hour = 2,
        Day = 3,
        Week = 4,
        Month = 5,
        Year = 6
    }
    public class OrderDetail
    {
        public string ItemDescription { get; set; } // => Description
        public int CurrencyID { get; set; } // CurrencyType
        public int OrderTypeID { get; set; } // Transaction Type
    }
    public enum CurrencyType
    {
        None = 0,
        USD = 1,
        EUR = 2,
        CAD = 3,
        GBP = 4,
        AUD = 5,
        ILS = 6,
        VerifiedByVisa = 7 //??
    }

}