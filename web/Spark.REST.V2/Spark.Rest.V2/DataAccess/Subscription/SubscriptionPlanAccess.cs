﻿using System;
using System.Collections.Generic;
using System.Linq;
using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Spark.Common.AccessService;
using Spark.Common.Adapter;
using Spark.Common.AuthorizationService;
using Spark.Common.PaymentProfileService;
using Spark.Common.RenewalService;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.REST.DataAccess.Subscription;
using Spark.Rest.V2.Models.Subscription;
using AuthorizationStatus = Spark.Common.AccessService.AuthorizationStatus;
using Brand = Matchnet.Content.ValueObjects.BrandConfig.Brand;
using TransactionType = Spark.Common.AccessService.TransactionType;
using Spark.Rest.DataAccess.Purchase;

namespace Spark.Rest.V2.DataAccess.Subscription
{
    ///  <summary>
    ///     This class represents the subscription plan services that can be updated and fetched.
    ///     TODO: test with a CI script that registers a new member and then changes their subscription options.
	// another test comment
    /// </summary>
    public class SubscriptionPlanAccess
    {
        private const int CallingSystemId = 1;
        private readonly RenewalSubscription _baseSubscription;
        private readonly Brand _brand;
        private readonly AuthorizationSubscription _freeTrialSubscription;
        private readonly int _memberId;
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(UnifiedPaymentSystemHelper));
        private ISparkServiceClientFactory _serviceClientFactory;

        private readonly PremiumType[] _validTypes =
        {
            PremiumType.HighlightedProfile,
            PremiumType.SpotlightMember,
            PremiumType.JMeter,
            PremiumType.AllAccess,
            PremiumType.ReadReceipt
        };

        public ISparkServiceClientFactory ServiceClientFactory
        {
            get { return _serviceClientFactory ?? (_serviceClientFactory = SparkServiceClientFactory.Singleton); }
            set { _serviceClientFactory = value; }
        }

        /// <summary>
        ///     Create a new Subscription Plan.
        /// </summary>
        /// <param name="memberId">The current member Id.</param>
        /// <param name="brand">The current brand that the member logged in to.</param>
        public SubscriptionPlanAccess(int memberId, Brand brand)
        {
            _memberId = memberId;
            _brand = brand;
            _baseSubscription = GetBaseSubscription();
            // free trial subscription is *not* the same as other subscriptions
            _freeTrialSubscription = GetFreeTrialSubscription();
        }

        #region Manage subscriptions

        /// <summary>
        ///     Update the member's subscription plan.
        /// </summary>
        /// <param name="request">The change request.</param>
        /// <returns>An enumeration of the changes made per change request.</returns>
        public IEnumerable<SubscriptionPlanChangeConfirmation> Update(SubscriptionPlanChangeRequest request)
        {
            var result = new List<SubscriptionPlanChangeConfirmation>();

            try
            {
                // member wants to terminate. for Free Trial use DisableFreeTrial
                if (!request.BaseSubscriptionPlan.IsEnabled && _baseSubscription.IsRenewalEnabled)
                {
                    int confirmationId;
                    var renewalResponse = Terminate(request.TerminationReasonId, out confirmationId);

                    // check if the transaction was successful
                    if (renewalResponse.InternalResponse.responsecode == "0")
                    {
                        if (request.IsSelfSuspended)
                        {
                            var member = MemberSA.Instance.GetMember(_memberId, MemberLoadFlags.None);
                            // suspend the account
                            member.SetAttributeInt(_brand, "SelfSuspendedFlag", 1);
                            MemberSA.Instance.SaveMember(member);

                            // remove from online members currently showing up
                            MembersOnlineSA.Instance.Remove(_brand.Site.Community.CommunityID, _memberId);
                        }
                    }
                    else
                    {
                        throw new Exception(
                            string.Format(
                                "Failed to update with Unified Renewal to disable auto-renewal for MemberID: {0}, please try again or contact Software Engineering.",
                                _memberId));
                    }

                    // add response to result set
                    result.Add(new SubscriptionPlanChangeConfirmation
                    {
                        RenewalSubscriptionId = confirmationId,
                        RenewalResponse = renewalResponse,
                        PremiumType = PremiumType.None
                    });
                }
                else
                {
                    // process the main subscription first
                    if (request.BaseSubscriptionPlan.IsEnabled && !_baseSubscription.IsRenewalEnabled)
                    {
                        result.Add(new SubscriptionPlanChangeConfirmation
                        {
                            RenewalSubscriptionId = _baseSubscription.RenewalSubscriptionID,
                            RenewalResponse = EnableBaseSubscription(),
                            PremiumType = PremiumType.None
                        });
                    }

                    // if the main subscription is enabled, process premium features
                    if (request.PremiumOptions == null || !request.PremiumOptions.Any()) return result;
                    foreach (var renewalSubscriptionDetail in _baseSubscription.RenewalSubscriptionDetails)
                    {
                        //skip if renewal detail is the same as base package
                        if (_baseSubscription.PrimaryPackageID == renewalSubscriptionDetail.PackageID)
                            continue;

                        //a la carte
                        var package = GetPackage(renewalSubscriptionDetail.PackageID);

                        // check if this subscription detail contains the premium feature
                        if (package != null && package.Items != null)
                        {
                            bool packageUpdated = false;
                            foreach (var premiumFeature in request.PremiumOptions)
                            {
                                foreach (Spark.Common.CatalogService.Item item in package.Items)
                                {
                                    if (item != null)
                                    {
                                        List<PremiumType> premiumTypes = GetPremiumOptionType(ConvertCatalogToAccessPrivilege(item.PrivilegeType).ToArray());

                                        if (premiumTypes != null)
                                        {
                                            foreach (PremiumType premiumType in premiumTypes)
                                            {
                                                if (premiumFeature.PremiumSubscriptionType == premiumType)
                                                {
                                                    //package matches premium type, let's update this renewal a la carte package
                                                    var renewalResponse = UpdatePremiumOption(premiumFeature, renewalSubscriptionDetail);

                                                    // add each response to result set
                                                    if (renewalResponse != null)
                                                    {
                                                        result.Add(new SubscriptionPlanChangeConfirmation
                                                        {
                                                            RenewalSubscriptionId = renewalSubscriptionDetail.RenewalSubscriptionID,
                                                            RenewalResponse = renewalResponse,
                                                            PremiumType = premiumFeature.PremiumSubscriptionType
                                                        });
                                                    }

                                                    packageUpdated = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (packageUpdated)
                                        break;
                                }

                                if (packageUpdated)
                                    break;
                            }
                        }

                    }
                }
            }
            finally
            {
                ServiceClientFactory.CloseRenewalServiceClient();
                ServiceClientFactory.CloseCatalogServiceClient();
            }

            return result;
        }

        /// <summary>
        ///     Get a subscription plan by the package id.
        /// </summary>
        /// <param name="packageId">The package Id</param>
        /// <returns>Tsubscription plan by the package id.</returns>
        private Plan GetSubscriptionPlanByPackageId(int packageId)
        {
            return PlanSA.Instance.GetPlan(packageId, _brand.BrandID);
        }

        /// <summary>
        ///     Enable the main subscription.
        /// </summary>
        /// <returns>The renewal response.</returns>
        private RenewalResponse EnableBaseSubscription()
        {
            RenewalResponse result = ServiceClientFactory.GetRenewalServiceClient().EnableAutoRenewal(
                    new int[] {},
                    _memberId,
                    _brand.Site.SiteID,
                    CallingSystemId,
                    Constants.NULL_INT,
                    string.Empty,
                    Constants.NULL_INT);

            if (result.InternalResponse.responsecode != "0")
            {
                throw new Exception(string.Format("Error renewing base subscription: {0}", result));
            }

            // update the AutoRenewalStatus, so the Reactivate Link behaves correctly if it's enabled
            // for the site.
            Member member = MemberSA.Instance.GetMember(_memberId, MemberLoadFlags.None);
            member.SetAttributeInt(_brand, "AutoRenewalStatus", 1);

            // undo the self suspend
            member.SetAttributeInt(_brand, "SelfSuspendedReasonType", 0);
            member.SetAttributeInt(_brand, "SelfSuspendedFlag", 0);

            // commit the changes
            MemberSA.Instance.SaveMember(member);

            return result;
        }

        /// <summary>
        ///     Update a premium option's subscription.
        /// </summary>
        /// <param name="premiumFeature">The premium option details.</param>
        /// <param name="premiumSubscriptionDetail">The premium option subscription.</param>
        /// <returns>The update transaction response status code.</returns>
        private RenewalResponse UpdatePremiumOption(SubscriptionPremiumOption premiumFeature,
            RenewalSubscriptionDetail premiumSubscriptionDetail)
        {
            RenewalResponse renewalResponse = null;

            // if this feature is enabled and not yet subscribed to
            if (premiumFeature.IsEnabled && !premiumSubscriptionDetail.IsRenewalEnabled)
            {
                renewalResponse = ServiceClientFactory.GetRenewalServiceClient().EnableALaCarteAutoRenewal(
                            new[] {premiumSubscriptionDetail.PackageID},
                            _memberId,
                            _brand.Site.SiteID,
                            CallingSystemId,
                            Constants.NULL_INT,
                            string.Empty,
                            Constants.NULL_INT);
            }
                // if this feature is disabled and subscribed to
            else if (!premiumFeature.IsEnabled && premiumSubscriptionDetail.IsRenewalEnabled)
            {
                renewalResponse = ServiceClientFactory.GetRenewalServiceClient().DisableALaCarteAutoRenewal(
                            new[] {premiumSubscriptionDetail.PackageID},
                            _memberId,
                            _brand.Site.SiteID,
                            CallingSystemId,
                            Constants.NULL_INT,
                            string.Empty,
                            Constants.NULL_INT);
            }

            return renewalResponse;
        }

        /// <summary>
        ///     Get the member's current subscription plan status.
        /// </summary>
        /// <returns>The member's current subscription plan status.</returns>
        public SubscriptionPlanStatus GetPlanStatus()
        {
            var member = MemberSA.Instance.GetMember(_memberId, MemberLoadFlags.None);

            var basePlan = GetBasePlan();
            var freeTrialPlan = GetFreeTrialPlan();

            // populate common properties
            var subscriptionPlanStatus = new SubscriptionPlanStatus
            {
                IsFreeTrial = IsInFreeTrial(),
                IsSelfSuspended = IsSelfSuspended(member)
            };

            var basicSubscriptionPrivilege = member.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription,
                _brand.BrandID, _brand.Site.SiteID, _brand.Site.Community.CommunityID);

            if (basicSubscriptionPrivilege == null || basicSubscriptionPrivilege.EndDatePST < DateTime.Now)
            {
                //could be cache issue after a purchase
                MemberSA.Instance.ExpireCachedMemberAccess(_memberId, true);
                member = MemberSA.Instance.GetMember(_memberId, MemberLoadFlags.None);

                basicSubscriptionPrivilege = member.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription,
                _brand.BrandID, _brand.Site.SiteID, _brand.Site.Community.CommunityID, MemberLoadFlags.IngoreSACache);
            }

            if (basicSubscriptionPrivilege != null)
            {
                subscriptionPlanStatus.SubscriptionEndDate = basicSubscriptionPrivilege.EndDateUTC;
                //JS-1329
                Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", member.MemberID, PrivilegeType.BasicSubscription.ToString(), basicSubscriptionPrivilege.EndDateUTC), ErrorHelper.GetCustomData());
            }

            // for regular or basic subscription
            if (basePlan != null)
            {
                subscriptionPlanStatus.PackageTypeId = GetPackageTypeIdFromBasePackage(_baseSubscription.PrimaryPackageID); ;
                subscriptionPlanStatus.PremiumOptions = GetPremiumOptions(basePlan).ToList();
                subscriptionPlanStatus.BaseSubscriptionPlan = new SubscriptionBase
                {
                    IsEnabled = _baseSubscription.IsRenewalEnabled,
                    CurrencyType = basePlan.CurrencyType,
                    RenewalDurationType = basePlan.RenewDurationType,
                    EndDate = basePlan.EndDate,
                    RenewalRate = basePlan.RenewCost,
                    RenewalDuration = basePlan.RenewDuration,
                    InitialCost = basePlan.InitialCost,
                    InitialDurationType = basePlan.InitialDurationType,
                    InitialDuration = basePlan.InitialDuration
                };
            }
            // for free trial subscription
            else if (freeTrialPlan != null)
            {
                subscriptionPlanStatus.PackageTypeId = GetPackageTypeIdFromFreeTrial(_freeTrialSubscription.PrimaryPackageID); ;
                subscriptionPlanStatus.FreeTrialSubscriptionPlan = new SubscriptionBase
                {
                    // this means it the free trial hasnâ€™t been cancelled by the user and is still active
                    IsEnabled = _freeTrialSubscription.IsCaptureEnabled,
                    CurrencyType = freeTrialPlan.CurrencyType,
                    RenewalRate = freeTrialPlan.RenewCost,
                    RenewalDuration = freeTrialPlan.RenewDuration,
                    InitialCost = freeTrialPlan.InitialCost,
                    InitialDurationType = freeTrialPlan.InitialDurationType,
                    InitialDuration = freeTrialPlan.InitialDuration
                };
            }
            return subscriptionPlanStatus;
        }

        /// <summary>
        ///     Get the member's current subscription plan status.
        /// </summary>
        /// <returns>The member's current subscription plan status.</returns>
        public SubscriptionPlanStatus GetPackageStatus()
        {
            try
            {
                var member = MemberSA.Instance.GetMember(_memberId, MemberLoadFlags.None);

                var basePackage = GetBasePackage();

                var freeTrialPackage = GetFreeTrialPackage();

                // populate common properties
                var subscriptionPlanStatus = new SubscriptionPlanStatus
                {
                    IsFreeTrial = IsInFreeTrial(),
                    IsSelfSuspended = IsSelfSuspended(member)
                };

                var basicSubscriptionPrivilege = member.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription,
                    _brand.BrandID, _brand.Site.SiteID, _brand.Site.Community.CommunityID);

                if (basicSubscriptionPrivilege == null || basicSubscriptionPrivilege.EndDatePST < DateTime.Now)
                {
                    //could be cache issue after a purchase
                    MemberSA.Instance.ExpireCachedMemberAccess(_memberId, true);
                    member = MemberSA.Instance.GetMember(_memberId, MemberLoadFlags.None);

                    basicSubscriptionPrivilege = member.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription,
                    _brand.BrandID, _brand.Site.SiteID, _brand.Site.Community.CommunityID, MemberLoadFlags.IngoreSACache);
                }

                if (basicSubscriptionPrivilege != null)
                {
                    subscriptionPlanStatus.SubscriptionEndDate = basicSubscriptionPrivilege.EndDateUTC;
                    //JS-1329
                    Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", member.MemberID, PrivilegeType.BasicSubscription.ToString(), basicSubscriptionPrivilege.EndDateUTC), ErrorHelper.GetCustomData());
                }

                // for regular or basic subscription
                Spark.Common.CatalogService.Item subscriptionItem = null;
                if (basePackage != null)
                {
                    subscriptionItem = GetSubscriptionItem(basePackage);
                    subscriptionPlanStatus.PackageTypeId = (int)basePackage.PackageType;
                    subscriptionPlanStatus.PremiumOptions = GetPremiumOptions(basePackage);
                    subscriptionPlanStatus.BaseSubscriptionPlan = new SubscriptionBase
                    {
                        IsEnabled = _baseSubscription.IsRenewalEnabled,
                        CurrencyType = (CurrencyType)Enum.Parse(typeof(CurrencyType), ((int)basePackage.CurrencyType).ToString()),
                        RenewalDurationType = (DurationType)Enum.Parse(typeof(DurationType), ((int)subscriptionItem.RenewalDurationType).ToString()),
                        EndDate = basePackage.ExpiredDate,
                        RenewalRate = subscriptionItem.RenewalAmount,
                        RenewalDuration = subscriptionItem.RenewalDuration,
                        InitialCost = subscriptionItem.Amount,
                        InitialDurationType = (DurationType)Enum.Parse(typeof(DurationType), ((int)subscriptionItem.DurationType).ToString()),
                        InitialDuration = subscriptionItem.Duration
                    };

                    //roll up bundled subscription renewal prices so the total is the same in the subscription and and premium options
                    decimal totalSubscriptionBundleRenewalRate = subscriptionPlanStatus.BaseSubscriptionPlan.RenewalRate;
                    if (subscriptionPlanStatus.PremiumOptions != null)
                    {
                        foreach (SubscriptionPremiumOption spo in subscriptionPlanStatus.PremiumOptions)
                        {
                            if (spo.IsBase)
                            {
                                totalSubscriptionBundleRenewalRate += spo.RenewalRate;
                            }
                        }

                        if (totalSubscriptionBundleRenewalRate != subscriptionPlanStatus.BaseSubscriptionPlan.RenewalRate)
                        {
                            subscriptionPlanStatus.BaseSubscriptionPlan.RenewalRate = totalSubscriptionBundleRenewalRate;
                            foreach (SubscriptionPremiumOption spo in subscriptionPlanStatus.PremiumOptions)
                            {
                                if (spo.IsBase)
                                {
                                    spo.RenewalRate = totalSubscriptionBundleRenewalRate;
                                }
                            }
                        }
                    }


                }
                // for free trial subscription
                else if (freeTrialPackage != null)
                {
                    subscriptionItem = GetSubscriptionItem(freeTrialPackage);
                    subscriptionPlanStatus.PackageTypeId = (int)freeTrialPackage.PackageType;
                    subscriptionPlanStatus.FreeTrialSubscriptionPlan = new SubscriptionBase
                    {
                        // this means it the free trial hasn’t been cancelled by the user and is still active
                        IsEnabled = _freeTrialSubscription.IsCaptureEnabled,
                        CurrencyType = (CurrencyType)Enum.Parse(typeof(CurrencyType), ((int)freeTrialPackage.CurrencyType).ToString()),
                        RenewalDurationType = (DurationType)Enum.Parse(typeof(DurationType), ((int)subscriptionItem.RenewalDurationType).ToString()),
                        EndDate = freeTrialPackage.ExpiredDate,
                        RenewalRate = subscriptionItem.RenewalAmount,
                        RenewalDuration = subscriptionItem.RenewalDuration,
                        InitialCost = subscriptionItem.Amount,
                        InitialDurationType = (DurationType)Enum.Parse(typeof(DurationType), ((int)subscriptionItem.DurationType).ToString()),
                        InitialDuration = subscriptionItem.Duration
                    };
                }

                return subscriptionPlanStatus;
            }
            finally
            {
                ServiceClientFactory.CloseRenewalServiceClient();
                ServiceClientFactory.CloseCatalogServiceClient();
            }            
        }

        private Spark.Common.CatalogService.Item GetSubscriptionItem(Spark.Common.CatalogService.Package package)
        {
            if (package != null && package.Items != null)
            {
                foreach (Spark.Common.CatalogService.Item item in package.Items)
                {
                    if (item != null)
                    {
                        foreach(Spark.Common.CatalogService.PrivilegeType privilegeType in item.PrivilegeType)
                        {
                            if (privilegeType == Common.CatalogService.PrivilegeType.BasicSubscription)
                            {
                                return item;
                            }
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Get Package Type ID
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        int GetPackageTypeIdFromFreeTrial(int packageId)
        {
            if (_freeTrialSubscription.PrimaryPackageID == Constants.NULL_INT ||
                _freeTrialSubscription.PrimaryPackageID == 0)
                return 0;
            var package = CatalogServiceWebAdapter
                .GetProxyInstance()
                .GetPackageDetails(_freeTrialSubscription.PrimaryPackageID);
            
            CatalogServiceWebAdapter.CloseProxyInstance();
            if (package == null)
                return 0;

            return (int)package.PackageType;
        }

        private int GetPackageTypeIdFromBasePackage(int packageId)
        {
            if (_baseSubscription.PrimaryPackageID == Constants.NULL_INT ||
                _baseSubscription.PrimaryPackageID == 0)
                return Constants.NULL_INT;
            var package = CatalogServiceWebAdapter
                .GetProxyInstance()
                .GetPackageDetails(_baseSubscription.PrimaryPackageID);

            if (package == null)
                return Constants.NULL_INT;

            return (int)package.PackageType;
        }

        /// <summary>
        ///     Get the premium subscription options
        /// </summary>
        /// <param name="basePlan">The base subscription plan.</param>
        /// <returns>The premium subscription options.</returns>
        private IEnumerable<SubscriptionPremiumOption> GetPremiumOptions(Plan basePlan)
        {
            if (_baseSubscription == null) yield break;
            foreach (var premiumSubscriptions in _baseSubscription.RenewalSubscriptionDetails)
            {
                Plan rsePlan = PlanSA.Instance.GetPlan(premiumSubscriptions.PackageID, _brand.BrandID);
                IEnumerable<PremiumType> types = GetPremiumOptionType((int) rsePlan.PremiumTypeMask);

                if (types == null) continue;
                foreach (var type in types)
                {
                    yield return GetSubscriptionPremiumOption(type, basePlan, rsePlan, premiumSubscriptions);
                }
            }
        }

        private List<SubscriptionPremiumOption> GetPremiumOptions(Spark.Common.CatalogService.Package basePackage)
        {
            if (_baseSubscription == null)
            {
                return null;
            }

            Spark.Common.CatalogService.Item subscriptionItem = GetSubscriptionItem(basePackage);
            List<SubscriptionPremiumOption> premiumOptions = new List<SubscriptionPremiumOption>();
            foreach (var premiumSubscriptions in _baseSubscription.RenewalSubscriptionDetails)
            {
                Spark.Common.CatalogService.Package package = GetPackage(premiumSubscriptions.PackageID);
                if (package != null && package.Items != null)
                {
                    SubscriptionPremiumOption premiumOption = null;
                    foreach (Spark.Common.CatalogService.Item item in package.Items)
                    {
                        //note: a la carte should only have one item except for All Access, which means we should return back an array or mask instead of a single premium type expected by clients (which means they will need to update)
                        //for now, it will display as the first premium type in the package
                        if (item != null)
                        {
                            List<PremiumType> premiumTypes = GetPremiumOptionType(ConvertCatalogToAccessPrivilege(item.PrivilegeType).ToArray());

                            if (premiumTypes != null)
                            {
                                foreach (PremiumType premiumType in premiumTypes)
                                {
                                    var rate = item.RenewalAmount * (subscriptionItem.RenewalDuration / item.RenewalDuration);

                                    if (premiumOption == null)
                                    {
                                        premiumOption = new SubscriptionPremiumOption()
                                        {
                                            CurrencyType = (CurrencyType)Enum.Parse(typeof(CurrencyType), ((int)package.CurrencyType).ToString()),
                                            IsEnabled = premiumSubscriptions.IsRenewalEnabled,
                                            RenewalDurationType = (DurationType)Enum.Parse(typeof(DurationType), ((int)item.RenewalDurationType).ToString()),
                                            PremiumSubscriptionType = premiumType,
                                            RenewalRate = rate,
                                            IsBase = (premiumSubscriptions.PackageID == _baseSubscription.PrimaryPackageID),
                                            PackageID = premiumSubscriptions.PackageID
                                        };
                                        premiumOption.PremiumSubscriptionTypeList = new List<PremiumType>();
                                        premiumOption.PremiumSubscriptionTypeList.Add(premiumType);
                                    }
                                    else
                                    {
                                        //bundled a la carte
                                        premiumOption.RenewalRate += rate;
                                        premiumOption.PremiumSubscriptionTypeList.Add(premiumType);
                                        if (premiumType == PremiumType.AllAccess || premiumType == PremiumType.AllAccessEmail)
                                        {
                                            premiumOption.PremiumSubscriptionType = PremiumType.AllAccess;
                                        }
                                    }                                    
                                }
                            }
                        }
                    }

                    if (premiumOption != null)
                    {
                        premiumOptions.Add(premiumOption);
                    }
                }
            }

            return premiumOptions;
        }

        private List<PrivilegeType> ConvertCatalogToAccessPrivilege(Spark.Common.CatalogService.PrivilegeType[] privileges)
        {
            List<PrivilegeType> accessPrivileges = new List<PrivilegeType>();
            if (privileges != null)
            {
                foreach (Spark.Common.CatalogService.PrivilegeType privilege in privileges)
                {
                    accessPrivileges.Add((PrivilegeType)Enum.Parse(typeof(PrivilegeType), ((int)privilege).ToString()));
                }
            }

            return accessPrivileges;
        }

        /// <summary>
        ///     Get the types of premium option from the bit mask.
        /// </summary>
        /// <param name="mask">The options mask.</param>
        /// <returns>The types of premium option from the bit mask.</returns>
        private IEnumerable<PremiumType> GetPremiumOptionType(int mask)
        {
            return (from PremiumType premiumType in _validTypes
                let type = (int) premiumType
                where (mask & type) == type
                select premiumType);
        }

        private List<PremiumType> GetPremiumOptionType(PrivilegeType[] privilegeTypes)
        {
            List<PremiumType> premiumTypes = new List<PremiumType>();
            foreach (PrivilegeType privilegeType in privilegeTypes)
            {
                switch (privilegeType)
                {
                    case PrivilegeType.AllAccess:
                        premiumTypes.Add(PremiumType.AllAccess);
                        break;
                    case PrivilegeType.AllAccessEmails:
                        premiumTypes.Add(PremiumType.AllAccessEmail);
                        break;
                    case PrivilegeType.ColorAnalysis:
                        premiumTypes.Add(PremiumType.ColorCode);
                        break;
                    case PrivilegeType.HighlightedProfile:
                        premiumTypes.Add(PremiumType.HighlightedProfile);
                        break;
                    case PrivilegeType.JMeter:
                        premiumTypes.Add(PremiumType.JMeter);
                        break;
                    case PrivilegeType.ReadReceipt:
                        premiumTypes.Add(PremiumType.ReadReceipt);
                        break;
                    case PrivilegeType.SpotlightMember:
                        premiumTypes.Add(PremiumType.SpotlightMember);
                        break;
                    default:
                        break;
                }
            }

            return premiumTypes;
        }

        /// <summary>
        ///     Get a premium option subscription.
        /// </summary>
        /// <param name="premiumType">The type of premium option.</param>
        /// <param name="basePlan">The base subscription plan.</param>
        /// <param name="premiumOptionPlan">The premium subscription plan.</param>
        /// <param name="renewalSubscriptionDetail">The subscription plan detials.</param>
        /// <returns>A premium option subscription.</returns>
        private SubscriptionPremiumOption GetSubscriptionPremiumOption(PremiumType premiumType,
            Plan basePlan,
            Plan premiumOptionPlan,
            RenewalSubscriptionDetail renewalSubscriptionDetail)
        {
            var rate = premiumOptionPlan.RenewCost*(basePlan.RenewDuration/premiumOptionPlan.RenewDuration);

            return new SubscriptionPremiumOption
            {
                CurrencyType = premiumOptionPlan.CurrencyType,
                IsEnabled = renewalSubscriptionDetail.IsRenewalEnabled,
                RenewalDurationType = premiumOptionPlan.RenewDurationType,
                PremiumSubscriptionType = premiumType,
                RenewalRate = rate,
                IsBase = (renewalSubscriptionDetail.PackageID == _baseSubscription.PrimaryPackageID)
            };
        }

        /// <summary>
        ///     Get the base subscription renewal plan. This returns null for Free Trial.
        /// </summary>
        /// <returns>The base subscription renewal plan.</returns>
        private RenewalSubscription GetBaseSubscription()
        {
            RenewalSubscription renewalSubscription = null;
            try
            {
                renewalSubscription = ServiceClientFactory.GetRenewalServiceClient().GetCurrentRenewalSubscription(_memberId, _brand.Site.SiteID);
            }
            finally
            {
                ServiceClientFactory.CloseRenewalServiceClient();
            }

            return renewalSubscription;
        }

        /// <summary>
        ///     Get the base subscription plan.
        /// </summary>
        /// <returns>The base subscription plan.</returns>
        private Plan GetBasePlan()
        {
            return _baseSubscription == null
                ? null
                : PlanSA.Instance.GetPlan(_baseSubscription.PrimaryPackageID, _brand.BrandID);
        }

        private Spark.Common.CatalogService.Package GetBasePackage()
        {
            if (_baseSubscription == null)
            {
                return null;
            }

            return GetPackage(_baseSubscription.PrimaryPackageID);

        }

        private Spark.Common.CatalogService.Package GetPackage(int packageID)
        {
            return ServiceClientFactory.GetCatalogServiceClient().GetPackageDetails(packageID);
        }

        private Plan GetFreeTrialPlan()
        {
            return _freeTrialSubscription == null
                ? null
                : PlanSA.Instance.GetPlan(_freeTrialSubscription.PrimaryPackageID, _brand.BrandID);
        }

        private Spark.Common.CatalogService.Package GetFreeTrialPackage()
        {
            if (_freeTrialSubscription == null)
            {
                return null;
            }

            try
            {
                return ServiceClientFactory.GetCatalogServiceClient().GetPackageDetails(_freeTrialSubscription.PrimaryPackageID);
            }
            finally
            {
                ServiceClientFactory.CloseRenewalServiceClient();
            }

        }

        /// <summary>
        ///     Is the member self suspended?
        /// </summary>
        /// <param name="member">The current member instance.</param>
        /// <returns>True if the member is self suspended.</returns>
        private bool IsSelfSuspended(IMemberDTO member)
        {
            return member.GetAttributeInt(_brand, "SelfSuspendedFlag") != 0;
        }

        /// <summary>
        ///     Is this a free trial?
        /// </summary>
        /// <returns>True if the member is on their free trial period.</returns>
        private bool IsInFreeTrial()
        {
            try
            {
                return AuthorizationServiceWebAdapter.GetProxyInstance()
                    .HasActiveFreeTrialSubscription(_memberId, _brand.Site.SiteID);
            }
            catch (Exception ex)
            {
                throw new Exception(
                    string.Format(
                        "Error in checking to see if the member has an active free trial subscription, Error message: {0}",
                        ex.Message));
            }
            finally
            {
                AuthorizationServiceWebAdapter.CloseProxyInstance();
            }
        }

        /// <summary>
        ///     Terminate the base subscription renewal.
        /// </summary>
        /// <param name="transactionReasonId">The reason for terminating the subscription.</param>
        /// <param name="confirmationId">The confirmation number.</param>
        /// <returns>The termination response.</returns>
        private RenewalResponse Terminate(int transactionReasonId, out int confirmationId)
        {
            //Update UPS Renewal
            RenewalResponse renewalResponse = ServiceClientFactory.GetRenewalServiceClient().DisableAutoRenewal(
                            _memberId,
                            _brand.Site.SiteID,
                            CallingSystemId,
                            Constants.NULL_INT,
                            string.Empty,
                            transactionReasonId);

            confirmationId = renewalResponse.TransactionID;
            if (renewalResponse.InternalResponse.responsecode != "0")
            {
                throw new Exception(
                    string.Format(
                        "Failed to update with Unified Renewal to disable auto-renewal for MemberID: {0}, please try again or contact Software Engineering.",
                        _memberId));
            }

            // this is used to display the autorenewal reactivate link in the top aux menu
            Member member = MemberSA.Instance.GetMember(_memberId, MemberLoadFlags.None);
            member.SetAttributeInt(_brand, "AutoRenewalStatus", 0);
            MemberSA.Instance.SaveMember(member);

            return renewalResponse;
        }

        #endregion

        #region Free Trials

        /// <summary>
        ///     Disable a member's free trial subscription.
        /// </summary>
        /// <returns>True if the subscription was updated successfully.</returns>
        public bool DisableFreeTrial()
        {
            // get the subscription
            AuthorizationSubscription freeTrialSubscription = GetFreeTrialSubscription();

            if (freeTrialSubscription == null)
            {
                throw new Exception(string.Format("No free trial subscription found for member:{0} brand:{1}", _memberId,
                    _brand.BrandID));
            }

            // check if free trial is currently enabled
            bool isFreeTrialEnabled = IsFreeTrialEnabled(freeTrialSubscription);

            if (isFreeTrialEnabled)
            {
                // update the subscription access
                AccessResponse accessResponse = UpdateFreeTrialAccess(freeTrialSubscription);

                // return the response code
                return accessResponse.InternalResponse.responsecode == "0";
            }

            // can't do anything in this case
            throw new Exception(string.Format("Free trial is already disabled for member:{0} brand:{1}", _memberId,
                _brand.BrandID));
        }

        /// <summary>
        ///     Update the free trial access status.
        /// </summary>
        /// <param name="freeTrialSubscription">The free trial subscription details.</param>
        /// <returns>The update response.</returns>
        private static AccessResponse UpdateFreeTrialAccess(AuthorizationSubscription freeTrialSubscription)
        {
            var accessFreeTrial = new AccessFreeTrial
            {
                AuthorizationSubscriptionID = freeTrialSubscription.AuthorizationSubscriptionID,
                AuthorizationCode = freeTrialSubscription.AuthorizationCode,
                AuthorizationChargeID = freeTrialSubscription.AuthorizationChargeID,
                AuthorizationAmount = freeTrialSubscription.AuthorizationAmount,
                PaymentProfileID = freeTrialSubscription.PaymentProfileID,
                PaymentType = "CreditCard",
                CustomerID = freeTrialSubscription.CustomerID,
                CallingSystemID = freeTrialSubscription.CallingSystemID,
                CallingSystemTypeID = 2,
                CaptureDate = freeTrialSubscription.CaptureDateUTC,
                CaptureAttempt = freeTrialSubscription.CaptureAttempt,
                AuthorizationOnlyPackageID = freeTrialSubscription.PrimaryPackageID,
                SecondaryAuthorizationOnlyPackageID = freeTrialSubscription.AllSecondaryPackageID,
                AdminID = 0,
                AdminUserName = String.Empty,
                TransactionType = TransactionType.CancelFreeTrial,
                AuthorizationStatus = AuthorizationStatus.Successful,
                ConvertedOrderID = 0,
                AccessReasonID = 0,
                UnifiedActionTypeID = 2
            };

            return AccessServiceWebAdapter.GetProxyInstance().AdjustAuthorizationOnlyPrivilege(accessFreeTrial);
        }

        /// <summary>
        ///     Check if the free trial is currently enabled//
        /// </summary>
		//
        /// <param name="freeTrialSubscription">The free trial subscription details.</param>
        /// <returns>True if the free trial is currently enabled</returns>
        private static bool IsFreeTrialEnabled(AuthorizationSubscription freeTrialSubscription)
        {
            return freeTrialSubscription.CaptureDateUTC >= DateTime.UtcNow;
        }

        /// <summary>
        ///     Get the free trial subscription details.
        /// </summary>
        /// <returns>The free trial subscription details.</returns>
        private AuthorizationSubscription GetFreeTrialSubscription()
        {
            AuthorizationSubscription result = null;

            try
            {
                PaymentProfileInfo defaultPaymentProfile = PaymentProfileServiceWebAdapter.GetProxyInstance()
                    .GetMemberDefaultPaymentProfile(_memberId, _brand.Site.SiteID);

                if (defaultPaymentProfile != null)
                {
                    result = AuthorizationServiceWebAdapter.GetProxyInstance()
                        .GetActiveAuthorizationSubscription(defaultPaymentProfile.PaymentProfileID, _memberId,
                            _brand.Site.SiteID);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(
                    string.Format(
                        "Error in getting the free trial subscription details for member:{0} brand:{1} message:{2}",
                        _memberId, _brand.BrandID, ex.Message));
            }
            finally
            {
                PaymentProfileServiceWebAdapter.CloseProxyInstance();
                AuthorizationServiceWebAdapter.CloseProxyInstance();
            }

            return result;
        }

        #endregion
    }
}
