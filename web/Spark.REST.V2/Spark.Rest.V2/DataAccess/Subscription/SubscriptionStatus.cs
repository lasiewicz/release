﻿using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;

namespace Spark.Rest.DataAccess.Subscription
{
    public static class SubscriptionStatus
    {
        public static bool MemberIsSubscriber(Brand brand, int memberId)
        {

            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);

            var accessCacheRefresh =
                Conversion.CBool(member.GetAttributeInt(brand, "AccessCacheRefresh", 0));
            if (accessCacheRefresh)
            {
                member.SetAttributeInt(brand, "AccessCacheRefresh", 0);
                MemberSA.Instance.SaveMember(member);
                MemberSA.Instance.ExpireCachedMemberAccess(memberId, true);
            }

            var isPaying = member.IsPayingMember(brand.Site.SiteID);
            return isPaying;
        }

    }
}