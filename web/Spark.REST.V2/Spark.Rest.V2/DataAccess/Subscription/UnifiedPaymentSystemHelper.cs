﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Security.Policy;
using System.Text;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters.TokenReplacement;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Newtonsoft.Json;
using Spark.Common;
using Spark.Common.AccessService;
using Spark.Common.Adapter;
using Spark.Common.OrderHistoryService;
using Spark.Common.PaymentProfileService;
using Spark.Common.UPS;
using Spark.REST.DataAccess.Content;
using Spark.REST.Entities.Subscription;
using Spark.REST.Helpers;
using Spark.REST.Models.Subscription;
using Spark.Rest.V2.DataAccess.Ups;
using Brand = Matchnet.Content.ValueObjects.BrandConfig.Brand;
using PaymentType = Matchnet.Purchase.ValueObjects.PaymentType;
using Spark.Rest.V2.Exceptions;
using Matchnet.OmnitureHelper;
using System.Linq;
using Spark.Rest.V2.BedrockPorts;
using System.Collections;
using Spark.Rest.V2.Models.Subscription;
using Newtonsoft.Json.Converters;
using Spark.Logger;
using Spark.Rest.Helpers;
using CurrencyType = Matchnet.Purchase.ValueObjects.CurrencyType;
using DurationType = Matchnet.DurationType;
using OrderInfo = Spark.Common.OrderHistoryService.OrderInfo;

#endregion

namespace Spark.REST.DataAccess.Subscription
{
    /// <summary>
    ///     Gather information required for the calling site to redirect to Payment UI.
    /// </summary>
    public class UnifiedPaymentSystemHelper
    {
        public enum PaymentUiPostDataType
        {
            Subscription = 1,
            Upsale = 2,
            PremiumServices = 3, //a la carte
            BillingInformation = 4
        }

        public static readonly UnifiedPaymentSystemHelper Instance = new UnifiedPaymentSystemHelper();

        private UnifiedPaymentSystemHelper()
        {
        }

        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(UnifiedPaymentSystemHelper));
        private static ISettingsSA _settingsService = null;

        public static ISettingsSA SettingsService
        {
            get { return _settingsService ?? RuntimeSettings.Instance; }
            set { _settingsService = value; }
        }

        public PaymentUiPostData GetPaymentUiPostData(PaymentUiPostDataRequest request, bool allowAdminPromos)
        {
            if (request is AdminPaymentUiPostDataRequest)
            {
                Log.LogInfoMessage(string.Format("AdminPaymenUiPostDataRequest: {0}", JsonConvert.SerializeObject(request)), ErrorHelper.GetCustomData());
            }
            else
            {
                Log.LogInfoMessage(string.Format("PaymenUiPostDataRequest: {0}", JsonConvert.SerializeObject(request)), ErrorHelper.GetCustomData());
            }

            PaymentUiPostData postData = new PaymentUiPostData();

            postData.GlobalLogId = KeySA.Instance.GetKey("UPSGlobalLogID");
            postData.PaymentUiUrl = SettingsService.GetSettingFromSingleton("UPS_PAYMENT_UI_URL",
                request.Brand.Site.Community.CommunityID,
                request.Brand.Site.SiteID,
                request.Brand.BrandID);
            postData.PaymentUiUrl += request.Brand.Site.Name.Replace(".", String.Empty).ToLower();

            string json = "";

            //admin check
            if (!allowAdminPromos && (request.PromoType == PromoType.AdminOnly || request.PromoType == PromoType.SupervisorOnly))
            {
                throw new Exception("PromoType not allowed. MemberId:" + request.MemberId
                                    + " BrandId:" + request.BrandId
                                    + " PromoType:" + request.PromoType.ToString());
            }

            //get paymentui json
            switch (request.PaymentUiPostDataType)
            {
                case PaymentUiPostDataType.Upsale:
                    json = GetJsonPaymentUiPostDataUpsale(request, postData.GlobalLogId);
                    break;
                case PaymentUiPostDataType.PremiumServices:
                    json = GetJsonPaymentUiPostDataPremiumServices(request, postData.GlobalLogId);
                    break;
                case PaymentUiPostDataType.BillingInformation:
                    json = GetJsonPaymentUiPostDataBillingInformation(request, postData.GlobalLogId);
                    break;
                default:
                    json = GetJsonPaymentUiPostData(request, postData.GlobalLogId);
                    break;
            }
            
            postData.Json = json.Encrypt();

            return postData;
        }

        public string GetJsonPaymentUiPostData(PaymentUiPostDataRequest request, int globalLogId)
        {
            Promo promo = null;

            if (request.PaymentType == PaymentType.None)
            {
                request.PaymentType = PaymentType.CreditCard;
            }

            if (request.PromoID > 0)
            {
                promo = PromoEngineSA.Instance.GetPromoByID(request.PromoID, request.BrandId, request.MemberId, false);
            }

            if (promo == null && request.PromoType == PromoType.SupervisorOnly)
            {
                promo = PromoEngineSA.Instance.GetSupervisorPromo(request.MemberId, request.Brand, request.PaymentType);
            }

            if (promo == null && request.PromoType == PromoType.AdminOnly)
            {
                promo = PromoEngineSA.Instance.GetAdminPromo(request.MemberId, request.Brand, request.PaymentType);
            }

            if (promo == null || promo.PromoID <= 0)
            {
                promo = PromoEngineSA.Instance.GetMemberPromo(request.MemberId, request.Brand, request.PaymentType, request.PromoType);
            }

            if (promo == null)
                throw new Exception("Promo not found. MemberId:" + request.MemberId
                                    + " BrandId:" + request.BrandId
                                    + " PromoType:" + request.PromoType.ToString());

            var upsLegacyDataId = KeySA.Instance.GetKey("UPSLegacyDataID");
            var member = MemberSA.Instance.GetMember(request.MemberId, MemberLoadFlags.None);


            var payment = new PaymentJson
            {
                UPSLegacyDataID = upsLegacyDataId,
                UPSGlobalLogID = globalLogId,
                GetCustomerPaymentProfile = 1,
                Version = 2,
                CallingSystemID = request.Brand.Site.SiteID,
                TemplateID = promo.UPSTemplateID, //changed from hard coded 500 value jul/14
                TimeStamp = DateTime.Now
            };

            payment.Data.Navigation.CancelURL = request.CancelUrl;
            payment.Data.Navigation.ReturnURL = request.ReturnUrl;
            payment.Data.Navigation.ConfirmationURL = request.ConfirmationUrl;
            payment.Data.Navigation.DestinationURL = request.DestinationUrl;
            payment.IsSubscriptionConfirmationEmailEnabled = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("ENABLE_SUBSCRIPTION_CONFIRMATION_EMAIL", request.Brand.Site.Community.CommunityID, request.Brand.Site.SiteID, request.Brand.BrandID));
            try
            {
                payment.IsUPSConfirm = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("UPS_CONFIRM", request.Brand.Site.Community.CommunityID, request.Brand.Site.SiteID, request.Brand.BrandID));
            }
            catch
            {
                payment.IsUPSConfirm = false;
            }

            if (payment.IsUPSConfirm == false)
            {
                //always true for norbert users
                if (member.GetAttributeInt(request.Brand, "REDRedesignBetaParticipatingFlag") == 1)
                {
                    payment.IsUPSConfirm = true;
                }
            }

            // Kount merchant ID
            payment.KountMerchantID = GetKountMerchantID(request.Brand);

            // Order Attributes
            var orderAttributes = new Dictionary<string, string>
            {
                {"PRTID", request.PrtId},
                {"SRID", request.SrId},
                {"PricingGroupID", promo.PromoID.ToString(CultureInfo.InvariantCulture)},
                {
                    Common.MemberLevelTracking.Key.TrackingLastApplication.ToString(),
                    request.MemberLevelTrackingLastApplication
                },
                {
                    "DeviceProperties_IsMobileDevice",
                    (request.MemberLevelTrackingIsMobileDevice == null
                        ? "false"
                        : request.MemberLevelTrackingIsMobileDevice.ToLower())
                },
                {
                    "DeviceProperties_IsTablet", (request.MemberLevelTrackingIsTablet == null
                        ? "false"
                        : request.MemberLevelTrackingIsTablet.ToLower())
                }

            };

            if (!String.IsNullOrEmpty(request.EID))
                orderAttributes.Add("EID", request.EID);


            foreach (var key in orderAttributes.Keys)
            {
                if (!String.IsNullOrEmpty(orderAttributes[key]))
                    payment.Data.OrderAttributes += key + "=" + orderAttributes[key] + ",";
            }

            if (payment.Data.OrderAttributes.Length > 0)
                payment.Data.OrderAttributes = payment.Data.OrderAttributes.TrimEnd(',');

            // Member Info
            payment.Data.MemberInfo.CustomerID = request.MemberId;
            payment.Data.MemberInfo.RegionID = member.GetAttributeInt(request.Brand, "RegionID");
            payment.Data.MemberInfo.IPAddress = request.ClientIp;
            payment.Data.MemberInfo.Language =
                ((Language) Enum.Parse(typeof (Language), request.Brand.Site.LanguageID.ToString())).ToString();
            var regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID,
                (int) Language.English);
            if (regionLanguage.CountryRegionID == RegionDataAccess.RegionUs)
                payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation.Trim().ToUpper();

            //LC: Adding extra fields needed for Kount RIS
            payment.Data.MemberInfo.EmailAddress = member.GetAttributeText(request.Brand, "EmailAddress");
            payment.Data.MemberInfo.GenderMask = AttributeHelper.GetGenderDescription(request.Brand, member.GetAttributeInt(request.Brand, "gendermask"), false);
            payment.Data.MemberInfo.BirthDate =   member.GetAttributeDate(request.Brand, "birthdate", DateTime.MinValue);
            payment.Data.MemberInfo.BrandInsertDate = member.GetAttributeDate(request.Brand, "BrandInsertDate",DateTime.MinValue);

            #region UDFS
            //JS-1228 LC: Adding UDF fields needed for Kount
            payment.Data.MemberInfo.UserName = member.GetUserName(request.Brand);
            payment.Data.MemberInfo.MaritalStatus = GetDescription("MaritalStatus", member.GetAttributeInt(request.Brand, "MaritalStatus"), request.Brand);
            payment.Data.MemberInfo.Occupation = member.GetAttributeText(request.Brand, "OccupationDescription");
            payment.Data.MemberInfo.Education = GetDescription("EducationLevel", member.GetAttributeInt(request.Brand, "EducationLevel"), request.Brand);
            payment.Data.MemberInfo.Eyes = GetDescription("EyeColor", member.GetAttributeInt(request.Brand, "EyeColor"), request.Brand);
            payment.Data.MemberInfo.PromotionID = member.GetAttributeInt(request.Brand, "PromotionID").ToString();
            payment.Data.MemberInfo.Hair = GetDescription("HairColor", member.GetAttributeInt(request.Brand, "HairColor"), request.Brand);
            payment.Data.MemberInfo.Height = member.GetAttributeInt(request.Brand, "Height").ToString();

            //TODO: Define webconstant to check Jdate community
            if (request.Brand.Site.Community.CommunityID == 3)
            {
                payment.Data.MemberInfo.Ethnicity = GetDescription("JDateEthnicity", member.GetAttributeInt(request.Brand, "JDateEthnicity"), request.Brand);
                payment.Data.MemberInfo.Religion = GetDescription("JDateReligion", member.GetAttributeInt(request.Brand, "JDateReligion"), request.Brand);
            }
            else
            {
                payment.Data.MemberInfo.Ethnicity = GetDescription("Ethnicity", member.GetAttributeInt(request.Brand, "Ethnicity"), request.Brand);
                payment.Data.MemberInfo.Religion = GetDescription("Religion", member.GetAttributeInt(request.Brand, "Religion"), request.Brand);
            }
            string aboutMe = member.GetAttributeText(request.Brand, "AboutMe");
            payment.Data.MemberInfo.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);

            #endregion UDF

            // Promo Info
            var promoInfo = new PaymentJsonArray { { "PromoID", promo.PromoID }, { "PageTitle", promo.PageTitle }, { "LegalNote", promo.LegalNote } };
            var promoEndDateLocalTime = promo.EndDate.ToUniversalTime().AddHours(request.Brand.Site.GMTOffset);
            var promoEndDateLocalTimeFormatted = promoEndDateLocalTime.ToString("d",
                request.Brand.Site.CultureInfo.DateTimeFormat);
            promoInfo.Add("EndDate", promoEndDateLocalTimeFormatted);
            var promoEndTime = promoEndDateLocalTime.Hour;
            promoInfo.Add("EndTime",promoEndTime);
            
            payment.Data.PromoInfo.Add(promoInfo);

            // Page Info
            var pageInfo = new PaymentJsonArray {{"PRTTitle", request.PrtTitle}};
            payment.Data.PageInfo.Add(pageInfo);

            #region Omniture Variables

            var omnitureVariablesRequest = request.OmnitureVariables;

            var omnitureVariables = new PaymentJsonArray();

            foreach (var variable in omnitureVariablesRequest)
            {
                omnitureVariables.Add(variable.Key, variable.Value);
            }

            omnitureVariables.Remove("s_account");
            omnitureVariables.Add("s_account", GetOmnitureReportSuite(request.Brand));
            omnitureVariables.Remove("eVar2");
            omnitureVariables.Add("eVar2", "m_sub_start");
            omnitureVariables.Remove("events");
            omnitureVariables.Add("events", "event9");
            omnitureVariables.Remove("eVar3");
            omnitureVariables.Add("eVar3", GetPlanIDListForOmniture(promo));
            omnitureVariables.Remove("eVar4");
            omnitureVariables.Add("eVar4", promo.PromoID);

            omnitureVariables.Remove("eVar6");
            omnitureVariables.Add("eVar6", request.PrtId);

            omnitureVariables.Remove("prop17");
            omnitureVariables.Add("prop17", OmnitureHelper.GetProfileCompetionPercentage(member, request.Brand));
            omnitureVariables.Remove("prop18");
            omnitureVariables.Add("prop18", OmnitureHelper.GetGender(member.GetAttributeInt(request.Brand, "gendermask")));
            omnitureVariables.Remove("prop19");
            omnitureVariables.Add("prop19", OmnitureHelper.GetAge(member.GetAttributeDate(request.Brand, "Birthdate")).ToString());
            omnitureVariables.Remove("prop20");
            if (request.Brand.Site.Community.CommunityID == (int)CommunityId.JDate)
            {
                omnitureVariables.Add("prop20", MemberHelper.GetDescription("JDateEthnicity", member.GetAttributeInt(request.Brand, "JDateEthnicity"),
                        request.Brand));
            }
            else
            {
                omnitureVariables.Add("prop20", MemberHelper.GetDescription("Ethnicity", member.GetAttributeInt(request.Brand, "Ethnicity"),
                        request.Brand));
            }
            omnitureVariables.Remove("prop21");
            omnitureVariables.Add("prop21", OmnitureHelper.GetRegionString(member, request.Brand, request.Brand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
            omnitureVariables.Remove("prop23");
            omnitureVariables.Add("prop23", request.MemberId.ToString());
            omnitureVariables.Remove("eVar36");
            omnitureVariables.Add("eVar36", GetOmnitureSiteURL(request.Brand, request.PromoType));
            omnitureVariables.Remove("eVar44");
            omnitureVariables.Add("eVar44", request.MemberId.ToString());

            payment.Data.OmnitureVariables.Add(omnitureVariables);

            #endregion

            #region Analytics Variables (Tealium)
            var analyticVariablesRequest = request.AnalyticVariables;

            var analyticVariables = new PaymentJsonArray();
            analyticVariables.Add("eId", string.IsNullOrEmpty(request.EID) ? "" : request.EID);

            foreach (var variable in analyticVariablesRequest)
            {
                analyticVariables.Remove(variable.Key);
                analyticVariables.Add(variable.Key, variable.Value);
            }

            var analyticAttributeSetResult = ProfileAccess.GetAttributeSetWithPhotoAttributes(request.Brand, member.MemberID, member.MemberID, "tealiumtargetingprofile_us" , false, "");
            if (analyticAttributeSetResult != null && analyticAttributeSetResult.AttributeSet != null)
            {
                foreach (KeyValuePair<string, object> entry in analyticAttributeSetResult.AttributeSet)
                {
                    analyticVariables.Remove(entry.Key);
                    analyticVariables.Add(entry.Key, entry.Value);
                }

            }            

            analyticVariables.Remove("tealiumUrl");
            analyticVariables.Add("tealiumUrl", SettingsService.GetSettingFromSingleton("TEALIUM_SRC",
                request.Brand.Site.Community.CommunityID,
                request.Brand.Site.SiteID,
                request.Brand.BrandID));

            payment.Data.AnalyticVariables.Add(analyticVariables);

            #endregion

            // Legacy Data Info
            payment.Data.LegacyDataInfo.Member = request.MemberId;
            payment.Data.LegacyDataInfo.BrandID = request.BrandId;
            payment.Data.LegacyDataInfo.SiteID = request.Brand.Site.SiteID;
            if (request is AdminPaymentUiPostDataRequest)
            {
                payment.Data.LegacyDataInfo.AdminMemberID = (request as AdminPaymentUiPostDataRequest).AdminMemberID;
            }

            #region Header Template Collections

            // Column Header Template Collection
            foreach (HeaderResourceTemplate template in promo.ColHeaderResourceTemplateCollection)
            {
                var headerTemplate = new PaymentJsonArray();

                var content = ResourceTemplateSA.Instance.GetResource(template.ResourceTemplateID,
                    new ColumnHeaderTokenReplacer());
                headerTemplate.Add("ResourceTemplateContent", content);
                headerTemplate.Add("HeaderNumber", template.HeaderNumber);

                payment.Data.ColHeaderTemplates.Add(headerTemplate);
            }

            // Row Header Template Collection
            foreach (RowHeaderResourceTemplate template in promo.RowHeaderResourceTemplateCollection)
            {
                var headerTemplate = new PaymentJsonArray();

                var content = ResourceTemplateSA.Instance.GetResource(template.ResourceTemplateID,
                    new RowHeaderTokenReplacer(template.Duration));
                headerTemplate.Add("ResourceTemplateContent", content);
                headerTemplate.Add("HeaderNumber", template.HeaderNumber);

                payment.Data.RowHeaderTemplates.Add(headerTemplate);
            }

            #endregion

            #region Add Promo Plans

            // Promo Plans
            foreach (PromoPlan promoPlan in promo.PromoPlans)
            {
                var package = new PaymentJsonPackage
                {
                    ID = promoPlan.PlanID,
                    Description = "",
                    SytemID = request.Brand.Site.SiteID,
                    StartDate = promoPlan.StartDate,
                    ExpiredDate = promoPlan.EndDate,
                    CurrencyType = SubscriptionHelper.GetCurrencyAbbreviation(promoPlan.CurrencyType)
                };

                var content = ResourceTemplateSA.Instance.GetResource(promoPlan.ResourceTemplateID,
                    new PlanTokenReplacer(promoPlan, 2));
                package.Items.Add("ResourceTemplateContent", content);
                package.Items.Add("EnabledForMember", promoPlan.EnabledForMember.ToString());
                package.Items.Add("InitialCost", promoPlan.InitialCost);
                package.Items.Add("InitialCostPerDuration", promoPlan.InitialCostPerDuration);
                package.Items.Add("ListOrder", promoPlan.ListOrder);
                package.Items.Add("ColumnGroup", promoPlan.ColumnGroup);
                package.Items.Add("PlanID", promoPlan.PlanID);
                package.Items.Add("CurrencyType", SubscriptionHelper.GetCurrencyAbbreviation(promoPlan.CurrencyType));
                package.Items.Add("BestValueFlag", promoPlan.BestValueFlag);
                package.Items.Add("DefaultPackage", (promoPlan.PlanID == promo.DefaultPlanID) ? true : false);
                package.Items.Add("PaymentTypeMask", promoPlan.PaymentTypeMask);
                package.Items.Add("PlanResourcePaymentTypeID", promoPlan.PlanResourcePaymentTypeID);
                package.Items.Add("CreditAmount", promoPlan.CreditAmount);
                package.Items.Add("PurchaseMode", promoPlan.PurchaseMode.ToString());
                package.Items.Add("PlanServices", promoPlan.PlanServices);
                package.Items.Add("IsPurchaseAllowedWithRemainingCredit", promoPlan.EnabledForMember);
                package.Items.Add("RenewCost", promoPlan.RenewCost);
                payment.Data.Packages.Add(package);
            }

            #endregion

            foreach (var col in payment.Data.ColHeaderTemplates)
            {
                var colNumber = Convert.ToInt32(col["HeaderNumber"]);

                foreach (var row in payment.Data.RowHeaderTemplates)
                {
                    var rowNumber = Convert.ToInt32(row["HeaderNumber"]);

                    if (SubscriptionHelper.DoesPackageExistAtRowCol(rowNumber, colNumber, payment.Data.Packages))
                        continue;
                    // Not Available Package...
                    var dummy = new PaymentJsonPackage {ID = 0, Description = ""};
                    dummy.Items.Add("EnabledForMember", false);
                    dummy.Items.Add("ResourceTemplateContent",
                        "<div class=\"cell-plan sub-none\"><p>Not available</p></div>");
                    dummy.Items.Add("ListOrder", rowNumber);
                    dummy.Items.Add("ColumnGroup", colNumber);

                    payment.Data.Packages.Add(dummy);
                }
            }

            if (payment.Data.Packages.Count <= 0)
            {
                Log.LogInfoMessage("No subscription packages available. MemberID: " + request.MemberId.ToString() + ", SiteID: " + request.Brand.Site.SiteID, ErrorHelper.GetCustomData());
                throw new SparkAPIReportableException(Rest.V2.Serialization.HttpSubStatusCodes.HttpSub400StatusCode.NoPackagesFound, "No subscription packages available. MemberID: " + request.MemberId.ToString() + ", SiteID: " + request.Brand.Site.SiteID.ToString());
            }

            var json = JsonConvert.SerializeObject(payment, new IsoDateTimeConverter());
            Log.LogInfoMessage("PaymentJson string to PUI: " + json, ErrorHelper.GetCustomData());
            PurchaseSA.Instance.UPSLegacyDataSave(json.Compress(), upsLegacyDataId);

            return json;
        }

        public string GetJsonPaymentUiPostDataUpsale(PaymentUiPostDataRequest request, int globalLogId)
        {
            var upsLegacyDataId = KeySA.Instance.GetKey("UPSLegacyDataID");
            OrderInfo orderInfo = null;

            var member = MemberSA.Instance.GetMember(request.MemberId, MemberLoadFlags.None);
            if (member == null || member.MemberID <= 0)
            {
                Log.LogInfoMessage("Member not found for ID: " + request.MemberId.ToString(), ErrorHelper.GetCustomData());
                throw new SparkAPIReportableException(Rest.V2.Serialization.HttpSubStatusCodes.HttpSub400StatusCode.InvalidMemberId, "Member not found for ID: " + request.MemberId.ToString());
            }

            //upsale default template 400 if not provided, fixed pricing will be 410, overridden later in the code
            var templateId = (request.TemplateId <= 0) ? 400 : request.TemplateId;

            var payment = new PaymentJson
            {
                UPSLegacyDataID = upsLegacyDataId,
                UPSGlobalLogID = globalLogId,
                GetCustomerPaymentProfile = 1,
                Version = 2,
                CallingSystemID = request.Brand.Site.SiteID,
                TemplateID = templateId,
                TimeStamp = DateTime.Now
            };

            payment.Data.Navigation.CancelURL = request.CancelUrl;
            payment.Data.Navigation.ReturnURL = request.ReturnUrl;
            payment.Data.Navigation.ConfirmationURL = request.ConfirmationUrl;
            payment.Data.Navigation.DestinationURL = request.DestinationUrl;
            payment.IsSubscriptionConfirmationEmailEnabled = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("ENABLE_SUBSCRIPTION_CONFIRMATION_EMAIL", request.Brand.Site.Community.CommunityID, request.Brand.Site.SiteID, request.Brand.BrandID));
            try
            {
                payment.IsUPSConfirm = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("UPS_CONFIRM", request.Brand.Site.Community.CommunityID, request.Brand.Site.SiteID, request.Brand.BrandID));
            }
            catch
            {
                payment.IsUPSConfirm = false;
            }

            if (payment.IsUPSConfirm == false)
            {
                //always true for norbert users
                if (member.GetAttributeInt(request.Brand, "REDRedesignBetaParticipatingFlag") == 1)
                {
                    payment.IsUPSConfirm = true;
                }
            }

            // Kount merchant ID
            payment.KountMerchantID = GetKountMerchantID(request.Brand);

            //get order info
            int orderID = Constants.NULL_INT;
            ObsfucatedPaymentProfileResponse paymentProfileResponse = null;

            if (!string.IsNullOrEmpty(request.OrderID))
            {
                orderID = Convert.ToInt32(request.OrderID);

                if (orderID > 0)
                {
                    Log.LogDebugMessage("Get order information for Order ID [" + orderID.ToString() + "]", ErrorHelper.GetCustomData());

                    //get UPS order info
                    orderInfo = OrderHistoryServiceWebAdapter.GetProxyInstance().GetOrderInfo(orderID);
                    OrderHistoryServiceWebAdapter.CloseProxyInstance();

                    if (orderInfo != null)
                    {
                        Log.LogDebugMessage("Successfully retrieved order information for Order ID [" + orderID.ToString() + "]", ErrorHelper.GetCustomData());

                        //get payment profile info
                        paymentProfileResponse = PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByOrderID(orderID, orderInfo.CustomerID, orderInfo.CallingSystemID);
                        PaymentProfileServiceWebAdapter.CloseProxyInstance();
                       

                        if (paymentProfileResponse != null && orderInfo != null)
                        {
                            Log.LogDebugMessage("Successfully retrieved payment profile information for Order ID [" + Convert.ToString(orderID) + "]", ErrorHelper.GetCustomData());

                            Matchnet.Purchase.ValueObjects.Plan primaryPlan = UpsGetPrimaryPlan(orderInfo, request.Brand);
                            //Refactored as part of JS-1701
                            var orderConfirmation = GetOrderConfirmation(request.Brand, orderID, member, paymentProfileResponse, orderInfo, primaryPlan);

                            payment.Data.OrderConfirmation.Add(orderConfirmation);
                        }
                        else
                        {
                            Log.LogInfoMessage("Cannot get payment profile details for the order confirmation. OrderID: " + orderID.ToString() + ", MemberID: " + orderInfo.CustomerID.ToString() + ", SiteID: " + orderInfo.CallingSystemID.ToString(), ErrorHelper.GetCustomData());
                            throw new SparkAPIReportableException(Rest.V2.Serialization.HttpSubStatusCodes.HttpSub400StatusCode.MissingPaymentProfile, "Cannot get payment profile details for the order confirmation. OrderID: " + orderID.ToString() + ", MemberID: " + orderInfo.CustomerID.ToString() + ", SiteID: " + orderInfo.CallingSystemID.ToString());
                        }

                    }
                    else
                    {
                        Log.LogInfoMessage("Cannot get order details for the order confirmation. OrderID: " + orderID.ToString(), ErrorHelper.GetCustomData());
                        throw new SparkAPIReportableException(Rest.V2.Serialization.HttpSubStatusCodes.HttpSub400StatusCode.GenericInvalidID, "Cannot get order details for the order confirmation. OrderID: " + orderID.ToString());
                    }
                }
            }

            // Get all the privileges that have not expired for this member for this site  
            AccessPrivilege[] accessPrivileges = AccessServiceWebAdapter.GetProxyInstanceForBedrock().GetCustomerPrivilegeListAllWithIgnoreCache(request.MemberId);
            AccessServiceWebAdapter.CloseProxyInstance();

            bool isAllAccessSubscriber = false;
            List<Spark.Common.AccessService.AccessPrivilege> activePrivileges = new List<Spark.Common.AccessService.AccessPrivilege>();
            foreach (Spark.Common.AccessService.AccessPrivilege checkAccessPrivilege in accessPrivileges)
            {
                if (checkAccessPrivilege.CallingSystemID == request.Brand.Site.SiteID)
                {
                    if (checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.BasicSubscription
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.HighlightedProfile
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.SpotlightMember
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.JMeter
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.ReadReceipt)
                    {
                        if (checkAccessPrivilege.EndDateUTC > DateTime.Now)
                        {
                            activePrivileges.Add(checkAccessPrivilege);

                            if (checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess)
                            {
                                isAllAccessSubscriber = true;
                            }
                        }
                    }
                }
            }

            Spark.Common.RenewalService.RenewalSubscription renewalSub = null;
            List<Plan> allowedUpsalePlanCollection = new List<Plan>();

            //determine if this if fixed pricing upsale
            bool isFixedPricingUpsale = false;
            if (request.PremiumType == PremiumType.AllAccessEmail)
            {
                //FIXED PRICING UPSALE
                isFixedPricingUpsale = true;

                if (request.TemplateId <= 0)
                {
                    payment.TemplateID = 410;
                }

                //All Access Email upsale is only allowed as fixed pricing, but member must be an all access subscriber
                if (!isAllAccessSubscriber)
                {
                    Log.LogInfoMessage("Upsale not allowed for All Access Emails without having an All Access Subscription. MemberID: " + request.MemberId.ToString(), ErrorHelper.GetCustomData());
                    throw new SparkAPIReportableException(Rest.V2.Serialization.HttpSubStatusCodes.HttpSub400StatusCode.MissingSubscription, "Upsale not allowed for All Access Emails without having an All Access Subscription. MemberID: " + request.MemberId.ToString());
                }

                List<int> fixedUpsalePlans = GetFixedPricingUpsalePackage(request.Brand.Site.SiteID);

                foreach (int planID in fixedUpsalePlans)
                {
                    Plan upsalePlan = PlanSA.Instance.GetPlan(planID, request.Brand.BrandID);

                    if (upsalePlan != null)
                    {
                        // The fixed pricing upsale package just contains all access emails that do not renew
                        // But in order to purchase all access emails, the member must have all access that has not yet expired 
                        // Also check the maximum number of all access emails that the member can purchase 
                        if ((upsalePlan.PremiumTypeMask & PremiumType.AllAccessEmail) == PremiumType.AllAccessEmail)
                        {
                            allowedUpsalePlanCollection.Add(upsalePlan);
                        }
                    }
                }
            }
            else
            {
                //VARIABLE PRICING UPSALE

                // Get the most recent Plan ID that the member is on  
                renewalSub = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(request.MemberId, request.Brand.Site.SiteID);
                RenewalServiceWebAdapter.CloseProxyInstance();

                if (renewalSub == null || renewalSub.RenewalSubscriptionID <= 0 || (renewalSub.RenewalDatePST < DateTime.Now))
                {
                    Log.LogInfoMessage("Error attempting to access upsale with no subscription. MemberID: " + request.MemberId.ToString() + ", SiteID: " + request.Brand.Site.SiteID.ToString(), ErrorHelper.GetCustomData());

                    //missing renewal or no sub privileges, not eligible for upgrade
                    throw new SparkAPIReportableException(Rest.V2.Serialization.HttpSubStatusCodes.HttpSub400StatusCode.MissingSubscription, "Error attempting to access upsale with no subscription. MemberID: " + request.MemberId.ToString() + ", SiteID: " + request.Brand.Site.SiteID.ToString());
                }

                int activePlanID = renewalSub.PrimaryPackageID;

                // Get the details of the most recent plan that the member is on  
                Spark.Common.CatalogService.Package activeSubscriptionPackage = Spark.Common.Adapter.CatalogServiceWebAdapter.GetProxyInstance().GetPackageDetails(activePlanID);
                Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();
                string upsaleFromType = "Standard";
                if (activeSubscriptionPackage.PackageType == Spark.Common.CatalogService.PackageType.Bundled)
                {
                    upsaleFromType = "BundledWithPremiumServices";
                }
                else if (activeSubscriptionPackage.PackageType == Spark.Common.CatalogService.PackageType.Basic)
                {
                    // Check to see if the member had purchased any a la carte premium service in addition to the
                    // standard plan that the member has most recently purchased
                    bool hasAtLeastOneActivePremiumService = false;

                    foreach (Spark.Common.AccessService.AccessPrivilege checkAccessPrivilege in activePrivileges)
                    {
                        if (checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.HighlightedProfile
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.SpotlightMember
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.JMeter
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.ReadReceipt)
                        {
                            hasAtLeastOneActivePremiumService = true;
                        }
                    }

                    if (hasAtLeastOneActivePremiumService)
                    {
                        upsaleFromType = "StandardWithAlacarte";
                    }
                    else
                    {
                        upsaleFromType = "Standard";
                    }
                }

                // Get the packages allowed for upsale for this member 
                Spark.Common.CatalogService.Item activeSubscriptionPackageBasicItem = null;
                foreach (Spark.Common.CatalogService.Item item in activeSubscriptionPackage.Items)
                {
                    List<Spark.Common.CatalogService.PrivilegeType> itemPrivileges = new List<Spark.Common.CatalogService.PrivilegeType>();
                    itemPrivileges.AddRange(item.PrivilegeType);
                    if (itemPrivileges.Contains(Spark.Common.CatalogService.PrivilegeType.BasicSubscription))
                    {
                        // Use the basic subscription item in the package to determine the duration and duration type of the package  
                        activeSubscriptionPackageBasicItem = item;
                        break;
                    }
                }
                int[] arrUpsalePackages = Spark.Common.Adapter.CatalogServiceWebAdapter.GetProxyInstance().GetUpsalePackages(activeSubscriptionPackageBasicItem.Duration, Enum.GetName(typeof(DurationType), activeSubscriptionPackageBasicItem.DurationType), request.Brand.Site.SiteID, upsaleFromType, activeSubscriptionPackageBasicItem.RenewalDuration, Enum.GetName(typeof(DurationType), activeSubscriptionPackageBasicItem.RenewalDurationType));
                Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();
                List<int> upsalePackages = new List<int>();
                upsalePackages.AddRange(arrUpsalePackages);

                List<Plan> colPlansAvailableForUpsale = new List<Plan>();
                foreach (int planID in upsalePackages)
                {
                    Plan upsalePlan = PlanSA.Instance.GetPlan(planID, request.Brand.BrandID);

                    if (upsalePlan != null)
                    {
                        colPlansAvailableForUpsale.Add(upsalePlan);
                    }
                }

                // Go through the upsale package list and remove the ones that are not allowed for purchase
                // since the member already has the maximum privilege time for upsale package  

                // There is an upsale package that contains both all access and renewable all access emails 
                // This package is only available for upsale if the all access expiration date is before the 
                // basic subscription expiration date 
                // There is no need to check the count for the all access email part of the package 
                // If the user wants to purchase more all access emails, they can do that as a fixed pricing upgrade 
                // In the fixed pricing upgrade purchase, there will be a check to see what the limit
                // of the all access emails are 
                foreach (Plan plan in colPlansAvailableForUpsale)
                {
                    if ((plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.HighlightedProfile, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.SpotlightMember, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.JMeter, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.AllAccess, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.ReadReceipt, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }

                }
            }

            // Order Attributes
            var orderAttributes = new Dictionary<string, string>
            {
                {"SRID", request.SrId},
                {
                    Common.MemberLevelTracking.Key.TrackingLastApplication.ToString(),
                    request.MemberLevelTrackingLastApplication
                },
                {
                    "DeviceProperties_IsMobileDevice",
                    (request.MemberLevelTrackingIsMobileDevice == null
                        ? "false"
                        : request.MemberLevelTrackingIsMobileDevice.ToLower())
                },
                {
                    "DeviceProperties_IsTablet", (request.MemberLevelTrackingIsTablet == null
                        ? "false"
                        : request.MemberLevelTrackingIsTablet.ToLower())
                }
            };

            if (!String.IsNullOrEmpty(request.EID))
                orderAttributes.Add("EID", request.EID);

            if (!string.IsNullOrEmpty(request.PrtId))
            {
                orderAttributes.Add("PRTID", request.PrtId);
                orderAttributes.Add("PricingGroupID", string.Empty);
                orderAttributes.Add("OriginalSubscriptionStatusBeforeUpsale", string.Empty);
                orderAttributes.Add("OriginalTemplateIDBeforeUpsale", string.Empty);
            }
            else if (orderInfo != null)
            {
                // Get these values from the previous purchase instead of the upsale purchase 
                orderAttributes.Add("PRTID", (orderInfo.PurchaseReasonTypeID > 0) ? Convert.ToString(orderInfo.PurchaseReasonTypeID) : string.Empty);
                orderAttributes.Add("SecondaryPRTIDs", (!string.IsNullOrEmpty(orderInfo.SecondaryPurchaseReasonTypeIDs)) ? orderInfo.SecondaryPurchaseReasonTypeIDs : string.Empty);
                orderAttributes.Add("PricingGroupID", (orderInfo.PromoID > 0) ? Convert.ToString(orderInfo.PromoID) : string.Empty);

                orderAttributes.Add("OriginalSubscriptionStatusBeforeUpsale", orderInfo.OriginalSubscriptionStatusBeforeUpsale);
                orderAttributes.Add("OriginalTemplateIDBeforeUpsale", Convert.ToString(orderInfo.OriginalTemplateIDBeforeUpsale));
            }
            else
            {
                orderAttributes.Add("PRTID", string.Empty);
                orderAttributes.Add("PricingGroupID", string.Empty);
                orderAttributes.Add("OriginalSubscriptionStatusBeforeUpsale", string.Empty);
                orderAttributes.Add("OriginalTemplateIDBeforeUpsale", string.Empty);
            }

            foreach (var key in orderAttributes.Keys)
            {
                if (!String.IsNullOrEmpty(orderAttributes[key]))
                    payment.Data.OrderAttributes += key + "=" + orderAttributes[key] + ",";
            }

            if (payment.Data.OrderAttributes.Length > 0)
                payment.Data.OrderAttributes = payment.Data.OrderAttributes.TrimEnd(',');

            // Member Info
            payment.Data.MemberInfo.CustomerID = request.MemberId;
            payment.Data.MemberInfo.RegionID = member.GetAttributeInt(request.Brand, "RegionID");
            payment.Data.MemberInfo.IPAddress = request.ClientIp;
            payment.Data.MemberInfo.Language =
                ((Language)Enum.Parse(typeof(Language), request.Brand.Site.LanguageID.ToString())).ToString();
            var regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID,
                (int)Language.English);
            if (regionLanguage.CountryRegionID == RegionDataAccess.RegionUs)
                payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation.Trim().ToUpper();

            //LC: Adding extra fields needed for Kount RIS
            payment.Data.MemberInfo.EmailAddress = member.GetAttributeText(request.Brand, "EmailAddress");
            payment.Data.MemberInfo.GenderMask = AttributeHelper.GetGenderDescription(request.Brand, member.GetAttributeInt(request.Brand, "gendermask"), false);
            payment.Data.MemberInfo.BirthDate = member.GetAttributeDate(request.Brand, "birthdate", DateTime.MinValue);
            payment.Data.MemberInfo.BrandInsertDate = member.GetAttributeDate(request.Brand, "BrandInsertDate", DateTime.MinValue);

            #region UDFS
            //JS-1228 LC: Adding UDF fields needed for Kount
            payment.Data.MemberInfo.UserName = member.GetUserName(request.Brand);
            payment.Data.MemberInfo.MaritalStatus = GetDescription("MaritalStatus", member.GetAttributeInt(request.Brand, "MaritalStatus"), request.Brand);
            payment.Data.MemberInfo.Occupation = member.GetAttributeText(request.Brand, "OccupationDescription");
            payment.Data.MemberInfo.Education = GetDescription("EducationLevel", member.GetAttributeInt(request.Brand, "EducationLevel"), request.Brand);
            payment.Data.MemberInfo.Eyes = GetDescription("EyeColor", member.GetAttributeInt(request.Brand, "EyeColor"), request.Brand);
            payment.Data.MemberInfo.PromotionID = member.GetAttributeInt(request.Brand, "PromotionID").ToString();
            payment.Data.MemberInfo.Hair = GetDescription("HairColor", member.GetAttributeInt(request.Brand, "HairColor"), request.Brand);
            payment.Data.MemberInfo.Height = member.GetAttributeInt(request.Brand, "Height").ToString();

            //TODO: Define webconstant to check Jdate community
            if (request.Brand.Site.Community.CommunityID == 3)
            {
                payment.Data.MemberInfo.Ethnicity = GetDescription("JDateEthnicity", member.GetAttributeInt(request.Brand, "JDateEthnicity"), request.Brand);
                payment.Data.MemberInfo.Religion = GetDescription("JDateReligion", member.GetAttributeInt(request.Brand, "JDateReligion"), request.Brand);
            }
            else
            {
                payment.Data.MemberInfo.Ethnicity = GetDescription("Ethnicity", member.GetAttributeInt(request.Brand, "Ethnicity"), request.Brand);
                payment.Data.MemberInfo.Religion = GetDescription("Religion", member.GetAttributeInt(request.Brand, "Religion"), request.Brand);
            }
            string aboutMe = member.GetAttributeText(request.Brand, "AboutMe");
            payment.Data.MemberInfo.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);

            #endregion UDF

            // Page Info
            var pageInfo = new PaymentJsonArray { { "PRTTitle", request.PrtTitle } };
            payment.Data.PageInfo.Add(pageInfo);

            #region Omniture Variables

            var omnitureVariablesRequest = request.OmnitureVariables;

            var omnitureVariables = new PaymentJsonArray();

            foreach (var variable in omnitureVariablesRequest)
            {
                omnitureVariables.Add(variable.Key, variable.Value);
            }

            omnitureVariables.Remove("s_account");
            omnitureVariables.Add("s_account", GetOmnitureReportSuite(request.Brand));
            omnitureVariables.Remove("pageName");
            omnitureVariables.Add("pageName", "m_upsale");
            omnitureVariables.Remove("events");
            if (payment.Data.OrderConfirmation.Count > 0)
            {
                omnitureVariables.Add("events", "purchase,event48,event10");
            }
            else
            {
                omnitureVariables.Add("events", "purchase,event48");
            }
            omnitureVariables.Remove("prop17");
            omnitureVariables.Add("prop17", OmnitureHelper.GetProfileCompetionPercentage(member, request.Brand));
            omnitureVariables.Remove("prop18");
            omnitureVariables.Add("prop18", OmnitureHelper.GetGender(member.GetAttributeInt(request.Brand, "gendermask")));
            omnitureVariables.Remove("prop19");
            omnitureVariables.Add("prop19", OmnitureHelper.GetAge(member.GetAttributeDate(request.Brand, "Birthdate")).ToString());
            omnitureVariables.Remove("prop20");
            if (request.Brand.Site.Community.CommunityID == (int)CommunityId.JDate)
            {
                omnitureVariables.Add("prop20", MemberHelper.GetDescription("JDateEthnicity", member.GetAttributeInt(request.Brand, "JDateEthnicity"),
                        request.Brand));
            }
            else
            {
                omnitureVariables.Add("prop20", MemberHelper.GetDescription("Ethnicity", member.GetAttributeInt(request.Brand, "Ethnicity"),
                        request.Brand));
            }
            omnitureVariables.Remove("prop21");
            omnitureVariables.Add("prop21", OmnitureHelper.GetRegionString(member, request.Brand, request.Brand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
            omnitureVariables.Remove("prop23");
            omnitureVariables.Add("prop23", request.MemberId.ToString());

            omnitureVariables.Remove("eVar4");
            omnitureVariables.Add("eVar4", orderAttributes["PricingGroupID"]);
            omnitureVariables.Remove("purchaseID");
            omnitureVariables.Add("purchaseID", request.OrderID);
            omnitureVariables.Remove("eVar31");
            omnitureVariables.Add("eVar31", orderAttributes["OriginalTemplateIDBeforeUpsale"]);
            omnitureVariables.Remove("eVar36");
            omnitureVariables.Add("eVar36", GetOmnitureSiteURL(request.Brand, request.PromoType));
            omnitureVariables.Remove("eVar44");
            omnitureVariables.Add("eVar44", request.MemberId.ToString());

            if (orderInfo != null)
            {
                Matchnet.Purchase.ValueObjects.Plan primaryPlan = UpsGetPrimaryPlan(orderInfo, request.Brand);
                if (primaryPlan != null)
                {
                    omnitureVariables.Remove("products");
                    omnitureVariables.Add("products", primaryPlan.PlanID.ToString() + ";" + primaryPlan.InitialDuration.ToString() + primaryPlan.InitialDurationType.ToString() + ";" + primaryPlan.RenewDuration
                        + ";" + orderInfo.TotalAmount);
                }
            }
            else
            {
                omnitureVariables.Remove("products");
                omnitureVariables.Add("products", String.Empty);
            }

            payment.Data.OmnitureVariables.Add(omnitureVariables);

            #endregion

            #region Analytics Variables (Tealium)
            var analyticVariablesRequest = request.AnalyticVariables;

            var analyticVariables = new PaymentJsonArray();
            analyticVariables.Add("eId", string.IsNullOrEmpty(request.EID) ? "" : request.EID);

            foreach (var variable in analyticVariablesRequest)
            {
                analyticVariables.Remove(variable.Key);
                analyticVariables.Add(variable.Key, variable.Value);
            }

            var analyticAttributeSetResult = ProfileAccess.GetAttributeSetWithPhotoAttributes(request.Brand, member.MemberID, member.MemberID, "tealiumtargetingprofile_us", false, "");
            if (analyticAttributeSetResult != null && analyticAttributeSetResult.AttributeSet != null)
            {
                foreach (KeyValuePair<string, object> entry in analyticAttributeSetResult.AttributeSet)
                {
                    analyticVariables.Remove(entry.Key);
                    analyticVariables.Add(entry.Key, entry.Value);
                }

            }

            analyticVariables.Remove("tealiumUrl");
            analyticVariables.Add("tealiumUrl", SettingsService.GetSettingFromSingleton("TEALIUM_SRC",
                request.Brand.Site.Community.CommunityID,
                request.Brand.Site.SiteID,
                request.Brand.BrandID));

            payment.Data.AnalyticVariables.Add(analyticVariables);

            #endregion

            // Legacy Data Info
            payment.Data.LegacyDataInfo.Member = request.MemberId;
            payment.Data.LegacyDataInfo.BrandID = request.BrandId;
            payment.Data.LegacyDataInfo.SiteID = request.Brand.Site.SiteID;

            if (request is AdminPaymentUiPostDataRequest)
            {
                payment.Data.LegacyDataInfo.AdminMemberID = (request as AdminPaymentUiPostDataRequest).AdminMemberID;
            }

            // A La Carte Plans
            foreach (Plan plan in allowedUpsalePlanCollection)
            {
                if (plan.EndDate != DateTime.MinValue)
                    continue;

                PaymentJsonPackage package;
                package = new PaymentJsonPackage();

                if (isFixedPricingUpsale)
                {
                    package.ID = plan.PlanID;
                    package.Description = "";
                    package.SytemID = request.Brand.Site.SiteID;
                    package.StartDate = DateTime.MinValue;
                    package.ExpiredDate = DateTime.MinValue;
                    package.CurrencyType = GetCurrencyAbbreviation(plan.CurrencyType);

                    package.Items.Add("InitialCostPerDuration", plan.InitialCost);
                    package.Items.Add("ProratedAmount", plan.InitialCost);
                    package.Items.Add("DurationType", "None");
                    package.Items.Add("Duration", 0);
                    package.Items.Add("PlanID", plan.PlanID);
                    package.Items.Add("CurrencyType", GetCurrencyAbbreviation(plan.CurrencyType));
                    package.Items.Add("PaymentTypeMask", plan.PaymentTypeMask);
                    package.Items.Add("CreditAmount", (plan.CreditAmount < 0) ? 0 : plan.CreditAmount);
                    package.Items.Add("PurchaseMode", PurchaseMode.New.ToString());
                    package.Items.Add("PremiumType", plan.PremiumTypeMask.ToString());
                    package.Items.Add("RenewCost", plan.RenewCost);

                    Spark.Common.CatalogService.Package emailPackage = Spark.Common.Adapter.CatalogServiceWebAdapter.GetProxyInstance().GetPackageDetails(plan.PlanID);
                    Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();

                    foreach (Spark.Common.CatalogService.Item emailPackageItem in emailPackage.Items)
                    {
                        foreach (Spark.Common.CatalogService.PrivilegeType emailPackageItemPrivilege in emailPackageItem.PrivilegeType)
                        {
                            if (emailPackageItemPrivilege == Spark.Common.CatalogService.PrivilegeType.AllAccessEmails)
                            {
                                package.Items.Add("DisburseCount", emailPackageItem.DisburseCount);
                                package.Items.Add("DisburseDuration", emailPackageItem.DisburseDuration);
                                package.Items.Add("DisburseDurationType", Enum.GetName(typeof(Spark.Common.CatalogService.DurationType), emailPackageItem.DisburseDurationType));
                            }
                        }
                    }

                    if (request.PremiumType == PremiumType.None)
                    {
                        payment.Data.Packages.Add(package);
                    }
                    else if ((plan.PremiumTypeMask & request.PremiumType) == request.PremiumType)
                    {
                        payment.Data.Packages.Add(package);
                    }
                }
                else
                {
                    package.ID = plan.PlanID;
                    package.Description = "";
                    package.SytemID = request.Brand.Site.SiteID;
                    package.StartDate = renewalSub.RenewalDatePST;// memberSub.EndDate;
                    package.ExpiredDate = renewalSub.RenewalDatePST;// memberSub.EndDate;
                    package.CurrencyType = GetCurrencyAbbreviation(plan.CurrencyType);

                    package.Items.Add("InitialCostPerDuration", plan.InitialCost);
                    decimal proratedAmount = GetMemberProratedAmount(plan.InitialCost, renewalSub.RenewalDatePST, GetStartDateForUpsalePlan(plan, activePrivileges));
                    package.Items.Add("ProratedAmount", proratedAmount);
                    package.Items.Add("DurationType", "Day");
                    package.Items.Add("Duration", GetDuration(renewalSub.RenewalDatePST, GetStartDateForUpsalePlan(plan, activePrivileges)));
                    package.Items.Add("PlanID", plan.PlanID);
                    package.Items.Add("CurrencyType", GetCurrencyAbbreviation(plan.CurrencyType));
                    package.Items.Add("PaymentTypeMask", plan.PaymentTypeMask);
                    package.Items.Add("CreditAmount", (plan.CreditAmount < 0) ? 0 : plan.CreditAmount);
                    package.Items.Add("PurchaseMode", PurchaseMode.New.ToString());
                    package.Items.Add("PremiumType", plan.PremiumTypeMask.ToString());
                    package.Items.Add("RenewCost", plan.RenewCost);

                    // If the package for upsale has amount of 0, do not display this package. 
                    if (proratedAmount > 0)
                    {
                        if (request.PremiumType == PremiumType.None)
                        {
                            payment.Data.Packages.Add(package);
                        }
                        else if ((plan.PremiumTypeMask & request.PremiumType) == request.PremiumType)
                        {
                            payment.Data.Packages.Add(package);
                        }
                    }
                }
            };

            if (payment.Data.Packages.Count <= 0)
            {
                Log.LogInfoMessage("No upsale packages available. MemberID: " + request.MemberId.ToString() + ", SiteID: " + request.Brand.Site.SiteID.ToString(), ErrorHelper.GetCustomData());
                throw new SparkAPIReportableException(Rest.V2.Serialization.HttpSubStatusCodes.HttpSub400StatusCode.NoPackagesFound, "No upsale packages available. MemberID: " + request.MemberId.ToString() + ", SiteID: " + request.Brand.Site.SiteID.ToString());
            }

            var json = JsonConvert.SerializeObject(payment);
            Log.LogInfoMessage("PaymentJson upsale string to PUI: " + json, ErrorHelper.GetCustomData());
            PurchaseSA.Instance.UPSLegacyDataSave(json.Compress(), upsLegacyDataId);

            return json;
        }

        #region Order Confirmation
        //Bundles the needed Order confirmation info for upsale page
        private static PaymentJsonArray GetOrderConfirmation(Brand brand, int orderID, Member member,
            ObsfucatedPaymentProfileResponse paymentProfileResponse, OrderInfo orderInfo, Plan primaryPlan)
        {
            const int SERVICEFEE_ITEM_ID = 1004;
            const int DISCOUNT_ITEM_ID = 100;
            const int REMAINING_CREDIT_ITEM_ID = 5000;

            string orderCustomerID = Constants.NULL_STRING;
            string orderConfirmationNumber = Constants.NULL_STRING;
            string orderCreditCardNumber = Constants.NULL_STRING;
            string orderCreditCardType = Constants.NULL_STRING;
            string orderTransactionDate = DateTime.Now.ToString();
            string orderPaymentType = Constants.NULL_STRING;
            string orderTotalAmount = Constants.NULL_STRING;
            string orderPricingGroupID = Constants.NULL_STRING;
            string orderPRTID = Constants.NULL_STRING;
            string subscriptionType = Constants.NULL_STRING;
            string itemSold = Constants.NULL_STRING;
            string packageID = Constants.NULL_STRING;
            string duration = Constants.NULL_STRING;

            orderConfirmationNumber = orderID.ToString();
            orderCustomerID = member.GetAttributeText(brand, "UserName");
            orderPaymentType = paymentProfileResponse.PaymentProfile.PaymentType;
            orderTotalAmount = orderInfo.TotalAmount.ToString();
            orderTransactionDate = DateTime.Now.ToString();
            // JS-1701 Adding additional fields needed for Tealium
            orderPricingGroupID = (orderInfo.PromoID > 0) ? Convert.ToString(orderInfo.PromoID) : Constants.NULL_STRING;
            orderPRTID = (orderInfo.PurchaseReasonTypeID > 0)? Convert.ToString(orderInfo.PurchaseReasonTypeID): Constants.NULL_STRING;
            duration = primaryPlan.InitialDuration.ToString() + primaryPlan.InitialDurationType.ToString().Substring(0, 1);
           
            if (paymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile)
            {
                ObsfucatedCreditCardPaymentProfile ccPaymentProfile =
                    paymentProfileResponse.PaymentProfile as ObsfucatedCreditCardPaymentProfile;
                orderCreditCardType = ccPaymentProfile.CardType;
                orderCreditCardNumber = ccPaymentProfile.LastFourDigitsCreditCardNumber;
            }
            else if (paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormFullWebSitePaymentProfile)
            {
                ObfuscatedCreditCardShortFormFullWebSitePaymentProfile ccShortFormFullWebSitePaymentProfile =
                    paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormFullWebSitePaymentProfile;
                orderCreditCardType = ccShortFormFullWebSitePaymentProfile.CardType;
                orderCreditCardNumber = ccShortFormFullWebSitePaymentProfile.LastFourDigitsCreditCardNumber;
            }
            else if (paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormMobileSitePaymentProfile)
            {
                ObfuscatedCreditCardShortFormMobileSitePaymentProfile ccShortFormMobileSitePaymentProfile =
                    paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormMobileSitePaymentProfile;
                orderCreditCardType = ccShortFormMobileSitePaymentProfile.CardType;
                orderCreditCardNumber = ccShortFormMobileSitePaymentProfile.LastFourDigitsCreditCardNumber;
            }
            else if (paymentProfileResponse.PaymentProfile is ObfuscatedDebitCardPaymentProfile)
            {
                ObfuscatedDebitCardPaymentProfile debitCardPaymentProfile =
                    paymentProfileResponse.PaymentProfile as ObfuscatedDebitCardPaymentProfile;
                orderCreditCardType = debitCardPaymentProfile.CardType;
                orderCreditCardNumber = debitCardPaymentProfile.LastFourDigitsCreditCardNumber;
            }
           
            // SubscriptionType subType =  SubscriptionType.NONE;
            if (orderInfo.OrderDetail.Length == 1)
            {
                subscriptionType = orderInfo.OrderDetail[0].ItemID == 1 ? SubscriptionType.STANDARD.ToString() : SubscriptionType.ALC.ToString();
                packageID =  orderInfo.OrderDetail[0].PackageID.ToString();
                itemSold = orderInfo.OrderDetail[0].ItemID.ToString();
            }
            else
            {
                StringBuilder itemsBuilder = new StringBuilder("");
                StringBuilder packageBuilder = new StringBuilder("");

                bool isItemIdOne = false;
                bool isItemServicefee = false;
                bool isDiscountItem = false;
                bool isRemaingCredit = false;
                bool isAnyotherItem = false;
              
                foreach (var odi in orderInfo.OrderDetail)
                {
                    if (odi.ItemID == 1)
                    {
                        isItemIdOne = true;
                    }
                    else if (odi.ItemID == 1004)
                    {
                        isItemServicefee = true;
                    }
                    else if(odi.ItemID == 5000)
                    {
                        isDiscountItem = true;
                    }
                    else if (odi.ItemID == 100)
                    {
                        isRemaingCredit = true;
                    }
                    else
                    {
                        isAnyotherItem = true;
                    }
                    itemsBuilder.Append(odi.ItemID + ",");
                    packageBuilder.Append(odi.PackageID + ",");
                }
                itemsBuilder.Remove(itemsBuilder.Length - 1, 1);
                packageBuilder.Remove(packageBuilder.Length - 1, 1);
                itemSold =  itemsBuilder.ToString();
                packageID = packageBuilder.ToString();
               
                if (isItemIdOne && isAnyotherItem)
                {
                    subscriptionType = SubscriptionType.PREMIUM.ToString();
                }
                else if (!isItemIdOne && isAnyotherItem)
                {
                    subscriptionType = SubscriptionType.ALC.ToString();
                }
                else if(isItemIdOne) 
                {
                    subscriptionType = SubscriptionType.STANDARD.ToString();
                }
            }

            PaymentJsonArray orderConfirmation = new PaymentJsonArray();

            orderConfirmation.Add("orderCustomerID", orderCustomerID);
            orderConfirmation.Add("orderConfirmationNumber", orderConfirmationNumber);
            orderConfirmation.Add("orderCreditCardNumber", orderCreditCardNumber);
            orderConfirmation.Add("orderCreditCardType", orderCreditCardType);
            orderConfirmation.Add("orderTransactionDate", orderTransactionDate);
            orderConfirmation.Add("orderPaymentType", orderPaymentType);
            orderConfirmation.Add("orderTotalAmount", orderTotalAmount);

            // JS-1701 Adding additional fields needed for Tealium
            orderConfirmation.Add("orderPricingGroupID", orderPricingGroupID);
            orderConfirmation.Add("orderPRTID", orderPRTID);
            orderConfirmation.Add("duration", duration);

            orderConfirmation.Add("packageID", packageID);
            orderConfirmation.Add("itemSold", itemSold);
            orderConfirmation.Add("subscriptionType", subscriptionType);

            return orderConfirmation;
        }
        #endregion



        public string GetJsonPaymentUiPostDataPremiumServices(PaymentUiPostDataRequest request, int globalLogId)
        {

            if (request.PaymentType == PaymentType.None)
            {
                request.PaymentType = PaymentType.CreditCard;
            }

            var upsLegacyDataId = KeySA.Instance.GetKey("UPSLegacyDataID");
            OrderInfo orderInfo = null;

            var member = MemberSA.Instance.GetMember(request.MemberId, MemberLoadFlags.None);
            if (member == null || member.MemberID <= 0)
            {
                Log.LogInfoMessage("Member not found for ID: " + request.MemberId.ToString(), ErrorHelper.GetCustomData());
                throw new SparkAPIReportableException(Rest.V2.Serialization.HttpSubStatusCodes.HttpSub400StatusCode.InvalidMemberId, "Member not found for ID: " + request.MemberId.ToString());
            }

            //  121 is the default mobile premium services template
            var templateId = (request.TemplateId <= 0) ? 121 : request.TemplateId;

            var payment = new PaymentJson
            {
                UPSLegacyDataID = upsLegacyDataId,
                UPSGlobalLogID = globalLogId,
                GetCustomerPaymentProfile = 1,
                Version = 2,
                CallingSystemID = request.Brand.Site.SiteID,
                TemplateID = templateId,
                TimeStamp = DateTime.Now
            };

            payment.Data.Navigation.CancelURL = request.CancelUrl;
            payment.Data.Navigation.ReturnURL = request.ReturnUrl;
            payment.Data.Navigation.ConfirmationURL = request.ConfirmationUrl;
            payment.Data.Navigation.DestinationURL = request.DestinationUrl;
            payment.IsSubscriptionConfirmationEmailEnabled = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("ENABLE_SUBSCRIPTION_CONFIRMATION_EMAIL", request.Brand.Site.Community.CommunityID, request.Brand.Site.SiteID, request.Brand.BrandID));
            try
            {
                payment.IsUPSConfirm = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("UPS_CONFIRM", request.Brand.Site.Community.CommunityID, request.Brand.Site.SiteID, request.Brand.BrandID));
            }
            catch
            {
                payment.IsUPSConfirm = false;
            }

            if (payment.IsUPSConfirm == false)
            {
                //always true for norbert users
                if (member.GetAttributeInt(request.Brand, "REDRedesignBetaParticipatingFlag") == 1)
                {
                    payment.IsUPSConfirm = true;
                }
            }

            // Kount merchant ID
            payment.KountMerchantID = GetKountMerchantID(request.Brand);

            // Get all the privileges that have not expired for this member for this site  
            AccessPrivilege[] accessPrivileges = AccessServiceWebAdapter.GetProxyInstanceForBedrock().GetCustomerPrivilegeListAllWithIgnoreCache(request.MemberId);
            AccessServiceWebAdapter.CloseProxyInstance();

            List<Spark.Common.AccessService.AccessPrivilege> activePrivileges = new List<Spark.Common.AccessService.AccessPrivilege>();
            foreach (Spark.Common.AccessService.AccessPrivilege checkAccessPrivilege in accessPrivileges)
            {
                if (checkAccessPrivilege.CallingSystemID == request.Brand.Site.SiteID)
                {
                    if (checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.BasicSubscription
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.HighlightedProfile
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.SpotlightMember
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.JMeter
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.ReadReceipt)
                    {
                        if (checkAccessPrivilege.EndDatePST > DateTime.Now)
                        {
                            activePrivileges.Add(checkAccessPrivilege);
                        }
                    }
                }
            }

            // Get renewal subscription info
            Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(request.MemberId, request.Brand.Site.SiteID);
            RenewalServiceWebAdapter.CloseProxyInstance();

            if (renewalSub == null || renewalSub.RenewalSubscriptionID <= 0 || (renewalSub.RenewalDatePST < DateTime.Now))
            {
                //missing renewal or no sub privileges, not eligible for upgrade
                Log.LogInfoMessage("Error attempting to access premium services with no subscription. MemberID: " + request.MemberId.ToString() + ", SiteID: " + request.Brand.Site.SiteID.ToString(), ErrorHelper.GetCustomData());
                throw new SparkAPIReportableException(Rest.V2.Serialization.HttpSubStatusCodes.HttpSub400StatusCode.MissingSubscription, "Error attempting to access premium services with no subscription. MemberID: " + request.MemberId.ToString() + ", SiteID: " + request.Brand.Site.SiteID.ToString());
            }

            int activePlanID = renewalSub.PrimaryPackageID;

            // Get the details of the most recent plan that the member is on  
            Spark.Common.CatalogService.Package activeSubscriptionPackage = Spark.Common.Adapter.CatalogServiceWebAdapter.GetProxyInstance().GetPackageDetails(activePlanID);
            Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();
            string upsaleFromType = "Standard";
            if (activeSubscriptionPackage.PackageType == Spark.Common.CatalogService.PackageType.Bundled)
            {
                upsaleFromType = "BundledWithPremiumServices";
            }
            else if (activeSubscriptionPackage.PackageType == Spark.Common.CatalogService.PackageType.Basic)
            {
                // Check to see if the member had purchased any a la carte premium service in addition to the
                // standard plan that the member has most recently purchased
                bool hasAtLeastOneActivePremiumService = false;

                foreach (Spark.Common.AccessService.AccessPrivilege checkAccessPrivilege in activePrivileges)
                {
                    if (checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.HighlightedProfile
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.SpotlightMember
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.JMeter
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess
                        || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.ReadReceipt)
                    {
                        hasAtLeastOneActivePremiumService = true;
                    }

                }

                if (hasAtLeastOneActivePremiumService)
                {
                    upsaleFromType = "StandardWithAlacarte";
                }
                else
                {
                    upsaleFromType = "Standard";
                }
            }

            //get premium a la carte plans
            PlanCollection originalCollection = PlanSA.Instance.GetPlans(request.Brand.BrandID, PlanType.ALaCarte, request.PaymentType);

            if (originalCollection == null)
                throw new Exception("Plancollection cannot be null.");

            List<Plan> planCollection = new List<Plan>();

            //highlight
            var highlightPlan = (from Plan p in originalCollection
                                 where p.PremiumTypeMask == PremiumType.HighlightedProfile
                                 orderby p.UpdateDate descending
                                 select p).Take(1).ToList();

            if (highlightPlan.Count == 1)
            {
                if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.HighlightedProfile, renewalSub.RenewalDatePST))
                {
                    planCollection.Add(highlightPlan[0]);
                }
            }

            //spotlight
            var spotlightPlan = (from Plan p in originalCollection
                                 where p.PremiumTypeMask == PremiumType.SpotlightMember
                                 orderby p.UpdateDate descending
                                 select p).Take(1).ToList();

            if (spotlightPlan.Count == 1)
            {
                if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.SpotlightMember, renewalSub.RenewalDatePST))
                {
                    planCollection.Add(spotlightPlan[0]);
                }
            }

            //read receipt and bundles with read receipt
            PremiumType bundledServices = PremiumType.None;

            if (Convert.ToBoolean(SettingsService.GetSettingFromSingleton("ENABLE_READ_RECEIPTS_PREMIUM_SERVICE_FEATURE", request.Brand.Site.Community.CommunityID, request.Brand.Site.SiteID, request.Brand.BrandID)))
            {
                var readReceiptPlan = (from Plan p in originalCollection
                                       where p.PremiumTypeMask == PremiumType.ReadReceipt
                                       orderby p.UpdateDate descending
                                       select p).Take(1).ToList();

                if (readReceiptPlan.Count == 1)
                {
                    if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.ReadReceipt, renewalSub.RenewalDatePST))
                    {
                        planCollection.Add(readReceiptPlan[0]);
                    }
                }

                bundledServices = PremiumType.SpotlightMember | PremiumType.HighlightedProfile | PremiumType.ReadReceipt;
                var bundledSpotlightMemberHighlightedProfileReadReceiptPlan = (from Plan p in originalCollection
                                                                               where p.PremiumTypeMask == bundledServices
                                                                               orderby p.UpdateDate descending
                                                                               select p).Take(1).ToList();

                if (bundledSpotlightMemberHighlightedProfileReadReceiptPlan.Count == 1)
                {
                    if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.SpotlightMember, renewalSub.RenewalDatePST)
                        && IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.HighlightedProfile, renewalSub.RenewalDatePST)
                        && IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.ReadReceipt, renewalSub.RenewalDatePST))
                    {
                        planCollection.Add(bundledSpotlightMemberHighlightedProfileReadReceiptPlan[0]);
                    }
                }

                bundledServices = PremiumType.SpotlightMember | PremiumType.ReadReceipt;
                var bundledSpotlightMemberReadReceiptPlan = (from Plan p in originalCollection
                                                             where p.PremiumTypeMask == bundledServices
                                                             orderby p.UpdateDate descending
                                                             select p).Take(1).ToList();

                if (bundledSpotlightMemberReadReceiptPlan.Count == 1)
                {
                    if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.SpotlightMember, renewalSub.RenewalDatePST)
                        && IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.ReadReceipt, renewalSub.RenewalDatePST))
                    {
                        planCollection.Add(bundledSpotlightMemberReadReceiptPlan[0]);
                    }
                }

                bundledServices = PremiumType.HighlightedProfile | PremiumType.ReadReceipt;
                var bundledHighlightedProfileReadReceiptPlan = (from Plan p in originalCollection
                                                                where p.PremiumTypeMask == bundledServices
                                                                orderby p.UpdateDate descending
                                                                select p).Take(1).ToList();

                if (bundledHighlightedProfileReadReceiptPlan.Count == 1)
                {
                    if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.HighlightedProfile, renewalSub.RenewalDatePST)
                        && IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.ReadReceipt, renewalSub.RenewalDatePST))
                    {
                        planCollection.Add(bundledHighlightedProfileReadReceiptPlan[0]);
                    }
                }

                bundledServices = PremiumType.SpotlightMember | PremiumType.HighlightedProfile | PremiumType.ReadReceipt | PremiumType.AllAccess;
                var bundledSpotlightMemberHighlightedProfileReadReceiptAllAccessPlan = (from Plan p in originalCollection
                                                                                        where p.PremiumTypeMask == bundledServices
                                                                                        orderby p.UpdateDate descending
                                                                                        select p).Take(1).ToList();

                if (bundledSpotlightMemberHighlightedProfileReadReceiptAllAccessPlan.Count == 1)
                {
                    if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.SpotlightMember, renewalSub.RenewalDatePST)
                        && IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.HighlightedProfile, renewalSub.RenewalDatePST)
                        && IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.ReadReceipt, renewalSub.RenewalDatePST)
                        && IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.AllAccess, renewalSub.RenewalDatePST))
                    {
                        planCollection.Add(bundledSpotlightMemberHighlightedProfileReadReceiptAllAccessPlan[0]);
                    }
                }

            }

            //spotlight and highlight bundle
            bundledServices = PremiumType.SpotlightMember | PremiumType.HighlightedProfile;
            var bundledSpotlightMemberHighlightedProfilePlan = (from Plan p in originalCollection
                                                                where p.PremiumTypeMask == bundledServices
                                                                orderby p.UpdateDate descending
                                                                select p).Take(1).ToList();

            if (bundledSpotlightMemberHighlightedProfilePlan.Count == 1)
            {
                if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.SpotlightMember, renewalSub.RenewalDatePST)
                    && IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.HighlightedProfile, renewalSub.RenewalDatePST))
                {
                    planCollection.Add(bundledSpotlightMemberHighlightedProfilePlan[0]);
                }
            }

            // There is an upsale package that contains both all access and renewable all access emails 
            // This package is only available for upsale if the all access expiration date is before the 
            // basic subscription expiration date 
            // There is no need to check the count for the all access email part of the package 
            // If the user wants to purchase more all access emails, they can do that as a fixed pricing upgrade 
            // In the fixed pricing upgrade purchase, there will be a check to see what the limit
            // of the all access emails are  
            Spark.Common.CatalogService.Item activeSubscriptionPackageBasicItem = null;
            foreach (Spark.Common.CatalogService.Item item in activeSubscriptionPackage.Items)
            {
                List<Spark.Common.CatalogService.PrivilegeType> itemPrivileges = new List<Spark.Common.CatalogService.PrivilegeType>();
                itemPrivileges.AddRange(item.PrivilegeType);
                if (itemPrivileges.Contains(Spark.Common.CatalogService.PrivilegeType.BasicSubscription))
                {
                    // Use the basic subscription item in the package to determine the duration and duration type of the package  
                    activeSubscriptionPackageBasicItem = item;
                    break;
                }
            }

            int[] arrUpsalePackages = Spark.Common.Adapter.CatalogServiceWebAdapter.GetProxyInstance().GetUpsalePackages(activeSubscriptionPackageBasicItem.Duration, Enum.GetName(typeof(DurationType), activeSubscriptionPackageBasicItem.DurationType), request.Brand.Site.SiteID, upsaleFromType, activeSubscriptionPackageBasicItem.RenewalDuration, Enum.GetName(typeof(DurationType), activeSubscriptionPackageBasicItem.RenewalDurationType));
            Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();
            List<int> upsalePackages = new List<int>();
            upsalePackages.AddRange(arrUpsalePackages);

            List<Plan> colPlansAvailableForUpsale = new List<Plan>();

            var isJmeterUpsaleEnabled =
                           Convert.ToBoolean(SettingsService.GetSettingFromSingleton("IS_JMETER_UPSALE_ENABLED",
                               request.Brand.Site.Community.CommunityID, request.Brand.Site.SiteID,
                               request.Brand.BrandID));
            Log.LogInfoMessage(string.Format("isJmeterUpsaleEnabled={0}", isJmeterUpsaleEnabled),
                ErrorHelper.GetCustomData());
            Log.LogInfoMessage(string.Format("upsalePackages={0}", upsalePackages.Count),
                ErrorHelper.GetCustomData());
            foreach (int planID in upsalePackages)
            {
                Plan upsalePlan = PlanSA.Instance.GetPlan(planID, request.Brand.BrandID);

                if (upsalePlan != null)
                {
                    if (request.Brand.Site.SiteID == (int) SITE_ID.JDateCoIL) // IL
                    {
                        if (isJmeterUpsaleEnabled)
                        {
                            if ((upsalePlan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter
                                || (upsalePlan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                            {
                                colPlansAvailableForUpsale.Add(upsalePlan);
                            }
                        }
                        //if JMETER is disabled, All Access plan is offered instead 
                        else
                        {
                            Log.LogInfoMessage(string.Format("upsale is allaccess={0}", upsalePlan.PremiumTypeMask & PremiumType.AllAccessEmail),
                                ErrorHelper.GetCustomData());
                            if ((upsalePlan.PremiumTypeMask & PremiumType.AllAccessEmail) == PremiumType.AllAccessEmail)
                                colPlansAvailableForUpsale.Add(upsalePlan);
                        }
                    }
                    else
                    {
                        colPlansAvailableForUpsale.Add(upsalePlan);
                    }
                }
            }

            foreach (Plan plan in colPlansAvailableForUpsale)
            {
                if ((plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                {
                    if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.AllAccess, renewalSub.RenewalDatePST))
                    {
                        planCollection.Add(plan);
                    }
                }
                else if ((plan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
                {
                    if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.JMeter, renewalSub.RenewalDatePST))
                    {
                        planCollection.Add(plan);
                    }
                }
            }

            // Order Attributes
            var orderAttributes = new Dictionary<string, string>
            {
                {"SRID", request.SrId},
                {"PRTID", request.PrtId},
                {
                    Common.MemberLevelTracking.Key.TrackingLastApplication.ToString(),
                    request.MemberLevelTrackingLastApplication
                },
                {
                    "DeviceProperties_IsMobileDevice",
                    (request.MemberLevelTrackingIsMobileDevice == null
                        ? "false"
                        : request.MemberLevelTrackingIsMobileDevice.ToLower())
                },
                {
                    "DeviceProperties_IsTablet", (request.MemberLevelTrackingIsTablet == null
                        ? "false"
                        : request.MemberLevelTrackingIsTablet.ToLower())
                }
            };

            if (!String.IsNullOrEmpty(request.EID))
                orderAttributes.Add("EID", request.EID);

            foreach (string key in orderAttributes.Keys)
            {
                if (orderAttributes[key] != string.Empty)
                    payment.Data.OrderAttributes += key + "=" + orderAttributes[key] + ",";
            }

            if (payment.Data.OrderAttributes.Length > 0)
                payment.Data.OrderAttributes = payment.Data.OrderAttributes.TrimEnd(',');

            // Member Info
            payment.Data.MemberInfo.CustomerID = request.MemberId;
            payment.Data.MemberInfo.RegionID = member.GetAttributeInt(request.Brand, "RegionID");
            if (payment.Data.MemberInfo.RegionID == Constants.NULL_INT
                || payment.Data.MemberInfo.RegionID < 0)
            {
                // There must be a valid RegionID to price the cart so get it from the database if the cache does not have a valid RegionID  
                Member refreshedMember = MemberSA.Instance.GetMember(35, MemberLoadFlags.IngoreSACache);
                payment.Data.MemberInfo.RegionID = refreshedMember.GetAttributeInt(request.Brand, "RegionID");
            }
            payment.Data.MemberInfo.IPAddress = request.ClientIp;
            payment.Data.MemberInfo.Language = ((Language)Enum.Parse(typeof(Language), request.Brand.Site.LanguageID.ToString())).ToString();

            Matchnet.Content.ValueObjects.Region.RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID, (int)Language.English);
            if (regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation.Trim().ToUpper();

            //LC: Adding extra fields needed for Kount RIS
            payment.Data.MemberInfo.EmailAddress = member.GetAttributeText(request.Brand, "EmailAddress");
            payment.Data.MemberInfo.GenderMask = AttributeHelper.GetGenderDescription(request.Brand, member.GetAttributeInt(request.Brand, "gendermask"), false);
            payment.Data.MemberInfo.BirthDate = member.GetAttributeDate(request.Brand, "birthdate", DateTime.MinValue);
            payment.Data.MemberInfo.BrandInsertDate = member.GetAttributeDate(request.Brand, "BrandInsertDate", DateTime.MinValue);

            #region UDFS
            //JS-1228 LC: Adding UDF fields needed for Kount
            payment.Data.MemberInfo.UserName = member.GetUserName(request.Brand);
            payment.Data.MemberInfo.MaritalStatus = GetDescription("MaritalStatus", member.GetAttributeInt(request.Brand, "MaritalStatus"), request.Brand);
            payment.Data.MemberInfo.Occupation = member.GetAttributeText(request.Brand, "OccupationDescription");
            payment.Data.MemberInfo.Education = GetDescription("EducationLevel", member.GetAttributeInt(request.Brand, "EducationLevel"), request.Brand);
            payment.Data.MemberInfo.Eyes = GetDescription("EyeColor", member.GetAttributeInt(request.Brand, "EyeColor"), request.Brand);
            payment.Data.MemberInfo.PromotionID = member.GetAttributeInt(request.Brand, "PromotionID").ToString();
            payment.Data.MemberInfo.Hair = GetDescription("HairColor", member.GetAttributeInt(request.Brand, "HairColor"), request.Brand);
            payment.Data.MemberInfo.Height = member.GetAttributeInt(request.Brand, "Height").ToString();

            //TODO: Define webconstant to check Jdate community
            if (request.Brand.Site.Community.CommunityID == 3)
            {
                payment.Data.MemberInfo.Ethnicity = GetDescription("JDateEthnicity", member.GetAttributeInt(request.Brand, "JDateEthnicity"), request.Brand);
                payment.Data.MemberInfo.Religion = GetDescription("JDateReligion", member.GetAttributeInt(request.Brand, "JDateReligion"), request.Brand);
            }
            else
            {
                payment.Data.MemberInfo.Ethnicity = GetDescription("Ethnicity", member.GetAttributeInt(request.Brand, "Ethnicity"), request.Brand);
                payment.Data.MemberInfo.Religion = GetDescription("Religion", member.GetAttributeInt(request.Brand, "Religion"), request.Brand);
            }
            string aboutMe = member.GetAttributeText(request.Brand, "AboutMe");
            payment.Data.MemberInfo.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);

            #endregion UDF

            #region Omniture Variables

            var omnitureVariablesRequest = request.OmnitureVariables;

            var omnitureVariables = new PaymentJsonArray();

            //omniture passed in
            foreach (var variable in omnitureVariablesRequest)
            {
                omnitureVariables.Add(variable.Key, variable.Value);
            }

            //mobile specific - need to update to support others like FWS
            omnitureVariables.Remove("s_account");
            omnitureVariables.Add("s_account", GetOmnitureReportSuite(request.Brand));
            omnitureVariables.Remove("pageName");
            omnitureVariables.Add("pageName", "m_Ala Carte - Select Product");
            omnitureVariables.Remove("events");
            if (payment.Data.OrderConfirmation.Count > 0)
            {
                omnitureVariables.Add("events", "purchase,event48,event10");
            }
            else
            {
                omnitureVariables.Add("events", "purchase,event48");
            }
            omnitureVariables.Remove("prop17");
            omnitureVariables.Add("prop17", OmnitureHelper.GetProfileCompetionPercentage(member, request.Brand));
            omnitureVariables.Remove("prop18");
            omnitureVariables.Add("prop18", OmnitureHelper.GetGender(member.GetAttributeInt(request.Brand, "gendermask")));
            omnitureVariables.Remove("prop19");
            omnitureVariables.Add("prop19", OmnitureHelper.GetAge(member.GetAttributeDate(request.Brand, "Birthdate")).ToString());
            omnitureVariables.Remove("prop20");
            if (request.Brand.Site.Community.CommunityID == (int)CommunityId.JDate)
            {
                omnitureVariables.Add("prop20", MemberHelper.GetDescription("JDateEthnicity", member.GetAttributeInt(request.Brand, "JDateEthnicity"),
                        request.Brand));
            }
            else
            {
                omnitureVariables.Add("prop20", MemberHelper.GetDescription("Ethnicity", member.GetAttributeInt(request.Brand, "Ethnicity"),
                        request.Brand));
            }
            omnitureVariables.Remove("prop21");
            omnitureVariables.Add("prop21", OmnitureHelper.GetRegionString(member, request.Brand, request.Brand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
            omnitureVariables.Remove("prop23");
            omnitureVariables.Add("prop23", request.MemberId.ToString());
            
            omnitureVariables.Remove("purchaseID");
            omnitureVariables.Add("purchaseID", request.OrderID);
            omnitureVariables.Remove("eVar36");
            omnitureVariables.Add("eVar36", GetOmnitureSiteURL(request.Brand, request.PromoType));
            omnitureVariables.Remove("eVar44");
            omnitureVariables.Add("eVar44", request.MemberId.ToString());
            omnitureVariables.Remove("eVar4");
            omnitureVariables.Add("eVar4", request.PrtId);

            if (orderInfo != null)
            {
                Matchnet.Purchase.ValueObjects.Plan primaryPlan = UpsGetPrimaryPlan(orderInfo, request.Brand);
                if (primaryPlan != null)
                {
                    omnitureVariables.Remove("products");
                    omnitureVariables.Add("products", primaryPlan.PlanID.ToString() + ";" + primaryPlan.InitialDuration.ToString() + primaryPlan.InitialDurationType.ToString() + ";" + primaryPlan.RenewDuration
                        + ";" + orderInfo.TotalAmount);
                }
            }
            else
            {
                omnitureVariables.Remove("products");
                omnitureVariables.Add("products", String.Empty);
            }

            payment.Data.OmnitureVariables.Add(omnitureVariables);

            #endregion

            #region Analytics Variables (Tealium)
            var analyticVariablesRequest = request.AnalyticVariables;

            var analyticVariables = new PaymentJsonArray();
            analyticVariables.Add("eId", string.IsNullOrEmpty(request.EID) ? "" : request.EID);

            foreach (var variable in analyticVariablesRequest)
            {
                analyticVariables.Remove(variable.Key);
                analyticVariables.Add(variable.Key, variable.Value);
            }

            var analyticAttributeSetResult = ProfileAccess.GetAttributeSetWithPhotoAttributes(request.Brand, member.MemberID, member.MemberID, "tealiumtargetingprofile_us", false, "");
            if (analyticAttributeSetResult != null && analyticAttributeSetResult.AttributeSet != null)
            {
                foreach (KeyValuePair<string, object> entry in analyticAttributeSetResult.AttributeSet)
                {
                    analyticVariables.Remove(entry.Key);
                    analyticVariables.Add(entry.Key, entry.Value);
                }

            }

            analyticVariables.Remove("tealiumUrl");
            analyticVariables.Add("tealiumUrl", SettingsService.GetSettingFromSingleton("TEALIUM_SRC",
                request.Brand.Site.Community.CommunityID,
                request.Brand.Site.SiteID,
                request.Brand.BrandID));

            payment.Data.AnalyticVariables.Add(analyticVariables);

            #endregion

            // Legacy Data Info
            payment.Data.LegacyDataInfo.Member = request.MemberId;
            payment.Data.LegacyDataInfo.BrandID = request.Brand.BrandID;
            payment.Data.LegacyDataInfo.SiteID = request.Brand.Site.SiteID;

            if (request is AdminPaymentUiPostDataRequest)
            {
                payment.Data.LegacyDataInfo.AdminMemberID = (request as AdminPaymentUiPostDataRequest).AdminMemberID;
            }

            // A La Carte Plans
            // Should only be two. One of each premium service.
            Log.LogInfoMessage(string.Format("# of plans={0}", planCollection.Count), ErrorHelper.GetCustomData());
            foreach (Plan plan in planCollection)
            {
                if (plan.EndDate != DateTime.MinValue)
                    continue;

                PaymentJsonPackage package;
                package = new PaymentJsonPackage();

                package.ID = plan.PlanID;
                package.Description = "";
                package.SytemID = request.Brand.Site.SiteID;
                package.StartDate = renewalSub.RenewalDatePST;// memberSub.EndDate;
                package.ExpiredDate = renewalSub.RenewalDatePST;// memberSub.EndDate;
                package.CurrencyType = GetCurrencyAbbreviation(plan.CurrencyType);

                package.Items.Add("InitialCostPerDuration", plan.InitialCost);
                package.Items.Add("DurationType", "Day");
                package.Items.Add("PlanID", plan.PlanID);
                package.Items.Add("CurrencyType", GetCurrencyAbbreviation(plan.CurrencyType));
                package.Items.Add("PaymentTypeMask", plan.PaymentTypeMask);
                package.Items.Add("CreditAmount", (plan.CreditAmount < 0) ? 0 : plan.CreditAmount);
                package.Items.Add("PurchaseMode", PurchaseMode.New.ToString());
                package.Items.Add("PremiumType", plan.PremiumTypeMask.ToString());
                package.Items.Add("RenewCost", plan.RenewCost);

                if ((plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile
                    || (plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember
                    || (plan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter
                    || (plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess
                    || (plan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
                {
                    decimal proratedAmount = GetMemberProratedAmount(plan.InitialCost, renewalSub.RenewalDatePST, GetStartDateForUpsalePlan(plan, activePrivileges));
                    package.Items.Add("ProratedAmount", proratedAmount);
                    package.Items.Add("Duration", GetDuration(renewalSub.RenewalDatePST, GetStartDateForUpsalePlan(plan, activePrivileges)));

                    // If the package for upsale has amount of 0, do not display this package. 
                    if (proratedAmount > 0)
                    {
                        if (request.PremiumType == PremiumType.None)
                        {
                            payment.Data.Packages.Add(package);
                        }
                        else if ((plan.PremiumTypeMask & request.PremiumType) == request.PremiumType)
                        {
                            payment.Data.Packages.Add(package);
                        }
                    }
                }
            };

            if (payment.Data.Packages.Count <= 0)
            {
                Log.LogInfoMessage("No premium packages available. MemberID: " + request.MemberId.ToString() + ", SiteID: " + request.Brand.Site.SiteID.ToString(), ErrorHelper.GetCustomData());
                throw new SparkAPIReportableException(Rest.V2.Serialization.HttpSubStatusCodes.HttpSub400StatusCode.NoPackagesFound, "No premium packages available. MemberID: " + request.MemberId.ToString() + ", SiteID: " + request.Brand.Site.SiteID.ToString());
            }

            var json = JsonConvert.SerializeObject(payment);
            Log.LogInfoMessage("PaymentJson premim services string to PUI: " + json, ErrorHelper.GetCustomData());
            PurchaseSA.Instance.UPSLegacyDataSave(json.Compress(), upsLegacyDataId);

            return json;
        }

        public string GetJsonPaymentUiPostDataBillingInformation(PaymentUiPostDataRequest request, int globalLogId)
        {
            if (request.PaymentType == PaymentType.None)
            {
                request.PaymentType = PaymentType.CreditCard;
            }

            var upsLegacyDataId = KeySA.Instance.GetKey("UPSLegacyDataID");

            var member = MemberSA.Instance.GetMember(request.MemberId, MemberLoadFlags.None);
            if (member == null || member.MemberID <= 0)
            {
                Log.LogInfoMessage("Member not found for ID: " + request.MemberId.ToString(), ErrorHelper.GetCustomData());
                throw new SparkAPIReportableException(Rest.V2.Serialization.HttpSubStatusCodes.HttpSub400StatusCode.InvalidMemberId, "Member not found for ID: " + request.MemberId.ToString());
            }

            //  110 is default taken from bedrock
            var templateId = (request.TemplateId <= 0) ? 110 : request.TemplateId;

            var payment = new PaymentJson
            {
                UPSLegacyDataID = upsLegacyDataId,
                UPSGlobalLogID = globalLogId,
                GetCustomerPaymentProfile = 1,
                Version = 2,
                CallingSystemID = request.Brand.Site.SiteID,
                TemplateID = templateId,
                TimeStamp = DateTime.Now
            };

            payment.Data.Navigation.CancelURL = request.CancelUrl;
            payment.Data.Navigation.ReturnURL = request.ReturnUrl;
            payment.Data.Navigation.ConfirmationURL = request.ConfirmationUrl;
            payment.Data.Navigation.DestinationURL = request.DestinationUrl;
            payment.IsSubscriptionConfirmationEmailEnabled = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("ENABLE_SUBSCRIPTION_CONFIRMATION_EMAIL", request.Brand.Site.Community.CommunityID, request.Brand.Site.SiteID, request.Brand.BrandID));

            // Kount merchant ID
            payment.KountMerchantID = GetKountMerchantID(request.Brand);

            // Member Info
            payment.Data.MemberInfo.CustomerID = request.MemberId;
            payment.Data.MemberInfo.RegionID = member.GetAttributeInt(request.Brand, "RegionID");
            payment.Data.MemberInfo.IPAddress = request.ClientIp;
            payment.Data.MemberInfo.Language = ((Language)Enum.Parse(typeof(Language), request.Brand.Site.LanguageID.ToString())).ToString();
            Matchnet.Content.ValueObjects.Region.RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID, (int)Language.English);
            if (regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation.Trim().ToUpper();

            //LC: Adding extra fields needed for Kount RIS
            payment.Data.MemberInfo.EmailAddress = member.GetAttributeText(request.Brand, "EmailAddress");
            payment.Data.MemberInfo.GenderMask = AttributeHelper.GetGenderDescription(request.Brand, member.GetAttributeInt(request.Brand, "gendermask"), false);
            payment.Data.MemberInfo.BirthDate = member.GetAttributeDate(request.Brand, "birthdate", DateTime.MinValue);
            payment.Data.MemberInfo.BrandInsertDate = member.GetAttributeDate(request.Brand, "BrandInsertDate", DateTime.MinValue);

            #region UDFS
            //JS-1228 LC: Adding UDF fields needed for Kount
            payment.Data.MemberInfo.UserName = member.GetUserName(request.Brand);
            payment.Data.MemberInfo.MaritalStatus = GetDescription("MaritalStatus", member.GetAttributeInt(request.Brand, "MaritalStatus"), request.Brand);
            payment.Data.MemberInfo.Occupation = member.GetAttributeText(request.Brand, "OccupationDescription");
            payment.Data.MemberInfo.Education = GetDescription("EducationLevel", member.GetAttributeInt(request.Brand, "EducationLevel"), request.Brand);
            payment.Data.MemberInfo.Eyes = GetDescription("EyeColor", member.GetAttributeInt(request.Brand, "EyeColor"), request.Brand);
            payment.Data.MemberInfo.PromotionID = member.GetAttributeInt(request.Brand, "PromotionID").ToString();
            payment.Data.MemberInfo.Hair = GetDescription("HairColor", member.GetAttributeInt(request.Brand, "HairColor"), request.Brand);
            payment.Data.MemberInfo.Height = member.GetAttributeInt(request.Brand, "Height").ToString();

            //TODO: Define webconstant to check Jdate community
            if (request.Brand.Site.Community.CommunityID == 3)
            {
                payment.Data.MemberInfo.Ethnicity = GetDescription("JDateEthnicity", member.GetAttributeInt(request.Brand, "JDateEthnicity"), request.Brand);
                payment.Data.MemberInfo.Religion = GetDescription("JDateReligion", member.GetAttributeInt(request.Brand, "JDateReligion"), request.Brand);
            }
            else
            {
                payment.Data.MemberInfo.Ethnicity = GetDescription("Ethnicity", member.GetAttributeInt(request.Brand, "Ethnicity"), request.Brand);
                payment.Data.MemberInfo.Religion = GetDescription("Religion", member.GetAttributeInt(request.Brand, "Religion"), request.Brand);
            }
            string aboutMe = member.GetAttributeText(request.Brand, "AboutMe");
            payment.Data.MemberInfo.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);

            #endregion UDF

            //get renewal info
            Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(request.MemberId, request.Brand.Site.SiteID);
            RenewalServiceWebAdapter.CloseProxyInstance();

            if (renewalSub == null || renewalSub.RenewalSubscriptionID <= 0)
            {
                Spark.Common.PaymentProfileService.PaymentProfileInfo defaultPaymentProfileInfo = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(request.MemberId, request.Brand.Site.SiteID);
                if (defaultPaymentProfileInfo != null)
                {
                    // Default payment profile may exists from a free trial subscription 
                    // If the member took a free trial subscription, than show the details of the billing information used to take the free trial subscription. 
                    payment.Data.MemberInfo.MonthlyRenewalRate = 0.0m;
                    payment.Data.MemberInfo.NextRenewalDate = String.Empty;
                }
                else
                {
                    //missing renewal or no sub privileges
                    Log.LogInfoMessage("Error attempting to access billing information with no past subscription. MemberID: " + request.MemberId.ToString() + ", SiteID: " + request.Brand.Site.SiteID.ToString(), ErrorHelper.GetCustomData());
                    throw new SparkAPIReportableException(Rest.V2.Serialization.HttpSubStatusCodes.HttpSub400StatusCode.MissingSubscription, "Error attempting to access billing information with no past subscription. MemberID: " + request.MemberId.ToString() + ", SiteID: " + request.Brand.Site.SiteID.ToString());
                }
            }
            else
            {
                Plan plan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, request.Brand.BrandID);
                payment.Data.MemberInfo.MonthlyRenewalRate = plan.RenewCost;
                payment.Data.MemberInfo.NextRenewalDate = renewalSub.RenewalDatePST.ToShortDateString();
            }


            var json = JsonConvert.SerializeObject(payment);
            Log.LogInfoMessage("PaymentJson billing information string to PUI: " + json, ErrorHelper.GetCustomData());
            PurchaseSA.Instance.UPSLegacyDataSave(json.Compress(), upsLegacyDataId);

            return json;
        }

        public TransactionInfo GetTransactionInfo(Brand brand, int memberId, int orderId)
        {
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);

            // Get the affiliate tracking pixel for the subscription confirmation page 
            //PagePixelControl ucPixel = new PagePixelControl();
            //ucPixel.PageID = 3500;
            //affiliateTrackingPixel = ucPixel.WritePixel();

            if (orderId == -1)
            {
                // return some fake data for testing purposes
                var payInfo = new TransactionInfo
                {
                    Username = "TestUserName123",
                    ConfirmationNumber = "0987654321",
                    CreditCardType = "JDate Visa",
                    MaskedCreditCardNumber = "4006",
                    TransactionDate = DateTime.Now
                };
                return payInfo;
            }

            //get UPS order info
            OrderInfo orderInfo;
            var orderHistoryProxy = OrderHistoryServiceWebAdapter.GetProxyInstance();
            if (orderHistoryProxy == null)
            {
                throw new Exception("Could not load order history proxy");
            }
            try
            {
                orderInfo = orderHistoryProxy.GetOrderInfo(orderId);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Could not load order info with Id {0}", orderId), ex);
            }
            finally
            {
                OrderHistoryServiceWebAdapter.CloseProxyInstance();
            }

            if (orderInfo == null)
            {
                throw new Exception("Cannot get order details for the order confirmation.");
            }

            Log.LogDebugMessage(string.Format("Successfully retrieved order information for Order ID [{0}]", orderId), ErrorHelper.GetCustomData());

            //get payment profile info
            ObsfucatedPaymentProfileResponse paymentProfileResponse;
            var paymentProfileProxy = PaymentProfileServiceWebAdapter.GetProxyInstance();
            if (paymentProfileProxy == null)
            {
                throw new Exception("Could not get payment profile proxy");
            }
            try
            {
                paymentProfileResponse = paymentProfileProxy.GetObsfucatedPaymentProfileByOrderID(orderId,
                    orderInfo.CustomerID,
                    orderInfo.
                        CallingSystemID);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Could not load payment profile with Id {0}", orderId), ex);
            }
            finally
            {
                PaymentProfileServiceWebAdapter.CloseProxyInstance();
            }

            if (paymentProfileResponse == null)
            {
                throw new Exception("Cannot get payment profile details for the order confirmation.");
            }
            Log.LogDebugMessage(string.Format("Successfully retrieved payment profile information for Order ID [{0}]", orderId), ErrorHelper.GetCustomData());

            var memberSubscriptionStatus = GetMemberSubcriptionStatus(memberId, brand);
            var enumSubscriptionStatus = (SubscriptionStatus) memberSubscriptionStatus;

            var plan = UpsGetPrimaryPlan(orderInfo, brand);
            if (plan == null)
            {
                throw new Exception("Plan not found for the given order.  Possible bad order ID.");
            }
            var planDescription = plan.PlanID + ";" + plan.InitialDuration + plan.InitialDurationType + ";" +
                                  plan.RenewDuration
                                  + ";" + plan.InitialCost + GetPremiumServiceOmniture(plan);

            var transactionInfo = new TransactionInfo
            {
                Username = member.GetUserName(brand),
                ConfirmationNumber = orderId.ToString(),
                TransactionDate = orderInfo.InsertDate,
                SubscriptionStatusDetailed =
                    ConvertSubscriptionStatusToString(enumSubscriptionStatus, true),
                SubscriptionStatus =
                    ConvertSubscriptionStatusToString(enumSubscriptionStatus, false),
                PlanDescription = planDescription
            };

            if (paymentProfileResponse.PaymentProfile == null)
            {
                Log.LogDebugMessage("payment profile is null", ErrorHelper.GetCustomData());
            }
            else
            {
                Log.LogDebugMessage(string.Format("paymentprofile is instance of type {0}",
                                paymentProfileResponse.PaymentProfile.GetType()), ErrorHelper.GetCustomData());
                var ccPaymentProfile = paymentProfileResponse.PaymentProfile as ObsfucatedCreditCardPaymentProfile;
                var ccShortFormMobileSitePaymentProfile =
                    paymentProfileResponse.PaymentProfile as
                    Spark.Common.PaymentProfileService.ObfuscatedCreditCardShortFormMobileSitePaymentProfile;
                var ccShortFormFullSitePaymentProfile =
                    paymentProfileResponse.PaymentProfile as
                    Spark.Common.PaymentProfileService.ObfuscatedCreditCardShortFormFullWebSitePaymentProfile;

                if (ccPaymentProfile != null)
                {
                    transactionInfo.CreditCardType = ccPaymentProfile.CardType;
                    transactionInfo.MaskedCreditCardNumber = ccPaymentProfile.LastFourDigitsCreditCardNumber;
                }

                if (ccShortFormMobileSitePaymentProfile != null)
                {
                    transactionInfo.CreditCardType = ccShortFormMobileSitePaymentProfile.CardType;
                    transactionInfo.MaskedCreditCardNumber =
                        ccShortFormMobileSitePaymentProfile.LastFourDigitsCreditCardNumber;
                }

                if (ccShortFormFullSitePaymentProfile != null)
                {
                    transactionInfo.CreditCardType = ccShortFormFullSitePaymentProfile.CardType;
                    transactionInfo.MaskedCreditCardNumber =
                        ccShortFormFullSitePaymentProfile.LastFourDigitsCreditCardNumber;
                }
            }

            return transactionInfo;
        }

        /// <summary>
        ///     This ensures member's access privileges are expired and recached.
        ///     There is a known delay issue so the full site does this as well.
        /// </summary>
        /// <param name="memberId"></param>
        public void ExpireCachedMemberAccess(int memberId)
        {
            MemberSA.Instance.ExpireCachedMemberAccess(memberId, true);
        }

        // Omniture - Copied from FrameWorkGlobals from the full site.
        public static int GetMemberSubcriptionStatus(int memberId, Brand brand)
        {
            var memberSubscriptionStatus = Constants.NULL_INT;

            try
            {
                var evaluateSubscriptionStatusAgain = false;

                var objMember = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
                if (objMember != null)
                {
                    //get sub status
                    memberSubscriptionStatus = objMember.GetAttributeInt(brand, "SubscriptionStatus", 0);
                    var enumSubscriptionStatus = (SubscriptionStatus) memberSubscriptionStatus;

                    //get basic sub expiration date
                    var dtSubendDate = DateTime.MinValue;
                    var ap = objMember.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription,
                        brand.BrandID, brand.Site.SiteID,
                        brand.Site.Community.CommunityID);
                    if (ap != null)
                    {
                        dtSubendDate = ap.EndDatePST;
                        //JS-1329
                        Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", objMember.MemberID, PrivilegeType.BasicSubscription.ToString(), ap.EndDatePST), ErrorHelper.GetCustomData());
                    }

                    if (enumSubscriptionStatus == SubscriptionStatus.PremiumSubscriber)
                    {
                        if (dtSubendDate != DateTime.MinValue)
                        {
                            // Member is currently identified as a premium subscriber
                            if ((PremiumServiceSA.Instance.GetMemberActivePremiumServices(memberId, brand).Count < 1)
                                || dtSubendDate < DateTime.Now)
                            {
                                // Member does not have an active premium service or the member does not have
                                // subscription privilege.  
                                // Reevaluate the subscription status of this member
                                evaluateSubscriptionStatusAgain = true;
                            }
                        }
                    }
                    else if (enumSubscriptionStatus != SubscriptionStatus.NonSubscriberExSubscriber
                             && enumSubscriptionStatus != SubscriptionStatus.NonSubscriberFreeTrial
                             && enumSubscriptionStatus != SubscriptionStatus.NonSubscriberNeverSubscribed)
                    {
                        if (dtSubendDate != DateTime.MinValue)
                        {
                            // Member is currently identified as a subscriber
                            if (dtSubendDate < DateTime.Now)
                            {
                                // Member does not have subscription privilege.  
                                // Reevaluate the subscription status of this member
                                evaluateSubscriptionStatusAgain = true;
                            }
                        }
                    }
                    else if (enumSubscriptionStatus == SubscriptionStatus.NonSubscriberNeverSubscribed)
                    {
                        if (dtSubendDate > DateTime.Now)
                        {
                            //member has subscription privilege but status is still never subscribed, let's reevaluate
                            //this would only be true for members who never purchased but was only given time from CS; usually QA accounts
                            evaluateSubscriptionStatusAgain = true;
                        }
                    }

                    if (evaluateSubscriptionStatusAgain)
                    {
                        // This is an edge case and should be a warning.
                        Log.LogWarningMessage(string.Format("Calling Purchase without caching MemberId:{0} SiteId:{1}", memberId, brand.Site.SiteID), ErrorHelper.GetCustomData());
                        memberSubscriptionStatus =
                            Convert.ToInt16(
                                PurchaseSA.Instance.GetMemberSubStatus(memberId, brand.Site.SiteID, dtSubendDate, false)
                                    .Status);
                        objMember.SetAttributeInt(brand, "SubscriptionStatus", memberSubscriptionStatus);
                        MemberSA.Instance.SaveMember(objMember);
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.Write("Error in GetMemberSubcriptionStatus(), " + ex);
                Log.LogException(string.Format("Error in GetMemberSubcriptionStatus() for MemberId:{0}", memberId), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            return memberSubscriptionStatus;
        }

        public string GetSubscriptionStatusGam(Brand brand, Member member)
        {
            var subStatusGam = string.Empty;

            if (member != null && member.MemberID > 0)
            {
                int memSubStatusID = GetMemberSubcriptionStatus(member.MemberID, brand);
                if (memSubStatusID > 0)
                {
                    switch (memSubStatusID)
                    {
                        case 2:
                            subStatusGam = "ex_sub";
                            break;
                        case 1:
                            subStatusGam = "never_sub";
                            break;
                        case 5:
                            subStatusGam = "ren_sub";
                            break;
                        case 6:
                            subStatusGam = "ren_win";
                            break;
                        case 3:
                            subStatusGam = "sub_fts";
                            break;
                        case 4:
                            subStatusGam = "sub_win";
                            break;
                        case 9:
                            subStatusGam = "premium";
                            break;
                    }
                }
            }

            return subStatusGam;
        }

        // Omniture - Copied from Omniture.ascx.cs from the full site.
        public string ConvertSubscriptionStatusToString(SubscriptionStatus subStatus, bool moreDetailed)
        {
            if (moreDetailed)
            {
                switch (subStatus)
                {
                    case SubscriptionStatus.PremiumSubscriber:
                        return "Premium";
                    case SubscriptionStatus.SubscribedFTS:
                        return "FTS";
                    case SubscriptionStatus.SubscribedWinBack:
                        return "WB";
                    case SubscriptionStatus.RenewalSubscribed:
                        return "Renewal";
                    case SubscriptionStatus.RenewalWinBack:
                        return "Renewal";
                    case SubscriptionStatus.SubscriberFreeTrial:
                        return "FreeTrial";
                    default:
                        return "";
                }
            }
            switch (subStatus)
            {
                case SubscriptionStatus.NonSubscriberNeverSubscribed:
                case SubscriptionStatus.NonSubscriberExSubscriber:
                    return "Member";
                case SubscriptionStatus.PremiumSubscriber:
                    return "PremiumSubscriber";
                default:
                    return "Subscriber";
            }
        }

        /// <summary>
        /// This is to map Currency Enum to the proper abbreviation.
        /// </summary>
        /// <param name="description"></param>
        public string GetCurrencyAbbreviation(CurrencyType currencyType)
        {
            switch (currencyType)
            {
                case CurrencyType.USDollar:
                    return "USD";
                case CurrencyType.Euro:
                    return "EUR";
                case CurrencyType.CanadianDollar:
                    return "CAD";
                case CurrencyType.Pound:
                    return "GBP";
                case CurrencyType.AustralianDollar:
                    return "AUD";
                case CurrencyType.Shekels:
                    return "NIS";
                case CurrencyType.VerifiedByVisa:
                    return "VBV";
                default:
                    return "None";
            }
        }

        private DateTime GetStartDateForUpsalePlan(Plan plan, List<Spark.Common.AccessService.AccessPrivilege> activePrivileges)
        {
            Spark.Common.AccessService.PrivilegeType checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.None;

            if ((plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.HighlightedProfile;
            }
            else if ((plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.SpotlightMember;
            }
            else if ((plan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.JMeter;
            }
            else if ((plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.AllAccess;
            }
            else if ((plan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.ReadReceipt;
            }
            else
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.None;
            }

            Spark.Common.AccessService.AccessPrivilege checkPrivilege = activePrivileges.Find(delegate(Spark.Common.AccessService.AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == checkPrivilegeType; });

            if (checkPrivilege == null)
            {
                return DateTime.Now.AddMinutes(60);
            }
            else
            {
                if (checkPrivilege.EndDatePST <= DateTime.Now)
                {
                    return DateTime.Now.AddMinutes(60);
                }
                else
                {
                    return checkPrivilege.EndDatePST;
                }
            }
        }

        /// <summary>
        /// Copied from Purchase
        /// </summary>
        /// <param name="initialCost"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private decimal GetMemberProratedAmount(decimal initialCost, DateTime endDate, DateTime startDate)
        {
            Log.LogInfoMessage(string.Format("initialCost:{0},endDate:{1},startDate:{2}",initialCost,endDate,startDate),ErrorHelper.GetCustomData());
            if (endDate < DateTime.Now)
            {
                throw new Exception("Subscription end date must be a future date. Enddate:" +
                                    endDate.ToString());
            }

            decimal amount = Constants.NULL_DECIMAL;
            decimal pricePerDay = Constants.NULL_DECIMAL;

            // The start date of the premium service should be either earlier than the renewal date 
            // or the same as the renewal date. If the start date of the premium service is past the
            // renewal date, then set the start date of the premium service to the renewal date. 
            if (startDate > endDate)
            {
                amount = 0.0m;
            }
            else
            {
                int monthsUntilRenewalDate = GetExactMonthsUntilRenewal(startDate, endDate);
                decimal subTotalFromMonths = (monthsUntilRenewalDate) * (initialCost);
                DateTime dteAfterMonthsAdded = startDate.AddMonths(monthsUntilRenewalDate);

                // Calculate cost per day
                DateTime dteEndDateOfRemainingDaysWithMonthAdded = dteAfterMonthsAdded.AddMonths(1);
                TimeSpan span = dteEndDateOfRemainingDaysWithMonthAdded.Subtract(dteAfterMonthsAdded);
                pricePerDay = (initialCost) / Convert.ToDecimal(span.Days);

                decimal subTotalFromRemainingDays = (pricePerDay) * (Convert.ToDecimal(GetDuration(endDate, dteAfterMonthsAdded)));
                //Make sure the prorated amount for remaining days is not greater than monthly price
                if (subTotalFromRemainingDays > initialCost)
                {
                    subTotalFromRemainingDays = initialCost;
                }

                amount = subTotalFromMonths + subTotalFromRemainingDays;
            }

            if (amount < 0)
                throw new Exception("Prorated Amount cannot be less then 0.");

            //Log.Debug("InitialCost:" + initialCost +
            //    "EndDate:" + endDate + "StartDate:" + startDate + "Amount:" + amount);
            return amount;

        }

     

        private int GetExactMonthsUntilRenewal(DateTime startDate, DateTime endDate)
        {
            int monthDifference = System.Data.Linq.SqlClient.SqlMethods.DateDiffMonth(startDate, endDate);

            if (monthDifference > 0)
            {
                // Check to see if the number of months in between these two dates should be one less
                DateTime dteMonthDifferenceAdded = startDate.AddMonths(monthDifference);

                if (endDate < dteMonthDifferenceAdded)
                {
                    monthDifference = (monthDifference - 1);
                }
            }

            return monthDifference;
        }

        //private int GetDuration(DateTime endDate)
        //{
        //    // Get remaining days left on this plan
        //    // Whenever there is any remaining time in a day, give that full day of remaining credit to the 
        //    // member.  
        //    // Round 2.1 to 3 days
        //    // Round 2.9 to 3 days
        //    TimeSpan span = endDate.Subtract(DateTime.Now);
        //    Int32 remainingDays = Convert.ToInt32(Math.Ceiling(span.TotalDays));

        //    //Log.Debug("RemainingDays:" + remainingDays);

        //    return remainingDays;
        //}

        /// <summary>
        /// Copied from Purchase
        /// </summary>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private int GetDuration(DateTime endDate, DateTime startDate)
        {
            // Get remaining days left on this plan
            // Whenever there is any remaining time in a day, give that full day of remaining credit to the 
            // member.  
            // Round 2.1 to 3 days
            // Round 2.9 to 3 days
            TimeSpan span = endDate.Subtract(startDate);
            Int32 remainingDays = Convert.ToInt32(Math.Ceiling(span.TotalDays));

            //Log.Debug("RemainingDays:" + remainingDays);

            return remainingDays;
        }

        // Omniture - Copied from SubscriptionConfirmation page from the full site.
        private static Plan UpsGetPrimaryPlan(OrderInfo orderInfo, Brand brand)
        {
            var legacyPlanIdFound = Constants.NULL_INT;
            var alaCartePlanIdFound = Constants.NULL_INT;
            var discountPlanIdFound = Constants.NULL_INT;

            if (orderInfo.OrderDetail.Length == 1)
            {
                return PlanSA.Instance.GetPlan(orderInfo.OrderDetail[0].PackageID, brand.BrandID);
            }
            else
            {
                foreach (var odi in orderInfo.OrderDetail)
                {
                    var plan = PlanSA.Instance.GetPlan(odi.PackageID, brand.BrandID);

                    if (plan != null)
                    {
                        if ((plan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte)
                        {
                            // An ala carte item exists in the order
                            if (alaCartePlanIdFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                alaCartePlanIdFound = odi.PackageID;
                            }
                        }
                        else if ((plan.PlanTypeMask & PlanType.Discount) == PlanType.Discount)
                        {
                            // A discount item exists in the order
                            if (discountPlanIdFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                discountPlanIdFound = odi.PackageID;
                            }
                        }
                        else
                        {
                            // The primary PlanID will be the item that is not an ala carte 
                            // or discount package  
                            if (legacyPlanIdFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                legacyPlanIdFound = odi.PackageID;
                                break;
                            }
                        }
                    }
                }

                if (legacyPlanIdFound != Constants.NULL_INT)
                {
                    // Return any PlanID as the primary PlanID as long as this is not an ala carte 
                    // package or a discount package
                    return PlanSA.Instance.GetPlan(legacyPlanIdFound, brand.BrandID);
                }
                else if (alaCartePlanIdFound != Constants.NULL_INT)
                {
                    // If the member only purchased an ala carte package and no base subscription packages,
                    // than return the first ala carte package found
                    return PlanSA.Instance.GetPlan(alaCartePlanIdFound, brand.BrandID);
                }
                else
                {
                    // If the order did not have any base subscriptions or ala carte packages, than there
                    // no primary PlanID was found
                    // Do not return the discount PlanID since a discount cannot exists without a base 
                    // subscription or ala carte purchase  
                    return null;
                }
            }
        }

        // Omniture - Copied from SubscriptionConfirmation page from the full site.
        private static string GetPremiumServiceOmniture(Plan plan)
        {
            var result = "";
            try
            {
                if (plan == null || (plan.PlanTypeMask & PlanType.PremiumPlan) != PlanType.PremiumPlan)
                    return result;

                if ((plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
                {
                    result = "Highlight";

                    if ((plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                        result += " Spotlight";
                }
                else if ((plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                {
                    result += "Spotlight";
                }

                if ((plan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
                {
                    result += " JMeter";
                }

                result.Trim();

                if (!String.IsNullOrEmpty(result))
                {
                    result = ",;" + result + ";1;0.00";
                }

                return result;
            }
            catch
            {
                return result;
            }
        }

        private static bool IsAllowedToPurchaseUpsalePlan(List<Spark.Common.AccessService.AccessPrivilege> activePrivileges, Spark.Common.AccessService.PrivilegeType checkPrivilegeType, DateTime renewalDate)
        {
            bool isAllowedToPurchaseUpsalePlan = false;

            Spark.Common.AccessService.AccessPrivilege checkPrivilege = activePrivileges.Find(delegate(Spark.Common.AccessService.AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == checkPrivilegeType; });
            if (checkPrivilege == null)
            {
                Log.LogInfoMessage("first time upsale - checkPrivilege is null",null);
                // Member can purchase this privilege since he does not have this privilege yet
                isAllowedToPurchaseUpsalePlan = true;
            }
            else
            {
                if (checkPrivilege.EndDatePST == DateTime.MinValue
                    || checkPrivilege.EndDatePST < DateTime.Now)
                {
                    // Member can purchase this privilege since he had this privilege but it expired  
                    isAllowedToPurchaseUpsalePlan = true;
                }
                else
                {
                    Log.LogInfoMessage(string.Format("NOT first time upsale - checkPrivilege.EndDatePST is {0} and renewDate is {1}", checkPrivilege.EndDatePST, renewalDate), ErrorHelper.GetCustomData());
                    // The start date of the premium service should be either earlier than the renewal date 
                    // or the same as the renewal date. If the start date of the premium service is past the
                    // renewal date, then set the start date of the premium service to the renewal date. 
                    if (checkPrivilege.EndDatePST > renewalDate)
                    {
                        Log.LogInfoMessage("isAllowedToPurchaseUpsalePlan",null);
                        isAllowedToPurchaseUpsalePlan = false;
                    }
                    else
                    {
                        System.TimeSpan dateDifference = checkPrivilege.EndDatePST.Subtract(renewalDate);
                        if (Math.Abs(dateDifference.TotalMinutes) > 20)
                        {
                            // There is still additional time that member can purchase for this privilege
                            isAllowedToPurchaseUpsalePlan = true;
                        }
                        else
                        {
                            // The end date of the privilege is the same as the end date of the basic subscription  
                            // so do not allow the member to purchase this privilege  
                            isAllowedToPurchaseUpsalePlan = false;
                        }
                    }
                }
            }

            return isAllowedToPurchaseUpsalePlan;
        }

        private string GetOmnitureReportSuite(Brand brand)
        {
            switch (brand.Site.SiteID)
            {
                case (int)SITE_ID.JDate:
                    return "sparkjdatecom";
                case (int)SITE_ID.JDateCoIL:
                    return "sparkjdatecoil";
                case (int)SITE_ID.JDateUK:
                    return "sparkjdatecouk";
                case (int)SITE_ID.JDateFR:
                    return "sparkjdatefr";
            }

            return "sparkMobileSuite";
        }

        private string GetOmnitureSiteURL(Brand brand)
        {
            //this was originally created just for mobile, so we'll leave that as the default
            return GetOmnitureSiteURL(brand, PromoType.Mobile);
        }

        private string GetOmnitureSiteURL(Brand brand, PromoType promoType)
        {
            if (promoType == PromoType.Mobile)
            {
                switch (brand.Site.SiteID)
                {
                    case (int)SITE_ID.JDate:
                        return "m.jdate.com";
                    case (int)SITE_ID.JDateCoIL:
                        return "m.jdate.co.il";
                    case (int)SITE_ID.JDateUK:
                        return "m.jdate.co.uk";
                    case (int)SITE_ID.JDateFR:
                        return "m.jdate.fr";
                }
            }
            else
            {
                return "www." + brand.Uri;
            }

            return "";
        }

        private string GetKountMerchantID(Brand brand)
        {
            string merchantID = "";
            try
            {
                merchantID = SettingsService.GetSettingFromSingleton("KOUNT_MERCHANT_ID", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting probably doesn't exist
            }

            return merchantID;
        }

        /// <summary>
        /// generates a comma del plan string for the plans passed in. 
        /// </summary>
        /// <param name="plans"></param>
        /// <returns></returns>
        private string GetPlanIDListForOmniture(Promo promo)
        {
            System.Text.StringBuilder planIDList = new System.Text.StringBuilder();

            if(promo.PromoPlans != null && promo.PromoPlans.Count > 0)
            {
                foreach (PromoPlan promoPlan in promo.PromoPlans)
                {
                    planIDList.Append(promoPlan.PlanID.ToString() + ",");
                }
            }
            
            return planIDList.ToString().TrimEnd(',');
        }

        private string CalculateAge(string birthDate)
        {
            var dtBirthDate = Convert.ToDateTime(birthDate);
            DateTime dtToday = DateTime.Now;

            // get the difference in year
            int years = dtToday.Year - dtBirthDate.Year;
            // subtract another year if we're before the
            // birth day in the current year
            if (dtToday.Month < dtBirthDate.Month || (dtToday.Month == dtBirthDate.Month && dtToday.Day < dtBirthDate.Day))
                years = years - 1;

            return years.ToString();
        }

        public string GetDescription(string attributeName, int attributeValue, Brand brand)
        {
            var attributeOptionCollection =
                AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            var attributeOption = attributeOptionCollection[attributeValue];

            if (attributeOption != null)
                return attributeOption.Description;

            return string.Empty;
        }

        private List<int> GetFixedPricingUpsalePackage(int siteID)
        {
            List<int> upsalePackages = new List<int>();

            if (siteID == (int)SITE_ID.JDate)
            {
                upsalePackages.Add(4500);
                upsalePackages.Add(4501);
            }
            else if (siteID == (int)SITE_ID.JDateUK)
            {
                upsalePackages.Add(4045);
                upsalePackages.Add(4046);
            }
            else if (siteID == (int)SITE_ID.JDateFR)
            {
                upsalePackages.Add(4050);
                upsalePackages.Add(4051);
            }
            else if (siteID == (int)SITE_ID.JDateCoIL)
            {
                upsalePackages.Add(4503);
            }

            return upsalePackages;
        }
    }

    public enum SubscriptionType
    {
        NONE =0,
        STANDARD = 1,
        PREMIUM = 2,
        ALC = 3
    }
}