﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace Spark.Rest.V2.DataAccess
{
    /// <summary>
    /// </summary>
    [Serializable]
    public class GetAttributeSetResult
    {
        /// <summary>
        /// </summary> //
        public GetAttributeSetResult()
        {
            AttributeSet = new Dictionary<string, object>();
        }

        /// <summary>
        /// </summary>
        public AttributeSetNotFoundReasonType AttributeSetNotFoundReason { get; set; }

        /// <summary>
        /// </summary>
        public Dictionary<string, object> AttributeSet { get; set; }

        public GetAttributeSetResult Copy()
        {
            var ret = (GetAttributeSetResult)  MemberwiseClone();
            ret.AttributeSet = new Dictionary<string, object>(ret.AttributeSet);
            return ret;
        }

    }

    /// <summary>
    /// </summary>
    public enum AttributeSetNotFoundReasonType
    {
        /// <summary>
        /// </summary>
        None = 1,

        /// <summary>
        /// </summary>
        AdminSuspended = 2,

        /// <summary>
        /// </summary>
        SelfSuspended = 3,

        /// <summary>
        /// </summary>
        ProfileBlocked = 4,

        /// <summary>
        /// </summary>
        NotMemberOfCommunity = 5,

        /// <summary>
        /// </summary>
        MemberNotFound = 6,

        /// <summary>
        /// </summary>
        BothMemberIdsRequired = 7
    }
}