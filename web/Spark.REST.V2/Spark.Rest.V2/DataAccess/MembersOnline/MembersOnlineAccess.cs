﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Spark.REST.DataAccess;
using Matchnet.Session.ServiceAdapters;
using Spark.Logger;
using Spark.Rest.Helpers;

#endregion

namespace Spark.Rest.V2.DataAccess.MembersOnline
{
    /// <summary>
    /// </summary>
    public sealed class MembersOnlineAccess : Controller
    {
        private static readonly MembersOnlineAccess _instance = new MembersOnlineAccess();
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MembersOnlineActionFilter));

        private MembersOnlineAccess()
        {
        }

        /// <summary>
        /// </summary>
        public static MembersOnlineAccess Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// </summary>
        /// <param name = "brand"></param>
        /// <param name = "memberId"></param>
        /// <param name = "appId"></param>
        public void Add(Brand brand, int memberId, int appId)
        {
            var displaySettings = ProfileAccess.GetDisplaySettings(memberId, brand);
            if (!displaySettings.Online) return; // Set to hidden. Don't add to MOL.

            var ttl = Convert.ToInt32(RuntimeSettings.GetApiAppSetting(appId, "MOL_MEMBER_TTL").ApiAppSettingValue);

            MembersOnlineSA.Instance.Add(brand.Site.Community.CommunityID, memberId, ttl);

            // too noisy. todo: this method gets called *way* too often
            //Log.DebugFormat("Added to mol communityId:{0} memberId:{1} cache ttl:{2} appId:{3}",
                            //brand.Site.Community.CommunityID, memberId, ttl, appId);
        }

        /// <summary>
        ///     Async method for add
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberId"></param>
        /// <param name="appId"></param>
        public void AddAsync(Brand brand, int memberId, int appId)
        {
            var ctx = System.Web.HttpContext.Current;
            MiscUtils.FireAndForget(o =>
            {
                //set HttpContext for thread
                System.Web.HttpContext.Current = ctx;
                try
                {
                    Add(brand, memberId, appId);
                }
                finally
                {
                    //unset HttpContext for thread (precaution for GC)
                    System.Web.HttpContext.Current = null;                    
                }
            }, "MOL AddAsync", "Failed to AddAsync");
        }

        /// <summary>
        /// </summary>
        /// <param name = "communityId"></param>
        /// <param name = "memberId"></param>
        public void Remove(int communityId, int memberId)
        {
            MembersOnlineSA.Instance.Remove(communityId, memberId);
        }

        /// <summary>
        /// </summary>
        public List<Dictionary<string, object>> GetMembersOnlineList(Brand brand, int memberId, int pageNumber,
            int pageSize,
            SortFieldType sortType, int minAge = 0,
            int maxAge = 0, int regionId = 0,
            int languageId = 0, int genderMask = 0)
        {
            pageNumber--;

            if (regionId == 0) regionId = brand.Site.DefaultRegionID; // brand county
            if (minAge == 0) minAge = brand.DefaultAgeMin;
            if (maxAge == 0) maxAge = brand.DefaultAgeMax;
            if (languageId == 0) languageId = Constants.NULL_INT; // any language

            Log.LogDebugMessage(string.Format("MemberID:{0} MinAge:{1} MaxAge:{2} RegionId:{3} LanguageId:{4} GenderMask:{5}",
                memberId, minAge, maxAge, regionId, languageId, genderMask), ErrorHelper.GetCustomData());

            var sortDirection = SortDirectionType.Asc;
            switch (sortType)
            {
                case SortFieldType.Age:
                case SortFieldType.HasPhoto:
                case SortFieldType.InsertDate:
                    sortDirection = SortDirectionType.Desc;
                    break;
            }

            var queryResult =
                MembersOnlineSA.Instance.GetMemberIDs(String.Empty, //this is ok if profileblocking setting is disabled
                    brand.Site.Community.CommunityID,
                    (short) genderMask, // 0 is everyone
                    regionId,
                    (short) minAge,
                    (short) (maxAge + 1),
                    languageId,
                    sortType, sortDirection,
                    pageNumber*pageSize,
                    pageSize,
                    memberId).ToArrayList();

            var list = new List<int>(queryResult.Count);
            list.AddRange(queryResult.Cast<object>().Cast<int>());

            return
                list.Select(
                    id => ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, id, memberId, "resultsetprofile").AttributeSet)
                    .ToList();
        }

        public bool IsConsolidatedMOLCountEnabled(Brand brand)
        {
            if (brand != null)
            {
                return Convert.ToBoolean(RuntimeSettings.GetSetting("USE_MOL_CONSOLIDATION_COUNT", brand.Site.Community.CommunityID));
            }
            else
            {
                return Convert.ToBoolean(RuntimeSettings.GetSetting("USE_MOL_CONSOLIDATION_COUNT"));
            }
        }

        public int GetMOLCommunityCount(Brand brand, bool ignoreCache)
        {
            if (IsConsolidatedMOLCountEnabled(brand))
            {
                return MembersOnlineSA.Instance.GetMOLCount(brand.Site.Community.CommunityID, ignoreCache);
            }
            else
            {
                return SessionSA.Instance.GetSessionCount(brand.Site.Community.CommunityID);
            }
        }

    }
}