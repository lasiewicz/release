﻿#region

using System;
using System.Linq;
using System.Web.Mvc;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.REST.Models;

#endregion

namespace Spark.Rest.V2.DataAccess.MembersOnline
{
    ///<summary>
    /// This filter is used to indicate the calling method triggers an add(update) to MOL.
    ///</summary>
    public class MembersOnlineActionFilter : ActionFilterAttribute
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MembersOnlineActionFilter));

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Making sure the caller does have the attribute set.
            var hasAttribute =
                filterContext.ActionDescriptor.GetCustomAttributes(typeof (MembersOnlineActionFilter), false).Any();

            if (!hasAttribute)
            {
                Log.LogDebugMessage(string.Format("Filter attribute not found for {0}", filterContext.ActionDescriptor.ActionName), ErrorHelper.GetCustomData(filterContext.HttpContext));
                return;
            }

            // Locate a member request object in the caller's action method parameters.
            BrandRequest brandRequest = null;
            foreach (var parameter in filterContext.ActionParameters)
            {
                object value;
                filterContext.ActionParameters.TryGetValue(parameter.Key, out value);
                brandRequest = value as BrandRequest;
                if (brandRequest != null)
                {
                    break;
                }
            }

            if (brandRequest == null)
            {
                throw new Exception(
                    String.Format(
                        "Method {0} with MembersOnlineActionFilter attribute does not have a BrandRequest object as a parameter.",
                        filterContext.ActionDescriptor.ActionName));
            }

            // memberID in MemberRequest is actually not populated by clients. Get it from OAuth.
            var memberId = (int) filterContext.HttpContext.Items["tokenMemberId"];
            if (memberId <= 0) return;

            var appId = (int)filterContext.HttpContext.Items["tokenAppId"];

            MembersOnlineAccess.Instance.AddAsync(brandRequest.Brand, memberId, appId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            return;
        }
    }
}