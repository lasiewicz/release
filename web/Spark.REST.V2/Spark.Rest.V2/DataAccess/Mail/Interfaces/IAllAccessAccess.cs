using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Rest.V2.Entities.Mail;

namespace Spark.Rest.V2.DataAccess.Mail.Interfaces
{
    public interface IAllAccessAccess
    {
        /// <summary>
        ///     Ported from FWS with modifications
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberid"></param>
        /// <returns></returns>
        bool IsAllAccessMember(Brand brand, int memberid);

        /// <summary>
        ///     Ported from FWS with modifications
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberid"></param>
        /// <returns></returns>
        AllAccessMember GetAllAccessMember(Brand brand, int memberid);

        /// <summary>
        ///     Ported from FWS with modifications
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberId"></param>
        /// <param name="count"></param>
        void AdjustMemberAccessEmailCount(Brand brand, int memberId, int count);

        /// <summary>
        ///     Ported from FWS with modifications
        ///     This method checks the message being replied to has the FreeReply mask or not.
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="fromMemberId"></param>
        /// <param name="replyMailId"></param>
        bool IsThisFreeReply(Brand brand, int fromMemberId, int replyMailId);

        /// <summary>
        ///     Ported from FWS with modifications
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        bool IsAllAcessEnabledSite(Brand brand);
    }
}