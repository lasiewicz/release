#region

using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ValueObjects;
using Spark.Rest.V2.Entities.Mail;

#endregion

namespace Spark.Rest.V2.DataAccess.Mail.Interfaces
{
    /// <summary>
    /// </summary>
    public interface IInternalMailAccess
    {
        /// <summary>
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SendMailMessageResult SendMailMessage(SendMailMessageRequest request);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetMessageFolderResult GetMessageFolder(GetMessageFolderRequest request);

        /// <summary>
        /// </summary>
        /// <param name="recipientMemberId"></param>
        /// <param name="brand"></param>
        /// <param name="messageSave"></param>
        /// <param name="fromMemberId"></param>
        /// <param name="sendResult"></param>
        /// <returns></returns>
        bool SendBlockedEmail(int recipientMemberId, Brand brand, MessageSave messageSave,
            int fromMemberId, out MessageSendResult sendResult);

        /// <summary>
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="messageSave"></param>
        /// <param name="fromMemberId"></param>
        /// <param name="memberMailId"></param>
        /// <returns></returns>
        bool SendDraftMessage(Brand brand, MessageSave messageSave, int fromMemberId, out int memberMailId);

        /// <summary>
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="isAllAccess"></param>
        /// <param name="isFraudHold"></param>
        /// <param name="sendResult"></param>
        /// <param name="freeReplyToAllAccess"></param>
        /// <param name="fromMemberId"></param>
        /// <param name="messageSave"></param>
        void SendExternalMailNotificationForSent(Brand brand, bool isAllAccess, bool isFraudHold,
            MessageSendResult sendResult, bool freeReplyToAllAccess, int fromMemberId, MessageSave messageSave);

        /// <summary>
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="fromMemberId"></param>
        /// <param name="recipientMemberId"></param>
        /// <param name="isAllAccess"></param>
        /// <param name="isIgnoreQuota"></param>
        /// <param name="isFreeReplyToAllAccess"></param>
        /// <param name="messageSave"></param>
        void AddToMembersYouEmailedHotListForSent(Brand brand, int fromMemberId,
            int recipientMemberId, bool isAllAccess, bool isIgnoreQuota,
            bool isFreeReplyToAllAccess, MessageSave messageSave);
    }
}