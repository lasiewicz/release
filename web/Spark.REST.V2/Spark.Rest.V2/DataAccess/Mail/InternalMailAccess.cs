#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Email.ServiceAdapters.Interfaces;
using Matchnet.Email.ValueObjects;
using Matchnet.EmailTracker.ValueObjects.ServiceDefinitions;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters.Interfaces;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Spark.Common.AccessService;
using Spark.Common.Localization;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.Rest.Serialization;
using Spark.Rest.V2.DataAccess.Mail.Interfaces;
using Spark.Rest.V2.Entities.Mail;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.Controllers;
using Spark.REST.DataAccess;
using Spark.REST.Entities.Mail;

#endregion

namespace Spark.Rest.V2.DataAccess.Mail
{
    /// <summary>
    ///     Major refactor of internal mail send with All Access and Fraud Hold support
    ///     into its own DI class
    ///     todo: push down to the SA layer
    /// </summary>
    internal class InternalMailAccess : IInternalMailAccess
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(InternalMailAccess));

        private readonly IAllAccessAccess _allAccessAccess;

        private readonly IEmailBlockedSender _emailMessageAdapter;
        private readonly IEmailTrackerService _emailTrackerAdapter;
        private readonly IExternalMailSA _externalMailAdapter;
        private readonly IListAddMember _listAdapter;
        private readonly IGetMember _memberAdapter;
        private readonly IAddQuotaItem _quotaAdapter;
        private readonly IResourceProvider _resourceProviderAdapter;
        private readonly ISettingsSA _settingsAdapter;

        private readonly object _lockMe = new object();

        /// <summary>
        ///     For DI
        /// </summary>
        public InternalMailAccess(IEmailBlockedSender emailMessageAdapter, IExternalMailSA externalMailAdapter,
            IGetMember memberAdapter, IListAddMember listAdapter,
            IAddQuotaItem quotaAdapter, IEmailTrackerService emailTrackerAdapter,
            IResourceProvider resourceProviderAdapter, ISettingsSA settingsAdapter, IAllAccessAccess allAccessAccess)
        {
            _allAccessAccess = allAccessAccess;

            _emailMessageAdapter = emailMessageAdapter;
            _externalMailAdapter = externalMailAdapter;
            _memberAdapter = memberAdapter;
            _listAdapter = listAdapter;
            _quotaAdapter = quotaAdapter;
            _emailTrackerAdapter = emailTrackerAdapter;
            _resourceProviderAdapter = resourceProviderAdapter;
            _settingsAdapter = settingsAdapter;
        }

        /// <summary>
        ///     This is the main method for sending an internal mail message
        ///     Includes support for All Access and Fraud Hold
        ///     A lot of code here chould be refactored further into the SA
        /// </summary>
        public SendMailMessageResult SendMailMessage(SendMailMessageRequest request)
        {
            var result = new SendMailMessageResult();

            #region parameter validation

            var validationResponse = ValidateSendMailMessageRequest(request.FromMemberId, request.RecipientMemberId,
                request.RequestMailType);
            if (validationResponse != null)
            {
                result.SparkHttpStatusCodeResult = validationResponse;
                return result;
            }

            #endregion

            // safe to part after validation above
            var mailType = (MailType) Enum.Parse(typeof (MailType), request.RequestMailType, true);

            #region check if this is a free to reply All Access

            // Before the message is sent, we need to check to see if this is a free reply or not.  If you check this after the message is sent,
            // you will never get FreeReply bool to come back true since when the reply is made, the FreeReply mask bit is set to 0 on the original/being replied
            // to message
            var isFreeReplyToAllAccess = _allAccessAccess.IsThisFreeReply(request.Brand, request.FromMemberId,
                request.ReplyToMessageId);

            #endregion

            // let's determine whether the member is eligible to send the message
            var isPayingMember = Rest.DataAccess.Subscription.SubscriptionStatus.MemberIsSubscriber(request.Brand, request.FromMemberId);

            if (!isFreeReplyToAllAccess && !isPayingMember && !request.IsDraft)
            {
                result.SparkHttpStatusCodeResult =
                    new SparkHttpUnauthorizedResult((int) HttpSub401StatusCode.SubscriberIdRequired,
                        String.Format(
                            "Access to this endpoint is restricted to subscriber members.  Member {0} does not have a subscription or a valid All Access Free to Reply",
                            request.FromMemberId));

                return result;
            }

            #region compose a new message object based on the request for sending

            var messageSave = new MessageSave(request.FromMemberId, request.RecipientMemberId,
                request.Brand.Site.Community.CommunityID, mailType, request.Subject, request.Body);

            #endregion

            #region if the recipient has blocked the sender, send a blocked email and finish the request

            MessageSendResult sendResultSendBlocked;
            if (SendBlockedEmail(request.RecipientMemberId, request.Brand, messageSave, request.FromMemberId,
                out sendResultSendBlocked))
            {
                result.SparkHttpStatusCodeResult =
                    new SparkHttpBadRequestResult((int) HttpSub400StatusCode.BlockedSenderIdForSendMail,
                        sendResultSendBlocked.EmailMessage.Subject);
                return result;
            }

            #endregion

            #region if the message is a draft

            if (request.DraftMessageId > 0)
            {
                //request.IsDraft: false - user is sending a saved draft so the draft will become a sent email
                //request.IsDraft: true - user is updating a saved draft
                messageSave.MessageListID = request.DraftMessageId;
            }

            //save and finish the request if we're creating or updating a draft
            if (request.IsDraft)
            {
                int draftMailId;
                if (SendDraftMessage(request.Brand, messageSave, request.FromMemberId, out draftMailId))
                {
                    // return the response here to finish the request
                    return new SendMailMessageResult
                    {
                        PostMessageResult = new PostMessageResult
                        {
                            MessageId = draftMailId,
                            Success = true,
                            FailureReason = String.Empty,
                        },
                        IsSent = true,
                        SparkHttpStatusCodeResult = null
                    };
                }
                // if saving as draft failed, return error
                result.SparkHttpStatusCodeResult =
                    new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedSendMailMessage,
                        "Failed to save message as a draft.");
                return result;
            }

            #endregion

            #region mark the message as an All Access(a.k.a. VIP) if the all access request is on

            if (request.IsAllAccess)
            {
                // check to make sure this is an All Access member before going any further
                if (!_allAccessAccess.IsAllAccessMember(request.Brand, request.FromMemberId))
                {
                    result.SparkHttpStatusCodeResult =
                        new SparkHttpBadRequestResult((int) HttpSub400StatusCode.NoAllAccess,
                            String.Format("Member does not have All Access. MemberId:{0}",
                                request.FromMemberId));
                    return result;
                }

                messageSave.MailOption = messageSave.MailOption | MailOption.VIP;
            }

            #endregion

            // todo: check for AllAccess privilege or does service check it?

            // actual call to send the message
            var sendResult = _emailMessageAdapter.SendMessage(messageSave, true, request.ReplyToMessageId, true);

            #region immediately finish the request on a non successful send

            if (sendResult.Status != MessageSendStatus.Success)
            {
                result.SparkHttpStatusCodeResult =
                    new SparkHttpBadRequestResult((int) HttpSub400StatusCode.FailedSendMailMessage,
                        sendResult.Error.ToString());
                return result;
            }

            #endregion

            // rest of the operation is based on a successful send

            // do not increment email quota if sending a reply
            var isIgnoreQuota = (request.ReplyToMessageId > 0);

            #region fraud hold status check and increment quota

            // On JDate, if you�ve just subscribed, your account automatically defaults to Yes under Subscription Fraud.  Thus, when you send email messages, your messages will go on hold until a CS rep changes your Subscription Fraud status in admintool to No. 
            var isFraudHold = (sendResult.MailOptions & MailOption.FraudHold) == MailOption.FraudHold;

            // If this is a FraudHold email, the HotList never gets updated, so we need to deduct the quota separately so that the member can't bypass the quota rule.  Normally quota is updated inside AddListMember method.
            if (isFraudHold)
            {
                _quotaAdapter.AddQuotaItem(request.Brand.Site.Community.CommunityID, request.FromMemberId,
                    QuotaType.SentEmail);
            }

            #endregion

            #region add to hot list

            // if this is a FraudHold email, we don't want the hotlist to be updated because the member
            // will be confused since the email won't appear in their inbox
            if (!isFraudHold)
            {
                AddToMembersYouEmailedHotListForSent(request.Brand, request.FromMemberId,
                    request.RecipientMemberId, request.IsAllAccess,
                    isIgnoreQuota, isFreeReplyToAllAccess, messageSave);
            }

            #endregion

            #region if this was an All Access message send, decrement from the member's balance

            if (request.IsAllAccess)
            {
                // check to make sure this is an All Access member
                // and also check is this is a reply or a free All Access reply
                if (_allAccessAccess.IsAllAccessMember(request.Brand, request.FromMemberId) && !isFreeReplyToAllAccess)
                {
                    // decrement by 1
                    _allAccessAccess.AdjustMemberAccessEmailCount(request.Brand, request.FromMemberId, -1);

                    Log.LogInfoMessage(string.Format("All Access email sent from:{0} to:{1}", request.FromMemberId,
                        request.RecipientMemberId), ErrorHelper.GetCustomData());
                }
            }

            #endregion

            // FWS code has reference to this setting NONSUB_CAN_SEND_FREE_EMAIL which no longer appears to be in use. It makes a lot of calls so not needlessly adding it here.

            // todo: FWS code has reference to member attribute VacationStartDate for vacation hold

            #region send any external email notifications related to the send

            SendExternalMailNotificationForSent(request.Brand, request.IsAllAccess, isFraudHold, sendResult,
                isFreeReplyToAllAccess, request.FromMemberId, messageSave);

            #endregion

            // we should be populating it with the recipientMessageListId but the initial implementation returns the opposite which all clients now depend on.
            //sendResult.RecipientMessageListID
            var messageListId = sendResult.SenderMessageListID;

            var postResult = new PostMessageResult
            {
                MessageId = messageListId,
                Success = true,
                FailureReason = String.Empty,
            };

            return new SendMailMessageResult
            {
                PostMessageResult = postResult,
                IsSent = true,
                SparkHttpStatusCodeResult = null
            };
        }

        public void AddToMembersYouEmailedHotListForSent(Brand brand, int fromMemberId,
            int recipientMemberId, bool isAllAccess, bool isIgnoreQuota,
            bool isFreeReplyToAllAccess, MessageSave messageSave)
        {
            // do not add to the hotlist if AllAccess, it has its own below, MembersYouSentAllAccessEmail
            if (!isAllAccess)
            {
                // save the to member in the current member's hotlist
                _listAdapter.AddListMember(HotListCategory.MembersYouEmailed,
                    brand.Site.Community.CommunityID,
                    brand.Site.SiteID,
                    fromMemberId,
                    recipientMemberId,
                    null,
                    Constants.NULL_INT,
                    true,
                    isIgnoreQuota);
            }
            else
            {
                if (_allAccessAccess.IsAllAccessMember(brand, fromMemberId) && !isFreeReplyToAllAccess)
                {
                    _listAdapter.AddListMember(HotListCategory.MembersYouSentAllAccessEmail,
                        brand.Site.Community.CommunityID,
                        brand.Site.SiteID,
                        fromMemberId,
                        messageSave.ToMemberID,
                        null,
                        Constants.NULL_INT,
                        true,
                        isIgnoreQuota);
                }
            }
        }

        /// <summary>
        ///     This is the main method for retrieving an internal mail message folder
        /// 
        ///     This method also determines if the member is eligible for retrieval.
        /// </summary>
        public GetMessageFolderResult GetMessageFolder(GetMessageFolderRequest request)
        {
            var result = new GetMessageFolderResult(request.MemberId);

            //todo refactor into own method

            #region Validation

            if (request.PageNumber < 1)
            {
                result.SparkHttpStatusCodeResult =
                    new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument,
                        String.Format("(from)request.MemberId: {0} is not valid", request.MemberId));
                return result;
            }

            if (request.PageNumber < 1)
            {
                result.SparkHttpStatusCodeResult =
                    new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument,
                        String.Format("Page number is 1 indexed, must be at least 1.", request.PageNumber));
                return result;
            }

            if (request.PageSize < 1)
            {
                result.SparkHttpStatusCodeResult =
                    new SparkHttpBadRequestResult((int)HttpSub400StatusCode.MissingArgument,
                        String.Format("Page size must be at least 1.", request.PageSize));
                return result;
            }

            #endregion

            var startRow = request.PageSize * (request.PageNumber - 1) + 1;

            System.Collections.ICollection matchnetMessages;

            // Call the new SA method to retrive system folders merged with VIP folders.
            if (request.FolderId <= (int)SystemFolders.Trash)
            {
                matchnetMessages = _emailMessageAdapter.RetrieveMessagesFromLogicalFolder(request.MemberId,
                    request.Brand.Site.Community.CommunityID, request.FolderId, startRow, request.PageSize,
                    "InsertDate desc", request.Brand);
            }
            else
            {
                matchnetMessages = _emailMessageAdapter.RetrieveMessages(request.MemberId,
                    request.Brand.Site.Community.CommunityID,
                    request.FolderId, startRow, request.PageSize,
                    "InsertDate desc", request.Brand);
            }

            // is a null check necessary here?

            var canReadReceipt = HasReadReceiptAccessPrivilege(request.Brand, request.MemberId);

            // in order to be able to step through with a single thread
            var maxDegree = 50;
#if DEBUG
            maxDegree = 1;
#endif
            var parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = maxDegree };

            var hasAllAccessMessage = false;

            // it's a non generic collection, use Linq's Cast function
            var ctx = HttpContext.Current;
            Parallel.ForEach(matchnetMessages.Cast<EmailMessage>(), parallelOptions, messageItem =>
            {
                //set HttpContext for thread
                HttpContext.Current = ctx;
                try
                {
                    if (messageItem.MessageList == null || messageItem.Message == null) return;

                    var message = GetMailMessage(messageItem, request.Brand, request.MemberId, canReadReceipt);

                    if (message == null) return;

                    if (message.MemberFolderId == (int) SystemFolders.VIPInbox ||
                        message.MemberFolderId == (int) SystemFolders.VIPTrash)
                        hasAllAccessMessage = true;

                    lock (_lockMe)
                    {
                        result.MailFolder.MessageList.Add(message);
                    }
                }
                finally
                {
                    //unset HttpContext for thread (precaution for GC)
                    HttpContext.Current = null;
                }
            });

            // let's determine whether the member is eligible to retrieve messages
            var isPayingMember = Rest.DataAccess.Subscription.SubscriptionStatus.MemberIsSubscriber(request.Brand, request.MemberId);

            // the member must have at least one read or unread AA or a subscriber
            if (!hasAllAccessMessage && !isPayingMember)
            {
                result.IsRetrieved = false;
                result.SparkHttpStatusCodeResult = new SparkHttpUnauthorizedResult((int)HttpSub401StatusCode.SubscriberIdRequired,
                    String.Format(
                        "Access to this endpoint is restricted to subscriber members.  Member {0} does not have a subscription or a valid All Access message",
                        request.MemberId));
                return result;
            }

            // need to remove any regular messages from the list if the member is only eligible for reading AA
            if (hasAllAccessMessage && !isPayingMember)
            {
                var filteredMessageList = result.MailFolder.MessageList.Where(message => message.MemberFolderId == (int) SystemFolders.VIPInbox || message.MemberFolderId == (int) SystemFolders.VIPTrash).ToList();

                result.MailFolder.MessageList = filteredMessageList;
            }

            result.IsRetrieved = true;
            result.MailFolder.MessageList = result.MailFolder.MessageList.OrderByDescending(x => x.InsertDate).ToList();

            return result;
        }

        /// <summary>
        ///     Ported and refactored from FWS with modifications.
        ///     Once a message has been sent, send notifications including External Mail to
        ///     let the recipient know that they have a message
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="isAllAccess"></param>
        /// <param name="isFraudHold"></param>
        /// <param name="sendResult"></param>
        /// <param name="freeReplyToAllAccess"></param>
        /// <param name="fromMemberId"></param>
        /// <param name="messageSave"></param>
        public void SendExternalMailNotificationForSent(Brand brand, bool isAllAccess, bool isFraudHold,
            MessageSendResult sendResult, bool freeReplyToAllAccess, int fromMemberId, MessageSave messageSave)
        {
            // this is the default for a regular mail message
            if (!isAllAccess && !isFraudHold)
            {
                _externalMailAdapter.SendInternalMailNotification(
                    sendResult.EmailMessage, brand.BrandID, sendResult.RecipientUnreadCount);
                return;
            }

            if (isAllAccess)
            {
                // if this email is a fraud hold email then the external email notification should be sent when the email is unfrauded by CS
                if (!isFraudHold)
                    _externalMailAdapter.SendAllAccessInitialEmail(fromMemberId, brand.BrandID,
                        sendResult.RecipientMessageListID, messageSave.ToMemberID, brand.BrandID,
                        messageSave.MessageHeader,
                        messageSave.MessageBody);

                // if this email is a fraud hold, start the tracker with "READ" status so that tracker ignores this entry
                // only when this is marked as unread when unfraud happens, EmailTracker should pay attention to it
                _emailTrackerAdapter.AddTrackedEmail(fromMemberId, messageSave.ToMemberID,
                    sendResult.RecipientMessageListID, brand.BrandID,
                    (int) PrivilegeType.AllAccessEmails, isFraudHold);
            }

            if (isAllAccess && freeReplyToAllAccess)
            {
                // if this email is a fraud hold email then the external email notification should be sent when the email is un frauded
                if (!isFraudHold)
                {
                    _externalMailAdapter.SendAllAccessReplyToInitialEmail(fromMemberId,
                        brand.BrandID, sendResult.RecipientMessageListID,
                        messageSave.ToMemberID, brand.BrandID, messageSave.MessageHeader,
                        messageSave.MessageBody);
                }
            }
        }

        /// <summary>
        ///     Refactored
        /// </summary>
        public bool SendDraftMessage(Brand brand, MessageSave messageSave, int fromMemberId, out int draftMailId)
        {
            var saved = _emailMessageAdapter.SaveMessage(messageSave, out draftMailId);

            if (!saved || draftMailId <= 0) return false;

            //only mark draft saved if it's a new draft
            if (messageSave.MessageListID != draftMailId)
            {
                _emailMessageAdapter.MarkDraftSaved(fromMemberId, brand.Site.Community.CommunityID, true, draftMailId);
            }

            return true;
        }

        /// <summary>
        ///     Refactored, if recipient has blocked sender, send blocked email and return result
        /// </summary>
        /// <param name="recipientMemberId"></param>
        /// <param name="brand"></param>
        /// <param name="messageSave"></param>
        /// <param name="fromMemberId"></param>
        /// <param name="sendResult"></param>
        /// <returns></returns>
        public bool SendBlockedEmail(int recipientMemberId, Brand brand, MessageSave messageSave,
            int fromMemberId, out MessageSendResult sendResult)
        {
            var toMember = _memberAdapter.GetMember(recipientMemberId, MemberLoadFlags.None);
            var messageHeader = _resourceProviderAdapter.GetResourceValue(brand.Site.SiteID,
                brand.Site.CultureInfo, ResourceGroupEnum.Global, "TXT_IGNORE_SUBJECT");
            var messageBody = _resourceProviderAdapter.GetResourceValue(brand.Site.SiteID,
                brand.Site.CultureInfo, ResourceGroupEnum.Global, "TXT_IGNORE_CONTENT");

            sendResult = _emailMessageAdapter.CheckAndSendMessageForBlockedSenderInRecipientList(
                brand.Site.Community.CommunityID,
                string.Format(messageHeader, messageSave.MessageHeader),
                string.Format(messageBody, toMember.GetUserName(brand)),
                recipientMemberId, fromMemberId, brand.Site.SiteID,
                brand.Site.Community.CommunityID);

            return sendResult != null && sendResult.Status == MessageSendStatus.Success;
        }

        public MailMessageV2 GetMailMessage(EmailMessage matchnetMessage, Brand brand, int memberId, bool canReadReceipt)
        {
            var messageListItem = matchnetMessage.MessageList;
            var message = matchnetMessage.Message;

            Dictionary<string, object> sender;
            try
            {
                sender = ProfileAccess.GetAttributeSetWithPhotoAttributes(brand,
                    matchnetMessage.FromMemberID,
                    memberId, "resultsetprofile").AttributeSet;
            }
            catch (Exception ex)
            {
                // why are we letting this continue, fixing bad data?
                Log.LogError(string.Format("bad member with id {0}", matchnetMessage.FromMemberID), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                return null;
            }

            var openDate = (matchnetMessage.OpenDate == DateTime.MinValue)
                ? matchnetMessage.OpenDate
                : matchnetMessage.OpenDate.ToUniversalTime();

            var mailMessage = new MailMessageV2
            {
                Id = messageListItem.MessageListID,
                MessageId = matchnetMessage.MailID,
                Sender = sender,
                Recipient = ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, matchnetMessage.ToMemberID,
                    memberId, "resultsetprofile").AttributeSet,
                Body = message.MessageBody,
                Subject = matchnetMessage.Subject,
                InsertDate = matchnetMessage.InsertDate.ToUniversalTime(),
                OpenDate = canReadReceipt ? (DateTime?) openDate : null,
                OpenDatePst = canReadReceipt ? (DateTime?)matchnetMessage.OpenDate : null,
                MessageStatus = (int) matchnetMessage.StatusMask,
                MailType = Enum.GetName(typeof (MailType), matchnetMessage.MailTypeID),
                MemberFolderId = matchnetMessage.MemberFolderID
            };

            // In case of an ecard, get the message body to build a url to Connect.
            if (mailMessage.MailType == "ECard")
            {
                mailMessage = GetEcardMailMessage(brand, memberId, mailMessage);
            }

            return mailMessage;
        }

        public MailMessageV2 GetEcardMailMessage(Brand brand, int memberId, MailMessageV2 mailMessage)
        {
            var messageWithBody = MailController.GetMessage(brand.BrandID,
                memberId,
                mailMessage.Id);

            var navigateUrl = new StringBuilder(messageWithBody.Body);

            navigateUrl.Append(messageWithBody.Body.IndexOf('?') > 0 ? "&" : "?");

            navigateUrl.Append("MessageID=" + mailMessage.Id);
            navigateUrl.Append("&toUserID=" + mailMessage.Recipient["memberId"]);

            mailMessage.Body = "http://connect.jdate.com/" + navigateUrl;

            return mailMessage;
        }

        /// <summary>
        ///     Refactor to not have dependency on Http responses
        /// </summary>
        /// <param name="fromMemberId"></param>
        /// <param name="recipientMemberId"></param>
        /// <param name="requestMailType"></param>
        /// <returns>null if validated</returns>
        private SparkHttpBadRequestResult ValidateSendMailMessageRequest(int fromMemberId, int recipientMemberId,
            string requestMailType)
        {
            if (fromMemberId < 1)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingArgument,
                    String.Format("(from)MemberId: {0} is not valid", fromMemberId));
            }

            if (recipientMemberId < 1)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingArgument,
                    String.Format("recipientMemberId: {0} is not valid", recipientMemberId));
            }

            MailType mailType;

            try
            {
                mailType = (MailType) Enum.Parse(typeof (MailType), requestMailType, true);
            }
            catch
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingArgument,
                    String.Format("Invalid mail type in request: {0}", requestMailType));
            }

            if (mailType != MailType.Email)
            {
                return new SparkHttpBadRequestResult((int) HttpSub400StatusCode.MissingArgument,
                    String.Format("mail type: {0} not implemented", mailType));
            }

            return null;
        }

        /// <summary>
        /// Validation method to see if the member has the right to view read receipt in mail
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberId"></param>
        /// <returns></returns>
        public bool HasReadReceiptAccessPrivilege(Brand brand, int memberId)
        {
            if (!Convert.ToBoolean(_settingsAdapter.GetSettingFromSingleton("ENABLE_READ_RECEIPTS_PREMIUM_SERVICE_FEATURE")))
            {
                return true;
            }

            var member = _memberAdapter.GetMember(memberId, MemberLoadFlags.None);

            // basic sub is required as a base, this will change in the future
            var accessPrivlegeBasicSub = member.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription,
                brand.BrandID, brand.Site.SiteID,
                brand.Site.Community.CommunityID);
            if ((accessPrivlegeBasicSub == null) || accessPrivlegeBasicSub.EndDateUTC < DateTime.Now.ToUniversalTime())
                // no basic sub, reject
                return false;

            // all access emails allowed to have read receipt
            var accessPrivlegeAllAccess = member.GetUnifiedAccessPrivilege(PrivilegeType.AllAccessEmails,
                brand.BrandID, brand.Site.SiteID,
                brand.Site.Community.CommunityID);
            if (accessPrivlegeAllAccess != null && accessPrivlegeAllAccess.EndDateUTC > DateTime.Now.ToUniversalTime())
                return true;

            // read receipt priv allowed to have read receipt, duh
            var accessPrivlegeReadReceipt = member.GetUnifiedAccessPrivilege(PrivilegeType.ReadReceipt,
                brand.BrandID, brand.Site.SiteID,
                brand.Site.Community.CommunityID);
            if (accessPrivlegeReadReceipt != null &&
                accessPrivlegeReadReceipt.EndDateUTC > DateTime.Now.ToUniversalTime()) return true;

            return false;
        }
    }
}