#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Spark.Common.AccessService;
using Spark.Common.MemberLevelTracking;
using Spark.Logger;
using Spark.Managers.Managers;
using Spark.Rest.Helpers;
using Spark.REST.Entities.Mail;
using Spark.Rest.V2.Entities.Mail;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Models.Mail;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.DataAccess;
using Spark.REST.Entities.Mail;

#endregion

namespace Spark.Rest.V2.DataAccess.Mail
{
    internal class MessageAccess
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof (MessageAccess));

        public static readonly MessageAccess Instance = new MessageAccess();

        private MessageAccess()
        {
        }

        public MessageSendResult SendMessage(Brand brand, int fromMemberId, int toMemberId, MailType mailType,
            string threadId, string messageSubject, string messageBody, DateTime messageDateTime)
        {
            var messageSave = new MessageSave(fromMemberId, toMemberId, brand.Site.Community.CommunityID,
                brand.Site.SiteID, mailType,
                messageSubject, messageBody, messageDateTime, threadId);
            var messageSendResult = EmailMessageSA.Instance.SendMessage(messageSave, true, Constants.NULL_INT, false);
            return messageSendResult;
        }

        public string[] GetFileNameAndExtension(string fileName)
        {
            if (String.IsNullOrEmpty(fileName)) return new string[2];
            var fileNameAndExtension = fileName.Split(new string[] {"."}, StringSplitOptions.RemoveEmptyEntries);
            return fileNameAndExtension;
        }

        public MessageAttachmentUploadResponse[] MessageAttachmentUploadResponses(IList<HttpPostedFileBase> photoFiles,
            int fromMemberId, int toMemberId, int communityId, int siteId, Application app,
            AttachmentType attachmentType, int messageId)
        {
            var photoCount = (null != photoFiles) ? photoFiles.Count : 0;
            var photoUpdateErrors = new Dictionary<int, MessageAttachmentUploadResponse>();
            var photoUpdates = new Dictionary<int, PhotoUpdate>();
            for (var i = 0; null != photoFiles && i < photoCount; i++)
            {
                var photo = photoFiles[i];
                byte[] newFileBytes = null;
                string fileName = null;
                var messageAttachmentUploadResponse = ValidateAttachment(photo, attachmentType, fromMemberId,
                    out newFileBytes, out fileName);
                if (null != messageAttachmentUploadResponse)
                {
                    photoUpdateErrors.Add(i, messageAttachmentUploadResponse);
                }
                else
                {
                    var photoUpdate = new PhotoUpdate(newFileBytes,
                        false,
                        Constants.NULL_INT,
                        Constants.NULL_INT,
                        null,
                        Constants.NULL_INT,
                        null,
                        0,
                        false,
                        false,
                        Constants.NULL_INT,
                        Constants.NULL_INT,
                        fileName, //
                        false);

                    photoUpdates.Add(i, photoUpdate);
                }
            }

            var uploadResponses = new MessageAttachmentUploadResponse[photoCount];
            for (var i = 0; null != photoUpdates && i < photoCount; i++)
            {
                MessageAttachmentUploadResponse messageAttachmentUploadResponse = null;
                if (photoUpdateErrors.ContainsKey(i))
                {
                    messageAttachmentUploadResponse = photoUpdateErrors[i];
                }
                else if (photoUpdates.ContainsKey(i))
                {
                    var photoUpdate = photoUpdates[i];
                    var fileName = photoUpdate.Caption;
                    MessageAttachmentSaveResult messageAttachmentSaveResult = null;
                    var fileNameAndExtension = Instance.GetFileNameAndExtension(fileName);
                    try
                    {
                        messageAttachmentSaveResult = EmailMessageSA.Instance.SaveMessageAttachment(fromMemberId,
                            toMemberId,
                            photoUpdate.ImageData,
                            fileNameAndExtension[1],
                            attachmentType,
                            messageId,
                            communityId,
                            siteId,
                            app);
                    }
                    catch (Exception ex)
                    {
                        Log.LogException(
                            string.Format("Exception saving file: {0} for fromMemberId: {1} to toMemberId:{2}", fileName,
                                fromMemberId, toMemberId), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                        messageAttachmentSaveResult = new MessageAttachmentSaveResult();
                        messageAttachmentSaveResult.Error = MessageAttachmentSaveError.Unknown;
                        messageAttachmentSaveResult.Status = MessageAttachmentSaveStatus.Failed;
                    }

                    if (null == messageAttachmentSaveResult)
                    {
                        messageAttachmentSaveResult = new MessageAttachmentSaveResult();
                        messageAttachmentSaveResult.Status = MessageAttachmentSaveStatus.Failed;
                    }

                    if (messageAttachmentSaveResult.Status != MessageAttachmentSaveStatus.Success)
                    {
                        messageAttachmentSaveResult.Error = MessageAttachmentSaveError.Unknown;
                    }

                    messageAttachmentUploadResponse = new MessageAttachmentUploadResponse
                    {
                        SuccessCode = (int) messageAttachmentSaveResult.Status,
                        FailureReason = messageAttachmentSaveResult.Error.ToString(),
                        MessageAttachmentId = messageAttachmentSaveResult.MessageAttachmentID,
                        PreviewUrl = messageAttachmentSaveResult.PreviewCloudPath,
                        FileName = fileNameAndExtension[0],
                        OriginalExtension = fileNameAndExtension[1]
                    };
                    if (messageAttachmentSaveResult.Error != MessageAttachmentSaveError.None)
                    {
                        messageAttachmentUploadResponse.HttpSubStatusCode =
                            (int) HttpSub500StatusCode.InternalServerError;
                    }
                }
                uploadResponses[i] = messageAttachmentUploadResponse;
            }
            return uploadResponses;
        }

        /// <summary>
        /// Validation method to see if the member has the right to view read receipt in mail
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberId"></param>
        /// <returns></returns>
        public bool HasReadReceiptAccessPrivilege(Brand brand, int memberId)
        {
            if (!Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_READ_RECEIPTS_PREMIUM_SERVICE_FEATURE", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID)))
            {
                return true;
            }

            var member = MemberSA.Instance.GetMember(memberId);
            
            // basic sub is required as a base, this will change in the future
            var accessPrivlegeBasicSub = member.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription,
                brand.BrandID, brand.Site.SiteID,
                brand.Site.Community.CommunityID);
            if ((accessPrivlegeBasicSub == null) || accessPrivlegeBasicSub.EndDateUTC < DateTime.Now.ToUniversalTime())
                // no basic sub, reject
                return false;
            
            // all access emails allowed to have read receipt
            var accessPrivlegeAllAccess = member.GetUnifiedAccessPrivilege(PrivilegeType.AllAccessEmails,
                brand.BrandID, brand.Site.SiteID,
                brand.Site.Community.CommunityID);
            if (accessPrivlegeAllAccess != null && accessPrivlegeAllAccess.EndDateUTC > DateTime.Now.ToUniversalTime())
                return true;
            
            // read receipt priv allowed to have read receipt, duh
            var accessPrivlegeReadReceipt = member.GetUnifiedAccessPrivilege(PrivilegeType.ReadReceipt,
                brand.BrandID, brand.Site.SiteID,
                brand.Site.Community.CommunityID);
            if (accessPrivlegeReadReceipt != null &&
                accessPrivlegeReadReceipt.EndDateUTC > DateTime.Now.ToUniversalTime()) return true;

            return false;
        }

        private static MessageAttachmentUploadResponse ValidateAttachment(HttpPostedFileBase photo,
            AttachmentType attachmentType, int fromMemberId, out byte[] newFileBytes, out string fileName)
        {
            newFileBytes = null;
            fileName = null;
            switch (attachmentType)
            {
                case AttachmentType.IMPhoto:
                    if (photo == null || photo.ContentLength <= 0)
                    {
                        var messageAttachmentUploadResponse = new MessageAttachmentUploadResponse
                        {
                            FailureReason = "No photo submitted",
                            HttpSubStatusCode = (int) HttpSub400StatusCode.MissingPhotoUploadData
                        };
                        return messageAttachmentUploadResponse;
                    }

                    fileName = Path.GetFileName(photo.FileName);
                    if (fileName == null)
                    {
                        var messageAttachmentUploadResponse = new MessageAttachmentUploadResponse
                        {
                            FailureReason = "Missing photo file name",
                            HttpSubStatusCode = (int) HttpSub400StatusCode.MissingPhotoUploadData
                        };
                        return messageAttachmentUploadResponse;
                    }

                    var photoRules = new PhotoRulesManager();
                    var fileNameAndExtension = Instance.GetFileNameAndExtension(fileName);
                    if (!photoRules.FileNameExtensionAllowed(fileName))
                    {
                        var messageAttachmentUploadResponse = new MessageAttachmentUploadResponse
                        {
                            FailureReason =
                                string.Format("Invalid file type: {0}", HttpUtility.JavaScriptStringEncode(fileName)),
                            HttpSubStatusCode = (int) HttpSub400StatusCode.UnsupportedPhotoFormat,
                            FileName = fileNameAndExtension[0],
                            OriginalExtension = fileNameAndExtension[1]
                        };
                        return messageAttachmentUploadResponse;
                    }

                    if (!photoRules.IsFileSizeAllowed(photo.ContentLength))
                    {
                        var messageAttachmentUploadResponse = new MessageAttachmentUploadResponse
                        {
                            FailureReason =
                                string.Format("Maximum file size exceeded {0}",
                                    HttpUtility.JavaScriptStringEncode(fileName)),
                            HttpSubStatusCode = (int) HttpSub400StatusCode.MaxPhotoSizeExceeded,
                            FileName = fileNameAndExtension[0],
                            OriginalExtension = fileNameAndExtension[1]
                        };
                        return messageAttachmentUploadResponse;
                    }

                    if (!photoRules.IsValidImage(photo.InputStream, photo.ContentLength))
                    {
                        var messageAttachmentUploadResponse = new MessageAttachmentUploadResponse
                        {
                            FailureReason =
                                string.Format("The file {0} is not a valid image",
                                    HttpUtility.JavaScriptStringEncode(fileName)),
                            HttpSubStatusCode = (int) HttpSub400StatusCode.UnsupportedPhotoFormat,
                            FileName = fileNameAndExtension[0],
                            OriginalExtension = fileNameAndExtension[1]
                        };
                        return messageAttachmentUploadResponse;
                    }

                    using (var fileStream = photo.InputStream)
                    {
                        try
                        {
                            fileStream.Position = 0;
                            newFileBytes = new byte[photo.ContentLength];
                            fileStream.Read(newFileBytes, 0, photo.ContentLength);
                            photo.InputStream.Flush();
                        }
                        catch (Exception ex)
                        {
                            Log.LogException(
                                string.Format("Exception reading file for member: {0} filename: {1}", fromMemberId,
                                    fileName), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                        }
                    }

                    if (newFileBytes == null || newFileBytes.Length <= 0)
                    {
                        var messageAttachmentUploadResponse = new MessageAttachmentUploadResponse
                        {
                            FailureReason =
                                string.Format("Failure reading photo with name:{0}",
                                    HttpUtility.JavaScriptStringEncode(fileName)),
                            HttpSubStatusCode = (int) HttpSub400StatusCode.MissingPhotoUploadData,
                            FileName = fileNameAndExtension[0],
                            OriginalExtension = fileNameAndExtension[1]
                        };
                        return messageAttachmentUploadResponse;
                    }
                    break;
                default:
                    break;
            }
            return null;
        }

        public Dictionary<string, Dictionary<MailType, int>> GetUnreadMessageCount(int memberId, Brand b)
        {
            if (null == b) throw new SparkAPIReportableArgumentException("Brand cannot be null");
            if (memberId < 1) throw new SparkAPIReportableArgumentException("MemberId must be greater than 0.");

            try
            {
                Dictionary<string, Dictionary<MailType, int>> unreadMessageCounts = EmailMessageSA.Instance.GetUnreadMessageCounts(memberId, b);
                return unreadMessageCounts;
            }
            catch (Exception e)
            {
                string message =
                    string.Format("Failure getting messages for memberId:{0}, brandId:{1}", memberId,
                        b.BrandID);
                Log.LogException(message, e, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(b));
                throw new SparkAPIException(message);
            }
        }
		
        public List<MailMessageV2> GetInstantMessageHistory(int memberID, int targetMemberID, Brand brand, int pageNumber, int pageSize, string threadID, bool ignoreCache, out int totalCount)
        {
            if (memberID < 1) throw new SparkAPIReportableException(HttpSub400StatusCode.InvalidMemberId, "MemberId must be greater than 0.");
            if (targetMemberID < 1) throw new SparkAPIReportableException(HttpSub400StatusCode.InvalidMemberId, "TargetMemberId must be greater than 0.");

            int startRow = ((pageNumber - 1) * pageSize) + 1;

            EmailMessageList messagesPage = EmailMessageSA.Instance.RetrieveInstantMessages(memberID, targetMemberID, brand.Site.Community.CommunityID, startRow, pageSize, threadID, ignoreCache);
            totalCount = 0;
            if (messagesPage != null)
            {
                totalCount = messagesPage.TotalCount;
            }
            return ConvertEmailMessageListToMailMessageList(messagesPage, brand, memberID);
        }

        public List<MailMessageV2> GetMailConversationMessages(int memberID, int targetMemberID, Brand brand, int pageNumber, int pageSize, ConversationType conversationType, bool markAsRead, out int totalCount)
        {
            if (memberID < 1) throw new SparkAPIReportableException(HttpSub400StatusCode.InvalidMemberId, "MemberId must be greater than 0.");
            if (targetMemberID < 1) throw new SparkAPIReportableException(HttpSub400StatusCode.InvalidMemberId, "TargetMemberId must be greater than 0.");

            //conversation types not supported
            if (conversationType == ConversationType.Trash || conversationType == ConversationType.None)
            {
                throw new SparkAPIReportableException(HttpSub400StatusCode.InvalidConversationType, "Conversation type " + conversationType.ToString() + " is not currently supported for messages");
            }


            int startRow = ((pageNumber - 1) * pageSize) + 1;

            EmailMessageList messagesPage = EmailMessageSA.Instance.RetrieveMailConversationMessages(memberID, targetMemberID, brand.Site.Community.CommunityID, startRow, pageSize, conversationType, markAsRead);
            totalCount = 0;
            if (messagesPage != null)
            {
                totalCount = messagesPage.TotalCount;
            }
            return ConvertEmailMessageListToMailMessageList(messagesPage, brand, memberID);
        }

        public List<MailMessageV2> ConvertEmailMessageListToMailMessageList(EmailMessageList emailMessageList, Brand brand, int memberId)
        {
            List<MailMessageV2> messageHistoryList = new List<MailMessageV2>();

            if (emailMessageList != null && emailMessageList.Count > 0)
            {
                var canReadReceipt = MessageAccess.Instance.HasReadReceiptAccessPrivilege(brand, memberId);
                foreach (EmailMessage message in emailMessageList)
                {
                    messageHistoryList.Add(ConvertEmailMessageToMailMessage(message, canReadReceipt));
                }
            }

            return messageHistoryList;
        }

        public MailMessageV2 ConvertEmailMessageToMailMessage(EmailMessage message, bool canReadReceipt)
        {
            DateTime? openDate = message.OpenDate == DateTime.MinValue ? message.OpenDate : (DateTime?)message.OpenDate.ToUniversalTime();
            DateTime? openDatePst = message.OpenDate == DateTime.MinValue ? message.OpenDate : (DateTime?)message.OpenDate;
            var mailMessage = new MailMessageV2
            {
                Id = message.MessageList.MessageListID,
                Body = message.Message.MessageBody,
                Subject = message.Message.MessageHeader,
                InsertDate = message.Message.InsertDate.ToUniversalTime(),
                InsertDatePst = message.Message.InsertDate,
                MessageStatus = (int)message.StatusMask,
                MailType = Enum.GetName(typeof(MailType), message.MailTypeID),
                SenderId = message.FromMemberID,
                MemberFolderId = message.MemberFolderID,
                ThreadId = message.ThreadID,
                OpenDate = canReadReceipt ? openDate : null,
                OpenDatePst = canReadReceipt ? openDatePst : null
            };

            mailMessage.IsAllAccess = IsAllAccessFolder(message.MemberFolderID);

            return mailMessage;
        }

        public bool IsAllAccessFolder(int memberFolderId)
        {
            switch (memberFolderId)
            {
                case (int)SystemFolders.VIPDraft:
                case (int)SystemFolders.VIPInbox:
                case (int)SystemFolders.VIPSent:
                case (int)SystemFolders.VIPTrash:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Delete a mail message.
        /// </summary>
        /// <param name="deleteMessage">The mail message to delete</param>
        /// <param name="memberId">The current member id.</param>
        /// <param name="brand">The current brand.</param>
        /// <returns>The transaciotn resutling from deleting the mail message.</returns>
        public PostMessageResult DeleteMailMessage(DeleteMessage deleteMessage, int memberId, Brand brand)
        {
            bool deleted = false;

            try
            {
                if (deleteMessage.CurrentFolderId == (Int32)SystemFolders.Trash)
                {
                    deleted = EmailMessageSA.Instance.DeleteMessage(memberId,
                        brand.Site.Community.CommunityID,
                        new ArrayList { deleteMessage.MessageId });
                }
                else
                {
                    deleted = EmailMessageSA.Instance.MoveToFolder(memberId,
                        brand.Site.Community.CommunityID,
                        new ArrayList { deleteMessage.MessageId },
                        (Int32)SystemFolders.Trash);
                }

                return new PostMessageResult
                {
                    MessageId = deleteMessage.MessageId,
                    Success = deleted,
                    FailureReason = string.Empty
                };
            }
            catch (Exception ex)
            {
                var message = string.Format("Failure deleting messageId:{0} for memberId:{1}, brandId:{2}",
                    deleteMessage.MessageId,
                    memberId,
                    brand.BrandID);

                Log.LogException(message, ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));

                return new PostMessageResult
                {
                    MessageId = deleteMessage.MessageId,
                    Success = deleted,
                    FailureReason = ex.Message
                };

            }
        }

        public MessageGeneralResult DeleteMailConversation(int memberID, int targetMemberID, Brand brand, int messageListID, ConversationType conversationType)
        {
            //delete conversation (all emails)
            MessageGeneralResult result = EmailMessageSA.Instance.DeleteMailConversation(memberID, targetMemberID, brand.Site.Community.CommunityID, messageListID, conversationType, true);

            //delete IM chat as well
            if (result.Status == MessageGeneralResultStatus.Success)
            {
                if (conversationType == ConversationType.Inbox || conversationType == ConversationType.Trash)
                {
                    result = EmailMessageSA.Instance.DeleteInstantMessages(memberID, targetMemberID, brand.Site.Community.CommunityID, messageListID);
                }
            }

            return result;
            
        }

        public bool MarkRead(int memberID, int communityID, List<int> messageListIDs)
        {
            if (messageListIDs == null || messageListIDs.Count <= 0)
            {
                throw new SparkAPIReportableException(HttpSub400StatusCode.MissingArgument, "Message Ids to mark as read are missing");
            }

            return EmailMessageSA.Instance.MarkRead(memberID, communityID, true, new ArrayList(messageListIDs));
        }

        public List<MailMember> GetInstantMessageHistoryMembers(int memberID, Brand brand, int pageNumber, int pageSize, bool ignoreCache, int bodyLength, out int totalCount)
        {
            if (memberID < 1) throw new SparkAPIReportableException(HttpSub400StatusCode.InvalidMemberId, "MemberId must be greater than 0.");

            int startRow = ((pageNumber - 1) * pageSize) + 1;

            MessageMemberCollection messageMembers = EmailMessageSA.Instance.RetrieveInstantMessageMembers(memberID, brand.Site.Community.CommunityID, true, startRow, pageSize, ignoreCache);
            totalCount = 0;
            if (messageMembers != null)
            {
                totalCount = messageMembers.TotalCount;
            }
            return ConvertMessageMemberCollectionToMailMemberList(messageMembers, brand, bodyLength);
        }

        public List<MailMember> GetMailConversationMembers(int memberID, Brand brand, ConversationType conversationType, int pageNumber, int pageSize, int bodyLength, out int totalCount)
        {
            if (memberID < 1) throw new SparkAPIReportableException(HttpSub400StatusCode.InvalidMemberId, "MemberId must be greater than 0.");

            int startRow = ((pageNumber - 1) * pageSize) + 1;

            MessageMemberCollection messageMembers = EmailMessageSA.Instance.RetrieveMailConversationMembers(memberID, brand.Site.Community.CommunityID, conversationType, startRow, pageSize);
            totalCount = 0;
            if (messageMembers != null)
            {
                totalCount = messageMembers.TotalCount;
            }
            return ConvertMessageMemberCollectionToMailMemberList(messageMembers, brand, bodyLength);
        }

        public List<MailMember> ConvertMessageMemberCollectionToMailMemberList(MessageMemberCollection messageMemberCollection, Brand brand, int bodyLength)
        {
            List<MailMember> mailMemberList = new List<MailMember>();

            if (messageMemberCollection != null && messageMemberCollection.Count > 0)
            {
                foreach (MessageMember message in messageMemberCollection)
                {
                    mailMemberList.Add(ConvertMessageMemberToMailMember(messageMemberCollection.MemberID, message, brand, bodyLength));
                }
            }

            return mailMemberList;
        }

        public MailMember ConvertMessageMemberToMailMember(int memberID, MessageMember message, Brand brand, int bodyLength)
        {
            var mailMember = new MailMember
            {
                Id = message.MessageListID,
                MemberId = message.TargetMemberID,
                GroupId = message.GroupID,
                InsertDate = message.InsertDate.ToUniversalTime(),
                InsertDatePst = message.InsertDate,
                MessageStatus = (int)message.StatusMask,
                MailType = Enum.GetName(typeof(MailType), message.MailTypeID),
                AllAccess = message.IsAllAccess,
                UnreadAllAccessCount = message.UnreadVipCount,
                UnreadMessageCount = message.UnreadMessageCount,
                DraftCount = message.DraftCount,
                AllAccessCount = message.VipCount,
                IsBodyAllAccess = IsAllAccessFolder(message.MemberFolderID),
                Member = ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, message.TargetMemberID, memberID, "resultsetprofile").AttributeSet,
            };

            if (bodyLength > 0)
            {
                mailMember.Body = (bodyLength > message.Body.Length) ? message.Body : message.Body.Substring(0, bodyLength);
            }

            return mailMember;
        }

        public LastConversation GetInstantMessageLastConversation(int memberID, int targetMemberID, int groupID)
        {
            return EmailMessageSA.Instance.GetInstantMessengerLastConversation(memberID, targetMemberID, groupID, true);
        }

        public void RemoveMailConversationMembersCache(int memberID, int groupID, ConversationType conversationType)
        {
            EmailMessageSA.Instance.RemoveMailConversationMembersCache(memberID, groupID, conversationType);
        }
    }
}