﻿#region

using System;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ServiceAdapters.Interfaces;
using Matchnet.Email.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Spark.Common.AccessService;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.Rest.V2.DataAccess.Mail.Interfaces;
using Spark.Rest.V2.Entities.Mail;

#endregion

namespace Spark.Rest.V2.DataAccess.Mail
{
    /// <summary>
    /// </summary>
    public class AllAccessAccess : IAllAccessAccess
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(AllAccessAccess));

        private readonly IEmailBlockedSender _emailMessageAdapter;
        private readonly IMemberAllAccess _memberAllAccessAdapter;
        private readonly ISettingsSA _settingsAdapter;

        /// <summary>
        ///     for DI
        /// </summary>
        public AllAccessAccess(IMemberAllAccess memberAllAccessAdapter, IEmailBlockedSender emailMessageAdapter,
            ISettingsSA settingsAdapter)
        {
            _memberAllAccessAdapter = memberAllAccessAdapter;
            _emailMessageAdapter = emailMessageAdapter;
            _settingsAdapter = settingsAdapter;
        }

        /// <summary>
        ///     Ported from FWS with modifications
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberid"></param>
        /// <returns></returns>
        public bool IsAllAccessMember(Brand brand, int memberid)
        {
            var allAccess = false;
            var allAccessMember = GetAllAccessMember(brand, memberid);

            if (allAccessMember != null)
                allAccess = allAccessMember.AllAccessExpireDate > DateTime.Now;

            return allAccess;
        }

        /// <summary>
        ///     Ported from FWS with modifications
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberid"></param>
        /// <returns></returns>
        public AllAccessMember GetAllAccessMember(Brand brand, int memberid)
        {
            var siteid = brand.Site.SiteID;
            var communityid = brand.Site.Community.CommunityID;
            var brandid = brand.BrandID;
            var samember = _memberAllAccessAdapter.GetMember(memberid, MemberLoadFlags.None);
            var privdate = samember.GetUnifiedAccessPrivilege(PrivilegeType.AllAccess, brandid, siteid, communityid);
            var privcount = samember.GetUnifiedAccessPrivilege(PrivilegeType.AllAccessEmails, brandid, siteid,
                communityid);

            // if we can't even get the date, this person doesn't have vip access
            if (privdate == null) return null;
            var member = new AllAccessMember {AllAccessExpireDate = privdate.EndDatePST};
            //JS-1329
            Log.LogInfoMessage(string.Format("MemberID: {0}, SubscriptionName:{1}, SubExpirationDatePST: {2}", memberid, PrivilegeType.AllAccess.ToString(), privdate.EndDatePST), ErrorHelper.GetCustomData());

            // you can have all access time-based but the count-based portion could be null so check for null here
            if (privcount != null)
                member.EmailCount = privcount.RemainingCount;

            return member;
        }

        /// <summary>
        ///     Ported from FWS with modifications
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberId"></param>
        /// <param name="count"></param>
        public void AdjustMemberAccessEmailCount(Brand brand, int memberId, int count)
        {
            //Call MemberSA to adjust count, this will update local cache while waiting for synchronization from Access

            var result = _memberAllAccessAdapter.AdjustUnifiedAccessCountPrivilege(memberId, Constants.NULL_INT, "",
                new[] {(int) PrivilegeType.AllAccessEmails}, brand.BrandID, brand.Site.SiteID,
                brand.Site.Community.CommunityID, count);
            if (!result)
            {
                throw new ApplicationException(
                    string.Format("AdjustCountPrivilege failed MemberID:{0}, PrivilegeType:{1}, SiteID:{2}",
                        memberId, (int) PrivilegeType.AllAccessEmails, brand.Site.SiteID));
            }
        }

        /// <summary>
        ///     Ported from FWS with modifications
        ///     This method checks the message being replied to has the FreeReply mask or not.
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="fromMemberId"></param>
        /// <param name="replyMailId"></param>
        public bool IsThisFreeReply(Brand brand, int fromMemberId, int replyMailId)
        {
            var freeReply = false;

            if (!IsAllAcessEnabledSite(brand) || replyMailId <= 0)
                return false;

            var message = _emailMessageAdapter.RetrieveMessage(brand.Site.Community.CommunityID,
                fromMemberId,
                replyMailId,
                null,
                Direction.None,
                false,
                brand);

            if (message == null) return false;

            if ((message.StatusMask & MessageStatus.OneFreeReply) == MessageStatus.OneFreeReply)
            {
                freeReply = true;
                Log.LogInfoMessage(string.Format("One Free Reply message for recipient:{0} replyMailId:{1}", fromMemberId,replyMailId), ErrorHelper.GetCustomData());
            }

            return freeReply;
        }

        /// <summary>
        ///     Ported from FWS with modifications
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        public bool IsAllAcessEnabledSite(Brand brand)
        {
            return Convert.ToBoolean(_settingsAdapter.GetSettingFromSingleton(SettingConstants.VIP_EMAIL_ENABLED,
                brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                brand.BrandID));
        }
    }
}