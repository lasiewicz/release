﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Promotion;
using Spark.REST.Models.Content.Promotion;

namespace Spark.Rest.DataAccess.Content
{
    public class PromotionDataAccess
    {
        public static readonly PromotionDataAccess Instance = new PromotionDataAccess();

        private PromotionDataAccess()
        {
        }

        public PromotionMappingResult MapURLToPromo(PromotionMappingRequest request)
        {
            var response = new PromotionMappingResult();

            PRMURLMapperList prmList = PromotionSA.Instance.GetPRMURLMapperList();

            if (prmList != null)
            {
                PRMURLMapper prmURLMapper = prmList.GetPRMByReferralURL(request.ReferralHost);
                if (prmURLMapper != null)
                {
                    if (String.IsNullOrEmpty(prmURLMapper.SourceURL))
                    {
                        if (!string.IsNullOrEmpty(request.ReferralURL) && request.ReferralURL.Length > 150)
                        {
                            request.ReferralURL = request.ReferralURL.Substring(0, 150);
                        }
                        //default PRM
                        response.PromotionID = prmURLMapper.PromotionID;
                        response.LuggageID = request.ReferralURL;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(request.ReferralURLQueryString) && request.ReferralURLQueryString.Length > 150)
                        {
                            request.ReferralURLQueryString = request.ReferralURLQueryString.Substring(0, 150);
                        }
                        //matching PRM
                        response.PromotionID = prmURLMapper.PromotionID;
                        response.LuggageID = request.ReferralURLQueryString;
                    }
                }
            }

            return response;
        }
    }
}