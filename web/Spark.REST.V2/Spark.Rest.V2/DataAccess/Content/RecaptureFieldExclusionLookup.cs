﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.REST.DataAccess.Content
{
    internal static class RecaptureFieldExclusionLookup
    {
        internal static readonly List<string> ExcludedFields = new List<string> {"password"};
        
    }
}