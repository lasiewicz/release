﻿#region

using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Spark.Rest.V2.Entities.Content.RegionData;
using Matchnet;

#endregion

namespace Spark.REST.DataAccess.Content
{
    /// <summary>
    ///   These are mostly borrowed from AdminTool/RegionManager.cs TODO: buggy, unit tests not passing
    /// </summary>
    public class RegionDataAccess
    {
        /// <summary>
        /// </summary>
        public const int RegionUs = 223;

        /// <summary>
        /// </summary>
        public const int RegionidCanada = 38;

        private const string CaZipRegEx =
            @"^([ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ])\ {0,1}(\d[ABCEGHJKLMNPRSTVWXYZ]\d)$";

        private const string UsZipRegEx = @"^\d{5}(?:[-\s]\d{4})?$";

        public static readonly RegionDataAccess Instance = new RegionDataAccess();

        private RegionDataAccess()
        {
        }

        // Ported from AdminTool
        // TODO only supports US & Canada as required at this time
        // Returns a list of cities within a zip code.
        public List<Entities.Content.RegionData.Region> GetCitiesByZip(int countryRegionId, string zipCode)
        {
            var regions = new List<Entities.Content.RegionData.Region>();

            // this method shouldn't called for countries outside US and Canada
            if (countryRegionId != RegionUs && countryRegionId != RegionidCanada)
                return regions;

            var colCities = RegionSA.Instance.RetrieveCitiesByPostalCode(zipCode);

            if (colCities == null)
                return regions;

            // for US only there is a possiblity of more cities per zipcode, so check for that
            if (countryRegionId == RegionUs)
            {
                regions.AddRange(from Region region in colCities
                                 select new Entities.Content.RegionData.Region
                                            {
                                                Description = region.Description,
                                                Id = region.RegionID
                                            });
                return regions;
            }

            var regionCanadaId = RegionSA.Instance.FindRegionIdByPostalCode(countryRegionId, zipCode);
            if (regionCanadaId == null) return regions;
            var regionCanada = RegionSA.Instance.RetrieveRegionByID(regionCanadaId.ID, 2);

            regions.Add(new Entities.Content.RegionData.Region
                            {
                                Description = regionCanada.Description,
                                Id = regionCanada.RegionID
                            });

            return regions;
        }

        ///<summary>
        ///</summary>
        ///<returns> </returns>
        public List<Entities.Content.RegionData.Region> GetCountries(int languageId)
        {
            var regions = new List<Entities.Content.RegionData.Region>();

            var countries = RegionSA.Instance.RetrieveCountries(languageId);

            if (countries == null)
                return regions;

            regions.AddRange(from Region region in countries
                             select new Entities.Content.RegionData.Region
                                        {
                                            Description = region.Description,
                                            Id = region.RegionID
                                        });
            return regions;
        }

        ///<summary>
        ///</summary>
        ///<param name="countryRegionId"> </param>
        ///<param name="languageId"> </param>
        ///<returns> </returns>
        public List<Entities.Content.RegionData.Region> GetStates(int countryRegionId, int languageId)
        {
            var regions = new List<Entities.Content.RegionData.Region>();
            RegionCollection regionCollection;

            //if (languageId == (int)Matchnet.Language.Hebrew)
            regionCollection = RegionSA.Instance.RetrieveChildRegions(countryRegionId, languageId);

            regions.AddRange(from Region region in regionCollection
                             select new Entities.Content.RegionData.Region
                                        {
                                            Description = region.Description,
                                            Id = region.RegionID,
                                            Depth = region.Depth
                                        });

            return regions;
        }

        internal List<Entities.Content.RegionData.Region> GetCities(int countryRegionId, int stateRegionId,
                                                                    int languageId)
        {
            var items = new List<Entities.Content.RegionData.Region>();

            var regionIdToPass = countryRegionId;

            if (stateRegionId > 0)
                regionIdToPass = stateRegionId;

            var regionCollection = RegionSA.Instance.RetrieveChildRegions(regionIdToPass, languageId);
            if (regionCollection == null)
                return items;

            foreach (Region r in regionCollection)
            {
                items.Add(new Entities.Content.RegionData.Region
                              {Id = r.RegionID, Description = r.Description, Depth = r.Depth});
            }

            return items;
        }

        ///<summary>
        ///</summary>
        ///<param name="parentRegionId"> </param>
        ///<param name="languageId"> </param>
        ///<returns> </returns>
        public List<Entities.Content.RegionData.Region> GetChildRegions(int parentRegionId, int languageId)
        {
            var regions = new List<Entities.Content.RegionData.Region>();

            var childRegions = RegionSA.Instance.RetrieveChildRegions(parentRegionId, languageId);
            if (childRegions == null)
                return regions;

            regions.AddRange(from Region region in childRegions
                             select new Entities.Content.RegionData.Region
                                        {
                                            Description = region.Description,
                                            Id = region.RegionID,
                                            Depth = region.Depth
                                        });
            return regions;
        }

        public RegionHierarchy GetRegionHierarchy(int regionId, int languageId)
        {
            RegionHierarchy regionHierarchy = new RegionHierarchy();
            var parentRegions = RegionSA.Instance.RetrievePopulatedHierarchy(regionId, languageId);
            if (parentRegions == null)
                return regionHierarchy;

            if (parentRegions.CountryRegionID != 0 && parentRegions.CountryRegionID != Constants.NULL_INT)
            {
                regionHierarchy.Country = new Entities.Content.RegionData.Country();
                regionHierarchy.Country.Id = parentRegions.CountryRegionID;
                regionHierarchy.Country.Depth = 1;
                regionHierarchy.Country.Description = parentRegions.CountryName;
                regionHierarchy.Country.Abbreviation = parentRegions.CountryAbbreviation;
            }

            if (parentRegions.StateRegionID != 0 && parentRegions.StateRegionID != Constants.NULL_INT)
            {
                regionHierarchy.State = new Entities.Content.RegionData.State();
                regionHierarchy.State.Id = parentRegions.StateRegionID;
                regionHierarchy.State.Depth = 2;
                regionHierarchy.State.Description = parentRegions.StateDescription;
                regionHierarchy.State.Abbreviation = parentRegions.StateAbbreviation;
            }

            if (parentRegions.CityRegionID != 0 && parentRegions.CityRegionID != Constants.NULL_INT)
            {
                regionHierarchy.City = new Entities.Content.RegionData.City();
                regionHierarchy.City.Id = parentRegions.CityRegionID;
                regionHierarchy.City.Depth = 3;
                regionHierarchy.City.Description = parentRegions.CityName;
            }

            if (parentRegions.PostalCodeRegionID != 0 && parentRegions.PostalCodeRegionID != Constants.NULL_INT)
            {
                regionHierarchy.PostalCode = new Entities.Content.RegionData.Region();
                regionHierarchy.PostalCode.Id = parentRegions.PostalCodeRegionID;
                regionHierarchy.PostalCode.Depth = 4;
                regionHierarchy.PostalCode.Description = parentRegions.PostalCode;
            }

            return regionHierarchy;
        }

        /// <summary>
        /// </summary>
        /// <param name="zipCode"> </param>
        /// <returns> </returns>
        public bool IsUsorCanadianZipCode(string zipCode)
        {
            var validZipCode =
                !((!Regex.Match(zipCode, UsZipRegEx).Success) && (!Regex.Match(zipCode, CaZipRegEx).Success));
            return validZipCode;
        }
    }
}