﻿#region

using System.Collections.Concurrent;
using System.Collections.Generic;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Common.Localization;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.REST.Entities.Content.AttributeData;

#endregion

namespace Spark.REST.DataAccess.Content
{
    internal class AttributeDataAccess
    {
        public static readonly AttributeDataAccess Instance = new AttributeDataAccess();

        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(AttributeDataAccess));

        private static readonly ConcurrentDictionary<int, ConcurrentDictionary<string, List<AttributeOption>>>
            AttributeOptionList =
                new ConcurrentDictionary<int, ConcurrentDictionary<string, List<AttributeOption>>>();

        private static readonly
            ConcurrentDictionary<int, ConcurrentDictionary<string, ConcurrentDictionary<int, AttributeOption>>>
            AttributeOptionDictionary =
                new ConcurrentDictionary<int, ConcurrentDictionary<string, ConcurrentDictionary<int, AttributeOption>>>();

        private AttributeDataAccess()
        {
        }

        /// <summary>
        ///     caches list of options for a given attribute, by brand
        /// </summary>
        /// <param name="brandId"></param>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        public List<AttributeOption> GetAttributeOptions(int brandId, string attributeName)
        {
            ConcurrentDictionary<string, List<AttributeOption>> optionLookup;

            if (!AttributeOptionList.TryGetValue(brandId, out optionLookup))
            {
                optionLookup = new ConcurrentDictionary<string, List<AttributeOption>>();
                AttributeOptionList.TryAdd(brandId, new ConcurrentDictionary<string, List<AttributeOption>>());
            }

            List<AttributeOption> optionList;
            attributeName = attributeName.ToLower();
            if (optionLookup.TryGetValue(attributeName, out optionList))
            {
                return optionList;
            }
            var brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            var attributeOptions = new List<AttributeOption>();
            var attributeOptionCollection =
                AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName,
                    brand.Site.Community.CommunityID,
                    brand.Site.SiteID,
                    brand.BrandID);
            if (attributeOptionCollection == null)
            {
                //throw new Exception(String.Format("Attribute options not found for {0}. Brand ID is {1}", attributeName, brand.BrandID));
                Log.LogWarningMessage(string.Format("Attribute options not found for {0}. Brand ID is {1}", attributeName, brand.BrandID), ErrorHelper.GetCustomData());
                return null;
            }

            foreach (Matchnet.Content.ValueObjects.AttributeOption.AttributeOption mtOption in attributeOptionCollection)
            {
                var description = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo,
                    ResourceGroupEnum.Global,
                    mtOption.ResourceKey);

                attributeOptions.Add(new AttributeOption
                {
                    AttributeOptionId = mtOption.AttributeOptionID,
                    Description = description,
                    ListOrder = mtOption.ListOrder,
                    Value = mtOption.Value
                });
            }

            if (!optionLookup.ContainsKey(attributeName))
            {
                optionLookup.TryAdd(attributeName, attributeOptions);
            }
            return attributeOptions;
        }

        /// <summary>
        ///     Retrieve a collection of attribute options list given a list of attribute names.
        /// </summary>
        /// <param name="brandId"></param>
        /// <param name="attributeNames"></param>
        /// <returns></returns>
        public List<AttributeOptions> GetAttributeOptionsCollection(int brandId, List<string> attributeNames)
        {
            var attributeOptionsCollection = new List<AttributeOptions>();

            for (var i = 0; i < attributeNames.Count; i++)
            {
                if (string.IsNullOrEmpty(attributeNames[i])) continue;
                var attributeOptions = new AttributeOptions
                {
                    AttributeName = attributeNames[i],
                    AttributeOptionList = GetAttributeOptions(brandId, attributeNames[i])
                };
                attributeOptionsCollection.Add(attributeOptions);
            }

            return attributeOptionsCollection;
        }

        /// <summary>
        ///     calls GetAttributeOptions and turns the retuned list into a Dictionary
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        public ConcurrentDictionary<int, AttributeOption> GetAttributeOptionDictionary(Brand brand, string attributeName)
        {
            ConcurrentDictionary<string, ConcurrentDictionary<int, AttributeOption>> optionLookup;
            if (!AttributeOptionDictionary.TryGetValue(brand.BrandID, out optionLookup))
            {
                optionLookup = new ConcurrentDictionary<string, ConcurrentDictionary<int, AttributeOption>>();
                AttributeOptionDictionary.TryAdd(brand.BrandID, new ConcurrentDictionary<string, ConcurrentDictionary<int, AttributeOption>>());
            }

            ConcurrentDictionary<int, AttributeOption> optionDictionary;
            attributeName = attributeName.ToLower();
            if (optionLookup.TryGetValue(attributeName, out optionDictionary))
            {
                return optionDictionary;
            }

            var attributeOptionList = GetAttributeOptions(brand.BrandID, attributeName);
            optionDictionary = new ConcurrentDictionary<int, AttributeOption>();
            if (attributeOptionList != null)
            {
                foreach (var option in attributeOptionList)
                {
                    optionDictionary[option.Value] = option;
                }
            }
            optionLookup[attributeName] = optionDictionary;
            return optionDictionary;
        }

        public AttributeMetadata GetAttributeMetadata(string attributeName)
        {
            var mtAttribute = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(attributeName);

            var attributeMetadata = new AttributeMetadata
            {
                Id = mtAttribute.ID,
                DataType = mtAttribute.DataType.ToString()
            };

            return attributeMetadata;
        }
    }
}