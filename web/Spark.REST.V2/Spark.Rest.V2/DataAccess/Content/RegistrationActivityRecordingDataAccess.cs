﻿using System;
using System.Collections.Generic;
using System.Linq;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.EmailNotifier.ServiceAdapters;
using Matchnet.EmailNotifier.ValueObjects;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.REST.Models.Content.RegistrationActivityRecording;

namespace Spark.REST.DataAccess.Content
{
    public class RegistrationActivityRecordingDataAccess
    {
        public const string REGISTRATION_SCHEDULE_NAME = "registration";
        public const string FIELD_REMOVED = "REMOVED";

        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(RegistrationActivityRecordingDataAccess));
        
        public static readonly RegistrationActivityRecordingDataAccess Instance =
            new RegistrationActivityRecordingDataAccess();

        public RegistrationRecordingResult RecordRegistrationStart(RegistrationStart registrationStart)
        {
            var result = new RegistrationRecordingResult {Recorded = true};
            try
            {
                ActivityRecordingSA.Instance.RecordRegistrationStart(registrationStart.RegistrationSessionID,
                                                                 registrationStart.FormFactor,
                                                                 registrationStart.ApplicationID,
                                                                 registrationStart.Brand.Site.SiteID,
                                                                 registrationStart.ScenarioID,
                                                                 registrationStart.IPAddress,
                                                                 registrationStart.TimeStamp);
            }
            catch (Exception ex)
            {
                Log.LogError("Error recording registartion start.", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(registrationStart.Brand));
                result.Recorded = false;
            }

            return result;
        }

        public RegistrationRecordingResult RecordRegistrationComplete(RegistrationComplete registrationComplete)
        {
            return RecordRegistrationComplete(registrationComplete.RegistrationSessionID, registrationComplete.MemberID,
                                              registrationComplete.TimeStamp);
        }

        public RegistrationRecordingResult RecordRegistrationComplete(string registrationSessionID, int memberID, DateTime timestamp)
        {
            var result = new RegistrationRecordingResult { Recorded = true };
            try
            {
                ActivityRecordingSA.Instance.RecordRegistrationComplete(registrationSessionID, memberID, timestamp);
            }
            catch (Exception ex)
            {
                Log.LogError("Error recording registartion complete.", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                result.Recorded = false;
            }

            return result;
        }

        public void DeleteRegistrationRecaptureRecord(Brand brand, string emailAddress, string recaptureID)
        {

                var isRecaptureEnabled =
                    (Convert.ToBoolean(RuntimeSettings.GetSetting(SettingConstants.ENABLE_REGISTRATION_CAPTURE, brand.Site.Community.CommunityID,
                                                                  brand.Site.SiteID, brand.BrandID)));

            if (isRecaptureEnabled && !string.IsNullOrEmpty(emailAddress) && !string.IsNullOrEmpty(recaptureID))
                {
                WCFEmailNotifierSA.Instance.DeleteScheduledEvent(recaptureID);
                }
        }

        public RegistrationRecordingResult RecordRegistrationCompleteWithRecaptureInfo(RegistrationCompleteWithRecaptureInfo registrationComplete)
        {
            var result = new RegistrationRecordingResult { Recorded = true };
            try
            {
                var brand = BrandConfigSA.Instance.GetBrandByID(registrationComplete.BrandID);
                
                RecordRegistrationComplete(registrationComplete);
                DeleteRegistrationRecaptureRecord(brand, registrationComplete.EmailAddress, registrationComplete.RecaptureID);
            }
            catch (Exception ex)
            {
                Log.LogError("Error recording registartion complete with recapture info.", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                result.Recorded = false;
            }

            return result;
        }


        public RegistrationRecordingResult RecordRegistrationStep(RegistrationStep registrationStep)
        {
            var result = new RegistrationRecordingResult { Recorded = true };
            try
            {
                foreach(var key in registrationStep.StepDetails.Keys)
                {
                    if (RecaptureFieldExclusionLookup.ExcludedFields.Contains(key.ToLower(), StringComparer.OrdinalIgnoreCase))
                    {
                        registrationStep.StepDetails[key] = FIELD_REMOVED;
                    }
                }
                
                ActivityRecordingSA.Instance.RecordRegistrationStep(registrationStep.RegistrationSessionID,
                                                                    registrationStep.StepID,
                                                                    registrationStep.StepDetails,
                                                                    registrationStep.TimeStamp);
            }
            catch (Exception ex)
            {
                Log.LogError("Error recording registartion step.", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                result.Recorded = false;
            }

            return result;
        }

        public RegistrationRecordingResultWithRecaptureInfo RecordRegistrationStepWithRecaptureInfo(RegistrationStepWithRecaptureInfo registrationStep)
        {
            var result = new RegistrationRecordingResultWithRecaptureInfo { Recorded = true };
            try
            {
                RecordRegistrationStep(registrationStep);

                var brand = BrandConfigSA.Instance.GetBrandByID(registrationStep.BrandID);

                var isRecaptureEnabled =
                    (Convert.ToBoolean(RuntimeSettings.GetSetting(SettingConstants.ENABLE_REGISTRATION_CAPTURE, brand.Site.Community.CommunityID,
                                                                  brand.Site.SiteID, brand.BrandID)));

                if ( isRecaptureEnabled && !string.IsNullOrEmpty(registrationStep.EmailAddress))
                {
                    var regisgtrationEvent = RegistrationStepToScheduledEvent(registrationStep);

                    var savedEvent = WCFEmailNotifierSA.Instance.SaveScheduledEvent(regisgtrationEvent);
                    result.RecaptureID = savedEvent.ScheduleGUID;
                }
            }
            catch (Exception ex)
            {
                Log.LogError("Error recording registartion step with recapture info.", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                result.Recorded = false;
            }

            return result;
        }

        public RegistrationRecaptureInfo GetRegistrationRecaptureInfo(RegistrationRecaptureRequest recaptureRequest)
        {
            var result = new RegistrationRecaptureInfo {RegistrationFields = new Dictionary<string, string>()};
            try
            {
                ScheduledEvent ev = WCFEmailNotifierSA.Instance.GetScheduledEvent(recaptureRequest.RecaptureID);
                if (ev != null)
                {
                    result.RecaptureID = ev.ScheduleGUID;
                    foreach(var key in ev.ScheduleDetails.Details.Keys)
                    {
                        result.RegistrationFields.Add(key.ToString(), ev.ScheduleDetails.Details[key].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError("Error getting recapture info. RecaptureGUID: " + recaptureRequest.RecaptureID, ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
            }

            return result;
        }

        private ScheduledEvent RegistrationStepToScheduledEvent(RegistrationStepWithRecaptureInfo registrationStep)
        {
            var registrationEvent = new ScheduledEvent();

            if (!string.IsNullOrEmpty(registrationStep.RecaptureID))
            {
                registrationEvent.ScheduleGUID = registrationStep.RecaptureID;
            }

            registrationEvent.EmailAddress = registrationStep.EmailAddress;
            registrationEvent.ScheduleName = REGISTRATION_SCHEDULE_NAME;
            registrationEvent.GroupID = registrationStep.BrandID;
            registrationEvent.ScheduleDetails = new ScheduleDetail();

            foreach(var field in registrationStep.AllRegFields)
            {
                if (!RecaptureFieldExclusionLookup.ExcludedFields.Contains(field.Key.ToLower(), StringComparer.OrdinalIgnoreCase))
                {
                    registrationEvent.ScheduleDetails.Add(field.Key, field.Value.ToString());    
                }
            }

            return registrationEvent;
        }
    }
}