﻿using System;
using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Spark.REST.Models.Content.Pixels;
using BedrockPixels = Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Content.ServiceAdapters;

namespace Spark.Rest.DataAccess.Content
{
    public class PixelAccess
    {
        public static readonly PixelAccess Instance = new PixelAccess();

        private PixelAccess()
        {
        }

        public PixelResult RetrievePixels(PixelRequest request)
        {
            var result = new PixelResult();

            var pageId = PageNametoId(request.PageName);

            var member = MemberSA.Instance.GetMember(request.MemberID, MemberLoadFlags.None);

            if (member != null)
            {
                var regionID = member.GetAttributeInt(request.Brand, "regionID", Constants.NULL_INT);
                var genderMask = member.GetAttributeInt(request.Brand, "gendermask", Constants.NULL_INT);
                var birthdate = member.GetAttributeDate(request.Brand, "birthdate", DateTime.MinValue);

                var bedrockPixelRequest = new Matchnet.Content.ValueObjects.PagePixel.PixelRequest(pageId,
                                                                                                   request.Brand.Site.SiteID,
                                                                                                   request.MemberID,
                                                                                                   regionID,
                                                                                                   genderMask);

                bedrockPixelRequest.MemberData.Birthdate = birthdate;
                bedrockPixelRequest.LanguageID = request.LanguageID;
                bedrockPixelRequest.LuggageID = request.LuggageID;
                bedrockPixelRequest.ReferralURL = request.ReferralURL;

                if (request.PromotionID != Constants.NULL_INT)
                {
                    bedrockPixelRequest.PromotionID = request.PromotionID;
                }

                result.RenderedPixels = PagePixelSA.Instance.RetrieveFilteredPagePixels(bedrockPixelRequest);
            }
            return result;
        }

        private static int PageNametoId(string name)
        {
            var id = 0;
            if(name.ToLower() == "registrationconfirmation" || name.ToLower() == "registrationwelcome")
            {
                id = 7200;
            }

            return id;
        }
    }
}