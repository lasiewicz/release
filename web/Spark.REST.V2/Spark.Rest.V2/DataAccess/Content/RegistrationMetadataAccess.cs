﻿using System.Collections.Generic;
using System.Linq;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Registration;
using Spark.REST.Entities.Content.AttributeData;
using RegVO = Matchnet.Content.ValueObjects.Registration;
using Matchnet.Content.ValueObjects.BrandConfig;
using ControlValidation = Spark.REST.Entities.Content.Registration.ControlValidation;
using RegControl = Spark.REST.Entities.Content.Registration.RegControl;
using Scenario = Spark.REST.Entities.Content.Registration.Scenario;
using Step = Spark.REST.Entities.Content.Registration.Step;
using RegControlDeviceOverride = Spark.REST.Entities.Content.Registration.RegControlDeviceOverride;
using RegControlScenarioOverride = Spark.REST.Entities.Content.Registration.RegControlScenarioOverride;
using ControlValidationDeviceOverride = Spark.REST.Entities.Content.Registration.ControlValidationDeviceOverride;

namespace Spark.REST.DataAccess.Content
{
    public class RegistrationMetadataAccess
    {
        public static readonly RegistrationMetadataAccess Instance = new RegistrationMetadataAccess();

        private RegistrationMetadataAccess()
        {
        }

        public List<Scenario> GetRegistrationScenarios(Brand brand)
        {
            List<Scenario> registrationScenarios = null;

            
            var registrationScenariosFromBedrock = RegistrationMetadataSA.Instance.GetScenariosBySite(brand.Site.SiteID);

            foreach (RegVO.Scenario scenario in registrationScenariosFromBedrock)
            {
                if (registrationScenarios == null) registrationScenarios = new List<Scenario>();

                var steps = new List<Step>();

                foreach (RegVO.Step step in scenario.Steps)
                {
                    var controls = new List<RegControl>();

                    foreach (RegVO.RegControl control in step.Controls)
                    {
                        var isMultiValue = false;
                        var dataType = string.Empty;
                            
                        List<ControlValidation> validations = null;
                        List<AttributeOption> attributeOptions = null;
                        List<RegControlDeviceOverride> controlDeviceOverrides = null;
                        List<RegControlScenarioOverride> controlScenarioOverrides = null;

                        if (control.Validations != null && control.Validations.Count > 0)
                        {
                            validations = new List<ControlValidation>();
                            foreach (RegVO.ControlValidation validation in control.Validations)
                            {
                                List<ControlValidationDeviceOverride> validationDeviceOverrides = null;
                                if (validation.DeviceOverrides != null)
                                {
                                    validationDeviceOverrides = new List<ControlValidationDeviceOverride>();
                                    validationDeviceOverrides.AddRange(
                                        from RegVO.ControlValidationDeviceOverride vdo in validation.DeviceOverrides
                                        select new ControlValidationDeviceOverride
                                                   {
                                                       DeviceType = vdo.DeviceType,
                                                       ErrorMessage = vdo.ErrorMessage
                                                   });


                                }

                                validations.Add(new ControlValidation
                                                            {
                                                                Type = (int) validation.Type,
                                                                Value = validation.Value,
                                                        ErrorMessage = validation.ErrorMessage,
                                                        DeviceOverrides = validationDeviceOverrides
                                                            });
                        }
                        }

                        if (control.AttributeID.HasValue && control.HasOptions && control.CustomOptions == null)
                        {
                            attributeOptions = AttributeDataAccess.Instance.GetAttributeOptions(brand.BrandID,
                                                                                                control.Name);
                        }

                        if (control.AttributeID.HasValue)
                        {
                            var attributeMetadata = AttributeDataAccess.Instance.GetAttributeMetadata(control.Name);
                            dataType = attributeMetadata.DataType;
                            if (attributeMetadata.DataType.ToLower() == "mask" && control.CustomOptions == null)
                            {
                                isMultiValue = true;
                            }
                        }

                        string apiName = ProfileAccess.GetPropertyNameForAttributeName(control.Name);


                        if (control.CustomOptions != null)
                        {
                            attributeOptions = new List<AttributeOption>();
                            attributeOptions.AddRange(
                                from RegVO.ControlCustomOption coption in control.CustomOptions
                                select new AttributeOption
                                            {
                                                Value = coption.Value,
                                                ListOrder = coption.ListOrder,
                                                Description = coption.Description
                                            });
                        }

                        if (control.DeviceOverrides != null)
                        {
                            controlDeviceOverrides = new List<RegControlDeviceOverride>();
                            controlDeviceOverrides.AddRange(
                                from RegVO.RegControlDeviceOverride dor in control.DeviceOverrides
                                select new RegControlDeviceOverride
                                            {
                                                DeviceType = dor.DeviceType,
                                                AdditionalText = dor.AdditionalText,
                                                Label = dor.Label,
                                                RequiredErrorMessage = dor.RequiredErrorMessage
                                            });

                        }  

                        if (control.ScenarioOverrides != null)
                        {
                            controlScenarioOverrides = new List<RegControlScenarioOverride>();
                            controlScenarioOverrides.AddRange(
                                from RegVO.RegControlScenarioOverride s in control.ScenarioOverrides
                                select new RegControlScenarioOverride
                                {
                                    RegScenarioID = s.RegScenarioID,
                                    DisplayType = (int)s.DisplayType,
                                    Required = s.Required,
                                    EnableAutoAdvance = s.EnableAutoAdvance,
                                    AdditionalText = s.AdditionalText,
                                    Label = s.Label,
                                    RequiredErrorMessage = s.RequiredErrorMessage
                                });

                        }

                        var regControl = new RegControl
                                                    {
                                                        Name = apiName != string.Empty ? apiName : control.Name,
                                                        ControlDisplayType = (int) control.DisplayType,
                                                        EnableAutoAdvance = control.EnableAutoAdvance,
                                                        AllowZeroValue = control.AllowZeroValue,
                                                        Required = control.Required,
                                                        RequiredErrorMessage = control.RequiredErrorMessage,
                                                        Label = control.Label,
                                                        AdditionalText = control.AdditionalText,
                                                        Validations = validations,
                                                        Options = attributeOptions,
                                                        IsAttribute = control.AttributeID.HasValue,
                                                        IsMultiValue = isMultiValue,
                                                         DataType = dataType,
                                                         DeviceOverrides = controlDeviceOverrides,
                                                         DefaultValue = control.DefaultValue,
                                                        ScenarioOverrides = controlScenarioOverrides
                                                    };

                        controls.Add(regControl);
                    }



                    var regStep = new Step
                                       {
                                           Controls = controls,
                                           CSSClass = step.CSSClass,
                                           Order = step.Order,
                                              TipText = step.TipText,
                                              HeaderText =  step.HeaderText,
                                              TitleText = step.TitleText
                                       };
                    steps.Add(regStep);
                }

                registrationScenarios.Add(new Scenario
                                                {
                                                    ID = scenario.ID,
                                                    SplashTemplate = scenario.SplashTemplate.Name,
                                                    RegistrationTemplate = scenario.RegistrationTemplate.Name,
                                                    ConfirmationTemplate = scenario.ConfirmationTemplate.Name,
                                                    Name = scenario.Name,
                                                    IsOnePageReg = scenario.IsOnePageReg,
                                                    IsControl = scenario.IsControl,
                                                    DeviceType = (int) scenario.DeviceType,
                                                    ExternalSessionType = (int) scenario.ExternalSessionType,
                                                    Steps = steps
                                                });

            }

            return registrationScenarios;
        }

        public string GetRandomScenarioName(Brand brand)
        {
           RegVO.Scenario randomScenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySite(brand.Site.SiteID);
           return randomScenario.Name;
        }

        public string GetRandomScenarioName(Brand brand, DeviceType deviceType)
        {
            RegVO.Scenario randomScenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySiteAndDeviceType(brand.Site.SiteID, deviceType);
            return randomScenario.Name;
        }
    }
}

 