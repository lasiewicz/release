﻿#region

using System;

#endregion

namespace Spark.Rest.V2.DataAccess.Log.Exceptions
{
    /// <summary>
    ///     Triggered when a request for error write is made. This will help distinguish better in raygun.io than seeing a
    ///     generic System.Exception
    /// </summary>
    public class LogAccessCustomException : Exception
    {
        public LogAccessCustomException(string message)
            : base(message)
        {
        }
    }
}