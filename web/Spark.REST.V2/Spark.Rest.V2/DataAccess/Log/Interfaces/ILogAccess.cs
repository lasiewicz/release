﻿#region

using System.Collections.Generic;

#endregion

namespace Spark.Rest.V2.DataAccess.Log.Interfaces
{
    public interface ILogAccess
    {
        LogAccess.LogMessageResult LogMessage(Dictionary<string, string> customData, LogAccess.MessageType messageType,
            string message, string timestamp, int applicationId = 0,
            int memberId = 0);
    }
}