﻿#region

using System;
using System.Collections.Generic;
using Spark.Logger;
using Spark.Rest.V2.DataAccess.Log.Exceptions;
using Spark.Rest.V2.DataAccess.Log.Interfaces;

#endregion

namespace Spark.Rest.V2.DataAccess.Log
{
    public class LogAccess : AccessBase, ILogAccess

    {
        /// <summary>
        ///     Indicates the type of handled error
        /// </summary>
        public enum LogMessageErrorType
        {
            None,
            InvalidMessage,
            InvalidTimestamp
        }

        /// <summary>
        ///     Indicates the type of logging
        /// </summary>
        public enum MessageType
        {
            InfoFormat,
            ErrorFormat
        }

        /// <summary>
        ///     Using our custom logger to logging locally and optionally to raygun.io
        /// </summary>
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof (LogAccess));

        /// <summary>
        ///     Refactored into a single method to handle all log requests
        /// </summary>
        /// <param name="customData"></param>
        /// <param name="messageType"></param>
        /// <param name="memberId"></param>
        /// <param name="message"></param>
        /// <param name="timestamp"></param>
        /// <param name="applicationId"></param>
        public LogMessageResult LogMessage(Dictionary<string, string> customData, MessageType messageType,
            string message, string timestamp, int applicationId = 0, int memberId = 0)
        {
            var logMessageResult = new LogMessageResult();

            if (!IsLogExternalClients()) return logMessageResult;

            // no good without any content in the message
            if (string.IsNullOrEmpty(message))
            {
                logMessageResult.LogMessageErrorType = LogMessageErrorType.InvalidMessage;
                return logMessageResult;
            }

            // no good without a valid timestamp
            DateTime dateTime;
            if (!DateTime.TryParse(timestamp, out dateTime))
            {
                logMessageResult.LogMessageErrorType = LogMessageErrorType.InvalidTimestamp;
                return logMessageResult;
            }

            string logMessage;
            switch (messageType)
            {
                case MessageType.InfoFormat:
                    // include the message
                    logMessage = string.Format("AppId: {0}, MemberId: {1}, Timestamp: {2}, Message: {3}",
                        applicationId, memberId, timestamp, message);
                    // no need to log Info messages to raygun, by default it doesn't
                    Log.LogInfoMessage(logMessage, customData);
                    break;
                case MessageType.ErrorFormat:
                    // do not include the message and treat that as the exception message
                    logMessage = string.Format("AppId: {0}, MemberId: {1}, " + "Timestamp: {2}", applicationId,
                        memberId, timestamp);
                    // log Error messages to raygun, by default it does
                    // only pass the error message in the exception for better aggregation
                    Log.LogError(logMessage, new LogAccessCustomException(message), customData);
                    break;
                default:
                    throw new Exception(string.Format("MessageType {0} not handled.", messageType));
            }

            logMessageResult.LogMessageErrorType = LogMessageErrorType.None;

            return logMessageResult;
        }

        /// <summary>
        ///     Refactored into own method to lessen the clutter of retrieving the setting
        /// </summary>
        /// <returns></returns>
        private bool IsLogExternalClients()
        {
            return (SettingsService.SettingExistsFromSingleton("API_LOG_EXTERNAL_CLIENTS", 0, 0)) &&
                   bool.TrueString.ToLower()
                       .Equals(SettingsService.GetSettingFromSingleton("API_LOG_EXTERNAL_CLIENTS"));
        }

        public struct LogMessageResult
        {
            public LogMessageErrorType LogMessageErrorType { get; set; }
        }
    }
}