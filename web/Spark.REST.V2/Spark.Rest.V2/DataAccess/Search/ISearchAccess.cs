﻿using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.REST.Entities.Search;
using Spark.REST.Models.Search;

namespace Spark.REST.DataAccess.Search
{
	internal interface ISearchAccess
	{
        SearchResults GetSearchResults(Brand brand, int memberId, int pageSize, int pageNumber);
        SearchResultsV2 GetSearchResultsV2(Brand brand, int memberId, int pageSize, int pageNumber);
        Entities.Search.SearchPreferences GetSearchPreferences(Brand brand, int memberId);
		bool SaveSearchPreferences(SearchPreferencesRequest prefs, Brand brand);
	}
}