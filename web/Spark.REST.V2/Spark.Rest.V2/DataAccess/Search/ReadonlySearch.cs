﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.Rest.V2.DataAccess.Premium;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Helpers;
using Spark.REST.Entities.Profile;
using Spark.REST.Entities.Search;
using Spark.SAL;
using SearchType = Spark.SAL.SearchType;

namespace Spark.REST.DataAccess.Search
{
	public enum SearchResultType
	{
		MiniProfile=1,
		AttributeSet=2
	}

	internal class ReadonlySearch
	{
		public const int KEYWORD_SEARCH_MAXCHAR_LENGTH = 200; 
		private const int WillingToConvert = 16394;
		private const int NotWillingToConvert = 2048;
		private const int NotSureIfWillingToConvert = 4096;

		private const int NotJewishMask = WillingToConvert | NotWillingToConvert | NotSureIfWillingToConvert;
		private const int JewishOnlyMask = Int32.MaxValue ^ NotJewishMask; // xor to remove the bits from JewishOnlyMask

		private const int JewishAndNonJewishMask = Int32.MaxValue;
		
		private ReadonlySearch() {}
		public static readonly ReadonlySearch Instance = new ReadonlySearch();

		private static readonly ISparkLogger log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(ReadonlySearch));
		private static readonly ISparkLogger timingLog = SparkLoggerManager.Singleton.GetSparkLogger("RollingTimingLogFileAppender");


		private static SearchPreferenceCollection GetQuickSearchPreferenceCollection(SAL.MemberSearch memberSearch)
		{
			var searchPreferenceCollection = new SearchPreferenceCollection();

			foreach (SAL.Preference preference in memberSearch.SearchPreferences)
			{
				string prefName = preference.Attribute.Name;

				switch (prefName.ToLower())
				{
					case "birthdate":
						var agePref = memberSearch[Spark.SAL.Preference.GetInstance("Birthdate")] as MemberSearchPreferenceRange;
						if (agePref != null)
						{
							searchPreferenceCollection.Add("MinAge", agePref.MinValue.ToString());
							searchPreferenceCollection.Add("MaxAge", agePref.MaxValue.ToString());
						}
						else
						{
							searchPreferenceCollection.Add("MinAge", "18");
							searchPreferenceCollection.Add("MaxAge", "99");
						}
						break;
					case "gendermask":
					case "jdatereligion":
					case "distance":
					case "regionid":
					case "hasphotoflag":
					case "schoolid":
					case "searchtypeid":
					case "areacode1":
					case "areacode2":
					case "areacode3":
					case "areacode4":
					case "areacode5":
					case "areacode6":
					case "searchorderby":
						var prefInt = (MemberSearchPreferenceInt)memberSearch[preference];
						if (prefInt.Value != Constants.NULL_INT && prefInt.Value != 0)
						{
							searchPreferenceCollection.Add(prefName, prefInt.Value.ToString());
						}
						break;
				}
			}

			return searchPreferenceCollection;
		}

		public SearchResults GetSearchResults(Brand brand, int memberId, string gender, string seekingGender, int minAge, int maxAge, int maxDistance,
			bool showOnlyJewishMembers, bool? showOnlyMembersWithPhotos, int pageSize, int pageNumber, int listPosition, int minHeight, int maxHeight)
		{
			var searchResults = GetSearchResults(brand, memberId, gender, seekingGender, minAge, maxAge, Constants.NULL_INT, maxDistance,
				showOnlyJewishMembers, showOnlyMembersWithPhotos, pageSize, pageNumber, listPosition, SearchResultType.MiniProfile,
				minHeight, maxHeight, QuerySorting.JoinDate, Matchnet.Search.Interfaces.SearchType.WebSearch, SearchEntryPoint.None, null, false, false, false, false, null);
			return (SearchResults) searchResults;
		}

		public SearchResultsV2 GetSearchResultsV2(Brand brand, int memberId, string gender, string seekingGender, int minAge, int maxAge, int regionId,
			int maxDistance, bool showOnlyJewishMembers, bool? showOnlyMembersWithPhotos, int pageSize, int pageNumber, int listPosition, int minHeight, int maxHeight,
			QuerySorting sortType, Matchnet.Search.Interfaces.SearchType searchType, SearchEntryPoint entryPoint, string keywords, bool includeSpotlight, bool useDefaultPreferences, bool? showOnlyRamah,
            bool includeBigData, Dictionary<string, List<int>> advancedSearchPreferences = null)
		{
			var searchResults = GetSearchResults(brand, memberId, gender, seekingGender, minAge, maxAge, regionId,
				maxDistance, showOnlyJewishMembers, showOnlyMembersWithPhotos, pageSize, pageNumber, listPosition, SearchResultType.AttributeSet,
				minHeight, maxHeight, sortType, searchType, entryPoint, keywords, includeSpotlight, useDefaultPreferences, showOnlyRamah, includeBigData, advancedSearchPreferences);
			return (SearchResultsV2) searchResults;
		}

		/// <summary>
		/// Gets the search results.
		/// </summary>
		/// <param name="brand">The brand.</param>
		/// <param name="memberId">The member identifier.</param>
		/// <param name="gender">The gender.</param>
		/// <param name="seekingGender">The seeking gender.</param>
		/// <param name="minAge">The minimum age.</param>
		/// <param name="maxAge">The maximum age.</param>
		/// <param name="regionId">The region identifier.</param>
		/// <param name="maxDistance">The maximum distance.</param>
		/// <param name="showOnlyJewishMembers">if set to <c>true</c> [show only jewish members].</param>
		/// <param name="showOnlyMembersWithPhotos">if set to <c>true</c> [show only members with photos].</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <param name="pageNumber">The page number.</param>
		/// <param name="searchResultType">Type of the search result.</param>
		/// <param name="minHeight">The minimum height.</param>
		/// <param name="maxHeight">The maximum height.</param>
		/// <param name="sortType">Type of the sort.</param>
		/// <param name="searchType">Type of the search.</param>
		/// <param name="entryPoint">The entry point.</param>
		/// <param name="keywords">The keywords.</param>
		/// <param name="includeSpotlight">if set to <c>true</c> [include spotlight].</param>
		/// <param name="advancedSearchPreferences">accepts any masked type attributes, continue using other arguments</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Page number is 1-indexed, must be at least 1.</exception>
		public object GetSearchResults(Brand brand, int memberId, string gender, string seekingGender, int minAge,
			int maxAge, int regionId, int maxDistance, bool showOnlyJewishMembers, bool? showOnlyMembersWithPhotos,
			int pageSize, int pageNumber, int listPosition, SearchResultType searchResultType, int minHeight, int maxHeight,
			QuerySorting sortType, Matchnet.Search.Interfaces.SearchType searchType, SearchEntryPoint entryPoint,
			string keywords, bool includeSpotlight, bool useDefaultPreferences, bool? showOnlyRamah, bool includeBigData, Dictionary<string, List<int>> advancedSearchPreferences = null)
		{
			Dictionary<string, string> customData = ErrorHelper.GetCustomData();
			string searchResultsVersion = (searchResultType == SearchResultType.AttributeSet) ? "GetSearchResultsV2" : "GetSearchResults";
			int brandId = brand.BrandID;
			int communityId = brand.Site.Community.CommunityID;
			int siteId = brand.Site.SiteID;

			Stopwatch stopwatch = new Stopwatch();
			timingLog.LogInfoMessage(string.Format("START: Method:{0}, Section:SAL.MemberSearchCollection.Load(memberId:{1}, brandId:{2})", searchResultsVersion, memberId, brandId), customData);
			stopwatch.Start();
			var search = SAL.MemberSearchCollection.Load(memberId, brand).PrimarySearch;
			stopwatch.Stop();
			long ellapsedTimeInMillis1 = stopwatch.ElapsedMilliseconds;
			timingLog.LogInfoMessage(string.Format("END: Method:{0}, Section:SAL.MemberSearchCollection.Load(memberId:{1}, brandId:{2}), ElapsedTime:{3}ms", searchResultsVersion, memberId, brandId, ellapsedTimeInMillis1), customData);

			if (search == null) return null;

            // Daily Matches uses its own business rull to dictate the size, setting the values to something to get past validation
            if (searchType == Matchnet.Search.Interfaces.SearchType.DailyMatches)
            {
                pageNumber = 1;
                pageSize = 1000; //Setting pageSize high so DailyMatches have enough profiles to filter 
            }

			//validate
			if (pageNumber < 1 && listPosition < 1)
			{
				throw new SparkAPIReportableArgumentException("Page number or list position is 1-indexed, must be at least 1.");
			}

			if (pageSize <= 0)
			{
				throw new SparkAPIReportableArgumentException("Page size must be at least 1");
			}

			//set preferences
			if (!string.IsNullOrEmpty(gender) && !string.IsNullOrEmpty(seekingGender))
			{
				try
				{
					search.Gender = (Gender)Enum.Parse(typeof(Gender), gender, true); // no reverse
					search.SeekingGender = (Gender)Enum.Parse(typeof(Gender), seekingGender, true);
				}
				catch (ArgumentException ex)
				{
					log.LogError("failed to parse gender", ex, customData);
					return null;
				}
			}

			if (minAge > 0)
			{
				search.Age.MinValue = minAge;
			}

			if (maxAge > 0)
			{
				search.Age.MaxValue = maxAge;
			}

			if (maxDistance > 0)
			{
				search.Distance = maxDistance;
			}

			if (showOnlyMembersWithPhotos.HasValue)
			{
				search.HasPhoto = showOnlyMembersWithPhotos.Value;
			}
			else if (!useDefaultPreferences)
			{
				//default to true if it's not a matches search
				search.HasPhoto = true;
			}

			//enable ability to use region id from client
			if (regionId > 0)
			{
				search.RegionID = regionId;
			}

			string searchArgs = search.ToString();
			timingLog.LogInfoMessage(string.Format("START: Method:{0}, Section:GetQuickSearchPreferenceCollection(search:{1})", searchResultsVersion, searchArgs), customData);
			stopwatch.Restart();
            var searchPrefsCollection =
                // use default search preferences for Daily Matches, ignore any values
                useDefaultPreferences || searchType == Matchnet.Search.Interfaces.SearchType.DailyMatches
                ? SAL.MemberSearch.getSearchPreferenceCollection(search)
                : GetQuickSearchPreferenceCollection(search);
			stopwatch.Stop();
			long ellapsedTimeInMillis2 = stopwatch.ElapsedMilliseconds;
			timingLog.LogInfoMessage(string.Format("END: Method:{0}, Section:GetQuickSearchPreferenceCollection(search:{1}), ElapsedTime:{2}ms", searchResultsVersion, searchArgs, ellapsedTimeInMillis2), customData);

			if (sortType != QuerySorting.None)
			{
				searchPrefsCollection["SearchOrderBy"] = ((int)sortType).ToString();
			}
			else if (searchType != Matchnet.Search.Interfaces.SearchType.YourMatchesWebSearch || !searchPrefsCollection.ContainsKey("SearchOrderBy"))
			{
				searchPrefsCollection["SearchOrderBy"] = ((int)QuerySorting.JoinDate).ToString();
			}

			//only add the param if boolean is true
			if (showOnlyJewishMembers)
			{
				searchPrefsCollection["JDateReligion"] = JewishOnlyMask.ToString(CultureInfo.InvariantCulture);
			}

			if (searchResultType == SearchResultType.AttributeSet)
			{
				searchPrefsCollection["SearchRedesign30"] = "1";
			}

			// in cm, process if either one has a value
			if (((minHeight != Constants.NULL_INT) || (minHeight > 0)) ||
				((maxHeight != Constants.NULL_INT) || (maxHeight > 0)))
			{
				if (minHeight != Constants.NULL_INT)
					searchPrefsCollection["MinHeight"] = minHeight.ToString(CultureInfo.InvariantCulture);
				if (maxHeight != Constants.NULL_INT)
					searchPrefsCollection["MaxHeight"] = maxHeight.ToString(CultureInfo.InvariantCulture);
			}

			//add keywords if present
			if (!string.IsNullOrEmpty(keywords))
			{
				if (keywords.Trim().Length > KEYWORD_SEARCH_MAXCHAR_LENGTH)
				{
					keywords = keywords.Trim().Substring(0, KEYWORD_SEARCH_MAXCHAR_LENGTH);
				}
				searchPrefsCollection["keywordsearch"] = keywords;
			}

			//add ramah
			if (showOnlyRamah.HasValue)
			{
				searchPrefsCollection["ramahAlum"] = showOnlyRamah.Value ? "1" : "0";
			}

			if (advancedSearchPreferences != null)
			{
				string advancedSearchPrefsString = advancedSearchPreferences.ToString();
				timingLog.LogInfoMessage(string.Format("START: Method:{0}, Section:advancedSearchPreferences:{1})", searchResultsVersion, advancedSearchPrefsString), customData);
				stopwatch.Restart();
				foreach (var pref in advancedSearchPreferences.Where(pref => pref.Value != null && pref.Value.Count > 0))
				{
					// let's calculate the mask value using bitwise OR
					var maskVal = 0;

                    if (pref.Key.ToLower() == "childrencount")
                    {
                        maskVal = pref.Value.Aggregate(maskVal, (current, value) => current | Convert.ToInt32(Math.Pow(2, value)));
                    }
                    else
                    {
                        maskVal = pref.Value.Aggregate(maskVal, (current, value) => current | value);
                    }
					// this Add method internally checks to see the key already exists. 
					// if so, it just overwrites the existing value.
					searchPrefsCollection.Add(pref.Key, maskVal.ToString(CultureInfo.InvariantCulture));
				}
				stopwatch.Stop();
				long ellapsedTimeInMillis3 = stopwatch.ElapsedMilliseconds;
				timingLog.LogInfoMessage(string.Format("END: Method:{0}, Section:advancedSearchPreferences:{1}), ElapsedTime:{2}ms", searchResultsVersion, advancedSearchPrefsString, ellapsedTimeInMillis3), customData);
			}

			//fixed error where start row was never 0. 0 is a valid start row value.
			var startRow = pageNumber > 0 ? pageSize*(pageNumber - 1) : listPosition - 1;

			string searchPrefCollectionString = searchPrefsCollection.ToString();
			timingLog.LogInfoMessage(string.Format("START: Method:{0}, Section:searchPrefsCollection:{1})", searchResultsVersion, searchPrefCollectionString), customData);
			stopwatch.Restart();     
			var validationResult = searchPrefsCollection.Validate();
			stopwatch.Stop();
			long ellapsedTimeInMillis4 = stopwatch.ElapsedMilliseconds;
			timingLog.LogInfoMessage(string.Format("END: Method:{0}, Section:searchPrefsCollection:{1}), ElapsedTime:{2}ms", searchResultsVersion, searchPrefCollectionString, ellapsedTimeInMillis4), customData);

			if (validationResult.Status == PreferencesValidationStatus.Failed)
			{
				log.LogWarningMessage(string.Format("Invalid search preferences used. Validation failed. memberId:{0} error:{1} ", memberId,
					validationResult.ValidationError),customData);
				// todo: better handling of this error should be implemented. return the message to the caller.
				return null;
			}

			timingLog.LogInfoMessage(string.Format("START: Method:{0}, Section:MemberSearchSA.Instance.Search(searchPrefsCollection:{1}, communityID:{2}, siteID:{3}, startRow:{4}, pageSize:{5}, memberId:{6}, SearchEngineType.FAST, searchType:{7}, entryPoint:{8}, false, false, false))", searchResultsVersion, searchPrefCollectionString, communityId, siteId, startRow, pageSize, memberId, searchType, entryPoint), customData);
			stopwatch.Restart();
			var searchSAresults = MemberSearchSA.Instance.Search(searchPrefsCollection, communityId,
				siteId, startRow, pageSize, memberId,
				SearchEngineType.FAST,
				searchType,
				entryPoint, false, false, false, timingLog);
			stopwatch.Stop();
			long ellapsedTimeInMillis5 = stopwatch.ElapsedMilliseconds;
			timingLog.LogInfoMessage(string.Format("END: Method:{0}, Section:MemberSearchSA.Instance.Search(searchPrefsCollection:{1}, communityID:{2}, siteID:{3}, startRow:{4}, pageSize:{5}, memberId:{6}, SearchEngineType.FAST, searchType:{7}, entryPoint:{8}, false, false, false)), ElapsedTime:{9}ms", searchResultsVersion, searchPrefCollectionString, communityId, siteId, startRow, pageSize, memberId, searchType, entryPoint, ellapsedTimeInMillis5), customData);

			timingLog.LogInfoMessage(string.Format("START: Method:{0}, Section:searchSAresults.ToArrayListOfMemberIDMatchscore()", searchResultsVersion), customData);
			stopwatch.Restart();
			var matchMemberIds = searchSAresults.ToArrayListOfMemberIDMatchscore();
			stopwatch.Stop();
			long ellapsedTimeInMillis6 = stopwatch.ElapsedMilliseconds;
			timingLog.LogInfoMessage(string.Format("END: Method:{0}, Section:searchSAresults.ToArrayListOfMemberIDMatchscore(), ElapsedTime:{1}ms", searchResultsVersion, ellapsedTimeInMillis6), customData);

            var attributeSetName = includeBigData ? "resultsetprofilebigV2" : "resultsetprofile";
			Dictionary<string, object> spotlight = null;
			if (includeSpotlight)
			{
				timingLog.LogInfoMessage(string.Format("START: Method:{0}, Section:PremiumAccess.Instance.GetNextSpotlight(brandId:{1}, memberId:{2}, searchSAresults.ToArrayList().Cast<int>().ToList(), 0)", searchResultsVersion, brandId, memberId), customData);
				stopwatch.Restart();
				List<int> searchIDs = searchSAresults.ToArrayList().Cast<int>().ToList();
				spotlight = PremiumAccess.Instance.GetNextSpotlight(brand, memberId, searchIDs, 0, attributeSetName);
				stopwatch.Stop();
				long ellapsedTimeInMillis7 = stopwatch.ElapsedMilliseconds;
				timingLog.LogInfoMessage(string.Format("END: Method:{0}, Section:PremiumAccess.Instance.GetNextSpotlight(brandId:{1}, memberId:{2}, searchSAresults.ToArrayList().Cast<int>().ToList():{3}, 0), ElapsedTime:{4}ms", searchResultsVersion, brandId, memberId, searchIDs, ellapsedTimeInMillis7), customData);
			}

			object searchResults;
			if (searchResultType == SearchResultType.AttributeSet)
			{
				searchResults = new SearchResultsV2()
				{
					Members = new List<Dictionary<string, object>>(),
					MatchesFound = searchSAresults.MatchesFound,
					Spotlight = spotlight
				};

				var resultsCount = searchSAresults.Items.Count;
				timingLog.LogInfoMessage(string.Format("START: Method:{0}, Section:GetAttributeSetWithPhotoAttributes() brandId:{1} numOfMembers:{2})", searchResultsVersion, brandId, resultsCount), customData);
				stopwatch.Restart();     
				if (null != matchMemberIds && matchMemberIds.Count > 0)
				{
					//using a fixed array to preserve sort order during parallel processing
					//http://stackoverflow.com/questions/11273377/parallel-for-maintain-input-list-order-on-output-list
					var attrSets = new Dictionary<string, object>[matchMemberIds.Count];
                    var ctx = System.Web.HttpContext.Current;
                    Parallel.For(0, matchMemberIds.Count, ParallelTasksHelper.GetMaxDegreeOfParallelism(), i =>
					{
                        //set HttpContext for thread
                        System.Web.HttpContext.Current = ctx;

					    try
					    {
					        var matchMemberId = (int[])matchMemberIds[i];
					        var attributeSetWithPhotoAttributes =
					            ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, matchMemberId[0], memberId, attributeSetName, false, null, matchMemberId[1]).AttributeSet;
					        if (null != attributeSetWithPhotoAttributes && attributeSetWithPhotoAttributes.Count > 0)
					        {
					            attrSets[i]=attributeSetWithPhotoAttributes;
					        }
					    }
					    finally
					    {
                            //unset HttpContext for thread (precaution for GC)
                            System.Web.HttpContext.Current = null;					     
					    }
					});
					((SearchResultsV2) searchResults).Members = new List<Dictionary<string, object>>(attrSets);
				}
				stopwatch.Stop();
				var ellapsedTimeInMillis8 = stopwatch.ElapsedMilliseconds;
				timingLog.LogInfoMessage(string.Format("END: Method:{0}, Section:GetAttributeSetWithPhotoAttributes() brandId:{1} numOfMembers:{2}), ElapsedTime:{3}ms", searchResultsVersion, brandId, resultsCount, ellapsedTimeInMillis8), customData);
			}
			else
			{
				searchResults = new SearchResults
				{
					Members = new List<MiniProfile>(),
					MatchesFound = searchSAresults.MatchesFound,
					Spotlight = spotlight
				};

				int resultsCount = searchSAresults.Items.Count;
				timingLog.LogInfoMessage(string.Format("START: Method:{0}, Section:GetMiniProfile() brandId:{1} numOfMembers:{2})", searchResultsVersion, brandId, resultsCount), customData);
				stopwatch.Restart();     
				foreach (var miniProfile in
					matchMemberIds.Cast<int[]>()
						.Select(matchMemberId => ProfileAccess.GetMiniProfile(brand, matchMemberId[0], memberId))
						.Where(miniProfile => miniProfile != null))
				{
					((SearchResults) searchResults).Members.Add(miniProfile);
				}
				stopwatch.Stop();
				long ellapsedTimeInMillis9 = stopwatch.ElapsedMilliseconds;
				timingLog.LogInfoMessage(string.Format("END: Method:{0}, Section:GetMiniProfile() brandId:{1} numOfMembers:{2}), ElapsedTime:{3}ms", searchResultsVersion, brandId, resultsCount, ellapsedTimeInMillis9), customData);
			}

			return searchResults;
		}

		public SearchResultsV2 GetSecretAdmirerSearchResults(Brand brand, int memberId, int minAge, int maxAge, int pageSize, int pageNumber, int listPosition, string attributeSetType)
		{
			var strictPreferences = SAL.MemberSearchCollection.Load(memberId, brand).PrimarySearch;

			if (strictPreferences == null) return null;

			SearchPreferenceCollection laxPrefs = new SearchPreferenceCollection();
			laxPrefs.Add("GenderMask", (strictPreferences[Spark.SAL.Preference.GetInstance("GenderMask")] as MemberSearchPreferenceMask).Value.ToString("d"));
			laxPrefs.Add("RegionID", strictPreferences.RegionID.ToString("d"));
			laxPrefs.Add("DomainID", brand.Site.Community.CommunityID.ToString("d"));
			laxPrefs.Add("SearchTypeID", SearchTypeID.Region.ToString("d")); //Region
			laxPrefs.Add("SearchOrderBy", QuerySorting.Proximity.ToString("d"));
			laxPrefs.Add("Radius", "160");
			laxPrefs.Add("HasPhotoFlag", "1");
			laxPrefs.Add("MinAge", minAge.ToString("d"));
			laxPrefs.Add("MaxAge", maxAge.ToString("d"));
			laxPrefs.Add("SearchType",((int)Matchnet.Search.Interfaces.SearchType.SecretAdmirerWebSearch).ToString("d"));
			laxPrefs.Add("MemberId", memberId.ToString("d"));
			//laxPrefs.Add("SearchRedesign30","1");

			if (pageNumber < 1 && listPosition < 1)
			{
				throw new SparkAPIReportableArgumentException("Page number or list position is 1-indexed, must be at least 1.");
			}

			if (pageSize <= 0)
			{
				throw new SparkAPIReportableArgumentException("Page size must be at least 1");
			}

			//fixed error where start row was never 0. 0 is a valid start row value.
			var startRow = pageNumber > 0 ? pageSize * (pageNumber - 1) : listPosition - 1;

			var searchSAresults = MemberSearchSA.Instance.Search(laxPrefs, brand.Site.Community.CommunityID, brand.Site.SiteID, startRow, pageSize);
			var matchMemberIds = searchSAresults.ToArrayList();
			var searchResults = new SearchResultsV2() { Members = new List<Dictionary<string, object>>() };
			foreach (var attributeSetResult in
				matchMemberIds.Cast<int>().Select(matchMemberId => ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, matchMemberId, matchMemberId, attributeSetType))
				.Where(attributeSetResult => attributeSetResult != null && attributeSetResult.AttributeSet.Count > 0))
			{
				searchResults.Members.Add(attributeSetResult.AttributeSet);
			}
			searchResults.MatchesFound = searchSAresults.MatchesFound;
			return searchResults;
		}

		public SearchResultsV2 GetVisitorSearchResults(Brand brand, int genderMask, int minAge, int maxAge, int regionID, int maxDistance,
				bool showOnlyJewishMembers, bool showOnlyMembersWithPhotos, int pageSize, int pageNumber, int listPosition)
		{

			var search = new SAL.MemberSearch(SearchType.Region, genderMask, minAge, maxAge, regionID, maxDistance)
			{
				HasPhoto = showOnlyMembersWithPhotos
			};

			var searchPrefsCollection = GetQuickSearchPreferenceCollection(search);
			searchPrefsCollection["SearchOrderBy"] = ((int) QuerySorting.JoinDate).ToString();
			searchPrefsCollection["SearchRedesign30"] = "1";

			//only add the param if boolean is true
			if (showOnlyJewishMembers)
			{
				searchPrefsCollection["JDateReligion"] = JewishOnlyMask.ToString(CultureInfo.InvariantCulture);
			}

			if (pageNumber < 1 && listPosition < 1)
			{
				throw new SparkAPIReportableArgumentException("Page number or list position is 1-indexed, must be at least 1.");
			}

			if (pageSize <= 0)
			{
				throw new SparkAPIReportableArgumentException("Page size must be at least 1");
			}

			//fixed error where start row was never 0. 0 is a valid start row value.
			var startRow = pageNumber > 0 ? pageSize * (pageNumber - 1) : listPosition - 1;

			var searchSAresults = MemberSearchSA.Instance.Search(searchPrefsCollection, brand.Site.Community.CommunityID,
																 brand.Site.SiteID, startRow, pageSize);
			var matchMemberIds = searchSAresults.ToArrayList();
			var searchResults = new SearchResultsV2() {Members = new List<Dictionary<string, object>>()};
			foreach (var miniProfileAttributeSet in matchMemberIds.Cast<int>().Select(
				matchMemberId =>
					ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, matchMemberId, Constants.NULL_INT,
						"resultsetprofile")).Where(miniProfileAttributeSet =>
							miniProfileAttributeSet != null && null !=miniProfileAttributeSet.AttributeSet && miniProfileAttributeSet.AttributeSet.Count > 0)
				)
			{
				searchResults.Members.Add(miniProfileAttributeSet.AttributeSet);
			}
			searchResults.MatchesFound = searchSAresults.MatchesFound;
			return searchResults;
		}

	}
}
