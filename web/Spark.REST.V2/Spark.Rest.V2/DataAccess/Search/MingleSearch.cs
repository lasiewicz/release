﻿using System.Collections;
using System.Collections.Generic;
using Spark.MingleSearchEngine.ServiceAdapters;
using Spark.MingleSearchEngine.ServiceAdaptersNRT;
using Spark.MingleSearchEngine.ValueObjects;
using Spark.REST.Entities.Search;

namespace Spark.Rest.V2.DataAccess.Search
{
    public class MingleSearch
    {
        public static readonly MingleSearch Instance = new MingleSearch();

        private MingleSearch(){}

        public SearchParameterCollection BuildSearchParameterCollection(int communityId, int seekingGender, int minage, int maxage, int minheight, int maxheight,
           double radiansLatitude, double radiansLongitude, int radiusInMiles, int ethnicity, int bodyType, int education, int religion, int smoke, int drink,
            int maritalStatus, bool hasPhoto, SearchSorting sortType, SearchResultType resultType, int maxResults, int hasEthnicity, int hasSmoke, int hasDrink,
            int hasReligion, int hasMaritalStatus, int hasBodyType, int hasChurchActivity, int hasEducation, int wantsEthnicity, int wantsSmoke, int wantsDrink, 
            int wantsReligion, int wantsMaritalStatus, int wantsBodyType, int wantsChurchActivity, int wantsEducation, int importanceEthnicity, int importanceSmoke, 
            int importanceDrink, int importanceReligion, int importanceMaritalStatus, int importanceBodyType, int importanceChurchActivity, int importanceEducation)
        {
            SearchParameterCollection searchParameterCollection = new SearchParameterCollection();
            searchParameterCollection.Add(new SearchParameter("communityid", communityId, SearchParameterType.Int));
            searchParameterCollection.Add(new SearchParameter("photo", (hasPhoto ? 1 : 0) + "|"+int.MaxValue, SearchParameterType.IntRangeFilter));
            searchParameterCollection.Add(new SearchParameter("location", radiansLatitude + "|" + radiansLongitude + "|" + radiusInMiles, SearchParameterType.LocationLatLng));

            if(maxResults>0)
            {
                searchParameterCollection.Add(new SearchParameter("maxResults", maxResults,SearchParameterType.MaxResults));
            }
            if (minheight > 0 || maxheight > 0)
            {
                searchParameterCollection.Add(new SearchParameter("height", minheight + "|" + maxheight, SearchParameterType.IntRangeFilter));
            }
            if (minage > 0 || maxage > 0)
            {
                searchParameterCollection.Add(new SearchParameter("birthdate", minage + "|" + maxage, SearchParameterType.AgeRangeFilter));
            }
            if (seekingGender > -1)
            {
                searchParameterCollection.Add(new SearchParameter("gender", seekingGender, SearchParameterType.Int));
            }
            if (ethnicity > -1)
            {
                searchParameterCollection.Add(new SearchParameter("ethnicity", ethnicity, SearchParameterType.Mask));
            }
            if (bodyType > -1)
            {
                searchParameterCollection.Add(new SearchParameter("bodytype", bodyType, SearchParameterType.Mask));
            }
            if (education > -1)
            {
                searchParameterCollection.Add(new SearchParameter("education", education, SearchParameterType.Mask));
            }
            if (maritalStatus > -1)
            {
                searchParameterCollection.Add(new SearchParameter("maritalstatus", maritalStatus, SearchParameterType.Mask));
            }
            if (religion > -1)
            {
                searchParameterCollection.Add(new SearchParameter("religion", religion, SearchParameterType.Mask));
            }
            if (smoke > -1)
            {
                searchParameterCollection.Add(new SearchParameter("smoke", smoke, SearchParameterType.Mask));
            }
            if (drink > -1)
            {
                searchParameterCollection.Add(new SearchParameter("drink", drink, SearchParameterType.Mask));
            }
            if (hasBodyType > -1)
            {
                searchParameterCollection.Add(new SearchParameter("bodytype", hasBodyType, SearchParameterType.Has));
            }
            if (hasChurchActivity > -1)
            {
                searchParameterCollection.Add(new SearchParameter("churchactivity", hasChurchActivity, SearchParameterType.Has));
            }
            if (hasDrink > -1)
            {
                searchParameterCollection.Add(new SearchParameter("drink", hasDrink, SearchParameterType.Has));
            }
            if (hasEducation > -1)
            {
                searchParameterCollection.Add(new SearchParameter("education", hasEducation, SearchParameterType.Has));
            }
            if (hasEthnicity > -1)
            {
                searchParameterCollection.Add(new SearchParameter("ethnicity", hasEthnicity, SearchParameterType.Has));
            }
            if (hasMaritalStatus > -1)
            {
                searchParameterCollection.Add(new SearchParameter("maritalstatus", hasMaritalStatus, SearchParameterType.Has));
            }
            if (hasReligion > -1)
            {
                searchParameterCollection.Add(new SearchParameter("religion", hasReligion, SearchParameterType.Has));
            }
            if (hasSmoke > -1)
            {
                searchParameterCollection.Add(new SearchParameter("smoke", hasSmoke, SearchParameterType.Has));
            }            
            if (wantsBodyType > -1)
            {
                searchParameterCollection.Add(new SearchParameter("bodytype", wantsBodyType, SearchParameterType.Wants));
            }
            if (wantsChurchActivity > -1)
            {
                searchParameterCollection.Add(new SearchParameter("churchactivity", wantsChurchActivity, SearchParameterType.Wants));                
            }
            if (wantsDrink > -1)
            {
                searchParameterCollection.Add(new SearchParameter("drink", wantsDrink, SearchParameterType.Wants));
            }
            if (wantsEducation > -1)
            {
                searchParameterCollection.Add(new SearchParameter("education", wantsEducation, SearchParameterType.Wants));                                
            }
            if (wantsEthnicity > -1)
            {
                searchParameterCollection.Add(new SearchParameter("ethnicity", wantsEthnicity, SearchParameterType.Wants));                                
            }
            if (wantsMaritalStatus > -1)
            {
                searchParameterCollection.Add(new SearchParameter("maritalstatus", wantsMaritalStatus, SearchParameterType.Wants));                                
            }
            if (wantsReligion > -1)
            {
                searchParameterCollection.Add(new SearchParameter("religion", wantsReligion, SearchParameterType.Wants));                                
            }
            if (wantsSmoke > -1)
            {
                searchParameterCollection.Add(new SearchParameter("smoke", wantsSmoke, SearchParameterType.Wants));                                
            }
            if (importanceBodyType > -1)
            {
                searchParameterCollection.Add(new SearchParameter("bodytype", importanceBodyType, SearchParameterType.Importance));
            }
            if (importanceChurchActivity > -1)
            {
                searchParameterCollection.Add(new SearchParameter("churchactivity", importanceChurchActivity, SearchParameterType.Importance));
            }
            if (importanceDrink > -1)
            {
                searchParameterCollection.Add(new SearchParameter("drink", importanceDrink, SearchParameterType.Importance));
            }
            if (importanceEducation > -1)
            {
                searchParameterCollection.Add(new SearchParameter("education", importanceEducation, SearchParameterType.Importance));
            }
            if (importanceEthnicity > -1)
            {
                searchParameterCollection.Add(new SearchParameter("ethnicity", importanceEthnicity, SearchParameterType.Importance));
            }
            if (importanceMaritalStatus > -1)
            {
                searchParameterCollection.Add(new SearchParameter("maritalstatus", importanceMaritalStatus, SearchParameterType.Importance));
            }
            if (importanceReligion > -1)
            {
                searchParameterCollection.Add(new SearchParameter("religion", importanceReligion, SearchParameterType.Importance));
            }
            if (importanceSmoke > -1)
            {
                searchParameterCollection.Add(new SearchParameter("smoke", importanceSmoke, SearchParameterType.Importance));
            }

            searchParameterCollection.Sort = sortType;
            searchParameterCollection.ResultType = resultType;
            return searchParameterCollection;
        }

        public SearchResultsV2 Search(SearchParameterCollection searchParameterCollection, int communityId)
        {
            SearchResultsV2 searchResults = null;
            ArrayList results = SearcherSA.Instance.RunQuery(searchParameterCollection, communityId);
            if (null != results)
            {
                searchResults=new SearchResultsV2() { Members = new List<Dictionary<string, object>>() }; 
                foreach (object o in results)
                {
                    if (o is int)
                    {
                        searchResults.Members.Add(new Dictionary<string, object>(){{o.ToString(),o}});
                    }
                    else if (o is Hashtable)
                    {
                        Hashtable h = (Hashtable) o;
                        searchResults.Members.Add(new Dictionary<string, object>() { { h["userid"].ToString(), h } });
                    }
                }
            }
            return searchResults;
        }

        public void UpdateSearch(MingleSearchMemberUpdate mingleSearchMemberUpdate, MingleSearchMember mingleSearchMember)
        {
            if (mingleSearchMemberUpdate.UpdateMode == UpdateModeEnum.remove)
            {
                SearcherNRTSA.Instance.NRTRemoveMember(mingleSearchMemberUpdate);                
            }
            else
            {
                SearcherNRTSA.Instance.NRTUpdateMember(mingleSearchMemberUpdate, mingleSearchMember);                
            }
        }


    }
}