﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ServiceAdapters;
using Spark.REST.Entities.Profile;
using Spark.REST.Entities.Search;
using Spark.REST.Helpers;
using Spark.REST.Models.Search;
using Spark.SAL;

namespace Spark.REST.DataAccess.Search
{
	internal class SearchAccessPersistCookie : ISearchAccess
	{
		private SearchAccessPersistCookie() {}
		public static readonly ISearchAccess Instance = new SearchAccessPersistCookie();

		private static readonly ILog log = LogManager.GetLogger(typeof(SearchAccessPersistCookie));

		private static int ParseDefault(string stringVal, int defaultVal) {
			int temp;
			return Int32.TryParse(stringVal, out temp) ? temp : defaultVal;
		}

		public SearchResults GetSearchResults(Brand brand, int memberId, int pageSize, int pageNumber)
		{
			SearchResults searchResults = null;
			var preferences = SearchPreferencesSA.Instance.GetSearchPreferences(memberId, brand.Site.Community.CommunityID);
			if (pageNumber < 1)
			{
				throw new Exception("Page number is 1-indexed, must be at least 1.");
			}
			var startRow = pageSize*(pageNumber - 1) + 1;
			var matchnetQueryResults = MemberSearchSA.Instance.Search(preferences, brand.Site.Community.CommunityID, brand.Site.SiteID, startRow, pageSize);
			if (matchnetQueryResults != null)
			{
				var memberIds = matchnetQueryResults.ToArrayList().Cast<int>().ToList();
				searchResults = new SearchResults { Members = new List<MiniProfile>() };
				foreach (var miniProfile in
					memberIds.Select(id => MemberAccess.GetMiniProfile(brand, id)).Where(miniProfile => miniProfile != null))
				{
					searchResults.Members.Add(miniProfile);
				}
			}
			return searchResults;
		}


		internal SearchResults GetSearchResultsOrg(Brand brand, int memberId, int pageSize, int pageNumber)
		{

			var searches = MemberSearchCollection.Load(memberId, brand);
			var search = searches.PrimarySearch;
			int totalResults;
			if (pageNumber < 1)
			{
				throw new Exception("Page number is 1-indexed, must be at least 1.");
			}
			var startRow = pageSize * (pageNumber - 1) + 1;

			var memberIds = search.GetSearchResults(startRow, pageSize, out totalResults);
			var searchResults = new SearchResults { Members = new List<MiniProfile>() };
			foreach (Member member in memberIds)
			{
				var miniProfile = MemberAccess.GetMiniProfile(brand, member.MemberID);
				searchResults.Members.Add(miniProfile);
			}
			return searchResults;
		}



		/// <summary>
		/// using the SAL version of this now
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="memberId"></param>
		/// <returns></returns>
		[Obsolete]
		public SearchPreferences GetSearchPreferences(Brand brand, int memberId)
		{
			var preferences = SearchPreferencesSA.Instance.GetSearchPreferences(memberId, brand.Site.Community.CommunityID);
			if (preferences != null)
			{
				var searchPreferences = new SearchPreferences
				                        	{
				                        		MemberId = memberId,
				                        		MinAge = ParseDefault(preferences.Value("minage"), 0),
				                        		MaxAge = ParseDefault(preferences.Value("maxage"), 99),
				                        		MaxDistance = ParseDefault(preferences.Value("distance"), 20)
				                        	};
				int genderMask;
				if (Int32.TryParse(preferences.Value("gendermask"), out genderMask))
				{
					// NOTE: gender and seeking gender are REVERSED when they are stored/retrieved.  They are stored from the perspective of the user being searched for
					searchPreferences.SeekingGender = AttributeHelper.GetGenderDescription(genderMask, false);
					searchPreferences.Gender = AttributeHelper.GetGenderDescription(genderMask, true);
//					searchPreferences.SeekingGender = AttributeHelper.GetGenderDescription(genderMask, true); // no reverse
//					searchPreferences.Gender = AttributeHelper.GetGenderDescription(genderMask, false);
				}
				else
				{
					throw new Exception("could not retrieve gender/seeking gender");
					// get from profile settings?
					//searchPreferences.Gender ;
					//searchPreferences.SeekingGender ;
				}

				searchPreferences.ShowOnlyMembersWithPhotos = ParseDefault(preferences.Value("hasphotoflag"), 0) == 1 ? true : false;
				//searchPreferences.ShowOnlyJewishMembers = true; // can be overriden during a session, then reverts back to true
				return searchPreferences;

				// per Stacey and Jesse, "Show only Jewish members" should be default for everyone on jdate.com and mobile - despite the code below existing in web tier (has not effect?)
				//int jDateRelgionMask;
				
				//if (Int32.TryParse(preferences.Value("jdatereligion"), out jDateRelgionMask)) {
				//    searchPreferences.ShowOnlyJewishMembers = false; // unknown if member is Jewish, default to unrestricted search
				//}
				//else {
				//    if (((jDateRelgionMask & 16384) == 16384) || //Willing to Convert
				//        ((jDateRelgionMask & 2048) == 2048) || //Not Willing to Convert
				//        ((jDateRelgionMask & 512) == 512)) //Not sure if I’m willing to convert
				//    {
				//        searchPreferences.ShowOnlyJewishMembers = false; // member is not Jewish, default to unrestricted search
				//    }
				//    else {
				//        searchPreferences.ShowOnlyJewishMembers = true; // member is Jewish, default to limit search to Jewish only
				//    }
				//}

			}
			else
			{
				// set up default prefs
			}
			return null;
		}


		/// <summary>
		/// using the SAL version of the now.  Keeping the code here for the moment
		/// </summary>
		/// <param name="prefs"></param>
		[Obsolete]


		public bool SaveSearchPreferences(SearchPreferencesRequest prefs, Brand brand)
		{
			var preferences = SearchPreferencesSA.Instance.GetSearchPreferences(prefs.MemberId, prefs.Brand.Site.Community.CommunityID);
			
			if (preferences == null) return false;

			// NOTE: gender and seeking gender are REVERSED when they are stored/retrieved.  
			// NOTE: They are stored from the perspective of the user being searched for
			var seekingString = "Seeking" + prefs.Gender;
			var seekingBit = (int) Enum.Parse(typeof(GenderMask), seekingString, true);
			var genderBit = (int)Enum.Parse(typeof(GenderMask), prefs.SeekingGender, true);
			var genderMask = genderBit | seekingBit;


			preferences["minage"] = prefs.MinAge.ToString();
			preferences["maxage"] = prefs.MaxAge.ToString();
			preferences["distance"] = prefs.MaxDistance.ToString();
			preferences["gendermask"] = genderMask.ToString();
			preferences["hasphotoflag"] = prefs.ShowOnlyMembersWithPhotos ? "1" : "0";

			SearchPreferencesSA.Instance.Save(prefs.MemberId, prefs.Brand.Site.Community.CommunityID, preferences);
			return true;
			//var searches = MemberSearchCollection.Load(prefs.MemberId, prefs.Brand);
			//var search = searches.PrimarySearch;
			//if (search == null)
			//{
			//    // don't need this yet since only one search is currently allowed
			//    //int genderMask = 0;
			//    //int regionId = 0;
			//    //search = new MemberSearch(prefs.MemberId, prefs.Brand.Site.Community.CommunityID, SearchType.PostalCode, genderMask,
			//    //                          prefs.MinAge, prefs.MaxAge, regionID, prefs.MaxDistance);
			//    //searches.Add(search);
			//    //searches.Save();
			//}
			//else
			//{
			//    search.Gender = Gender.Male;
			//    search.Gender = Gender.Female;
			//    search.Age.MinValue = prefs.MinAge;
			//    search.Age.MaxValue = prefs.MaxDistance;
			//    search.HasPhoto = prefs.ShowOnlyMembersWithPhotos;
			//    search.Distance = prefs.MaxDistance;
			//}
		}


	}
}
