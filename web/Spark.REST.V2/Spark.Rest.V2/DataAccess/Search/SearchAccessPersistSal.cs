﻿#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.Interfaces;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.REST.Entities.Profile;
using Spark.REST.Entities.Search;
using Spark.REST.Models.Search;
using Spark.SAL;
using SearchType = Matchnet.Search.Interfaces.SearchType;

#endregion

namespace Spark.REST.DataAccess.Search
{
    internal class SearchAccessSal : ISearchAccess
    {
        public static readonly SearchAccessSal Instance = new SearchAccessSal();

        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(SearchAccessSal));

        private SearchAccessSal()
        {
        }

        public SearchResults GetSearchResults(Brand brand, int memberId, int pageSize, int pageNumber)
        {
            var searchResults = GetSearchResults(brand, memberId, pageSize, pageNumber,
                SearchResultType.MiniProfile) as SearchResults;
            return searchResults;
        }

        public SearchResultsV2 GetSearchResultsV2(Brand brand, int memberId, int pageSize, int pageNumber)
        {
            var searchResults = GetSearchResults(brand, memberId, pageSize, pageNumber,
                SearchResultType.AttributeSet) as SearchResultsV2;
            return searchResults;
        }

        public Entities.Search.SearchPreferences GetSearchPreferences(Brand brand, int memberId)
        {
            var memberSearch = MemberSearchCollection.Load(memberId, brand).PrimarySearch;

            var heightPref = memberSearch[Preference.GetInstance("Height")] as MemberSearchPreferenceRange;

            if (memberSearch == null || heightPref == null) return null;

            var advancedSearchPreferencesMask = new Dictionary<string, object>();
            foreach (var name in Preference.PreferenceNames)
            {
                if (Preference.GetInstance(name.ToString()).PreferenceType != PreferenceType.Mask) continue;
                var mask = memberSearch[Preference.GetInstance(name.ToString())] as MemberSearchPreferenceMask;
                if (mask != null)
                {
                    if (name.ToString().ToLower() == "childrencount")
                    {
                        if (mask.Value > 0)
                        {
                            //API clients are using attribute options endpoint to get ChildrenCount and the values in DB are 0, 1, 2, 3, instead of 1, 2, 4, 8
                            List<int> childrenCounts = new List<int>();
                            if ((mask.Value & 1) == 1)
                            {
                                childrenCounts.Add(0);
                            }

                            if ((mask.Value & 2) == 2)
                            {
                                childrenCounts.Add(1);
                            }

                            if ((mask.Value & 4) == 4)
                            {
                                childrenCounts.Add(2);
                            }

                            if ((mask.Value & 8) == 8)
                            {
                                childrenCounts.Add(3);
                            }

                            advancedSearchPreferencesMask.Add(name.ToString(), childrenCounts);
                        }
                        else
                        {
                            advancedSearchPreferencesMask.Add(name.ToString(), mask.Value);
                        }
                    }
                    else
                    {
                        advancedSearchPreferencesMask.Add(name.ToString(), mask.Value);
                    }
                }
            }

            var searchPreferences = new Entities.Search.SearchPreferences
            {
                MemberId = memberId,
                MinAge = memberSearch.Age.MinValue,
                MaxAge = memberSearch.Age.MaxValue,
                MaxDistance = memberSearch.Distance,
                Gender = memberSearch.Gender.ToString(), 
                SeekingGender = memberSearch.SeekingGender.ToString(),
                ShowOnlyMembersWithPhotos = memberSearch.HasPhoto,
                MinHeight = heightPref.MinValue,
                MaxHeight = heightPref.MaxValue,
                RegionId = memberSearch.RegionID.ToString(CultureInfo.InvariantCulture),
                AdvancedSearchPreferences = advancedSearchPreferencesMask
            };

            return searchPreferences;
        }

        public bool SaveSearchPreferences(SearchPreferencesRequest prefs, Brand brand)
        {
            var memberSearch = MemberSearchCollection.Load(prefs.MemberId, brand).PrimarySearch;
            if (memberSearch == null) return false;

            try
            {
                if (!string.IsNullOrEmpty(prefs.Gender))
                    memberSearch.Gender = (Gender) Enum.Parse(typeof (Gender), prefs.Gender, true); // no reverse
                if (!string.IsNullOrEmpty(prefs.SeekingGender))
                    memberSearch.SeekingGender = (Gender) Enum.Parse(typeof (Gender), prefs.SeekingGender, true);
            }
            catch (ArgumentException ex)
            {
                Log.LogError("failed to parse gender", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                return false;
            }

            if (prefs.RegionId > 0)
            {
                memberSearch.RegionID = prefs.RegionId;
            }
            else if (!string.IsNullOrEmpty(prefs.Location))
            {
                int regionId;
                if (int.TryParse(prefs.Location, out regionId))
                    memberSearch.RegionID = regionId;
            }
            if (prefs.MinAge != Constants.NULL_INT && prefs.MinAge != 0)
                memberSearch.Age.MinValue = prefs.MinAge;
            if (prefs.MaxAge != Constants.NULL_INT && prefs.MaxAge != 0)
                memberSearch.Age.MaxValue = prefs.MaxAge;
            if ( prefs.MaxDistance != Constants.NULL_INT && prefs.MaxDistance != 0)
                memberSearch.Distance = prefs.MaxDistance;
            // Default set to true
            if (prefs.ShowOnlyMembersWithPhotos.HasValue)
            {
                memberSearch.HasPhoto = prefs.ShowOnlyMembersWithPhotos.Value;
            }
            else
            {
                memberSearch.HasPhoto = prefs.ShowOnlyMembersWithPhotos.Value;
            }
            var mspHeight =
                MemberSearchPreference.CreateInstance(memberSearch.MemberSearchID, Preference.GetInstance("Height")) as
                    MemberSearchPreferenceRange;
            if (prefs.MinHeight != Constants.NULL_INT || prefs.MinHeight != 0)
                if (mspHeight != null) mspHeight.MinValue = prefs.MinHeight;
            if (prefs.MaxHeight != Constants.NULL_INT || prefs.MaxHeight != 0)
                if (mspHeight != null) mspHeight.MaxValue = prefs.MaxHeight;
            memberSearch[Preference.GetInstance("Height")] = mspHeight;

            if (prefs.AdvancedPreferences != null)
            {
                foreach (var keyValuePair in prefs.AdvancedPreferences)
                {
                    if (Preference.GetInstance(keyValuePair.Key).PreferenceType != PreferenceType.Mask) continue;
                    var newValue = 0;

                    if (keyValuePair.Key.ToLower() == "childrencount")
                    {
                        newValue = keyValuePair.Value.Aggregate(0, (current, value) => current | Convert.ToInt32(Math.Pow(2, value)));
                    }
                    else
                    {
                        newValue = keyValuePair.Value.Aggregate(0, (current, optionValue) => current | optionValue);
                    }
                    
                    var mask = memberSearch[Preference.GetInstance(keyValuePair.Key)] as MemberSearchPreferenceMask;
                    if (mask == null) continue;
                    mask.Value = newValue;
                    memberSearch[Preference.GetInstance(keyValuePair.Key)] = mask;
                }
            }

            memberSearch.Save();

            return true;
        }

        public object GetSearchResults(Brand brand, int memberId, int pageSize, int pageNumber, SearchResultType searchResultType)
        {
            var searches = MemberSearchCollection.Load(memberId, brand);
            var search = searches.PrimarySearch;
            int totalResults;
            if (pageNumber < 1)
            {
                throw new Exception("Page number is 1-indexed, must be at least 1.");
            }
            var startRow = pageSize*(pageNumber - 1) + 1;
            var matchPercentages = new Dictionary<int, int>();
            var memberIds = search.GetSearchResults(startRow, pageSize, out totalResults, SearchType.APISearch,
                SearchEntryPoint.None, matchPercentages);
            object searchResults;
            if (searchResultType == SearchResultType.AttributeSet)
            {
                searchResults = new SearchResultsV2 { Members = new List<Dictionary<string, object>>() };
                foreach (Member member in memberIds)
                {
                    var miniProfileAttributeSet = ProfileAccess.GetAttributeSetWithPhotoAttributes(brand,
                        member.MemberID,
                        memberId,
                        "resultsetprofile",
                        false,
                        null,
                        matchPercentages[member.MemberID]);
                    ((SearchResultsV2) searchResults).Members.Add(miniProfileAttributeSet.AttributeSet);
                }
            }
            else
            {
                searchResults = new SearchResults {Members = new List<MiniProfile>()};
                foreach (Member member in memberIds)
                {
                    var miniProfile = ProfileAccess.GetMiniProfile(brand, member.MemberID, memberId);
                    ((SearchResults) searchResults).Members.Add(miniProfile);
                }
            }
            return searchResults;
        }
    }
}
