﻿using System;
using System.Collections.Generic;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ServiceAdapters;
using Spark.Logger;
using Spark.REST.Entities.Profile;
using Spark.REST.Entities.Search;
using Spark.REST.Helpers;
using Spark.REST.Models.Search;
using Spark.SAL;

namespace Spark.REST.DataAccess.Search
{
	internal class SearchAccessPersistSa: ISearchAccess
	{
		private SearchAccessPersistSa() {}
		public static readonly SearchAccessPersistSa Instance = new SearchAccessPersistSa();

        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(SearchAccessPersistSa));

		private static int ParseDefault(string stringVal, int defaultVal) {
			int temp;
			return Int32.TryParse(stringVal, out temp) ? temp : defaultVal;
		}

		/// <summary>
		/// using the SAL version of this now
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="memberId"></param>
		/// <param name="pageSize"></param>
		/// <param name="pageNumber"></param>
		/// <returns></returns>
		[Obsolete] 
		public SearchResults GetSearchResults(Brand brand, int memberId, int pageSize, int pageNumber)
		{

			var searches = MemberSearchCollection.Load(memberId, brand);
			var search = searches.PrimarySearch;
			int totalResults;
			if (pageNumber < 1)
			{
				throw new Exception("Page number is 1-indexed, must be at least 1.");
			}
			var startRow = pageSize * (pageNumber - 1) + 1;

			var memberIds = search.GetSearchResults(startRow, pageSize, out totalResults);
			var searchResults = new SearchResults { Members = new List<MiniProfile>() };
			foreach (Member member in memberIds)
			{
				var miniProfile = ProfileAccess.GetMiniProfile(brand, member.MemberID, memberId);
				searchResults.Members.Add(miniProfile);
			}
			return searchResults;
		}


		/// <summary>
		/// using the SAL version of this now
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="memberId"></param>
		/// <returns></returns>
		[Obsolete]
        public Entities.Search.SearchPreferences GetSearchPreferences(Brand brand, int memberId)
		{
			var preferences = SearchPreferencesSA.Instance.GetSearchPreferences(memberId, brand.Site.Community.CommunityID);
			if (preferences != null)
			{
                var searchPreferences = new Entities.Search.SearchPreferences
				                        	{
				                        		MemberId = memberId,
				                        		MinAge = ParseDefault(preferences.Value("minage"), 0),
				                        		MaxAge = ParseDefault(preferences.Value("maxage"), 99),
				                        		MaxDistance = ParseDefault(preferences.Value("distance"), 20)
				                        	};
				int genderMask;
				if (Int32.TryParse(preferences.Value("gendermask"), out genderMask))
				{
					// NOTE: gender and seeking gender are REVERSED when they are stored/retrieved.  They are stored from the perspective of the user being searched for
					searchPreferences.SeekingGender = AttributeHelper.GetGenderDescription(brand, genderMask, false);
					searchPreferences.Gender = AttributeHelper.GetGenderDescription(brand, genderMask, true);
				}
				else
				{
					throw new Exception("could not retrieve gender/seeking gender");
				}

				searchPreferences.ShowOnlyMembersWithPhotos = ParseDefault(preferences.Value("hasphotoflag"), 0) == 1 ? true : false;
				return searchPreferences;

			}
			else
			{
				// set up default prefs
			}
			return null;
		}

	    /// <summary>
	    /// using the SAL version of the now.  Keeping the code here for the moment
	    /// </summary>
	    /// <param name="prefs"></param>
	    /// <param name="brand"></param>
	    [Obsolete]
		public bool SaveSearchPreferences(SearchPreferencesRequest prefs, Brand brand)
		{
			var preferences = SearchPreferencesSA.Instance.GetSearchPreferences(prefs.MemberId, prefs.Brand.Site.Community.CommunityID);
			
			if (preferences == null) return false;

			// NOTE: gender and seeking gender are REVERSED when they are stored/retrieved.  
			// NOTE: They are stored from the perspective of the user being searched for
			var seekingString = "Seeking" + prefs.Gender;
			var seekingBit = (int) Enum.Parse(typeof(GenderMask), seekingString, true);
			var genderBit = (int)Enum.Parse(typeof(GenderMask), prefs.SeekingGender, true);
			var genderMask = genderBit | seekingBit;


			preferences["minage"] = prefs.MinAge.ToString();
			preferences["maxage"] = prefs.MaxAge.ToString();
			preferences["distance"] = prefs.MaxDistance.ToString();
			preferences["gendermask"] = genderMask.ToString();
			preferences["hasphotoflag"] = (prefs.ShowOnlyMembersWithPhotos.HasValue && !prefs.ShowOnlyMembersWithPhotos.Value) ? "0" : "1"; //default to true

			SearchPreferencesSA.Instance.Save(prefs.MemberId, prefs.Brand.Site.Community.CommunityID, preferences);
			return true;

		}

        public SearchResultsV2 GetSearchResultsV2(Brand brand, int memberId, int pageSize, int pageNumber)
        {
            throw new NotImplementedException();
        }
    }
}
