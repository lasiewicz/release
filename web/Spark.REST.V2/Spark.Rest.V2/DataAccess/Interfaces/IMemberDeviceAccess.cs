﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.PushNotification;

namespace Spark.Rest.V2.DataAccess.Interfaces
{
    public interface IMemberDeviceAccess
    {
        IMemberAppDeviceService MemberAppDeviceService { get; set; }
        IPushNotificationMetadataSAService PushNotificationMetadataService { get; set; }
        
        string AddAppDevice(Int32 memberId, Int32 appId, Brand brand, string deviceId,
            string deviceType, string appVersion, string OSVersion);

        void UpdateDeviceNotificationSettings(int memberId, int appId, string deviceId, 
            PushNotificationCategoryId notificationCategoryId, bool enabled);

        void DeleteAppDevice(Int32 memberId, Int32 appId, string deviceId);

        void UpdateLastLoginDate(int memberId, Brand brand, string ipAddress, string userAgent, string headers, int applicationId);
    }
}