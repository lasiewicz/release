﻿using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.PremiumServices.ServiceAdapter;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions;
using Spark.Logger;
using Spark.Rest.V2.Entities.Premium;
using Spark.Rest.V2.Models.Premium;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.DataAccess;
using Spark.SAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.BaseClasses;
using Matchnet.Content.ValueObjects.AttributeMetadata;
//using Spark.Rest.V2.Models;

namespace Spark.Rest.V2.DataAccess.Premium
{
    public class PremiumAccess
    {
        #region Singleton
        public static readonly PremiumAccess Instance = new PremiumAccess();
        private PremiumAccess()
        {

        }
        #endregion

        public InstanceMemberCollection GetMemberPremiumServices(Brand brand, int memberID)
        {
            InstanceMemberCollection memberPremiumServices = ServiceMemberSA.Instance.GetMemberServices(brand.BrandID, memberID);
            return memberPremiumServices;
        }

        public SpotlightSettings GetSpotlightSettings(Brand brand, int memberID)
        {
            SpotlightSettings spotlightSettings = null;
            var memberPremiumServices = GetMemberPremiumServices(brand, memberID);

            if (memberPremiumServices != null && memberPremiumServices.Count > 0)
            {
                foreach (InstanceMember m in memberPremiumServices)
                {
                    PremiumService s = Matchnet.PremiumServices.ServiceAdapter.ServiceSA.Instance.GetPremiumService(m.ServiceInstanceID);
                    if (s.ServiceID == (int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember)
                    {
                        spotlightSettings = new SpotlightSettings(m);
                        break;
                    }
                }
            }

            if (spotlightSettings == null)
            {
                spotlightSettings = new SpotlightSettings(null);
            }

            return spotlightSettings;

        }

        public HttpSub400StatusCode UpdateSpotlightSettings(UpdateSpotlightSettingsRequest settingsRequest, int memberID)
        {
            var spotlightSettings = GetSpotlightSettings(settingsRequest.Brand, memberID);
            if (settingsRequest.MinAge > settingsRequest.MaxAge || settingsRequest.MaxAge > 99 || settingsRequest.MinAge < 18
                || settingsRequest.RegionId <= 0 || settingsRequest.Distance <= 0)
            {
                return HttpSub400StatusCode.GenericFailedValidation;
            }
            spotlightSettings.AgeMax = settingsRequest.MaxAge;
            spotlightSettings.AgeMin = settingsRequest.MinAge;
            spotlightSettings.Distance = settingsRequest.Distance;
            spotlightSettings.EnableFlag = settingsRequest.IsSpotlightEnabled;
            spotlightSettings.GenderMask = Spark.SAL.GenderUtils.GetGenderMask(settingsRequest.SeekingGender, settingsRequest.Gender);
            spotlightSettings.RegionID = settingsRequest.RegionId;

            Matchnet.PremiumServices.ServiceAdapter.ServiceMemberSA.Instance.SaveMemberService(settingsRequest.Brand, memberID, spotlightSettings.ServiceMember);
            return HttpSub400StatusCode.None;
        }

        public Dictionary<string, object> GetNextSpotlight(Brand brand, int memberID, List<int> searchIDs, int pageID, string attributeSetName)
        {
            int spotlightMemberID = 0;
            Dictionary<string, object> miniProfileAttributeSet = null;

            if (memberID > 0)
            {
                Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                if (member != null)
                {
                    //get self spotlight profile
                    spotlightMemberID = SpotlightMemberSA.Instance.GetSelfProfileSpotlight(brand, member, true);
                    if (spotlightMemberID > 0)
                    {
                        miniProfileAttributeSet = ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, spotlightMemberID, memberID, attributeSetName).AttributeSet;
                        return miniProfileAttributeSet;
                    }

                    //get next spotlight profile
                    bool promoMemberFlag = false;
                    Matchnet.PremiumServices.ValueObjects.PremiumServiceCollections services = Matchnet.PremiumServices.ServiceAdapter.ServiceSA.Instance.GetPremiumServiceCollection();
                    spotlightMemberID = SpotlightMemberSA.Instance.GetNextSpotlight(brand, member, searchIDs, services, pageID, false, out promoMemberFlag);
                    if (spotlightMemberID > 0)
                    {
                        miniProfileAttributeSet = ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, spotlightMemberID, memberID, "resultsetprofile").AttributeSet;
                    }
                }
            }

            return miniProfileAttributeSet;

        }

    }

    /// <summary>
    ///  Question and Answer methods
    /// </summary>
    public class PremiumAccessQuestionAnswer : IPremiumAccessQuestionAnswer
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(PremiumAccessQuestionAnswer));

        private readonly IQuestionAnswerAdapter _questionAnswer;

        /// <summary>
        ///     Dependency Inject
        /// </summary>
        /// <param name="questionAnswer"></param>
        public PremiumAccessQuestionAnswer(IQuestionAnswerAdapter questionAnswer)
        {
            _questionAnswer = questionAnswer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public QuestionResponse GetQuestion(int questionId)
        {
            return _questionAnswer.GetQuestion(questionId).Copy<QuestionResponse>();
        }

        public QuestionResponse GetQuestion(int questionId, int pageSize, int pageNumber, AnswerSortOrder sortOrder)
        {
            var question = _questionAnswer.GetQuestionPaged(questionId, pageNumber, pageSize,
                sortOrder == AnswerSortOrder.Newest);

            var ret = question.Copy<QuestionResponse>();
            ret.TotalAnswers = question.TotalAnswers;
            return ret;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public QuestionList GetActiveQuestionList(int siteId)
        {
            return _questionAnswer.GetActiveQuestionList(siteId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="pageSize"></param>
        /// <param name="startRow"></param>
        /// <param name="sort"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public PagedResult<QuestionResponse> GetActiveQuestionListPaged(int siteId, int pageNumber, int pageSize, QuestionListSort sort)
        {
            var questions = _questionAnswer.GetActiveQuestionListPaged(siteId, pageNumber, pageSize, sort);
            var stockAnswers =_questionAnswer.GetStockAnswers(questions.Items.Select(question => question.QuestionID).ToList());
            var mapping = new Dictionary<int, List<StockAnswer>>();
            foreach (var answer in stockAnswers)
            {
                if(!mapping.ContainsKey(answer.QuestionID))    
                    mapping[answer.QuestionID] = new List<StockAnswer>();
                mapping[answer.QuestionID].Add(answer);
            }

            foreach (var question in questions.Items)
            {
                if (mapping.ContainsKey(question.QuestionID))
                    question.StockAnswers = mapping[question.QuestionID];
            }

            return questions;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public ActiveQuestionIDList GetActiveQuestionIds(int siteId)
        {
            return _questionAnswer.GetActiveQuestionIDs(siteId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public MemberQuestionList GetMemberQuestions(int memberId, int siteId)
        {
            return _questionAnswer.GetMemberQuestions(memberId, siteId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="daysBack"></param>
        /// <returns></returns>
        public PagedResult<Question> GetRecentAnswerList(int siteId, int daysBack, int pageNumber,int pageSize)
        {
            return _questionAnswer.GetRecentQuestionListPaged(siteId, daysBack, pageNumber, pageSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="siteId"></param>
        /// <param name="questionId"></param>
        /// <param name="answerValue"></param>
        public void AddAnswer(int memberId, int siteId, int questionId, string answerValue)
        {
            _questionAnswer.AddAnswer(memberId,siteId,questionId,answerValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="siteId"></param>
        /// <param name="questionId"></param>
        /// <param name="answerValue"></param>
        public void UpdateAnswer(int memberId, int siteId, int questionId, string answerValue)
        {
            
            var answer = new Answer()
            {
                QuestionID = questionId,
                MemberID =  memberId,
                AnswerValue  = answerValue
            };
            
            _questionAnswer.UpdateAnswer(answer,siteId);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="siteId"></param>
        public void AdminUpdateAnswer(Answer answer, int siteId)
        {
            _questionAnswer.AdminUpdateAnswer(answer,siteId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="questionID"></param>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        public void RemoveMemberAnswer(int questionId, int siteId, int memberId)
        {
            _questionAnswer.RemoveMemberAnswer(questionId,siteId,memberId);    
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public interface IPremiumAccessQuestionAnswer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="questionId"></param>
        /// <returns></returns>
        QuestionResponse GetQuestion(int questionId);

        QuestionResponse GetQuestion(int questionId, int pageSize, int pageNumber, AnswerSortOrder sortOrder);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        QuestionList GetActiveQuestionList(int siteId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        PagedResult<QuestionResponse> GetActiveQuestionListPaged(int siteId, int pageNumber, int pageSize, QuestionListSort sort);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        ActiveQuestionIDList GetActiveQuestionIds(int siteId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        MemberQuestionList GetMemberQuestions(int memberId, int siteId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="daysBack"></param>
        /// <returns></returns>
        PagedResult<Question> GetRecentAnswerList(int siteId, int daysBack, int pageNumber, int pageSize);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="siteId"></param>
        /// <param name="questionId"></param>
        /// <param name="answerValue"></param>
        void AddAnswer(int memberId, int siteId, int questionId, string answerValue);

        void UpdateAnswer(int memberId, int siteId, int questionId, string answerValue);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="siteId"></param>
        void AdminUpdateAnswer(Answer answer, int siteId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="questionID"></param>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        void RemoveMemberAnswer(int questionId, int siteId, int memberId);
    }
}