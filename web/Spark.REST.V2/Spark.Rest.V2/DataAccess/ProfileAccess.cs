﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Matchnet;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;
using Spark.Common.Localization;
using Spark.Logger;
using Spark.Managers.Managers;
using Spark.Rest.Entities.Applications;
using Spark.Rest.Helpers;
using Spark.Rest.V2.BedrockPorts;
using Spark.Rest.V2.DataAccess;
using Spark.Rest.V2.DataAccess.Profile;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Helpers.Mappers;
using Spark.Rest.V2.Models.Profile;
using Spark.REST.Configuration;
using Spark.REST.DataAccess.Attributes;
using Spark.REST.DataAccess.Content;
using Spark.REST.DataAccess.Subscription;
using Spark.REST.Entities.Content.AttributeData;
using Spark.REST.Entities.Profile;
using Spark.REST.Helpers;

#endregion

namespace Spark.REST.DataAccess
{
    /// <summary>
    /// The ProfileAccess static helper class for providing
    /// </summary>
    public static class ProfileAccess
    {
        private const string GamEnUs = "enustargetingprofile";
        private const string GamHeIl = "heiltargetingprofile";
        public enum RamahAssociatedCommunity { Jdate=3 };

        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(ProfileAccess));

        internal static bool UpdateProfileEnabledStatus(int memberId, int adminId, int reasonId, bool enabled, Brand brand)
        {
            try
            {
                var settingsManager = new SettingsManager();
                var logonOnlySite = settingsManager.GetSettingBool(SettingConstants.LOGON_ONLY_SITE, brand);

                if (logonOnlySite)
                {
                    var activeFlag = enabled ? 0 : 1;
                    MemberSA.Instance.ToggleLogonDisabledFlag(memberId, activeFlag);
                }
                else
                {
                    var adminActionReasonId = Enum.IsDefined(typeof (AdminActionReasonID), reasonId)
                        ? (AdminActionReasonID) reasonId
                        : AdminActionReasonID.Unknown;

                    var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
                    var globalStatusMask =
                        (GlobalStatusMask) member.GetAttributeInt(brand, AttributeConstants.GLOBALSTATUSMASK);
                    AdminAction adminAction;

                    if (!enabled)
                    {
                        globalStatusMask = globalStatusMask | GlobalStatusMask.AdminSuspended;
                        adminAction = AdminAction.AdminSuspendMember;
                    }
                    else
                    {
                        globalStatusMask = globalStatusMask & (~GlobalStatusMask.AdminSuspended);
                        adminAction = AdminAction.AdminUnsuspendMember;
                    }

                    member.SetAttributeInt(brand, AttributeConstants.GLOBALSTATUSMASK, (Int32) globalStatusMask);
                    var msr = MemberSA.Instance.SaveMember(member);
                    if (msr.SaveStatus != MemberSaveStatusType.Success)
                    {
                        return false;
                    }

                    AdminSA.Instance.AdminActionLogInsert(
                        memberId,
                        Constants.GROUP_PERSONALS,
                        (Int32) adminAction,
                        adminId,
                        AdminMemberIDType.AdminProfileMemberID,
                        adminActionReasonId);
                }

                return true;
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Error updating profile enabled status. MemberID: {0} Enabled: {1}", memberId, enabled),ex, ErrorHelper.GetCustomData());
                return false;
            }
        }

        /// <summary>
        ///     need to convert this to use miniProfile attribute set
        /// </summary>
        /// <param name="brand"> </param>
        /// <param name="memberId"> </param>
        /// <param name="requestingMemberId">Used for setting profile blocked flag</param>
        /// <returns> </returns>
        public static MiniProfile GetMiniProfile(Brand brand, int memberId, int requestingMemberId)
        {
            var communityId = brand.Site.Community.CommunityID;
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            var miniProfile = new MiniProfile
            {
                Id = member.MemberID,
                Username = member.GetUserName(brand),
                ApprovedPhotoCount = 0
            };

            if (member.HasApprovedPhoto(communityId))
            {
                var photos = member.GetPhotos(communityId);
                if (photos != null)
                {
                    var defaultFound = false;
                    for (byte i = 0; i < photos.Count; i++)
                    {
                        var photo = photos[i];
                        if (photo == null || !photo.IsApproved) continue;
                        var fullPath = MemberHelper.GetPhotoUrl(photo, PhotoType.Full, brand);
                        var thumbPath = MemberHelper.GetPhotoUrl(photo, PhotoType.Thumbnail, brand);

                        miniProfile.ApprovedPhotoCount++;
                        miniProfile.DefaultPhoto = new Photo
                        {
                            FullId = photo.FileID,
                            ThumbId = photo.ThumbFileID,
                            FullPath = fullPath,
                            ThumbPath = thumbPath,
                            Caption = photo.Caption,
                            IsCaptionApproved = photo.IsCaptionApproved,
                            IsPhotoApproved = photo.IsApproved
                        };
                        if (defaultFound || String.IsNullOrEmpty(fullPath)) continue;
                        miniProfile.PhotoUrl = fullPath;
                        miniProfile.ThumbnailUrl = thumbPath;

                        defaultFound = true;
                    }
                }
            }

            miniProfile.LastLoggedIn = member.GetLastLogonDate(communityId);
            miniProfile.LastUpdatedProfile = member.GetAttributeDate(brand, "LastUpdated");
            miniProfile.Location = AttributeHelper.GetLocation(brand, member.GetAttributeInt(brand, "RegionID"), false);
            miniProfile.MaritalStatus =
                AttributeHelper.GetFirstDescription(AttributeHelper.GetMaskDescriptions("MaritalStatus", member.GetAttributeInt(brand, "MaritalStatus"),brand));
            var genderMask = member.GetAttributeInt(brand, "GenderMask");
            miniProfile.Gender = AttributeHelper.GetGenderDescription(brand, genderMask, false);
            miniProfile.GenderSeeking = AttributeHelper.GetGenderDescription(brand, genderMask, true);
            miniProfile.Age = AttributeHelper.GetAge(member.GetAttributeDate(brand, "Birthdate"));
            miniProfile.JDateRelegion = MemberHelper.GetDescription("JDateReligion", member.GetAttributeInt(brand, "JDateReligion"), brand);
            miniProfile.MatchRating = MemberHelper.GetMatchRating(brand, memberId, requestingMemberId);

            try
            {
                var attributeMaskValue = member.GetAttributeInt(brand, "JDateEthnicity");
                if (attributeMaskValue != Constants.NULL_INT)
                {
                    var descriptions = MemberHelper.GetDescriptions("JDateEthnicity", attributeMaskValue, brand);
                    if (null != descriptions && descriptions.Count > 0)
                    {
                        miniProfile.JDateEthnicity = descriptions[0];
                    }
                }
            }
            catch (Exception e)
            {
                Log.LogError("No JDateEthnicity for memberid:" + memberId, e, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }
            var memberSubscriptionStatus = UnifiedPaymentSystemHelper.GetMemberSubcriptionStatus(memberId, brand);
            var enumSubscriptionStatus = (Matchnet.Purchase.ValueObjects.SubscriptionStatus) memberSubscriptionStatus;
            miniProfile.SubscriptionStatus = UnifiedPaymentSystemHelper.Instance.ConvertSubscriptionStatusToString(enumSubscriptionStatus, true);
            miniProfile.ZipCode = AttributeHelper.GetZipCode(brand, member.GetAttributeInt(brand, "RegionID"));
            miniProfile.RegistrationDate = member.GetAttributeDate(brand, "BrandInsertDate");
            miniProfile.SubscriptionStatusGam = UnifiedPaymentSystemHelper.Instance.GetSubscriptionStatusGam(brand, member);
            miniProfile.Heightcm = member.GetAttributeInt(brand, "Height");
            miniProfile.SelfSuspendedFlag = member.GetAttributeBool(brand, "SelfSuspendedFlag");
            miniProfile.AdminSuspendedFlag = MemberHelper.IsMemberAdminSuspended(brand, member);
            miniProfile.IsHighlighted = MemberHelper.IsHighlighted(brand, member);
            miniProfile.IsSpotlighted = MemberHelper.IsSpotlighted(brand, member);

            if (requestingMemberId == Constants.NULL_INT) return miniProfile;
            if (requestingMemberId == memberId) return miniProfile;

            // Am I blocking them?
            miniProfile.BlockContact = MemberHelper.IsRequestedMemberBlocked(brand, memberId, requestingMemberId, HotListCategory.IgnoreList);
            miniProfile.BlockSearch = MemberHelper.IsRequestedMemberBlocked(brand, memberId, requestingMemberId,HotListCategory.ExcludeList);
            miniProfile.RemoveFromSearch = MemberHelper.IsRequestedMemberBlocked(brand, memberId, requestingMemberId, HotListCategory.ExcludeFromList);
            // Are they blocking me?
            miniProfile.BlockContactByTarget = MemberHelper.IsRequestedMemberBlocked(brand, requestingMemberId, memberId, HotListCategory.IgnoreList);
            miniProfile.BlockSearchByTarget = MemberHelper.IsRequestedMemberBlocked(brand, requestingMemberId, memberId, HotListCategory.ExcludeList);
            miniProfile.RemoveFromSearchByTarget = MemberHelper.IsRequestedMemberBlocked(brand, requestingMemberId, memberId, HotListCategory.ExcludeFromList);
            miniProfile.IsViewerFavorite = HotListAccess.MemberIsOnHotlist(brand, HotListCategory.Default, requestingMemberId, member.MemberID);

            return miniProfile;
        }

        /// <summary>
        /// A wrapped call around GetAttributeSet to populate photo attributes, should be refactored
        /// </summary>
        /// <param name="brand">The brand.</param>
        /// <param name="targetMemberId">The target member identifier.</param>
        /// <param name="viewerId">The viewer identifier.</param>
        /// <param name="attributeSetName">Name of the attribute set.</param>
        /// <param name="isVisitor">if set to <c>true</c> [is visitor].</param>
        /// <param name="urlReferrer">The URL referrer.</param>
        /// <param name="matchScore">The match score.</param>
        /// <returns></returns>
        /// <exception cref="SparkAPIReportableArgumentException"></exception>
        public static GetAttributeSetResult GetAttributeSetWithPhotoAttributes(Brand brand, int targetMemberId, int viewerId, string attributeSetName, bool isVisitor=false, string urlReferrer="", int? matchScore = null)
        {
            var settingsManager = new SettingsManager();
            var isMemberAttrCachedEnabled = settingsManager.GetSettingBool(Matchnet.Member.ValueObjects.ServiceConstants.ENABLE_MEMBER_ATTR_SET_CACHE_CONSTANT, brand);

            var cacheKey = attributeSetName.ToLower() + Matchnet.Member.ValueObjects.ServiceConstants.FIELD_SEPARATOR + targetMemberId;
            
            GetAttributeSetResult attributeSetResult = null;

            APIAttributeSets providedType;

            if (!Enum.TryParse(attributeSetName, true, out providedType))
            {
                throw new SparkAPIReportableArgumentException(string.Format("No attribute set enum property found with name {0}.", attributeSetName));
            }

            if (!isVisitor)
            {
                isVisitor = (viewerId == Constants.NULL_INT);
            }

            //update viewed hotlist
            if (viewerId > 0 && targetMemberId > 0 && viewerId != targetMemberId && attributeSetName.ToLower().IndexOf("fullprofile") >= 0)
            {
                var ctx = System.Web.HttpContext.Current;
                MiscUtils.FireAndForget(o =>
                {
                    //set HttpContext for thread
                    System.Web.HttpContext.Current = ctx;
                    try
                    {
                        HotListAccess.SendHotListEntry(brand, targetMemberId, viewerId);
                    }
                    finally
                    {
                        //unset HttpContext for thread (precaution for GC)
                        System.Web.HttpContext.Current = null;
                    }
                }, "GetAttributeSet",
                    string.Format("Failed to complete SendHotListentry(brand(id):{0}, viewerId:{1}, viewerMemberId: {2}", brand.BrandID, viewerId, targetMemberId));
            }


            //get attributeset
            if (isMemberAttrCachedEnabled)
            {
                attributeSetResult = CachingHelper.WebCachebucket.Get(cacheKey) as GetAttributeSetResult;
            }

            var member = MemberSA.Instance.GetMember(targetMemberId, MemberLoadFlags.None);

            if (null != attributeSetResult)
            {
                var result = GetNonCacheableProperties(attributeSetResult, attributeSetName, brand, viewerId, targetMemberId, matchScore);
                return AdjustCommunityAttributes(attributeSetResult,member,brand);
                //TOO NOISY FOR LOGS
//                Log.LogInfoMessage(result, ErrorHelper.GetCustomData());
            }
            
            
            attributeSetResult = GetAttributeSetResult(brand, targetMemberId, viewerId, attributeSetName, isVisitor, matchScore);

            if (providedType == APIAttributeSets.TargetingProfile_il)
            {
                attributeSetResult.AttributeSet["prm"] = MemberHelper.GetGamPrm(brand, member, urlReferrer);
            }

            attributeSetResult = AdjustCommunityAttributes(attributeSetResult, member, brand);

            if (isMemberAttrCachedEnabled)
            {
                CachingHelper.WebCachebucket.Insert(cacheKey, attributeSetResult);
            }
            
            return attributeSetResult;
        }

        static GetAttributeSetResult AdjustCommunityAttributes(GetAttributeSetResult attributeSetResult, Member member, Brand brand)
        {
            var ret = attributeSetResult.Copy();
            ret.AttributeSet["lastLoggedIn"] = member.GetLastLogonDate(brand.Site.Community.CommunityID);
            return ret;
        }

        internal static string GetNonCacheableProperties(GetAttributeSetResult result, string attributeSetName, Brand brand, int viewerMemberId, int viewedMemberId, int? matchScore = null)
        {
            var attributeSet = GetAttributeSet(attributeSetName);
            if (!attributeSet.NonCacheableProperties.Any()) return string.Format("No non-cacheable properties found for {0}", attributeSetName);
            var nonCachedAttributeSet = new AttributeSet(attributeSet.SetName, attributeSet.NonCacheablePropertyNames, attributeSet.NonCacheableProperties);
            var attributeSetDict = GetAttributeValueDictionary(nonCachedAttributeSet, brand, viewerMemberId, viewedMemberId, matchScore);            
            foreach (var propertyName in attributeSetDict.Keys)
                result.AttributeSet[propertyName] = attributeSetDict[propertyName];
				
           return string.Format("Found {0} non-cacheable properties and added them to the AttributeSet {1}", attributeSetDict.Count(), attributeSetName);
        }

        internal static string GetMemberSubscriptionStatus(Brand brand, int memberId)
        {
            var status = "InvalidMember";
            try
            {
                var objMember = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
                if (objMember == null || objMember.EmailAddress == null)
                    // A member object may be returned, even if the member does not exists.  
                    // Check for existance of email address to ensure that member is valid
                {
                    return status;
                }
                var memberSubscriptionStatus = UnifiedPaymentSystemHelper.GetMemberSubcriptionStatus(memberId, brand);
                var enumSubscriptionStatus =
                    (Matchnet.Purchase.ValueObjects.SubscriptionStatus) memberSubscriptionStatus;
                status = UnifiedPaymentSystemHelper.Instance.ConvertSubscriptionStatusToString(enumSubscriptionStatus,
                    false);
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Member with ID {0} not found", memberId), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }
            return status;
        }

        internal static MemberBasicLogonInfo GetMemberBasicLogonInfo(int memberId, Brand brand)
        {
            try
            {
                return MemberSA.Instance.GetMemberBasicLogonInfo(memberId, brand.Site.Community.CommunityID);
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("MemberBasicLogonInfo with ID {0} not found", memberId), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }

            return null;
        }

        private static bool ValidateIfOptionValue(Brand brand, string key, int optionValue)
        {
            var optionDictionary = AttributeDataAccess.Instance.GetAttributeOptionDictionary(brand, key);
            if (optionDictionary == null || optionDictionary.Count <= 0)
                return true; // this NUMBER field is just a number, not an option value
            AttributeOption option;
            return optionDictionary.TryGetValue(optionValue, out option);
        }

        /// <summary>
        /// This method returns a collection of attributes which are grouped in the attributeSetList.xml file
        /// </summary>
        /// <param name="brand">The brand.</param>
        /// <param name="viewedMemberId">The member being viewed</param>
        /// <param name="viewerMemberId">The viewer (can affect the data returned, such as whether viewer is a favorite</param>
        /// <param name="attributeSetName">The key from the XML file</param>
        /// <param name="isVisitor">Is the viewing member a visitor? Default is false</param>
        /// <param name="matchScore">The match score.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException"></exception>
        /// <exception cref="System.Exception"></exception>
        private static GetAttributeSetResult GetAttributeSetResult(Brand brand, int viewedMemberId, int viewerMemberId, string attributeSetName, bool isVisitor = false, int? matchScore = null)
        {
            // not a visitor, but one or more member Id not valid
            if ((!isVisitor) && (viewerMemberId <= 0 || viewedMemberId <= 0))
            {
                return new GetAttributeSetResult
                {
                    AttributeSetNotFoundReason = AttributeSetNotFoundReasonType.BothMemberIdsRequired
                };
            }
            var member = MemberSA.Instance.GetMember(viewedMemberId, MemberLoadFlags.None);
            if (member == null)
                return new GetAttributeSetResult
                {
                    AttributeSetNotFoundReason = AttributeSetNotFoundReasonType.MemberNotFound
                };

            var result = new GetAttributeSetResult
            {
                AttributeSetNotFoundReason = AttributeSetNotFoundReasonType.None
            };

            var attributeSet = GetAttributeSet(attributeSetName);
            var attributeValueDict = GetAttributeValueDictionary(attributeSet, brand, viewerMemberId, viewedMemberId, matchScore);

            result.AttributeSet = attributeValueDict;
            result = AdjustCommunityAttributes(result, member, brand);
            
            return result;
        }

        private static AttributeSet GetAttributeSet(string attributeSetName)
        {
            var attributeSetDict = AttributeConfigReader.Instance.AttributeSetDictionary;
            if(attributeSetDict == null)
                throw new Exception("The AttributeSetDictionary is null.");
            AttributeSet attributeSet;
            var found = attributeSetDict.TryGetValue(attributeSetName.ToLower(), out attributeSet);
            if (found && attributeSet != null) return attributeSet;
            var argumentException = new ArgumentException(String.Format("No attribute set found with name {0}", attributeSetName));
            Log.LogError(string.Format("AttributeSetDict count {0}", attributeSetDict.Count), argumentException, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
            throw argumentException;
        }

        private static Dictionary<string, object> GetAttributeValueDictionary(AttributeSet attributeSet, Brand brand, int viewerMemberId, int viewedMemberId, int? matchScore = null)
        {
            var viewedMember = MemberSA.Instance.GetMember(viewedMemberId, MemberLoadFlags.None);
            var attributeValueDict = new Dictionary<string, object>();
            foreach (var property in attributeSet.Properties)
            {
                PseudoAttributeReaders.PseudoAttributeDelegate propertyGetter;
                if (property.Name.ToLower() == "matchrating" && matchScore != null) //this will allow a matchScore to be assigned on a search
                {
                    attributeValueDict.Add(property.Name, matchScore);
                    continue;
                }
                
                if (PseudoAttributeReaders.PseudoAttributeMethodMap.TryGetValue(property.Name.ToLower(), out propertyGetter))
                {
                    var attributeValue = propertyGetter(brand, viewedMember, viewerMemberId);
                    if (attributeSet.SetName.ToLower() == GamEnUs || attributeSet.SetName.ToLower() == GamHeIl)
                    {
                        var gamName = DfpAttributeHelper.GetPseudoAttributesGamName(property.Name);
                        attributeValueDict.Add(string.IsNullOrEmpty(gamName) ? property.Name.ToLower() : gamName, attributeValue);
                    }
                    else
                    {
                        attributeValueDict.Add(property.Name, attributeValue);
                    }
                    continue;
                }

                if (property.AttributeName == null)
                {
                    throw new Exception(String.Format("property {0} must have either an associated attribute or a pseudoAttribute method", property.Name));
                }

                var meta = AttributeDataAccess.Instance.GetAttributeMetadata(property.AttributeName);
                switch (meta.DataType.ToUpper())
                {
                    case "NUMBER":
                        var attributeIntValue =
                            AttributeHelper.GetNullableInt(viewedMember.GetAttributeInt(brand, property.AttributeName),
                                property.ZeroMeansNull);
                        var optionDictionary = AttributeDataAccess.Instance.GetAttributeOptionDictionary(brand, property.AttributeName);
                        var foundOption = false;
                        if (optionDictionary != null && optionDictionary.Count > 0)
                        // if there is an options list for this attribute, limit selections to what's in the list
                        {
                            //AttributeOption option;
                            foreach (var option in optionDictionary.Values.Where(option => attributeIntValue == option.Value))
                            {
                                attributeValueDict.Add(property.Name, option.Description);
                                foundOption = true;
                                break;
                            }
                            //Log.WarnFormat("attribute {0} appears to be an option type, but has no matching option for value {1}", property.Name, attributeIntValue);
                        }
                        if (!foundOption)
                        {
                            attributeValueDict.Add(property.Name, attributeIntValue);
                        }
                        break;
                    case "TEXT":
                        var attributeTextValue = viewedMember.GetAttributeTextApproved(brand, property.AttributeName,
                            viewerMemberId,
                            ResourceProvider.Instance.
                                GetResourceValue(
                                    brand.Site.SiteID,
                                    brand.Site.CultureInfo,
                                    ResourceGroupEnum.Global,
                                    "FREETEXT_NOT_APPROVED"));
                        attributeValueDict.Add(property.Name, attributeTextValue);
                        break;
                    case "BIT":
                        var attributeBoolValue = viewedMember.GetAttributeBool(brand, property.AttributeName);
                        attributeValueDict.Add(property.Name, attributeBoolValue);
                        break;
                    case "DATE":
                        var attributeDateValue = viewedMember.GetAttributeDate(brand, property.AttributeName);
                        attributeValueDict.Add(property.Name, attributeDateValue);
                        break;
                    case "MASK":
                        var maskValue = viewedMember.GetAttributeInt(brand, property.AttributeName);
                        var descriptions = new List<string>();
                        if (maskValue != Constants.NULL_INT)
                        {
                            var optionDict = AttributeDataAccess.Instance.GetAttributeOptionDictionary(brand,
                                property.
                                    AttributeName);
                            descriptions = (from attributeOption in optionDict.Values
                                            where (maskValue & attributeOption.Value) == attributeOption.Value
                                            select attributeOption.Description).ToList();
                        }
                        attributeValueDict.Add(property.Name, descriptions);
                        break;
                    default:
                        throw new Exception(String.Format("Unknown data type for attribute {0}", property.AttributeName));
                }
            }
            return attributeValueDict;
        }

        /// <summary>
        /// Assigns the property.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dto">The dto.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="attrtibuteValue">The attrtibute value.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public static T AssignProperty<T>(T dto, string propertyName, object attrtibuteValue) where T : class
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     UNFINISHED! This can be used to return a strongly typed object, FullProfile, MiniProfile, etc., at the performance
        ///     expense of using reflection
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="brand"> </param>
        /// <param name="viewedMemberId"> </param>
        /// <param name="viewerMemberId"> </param>
        /// <param name="attributeSetName"> </param>
        /// <returns> </returns>
        public static T GetAttributeSet<T>(Brand brand, int viewedMemberId, int viewerMemberId, string attributeSetName) where T : class, new()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the name of the property name for attribute.
        /// </summary>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <returns></returns>
        public static string GetPropertyNameForAttributeName(string attributeName)
        {
            var propertyName = string.Empty;
            var attributeProperties = AttributeConfigReader.Instance.AttributeWhitelistDictionaryByAttribute;
            if (attributeProperties.Keys.Contains(attributeName.ToLower()))
            {
                propertyName = attributeProperties[attributeName.ToLower()].Name;
            }

            return propertyName;
        }

        /// <summary>
        /// Updates the properties.
        /// </summary>
        /// <param name="brand">The brand.</param>
        /// <param name="memberId">The member identifier.</param>
        /// <param name="attributeData">The attribute data.</param>
        /// <param name="attributeDataMultiValue">The attribute data multi value.</param>
        /// <exception cref="System.ArgumentException">attributeData/attributeDataMultiValue</exception>
        /// <exception cref="System.Exception"></exception>
        /// <exception cref="System.NotImplementedException">setting of boolean attribute values has not been implemented</exception>
        public static void UpdateProperties(Brand brand, int memberId, Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            var stopwatch = new Stopwatch();
            try
            {
                if (attributeData == null) attributeData = new Dictionary<string, object>();
                if (attributeDataMultiValue == null) attributeDataMultiValue = new Dictionary<string, List<int>>();

                if (attributeData.Count == 0 && attributeDataMultiValue.Count == 0)
                {
                    throw new ArgumentException("attributeData/attributeDataMultiValue");
                }
                var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
                //Set default value for RamahAlumn when Ramah attributes are set for the first time
                SetRamahAttributes(brand, member, attributeData, attributeDataMultiValue);
                foreach (var propertyName in attributeData.Keys)
                {
                    var val = attributeData[propertyName];
                    PseudoAttributeUpdaters.PseudoAttributeDelegate propertySetter;
                    if (PseudoAttributeUpdaters.PseudoAttributeMethodMap.TryGetValue(propertyName.ToLower(), out propertySetter))
                    {
                        propertySetter(brand, member, val);
                        continue;
                    }
                    ProfileProperty property;
                    if (!AttributeConfigReader.Instance.AttributeWhitelistDictionary.TryGetValue(propertyName.ToLower(), out property))
                    {
                        throw new Exception(String.Format("property {0} must have either an assoicated attribute or a pseudoAttribute method", propertyName));
                    }
                    var attributeName = property.AttributeName;
                    var meta = AttributeDataAccess.Instance.GetAttributeMetadata(attributeName);
                    switch (meta.DataType.ToUpper())
                    {
                        case "NUMBER":
                            int intVal;
                            if (val is Int64)
                            {
                                intVal = (int) (long) (val);
                                // direct cast to int would fail, need to cast to long first
                            }
                            else if (val is Int32)
                            {
                                intVal = (int) (val);
                            }
                            else
                            {
                                throw new ArgumentException(String.Format("Invalid value for {0}.  Expected a number, input was: {1}", propertyName, val));
                            }

                            if (!ValidateIfOptionValue(brand, propertyName, intVal))
                            {
                                // no assoicated options?  assume this is a simple number and set the value  todo: verify there isn't a more deterministic way to know this
                                throw new ArgumentException(String.Format("{0} is not a valid choice for option {1}", intVal, propertyName));
                            }
                            member.SetAttributeInt(brand, attributeName, intVal);
                            break;
                        case "TEXT":
                            var textValue = val as string;
							if (string.IsNullOrWhiteSpace(textValue))
                            {
                                textValue = null;
                            }
                            var textStatusType = (property.RequiresApproval && !string.IsNullOrEmpty(textValue)) ? TextStatusType.Pending : TextStatusType.Auto;
                            member.SetAttributeText(brand, attributeName, textValue, textStatusType);
                            break;
                        case "BIT":
                            if (!(val is Boolean))
                            {
                                throw new ArgumentException(string.Format("Invalid value for {0}.  Expected a boolean, input was: {1}", propertyName, val));
                            }
                            //throw new NotImplementedException("setting of boolean attribute values has not been implemented");
                            member.SetAttributeInt(brand, attributeName, Convert.ToInt32(val));
                            break;
                        case "DATE":
                            DateTime dateTimeAttribute;
                            if (DateTime.TryParse(Convert.ToString(val), out dateTimeAttribute))
                            {
                                member.SetAttributeDate(brand, attributeName, dateTimeAttribute);
                            }
                            else
                            {
                                throw new ArgumentException(String.Format("Invalid value for {0}.  Expected a datetime, input was: {1}", propertyName, val));
                            }
                            break;
                        case "MASK":
                            throw new ArgumentException(String.Format("Use attributesMultiValue for {0}, which is a mask type.", propertyName));
                        default:
                            throw new Exception(String.Format("Unknown data type for attribute {0}", propertyName));
                    }
                }
                foreach (var propertyName in attributeDataMultiValue.Keys)
                {
                    var val = attributeDataMultiValue[propertyName];

                    ProfileProperty property;
                    if (
                        !AttributeConfigReader.Instance.AttributeWhitelistDictionary.TryGetValue(propertyName.ToLower(),
                            out property))
                    {
                        throw new Exception(String.Format("multivalue property {0} must have an assoicated attribute",
                            propertyName));
                    }
                    var meta = AttributeDataAccess.Instance.GetAttributeMetadata(property.AttributeName);
                    if (meta.DataType.ToUpper() != "MASK")
                    {
                        throw new ArgumentException(
                            String.Format("{0} is not a Mask type, cannot be set with attributesMultiValue",
                                propertyName));
                    }
                    var maskVal = 0;
                    foreach (var optionValue in val)
                    {
                        if (!ValidateIfOptionValue(brand, property.AttributeName, optionValue))
                        {
                            // no assoicated options?  assume this is a simple number and set the value  todo: verify there isn't a more deterministic way to know this
                            throw new ArgumentException(String.Format("{0} is not a valid choice for option {1}",
                                optionValue, propertyName));
                        }
                        maskVal |= optionValue;
                    }
                    member.SetAttributeInt(brand, property.AttributeName.ToLower(), maskVal);
                }
                MemberSA.Instance.SaveMember(member);
            }
            finally
            {
                stopwatch.Stop();
                Log.LogDebugMessage(string.Format("Finished UpdateProperties() in {0} millis - Brand : {1} MemberId : {2}",
                    stopwatch.ElapsedMilliseconds, brand.BrandID, memberId), ErrorHelper.GetCustomData());
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="memberId"> </param>
        /// <param name="brand"> </param>
        /// <returns> </returns>
        public static DisplaySettings GetDisplaySettings(int memberId, Brand brand)
        {
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            var hideMask = member.GetAttributeInt(brand, "HideMask");
            return new DisplaySettings((AttributeOptionHideMask) hideMask);
        }

        /// <summary>
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="attributeSetName"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public static Dictionary<string, object> LookupAttributeSetByMemberId(int memberId, int targetMemberId, string attributeSetName, Brand brand)
        {
            var result = GetAttributeSetResult(brand, targetMemberId, memberId, attributeSetName);
            return result.AttributeSetNotFoundReason == AttributeSetNotFoundReasonType.None ? result.AttributeSet : null;
        }

        /// <summary>
        /// Get the memeber's email settings.
        /// </summary>
        /// <param name="memberId">The member id</param>
        /// <param name="brand">The current brand</param>
        /// <returns>The memeber's email settings.</returns>
        public static EmailSettings GetEmailSettings(int memberId, Brand brand)
        {
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);

            // get default value for group, if one exists
            var group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);
            var defaultValue = member.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));

            //load masks, or defaults if any
            var emailAlertSettingsMask = member.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, defaultValue);
            var secretAdmirerEmailFrequency = member.GetAttributeInt(brand, "GotAClickEmailPeriod", defaultValue);
            var shouldNotifyMatches = member.GetAttributeInt(brand, "MatchNewsletterPeriod", defaultValue); 
            var newsLetterSettingsMask = member.GetAttributeInt(brand, "NewsletterMask", defaultValue); 
            var messageSettingsMask = member.GetAttributeInt(brand, "MailboxPreference", defaultValue); // WebConstants.ATTRIBUTE_NAME_MAILBOXPREFERENCE not reachable
            var vacationStartDate = member.GetAttributeDate(brand, "VacationStartDate", DateTime.MinValue);

            // get the mapper
            var mapper = AttributeToModelMapper.Instance;

            // convert to model and return
            return new EmailSettings
            {
                EmailAlertSettings = mapper.MapToEmailAlertSettings(emailAlertSettingsMask, secretAdmirerEmailFrequency, shouldNotifyMatches),
                MessageSettings = mapper.MapToMessageSettings(messageSettingsMask, vacationStartDate),
                NewsLetterSettings = mapper.MapToNewsLetterSettings(newsLetterSettingsMask)
            };
        }

        /// <summary>
        /// Update the memeber's email settings.
        /// </summary>
        /// <param name="settings">The memeber's email settings.</param>
        /// <param name="brand">The current brand</param>
        /// <param name="memberId">The member id</param>
        public static void UpdateEmailSettings(EmailSettingsRequest settings, Brand brand, int memberId)
        {
            // get the mapper
            var mapper = ModelToAttributeMapper.Instance;
            
            // convert to mask attributes
            var emailAlertSettingsMask = mapper.MapFromEmailAlertSettings(settings.EmailAlertSettings);
            var secretAdmirerEmailFrequency = settings.EmailAlertSettings.SecretAdmirerEmailFrequency;
            var newsLetterSettingsMask = mapper.MapFromNewsLetterSettings(settings.NewsLetterSettings);
            var messageSettingsMask = mapper.MapFromMessageSettings(settings.MessageSettings);

            // save
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            member.SetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, emailAlertSettingsMask);
            member.SetAttributeInt(brand, "GotAClickEmailPeriod", secretAdmirerEmailFrequency);
            member.SetAttributeInt(brand, "MatchNewsletterPeriod", settings.EmailAlertSettings.ShouldNotifyMatches ? 1 : 0); 
            member.SetAttributeInt(brand, "NewsletterMask", newsLetterSettingsMask); 
            member.SetAttributeInt(brand, "MailboxPreference", messageSettingsMask); // WebConstants.ATTRIBUTE_NAME_MAILBOXPREFERENCE not reachable
            member.SetAttributeDate(brand, "VacationStartDate", settings.MessageSettings.ShouldReplyOnMyBehalf ? DateTime.Now : DateTime.MinValue);
            MemberSA.Instance.SaveMember(member);
        }

        /// <summary>
        /// Self suspend a member's profile.
        /// </summary>
        /// <param name="request">The self suspend request.</param>
        /// <param name="brand">The current brand</param>
        /// <param name="memberId">The member id</param>
        public static void SuspendProfile(MemberSuspendRequest request, Brand brand, int memberId)
        {
            // save
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            member.SetAttributeInt(brand, "SelfSuspendedReasonType", request.SuspendReasonId);
            member.SetAttributeInt(brand, "SelfSuspendedFlag", 1);
            MemberSA.Instance.SaveMember(member);

            // remove from online members currently showing up
            MembersOnlineSA.Instance.Remove(brand.Site.Community.CommunityID, memberId);
        }

        /// <summary>
        /// Get the reason for a member self suspending their profile.
        /// 1	found my soulmate on the site
        /// 2	found my soulmate on my own
        /// 4	too many responses
        /// 8	too few responses
        /// 16	taking a break - vacation
        /// 32	technical issues (logging in, etc.)
        /// 64	site performance (photos, essays, etc.)
        /// 128	Suspend other
        /// </summary>
        /// <param name="memberId">The member id</param>
        /// <param name="brand">The current brand</param>
        /// <returns>The reason for a member self suspending their profile</returns>
        public static int GetSuspendedProfileReasonId(Brand brand, int memberId)
        {
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            return member.GetAttributeInt(brand, "SelfSuspendedReasonType", 0);
        }

        /// <summary>
        /// Reactivate a members self suspended profile.
        /// </summary>
        /// <param name="brand">The current brand</param>
        /// <param name="memberId">The member id</param>
        public static void ReActivateSuspendedProfile(Brand brand, int memberId)
        {
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            member.SetAttributeInt(brand, "SelfSuspendedReasonType", 0);
            member.SetAttributeInt(brand, "SelfSuspendedFlag", 0);
            MemberSA.Instance.SaveMember(member);
        }

        /// <summary>
        /// Update the member's username.
        /// </summary>
        /// <param name="userName">The new username.</param>
        /// <param name="brand">The current brand</param>
        /// <param name="memberId">The member id</param>
        public static void UpdateUsername(string userName, Brand brand, int memberId)
        {
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            member.SetUsername(userName, brand);
            MemberSA.Instance.SaveMember(member, brand, true);
        }

        /// <summary>
        /// Updates the member's brand last logon date
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberId"></param>
        /// <param name="verifyLogonSiteOnly"></param>
        public static void UpdateBrandLastLogonDate(Brand brand, int memberId, bool verifyLogonSiteOnly)
        {
            var settingsManager = new SettingsManager();
            var logonOnlySite = verifyLogonSiteOnly && settingsManager.GetSettingBool(SettingConstants.LOGON_ONLY_SITE, brand);

            if (!logonOnlySite)
            {
                //if only a logonsite, no need to update profile properties because a member record doesn't exist
                UpdateProperties(brand, memberId, new Dictionary<string, object> { { "lastLoggedIn", DateTime.Now } }, null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="accessToken"></param>
        /// <param name="isRedesignBeta"></param>
        /// <param name="isRedesignBetaOffered"></param>
        public static void UpdateRedRedesignAttributes(Brand brand, TokenPairResponseV2 accessToken, string isRedesignBeta, string isRedesignBetaOffered)
        {
            var member = MemberSA.Instance.GetMember(accessToken.MemberId, MemberLoadFlags.None);
            //check the member. 
            if (null != member 
                && (member.GetAttributeBool(brand, "REDRedesignBetaOfferedFlag") ||
                member.GetAttributeBool(brand, "REDRedesignBetaParticipatingFlag")))
            {
                //1. if member was offered a choice of optin/out, use the participating value from db.
                //2. or if member was beta-ed from bedrock and hasn't opted out, continue to beta
            }
            else  //else use flags passed in from SUA to determine beta status 
            {

                Dictionary<string, object> attributeData = null;
                if (!string.IsNullOrEmpty(isRedesignBeta))
                {
                    if (null == attributeData) attributeData = new Dictionary<string, object>();
                    bool isParticipatingInRedesign = (Boolean.TrueString.ToLower() == isRedesignBeta.ToLower());
                    attributeData["REDRedesignBetaParticipatingFlag"] = isParticipatingInRedesign;
                }

                if (!string.IsNullOrEmpty(isRedesignBetaOffered))
                {
                    if (null == attributeData) attributeData = new Dictionary<string, object>();
                    bool isOfferedRedesign = (Boolean.TrueString.ToLower() == isRedesignBetaOffered.ToLower());
                    attributeData["REDRedesignBetaOfferedFlag"] = isOfferedRedesign;
                }

                //update the member with the correct values.
                if (null != attributeData)
                {
                    UpdateProperties(brand, accessToken.MemberId, attributeData, null);
                }
            }

            //update the access token with the correct values.
            member = MemberSA.Instance.GetMember(accessToken.MemberId, MemberLoadFlags.None);
            if (null != member)
            {
                accessToken.REDRedesignBetaParticipatingFlag = member.GetAttributeBool(brand, "REDRedesignBetaParticipatingFlag");
                accessToken.REDRedesignBetaOfferedFlag = member.GetAttributeBool(brand, "REDRedesignBetaOfferedFlag");
            }
        }

        /// <summary>
        /// Sets Ramah attributes for the first time
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="member"></param>
        /// <param name="attributeData"></param>
        /// <param name="attributeDataMultiValue"></param>
        internal static void SetRamahAttributes(Brand brand, Member member, Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue)
        {
            if (member.GetAttributeBool(brand,"RamahAlum") == false && (attributeDataMultiValue.ContainsKey("RamahCamp") || attributeData.ContainsKey("RamahStartYear") ||
                attributeData.ContainsKey("RamahEndYear")))
            {
                if (Enum.IsDefined(typeof(RamahAssociatedCommunity), brand.Site.Community.CommunityID) == false)
                    throw new Exception("Ramah attributes are not supported for this Community: "+brand.Site.Community.Name);
                else
                {
                    attributeData["RamahAlum"] = true;
                    attributeData["RamahInsertDate"] = DateTime.Now;
                }
            }
        }
    }
}
