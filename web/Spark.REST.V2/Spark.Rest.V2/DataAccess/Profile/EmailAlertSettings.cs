﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.DataAccess.Profile
{
    public class EmailAlertSettings
    {
        /// <summary>
        /// Daily = 1, Twice a week = 3, Once a week = 7, Never = 0
        /// </summary>
        public int SecretAdmirerEmailFrequency { get; set; }

        /// <summary>
        /// Notify me when someone I clicked "Yes" on responds by clicking "Yes" in return.
        /// </summary>
        public bool ShouldNotifySecretAdmirerAlerts { get; set; }
        
        /// <summary>
        /// Send my newest Matches directly to me.
        /// </summary>
        public bool ShouldNotifyMatches { get; set; }

        /// <summary>
        /// Notify me when someone adds me to their Hot List.
        /// </summary>
        public bool ShouldNotifyWhenAddedToHotlist { get; set; }

        /// <summary>
        /// Notify me when someone sends me a message on JDate
        /// </summary>
        public bool ShouldNotifyEmailAlert { get; set; }

        /// <summary>
        /// Notify me when someone sends me an E-card on JDate
        /// </summary>
        public bool ShouldNotifyEcard { get; set; }

        /// <summary>
        /// Notify me when someone views my profile (sent once a day).
        /// </summary>
        public bool ShouldNotifyWhenProfileViewed { get; set; }

        /// <summary>
        /// Notify me when someone wants to read more about me on my profile.
        /// </summary>
        public bool ShouldNotifyProfileEssayRequests { get; set; }
    }
}