﻿using Newtonsoft.Json;
using Spark.Rest.V2.BedrockPorts;

namespace Spark.Rest.V2.DataAccess.Profile
{
    /// <summary>
    /// Since the HideMask has an inverted usage from what is shown on the sites
    /// and apps this object will perform the bitwise operations on the underlying <see cref="AttributeOptionHideMask"/>
    /// </summary>
    public class DisplaySettings
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplaySettings"/> class.
        /// </summary>
        public DisplaySettings() {}

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplaySettings"/> class.
        /// </summary>
        /// <param name="hideMask">The hide mask.</param>
        public DisplaySettings(AttributeOptionHideMask hideMask)
        {
            HideMask = hideMask;
        }

        /// <summary>
        ///  Show/Hide When You're Online
        /// 
        /// Select true if you'd like members to know when you're logged into the site. 
        /// You'll show up in the Members Online section, and the "I'm online" button will flash in your profile.
        /// </summary>
        [JsonProperty(PropertyName = "online")]
        public bool Online
        {
            get { return !HideMask.HasFlag(AttributeOptionHideMask.HideMembersOnline); }
            set
            {
                if (value)
                {
                    HideMask &= ~AttributeOptionHideMask.HideMembersOnline;
                }
                else
                {
                    HideMask |= AttributeOptionHideMask.HideMembersOnline;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show or hide the profile in searches.
        /// </summary>
        /// <value>
        /// <c>true</c> if show, otherwise, <c>false</c>.
        /// </value>
        [JsonProperty(PropertyName = "showProfileInSearch")]
        public bool ShowProfileInSearch
        {
            get { return !HideMask.HasFlag(AttributeOptionHideMask.HideSearch); }
            set
            {
                if (value)
                {
                    HideMask &= ~AttributeOptionHideMask.HideSearch;
                }
                else
                {
                    HideMask |= AttributeOptionHideMask.HideSearch;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the member wants photos shown to non-members.
        /// </summary>
        /// <value>
        /// <c>true</c> if the member wants photos hidden from non-members; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty(PropertyName = "showPhotosToNonMembers")]
        public bool ShowPhotosToNonMembers
        {
            get { return !HideMask.HasFlag(AttributeOptionHideMask.HidePhotos); }
            set
            {
                if (value)
                {
                    HideMask &= ~AttributeOptionHideMask.HidePhotos;
                }
                else
                {
                    HideMask |= AttributeOptionHideMask.HidePhotos;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the member wants let other members
        /// know when they view or hotlist another member.
        /// </summary>
        /// <value>
        /// <c>true</c> if the member wants to show when they view or hotlist; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty(PropertyName = "showWhenViewOrHotlist")]
        public bool ShowWhenViewOrHotlist
        {
            get { return !HideMask.HasFlag(AttributeOptionHideMask.HideHotLists); }
            set
            {
                if (value)
                {
                    HideMask &= ~AttributeOptionHideMask.HideHotLists;
                }
                else
                {
                    HideMask |= AttributeOptionHideMask.HideHotLists;
                }
            }
        }

        /// <summary>
        /// The HideMask being sent to or retrieved from the underlying system.
        /// </summary>
        /// <value>
        /// The hide mask.
        /// </value>
        [JsonIgnore]
        public AttributeOptionHideMask HideMask { get; private set; }
    }
}
