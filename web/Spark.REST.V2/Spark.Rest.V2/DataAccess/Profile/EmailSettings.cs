﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.DataAccess.Profile
{
    public class EmailSettings
    {
        /// <summary>
        /// Emails Settings and Alerts
        /// </summary>
        public EmailAlertSettings EmailAlertSettings { get; set; }

        /// <summary>
        /// Message Settings
        /// </summary>
        public MessageSettings MessageSettings { get; set; }

        /// <summary>
        /// Newsletters, Events and Offers 
        /// </summary>
        public NewsLetterSettings NewsLetterSettings { get; set; }
    }
}