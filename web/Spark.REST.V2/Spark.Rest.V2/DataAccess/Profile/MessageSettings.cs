﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.DataAccess.Profile
{
    public class MessageSettings
    {
        /// <summary>
        /// Reply settings: Include original messages in replies
        /// </summary>
        public bool ShouldIncludeOriginalInReplies { get; set; }

        /// <summary>
        /// Save sent messages: Save copies of sent messages
        /// </summary>
        public bool ShouldSaveSentCopies { get; set; }
        
        /// <summary>
        ///  I'm taking a break... Let us reply on your behalf with the following message:
        /// "I am taking a break at the moment and will respond when I return."
        /// </summary>
        public bool ShouldReplyOnMyBehalf { get; set; }
    }
}