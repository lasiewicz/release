﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.DataAccess.Profile
{
    public class NewsLetterSettings
    {
        /// <summary>
        /// Invitations by Email: Send me invitations to exciting events, parties, and other great date ideas. 
        /// </summary>
        public bool ShouldSendEventsInvitations { get; set; }

        /// <summary>
        /// Newsletters and Offers: Send me dating tips and advice, qualified partner offers and exclusive JDate announcements. 
        /// </summary>
        public bool? ShouldSendDatingTips { get; set; }

        // Allows separation of Newsletters and Offers, these will be used if ShouldSendDatingTips is null (which is left in for backwards compatibility)
        public bool ShouldSendNewsletters { get; set; }
        public bool ShouldSendOffers { get; set; }
    }
}