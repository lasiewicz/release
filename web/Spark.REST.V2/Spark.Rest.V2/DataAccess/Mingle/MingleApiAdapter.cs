﻿using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Newtonsoft.Json;
using RestSharp;
using Spark.Rest.V2.Models.InstantMessenger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace Spark.Rest.V2.DataAccess.Mingle
{
    public class MingleApiAdapter
    {
        #region Singleton manager
        private static readonly MingleApiAdapter _Instance = new MingleApiAdapter();
        private MingleApiAdapter()
        {
            if (ConfigurationManager.AppSettings["Environment"] != "prod")
            {
                //utah api uses ssl and may be self-signed
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            }

        }
        public static MingleApiAdapter Instance
        {
            get { return _Instance; }
        }

        #endregion

        public string CreateConversation(InstantMessengerCreateConversationRequest request)
        {
            string jsonBody = JsonConvert.SerializeObject(new { to_memberid = request.to_memberid });
            var restClient = new RestClient(GetMingleApiBaseURL(request.Brand));
            var restRequest = new RestRequest("{siteid}/chat/createConversation", Method.POST);
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddParameter("siteid", request.Brand.Site.SiteID, ParameterType.UrlSegment);
            restRequest.AddParameter("access_token", request.access_token, ParameterType.QueryString);
            restRequest.AddParameter("application/json", jsonBody, ParameterType.RequestBody);

            var response = restClient.Execute(restRequest);

            return response.Content;
        }

        public string RecordInviteSent(InstantMessengerRecordInviteSentRequest request)
        {
            var restClient = new RestClient(GetMingleApiBaseURL(request.Brand));
            var restRequest = new RestRequest("{siteid}/chat/recordInviteSent/{roomid}/{to_memberid}", Method.GET);
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddParameter("siteid", request.Brand.Site.SiteID, ParameterType.UrlSegment);
            restRequest.AddParameter("roomid", request.roomid, ParameterType.UrlSegment);
            restRequest.AddParameter("to_memberid", request.to_memberid, ParameterType.UrlSegment);
            restRequest.AddParameter("access_token", request.access_token, ParameterType.QueryString);

            var response = restClient.Execute(restRequest);

            return response.Content;
        }

        public string ValidateInvite(InstantMessengerValidateInviteRequest request)
        {
            var restClient = new RestClient(GetMingleApiBaseURL(request.Brand));
            var restRequest = new RestRequest("{siteid}/chat/validateInvite/{roomid}/{to_memberid}/{from_memberid}", Method.GET);
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddParameter("siteid", request.Brand.Site.SiteID, ParameterType.UrlSegment);
            restRequest.AddParameter("roomid", request.roomid, ParameterType.UrlSegment);
            restRequest.AddParameter("to_memberid", request.to_memberid, ParameterType.UrlSegment);
            restRequest.AddParameter("from_memberid", request.from_memberid, ParameterType.UrlSegment);
            restRequest.AddParameter("access_token", request.access_token, ParameterType.QueryString);

            var response = restClient.Execute(restRequest);

            return response.Content;
        }

        public string GetIMInfo(InstantMessengerGetImInfoRequest request)
        {
            var restClient = new RestClient(GetMingleApiBaseURL(request.Brand));
            var restRequest = new RestRequest("{siteid}/chat/getIMInfo", Method.GET);
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddParameter("siteid", request.Brand.Site.SiteID, ParameterType.UrlSegment);
            restRequest.AddParameter("access_token", request.access_token, ParameterType.QueryString);
            restRequest.AddParameter("imid", request.imid, ParameterType.QueryString);

            var response = restClient.Execute(restRequest);

            return response.Content;
        }


        #region Mingle API Settings
        public static string GetMingleApiAppId(Brand brand)
        {
            try
            {
                return RuntimeSettings.Instance.GetSettingFromSingleton("UTAH_API_APPID", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //probably missing setting
                return "450758";
            }
        }

        public static string GetMingleApiClientSecret(Brand brand)
        {
            try
            {
                return RuntimeSettings.Instance.GetSettingFromSingleton("UTAH_API_CLIENTSECRET", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //probably missing setting
                return "RvWytIEO6lZmX7hrp1LK25UM2e";
            }
        }

        public static string GetMingleApiBaseURL(Brand brand)
        {
            try
            {
                return RuntimeSettings.Instance.GetSettingFromSingleton("UTAH_API_BASEURL", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //probably missing setting
                return "https://api.securemingle.com/v3";
            }
        }

        #endregion
    }
}