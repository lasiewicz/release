﻿#region

using System;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.REST.Models.Session;

#endregion

namespace Spark.REST.DataAccess.Session
{
    public class SessionTrackingDataAccess
    {
        public static readonly SessionTrackingDataAccess Instance = new SessionTrackingDataAccess();
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(SessionTrackingDataAccess));

        private SessionTrackingDataAccess()
        {
        }

        public CreateSessionTrackingResponse CreateSessionTracking(
            CreateSessionTrackingRequest createSessionTrackingRequest)
        {
            var createSessionTrackingResponse = new CreateSessionTrackingResponse();
            var sessionTrackingID = 0;

            createSessionTrackingResponse.SessionTrackingID = sessionTrackingID;

            try
            {
                var session = new UserSession(new Guid(createSessionTrackingRequest.SessionID));
                sessionTrackingID = SessionSA.Instance.GetNextSessionTrackingID(session);

                session.Add("SessionTrackingID", sessionTrackingID, SessionPropertyLifetime.Temporary);
                session.Add("SiteID", createSessionTrackingRequest.Brand.Site.SiteID, SessionPropertyLifetime.Temporary);
                session.Add("ClientIP", createSessionTrackingRequest.ClientIP, SessionPropertyLifetime.Temporary);

                if (createSessionTrackingRequest.PromotionID > 0)
                {
                    session.Add("PromotionID", createSessionTrackingRequest.PromotionID,
                                SessionPropertyLifetime.Temporary);
                }
                if (!string.IsNullOrEmpty(createSessionTrackingRequest.LuggageID))
                {
                    session.Add("Luggage", createSessionTrackingRequest.LuggageID, SessionPropertyLifetime.Temporary);
                }
                if (createSessionTrackingRequest.BannerID > 0)
                {
                    session.Add("BannerID_Tracking", createSessionTrackingRequest.BannerID,
                                SessionPropertyLifetime.Temporary);
                }
                if (!string.IsNullOrEmpty(createSessionTrackingRequest.ReferrerURL))
                {
                    session.Add("ReferralURL", createSessionTrackingRequest.ReferrerURL,
                                SessionPropertyLifetime.Temporary);
                }
                if (!string.IsNullOrEmpty(createSessionTrackingRequest.LandingPageID ))
                {
                    session.Add("LandingPageID", createSessionTrackingRequest.LandingPageID,
                                SessionPropertyLifetime.Temporary);
                }
                if (!string.IsNullOrEmpty(createSessionTrackingRequest.LandingPageTestID ))
                {
                    session.Add("LandingPageTestID", createSessionTrackingRequest.LandingPageTestID,
                                SessionPropertyLifetime.Temporary);
                }

                SessionSA.Instance.SaveSessionDetails(session, createSessionTrackingRequest.Brand.Site.SiteID,
                                                      SessionType.Visitor);
                createSessionTrackingResponse.SessionTrackingID = sessionTrackingID;
            }
            catch (Exception ex)
            {
                Log.LogError("Error creating session tracking. SessionID: " + createSessionTrackingRequest.SessionID, ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(createSessionTrackingRequest.Brand));
            }

            return createSessionTrackingResponse;
        }

        public void UpdateSessionTrackingForRegistration(string sessionID, int sessionTrackingID, int siteID,
                                                         int memberID)
        {
            try
            {
                var session = new UserSession(new Guid(sessionID));

                session.Add("SessionTrackingID", sessionTrackingID, SessionPropertyLifetime.Temporary);
                session.Add("SiteID", siteID, SessionPropertyLifetime.Temporary);
                session.Add("MemberID", memberID, SessionPropertyLifetime.Temporary);
                session.Add("RegistrationDate", DateTime.Now, SessionPropertyLifetime.Temporary);

                SessionSA.Instance.SaveSessionDetails(session, siteID, SessionType.Member);
            }
            catch (Exception ex)
            {
                Log.LogError("Error updating session tracking. SessionID: " + sessionID, ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
            }
        }
    }
}