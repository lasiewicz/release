﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects;
using Spark.API.ServiceAdapters;
using Spark.API.ValueObjects;
using Spark.Common.Localization;
using Spark.Common.Security;
using Spark.Logger;
using Spark.LoginFraud;
using Spark.Managers.Managers;
using Spark.RabbitMQ.Client;
using Spark.Rest.DataAccess.Purchase;
using Spark.Rest.DataAccess.Subscription;
using Spark.Rest.Entities.Applications;
using Spark.Rest.Helpers;
using Spark.Rest.V2.BedrockPorts;
using Spark.Rest.V2.DataAccess;
using Spark.Rest.V2.Entities.Applications;
using Spark.Rest.V2.Helpers;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.Helpers;

#endregion

namespace Spark.REST.DataAccess.Applications
{
    /// <summary>
    /// </summary>
    public enum ApplicationResultType
    {
        /// <summary>
        /// </summary>
        TokenPairResponse = 1,

        /// <summary>
        /// </summary>
        TokenPairResponseV2 = 2
    }

    internal static class ApplicationAccess
    {
        // version history
        // 1 - initial version
        // 2 - added version to be int 32 from int16
        // 3 - changes to make token url safe
        // 4 - added AdminMemberID
        private const int CurrentTokenVersion =4;
        private const int UrlSafeTokenVersion = 3;
        
        private const string ApplicationResourceFormat = "http://{0}/apps/application/{1}?client_id={2}&client_secret={3}";

        private static readonly TimeSpan AccessTokenLifetime;
        private static readonly TimeSpan AccessTokenLongLifetime;
        private static readonly TimeSpan RefreshTokenLifetime;
        private static Publisher _publisher;
        private static readonly object LockObject = new object();
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof (ApplicationAccess));
        private static IGetMember _getMemberService = null;
        private static ISaveMember _saveMemberService = null;
        private static ISettingsSA _settingsService = null;

        public static IGetMember GetMemberService
        {
            get { return _getMemberService ?? MemberSA.Instance; }
            set { _getMemberService = value; }
        }

        public static ISaveMember SaveMemberService
        {
            get { return _saveMemberService ?? MemberSA.Instance; }
            set { _saveMemberService = value; }
        }

        public static ISettingsSA SettingsService
        {
            get { return _settingsService ?? RuntimeSettings.Instance; }
            set { _settingsService = value; }
        }


        static ApplicationAccess()
        {
            try
            {
                var lifeTimeString = ConfigurationManager.AppSettings["AccessTokenLifetimeMins"];
                var lifetimeMins = Int32.Parse(lifeTimeString);
                AccessTokenLifetime = new TimeSpan(0, lifetimeMins, 0);
                lifeTimeString = ConfigurationManager.AppSettings["AccessTokenLongLifetimeMins"];
                lifetimeMins = Int32.Parse(lifeTimeString);
                AccessTokenLongLifetime = new TimeSpan(0, lifetimeMins, 0);
                lifeTimeString = ConfigurationManager.AppSettings["RefreshTokenLifetimeMins"];
                lifetimeMins = Int32.Parse(lifeTimeString);
                RefreshTokenLifetime = new TimeSpan(0, lifetimeMins, 0);
            }
            catch (Exception ex)
            {
                Log.LogError("failed to read token AccessTokenLifetimeMins, RefreshTokenLifetimeMins settings from webconfig", ex,
                    ErrorHelper.GetCustomData(),ErrorHelper.ShouldLogExternally());
                throw;
            }
        }

        public static Publisher Publisher
        {
            get
            {
                if (null == _publisher)
                {
                    lock (LockObject)
                    {
                        if (null == _publisher)
                        {
                            SetupRabbitMqPublisher();
                        }
                    }
                }
                return _publisher;
            }
            set { _publisher = value; }
        }

        private static void SetupRabbitMqPublisher()
        {
            var rabbitMqUserName = SettingsService.GetSettingFromSingleton(SettingConstants.RABBITMQ_USER_ACCOUNT);
            var rabbitMqPassword = SettingsService.GetSettingFromSingleton(SettingConstants.RABBITMQ_PASSWORD);
            var rabbitMqServer = SettingsService.GetSettingFromSingleton(SettingConstants.ACTIVITYRECORDING_PROCESSOR_RABBITMQ_SERVER);
            _publisher = new Publisher(rabbitMqServer, rabbitMqUserName, rabbitMqPassword);
        }

        internal static TokenPair GetAccessRefreshToken(int applicationId, int memberId)
        {
            // too noisy
            //Log.DebugFormat("Getting app member for app {0}, member {1}", applicationId, memberId);
            var existingMember = APISA.Instance.GetAppMember(applicationId, memberId);
            if (existingMember == null)
            {
                Log.LogDebugMessage(string.Format("no app member found for app id {0}, member id {1}", applicationId, memberId), ErrorHelper.GetCustomData());
                return null;
            }
            if (String.IsNullOrEmpty(existingMember.AccessToken))
            {
                Log.LogDebugMessage(string.Format("null or empty access token for app id {0}, member id {1}", applicationId, memberId), ErrorHelper.GetCustomData());
            }
            if (String.IsNullOrEmpty(existingMember.RefreshToken))
            {
                Log.LogDebugMessage(string.Format("null or empty refresh token for app id {0}, member id {1}", applicationId, memberId), ErrorHelper.GetCustomData());
            }

            var tokenPair = new TokenPair
            {
                AccessToken = new TokenWithExpiration
                {
                    Token = existingMember.AccessToken,
                    ExpiresDateTime = existingMember.AccessTokenExpirationDateTime
                },
                RefreshToken = new TokenWithExpiration
                {
                    Token = existingMember.RefreshToken,
                    ExpiresDateTime =
                        existingMember.RefreshTokenExpirationDateTime
                }
            };
            return tokenPair;
        }

        internal static CreateCredentialsResponse CreateLogonCredentialsAndReturnTokens(Brand brand, int applicationId, string email, string password, string userName, string tokenLifeType)
        {
            if (applicationId < 1)
            {
                return new CreateCredentialsResponse
                {
                    ResponseCode = (int) HttpStatusCode.BadRequest,
                    ResponseSubCode = (int) HttpSub400StatusCode.MissingApplicationId,
                    Error = "Application ID is required. "
                };
            }

            if (String.IsNullOrEmpty(email))
            {
                return new CreateCredentialsResponse
                {
                    ResponseCode = (int) HttpStatusCode.BadRequest,
                    ResponseSubCode = (int) HttpSub400StatusCode.MissingEmail,
                    Error = "Email is a required field."
                };
            }

            if (email.Length > Matchnet.Member.ValueObjects.ServiceConstants.EMAIL_ADDRESS_MAX_LENGTH)
            {
                return new CreateCredentialsResponse
                {
                    ResponseCode = (int) HttpStatusCode.BadRequest,
                    ResponseSubCode = (int) HttpSub400StatusCode.MissingEmail,
                    Error =
                        string.Format("Email must be no more than {0} characters.",
                            Matchnet.Member.ValueObjects.ServiceConstants.EMAIL_ADDRESS_MAX_LENGTH)
                };
            }

            if (String.IsNullOrEmpty(password))
            {
                return new CreateCredentialsResponse
                {
                    ResponseCode = (int) HttpStatusCode.BadRequest,
                    ResponseSubCode = (int) HttpSub400StatusCode.MissingPassword,
                    Error = "Password is required."
                };
            }

            if (string.IsNullOrEmpty(userName))
            {
                return new CreateCredentialsResponse
                {
                    ResponseCode = (int) HttpStatusCode.BadRequest,
                    ResponseSubCode = (int) HttpSub400StatusCode.InvalidUserName,
                    Error = "Username is required."
                };
            }

            if (userName.Length > Matchnet.Member.ValueObjects.ServiceConstants.USERNAME_MAX_LENGTH)
            {
                return new CreateCredentialsResponse
                {
                    ResponseCode = (int) HttpStatusCode.BadRequest,
                    ResponseSubCode = (int) HttpSub400StatusCode.InvalidUserName,
                    Error =
                        string.Format("Username must be no more than {0} characters.",
                            Matchnet.Member.ValueObjects.ServiceConstants.USERNAME_MAX_LENGTH)
                };
            }

            var app = GetApplication(applicationId);
            if (app == null)
            {
                return new CreateCredentialsResponse
                {
                    ResponseCode = (int) HttpStatusCode.BadRequest,
                    ResponseSubCode = (int) HttpSub400StatusCode.InvalidApplicationId,
                    Error = "Invalid applicationId."
                };
            }

            CreateLogonCredentialsResult credentialsResult;

            try
            {
                credentialsResult = MemberSA.Instance.CreateLogonCredentials(email, userName, password, brand);
            }
            catch (Exception ex)
            {
                Log.LogError("Exception creating logon credentials with MemberSA using email and password",ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));

                return new CreateCredentialsResponse
                {
                    ResponseCode = (int) HttpStatusCode.Unauthorized,
                    ResponseSubCode = (int) HttpSub401StatusCode.InvalidEmailOrPassword,
                    Error = "Error validating user by email and password"
                };
            }

            if (credentialsResult == null)
            {
                return new CreateCredentialsResponse
                {
                    ResponseCode = (int) HttpStatusCode.Unauthorized,
                    ResponseSubCode = (int) HttpSub401StatusCode.InvalidEmailOrPassword,
                    Error = "Error creating logon credentials"
                };
            }

            if (credentialsResult.Status != LogonCredentialsStatus.Success)
            {
                HttpSub401StatusCode subStatusCode;
                string errorMessage;

                switch (credentialsResult.Status)
                {
                    case LogonCredentialsStatus.PasswordLengthExceeded:
                        subStatusCode = HttpSub401StatusCode.InvalidEmailOrPassword;
                        errorMessage = "Password was too long";
                        break;
                    case LogonCredentialsStatus.EmailAddressNotAllowed:
                    case LogonCredentialsStatus.EmailAddressBlocked:
                    case LogonCredentialsStatus.InvalidEmail:
                        subStatusCode = HttpSub401StatusCode.InvalidEmailOrPassword;
                        errorMessage = "There was a problem with the email address";
                        break;
                    case LogonCredentialsStatus.AlreadyRegistered:
                        subStatusCode = HttpSub401StatusCode.CredentialsAlreadyCreated;
                        errorMessage = "User already registered";
                        break;
                    default:
                        subStatusCode = HttpSub401StatusCode.InvalidEmailOrPassword;
                        errorMessage = "Could not create logon credentials";
                        break;
                }

                return new CreateCredentialsResponse
                {
                    ResponseCode = (int) HttpStatusCode.Unauthorized,
                    ResponseSubCode = (int) subStatusCode,
                    Error = errorMessage
                };
            }

            var synchMinglePassword = Convert.ToBoolean(SettingsService.GetSettingFromSingleton(SettingConstants.MINGLE_PW_SYNCH_ENABLED, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
            if (synchMinglePassword)
            {
                SynchPasswordToMingle(credentialsResult.MemberId, brand.BrandID, email, password);
            }

            var tokenTrusted = TokenPairResponseV2.Parse(CreateAccessTokenTrusted(brand, applicationId, credentialsResult.MemberId,
                    tokenLifeType));
            var response = CreateCredentialsResponse.Parse(tokenTrusted);
            response.UserName = userName;
            response.MemberId = credentialsResult.MemberId;

            return response;
        }

        internal static Task<object> GetOrCreateAccessTokenUsingPasswordAsync(Brand brand, int applicationId, string email,
            string password, bool passwordIsEncrypted, ApplicationResultType resultType, string tokenLifeType,
            bool logonOnlySite)
        {
            return GetOrCreateAccessTokenUsingPasswordAsync(brand, applicationId, email, password, passwordIsEncrypted,
                resultType, tokenLifeType, logonOnlySite, string.Empty,
                string.Empty, string.Empty, string.Empty);
        }

        internal async static Task<object> GetOrCreateAccessTokenUsingPasswordAsync(Brand brand, int applicationId, string email,
            string password, bool passwordIsEncrypted, ApplicationResultType resultType, string tokenLifeType,
            bool logonOnlySite, string ipAddress, string userAgent, string httpHeaders, string loginSessionId)
        {
            var settingsManager = new SettingsManager();
            var logTimingData = settingsManager.GetSettingBool(SettingConstants.LOG_LOGIN_TIMING_DATA, brand);

            Log.LogDebugMessage(string.Format("Entered GetOrCreateAccessTokenUsingPasswordAsync EmailAddress: {0} BrandId: {1}", email, brand.BrandID), ErrorHelper.GetCustomData());

            Stopwatch stopwatch = null;
            long lastElapsedMilliseconds = 0;

            if (logTimingData)
            {
                stopwatch = new Stopwatch();
                stopwatch.Start();
            }
            try
            {
                var validInput = true;
                var reason = String.Empty;

                if (applicationId < 1)
                {
                    reason += "Application ID is required.  ";
                    validInput = false;
                    if (resultType == ApplicationResultType.TokenPairResponseV2)
                    {
                        return new TokenPairResponseV2
                        {
                            ResponseCode = (int) HttpStatusCode.BadRequest,
                            ResponseSubCode = (int) HttpSub400StatusCode.MissingApplicationId,
                            Error = reason
                        };
                    }
                }

                if (String.IsNullOrEmpty(email))
                {
                    reason += "Email is a required field.  ";
                    validInput = false;
                    if (resultType == ApplicationResultType.TokenPairResponseV2)
                    {
                        return new TokenPairResponseV2
                        {
                            ResponseCode = (int) HttpStatusCode.BadRequest,
                            ResponseSubCode = (int) HttpSub400StatusCode.MissingEmail,
                            Error = reason
                        };
                    }
                }
                if (String.IsNullOrEmpty(password))
                {
                    reason += "password is required.  ";
                    validInput = false;
                    if (resultType == ApplicationResultType.TokenPairResponseV2)
                    {
                        return new TokenPairResponseV2
                        {
                            ResponseCode = (int) HttpStatusCode.BadRequest,
                            ResponseSubCode = (int) HttpSub400StatusCode.MissingPassword,
                            Error = reason
                        };
                    }
                }

                if (!validInput)
                {
                    return new TokenPairResponse {Error = reason};
                }
                var hackAppIdToAvoidLogLastLogon =
                    Convert.ToInt32(ConfigurationManager.AppSettings["hackAppIdToAvoidLogLastLogon"]);
                var logLastLogon = applicationId != hackAppIdToAvoidLogLastLogon;

                Log.LogDebugMessage(string.Format("ApplicationId : {0} Email : {1} LogLastLogon : {2} HackAppId:{3}", applicationId, email,
                    logLastLogon, hackAppIdToAvoidLogLastLogon), ErrorHelper.GetCustomData());
                var app = GetApplication(applicationId);
                if (app == null)
                {
                    if (resultType == ApplicationResultType.TokenPairResponseV2)
                    {
                        return new TokenPairResponseV2
                        {
                            ResponseCode = (int) HttpStatusCode.BadRequest,
                            ResponseSubCode = (int) HttpSub400StatusCode.InvalidApplicationId,
                            Error = reason
                        };
                    }
                    return new TokenPairResponse {Error = String.Format("Invalid application ID {0}", applicationId)};
                }

                if (logTimingData)
                {
                    lastElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
                    Log.LogDebugMessage(string.Format("GetOrCreateAccessTokenUsingPasswordAsync Timing EmailAddress: {0} BrandId: {1} Event: FinishedSetup ElapsedMilliseconds: {2} ",
                        email, brand.BrandID, lastElapsedMilliseconds), ErrorHelper.GetCustomData());
                }

                AuthenticationResult authResult;
                long currentElapsedMilliseconds;
                try
                {
                    #region Fraud check

                    //first call login fraud if enabled and parameters supplied
                    var fraudCheckResponse = new FraudCheckResponse {ValidResult = false};
                    var loginFraudHelper = new LoginFraudHelper(brand);

                    if (loginFraudHelper.IsLoginFraudEnabled(ipAddress))
                    {
                        fraudCheckResponse = await loginFraudHelper.CheckFraudAsync(email, ipAddress, userAgent, httpHeaders,
                            applicationId, brand.BrandID, loginSessionId);

                        if (logTimingData)
                        {
                            currentElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
                            Log.LogDebugMessage(string.Format("GetOrCreateAccessTokenUsingPasswordAsync Timing EmailAddress: {0} BrandId: {1} Event: CalledCheckFraud TotalElapsedMilliseconds: {2} LastOpElapsedMilliseconds: {3}",
                                email, brand.BrandID, currentElapsedMilliseconds,
                                (currentElapsedMilliseconds - lastElapsedMilliseconds)),ErrorHelper.GetCustomData());
                            lastElapsedMilliseconds = currentElapsedMilliseconds;
                        }

                        if (fraudCheckResponse.ValidResult && fraudCheckResponse.ActionId != FraudActionType.NotFraud)
                        {
                            Log.LogDebugMessage(string.Format("Failed login fraud - Email: {0} fraud action: {1} fraud step:{2} StatusCode: {3} TimedOut: {4}",
                                email, fraudCheckResponse.ActionId, fraudCheckResponse.StepId, fraudCheckResponse.StatusCode, fraudCheckResponse.TimedOut),ErrorHelper.GetCustomData());

                            const string error = "Error validating user by email and password";
                            if (resultType == ApplicationResultType.TokenPairResponseV2)
                            {
                                return new TokenPairResponseV2
                                {
                                    ResponseCode = (int) HttpStatusCode.Unauthorized,
                                    ResponseSubCode = (int) HttpSub401StatusCode.InvalidEmailOrPassword,
                                    Error = error
                                };
                            }
                            return new TokenPairResponse {Error = error};
                        }
                    }

                #endregion

                    if (logonOnlySite)
                    {
                        authResult = MemberSA.Instance.BasicAuthenticate(email, password,
                            brand.Site.Community.CommunityID);
                    }
                    else
                    {
                        authResult = passwordIsEncrypted
                            ? MemberSA.Instance.Authenticate(brand, email, null, password, logLastLogon)
                            : MemberSA.Instance.Authenticate(brand, email, password, Constants.NULL_STRING,
                                logLastLogon);
                    }

                    ActivityRecordingAccess.Instance.RecordAuthenticationAttemptActivity(loginSessionId, email,
                        authResult.MemberID,
                        brand.BrandID,
                        authResult.Status);

                    if (loginFraudHelper.IsLoginFraudEnabled(ipAddress) && fraudCheckResponse.ValidResult)
                    {
                        var memberid = authResult.MemberID != Constants.NULL_INT ? authResult.MemberID : 0;
                        loginFraudHelper.UpdateLog(email, fraudCheckResponse.LogId, ipAddress, memberid,
                            authResult.Status);
                    }
                }
                catch (Exception ex)
                {
                    Log.LogError("Exception authenticating with MemberSA using email and password", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                    const string error = "Error validating user by email and password";
                    if (resultType == ApplicationResultType.TokenPairResponseV2)
                    {
                        return new TokenPairResponseV2
                        {
                            ResponseCode = (int) HttpStatusCode.Unauthorized,
                            ResponseSubCode = (int) HttpSub401StatusCode.InvalidEmailOrPassword,
                            Error = error
                        };
                    }
                    return new TokenPairResponse {Error = error};
                }
                finally
                {
                    if (logTimingData)
                    {
                        currentElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
                        Log.LogDebugMessage(string.Format("GetOrCreateAccessTokenUsingPasswordAsync Timing EmailAddress: {0} BrandId: {1} Event: FinishedAuth TotalElapsedMilliseconds: {2} LastOpElapsedMilliseconds: {3}",
                            email, brand.BrandID, currentElapsedMilliseconds, (currentElapsedMilliseconds - lastElapsedMilliseconds)), ErrorHelper.GetCustomData());
                        lastElapsedMilliseconds = currentElapsedMilliseconds;
                    }
                }

                #region Invalid authentication

                if (authResult.Status != AuthenticationStatus.Authenticated)
                {
                    if (resultType != ApplicationResultType.TokenPairResponseV2)
                        return new TokenPairResponse
                        {
                            Error = authResult.Status.ToString(),
                        };
                    var subStatusCode = (authResult.Status == AuthenticationStatus.AdminSuspended)
                        ? HttpSub401StatusCode.MemberSuspended
                        : HttpSub401StatusCode.InvalidEmailOrPassword;

                    return new TokenPairResponseV2
                    {
                        ResponseCode = (int) HttpStatusCode.Unauthorized,
                        ResponseSubCode = (int) subStatusCode,
                        Error = authResult.Status.ToString()
                    };
                }

                #endregion

                var tokens = GetAccessRefreshToken(applicationId, authResult.MemberID);
                if (tokens != null)
                {
                    if (String.IsNullOrEmpty(tokens.RefreshToken.Token) ||
                        tokens.RefreshToken.ExpiresDateTime.AddMinutes(-1) < DateTime.Now)
                    {
                        var createAccessTokenUsingPassword = CreateAccessTokenTrusted(brand, applicationId,
                            authResult.MemberID, tokenLifeType);
                        return resultType == ApplicationResultType.TokenPairResponseV2
                            ? TokenPairResponseV2.Parse(createAccessTokenUsingPassword)
                            : createAccessTokenUsingPassword;
                    }
                    if (String.IsNullOrEmpty(tokens.AccessToken.Token) ||
                        tokens.AccessToken.ExpiresDateTime.AddMinutes(-1) < DateTime.Now)
                    {
                        object createAccessTokenRefresh = GetOrCreateAccessTokenRefresh(brand, applicationId,
                            authResult.MemberID,
                            tokens.RefreshToken.Token,
                            resultType);
                        return createAccessTokenRefresh; // access token expired, create new access token
                    }

                    #region Retrieve subscription information, subscription check, IAP check, IAB check.
                    // todo: Are we paying a price by pulling all this information during logon for the sake of convenience?

                    var isPayingMember = SubscriptionStatus.MemberIsSubscriber(brand, authResult.MemberID);
                    var member = MemberSA.Instance.GetMember(authResult.MemberID, MemberLoadFlags.None);
                    // wrap get attribute in try/catch block to deal with attribute group problems 
                    string iapOrigTransId;
                    try
                    {
                        iapOrigTransId = member.GetAttributeText(brand, PurchaseAccess.IOS_IAP_RECEIPT_ORIG_TRANS_ID_ATTR);
                    }
                    catch (Exception e)
                    {
                        Log.LogError(string.Format("Could not find attribute:'{0}' for memberId:{1}", PurchaseAccess.IOS_IAP_RECEIPT_ORIG_TRANS_ID_ATTR, authResult.MemberID), e, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                        iapOrigTransId = string.Empty;
                    }

                    string googlePlaySubToken;
                    try
                    {
                        googlePlaySubToken = member.GetAttributeText(brand, PurchaseAccess.ANDROID_IAB_SUBSCRIPTION_TOKENID_ATTR);
                    }
                    catch (Exception e)
                    {
                        Log.LogError(string.Format("Could not find attribute:'{0}' for memberId:{1}", PurchaseAccess.ANDROID_IAB_SUBSCRIPTION_TOKENID_ATTR, authResult.MemberID), e, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                        googlePlaySubToken = string.Empty;
                    } 

                    bool isIAPSub = isPayingMember && !string.IsNullOrEmpty(iapOrigTransId);
                    bool isIABSub = isPayingMember && !string.IsNullOrEmpty(googlePlaySubToken);

                    bool isParticipatingInRedesign = member.GetAttributeBool(brand, "REDRedesignBetaParticipatingFlag");
                    bool isOfferedRedesign = member.GetAttributeBool(brand, "REDRedesignBetaOfferedFlag");

                    var accessTokenUsingPassword = new TokenPairResponse(tokens)
                    {
                        MemberId = authResult.MemberID,
                        IsPayingMember = isPayingMember,
                        IsIAPPayingMember = isIAPSub,
                        IsIABPayingMember = isIABSub,
                        IsSelfSuspended = MemberHelper.IsMemberSelfSuspended(brand, member),
                        REDRedesignBetaParticipatingFlag = isParticipatingInRedesign,
                        REDRedesignBetaOfferedFlag = isOfferedRedesign
                    };

                    #endregion

                    if (logTimingData)
                    {
                        currentElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
                        Log.LogDebugMessage(string.Format("GetOrCreateAccessTokenUsingPasswordAsync Timing EmailAddress: {0} BrandId: {1} Event: CreatedTokens TotalElapsedMilliseconds: {2} LastOpElapsedMilliseconds: {3}",
                            email, brand.BrandID, currentElapsedMilliseconds, (currentElapsedMilliseconds - lastElapsedMilliseconds)), ErrorHelper.GetCustomData());
                    }

                    if (resultType != ApplicationResultType.TokenPairResponseV2) return accessTokenUsingPassword;

                    if (!(accessTokenUsingPassword is TokenPairResponseV2))
                    {
                        return TokenPairResponseV2.Parse(accessTokenUsingPassword);
                    }
                    return accessTokenUsingPassword;
                }

                var tokenTrusted = CreateAccessTokenTrusted(brand, applicationId, authResult.MemberID, tokenLifeType);

                if (!logTimingData)
                    return resultType == ApplicationResultType.TokenPairResponseV2
                        ? TokenPairResponseV2.Parse(tokenTrusted)
                        : tokenTrusted;
                currentElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
                Log.LogDebugMessage(string.Format("GetOrCreateAccessTokenUsingPasswordAsync Timing EmailAddress: {0} BrandId: {1} Event: CreatedTokens TotalElapsedMilliseconds: {2} LastOpElapsedMilliseconds: {3}",
                    email, brand.BrandID, currentElapsedMilliseconds, (currentElapsedMilliseconds - lastElapsedMilliseconds)), ErrorHelper.GetCustomData());

                return resultType == ApplicationResultType.TokenPairResponseV2
                    ? TokenPairResponseV2.Parse(tokenTrusted)
                    : tokenTrusted;
            }
            finally
            {
                if (stopwatch != null) stopwatch.Stop();
            }
        }

        internal static TokenPairResponse GetOrCreateAccessTokenRefresh(Brand brand, int applicationId, int memberId,
            string refreshToken)
        {
            return GetOrCreateAccessTokenRefresh(brand, applicationId, memberId, refreshToken, ApplicationResultType.TokenPairResponseV2);
        }

        internal static TokenPairResponse GetOrCreateAccessTokenRefresh(Brand brand, int applicationId, int memberId,
            string refreshToken, ApplicationResultType resultType)
        {
            //Log.DebugFormat("Getting app with id {0}", applicationId);
            var app = GetApplication(applicationId);
            if (app == null)
            {
                var error = String.Format("Invalid application ID {0}", applicationId);
                if (resultType == ApplicationResultType.TokenPairResponseV2)
                {
                    return new TokenPairResponseV2
                    {
                        ResponseCode = (int) HttpStatusCode.BadRequest,
                        ResponseSubCode = (int) HttpSub400StatusCode.InvalidApplicationId,
                        Error = error
                    };
                }
                return new TokenPairResponse {Error = error};
            }

            // validate refresh token
            var tokens = GetAccessRefreshToken(applicationId, memberId);
            if (tokens == null)
            {
                var error = string.Format("null tokens for app {0} member {1}", applicationId, memberId);
                Log.LogDebugMessage(error, ErrorHelper.GetCustomData());
                if (resultType == ApplicationResultType.TokenPairResponseV2)
                {
                    return new TokenPairResponseV2
                    {
                        ResponseCode = (int) HttpStatusCode.Unauthorized,
                        ResponseSubCode = (int) HttpSub401StatusCode.TokenNotFoundForMember,
                        Error = error
                    };
                }
                return new TokenPairResponse {Error = "No tokens found for the application user"};
            }
            if (String.IsNullOrEmpty(refreshToken))
            {
                var error = string.Format("null refresh token for app {0} member {1}", applicationId, memberId);
                Log.LogDebugMessage(error, ErrorHelper.GetCustomData());
                if (resultType == ApplicationResultType.TokenPairResponseV2)
                {
                    return new TokenPairResponseV2
                    {
                        ResponseCode = (int) HttpStatusCode.Unauthorized,
                        ResponseSubCode = (int) HttpSub401StatusCode.TokenNotFoundForMember,
                        Error = error
                    };
                }
                return new TokenPairResponse {Error = "Refresh token is required"};
            }
            if (tokens.RefreshToken.ExpiresDateTime < DateTime.Now)
            {
                var error = string.Format("expired refresh token for app {0} member {1}", applicationId, memberId);
                Log.LogDebugMessage(error, ErrorHelper.GetCustomData());
                if (resultType == ApplicationResultType.TokenPairResponseV2)
                {
                    return new TokenPairResponseV2
                    {
                        ResponseCode = (int) HttpStatusCode.Unauthorized,
                        ResponseSubCode = (int) HttpSub401StatusCode.ExpiredToken,
                        Error = error
                    };
                }
                return new TokenPairResponse {Error = "Refresh token has expired"};
            }

            if (tokens.RefreshToken.Token == refreshToken)
            {
                if (tokens.AccessToken.ExpiresDateTime < DateTime.Now)
                {
                    var expiresIn = AccessTokenLifetime;
                    //Log.DebugFormat("create token in memory for app {0} member {1}", applicationId, memberId);
                    tokens.AccessToken = CreateToken(applicationId, memberId, 0, expiresIn);
                    //Log.DebugFormat("saving appMember/tokens for app {0} member {1}", applicationId, memberId);
                    SaveAppMember(applicationId, memberId, tokens, brand);
                }

                //Log.DebugFormat("checking is paying member - app {0} member {1}", applicationId, memberId);
                var isPayingMember = SubscriptionStatus.MemberIsSubscriber(brand, memberId);
                var tokenPairResponse = new TokenPairResponse(tokens) {MemberId = memberId, IsPayingMember = isPayingMember};
                Log.LogDebugMessage(string.Format("returning tokens for - app {0} member {1}", applicationId, memberId), ErrorHelper.GetCustomData());
                if (resultType == ApplicationResultType.TokenPairResponseV2)
                {
                    tokenPairResponse = TokenPairResponseV2.Parse(tokenPairResponse);
                }
                return tokenPairResponse;
            }
            var errorMessage = string.Format("Invalid refresh token: {0} expected token {1}", refreshToken, tokens.RefreshToken.Token);
            Log.LogDebugMessage(errorMessage, ErrorHelper.GetCustomData());

            if (resultType == ApplicationResultType.TokenPairResponseV2)
            {
                return new TokenPairResponseV2
                {
                    ResponseCode = (int) HttpStatusCode.Unauthorized,
                    ResponseSubCode = (int) HttpSub401StatusCode.InvalidAccessToken,
                    Error = errorMessage
                };
            }
            return new TokenPairResponse {Error = String.Format("Invalid refresh token {0}", refreshToken)};
        }

        internal static TokenPairResponse CreateAccessTokenTrusted(Brand brand, int applicationId, int memberId,
            string tokenLifeType)
        {
            var accessToken = CreateToken(applicationId, memberId, 0,
                tokenLifeType == "short" ? AccessTokenLifetime : AccessTokenLongLifetime);

            var refreshtoken = CreateToken(applicationId, memberId, 30, RefreshTokenLifetime);

            var tokenData = new TokenPair {AccessToken = accessToken, RefreshToken = refreshtoken};
            SaveAppMember(applicationId, memberId, tokenData, brand);
            var isPayingMember = SubscriptionStatus.MemberIsSubscriber(brand, memberId);
            var accessTokenResponse = new TokenPairResponse(tokenData){MemberId = memberId, IsPayingMember = isPayingMember};
            Log.LogDebugMessage(string.Format("Created new tokens for member {0} application {1} refresh token {2} expires {3} access token {4} expires {5}",
                memberId, applicationId, refreshtoken.Token, refreshtoken.ExpiresDateTime, accessToken.Token,accessToken.ExpiresDateTime), ErrorHelper.GetCustomData());

            return accessTokenResponse;
        }

        
        internal static TokenWithExpiration CreateToken(int applicationId, int memberId, int cryptogrphicPaddingBytes,
            TimeSpan expiresIn, int adminMemberId = 0)
        {
            // adding random bytes to the unencrypted string makes the data more secure from hackers, but increases token length
            var randomBytes = cryptogrphicPaddingBytes > 0 ? Encryption.GetCryptoBytes(cryptogrphicPaddingBytes) : null;

            var expirationDate = DateTime.Now + expiresIn;

            var appIdBytes = BitConverter.GetBytes(applicationId);
            var memberIdBytes = BitConverter.GetBytes(memberId);
            var adminMemberIdBytes = BitConverter.GetBytes(adminMemberId);
            var expirationBytes = BitConverter.GetBytes(expirationDate.Ticks);

            var bytes =
                new byte[appIdBytes.Length + memberIdBytes.Length + adminMemberIdBytes.Length+ expirationBytes.Length + cryptogrphicPaddingBytes];
            var count = 0;
            appIdBytes.CopyTo(bytes, 0);
            count += appIdBytes.Length;
            memberIdBytes.CopyTo(bytes, count);
            count += memberIdBytes.Length;
            adminMemberIdBytes.CopyTo(bytes, count);
            count += adminMemberIdBytes.Length;
            expirationBytes.CopyTo(bytes, count);
            if (randomBytes != null)
            {
                count += expirationBytes.Length;
                randomBytes.CopyTo(bytes, count);
            }
            
            // generate an url safe token
            var encryptedToken = Encryption.EncryptBytes(bytes, true);
            // instead of a slash use a dash to be url safe
            var versionedToken = String.Format("{0}-{1}", CurrentTokenVersion, encryptedToken);
            var tokenData = new TokenWithExpiration {Token = versionedToken, ExpiresDateTime = expirationDate};

            return tokenData;
        }

        /// <summary>
        ///     Refactored many parsing and validating methods into its own
        /// </summary>
        /// <param name="token"></param>
        /// <param name="identifier"></param>
        /// <returns>Version value of 0 means it's an invalid token</returns>
        private static ParsedToken ParseToken(string token, char identifier)
        {
            var parsedToken = new ParsedToken();

            int version;

            var position = token.IndexOf(identifier);

            // identifier not found
            if (position < 0) return parsedToken;

            // could be a non int
            var stringVersion = token.Substring(0, position);

            // is the version an int?
            var isInt = int.TryParse(stringVersion, out version);

            // something other than an int
            if (!isInt)
            {
                Log.LogError("Invalid access token version", null, ErrorHelper.GetCustomData(),
                    ErrorHelper.ShouldLogExternally());
                return parsedToken;
            }

            // can this happen? was in existing code.
            if (position > token.Length) return parsedToken;

            // fully validated, return with found values.
            parsedToken.Version = version;
            parsedToken.Position = position;

            return parsedToken;
        }

        internal static bool DecryptAccessToken(string token, out int memberId, out int applicationId,
            out DateTime accessExpires, out string failReason, out int failCode, out int adminMemberId)
        {
            memberId = 0;
            adminMemberId = 0;
            applicationId = 0;
            accessExpires = new DateTime();

            // backwards compatibility support for old tokens using slash for versioning
            var parsedToken = ParseToken(token, '/');
            // if we get a proper version here, this token is on version 3
            if (parsedToken.Version == 0)  parsedToken = ParseToken(token, '-');

            // no proper version was found
            if (parsedToken.Version == 0)
            {
                failReason = "Invalid access token";
                failCode = (int)HttpSub401StatusCode.InvalidAccessToken;
                return false;
            }

            var encryptedToken = token.Substring(parsedToken.Position + 1);
            byte[] accessTokenBytes = null;

            // any larger version than current is invalid
            if (!(parsedToken.Version <= CurrentTokenVersion))
            {
                failReason = "Invalid access token version";
                failCode = (int) HttpSub401StatusCode.InvalidAccessTokenVersion;
                return false;
            }

            try
            {
                // starting with version 3, use url safe param.
                var decrypted = parsedToken.Version < UrlSafeTokenVersion ? Encryption.DecryptString(encryptedToken) : Encryption.DecryptString(encryptedToken, true);

                accessTokenBytes = Encoding.Default.GetBytes(decrypted);
            }
            catch (Exception ex)
            {
                Log.LogError("Failed to decrypt access token", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
            }
            if (accessTokenBytes == null)
            {
                failReason = "Invalid access token";
                failCode = (int) HttpSub401StatusCode.InvalidAccessToken;
                return false;
            }

            failReason = String.Empty;
            failCode = 0;
            var count = 0;

            try
            {
                long expirationDateTicks;
                switch (parsedToken.Version)
                {
                    case 4:
                        // admin member id added to token
                        applicationId = BitConverter.ToInt32(accessTokenBytes, count);
                        count += 4;
                        memberId = BitConverter.ToInt32(accessTokenBytes, count);
                        count += 4;
                        adminMemberId = BitConverter.ToInt32(accessTokenBytes, count);
                        count += 4;
                        expirationDateTicks = BitConverter.ToInt64(accessTokenBytes, count);
                        accessExpires = new DateTime(expirationDateTicks);
                        break;

                    case 3:
                        // nothing has changed here for version 3 which was added to support url safe base64 string
                    case 2:
                        // the app ids for CM were too big for Int16 so we had to make new tokens for the new app ids.
                        applicationId = BitConverter.ToInt32(accessTokenBytes, count);
                        count += 4;
                        memberId = BitConverter.ToInt32(accessTokenBytes, count);
                        count += 4;
                        expirationDateTicks = BitConverter.ToInt64(accessTokenBytes, count);
                        accessExpires = new DateTime(expirationDateTicks);
                        break;

                    case 1:
                        //TODO: REMOVE THIS VERSION WHEN ALL ACCESS TOKENS EXPIRE IN DB (6/2014)
                        applicationId = BitConverter.ToInt16(accessTokenBytes, count);
                        count += 2;
                        memberId = BitConverter.ToInt32(accessTokenBytes, count);
                        count += 4;
                        expirationDateTicks = BitConverter.ToInt64(accessTokenBytes, count);
                        accessExpires = new DateTime(expirationDateTicks);
                        break;
                    default:
                        throw new Exception("Invalid token version");
                }
            }
            catch (Exception e)
            {
                Log.LogError("Failed to convert access token.", e, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                return false;
            }
            return true;
        }

        /// <summary>
        ///     This is a pure CPU bound operation
        /// </summary>
        /// <param name="token"></param>
        /// <param name="failReason"></param>
        /// <param name="failCode"></param>
        /// <param name="memberId"></param>
        /// <returns></returns>
        internal static bool ValidateAccessToken(string token, out string failReason, out int failCode, out int memberId)
        {
            DateTime tokenAccessExpiration;
            int tokenAppId;
            int adminMemberId;
            if (!DecryptAccessToken(token, out memberId, out tokenAppId, out tokenAccessExpiration, out failReason,
                    out failCode, out adminMemberId))
            {
                return false;
            }

            return ValidateAccessToken(memberId, token, out failReason, out failCode);
        }

        internal static bool ValidateAccessToken(string token, out string failReason, out int failCode)
        {
            DateTime tokenAccessExpiration;
            int tokenMemberId;
            int tokenAppId;
            int adminMemberId;

            if (!DecryptAccessToken(token, out tokenMemberId, out tokenAppId, out tokenAccessExpiration, out failReason,
                    out failCode, out adminMemberId))
            {
                return false;
            }

            return ValidateAccessToken(tokenMemberId, token, out failReason, out failCode);
        }

        internal static bool ValidateAccessToken(int memberId, string token, out string failReason, out int failCode)
        {
            if (memberId == 0)
            {
                failReason = "Invalid member Id";
                failCode = (int) HttpSub401StatusCode.InvalidMemberId;
                return false;
            }

            DateTime tokenAccessExpiration;
            int tokenMemberId;
            int tokenAppId;
            int tokenAdminMemberId;

            if (!DecryptAccessToken(token, out tokenMemberId, out tokenAppId, out tokenAccessExpiration, out failReason,
                    out failCode, out tokenAdminMemberId))
            {
                return false;
            }
            if (tokenAccessExpiration < DateTime.Now)
            {
                failReason = "token has expired";
                failCode = (int) HttpSub401StatusCode.ExpiredToken;
                return false;
            }
            if (tokenMemberId == 0)
            {
                failReason = "Invalid token member";
                failCode = (int) HttpSub401StatusCode.InvalidMemberId;
                return false;
            }

            if (tokenMemberId != memberId)
            {
                failReason = "Invalid token for the app user";
                failCode = (int) HttpSub401StatusCode.TokenNotFoundForMember;
                return false;
            }

            if (tokenAdminMemberId < 0)
            {
                failReason = "Invalid admin member id";
                failCode = (int)HttpSub401StatusCode.InvalidAdminMemberId;
                return false;
            }

            var persistedToken = GetAccessRefreshToken(tokenAppId, memberId);
            if (persistedToken == null)
            {
                failReason = "Could not find stored access tokens for this app and member";
                failCode = (int) HttpSub401StatusCode.TokenNotFoundForMember;
                return false;
            }
            if (string.IsNullOrEmpty(persistedToken.AccessToken.Token))
            {
                failReason = "Stored access token not found for this member";
                failCode = (int) HttpSub401StatusCode.TokenNotFoundForMember;
                return false;
            }
            if (token.Replace(" ", "+") != persistedToken.AccessToken.Token)
            {
                Log.LogWarningMessage(string.Format("token mismatch, input token and time: {0}  - DB token {1} Expires: {2}", token,
                    persistedToken.AccessToken.Token, persistedToken.AccessToken.ExpiresDateTime), ErrorHelper.GetCustomData());
                failReason = "Token mismatch - invalid or revoked access token";
                failCode = (int) HttpSub401StatusCode.MismatchedToken;
                return false;
            }
            return true;
        }

        /// <summary>
        ///     Yet another internal method for validating an access token *but* with the flexibility of a result class
        ///     to avoid using out params.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        internal static ValidateTokenResponseV2 ValidateAccessToken(Brand brand, string token)
        {
            DateTime tokenAccessExpiration;
            int tokenAppId, memberId, failCode, adminMemberId;
            string failReason;

            if (!DecryptAccessToken(token, out memberId, out tokenAppId, out tokenAccessExpiration, out failReason,
                out failCode, out adminMemberId))
            {
                return new ValidateTokenResponseV2
                {
                    IsValid = false,
                    MemberId = memberId,
                    FailCode = failCode,
                    FailReason = failReason,
                    TokenAccessExpiration = tokenAccessExpiration,
                    TokenAppId = tokenAppId,
                    IsSelfSuspended = false
                };
            }

            var isValid = ValidateAccessToken(memberId, token, out failReason, out failCode);

            var validMember = MemberSA.Instance.GetMember(memberId);
            bool isSelfSuspend = (null != validMember && MemberHelper.IsMemberSelfSuspended(brand, validMember));
            bool isParticipatingInRedesign = (null != validMember && validMember.GetAttributeBool(brand, "REDRedesignBetaParticipatingFlag"));
            bool isOfferedRedesign = (null != validMember && validMember.GetAttributeBool(brand, "REDRedesignBetaOfferedFlag"));
            return new ValidateTokenResponseV2
            {
                IsValid = isValid,
                MemberId = memberId,
                FailCode = failCode,
                FailReason = failReason,
                TokenAccessExpiration = tokenAccessExpiration,
                TokenAppId = tokenAppId,
                IsSelfSuspended = isSelfSuspend,
                REDRedesignBetaParticipatingFlag = isParticipatingInRedesign,
                REDRedesignBetaOfferedFlag = isOfferedRedesign
            };
        }

        internal static bool ValidateClientCredentials(int appId, string clientSecret, out string failReason,
            out int failCode)
        {
            var application = GetApplication(appId);
            if (application == null)
            {
                failReason = "Invalid application";
                failCode = (int) HttpSub401StatusCode.InvalidApplicationId;
                return false;
            }

            // Only verify when passed in.
            if (!String.IsNullOrEmpty(clientSecret) && clientSecret != application.ClientSecret)
            {
                failReason = String.Format("Bad client secret: {0}", clientSecret);
                failCode = (int) HttpSub401StatusCode.MismatchClientSecret;
                return false;
            }
            failReason = String.Empty;
            failCode = 0;
            return true;
        }

        private static App TranformApplicationToAppVo(Application application)
        {
            var app = new App
            {
                AppId = application.ApplicationId,
                AutoAuth = application.AutoAuth,
                BrandId = application.BrandId,
                Secret = application.ClientSecret,
                Title = application.Title,
                Description = application.Description,
                MemberId = application.OwnerMemberId,
                RedirectUrl = application.RedirectUrl,
                ContactUsEmail = application.ContactUsEmail
            };
            return app;
        }

        private static Application TransformAppVoToApplication(App app)
        {
            var application = new Application
            {
                ApplicationId = app.AppId,
                AutoAuth = app.AutoAuth,
                BrandId = app.BrandId,
                ClientSecret = app.Secret,
                Title = app.Title,
                Description = app.Description,
                OwnerMemberId = app.MemberId,
                RedirectUrl = app.RedirectUrl,
                ContactUsEmail = app.ContactUsEmail
            };
            return application;
        }

        /// <summary>
        ///     easiest way to call this is to use RESTClient
        ///     https://preprod.api.spark.net/v2/brandid/1003/apps/application?applicationId=1003&client_secret
        ///     =SXO0NoMjOqPDvPNGmEwZsHxnT5oyXTmYKpBXCx3SJTE1
        ///     content-type:application/json
        ///     post body
        ///     {
        ///     "brandId":"1003",
        ///     "ownerMemberId":"27029711",
        ///     "clientSecret":"o4M7iVhmuyqrxqF60M+pk+cxEWzGjCH7Qi+6GOjPjKg=",
        ///     "title":"JDate iPhone App 2",
        ///     "description":"iPhone optimized app for JDate circa 2014",
        ///     "redirectUrl":"http://www.jdate.com"
        ///     }
        /// </summary>
        /// <param name="application"></param>
        /// <param name="resourceRoot"></param>
        /// <returns></returns>
        internal static string CreateApplication(Application application, string resourceRoot)
        {
            application.ApplicationId = 0;
            application.AutoAuth = false;
            var encrypted = Encryption.EncryptBytes(Encryption.GetCryptoBytes(15));
            var app = TranformApplicationToAppVo(application);
            var appId = APISA.Instance.CreateApp(app);
            app.AppId = appId;
            return String.Format(ApplicationResourceFormat, resourceRoot, app.AppId, app.AppId, app.Secret);
        }

        internal static Application GetApplication(int appId)
        {
            var apps = APISA.Instance.GetApps();
            var returnApp = apps != null
                ? apps.Where(app => app != null).Where(app => app.AppId == appId).Select(
                    TransformAppVoToApplication).FirstOrDefault()
                : null;
            return returnApp;
        }

        private static void SaveApplication(Application application)
        {
            var app = TranformApplicationToAppVo(application);
            APISA.Instance.UpdateApp(app);
        }

        internal static bool UpdateApplication(int appId, Application application)
        {
            var storedApplication = GetApplication(appId);
            if (storedApplication == null)
            {
                return false;
            }
            // only allow certain fields to be updated
            storedApplication.Description = application.Description;
            storedApplication.Title = application.Title;
            storedApplication.RedirectUrl = application.RedirectUrl;
            SaveApplication(application);
            return true;
        }

        internal static bool DeleteApplication(int appId)
        {
            APISA.Instance.DeleteApp(appId);
            return true;
        }

        internal static string ResetClientSecret(int appId, string resourceRoot)
        {
            var application = GetApplication(appId);
            if (application == null)
            {
                return null;
            }
            var encrypted = Encryption.EncryptBytes(Encryption.GetCryptoBytes(15));
            SaveApplication(application);
            return String.Format(ApplicationResourceFormat, resourceRoot, application.ApplicationId,
                application.ApplicationId, application.ClientSecret);
        }

        private static void SaveAppMember(int applicationId, int memberId, TokenPair tokenPair, Brand brand)
        {
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);

            if (member == null) return;

            if (MemberHelper.IsMemberAdminSuspended(brand, member))
            {
                throw new Exception("member is admin suspended. cannot create a new access token.");
            }

            var appMember = new AppMember
            {
                AccessToken = tokenPair.AccessToken.Token,
                AccessTokenExpirationDateTime = tokenPair.AccessToken.ExpiresDateTime,
                AppId = applicationId,
                MemberId = memberId,
                RefreshToken = tokenPair.RefreshToken.Token,
                RefreshTokenExpirationDateTime = tokenPair.RefreshToken.ExpiresDateTime
            };
            var existingMember = APISA.Instance.GetAppMember(applicationId, memberId);
            if (existingMember == null)
            {
                Log.LogDebugMessage(string.Format("Creating App member for app {0}, member {1} access token {2} refresh token {3}",
                    appMember.AppId, appMember.MemberId, appMember.AccessToken, appMember.RefreshToken), ErrorHelper.GetCustomData());
                APISA.Instance.CreateAppMember(appMember);
            }
            else
            {
                Log.LogDebugMessage(string.Format("Updating App member for app {0}, member {1} access token {2} refresh token {3}",
                    appMember.AppId, appMember.MemberId, appMember.AccessToken, appMember.RefreshToken), ErrorHelper.GetCustomData());
                APISA.Instance.UpdateAppMember(appMember);
            }
        }

        internal static bool HardResetPassword(int memberId, Brand brand)
        {
            Log.LogDebugMessage(string.Format("ApplicationAccess.HardResetPassword entered. MemberId: {0} BrandId: {1}", memberId,
                brand.BrandID), ErrorHelper.GetCustomData());

            return MemberSA.Instance.ChangePasswordToRandomString(memberId, brand.Site.Community.CommunityID);
        }

        /// <summary>
        ///     Creates a reset password token and send out an email to the member with the link to reset the password.
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="emailAddress"> Email address of the member </param>
        /// <param name="suaUrl">Spark Unified Authorization url</param>
        /// <param name="templateId"> </param>
        /// <param name="applicationId">client Id</param>
        /// <returns> False if email is not found </returns>
        internal static PasswordResetResponse ResetPassword(Brand brand, string emailAddress, string suaUrl,
            string templateId, int applicationId,
            string ipAddress, string userAgent, string httpHeaders, string loginSessonId)
        {
            var passwordResetResponse = new PasswordResetResponse {EmailSent = false};
            if (string.IsNullOrEmpty(emailAddress)) return passwordResetResponse;
            var memberId = MemberSA.Instance.GetMemberIDByEmail(emailAddress.Trim(), brand.Site.Community.CommunityID);
            if (!(memberId > 0))
            {
                ActivityRecordingAccess.Instance.RecordPasswordResetRequestActivity(loginSessonId, emailAddress, false,
                    0, brand.BrandID);
                return passwordResetResponse;
            }

            // generate the token
            var tokenGuid = SaveMemberService.ResetPassword(memberId);
            // send off an externalmail with this value
            //TODO: get sua passreset page
            var resetUrl =
                string.Format(
                    "https://{0}/password/reset/{1}?resettoken={2}&mid={3}&clientid={4}&loginsessionid={5}",
                    suaUrl, templateId, HttpUtility.UrlEncode(tokenGuid), memberId, applicationId, loginSessonId);

            passwordResetResponse.EmailSent = ExternalMailSA.Instance.ResetPassword(brand.BrandID, memberId, resetUrl);
            ActivityRecordingAccess.Instance.RecordPasswordResetRequestActivity(loginSessonId, emailAddress,
                passwordResetResponse.EmailSent, memberId, brand.BrandID);

            var loginFraudHelper = new LoginFraudHelper(brand);
            loginFraudHelper.LogAction(emailAddress, LogActionType.ResetPasswordRequest, ipAddress, userAgent,
                httpHeaders, memberId, applicationId, false, passwordResetResponse.EmailSent);

            var loggingMan = new LoggingManager();
            loggingMan.LogInfoMessage("MemberPasswordManager",
                string.Format(
                    "Reset password request completed with BrandId:{0}, MemberId:{1}, ResetURL:{2}, Call to ExternalMail success?:{3}",
                    brand.BrandID,
                    memberId, resetUrl, passwordResetResponse.EmailSent));

            return passwordResetResponse;
        }

        /// <summary>
        ///     Changes the member's password and sends the changed profile external mail to the member. This method doesn't
        ///     validate the password, so make sure the password is valid before calling this.
        /// </summary>
        /// <param name="member"> </param>
        /// <param name="brand"> </param>
        /// <param name="password"> </param>
        /// <returns> </returns>
        internal static bool ChangePassword(int memberId, Brand brand, string password, int applicationId,
            string ipAddress, string userAgent, string httpHeaders, string loginSessionId)
        {
            var passwordText = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo,
                ResourceGroupEnum.Global,
                "TXT_PASSWORD");

            var success = false;
            var emailAddress = string.Empty;
            var settingsManager = new SettingsManager();
            var logonOnlySite = settingsManager.GetSettingBool(SettingConstants.LOGON_ONLY_SITE, brand);
            var loginFraudHelper = new LoginFraudHelper(brand);

            if (logonOnlySite)
            {
                var passwordChangeSuccess = MemberSA.Instance.SavePassword(memberId, brand.Site.Community.CommunityID,
                    password);

                if (passwordChangeSuccess)
                {
                    var basicLogonInfo = MemberSA.Instance.GetMemberBasicLogonInfo(memberId,
                        brand.Site.Community.CommunityID);

                    ExternalMailSA.Instance.SendProfileChangedConfirmationEmail(memberId,
                        brand.BrandID,
                        basicLogonInfo.EmailAddress,
                        string.Empty,
                        basicLogonInfo.UserName,
                        new List<string> {passwordText});
                    emailAddress = basicLogonInfo.EmailAddress;
                }

                success = passwordChangeSuccess;
            }
            else
            {
                var member = GetMemberService.GetMember(memberId, MemberLoadFlags.None);
                member.Password = password;

                var result = SaveMemberService.SaveMember(member, brand.Site.Community.CommunityID);
                if (result.SaveStatus == MemberSaveStatusType.Success)
                {
                    ExternalMailSA.Instance.SendProfileChangedConfirmationEmail(member.MemberID,
                        brand.BrandID,
                        member.EmailAddress,
                        member.GetAttributeText(brand, "sitefirstname"),
                        member.GetUserName(brand),
                        new List<string> {passwordText});
                    emailAddress = member.EmailAddress;
                    success = true;
                }
            }

            if (success)
            {
                var synchMinglePassword = Convert.ToBoolean(SettingsService.GetSettingFromSingleton(Matchnet.Configuration.ValueObjects.SettingConstants.MINGLE_PW_SYNCH_ENABLED, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
                if (synchMinglePassword)
                {
                    SynchPasswordToMingle(memberId, brand.BrandID, emailAddress, password);
                }
            }

            if (!string.IsNullOrEmpty(loginSessionId))
            {
                ActivityRecordingAccess.Instance.RecordPasswordResetAttemptActivity(loginSessionId, emailAddress,
                    success, memberId, brand.BrandID);
            }

            loginFraudHelper.LogAction(emailAddress, LogActionType.ResetPassword, ipAddress, userAgent,
                httpHeaders, memberId, applicationId, false, success);

            return success;
        }

        internal static bool AuthenticatePassword(int memberId, Brand brand, string email, string password)
        {
            var authenticated = MemberSA.Instance.BasicAuthenticate(email, password, brand.Site.Community.CommunityID);
            return authenticated.Status == AuthenticationStatus.Authenticated;
        }

        internal static AuthenticatedPasswordChangeResponse AuthenticatedChangePassword(int memberId,
            string currentPassword, string newPassword, string accessToken, Brand brand, int applicationId,
            string ipAddress, string userAgent, string httpHeaders)
        {
            Log.LogInfoMessage(string.Format("ApplicationAccess.AuthenticatedChangePassword entered. MemberId: {0} BrandId: {1}",
                memberId, brand.BrandID), ErrorHelper.GetCustomData());

            int failCode;
            string failReason;
            var communityId = brand.Site.Community.CommunityID;

            var response = new AuthenticatedPasswordChangeResponse {Status = AuthenticatedPasswordChangeStatus.Success};
            
            var accessTokenValidate = ValidateAccessToken(memberId, accessToken, out failReason, out failCode);
            if (!accessTokenValidate)
            {
                response.Status = AuthenticatedPasswordChangeStatus.InvalidAccessToken;
                return response;
            }

            //If the current password is the same as the new password entered into control, then dont make change and notify user. 
            if (currentPassword.Trim() == newPassword.Trim())
            {
                response.Status = AuthenticatedPasswordChangeStatus.NoChange;
                return response;
            }

            Log.LogInfoMessage(string.Format("ApplicationAccess.AuthenticatedChangePassword - token validated. MemberId: {0} BrandId: {1}",
                memberId, brand.BrandID), ErrorHelper.GetCustomData());

            var memberBasicLogonInfo = MemberSA.Instance.GetMemberBasicLogonInfo(memberId, communityId);

            if (memberBasicLogonInfo == null)
            {
                response.Status = AuthenticatedPasswordChangeStatus.GeneralFailure;
                return response;
            }

            Log.LogInfoMessage(string.Format("ApplicationAccess.AuthenticatedChangePassword - retrieved logon info. MemberId: {0} BrandId: {1}",
                memberId, brand.BrandID), ErrorHelper.GetCustomData());

            var currentPasswordValid = MemberSA.Instance.VerifyPassword(memberBasicLogonInfo.EmailAddress, communityId,
                currentPassword);

            if (!currentPasswordValid)
            {
                response.Status = AuthenticatedPasswordChangeStatus.OriginalPasswordNotValid;
                return response;
            }

            Log.LogInfoMessage(string.Format("ApplicationAccess.AuthenticatedChangePassword - current password verified. MemberId: {0} BrandId: {1}",
                memberId, brand.BrandID), ErrorHelper.GetCustomData());

            var passwordChanged = MemberSA.Instance.SavePassword(memberId, communityId, newPassword);
            if (passwordChanged)
            {
                var synchMinglePassword = Convert.ToBoolean(SettingsService.GetSettingFromSingleton(SettingConstants.MINGLE_PW_SYNCH_ENABLED, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
                if (synchMinglePassword)
                {
                    SynchPasswordToMingle(memberId, brand.BrandID, memberBasicLogonInfo.EmailAddress, newPassword);
                }

                Log.LogInfoMessage(string.Format("ApplicationAccess.AuthenticatedChangePassword - password changed. MemberId: {0} BrandId: {1}",
                    memberId, brand.BrandID), ErrorHelper.GetCustomData());
            }
            else
            {
                Log.LogInfoMessage(string.Format("ApplicationAccess.AuthenticatedChangePassword - password change failure. MemberId: {0} BrandId: {1}",
                        memberId, brand.BrandID), ErrorHelper.GetCustomData());
                    
                response.Status = AuthenticatedPasswordChangeStatus.GeneralFailure;
            }

            var loginFraudHelper = new LoginFraudHelper(brand);
            loginFraudHelper.LogAction(memberBasicLogonInfo.EmailAddress, LogActionType.ChangedPassword, ipAddress,
                userAgent, httpHeaders,
                memberId, applicationId, false, passwordChanged);

            return response;
        }

        /// <summary>
        ///     Checks to see if the token value is still active. If so the memberID is returned.
        /// </summary>
        /// <param name="tokenGuidString"> </param>
        /// <returns> MemberID if the token is valid </returns>
        internal static int ValidateResetPasswordToken(string tokenGuidString)
        {
            return MemberSA.Instance.ValidateResetPasswordToken(tokenGuidString);
        }

        /// <summary>
        ///     Our current password strength requirement is pretty weak. Only between 4 to 16 characters. I'm trying to centralize
        ///     the logic here so when we make the requirements stronger, we can do it here.
        /// </summary>
        /// <param name="password"> Password to test for strength </param>
        /// <returns> True if it meets our current standard for strength of password </returns>
        internal static bool ValidatePasswordStrength(string password)
        {
            if (string.IsNullOrEmpty(password))
                return false;

            return (password.Length <= 16 && password.Length >= 4);
        }

        internal static void SynchPasswordToMingle(int memberId, int brandId, string emailAddress, string password)
        {
            Log.LogInfoMessage(string.Format("ApplicationAccess.SynchPasswordToMingle - synch started. MemberId: {0} BrandId: {1} EmailAddress: {2}",
                memberId, brandId, emailAddress), ErrorHelper.GetCustomData());

            var workFactor = Convert.ToInt32(SettingsService.GetSettingFromSingleton("CRYPTO_WORK_FACTOR"));
            var passwordHash = Matchnet.Security.Crypto.Instance.EncryptTextPlusSalt(password, workFactor);

            var encryptedTextAndSalt = Matchnet.Security.Crypto.Instance.SplitEncryptedTextAndSalt(passwordHash);
            var encryptedPasswordHash = encryptedTextAndSalt[0];
            var encryptedPasswordSalt = encryptedTextAndSalt[1];

            var passwordSynchRequest = new Spark.MingleAPIAdapter.ExternalModels.ExternalMinglePasswordSynchRequest();
            passwordSynchRequest.MemberId = memberId;
            passwordSynchRequest.BrandId = brandId;
            passwordSynchRequest.EmailAddress = emailAddress;
            passwordSynchRequest.P1 = encryptedPasswordHash;
            passwordSynchRequest.P2 = encryptedPasswordSalt;
            Publisher.Publish<Spark.MingleAPIAdapter.ExternalModels.ExternalMinglePasswordSynchRequest>(passwordSynchRequest);

            Log.LogInfoMessage(string.Format("ApplicationAccess.SynchPasswordToMingle - synch ended. MemberId: {0} BrandId: {1} EmailAddress: {2}",
                memberId, brandId, emailAddress), ErrorHelper.GetCustomData());
        }

        internal static EmailChangeResponse AuthenticatedChangeEmail(int memberId, string currentEmail, string newEmail, Brand brand, string ipAddress)
        {

            Log.LogInfoMessage(string.Format("ApplicationAccess.AuthenticatedChangeEmail entered. MemberId: {0} BrandId: {1} Email: {2}",
                memberId, brand.BrandID, currentEmail), ErrorHelper.GetCustomData());

            var response = new EmailChangeResponse { Status = EmailChangeStatus.Success };
            var emailVerifyHelper = new EmailVerifyHelper(brand);

            if (string.IsNullOrEmpty(currentEmail) || string.IsNullOrEmpty(currentEmail.Trim()) ||
                !emailVerifyHelper.IsEmailValid(currentEmail.Trim())) 
            {
                response.Status = EmailChangeStatus.OriginalEmailNotValid;
                return response;
                //throw new SparkAPIException("Current email address is not valid");
            }

            // Previously, the code simply returned EmailChangeStatus.NewEmailNotValid if the newEmail was null,empty OR if the current email was the same as the new email.
            // It makes more sense to return NoChange for instances where they are the same.
            if (string.IsNullOrEmpty(newEmail) || string.IsNullOrEmpty(newEmail.Trim()) || !emailVerifyHelper.IsEmailValid(newEmail.Trim()))
            {
                response.Status = EmailChangeStatus.NewEmailNotValid;
                return response;
                //throw new SparkAPIException("New email address is not valid");
            }

            if (currentEmail.Trim() == newEmail.Trim())
            {
                response.Status = EmailChangeStatus.NoChange;
                return response;
            }

            if (MemberSA.Instance.CheckEmailAddressExists(newEmail, brand.Site.Community.CommunityID))
            {
                response.Status = EmailChangeStatus.EmailAddressAlreadyExists;
                return response;
                //throw new SparkAPIReportableException(HttpSub400StatusCode.EmailAlreadyExists, string.Format("The email address '{0}' already exists in the system. Please enter a new address", newEmail));
            }

            var member = GetMemberService.GetMember(memberId, MemberLoadFlags.None);

            // If caller supplied a currentEmail that is different from the EmailAddress in DB, then return CurrentEmailIncorrect.
            if(member.EmailAddress != currentEmail)
            {
                response.Status = EmailChangeStatus.CurrentEmailIncorrect;
                return response;
            }

            member.EmailAddress = newEmail;

            if (emailVerifyHelper.EnableEmailVerification)
            {
                if (emailVerifyHelper.IfMustBlock(WebConstants.EmailVerificationSteps.AfterEmailChange) ||
                    emailVerifyHelper.IfMustHide(WebConstants.EmailVerificationSteps.AfterEmailChange))
                {
                    emailVerifyHelper.SetNotVerifiedMemberAttributes(member, WebConstants.EmailVerificationSteps.AfterEmailChange);
                }
            }
            else
            {
                var globalStatusMask = (GlobalStatusMask) member.GetAttributeInt(brand, AttributeConstants.GLOBALSTATUSMASK);
                globalStatusMask = globalStatusMask & (~GlobalStatusMask.VerifiedEmail);
                member.SetAttributeInt(brand, AttributeConstants.GLOBALSTATUSMASK, (int) globalStatusMask);
            }

            var result = SaveMemberService.SaveMember(member, brand.Site.Community.CommunityID);
            if (result.SaveStatus == MemberSaveStatusType.Success)
            {
                //profile changed email notification (send to previous email address)
                var changedFields = new List<string>
                {
                    ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global, "TXT_EMAIL")
                };
                ExternalMailSA.Instance.SendProfileChangedConfirmationEmail(memberId, brand.BrandID, currentEmail,
                    member.GetAttributeText(brand, "sitefirstname"), member.GetUserName(brand), changedFields);

                //email verification
                response.EmailSent = ExternalMailSA.Instance.SendEmailVerification(memberId, brand.BrandID);

                //flag to indicate if member should be asked to verify email address
                response.EnableEmailVerification = emailVerifyHelper.EnableEmailVerification;
            }
            else
            {
                response.Status = EmailChangeStatus.NewEmailNotValid;
                return response;
                //throw new SparkAPIException("New email address is not valid");
            }

            //send email change action to utah api
            var ctx = System.Web.HttpContext.Current;
            MiscUtils.FireAndForget(o =>
            {
                //set HttpContext for thread
                System.Web.HttpContext.Current = ctx;
                try
                {
                    var loginFraudHelper = new LoginFraudHelper(brand);
                    loginFraudHelper.ChangeEmail(memberId, newEmail, ipAddress);
                }
                finally
                {
                    //unset HttpContext for thread (precaution for GC)
                    System.Web.HttpContext.Current = null;                    
                }
            }, "ApplicationAccess", "Failed to send change email to mingle api!");
            return response;
        }
    }

    internal class ParsedToken
    {
        public ParsedToken()
        {
            Version = 0;
            Position = 0;
        }

        public int Version { get; set; }
        /// <summary>
        ///     Position of the identifier
        /// </summary>
        public int Position { get; set; }
    }
}