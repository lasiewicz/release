﻿#region

using Matchnet.Configuration.ServiceAdapters;

#endregion

namespace Spark.Rest.V2.DataAccess
{
    /// <summary>
    ///     Base class for Data Access classes. Do NOT introduce any HTTP Context stuff in there!
    /// </summary>
    public abstract class AccessBase
    {
        private ISettingsSA _settingsService;

        public ISettingsSA SettingsService
        {
            get { return _settingsService ?? (_settingsService = RuntimeSettings.Instance); }
            set { _settingsService = value; }
        }
    }
}