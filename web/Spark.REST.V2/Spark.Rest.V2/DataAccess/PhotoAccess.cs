﻿using System;
using System.Collections.Generic;
using Spark.REST.Entities.Profile;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Logger;
using Spark.REST.Helpers;

namespace Spark.REST.DataAccess
{
    /// <summary>
    /// 
    /// </summary>
	public static class PhotoAccess
	{
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(PhotoAccess));

        /// <summary>
        /// Gets the photos.
        /// </summary>
        /// <param name="brand">The brand.</param>
        /// <param name="memberId">The member identifier.</param>
        /// <param name="viewerId">The viewer identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error getting photos. MemberID: + memberId + , ViewerID: + viewerId</exception>
		public static List<Photo> GetPhotos(Brand brand, int memberId, int viewerId)
		{
			try
			{
				var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
				var items = member.GetPhotos(brand.Site.Community.CommunityID);
				var photos = new List<Photo>();
			    for (byte i = 0; i < items.Count; i++)
			    {
			        var item = items[i];

			        if ((!item.IsApproved && viewerId != memberId)) continue;
			        var photo = new Photo
			        {
			            MemberPhotoId = item.MemberPhotoID,
			            FullId = item.FileID,
			            ThumbId = item.ThumbFileID,
			            FullPath = MemberHelper.GetPhotoUrl(item, PhotoType.Full, brand),
			            ThumbPath = MemberHelper.GetPhotoUrl(item, PhotoType.Thumbnail, brand),
			            Caption = (item.IsCaptionApproved || viewerId == memberId) ? item.Caption : string.Empty,
			            // change to String.Empty null once all clients can handle it
			            ListOrder = item.ListOrder,
			            IsCaptionApproved = item.IsCaptionApproved,
			            IsPhotoApproved = item.IsApproved,
			            IsMain = item.IsMain,
			            IsApprovedForMain = item.IsApprovedForMain
			        };

			        photos.Add(photo);
			    }

                photos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

				return photos;
			}
			catch (Exception exception)
			{
				throw new Exception("Error getting photos. MemberID:" + memberId + ", ViewerID:" + viewerId, exception);
			}

		}

        /// <summary>
        /// Gets the photo count.
        /// </summary>
        /// <param name="brand">The brand.</param>
        /// <param name="memberId">The member identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error getting photos. MemberID: + memberId</exception>
		public static int GetPhotoCount(Brand brand, int memberId)
		{
			try
			{
				var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
				var items = member.GetPhotos(brand.Site.Community.CommunityID);
				return items.Count;
			}
			catch (Exception exception)
			{
				throw new Exception("Error getting photos. MemberID:" + memberId, exception);
			}
		}

        /// <summary>
        /// Gets the default photo.
        /// </summary>
        /// <param name="brand">The brand.</param>
        /// <param name="viewerId">The viewer identifier.</param>
        /// <param name="member">The member.</param>
        /// <returns></returns>
        public static Photo GetDefaultPhoto(Brand brand, int viewerId, Member member)
        {
            var memberPhoto = GetDefaultMemberPhoto(brand, viewerId, member);

            if (memberPhoto == null) return null;
            var photo = new Photo
            {
                MemberPhotoId = memberPhoto.MemberPhotoID,
                FullId = memberPhoto.FileID,
                ThumbId = memberPhoto.ThumbFileID,
                FullPath = MemberHelper.GetPhotoUrl(memberPhoto, PhotoType.Full, brand),
                ThumbPath = MemberHelper.GetPhotoUrl(memberPhoto, PhotoType.Thumbnail, brand),
                Caption = memberPhoto.Caption,
                ListOrder = memberPhoto.ListOrder,
                IsCaptionApproved = memberPhoto.IsCaptionApproved,
                IsPhotoApproved = memberPhoto.IsApproved,
                IsMain = memberPhoto.IsMain,
                IsApprovedForMain = memberPhoto.IsApprovedForMain
            };

            return photo;
        }

        /// <summary>
        /// Selects a "default" photo object to use as a thumbnail (or whatever), returning a private photo if there are no approved,
        /// non-private photos, and null if there are no approved photos.
        /// </summary>
        public static Matchnet.Member.ValueObjects.Photos.Photo GetDefaultMemberPhoto(Brand brand, int viewerId, Member member)
        {
            return member.GetDefaultPhoto(brand.Site.Community.CommunityID, MemberHelper.PublicPhotosOnly(brand, viewerId, member));
        }

        

	}
}