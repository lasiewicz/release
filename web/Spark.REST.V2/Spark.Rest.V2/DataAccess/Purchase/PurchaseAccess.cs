﻿#region

using System;
using System.Net;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.MemberDTO;
using Spark.Common.AccessService;
using Spark.Common.Adapter;
using Spark.Common.PaymentProfileService;
using Spark.Common.PurchaseService;
using Spark.Logger;
using Spark.REST.Entities.Purchase;
using Spark.Rest.Helpers;
using Spark.Rest.V2.DataAccess;
using Spark.Rest.V2.Entities.Purchase;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using PrivilegeType = Spark.Common.AccessService.PrivilegeType;

#endregion

namespace Spark.Rest.DataAccess.Purchase
{
    public interface IPurchaseAccess
    {
        IOSValidateResponse ValidateIOSReceipt(Brand brand, string encodedReceiptData, int memberId);
        IAPControllerStatus ProcessIOSOrder(Brand brand, string encodedReceiptData, int memberId, string orderAttributeXml, int requestRegionId = 0);
        IOSReceipt GetIOSReceiptDetails(string encodedReceiptData, out APIAdapter.RestStatus restStatus, int siteId);

        AndroidValidateResponse ValidateAndroidSubscription(Brand brand, string subscriptionToken, int memberId);
        IAPControllerStatus ProcessAndroidOrder(Brand brand, string subscriptionToken, int memberId, string orderAttributeXml, int requestRegionId = 0);
        AndroidSubscription GetAndroidSubscriptionDetails(string subscriptionToken, out APIAdapter.RestStatus restStatus, int siteId);
        AndroidSubscription TerminateAndroidSubscriptionRenewal(string subscriptionToken, out APIAdapter.RestStatus restStatus, int siteId);
        AndroidSubscription VoidAndroidSubscription(string subscriptionToken, out APIAdapter.RestStatus restStatus, int siteId);
    }

    public class PurchaseAccess : IPurchaseAccess
    {
        public const string IOS_IAP_RECEIPT_ORIG_TRANS_ID_ATTR = "IOS_IAP_Receipt_Original_TransactionID";
        public const string LIFETIME_IOS_ORIGINAL_TRANSACTIONID = "Lifetime_IOS_Original_TransactionID";
        public const string ENABLE_RECORD_LATEST_IOS_IAP_RECEIPT = "ENABLE_RECORD_LATEST_IOS_IAP_RECEIPT";
        public const int IAP_RENEWAL_TERMINATE_REASON_ID = 83;
        public const string ANDROID_IAB_SUBSCRIPTION_TOKENID_ATTR = "Android_IAB_Subscription_TokenID";
        public const string ENABLE_RECORD_LATEST_ANDROID_IAB_RECEIPT = "ENABLE_RECORD_LATEST_ANDROID_IAB_RECEIPT";
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof (PurchaseAccess));
        public static readonly IPurchaseAccess Singleton = new PurchaseAccess();
//        public const int IAP_RENEWAL_TERMINATE_REASON_ID = 83;
        private IActivityRecordingAccess _activityRecordingAccess;
        private IDateGenerator _dateGenerator;
        private IGetMember _getMemberService;
        private ISaveMember _saveMemberService;
        private ISparkServiceClientFactory _serviceClientFactory;
        private ISettingsSA _settingsService;
        private ISparkAPIAdapter _sparkApiAdapter;

        private PurchaseAccess()
        {
        }

        public IGetMember GetMemberService
        {
            get { return _getMemberService ?? (_getMemberService = MemberSA.Instance); }
            set { _getMemberService = value; }
        }

        public ISaveMember SaveMemberService
        {
            get { return _saveMemberService ?? (_saveMemberService = MemberSA.Instance); }
            set { _saveMemberService = value; }
        }

        public IActivityRecordingAccess ActivityRecordingAccess
        {
            get { return _activityRecordingAccess ?? (_activityRecordingAccess = V2.DataAccess.ActivityRecordingAccess.Instance); }
            set { _activityRecordingAccess = value; }
        }

        public IDateGenerator DateGenerator
        {
            get { return _dateGenerator ?? (_dateGenerator = Purchase.DateGenerator.Singleton); }
            set { _dateGenerator = value; }
        }

        public ISparkAPIAdapter SparkApiAdapter
        {
            get { return _sparkApiAdapter ?? (_sparkApiAdapter = Purchase.SparkApiAdapter.Singleton); }
            set { _sparkApiAdapter = value; }
        }

        public ISettingsSA SettingsService
        {
            get { return _settingsService ?? (_settingsService = RuntimeSettings.Instance); }
            set { _settingsService = value; }
        }

        public ISparkServiceClientFactory ServiceClientFactory
        {
            get { return _serviceClientFactory ?? (_serviceClientFactory = SparkServiceClientFactory.Singleton); }
            set { _serviceClientFactory = value; }
        }

        public IOSValidateResponse ValidateIOSReceipt(Brand brand, string encodedReceiptData, int memberId)
        {            

            if(null == brand) throw new SparkAPIReportableArgumentException("Brand cannot be null");
            if (string.IsNullOrEmpty(encodedReceiptData))
            {
                var receiptData = GetIOSReceipt(brand, memberId);
                //try to get receipt data from member's payment profile
                if (!string.IsNullOrEmpty(receiptData))
                {
                    encodedReceiptData = receiptData;
                }
                else  // if no receipt throw argument exception
                {
                    throw new SparkAPIReportableArgumentException("EncodedReceiptData cannot be null or empty.");
                }
            }
            if (ShouldLogEncodedReceiptData(brand))
            {
                Log.LogInfoMessage(string.Format("MemberId:{0}, EncodedReceiptData:'{1}'", memberId, encodedReceiptData), ErrorHelper.GetCustomData());
            }
            if(memberId < 1) throw new SparkAPIReportableArgumentException("MemberId must be greater than 0.");

            var iapStatus = IAPControllerStatus.None;
            var member = GetMemberService.GetMember(memberId, MemberLoadFlags.None);

            var memberOriginalTransID = member.GetAttributeText(brand, LIFETIME_IOS_ORIGINAL_TRANSACTIONID);
            Log.LogInfoMessage(string.Format("MemberId:{0}, MemberOriginalTransID:{1}", memberId, memberOriginalTransID), ErrorHelper.GetCustomData());
            var accessPrivilege = member.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
            Log.LogInfoMessage(string.Format("MemberId:{0}, BasicSubExpirationDateUTC:{1}", memberId, (null != accessPrivilege)?accessPrivilege.EndDateUTC.ToShortDateString():null), ErrorHelper.GetCustomData());

            var iosReceipt = SparkApiAdapter.GetIOSReceipt(brand, encodedReceiptData, memberId);
            //validate original transaction id for the member
            var lastInAppReceipt = iosReceipt.GetLatestInAppInfo();
            //record the last inapp receipt to activity recording
            if (IsRecordIAPReceiptEnabled(brand))
            {
                ActivityRecordingAccess.RecordLatestInAppReceiptActivity(memberId, brand.BrandID, lastInAppReceipt);
            }

            //JS-1276 PREVENT 2 MEMBERS FROM SHARING THE SAME RECEIPT FOR IAP
            var iosReceiptCheckResponse = ServiceClientFactory.GetPurchaseServiceClient().IsUserAllowedToUseIOSReceiptOriginalTransactionID(memberId, brand.Site.SiteID, lastInAppReceipt.original_transaction_id);

            if (iosReceiptCheckResponse != null && !iosReceiptCheckResponse.isUserAllowedToUseIOSReceiptOriginalTransactionID)
            {
                iapStatus = IAPControllerStatus.InvalidReceipt;
                ActivityRecordingAccess.RecordStatusForInAppReceiptActivity(memberId, brand.BrandID, iapStatus, lastInAppReceipt);
                Log.LogInfoMessage(string.Format("MemberId:{0}, IAP Controller Status:'{1}', Reason:'{2}'", memberId, iapStatus, "Block sharing IOS Receipt : " + lastInAppReceipt.original_transaction_id), ErrorHelper.GetCustomData());
                return new IOSValidateResponse(lastInAppReceipt.original_transaction_id, iapStatus, lastInAppReceipt);
            }
            
            //for first time purchase, member original transaction id will be empty. have to set it before we compute iap status.
            if (string.IsNullOrEmpty(memberOriginalTransID))
            {
                member.SetAttributeText(brand, LIFETIME_IOS_ORIGINAL_TRANSACTIONID, lastInAppReceipt.original_transaction_id, TextStatusType.Auto);
                SaveMemberService.SaveMember(member);
                memberOriginalTransID = lastInAppReceipt.original_transaction_id;
            }

            //compute iap status - BEGIN
            //first check if member cancelled or if receipt matches existing receipt for member
            var isCancelled = (!string.IsNullOrEmpty(lastInAppReceipt.cancellation_date)); // if receipt has cancellation date value, customer cancelled via Apple CS

            if (isCancelled)
                {
                iapStatus = IAPControllerStatus.VoidPreviousOrder;
                ActivityRecordingAccess.RecordStatusForInAppReceiptActivity(memberId, brand.BrandID, iapStatus, lastInAppReceipt);
                Log.LogInfoMessage(string.Format("MemberId:{0}, IAP Controller Status:'{1}', Reason:'{2}'", memberId, iapStatus, "Cancelled Receipt Date =" + lastInAppReceipt.CancellationDateUTC.ToShortDateString()), ErrorHelper.GetCustomData());
            }
            else //if receipt not cancelled and receipt belongs to member, then proceed with iap status computation
            {
                var utcNow = DateGenerator.UTCNow;
                // privilege sub date expired or not synced with receipt date
                //
                var outdatedPrivilegeSubscriptionDate = (null != accessPrivilege &&
                                                          accessPrivilege.EndDateUTC < lastInAppReceipt.ExpiresDateUTC &&
                                                          (utcNow < lastInAppReceipt.ExpiresDateUTC && utcNow >= accessPrivilege.EndDateUTC));
                // first time purchase or failed ups call for first time purchase                    
                var firstTimePurchaseOrfailedUpsCall = (null == accessPrivilege && utcNow < lastInAppReceipt.ExpiresDateUTC);
                // auto-renewal turned off in ios                    
                var outdatedReceiptSubscriptionDate = (null != accessPrivilege &&
                                                        accessPrivilege.EndDateUTC >= lastInAppReceipt.ExpiresDateUTC &&
                                                        utcNow > lastInAppReceipt.ExpiresDateUTC);
                    
                // if first time purchase, ups failed on first call, or access privilege date is older than receipt expiration date
                // then purchase order
                if (firstTimePurchaseOrfailedUpsCall || outdatedPrivilegeSubscriptionDate)
                {
                    iapStatus = IAPControllerStatus.PurchaseOrder;
                    //save lifetime orig trans id for ups callback service
                    member.SetAttributeText(brand, LIFETIME_IOS_ORIGINAL_TRANSACTIONID, lastInAppReceipt.original_transaction_id, TextStatusType.Auto);
                    SaveMemberService.SaveMember(member);
                    ActivityRecordingAccess.RecordStatusForInAppReceiptActivity(memberId, brand.BrandID, iapStatus, lastInAppReceipt);
                    Log.LogInfoMessage(string.Format("MemberId:{0}, IAP Controller Status:'{1}', Reason:'{2}'", memberId, iapStatus, "NonSub? " + (null == accessPrivilege) + " OR " + utcNow.ToString("G") + " < " + lastInAppReceipt.ExpiresDateUTC.ToString("G")), ErrorHelper.GetCustomData());
                }
                else if (outdatedReceiptSubscriptionDate) // if receipt expiration date is past then terminate subscription
                {
                    iapStatus = IAPControllerStatus.TerminateRenewal;
                    ActivityRecordingAccess.RecordStatusForInAppReceiptActivity(memberId, brand.BrandID, iapStatus, lastInAppReceipt);
                    Log.LogInfoMessage(string.Format("MemberId:{0}, IAP Controller Status:'{1}', Reason:'{2}'", memberId, iapStatus, accessPrivilege.EndDateUTC + " >= " + lastInAppReceipt.ExpiresDateUTC.ToString("G") + " OR " + utcNow.ToString("G") + " > " + lastInAppReceipt.ExpiresDateUTC.ToString("G")), ErrorHelper.GetCustomData());
                }

                if (Convert.ToBoolean(SettingsService.GetSettingFromSingleton("ENABLE_RAYGUNIO_FOR_IAP_STATUS")) && iapStatus == IAPControllerStatus.None)
                {
                    //Add more logging for None case
                    Log.LogInfoMessage(string.Format("MemberId:'{0}', OutdatedPrivilegeSubscriptionDate:'{1}', FirstTimePurchaseOrfailedUpsCall:'{2}', OutdatedReceiptSubscriptionDate:'{3}'",
                            memberId, outdatedPrivilegeSubscriptionDate.ToString(),firstTimePurchaseOrfailedUpsCall.ToString(), outdatedReceiptSubscriptionDate.ToString()),ErrorHelper.GetCustomData(), true);
                    Log.LogInfoMessage(string.Format("MemberId:{0}, IAP Controller Status:'{1}', UTCNow:'{2}', Access Privilege End Date:'{3}', Receipt Expiration Date: '{4} ",
                            memberId, iapStatus, utcNow.ToString("G"),(null == accessPrivilege) ? "null" : accessPrivilege.EndDateUTC.ToString("G"),lastInAppReceipt.ExpiresDateUTC.ToString("G")), ErrorHelper.GetCustomData(), true);
                }
            }

            if (iapStatus == IAPControllerStatus.None)
            {
                ActivityRecordingAccess.RecordStatusForInAppReceiptActivity(memberId, brand.BrandID, iapStatus, lastInAppReceipt);
                var privEndDate = (null == accessPrivilege) ? "null" : accessPrivilege.EndDateUTC.ToString();
                if (Convert.ToBoolean(SettingsService.GetSettingFromSingleton("ENABLE_RAYGUNIO_FOR_IAP_STATUS")))
                {
                    Log.LogInfoMessage(string.Format("MemberId:{0}, IAP Controller Status:'{1}', Access Privilege End Date:'{2}', Receipt Expiration Date: '{3}',Reason:'{4}'",
                            memberId, iapStatus, privEndDate, lastInAppReceipt.ExpiresDateUTC.ToString("G"),"No changes to IAP for Member."), ErrorHelper.GetCustomData(), true);
                }
                else
                {
                    Log.LogInfoMessage(string.Format("MemberId:{0}, IAP Controller Status:'{1}', Access Privilege End Date:'{2}', Receipt Expiration Date: '{3}',Reason:'{4}'",
                            memberId, iapStatus, privEndDate, lastInAppReceipt.ExpiresDateUTC.ToString("G"),"No changes to IAP for Member."), ErrorHelper.GetCustomData());
                }
            }
            //compute iap status - END
            
            return new IOSValidateResponse(lastInAppReceipt.original_transaction_id, iapStatus, lastInAppReceipt);
        }


        public IOSReceipt GetIOSReceiptDetails(string encodedReceiptData, out APIAdapter.RestStatus restStatus, int siteId)
        {
            var iosReceipt = new IOSReceipt();
            restStatus = SparkApiAdapter.MakeIOSApiCallBySiteId(encodedReceiptData, out iosReceipt, siteId);

            if (restStatus.StatusCode != HttpStatusCode.OK)
            {
                throw new SparkAPIException(string.Format("Error: 'Apple IOS', Status Code:{0}, SiteId:{1}, ErrorMessage:{2}, Response from Apple:{3}",
                    restStatus.StatusCode, siteId, restStatus.ErrorMessage, restStatus.Content));
            }
            if (iosReceipt.status != 0 && iosReceipt.status != 21006)
            //0 == valid receipt, 21006 == valid receipt with expired subscription
            {
                throw new SparkAPIException(string.Format("Error: 'Apple IOS Receipt', Status Code:{0}, SiteId:{1}, ErrorMessage:{2}", 
                    restStatus.StatusCode, siteId, restStatus.ErrorMessage));
            }

            bool noLatestReceiptInfo = (null == iosReceipt.latest_receipt_info || iosReceipt.latest_receipt_info.Count == 0);
            bool noInApp = (null == iosReceipt.receipt || null == iosReceipt.receipt.in_app || iosReceipt.receipt.in_app.Count == 0);
            if (noLatestReceiptInfo && noInApp)
            {
                throw new SparkAPIException(string.Format("No in app receipts for siteId:{0} ", siteId));
            }

            return iosReceipt;
        }

        public IAPControllerStatus ProcessIOSOrder(Brand brand, string encodedReceiptData, int memberId, string orderAttributeXml, int requestRegionId)
        {
            if (null == brand) throw new SparkAPIReportableArgumentException("Brand cannot be null");
            if (string.IsNullOrEmpty(encodedReceiptData))
            {
                var receiptData = GetIOSReceipt(brand, memberId);
                //try to get receipt data from member's payment profile
                if (!string.IsNullOrEmpty(receiptData))
                {
                    encodedReceiptData = receiptData;
                }
                else  // if no receipt throw argument exception
                {
                    throw new SparkAPIReportableArgumentException("EncodedReceiptData cannot be null or empty.");
                }
            }
            if (memberId < 1) throw new SparkAPIReportableArgumentException("MemberId must be greater than 0.");

            var iosValidateResponse = ValidateIOSReceipt(brand, encodedReceiptData, memberId);
            var iapStatus = iosValidateResponse.Status;

            var member = GetMemberService.GetMemberDTO(memberId, MemberType.Full);

            //Initiate in app purchase save to UPS
            var callingSystemId = brand.Site.SiteID;
            var callingSystemTypeId = (int) SystemType.User;
            try
            {
                switch (iapStatus)
                {
                    case IAPControllerStatus.PurchaseOrder:
                    {
                        var purchaseDate = iosValidateResponse.LastInAppReceipt.PurchaseDateUTC;
                        var regionId = requestRegionId > 0 ? requestRegionId : member.GetAttributeInt(brand, "RegionId");
                        var isSubscriptionConfirmationEmailEnabled = false; // Potential Runtime Setting
                        var externalPackageId = iosValidateResponse.LastInAppReceipt.product_id;
                        var memberOriginalTransID = member.GetAttributeText(brand, LIFETIME_IOS_ORIGINAL_TRANSACTIONID);

                        Log.LogInfoMessage(string.Format("Start UPS IAP Purchase: MemberId:{0}, External PackageId:'{1}', CallingSystemId:{2}, CallingSystemTypeId:{3}, PurchaseDate:{4}, RegionId:{5}, OrderAttributeXml:'{6}', MemberOriginalTransID: {7}", memberId, externalPackageId, callingSystemId, callingSystemTypeId, purchaseDate.ToShortDateString(), regionId, orderAttributeXml, memberOriginalTransID), ErrorHelper.GetCustomData());
                        var iapPurchaseResponse = ServiceClientFactory.GetPurchaseServiceClient().DoInApplicationPurchase(externalPackageId, encodedReceiptData, memberOriginalTransID, memberId, callingSystemId, callingSystemTypeId, purchaseDate, regionId, orderAttributeXml, isSubscriptionConfirmationEmailEnabled);
                        Log.LogInfoMessage(string.Format("End UPS IAP Purchase: MemberId:{0}, External PackageId:'{1}', CallingSystemId:{2}, CallingSystemTypeId:{3}, PurchaseDate:{4}, RegionId:{5}, OrderAttributeXml:'{6}', UPS ResponseCode:{7}, UPS Response Message:'{8}'", memberId, externalPackageId, callingSystemId, callingSystemTypeId, purchaseDate.ToShortDateString(), regionId, orderAttributeXml, iapPurchaseResponse.InternalResponse.responsecode, iapPurchaseResponse.InternalResponse.responsemessage), ErrorHelper.GetCustomData());
                        if (iapPurchaseResponse.InternalResponse.responsecode != "0")
                        {
                            throw new SparkAPIReportableException(HttpSub400StatusCode.FailedIAPPurchase, string.Format("Failed to complete iap purchase for memberId:{0}, siteId:{1}, extPackageId:{2}, purchaseDate:{3}, orderId:{4}, reason:{5}", memberId, callingSystemId, externalPackageId, purchaseDate, iapPurchaseResponse.orderID, iapPurchaseResponse.InternalResponse.responsemessage));
                        }
                        // Let's make sure Member isn't holding onto any cached access for the member. Since the UPS callback
                        // does not fire for CM members on the BH side, this call is required.
                        MemberSA.Instance.ExpireCachedMemberAccess(memberId);
                    }
                        break;
                    case IAPControllerStatus.TerminateRenewal:
                    {
                        //TODO: Pass in Subscription End Date to adjust Access Privilege End Date
                        Log.LogInfoMessage(string.Format("Start UPS IAP Disable AutoRenewal: MemberId:{0}, CallingSystemId:{1}, CallingSystemTypeId:{2}, ReasonId:{3}", memberId, callingSystemId, callingSystemTypeId, IAP_RENEWAL_TERMINATE_REASON_ID), ErrorHelper.GetCustomData());
                        var adminId = int.MinValue;
                        var adminUserName = string.Empty;
//                        callingSystemTypeId = 6; //COMMENT OUT FOR PROD.  THIS IS FOR TESTING ONLY!!!!
                        var iapRenewalResponse = ServiceClientFactory.GetRenewalServiceClient().DisableAutoRenewal(memberId, callingSystemId, callingSystemTypeId, adminId, adminUserName, IAP_RENEWAL_TERMINATE_REASON_ID);
                        Log.LogInfoMessage(string.Format("End UPS IAP Disable AutoRenewal: MemberId:{0}, CallingSystemId:{1}, CallingSystemTypeId:{2}, ReasonId:{3}, UPS ResponseCode:{4}, UPS Response Message:'{5}'", memberId, callingSystemId, callingSystemTypeId, IAP_RENEWAL_TERMINATE_REASON_ID, iapRenewalResponse.InternalResponse.responsecode, iapRenewalResponse.InternalResponse.responsemessage), ErrorHelper.GetCustomData());
                        if (iapRenewalResponse.InternalResponse.responsecode != "0")
                        {
                            throw new SparkAPIReportableException(HttpSub400StatusCode.FailedIAPDisableAutoRenewal, string.Format("Failed to complete iap renewal termination for memberId:{0}, siteId:{1}, transactionId:{2}, reason:{3}", memberId, callingSystemId, iapRenewalResponse.TransactionID, iapRenewalResponse.InternalResponse.responsemessage));
                        }                
                    }
                        break;
                    case IAPControllerStatus.VoidPreviousOrder:
                    {
                        //TODO: Pass in Cancellation Date to adjust Access Privilege End Date
                        Log.LogInfoMessage(string.Format("Start UPS IAP Credit Order: MemberId:{0}, CallingSystemId:{1}, CallingSystemTypeId:{2}", memberId, callingSystemId, callingSystemTypeId), ErrorHelper.GetCustomData());

                        //TODO: Anthony - Talk to Jay Rho about internalRenewalTransactionID (4th parameter)
                        var iapCreditOrderResponse = ServiceClientFactory.GetPurchaseServiceClient().DoIAPCreditOrder(memberId, callingSystemId, callingSystemTypeId, 0);
                        Log.LogInfoMessage(string.Format("End UPS IAP Credit Order: MemberId:{0}, CallingSystemId:{1}, CallingSystemTypeId:{2}, UPS ResponseCode:{3}, UPS Response Message:'{4}'", memberId, callingSystemId, callingSystemTypeId, iapCreditOrderResponse.InternalResponse.responsecode, iapCreditOrderResponse.InternalResponse.responsemessage), ErrorHelper.GetCustomData());
                        if (iapCreditOrderResponse.InternalResponse.responsecode != "0")
                        {
                            throw new SparkAPIReportableException(HttpSub400StatusCode.FailedIAPCreditOrder, string.Format("Failed to complete iap credit order for memberId:{0}, siteId:{1}, reason:{2}", memberId, callingSystemId, iapCreditOrderResponse.InternalResponse.responsemessage));
                        }
                        // Let's make sure Member isn't holding onto any cached access for the member. Since the UPS callback
                        // does not fire for CM members on the BH side, this call is required.
                        MemberSA.Instance.ExpireCachedMemberAccess(memberId);
                    }
                        break;
                }
            }
            finally
            {
                ServiceClientFactory.ClosePurchaseServiceClient();
                ServiceClientFactory.CloseRenewalServiceClient();
            }
            return iapStatus;
        }

        public AndroidValidateResponse ValidateAndroidSubscription(Brand brand, string subscriptionToken, int memberId)
        {
            if (null == brand) throw new SparkAPIReportableArgumentException("Brand cannot be null");
            if (string.IsNullOrEmpty(subscriptionToken))
            {
                var receiptData = GetAndroidSubscriptionToken(brand, memberId);
                //try to get receipt data from member's payment profile
                if (!string.IsNullOrEmpty(receiptData))
                {
                    subscriptionToken = receiptData;
                }
                else // if no receipt throw argument exception
                {
                    throw new SparkAPIReportableArgumentException("SubscriptionToken cannot be null or empty.");
                }
            }
            if (ShouldLogAndroidSubscriptionData(brand))
            {
                Log.LogInfoMessage(string.Format("MemberId:{0}, SubscriptionToken:'{1}'", memberId, subscriptionToken),
                    ErrorHelper.GetCustomData());
            }
            if (memberId < 1) throw new SparkAPIReportableArgumentException("MemberId must be greater than 0.");

            var androidSubStatus = IAPControllerStatus.None;
            var member = GetMemberService.GetMember(memberId, MemberLoadFlags.None);

            var memberAndroidSubTokenID = member.GetAttributeText(brand, ANDROID_IAB_SUBSCRIPTION_TOKENID_ATTR);
            Log.LogInfoMessage(string.Format("MemberId:{0}, MemberAndroidSubTokenID:{1}", memberId, memberAndroidSubTokenID), ErrorHelper.GetCustomData());
            var accessPrivilege = member.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
            Log.LogInfoMessage(string.Format("MemberId:{0}, BasicSubExpirationDateUTC:{1}", memberId, (null != accessPrivilege) ? accessPrivilege.EndDateUTC.ToShortDateString() : null), ErrorHelper.GetCustomData());

            APIAdapter.RestStatus restStatus;
            AndroidSubscription androidSubscription = null;
            try
            {
                androidSubscription = GetAndroidSubscriptionDetails(subscriptionToken, out restStatus, brand.Site.SiteID);

            if (restStatus.StatusCode != HttpStatusCode.OK || null == androidSubscription)
            {
                    throw new SparkAPIException(
                        string.Format("Error: Google Android status code:{0}, memberId:{1}, brandId:{2}, reason:'{3}'",
                            restStatus.StatusCode, memberId, brand.BrandID, restStatus.ErrorMessage));
            }
            if (androidSubscription.Status != 0)
            {
                    throw new SparkAPIException(
                        string.Format(
                            "Error: Google Subscription status code:{0}, memberId:{1}, brandId:{2}, reason:'{3}'",
                            androidSubscription.Status, memberId, brand.BrandID, androidSubscription.ErrorReason));
                }
            }
            catch (Exception e)
            {
                var message = string.Format("Error retrieving Google subscription status for memberId:{0}, brandId:{1}, subscriptionToken:{2}",
                        memberId, brand.BrandID, subscriptionToken);
                Log.LogError(message, e, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                throw new SparkAPIException(message);
            }

            Log.LogInfoMessage(string.Format("MemberId:{0}, Rest Status Code:{1}, Rest Status Error:'{2}'", memberId, restStatus.StatusCode, restStatus.ErrorMessage), ErrorHelper.GetCustomData());

            //record the last android subscription to activity recording
            if (IsRecordAndroidIABEnabled(brand))
            {
                ActivityRecordingAccess.RecordLatestInAppBillingActivity(memberId, brand.BrandID, androidSubscription);
            }

            //for first time purchase, member subscription token will be empty. have to set it before we compute iap status.
            //NOTE: THIS DOES NOT PREVENT 2 MEMBERS FROM SHARING THE SAME RECEIPT FOR SUBSCRIPTION
            if (string.IsNullOrEmpty(memberAndroidSubTokenID))
            {
                member.SetAttributeText(brand, ANDROID_IAB_SUBSCRIPTION_TOKENID_ATTR, subscriptionToken, TextStatusType.Auto);
                SaveMemberService.SaveMember(member);
                memberAndroidSubTokenID = subscriptionToken;
            }

            //compute iap status - BEGIN
            var isMemberReceipt = (memberAndroidSubTokenID == subscriptionToken);

            if (!isMemberReceipt)
            {
                androidSubStatus = IAPControllerStatus.None;
                ActivityRecordingAccess.RecordStatusForInAppBillingActivity(memberId, brand.BrandID, androidSubStatus, androidSubscription);
                Log.LogInfoMessage(string.Format("MemberId:{0}, IAP Controller Status:'{1}', Reason:'{2}'", memberId, androidSubStatus, memberAndroidSubTokenID + "!=" + subscriptionToken), ErrorHelper.GetCustomData());
            }
            else //if receipt belongs to member, then proceed with iab status computation
            {
                var utcNow = DateGenerator.UTCNow;
                // privilege sub date expired or not synced with android sub date
                var outdatedPrivilegeSubscriptionDate = (null != accessPrivilege && accessPrivilege.EndDateUTC < androidSubscription.ExpirationDate && utcNow < androidSubscription.ExpirationDate);
                // first time purchase or failed ups call for first time purchase                    
                var firstTimePurchaseOrfailedUpsCall = (null == accessPrivilege && utcNow < androidSubscription.ExpirationDate);
                // auto-renewal turned off in android
                var outdatedReceiptSubscriptionDate = (null != accessPrivilege && accessPrivilege.EndDateUTC >= androidSubscription.ExpirationDate && utcNow > androidSubscription.ExpirationDate);

                // if first time purchase, ups failed on first call, or access privilege date is older than android sub expiration date
                // then purchase order
                if (firstTimePurchaseOrfailedUpsCall || outdatedPrivilegeSubscriptionDate)
                {
                    androidSubStatus = IAPControllerStatus.PurchaseOrder;
                    member.SetAttributeText(brand, ANDROID_IAB_SUBSCRIPTION_TOKENID_ATTR, subscriptionToken, TextStatusType.Auto);
                    SaveMemberService.SaveMember(member);
                    ActivityRecordingAccess.RecordStatusForInAppBillingActivity(memberId, brand.BrandID, androidSubStatus, androidSubscription);
                    Log.LogInfoMessage(string.Format("MemberId:{0}, IAP Controller Status:'{1}', Reason:'{2}'", memberId, androidSubStatus, "NonSub? " + (null == accessPrivilege) + " OR " + utcNow.ToShortDateString() + " < " + androidSubscription.ExpirationDate.ToShortDateString()), ErrorHelper.GetCustomData());
                }
                else if (outdatedReceiptSubscriptionDate && !androidSubscription.IsAutoRenew)
                // if android sub expiration date is past then terminate subscription
                {
                    androidSubStatus = IAPControllerStatus.TerminateRenewal;
                    // If auto-renew is off and sub is expired then remove attr
                    member.SetAttributeText(brand, ANDROID_IAB_SUBSCRIPTION_TOKENID_ATTR, null, TextStatusType.Auto);
                    SaveMemberService.SaveMember(member);
                    ActivityRecordingAccess.RecordStatusForInAppBillingActivity(memberId, brand.BrandID, androidSubStatus, androidSubscription);
                    Log.LogInfoMessage(string.Format("MemberId:{0}, IAP Controller Status:'{1}', Reason:'{2}'", memberId, androidSubStatus, accessPrivilege.EndDateUTC + " >= " + androidSubscription.ExpirationDate.ToShortDateString() + " OR " + utcNow.ToShortDateString() + " > " + androidSubscription.ExpirationDate.ToShortDateString() + "|| IsAutoRenew?" + androidSubscription.IsAutoRenew), ErrorHelper.GetCustomData());
                }
            }

            if (isMemberReceipt && androidSubStatus == IAPControllerStatus.None)
            {
                ActivityRecordingAccess.RecordStatusForInAppBillingActivity(memberId, brand.BrandID, androidSubStatus, androidSubscription);
                Log.LogInfoMessage(string.Format("MemberId:{0}, IAP Controller Status:'{1}', Reason:'{2}'", memberId, androidSubStatus, "No changes to IAP for Member."), ErrorHelper.GetCustomData());
            }
            //compute iap status - END

            return new AndroidValidateResponse(subscriptionToken, androidSubStatus, androidSubscription);
        }

        public IAPControllerStatus ProcessAndroidOrder(Brand brand, string subscriptionToken, int memberId,
            string orderAttributeXml, int requestRegionId)
        {
            if (null == brand) throw new SparkAPIReportableArgumentException("Brand cannot be null");
            if (string.IsNullOrEmpty(subscriptionToken))
            {
                var receiptData = GetAndroidSubscriptionToken(brand, memberId);
                //try to get receipt data from member's payment profile
                if (!string.IsNullOrEmpty(receiptData))
                {
                    subscriptionToken = receiptData;
                }
                else // if no receipt throw argument exception
                {
                    throw new SparkAPIReportableArgumentException("SubscriptionToken cannot be null or empty.");
                }
            }
            if (memberId < 1) throw new SparkAPIReportableArgumentException("MemberId must be greater than 0.");

            var validateAndroidSubscription = ValidateAndroidSubscription(brand, subscriptionToken, memberId);
            var androidSubStatus = validateAndroidSubscription.Status;

            var member = GetMemberService.GetMemberDTO(memberId, MemberType.Full);

            //Initiate in app purchase save to UPS
            var callingSystemId = brand.Site.SiteID;
            var callingSystemTypeId = (int) SystemType.User;
            try
            {
                switch (androidSubStatus)
                {
                    case IAPControllerStatus.PurchaseOrder:
                    {
                        var purchaseDate = validateAndroidSubscription.AndroidSubscriptionObj.StartDate;
                        var regionId = requestRegionId > 0 ? requestRegionId : member.GetAttributeInt(brand, "RegionId");
                        var isSubscriptionConfirmationEmailEnabled = false; // Potential Runtime Setting
                        var externalPackageId = validateAndroidSubscription.AndroidSubscriptionObj.SubscriptionId;

                        Log.LogInfoMessage(string.Format("Start UPS Google IAB Purchase: MemberId:{0}, External PackageId:'{1}', CallingSystemId:{2}, CallingSystemTypeId:{3}, PurchaseDate:{4}, RegionId:{5}, OrderAttributeXml:'{6}'", memberId, externalPackageId, callingSystemId, callingSystemTypeId, purchaseDate.ToShortDateString(), regionId, orderAttributeXml), ErrorHelper.GetCustomData());
                        var googlePlayPurchase = ServiceClientFactory.GetPurchaseServiceClient().DoGooglePlayPurchase(externalPackageId, subscriptionToken, memberId, callingSystemId, callingSystemTypeId, purchaseDate, regionId, orderAttributeXml, isSubscriptionConfirmationEmailEnabled);
                        Log.LogInfoMessage(string.Format("End UPS Google IAB Purchase: MemberId:{0}, External PackageId:'{1}', CallingSystemId:{2}, CallingSystemTypeId:{3}, PurchaseDate:{4}, RegionId:{5}, OrderAttributeXml:'{6}', UPS ResponseCode:{7}, UPS Response Message:'{8}'", memberId, externalPackageId, callingSystemId, callingSystemTypeId, purchaseDate.ToShortDateString(), regionId, orderAttributeXml, googlePlayPurchase.InternalResponse.responsecode, googlePlayPurchase.InternalResponse.responsemessage), ErrorHelper.GetCustomData());
                        if (googlePlayPurchase.InternalResponse.responsecode != "0")
                        {
                            throw new SparkAPIReportableException(HttpSub400StatusCode.FailedGooglePlayPurchase, string.Format("Failed to complete google play purchase for memberId:{0}, siteId:{1}, extPackageId:{2}, purchaseDate:{3}, reason:{4}", memberId, callingSystemId, externalPackageId, purchaseDate, googlePlayPurchase.InternalResponse.responsemessage));
                        }
                        // Let's make sure Member isn't holding onto any cached access for the member. Since the UPS callback
                        // does not fire for CM members on the BH side, this call is required.
                        MemberSA.Instance.ExpireCachedMemberAccess(memberId);
                    }
                        break;
                    case IAPControllerStatus.TerminateRenewal:
                    {
                        //TODO: Pass in Subscription End Date to adjust Access Privilege End Date
                        Log.LogInfoMessage(string.Format("Start UPS IAB Disable AutoRenewal: MemberId:{0}, CallingSystemId:{1}, CallingSystemTypeId:{2}, ReasonId:{3}", memberId, callingSystemId, callingSystemTypeId, IAP_RENEWAL_TERMINATE_REASON_ID), ErrorHelper.GetCustomData());
                        var googlePlayRenewalResponse = ServiceClientFactory.GetRenewalServiceClient().DisableAutoRenewal(memberId, callingSystemId, callingSystemTypeId, int.MinValue, string.Empty, IAP_RENEWAL_TERMINATE_REASON_ID);
                        Log.LogInfoMessage(string.Format("End UPS IAB Disable AutoRenewal: MemberId:{0}, CallingSystemId:{1}, CallingSystemTypeId:{2}, ReasonId:{3}, UPS ResponseCode:{4}, UPS Response Message:'{5}'", memberId, callingSystemId, callingSystemTypeId, IAP_RENEWAL_TERMINATE_REASON_ID, googlePlayRenewalResponse.InternalResponse.responsecode, googlePlayRenewalResponse.InternalResponse.responsemessage), ErrorHelper.GetCustomData());
                        if (googlePlayRenewalResponse.InternalResponse.responsecode != "0")
                        {
                            throw new SparkAPIReportableException(HttpSub400StatusCode.FailedGooglePlayDisableAutoRenewal, string.Format("Failed to complete google play renewal termination for memberId:{0}, siteId:{1}, reason:{2}", memberId, callingSystemId, googlePlayRenewalResponse.InternalResponse.responsemessage));
                        }
                    }
                        break;
                    case IAPControllerStatus.VoidPreviousOrder:
                    {
                        //TODO: Pass in Cancellation Date to adjust Access Privilege End Date
                        Log.LogInfoMessage(string.Format("Start UPS IAB Credit Order: MemberId:{0}, CallingSystemId:{1}, CallingSystemTypeId:{2}", memberId, callingSystemId, callingSystemTypeId), ErrorHelper.GetCustomData());

                        //TODO: Anthony - Talk to Jay Rho about internalRenewalTransactionID (4th parameter)
                        var googlePlayCreditOrder = ServiceClientFactory.GetPurchaseServiceClient().DoGooglePlayCreditOrder(memberId, callingSystemId, callingSystemTypeId, 0);
                        Log.LogInfoMessage(string.Format("End UPS IAB Credit Order: MemberId:{0}, CallingSystemId:{1}, CallingSystemTypeId:{2}, UPS ResponseCode:{3}, UPS Response Message:'{4}'", memberId, callingSystemId, callingSystemTypeId, googlePlayCreditOrder.InternalResponse.responsecode, googlePlayCreditOrder.InternalResponse.responsemessage), ErrorHelper.GetCustomData());
                        if (googlePlayCreditOrder.InternalResponse.responsecode != "0")
                        {
                            throw new SparkAPIReportableException(HttpSub400StatusCode.FailedGooglePlayCreditOrder, string.Format("Failed to complete google play credit order for memberId:{0}, siteId:{1}, reason:{2}", memberId, callingSystemId, googlePlayCreditOrder.InternalResponse.responsemessage));
                        }
                        // Let's make sure Member isn't holding onto any cached access for the member. Since the UPS callback
                        // does not fire for CM members on the BH side, this call is required.
                        MemberSA.Instance.ExpireCachedMemberAccess(memberId);
                    }
                        break;
                }
            }
            finally
            {
                ServiceClientFactory.ClosePurchaseServiceClient();
                ServiceClientFactory.CloseRenewalServiceClient();
            }
            return androidSubStatus;
        }

        public AndroidSubscription GetAndroidSubscriptionDetails(string subscriptionToken, out APIAdapter.RestStatus restStatus, int siteId)
        {
            AndroidSubscription androidSubscription;
            restStatus = SparkApiAdapter.MakeAndroidApiCallBySiteId(subscriptionToken, out androidSubscription, siteId, GoogleApiCall.Get);
            return androidSubscription;
        }

        public AndroidSubscription TerminateAndroidSubscriptionRenewal(string subscriptionToken, out APIAdapter.RestStatus restStatus, int siteId)
        {
            AndroidSubscription androidSubscription;
            restStatus = SparkApiAdapter.MakeAndroidApiCallBySiteId(subscriptionToken, out androidSubscription, siteId, GoogleApiCall.Cancel);
            return androidSubscription;
        }

        public AndroidSubscription VoidAndroidSubscription(string subscriptionToken, out APIAdapter.RestStatus restStatus, int siteId)
        {
            AndroidSubscription androidSubscription;
            restStatus = SparkApiAdapter.MakeAndroidApiCallBySiteId(subscriptionToken, out androidSubscription, siteId, GoogleApiCall.Revoke);
            return androidSubscription;
        }

        private bool IsRecordIAPReceiptEnabled(Brand brand)
        {
            try
            {
                return SettingsService.SettingExistsFromSingleton(ENABLE_RECORD_LATEST_IOS_IAP_RECEIPT,
                    brand.Site.Community.CommunityID, brand.Site.SiteID) &&
                       SettingsService.GetSettingFromSingleton(ENABLE_RECORD_LATEST_IOS_IAP_RECEIPT,
                           brand.Site.Community.CommunityID, brand.Site.SiteID).ToLower() == Boolean.TrueString.ToLower();
            }
            catch (Exception e)
            {
                Log.LogException(e.Message, e, null, false);
                return false;
    }
        }

        private bool IsRecordAndroidIABEnabled(Brand brand)
        {
            try
            {
                return SettingsService.SettingExistsFromSingleton(ENABLE_RECORD_LATEST_ANDROID_IAB_RECEIPT,
                    brand.Site.Community.CommunityID, brand.Site.SiteID) &&
                       SettingsService.GetSettingFromSingleton(ENABLE_RECORD_LATEST_ANDROID_IAB_RECEIPT,
                           brand.Site.Community.CommunityID, brand.Site.SiteID).ToLower() == Boolean.TrueString.ToLower();
            }
            catch (Exception e)
            {
                Log.LogException(e.Message, e, null, false);
                return false;
            }
        }

        private bool ShouldLogEncodedReceiptData(Brand brand)
        {
            return true;
        }

        private bool ShouldLogAndroidSubscriptionData(Brand brand)
        {
            return true;
        }

        private string GetIOSReceipt(Brand brand, int memberId)
        {
            var encodedReceipt = string.Empty;
            try
            {
                var paymentProfileMapperClient = ServiceClientFactory.GetPaymentProfileClient();
                var paymentProfileResponse = paymentProfileMapperClient.GetObsfucatedPaymentProfileByMemberID(memberId,
                    brand.Site.SiteID);
                var obfuscatedInApplicationPurchasePaymentProfile =
                    paymentProfileResponse.PaymentProfile as ObfuscatedInApplicationPurchasePaymentProfile;
            if (null != obfuscatedInApplicationPurchasePaymentProfile)
            {
                encodedReceipt = obfuscatedInApplicationPurchasePaymentProfile.ReceiptID;
            }
            }
            finally
            {
                ServiceClientFactory.ClosePaymentProfileClient();
            }
            return encodedReceipt;
        }

        private string GetAndroidSubscriptionToken(Brand brand, int memberId)
        {
            var subscriptionToken = string.Empty;
            try
            {
                var paymentProfileMapperClient = ServiceClientFactory.GetPaymentProfileClient();
            var paymentProfileResponse = paymentProfileMapperClient.GetObsfucatedPaymentProfileByMemberID(memberId, brand.Site.SiteID);
            var obfuscatedGooglePlayPaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedGooglePlayPaymentProfile;
            if (null != obfuscatedGooglePlayPaymentProfile)
            {
                subscriptionToken = obfuscatedGooglePlayPaymentProfile.ReceiptID;
            }
            }
            finally
            {
                ServiceClientFactory.ClosePaymentProfileClient();
            }
            return subscriptionToken;
        }
    }
}
