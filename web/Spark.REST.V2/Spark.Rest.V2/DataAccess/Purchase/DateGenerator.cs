﻿using System;

namespace Spark.Rest.DataAccess.Purchase
{
    public interface IDateGenerator
    {
        DateTime LocalNow { get; }
        DateTime UTCNow { get; }
    }

    public class DateGenerator : IDateGenerator
    {
        public readonly static IDateGenerator Singleton = new DateGenerator();
        //private int _dateOffset = -15; // 15 minute grace period for privilege expiration
        private int _dateOffset = 0; // Fix for IAP renewals JS-1610

        private DateGenerator(){}

        public DateTime LocalNow
        {
            get { return DateTime.Now.AddMinutes(_dateOffset); }
        }

        public DateTime UTCNow
        {
            get { return DateTime.UtcNow.AddMinutes(_dateOffset); }
        }
    }
}