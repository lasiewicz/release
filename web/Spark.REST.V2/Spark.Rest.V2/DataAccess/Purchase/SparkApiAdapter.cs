using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Hosting;
using Google;
using Google.Apis.AndroidPublisher.v2;
using Google.Apis.AndroidPublisher.v2.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Matchnet.Purchase.ValueObjects;
using Spark.Common.Adapter;
using Spark.Common.CatalogService;
using Spark.Logger;
using Spark.REST.Entities.Purchase;
using Spark.REST.Helpers;
using Spark.Rest.Helpers;
using Spark.Rest.V2.Entities.Purchase;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Helpers;
using Brand = Matchnet.Content.ValueObjects.BrandConfig.Brand;
using PaymentType = Spark.Common.PurchaseService.PaymentType;

namespace Spark.Rest.DataAccess.Purchase
{
    public interface ISparkAPIAdapter
    {
        IOSReceipt GetIOSReceipt(Brand brand, string encodedReceiptData, int memberId);

        APIAdapter.RestStatus MakeIOSApiCallBySiteId(string encodedReceiptData, out IOSReceipt iosReceipt, int siteId);
        APIAdapter.RestStatus MakeIOSApiCallByClientId(string encodedReceiptData, out IOSReceipt iosReceipt, int clientId);

        APIAdapter.RestStatus MakeAndroidApiCallBySiteId(string subscriptionToken, out AndroidSubscription androidSubscription, int siteId, GoogleApiCall googleApiCall);
        APIAdapter.RestStatus MakeAndroidApiCallByClientId(string subscriptionToken, out AndroidSubscription androidSubscription, int clientId, GoogleApiCall googleApiCall);
    }

    public enum GoogleApiCall
    {
        /// <summary>
        /// Gets current Google Play subscription status
        /// </summary>
        Get=1,
        /// <summary>
        /// Turns off AutoRenewal for current Google Play subscription
        /// </summary>
        Cancel=2,
        /// <summary>
        /// Credit for current Google Play subscription
        /// </summary>
        Revoke = 3
    }

    public class SparkApiAdapter : ISparkAPIAdapter
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(PurchaseAccess));

        private static Dictionary<int, string> IOS_ClientIdsAndPasswords = new Dictionary<int, string>();
        private static Dictionary<int, string> IOS_SiteIdsAndPasswords = new Dictionary<int, string>();
        private static Dictionary<int, string> ANDROID_ClientIdsAndServiceAccounts = new Dictionary<int, string>();
        private static Dictionary<int, string> ANDROID_SiteIdsAndServiceAccounts = new Dictionary<int, string>();
        private static Dictionary<int, X509Certificate2> ANDROID_ClientIdsAndServiceCerts = new Dictionary<int, X509Certificate2>();
        private static Dictionary<int, X509Certificate2> ANDROID_SiteIdsAndServiceCerts = new Dictionary<int, X509Certificate2>();
        private const string IOS_JDATE_APP_PASS = "7712c691ab8141a6adcac6b717e5ef7d";
        private const string IOS_JDATE_IL_APP_PASS = "6d14b70c789b4dc68aa02eccee500f71";
        private const string IOS_CM_APP_PASS = "6890e32808d04e8f97317407fc9fc3b6";
        private IDateGenerator _dateGenerator = null;
        private ISparkServiceClientFactory _serviceClientFactory = null;


        private readonly X509Certificate2 CM_ANDROID_SERVICE_ACCOUNT_CERTIFICATE; 
        private const string CM_ANDROID_SERVICE_ACCOUNT_EMAIL = "20372736972-jm6qljf0kac38lsa0rhu26dro1kcen78@developer.gserviceaccount.com";

        private readonly X509Certificate2 JDATE_ANDROID_SERVICE_ACCOUNT_CERTIFICATE;
        private const string JDATE_ANDROID_SERVICE_ACCOUNT_EMAIL = "220224101612-ai26nvkt9r35eso84cmm74343dcsctnk@developer.gserviceaccount.com";

        public static readonly SparkApiAdapter Singleton = new SparkApiAdapter();

        public IDateGenerator DateGenerator
        {
            get { return _dateGenerator ?? (_dateGenerator = Purchase.DateGenerator.Singleton); }
            set { _dateGenerator = value; }
        }

        public ISparkServiceClientFactory ServiceClientFactory
        {
            get { return _serviceClientFactory ?? (_serviceClientFactory = SparkServiceClientFactory.Singleton); }
            set { _serviceClientFactory = value; }
        }

        private SparkApiAdapter()
        {
            //IOS
            //client ids and app secrets
            IOS_ClientIdsAndPasswords[999] = IOS_JDATE_APP_PASS;
            IOS_ClientIdsAndPasswords[1013] = IOS_JDATE_IL_APP_PASS;
            IOS_ClientIdsAndPasswords[352799] = IOS_CM_APP_PASS;
            //site ids and app secrets
            IOS_SiteIdsAndPasswords[103] = IOS_JDATE_APP_PASS;
            IOS_SiteIdsAndPasswords[4] = IOS_JDATE_IL_APP_PASS;
            IOS_SiteIdsAndPasswords[9081] = IOS_CM_APP_PASS;

            //ANDROID
            // If this gives FileNotFoundException see http://stackoverflow.com/questions/14263457/
            CM_ANDROID_SERVICE_ACCOUNT_CERTIFICATE = new X509Certificate2(HostingEnvironment.ApplicationPhysicalPath + "cmKey.p12", "notasecret",
                X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);

            JDATE_ANDROID_SERVICE_ACCOUNT_CERTIFICATE = new X509Certificate2(HostingEnvironment.ApplicationPhysicalPath + "jdateUSKey.p12", "notasecret",
                X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);

            //client ids and app secrets
            ANDROID_ClientIdsAndServiceAccounts[378836] = CM_ANDROID_SERVICE_ACCOUNT_EMAIL;
            ANDROID_ClientIdsAndServiceCerts[378836] = CM_ANDROID_SERVICE_ACCOUNT_CERTIFICATE;

            //TODO: Create api client id and secret for jdate/il android app
            ANDROID_ClientIdsAndServiceAccounts[1030] = JDATE_ANDROID_SERVICE_ACCOUNT_EMAIL;
            ANDROID_ClientIdsAndServiceCerts[1030] = JDATE_ANDROID_SERVICE_ACCOUNT_CERTIFICATE;

            //site ids and app secrets
            ANDROID_SiteIdsAndServiceAccounts[9081] = CM_ANDROID_SERVICE_ACCOUNT_EMAIL;
            ANDROID_SiteIdsAndServiceCerts[9081] = CM_ANDROID_SERVICE_ACCOUNT_CERTIFICATE;

            ANDROID_SiteIdsAndServiceAccounts[103] = JDATE_ANDROID_SERVICE_ACCOUNT_EMAIL;
            ANDROID_SiteIdsAndServiceCerts[103] = JDATE_ANDROID_SERVICE_ACCOUNT_CERTIFICATE;
        }

        public IOSReceipt GetIOSReceipt(Brand brand, string encodedReceiptData, int memberId)
        {
            IOSReceipt iosReceipt = new IOSReceipt();
            APIAdapter.RestStatus restStatus = MakeIOSApiCallBySiteId(encodedReceiptData, out iosReceipt, brand.Site.SiteID);
            if (restStatus.StatusCode != HttpStatusCode.OK)
            {
                throw new SparkAPIException(string.Format("Error: 'Apple IOS', Status Code:{0}, MemberId:{1}, BrandId:{2}, ErrorMessage:{3}, Response from Apple:{4}",
                    restStatus.StatusCode, memberId, brand.BrandID, restStatus.ErrorMessage, restStatus.Content));
            }
            if (iosReceipt.status != 0 && iosReceipt.status != 21006)
            //0 == valid receipt, 21006 == valid receipt with expired subscription
            {
                throw new SparkAPIException(
                    string.Format("Error: 'Apple IOS Receipt', Status Code:{0}, MemberId:{1}, BrandId:{2}", iosReceipt.status, memberId, brand.BrandID));
            }
            bool noLatestReceiptInfo = (null == iosReceipt.latest_receipt_info || iosReceipt.latest_receipt_info.Count == 0);
            bool noInApp = (null == iosReceipt.receipt || null == iosReceipt.receipt.in_app || iosReceipt.receipt.in_app.Count == 0);
            if (noLatestReceiptInfo && noInApp)
            {
                throw new SparkAPIException(string.Format("No in app receipts for memberId:{0}, brandId:{1}", memberId, brand.BrandID));
            }
            Log.LogInfoMessage(string.Format("MemberId:{0}, Rest Status Code:{1}, Rest Status Error:'{2}', IOS Status:{3}", memberId, restStatus.StatusCode, restStatus.ErrorMessage, iosReceipt.status), ErrorHelper.GetCustomData());
            //check for non-renewable receipts. if non-renewable, calculate expiration date.
            try
            {
                Dictionary<string, Package> skuPackageMap = new Dictionary<string, Package>();
                //process latest receipt info array
                if (!noLatestReceiptInfo)
                {
                    CatalogServiceClient catalogServiceClient = ServiceClientFactory.GetCatalogServiceClient();
                    ProcessInAppInfoList(iosReceipt.latest_receipt_info, skuPackageMap, catalogServiceClient);
                    //Sort LatestReceiptInfo list by expires date
                    iosReceipt.latest_receipt_info.Sort();
                }

                //process in app array
                if (!noInApp)
                {
                    CatalogServiceClient catalogServiceClient = ServiceClientFactory.GetCatalogServiceClient();
                    ProcessInAppInfoList(iosReceipt.receipt.in_app, skuPackageMap, catalogServiceClient);
                    //Sort InApp list by expires date
                    iosReceipt.receipt.in_app.Sort();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(
                    string.Format("Could not get Decoded IOS Receipt for member:{0}, brand:{1}, encodedReceipt:{2}",
                        memberId, brand.BrandID, encodedReceiptData), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
            }
            finally
            {
                ServiceClientFactory.CloseCatalogServiceClient();
            }

            return iosReceipt;
        }

        private void ProcessInAppInfoList(IEnumerable<IOSInAppInfo> iosInAppInfoList, Dictionary<string, Package> skuPackageMap, CatalogServiceClient catalogServiceClient)
        {
            foreach (IOSInAppInfo inAppInfo in iosInAppInfoList)
            {
                string externalPackageId = string.Empty;
                try
                {
                    if (string.IsNullOrEmpty(inAppInfo.expires_date))
                    {
                        Package packageDetailsFromExternalPackageId;
                        externalPackageId = inAppInfo.product_id;
                        if (!skuPackageMap.ContainsKey(externalPackageId))
                        {
                            packageDetailsFromExternalPackageId =
                                catalogServiceClient.GetPackageDetailsFromExternalPackageID(externalPackageId,
                                    PaymentType.InApplicationPurchase.ToString());
                            skuPackageMap[externalPackageId] = packageDetailsFromExternalPackageId;
                        }
                        else
                        {
                            packageDetailsFromExternalPackageId = skuPackageMap[externalPackageId];
                        }
                        Item item = packageDetailsFromExternalPackageId.Items.First();
                        int duration = item.Duration;
                        DurationType durationType = item.DurationType;
                        switch (durationType)
                        {
                            case DurationType.Minutes:
                                inAppInfo.ExpiresDateUTC =
                                    new DateTime(inAppInfo.PurchaseDateUTC.Ticks).AddMinutes(duration);
                                break;
                            case DurationType.Hour:
                                inAppInfo.ExpiresDateUTC =
                                    new DateTime(inAppInfo.PurchaseDateUTC.Ticks).AddHours(duration);
                                break;
                            case DurationType.Day:
                                inAppInfo.ExpiresDateUTC =
                                    new DateTime(inAppInfo.PurchaseDateUTC.Ticks).AddDays(duration);
                                break;
                            case DurationType.Week:
                                inAppInfo.ExpiresDateUTC =
                                    new DateTime(inAppInfo.PurchaseDateUTC.Ticks).AddDays(duration*7);
                                break;
                            case DurationType.Month:
                                inAppInfo.ExpiresDateUTC =
                                    new DateTime(inAppInfo.PurchaseDateUTC.Ticks).AddMonths(duration);
                                break;
                            case DurationType.Year:
                                inAppInfo.ExpiresDateUTC =
                                    new DateTime(inAppInfo.PurchaseDateUTC.Ticks).AddYears(duration);
                                break;
                            default:
                                inAppInfo.ExpiresDateUTC = DateGenerator.UTCNow;
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.LogError(string.Format("Could not process sku:'{0}'", externalPackageId), e, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                }
            }
        }

        public APIAdapter.RestStatus MakeIOSApiCallBySiteId(string encodedReceiptData, out IOSReceipt iosReceipt, int siteId)
        {
            string iosIAPURL = ConfigurationManager.AppSettings["IOS_IAP_URL"];

            string appPassword = IOS_SiteIdsAndPasswords[siteId];

            var requestBody = new Dictionary<string, string>();
            requestBody["receipt-data"] = encodedReceiptData;
            requestBody["password"] = appPassword;

            return MakeIOSApiCall(iosIAPURL, "/verifyReceipt", requestBody, out iosReceipt);
        }

        public APIAdapter.RestStatus MakeIOSApiCallByClientId(string encodedReceiptData, out IOSReceipt iosReceipt, int clientId)
        {
            string iosIAPURL = ConfigurationManager.AppSettings["IOS_IAP_URL"];

            string appPassword = IOS_ClientIdsAndPasswords[clientId];

            var requestBody = new Dictionary<string, string>();
            requestBody["receipt-data"] = encodedReceiptData;
            requestBody["password"] = appPassword;

            return MakeIOSApiCall(iosIAPURL, "/verifyReceipt", requestBody, out iosReceipt);
        }

        private APIAdapter.RestStatus MakeIOSApiCall(string url, string urlPath, Dictionary<string, string> requestBody, out IOSReceipt iosReceipt)
        {
            APIAdapter.RestStatus restStatus = APIAdapter.Singleton.SendApiRequest(url, urlPath,
                WebRequestMethods.Http.Post, null, null,
                requestBody, out iosReceipt, "application/json", 0, false,
                ConfigurationManager.AppSettings["Environment"] != "prod");
            return restStatus;
        }

        public APIAdapter.RestStatus MakeAndroidApiCallBySiteId(string subscriptionToken, out AndroidSubscription androidSubscription, int siteId, GoogleApiCall googleApiCall)        
        {
            string serviceAccount = ANDROID_SiteIdsAndServiceAccounts[siteId];
            var cert = ANDROID_SiteIdsAndServiceCerts[siteId];

            var restStatus = MakeAndroidCall(subscriptionToken, out androidSubscription, serviceAccount, cert, googleApiCall);
            return restStatus;
        }

        public APIAdapter.RestStatus MakeAndroidApiCallByClientId(string subscriptionToken, out AndroidSubscription androidSubscription, int clientId, GoogleApiCall googleApiCall)
        {
            string serviceAccount = ANDROID_ClientIdsAndServiceAccounts[clientId];
            var cert = ANDROID_ClientIdsAndServiceCerts[clientId];

            var restStatus = MakeAndroidCall(subscriptionToken, out androidSubscription, serviceAccount, cert, googleApiCall);
            return restStatus;
        }

        private APIAdapter.RestStatus MakeAndroidCall(string subscriptionToken, out AndroidSubscription androidSubscription, string serviceAccount, X509Certificate2 cert, GoogleApiCall googleApiCall)
        {
            ServiceAccountCredential credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(serviceAccount)
                {
                    Scopes = new[] {AndroidPublisherService.Scope.Androidpublisher}
                }.FromCertificate(cert));

            var androidPublisherService = new AndroidPublisherService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
            });

            APIAdapter.RestStatus restStatus = new APIAdapter.RestStatus();
            androidSubscription = new AndroidSubscription();
            string[] tokenSegments = subscriptionToken.Split(new string[] {"|"}, StringSplitOptions.RemoveEmptyEntries);
            string packageName = tokenSegments[0];
            string subscriptionId = tokenSegments[1];
            string tokenId = tokenSegments[2];
            SubscriptionPurchase subscriptionPurchase=null;
            try
            {
                switch (googleApiCall)
                {
                    case GoogleApiCall.Cancel:
                        string cancelSubscription = androidPublisherService.Purchases.Subscriptions.Cancel(packageName, subscriptionId, tokenId).Execute();
                        break;
                    case GoogleApiCall.Revoke:
                        string revokeSubscription = androidPublisherService.Purchases.Subscriptions.Revoke(packageName, subscriptionId, tokenId).Execute();
                        break;
                    default:
                        break;
                }
                subscriptionPurchase = androidPublisherService.Purchases.Subscriptions.Get(packageName, subscriptionId, tokenId).Execute();
            }
            catch (GoogleApiException gae)
            {
                restStatus.StatusCode = gae.HttpStatusCode;
                restStatus.ErrorMessage = gae.Message;
                androidSubscription.Status = (int)gae.HttpStatusCode;
                androidSubscription.ErrorReason = gae.Message;               
            } catch (HttpUnhandledException he) {
                // No Json body was returned by the API.
                androidSubscription.Status = he.WebEventCode;
                androidSubscription.ErrorReason = he.Message;               
            } 

            if (null != subscriptionPurchase)
            {
                var expiryDateTime = SubscriptionHelper.ConvertEpochMillisToDateTime(subscriptionPurchase.ExpiryTimeMillis ?? 0);
                var startDateTime = SubscriptionHelper.ConvertEpochMillisToDateTime(subscriptionPurchase.StartTimeMillis ?? 0);                
                androidSubscription.Init(subscriptionPurchase.Kind, expiryDateTime, startDateTime, subscriptionPurchase.AutoRenewing ?? false, packageName, subscriptionId, tokenId);
                androidSubscription.Status = 0;
            }

            restStatus.StatusCode = HttpStatusCode.OK;
            return restStatus;
        }
    }
}