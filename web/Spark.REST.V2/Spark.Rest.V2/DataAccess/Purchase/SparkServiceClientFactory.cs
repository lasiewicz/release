using Matchnet.Helpers;
using Spark.Common.Adapter;
using Spark.Common.CatalogService;
using Spark.Common.PaymentProfileService;
using Spark.Common.PurchaseService;
using Spark.Common.RenewalService;

namespace Spark.Rest.DataAccess.Purchase
{
    public interface ISparkServiceClientFactory
    {
        IHttpContextHelper HTTPContextHelper { get; set; }

        PurchaseServiceClient GetPurchaseServiceClient();
        RenewalServiceClient GetRenewalServiceClient();
        CatalogServiceClient GetCatalogServiceClient();
        PaymentProfileMapperClient GetPaymentProfileClient();

        void ClosePurchaseServiceClient();
        void CloseRenewalServiceClient();
        void CloseCatalogServiceClient();
        void ClosePaymentProfileClient();
    }

    public class SparkServiceClientFactory : ISparkServiceClientFactory
    {
        public static readonly SparkServiceClientFactory Singleton = new SparkServiceClientFactory();
        private IHttpContextHelper _httpContextHelper = null;

        public IHttpContextHelper HTTPContextHelper
        {
            get { return _httpContextHelper ?? (_httpContextHelper = new HttpContextHelper()); }
            set { _httpContextHelper = value; }
        }

        private SparkServiceClientFactory(){}

        public PurchaseServiceClient GetPurchaseServiceClient()
        {
            return PurchaseServiceWebAdapter.GetProxyInstance(HTTPContextHelper.CurrentHttpContextBase);
        }

        public RenewalServiceClient GetRenewalServiceClient()
        {
            return RenewalServiceWebAdapter.GetProxyInstance(HTTPContextHelper.CurrentHttpContextBase);
        }

        public CatalogServiceClient GetCatalogServiceClient()
        {
            return CatalogServiceWebAdapter.GetProxyInstance(HTTPContextHelper.CurrentHttpContextBase);
        }

        public PaymentProfileMapperClient GetPaymentProfileClient()
        {
            return PaymentProfileServiceWebAdapter.GetProxyInstance(HTTPContextHelper.CurrentHttpContextBase);
        }

        public void ClosePurchaseServiceClient()
        {
            PurchaseServiceWebAdapter.CloseProxyInstance(HTTPContextHelper.CurrentHttpContextBase);
        }

        public void CloseRenewalServiceClient()
        {
            RenewalServiceWebAdapter.CloseProxyInstance(HTTPContextHelper.CurrentHttpContextBase);
        }

        public void CloseCatalogServiceClient()
        {
            CatalogServiceWebAdapter.CloseProxyInstance(HTTPContextHelper.CurrentHttpContextBase);
        }

        public void ClosePaymentProfileClient()
        {
            PaymentProfileServiceWebAdapter.CloseProxyInstance(HTTPContextHelper.CurrentHttpContextBase);
        }

    }
}