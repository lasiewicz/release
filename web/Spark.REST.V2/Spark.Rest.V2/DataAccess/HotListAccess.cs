﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Spark.Logger;
using Spark.Rest.BedrockPorts;
using Spark.Rest.Helpers;
using Spark.Rest.V2.Entities.HotList;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Helpers;
using Spark.Rest.V2.Models.HotList;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;
using Spark.REST.Entities.HotList;

#endregion

namespace Spark.REST.DataAccess
{
    /// <summary>
    /// 
    /// </summary>
    public enum HotListResultType
    {
        /// <summary>
        /// The mini profile
        /// </summary>
        MiniProfile = 1,
        /// <summary>
        /// The attribute set
        /// </summary>
        AttributeSet = 2
    }

    internal static class HotListAccess
    {
        private const int MaxHotListEntriesForNewCount = 100;
        public const string ATTR_BASE_LastHotListPageVisit = "LastHotListPageVisit-";
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(HotListAccess));
		private const string LIST_NOTIFICATIONS_ENABLED = "LIST_NOTIFICATIONS_ENABLED";

        private static readonly ISparkLogger TimingLog = SparkLoggerManager.Singleton.GetSparkLogger("RollingTimingLogFileAppender");

        /// <summary>
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberId"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        internal static Dictionary<HotListCategory, int> GetHotListTotalCounts(Brand brand, int memberId, List<HotListCategory> categories)
        {
            // todo: refactor to MT
            var hotListCounts = new Dictionary<HotListCategory, int>();
            var memberListCounts = ListSA.Instance.GetListCounts(memberId,
                brand.Site.Community.CommunityID, brand.Site.SiteID);
            foreach (var category in categories)
            {
                try
                {
                    var count = memberListCounts[category];
                    hotListCounts.Add(category, count);
                }
                catch
                {
                    hotListCounts.Add(category, 0);
                }
            }
            return hotListCounts;
        }

        /// <summary>
        ///     Updates the last time this hotlist was viewed.
        ///     Used to track the count of unread hotlist entries.
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberId"></param>
        /// <param name="category"></param>
        internal static DateTime UpdateViewedHotListTimeStamp(Brand brand, int memberId, HotListCategory category)
        {
            var sw = new Stopwatch();
            var lastVisitAttributeIds = new List<string>();
            switch (category)
            {
                case HotListCategory.FavoritesCombined:
                    lastVisitAttributeIds.Add(ATTR_BASE_LastHotListPageVisit + HotListCategory.WhoAddedYouToTheirFavorites);
                    break;
                case HotListCategory.TeasedCombined:
                    lastVisitAttributeIds.Add(ATTR_BASE_LastHotListPageVisit + HotListCategory.WhoTeasedYou);
                    break;
                case HotListCategory.ViewedCombined:
                    lastVisitAttributeIds.Add(ATTR_BASE_LastHotListPageVisit + HotListCategory.WhoViewedYourProfile);
                    break;
                default:
                    lastVisitAttributeIds.Add(ATTR_BASE_LastHotListPageVisit + category);
                    break;
            }
            
            sw.Start();
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);

            DateTime lastDate=DateTime.Now;
            foreach (var lastVisitAttributeId in lastVisitAttributeIds)
            {
                TimingLog.LogInfoMessage(
                    string.Format("GetMember(memberId: {1}, MemberLoadFlags.None) completed in {0} ms",
                        sw.ElapsedMilliseconds, memberId), new Dictionary<string, string>());
                member.SetAttributeDate(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID,
                    lastVisitAttributeId, DateTime.Now);

                sw.Reset();
                sw.Start();
                MemberSA.Instance.SaveMember(member);
                TimingLog.LogInfoMessage(
                    string.Format("SaveMember(memberid: {1}) completed in {0} ms ", sw.ElapsedMilliseconds,
                        member.MemberID),
                    null);

                sw.Reset();
                sw.Start();
                lastDate = member.GetAttributeDate(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID,
                    lastVisitAttributeId, DateTime.MinValue);

                TimingLog.LogInfoMessage(
                    string.Format(
                        "GetAttributeDate(communityID: {1}, siteId:{2}, brandId: {3}, lastVisitAttributeId: {4}, defaultValue: {5}) completed in {0} ms",
                        sw.ElapsedMilliseconds,
                        brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID,
                        lastVisitAttributeId, DateTime.MinValue), null);
            }

            return lastDate;
        }

        internal static bool HasNewMail(Brand brand, int memberId)
        {
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            return member.GetAttributeBool(brand, "HasNewMail");
        }

        internal static Dictionary<HotListCategory, int> GetHotListNewCounts(Brand brand, int memberId, List<HotListCategory> categories)
        {
            var hotListCounts = new Dictionary<HotListCategory, int>(categories.Count);
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);

            var ctx = System.Web.HttpContext.Current;
            Parallel.ForEach(categories, ParallelTasksHelper.GetMaxDegreeOfParallelism() , category =>
            {
                //don't process combined categories. this prevents the list viewed date from being updated
                if (category == HotListCategory.ViewedCombined || category == HotListCategory.FavoritesCombined || category == HotListCategory.TeasedCombined)
                {
                    return;
                }

                //set HttpContext for thread
                System.Web.HttpContext.Current = ctx;
                try
                {
                    var count = 0;
                    var lastVisitAttributeId = ATTR_BASE_LastHotListPageVisit + category;
                    var attributeIsSet = false;
                    var today = DateTime.Now;
                    var lastVisit = today;
                    try
                    {
                        var sw = new Stopwatch();
                        sw.Start();
                        lastVisit = member.GetAttributeDate(brand.Site.Community.CommunityID, brand.Site.SiteID,
                            brand.BrandID, lastVisitAttributeId, lastVisit);
                        attributeIsSet = true;
                        sw.Stop();

                        TimingLog.LogInfoMessage(string.Format(
                            "member.GetAttributeDate(communityId: {0}, siteId: {1}, brandId: {2}, attributeName: {3}, defaultValue: {4} ) completed in {5} ms",
                            brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, lastVisitAttributeId, lastVisit, sw.ElapsedMilliseconds), null);

                    }
                    catch (Exception ex)
                    {
                        TimingLog.LogInfoMessage(string.Format(
                            "Failed to get last visit date for member {0}: member.GetAttributeDate(communityId: {1}, siteId: {2}, brandId: {3}, attributeName: {4}, defaultValue: {5} )",
                            memberId, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, lastVisitAttributeId, lastVisit), null);

                        Log.LogError(ex.Message, ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                    }

                    if (attributeIsSet)
                    {
                        var hotlistEntries = GetHotlistMembers(memberId, category, brand, MaxHotListEntriesForNewCount, 1, false);
                        count = hotlistEntries.Count(entry => entry.ActionDate > lastVisit);
                    }
                    else
                    {
                        UpdateViewedHotListTimeStamp(brand, memberId, category);
                    }

                    lock (hotListCounts)
                    {
                        if (!hotListCounts.ContainsKey(category))
                            hotListCounts.Add(category, count);
                    }
                }
                finally
                {
                    //unset HttpContext for thread (precaution for GC)
                    System.Web.HttpContext.Current = null;                    
                }
            });
            return hotListCounts;
        }

        internal static List<HotListEntry> GetHotlistMembers(int memberId, HotListCategory category, Brand brand, int pageSize, int pageNumber, bool decorateMiniProfiles)
        {
            var stopwatch = new Stopwatch();

            stopwatch.Start();

            var entries = GetHotlistMembersInParallel(memberId, category, brand, pageSize, pageNumber, decorateMiniProfiles, HotListResultType.MiniProfile);

            stopwatch.Stop();
            TimingLog.LogInfoMessage(
                string.Format(
                    "{0} - GetHotlistMembers(memberId: {1}, category: {2}, brand(id):{3}, pageSize: {4}, pageNumber: {5}, decoratedMiniProfiles: {6} ResultType: miniprofile",
                    stopwatch.ElapsedMilliseconds, memberId, category, brand == null ? "null" : brand.BrandID.ToString(CultureInfo.InvariantCulture), pageSize, pageNumber,
                    decorateMiniProfiles), null);

            var hotListEntries = entries.Cast<HotListEntry>().ToList();
            return hotListEntries;
        }

        internal static List<HotListEntryV2> GetHotlistMembersV2(int memberId, HotListCategory category, Brand brand, int pageSize, int pageNumber, bool decorateMiniProfiles)
        {
            var entries = GetHotlistMembersInParallel(memberId, category, brand, pageSize, pageNumber, decorateMiniProfiles, HotListResultType.AttributeSet);
            var hotListEntries = entries.Cast<HotListEntryV2>().ToList();
            return hotListEntries;
        }

        internal static List<HotListEntryV2> GetHotlistMembersMultiple(int memberId, List<HotListCategory> categoryList, Brand brand, int pageSize, int pageNumber, bool decorateMiniProfiles)
        {
            var entries = GetCombinedHotlistMembersInParallel(memberId, categoryList, brand, pageSize, pageNumber, decorateMiniProfiles);
            var hotListEntries = entries.Cast<HotListEntryV2>().ToList();
            return hotListEntries;
        }

        internal static ArrayList GetHotlistMemberIDs(int memberId, HotListCategory category, Brand brand, int pageSize, int pageNumber)
        {
            var memberListAccess = ListSA.Instance.GetList(memberId);

            if (memberListAccess == null) return null;
            var startRow = pageSize * (pageNumber - 1) + 1;
            int rowcount;

            var listMembers = memberListAccess.GetListMembers(category,
                brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                startRow,
                pageSize,
                out rowcount);

            return listMembers;
        }

        /// <summary>
        ///     A workdaround method to support the concept of combined or merged hot lists per Mobile's request.
        ///     Given a short period of time, instead of correct implementing in the SA, doing this in API.
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="categoryList">List of categories, must have more than one unless it's a "combined" hot list category</param>
        /// <param name="brand"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="decorateMiniProfiles"></param>
        /// <returns></returns>
        private static List<object> GetCombinedHotlistMembersInParallel(int memberId, List<HotListCategory> categoryList,
            Brand brand, int pageSize, int pageNumber, bool decorateMiniProfiles)
        {

            //max limit of categories
            if (categoryList.Count > 10)
            {
                throw new SparkAPIReportableException(HttpSub400StatusCode.ExceededMaxAllowed, "Maximum hotlist categories allowed is 10");
            }

            var sw = new Stopwatch();
            sw.Start();
            var memberListAccess = ListSA.Instance.GetList(memberId);
            sw.Stop();

            TimingLog.LogInfoMessage(string.Format("GetCombinedHotlistMembersInParallel ListSA.Instance.GetList(memberId: {0}) took {1} ms", memberId, sw.ElapsedMilliseconds),
                null);

            var startRow = pageSize * (pageNumber - 1) + 1;

            sw.Reset();
            sw.Start();

            int runningRowcount = 0;
            int rowcount;
            var listMembersDict = new Dictionary<HotListCategory, List<Int32>>();
            var duplicateCategoryList = new List<HotListCategory>();
            StringBuilder categorySB = new StringBuilder();

            int maxFetch = pageSize * pageNumber;
            if (maxFetch > 1000)
            {
                maxFetch = 1000;
            }

            foreach (HotListCategory category in categoryList)
            {
                if (!duplicateCategoryList.Contains(category))
                {
                    duplicateCategoryList.Add(category);
                    categorySB.Append(category.ToString() + ",");

                    // Let's get the member Ids as many as we can
                    switch (category)
                    {
                        case HotListCategory.FavoritesCombined:

                            var myFavorites = memberListAccess.GetListMembers(HotListCategory.Default,
                                brand.Site.Community.CommunityID,
                                brand.Site.SiteID,
                                1,
                                maxFetch,
                                out rowcount);
                            runningRowcount += rowcount;

                            var whoFavorites = memberListAccess.GetListMembers(HotListCategory.WhoAddedYouToTheirFavorites,
                                brand.Site.Community.CommunityID,
                                brand.Site.SiteID,
                                1,
                                maxFetch,
                                out rowcount);
                            runningRowcount += rowcount;

                            listMembersDict[HotListCategory.Default] = myFavorites.Cast<Int32>().ToList();
                            listMembersDict[HotListCategory.WhoAddedYouToTheirFavorites] = whoFavorites.Cast<Int32>().ToList();
                            break;

                        case HotListCategory.TeasedCombined:

                            var myTeased = memberListAccess.GetListMembers(HotListCategory.MembersYouTeased,
                                brand.Site.Community.CommunityID,
                                brand.Site.SiteID,
                                1,
                                maxFetch,
                                out rowcount);
                            runningRowcount += rowcount;

                            var whoTeased = memberListAccess.GetListMembers(HotListCategory.WhoTeasedYou,
                                brand.Site.Community.CommunityID,
                                brand.Site.SiteID,
                                1,
                                maxFetch,
                                out rowcount);
                            runningRowcount += rowcount;

                            listMembersDict[HotListCategory.MembersYouTeased] = myTeased.Cast<Int32>().ToList();
                            listMembersDict[HotListCategory.WhoTeasedYou] = whoTeased.Cast<Int32>().ToList();
                            break;

                        case HotListCategory.ViewedCombined:

                            var myViewed = memberListAccess.GetListMembers(HotListCategory.MembersYouViewed,
                                brand.Site.Community.CommunityID,
                                brand.Site.SiteID,
                                1,
                                maxFetch,
                                out rowcount);
                            runningRowcount += rowcount;

                            var whoViewed = memberListAccess.GetListMembers(HotListCategory.WhoViewedYourProfile,
                                brand.Site.Community.CommunityID,
                                brand.Site.SiteID,
                                1,
                                maxFetch,
                                out rowcount);
                            runningRowcount += rowcount;

                            listMembersDict[HotListCategory.MembersYouViewed] = myViewed.Cast<Int32>().ToList();
                            listMembersDict[HotListCategory.WhoViewedYourProfile] = whoViewed.Cast<Int32>().ToList();
                            break;

                        default:
                            var listMembers = memberListAccess.GetListMembers(category,
                                brand.Site.Community.CommunityID,
                                brand.Site.SiteID,
                                1,
                                maxFetch,
                                out rowcount);
                            runningRowcount += rowcount;

                            listMembersDict[category] = listMembers.Cast<Int32>().ToList();
                            break;
                    }
                }
            }

            if (listMembersDict.Count <= 1)
            {
                throw new SparkAPIReportableException(HttpSub400StatusCode.GenericError, "Only combined or multiple categories are allowed in this method.");
            }

            sw.Stop();

            TimingLog.LogInfoMessage(
                string.Format("GetCombinedHotlistMembersInParallel memberListAccess.GetListMembers(category:{0}, communityId: {1}, siteId: {2}, startRow: {3}, pageSize: {4}) completed in: {5} ms and returned a row count of {6}",
                    categorySB.ToString(), brand.Site.Community.CommunityID, brand.Site.SiteID, startRow, pageSize,
                    sw.ElapsedMilliseconds, runningRowcount), null);

            // Using a custom internal class (at the bottom of this .cs) 
            // since ListItemDetail does not contain MemberId property
            var sortedListMembers = new List<Tuple<HotListCategory, Int32, DateTime, ListItemDetail>>();

            // Load list details for each member in parallel. 
            // Each ditionary item contains a category and member Ids for that category.
            var members = sortedListMembers; // prevent access to modified closure.

            var ctx1 = System.Web.HttpContext.Current;
            Parallel.ForEach(listMembersDict, ParallelTasksHelper.GetMaxDegreeOfParallelism(), listMembers => Parallel.ForEach(listMembers.Value, listMemberId =>
            {
                //set HttpContext for thread
                System.Web.HttpContext.Current = ctx1;

                try
                {
                    // Using the key ensure retrieving for the correct category
                    var listDetail = memberListAccess.GetListItemDetail(listMembers.Key,
                        brand.Site.Community.CommunityID,
                        brand.Site.SiteID,
                        listMemberId);

                    // This sometimes comes back null, prevent.
                    if (listDetail == null) return;

                    lock (members)
                    {
                        members.Add(new Tuple<HotListCategory, int, DateTime, ListItemDetail>(listMembers.Key,
                            listMemberId, listDetail.ActionDate, listDetail));
                    }
                }
                finally
                {
                    //unset HttpContext for thread (precaution for GC)
                    System.Web.HttpContext.Current = null;
                }
            }));

            // Sort the combined lists and order by date descending, member id is tiebreaker
            sortedListMembers = sortedListMembers.OrderByDescending(x => x.Item3).ToList();

            // Extract the request page, zero based index.
            // Just return whatever's available if the page size is bigger than the total count.
            // There's no point calling GetRange is the above statement is true.
            if (sortedListMembers.Count >= pageSize && pageNumber >= 1)
            {
                try
                {
                    // calculate final page size since it will be dynamic
                    int rangeStartIndex = startRow - 1;
                    if (rangeStartIndex + pageSize > sortedListMembers.Count)
                    {
                        pageSize = sortedListMembers.Count - rangeStartIndex;
                    }

                    sortedListMembers = sortedListMembers.GetRange(rangeStartIndex, pageSize);
                }
                catch (ArgumentException)
                {
                    Log.LogWarningMessage(string.Format("GetCombinedHotlistMembersInParallel Invalid start row or page size was given. Total count:{0} StartRow:{1} PageSize:{2}",
                        sortedListMembers.Count, startRow, pageSize), ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));

                    // Return an empty result when there's no matching set.
                    return new List<object>();
                }
            }


            //NOTE: Parallel here can potential cause issues with processing web requests, may need to change this to batches or create custom thread processing
            //using a fixed array to preserve sort order during parallel processing
            //http://stackoverflow.com/questions/11273377/parallel-for-maintain-input-list-order-on-output-list
            var sortedListMemberArray = new HotListEntryV2[sortedListMembers.Count];
            // We have the members we want to return now, let's populate with mini profile data.
            var ctx2 = System.Web.HttpContext.Current;
            Parallel.For(0, sortedListMembers.Count, ParallelTasksHelper.GetMaxDegreeOfParallelism(), i =>
            {
                //set HttpContext for thread
                System.Web.HttpContext.Current = ctx2;

                try
                {
                    var sortedListMember = sortedListMembers[i];
                    if (sortedListMember == null) 
                        return;
                    // Check for missing list details
                    if (sortedListMember.Item3 == null) 
                        return;
                    // Check for bad MemberId
                    if (sortedListMember.Item2 < 0) 
                        return;

                    var entry = new HotListEntryV2
                    {
                        ActionDate = sortedListMember.Item3,
                        Category = sortedListMember.Item1.ToString(),
                        Comment = sortedListMember.Item4.Comment
                    };

                    if (decorateMiniProfiles)
                    {
                        var miniProfile = ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, sortedListMember.Item2,
                            memberId, "resultsetprofile").AttributeSet;
                        entry.MiniProfile = miniProfile;
                    }

                    sortedListMemberArray[i] = entry;
                }
                finally
                {
                    //unset HttpContext for thread (precaution for GC)
                    System.Web.HttpContext.Current = null;
                }
            });

            //return sorted hotlist entries
            var hotListEntries = new List<object>(sortedListMemberArray);
            return hotListEntries;
        }

        internal static List<object> GetHotlistMembersInParallel(int memberId, HotListCategory category, Brand brand,
            int pageSize, int pageNumber, bool decorateMiniProfiles, HotListResultType hotListResultType)
        {
            // Intercept here to process combined custom hot list or multiple categories.
            switch (category)
            {
                case HotListCategory.FavoritesCombined:
                case HotListCategory.TeasedCombined:
                case HotListCategory.ViewedCombined:
                    List<HotListCategory> categoryList = new List<HotListCategory>();
                    categoryList.Add(category);
                    return GetCombinedHotlistMembersInParallel(memberId, categoryList, brand, pageSize, pageNumber,
                        decorateMiniProfiles);
            }

            // For the rest of the normal hotlist categories
            var sw = new Stopwatch();
            sw.Start();
            var memberListAccess = ListSA.Instance.GetList(memberId);
            sw.Stop();

            TimingLog.LogInfoMessage(
                string.Format("GetCombinedHotlistMembersInParallel ListSA.Instance.GetList(memberId: {0}) took {1} ms", memberId, sw.ElapsedMilliseconds),
                null);

            var hotListEntries = new List<object>();

            int rowcount;
            var startRow = pageSize*(pageNumber - 1) + 1;

            sw.Reset();
            sw.Start();

            var listMembers = memberListAccess.GetListMembers(category,
                brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                startRow,
                pageSize,
                out rowcount);

            sw.Stop();

            TimingLog.LogInfoMessage(
                string.Format(
                    "GetCombinedHotlistMembersInParallel memberListAccess.GetListMembers(category:{0}, communityId: {1}, siteId: {2}, startRow: {3}, pageSize: {4}) completed in: {5} ms and returned a row count of {6}",
                    category, brand.Site.Community.CommunityID, brand.Site.SiteID, startRow, pageSize, sw.ElapsedMilliseconds, rowcount), null);

            if (listMembers == null) return hotListEntries;

            var ctx = System.Web.HttpContext.Current;
            Parallel.ForEach(listMembers.Cast<object>(), ParallelTasksHelper.GetMaxDegreeOfParallelism(), i =>
            {
                //set HttpContext for thread
                System.Web.HttpContext.Current = ctx;

                try
                {
                    var hotListMemberId = (int)i;
                    var listDetail = memberListAccess.GetListItemDetail(category,
                        brand.Site.Community.CommunityID,
                        brand.Site.SiteID,
                        hotListMemberId);

                    if (listDetail == null) return;

                    if (hotListResultType == HotListResultType.AttributeSet)
                    {
                        var entry = new HotListEntryV2
                        {
                            ActionDate = listDetail.ActionDate,
                            Category = category.ToString(),
                            Comment = listDetail.Comment
                        };

                        if (decorateMiniProfiles)
                        {
                            var miniProfile = ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, hotListMemberId,
                                memberId, "resultsetprofile").AttributeSet;
                            entry.MiniProfile = miniProfile;
                        }

                        lock (hotListEntries)
                            hotListEntries.Add(entry);
                    }
                    else
                    {
                        var entry = new HotListEntry
                        {
                            ActionDate = listDetail.ActionDate,
                            Category = category.ToString(),
                            Comment = listDetail.Comment
                        };

                        if (decorateMiniProfiles)
                        {
                            var miniProfile = ProfileAccess.GetMiniProfile(brand, hotListMemberId, memberId);
                            entry.MiniProfile = miniProfile;
                        }
                        lock (hotListEntries)
                            hotListEntries.Add(entry);
                    }
                }
                finally
                {
                    //unset HttpContext for thread (precaution for GC)
                    System.Web.HttpContext.Current = null;
                }
            });

            // Sort by last Favorited or ActionDate desc. 
            // The stored proc, mnMember..up_List_List does not sort
            // and the Sort() in List sorts by the Id. When we move to Couchbase, look into
            // adding sorting by the action date.
            if (hotListResultType == HotListResultType.AttributeSet)
            {
                hotListEntries.Sort((p1, p2) => -1 * DateTime.Compare(
                    ((HotListEntryV2) p1).ActionDate, ((HotListEntryV2) p2).ActionDate));
            }
            else
            {
                hotListEntries.Sort((p1, p2) => -1 * DateTime.Compare(
                    ((HotListEntry) p1).ActionDate, ((HotListEntry) p2).ActionDate));
            }

            return hotListEntries;
        }

        internal static List<object> GetHotlistMembers(int memberId, HotListCategory category, Brand brand, int pageSize,
            int pageNumber, bool decorateMiniProfiles, HotListResultType hotListResultType)
        {
            var memberListAccess = ListSA.Instance.GetList(memberId);
            var hotListEntries = new List<object>();

            int rowcount;
            var startRow = pageSize*(pageNumber - 1) + 1;

            var listMembers = memberListAccess.GetListMembers(category, brand.Site.Community.CommunityID, brand.Site.SiteID, startRow, pageSize, out rowcount);

            if (listMembers == null) return hotListEntries;

            foreach (var listMember in listMembers)
            {
                var hotListMemberId = (int) listMember;
                var listDetail = memberListAccess.GetListItemDetail(category,
                    brand.Site.Community.CommunityID,
                    brand.Site.SiteID,
                    hotListMemberId);

                if (listDetail == null) continue;
                if (hotListResultType == HotListResultType.AttributeSet)
                {
                    var entry = new HotListEntryV2()
                    {
                        ActionDate = listDetail.ActionDate,
                        Category = category.ToString(),
                        Comment = listDetail.Comment
                    };

                    if (decorateMiniProfiles)
                    {
                        var miniProfile = ProfileAccess.GetAttributeSetWithPhotoAttributes(brand, hotListMemberId, memberId, "resultsetprofile").AttributeSet;
                        entry.MiniProfile = miniProfile;
                    }
                    hotListEntries.Add(entry);
                }
                else
                {
                    var entry = new HotListEntry
                    {
                        ActionDate = listDetail.ActionDate,
                        Category = category.ToString(),
                        Comment = listDetail.Comment
                    };

                    if (decorateMiniProfiles)
                    {
                        var miniProfile = ProfileAccess.GetMiniProfile(brand, hotListMemberId, memberId);
                        entry.MiniProfile = miniProfile;
                    }
                    hotListEntries.Add(entry);
                }
            }
            return hotListEntries;
        }

        internal static ListActionStatus AddToHotlist(Brand brand, HotListCategory hotListCategory, int memberId, int targetMemberId, string comment)
        {
            var sw = new Stopwatch();
            sw.Start();
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            sw.Stop();
            TimingLog.LogInfoMessage(string.Format("MemberSA.Instance.GetMember(memberId: {0}, MemberLoadFlags.None) took {1} ms", memberId, sw.ElapsedMilliseconds), null);
            var hideHotLists = ((AttributeOptionHideMask)member.GetAttributeInt(brand, "HideMask", 0)).HasFlag(AttributeOptionHideMask.HideHotLists);

            //Todo: Skip the check for MembersYouEmailed/MembersYouTeased categories as UpdateDate filed has to be updated with new datetime.
            if (MemberIsOnHotlist(brand, hotListCategory, memberId, targetMemberId))
            {
                return ListActionStatus.HasAlreadyAddedMemberToList;
            }

            try
            {
                var result = ListSA.Instance.AddListMember(hotListCategory, brand.Site.Community.CommunityID,
                    brand.Site.SiteID, memberId, targetMemberId, comment, Constants.NULL_INT, !hideHotLists, false);

                return result.Status;
            }
            catch (Exception ex)
            {
                Log.LogError("Failed to add member to hotlist", ex, ErrorHelper.GetCustomData(),ErrorHelper.ShouldLogExternally(brand));
                throw;
            }
        }

        internal static ListActionStatus AddToHotlist(Brand brand, HotListCategory hotListCategory, int memberId, int targetMemberId, string comment, DateTime timeStamp)
        {
            var sw = new Stopwatch();
            sw.Start();
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            sw.Stop();
            TimingLog.LogInfoMessage(string.Format("MemberSA.Instance.GetMember(memberId: {0}, MemberLoadFlags.None) took {1} ms", memberId, sw.ElapsedMilliseconds), null);
            var hideHotLists = ((AttributeOptionHideMask)member.GetAttributeInt(brand, "HideMask", 0)).HasFlag(AttributeOptionHideMask.HideHotLists);

            //Disable this check for list migration so UpdateDate filed will be updated with new datetime.
            //if (MemberIsOnHotlist(brand, hotListCategory, memberId, targetMemberId))
            //{
            //    return ListActionStatus.HasAlreadyAddedMemberToList;
            //}

            try
            {
                var result = ListSA.Instance.AddListMember(hotListCategory, brand.Site.Community.CommunityID,
                    brand.Site.SiteID, memberId, targetMemberId, comment, Constants.NULL_INT, timeStamp, !hideHotLists, false);

                return result.Status;
            }
            catch (Exception ex)
            {
                Log.LogError("Failed to add member to hotlist", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                throw;
            }
        }

        /// <summary>
        /// Adds to a listItem obj a note value.
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="hotListCategory"></param>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="comment"></param>
        /// <param name="timeStamp"></param>
        /// <returns></returns>
        /// <remarks>
        /// When saving we simply ignore quotas as we assume that the listItem exists already
        /// </remarks>
        internal static ListActionStatus AddHotlistNote(Brand brand, HotListCategory hotListCategory, int memberId,
            int targetMemberId, string comment, DateTime timeStamp)
        {
            const int hideHotListsValue = 2;
            var sw = new Stopwatch();
            sw.Start();
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            sw.Stop();
            TimingLog.LogInfoMessage(string.Format("MemberSA.Instance.GetMember(memberId: {0}, MemberLoadFlags.None) took {1} ms", memberId, sw.ElapsedMilliseconds), null);

            var hideHotLists = ((member.GetAttributeInt(brand, "HideMask", 0) & hideHotListsValue) == hideHotListsValue);

            try
            {
                var result = ListSA.Instance.AddListMember(hotListCategory, brand.Site.Community.CommunityID,
                    brand.Site.SiteID,
                    memberId, targetMemberId, comment, Constants.NULL_INT, timeStamp,
                    !hideHotLists, true); 

                return result.Status;
            }
            catch (Exception ex)
            {
                Log.LogError("Failed to add member to hotlist", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(brand));
                throw;
            }
        }


        internal static void RemoveFromHotlist(Brand brand, HotListCategory hotListCategory, int memberId,
            int favoriteMemberId)
        {
            ListSA.Instance.RemoveListMember(hotListCategory, brand.Site.Community.CommunityID,
                memberId, favoriteMemberId);
        }
        
        internal static bool MemberIsOnHotlist(Brand brand, HotListCategory hotListCategory, int listOwnerMemberId, int targetMemberId)
        {
            var sw = new Stopwatch();
            sw.Start();
            var memberListAccess = ListSA.Instance.GetList(listOwnerMemberId);
            sw.Stop();

            TimingLog.LogInfoMessage(string.Format("ListSA.Instance.GetList(memberId: {0}) took {1} ms", listOwnerMemberId, sw.ElapsedMilliseconds), null);

            //TODO: remove hardcoded page size, we need to add a new QoutaType
            //int maxAllowedFromList =
            //       QuotaDefinitionSA.Instance.GetQuotaDefinitions()
            //           .GetQuotaDefinition(QuotaType. , pCommunityID)
            //           .MaxAllowed;

            const int pageSize = 1000;
            var pageNumber = 1;
            ArrayList memberIds;
            sw.Reset();
            sw.Start();
            do
            {
                var startRow = pageSize*(pageNumber - 1) + 1;
                int rowcount;
                memberIds = memberListAccess.GetListMembers(hotListCategory,
                    brand.Site.Community.CommunityID,
                    brand.Site.SiteID,
                    startRow,
                    pageSize,
                    out rowcount);

                if (memberIds != null)
                {
                    if (memberIds.Cast<int>().Any(hotListMemberId => hotListMemberId == targetMemberId))
                    {
                        sw.Stop();
                        TimingLog.LogInfoMessage(string.Format("MemberIsOnHotlist: GettingListMembers and finding targetMember took {0} ms", sw.ElapsedMilliseconds), null);
                        return true; // found a match
                    }
                }
                pageNumber++;
            } while (memberIds != null && memberIds.Count > 0);
            sw.Stop();
            TimingLog.LogInfoMessage(string.Format("MemberIsOnHotlist: GettingListMembers took {0} ms, no member found ", sw.ElapsedMilliseconds), null);
            return false;
        }

        /// <summary>
        ///     Update hotlists of each member to show that the viewer has looked at the member's profile
        ///     (logic taken from \bedrock\web\bedrock.matchnet.com\Applications\MemberProfile\ViewProfile.ascx.cs)
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="viewedMemberId">member being viewed</param>
        /// <param name="viewerMemberId">viewing member</param>
        /// <returns>true if update succeeded</returns>
        public static bool SendHotListEntry(Brand brand, int viewedMemberId, int viewerMemberId)
        {
            if (viewedMemberId == viewerMemberId) return false; // don't update if looking at self
            
            //TODO: we can also use a tasks here
            var viewerMember = MemberSA.Instance.GetMember(viewerMemberId, MemberLoadFlags.None);
            var viewedMember = MemberSA.Instance.GetMember(viewedMemberId, MemberLoadFlags.None);
            
            if (viewerMember == null || viewedMember == null) return false; // bad user, can't continue

            Log.LogDebugMessage(string.Format("updating hotlist and sending notification, logged in user {0} {1}, viewed user {2} {3}",
                viewerMemberId, viewerMember.GetUserName(brand),
                viewedMemberId, viewedMember.GetUserName(brand)), ErrorHelper.GetCustomData());
            
            var hideMask = viewerMember.GetAttributeInt(brand, "HideMask", 0);
            var viewerIsHidden = (hideMask & (int) AttributeOptionHideMask.HideHotLists) ==
                                 (int) AttributeOptionHideMask.HideHotLists;

            ListSA.Instance.AddListMember(HotListCategory.MembersYouViewed,
                brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                viewerMemberId,
                viewedMemberId,
                null,
                Constants.NULL_INT,
                !viewerIsHidden,
                false);

            var notificationsEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_USER_NOTIFICATIONS",
                brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                brand.BrandID));

            if (notificationsEnabled)
            {
                UserNotificationAccess.SendUserNotification(brand, viewedMember, viewerMember, viewerIsHidden);
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="memberId"></param>
        internal static void SaveYNMVote(YNMVoteRequest request, int memberId)
        {
            ClickListType voteType;

            if (request.Yes)
                voteType = ClickListType.Yes;
            else if (request.No)
                voteType = ClickListType.No;
            else
                voteType = ClickListType.Maybe;

            ListSA.Instance.AddClick(request.Brand.Site.Community.CommunityID,
                request.Brand.Site.SiteID,
                request.Brand.Site.LanguageID,
                memberId,
                request.AddMemberId,
                voteType);

            // Disable notifications for CM Lists
            if (AreNotificationsEnabled(request.Brand.Site.Community.CommunityID))
                UserNotificationAccess.AddUserNotificationForYNM(memberId, request.AddMemberId, request.Brand);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="memberId"></param>
        internal static void SaveYNMVote(Brand brand, YNMVoteRequest request, int memberId)
        {
            ClickListType voteType;

            if (request.Yes)
                voteType = ClickListType.Yes;
            else if (request.No)
                voteType = ClickListType.No;
            else
                voteType = ClickListType.Maybe;

            ListSA.Instance.AddClick(brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                brand.Site.LanguageID,
                memberId,
                request.AddMemberId,
                voteType, request.Timestamp > DateTime.MinValue? request.Timestamp: DateTime.Now);

            // Disable notifications for CM Lists
            if (AreNotificationsEnabled(brand.Site.Community.CommunityID))
                UserNotificationAccess.AddUserNotificationForYNM(memberId, request.AddMemberId, brand);
        }

        [Flags]
        private enum AttributeOptionHideMask
        {
            HideSearch = 1,
            HideHotLists = 2,
            HideMembersOnline = 4,
            HidePhotos = 8
        }

        internal static void SaveViralStatus(int communityId,  int memberId, int targetMemberId, bool alertEmailSent, bool clickEmailSent)
        {
            ListSA.Instance.SaveViralStatus(communityId, memberId, targetMemberId, alertEmailSent, clickEmailSent);
        }

        #region Contact History
        internal static List<ContactHistoryItem> GetContactHistory(int memberId, int targetMemberId, Brand brand)
        {
            List<ContactHistoryItem> listItems = new List<ContactHistoryItem>();

            Matchnet.List.ServiceAdapters.List memberList = ListSA.Instance.GetList(memberId, ListLoadFlags.None);

            if (HasAnyContactHistory(memberId, targetMemberId, brand, memberList))
            {
                List<HotListType> hotListTypeList = GetContactHistoryHotListTypes();
                Matchnet.List.ServiceAdapters.List targetMemberList = ListSA.Instance.GetList(targetMemberId, ListLoadFlags.None);

                foreach (HotListType hlType in hotListTypeList)
                {
                    //you're checking out
                    ContactHistoryItem sourceHistoryItem = GetContactHistoryItem(memberId, targetMemberId, brand, hlType, HotListDirection.OnYourList, memberList, targetMemberList);
                    if (sourceHistoryItem != null)
                        listItems.Add(sourceHistoryItem);

                    //checking you out
                    ContactHistoryItem targetHistoryItem = GetContactHistoryItem(memberId, targetMemberId, brand, hlType, HotListDirection.OnTheirList, memberList, targetMemberList);
                    if (targetHistoryItem != null)
                        listItems.Add(targetHistoryItem);

                    //check for "mutual"
                    if (sourceHistoryItem != null && hlType == HotListType.YNM)
                    {
                        //mutual yes
                        if (sourceHistoryItem.HistoryType == HotListCategory.MutualYes.ToString())
                        {
                            ContactHistoryItem historyItem = new ContactHistoryItem();
                            historyItem.ActionDate = DateTime.MaxValue;
                            historyItem.HistoryType = hlType.ToString();
                            historyItem.IsMutual = true;
                            listItems.Add(historyItem);
                        }
                    }
                    else if (sourceHistoryItem != null && targetHistoryItem != null)
                    {
                        //mutual whatever hotlist type
                        ContactHistoryItem historyItem = new ContactHistoryItem();
                        historyItem.ActionDate = DateTime.MaxValue;
                        historyItem.HistoryType = hlType.ToString();
                        historyItem.IsMutual = true;
                        listItems.Add(historyItem);
                    }
                }

                listItems = listItems.OrderByDescending(x => x.ActionDate).ToList();
            }

            return listItems;
        }

        public static List<HotListType> GetContactHistoryHotListTypes()
        {
            List<HotListType> hotListTypeList = new List<HotListType>();
            hotListTypeList.Add(HotListType.Friend);
            hotListTypeList.Add(HotListType.ViewProfile);
            hotListTypeList.Add(HotListType.Email);
            hotListTypeList.Add(HotListType.ECard);
            hotListTypeList.Add(HotListType.Tease);
            hotListTypeList.Add(HotListType.IM);
            hotListTypeList.Add(HotListType.Ignore); //you blocked target from contacting you
            hotListTypeList.Add(HotListType.ExcludeList); //you blocked target from showing up in your searches
            hotListTypeList.Add(HotListType.ExcludeFromList); //you blocked target from seeing you in their searches
            hotListTypeList.Add(HotListType.YNM);

            return hotListTypeList;
        }

        private static bool HasAnyContactHistory(int memberId, int targetMemberId, Brand brand, List memberList)
        {
            bool sourceList = false;
            bool targetList = false;

            List<HotListType> hotListTypeList = GetContactHistoryHotListTypes();
            foreach (HotListType hlType in hotListTypeList)
            {
                HasContactHistory(memberId, targetMemberId, brand, hlType, memberList, out sourceList, out targetList);
                if (sourceList || targetList)
                    return true;
            }

            return false;
        }

        private static ContactHistoryItem GetContactHistoryItem(int memberId, int targetMemberId, Brand brand, HotListType hotListType, HotListDirection hotlistDirection, List memberList, List targetMemberList)
        {
            ContactHistoryItem historyItem = null;
            HotListCategory category = HotListCategory.Default;

            if (hotListType == HotListType.YNM)
            {
                if (hotlistDirection == HotListDirection.OnYourList)
                {
                    historyItem = new ContactHistoryItem();

                    ClickMask clickMask = memberList.GetClickMask(brand.Site.Community.CommunityID, brand.Site.SiteID, targetMemberId);

                    if ((((clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes) && ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)))
                    {
                        category = HotListCategory.MembersClickedYes;
                    }
                    else if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)
                    {
                        category = HotListCategory.MembersClickedYes;
                    }
                    else if ((clickMask & ClickMask.MemberNo) == ClickMask.MemberNo)
                    {
                        category = HotListCategory.MembersClickedNo;
                    }
                    else if ((clickMask & ClickMask.MemberMaybe) == ClickMask.MemberMaybe)
                    {
                        category = HotListCategory.MembersClickedMaybe;
                    }
                    else
                    {
                        historyItem = null;
                    }

                    if (historyItem != null)
                    {
                        historyItem.HistoryType = category.ToString();

                        ListItemDetail listDetail = memberList.GetListItemDetail(category,
                                        brand.Site.Community.CommunityID,
                                        brand.Site.SiteID,
                                        targetMemberId);

                        if (listDetail != null)
                        {
                            historyItem.ActionDate = listDetail.ActionDate;
                        }
                    }
                }
                else
                {
                    //we don't show YNM from others' list
                }
            }
            else if ((hotListType == HotListType.Ignore || hotListType == HotListType.ExcludeFromList || hotListType == HotListType.ExcludeList) && hotlistDirection == HotListDirection.OnTheirList)
            {
                //we won't allow contact history for blocks on other people's list
                historyItem = null;
            }
            else
            {
                HotListCategory listCategory = ListInternal.MapListCategory(hotListType, hotlistDirection);

                if (hotListType != HotListType.Friend && listCategory == HotListCategory.Default)
                {
                    //this indicates the hotListType does not have any mapping
                    historyItem = null;
                }
                else
                {
                    ListItemDetail listDetail = memberList.GetListItemDetail(listCategory,
                                        brand.Site.Community.CommunityID,
                                        brand.Site.SiteID,
                                        targetMemberId);

                    if (listDetail != null)
                    {
                        bool isValid = true;
                        if (hotListType == HotListType.ViewProfile && hotlistDirection == HotListDirection.OnTheirList)
                        {
                            //check if target member has blocked you, don't show that they "viewed your profile"
                            if (HasTargetBlockedYou(memberId, targetMemberId, brand, targetMemberList))
                                isValid = false;
                        }

                        if (isValid)
                        {
                            historyItem = new ContactHistoryItem();
                            historyItem.ActionDate = listDetail.ActionDate;
                            historyItem.HistoryType = listCategory.ToString();
                        }
                        else
                        {
                            historyItem = null;
                        }
                    }
                }
            }

            return historyItem;

        }        

        private static void HasContactHistory(int memberId, int targetMemberId, Brand brand, HotListType hotListType, List memberList, out bool sourceList, out bool targetList)
        {
            if (memberList != null)
            {
                HotListCategory listCategorySource = ListInternal.MapListCategory(hotListType, HotListDirection.OnYourList);
                HotListCategory listCategoryTarget = ListInternal.MapListCategory(hotListType, HotListDirection.OnTheirList);

                if (hotListType != HotListType.Friend && listCategorySource == HotListCategory.Default && listCategoryTarget == HotListCategory.Default)
                {
                    //this indicates the hotListType does not have any mapping
                    sourceList = false;
                    targetList = false;
                }
                else
                {
                    //you're checking out
                    sourceList = memberList.IsHotListed(listCategorySource, brand.Site.Community.CommunityID, targetMemberId);
                    //checking you out
                    targetList = memberList.IsHotListed(listCategoryTarget, brand.Site.Community.CommunityID, targetMemberId);
                }
            }
            else
            {
                sourceList = false;
                targetList = false;
            }
        }

        private static bool HasTargetBlockedYou(int memberId, int targetMemberId, Brand brand, Matchnet.List.ServiceAdapters.List targetMemberList)
        {
            bool isblocked = targetMemberList.IsHotListed(HotListCategory.ExcludeFromList, brand.Site.Community.CommunityID, memberId);
            if (!isblocked)
            {
                isblocked = targetMemberList.IsHotListed(HotListCategory.IgnoreList, brand.Site.Community.CommunityID, memberId);
            }
            if (!isblocked)
            {
                isblocked = targetMemberList.IsHotListed(HotListCategory.ExcludeList, brand.Site.Community.CommunityID, memberId);
            }

            return isblocked;

        }



        #endregion

        private static bool AreNotificationsEnabled(int communityId)
        {
            var returnValue = true;
            try
            {
                Boolean.TryParse(RuntimeSettings.Instance.GetSettingFromSingleton(LIST_NOTIFICATIONS_ENABLED, communityId), out returnValue);
            }
            catch (Exception ex)
            {
                Log.LogError("Failed to get LIST_NOTIFICATIONS_ENABLED setting.", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
            }
            return returnValue;
        }
    }
}
