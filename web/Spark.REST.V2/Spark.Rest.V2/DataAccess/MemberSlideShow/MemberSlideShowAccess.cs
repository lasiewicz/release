﻿#region

using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.MemberSlideshow.ServiceAdapters;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.REST.Models.Search;
using GenderUtils = Matchnet.GenderUtils;
using MemberSearchCollection = Spark.SAL.MemberSearchCollection;

#endregion

namespace Spark.Rest.V2.DataAccess.MemberSlideShow
{
    /// <summary>
    /// </summary>
    public sealed class MemberSlideShowAccess : Controller
    {
        private static readonly MemberSlideShowAccess _instance = new MemberSlideShowAccess();
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MemberSlideShowAccess));

        private MemberSlideShowAccess()
        {
        }

        /// <summary>
        /// </summary>
        public static MemberSlideShowAccess Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// </summary>
        /// <param name="memberId"> </param>
        /// <param name="brand"> </param>
        /// <returns> next member to show or null if none found. </returns>
        public IMember GetValidMember(int memberId, Brand brand)
        {
            var slideshow = MemberSlideshowSA.Instance.GetSlideshow(memberId, brand);
            return MemberSlideshowSA.Instance.GetValidMemberFromSlideshow(slideshow, brand, true);
        }

        ///<summary>
        ///</summary>
        ///<param name="memberId"> </param>
        ///<param name="brand"> </param>
        ///<param name="request"> </param>
        ///<returns> </returns>
        public IMember GetValidMember(int memberId, Brand brand, SearchPreferencesRequest request)
        {
            var slideshow = GetSlideshow(memberId, brand, request);

            return MemberSlideshowSA.Instance.GetValidMemberFromSlideshow(slideshow, brand, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="brand"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<IMember> GetValidMembers(int memberId, Brand brand, SearchPreferencesRequest request)
        {
            var slideshow = GetSlideshow(memberId, brand, request);

            return MemberSlideshowSA.Instance.GetValidMembersFromSlideshow(slideshow, brand, true, request.BatchSize);
        }

        private static Slideshow GetSlideshow(int memberId, Brand brand, SearchPreferencesRequest request)
        {
            var primarySearch = MemberSearchCollection.Load(memberId, brand).PrimarySearch;
            if (primarySearch == null)
                throw new Exception(String.Format("Member search preferences cannot be null. MemberId:{0}", memberId));

            if (request.MinAge == 0) request.MinAge = primarySearch.Age.MinValue;
            if (request.MaxAge == 0) request.MaxAge = primarySearch.Age.MaxValue;
            if (request.MaxDistance == 0) request.MaxDistance = primarySearch.Distance;
            if (String.IsNullOrEmpty(request.Location))
            {
                request.Location = primarySearch.RegionID.ToString();
            }
            if (String.IsNullOrEmpty(request.Gender)) request.Gender = primarySearch.Gender.ToString();
            if (String.IsNullOrEmpty(request.SeekingGender))
                request.SeekingGender = primarySearch.SeekingGender.ToString();

            var gender = (GenderMask) Enum.Parse(typeof (GenderMask), request.Gender, true); // no reverse
            var genderSeeking = (GenderMask) Enum.Parse(typeof (GenderMask), "Seeking" + request.SeekingGender, true);
            var genderMask = (int) (gender | genderSeeking);
            genderMask = GenderUtils.FlipMaskIfHeterosexual(genderMask);
            var region = RegionSA.Instance.RetrievePopulatedHierarchy(Convert.ToInt32(request.Location),
                                                                      brand.Site.LanguageID);

            var searchPreferenceCollection = new SearchPreferenceCollection
                                                 {
                                                     {"GenderMask", genderMask.ToString("d")},
                                                     {"RegionID", request.Location},
                                                     {"DomainID", brand.Site.Community.CommunityID.ToString("d")},
                                                     {"SearchTypeID", SearchTypeID.Region.ToString("d")},
                                                     {"SearchOrderBy", QuerySorting.Proximity.ToString("d")},
                                                     {"Radius", request.MaxDistance.ToString("d")},
                                                     {"HasPhotoFlag", "1"}, // for now we always want members with photos
                                                     {"MinAge", request.MinAge.ToString("d")},
                                                     {"MaxAge", request.MaxAge.ToString("d")},
                                                     {"CountryRegionID", region.CountryRegionID.ToString("d")}
                                                 };

            Log.LogDebugMessage(string.Format(
                "Get slideshow with prefs GenderMask:{0}, RegionID:{1}, MinAge:{2}, MaxAge:{3}, CommunityId:{4}, Radius:{5}, CountryRegionId:{6}",
                genderMask, request.Location, request.MinAge, request.MaxAge,
                brand.Site.Community.CommunityID,
                request.MaxDistance, region.CountryRegionID), ErrorHelper.GetCustomData());

            // todo this removecache call needs to happen in general search too.
            if (request.IsNewPrefs)
                MemberSlideshowSA.Instance.RemoveFromCache(memberId, brand.Site.Community.CommunityID);
            var slideshow = MemberSlideshowSA.Instance.GetSlideshow(memberId, brand, searchPreferenceCollection, true);
            return slideshow;
        }
    }
}