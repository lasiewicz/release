﻿using System;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Common.Localization;
using Spark.Rest.V2.BedrockPorts;

namespace Spark.Rest.BedrockPorts
{
	public static class DateFormatter
	{
		public static string ElapsedTimeText(Brand brand, DateTime date)
		{
			var ago = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.LastLoginDate, "PRO_AGO");
            var minute = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo,ResourceGroupEnum.LastLoginDate, "PRO_MINUTE");
            var minutes = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.LastLoginDate, "PRO_MINUTES");
            var hour = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.LastLoginDate, "PRO_HOUR");
            var hours = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.LastLoginDate, "PRO_HOURS");
            var day = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.LastLoginDate, "PRO_DAY");
            var days = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.LastLoginDate, "PRO_DAYS");
            var before = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.LastLoginDate, "PRO_BEFORE");

			if (date != DateTime.MinValue)
			{
				var now = DateTime.Now;
				var timeSpan = now.Subtract(date);

				if (timeSpan.Days >= 60)
				{
					if (brand.Site.Direction.ToString() == "rtl")
					{
						return String.Format("{0} {1}", before,
                                             ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.LastLoginDate, "PRO_MORE_THAN_60_DAYS_AGO"));
					}
					else
					{

                        return ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.LastLoginDate, "PRO_MORE_THAN_60_DAYS_AGO");
					}
				}
				else if (timeSpan.Days >= 1)
				{
					var outday = (timeSpan.Days == 1) ? day : days;

					if (brand.Site.SiteID == (int)SITE_ID.JDateFR)
					{
						return String.Format("{0} {1} {2}", ago, timeSpan.Days, outday);
					}
					else if (brand.Site.Direction.ToString() == "rtl")
					{
						return String.Format("{0} {1} {2} {3}", before, timeSpan.Days, outday, ago);
					}
					else
					{
						return String.Format("{0} {1} {2}", timeSpan.Days, outday, ago);
					}
				}
				else if (timeSpan.Hours >= 1)
				{
					String outHour = (timeSpan.Hours == 1) ? hour : hours;

					if (brand.Site.SiteID == (int)SITE_ID.JDateFR)
					{
						return String.Format("{0} {1} {2}", ago, timeSpan.Hours, outHour);
					}
					else if (brand.Site.Direction.ToString() == "rtl")
					{
						return String.Format("{0} {1} {2} {3}", before, timeSpan.Hours, outHour, ago);
					}
					else
					{
						return String.Format("{0} {1} {2}", timeSpan.Hours, outHour, ago);
					}
				}
				else
				{
					int totalMinutes = (timeSpan.Minutes > 1) ? timeSpan.Minutes : 1;
					String outminute = (totalMinutes == 1) ? minute : minutes;

					if (brand.Site.SiteID == (int)SITE_ID.JDateFR)
					{
						return String.Format("{0} {1} {2}", ago, totalMinutes, outminute);
					}
					else if (brand.Site.Direction.ToString() == "rtl")
					{
						return String.Format("{0} {1} {2} {3}", before, totalMinutes, outminute, ago);
					}
					else
					{
						return String.Format("{0} {1} {2}", totalMinutes, outminute, ago);
					}
				}

			}
			else
			{
				return Constants.NULL_STRING;
			}
		}
	}
}