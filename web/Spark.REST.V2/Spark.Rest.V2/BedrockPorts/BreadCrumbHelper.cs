#region

using System;
using System.Configuration;
using System.Web;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;
using Spark.Rest.V2.BedrockPorts;

#endregion

namespace Spark.Rest.BedrockPorts
{
    /// <summary>
    ///   Contains methods and types to assist in maintaining
    ///   a breadcrumb trail.
    /// </summary>
    public class BreadCrumbHelper
    {
        #region EntryPoint enum

        /// <summary>
        ///   EntryPoint defines various types of entry points
        ///   into the application.
        /// </summary>
        public enum EntryPoint
        {
            /// <summary>
            ///   Search Results.
            /// </summary>
            SearchResults = 1,

            /// <summary>
            ///   Hot Lists.
            /// </summary>
            HotLists = 2,

            /// <summary>
            ///   Members Online.
            /// </summary>
            MembersOnline = 3,

            /// <summary>
            ///   Home Page.
            /// </summary>
            HomePage = 4,

            /// <summary>
            ///   Thumb Trail.
            /// </summary>
            ThumbTrail = 5,

            /// <summary>
            ///   Messages.
            /// </summary>
            Messages = 6,

            /// <summary>
            ///   Look Up Member.
            /// </summary>
            LookUpMember = 7,

            /// <summary>
            ///   ???
            /// </summary>
            AstroScope = 9,

            /// <summary>
            ///   Admin.
            /// </summary>
            AdminView = 8,

            /// <summary>
            ///   Photo Gallery.
            /// </summary>
            PhotoGallery = 10,

            /// <summary>
            ///   Quick Search Results.
            /// </summary>
            QuickSearchResults = 11, //11062008 TL - Added entry point to represent quick search

            /// <summary>
            ///   Keyword Search Results.
            /// </summary>
            KeywordSearchResults = 12,

            /// <summary>
            ///   Reverse Search Results.
            /// </summary>
            ReverseSearchResults = 13,
            /// <summary>
            ///   Question Answer page
            /// </summary>
            QuestionPage = 14,

            /// <summary>
            ///   MemberSlideshow
            /// </summary>
            MemberSlideshow = 15,

            /// <summary>
            ///   Advanced Search Results.
            /// </summary>
            AdvancedSearchResults = 16,
            /// <summary>
            ///   HomeHeroProfileMatches
            /// </summary>
            HomeHeroProfileMatches = 17,
            /// <summary>
            ///   HomeHeroProfileMOL
            /// </summary>
            HomeHeroProfileMOL = 18,
            /// <summary>
            /// Secret Admirer Page
            /// </summary>
            SlideShowPage = 19,
            /// <summary>
            ///   HomeHeroProfileMatches
            /// </summary>
            HomeHeroProfileFilmStripMatches = 20,
            /// <summary>
            /// HomeHeroProfileMOL
            /// </summary>
            HomeHeroProfileFilmStripMOL = 21,
            /// <summary>
            /// HomeHeroProfileMOL
            /// </summary>
            HomeHeroProfileFilmStripYourProfile = 22,
            /// <summary>
            ///   Clicked from MemberLike profile link.
            /// </summary>
            MemberLikeLink = 23,
            /// <summary>
            ///   Clicked from Member Profile.
            /// </summary>
            MemberProfile = 24,
            /// <summary>
            ///   This is from UserNotificationFactory, special case where memberID and g.Member are the same!
            /// </summary>
            NotificationCreate = 99998,
            /// <summary>
            /// Nada.
            /// </summary>
            NoArgs = 99999,

            /// <summary>
            ///   None provided.
            /// </summary>
            Unknown = 100000
        }

        #endregion

        #region PremiumEntryPoint enum

        [Flags]
        public enum PremiumEntryPoint
        {
            NoTracking = 0,
            NoPremium = 1,
            Highlight = 2,
            Spotlight = 4
        }

        #endregion

        #region Constants

        public const string DefaultNextProfileText = "&#187;";
        public const string ViewProfileBase = "/Applications/MemberProfile/ViewProfile.aspx";
        public const string JavascriptBackLink = "javascript:history.back();";

        private static class QuickSearchConstants
        {
            #region Nested type: Params

            public static class Params
            {
                public const string OrderBy = "MOOB";
                public const string GenderMask = "MOGM";
                public const string RegionId = "MOR";
                public const string AgeMinimum = "MOAM";
                public const string AgeMaximum = "MOAX";
                public const string LanguageMask = "MOLM";
            }

            #endregion
        }

        #endregion Constants

        #region Properties

        public int DomainId { get; private set; }

        public EntryPoint MyEntryPoint { get; private set; }

        public int MemberId { get; private set; }

        // public ContextGlobal G { get; private set; }

        public MOCollection MyMOCollection { get; private set; }

        public int HotListCategoryID { get; private set; }

        #endregion

        #region Constructors

        //public BreadCrumbHelper(EntryPoint myEntryPoint, ContextGlobal g, int memberID, MOCollection myMOCollection, int hotListCategoryID)
        //{
        //    DomainId = g.Brand.Site.Community.CommunityID;
        //    G = g;
        //    MemberId = memberID;
        //    MyMOCollection = myMOCollection;
        //    HotListCategoryID = hotListCategoryID;
        //    MyEntryPoint = myEntryPoint;
        //}

        #endregion

        private static string GetViewProfileLinkBase(Brand brand, int memberID, ParameterCollection parameters,
                                                     bool forceSameWindow)
        {
            return GetViewProfileLinkBase(brand, memberID, parameters, forceSameWindow, string.Empty, EntryPoint.Unknown,
                                          Constants.NULL_INT);
        }

        //string link = Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NotificationCreate, NotificationCreator.MemberID, 0, null, 0, false, (int)Matchnet.Web.Framework.Ui.BreadCrumbHelper.PremiumEntryPoint.NoPremium, "", NotificationOwner.MemberID);
        private static string GetViewProfileLinkBase(Brand brand, int memberID, ParameterCollection parameters,
                                                     bool forceSameWindow, string url, EntryPoint entryPoint,
                                                     int ownerMemberID)
        {
            // url fix
            string environment = ConfigurationManager.AppSettings["Environment"] ?? "prod";
            string env = "www";
            switch (environment.ToLower())
            {
                case "dev":
                case "local":
                    env = "local";
                    break;
                case "newstage2":
                    env = "web1-temp.stgv2";
                    break;
                case "newstage3":
                    env = "web1-temp.stgv3";
                    break;
                case "preprod":
                case "prod":
                default:
                    env = "www";
                    break;
            }

            var viewProfileBase = String.Format("http://{0}.{1}{2}", env, brand.Uri, ViewProfileBase);

            //04082008 TL - MPR-102 - Set default to true; Following logic will override it appropriately
            var newWindow = true;

            var layoutTemplate = LayoutTemplate.Wide2Columns;

            if (forceSameWindow)
            {
                newWindow = false;
            }

            //04082008 TL - MPR-102 - Check settings to disable profile popups
            //12042008 TL - Site Redesign Phase 2, disable profile popup for site2.0 members.
            //if (IsProfilePopupDisabled(memberID) || g.IsSite20Enabled)
            //{
            //    newWindow = false;
            //    layoutTemplate = LayoutTemplate.Wide2Columns;
            //}

            ////11182010 TL - Re-enable profile popup for Mingle
            //if (IsProfilePopupEnabledForSite(g.Brand))
            //{
            //    if (!IsProfilePopupDisabled(memberID, entryPoint, ownerMemberID))
            //    {
            //        newWindow = true;
            //        layoutTemplate = LayoutTemplate.WidePopup;
            //    }
            //}

            var thisUrl = (string.IsNullOrEmpty(url)) ? HttpContext.Current.Request.Url.ToString() : url;

            return LinkFactory.Instance.GetLink(
                viewProfileBase
                , parameters
                , brand
                , thisUrl
                , false
                , newWindow
                , brand.Site.SiteID + "MemberProfile" //memberID.ToString() -- TL: Keep one popup window per site
                , 760 //width
                , 640 //height
                , "scrollbars=yes,resizable=yes"
                , true
                , layoutTemplate);
        }

        public static string MakeViewProfileLink(Brand brand, int pMemberID)
        {
            return MakeViewProfileLink(brand, EntryPoint.NoArgs, pMemberID);
        }

        public static string MakeViewProfileLink(Brand brand, EntryPoint pEntryPoint, int pMemberID)
        {
            return MakeViewProfileLink(brand, pEntryPoint, pMemberID, 1, null, Constants.NULL_INT);
        }

        public static string MakeViewProfileLink(Brand brand, EntryPoint entryPoint, int memberID, int ordinal,
                                                 MOCollection moCollection, int hotListCategoryID)
        {
            return MakeViewProfileLink(brand, entryPoint, memberID, ordinal, moCollection, hotListCategoryID, false);
        }

        public static string MakeViewProfileLink(Brand brand, EntryPoint entryPoint, int memberID, int ordinal,
                                                 MOCollection moCollection, int hotListCategoryID, bool forceSameWindow)
        {
            return MakeViewProfileLink(brand, entryPoint, memberID, ordinal, moCollection, hotListCategoryID,
                                       forceSameWindow, (int) PremiumEntryPoint.NoTracking);
        }

        public static string MakeViewProfileLink(Brand brand, EntryPoint entryPoint, int memberID, int ordinal,
                                                 MOCollection moCollection, int hotListCategoryID, bool forceSameWindow,
                                                 int premiumEntryPointVal)
        {
            return MakeViewProfileLink(brand, entryPoint, memberID, ordinal, moCollection, hotListCategoryID,
                                       forceSameWindow, premiumEntryPointVal, string.Empty, Constants.NULL_INT);
        }

        public static string MakeViewProfileLink(Brand brand, EntryPoint entryPoint, int memberID, int ordinal,
                                                 MOCollection moCollection, int hotListCategoryID, bool forceSameWindow,
                                                 int premiumEntryPointVal, string url, int ownerMemberID)
        {
            var parameters = new ParameterCollection();

            parameters.Add("MemberID", memberID.ToString());
            parameters.Add("Ordinal", ordinal.ToString());

            switch (entryPoint)
            {
                case EntryPoint.ReverseSearchResults:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.ReverseSearchResults).ToString());
                    break;

                case EntryPoint.KeywordSearchResults:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.KeywordSearchResults).ToString());
                    break;

                case EntryPoint.SearchResults:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.SearchResults).ToString());
                    break;

                case EntryPoint.HotLists:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int) EntryPoint.HotLists).ToString());
                    parameters.Add("HotListCategoryID", hotListCategoryID.ToString());
                    break;

                case EntryPoint.MembersOnline:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.MembersOnline).ToString());
                    parameters = AddMOLParameters(parameters, moCollection);
                    break;

                case EntryPoint.HomePage:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int) EntryPoint.HomePage).ToString());
                    break;

                case EntryPoint.Messages:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int) EntryPoint.Messages).ToString());
                    break;
                case EntryPoint.PhotoGallery:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.PhotoGallery).ToString());
                    break;
                case EntryPoint.QuickSearchResults:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.QuickSearchResults).ToString());
                    break;
                case EntryPoint.AdvancedSearchResults:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.AdvancedSearchResults).ToString());
                    break;
                case EntryPoint.QuestionPage:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.QuestionPage).ToString());
                    break;
                case EntryPoint.MemberSlideshow:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.MemberSlideshow).ToString());
                    break;
                case EntryPoint.HomeHeroProfileMatches:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.HomeHeroProfileMatches).ToString());
                    break;
                case EntryPoint.HomeHeroProfileMOL:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.HomeHeroProfileMOL).ToString());
                    parameters = AddMOLParameters(parameters, moCollection);
                    break;
                case EntryPoint.HomeHeroProfileFilmStripMatches:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.HomeHeroProfileFilmStripMatches).ToString());
                    break;
                case EntryPoint.HomeHeroProfileFilmStripMOL:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.HomeHeroProfileFilmStripMOL).ToString());
                    parameters = AddMOLParameters(parameters, moCollection);
                    break;
                case EntryPoint.SlideShowPage:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.SlideShowPage).ToString());
                    break;
                case EntryPoint.NotificationCreate:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                   ((int) EntryPoint.NotificationCreate).ToString());
                    break;
                default:
                    try
                    {
                        parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT,
                                       (Convert.ToInt32(entryPoint).ToString()));
                    }
                    catch
                    {
                    }
                    break;
            }

            if (premiumEntryPointVal != (int) PremiumEntryPoint.NoTracking)
            {
                // TO TRACK THE PREMIUM SERVICE THAT THE TARGET MEMBER OWNS
                // ONLY TRACK WHEN AN EXPLICIT PREMIUM ENTRY POINT WAS PROVIDED 
                // IN THE TARGET MEMBER PROFILE  
                parameters.Add(WebConstants.URL_PARAMETER_NAME_PREMIUM_ENTRYPOINT, (premiumEntryPointVal).ToString());
                //parameters.Add(WebConstants.URL_PARAMETER_NAME_PREMIUM_ENTRYPOINT, ((int)premiumEntryPoint).ToString());
            }

            return GetViewProfileLinkBase(brand, memberID, parameters, forceSameWindow, url, entryPoint, ownerMemberID);
        }

        #region Link Creation Routines

        public static ParameterCollection AddMOLParameters(ParameterCollection parameterCollection, MOCollection coll)
        {
            // In other parts of the code (i.e. VisitorLimits) the paths are too long.  If a value is a NULL number, then
            // it will not be added to the QueryString.
            parameterCollection.Add(QuickSearchConstants.Params.OrderBy, coll.OrderBy.ToString());
            if (coll.GenderMask != Constants.NULL_INT)
                parameterCollection.Add(QuickSearchConstants.Params.GenderMask, coll.GenderMask.ToString());
            if (coll.RegionID != Constants.NULL_INT)
                parameterCollection.Add(QuickSearchConstants.Params.RegionId, coll.RegionID.ToString());
            if (coll.AgeMin != Constants.NULL_INT)
                parameterCollection.Add(QuickSearchConstants.Params.AgeMinimum, coll.AgeMin.ToString());
            if (coll.AgeMax != Constants.NULL_INT)
                parameterCollection.Add(QuickSearchConstants.Params.AgeMaximum, coll.AgeMax.ToString());
            if (coll.LanguageMask != Constants.NULL_INT)
                parameterCollection.Add(QuickSearchConstants.Params.LanguageMask, coll.LanguageMask.ToString());

            return parameterCollection;
        }

        #endregion
    }
}