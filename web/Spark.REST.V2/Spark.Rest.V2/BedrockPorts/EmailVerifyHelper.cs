﻿using System;
using System.Text.RegularExpressions;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

namespace Spark.Rest.V2.BedrockPorts
{

    public class EmailVerifyHelper
    {
        public const string SESSION_EMAIL_VERIFY_NOTIFICATION_DISPLAY = "DISPLAY_EMAIL_VERIFIED_MSG";
        public const string RESX_EMAIL_VERIFY_CONFIRMATION = "EMAIL_VERIFIED_MSG";
        public const string SESSION_EMAIL_VERIFY_JUST_REGISTERED = "EMAIL_VERIFIED_JUST_REGISTERED";
        public const string SESSION_EMAIL_VERIFY_JUST_VERIFIED = "EMAIL_VERIFIED_JUST_VERIFIED";

        public enum HashCodeMask : int
        {
            fromMemberID = 0,
            fromEmailAddress = 1
        }

        bool _enableEmailVerifyFlag;
        WebConstants.ForcedBlockingMask _blockingMaskSetting = WebConstants.ForcedBlockingMask.NotBlocked;

        int _communityid;
        int _siteid;
        int _brandid;
        Brand _brand;
        HashCodeMask _hashCodeMask = HashCodeMask.fromMemberID;

        public EmailVerifyHelper(Brand brand)
        {
            try
            {
                _communityid = brand.Site.Community.CommunityID;
                _siteid = brand.Site.SiteID;
                _brandid = brand.BrandID;
                _brand = brand;

                _enableEmailVerifyFlag = Boolean.TrueString.ToLower() == RuntimeSettings.GetSetting("EMAIL_VERIFICATION_ENABLE_FLAG", _communityid, _siteid, _brandid).ToLower();
                _blockingMaskSetting = (WebConstants.ForcedBlockingMask)Enum.Parse(typeof(WebConstants.ForcedBlockingMask), RuntimeSettings.GetSetting("EMAIL_VERIFICATION_BLOCK_MASK", _communityid, _siteid, _brandid));
                _hashCodeMask = (HashCodeMask)Enum.Parse(typeof(HashCodeMask), RuntimeSettings.GetSetting("EMAIL_VERIFICATION_HASH_MASK", _communityid, _siteid, _brandid));

            }
            catch (Exception ex)
            { }
        }

        public bool EnableEmailVerification
        {
            get { return _enableEmailVerifyFlag; }
        }

        public WebConstants.ForcedBlockingMask EmailVerificationBlocking
        {
            get { return _blockingMaskSetting; }
        }

        public HashCodeMask HashCodeGenerationMask
        {
            get { return _hashCodeMask; }
        }

        public Brand Brand
        {
            get { return _brand; }
            set { _brand = value; }
        }

        public bool IsMemberBlocked(Member member)
        {
            WebConstants.ForcedBlockingMask blockingmask;
            bool ret = false;
            try
            {
                if (member != null)
                {
                    blockingmask = GetBlockingMaskAttr(member);
                    ret = IsMemberBlocked(blockingmask);

                }

                return ret;
            }
            catch (Exception ex)
            { return ret; }
        }

        public bool IsMemberBlocked(WebConstants.ForcedBlockingMask mask)
        {
            bool ret = false;
            if ((mask & WebConstants.ForcedBlockingMask.BlockedAfterReg) == WebConstants.ForcedBlockingMask.BlockedAfterReg && IfMustBlock(WebConstants.EmailVerificationSteps.AfterRegistration))
                ret = true;

            if ((mask & WebConstants.ForcedBlockingMask.BlockedAfterEmailChange) == WebConstants.ForcedBlockingMask.BlockedAfterEmailChange && IfMustBlock(WebConstants.EmailVerificationSteps.AfterEmailChange))
                ret = true;

            return ret;
        }

        public void SetVerifiedMemberAttributes(Member member)
        {
            WebConstants.ForcedBlockingMask blockingmask;
            GlobalStatusMask globalstatusmask;
            int hideMask;
            if (member == null)
                return;

            blockingmask = GetBlockingMaskAttr(member);
            globalstatusmask = GetGlobalStatusMaskAttr(member);
            if ((blockingmask & WebConstants.ForcedBlockingMask.BlockedAfterEmailChange) == WebConstants.ForcedBlockingMask.BlockedAfterEmailChange)
            { blockingmask = blockingmask & (~WebConstants.ForcedBlockingMask.BlockedAfterEmailChange); }

            if ((blockingmask & WebConstants.ForcedBlockingMask.BlockedAfterReg) == WebConstants.ForcedBlockingMask.BlockedAfterReg)
            { blockingmask = blockingmask & (~WebConstants.ForcedBlockingMask.BlockedAfterReg); }

            globalstatusmask = globalstatusmask | GlobalStatusMask.VerifiedEmail;

            hideMask = member.GetAttributeInt(_brand, "HideMask", 0);

            if ((blockingmask & WebConstants.ForcedBlockingMask.HideFromMOL) == WebConstants.ForcedBlockingMask.HideFromMOL)
            {
                hideMask = hideMask & ~(int)AttributeOptionHideMask.HideMembersOnline;
                blockingmask = blockingmask & ~WebConstants.ForcedBlockingMask.HideFromMOL;
            }
            if ((blockingmask & WebConstants.ForcedBlockingMask.HideFromSearch) == WebConstants.ForcedBlockingMask.HideFromSearch)
            {
                hideMask = hideMask & ~(int)AttributeOptionHideMask.HideSearch;
                blockingmask = blockingmask & ~WebConstants.ForcedBlockingMask.HideFromSearch;
            }
            member.SetAttributeInt(_brand, "HideMask", hideMask);
            member.SetAttributeInt(_brand, AttributeConstants.GLOBALSTATUSMASK, (int)globalstatusmask);
            member.SetAttributeInt(_brand, "BlockingMask", (int)blockingmask);
            member.SetAttributeDate(_brand, "EmailVerificationDate", DateTime.Now);
        }

        public void SetNotVerifiedMemberAttributes(Member member, WebConstants.EmailVerificationSteps emailVerifyStep)
        {
            GlobalStatusMask globalstatusmask;
            WebConstants.ForcedBlockingMask blockingmask;
            int hideMask;
            int hideMaskOld;
            if (member == null)
                return;

            blockingmask = GetBlockingMaskAttr(member);
            globalstatusmask = GetGlobalStatusMaskAttr(member);
            globalstatusmask = globalstatusmask & (~GlobalStatusMask.VerifiedEmail);

            if (IfMustBlock(emailVerifyStep))
            {
                if (emailVerifyStep == WebConstants.EmailVerificationSteps.AfterEmailChange)
                    blockingmask = blockingmask | WebConstants.ForcedBlockingMask.BlockedAfterEmailChange;

                if (emailVerifyStep == WebConstants.EmailVerificationSteps.AfterRegistration)
                    blockingmask = blockingmask | WebConstants.ForcedBlockingMask.BlockedAfterReg;
            }

            hideMask = member.GetAttributeInt(_brand, "HideMask", 0);
            hideMaskOld = hideMask;
            if (IfMustHide(emailVerifyStep))
            {
                if (IfMustHide(WebConstants.ForcedBlockingMask.HideFromSearch))
                {
                    hideMask = hideMask | (int)AttributeOptionHideMask.HideMembersOnline;
                    if ((hideMaskOld & (int)AttributeOptionHideMask.HideMembersOnline) != (int)AttributeOptionHideMask.HideMembersOnline)
                    {
                        blockingmask = blockingmask | WebConstants.ForcedBlockingMask.HideFromMOL;
                    }

                }
                if (IfMustHide(WebConstants.ForcedBlockingMask.HideFromMOL))
                {
                    hideMask = hideMask | (int)AttributeOptionHideMask.HideSearch;

                    if ((hideMaskOld & (int)AttributeOptionHideMask.HideSearch) != (int)AttributeOptionHideMask.HideSearch)
                    {
                        blockingmask = blockingmask | WebConstants.ForcedBlockingMask.HideFromSearch;
                    }
                }

                member.SetAttributeInt(_brand, "HideMask", hideMask);
            }

            member.SetAttributeInt(_brand, "GlobalStatusMask", (int)globalstatusmask);
            member.SetAttributeInt(_brand, "BlockingMask", (int)blockingmask);
        }



        public bool IfMustBlock(WebConstants.EmailVerificationSteps blockMask)
        {
            bool ret = ((int)_blockingMaskSetting & (int)blockMask) == (int)blockMask;
            return ret;
        }

        public bool IfMustHide(WebConstants.EmailVerificationSteps blockMask)
        {
            bool ret = false;
            if ((blockMask & WebConstants.EmailVerificationSteps.AfterEmailChange) == WebConstants.EmailVerificationSteps.AfterEmailChange)
                return false;
            if (IfMustHide(WebConstants.ForcedBlockingMask.HideFromMOL) || IfMustHide(WebConstants.ForcedBlockingMask.HideFromSearch))
                ret = true;

            return ret;
        }

        public bool IfMustHide(WebConstants.ForcedBlockingMask blockMask)
        {
            bool ret = ((int)_blockingMaskSetting & (int)blockMask) == (int)blockMask;
            return ret;
        }


        public WebConstants.ForcedBlockingMask GetBlockingMaskAttr(Member member)
        {
            WebConstants.ForcedBlockingMask mask = WebConstants.ForcedBlockingMask.NotBlocked;
            if (member != null)
            {
                mask = (WebConstants.ForcedBlockingMask)Enum.Parse(typeof(WebConstants.ForcedBlockingMask), member.GetAttributeInt(_brand, "BlockingMask", 0).ToString());
            }
            return mask;
        }

        public GlobalStatusMask GetGlobalStatusMaskAttr(Member member)
        {
            GlobalStatusMask mask = GlobalStatusMask.Default;
            if (member != null)
            {
                mask = (GlobalStatusMask)Enum.Parse(typeof(GlobalStatusMask), member.GetAttributeInt(_brand, "GlobalStatusMask", 0).ToString());
            }
            return mask;
        }

        public bool IsEmailValid(string emailAddress)
        {
            if(string.IsNullOrEmpty(emailAddress)) return false;
            //This is the same regex used by SUA.
            string pattern = @"^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$";
            Regex regex = new Regex(pattern,RegexOptions.Multiline|RegexOptions.Compiled|RegexOptions.IgnoreCase|RegexOptions.ECMAScript);
            return regex.IsMatch(emailAddress);
        }

        public bool IsValidDNEmail(string email)
        {
            bool isValid = true;

            if (!String.IsNullOrEmpty(email) && email.Length > 3)
            {
                if (!IsEmailValid(email))
                    isValid = false;
            }
            else
            {
                isValid = false;
            }

            return isValid;
        }

        public bool IsMemberVerified(Member member, Brand brand)
        {
            bool ret = false;
            GlobalStatusMask mask = GlobalStatusMask.Default;

            if (member != null)
            {
                mask = (GlobalStatusMask)Enum.Parse(typeof(GlobalStatusMask), member.GetAttributeInt(brand, "GlobalStatusMask", 0).ToString());
            }
            if ((mask & GlobalStatusMask.VerifiedEmail) == GlobalStatusMask.VerifiedEmail)
                ret = true;

            return ret;
        }


        public bool IsMemberHiddenFromSearch(Member member, Brand brand, AttributeOptionHideMask hideMaskOption)
        {
            bool ret = false;
            int mask = 0;

            if (member != null)
            {
                mask = Conversion.CInt(member.GetAttributeInt(brand, "HideMask", 0).ToString());
            }
            if ((mask & (int)hideMaskOption) == (int)hideMaskOption)
                ret = true;

            return ret;
        }

    }
}
