using System;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Configuration.ServiceAdapters;
using Spark.CloudStorage;

namespace Spark.Rest.BedrockPorts
{
    public enum PhotoType
    {
        Full = 1,
        Thumbnail
    }

    public enum PrivatePhotoImageType
    {
        Full = 0,
        BackgroundThumb = 1,
        Thumb = 2,
        TinyThumb = 3, 
        None=4
    }

    public enum NoPhotoImageType
    {
        BackgroundThumb = 0,
        Thumb = 1,
        BackgroundTinyThumb = 2,
        ThumbV2 = 3,
        TinyThumbV2 = 4,
        Matchmail = 5, 
        None=6
    }
    
    public class MemberPhotoHelper
    {
        //All static methods

        #region Image Constants
        //composite of background and text, large size for use in member profile
        private const string IMAGE_PRIVATE_PHOTO_FULL = "privatephoto200x250_ynm4.gif";
        private const string IMAGE_PRIVATE_PHOTO_FULL_FEMALE = "privatephoto200x250_ynm4.gif";

        //background only, does not contain text, for use as thumbnail in searches and profiles (text appears on top).
        private const string IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB = "privatephoto80x104_ynm4.gif";
        private const string IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB_FEMALE = "privatephoto80x104_ynm4.gif";

        //composite of background and text, medium size and needs to be a non-interlaced jpg/jpeg for use in Userplane flash movie (and emails)
        private const string IMAGE_PRIVATE_PHOTO_THUMB = "privatephoto80x104_ynm4.jpg";
        private const string IMAGE_PRIVATE_PHOTO_THUMB_FEMALE = "privatephoto80x104_ynm4.jpg";

        //composite of background and text, small size for use in IM notifier
        private const string IMAGE_PRIVATE_PHOTO_TINY_THUMB = "privatephoto43x56_ym4.gif";
        private const string IMAGE_PRIVATE_PHOTO_TINY_THUMB_FEMALE = "privatephoto43x56_ym4.gif";

        //background only, does not contain text, for use as thumbnail in searches and profiles (text appears on top).
        private const string IMAGE_NOPHOTO_BACKGROUND_THUMB = "no-photo-m-m.png"; //"noPhoto.gif"; //current 80x103
        private const string IMAGE_NOPHOTO_BACKGROUND_THUMB_FEMALE = "no-photo-m-f.png"; //"noPhoto.gif";

        //composite of background and text, medium size and needs to be a non-interlaced jpg/jpeg for use in Userplane flash movie (and emails)
        private const string IMAGE_NOPHOTO_THUMB = "no-photo-m-m.png";//"noPhoto.jpg"; //current 80x103
        private const string IMAGE_NOPHOTO_THUMB_FEMALE = "no-photo-m-f.png";//"noPhoto.jpg";

        //background only, does not contain text, small size for use in IM notifier
        private const string IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB = "no-photo-s-m.png";//"noPhoto_small.gif"; //current 43x56
        private const string IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB_FEMALE = "no-photo-s-f.png";//"noPhoto_small.gif";

        //for use as thumbnail (new version 2.0)
        private const string IMAGE_NOPHOTO_THUMB_V2 = "no-photo-s-m.png"; //"no-photo-sm.gif"; //current 43x56
        private const string IMAGE_NOPHOTO_THUMB_V2_FEMALE = "no-photo-s-f.png";//"no-photo-sm.gif";

        //for use as tiny thumbnail (new version 2.0)
        private const string IMAGE_NOPHOTO_TINY_THUMB_V2 = "no-photo-s-m.png";//"no-photo-sm-54x70.gif";
        private const string IMAGE_NOPHOTO_TINY_THUMB_V2_FEMALE = "no-photo-s-f.png";//"no-photo-sm-54x70.gif";

        //Used by MMImageDisplay.aspx (email related), must stay as jpg
        private const string IMAGE_NOPHOTO_MM = "no-photo-l-m.jpg"; //nophotoLargeMM.jpg 246x328
        private const string IMAGE_NOPHOTO_MM_FEMALE = "no-photo-l-f.jpg"; //nophotoLargeMM.jpg
        #endregion

        private ISettingsSA _settingsService = null;
        private string _imgDirectory = String.Empty;

		public MemberPhotoHelper(ISettingsSA settingsService)
		{
			_settingsService = settingsService;
			_imgDirectory = RuntimeSettings.GetSetting("IMG_URL") + "/";
		}

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="photo"></param>
        /// <param name="brand"></param>
        /// <returns>True if the Photo is private and the Brand allows private photos.</returns>
        public static bool IsPrivatePhoto(Photo photo, Brand brand)
        {
            return (photo.IsPrivate && brand.GetStatusMaskValue(StatusType.PrivatePhotos));
        }

        public static bool PhotoIsEmpty(Photo photo, PhotoType photoType)
        {
            bool showPhotosFromCloud = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD"));

            if (photoType == PhotoType.Full)
            {
                return showPhotosFromCloud
                           ? String.IsNullOrEmpty(photo.FileCloudPath)
                           : String.IsNullOrEmpty(photo.FileWebPath);
            }
            return showPhotosFromCloud
                ? String.IsNullOrEmpty(photo.ThumbFileCloudPath)
                : String.IsNullOrEmpty(photo.ThumbFileWebPath);
        }

    	/// <summary>
    	/// Selects a "default" photo object to use as a thumbnail (or whatever), returning a private photo if there are no approved,
    	/// non-private photos, and null if there are no approved photos.
    	/// </summary>
    	/// <param name="member"></param>
    	/// <param name="brand"></param>
    	/// <returns>The member's Photo or null if there is no photo.</returns>
    	public Photo GetDefaultPhoto(IMember member, Brand brand)
        {
            return GetDefaultPhoto(member, brand.Site.Community.CommunityID);
        }

    	/// <summary>
    	/// Selects a "default" photo object to use as a thumbnail (or whatever), returning a private photo if there are no approved,
    	/// non-private photos, and null if there are no approved photos.
    	/// </summary>
    	/// <param name="member"></param>
    	/// <param name="communityId"> </param>
    	/// <returns>The member's Photo or null if there is no photo.</returns>
    	public Photo GetDefaultPhoto(IMember member, Int32 communityId)
        {
            var photos = member.GetPhotos(communityId);
    	    Photo retVal = null;

            for (byte photoNum = 0; photoNum < photos.Count; photoNum++)
            {
                var photo = photos[photoNum];
                if (!photo.IsApproved) continue;
                if (photo.IsPrivate)
                {
                    retVal = photo;  //This private photo will get returned if no non-private approved photos are found
                }
                else
                {
                    retVal = photo; //We found a good photo, get out
                    break;
                }
            }

    	    return retVal;
        }
        
        public string GetPhotoDisplayURL(IMember memberViewing, IMember memberBeingViewed, Brand brand,
            Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType, bool useFullURL)
        {
            //there is no NO PHOTO image for full size photos
            return GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, photoType, privatePhotoType,
                               NoPhotoImageType.None, useFullURL);
        }
        public string GetPhotoDisplayURL(IMember memberViewing, IMember memberBeingViewed, Brand brand,
            Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType)
        {
            //there is no NO PHOTO image for full size photos
            return GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, photoType, privatePhotoType,
                               NoPhotoImageType.None, false);
        }

        public string GetPhotoDisplayURL(IMember memberViewing, IMember memberBeingViewed, Brand brand,
            Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType, NoPhotoImageType noPhotoType)
        {
            //there is no NO PHOTO image for full size photos
            return GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, photoType, privatePhotoType,
                               noPhotoType, false);
        }

        public string GetPhotoDisplayURL(IMember memberViewing, IMember memberBeingViewed, Brand brand,
            Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType, NoPhotoImageType noPhotoType, bool useFullURL)
        {
            return GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, photoType, privatePhotoType,
                               noPhotoType, useFullURL, false);
        }

        public string GetPhotoDisplayURL(IMember memberViewing, IMember memberBeingViewed, Brand brand,
            Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType, NoPhotoImageType noPhotoType, bool useFullURL, bool displayPrivatePhotos)
        {
            bool isMale = IsMaleGender((Member)memberBeingViewed, brand);
            string privatePhotoFile = (privatePhotoType == PrivatePhotoImageType.None) ? String.Empty : GetPrivatePhotoFile(privatePhotoType, isMale);
            string noPhotoFile = (noPhotoType == NoPhotoImageType.None) ? String.Empty : GetNoPhotoFile(noPhotoType, isMale);

            if (photo == null)
            {
                return noPhotoFile;
            }

            if (!displayPrivatePhotos && HidePhotos(memberViewing, memberBeingViewed, brand))
            {
                return noPhotoFile;
            }

            return GetPhotoUrl(photo, brand, privatePhotoFile, noPhotoFile, photoType, useFullURL, displayPrivatePhotos);
        }

        public string GetPrivatePhotoFile(PrivatePhotoImageType imageType, IMember member, Brand brand)
        {
            bool isMale = IsMaleGender((Member)member, brand);
            return GetPrivatePhotoFile(imageType, isMale);
        }

        public string GetPrivatePhotoFile(PrivatePhotoImageType imageType, bool isMale)
        {
            string imageFile = String.Empty;

            switch (imageType)
            {
                case PrivatePhotoImageType.BackgroundThumb:
                    imageFile = isMale
                                    ? IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB
                                    : IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB_FEMALE;
                    break;
                case PrivatePhotoImageType.Full:
                    imageFile = isMale
                                    ? IMAGE_PRIVATE_PHOTO_FULL
                                    : IMAGE_PRIVATE_PHOTO_FULL_FEMALE;
                    break;
                case PrivatePhotoImageType.Thumb:
                    imageFile = isMale
                                    ? IMAGE_PRIVATE_PHOTO_THUMB
                                    : IMAGE_PRIVATE_PHOTO_THUMB_FEMALE;
                    break;
                case PrivatePhotoImageType.TinyThumb:
                    imageFile = isMale
                                    ? IMAGE_PRIVATE_PHOTO_TINY_THUMB
                                    : IMAGE_PRIVATE_PHOTO_TINY_THUMB_FEMALE;
                    break;
            }

            return _imgDirectory + imageFile;
        }

    	public static bool IsMaleGender(Member pMember, Brand b)
    	{
    		const int GENDERID_MALE = 1;
    		const int GENDERID_FEMALE = 2;

    		var isMale = true;
    		if (pMember != null && b != null)
    		{
    			int genderMask = pMember.GetAttributeInt(b, "gendermask");
    			if (genderMask > 0)
    			{
    				if ((genderMask & GENDERID_MALE) == GENDERID_MALE)
    					isMale = true;
    				else if ((genderMask & GENDERID_FEMALE) == GENDERID_FEMALE)
    					isMale = false;
    			}
    		}
    		return isMale;
    	}


    	public string GetNoPhotoFile(NoPhotoImageType imageType, IMember member, Brand brand)
        {
            bool isMale = IsMaleGender((Member)member, brand);
            return GetNoPhotoFile(imageType, isMale);
        }

        public string GetNoPhotoFile(NoPhotoImageType imageType, bool isMale)
        {
            string imageFile = String.Empty;

            switch (imageType)
            {
                case NoPhotoImageType.BackgroundThumb:
                    imageFile = isMale
                                    ? IMAGE_NOPHOTO_BACKGROUND_THUMB
                                    : IMAGE_NOPHOTO_BACKGROUND_THUMB_FEMALE;
                    break;
                case NoPhotoImageType.BackgroundTinyThumb:
                    imageFile = isMale
                                    ? IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB
                                    : IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB_FEMALE;
                    break;
                case NoPhotoImageType.Matchmail:
                    imageFile = isMale ? IMAGE_NOPHOTO_MM : IMAGE_NOPHOTO_MM_FEMALE;
                    break;
                case NoPhotoImageType.Thumb:
                    imageFile = isMale ? IMAGE_NOPHOTO_THUMB : IMAGE_NOPHOTO_THUMB_FEMALE;
                    break;
                case NoPhotoImageType.ThumbV2:
                    imageFile = isMale
                                    ? IMAGE_NOPHOTO_THUMB_V2
                                    : IMAGE_NOPHOTO_THUMB_V2_FEMALE;
                    break;
                case NoPhotoImageType.TinyThumbV2:
                    imageFile = isMale
                        ? IMAGE_NOPHOTO_TINY_THUMB_V2
                        : IMAGE_NOPHOTO_TINY_THUMB_V2_FEMALE;
                    break;
            }

            return _imgDirectory + imageFile;
        }

        public bool HidePhotos(IMember memberViewing, IMember memberBeingViewed, Brand brand)
        {
            return !memberBeingViewed.GetAttributeBool(brand, "DisplayPhotosToGuests") && memberViewing == null;
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// todo: refactor with memberhelper.cs getphotourl method
        /// </summary>
        /// <param name="photo"></param>
        /// <param name="brand"></param>
        /// <param name="privatePhotoFile"></param>
        /// <param name="noPhotoFile"></param>
        /// <param name="photoType"></param>
        /// <param name="useFullUrl"></param>
        /// <param name="displayPrivatePhotos"></param>
        /// <returns></returns>
        private string GetPhotoUrl(Photo photo, Brand brand, string privatePhotoFile, string noPhotoFile, PhotoType photoType, bool useFullUrl, bool displayPrivatePhotos)
        {
            var memberPhotoUrl = string.Empty;
            var showPhotosFromCloud =
                Convert.ToBoolean(_settingsService.GetSettingFromSingleton("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD",
                    brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));

            if (photo != null)
            {
                if (!displayPrivatePhotos && IsPrivatePhoto(photo, brand))
                {
                    memberPhotoUrl = privatePhotoFile;
                    return memberPhotoUrl;
                        //no need to continue. Return relative to site root path to private photo file. We don't want to use proxy on non-absolute private photo URL. see 17305.
                }

                if (!showPhotosFromCloud)
                {
                    switch (photoType)
                    {
                        case PhotoType.Full:
                            if (photo.FileCloudPath != null)
                                memberPhotoUrl = photo.FileCloudPath;
                            break;
                        case PhotoType.Thumbnail:
                            if (photo.ThumbFileCloudPath != null)
                                memberPhotoUrl = photo.ThumbFileCloudPath;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException("photoType", photoType, null);
                    }
                }
                else
                {

                    switch (photoType)
                    {
                        case PhotoType.Full:
                            if (photo.FileWebPath != null)
                                memberPhotoUrl = photo.FileWebPath;
                            break;
                        case PhotoType.Thumbnail:
                            if (photo.ThumbFileWebPath != null)
                                memberPhotoUrl = photo.ThumbFileWebPath;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException("photoType", photoType, null);
                    }
                }
            }

            //if photo URL is still empty, use the default noPhoto file
            if (memberPhotoUrl == string.Empty)
            {
                memberPhotoUrl = noPhotoFile;
            }
            else
            {
                if (showPhotosFromCloud)
                {
                    var cloudClient = new Client(brand.Site.Community.CommunityID, brand.Site.SiteID, _settingsService);

                    memberPhotoUrl = cloudClient.GetFullCloudImagePath(memberPhotoUrl, FileType.MemberPhoto, false);
                }
                else
                {
                    if (useFullUrl)
                    {
                        memberPhotoUrl = prependFullURL(brand, memberPhotoUrl);
                    }
                }
            }

            return memberPhotoUrl;
        }

        /// <summary>
        /// This is to defeat the "this page contains nonsecure items" dialog that appears when you open a https page.
        /// Instead of getting image from http url, we get image from aspx page on https site that acts as proxy for that image.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string useSecureProxyImage(string url)
        {
            return "/Applications/MemberProfile/MemberPhotoFile.aspx?Path=" + HttpUtility.UrlEncode(url);
        }

        private string prependFullURL(Brand brand, string relativePath)
        {
            return "http://" + brand.Site.DefaultHost + "." + brand.Uri + relativePath;
        }

        #endregion Private Methods
    }
}
