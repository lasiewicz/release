﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Logger;
using Spark.Rest.Helpers;

namespace Spark.Rest.V2.BedrockPorts
{
    public static class DfpAttributeHelper
    {
        const string IsraelCultureCode = "he-il";
        const string UsCultureCode = "en-us";
        public const int NULL_INT = -2147483647;
        public const int NonPermitMember = 0;
        public const int PermitMember = 1; // 1 bit
        public const int SubscribedMemberMask = 1023; //10 bits 111111111, subscribe member should have privilege to everything

        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(DfpAttributeHelper));
        private static XDocument gamAttributesXDoc;
        

        public enum MessmoCapableStatus
        {
            NotCapable = 0,
            Capable = 1,
            Pending = 2,
            Failed = 3,
        }

        

        public enum Custody
        {
            Blank = 0,
            NoKids = 2,
            FarAway = 4,
            Sometimes = 8,
            DontLiveWithMe = 16,
            SomeLiveWithMe = 32,
            WeekendsOnly = 64,
            LiveWithMe = 128
        }

        static DfpAttributeHelper()
        {
            gamAttributesXDoc = XDocument.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration\\GAM.xml"));
        }

        private static string GetDfpDescription(string attributeName, int attributeOptionValue, Brand brand)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection != null)
            {
                var attributeOption = attributeOptionCollection[attributeOptionValue];

                return attributeOption != null ? GetAdAttributes(attributeName, attributeOption.Description)  : "no_value";
            }

            return string.Empty;
        }

        /// <summary>
        /// To be used for GAM only.
        /// </summary>
        [Serializable]
        private class Attribute
        {
            public string Name { get; set; }
            public string GAMName { get; set; }
            public List<AttributeOption> AttributeOptions { get; set; }
        }

        /// <summary>
        /// To be used for GAM only.
        /// </summary>
        [Serializable]
        private class AttributeOption
        {
            public string Value { get; set; }
            public string GAMValue { get; set; }
        }


        /// <summary>
        /// Extracts the mapped value between BH attribute option and GAM attribute option using LINQ.
        /// </summary>
        /// <param name="attributeName"></param>
        /// <param name="attributeOptionValue"></param>
        /// <returns></returns>
        public static string GetAdAttributes(string attributeName, string attributeOptionValue)
        {
            var returnValue = "undefined";

            try
            {
                var attributes = (from x in gamAttributesXDoc.Elements("attributes").Elements("attribute")
                                  where x.Element("name").Value == attributeName
                                  select new Attribute
                                  {
                                      Name = x.Element("name").Value,
                                      GAMName = x.Element("gamname").Value,
                                      AttributeOptions = (from y in x.Element("attributeoptions").Elements("attributeoption")
                                                          select new AttributeOption
                                                          {
                                                              Value = y.Element("value").Value,
                                                              GAMValue = y.Element("gamvalue").Value
                                                          }).ToList()
                                  }).ToList();

                if (attributes.Count > 0)
                {
                    returnValue = (from x in attributes[0].AttributeOptions
                        where x.Value.ToLower() == attributeOptionValue.ToLower()
                        select x).ToList()[0].GAMValue;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Failed to find GAM Attribute with name: '{0}' and value '{1}' in XML file", attributeName, attributeOptionValue), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
            }

            return returnValue;
        }

        /// <summary>
        /// Extracts the mapped value between BH attribute option and GAM attribute option using LINQ.
        /// </summary>
        /// <param name="attributeName"></param>
        /// <param name="attributeOptionValue"></param>
        /// <returns></returns>
        public static string GetPseudoAttributesGamName(string attributeName)
        {
            var returnValue = "undefined";

            try
            {
                var attributes = (from x in gamAttributesXDoc.Elements("attributes").Elements("attribute")
                                  where x.Element("gamname").Value == attributeName
                                  select new Attribute
                                  {
                                      Name = x.Element("name").Value,
                                      GAMName = x.Element("gamname").Value,
                                      AttributeOptions = (from y in x.Element("attributeoptions").Elements("attributeoption")
                                                          select new AttributeOption
                                                          {
                                                              Value = y.Element("value").Value,
                                                              GAMValue = y.Element("gamvalue").Value
                                                          }).ToList()
                                  }).ToList();

                returnValue = attributes.Where(x => x.Name==attributeName).Select(x => x.GAMName).FirstOrDefault();

            }
            catch (Exception ex)
            {
                Log.LogError(string.Format("Failed to find GAM Attribute with name: '{0}' in XML file", attributeName), ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
            }

            return returnValue;
        }

    }
}