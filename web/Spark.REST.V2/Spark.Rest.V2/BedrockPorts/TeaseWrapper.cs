﻿#region

using System;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

#endregion

namespace Spark.REST.BedrockPorts
{
    /// <summary>
    ///   todo: refactor to MT
    ///   This code, ported from the web tier (FlirtHandler.cs), should be migrated to middle tier services
    /// </summary>
    public static class TeaseWrapper
    {
        //  code chunk for a GetTease method:
        //int selectedTeaseID = teaseId;

        //TeaseCategoryCollection categories = TeaseSA.Instance.GetTeaseCategoryCollection(g.Brand.Site.SiteID);
        //Matchnet.Email.ValueObjects.TeaseCategory selectedCategory = null;

        //TeaseCollection teases = TeaseSA.Instance.GetTeaseCollection(categoryId, brand.Site.SiteID);
        //Matchnet.Email.ValueObjects.Tease selectedTease = null;

        //foreach (Matchnet.Email.ValueObjects.TeaseCategory category in categories)
        //{
        //    if (category.TeaseCategoryID == categoryId)
        //    {
        //        selectedCategory = category;
        //        break;
        //    }
        //}

        //foreach (Matchnet.Email.ValueObjects.Tease tease in teases)
        //{
        //    if (tease.TeaseID == selectedTeaseID)
        //    {
        //        selectedTease = tease;
        //        break;
        //    }
        //}

        /// <summary>
        ///   another method that belongs in middle tier services
        /// </summary>
        /// <param name = "brand"></param>
        /// <param name = "sendingMemberId"></param>
        /// <param name = "targetMemberId"></param>
        /// <returns></returns>
        private static bool HasAlreadyTeasedMember(Brand brand, int sendingMemberId, int targetMemberId)
        {
            var isHotListed = false;
            try
            {
                var communityId = brand.Site.Community.CommunityID;
                //check if targetmember on sendingmember hotlist tease category
                var senderList = ListSA.Instance.GetList(sendingMemberId);
                isHotListed = senderList.IsHotListed(HotListType.Tease, HotListDirection.OnYourList, communityId,
                                                     targetMemberId);

                if (isHotListed)
                {
                    return isHotListed;
                }

                /* fix to bug#16754 :Deleting a member from 'Flirted with' list, makes it possible to flirt that member over and over again.
				Check if targetmember list has hotlist tease for sendingmemebr with direction flag=0
			 */
                var targetList = ListSA.Instance.GetList(targetMemberId);
                isHotListed = targetList.IsHotListed(HotListType.Tease, HotListDirection.OnTheirList, communityId, sendingMemberId);
            }
            catch
            {
            } // swallow?  no logging?
            return isHotListed;
        }

        public static ListActionStatus SendTease(Brand brand, int senderMemberId, int recipientMemberId, int categoryId, int teaseId, string teaseText, out int messageListId,
                                                 out int teasesLeft)
        {
            messageListId = 0;
            teasesLeft = 0;

            if (senderMemberId < 1)
            {
                throw new ArgumentOutOfRangeException("senderMemberId");
            }
            if (recipientMemberId < 1)
            {
                throw new ArgumentOutOfRangeException("recipientMemberId");
            }
            MessageSave messageSave = null;
            ListSaveResult hotListSaveResult = null;

            if (HasAlreadyTeasedMember(brand, senderMemberId, recipientMemberId))
            {
                return ListActionStatus.HasAlreadyTeasedMember;
            }
            hotListSaveResult = ListSA.Instance.AddListMember(HotListCategory.MembersYouTeased,
                                                              brand.Site.Community.CommunityID,
                                                              brand.Site.SiteID,
                                                              senderMemberId,
                                                              recipientMemberId,
                                                              null,
                                                              teaseId,
                                                              true,
                                                              false);

            //if (UserNotificationFactory.IsUserNotificationsEnabled(g))
            //{
            //    UserNotificationParams unParams = UserNotificationParams.GetParamsObject(_control.Member.MemberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
            //    UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new FlirtedYouUserNotification(), _control.Member.MemberID, g.Member.MemberID, g);
            //    if (notification.IsActivated())
            //    {
            //        UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
            //    }
            //}

            switch (hotListSaveResult.Status)
            {
                case ListActionStatus.Success:

                    messageSave = new MessageSave(senderMemberId, recipientMemberId, brand.Site.Community.CommunityID, brand.Site.SiteID, MailType.Tease, "Flirt",
                                                  teaseText);
                    //g.GetResource("TXT_TEASE", _control.ResourceControl), // "Flirt", "Smile", etc.
                    //g.GetResource(selectedTease.ResourceKey, _control.ResourceControl)); // text of the actual flirt

                    var saveCopy = false;
                    var sender = MemberSA.Instance.GetMember(senderMemberId, MemberLoadFlags.None);
                    var mailboxMask = (AttributeOptionMailboxPreference) sender.GetAttributeInt(brand, "MailboxPreference");
                    if ((mailboxMask & AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) == AttributeOptionMailboxPreference.SaveCopiesOfSentMessages)
                    {
                        saveCopy = true;
                    }

                    var messageSendResult = EmailMessageSA.Instance.SendMessage(messageSave, saveCopy, Constants.NULL_INT, false);
                    var result = (messageSendResult.Status == MessageSendStatus.Success);
                    messageListId = messageSendResult.SenderMessageListID;
                    if (result)
                    {
                        ExternalMailSA.Instance.SendInternalMailNotification(messageSendResult.EmailMessage, brand.BrandID, messageSendResult.RecipientUnreadCount);
                    }

                    teasesLeft = hotListSaveResult.MaxAllowed - hotListSaveResult.Counter;
                    return hotListSaveResult.Status;

                case ListActionStatus.ExceededMaxAllowed:
                case ListActionStatus.ExceededMaxAllowedForMember: //	ERROR_TOO_MANY_TEASES_SENT
                    return hotListSaveResult.Status;

                default:
                    // TOFIX: the db ID no longer carries any meaning. if we dont know what the specific case is, its an error.
                    throw new ArgumentOutOfRangeException("ListActionStatus", "Unknown ListActionStatus: " + hotListSaveResult.Status);
            }
        }
    }
}