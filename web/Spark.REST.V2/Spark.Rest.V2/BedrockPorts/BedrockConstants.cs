﻿#region

using System;

#endregion

// aka webconstants from bedrock web

namespace Spark.Rest.V2.BedrockPorts
{
    ///<summary>
    ///</summary>
    public class WebConstants
    {
        ///<summary>
        ///</summary>
        public const string URL_PARAMETER_NAME_LAYOUTTEMPLATEID = "LayoutTemplateID";
        ///<summary>
        ///</summary>
        public const string URL_PARAMETER_NAME_ENTRYPOINT = "EntryPoint";
        ///<summary>
        ///</summary>
        public const string URL_PARAMETER_NAME_PREMIUM_ENTRYPOINT = "PremEntPoint";

        public enum ForcedBlockingMask
        {
            NotBlocked = 0,
            BlockedAfterReg = 1,
            BlockedAfterEmailChange = 2,
            HideFromSearch = 8,
            HideFromMOL = 16
        }

        public enum EmailVerificationSteps
        {
            None = 0,
            AfterRegistration = 1,
            AfterEmailChange = 2

        }

    }

    ///<summary>
    /// This enum is bit flags for indicating the member's preferences for their display settings.
    ///</summary>
    [Flags]
    public enum AttributeOptionHideMask
    {
        ///<summary>
        /// Indicates if member wants their profile hidden in searches
        ///</summary>
        HideSearch = 1,
        ///<summary>
        /// Indicates if member wants to hide when view or hotlist another member's profile
        ///</summary>
        HideHotLists = 2,
        /// <summary>
        /// Indicates if member wants to be hidden from other members when they are online
        /// </summary>
        HideMembersOnline = 4,
        /// <summary>
        /// Indiccates if members wants photos hidden from non-members.
        /// </summary>
        HidePhotos = 8
    }

    /// <summary>
    /// 
    /// </summary>
    public enum CommunityId
    {
        ///<summary>
        ///</summary>
        JDate = 3
    }
    #region Site ID enum

    // todo: clean this up/remove if possible
    public enum SITE_ID
    {
        JDateCoIL = 4,
        MatchnetUK = 6,
        DateCA = 13,
        Cupid = 15,
        Spark = 100,
        AmericanSingles = 101,
        Glimpse = 102,
        JDate = 103,
        JDateFR = 105,
        JDateUK = 107,
        Matchnet = 108,
        CollegeLuv = 112,
        JewishMingle = 9171,
        NRGDating = 19,
        ItalianSinglesConnection = 9161,
        BBW = 9041,
        BlackSingles = 9051
    }

    #endregion

    public class ConstantsTemp
    {
        // Region
        public const int REGION_US = 223;
        public const int REGIONID_USA = 223;
        public const int REGIONID_CANADA = 38;
        public const int REGIONID_ISRAEL = 105;
        public const int REGIONID_GERMANY = 83;
        public const int REGIONID_UNITED_KINGDOM = 222;
        public const int REGIONID_AUSTRALIA = 13;
        public const int REGIONID_FRANCE = 76;
        public const int REGIONID_ARGENTINA = 10;
        public const int REGIONID_BELGIUM = 21;
        public const int REGIONID_LUXEMBURG = 125;
        public const int REGIONID_SWITZERLAND = 204;

        public const int MAX_REGION_STRING_LENGTH = 40;
        public const int MAX_HEADLINEORABOUT_STRING_LENGTH = 35;

        // Gender Mask
        public const int GENDERID_MALE = 1;
        public const int GENDERID_FEMALE = 2;
        public const int GENDERID_SEEKING_MALE = 4;
        public const int GENDERID_SEEKING_FEMALE = 8;
        public const int GENDERID_MTF = 16;
        public const int GENDERID_FTM = 32;
        public const int GENDERID_SEEKING_MTF = 64;
        public const int GENDERID_SEEKING_FTM = 128;
        
    }
}