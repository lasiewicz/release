﻿#region

using System;
using System.Collections.Generic;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Spark.Common.Localization;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.Rest.V2.BedrockPorts;
using Spark.REST.DataAccess;

#endregion

namespace Spark.Rest.BedrockPorts
{
    /// <summary>
    ///   User Notification code in Bedrock needs a major refactoring into the SA code. Until then..
    /// </summary>
    public static class UserNotificationAccess
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(UserNotificationAccess));

        private static string GenerateText(Brand brand, IMember notificationCreator, IMember notificationOwner,
                                           string resourceKey)
        {
            var baseText = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo,
                                                                      ResourceGroupEnum.UserNotifications,
                                                                      resourceKey);
            Log.LogDebugMessage(string.Format("text :{0}", baseText), ErrorHelper.GetCustomData());

            var link =
                BreadCrumbHelper.MakeViewProfileLink(brand,
                                                     BreadCrumbHelper.EntryPoint.NotificationCreate,
                                                     notificationCreator.MemberID, 0, null, 0,
                                                     false, (int) BreadCrumbHelper.PremiumEntryPoint.NoPremium, "",
                                                     notificationOwner.MemberID);

            Log.LogDebugMessage(string.Format("link :{0}", link), ErrorHelper.GetCustomData());
            var text = string.Format(baseText, link, notificationCreator.GetUserName(brand));
            return text;
        }

        private static string GetThumbnail(Brand brand, Member notificationCreator, Member notificationOwner)
        {
            var photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            string thumbHtml;
            var siteid = brand.Site.SiteID;
            //for non-subs on jdate and jdate.uk, suppress member thumbnails and links
            if (siteid == (int) SITE_ID.JDate || siteid == (int) SITE_ID.JDateUK)
            {
                if (!notificationOwner.IsPayingMember(siteid))
                {
                    var baseText1 = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo,
                                                                               ResourceGroupEnum.UserNotifications,
                                                                               "SPRITE_HTML");
                    thumbHtml = string.Format(baseText1, "s-icon-notification-match");
                    return thumbHtml;
                }
            }

            //string profileLink = "/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&MemberID=" + NotificationCreator.MemberID;
            var profileLink = BreadCrumbHelper.MakeViewProfileLink(brand, BreadCrumbHelper.EntryPoint.NotificationCreate,
                                                                   notificationCreator.MemberID, 0, null, 0, false,
                                                                   (int) BreadCrumbHelper.PremiumEntryPoint.NoPremium,
                                                                   "", notificationOwner.MemberID);

            string thumbnailStr = GetThumbnailFile(brand, notificationCreator, notificationOwner);

            var baseText2 = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.UserNotifications,
                                                                       "IMAGE_HTML");
            thumbHtml = string.Format(baseText2, profileLink, thumbnailStr, notificationCreator.GetUserName(brand));
            return thumbHtml;
        }

        public static string GetThumbnailFile(Brand brand, Member notificationCreator, Member notificationOwner)
        {
            string thumbnailStr;
            var photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);

            if (notificationCreator.HasApprovedPhoto(brand.Site.Community.CommunityID))
            {
                var photo = photoHelper.GetDefaultPhoto(notificationCreator, brand);
                thumbnailStr = photoHelper.GetPhotoDisplayURL(notificationOwner, notificationCreator, brand, photo,
                                                              PhotoType.Thumbnail,
                                                              PrivatePhotoImageType.TinyThumb,
                                                              NoPhotoImageType.BackgroundTinyThumb);
            }
            else
            {
                var isMale = MemberPhotoHelper.IsMaleGender(notificationCreator, brand);
                thumbnailStr = photoHelper.GetNoPhotoFile(NoPhotoImageType.BackgroundTinyThumb, isMale);
            }

            return thumbnailStr;
        }

        public static void SendUserNotification(Brand brand, Member viewedMember, Member viewerMember, bool? viewerIsHidden)
        {
            var notificationsEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_USER_NOTIFICATIONS",
                                                                                    brand.Site.Community.CommunityID,
                                                                                    brand.Site.SiteID,
                                                                                    brand.BrandID));
            if (!notificationsEnabled)
            {
                Log.LogDebugMessage("notifications are disabled", ErrorHelper.GetCustomData());
            }

            if (viewerIsHidden == null)
            {
                var hideMask = viewerMember.GetAttributeInt(brand, "HideMask", 0);
                viewerIsHidden = (hideMask & (int) AttributeOptionHideMask.HideHotLists) == (int) AttributeOptionHideMask.HideHotLists;
            }

            if (notificationsEnabled && !(bool) viewerIsHidden)
            {
                var unParams = UserNotificationParams.GetParamsObject(viewedMember.MemberID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                var thumbnail = GetThumbnail(brand, viewerMember, viewedMember);
                var thumbnailFile = GetThumbnailFile(brand, viewerMember, viewedMember);
                var timestamp = DateFormatter.ElapsedTimeText(brand, DateTime.Now);
                bool viewedMemberIsFavorite = HotListAccess.MemberIsOnHotlist(brand, HotListCategory.Default, viewedMember.MemberID, viewerMember.MemberID);
                var resourceKey = viewedMemberIsFavorite ? "USER_NOTIFICATION_FAVORITE_VIEWED_YOU_MESSAGE_TXT" : "USER_NOTIFICATION_VIEWED_YOU_MESSAGE_TXT";
                var text = GenerateText(brand, viewerMember, viewedMember, resourceKey);
                var omnitureTag = String.Empty;
                var viewObj = new UserNotificationViewObject(
                    viewerMember.GetUserName(brand),
                    viewedMember.MemberID,
                    viewerMember.MemberID,
                    thumbnail,
                    text,
                    timestamp,
                    true,
                    DateTime.Now,
                    brand.Site.SiteID,
                    brand.Site.Community.CommunityID,
                    resourceKey,
                    omnitureTag,
                    false,
                    thumbnailFile,
                    NotificationType.ViewedYourProfile);
                InitializeStandardDataPoints(viewObj, viewerMember, brand);
                if (viewedMemberIsFavorite)
                {
                    viewObj.DataPoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                }

                UserNotificationsServiceSA.Instance.AddUserNotification(unParams, viewObj);
            }
        }

        ///<summary>
        ///</summary>
        ///<param name = "ownerId"></param>
        ///<param name = "creatorId"></param>
        ///<param name = "brand"></param>
        ///<returns></returns>
        public static void AddUserNotificationForIM(int ownerId, int creatorId, Brand brand)
        {
            if (!Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_USER_NOTIFICATIONS",
                                                              brand.Site.Community.CommunityID,
                                                              brand.Site.SiteID,
                                                              brand.BrandID)))
                return;

            var notificationMember = MemberSA.Instance.GetMember(ownerId, MemberLoadFlags.None);
            var notificationCreator = MemberSA.Instance.GetMember(creatorId, MemberLoadFlags.None);
            var thumbnail = GetThumbnail(brand, notificationCreator, notificationMember);
            var thumbnailFile = GetThumbnailFile(brand, notificationCreator, notificationMember);
            var timestamp = DateFormatter.ElapsedTimeText(brand, DateTime.Now);
            var unParams = UserNotificationParams.GetParamsObject(ownerId, brand.Site.SiteID, brand.Site.Community.CommunityID);
            bool notificationMemberIsFavorite = HotListAccess.MemberIsOnHotlist(brand, HotListCategory.Default, notificationMember.MemberID, notificationCreator.MemberID);
            var resourceKey = notificationMemberIsFavorite ? "USER_NOTIFICATION_FAVORITE_IMED_YOU_MESSAGE_TXT" : "USER_NOTIFICATION_IMED_YOU_MESSAGE_TXT";

            //for non-subs on jdate and jdate.uk, suppress member thumbnails and links
            if (brand.Site.SiteID == (int) SITE_ID.JDate || brand.Site.SiteID == (int) SITE_ID.JDateUK)
            {
                if (!notificationMember.IsPayingMember(brand.Site.SiteID))
                {
                    resourceKey = "NON_SUB_USER_NOTIFICATION_IMED_YOU_MESSAGE_TXT";
                }
            }
            Log.LogDebugMessage(string.Format("ResourceKey:{0}", resourceKey), ErrorHelper.GetCustomData());
            var text = GenerateText(brand, notificationCreator, notificationMember, resourceKey);
            Log.LogDebugMessage(string.Format("Final text:{0}", text), ErrorHelper.GetCustomData());
            var viewObj = new UserNotificationViewObject(
                notificationCreator.GetUserName(brand), 
                notificationMember.MemberID,
                notificationCreator.MemberID,
                thumbnail,
                text,
                timestamp,
                true,
                DateTime.Now,
                brand.Site.SiteID,
                brand.Site.Community.CommunityID,
                resourceKey,
                string.Empty,
                false,
                thumbnailFile, 
                NotificationType.IMedYou);
            InitializeStandardDataPoints(viewObj, notificationCreator, brand);
            if (notificationMemberIsFavorite)
            {
                viewObj.DataPoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }

            UserNotificationsServiceSA.Instance.AddUserNotification(unParams, viewObj);
        }

        /// <summary>
        /// </summary>
        /// <param name = "toMemberId"></param>
        /// <param name = "brand"></param>
        /// <param name = "fromMemberId"></param>
        public static void AddUserNotificationForYNM(int fromMemberId, int toMemberId, Brand brand)
        {
            if (!Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_USER_NOTIFICATIONS", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID)))
                return;

            var notificationCreator = MemberSA.Instance.GetMember(fromMemberId, MemberLoadFlags.None);
            var notificationMember = MemberSA.Instance.GetMember(toMemberId, MemberLoadFlags.None);

            var list = ListSA.Instance.GetList(notificationCreator.MemberID);
            var clickMask = list.GetClickMask(brand.Site.Community.CommunityID, brand.Site.SiteID, notificationMember.MemberID);
            var isMutualYes = ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes
                               && (clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes);

            // Only send User Notificatoin on MutualYes
            if (!isMutualYes) return;

            const string resourceKey = "USER_NOTIFICATION_MUTUAL_YES_MESSAGE_TXT";

            //add to both users' notifications
            var unParams = UserNotificationParams.GetParamsObject(toMemberId, brand.Site.SiteID, brand.Site.Community.CommunityID);
            var thumbnail = GetThumbnail(brand, notificationCreator, notificationMember);
            var thumbnailFile = GetThumbnailFile(brand, notificationCreator, notificationMember);
            var timestamp = DateFormatter.ElapsedTimeText(brand, DateTime.Now);
            var text = GenerateText(brand, notificationCreator, notificationMember, resourceKey);

            var viewObj = new UserNotificationViewObject(
                notificationCreator.GetUserName(brand),
                notificationMember.MemberID,
                notificationCreator.MemberID,
                thumbnail,
                text,
                timestamp,
                true,
                DateTime.Now,
                brand.Site.SiteID,
                brand.Site.Community.CommunityID,
                resourceKey,
                string.Empty,
                false,
                thumbnailFile, 
                NotificationType.MutualYes);
            InitializeStandardDataPoints(viewObj, notificationCreator, brand);
            bool notificationMemberIsFavorite = HotListAccess.MemberIsOnHotlist(brand, HotListCategory.Default, notificationMember.MemberID, notificationCreator.MemberID);
            if (notificationMemberIsFavorite)
            {
                viewObj.DataPoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }
            UserNotificationsServiceSA.Instance.AddUserNotification(unParams, viewObj);

            var unParams2 = UserNotificationParams.GetParamsObject(fromMemberId, brand.Site.SiteID, brand.Site.Community.CommunityID);
            thumbnail = GetThumbnail(brand, notificationMember, notificationCreator);
            thumbnailFile = GetThumbnailFile(brand, notificationMember, notificationCreator);
            text = GenerateText(brand, notificationMember, notificationCreator, resourceKey);

            var viewObj2 = new UserNotificationViewObject(
                notificationMember.GetUserName(brand),
                notificationCreator.MemberID,
                notificationMember.MemberID,
                thumbnail,
                text,
                timestamp,
                true,
                DateTime.Now,
                brand.Site.SiteID,
                brand.Site.Community.CommunityID,
                resourceKey,
                string.Empty,
                false,
                thumbnailFile,
                NotificationType.MutualYes);
            bool notificationCreatorIsFavorite = HotListAccess.MemberIsOnHotlist(brand, HotListCategory.Default, notificationCreator.MemberID, notificationMember.MemberID);
            InitializeStandardDataPoints(viewObj2, notificationMember, brand);
            if (notificationCreatorIsFavorite)
            {
                viewObj2.DataPoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }

            UserNotificationsServiceSA.Instance.AddUserNotification(unParams2, viewObj2);
        }

        public static void InitializeStandardDataPoints(UserNotificationViewObject userNotificationViewObject, Member creator, Brand b)
        {
            try
            {
                if (creator != null)
                {
                    Dictionary<string, string> datapoints = userNotificationViewObject.DataPoints;
                    int genderMask = creator.GetAttributeInt(b, "GenderMask");
                    int regionId = creator.GetAttributeInt(b, "RegionId");
                    string maritalStatus = GlobalUtils.Singleton.GetContent("MaritalStatus", creator.GetAttributeInt(b, "MaritalStatus"), b);

                    datapoints.Add(UserNotificationDataPointConstants.CREATOR_USER_NAME, creator.GetUserName(b));
                    datapoints.Add(UserNotificationDataPointConstants.CREATOR_AGE, GlobalUtils.Singleton.GetAge(creator, b).ToString());
                    datapoints.Add(UserNotificationDataPointConstants.CREATOR_GENDERMASK, GlobalUtils.Singleton.GetGenderMaskString(genderMask, b));
                    datapoints.Add(UserNotificationDataPointConstants.CREATOR_LOCATION, GlobalUtils.Singleton.GetRegionString(regionId, b.Site.LanguageID, false, true, false, b));
                    datapoints.Add(UserNotificationDataPointConstants.CREATOR_MARITAL_STATUS, maritalStatus);
                }
            }
            catch (Exception e)
            {
                Log.LogError("Could not initialize data points!!", e, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally(b));
            }
        }

        public static List<UserNotificationViewObject> GetUserNotifications(int memberId, Brand brand, int startNum, int pageSize, bool refreshDataPointCache)
        {
            var unParams = UserNotificationParams.GetParamsObject(memberId,
                                                      brand.Site.SiteID,
                                                      brand.Site.Community.CommunityID);
            return UserNotificationsServiceSA.Instance.GetUserNotificationsWithDataPoints(unParams, startNum, pageSize, refreshDataPointCache);
        }
    }
}
