﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Photos;

namespace Spark.Rest.BedrockPorts
{
	public class PhotoManager
	{
		
		///// <summary>
		///// Retrieve all member photos not caring for any sort of approval status. This method checks for PhotoIsEmpty before including
		///// the photo in the return list.
		///// </summary>
		///// <param name="photoType">PhotoType to use when checking for photo is empty.</param>
		///// <returns>List of Photo objects or null if member has none.</returns>
		//public List<Photo> GetAllPhotos(IMember member, Brand brand, PhotoType photoType)
		//{
		//    var photos = member.GetPhotos(brand.Site.Community.CommunityID);
		//    if (photos == null || photos.Count == 0)
		//        return null;

		//    var allPhotos = new List<Photo>();
		//    for (byte i = 0; i < photos.Count; i++)
		//    {
		//        allPhotos.Add(photos[i]);
		//    }

		//    allPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

		//    return allPhotos;
		//}

		//public bool updatePhotoCaption(IMember member, Brand brand, int memberPhotoId, string caption)
		//{
		//    if (member != null || memberPhotoId <= 0)
		//    {
		//        return false;
		//    }
		//    if (memberPhotoId > 0)
		//    {
		//        var photos = GetAllPhotos(member, brand, PhotoType.Full);
		//        Photo photo = null;
		//        foreach (var tempPhoto in photos.Where(tempPhoto => tempPhoto.MemberPhotoID == memberPhotoId))
		//        {
		//            photo = tempPhoto;
		//        }
		//        if (photo != null)
		//        {
		//            if ( photo.Caption == caption)
		//            {
		//                return true;
		//            }

		//            var update = new PhotoUpdate(photo.)
		//            _photoUpdates.Add(new PhotoUpdate(fileBytes,
		//                                              false,
		//                                              memberPhotoID,
		//                                              photo.FileID,
		//                                              photo.FileWebPath,
		//                                              photo.ThumbFileID,
		//                                              photo.ThumbFileWebPath,
		//                                              listOrder,
		//                                              photo.IsApproved,
		//                                              isPrivate,
		//                                              photo.AlbumID,
		//                                              photo.AdminMemberID, caption, isCaptionApproved,
		//                                              photo.FileCloudPath, photo.ThumbFileCloudPath,
		//                                              photo.IsApprovedForMain, isMain, true));
		//        }
		//    }
		//}
	}
}