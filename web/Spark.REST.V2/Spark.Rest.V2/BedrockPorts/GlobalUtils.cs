﻿using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Member.ValueObjects.Interfaces;
using Spark.Common.Localization;
using System;
using Spark.Logger;

namespace Spark.Rest.V2.BedrockPorts
{
    public class GlobalUtils
    {
         private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(GlobalUtils));
        public static readonly GlobalUtils Singleton = new GlobalUtils();

        private GlobalUtils(){}

        public string GetContent(string pAttributeName, int pAttributeValue, Brand brand)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(pAttributeName, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return String.Empty;

            AttributeOption attributeOption = attributeOptionCollection[pAttributeValue];

            if (attributeOption != null)
            {
                return ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo,
                    ResourceGroupEnum.Global, 
                    attributeOption.ResourceKey);
            }

            return String.Empty;
        }

        public int GetAge(DateTime birthDate)
        {
            if (birthDate == DateTime.MinValue) return 0;

            int age = DateTime.Today.Year - birthDate.Year; 
            if (birthDate > DateTime.Today.AddYears(-age)) 
                age--; 
            return age;
        }

        public int GetAge(IMemberDTO member, Brand brand)
        {
            if (member != null && brand != null)
                return GetAge(member.GetAttributeDate(brand, "BirthDate", DateTime.MinValue));
            else
                return 0;
        }

        public string GetGenderMaskString(int genderMask, Brand brand)
        {
            string gender = GetGenderString(genderMask, brand);
            string seeking = GetSeekingGenderString(genderMask, brand);

            if (seeking == string.Empty || gender == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                return gender + " " + ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global, "SEEKING") + " " + seeking;
            }
        }

        public string GetGenderString(int genderMask, Brand brand)
        {
            string genderStr = string.Empty;

            if (genderMask < 0)
                return genderStr;

            if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                return ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global, "MALE");

            if ((genderMask & ConstantsTemp.GENDERID_FEMALE) == ConstantsTemp.GENDERID_FEMALE)
                return ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global, "FEMALE");

            if ((genderMask & ConstantsTemp.GENDERID_MTF) == ConstantsTemp.GENDERID_MTF)
                return ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global, "MTF");

            if ((genderMask & ConstantsTemp.GENDERID_FTM) == ConstantsTemp.GENDERID_FTM)
                return ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global, "FTM");

            // Else
            return genderStr;
        }

        public string GetSeekingGenderString(int genderMask, Brand brand)
        {
            string seeking = string.Empty;

            if ((genderMask & ConstantsTemp.GENDERID_SEEKING_FEMALE) == ConstantsTemp.GENDERID_SEEKING_FEMALE)
                seeking = ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global, "SEEKING_FEMALE");

            if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MALE) == ConstantsTemp.GENDERID_SEEKING_MALE)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global, "SEEKING_MALE");
            }

            if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MTF) == ConstantsTemp.GENDERID_SEEKING_MTF)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global, "SEEKING_MTF");
            }

            if ((genderMask & ConstantsTemp.GENDERID_SEEKING_FTM) == ConstantsTemp.GENDERID_SEEKING_FTM)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + ResourceProvider.Instance.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, ResourceGroupEnum.Global, "SEEKING_FTM");
            }

            return seeking;
        }

        private static bool isDefaultRegion(RegionLanguage regionLanguage, Brand brand)
        {
            if (brand == null || regionLanguage.CountryRegionID == brand.Site.DefaultRegionID)
            {
                return true;
            }
            return false;
        }

        public string GetRegionString(int regionID, int languageID, bool showPostalCode, bool showAbbreviatedState, bool showAbbreviatedCountry, Brand brand)
        {
            return GetRegionString(regionID, languageID, showPostalCode, showAbbreviatedState, showAbbreviatedCountry, true, brand);
        }

        public string GetRegionString(int regionID, int languageID, bool showPostalCode, bool showAbbreviatedState, bool showAbbreviatedCountry, bool showFullState, Brand brand)
        {
            string regionString = string.Empty;

            if (regionID > 0)
            {
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);

                // Incrementally build up the regionString with commas where necessary
                // based on the contents being empty or not before and after each comma
                // This made for CI/CP and tested on CL/GL 
                // start WEL
                regionString = regionLanguage.CityName;

                string stateString = string.Empty;

                // TT #13622, hide the state name if the region is not in the same country as the Site's default region.
                if (isDefaultRegion(regionLanguage, brand) && (showFullState || showAbbreviatedState))
                {
                    if (showAbbreviatedState && regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                    {
                        stateString = regionLanguage.StateAbbreviation;
                    }
                    else
                    {
                        stateString = regionLanguage.StateDescription;
                    }
                }

                if (!string.IsNullOrEmpty(regionString) && !string.IsNullOrEmpty(stateString))
                {
                    regionString += ", ";
                }
                regionString += stateString;

                if (!isDefaultRegion(regionLanguage, brand))
                {
                    string countryString = string.Empty;
                    if (showAbbreviatedCountry)
                    {
                        countryString = regionLanguage.CountryAbbreviation;
                    }
                    else
                    {
                        countryString = regionLanguage.CountryName;
                    }
                    if (!string.IsNullOrEmpty(regionString) && !string.IsNullOrEmpty(countryString))
                    {
                        regionString += ", ";
                    }
                    regionString += countryString;
                }

                // end WEL

                if (regionString != null && regionString.Length > ConstantsTemp.MAX_REGION_STRING_LENGTH)
                {
                    regionString = regionString.Substring(0, ConstantsTemp.MAX_REGION_STRING_LENGTH - 3) + "...";
                }
            }
            else
            {
                regionString = "N/A";
            }
            return regionString;
        }

    }
}