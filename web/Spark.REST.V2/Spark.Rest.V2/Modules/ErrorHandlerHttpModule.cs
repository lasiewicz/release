﻿#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Web;
using Newtonsoft.Json;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.Rest.Modules;
using Spark.Rest.Serialization;
using Spark.Rest.V2.Exceptions;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

#endregion

namespace Spark.Rest.V2.Modules
{
    /// <summary>
    ///     This is the only global error handler of this solution. ASP.NET's error handling features have been disabled in web.config
    /// </summary>
    // ReSharper disable InconsistentNaming
    public class ErrorHandlerHttpModule : IHttpModule
    {
        /// <summary>
        /// 
        /// </summary>
        public const string IS_PROCESSED_KEY = "IsProcessed";

        private static readonly ISparkLogger Log =
            SparkLoggerManager.Singleton.GetSparkLogger(typeof (ErrorHandlerHttpModule));

        #region IHttpModule Members

        public void Init(HttpApplication context)
        {
            context.Error += Application_Error;
            context.EndRequest += Application_EndRequest;
        }

        public void Dispose()
        {
        }

        #endregion

        /// <summary>
        ///     This is the only event that is guranteed to be raised
        ///     Should we be handling errors here when we're intercepting errors with Application_Error?
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Application_EndRequest(object sender, EventArgs e)
        {
            // need try catch here otherwise we could get unhandled exception
            try
            {
                // do not process any further on OK status
                if (HttpContext.Current.Response.StatusCode == (int) HttpStatusCode.OK) return;

                // only allow the following statues to fall through for error logging
                if (HttpContext.Current.Response.StatusCode != (int) HttpStatusCode.NotFound &&
                    HttpContext.Current.Response.StatusCode != (int) HttpStatusCode.BadRequest &&
                    HttpContext.Current.Response.StatusCode != (int) HttpStatusCode.Forbidden &&
                    HttpContext.Current.Response.StatusCode != (int) HttpStatusCode.InternalServerError &&
                    HttpContext.Current.Response.StatusCode != (int) HttpStatusCode.Unauthorized &&
                    HttpContext.Current.Response.StatusCode != (int) HttpStatusCode.BadRequest &&
                    HttpContext.Current.Response.StatusCode != (int)HttpStatusCode.ServiceUnavailable) return;

                var contextWrapper = new HttpContextWrapper(((HttpApplication) sender).Context);

                // if the response is in json, return as it is.
                // Probaby due to Application_Error > ReturnErrorResponse handling?
                if (HttpContext.Current.Response.ContentType.ToLower().Contains("json"))
                {
                    // logging here to see what kind of requests actually end up in this condition
                    // we should remove the use of this check 
                    Log.LogDebugMessage("Contains json is true for request.", ErrorHelper.GetCustomData(contextWrapper));
                    return;
                }

                // why was the use of this check added? 
                if (HttpContext.Current.Items.Contains(IS_PROCESSED_KEY))
                {
                    // logging here to see what kind of requests actually end up in this condition
                    // we should remove the use of this check 
                    Log.LogDebugMessage("IS_PROCESSED_KEY is true for request.",
                        ErrorHelper.GetCustomData(contextWrapper));
                    return;
                }
                HttpContext.Current.Items[IS_PROCESSED_KEY] = bool.TrueString.ToLower();

                ProcessHttpException(contextWrapper);
            }
            catch (Exception exception)
            {
                Log.LogException("Application_EndRequest unhandled error", exception);
            }
        }

        /// <summary>
        ///     This can be raised anytime during the life cycle
        ///     Why was this event added when Application_EndRequest is processing errors?
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Application_Error(object sender, EventArgs e)
        {
            // need try catch here otherwise we could get unhandled exception
            try
            {
                var contextWrapper = new HttpContextWrapper(((HttpApplication)sender).Context);
                ProcessHttpException(contextWrapper);
            }
            catch (Exception exception)
            {
                Log.LogException("Application_Error unhandled error", exception);
            }
        }

        /// <summary>
        ///     The site should be running in HttpRuntime.UsingIntegratedPipeline
        /// </summary>
        /// <param name="contextWrapper"></param>
        private static void ProcessHttpException(HttpContextWrapper contextWrapper)
        {
            HttpException httpException = null;
            Dictionary<string, string> customData = null;

            try
            {
                var originalexception = HttpContext.Current.Server.GetLastError();
                var exception = originalexception == null ? null : originalexception.GetBaseException();
                if (exception == null)
                {
                    exception = new HttpException(contextWrapper.Response.StatusCode,
                        contextWrapper.Response.StatusDescription,
                        contextWrapper.Response.SubStatusCode);
                }
                else if (exception is SparkAPIReportableException)
                {
                    //400
                    var sparkAPIReportableException = exception as SparkAPIReportableException;
                    exception = new HttpException((int)HttpStatusCode.BadRequest, exception.Message, (int)sparkAPIReportableException.SubStatusCode);
                }
                else if (!(exception is HttpException))
                {
                    //500
                    var jsonError = new JsonError { UniqueId = Convert.ToString(Guid.NewGuid()), Message = exception.Message };
                    var he = new HttpException((int) HttpStatusCode.InternalServerError, jsonError.Message,
                        (int) HttpSub500StatusCode.InternalServerError);
                    he.Data["uniqueId"] = jsonError.UniqueId;
                    exception = he;
                }

                httpException = exception as HttpException;
                var httpStatusCode = httpException.GetHttpCode();

                customData = ErrorHelper.GetCustomData(contextWrapper);
                if (originalexception == null)
                {
                    originalexception = exception;
                }

                if (httpStatusCode == (int) HttpStatusCode.Unauthorized)
                {
                    Log.LogWarningMessage(originalexception.Message, customData);
                }
                else if (httpStatusCode != (int) HttpStatusCode.BadRequest)
                {
                    Log.LogException(originalexception.Message, originalexception, customData, ErrorHelper.ShouldLogExternally());
                }

                ReturnErrorResponse(httpException, httpException.GetHttpCode());

                HttpContext.Current.Server.ClearError();
            }
            catch (Exception exception)
            {
                // nothing should blow up in catch block. be as safe as possible with using custom objects.
                var message = (httpException != null && string.IsNullOrEmpty(httpException.Message))
                    ? httpException.Message
                    : "httpexception message not found";

                // even if customData is null, that's handled internally by the method
                Log.LogException(message, exception, customData);

                ReturnErrorResponse(httpException, (int)HttpStatusCode.InternalServerError);
                
                //make sure to log this in the web log
                LoggingHttpModule.LogInfo(contextWrapper);
            }
        }

        /// <summary>
        ///     Returns the current request with an error response in json
        /// </summary>
        /// <param name="httpException"></param>
        /// <param name="httpStatusCode"></param>
        private static void ReturnErrorResponse(HttpException httpException, int httpStatusCode)
        {
            var contextWrapper = new HttpContextWrapper(HttpContext.Current.ApplicationInstance.Context);
            var statusCode = httpStatusCode;
            var subStatusCode = 0;
            var statusDescription = httpStatusCode.ToString();
            var errorMessage = contextWrapper.Response.StatusDescription;
            if (null != httpException)
            {
                statusCode = httpException.GetHttpCode();
                subStatusCode = httpException.ErrorCode;
                if (statusCode > 0)
                {
                    statusDescription = Enum.Parse(typeof (HttpStatusCode), statusCode.ToString(CultureInfo.InvariantCulture)).ToString();
                }
                errorMessage = httpException.Message;
            }
            contextWrapper.Response.ClearHeaders();
            contextWrapper.Response.ClearContent();
            contextWrapper.Response.TrySkipIisCustomErrors = true;
            contextWrapper.Response.StatusCode = statusCode;
            contextWrapper.Response.SubStatusCode = subStatusCode;
            contextWrapper.Server.ClearError();
            contextWrapper.Response.AddHeader("content-type", "application/json");
            var jsonResult = new NewtonsoftJsonResult
            {
                Result = new
                {
                    code = statusCode,
                    status = statusDescription,
                    error = new
                    {
                        code = subStatusCode,
                        message = errorMessage,
                    }
                }
            };
            contextWrapper.Response.Write(JsonConvert.SerializeObject(jsonResult.Result));
            contextWrapper.Items[LoggingHttpModule.IS_LOGGED_KEY] = bool.FalseString.ToLower();       
        }
    }
}