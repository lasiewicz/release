﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml;
using System.Xml.Linq;
using Matchnet;
using Matchnet.ActivityRecording.ServiceAdapters;
using Newtonsoft.Json;
using Spark.Logger;
using Spark.Managers.Managers;
using Spark.Rest.Helpers;
using Spark.REST.Helpers;

namespace Spark.Rest.Modules
{
    public class LoggingHttpModule : IHttpModule
    {
        public const string IS_LOGGED_KEY = "__IsLogged__";
        public const string STOPWATCH_KEY = "__Stopwatch__";
        public const string ELLAPSED_REQUEST_TIME_KEY = "__ellapsed_req_millis__";
        public const string LOG_FORMAT = "OriginIP: {0}, ClientIP: {1}, Url: '{2}', Http Status Code: {3}, Http SubStatus Code: {4}, Http Verb: {5}, App ID: {6}, Member ID: {7}, Brand ID: {8}, Host: '{9}', MachineName: '{10}', ActionName: '{11}', Request Time: {12}ms";
        private static ISparkLogger _log = null;

        public static ISparkLogger LogApiRequest
        {
            set { _log = value; }
            get
            {
                if (null == _log)
                {
                    _log = SparkLoggerManager.Singleton.GetSparkLogger("RollingWebLogFileAppender");
                }
                return _log;
            }
        }

        private static ISparkLogger _logError = null;

        public static ISparkLogger LogError
        {
            set { _logError = value; }
            get
            {
                if (null == _logError)
                {
                    _logError = SparkLoggerManager.Singleton.GetSparkLogger(typeof(LoggingHttpModule));
                }
                return _logError;
            }
        }

        private static IActivityRecordingSA _activityRecordingSA = null;

        public static IActivityRecordingSA ActivityRecordingService
        {
            get
            {
                if (null == _activityRecordingSA)
                {
                    _activityRecordingSA = ActivityRecordingSA.Instance;
                }
                return _activityRecordingSA;
            }
            set { _activityRecordingSA = value; }
        }

        private static SettingsManager _settingsManager = new SettingsManager();

        public static SettingsManager SettingsManager
        {
            get { return _settingsManager; }
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += Application_PreRequestHandlerExecute;
            context.Error += Application_Error;
            context.PostLogRequest += Application_PostRequestHandlerExecute;
        }

        public void Dispose()
        {
        }

        private static void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            HttpApplication httpApplication = (HttpApplication)sender;
            var contextWrapper = new HttpContextWrapper(httpApplication.Context);
            if (contextWrapper.Items != null && contextWrapper.Items.Contains(IS_LOGGED_KEY)) return;
            Stopwatch stopWatch = GetStopWatch(contextWrapper);
            stopWatch.Start();

        }

        private static void Application_PostRequestHandlerExecute(object sender, EventArgs e)
        {
            HttpApplication httpApplication = (HttpApplication)sender;            
            var contextWrapper = new HttpContextWrapper(httpApplication.Context);
            LogInfo(contextWrapper);
        }

        private static void Application_Error(object sender, EventArgs e)
        {
            //this is here to prevent logging the 200 http code request on thrown exceptions. These excpetions will be logged with the ErrorsController.
            HttpContext.Current.Items[IS_LOGGED_KEY] = Boolean.TrueString.ToLower();
        }

        public static void LogInfo(HttpContextBase contextBase)
        {
            try
            {
                if (contextBase.Items != null && contextBase.Items.Contains(IS_LOGGED_KEY)
                    && contextBase.Items[IS_LOGGED_KEY].ToString().ToLower() == Boolean.TrueString.ToLower()) return;
                contextBase.Items[IS_LOGGED_KEY] = Boolean.TrueString.ToLower();
                Stopwatch stopWatch = GetStopWatch(contextBase);
                stopWatch.Stop();
                int ellaspedReqTimeInMillis = 0;
                if (contextBase.Items.Contains(ELLAPSED_REQUEST_TIME_KEY))
                {
                    ellaspedReqTimeInMillis = (int)contextBase.Items[ELLAPSED_REQUEST_TIME_KEY];
                }
                // Origin/Client IP addresses, URL, HTTP verb, APPID, MemberID (if available)
                string originIP = GetOriginIP(contextBase);
                string clientIP = MemberHelper.GetClientIP(contextBase.Request);
                HttpRequestBase httpRequestBase = contextBase.Request;
                string url = httpRequestBase.RawUrl;
                string path = httpRequestBase.Url.AbsolutePath;
                int httpStatusCode = contextBase.Response.StatusCode;
                int httpSubStatusCode = 0;
                if (HttpRuntime.UsingIntegratedPipeline)
                {
                    httpSubStatusCode = contextBase.Response.SubStatusCode;
                }
                string method = httpRequestBase.HttpMethod;
                int appID = (contextBase.Items.Contains("tokenAppId"))
                                ? Int32.Parse(contextBase.Items["tokenAppId"].ToString())
                                : 0;
                int memberID = (contextBase.Items.Contains("tokenMemberId"))
                                   ? Int32.Parse(contextBase.Items["tokenMemberId"].ToString())
                                   : 0;
                long totalEllapsedRequestTime = (stopWatch.ElapsedMilliseconds + ellaspedReqTimeInMillis);

                var brandID = 0;
                if (null != contextBase.Request.RequestContext)
                {
                    var routeData = contextBase.Request.RequestContext.RouteData;

                    if (routeData != null && routeData.Values["brandId"] != null)
                    {
                        brandID = Convert.ToInt32(routeData.Values["brandId"]);
                    }
                }

                // get url host
                string host = (null != httpRequestBase.Url) ? httpRequestBase.Url.Host.ToLower() : null;

                // get the machine name
                string machineName = Environment.MachineName;

                //route data from controller
                string actionName = null;
                RouteCollection routeCollection = new MvcHttpHandler().RouteCollection;
                RouteData data = routeCollection.GetRouteData(contextBase);
                if (null != data && null != data.Values)
                {
                    actionName = data.Values["action"].ToString();
                }

                string formDataXML = ToXML(httpRequestBase.Form);

                string requestBodyXML = null;
                if (contextBase.Items.Contains("_requestBody_"))
                {
                    string requestBody = (string)contextBase.Items["_requestBody_"];
                    if (!string.IsNullOrEmpty(requestBody))
                    {                        
                        XmlDocument doc = JsonConvert.DeserializeXmlNode(@"{'data':"+requestBody+"}");
                        StringBuilder builder = new StringBuilder();
                        doc.Save(new StringWriter(builder));
                        requestBodyXML = builder.ToString();
                    }
                }

                //log to file
                LogApiRequest.LogDebugMessage(string.Format(LOG_FORMAT, originIP, clientIP, url, httpStatusCode, httpSubStatusCode, method, appID, memberID, brandID, host, machineName, actionName, totalEllapsedRequestTime), ErrorHelper.GetCustomData());

                if (SettingsManager.GetSettingBool("LOG_API_WEB_TO_DB", true)
                    && (!path.ToLower().EndsWith("/healthcheck") && !path.ToLower().EndsWith("/cachemanager.aspx")))
                {
                    //log to db
                    var ctx = System.Web.HttpContext.Current;
                    MiscUtils.FireAndForget(o =>
                    {
                        //set HttpContext for thread
                        System.Web.HttpContext.Current = ctx;
                        try
                        {
                            ActivityRecordingService.LogApiRequest(originIP, clientIP, url, httpStatusCode,
                                httpSubStatusCode, method, appID, memberID, totalEllapsedRequestTime, brandID, host,
                                machineName, actionName, formDataXML, requestBodyXML);
                        }
                        finally
                        {
                            //unset HttpContext for thread (precaution for GC)
                            System.Web.HttpContext.Current = null;                            
                        }
                    },
                        "LoggingHttpModule", "Could not log api requests to api web log!");
                }
            }
            catch(Exception ex)
            {
                LogError.LogError("Could not log api request!", ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
            }
        }

        public static Stopwatch GetStopWatch(HttpContextBase contextBase)
        {
            Stopwatch stopWatch = null;
            if (!contextBase.Items.Contains(STOPWATCH_KEY))
            {
                contextBase.Items[STOPWATCH_KEY] = new Stopwatch();
            }
            stopWatch = (Stopwatch)contextBase.Items[STOPWATCH_KEY];
            return stopWatch;
        }

        public static string GetOriginIP(HttpContextBase contextBase)
        {
            string _originIP = contextBase.Request.Headers["X-Spark-OriginIP"] ??
                               contextBase.Request.ServerVariables["X-Spark-OriginIP"];

            Int32 commaPos = (!String.IsNullOrEmpty(_originIP)) ? _originIP.IndexOf(",") : -1;
            if (commaPos > -1)
            {
                _originIP = _originIP.Substring(0, commaPos);
            }

            return _originIP;
        }

        public static string ToXML(NameValueCollection nameValueCollection)
        {
            IEnumerable<string> keys;

            try
            {
                keys = nameValueCollection.AllKeys.Where(k => k != null);
            }
            catch (HttpRequestValidationException)
            {
                return null;
            }


            var xmlStringBuilder = new StringBuilder();
            string format = "<{0}>{1}</{0}>";

            foreach(string key in keys)
            {
                try
                {

                    var cdata = new XCData(nameValueCollection[key]);

                    xmlStringBuilder.AppendFormat(format, XmlKeyFormatter(key), cdata);
                }
                catch (HttpRequestValidationException e)
                {
                    // If changing QueryString to be of type string in future, will need to account for possible
                    // illegal values - in this case it is contained at the end of e.Message along with an error message

                    int firstInstance = e.Message.IndexOf('\"');
                    int lastInstance = e.Message.LastIndexOf('\"');

                    if (firstInstance != -1 && lastInstance != -1)
                    {
                        var exCdata = new XCData(e.Message.Substring(firstInstance + 1, lastInstance - firstInstance - 1));
                        xmlStringBuilder.AppendFormat(format, XmlKeyFormatter(key), exCdata);
                    }
                    else
                    {
                        xmlStringBuilder.AppendFormat(format, XmlKeyFormatter(key), String.Empty);
                    }
                }
            }

            if (xmlStringBuilder.Length == 0) return null;

            return string.Format(format,"data",xmlStringBuilder);
        }

        private static string XmlKeyFormatter(string xmlKey)
        {
            var xmlFriendlyKey = string.Empty;
            try
            {
                XmlConvert.VerifyName(xmlKey);
                xmlFriendlyKey = xmlKey;
            }
            catch  //lets escape the key
            {
                xmlFriendlyKey = XmlConvert.EncodeName(xmlKey);
            }

            return xmlFriendlyKey;
        }
    }
}
