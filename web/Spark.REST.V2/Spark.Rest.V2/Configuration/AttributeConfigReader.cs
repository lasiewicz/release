﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Spark.Logger;
using Spark.Rest.Helpers;
using Spark.Rest.V2.Configuration;

namespace Spark.REST.Configuration
{
    /// <summary>
    /// </summary>
    public class AttributeConfigReader
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(AttributeConfigReader));

        private static volatile object _listLocker = new object();
        private static volatile object _dictLocker = new object();
        private static volatile object _attributeSetLocker = new object();
        private static volatile object _listLocker2 = new object();
        private static volatile object _dictLocker2 = new object();

        internal static AttributeConfigReader Instance = new AttributeConfigReader();

        private static string _xmlPathForAttributeSet;
        private static string _xmlPathForPropertyWhitelist;
        private static string _xmlPathForMigrationAttributeBlacklist;

        private static volatile PropertyWhitelist _propertyWhitelist;
        private static volatile MigrationAttributeBlacklist _migrationAttributeBlacklist;
        private Dictionary<string, AttributeSet> _attributeSetDictionary;

        private volatile Dictionary<string, ProfileProperty> _attributeWhitelistDictionary;
        private volatile Dictionary<string, ProfileProperty> _attributeWhitelistDictionaryByAttribute;
        private volatile Dictionary<string, MingleMigrationAttribute> _mingleAttributeBlacklistDictionaryByAttribute;

        private AttributeConfigReader()
        {
        }

        /// <summary>
        ///     Gets or sets the attribute set XML path.
        /// </summary>
        /// <value>
        ///     The attribute set XML path.
        /// </value>
        public static string AttributeSetXmlPath
        {
            get
            {
                return _xmlPathForAttributeSet ??
                       Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration/AttributeSetList.xml");
            }
            set { _xmlPathForAttributeSet = value; }
        }

        /// <summary>
        ///     Gets or sets the property whitelist XML path.
        /// </summary>
        /// <value>
        ///     The property whitelist XML path.
        /// </value>
        public static string PropertyWhitelistXmlPath
        {
            get
            {
                return _xmlPathForPropertyWhitelist ??
                       Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration/AttributeWhitelist.xml");
            }
            set { _xmlPathForPropertyWhitelist = value; }
        }

        /// <summary>
        ///     Gets or sets the migration attribute blacklist XML path.
        /// </summary>
        /// <value>
        ///     The migration attribute blacklist XML path.
        /// </value>
        public static string MigrationAttributeBlacklistXmlPath
        {
            get
            {
                return _xmlPathForMigrationAttributeBlacklist ??
                       Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                           "Configuration/MingleMigrationAttributeLastUpdateBlackList.xml");
            }
            set { _xmlPathForMigrationAttributeBlacklist = value; }
        }

        /// <summary>
        ///     Gets the property whitelist.
        /// </summary>
        /// <value>
        ///     The property whitelist.
        /// </value>
        public static PropertyWhitelist PropertyWhitelist
        {
            get
            {
                if (_propertyWhitelist == null)
                {
                    lock (_listLocker)
                    {
                        if (_propertyWhitelist != null) return _propertyWhitelist;

                        var xmlPath = PropertyWhitelistXmlPath;
                        {
                            TextReader textReader = null;
                            try
                            {
                                textReader = new StreamReader(xmlPath);
                                var deserializer = new XmlSerializer(typeof (PropertyWhitelist));
                                _propertyWhitelist = deserializer.Deserialize(textReader) as PropertyWhitelist;
                            }
                            catch (Exception ex)
                            {
                                Log.LogError("Failed to load attribute whitelist",ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                            }
                            finally
                            {
                                if (textReader != null)
                                {
                                    textReader.Close();
                                }
                            }
                        }
                    }
                }
                return _propertyWhitelist;
            }
        }
        
        internal Dictionary<string, ProfileProperty> AttributeWhitelistDictionary
        {
            get
            {
                if (_attributeWhitelistDictionary == null)
                {
                    lock (_dictLocker)
                    {
                        if (_attributeWhitelistDictionary != null) return _attributeWhitelistDictionary;
                        if (PropertyWhitelist.Properties.Count <= 0) return _attributeWhitelistDictionary;
                        var tempDict = new Dictionary<string, ProfileProperty>(PropertyWhitelist.Properties.Count);
                        foreach (var attribute in PropertyWhitelist.Properties)
                        {
                            if (tempDict.ContainsKey(attribute.Name.ToLower()))
                                throw new Exception(String.Format("duplicate whitelist key found: {0}", attribute.Name));
                            tempDict.Add(attribute.Name.ToLower(), attribute);
                        }
                        _attributeWhitelistDictionary = tempDict;
                        Log.LogInfoMessage(string.Format("Added {0} attributes to whitelist dictionary", tempDict.Count), ErrorHelper.GetCustomData());
                    }
                }
                return _attributeWhitelistDictionary;
            }
        }

        internal Dictionary<string, ProfileProperty> AttributeWhitelistDictionaryByAttribute
        {
            get
            {
                if (_attributeWhitelistDictionaryByAttribute == null)
                {
                    lock (_dictLocker)
                    {
                        if (_attributeWhitelistDictionaryByAttribute != null)
                            return _attributeWhitelistDictionaryByAttribute;
                        var reverseLookupProperties = PropertyWhitelist.Properties.Where(p => p.UseInReverseLookup).ToList();

                        if (PropertyWhitelist.Properties.Count <= 0) return _attributeWhitelistDictionaryByAttribute;
                        var tempDict = new Dictionary<string, ProfileProperty>(reverseLookupProperties.Count);
                        foreach (var attribute in reverseLookupProperties)
                        {
                            if (!string.IsNullOrEmpty(attribute.AttributeName) && tempDict.ContainsKey(attribute.AttributeName.ToLower()))
                            {
                                throw new Exception(String.Format("duplicate whitelist key found: {0}", attribute.Name));
                            }
                            if (!string.IsNullOrEmpty(attribute.AttributeName))
                            {
                                tempDict.Add(attribute.AttributeName.ToLower(), attribute);
                            }
                        }
                        _attributeWhitelistDictionaryByAttribute = tempDict;
                        Log.LogInfoMessage(string.Format("Added {0} attributes to whitelist dictionary by attribute", tempDict.Count), ErrorHelper.GetCustomData());
                    }
                }
                return _attributeWhitelistDictionaryByAttribute;
            }
        }

        internal Dictionary<string, AttributeSet> AttributeSetDictionary
        {
            get
            {
                if (_attributeSetDictionary == null)
                {
                    lock (_attributeSetLocker)
                    {
                        if (_attributeSetDictionary != null) return _attributeSetDictionary;
                        var xmlPath = AttributeSetXmlPath;
                        AttributeSetList attributeSetList = null;
                        TextReader textReader = null;
                        try
                        {
                            textReader = new StreamReader(xmlPath);
                            var deserializer = new XmlSerializer(typeof (AttributeSetList));
                            attributeSetList = deserializer.Deserialize(textReader) as AttributeSetList;
                            textReader.Close();
                        }
                        catch (Exception exception)
                        {
                            Log.LogError("Error loading AttributeSetList.xml", exception, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                        }
                        finally
                        {
                            if (textReader != null)
                                textReader.Dispose();
                        }

                        if (attributeSetList == null) return _attributeSetDictionary;
                       
                        var attributeSetGroups = GetAttributeSetGroups(attributeSetList);
                        _attributeSetDictionary = new Dictionary<string, AttributeSet>();
                        foreach (var groupItem in attributeSetGroups)
                        {
                            var setListVersion = groupItem.Key;
                            var setList = groupItem.Value;
                            
                            foreach (var set in setList.AttributeSets)
                            {
                                set.SetName = set.SetName.ToLower();
                                var attributeHashSet = new HashSet<string>();
                                // more efficient than doing Contains() on the List<>
                                foreach (var propName in set.PropertyNames)
                                {
                                    CheckPropertiesInMasterProfile(setList, setListVersion, set, propName.Name);
                                    if (attributeHashSet.Contains(propName.Name.ToLower()))
                                    {
                                        _attributeSetDictionary = null;
                                        var errorMessage = string.Format("Duplicate entry in attribute set {0} item {1}", set.SetName, propName.Name);
                                        Log.LogError(errorMessage, new Exception(), ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                                        throw new Exception(errorMessage);
                                    }
                                    ProfileProperty property;
                                    if (!AttributeWhitelistDictionary.TryGetValue(propName.Name.ToLower(), out property))
                                    {
                                        _attributeSetDictionary = null;
                                        var errorMessage = String.Format("Property {0} is listed in set {1}, but is missing from whitelist", propName.Name, set.SetName);
                                        Log.LogError(errorMessage, new Exception(), ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());                                        
                                        Log.LogError(string.Format("Whitelist contains {0} attributes", AttributeWhitelistDictionary.Count), new Exception(), ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                                        throw new Exception(errorMessage);
                                    }
                                    attributeHashSet.Add(propName.Name.ToLower());
                                    set.Properties.Add(property);
                                    Log.LogInfoMessage(string.Format("Added {0} properties to attribute set {1}.", set.Properties.Count, set.SetName), ErrorHelper.GetCustomData());
                                }
                                if (_attributeSetDictionary.ContainsKey(set.SetName))
                                {
                                    _attributeSetDictionary = null;
                                    throw new Exception(String.Format("Duplicate attribute set found {0}", set.SetName));
                                }
                                _attributeSetDictionary.Add(set.SetName, set);
                                 Log.LogInfoMessage(string.Format("Added {0} attributes to attribute set dictionary", _attributeSetDictionary.Count), ErrorHelper.GetCustomData());
                            }
                        }
                    }
				}
				return _attributeSetDictionary;
			}
		}

        static Dictionary<int, AttributeSetList> GetAttributeSetGroups(AttributeSetList attributeSetList)
        {
            var attributeSetGroups = new Dictionary<int, AttributeSetList>();
            var version = 1;
            var versionRegex = new Regex("V[2-9][0-9]*$", RegexOptions.IgnoreCase); //if the profile set has no version it is the first version set
            var firstVersionSet = attributeSetList.AttributeSets.Where(x => !versionRegex.IsMatch(x.SetName.ToLower())).ToList();
            if(firstVersionSet.Any())
                attributeSetGroups.Add(version, new AttributeSetList {AttributeSets = firstVersionSet});
            for (var i = 2; i < 1000; i++) //this iterates through the attribute set list and adds attribute set whose names end with version identifier
            {
                version = i;
                var regex = new Regex(string.Format("V{0}$",version), RegexOptions.IgnoreCase);
                var versionSet = attributeSetList.AttributeSets.Where(x => regex.IsMatch(x.SetName.ToLower())).ToList();
                if (!versionSet.Any()) break;
                attributeSetGroups.Add(version, new AttributeSetList { AttributeSets = versionSet });
            }
            return attributeSetGroups;
        }

        static void CheckPropertiesInMasterProfile(AttributeSetList setList, int version, AttributeSet profileToCheck, string propertyName)
        {
            if (version == 1) return;  //version 1 of the AttributeSetList will not be checked
            const string fullProfileSetName = "FullProfile";
            var fullProfile = GetFullProfileSet(version, fullProfileSetName, setList);
            if (fullProfile == null) return;
            var attributeSetWhiteList = new[] {fullProfile.SetName, "TargetingProfile_us", "TargetingProfile_il"}; //these profiles will not be checked against the master profile
            var propertyNameWhiteList = new[] { "primaryPhoto", "defaultPhoto" }; //this is a list of attributes than can appear in the subprofile sets but is not required in the master profile
            
            var attributeSetWhiteListToIgnore = GetAttributeSetWhiteList(attributeSetWhiteList, version);
            if (attributeSetWhiteListToIgnore.Any(x => String.Equals(x, profileToCheck.SetName, StringComparison.CurrentCultureIgnoreCase))) return;
            //this is the check that ensures the attributes in the subprofile sets are also in the master profile 
            if (propertyNameWhiteList.Any(x => String.Equals(x, propertyName, StringComparison.CurrentCultureIgnoreCase))) return;
            if (fullProfile.PropertyNames.Any(x => string.Equals(x.Name, propertyName, StringComparison.CurrentCultureIgnoreCase))) return;
            var errorMessage = string.Format(
                "The attribute set {0} has property {1} which is not contained in the master profile {2}. " +
                "This property must also be in the master profile", profileToCheck.SetName, propertyName, fullProfile.SetName);
            Log.LogError(errorMessage, new Exception(), ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
            throw new Exception(errorMessage);
        }

        static AttributeSet GetFullProfileSet(int version, string fullProfileSetName, AttributeSetList setList)
        {
            if (version == 1) return null;
            
            var fullProfileSetNameWithVersion = string.Format("{0}V{1}", fullProfileSetName, version);
            var fullProfile = setList.AttributeSets.SingleOrDefault(x => String.Equals(x.SetName, fullProfileSetNameWithVersion, StringComparison.CurrentCultureIgnoreCase));
            if (fullProfile == null)
                throw new Exception(string.Format("The attribute set list found does not contain the profile set {0}, which must be present", fullProfileSetNameWithVersion));
            return fullProfile;
        }

        static IEnumerable<string> GetAttributeSetWhiteList(string[] attributeSetWhiteList, int version)
        {
            var attributeSetListToCheck = new string[attributeSetWhiteList.Count()];
            if (version == 1) return attributeSetWhiteList;
            for(var i = 0; i < attributeSetWhiteList.Count(); i++)
            {
                attributeSetListToCheck[i] = string.Format("{0}V{1}", attributeSetWhiteList[i], version);
            }
            return attributeSetListToCheck;
        }
       

        /// <summary>
        ///     Gets the migration attribute blacklist.
        /// </summary>
        /// <value>
        ///     The migration attribute blacklist.
        /// </value>
        public static MigrationAttributeBlacklist MigrationAttributeBlacklist
        {
            get
            {
                if (_migrationAttributeBlacklist == null)
                {
                    lock (_listLocker2)
                    {
                        if (_migrationAttributeBlacklist != null) return _migrationAttributeBlacklist;
                        var xmlPath = MigrationAttributeBlacklistXmlPath;
                        {
                            TextReader textReader = null;
                            try
                            {
                                textReader = new StreamReader(xmlPath);
                                var deserializer = new XmlSerializer(typeof (MigrationAttributeBlacklist));
                                _migrationAttributeBlacklist = deserializer.Deserialize(textReader) as MigrationAttributeBlacklist;
                            }
                            catch (Exception ex)
                            {
                                Log.LogError("Failed to load attribute blacklist",ex, ErrorHelper.GetCustomData(), ErrorHelper.ShouldLogExternally());
                            }
                            finally
                            {
                                if (textReader != null)
                                {
                                    textReader.Close();
                                }
                            }
                        }
                    }
                }
                return _migrationAttributeBlacklist;
            }
        }

        internal Dictionary<string, MingleMigrationAttribute> MingleAttributeBlacklistlistDictionaryByAttribute
        {
            get
            {
                if (_mingleAttributeBlacklistDictionaryByAttribute == null)
                {
                    lock (_dictLocker2)
                    {
                        if (_mingleAttributeBlacklistDictionaryByAttribute != null)
                            return _mingleAttributeBlacklistDictionaryByAttribute;
                        if (MigrationAttributeBlacklist.AttributeBlackList.Count <= 0)
                            return _mingleAttributeBlacklistDictionaryByAttribute;
                        var tempDict = new Dictionary<string, MingleMigrationAttribute>();
                        foreach (var attribute in MigrationAttributeBlacklist.AttributeBlackList)
                        {
                            if (!string.IsNullOrEmpty(attribute.Name) && tempDict.ContainsKey(attribute.Name.ToLower()))
                            {
                                throw new Exception(String.Format("duplicate blacklist key found: {0}",attribute.Name));
                            }
                            if (!string.IsNullOrEmpty(attribute.Name))
                            {
                                tempDict.Add(attribute.Name.ToLower(), attribute);
                            }
                        }
                        _mingleAttributeBlacklistDictionaryByAttribute = tempDict;
                        Log.LogInfoMessage(string.Format("Added {0} attributes to blacklist dictionary by attribute", tempDict.Count), ErrorHelper.GetCustomData());
                    }
                }
                return _mingleAttributeBlacklistDictionaryByAttribute;
            }
        }
    }
}
