﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Matchnet.Caching;

#endregion

namespace Spark.Rest.V2.Configuration
{
    /// <summary>
    ///     Instead of System.Web.MVC.OutputCache which doesn't work with our apparent setup, created a custom one.
    /// 
    ///     Here's my SO post in detail.
    ///         http://stackoverflow.com/questions/24860685/varybyparam-fails-if-a-param-is-a-list/25222039#25222039
    /// 
    ///     Refer to ContentController -> GetAttributeOptionsCollection for a working example.
    /// 
    ///     Use of context to pass cachedKey is explained here
    ///         http://stackoverflow.com/questions/8937200/are-actionfilterattributes-reused-across-threads-how-does-that-work
    /// </summary>
    public class SparkOutputCache : ActionFilterAttribute
    {
        /// <summary>
        ///     Number of seconds
        /// </summary>
        public int Duration { get; set; }

        /// <summary>
        ///     Comma separated list of parameters to use for creating a unique cache key
        /// </summary>
        public string VaryByParam { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var cachedKey = string.Empty;

            if (filterContext.HttpContext.Request.Url != null)
            {
                var path = filterContext.HttpContext.Request.Url.PathAndQuery;
                if (null != VaryByParam)
                {
                    // e.g. langugeId;regionId
                    var varyByParams = VaryByParam.Split(';');

                    // let's see if there's a matching property in the action parameters
                    foreach (var varyByParam in varyByParams)
                    {
                        object varyByParamObject = null;

                        varyByParamObject = FindParameterValue(filterContext.ActionParameters, varyByParam);

                        // it could be null if there's a json param
                        if (varyByParamObject != null)
                            cachedKey += ":" + varyByParamObject;
                    }
                }

                // add more uniqueness with the request path
                if (!string.IsNullOrEmpty(cachedKey))
                {
                    cachedKey = path + cachedKey;
                    filterContext.HttpContext.Items["SparkOutputCacheCachedKey"] = cachedKey;
                }
            }

            if (!string.IsNullOrEmpty(cachedKey) && filterContext.HttpContext.Cache[cachedKey] != null)
                // cached version found, return and end the request
                filterContext.Result = (ActionResult) filterContext.HttpContext.Cache[cachedKey];
            else
                base.OnActionExecuting(filterContext);
        }

        public static object FindParameterValue(IDictionary<string, object> actionParameters, string varyByParam)
        {
            object varyByParamObject=null;
            // there no matching parameter in the action parameters
            if (!actionParameters.ContainsKey(varyByParam))
            {
                // what we're looking for could be a property in a complex type like request using BrandRequest
                // let's walk through the properties and see if we can find one that matches
                foreach (var actionParameter in actionParameters)
                {
                    var props = actionParameter.Value.GetType().GetProperties();
                    var param = varyByParam;
                    foreach (var prop in props.Where(prop => String.Equals(prop.Name,
                        param, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        // found a matching property. e.g. RegionRequest.LanguageId
                        varyByParamObject = prop.GetValue(actionParameter.Value).ToString();
                    }
                }
            }
            else
            {
                // found a matching parameter
                varyByParamObject = (actionParameters[varyByParam]);
            }
            return varyByParamObject;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var cachedKey = filterContext.HttpContext.Items["SparkOutputCacheCachedKey"] as string;

            if (string.IsNullOrEmpty(cachedKey)) return;

            // add a new cache item
            filterContext.HttpContext.Cache.Add(cachedKey, filterContext.Result, null,
                DateTime.Now.AddSeconds(Duration), System.Web.Caching.Cache.NoSlidingExpiration,
                System.Web.Caching.CacheItemPriority.Default, null);
            base.OnActionExecuted(filterContext);
        }
    }
}