﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Spark.Rest.V2.Configuration
{
    [Serializable]
    public class MigrationAttributeBlacklist
    {
        [XmlElement(ElementName = "Attribute")]
        public List<MingleMigrationAttribute> AttributeBlackList { get; set; }
    }

    [Serializable]
    public class MingleMigrationAttribute
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }
    }

}