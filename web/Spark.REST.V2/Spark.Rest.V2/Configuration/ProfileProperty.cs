﻿using System;
using System.Xml.Serialization;

namespace Spark.REST.Configuration
{
	[Serializable]
	public class ProfileProperty
	{
	    [XmlAttribute("Name")]
		public string Name;

		[XmlAttribute("Attribute")]
		public String AttributeName;

		[XmlAttribute("IsWriteable")]
		public bool IsWriteable;

		[XmlAttribute("ZeroMeansNull")]
		public bool ZeroMeansNull;

        [XmlAttribute("UseInReverseLookup")]
        public bool UseInReverseLookup;

        [XmlAttribute("RequiresApproval")]
	    public bool RequiresApproval;
       
	}
}
