﻿using System;

namespace Spark.Rest.Configuration
{
    ///<summary>
    ///</summary>
    public class ApiMethod : Attribute
    {
        /// <summary>
        /// Gets or sets the type of the response.
        /// </summary>
        /// <value>
        /// The type of the response.
        /// </value>
        public Type ResponseType { get; set; }
    }
}