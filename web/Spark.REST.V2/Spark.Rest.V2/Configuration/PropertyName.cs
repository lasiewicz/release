﻿using System;
using System.Xml.Serialization;

namespace Spark.REST.Configuration
{
    /// <summary>
    /// 
    /// </summary>
	[Serializable]
	public class PropertyName
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyName"/> class.
        /// </summary>
        public PropertyName()
        {
            IsCacheable = true;  
            //setting to true so we don't need to place this attribute on each items
            //xml representation (since it would default to false when serialized)
        }

        /// <summary>
        /// The name
        /// </summary>
		[XmlAttribute("Name")]
		public string Name;

        /// <summary>
        /// Determines whether this property is cacheable or not
        /// </summary>
	    [XmlAttribute("IsCacheable")] 
        public bool IsCacheable;
	}
}