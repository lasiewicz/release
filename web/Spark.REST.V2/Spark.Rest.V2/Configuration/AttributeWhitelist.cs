﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Spark.REST.Configuration
{
	[Serializable]
	public class AttributeWhitelist
	{
		[XmlElement("Attribute")]
		public List<MemberAttribute> Attributes;
	}
}