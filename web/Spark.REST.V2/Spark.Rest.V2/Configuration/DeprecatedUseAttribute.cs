﻿#region

using System.Web.Mvc;
using Spark.Logger;
using Spark.Rest.Helpers;

#endregion

namespace Spark.Rest.V2.Configuration
{
    /// <summary>
    /// 
    /// </summary>
    public class DeprecatedUseAttribute : ActionFilterAttribute
    {
        private static ISparkLogger _log = null;

        public static ISparkLogger Log
        {
            set { _log = value; }
            get
            {
                if (null == _log)
                {
                    _log = SparkLoggerManager.Singleton.GetSparkLogger("RollingDeprecatedLogFileAppender");
                }
                return _log;
            }
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            int appId = (int)httpContext.Items["tokenAppId"];
            string actionName = filterContext.ActionDescriptor.ActionName;
            object brandId = SparkOutputCache.FindParameterValue(filterContext.ActionParameters, "brandId");
            Log.LogInfoMessage(string.Format("Decprecated Action:'{0}' called by GetAppId:{1} and BrandId:{2}", actionName, appId, brandId), ErrorHelper.GetCustomData(filterContext.HttpContext));
        }
    }
}