﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Spark.REST.Models;
using Spark.Rest.Modules;

namespace Spark.Rest.V2.Configuration
{
    public class JsonNetModelBinder : IModelBinder
    {
        private IModelBinder fallbackModelBinder;

        public JsonNetModelBinder(IModelBinder fallbackModelBinder)
        {
            this.fallbackModelBinder = fallbackModelBinder;
        }

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var bindModel = this.fallbackModelBinder.BindModel(controllerContext, bindingContext);
            HttpContextBase httpContextBase = controllerContext.HttpContext;
            if (httpContextBase.Request.HttpMethod.Equals("GET", StringComparison.InvariantCultureIgnoreCase))
            {
                return bindModel;
            }
            else
            {
                //ONLY do this for POST,PUT,DELETE requests that transmit application/json
                if (bindModel is RequestBase && httpContextBase.Request.ContentType.Contains("application/json"))
                {
                    if (((RequestBase)bindModel).JSONObject == null)
                    {
                        string requestBodyText = GetRequestBodyText(httpContextBase);
                        if (!String.IsNullOrEmpty(requestBodyText))
                        {
                            httpContextBase.Items["_requestBody_"] = requestBodyText;
                            ((RequestBase)bindModel).JSONObject = JsonConvert.DeserializeObject(requestBodyText, new JsonSerializerSettings
                            {
                                DateTimeZoneHandling = DateTimeZoneHandling.Utc
                            });
                        }
                    }
                }
                return bindModel;
            }
        }

        public static string GetRequestBodyText(HttpContextBase httpContextBase)
        {
            string bodyText;
            using (var stream = httpContextBase.Request.InputStream)
            {
                stream.Seek(0, SeekOrigin.Begin);
                using (var reader = new StreamReader(stream))
                    bodyText = reader.ReadToEnd();
            }
            return bodyText;
        }
    }
}