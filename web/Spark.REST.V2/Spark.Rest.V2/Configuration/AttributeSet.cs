﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Spark.REST.Configuration
{
    /// <summary>
    /// 
    /// </summary>
	[Serializable]
	public class AttributeSet
	{
        private List<PropertyName> _nonCacheablePropertyNames;
	    private List<ProfileProperty> _nonCacheableProperties;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeSet"/> class.
        /// </summary>
        public AttributeSet() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeSet"/> class.
        /// </summary>
        /// <param name="setName">Name of the set.</param>
        /// <param name="propertyNames">The property names.</param>
        /// <param name="profileProperties">The profile properties.</param>
        public AttributeSet(string setName, List<PropertyName> propertyNames, List<ProfileProperty> profileProperties)
        {
            SetName = setName;
            PropertyNames = propertyNames;
            Properties = profileProperties;
        }

        /// <summary>
        /// The set name
        /// </summary>
		[XmlAttribute("Name")]
        public string SetName { get; set; }

        /// <summary>
        /// The property names
        /// </summary>
		[XmlElement("Attribute")]
        public List<PropertyName> PropertyNames { get; set; }


        /// <summary>
        /// The properties
        /// </summary>
        [XmlElement("Properties")]
        public List<ProfileProperty> Properties { get; set; }

        /// <summary>
        /// Gets the non cacheable property names.
        /// </summary>
        /// <value>
        /// The non cacheable property names.
        /// </value>
        [XmlIgnore]
        public List<PropertyName> NonCacheablePropertyNames
        {
           get
           { 
               if (_nonCacheablePropertyNames != null)
                   return _nonCacheablePropertyNames;
               if (PropertyNames == null || !PropertyNames.Any()) 
                   return null;
               _nonCacheablePropertyNames = PropertyNames.Where(x => !x.IsCacheable).ToList();
               return _nonCacheablePropertyNames;
           }
        }

        /// <summary>
        /// Gets the non cacheable properties.
        /// </summary>
        /// <value>
        /// The non cacheable properties.
        /// </value>
        [XmlIgnore]
	    public List<ProfileProperty> NonCacheableProperties
	    {
	        get
	        {
	            if (_nonCacheableProperties != null) 
                    return _nonCacheableProperties;
	            if (NonCacheablePropertyNames == null || Properties == null || !Properties.Any() || PropertyNames.Count() != Properties.Count())
                    return null;
                _nonCacheableProperties = Properties.Where(x => NonCacheablePropertyNames.Select(y => y.Name.ToLower()).Contains(x.Name.ToLower())).ToList();
	            return _nonCacheableProperties;
	        }
	    }

	
	}
}
