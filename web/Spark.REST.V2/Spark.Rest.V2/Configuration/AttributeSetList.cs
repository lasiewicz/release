﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Spark.REST.Configuration
{
    /// <summary>
    /// The <c>AttributeSetList</c> provides the set 
    /// of attributes that compose a profile definition
    /// (i.e. FullProfile, MiniProfile, VistorProfile, etc.)
    /// </summary>
	[Serializable]
	public class AttributeSetList
	{
        /// <summary>
        /// The full list of all attribute sets
        /// </summary>
		[XmlElement("Set")]
		public List<AttributeSet> AttributeSets;
	}
}