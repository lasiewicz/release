﻿#region

using System.Web;
using System.Web.Routing;

#endregion

namespace Spark.Rest.V2.Configuration
{
    /// <summary>
    /// 
    /// </summary>
    public class AcceptVersionConstraint : IRouteConstraint
    {
        private readonly string _allowedVersion = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string AllowedVersion
        {
            get { return _allowedVersion; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="allowedVersion"></param>
        public AcceptVersionConstraint(string allowedVersion)
        {
            _allowedVersion = allowedVersion;
        }

        #region IRouteConstraint Members

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            var acceptVersion = GetAcceptVersion(httpContext);

            return !string.IsNullOrEmpty(acceptVersion) && System.String.Compare(acceptVersion, AllowedVersion, System.StringComparison.OrdinalIgnoreCase) == 0;
        }

        private static string GetAcceptVersion(HttpContextBase httpContext)
        {
            var acceptVersion = httpContext.Request.Headers["Accept"];
            if (string.IsNullOrEmpty(acceptVersion))
            {
                acceptVersion = string.Empty;
            }
            char[] dellimeterChars = {';', ','};

            var elems = acceptVersion.Split(dellimeterChars);
            foreach (var elem in elems)
            {
                var strings = elem.Split('=');
                if (strings.Length <= 1 || System.String.Compare(strings[0].Trim(), "version", System.StringComparison.OrdinalIgnoreCase) != 0)
                    continue;
                acceptVersion = strings[1].Trim();
                break;
            }
            return acceptVersion;
        }

        #endregion
    }
}