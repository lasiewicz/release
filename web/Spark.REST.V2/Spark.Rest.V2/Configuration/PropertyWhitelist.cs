﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Spark.REST.Configuration
{
	[Serializable]
	public class PropertyWhitelist
	{
		[XmlElement("Property")]
		public List<ProfileProperty> Properties;
	}
}