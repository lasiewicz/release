﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spark.Rest.V2.Configuration
{
    /// <summary>
    /// Use this attribute to set the content type in the Response to a non-json type. This is useful for
    /// older browsers (IE9, etc) that cannot accept application/json as the content type in the Response.
    /// 
    /// </summary>
    public class ResponseContentTypeAttribute : ActionFilterAttribute
    {

        public string ContentTypeToSet { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool acceptsApplicationJSONResponse = false;
            string[] acceptTypes = filterContext.HttpContext.Request.AcceptTypes;
            if (null != acceptTypes)
            {
                foreach (string acceptType in acceptTypes)
                {
                    if (acceptType == "application/json")
                    {
                        acceptsApplicationJSONResponse = true;
                        break;
                    }
                }

                if (acceptsApplicationJSONResponse || string.IsNullOrEmpty(ContentTypeToSet))
                {
                    filterContext.RequestContext.HttpContext.Items["contentType"] = "application/json";
                }
                else
                {
                    filterContext.RequestContext.HttpContext.Items["contentType"] = ContentTypeToSet;
                }
            }
        }

    }
}