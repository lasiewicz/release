﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Entities.MemberDevices
{
    public class AddMemberDeviceResult
    {
        public string Alias { get; set; }
    }
}