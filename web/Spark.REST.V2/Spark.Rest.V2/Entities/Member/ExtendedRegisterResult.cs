﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.REST.Entities.Member
{
    [DataContract(Name = "ExtendedRegisterResult", Namespace = "")]
    public class ExtendedRegisterResult : RegisterResult
    {
        [DataMember(Name = "accessToken")]
        public string AccessToken { get; set; }

        [DataMember(Name = "AccessExpiresTime")]
        public DateTime AccessExpiresTime { get; set; }

        [DataMember(Name = "expiresIn")]
        public int ExpiresIn { get; set; }

        [DataMember(Name = "isPayingMember")]
        public bool IsPayingMember { get; set; }
    }
}