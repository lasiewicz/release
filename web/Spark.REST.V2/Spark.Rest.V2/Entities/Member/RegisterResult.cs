﻿using System.Runtime.Serialization;

namespace Spark.REST.Entities.Member
{
    [DataContract(Name = "RegisterResult", Namespace = "")]
    public class RegisterResult
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
        [DataMember(Name = "RegisterStatus")]
        public string RegisterStatus { get; set; }
        [DataMember(Name = "Username")]
        public string Username { get; set; }
    }
}
