﻿using System.Runtime.Serialization;

namespace Spark.REST.Entities.Member
{
    [DataContract(Name = "AccessInfo", Namespace = "")]
    public class AccessInfo
    {
        [DataMember(Name = "IsPayingMember")]
        public bool IsPayingMember { get; set; }
    }
}