﻿using System.Runtime.Serialization;

namespace Spark.REST.Entities.Member
{
    [DataContract(Name = "MemberStatus", Namespace = "")]
    public class MemberStatus
    {
        [DataMember(Name = "subscriptionStatus")]
        public string SubscriptionStatus { get; set; }
    }
}