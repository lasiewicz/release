﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Entities.Member
{
    [DataContract(Name = "MemberBasicLogonInfo", Namespace = "")]
    public class MemberBasicLogonInfo
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }

        [DataMember(Name = "EmailAddress")]
        public string EmailAddress { get; set; }

        [DataMember(Name = "UserName")]
        public string UserName { get; set; }
    }
}