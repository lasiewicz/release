﻿using System.Runtime.Serialization;

namespace Spark.REST.Entities.Member
{
    [DataContract(Name = "CompleteRegistrationResult", Namespace = "")]
    public class CompleteRegistrationResult
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
        [DataMember(Name = "Status")]
        public string Status { get; set; }
    }
}
