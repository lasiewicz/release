﻿using System.Runtime.Serialization;

namespace Spark.Rest.Entities.Profile
{
    /// <summary>
    /// The response object for PhotoUpload requests
    /// </summary>
	public class PhotoUploadResponse
	{
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="PhotoUploadResponse"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
		[DataMember(Name = "success")]
		public bool Success { get; set; }

        /// <summary>
        /// Gets or sets the photo identifier.
        /// </summary>
        /// <value>
        /// The photo identifier.
        /// </value>
		[DataMember(Name = "photoId")]
		public int PhotoId { get; set; }

        /// <summary>
        /// Gets or sets the failure reason.
        /// </summary>
        /// <value>
        /// The failure reason.
        /// </value>
		[DataMember(Name = "failReason")]
		public string FailureReason { get; set; }
	}
}