#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

#endregion

namespace Spark.REST.Entities.Profile
{
    /// <summary>
    ///     To be deprecated someday...
    /// </summary>
    [DataContract(Name = "MiniProfile", Namespace = "")]
    public class MiniProfile : IEntity
    {
        private string _username;

        /// <summary>
        /// </summary>
        public MiniProfile()
        {
            SelfSuspendedFlag = false;
            AdminSuspendedFlag = false;
            BlockContactByTarget = false;
            BlockSearchByTarget = false;
            RemoveFromSearchByTarget = false;
            BlockContact = false;
            BlockSearch = false;
            RemoveFromSearch = false;
        }

        // returns username or memberId if not set
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        [DataMember(Name = "username")]
        public string Username
        {
            get { return String.IsNullOrEmpty(_username) ? Id.ToString(CultureInfo.InvariantCulture) : _username; }
            set { _username = value; }
        }

        /// <summary>
        /// Gets or sets the approved photo count.
        /// </summary>
        /// <value>
        /// The approved photo count.
        /// </value>
        [DataMember(Name = "approvedPhotoCount")]
        public int ApprovedPhotoCount { get; set; }

        /// <summary>
        /// Gets or sets the default photo.
        /// </summary>
        /// <value>
        /// The default photo.
        /// </value>
        [DataMember(Name = "defaultPhoto")]
        public Photo DefaultPhoto { get; set; }

        /// <summary>
        /// Gets or sets the thumbnail URL.
        /// </summary>
        /// <value>
        /// The thumbnail URL.
        /// </value>
        [Obsolete("use default photo instead")]
        [DataMember(Name = "thumbnailUrl")]
        public string ThumbnailUrl { get; set; }

        /// <summary>
        /// Gets or sets the photo URL.
        /// </summary>
        /// <value>
        /// The photo URL.
        /// </value>
        [Obsolete("use default photo instead")]
        [DataMember(Name = "photoUrl")]
        public string PhotoUrl { get; set; }

        /// <summary>
        /// Gets or sets the marital status.
        /// </summary>
        /// <value>
        /// The marital status.
        /// </value>
        [DataMember(Name = "maritalStatus")]
        public string MaritalStatus { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        [DataMember(Name = "gender")]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the gender seeking.
        /// </summary>
        /// <value>
        /// The gender seeking.
        /// </value>
        [DataMember(Name = "genderSeeking")]
        public string GenderSeeking { get; set; }

        /// <summary>
        /// Gets or sets the looking for.
        /// </summary>
        /// <value>
        /// The looking for.
        /// </value>
        [DataMember(Name = "lookingFor")] // Relationship Type
        public List<string> LookingFor { get; set; }

        /// <summary>
        /// Gets or sets the age.
        /// </summary>
        /// <value>
        /// The age.
        /// </value>
        [DataMember(Name = "age")]
        public int? Age { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        [DataMember(Name = "location")] // City, State
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the heightcm.
        /// </summary>
        /// <value>
        /// The heightcm.
        /// </value>
        [DataMember(Name = "height")]
        public int Heightcm { get; set; }

        /// <summary>
        /// Gets or sets the last logged in.
        /// </summary>
        /// <value>
        /// The last logged in.
        /// </value>
        [DataMember(Name = "lastLoggedIn")]
        public DateTime? LastLoggedIn { get; set; }

        /// <summary>
        /// Gets or sets the last updated profile.
        /// </summary>
        /// <value>
        /// The last updated profile.
        /// </value>
        [DataMember(Name = "lastUpdatedProfile")]
        public DateTime? LastUpdatedProfile { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is online.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is online; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "isOnline")]
        public bool IsOnline { get; set; }

        /// <summary>
        /// Gets or sets the subscription status.
        /// </summary>
        /// <value>
        /// The subscription status.
        /// </value>
        [DataMember(Name = "subscriptionStatus")]
        public string SubscriptionStatus { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        /// <value>
        /// The zip code.
        /// </value>
        [DataMember(Name = "zipCode")]
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the registration date.
        /// </summary>
        /// <value>
        /// The registration date.
        /// </value>
        [DataMember(Name = "registrationDate")]
        public DateTime? RegistrationDate { get; set; }

        /// <summary>
        /// Gets or sets the subscription status gam.
        /// </summary>
        /// <value>
        /// The subscription status gam.
        /// </value>
        [DataMember(Name = "subscriptionStatusGam")]
        public string SubscriptionStatusGam { get; set; }

        /// <summary>
        /// Gets or sets the j date ethnicity.
        /// </summary>
        /// <value>
        /// The j date ethnicity.
        /// </value>
        [DataMember(Name = "jDateEthnicity")]
        public string JDateEthnicity { get; set; }

        /// <summary>
        /// Gets or sets the j date relegion.
        /// </summary>
        /// <value>
        /// The j date relegion.
        /// </value>
        [DataMember(Name = "jDateReligion")]
        public string JDateRelegion { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is highlighted.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is highlighted; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "isHighlighted")]
        public bool IsHighlighted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is spotlighted.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is spotlighted; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "isSpotlighted")]
        public bool IsSpotlighted { get; set; }

        /// <summary>
        ///     False by default
        /// </summary>
        [DataMember(Name = "selfSuspendedFlag")]
        public bool SelfSuspendedFlag { get; set; }

        /// <summary>
        ///     False by default
        /// </summary>
        [DataMember(Name = "adminSuspendedFlag")]
        public bool AdminSuspendedFlag { get; set; }

        /// <summary>
        ///     Only applicable when two members are involved.
        ///     False by default.
        /// </summary>
        [DataMember(Name = "blockContactByTarget")]
        public bool BlockContactByTarget { get; set; }

        /// <summary>
        ///     Only applicable when two members are involved.
        ///     False by default.
        /// </summary>
        [DataMember(Name = "blockSearchByTarget")]
        public bool BlockSearchByTarget { get; set; }

        /// <summary>
        ///     Only applicable when two members are involved.
        ///     False by default.
        /// </summary>
        [DataMember(Name = "removeFromSearchByTarget")]
        public bool RemoveFromSearchByTarget { get; set; }

        /// <summary>
        ///     Only applicable when two members are involved.
        ///     False by default.
        /// </summary>
        [DataMember(Name = "blockContact")]
        public bool BlockContact { get; set; }

        /// <summary>
        ///     Only applicable when two members are involved.
        ///     False by default.
        /// </summary>
        [DataMember(Name = "blockSearch")]
        public bool BlockSearch { get; set; }

        /// <summary>
        ///     Only applicable when two members are involved.
        ///     False by default.
        /// </summary>
        [DataMember(Name = "removeFromSearch")]
        public bool RemoveFromSearch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is viewer favorite.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is viewer favorite; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "isViewerFavorite")]
        public bool IsViewerFavorite { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the match rating.
        /// </summary>
        /// <value>
        /// The match rating.
        /// </value>
        [DataMember(Name = "matchRating")]
        public int MatchRating { get; set; }
    }
}