﻿using System.Runtime.Serialization;

namespace Spark.Rest.V2.Entities.Profile
{
    /// <summary>
    /// The response object for MemberExists request
    /// </summary>
    [DataContract(Name = "MemberExistsResponse", Namespace = "")]
    public class MemberExistsResponse
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="MemberExistsResponse"/> is exists.
        /// </summary>
        /// <value>
        ///   <c>true</c> if exists; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "exists")]
        public bool Exists { get; set; }
    }
}