using System;
using System.Runtime.Serialization;

namespace Spark.REST.Entities.Profile
{
	[DataContract(Name = "Kibitz", Namespace = "")]
	public class Kibitz : IEntity
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

		public string Question { get; set; }

		public string Answer { get; set; }

		public int DaysAgoAnswered { get; set; }

		public string MyAnswer { get; set; }
	}
}