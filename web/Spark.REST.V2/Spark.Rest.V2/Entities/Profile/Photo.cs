﻿#region

using System;
using System.Runtime.Serialization;

#endregion

namespace Spark.REST.Entities.Profile
{
    /// <summary>
    /// The DTO for Photo
    /// </summary>
    [Serializable]
    [DataContract(Name = "Photo", Namespace = "")]
    public class Photo
    {
        /// <summary>
        /// Gets or sets the member photo identifier.
        /// </summary>
        /// <value>
        /// The member photo identifier.
        /// </value>
        [DataMember(Name = "memberPhotoId")]
        public Int32 MemberPhotoId { get; set; }

        /// <summary>
        /// Gets or sets the full identifier.
        /// </summary>
        /// <value>
        /// The full identifier.
        /// </value>
        [DataMember(Name = "fullId")]
        public Int32 FullId { get; set; }

        /// <summary>
        /// Gets or sets the thumb identifier.
        /// </summary>
        /// <value>
        /// The thumb identifier.
        /// </value>
        [DataMember(Name = "thumbId")]
        public Int32 ThumbId { get; set; }

        /// <summary>
        /// Gets or sets the full path.
        /// </summary>
        /// <value>
        /// The full path.
        /// </value>
        [DataMember(Name = "fullPath")]
        public String FullPath { get; set; }

        /// <summary>
        /// Gets or sets the thumb path.
        /// </summary>
        /// <value>
        /// The thumb path.
        /// </value>
        [DataMember(Name = "thumbPath")]
        public String ThumbPath { get; set; }

        /// <summary>
        /// Gets or sets the caption.
        /// </summary>
        /// <value>
        /// The caption.
        /// </value>
        [DataMember(Name = "caption")]
        public String Caption { get; set; }

        /// <summary>
        /// Gets or sets the list order.
        /// </summary>
        /// <value>
        /// The list order.
        /// </value>
        [DataMember(Name = "listorder")]
        public byte ListOrder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is caption approved.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is caption approved; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "isCaptionApproved")]
        public Boolean IsCaptionApproved { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is photo approved.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is photo approved; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "isPhotoApproved")]
        public Boolean IsPhotoApproved { get; set; }

        /// <summary>
        /// Indicates if it's the main/default photo
        /// </summary>
        [DataMember(Name = "IsMain")]
        public bool IsMain { get; set; }

        /// <summary>
        /// If not approved, it cannot be selected as a main
        /// </summary>
        [DataMember(Name = "IsApprovedForMain")]
        public bool IsApprovedForMain { get; set; }
    }
}