﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Spark.SAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Entities.Premium
{
    [DataContract(Name = "SpotlightSettingsResponse", Namespace = "")]
    public class SpotlightSettingsResponse
    {
        [DataMember(Name = "isSpotlightEnabled")]
        public bool IsSpotlightEnabled { get; set; }

        [DataMember(Name = "gender")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Gender Gender { get; set; }

        [DataMember(Name = "seekingGender")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Gender SeekingGender { get; set; }

        [DataMember(Name = "minAge")]
        public int MinAge { get; set; }

        [DataMember(Name = "maxAge")]
        public int MaxAge { get; set; }

        [DataMember(Name = "distance")]
        public int Distance { get; set; }

        [DataMember(Name = "regionId")]
        public int RegionId { get; set; }

        public SpotlightSettingsResponse()
        {

        }

        public void LoadSettings(SpotlightSettings settings)
        {
            Gender = Spark.SAL.GenderUtils.GetSeekingGender(settings.GenderMask);
            SeekingGender = Spark.SAL.GenderUtils.GetGender(settings.GenderMask);
            IsSpotlightEnabled = settings.EnableFlag;
            MinAge = settings.AgeMin;
            MaxAge = settings.AgeMax;
            Distance = settings.Distance;
            RegionId = settings.RegionID;
        }

    }
}