using System.Collections.Generic;
using Spark.REST.Entities.Mail;
using Spark.Rest.Serialization;

namespace Spark.Rest.V2.Entities.Mail
{
    /// <summary>
    /// 
    /// </summary>
    public class GetMessageFolderResult
    {
        /// <summary>
        /// 
        /// </summary>
        public GetMessageFolderResult(int memberId)
        {
            MailFolder = new MailFolderV2
            {
                MailboxOwnerMemberId = memberId,
                MessageList = new List<MailMessageV2>(),
            };
            IsRetrieved = false;
            SparkHttpStatusCodeResult = null;
        }

        /// <summary>
        /// 
        /// </summary>
        public MailFolderV2 MailFolder { get; set; }

        /// <summary>
        ///     Indicates if the folder was successfully retrieved from the backend
        /// </summary>
        public bool IsRetrieved { get; set; }

        /// <summary>
        ///     Should be more generic
        /// </summary>
        public SparkHttpStatusCodeResult SparkHttpStatusCodeResult { get; set; }

    }
}