using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Spark.REST.Entities.Mail
{
	[DataContract(Name = "TeaseCategory", Namespace = "")]
	public class TeaseCategory : IEntity
	{
		[DataMember(Name = "Id")]
		public Int32 Id { get; set; }

		/// <summary>
		/// Represents the SiteID for this message.
		/// </summary>
        [DataMember(Name = "Description")]
        public String Description { get; set; }

        [DataMember(Name = "Teases")]
        public List<Tease> Teases { get; set; }
    
    }
}