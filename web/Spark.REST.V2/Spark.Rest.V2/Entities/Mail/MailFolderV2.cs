using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Spark.REST.Entities.Mail
{
	[DataContract(Name = "MailFolder", Namespace = "")]
	public class MailFolderV2 : IEntity
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

		/// <summary>
		/// Represents the CommunityID for this message.
		/// </summary>
		[DataMember(Name = "groupId")]
		public int GroupId { get; set; }

		[DataMember(Name = "mailboxOwnerMemberId")]
		public int MailboxOwnerMemberId { get; set; }

		[DataMember(Name = "description")]
		public string Description { get; set; }

        [DataMember(Name = "messageList")]
		public List<MailMessageV2> MessageList { get; set; }
	}
}