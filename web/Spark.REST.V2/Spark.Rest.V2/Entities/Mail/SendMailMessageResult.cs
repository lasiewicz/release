#region

using Spark.REST.Entities.Mail;
using Spark.Rest.Serialization;

#endregion

namespace Spark.Rest.V2.Entities.Mail
{
    /// <summary>
    /// </summary>
    public class SendMailMessageResult
    {
        /// <summary>
        /// 
        /// </summary>
        public SendMailMessageResult()
        {
            PostMessageResult = null;
            IsSent = false;
            SparkHttpStatusCodeResult = null;
        }
        /// <summary>
        /// </summary>
        public PostMessageResult PostMessageResult { get; set; }

        /// <summary>
        ///     Only set to true after all operation has completed
        /// </summary>
        public bool IsSent { get; set; }

        /// <summary>
        ///     Should be more generic
        /// </summary>
        public SparkHttpStatusCodeResult SparkHttpStatusCodeResult { get; set; }
    }
}