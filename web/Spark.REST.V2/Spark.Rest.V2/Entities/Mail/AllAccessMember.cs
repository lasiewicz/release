#region

using System;
using System.Runtime.Serialization;

#endregion

namespace Spark.Rest.V2.Entities.Mail
{
    /// <summary>
    ///     Ported from FWS with modifications, renaming to AllAccess to be consistent
    /// </summary>
    [DataContract(Name = "AllAccessMember", Namespace = "")]
    public class AllAccessMember
    {
        /// <summary>
        /// </summary>
        public DateTime AllAccessExpireDate { get; set; }

        /// <summary>
        /// </summary>
        public int EmailCount { get; set; }
    }
}