﻿#region

using Matchnet.Content.ValueObjects.BrandConfig;

#endregion

namespace Spark.Rest.V2.Entities.Mail
{
    /// <summary>
    ///     Brand brand, int memberId, int folderId, int pageSize, int pageNumber
    /// </summary>
    public class GetMessageFolderRequest
    {
        private readonly Brand _brand;
        private readonly int _folderId;
        private readonly int _memberId;
        private readonly int _pageNumber;
        private readonly int _pageSize;

        /// <summary>
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="memberId"></param>
        /// <param name="folderId"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        public GetMessageFolderRequest(Brand brand, int memberId, int folderId, int pageSize, int pageNumber)
        {
            _brand = brand;
            _memberId = memberId;
            _folderId = folderId;
            _pageSize = pageSize;
            _pageNumber = pageNumber;
        }

        public Brand Brand
        {
            get { return _brand; }
        }

        public int MemberId
        {
            get { return _memberId; }
        }

        public int FolderId
        {
            get { return _folderId; }
        }

        public int PageSize
        {
            get { return _pageSize; }
        }

        public int PageNumber
        {
            get { return _pageNumber; }
        }
    }
}