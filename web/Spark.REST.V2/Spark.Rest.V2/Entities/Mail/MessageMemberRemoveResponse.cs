﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Entities.Mail
{
    public class MessageMemberRemoveResponse
    {
        public bool Success { get; set; }
        public int MessageId { get; set; }
        public string FailureReason { get; set; }
    }
}