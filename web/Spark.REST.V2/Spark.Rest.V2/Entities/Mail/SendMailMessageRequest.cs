﻿#region

using Matchnet.Content.ValueObjects.BrandConfig;

#endregion

namespace Spark.Rest.V2.Entities.Mail
{
    /// <summary>
    /// </summary>
    public class SendMailMessageRequest
    {
        private readonly string _body;
        private readonly Brand _brand;
        private readonly int _fromMemberId;
        private readonly bool _isAllAccess;
        private readonly bool _isDraft;
        private readonly int _recipientMemberId;
        private readonly int _replyToMessageId;
        private readonly string _requestMailType;
        private readonly string _subject;
        private readonly int _draftMessageId;

        /// <summary>
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="fromMemberId"></param>
        /// <param name="recipientMemberId"></param>
        /// <param name="replyToMessageId"></param>
        /// <param name="isAllAccess"></param>
        /// <param name="isDraft"></param>
        /// <param name="requestMailType"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        public SendMailMessageRequest(Brand brand, int fromMemberId, int recipientMemberId, int replyToMessageId,
            bool isAllAccess, bool isDraft, string requestMailType, string subject, string body, int draftMessageId)
        {
            _brand = brand;
            _fromMemberId = fromMemberId;
            _recipientMemberId = recipientMemberId;
            _replyToMessageId = replyToMessageId;
            _isAllAccess = isAllAccess;
            _isDraft = isDraft;
            _requestMailType = requestMailType;
            _subject = subject;
            _body = body;
            _draftMessageId = draftMessageId;
        }

        public Brand Brand
        {
            get { return _brand; }
        }

        public int FromMemberId
        {
            get { return _fromMemberId; }
        }

        public int RecipientMemberId
        {
            get { return _recipientMemberId; }
        }

        public int ReplyToMessageId
        {
            get { return _replyToMessageId; }
        }

        public bool IsAllAccess
        {
            get { return _isAllAccess; }
        }

        public bool IsDraft
        {
            get { return _isDraft; }
        }

        public string RequestMailType
        {
            get { return _requestMailType; }
        }

        public string Subject
        {
            get { return _subject; }
        }

        public string Body
        {
            get { return _body; }
        }

        public int DraftMessageId
        {
            get { return _draftMessageId; }
        }
    }
}