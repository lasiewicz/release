﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Entities.Mail
{
    public class MessageAttachmentUploadRequest : FileRequest
    {
        [DataMember(Name = "toMemberId")]
        public int ToMemberId { get; set; }

        [DataMember(Name = "appCode")]
        public int AppCode { get; set; }

        [DataMember(Name = "attachmentType")]
        public int AttachmentType { get; set; }

        [DataMember(Name = "redirectUrl")]
        public string RedirectUrl { get; set; }
    }
}