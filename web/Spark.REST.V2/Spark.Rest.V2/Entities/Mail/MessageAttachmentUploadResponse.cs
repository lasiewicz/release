﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Entities.Mail
{
    public class MessageAttachmentUploadResponse
    {
        [DataMember(Name = "fileName")]
        public string FileName { get; set; }

        [DataMember(Name = "originalExtension")]
        public string OriginalExtension { get; set; }

        [DataMember(Name = "successCode")]
        public int SuccessCode { get; set; }

        [DataMember(Name = "httpSubStatusCode")]
        public int HttpSubStatusCode { get; set; }

        [DataMember(Name = "messageAttachmentId")]
        public int MessageAttachmentId { get; set; }

        [DataMember(Name = "failureReason")]
        public string FailureReason { get; set; }

        [DataMember(Name = "previewUrl")]
        public string PreviewUrl { get; set; }

        
    }
}