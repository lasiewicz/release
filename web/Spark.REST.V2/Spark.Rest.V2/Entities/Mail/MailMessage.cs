using System;
using System.Runtime.Serialization;
using Spark.REST.Entities.Profile;

namespace Spark.REST.Entities.Mail
{
	[DataContract(Name = "MailMessage", Namespace = "")]
	public class MailMessage : IEntity
	{
	    /// <summary>
	    ///     This ties into mnIMail..MessageList MessageListId column. The table stores metadata.
	    /// </summary>
	    [DataMember(Name = "id")]
	    public int Id { get; set; }

	    /// <summary>
	    ///     This ties into mnIMail..Message MessageId column. The table stores message content.
	    /// </summary>
	    [DataMember(Name = "messageId")]
	    public int MessageId { get; set; }

        [DataMember(Name = "sender")]
		public MiniProfile Sender { get; set; }

		[DataMember(Name = "recipient")]
		public MiniProfile Recipient { get; set; }

		[DataMember(Name = "subject")]
		public string Subject { get; set; }

        [DataMember(Name = "mailType")]
        public string MailType { get; set; }

		[DataMember(Name = "body")]
		public string Body { get; set; }

		[DataMember(Name = "insertDate")]
		public DateTime InsertDate { get; set; }

		[DataMember(Name = "openDate")]
		public DateTime? OpenDate { get; set; }

		[DataMember(Name = "messageStatus")]
		public int MessageStatus { get; set; }

		[DataMember(Name = "memberFolderId")]
		public int MemberFolderId { get; set; }

        [DataMember(Name = "isAllAccess")]
        public bool IsAllAccess { get; set; }
	}
}
