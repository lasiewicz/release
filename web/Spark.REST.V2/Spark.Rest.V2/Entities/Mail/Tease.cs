using System;
using System.Runtime.Serialization;

namespace Spark.REST.Entities.Mail
{
	[DataContract(Name = "Tease", Namespace = "")]
	public class Tease : IEntity
	{
		[DataMember(Name = "ID")]
		public Int32 Id { get; set; }

		[DataMember(Name = "Description")]
        public String Description { get; set; }

	}
}