﻿using Spark.REST.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Entities.Mail
{
    public class MailMember : IEntity
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int GroupId { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime InsertDatePst { get; set; }
        public int MessageStatus { get; set; }
        public string MailType { get; set; }
        public string Body { get; set; }
        public bool IsBodyAllAccess { get; set; }

        public bool AllAccess { get; set; }
        public int AllAccessCount { get; set; }
        public int UnreadAllAccessCount { get; set; }
        public int UnreadMessageCount { get; set; }        
        public int DraftCount { get; set; }

        //member profile attributes
        public Dictionary<string, object> Member { get; set; }
    }
}