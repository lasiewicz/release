using System;
using System.Runtime.Serialization;

namespace Spark.REST.Entities.Mail
{
	[DataContract(Name = "PostMessageResult", Namespace = "")]
	public class PostMessageResult
	{
		[DataMember(Name = "success")]
		public bool Success { get; set; }

		[DataMember(Name = "messageId")]
		public int MessageId { get; set; }

		[DataMember(Name = "failReason")]
		public string FailureReason { get; set; }

		[DataMember(Name = "teasesLeft")]
		public int TeasesLeft { get; set; }
	}
}