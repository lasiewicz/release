#region

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Spark.REST.Entities.Mail
{
    [DataContract(Name = "MailMessage", Namespace = "")]
    public class MailMessageV2 : IEntity
    {
        /// <summary>
        ///     This ties into mnIMail..Message MessageId column. The table stores message content.
        /// </summary>
        [DataMember(Name = "messageId")]
        public int MessageId { get; set; }

        [DataMember(Name = "sender")]
        public Dictionary<string, object> Sender { get; set; }

        [DataMember(Name = "recipient")]
        public Dictionary<string, object> Recipient { get; set; }

        [DataMember(Name = "subject")]
        public string Subject { get; set; }

        [DataMember(Name = "mailType")]
        public string MailType { get; set; }

        [DataMember(Name = "body")]
        public string Body { get; set; }

        [DataMember(Name = "insertDate")]
        public DateTime InsertDate { get; set; }

        [DataMember(Name = "insertDatePst")]
        public DateTime InsertDatePst { get; set; }

        /// <summary>
        ///     used for read receipt
        /// </summary>
        [DataMember(Name = "openDate")]
        public DateTime? OpenDate { get; set; }

        [DataMember(Name = "openDatePst")]
        public DateTime? OpenDatePst { get; set; }

        [DataMember(Name = "messageStatus")]
        public int MessageStatus { get; set; }

        [DataMember(Name = "memberFolderId")]
        public int MemberFolderId { get; set; }

        [DataMember(Name = "isAllAccess")]
        public bool IsAllAccess { get; set; }

        [DataMember(Name = "senderId")]
        public int SenderId { get; set; }

        [DataMember(Name="threadId")]
        public string ThreadId { get; set; }

        /// <summary>
        ///     This ties into mnIMail..MessageList MessageListId column. The table stores metadata.
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }
    }
}