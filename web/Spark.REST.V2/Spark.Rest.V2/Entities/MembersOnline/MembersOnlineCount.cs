﻿using System.Runtime.Serialization;

namespace Spark.Rest.Entities.MembersOnline
{
    [DataContract(Name = "MembersOnline", Namespace = "")]
    public class MembersOnlineCount
    {
        [DataMember(Name = "count")]
        public int Count { get; set; }
    }
}