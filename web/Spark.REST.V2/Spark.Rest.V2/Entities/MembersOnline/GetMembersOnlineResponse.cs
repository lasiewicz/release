﻿#region

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Spark.Rest.V2.Controllers
{
    /// <summary>
    /// </summary>
    [DataContract(Name = "GetMembersOnlineResponse")]
    public class GetMembersOnlineResponse
    {
        /// <summary>
        /// </summary>
        [DataMember(Name = "Members")]
        public List<Dictionary<string, object>> Members { get; set; }
    }
}