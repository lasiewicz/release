﻿using Spark.REST.Models;

namespace Spark.Rest.V2.Entities.InstantMessenger
{
    /// <summary>
    /// 
    /// </summary>
    public class InstantMessageHistoryMembersRequest : BrandRequest
    {
        /// <summary>
        /// Gets or sets a value indicating whether [ignore cache].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [ignore cache]; otherwise, <c>false</c>.
        /// </value>
        public bool IgnoreCache { get; set; }
    }
}