﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Models;

namespace Spark.Rest.V2.Entities.InstantMessenger
{
    public class InstantMessageSaveRequest : MemberRequest
    {
        [DataMember(Name = "toMemberId")]
        public int ToMemberId { get; set; }

        [DataMember(Name = "threadId")]
        public string ThreadId { get; set; }

        [DataMember(Name = "messageSubject")]
        public string MessageSubject { get; set; }

        [DataMember(Name = "messageBody")]
        public string MessageBody { get; set; }

        [DataMember(Name = "messageDateTime")]
        public DateTime MessageDateTime { get; set; }

        [DataMember(Name = "messageStyles")]
        public string MessageStyles { get; set; }

        [DataMember(Name = "roomId")]
        public int RoomId { get; set; }
    }
}