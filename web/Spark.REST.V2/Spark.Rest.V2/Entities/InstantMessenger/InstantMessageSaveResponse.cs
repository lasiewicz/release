﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Entities.InstantMessenger
{
    public class InstantMessageSaveResponse
    {
        [DataMember(Name = "resultCode")]
        public int ResultCode { get; set; }

    }
}