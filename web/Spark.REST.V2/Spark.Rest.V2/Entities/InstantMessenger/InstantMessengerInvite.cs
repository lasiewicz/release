﻿#region

using System;
using System.Runtime.Serialization;
using Matchnet;
using Spark.REST.Models;

#endregion

namespace Spark.Rest.V2.Entities.InstantMessenger
{
    /// <summary>
    /// </summary>
    [DataContract(Name = "InstantMessengerInvite", Namespace = "")]
    public class InstantMessengerInvite
    {
        /// <summary>
        /// 
        /// </summary>
        public InstantMessengerInvite()
        {
            SenderMemberId = Constants.NULL_INT;
            ConversationTokenId = Constants.NULL_STRING;
            CanMemberReply = false;
        }
        /// <summary>
        /// Member initiating conversation
        /// </summary>
        [DataMember(Name = "SenderMemberId")]
        public int SenderMemberId { get; set; }

        /// <summary>
        /// </summary>
        [DataMember(Name = "ConversationKey")]
        public string ConversationKey { get; set; }

        /// <summary>
        /// </summary>
        [DataMember(Name = "InviteDate")]
        public DateTime InviteDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "CanMemberReply")]
        public bool CanMemberReply { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "ConversationTokenId")]
        public string ConversationTokenId { get; set; }
    }
}