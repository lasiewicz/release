﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Entities.InstantMessenger
{
    public class InstantMessageCanIMResponse
    {
        public bool CanIM { get; set; }
        public string Reason { get; set; }
    }
}