﻿using Spark.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Entities.InstantMessenger
{
    public class InstantMessageCanIMRequest : BrandRequest
    {
        public int SenderMemberId { get; set; }
        public int TargetMemberId { get; set; }
        public bool CanInviteCheckOnly { get; set; }
    }
}