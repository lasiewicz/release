#region

using System;
using System.Runtime.Serialization;
using Spark.REST.Entities.Profile;

#endregion

namespace Spark.REST.Entities.HotList
{
    [DataContract(Name = "HotListEntry", Namespace = "")]
    public class HotListEntry
    {
        public HotListEntry()
        {
            Comment = string.Empty;
        }

        [DataMember(Name = "actionDate")]
        public DateTime ActionDate { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }

        [DataMember(Name = "comment")]
        public string Comment { get; set; }

        [DataMember(Name = "miniProfile")]
        public MiniProfile MiniProfile { get; set; }
    }
}