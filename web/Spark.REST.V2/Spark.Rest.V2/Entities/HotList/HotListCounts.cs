#region

using System.Runtime.Serialization;

#endregion

namespace Spark.REST.Entities.HotList
{
    [DataContract(Name = "HotListCounts", Namespace = "")]
    public class HotListCounts
    {
        [DataMember(Name = "hasNewMail")]
        public bool HasNewMail { get; set; }

        [DataMember(Name = "myFavoritesTotal")]
        public int MyFavoritesTotal { get; set; }

        [DataMember(Name = "viewedMyProfileTotal")]
        public int ViewedMyProfileTotal { get; set; }

        [DataMember(Name = "addedMeToTheirFavoritesTotal")]
        public int AddedMeToTheirFavoritesTotal { get; set; }

        [DataMember(Name = "viewedMyProfileNew")]
        public int ViewedMyProfileNew { get; set; }

        [DataMember(Name = "addedMeToTheirFavoritesNew")]
        public int AddedMeToTheirFavoritesNew { get; set; }

        [DataMember(Name = "MembersYouTeased")]
        public int MembersYouTeased { get; set; }

        [DataMember(Name = "MembersYouEmailed")]
        public int MembersYouEmailed { get; set; }

        [DataMember(Name = "MembersYouIMed")]
        public int MembersYouIMed { get; set; }

        [DataMember(Name = "MembersYouViewed")]
        public int MembersYouViewed { get; set; }

        [DataMember(Name = "MembersClickedYes")]
        public int MembersClickedYes { get; set; }

        [DataMember(Name = "MembersClickedNo")]
        public int MembersClickedNo { get; set; }

        [DataMember(Name = "MembersClickedMaybe")]
        public int MembersClickedMaybe { get; set; }

        [DataMember(Name = "MutualYes")]
        public int MutualYes { get; set; }

        [DataMember(Name = "MutualYesNew")]
        public int MutualYesNew { get; set; }

        /// <summary>
        ///     Sum of HotListCategory.MembersYouTeased, HotListCategory.WhoTeasedYou
        /// </summary>
        [DataMember(Name = "TeasedCombinedTotal")]
        public int TeasedCombinedTotal { get; set; }

        /// <summary>
        ///     Sum of HotListCategory.Default, HotListCategory.WhoAddedYouToTheirFavorites
        /// </summary>
        [DataMember(Name = "FavoritesCombinedTotal")]
        public int FavoritesCombinedTotal { get; set; }

        /// <summary>
        ///     Sum of HotListCategory.MembersYouViewed, HotListCategory.WhoViewedYourProfile
        /// </summary>
        [DataMember(Name = "ViewedCombinedTotal")]
        public int ViewedCombinedTotal { get; set; }

        [DataMember(Name = "WhoEmailedYou")]
        public int WhoEmailedYou { get; set; }

        [DataMember(Name = "WhoTeasedYou")]
        public int WhoTeasedYou { get; set; }

        [DataMember(Name = "WhoIMedYou")]
        public int WhoIMedYou { get; set; }

        [DataMember(Name = "IgnoreList")]
        public int IgnoreList { get; set; }
    }
}