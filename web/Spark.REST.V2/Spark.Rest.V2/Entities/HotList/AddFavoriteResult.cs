using System;
using System.Runtime.Serialization;

namespace Spark.REST.Entities.HotList
{
	[DataContract(Name = "AddFavoriteResult", Namespace = "")]
	public class AddFavoriteResult
	{
		[DataMember(Name = "success")]
		public bool Success { get; set; }

		[DataMember(Name = "failReason", EmitDefaultValue=true)]
		public string FailureReason { get; set; }
	}
}