﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Entities.HotList
{
    [DataContract(Name = "ContactHistoryItem", Namespace = "")]
    public class ContactHistoryItem
    {
        [DataMember(Name = "actionDate")]
        public DateTime ActionDate { get; set; }

        [DataMember(Name = "historyType")]
        public string HistoryType { get; set; }

        [DataMember(Name = "isMutual")]
        public bool IsMutual { get; set; }
    }
}