#region

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Spark.REST.Entities.HotList
{
    [DataContract(Name = "HotListEntry", Namespace = "")]
    public class HotListEntryV2
    {
        public HotListEntryV2()
        {
            Comment = string.Empty;
        }

        [DataMember(Name = "actionDate")]
        public DateTime ActionDate { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }

        [DataMember(Name = "comment")]
        public string Comment { get; set; }

        [DataMember(Name = "miniProfile")]
        public Dictionary<string, object> MiniProfile { get; set; }
    }
}