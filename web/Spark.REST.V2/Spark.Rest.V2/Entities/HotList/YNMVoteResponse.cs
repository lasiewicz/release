﻿#region

using System.Runtime.Serialization;
using Matchnet.List.ValueObjects;
using Spark.Rest.Entities;

#endregion

namespace Spark.Rest.V2.Controllers
{
    /// <summary>
    ///   Retrieve member's YNM vote status.
    /// </summary>
    [DataContract(Name = "YNMVoteResponse", Namespace = "")]
    public class YNMVoteResponse
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "Yes")]
        public bool Yes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "No")]
        public bool No { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "Maybe")]
        public bool Maybe { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "MutualYes")]
        public bool MutualYes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public YNMVoteResponse()
        {
            Yes = false;
            No = false;
            Maybe = false;
            MutualYes = false;
        }
    }
}