﻿using Spark.REST.Entities.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Entities.HotList
{
    public class ContactHistoryResponse
    {
        public int MemberID { get; set; }
        public int TargetMemberID { get; set; }
        public Dictionary<string, object> MiniProfile { get; set; }
        public Dictionary<string, object> TargetMiniProfile { get; set; }
        public List<ContactHistoryItem> ContactHistoryList { get; set; }
    }
}