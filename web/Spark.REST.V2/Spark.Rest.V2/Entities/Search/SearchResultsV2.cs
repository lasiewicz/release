#region

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Spark.REST.Entities.Search
{
    [DataContract(Name = "SearchResults", Namespace = "")]
    public class SearchResultsV2
    {
        /// <summary>
        ///     Member profile attribute sets
        /// </summary>
        [DataMember(Name = "Members")]
        public List<Dictionary<string, object>> Members { get; set; }

        [DataMember(Name="Spotlight")]
        public Dictionary<string, object> Spotlight { get; set; }

        /// <summary>
        ///     Total number of matches found
        /// </summary>
        [DataMember(Name = "MatchesFound")]
        public int MatchesFound { get; set; }

    }
}