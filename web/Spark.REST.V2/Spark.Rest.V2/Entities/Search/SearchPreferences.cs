#region

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Spark.REST.Entities.Search
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "SearchPreferences", Namespace = "")]
    public class SearchPreferences
    {
        /// <summary>
        /// Gets or sets the member identifier.
        /// </summary>
        /// <value>
        /// The member identifier.
        /// </value>
        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        [DataMember(Name = "gender")]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the seeking gender.
        /// </summary>
        /// <value>
        /// The seeking gender.
        /// </value>
        [DataMember(Name = "seekingGender")]
        public string SeekingGender { get; set; }

        /// <summary>
        /// Gets or sets the minimum age.
        /// </summary>
        /// <value>
        /// The minimum age.
        /// </value>
        [DataMember(Name = "minAge")]
        public int MinAge { get; set; }

        /// <summary>
        /// Gets or sets the maximum age.
        /// </summary>
        /// <value>
        /// The maximum age.
        /// </value>
        [DataMember(Name = "maxAge")]
        public int MaxAge { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        [DataMember(Name = "location")]
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the region identifier.
        /// </summary>
        /// <value>
        /// The region identifier.
        /// </value>
        [DataMember(Name = "regionId")]
        public string RegionId { get; set; }

        /// <summary>
        /// Gets or sets the maximum distance.
        /// </summary>
        /// <value>
        /// The maximum distance.
        /// </value>
        [DataMember(Name = "maxDistance")]
        public int MaxDistance { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [show only members with photos].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show only members with photos]; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "showOnlyMembersWithPhotos")]
        public bool ShowOnlyMembersWithPhotos { get; set; }

        /// <summary>
        /// Gets or sets the minimum height.
        /// </summary>
        /// <value>
        /// The minimum height.
        /// </value>
        [DataMember(Name = "MinHeight")]
        public int MinHeight { get; set; }

        /// <summary>
        /// Gets or sets the maximum height.
        /// </summary>
        /// <value>
        /// The maximum height.
        /// </value>
        [DataMember(Name = "MaxHeight")]
        public int MaxHeight { get; set; }

        /// <summary>
        ///     dictionary of multi-value search preference attributes
        ///     this follows what getattributeset does
        /// </summary>
        [DataMember(Name = "AdvancedSearchPreferences")]
        public Dictionary<string, object> AdvancedSearchPreferences { get; set; }
    }
}
