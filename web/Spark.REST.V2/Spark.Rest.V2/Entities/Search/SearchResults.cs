#region

using System.Collections.Generic;
using System.Runtime.Serialization;
using Spark.REST.Entities.Profile;

#endregion

namespace Spark.REST.Entities.Search
{
    [DataContract(Name = "SearchResults", Namespace = "")]
    public class SearchResults
    {
        [DataMember(Name = "Members")]
        public List<MiniProfile> Members { get; set; }

        [DataMember(Name = "Spotlight")]
        public Dictionary<string, object> Spotlight { get; set; }

        /// <summary>
        /// Total number of matches found
        /// </summary>
        [DataMember(Name = "MatchesFound")]
        public int MatchesFound { get; set; }

    }
}