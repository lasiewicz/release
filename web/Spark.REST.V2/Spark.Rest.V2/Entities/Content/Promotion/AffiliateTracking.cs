﻿namespace Spark.Rest.Entities.Content.Promotion
{
    /// <summary>
    /// Contains affiliate tracking data for a given url.
    /// </summary>
    public class AffiliateTracking
    {
        /// <summary>
        /// PromotionId and PRM are the same. PromotionId is the correct member attribute name.
        /// </summary>
        public int PromotionId { get; set; }
        /// <summary>
        /// Luggage and LGID are the same. Luggage is the correct member attribute name.
        /// </summary>
        public string Luggage { get; set; }
    }
}