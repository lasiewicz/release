﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Entities;

namespace Spark.REST.Entities.Content.Registration
{
    [DataContract(Name = "Step", Namespace = "")]
    public class Step
    {
        [DataMember(Name = "CSSClass")]
        public string CSSClass { get; set; }

        [DataMember(Name = "TipText")]
        public string TipText { get; set; }

        [DataMember(Name = "Order")]
        public int Order { get; set; }

        [DataMember(Name = "HeaderText")]
        public string HeaderText { get; set; }

        [DataMember(Name = "TitleText")]
        public string TitleText { get; set; }

        [DataMember(Name = "Controls")]
        public List<RegControl> Controls { get; set; }
    }
}