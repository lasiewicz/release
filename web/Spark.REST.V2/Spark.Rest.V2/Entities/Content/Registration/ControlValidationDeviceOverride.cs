﻿using System.Runtime.Serialization;

using Matchnet.Content.ValueObjects.Registration;

namespace Spark.REST.Entities.Content.Registration
{
    [DataContract(Name = "ControlValidationDeviceOverride", Namespace = "")]
    public class ControlValidationDeviceOverride
    {
        [DataMember(Name = "DeviceType")]
        public DeviceType DeviceType { get; set; }

        [DataMember(Name = "ErrorMessage")]
        public string ErrorMessage { get; set; }
    }
}