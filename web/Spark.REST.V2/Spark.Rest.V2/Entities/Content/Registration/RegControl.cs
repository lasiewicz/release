﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Entities;
using Spark.REST.Entities.Content.AttributeData;
using Spark.REST.Entities.Content.Registration;


namespace Spark.REST.Entities.Content.Registration
{
    [DataContract(Name = "RegControl", Namespace = "")]
    public class RegControl
    {
        [DataMember(Name = "Name")]
        public string Name { get; set; }

        [DataMember(Name = "DataType")]
        public string DataType { get; set; }

        [DataMember(Name = "IsAttribute")]
        public bool IsAttribute { get; set; }

        [DataMember(Name = "IsMultiValue")]
        public bool IsMultiValue { get; set; }

        [DataMember(Name = "ControlDisplayType")]
        public int ControlDisplayType { get; set; }

        [DataMember(Name = "Required")]
        public bool Required { get; set; }

        [DataMember(Name = "RequiredErrorMessage")]
        public string RequiredErrorMessage { get; set; }

        [DataMember(Name = "Label")]
        public string Label { get; set; }

        [DataMember(Name = "AdditionalText")]
        public string AdditionalText { get; set; }

        [DataMember(Name = "EnableAutoAdvance")]
        public bool EnableAutoAdvance { get; set; }

        [DataMember(Name = "AllowZeroValue")]
        public bool AllowZeroValue { get; set; }

        [DataMember(Name = "DefaultValue")]
        public string DefaultValue { get; set; }

        [DataMember(Name = "Validations")]
        public List<ControlValidation> Validations { get; set; }

        [DataMember(Name = "Options")]
        public List<AttributeOption> Options { get; set; }

        [DataMember(Name = "DeviceOverrides")]
        public List<RegControlDeviceOverride> DeviceOverrides { get; set; }

        [DataMember(Name = "ScenarioOverrides")]
        public List<RegControlScenarioOverride> ScenarioOverrides { get; set; }
        
    }


}