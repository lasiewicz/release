﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.REST.Entities;

namespace Spark.REST.Entities.Content.Registration
{
    [DataContract(Name = "Scenario", Namespace = "")]
    public class Scenario
    {
        [DataMember(Name = "ID")]
        public int ID { get; set; }
        
        [DataMember(Name = "Name")]
        public string Name { get; set; }

        [DataMember(Name = "IsOnePageReg")]
        public bool IsOnePageReg { get; set; }

        [DataMember(Name = "IsControl")]
        public bool IsControl { get; set; }

        [DataMember(Name = "SplashTemplate")]
        public string SplashTemplate { get; set; }

        [DataMember(Name = "RegistrationTemplate")]
        public string RegistrationTemplate { get; set; }

        [DataMember(Name = "ConfirmationTemplate")]
        public string ConfirmationTemplate { get; set; }

        [DataMember(Name = "DeviceType")]
        public int DeviceType { get; set; }

        [DataMember(Name = "ExternalSessionType")]
        public int ExternalSessionType { get; set; }

        [DataMember(Name = "Steps")]
        public List<Step> Steps { get; set; }
    }
}