﻿using System.Runtime.Serialization;
using Matchnet.Content.ValueObjects.Registration;

namespace Spark.REST.Entities.Content.Registration
{
    [DataContract(Name = "RegControlDeviceOverride", Namespace = "")]
    public class RegControlDeviceOverride
    {
        [DataMember(Name = "DeviceType")]
        public DeviceType DeviceType { get; set; }

        [DataMember(Name = "AdditionalText")]
        public string AdditionalText { get; set; }

        [DataMember(Name = "Label")]
        public string Label { get; set; }

        [DataMember(Name = "RequiredErrorMessage")]
        public string RequiredErrorMessage { get; set; }
    }
}