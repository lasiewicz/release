using System.Runtime.Serialization;

namespace Spark.REST.Entities.Content
{
	[DataContract(Name = "setting", Namespace = "")]
	public class Setting
	{
		[DataMember(Name = "name")]
		public string Name { get; set; }

		[DataMember(Name = "isEnabled")]
		public bool IsEnabled { get; set; }
	}
}