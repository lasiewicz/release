﻿using Spark.REST.Entities.Content.RegionData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Entities.Content.RegionData
{
    [DataContract(Name = "RegionHierarchy", Namespace = "")]
    public class RegionHierarchy
    {
        [DataMember(Name = "PostalCode")]
        public Region PostalCode { get; set; }

        [DataMember(Name = "City")]
        public City City { get; set; }

        [DataMember(Name = "State")]
        public State State { get; set; }

        [DataMember(Name = "Country")]
        public Country Country { get; set; }
    }
}