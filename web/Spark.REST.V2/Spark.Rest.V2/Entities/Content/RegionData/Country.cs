﻿using System.Runtime.Serialization;

namespace Spark.REST.Entities.Content.RegionData
{
    [DataContract(Name = "Country", Namespace = "")]
    public class Country : Region
    {
        [DataMember(Name = "Abbreviation")]
        public string Abbreviation { get; set; }
    }
}