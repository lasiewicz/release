﻿using System.Runtime.Serialization;

namespace Spark.REST.Entities.Content.RegionData
{
    [DataContract(Name = "State", Namespace = "")]
    public class State : Region
    {
        [DataMember(Name = "Abbreviation")]
        public string Abbreviation { get; set; }
    }
}