﻿using System.Runtime.Serialization;

namespace Spark.REST.Entities.Content.RegionData
{
    [DataContract(Name = "ChildRegion", Namespace = "")]
    public class ChildRegion : Region
    {
    }
}