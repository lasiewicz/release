using System.Runtime.Serialization;

namespace Spark.REST.Entities.Content
{
	[DataContract(Name = "setting", Namespace = "")]
	public class SettingV2
	{
		[DataMember(Name = "name")]
		public string Name { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }
    }
}