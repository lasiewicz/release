﻿using System.Runtime.Serialization;

namespace Spark.REST.Entities.Content.AttributeData
{
    [DataContract(Name = "AttributeMetadata", Namespace = "")]
    public class AttributeMetadata
    {
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        [DataMember(Name = "DataType")]
        public string DataType { get; set; }
    }
}
