﻿#region

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Spark.REST.Entities.Content.AttributeData
{
    [DataContract(Name = "AttributeOptions", Namespace = "")]
    public class AttributeOptions
    {
        [DataMember(Name = "AttributeName")]
        public string AttributeName { get; set; }

        [DataMember(Name = "AttributeOptionList")]
        public List<AttributeOption> AttributeOptionList { get; set; }
    }
}