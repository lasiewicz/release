﻿#region

using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

#endregion

namespace Spark.Rest.V2.Entities.Content.AttributeData
{
    /// <summary>
    ///     List of attribute names used in requests.
    ///     This class is created to implement a custom ToString() override for uniqueness in MVC OutputCache VaryByParam. When
    ///     an input parameter is an object, VaryByParam uses .ToString() on the object which would always result in "List
    ///     <String>"
    /// </summary>
    [DataContract(Name = "AttributeNames", Namespace = "")]
    public class AttributeNames : List<string>
    {
        /// <summary>
        ///     This should gurantee cache uniqueness with a list of attributenames.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var attributeNameString = new StringBuilder();
            foreach (var attributeName in this)
            {
                attributeNameString.Append(attributeName);
            }
            return attributeNameString.ToString();
        }
    }
}