﻿using System.Runtime.Serialization;

namespace Spark.REST.Entities.Content.BrandConfig
{
	[DataContract(Name = "Community", Namespace = "")]
    public class Community : IEntity
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name{ get; set; }
	}
}