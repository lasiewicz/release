﻿using System;
using System.Runtime.Serialization;

namespace Spark.REST.Entities.Content.BrandConfig
{
	[DataContract(Name = "Brand", Namespace = "")]
	public class Brand : IEntity
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

        [DataMember(Name = "site")]
        public Site Site { get; set; }

        [DataMember(Name = "uri")]
        public string Uri{ get; set; }
	}
}