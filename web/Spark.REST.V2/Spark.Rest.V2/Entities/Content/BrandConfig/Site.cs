﻿using System.Runtime.Serialization;

namespace Spark.REST.Entities.Content.BrandConfig
{
	[DataContract(Name = "Site", Namespace = "")]
	public class Site : IEntity
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name{ get; set; }

        [DataMember(Name = "community")]
        public Community Community { get; set; }
	}
}