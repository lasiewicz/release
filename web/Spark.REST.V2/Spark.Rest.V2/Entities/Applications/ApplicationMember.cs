﻿using System;
using System.Runtime.Serialization;

namespace Spark.Rest.Entities.Applications
{
	[DataContract(Name = "ApplicationMember", Namespace = "")]
	public class ApplicationMember
	{
		[DataMember(Name = "applicationId")]
		public int ApplicationId { get; set; }
		[DataMember(Name = "memberId")]
		public int MemberId { get; set; }
		[DataMember(Name = "accessToken")]
		public string AccessToken { get; set; }
		[DataMember(Name = "accessTokenExpires")]
		public DateTime AccessTokenExpires { get; set; }
		[DataMember(Name = "refreshToken")]
		public string RefreshToken { get; set; }
		[DataMember(Name = "refreshTokenExpires")]
		public DateTime RefreshTokenExpires { get; set; }
	}
}