﻿namespace Spark.Rest.V2.Entities.Applications
{
    public class PasswordChangeResponse
    {
        public bool PasswordChanged { get; set; }
    }
}