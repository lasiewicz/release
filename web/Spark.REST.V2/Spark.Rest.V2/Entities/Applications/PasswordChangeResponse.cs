﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Entities.Applications
{
    public class PasswordResetResponse
    {
        public bool EmailSent { get; set; }
    }
}