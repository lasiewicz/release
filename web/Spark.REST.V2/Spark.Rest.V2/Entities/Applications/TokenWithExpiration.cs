﻿using System;

namespace Spark.Rest.Entities.Applications
{
	public class TokenWithExpiration
	{
		public string Token { get; set; }
		public DateTime ExpiresDateTime { get; set; }
		public int ExpiresIn
		{
			get
			{
				var diff = ExpiresDateTime - DateTime.Now;
				return (int)diff.TotalSeconds;
			}
		}

	}
}