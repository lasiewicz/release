﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Entities.Applications
{
    public enum AuthenticatedPasswordChangeStatus
    {
        Success=1,
        OriginalPasswordNotValid=2,
        InvalidAccessToken=3,
        GeneralFailure=4,
        NoChange = 5
    }

    [DataContract(Name = "AuthenticatedPasswordChangeResponse")]
    public class AuthenticatedPasswordChangeResponse
    {
        [DataMember(Name = "EmailSent")]
        public bool EmailSent { get; set; }

        [DataMember(Name = "Status")]
        public AuthenticatedPasswordChangeStatus Status { get; set; }
    }
}