﻿using System;
using System.Runtime.Serialization;

namespace Spark.Rest.Entities.Applications
{
	public class TokenPair
	{
        [IgnoreDataMember]
		public TokenWithExpiration AccessToken { get; set; }
        [IgnoreDataMember]
        public TokenWithExpiration RefreshToken { get; set; }
		//add token_type "bearer"?
	}
}