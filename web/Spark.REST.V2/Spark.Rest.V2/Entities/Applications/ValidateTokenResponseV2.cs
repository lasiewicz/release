﻿#region

using System;
using System.Runtime.Serialization;

#endregion

namespace Spark.Rest.V2.Entities.Applications
{
    [DataContract(Name = "ValidateAccessTokenResponse")]
    public class ValidateTokenResponse
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
    }
    
    /// <summary>
    ///     A response class to contain many properties associated with validating an access token
    /// </summary>
    [DataContract(Name = "ValidateAccessTokenResponse")]
    public class ValidateTokenResponseV2
    {
        /// <summary>
        ///     Some default values to avoid null errors
        /// </summary>
        public ValidateTokenResponseV2()
        {
            IsValid = false;
            MemberId = 0;
            FailReason = String.Empty;
            FailCode = 0;
            TokenAccessExpiration = DateTime.MinValue;
            TokenAppId = 0;
        }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }

        [DataMember(Name = "failReason")]
        public string FailReason { get; set; }

        [DataMember(Name = "failCode")]
        public int FailCode { get; set; }

        [DataMember(Name = "tokenAccessExpiration")]
        public DateTime TokenAccessExpiration { get; set; }

        [DataMember(Name = "tokenAppId")]
        public int TokenAppId { get; set; }

        [DataMember(Name = "isSelfSuspended")]
        public bool IsSelfSuspended { get; set; }

        [DataMember(Name = "REDRedesignBetaParticipatingFlag")]
        public bool REDRedesignBetaParticipatingFlag { get; set; }

        [DataMember(Name = "REDRedesignBetaOfferedFlag")]
        public bool REDRedesignBetaOfferedFlag { get; set; }            
    }
}