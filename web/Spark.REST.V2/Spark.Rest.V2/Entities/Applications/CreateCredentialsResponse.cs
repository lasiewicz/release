﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Rest.Entities.Applications;

namespace Spark.Rest.V2.Entities.Applications
{
    public class CreateCredentialsResponse : TokenPairResponseV2
    {
        public string UserName { get; set; }

        public int MemberId { get; set; }

        public static CreateCredentialsResponse Parse(TokenPairResponseV2 tpr)
        {
            return new CreateCredentialsResponse
            {
                AccessExpiresTime = tpr.AccessExpiresTime,
                AccessToken = tpr.AccessToken,
                Error = tpr.Error,
                ExpiresIn = tpr.ExpiresIn,
                IsPayingMember = tpr.IsPayingMember,
                MemberId = tpr.MemberId,
                RefreshToken = tpr.RefreshToken,
                RefreshTokenExpiresTime = tpr.RefreshTokenExpiresTime
            };
        }

    }
}