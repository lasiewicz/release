﻿#region

using System.Runtime.Serialization;

#endregion

namespace Spark.Rest.V2.Entities.Applications
{
    [DataContract(Name = "AuthenticateAccessTokenResponse", Namespace = "")]
    public class AuthenticateAccessTokenResponse
    {
        [DataMember(Name = "Authenticated")]
        public bool Authenticated { get; set; }

        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }

        [DataMember(Name = "FailCode")]
        public int FailCode { get; set; }

        [DataMember(Name = "FailReason")]
        public string FailReason { get; set; }

        [DataMember(Name = "REDRedesignBetaParticipatingFlag")]
        public bool REDRedesignBetaParticipatingFlag { get; set; }

        [DataMember(Name = "REDRedesignBetaOfferedFlag")]
        public bool REDRedesignBetaOfferedFlag { get; set; }            

    }
}