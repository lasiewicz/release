﻿#region

using System;
using System.Net;
using System.Runtime.Serialization;

#endregion

namespace Spark.Rest.Entities.Applications
{
    public class TokenPairResponse
    {
        public TokenPairResponse()
        {
        }

        public TokenPairResponse(TokenPair tokenPairData)
        {
            AccessToken = tokenPairData.AccessToken.Token;
            ExpiresIn = tokenPairData.AccessToken.ExpiresIn;
            AccessExpiresTime = tokenPairData.AccessToken.ExpiresDateTime.ToUniversalTime();
            RefreshToken = tokenPairData.RefreshToken.Token;
            RefreshTokenExpiresTime = tokenPairData.RefreshToken.ExpiresDateTime.ToUniversalTime();
        }

        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }

        [DataMember(Name = "accessToken")]
        public string AccessToken { get; set; }

        [DataMember(Name = "expiresIn")]
        public int ExpiresIn { get; set; }

        [DataMember(Name = "expiresTime")]
        public DateTime AccessExpiresTime { get; set; }

        [DataMember(Name = "refreshToken")]
        public string RefreshToken { get; set; }

        /// <summary>
        ///     Expiration time of the refresh token
        /// </summary>
        [DataMember(Name = "refreshTokenExpiresTime")]
        public DateTime RefreshTokenExpiresTime { get; set; }

        [DataMember(Name = "error")]
        public string Error { get; set; }

        /// <summary>
        ///     to indicate if the member's a subscriber
        /// </summary>
        [DataMember(Name = "isPayingMember")]
        public bool IsPayingMember { get; set; }

        /// <summary>
        ///     boolean default is false
        /// </summary>
        [DataMember(Name = "isSelfSuspended")]
        public bool IsSelfSuspended { get; set; }

        /// <summary>
        ///     to indicate if the member's a iap subscriber
        /// </summary>
        [DataMember(Name = "isIAPPayingMember")]
        public bool IsIAPPayingMember { get; set; }

        /// <summary>
        ///     to indicate if the member's a google play subscriber
        /// </summary>
        [DataMember(Name = "isIABPayingMember")]
        public bool IsIABPayingMember { get; set; }

        [DataMember(Name = "REDRedesignBetaParticipatingFlag")]
        public bool REDRedesignBetaParticipatingFlag { get; set; }

        [DataMember(Name = "REDRedesignBetaOfferedFlag")]
        public bool REDRedesignBetaOfferedFlag { get; set; }  
          
        /// <summary>
        ///  admin member id who requested token
        /// </summary>
        [DataMember(Name = "adminMemberID")]
        public int AdminMemberID { get; set; }
    }

    public class TokenPairResponseV2 : TokenPairResponse
    {
        private int responseCode = (int) HttpStatusCode.OK;
        private int responseSubCode = 0;

        public TokenPairResponseV2() : base()
        {
        }

        public TokenPairResponseV2(TokenPair tokenPair) : base(tokenPair)
        {
        }

        [IgnoreDataMember]
        public int ResponseCode
        {
            get { return responseCode; }
            set { responseCode = value; }
        }

        [IgnoreDataMember]
        public int ResponseSubCode
        {
            get { return responseSubCode; }
            set { responseSubCode = value; }
        }

        public static TokenPairResponseV2 Parse(TokenPairResponse tpr)
        {
            var tokenPairResponseV2 = new TokenPairResponseV2
            {
                AccessExpiresTime = tpr.AccessExpiresTime,
                AccessToken = tpr.AccessToken,
                Error = tpr.Error,
                ExpiresIn = tpr.ExpiresIn,
                IsPayingMember = tpr.IsPayingMember,
                IsIAPPayingMember = tpr.IsIAPPayingMember,
                MemberId = tpr.MemberId,
                RefreshToken = tpr.RefreshToken,
                RefreshTokenExpiresTime = tpr.RefreshTokenExpiresTime,
                IsSelfSuspended = tpr.IsSelfSuspended,
                REDRedesignBetaParticipatingFlag = tpr.REDRedesignBetaParticipatingFlag,
                REDRedesignBetaOfferedFlag = tpr.REDRedesignBetaOfferedFlag,
                AdminMemberID = tpr.AdminMemberID
            };
            return tokenPairResponseV2;
        }
    }
}