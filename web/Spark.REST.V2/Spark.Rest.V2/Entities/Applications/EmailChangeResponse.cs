﻿using System.Runtime.Serialization;

namespace Spark.Rest.V2.Entities.Applications
{
    public enum EmailChangeStatus
    {
        NoChange = 0,
        Success = 1,
        NewEmailNotValid = 2,
        OriginalEmailNotValid = 3,
        InvalidAccessToken = 4,
        GeneralFailure = 5,
        EmailAddressAlreadyExists = 6,
        CurrentEmailIncorrect = 7
    }


    [DataContract(Name = "EmailChangeResponse")]
    public class EmailChangeResponse
    {
        [DataMember(Name = "EmailSent")]
        public bool EmailSent { get; set; }

        [DataMember(Name = "EnableEmailVerification")]
        public bool EnableEmailVerification { get; set; }

        [DataMember(Name = "Status")]
        public EmailChangeStatus Status { get; set; }
    }
}