﻿using System.Runtime.Serialization;

namespace Spark.REST.Entities
{
    /// <summary>
    /// The LoginCredentials DTO 
    /// </summary>
	[DataContract(Name = "loginCredentials", Namespace = "")]
	public class LoginCredentials
	{
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
		[DataMember(Name = "email")]
		public string Email { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
		[DataMember(Name = "password")]
		public string Password { get; set; }
	}
}
