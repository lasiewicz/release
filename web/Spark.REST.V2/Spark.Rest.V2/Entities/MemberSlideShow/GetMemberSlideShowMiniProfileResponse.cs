﻿#region

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Spark.Rest.V2.Entities.MemberSlideShow
{
    ///<summary>
    ///</summary>
    [DataContract(Name = "GetMemberSlideShowMiniProfileResponse")]
    public class GetMemberSlideShowMiniProfileResponse : GetMemberSlideShowResponse
    {
        /// <summary>
        /// </summary>
        [DataMember(Name = "MiniProfile")]
        public Dictionary<string, object> MiniProfile { get; set; }
    }
}