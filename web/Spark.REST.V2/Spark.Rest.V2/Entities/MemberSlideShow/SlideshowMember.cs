﻿#region

using System.Collections.Generic;
using System.Runtime.Serialization;
using Spark.REST.Entities.Profile;

#endregion

namespace Spark.Rest.V2.Entities.MemberSlideShow
{
    [DataContract(Name = "SlideshowMember")]
    public class SlideshowMember
    {
        [DataMember(Name = "MiniProfile")]
        public Dictionary<string, object> MiniProfile { get; set; }

        [DataMember(Name = "Photos")]
        public List<Photo> Photos { get; set; }
    }
}