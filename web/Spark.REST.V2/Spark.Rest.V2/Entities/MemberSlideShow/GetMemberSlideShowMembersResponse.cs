﻿#region

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Spark.Rest.V2.Entities.MemberSlideShow
{
    [DataContract(Name = "GetMemberSlideShowMiniProfileResponse")]
    public class GetMemberSlideShowMembersResponse
    {
        [DataMember(Name = "SlideshowMembers")]
        public List<SlideshowMember> SlideshowMembers { get; set; }
    }
}