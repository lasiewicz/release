﻿#region

using System.Runtime.Serialization;

#endregion

namespace Spark.Rest.V2.Entities.MemberSlideShow
{
    /// <summary>
    /// </summary>
    [DataContract(Name = "GetMemberSlideShowResponse")]
    public class GetMemberSlideShowResponse
    {
        public GetMemberSlideShowResponse()
        {
            MemberFound = true;
        }
        /// <summary>
        /// </summary>
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }

        /// <summary>
        /// </summary>
        [DataMember(Name = "MemberFound")]
        public bool MemberFound { get; set; }
    }
}