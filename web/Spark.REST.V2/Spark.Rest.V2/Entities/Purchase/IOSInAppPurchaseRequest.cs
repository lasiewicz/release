#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using Matchnet.Content.ValueObjects.Admin;
using Spark.Common.Adapter;
using Spark.REST.Models;

#endregion

namespace Spark.REST.Entities.Purchase
{
    [DataContract(Name = "IOSInAppRenewRequest", Namespace = "")]
    public class IOSInAppRenewRequest : IOSInAppPurchaseRequest
    {
        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }

        [DataMember(Name = "renewalSku")]
        public string RenewalSku { get; set; }

    }

    [DataContract(Name = "IOSInAppPurchaseRequest", Namespace = "")]
    public class IOSInAppPurchaseRequest : BrandRequest
    {
        [DataMember(Name = "encodedReceiptJSON")]
        public string EncodedReceiptJSON { get; set; }

        [DataMember(Name = "orderAttributeXML")]
        public string OrderAttributeXML { get; set; }

        //TODO: Flag added for UPS Testing.  Remove before launch to prod. -arod
        private int _iapStatus = -1;
        [DataMember(Name = "iapStatus")]
        public int IapStatus
        {
            get { return _iapStatus; }
            set { _iapStatus = value; }
        }

        /// <summary>
        ///     For CM to pass in their region Id
        /// </summary>
        [DataMember(Name = "regionId")]
        public int RegionId { get; set; }
    }

    [DataContract(Name = "IOSInAppValidateReceiptRequest", Namespace = "")]
    public class IOSInAppValidateReceiptRequest : BrandRequest
    {
        [DataMember(Name = "encodedReceiptJSON")]
        public string EncodedReceiptJSON { get; set; }
    }

    [DataContract(Name = "IOSPurchaseProductsRequest", Namespace = "")]
    public class IOSPurchaseProductsRequest : BrandRequest
    {
        [DataMember(Name = "requestData")]
        public string requestData { get; set; }
    }

    [DataContract(Name = "IOSPurchaseProduct", Namespace = "")]
    public class IOSPurchaseProduct
    {
        public IOSPurchaseProduct(string productId, string price, string duration, string description)
        {
            Description = description;
            Duration = duration;
            Price = price;
            ProductId = productId;
        }

        [DataMember(Name = "ProductId")]
        public string ProductId { get; set; }

        [DataMember(Name = "Duration")]
        public string Duration { get; set; }

        [DataMember(Name = "Price")]
        public string Price { get; set; }

        [DataMember(Name = "Description")]
        public string Description { get; set; }
    }

    public class IOSValidateResponse
    {
        public IOSValidateResponse(string originalTransactionId, IAPControllerStatus status, IOSInAppInfo latestReceiptInfo)
        {
            OriginalTransactionId = originalTransactionId;
            Status = status;
            LastInAppReceipt = latestReceiptInfo;
        }

        public IAPControllerStatus Status { get; set; }
        public string OriginalTransactionId { get; set; }
        public IOSInAppInfo LastInAppReceipt { get; set; }
    }

    public interface IOSInAppInfo
    {
        DateTime ExpiresDateUTC { get; set; }
        DateTime PurchaseDateUTC { get; }
        DateTime CancellationDateUTC { get; }
        string product_id { get; set; }
        string quantity { get; set; }
        string transaction_id { get; set; }
        string original_transaction_id { get; set; }
        string purchase_date { get; set; }
        string purchase_date_pst { get; set; }
        string original_purchase_date { get; set; }
        string original_purchase_date_pst { get; set; }
        string expires_date { get; set; }
        string expires_date_pst { get; set; }
        string cancellation_date { get; set; }
        string cancellation_date_pst { get; set; }
        string web_order_line_item_id { get; set; }
        string is_trial_period { get; set; }
    }

    public class InApp : IOSInAppInfo, IComparable<IOSInAppInfo>
    {
        private DateTime _expiresDateUTC = DateTime.MinValue;
        private DateTime _purchaseDateUTC = DateTime.MinValue;
        private DateTime _cancellationDateUTC = DateTime.MinValue;

        public DateTime ExpiresDateUTC
        {
            get
            {
                if (!string.IsNullOrEmpty(this.expires_date) && DateTime.MinValue == _expiresDateUTC)
                {
                    _expiresDateUTC = DateTime.ParseExact(this.expires_date,
                                                        "yyyy-MM-dd HH:mm:ss 'Etc/GMT'",
                                                        CultureInfo.InvariantCulture,
                                                        DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);
                }
                return _expiresDateUTC;
            }
            set { _expiresDateUTC = value; }
        }

        public DateTime PurchaseDateUTC
        {
            get
            {
                if (!string.IsNullOrEmpty(this.purchase_date) && DateTime.MinValue == _purchaseDateUTC)
                {
                    _purchaseDateUTC = DateTime.ParseExact(this.purchase_date,
                                        "yyyy-MM-dd HH:mm:ss 'Etc/GMT'",
                                        CultureInfo.InvariantCulture,
                                        DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);

                }
                return _purchaseDateUTC;
            }
        }

        public DateTime CancellationDateUTC
        {
            get
            {
                if (!string.IsNullOrEmpty(this.cancellation_date) && DateTime.MinValue == _cancellationDateUTC)
                {
                    _cancellationDateUTC = DateTime.ParseExact(this.cancellation_date,
                                                            "yyyy-MM-dd HH:mm:ss 'Etc/GMT'",
                                                            CultureInfo.InvariantCulture,
                                                            DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);
                }
                return _cancellationDateUTC;
            }
        }

        
        public string quantity { get; set; }
        public string product_id { get; set; }
        public string transaction_id { get; set; }
        public string original_transaction_id { get; set; }
        public string purchase_date { get; set; }
        public string purchase_date_ms { get; set; }
        public string purchase_date_pst { get; set; }
        public string original_purchase_date { get; set; }
        public string original_purchase_date_ms { get; set; }
        public string original_purchase_date_pst { get; set; }
        public string expires_date { get; set; }
        public string expires_date_ms { get; set; }
        public string expires_date_pst { get; set; }
        public string cancellation_date { get; set; }
        public string cancellation_date_ms { get; set; }
        public string cancellation_date_pst { get; set; }        
        public string web_order_line_item_id { get; set; }
        public string is_trial_period { get; set; }

        public int CompareTo(IOSInAppInfo other)
        {
            return this.ExpiresDateUTC.CompareTo(other.ExpiresDateUTC);
        }
    }

    public class Receipt
    {
        public string receipt_type { get; set; }
        public string bundle_id { get; set; }
        public string application_version { get; set; }
        public string request_date { get; set; }
        public string request_date_ms { get; set; }
        public string request_date_pst { get; set; }
        public string original_purchase_date { get; set; }
        public string original_purchase_date_ms { get; set; }
        public string original_purchase_date_pst { get; set; }
        public string original_application_version { get; set; }
        public List<InApp> in_app { get; set; }
    }

    public class LatestReceiptInfo : IOSInAppInfo, IComparable<IOSInAppInfo>
    {
        private DateTime _expiresDateUTC = DateTime.MinValue;
        private DateTime _purchaseDateUTC = DateTime.MinValue;
        private DateTime _cancellationDateUTC = DateTime.MinValue;

        public DateTime ExpiresDateUTC
        {
            get
            {
                if (!string.IsNullOrEmpty(this.expires_date) && DateTime.MinValue == _expiresDateUTC)
                {
                    _expiresDateUTC = DateTime.ParseExact(this.expires_date,
                                                        "yyyy-MM-dd HH:mm:ss 'Etc/GMT'",
                                                        CultureInfo.InvariantCulture,
                                                        DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);
                }
                return _expiresDateUTC;
            }
            set { _expiresDateUTC = value; }
        }

        public DateTime PurchaseDateUTC
        {
            get
            {
                if (!string.IsNullOrEmpty(this.purchase_date) && DateTime.MinValue == _purchaseDateUTC)
                {
                    _purchaseDateUTC = DateTime.ParseExact(this.purchase_date,
                                        "yyyy-MM-dd HH:mm:ss 'Etc/GMT'",
                                        CultureInfo.InvariantCulture,
                                        DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);

                }
                return _purchaseDateUTC;
            }
        }

        public DateTime CancellationDateUTC
        {
            get
            {
                if (!string.IsNullOrEmpty(this.cancellation_date) && DateTime.MinValue == _cancellationDateUTC)
                {
                    _cancellationDateUTC = DateTime.ParseExact(this.cancellation_date,
                                                            "yyyy-MM-dd HH:mm:ss 'Etc/GMT'",
                                                            CultureInfo.InvariantCulture,
                                                            DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);
                }
                return _cancellationDateUTC;
            }
        }

        public string quantity { get; set; }
        public string product_id { get; set; }
        public string transaction_id { get; set; }
        public string original_transaction_id { get; set; }
        public string purchase_date { get; set; }
        public string purchase_date_ms { get; set; }
        public string purchase_date_pst { get; set; }
        public string original_purchase_date { get; set; }
        public string original_purchase_date_ms { get; set; }
        public string original_purchase_date_pst { get; set; }
        public string expires_date { get; set; }
        public string expires_date_ms { get; set; }
        public string expires_date_pst { get; set; }
        public string cancellation_date { get; set; }
        public string cancellation_date_ms { get; set; }
        public string cancellation_date_pst { get; set; }
        public string web_order_line_item_id { get; set; }
        public string is_trial_period { get; set; }

        public int CompareTo(IOSInAppInfo other)
        {
            return this.ExpiresDateUTC.CompareTo(other.ExpiresDateUTC);
        }
    }

    public class IOSReceipt
    {
        private int _status = -1;
        public int status { get { return _status; } set { _status = value; } }
        public string environment { get; set; }
        public Receipt receipt { get; set; }
        public List<LatestReceiptInfo> latest_receipt_info { get; set; }
        public string latest_receipt { get; set; }

        public IOSInAppInfo GetLatestInAppInfo()
        {
            IOSInAppInfo latestInAppInfo = null;
            if (null != latest_receipt_info && latest_receipt_info.Count > 0)
            {
                latestInAppInfo=latest_receipt_info.Last();
            }

            if (null == latestInAppInfo && null != receipt.in_app && receipt.in_app.Count > 0)
            {
                latestInAppInfo = receipt.in_app.Last();
            }
            return latestInAppInfo;
        }
    }
}