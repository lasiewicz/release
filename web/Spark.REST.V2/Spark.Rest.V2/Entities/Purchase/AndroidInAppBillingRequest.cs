﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.Common.Adapter;
using Spark.REST.Models;

namespace Spark.Rest.V2.Entities.Purchase
{

    [DataContract(Name = "AndroidInAppRenewRequest", Namespace = "")]
    public class AndroidInAppRenewRequest : AndroidInAppBillingRequest
    {
        [DataMember(Name = "memberId")]
        public int MemberId { get; set; }
    }


    [DataContract(Name = "AndroidInAppBillingRequest", Namespace = "")]
    public class AndroidInAppBillingRequest : AndroidInAppValidateReceiptRequest
    {
        [DataMember(Name = "orderAttributeXML")]
        public string OrderAttributeXML { get; set; }

        //TODO: Flag added for UPS Testing.  Remove before launch to prod. -arod
        private int _iapStatus = -1;
        [DataMember(Name = "iapStatus")]
        public int IapStatus
        {
            get { return _iapStatus; }
            set { _iapStatus = value; }
        }

    }

    [DataContract(Name = "AndroidInAppValidateSubscriptionRequest", Namespace = "")]
    public class AndroidInAppValidateReceiptRequest : BrandRequest
    {
        [DataMember(Name = "packageName")]
        public string PackageName { get; set; }

        [DataMember(Name = "subscriptionId")]
        public string SubscriptionId { get; set; }

        [DataMember(Name = "tokenId")]
        public string TokenId { get; set; }

        /// <summary>
        /// SubscriptionToken is pipe-delimited string of the following:
        /// PackageName|SubscriptionId|TokenId
        /// </summary>
        [DataMember(Name = "subscriptionToken")]
        public string SubscriptionToken { get; set; }

        /// <summary>
        ///     For CM to pass in their region Id
        /// </summary>
        [DataMember(Name = "regionId")]
        public int RegionId { get; set; }
    }

    public class AndroidValidateResponse
    {
        public AndroidValidateResponse(string subscriptionToken, IAPControllerStatus status, AndroidSubscription androidSubscription)
        {
            Status = status;
            AndroidSubscriptionObj = androidSubscription;
            SubscriptionToken = subscriptionToken;
        }

        public IAPControllerStatus Status { get; set; }
        public string SubscriptionToken { get; set; }
        public AndroidSubscription AndroidSubscriptionObj { get; set; }
    }

    public class AndroidSubscription
    {
        private int _status = -1;
        public int Status { get { return _status; } set { _status = value; } }
        public string ErrorReason { get; set; }
        public string Kind { get; set; }
        public bool IsAutoRenew { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime StartDate { get; set; }
        public string PackageName { get; set; }
        public string SubscriptionId { get; set; }
        public string TokenId { get; set; }

        public void Init(string kind, DateTime expirationDate, DateTime startDate, bool isAutoRenew, string packageName, string subscriptionId, string tokenId)
        {
            Kind = kind;
            ExpirationDate = expirationDate;
            StartDate = startDate;
            IsAutoRenew = isAutoRenew;
            PackageName = packageName;
            SubscriptionId = subscriptionId;
            TokenId = tokenId;
        }
    }

}