﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Spark.REST.Entities.Session
{
    [DataContract(Name = "session", Namespace = "")]
    public class Session
    {
        [DataMember(Name = "key")]
        public String Key { get; set; }

        [DataMember(Name = "properties")]
        public Dictionary<String, Object> Properties { get; set; }
    }
}