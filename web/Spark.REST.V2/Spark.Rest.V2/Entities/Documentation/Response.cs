﻿using System;
using System.Collections.Generic;

namespace Spark.Rest.Entities.Documentation
{
    public class Response
    {
        public Type Type { get; set; }
        public string FriendlyName { get; set; }
        public string CachedName { get; set; }
        public bool IsCollection { get; set; }
        public Type CollectionOfType { get; set; }
        public Dictionary<string, object> KeyValuePairs { get; set; }
    }
}