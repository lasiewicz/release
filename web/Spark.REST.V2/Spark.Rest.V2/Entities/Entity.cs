using System;

namespace Spark.REST.Entities
{
    /// <summary>
    /// The interface which define and entity
    /// </summary>
	public interface IEntity
	{
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
		int Id { get; set; }
	}
}