using System.Runtime.Serialization;

namespace Spark.REST.Entities
{
    /// <summary>
    /// 
    /// </summary>
	[DataContract(Name = "authentication", Namespace = "")]
	public class Authentication : IEntity
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
		[DataMember(Name = "userName")]
		public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>
        /// The status code.
        /// </value>
		[DataMember(Name = "statusCode")]
		public int StatusCode { get; set; }

        /// <summary>
        /// Gets or sets the status message.
        /// </summary>
        /// <value>
        /// The status message.
        /// </value>
		[DataMember(Name = "statusMessage")]
		public string StatusMessage { get; set; }

	}
}