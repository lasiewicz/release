﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Matchnet.Member.ValueObjects.Photos;

namespace Spark.Rest.V2.Entities.MingleMigration
{
    [DataContract(Name = "MinglePhotoApprovalResult", Namespace = "")]
    public class MinglePhotoApprovalResult
    {
        public MinglePhotoApprovalResult()
        {
            FileResults = new List<MinglePhotoFileResult>();
        }

        [DataMember(Name = "FileResults")]
        public List<MinglePhotoFileResult> FileResults { get; set; }
    }

    [DataContract(Name = "MinglePhotoFileResult", Namespace = "")]
    public class MinglePhotoFileResult
    {
        [DataMember(Name = "FileType")]
        public PhotoFileType FileType { get; set; }

        [DataMember(Name = "CloudPath")]
        public string CloudPath { get; set; }
    }
}