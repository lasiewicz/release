﻿namespace Spark.Rest.V2.Entities.MingleMigration
{
    public class MinglePhotoTransferCollectionResult
    {
        public int MemberId { get; set; }
        public int MemberPhotoId { get; set; }
        public int ListOrder { get; set; }
    }
}