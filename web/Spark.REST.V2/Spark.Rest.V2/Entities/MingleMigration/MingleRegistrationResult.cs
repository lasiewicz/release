﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Entities
{
    [DataContract(Name = "MingleRegistrationResult", Namespace = "")]
    public class MingleRegistrationResult
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }
        [DataMember(Name = "RegisterStatus")]
        public string RegisterStatus { get; set; }
        [DataMember(Name = "Username")]
        public string Username { get; set; }
    }
}