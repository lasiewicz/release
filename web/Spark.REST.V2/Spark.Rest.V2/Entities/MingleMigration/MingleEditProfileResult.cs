﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Entities.MingleMigration
{
    [DataContract(Name = "MingleEditProfileResult", Namespace = "")]
    public class MingleEditProfileResult
    {
        public bool Success { get; set; }
    }
}