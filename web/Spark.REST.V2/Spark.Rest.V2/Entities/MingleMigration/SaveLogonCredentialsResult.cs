﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Spark.Rest.V2.Entities.MingleMigration
{
    [DataContract(Name = "SaveLogonCredentialsResult", Namespace = "")]
    public class SaveLogonCredentialsResult
    {
        [DataMember(Name = "Status")]
        public string Status { get; set; }
        [DataMember(Name = "Username")]
        public string Username { get; set; }
    }
}