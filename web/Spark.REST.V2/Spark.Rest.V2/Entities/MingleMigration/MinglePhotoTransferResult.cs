﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Rest.V2.Entities.MingleMigration
{
    public class MinglePhotoTransferResult
    {
        public int MemberPhotoId { get; set; }
    }
}