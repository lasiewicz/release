﻿namespace Spark.Rest.V2.Entities.MingleMigration
{
    public class UploadPhotoResult
    {
        public UploadPhotoResult()
        {
            IsSuccessful = false;
            MinglePhotoUploadResult = new MinglePhotoUploadResult();
        }

        public bool IsSuccessful { get; set; }
        public MinglePhotoUploadResult MinglePhotoUploadResult { get; set; }
    }

    public class MinglePhotoUploadResult
    {
        public int MemberPhotoId { get; set; }
        public string CloudPath { get; set; }
        public string FilePath { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }
}