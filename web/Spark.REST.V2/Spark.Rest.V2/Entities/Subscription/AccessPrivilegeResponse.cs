﻿#region

using System;
using System.Runtime.Serialization;

#endregion

namespace Spark.Rest.V2.Entities.Subscription
{
    /// <summary>
    /// A custom response class for UPS Access Privilege
    /// </summary>
    [DataContract(Name = "AccessPrivilegeResponse", Namespace = "")]
    public class AccessPrivilegeResponse
    {
        /// name of this privilege
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// when does this privilege end?
        [DataMember(Name = "endDateUtc")]
        public DateTime EndDateUtc { get; set; }

        /// only applicable to some privileges
        [DataMember(Name = "remainingCount")]
        public int? RemainingCount { get; set; }
    }
}