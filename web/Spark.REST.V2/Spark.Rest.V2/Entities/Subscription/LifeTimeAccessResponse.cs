﻿#region

using System;
using System.Runtime.Serialization;

#endregion

namespace Spark.Rest.V2.Entities.Subscription
{
    [DataContract(Name = "LifeTimeAccessResponse", Namespace = "")]
    public class LifeTimeAccessResponse
    {
        [DataMember(Name = "MemberId")]
        public int MemberId { get; set; }

        [DataMember(Name = "RenewalDate")]
        public DateTime RenewalDate { get; set; }
    }
}