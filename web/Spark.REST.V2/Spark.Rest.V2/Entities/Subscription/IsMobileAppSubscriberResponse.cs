﻿#region

using System.Runtime.Serialization;

#endregion

namespace Spark.Rest.V2.Entities.Subscription
{
    /// <summary>
    /// </summary>
    [DataContract(Name = "IsMobileAppSubscriberResponse", Namespace = "")]
    public class IsMobileAppSubscriberResponse
    {
        /// <summary>
        ///     IAP, IAB, etc
        /// </summary>
        [DataMember(Name = "mobileStore")]
        public string MobileStore { get; set; }

        /// <summary>
        /// </summary>
        [DataMember(Name = "isSubscriber")]
        public bool IsSubscriber { get; set; }
    }
}