﻿using System;
using System.Runtime.Serialization;

namespace Spark.Rest.Entities
{
    ///<summary>
    ///</summary>
    [Obsolete]
    [DataContract(Name = "result", Namespace = "")]
    public class BaseResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="BaseResult"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "success")]
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets the failure reason.
        /// </summary>
        /// <value>
        /// The failure reason.
        /// </value>
        [DataMember(Name = "failReason", EmitDefaultValue = true)]
        public string FailureReason { get; set; }
    }
}