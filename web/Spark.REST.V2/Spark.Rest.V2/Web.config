﻿<?xml version="1.0" encoding="utf-8"?>
<!--
web.config file name convention 
Debug = Local
Release = Production
Stage = Used if all stage environments have the exact same web.config
One =  101/102
Two =  201/202
Three =  301/302
ThreeV3 = 303
ThreeV4 = 304
Preprod = lawebcore00
-->
<configuration>
  <configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" />
    <section name="RaygunSettings" type="Mindscape.Raygun4Net.RaygunSettings, Mindscape.Raygun4Net" />
  </configSections>
  <RaygunSettings apikey="vY22R+pG2C8h5j8jBeKN+A==" />
  <log4net debug="true">
    <appender name="RollingLogFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="c:\\logfiles\\Spark.REST.V2.log" />
      <immediateFlush value="true" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="10" />
      <maximumFileSize value="100MB" />
      <staticLogFileName value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5p %d %5rms %-22.22c{1} %-18.18M - %m%n" />
      </layout>
    </appender>
    <appender name="RollingWebLogFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="c:\\logfiles\\Spark.REST.Web.V2.log" />
      <immediateFlush value="true" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="10" />
      <maximumFileSize value="100MB" />
      <staticLogFileName value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5p %d %5rms %-22.22c{1} %-18.18M - %m%n" />
      </layout>
    </appender>
    <appender name="RollingDeprecatedLogFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="c:\\logfiles\\Spark.REST.Deprecated.V2.log" />
      <immediateFlush value="true" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="10" />
      <maximumFileSize value="100MB" />
      <staticLogFileName value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5p %d %5rms %-22.22c{1} %-18.18M - %m%n" />
      </layout>
    </appender>
    <appender name="RollingTimingLogFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="c:\\logfiles\\Spark.REST.Timing.V2.log" />
      <immediateFlush value="true" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="10" />
      <maximumFileSize value="100MB" />
      <staticLogFileName value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5p %d %5rms %-22.22c{1} %-18.18M - %m%n" />
      </layout>
    </appender>    
    <!--
     EventLogAppender requires an application source to be added to the event log. 
     
     Two options for creating the source
       1. Manully create a new application in regedit HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Eventlog\Application 
       2. c:\» eventcreate /ID 1 /L APPLICATION /T INFORMATION /SO "Spark.REST.V2" /D "test log message"
     
     In order to rid of the event log description missing blob, do the following
       Go to the registry key, edit EventMessageFile key's value to, 
        c:\Windows\Microsoft.NET\Framework64\v4.0.30319\EventLogMessages.dll
        
     There is no need to worry about security if Network Service is used as it should be.
     -->
    <appender name="EventLogAppender" type="log4net.Appender.EventLogAppender">
      <eventId value="8282" />
      <applicationName value="Spark.REST.V2" />
      <filter type="log4net.Filter.LevelRangeFilter">
        <acceptOnMatch value="true" />
        <levelMin value="WARN" />
        <levelMax value="FATAL" />
      </filter>
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date [%thread] %-5level %logger [%property{NDC}] - %message%newline" />
      </layout>
    </appender>
    <root>
      <level value="ALL" />
      <appender-ref ref="RollingLogFileAppender" />
      <appender-ref ref="EventLogAppender" />
    </root>
    <logger additivity="false" name="RollingWebLogFileAppender">
      <level value="ALL" />
      <appender-ref ref="RollingWebLogFileAppender" />
    </logger>
    <logger additivity="false" name="RollingDeprecatedLogFileAppender">
      <level value="ALL" />
      <appender-ref ref="RollingDeprecatedLogFileAppender" />
    </logger>
    <logger additivity="false" name="RollingTimingLogFileAppender">
      <level value="ALL" />
      <appender-ref ref="RollingTimingLogFileAppender" />
    </logger>
  </log4net>
  <appSettings>
    <!-- for MVC 3 to 4 upgrade-->
    <add key="webpages:Version" value="2.0.0.0" />
    <!-- for MVC 3 to 4 upgrade-->
    <add key="PreserveLoginUrl" value="true" />
    <add key="log4net.Internal.Debug" value="true" />
    <add key="Spark.Logger.MaxTaskCount" value="25" />
    <add key="EnforceOAuth" value="true" />
    <add key="AccessTokenLifetimeMins" value="2880" />
    <add key="AccessTokenLongLifetimeMins" value="43200" />
    <add key="RefreshTokenLifetimeMins" value="43200" />
    <!-- 2880 is two days, 86400 is 60 days, 43200 is 30 days -->
    <add key="RouteDebugger:Enabled" value="false" />
    <add key="Environment" value="dev" />
    <add key="hackAppIdToAvoidLogLastLogon" value="1054" />
    <add key="IOS_IAP_URL" value="https://sandbox.itunes.apple.com" />
    <add key="MessageAttachmentURLPath" value="http://s3.amazonaws.com/sparkmessageattachmentsdev" />
  </appSettings>
  <!--
    For a description of web.config changes for .NET 4.5 see http://go.microsoft.com/fwlink/?LinkId=235367.

    The following attributes can be set on the <httpRuntime> tag.
      <system.Web>
        <httpRuntime targetFramework="4.5" />
      </system.Web>
  -->
  <system.web>
    <!-- 
      Completely turn off session to ensure API stays stateless
      http://stackoverflow.com/questions/17064380/what-happens-in-beginprocessrequest
    -->
    <sessionState mode="Off" />
    <compilation debug="true" targetFramework="4.5">
      <assemblies>
        <add assembly="System.Web.Helpers, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Mvc, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.WebPages, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Abstractions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Routing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
      </assemblies>
    </compilation>
    <pages controlRenderingCompatibilityVersion="4.0" clientIDMode="AutoID">
      <namespaces>
        <add namespace="System.Web.Helpers" />
        <add namespace="System.Web.Mvc" />
        <add namespace="System.Web.Mvc.Ajax" />
        <add namespace="System.Web.Mvc.Html" />
        <add namespace="System.Web.Routing" />
        <add namespace="System.Web.WebPages" />
      </namespaces>
    </pages>
    <httpRuntime relaxedUrlToFileSystemMapping="true" maxRequestLength="2097150" />
    
    <!-- customErrors is a deprecated way to define error pages that redirects, use HttpErrors-->

  </system.web>
  <system.webServer>
    <!-- to turn off warning from adding errorhandlerhttpmodule in <system.web><httpmodules>
    <validation validateIntegratedModeConfiguration="false"/>-->
    <httpProtocol>
      <customHeaders>
        <add name="Access-Control-Allow-Origin" value="*" />
        <add name="Access-Control-Allow-Headers" value="X-Requested-With, X-Requested-By, Content-Type, Accept, Origin" />
        <add name="Access-Control-Allow-Methods" value="POST, GET, PUT, DELETE, OPTIONS" />
      </customHeaders>
    </httpProtocol>
    <modules runAllManagedModulesForAllRequests="true">
      <add name="LoggingHttpModule" type="Spark.Rest.Modules.LoggingHttpModule" />
      <add name="ErrorHandlerHttpModule" type="Spark.Rest.V2.Modules.ErrorHandlerHttpModule" />
      <add name="CacheManagerModule" type="Spark.REST.CacheManagerModule" />
      <remove name="WebDAVModule" />
      <!--<add name="RaygunErrorModule" type="Mindscape.Raygun4Net.RaygunHttpModule"/>-->
    </modules>
    <handlers>
      <remove name="UrlRoutingHandler" />
    </handlers>
    <!-- httpErrors has been commented out since the custom error module handles all errors-->
    <!-- https://msdn.microsoft.com/en-us/library/ms690576%28v=vs.90%29.aspx -->
    <!--<httpErrors errorMode="Custom" existingResponse="Auto">
    </httpErrors>-->
    <httpCompression directory="%SystemDrive%\inetpub\temp\IIS Temporary Compressed Files">
      <scheme name="gzip" dll="%Windir%\system32\inetsrv\gzip.dll" />
      <!-- dynamic compression is to compress dynamic responses-->
      <dynamicTypes>
        <add mimeType="text/*" enabled="true" />
        <add mimeType="message/*" enabled="true" />
        <add mimeType="application/javascript" enabled="true" />
        <!-- for json, the following two entires do not work in web.config but only on the server level
        in IIS7->Server->Configuration Editor->system.webServer/httpCompression/dynamicTypes/
        add the following two json entries-->
        <add mimeType="application/json" enabled="true" />
        <add mimeType="application/json; charset=utf-8" enabled="true" />
        <add mimeType="*/*" enabled="false" />
      </dynamicTypes>
      <!-- static compression is to compress files that are directly served from the hdd-->
      <staticTypes>
        <add mimeType="text/*" enabled="true" />
        <add mimeType="message/*" enabled="true" />
        <add mimeType="application/javascript" enabled="true" />
        <add mimeType="*/*" enabled="false" />
      </staticTypes>
    </httpCompression>
    <urlCompression doStaticCompression="true" doDynamicCompression="true" />
  </system.webServer>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-2.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="log4net" publicKeyToken="669e0ddf0bb1aa2a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-1.2.13.0" newVersion="1.2.13.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Mindscape.Raygun4Net" publicKeyToken="06087ed14ddaba9c" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.3.3.0" newVersion="3.3.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
        <bindingRedirect oldVersion="1.0.0.0-5.0.0.0" newVersion="6.0.1.17001" />
        <publisherPolicy apply="no" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Net.Http.Primitives" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.2.22.0" newVersion="4.2.22.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.serviceModel>
    <bindings>
      <basicHttpBinding>
        <binding name="BasicHttpBinding_IPurchaseService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:10:00" allowCookies="false" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" maxBufferPoolSize="2147483647" maxBufferSize="2147483647" maxReceivedMessageSize="2147483647" textEncoding="utf-8" transferMode="Buffered" useDefaultWebProxy="true" messageEncoding="Text">
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
          <security mode="TransportCredentialOnly">
            <transport clientCredentialType="Windows" proxyCredentialType="None" realm="" />
            <message clientCredentialType="UserName" algorithmSuite="Default" />
          </security>
        </binding>
        <binding name="BasicHttpBinding_IPaymentProfileMapper" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:10:00" allowCookies="false" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" maxBufferPoolSize="2147483647" maxBufferSize="2147483647" maxReceivedMessageSize="2147483647" textEncoding="utf-8" transferMode="Buffered" useDefaultWebProxy="true" messageEncoding="Text">
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
          <security mode="TransportCredentialOnly">
            <transport clientCredentialType="Windows" proxyCredentialType="None" realm="" />
            <message clientCredentialType="UserName" algorithmSuite="Default" />
          </security>
        </binding>
        <binding name="BasicHttpBinding_IOrderHistoryManager" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:10:00" allowCookies="false" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" maxBufferPoolSize="2147483647" maxBufferSize="2147483647" maxReceivedMessageSize="2147483647" textEncoding="utf-8" transferMode="Buffered" useDefaultWebProxy="true" messageEncoding="Text">
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
          <security mode="TransportCredentialOnly">
            <transport clientCredentialType="Windows" proxyCredentialType="None" realm="" />
            <message clientCredentialType="UserName" algorithmSuite="Default" />
          </security>
        </binding>
        <binding name="BasicHttpBinding_IPaymentWrapperService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:10:00" allowCookies="false" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" maxBufferPoolSize="2147483647" maxBufferSize="2147483647" maxReceivedMessageSize="2147483647" textEncoding="utf-8" transferMode="Buffered" useDefaultWebProxy="true" messageEncoding="Text">
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
          <security mode="TransportCredentialOnly">
            <transport clientCredentialType="Windows" proxyCredentialType="None" realm="" />
            <message clientCredentialType="UserName" algorithmSuite="Default" />
          </security>
        </binding>
        <binding name="BasicHttpBinding_IRenewalService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" allowCookies="false" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" maxBufferPoolSize="2147483647" maxBufferSize="2147483647" maxReceivedMessageSize="2147483647" textEncoding="utf-8" transferMode="Buffered" useDefaultWebProxy="true" messageEncoding="Text">
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
          <security mode="None">
            <transport clientCredentialType="None" proxyCredentialType="None" realm="" />
            <message clientCredentialType="UserName" algorithmSuite="Default" />
          </security>
        </binding>
        <binding name="BasicHttpBinding_IAccessService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" allowCookies="false" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" maxBufferPoolSize="2147483647" maxBufferSize="2147483647" maxReceivedMessageSize="2147483647" textEncoding="utf-8" transferMode="Buffered" useDefaultWebProxy="true" messageEncoding="Text">
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
          <security mode="None">
            <transport clientCredentialType="None" proxyCredentialType="None" realm="" />
            <message clientCredentialType="UserName" algorithmSuite="Default" />
          </security>
        </binding>
        <binding name="BasicHttpBinding_ICatalogService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" allowCookies="false" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" maxBufferSize="65536" maxBufferPoolSize="524288" maxReceivedMessageSize="65536" messageEncoding="Text" textEncoding="utf-8" transferMode="Buffered" useDefaultWebProxy="true">
          <readerQuotas maxDepth="32" maxStringContentLength="8192" maxArrayLength="16384" maxBytesPerRead="4096" maxNameTableCharCount="16384" />
          <security mode="None">
            <transport clientCredentialType="None" proxyCredentialType="None" realm="" />
            <message clientCredentialType="UserName" algorithmSuite="Default" />
          </security>
        </binding>
        <binding name="BasicHttpBinding_IAuthorizationService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" allowCookies="false" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" maxBufferSize="2147483647" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" messageEncoding="Text" textEncoding="utf-8" transferMode="Buffered" useDefaultWebProxy="true">
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
          <security mode="None">
            <transport clientCredentialType="None" proxyCredentialType="None" realm="" />
            <message clientCredentialType="UserName" algorithmSuite="Default" />
          </security>
        </binding>
		      <binding name="BasicHttpBinding_IFraudService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" allowCookies="false" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" maxBufferSize="2147483647" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" messageEncoding="Text" textEncoding="utf-8" transferMode="Buffered" useDefaultWebProxy="true">
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
          <security mode="None">
            <transport clientCredentialType="None" proxyCredentialType="None"
                realm="">
            </transport>
            <message clientCredentialType="UserName" algorithmSuite="Default" />
          </security>
        </binding>
       </basicHttpBinding>
    </bindings>
    <client>
      <endpoint address="http://ladevsvc103:2789/PurchaseService.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IPurchaseService" contract="PurchaseService.IPurchaseService" name="BasicHttpBinding_IPurchaseService" />
      <endpoint address="http://ladevsvc103:2789/PaymentProfileService.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IPaymentProfileMapper" contract="PaymentProfileService.IPaymentProfileMapper" name="BasicHttpBinding_IPaymentProfileMapper" />
      <endpoint address="http://ladevsvc103:2789/OrderHistoryService.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IOrderHistoryManager" contract="OrderHistoryService.IOrderHistoryManager" name="BasicHttpBinding_IOrderHistoryManager" />
      <endpoint address="http://ladevsvc103:2788/PaymentWrapperService.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IPaymentWrapperService" contract="PaymentWrapperService.IPaymentWrapperService" name="BasicHttpBinding_IPaymentWrapperService" />
      <endpoint address="http://ladevsvc103:6787/RenewalService.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IRenewalService" contract="RenewalService.IRenewalService" name="BasicHttpBinding_IRenewalService" />
      <endpoint address="http://ladevsvc103:6788/AccessService.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IAccessService" contract="AccessService.IAccessService" name="BasicHttpBinding_IAccessService" />
      <endpoint address="http://ladevsvc103:2789/CatalogService.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_ICatalogService" contract="CatalogService.ICatalogService" name="BasicHttpBinding_ICatalogService" />
      <endpoint address="http://ladevsvc103:7787/AuthorizationService.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IAuthorizationService" contract="AuthorizationService.IAuthorizationService" name="BasicHttpBinding_IAuthorizationService" />
      <endpoint address="http://ladevsvc103:4789/FraudService.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IFraudService" contract="FraudService.IFraudService" name="BasicHttpBinding_IFraudService" />
    </client>
  </system.serviceModel>
</configuration>
