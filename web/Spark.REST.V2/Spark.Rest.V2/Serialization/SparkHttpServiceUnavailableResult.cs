﻿using Spark.Rest.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Spark.Rest.V2.Serialization
{
    public class SparkHttpServiceUnavailableResult : SparkHttpStatusCodeResult
    {
        private readonly int _subStatusCode;

        public SparkHttpServiceUnavailableResult(int subStatusCode, string description)
            : base((int)HttpStatusCode.ServiceUnavailable, subStatusCode, description)
        {
            _subStatusCode = subStatusCode;
        }

        public new int SubStatusCode
        {
            get { return _subStatusCode; }
        }
    }
}
