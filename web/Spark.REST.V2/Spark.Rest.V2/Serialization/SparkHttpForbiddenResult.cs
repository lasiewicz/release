#region

using System.Net;

#endregion

namespace Spark.Rest.Serialization
{
    public class SparkHttpForbiddenResult : SparkHttpStatusCodeResult
    {
        /// <summary>
        ///   Authenticated but doesn't have subscription, etc.
        /// </summary>
        /// <param name = "subStatusCode"></param>
        /// <param name = "description"></param>
        public SparkHttpForbiddenResult(int subStatusCode, string description)
            : base((int) HttpStatusCode.Forbidden, subStatusCode, description)
        {
        }
    }
}