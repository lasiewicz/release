﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Rest.V2.Serialization.HttpSubStatusCodes;

namespace Spark.Rest.Serialization
{
    public class SparkGenericHttpInternalServerErrorResult : SparkHttpInternalServerErrorResult
    {
        public SparkGenericHttpInternalServerErrorResult()
            : base((int)HttpSub500StatusCode.InternalServerError, "An unexpected error occurred")
        {
            
        }
    }
}