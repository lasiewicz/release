#region

using System.Net;

#endregion

namespace Spark.Rest.Serialization
{
    public class SparkHttpInternalServerErrorResult : SparkHttpStatusCodeResult
    {
        private readonly int _subStatusCode;

        public SparkHttpInternalServerErrorResult(int subStatusCode, string description)
            : base((int) HttpStatusCode.InternalServerError, subStatusCode, description)
        {
            _subStatusCode = subStatusCode;
        }

        public new int SubStatusCode
        {
            get { return _subStatusCode; }
        }
    }
}