#region

using System.Web.Mvc;

#endregion

namespace Spark.Rest.Serialization
{
    public class SparkHttpStatusCodeResult : HttpStatusCodeResult
    {
        private readonly int _subStatusCode;

        public SparkHttpStatusCodeResult(int statusCode, int subStatusCode, string statusDescription)
            : base(statusCode, ShortenStatusDescription(statusDescription))
        {
            _subStatusCode = subStatusCode;
        }

        public int SubStatusCode
        {
            get { return _subStatusCode; }
        }

        public override void ExecuteResult(ControllerContext context)
        {
            base.ExecuteResult(context);
            if (context.HttpContext.Response.StatusCode != StatusCode)
            {
                context.HttpContext.Response.StatusCode = StatusCode;
            }
            context.HttpContext.Response.SubStatusCode = SubStatusCode;
        }

        private static string ShortenStatusDescription(string statusDescription)
        {
            if (statusDescription.Length > 512)
            {
                return statusDescription.Substring(0, 512);
            }

            return statusDescription;
        }
    }
}