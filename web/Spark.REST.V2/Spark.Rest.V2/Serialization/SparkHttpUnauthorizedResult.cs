#region

using System.Net;

#endregion

namespace Spark.Rest.Serialization
{
    public class SparkHttpUnauthorizedResult : SparkHttpStatusCodeResult
    {
        private readonly int _subStatusCode;

        public SparkHttpUnauthorizedResult(int subStatusCode, string description)
            : base((int) HttpStatusCode.Unauthorized, subStatusCode, description)
        {
            _subStatusCode = subStatusCode;
        }

        public new int SubStatusCode
        {
            get { return _subStatusCode; }
        }
    }
}