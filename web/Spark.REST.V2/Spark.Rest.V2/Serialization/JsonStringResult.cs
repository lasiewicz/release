﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spark.Rest.V2.Serialization
{
    /// <summary>
    /// This result is used to return back a raw string as JSON without any wrappers
    /// </summary>
    public class JsonStringResult : ContentResult
    {
        public JsonStringResult(string json)
        {
            Content = json;
            ContentType = "application/json";
            ContentEncoding = System.Text.Encoding.UTF8;
        }

    }
}