using System.Net;
using System.Web.Mvc;


namespace Spark.Rest.Serialization
{
    public class SparkHttpBadRequestResult : SparkHttpStatusCodeResult
    {
        private readonly int _subStatusCode;

        public new int SubStatusCode
        {
            get { return _subStatusCode; }
        }

        public SparkHttpBadRequestResult(int subStatusCode, string description): base((int)HttpStatusCode.BadRequest, subStatusCode, description)
        {
            _subStatusCode = subStatusCode;
        }
    }
}