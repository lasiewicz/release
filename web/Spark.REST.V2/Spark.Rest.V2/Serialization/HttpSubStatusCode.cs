﻿namespace Spark.Rest.V2.Serialization.HttpSubStatusCodes
{
    public enum HttpSub400StatusCode
    {
        None = 0,
        ValidMemberIdRequired = 40000,
        MissingMemberId = 40001,
        InvalidMemberId = 40002,
        FailedUploadPhoto = 40003,
        MissingPhotoUploadData = 40004,
        FailedDeletePhoto = 40005,
        UnsupportedPhotoFormat = 40006,
        MaxPhotoSizeExceeded = 40007,
        MaxNumberOfPhotosExceeded = 40008,
        InvalidMemberPhotoId = 40009,
        FailedPhotoSave = 40010,
        FailedRemoveFromHotList = 40011,
        FailedAddToHotList = 40012,
        FailedSendMailMessage = 40018,
        FailedSendTease = 40019,
        InvalidBrandId = 40021,
        UnknownHotListCategory = 40022,
        UnsupportedHotListCategory = 40023,
        MissingEmail = 40024,
        MissingPassword = 40025,
        InvalidApplicationId = 40026,
        MissingApplicationId = 40027,
        MissingAttributesToUpdate = 40030,
        FailedIAPPurchase = 40031,
        FailedIAPDisableAutoRenewal = 40032,
        FailedIAPCreditOrder = 40033,
        FailedGooglePlayPurchase = 40034,
        FailedGooglePlayDisableAutoRenewal = 40035,
        FailedGooglePlayCreditOrder = 40036,
        FailedSaveMatchPreferences = 40040,
        FailedAuthentication = 40050,
        FailedCreateSession = 40055,
        FailedSaveSession = 40056,
        FailedGetSession = 40057,
        FailedGetMemberFromSession = 40058,
        InvalidSessionProperty = 40059,
        FailedRegistration = 40060,
        InvalidPageSize = 40061,
        BlockedSenderIdForSendMail = 40062,
        InvalidPasswordResetToken = 40063,
        InvalidLocation = 40064,
        AttributeMappingFailure = 40065,
        SearchPreferenceMappingFailure = 40066,
        MingleRegistrationRequestMissingRequiredParameters = 40067,
        FailedMingleMemberUpdate = 40068,
        InvalidUserName = 40069,
        FailedSaveLogonCredentials = 40070,
        FailedUpdateLogonActiveFlag = 40071,
        FailedUpdateProfileEnabledStatus = 40072,
        FailedLifeTimeAccess = 40073, 
        FailedMigrationPhotoApproval=40074, 
        FailedMigrationPhotoRejection=40076, 
        FailedMigrationPhotoUpload=40077, 
        FailedPhotoNotFound=40078,
        FailedMigrationPhotoReorder = 40079,
        FailedMigrationPhotoCaptionUpdate = 40080, 
        DeviceNotRegisteredToMember=40082,
        NoPromoFound=40083,
        MissingSubscription=40084,
        MissingPaymentProfile=40085,
        MissingArgument = 40099,
        InvalidPushNotificationType = 40100,
        InvalidConversationType = 40101,
        NoPackagesFound = 40102,
        InvalidEmail = 40103,
        GenericFailedValidation = 400098,
        GenericInvalidID = 400099,
        GenericError = 400100,
        GenericNotFound = 400101,
        FailedUpdatingSubscriptionPlan = 400110,
        FailedGettingSubscriptionPlan = 400111,
        FailedDisablingFreeTrial = 400112,
        FailedUpdateLastLogin = 400120,
        NotCategoryOwner = 400121, 
        ExceededMaxAllowed = 400122, 
        ExceededMaxAllowedForMember = 400123,
        HasAlreadyTeasedMember = 400124,
        HasAlreadyAddedMemberToList = 400125,
        NoAllAccess = 400126,
        EmailAlreadyExists = 400127,
        FailedFraudInfoGet = 400128,
        InvalidAdminMemberId = 400129
    }

    public enum HttpSub401StatusCode
    {
        None = 0,
        MissingAccessToken = 41000,
        InvalidAccessTokenVersion = 41002,
        ExpiredToken = 41003,
        TokenNotFoundForMember = 41004,
        MismatchedToken = 41005,
        InvalidApplicationId = 41007,
        InvalidMemberId = 41008,
        IPBlocked = 41011,
        InvalidEmailOrPassword = 41012,
        InvalidAccessToken = 41013,
        SubscriberIdRequired = 41020,
        ValidClientSecretRequired = 41021,
        MismatchClientSecret = 41022,
        InvalidAttributeToUpdate = 41023,
        MissingClientIP = 41024,
        InvalidClientIP = 41025,
        CredentialsAlreadyCreated = 41026,
        MemberSuspended = 41027,
        InvalidAdminMemberId = 41028
    }

    public enum HttpSub403StatusCode
    {
        SubscriptionRequired = 42000,
        ProfileViewNotAllowed = 42001
    }

    public enum HttpSub404StatusCode
    {
        InvalidResourcePath = 44000
    }

    public enum HttpSub500StatusCode
    {
        InternalServerError = 50005
    }

    public enum HttpSub503StatusCode
    {
        ServerMaintenanceError = 53001
    }
}
