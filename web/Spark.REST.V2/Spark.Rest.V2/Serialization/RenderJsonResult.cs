﻿#region

using System;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

#endregion

namespace Spark.Rest.Serialization
{
    /// <summary>
    ///     An action result that renders the given object using JSON to the response stream.
    /// </summary>
    public class NewtonsoftJsonResult : ActionResult
    {
        /// <summary>
        ///     The result object to render using JSON.
        /// </summary>
        public object Result { get; set; }

        public bool IgnoreNotImplementedException { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            var contentType = "application/json";
            if (context.HttpContext.Items.Contains("contentType"))
            {
                var s = context.HttpContext.Items["contentType"] as string;
                if (!string.IsNullOrEmpty(s)) contentType = s;
            }
            context.HttpContext.Response.ContentType = contentType;
            var settings = new JsonSerializerSettings
                {
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                    DateFormatString = "yyyy-MM-dd\\THH:mm:ss.fffK" //ensures milliseconds are always 3 digits to solve issue in IE 9 clients
                };
            
            if (IgnoreNotImplementedException)
                settings.Error = (serializer, err) =>
                {
                    if(err.ErrorContext.Error.InnerException is NotImplementedException)
                        err.ErrorContext.Handled = true;
                };
 
            var serializedResult = JsonConvert.SerializeObject(Result, settings);

            if (context.HttpContext.Items.Contains("redirectUrl") && !string.IsNullOrEmpty((string) context.HttpContext.Items["redirectUrl"]))
            {
                var redirectUrl = context.HttpContext.Items["redirectUrl"] as string;
                var encodedJson = HttpUtility.UrlEncode(serializedResult);
                if (redirectUrl == null) return;
                redirectUrl = redirectUrl.Replace("%s", "").Replace("?", "");
                context.HttpContext.Response.Redirect(redirectUrl + "?" + encodedJson);
            }
            else
            {
                context.HttpContext.Response.Write(serializedResult);
            }
        }
    }
}