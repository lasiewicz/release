﻿// Install VS Extension JScript Editor Extensions for region support.
// Scroll all the way down for helper functions.
// Use ajaxHelper function to make ajax calls.

var _statusCode;
var _mainApiUrl = _apiUrl + "/brandId/" + _brandId;

//QUnit.config.autostart = false;

//require(['tests.js'], function () {
//    QUnit.start(); //tests loaded, run tests
//});

//#region Docs
{
    module("Docs");

    asyncTest("[get docs]", 1, function() {
        ajaxHelper(_apiUrl + "/docs", 'GET', null, null).success(function(data) {
            ok(data.data.length > 0, "retrieved languages. count:" + data.data.length);
        });
    });
}
//#endregion Docs

//#region Content
{
    module("Content");

    asyncTest("[get attribute options for languagemask]", 2, function() {
        var url = _mainApiUrl + "/content/attributes/options/languagemask";
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok(data.data.length > 0, "retrieved languages. count:" + data.data.length);
        });
    });

    asyncTest("[get attribute options collection for languagemask, haircolor]", 2, function() {
        var url = _mainApiUrl + "/content/attributes/optionscollection";
        var requestData = { "attributeNames": ["haircolor", "languagemask"] };
        ajaxHelper(url, 'POST', requestData, null).success(function(data) {
            ok(data.data[0].AttributeOptionList.length > 0, "hair options count:" + data.data[0].AttributeOptionList.length);
        });
    });
}
//#endregion Content

//#region OAuth
{
    module("OAuth");

    asyncTest("[post ResetPassword] password reset request for MemberA sent", 2, function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/oauth2/accesstoken/application/" + _appId + "/resetpassword?client_secret=" + _clientSecret;
        var requestData = {
            "email": _emailAddress,
            "SuaUrl": "API-UNIT-TEST-DEADLINK",
            "TemplateId": "jdatecom",
            "IpAddress": "192.168.1.1",
            "UserAgent": "Mozilla/5.0 (iPod; U; CPU iPhone OS 4_3_3 like Mac OS X; ja-jp) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5",
            "HttpHeaders": ""
        };
        ajaxHelper(url, 'POST', requestData, null).success(function(data) {
            ok(data.data.EmailSent);
        });
    });

    asyncTest("[post CreateAccessTokenPassword] for MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/oauth2/accesstokenwithloginfraud/application/" + _appId + "?client_secret=" + _clientSecret;
        var requestData = { "email": _emailAddress, "password": _password, "ipaddress": "192.168.1.103" };
        ajaxHelper(url, 'POST', requestData, null).success(function(data) {
            sessionStorage.setItem("AccessToken", data.data.AccessToken);
            ok((data.data.AccessToken != null), "retrieved an access token for " + _emailAddress + " access token:" + sessionStorage.getItem("AccessToken"));
        });
    });

    asyncTest("[post CreateAccessTokenPassword] for MemberB", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/oauth2/accesstoken/application/" + _appId + "?client_secret=" + _clientSecret;
        var requestData = { "email": _targetMemberEmailAddress, "password": _targetMemberPassword };
        ajaxHelper(url, 'POST', requestData, null).success(function(data) {
            sessionStorage.setItem("TargetMemberAccessToken", data.data.AccessToken);
            ok((data.data.AccessToken != null), "retrieved an access token for " + _targetMemberEmailAddress + " access token:" + sessionStorage.getItem("TargetMemberAccessToken"));
        });
    });

    asyncTest("[get AuthenticateAccessToken] Authenticate MemberB's access token. Also calls PostLogonUpdate internally", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/oauth2/accesstoken/authenticate?access_token=" + sessionStorage.getItem("TargetMemberAccessToken");
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok(_targetMemberId == data.data.MemberId, "authenticate access token for memberId:" + data.data.MemberId);
        });
    });

    asyncTest("[get ValidateAccessToken V2.2] Validates MemberA's token with additional data", function () {
        var url = _apiUrl + "/brandId/" + _brandId + "/oauth2/accesstoken/validate/?accesstoken=" + sessionStorage.getItem("AccessToken") + "&applicationId=" + _appId;
        var customHeader = { 'accept': 'application/json; version=V2.2' };
        ajaxHelper(url, 'GET', null, customHeader).success(function (data) {
            ok(_memberId == data.data.memberId, "validated access token for memberId:" + data.data.memberId);
        });
    });
}
//#region OAuth

//#region Profile
{
    module("Profile");

    asyncTest("[get AttributeSetMiniProfile] MemberB fetches MemberA mini profile", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/profile/attributeset/miniProfile/" + _memberId + "?access_token=" + sessionStorage.getItem("TargetMemberAccessToken");
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok(_memberId == data.data.memberId, "fetched MemberA's mini profile for MemberB. username:" + data.data.username + " IsOnline:" + data.data.isOnline);
        });
    });

    asyncTest("[get AttributeSetFullProfile] MemberA fetches MemberB mini profile", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/profile/attributeset/fullProfile/" + _targetMemberId + "?access_token=" + sessionStorage.getItem("AccessToken");
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok(_targetMemberId == data.data.memberId, "fetched MemberB's full profile for MemberA. username:" + data.data.username + " IsOnline:" + data.data.isOnline);
        });
    });

    asyncTest("[put & get UpdateDisplaySettings] update MemberA MOL display setting to show", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/profile/displaysettings?access_token=" + sessionStorage.getItem("AccessToken");
        var requestData = { "online": true };
        ajaxHelper(url, 'PUT', requestData, null, true);
        url = _apiUrl + "/brandId/" + _brandId + "/profile/displaysettings?access_token=" + sessionStorage.getItem("AccessToken");
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok(data.data.Online, "updated and fetched display settings. show online value:" + data.data.Online);
        });
    });


    asyncTest("[get AttributeSetForVisitor] visitor fetches MemberB mini profile", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/visitorprofile/" + _targetMemberId + "?client_secret=" + _clientSecret + "&applicationId=" + _appId;
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok(data.data.username != null, "fetched MemberB's profile for visitor. username:" + data.data.username + ", HeightCm:" + data.data.heightCm + ", AboutMe:" + data.data.aboutMe);
        });
    });

    asyncTest("[get LookupAttributeSetByMemberId] MemberA looks up by MemberB Id", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/profile/lookupByMemberId/miniprofile/" + _targetMemberId + "?access_token=" + sessionStorage.getItem("AccessToken");
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok(data.data.username != null, "fetched MemberB's miniprofile. username:" + data.data.username + ", HeightCm:" + data.data.heightCm + ", AboutMe:" + data.data.aboutMe);
        });
    });

    asyncTest("[put UpdateMemberAttributes] MemberA updates attributes", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/profile/attributes/" + "?access_token=" + sessionStorage.getItem("AccessToken");
        var attributesMultiValueJson = {
        };
        var attributesJson = {
            SODBFirstName: "won",
            SODBLastName: "lee"
        };
        attributesMultiValueJson = JSON.stringify(attributesMultiValueJson);
        attributesJson = JSON.stringify(attributesJson);
        var requestData = {
            AttributesMultiValueJson: attributesMultiValueJson,
            AttributesJson: attributesJson
        };
        ajaxHelper(url, 'PUT', requestData, null).success(function(data) {
        });
    });
}
//#region Profile

//#region Photo

asyncTest("[get Photos] MemberA photos metadata", function() {
    var url = _apiUrl + "/brandId/" + _brandId + "/profile/photos/" + _memberId + "?access_token=" + sessionStorage.getItem("AccessToken");
    ajaxHelper(url, 'GET', null, null).success(function(data) {
        ok(data.data.length >= 0, "fetched MemberB's photos. count:" + data.data.length);
    });
});

asyncTest("[get & put Photos] MemberA photos metadata", function() {
    var url = _apiUrl + "/brandId/" + _brandId + "/profile/photos/" + _memberId + "?access_token=" + sessionStorage.getItem("AccessToken");
    var memberPhotoId;
    var listOrderOfPhotoToMarkMainWithZeroIndex = 2;
    stop();
    ajaxHelper(url, 'GET', null, 5000)
        .then(function(data) {
            ok(data.data.length >= 2, "fetched MemberA's photos. count:" + data.data.length);
            memberPhotoId = data.data[listOrderOfPhotoToMarkMainWithZeroIndex].memberPhotoId;
            ok(memberPhotoId != null && memberPhotoId >= 0,
                " current photo memberPhotoId:" + data.data[0].memberPhotoId
                + " current photo listOrder:" + data.data[0].listorder
                + " new main photo memberPhotoId:" + memberPhotoId
                + " new main listOrder:" + data.data[listOrderOfPhotoToMarkMainWithZeroIndex].listorder);
        }).then(function() {
            stop();
            var currentdate = new Date();
            var datetime = currentdate.getMonth() + "/" + (currentdate.getDate() + 1) + "/" + currentdate.getFullYear() + " "
                + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
            var caption = "I am main now " + datetime;
            url = _apiUrl + "/brandId/" + _brandId + "/profile/photos/" + memberPhotoId + "?access_token=" + sessionStorage.getItem("AccessToken");
            var requestData = { "IsMain": true, "Caption": caption, "ListOrder": 1 };
            return ajaxHelper(url, 'PUT', requestData, 5000).then(function(data) {
                ok(data.data.PhotoId == memberPhotoId,
                    "updated MemberA's second photo as main. Response photoId:" + data.data.PhotoId);
            });
        }).then(function() {
            url = _apiUrl + "/brandId/" + _brandId + "/profile/photos/" + _memberId + "?access_token=" + sessionStorage.getItem("AccessToken");
            return ajaxHelper(url, 'GET', null, 5000).then(function(data) {
                ok(data.data.length >= 2, "fetched MemberA's photos second time. count:" + data.data.length);
                ok(data.data[0].memberPhotoId == memberPhotoId, "firstMemberPhotoId:" + data.data[0].memberPhotoId + " markedMainId:" + memberPhotoId
                    + " old MainMemberPhotoId:" + data.data[listOrderOfPhotoToMarkMainWithZeroIndex].memberPhotoId
                    + " old Main ListOrder:" + data.data[listOrderOfPhotoToMarkMainWithZeroIndex].listorder);
            });
        });
});

//#region Photo

//#region IM
{
    module("Instant Messenger");

    asyncTest("[post AddInvite] MemberB invites MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/instantmessenger/invite?access_token=" + sessionStorage.getItem("TargetMemberAccessToken");
        var requestData = { "recipientMemberId": _memberId };
        ajaxHelper(url, 'POST', requestData, null).success(function(data) {
            ok((data.data.ConversationTokenId !== ""), "added a new IM invite for memberId:" + _targetMemberId +
                "tokenId:" + data.data.ConversationTokenId);
        });

    });

    asyncTest("[get GetInvites] for MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/instantmessenger/invites?access_token=" + sessionStorage.getItem("AccessToken");
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok((data.data.length > 0), "retrieved invites total: " + data.data.length + " conversationKey: " + sessionStorage.getItem("ConversationKey"));
            sessionStorage.setItem("ConversationKey", data.data[0].ConversationKey);
        });
    });

    asyncTest("[remove RemoveInvite] from MemberB for MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/instantmessenger/invite?access_token=" + sessionStorage.getItem("AccessToken");
        var requestData = { "ConversationKey": sessionStorage.getItem("ConversationKey") };
        ajaxHelper(url, 'DELETE', requestData, null);
    });

    asyncTest("[post SendMissedIM] send missed IM to MemberA from MemberB", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/instantmessenger/missedIM?access_token=" + sessionStorage.getItem("TargetMemberAccessToken");
        var messages = ["First message", "Second message", "Third message"];
        var requestData = { "recipientMemberId": _memberId, "Messages": messages };
        ajaxHelper(url, 'POST', requestData, null).success(function(data) {
            ok(true, "please check MemberA's mailbox. need addtional work to test the inbox.");
        });
    });

    asyncTest("[get CanMemberReply] for MemberA recieing IM from MemberB", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/instantmessenger/canmemberreply/" + _targetMemberId + "?access_token=" + sessionStorage.getItem("AccessToken");
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok(data.data, "canMemberReply: " + data.data);
        });
    });

    asyncTest("[save IM message] IMs to be saved in db", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/application/" + _appId + "/instantmessenger/saveIM?client_secret=" + _clientSecret;;
        var requestData = {
            "memberId": _memberId,
            "ToMemberId": _targetMemberId,
            "ThreadId": "183jafnjhkas8382937hkjh897hk",
            "MessageSubject": "a_subject",
            "MessageBody": htmlEncode('hey there, check out this pic <photo id=\'sparktag\' src=\'http://www.image.com\' />'),
            "MessageDateTime": "2013-11-22T02:25:00Z"
        };
        ajaxHelper(url, 'POST', requestData, null).success(function(data) {
            ok(data.data.SendStatus == "Success", "please check MemberB's mailbox. need additional work to test the inbox. sendStatus:" + data.data.SendStatus);
        });
    });

}
//#region IM

//#region List
{
    module("HotList");

    asyncTest("[get hotlist counts for MemberA]", 2, function () {
        var url = _mainApiUrl + "/hotlist/counts?access_token=" + sessionStorage.getItem("AccessToken");
        var customHeader = { 'accept': 'application/json; version=V2.1' };
        ajaxHelper(url, 'GET', null, customHeader).success(function (data) {
            ok(data.data.TeasedCombinedTotal > 0, "retrieved TeasedCombinedTotal counts:" + data.data.TeasedCombinedTotal);
        });
    });

    asyncTest("[get hotlists for MemberA]", 2, function() {
        var url = _mainApiUrl + "/hotlist/default/pagesize/5/pagenumber/1?access_token=" + sessionStorage.getItem("AccessToken");
        var customHeader = { 'accept': 'application/json; version=V2.1' };
        ajaxHelper(url, 'GET', null, customHeader).success(function (data) {
            ok(data.data.length > 0, "retrieved hotlist. count:" + data.data.length);
        });
    });

    asyncTest("[get FavoritesCombined custom hotlist for MemberA]", 2, function () {
        var url = _mainApiUrl + "/hotlist/FavoritesCombined/pagesize/10/pagenumber/2?access_token=" + sessionStorage.getItem("AccessToken");
        var customHeader = { 'accept': 'application/json; version=V2.1' };
        ajaxHelper(url, 'GET', null, customHeader).success(function (data) {
            ok(data.data.length > 0, "retrieved hotlist. count:" + data.data.length);
        });
    });

    asyncTest("[post delete to hotlist]", 1, function() {
        var url = _mainApiUrl + "/hotlist/default/targetmemberid/" + _targetMemberId + "?access_token=" + sessionStorage.getItem("AccessToken");
        var requestData = { "comment": "qunit test comment", "method": "delete" };
        ajaxHelper(url, 'POST', requestData, null);
    });

    asyncTest("[post add to hotlist]", 1, function() {
        var url = _mainApiUrl + "/hotlist/default/targetmemberid/" + _targetMemberId + "?access_token=" + sessionStorage.getItem("AccessToken");
        var requestData = { "comment": "qunit test comment", "method": "add" };
        ajaxHelper(url, 'POST', requestData, null);
    });
}
{
    module("HotList - YNM");

    asyncTest("[post SaveYNMVote] vote yes on MemberB for MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/hotlist/ynmvote/?access_token=" + sessionStorage.getItem("AccessToken");
        var requestData = { "Yes": true, "AddMemberId": _targetMemberId };
        ajaxHelper(url, 'POST', requestData, null).success(function(data) {
            ok(true, "voted yes on MemberB for MemberA.");
        });
    });

    asyncTest("[get GetYNMVote] retrieve ynm vote on MemberB for MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/hotlist/ynmvote/targetMemberId/" + _targetMemberId + "/?access_token=" + sessionStorage.getItem("AccessToken");
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok((data.data.Yes = true), "retrieved ynm vote on MemberB for MemberA. yes:" + data.data.Yes + " no:" + data.data.No + " maybe:" + data.data.Maybe);
        });
    });

}
//#region List

//#regionUserNotifications
{
    module("UserNotifications");

    asyncTest("[get user notifications] get user notifications for MemberA", function () {
        var url = _apiUrl + "/brandId/" + _brandId + "/startNum/0/pageSize/25/usernotifications?applicationId=998&access_token=" + sessionStorage.getItem("AccessToken");
        ajaxHelper(url, 'GET', null, null).success(function (data) {
            ok(data.data.length >= 0, "retrieved " + data.data.length + " notifications for MemberA.");
        });
    });

    asyncTest("[add user notifications] Add user notification with data points for MemberA from MemberB", function () {
        var url = _apiUrl + "/brandId/" + _brandId + "/profile/attributeset/fullProfile/" + _memberId + "?access_token=" + sessionStorage.getItem("TargetMemberAccessToken");
        ajaxHelper(url, 'GET', null, null, true);

        var url = _apiUrl + "/brandId/" + _brandId + "/startNum/0/pageSize/25/usernotifications?applicationId=998&access_token=" + sessionStorage.getItem("AccessToken");
        ajaxHelper(url, 'GET', null, null).success(function (data) {
            ok(data.data.length > 0 && data.data[0].DataPoints["CREATOR_USER_NAME"] != null, "retrieved " + data.data.length + " notifications with data points for MemberA.");
        });
    });
}

//#regionUserNotifications

//#region MemberSlideShow
{
    module("MemberSlideShow");
    asyncTest("[get MemberSlideShow with Prefs] retrieve memberslideshow with prefs for MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/memberslideshow/slideshow/?access_token=" + sessionStorage.getItem("AccessToken")
            + "&gender=male&seekinggender=female&minage=18&maxage=88&location=90210&maxdistance=160";
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok((data.data.MemberId > 0), "retrieved memberslideshow for MemberA. memberId:" + data.data.MemberId);
        });
    });

    asyncTest("[get MemberSlideShowMiniProfile with Prefs] retrieve memberslideshowminiprofile  with prefs for MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/memberslideshow/slideshow/miniprofile?access_token=" + sessionStorage.getItem("AccessToken")
            + "&gender=male&seekinggender=female&minage=18&maxage=88&location=90210&maxdistance=160";
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok((data.data.MemberId > 0), "retrieved memberslideshow with prefs for MemberA. memberId:" + data.data.MemberId);
        });
    });

    asyncTest("[get MemberSlideShowMembers with Prefs] retrieve memberslideshowminiprofile  with prefs for MemberB", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/memberslideshow/members?access_token=" + sessionStorage.getItem("TargetMemberAccessToken")
            + "&gender=male&seekinggender=female&minage=18&maxage=99&location=90210&maxdistance=1000&isnewprefs=true&batchsize=3";
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok((data.data.SlideshowMembers.length >= 0), "retrieved slideshowmembers back. count:" + data.data.SlideshowMembers.length);
        });
    });
}
//#region MemberSlideShow

//#region MembersOnline
{
    module("MembersOnline");

    asyncTest("[get MembersOnlineCount] for MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/membersonline/count/?access_token=" + sessionStorage.getItem("AccessToken");
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok((data.data.count >= 0), "retrieved mol(session) count:" + data.data.count);
        });
    });

    asyncTest("[get MembersOnline] retrieve mol list for MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/membersonline/list/?access_token=" + sessionStorage.getItem("AccessToken")
            + "&SortType=2&PageSize=15&PageNumber=1&MinAge=22&MaxAge=99&LanguageId=2&SeekingGender=Female";
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok((data.data.Members.length >= 0), "retrieved mol list for MemberA. count:" + data.data.Members.length);
        });
    });

    asyncTest("[post AddMembersOnline] retrieve mol list for MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/addmembersonline/?access_token=" + sessionStorage.getItem("AccessToken");
        ajaxHelper(url, 'POST', null, null).success(function(data) {
            ok(true, "added MemberA to mol");
        });
    });
}
//#region MembersOnline

//#region Search
{
    module("Search");

    asyncTest("[get search prefs", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/match/preferences/?access_token=" + sessionStorage.getItem("TargetMemberAccessToken");
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok((data.data.minAge > 0), "retrieved search preferences for MemberB. MinAge:" + data.data.minAge);
        });
    });

    asyncTest("[put search prefs] update MemberB search prefs", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/match/preferences?access_token=" + sessionStorage.getItem("TargetMemberAccessToken");
        // there's a known bug with deserrializing dictionaries in asp.net, fixed in mvc 4.
        // http://stackoverflow.com/questions/4710729/post-json-dictionary
        var advancedPreferencesJson = {
            JDateReligion: [
                "2", "256", "8", "16", "256", "512", "16384"
            ],
            JDateEthnicity: [
                "8", "1"
            ]
        };
        advancedPreferencesJson = JSON.stringify(advancedPreferencesJson);
        var requestData = {
            gender: "male",
            seekingGender: "female",
            minAge: "22",
            maxAge: "55",
            isNewPrefs: "true",
            maxDistance: 2000,
            ShowOnlyMembersWithPhotos: "false",
            minHeight: 137,
            maxHeight: 244,
            AdvancedPreferencesJson: advancedPreferencesJson
        };
        ajaxHelper(url, 'PUT', requestData, null).success(function(data) {
            ok(true, "please check MemberB's search prefs.");
        });
    });

    asyncTest("[post search] search results for MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/search/results?access_token=" + sessionStorage.getItem("AccessToken");
        var advancedPreferencesJson = {
            JDateReligion: [
                "2", "256", "8", "16", "256", "512", "16384"
            ],
            JDateEthnicity: [
                "8", "1"
            ]
        };
        advancedPreferencesJson = JSON.stringify(advancedPreferencesJson);
        var requestData = {
            "pageSize": 50,
            "pageNumber": 1,
            "showOnlyJewishMembers": false,
            "gender": "male",
            "seekingGender": "female",
            "minAge": 20,
            "maxAge": 55,
            "location": "3443821",
            "maxDistance": 50,
            "showOnlyMembersWithPhotos": false,
            "MinHeight": 0,
            "MaxHeight": 240,
            AdvancedPreferencesJson: advancedPreferencesJson
        };
        var customHeader = { 'accept': 'application/json; version=V2.1' };
        ajaxHelper(url, 'POST', requestData, customHeader).success(function(data) {
            ok(data.data.Members.length >= 0, "retrieved " + data.data.Members.length + " search results for MemberA.");
        });
    });

    asyncTest("[get visitor search via post] get search results for visitor", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/visitorsearch/results?client_secret=" + _clientSecret + "&applicationId=" + _appId;
        var requestData = { "pageSize": 50, "pageNumber": 1, "showOnlyJewishMembers": false, "genderMask": 6, "regionId": 3443821, "maxDistance": 50, "minAge": 20, "maxAge": 55, "showOnlyMembersWithPhotos": false };
        ajaxHelper(url, 'POST', requestData, null).success(function(data) {
            ok(data.data.Members.length >= 0, "retrieved " + data.data.Members.length + " search results for MemberA.");
        });
    });

    asyncTest("[get secretadmirer search via post] get secret admirer search for MemberA", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/search/secretadmirer?access_token=" + sessionStorage.getItem("AccessToken");
        var requestData = { "pageSize": 50, "pageNumber": 1, "showOnlyJewishMembers": false, "genderMask": 6, "MinAge": 18, "MaxAge": 99 };
        ajaxHelper(url, 'POST', requestData, null).success(function(data) {
            ok(data.data.Members.length >= 0, "retrieved " + data.data.Members.length + " secret admirer search results for MemberA.");
        });
    });

    // it always fails..
    //module("Mingle Search");
    //test("[get mingle search via post] get mingle search results", function() {
    //    var jsonObj = '{"seekingGender": 1,"minAge": 20,"maxAge": 75,"minHeight": 48,"maxHeight": 72,"radiansLat": 0.594637,"radiansLng": -2.066426,"radiusInMiles": 100,"hasPhoto": false,"ethnicity": 3071,"bodyType": 255,"education": 1023,"religion": 16776703,"smoke": 15,"drink": 31,"maritalStatus": 12,"sortType": "LastLogonDate","resultType": "DetailedResult","maxResults": 50}';
    //    var _mingleBrandId = 90810;
    //    var url = _apiUrl + "/brandId/" + _mingleBrandId + "/minglesearch/results?client_secret=" + _clientSecret + "&applicationId=" + _appId;
    //    console.log(url);
    //    var response;

    //    $.ajax({
    //        type: 'POST',
    //        url: url,
    //        data: jsonObj,
    //        success: function(data) {
    //            response = data.data;
    //        },
    //        error: function(data) {
    //            response = data.data;
    //        },
    //        dataType: "json",
    //        contentType: "application/json",
    //        async: true,
    //        complete: function(e, xhr, settings) {
    //            _statusCode = e.status;
    //            start();
    //        }
    //    });

    //    ok(((_statusCode == "200")), "retrieved " + response.Members.length + " mingle search results.");
    //});
}
//#region Search

//#region Subscription
{
    module("Subscription");

    asyncTest("[get IsMobileAppSubscriber] find out if Member A has mobile app subs", function () {
        // check life time access
        url = _apiUrl + "/brandId/" + _brandId + "/subscription/ismobileappsubscriber/" + _memberId + "/?client_secret=" + _clientSecret + "&applicationId=" + _appId;
        ajaxHelper(url, 'GET', null, null).success(function (data) {
            ok(data.data != null, "Retrieved response" + data.data);
        });
    });

    asyncTest("[put & get LifeTimeAccess] grant life time access for MemberA and check by renewaldate", function() {
        // grant life time access
        var url = _apiUrl + "/brandId/" + _brandId + "/subscription/lifetimeaccess?client_secret=" + _clientSecret + "&applicationId=" + _appId;
        var requestData = { "memberId": _targetMemberId, "adminmemberId": _memberId };
        ajaxHelper(url, 'PUT', requestData, null);

        // check life time access
        url = _apiUrl + "/brandId/" + _brandId + "/subscription/lifetimeaccess/" + _targetMemberId + "?client_secret=" + _clientSecret + "&applicationId=" + _appId;
        ajaxHelper(url, 'GET', requestData, null).success(function(data) {
            ok(data.data.RenewalDate != null, "MemberB's life time access renewal date:." + data.data.RenewalDate);
        });
    });

    asyncTest("[get AccessPrivileges] get a list of all access privileges for MemberA", function() {
        // check life time access
        url = _apiUrl + "/brandId/" + _brandId + "/subscription/accessprivileges/?access_token=" + sessionStorage.getItem("AccessToken");
        ajaxHelper(url, 'GET', null, null).success(function(data) {
            ok(data.data.length >= 1, "Retrieved MemberA access privileges total:" + data.data.length);
        });
    });
}
//#regionSubscription

//#regionMail
{
    module("Mail");

    asyncTest("[get mailfolder] get Member A's inbox", function() {
        var url = _apiUrl + "/brandId/" + _brandId + "/mail/folder/1/pagesize/4/page/1?access_token=" + sessionStorage.getItem("AccessToken");
        ajaxHelper(url, 'GET', null, null, true).success(function(data) {
            ok(data.data.messageList.length > 0, "retrieved message. count:" + data.data.messageList.length);
            start();
        });
    });

    // edge case, turn on for troubleshooting.
    //asyncTest("[post sendtease] send tease twice from MemberA to MemberB", function() {
    //    var url = _apiUrl + "/brandId/" + _brandId + "/mail/tease?access_token=" + sessionStorage.getItem("AccessToken");
    //    //TEASE_CATEGORY_HUMOROUS - 46
    //    // TEASE_HUMOROUS_REALITY - 46
    //    var requestData = { "RecipientMemberId": _targetMemberId, "TeaseId": 46, "TeaseCategoryId": 46, "Body": "test body humorous reality" };
    //    ajaxHelper(url, 'POST', requestData, null, true).complete(function(e, textStatus) {
    //        // sending double tease test cannot be consistent if it's been called before or elsewhere
    //        // by checking for 400 on the first call we can at least notify the tester
    //        // this .complete is fired after the other .complete call finishes in the helper method
    //        if (e.status != "400") {
    //            ajaxHelper(url, 'POST', requestData, null, false, "400");
    //        };
    //        ok(true, "received 400 on the first call. not a legit test");
    //        start();
    //    });
    //});

}
//#regionMail

function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    return $('<div/>').text(value).html();
}

// todo: better to use always, done, and error. 
// success, complete and fail are essentially deprecated according to jquery docs.
function ajaxHelper(requestUrl, httpVerbType, requestData, customHeaders, doNotTriggerStart, expectedStatusCode) {
    return $.ajax({
        url: requestUrl,
        data: JSON.stringify(requestData),
        type: httpVerbType,
        headers: customHeaders,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        timeOut: 5000 // tried 1 sec, too short for some tests
    }).complete(function(e, textStatus) {
        var statusCodeToMatch = "200"; // most tests are looking for 200 OK
        if (expectedStatusCode != null && !isNaN(parseInt(expectedStatusCode))) statusCodeToMatch = expectedStatusCode;
        ok(e.status == statusCodeToMatch, "received status code: " + e.status + " " + textStatus);
    }).always(function() {
        if (!doNotTriggerStart) start();
    });
};