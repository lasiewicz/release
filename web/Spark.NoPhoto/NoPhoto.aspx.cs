using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

namespace Spark.NoPhoto
{
	/// <summary>
	/// DEPLOYMENT:  This application is to run only on servers that serve photos.  When deploying to 
	/// DEV, use the Deploy_dev.bat file.  When deploying to PROD, this must be deployed to all FileXX 
	/// servers and it must reside under the main website.  The custom errors must then be set so that 
	/// 404 errors go to this page.  See BHDEVWEB01 for an idea of how it should be configured.  Also, 
	/// there must be a subdirectory called "NoPhotoImgs" that contains all the NoPhoto images in the form:  
	/// "NoPhoto_XXXX.gif" where XXXX is the siteID (i.e. for AS, this photo must be called NoPhoto_101.gif. 
	/// There must be a NoPhoto image for every site or it will default to the AS no photo image. 
	/// </summary>
	public class NoPhoto : System.Web.UI.Page
	{
		private const String QSPARAM_SITEID = "siteID";
		private const String DEFAULT_SITEID = "101";
		private readonly static Regex isNumericRegex = new Regex(@"^\d+$");

		private void Page_Load(object sender, System.EventArgs e)
		{
			String siteID = DEFAULT_SITEID;

			// The RawURL will be in the format:  /Spark.NoPhoto/NoPhoto.aspx?404;http://dev.americansingles.com:80/fileroot1/2004/06/10/13/30142.jpg?siteID=102
			// Need to parse out QSPARAM_SITEID (siteID).
			String rawURL = Request.RawUrl;

			Int32 startIndex = rawURL.IndexOf(QSPARAM_SITEID);
				
			if (startIndex != -1)
			{	
				Int32 endIndex = rawURL.IndexOf("&", startIndex);
				startIndex += QSPARAM_SITEID.Length + 1;
	
				if (endIndex == -1)
					siteID = rawURL.Substring(startIndex);
				else
					siteID = rawURL.Substring(startIndex, endIndex);
			}

			using (System.Drawing.Image img = LoadNoPhotoImage(siteID))
			{
				Response.ContentType = "image/gif";
		
				img.Save(Response.OutputStream, ImageFormat.Gif);
			}
		}

		private System.Drawing.Image LoadNoPhotoImage(String siteID)
		{
			Bitmap bmp;
			String imgPath = Server.MapPath("NoPhotoImgs/NoPhoto_" + siteID + ".gif");
  
			try
			{
				bmp = new Bitmap(imgPath);
			}
			catch
			{
				imgPath = Server.MapPath("NoPhotoImgs/NoPhoto_" + DEFAULT_SITEID + ".gif");

				bmp = new Bitmap(imgPath);
			}
  
			return bmp;
		}

		private static Boolean IsNumeric(String value)
		{
			Match match = isNumericRegex.Match(value);

			return match.Success;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
