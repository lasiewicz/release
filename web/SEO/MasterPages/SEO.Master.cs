using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Spark.Web.Framework;

namespace Spark.Web.MasterPages
{
   
    public partial class SEO : System.Web.UI.MasterPage
    {
        private ContextGlobal _g= new ContextGlobal();

        public ContextGlobal GContext
        {
            get { return _g; }
            set { _g = value; }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            _BreadCrumb.RootUrl = "http://www." + GContext.Brand.Uri;
            _Omniture.PageName = _g.OmniturePageName;
        }

    }
}
