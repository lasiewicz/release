<%@ Page Language="C#"  MasterPageFile="~/MasterPages/SEO.master" AutoEventWireup="true" CodeBehind="SEO.aspx.cs" Inherits="Spark.Web.Applications.SEO.SEO" %>
<%@ Register Src="FeaturedMembers.ascx" TagName="FeaturedMembers" TagPrefix="SparkApp" %>
<%@ Register Src="ListSEORegions.ascx" TagName="ListSEORegions" TagPrefix="SparkApp" %>
<%@ Register Src="~/Ui/Footer.ascx" TagName="Footer" TagPrefix="SparkUI" %>
<%@ Register Src="~/Ui/Header.ascx" TagName="Header" TagPrefix="SparkUI" %>
<%@ Register Src="~/Ui/LeftNav.ascx" TagName="LeftNav" TagPrefix="SparkUI" %>

<asp:Content ID="cntHeader" ContentPlaceHolderID="cntMasterHeader" Runat="Server">
    <SparkUI:Header id="_Header" runat="server" >
    </SparkUI:Header>
</asp:Content>

<asp:Content ID="cntLeftNav" ContentPlaceHolderID="cntMasterLeftNav" Runat="Server">
    <SparkUI:LeftNav id="_LeftNav" runat="server">
    </SparkUI:LeftNav>
</asp:Content>

<asp:Content ID="cntPageContent" ContentPlaceHolderID="cntMasterPageContent" Runat="Server">
<h1><asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
<p><asp:Literal runat="server" ID="_litSEOText" ></asp:Literal></p>
<asp:PlaceHolder ID="plchMain" runat="server"  Visible=False/>
<asp:PlaceHolder ID="plchState" runat="server"  Visible=False/>    
<asp:PlaceHolder ID="plchCity" runat="server"  Visible=False/>
<asp:PlaceHolder ID="plchDataCA" runat="server" Visible="false" />
</asp:Content>

<asp:Content ID="cntFooter" ContentPlaceHolderID="cntMasterFooter" Runat="Server"  >
    <SparkUI:Footer id="_Footer" runat="server">
    </SparkUI:Footer>
</asp:Content>

