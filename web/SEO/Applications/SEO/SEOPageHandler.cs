using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
namespace Spark.Web.Applications.SEO
{
    #region Enumerations
    public enum LayoutColumns : int { One = 1, Two = 2 }
    public enum SuffixType : int { Fixed = 1, State = 2 }
    public enum DisplayMode { State = 0, City = 1 };
    public enum SEOPages { Main = 0, State = 1, City = 2 };
    #endregion
    public class SEOPageHelper
    {
        /// <summary>
        /// validates the input parameter against Content.Region middle tier 
        /// </summary>
        /// <param name="strState"></param>
        /// <returns></returns>
        static public SEORegion ValidateStateCity(string strState,string strCity)
        {

            SEORegion objResult = null;
            SEORegionCollection states = RegionSA.Instance.GetSEOUSStates();
            
            Boolean blnCheckCity = (strCity!= string.Empty);
            try
            {

                if (strState != string.Empty)
                {
                    foreach (SEORegion state in states)
                    {
                        if ((state.Description.ToLower()).Replace(" ", "").Replace(".", "").Equals(strState))
                        {
                            objResult = state;
                            if (blnCheckCity)
                            {
                                SEORegionCollection stateCities = RegionSA.Instance.GetSEOCitiesInState(state.RegionID);
                                foreach (SEORegion city in stateCities)
                                {
                                    if ((city.Description.ToLower()).Replace(" ", "").Equals(strCity))
                                    {
                                        objResult = city;
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw (ex);
            }
            return objResult;
        }        


    }
}
