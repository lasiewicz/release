using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Spark.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;


namespace Spark.Web.Applications.SEO
{
    public partial class MiniProfile : Spark.Web.Framework.FrameworkControl
    {
        private Member _member=null;
        private DisplayMode _displayMode = DisplayMode.State; // 0 for state 1 for city         
        private string _memberProfLink = string.Empty;
        private string _memberImageLink = string.Empty;
        private const int  MAXSTRINGLENGTH=60;

        public DisplayMode Mode
        {
            get { return _displayMode; }
            set { _displayMode = value; }
        }
        public Member Member
        {
            get { return _member; }
            set { _member = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Brand brand = ((Spark.Web.MasterPages.SEO)this.Page.Master).GContext.Brand;
            SetUrls(brand);
            if (_member != null)
            {
                if (_displayMode == DisplayMode.State)
                {
                    // Set User name
                    
                    this._stateUserName.Text = Ellipsis(_member.Username, 17);
                    this._stateUserName.NavigateUrl = _memberProfLink.Replace("<MID>", _member.MemberID.ToString());
                    //  Set Photo
                    SetThumb(this._UserPicState, _memberProfLink.Replace("<MID>", _member.MemberID.ToString()), brand);
                    // Set photo link to user profile
                    this._lnkUserImg.HRef = _memberProfLink.Replace("<MID>", _member.MemberID.ToString());
                    this.plchStateDetail.Visible = true;

                }else
                if (_displayMode == DisplayMode.City)
                {
                  
                    // Set User name
                    this._userName.Text = Ellipsis(_member.Username, 17);
                    this._userName.NavigateUrl = _memberProfLink.Replace("<MID>", _member.MemberID.ToString());

                    // Set Age
                    DateTime birthDate = _member.GetAttributeDate(brand, "BirthDate", DateTime.MinValue);
                    this._age.Text = GetAgeCaption(birthDate);

                    // Set about me
                    AboutMe(this._AboutMe, brand, _member.MemberID, 260, 20);
                    //  Set Photo
                    SetThumb (this._UserPicCity,_memberProfLink.Replace("<MID>", _member.MemberID.ToString()),brand);
                    // Set photo link to user profile
                    this._lnkUserImgCity.HRef = _memberProfLink.Replace("<MID>", _member.MemberID.ToString());
                    // set view profile
                    this._lnkvwProfile.NavigateUrl = _memberProfLink.Replace("<MID>", _member.MemberID.ToString());                  
                    this.plchCityDetail.Visible = true;
                }
            }
        }

        private void SetUrls(Brand brand)
        {
            _memberProfLink = "http://www." + brand.Uri + "/Applications/MemberProfile/ViewProfile.aspx?p=7070&MemberID=<MID>&prm=26854";
            _memberImageLink = "http://" + brand.Uri;
        }

        private void SetThumb(System.Web.UI.HtmlControls.HtmlImage imgThumb, string thumbDestinationUrl, Brand brand)
		{
			//imgThumb.Attributes["onclick"]=thumbDestinationUrl;
            bool noPhoto = false;

			Photo photo = MemberPhotoHelper.GetPhoto(_member, brand);
            if (!_member.GetAttributeBool(brand, WebConstants.ATTRIBUTE_NAME_DISPLAYPHOTOSTOGUESTS) ) 
            {
                // Only show photo to guests and not logged in
                //noPhoto.DestURL = imgThumb.NavigateUrl;
                //noPhoto.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto.PhotoMode.OnlyMembers;
                noPhoto = true;
            }


            if (!noPhoto && photo != null && photo.IsApproved)
            {
                if (MemberPhotoHelper.IsPrivatePhoto(photo, brand))
                {
                    // Private photo and Cupid domain
                    //	noPhoto.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto.PhotoMode.NoPrivatePhoto;
                    noPhoto = true;
                }

                if (!noPhoto && photo.ThumbFileWebPath != null)
                {
                    // This must be appended to all photos that hit the file servers in order for the site-specific
                    // "Temporarily Unavailable" images to appear.  Appending this to any other photos should not cause errors. 
                    /// (refactored QSPARAM_SITEID)
                    String siteIDParam = "?" + WebConstants.URL_PARAMETER_NAME_SITEID + "=" + brand.Site.SiteID.ToString();
                    imgThumb.Src = _memberImageLink + photo.ThumbFileWebPath + siteIDParam;
                }
                else
                    noPhoto = true;
            }
            else
                noPhoto = true;

            if (noPhoto)
                imgThumb.Src = g.GetResource(this.Page, "NO_PHOTO_SRC", true);                  
		}

        private void AboutMe(Literal txtAboutMe, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, int MemberID, int MAXSTRINGLENGTH, int MAXWORDLENGTH)
        {
            string Headline = Ellipsis(this._member.GetAttributeTextApproved(brand, "AboutMe", MemberID, "Not Approved"), MAXSTRINGLENGTH);

            if (Headline != String.Empty && Headline != null)
            {
                char[] delimiters = { ' ' };
                string[] contents = Headline.Split(delimiters);

                bool longWordFound = false;

                for (int i = 0; i < contents.Length; i++)
                {
                    if (contents[i].Length > MAXWORDLENGTH)
                    {
                        for (int j = 1; j <= contents[i].Length / MAXWORDLENGTH; j++)
                        {
                            if (contents[i].Length * j > MAXWORDLENGTH)
                            {
                                contents[i] = contents[i].Insert(MAXWORDLENGTH * j, " ");
                            }
                        }
                        longWordFound = true;
                    }
                }

                if (longWordFound)
                {
                    Headline = String.Join(" ", contents);
                }
            }

            txtAboutMe.Text = Headline;

        }
        private string GetAgeCaption(DateTime birthDate)
        {
            int age = GetAge(birthDate);

            if (age > 0)
            {
                return age.ToString();
            }
            else
            {
                return string.Empty; //g.GetResource(ageNotValidResourceKey, caller);
            }
        }
        private int GetAge(DateTime birthDate)
        {
            if (birthDate == DateTime.MinValue) return 0;
            return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
        }
        private string Ellipsis(string currentValue, int maxLength)
        {
            if (currentValue != null && currentValue.Length > maxLength)
            {
                currentValue = currentValue.Substring(0, maxLength - 3) + "...";
            }

            return currentValue;
        }
    }
}