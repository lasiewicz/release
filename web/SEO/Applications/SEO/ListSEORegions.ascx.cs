using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;

namespace Spark.Web.Applications.SEO          
{
   

    public partial class ListSEORegions : Spark.Web.Framework.FrameworkControl
    {


        RepeaterItemEventHandler _itemDataBound;
        
        private LayoutColumns _intColumns;
        private SuffixType _suffixType= SuffixType.Fixed;
        private SEORegionCollection _dataSource;
        private DisplayMode _linkType= DisplayMode.State;
        private string _prefix;
        private string _suffix;

        public DisplayMode LinkType
        {
            get { return _linkType; }
            set { _linkType = value; }
        }
        public string Title
        {
            get { return _title.Text; }
            set { _title.Text = value; }
        }
        public string Prefix
        {
            get { return _prefix; }
            set { _prefix = value; }
        }
        public string Suffix
        {
            get { return _suffix; }
            set { _suffix = value; }
        }
        public SEORegionCollection SEODataSource
        {
            get { return _dataSource; }
            set { _dataSource = value; }
        }
        public LayoutColumns Columns
        {
            get { return _intColumns; }
            set { _intColumns = value; }
        }
        /// <summary>
        /// This property sets/gets the display mode of each hyperlink in the list.
        /// <remarks>Fixed mode(default value): Suffix property of this control is appended to the Hyperlink text.
        ///          State mode: Uses state's abbreviation of the SEORegion object as the suffix and appends it to Hyperlink's text property.
        /// </remarks>
        /// </summary>
        public SuffixType SuffixMode
        {
            get { return _suffixType; }
            set { _suffixType = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SuffixMode== SuffixType.State)
            {
                _itemDataBound = rptrStateSuffix_ItemDataBound;
            }else 
            {
                _itemDataBound = rptrFixedSuffix_ItemDataBound;
            }

            if (Columns == LayoutColumns.One)
            {
                this._oneColumn.Visible = true;
                this._twoColumn.Visible = false;
                rptrOneColumn.ItemDataBound += new RepeaterItemEventHandler(_itemDataBound);
                rptrOneColumn.DataSource = SEODataSource;
                rptrOneColumn.DataBind();
            }
            else if (Columns == LayoutColumns.Two)
            {
                this._twoColumn.Visible = true;
                this._oneColumn.Visible = false;
                rptrTwoColumn.ItemDataBound += new RepeaterItemEventHandler(_itemDataBound);
                rptrTwoColumn.DataSource = SEODataSource;
                rptrTwoColumn.DataBind(); 
            }
            
        }     
        protected void rptrFixedSuffix_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Columns ==  LayoutColumns.One)
                {
                    HyperLink lnkItem = (HyperLink)e.Item.FindControl("lnkRegion");
                    lnkItem.NavigateUrl = SEOPageHelper.GetLink(Request.Url, ((SEORegion)e.Item.DataItem), _linkType);
                    lnkItem.Text = string.Format("{0}{1}{2}", Prefix, ((SEORegion)e.Item.DataItem).Description, Suffix);            
                }
                else if (Columns ==  LayoutColumns.Two)
                {
                    HyperLink lnkItem = (HyperLink)e.Item.FindControl("lnkRegion0");
                    // context.Request.Url.Host                    
                    lnkItem.NavigateUrl = SEOPageHelper.GetLink(Request.Url, ((SEORegion)e.Item.DataItem), _linkType);//string.Format("{0}{1}/{2}/", "http://", Request.Url.Authority, ((SEORegion)e.Item.DataItem).Description.Replace(" ", ""));
                    lnkItem.Text = string.Format("{0}{1}{2}", Prefix, ((SEORegion)e.Item.DataItem).Description, Suffix);
                }
            }
        }
        protected void rptrStateSuffix_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Columns ==  LayoutColumns.One)
                {
                    HyperLink lnkItem = (HyperLink)e.Item.FindControl("lnkRegion");
                    lnkItem.NavigateUrl = SEOPageHelper.GetLink(Request.Url, ((SEORegion)e.Item.DataItem), _linkType);// string.Format("{0}{1}/{2}/{3}/", "http://", Request.Url.Authority, ((SEORegion)e.Item.DataItem).ParentDescription.Replace(" ", ""), ((SEORegion)e.Item.DataItem).Description.Replace(" ", ""));
                    lnkItem.Text = string.Format("{0}, {1}", ((SEORegion)e.Item.DataItem).Description, ((SEORegion)e.Item.DataItem).ParentAbbrv);
                }
                else if (Columns ==  LayoutColumns.Two)
                {
                    HyperLink lnkItem = (HyperLink)e.Item.FindControl("lnkRegion0");
                    lnkItem.NavigateUrl = SEOPageHelper.GetLink(Request.Url, ((SEORegion)e.Item.DataItem), _linkType);// string.Format("{0}{1}/{2}/{3}/", "http://", Request.Url.Authority, ((SEORegion)e.Item.DataItem).ParentDescription.Replace(" ", ""), ((SEORegion)e.Item.DataItem).Description.Replace(" ", ""));
                    lnkItem.Text = string.Format("{0}, {1}", ((SEORegion)e.Item.DataItem).Description, ((SEORegion)e.Item.DataItem).ParentAbbrv);
                }
            }
        }
    }
}