using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Matchnet.Content.ValueObjects.Region;
using Matchnet.Content.ServiceAdapters;
using Spark.Web.Framework;

namespace Spark.Web.Applications.SEO
{
    public partial class SEO : System.Web.UI.Page
    {
        public string SiteUri
        {
            get
            {
                return g.Brand.Uri;
            }
        }

        private ContextGlobal g
        {
            get
            {
                return ((Spark.Web.MasterPages.SEO)this.Master).GContext;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SEOPageHandler();                       
        }
        /// <summary>
        /// This method initializes web page components (header, breadCrumb, leftNav,content and footer) for SEO pages (main, state and city)
        /// based on provided query strings.
        /// </summary>
        private void SEOPageHandler() 
        {
            string state = string.Empty,
                   city = string.Empty;

            // Recognize what page to display                                 C
            // Validate Query string parameters(Extract region object)        C
            // Set Head(title, decription, keywords)                          C
            // Set Left Nav                                                   C
            // Set Bread Crumb                                                C
            // Set SEO text                                                   C
            // Set content's placeholders                                     C
            // Set Footer                                                     C

            this.plchMain.Visible = false;
            this.plchState.Visible = false;
            this.plchCity.Visible = false;

            switch (Page.Request.QueryString.Count)
            {
                case 2:                     // processing city page
                    if (Page.Request.QueryString.AllKeys[0].ToLower().Equals("state") && Page.Request.QueryString.AllKeys[1].ToLower().Equals("city"))
                    {
                        state = Page.Request.QueryString[0].ToLower();
                        city = Page.Request.QueryString[1].ToLower();
                        // Extract SEO region based on the query string provided parameter
                        SEORegion seoCity = SEOPageHelper.ValidateStateCity(state, city);
                        if (seoCity != null)
                        {
                            if (seoCity.ParentAbbrv.Length > 0) // This means state and city both matched
                            {
                                DisplayPages(SEOPages.City, seoCity);   
                            }
                            else // This means only state had a match i.e. invalid city
                            {
                                // Display state page
                                return;
                            }
                        }
                        else  //invalid state/city in query string
                        {   // Display main page
                            DisplayPages(SEOPages.Main, null);
                            return;
                        }
                    }
                    break;
                case 1:                     // processing state page
                    if (Page.Request.QueryString.AllKeys[0].ToLower().Equals("state"))
                    {
                        state = Page.Request.QueryString[0].ToLower();
                        SEORegion seoState = SEOPageHelper.ValidateStateCity(state, "");
                        if (seoState != null)
                        {
                            DisplayPages( SEOPages.State, seoState);
                        }
                        else  //invalid state in query string
                        {
                            DisplayPages(SEOPages.Main, null);
                            return;
                        }
                    }
                    break;
                default:                // processing main page                                       
                    DisplayPages(SEOPages.Main,null);
                    break;
            }
        }
        private void DisplayPages(SEOPages seoPage, SEORegion seoRegion)
        {

            // Set Head(title, decription, keywords)                                                                                      
            InitHead(seoPage, seoRegion);
            // Set Bread Crumb    
            InitBreadCrumb(seoPage, seoRegion);
            // Set SEO text  
            InitSEOText(seoPage, seoRegion);

            switch (seoPage)
            {
                case SEOPages.City:
                    
                    // Set content's placeholders
                    this.plchCity.Visible = true;
                    // set regionID
                    FeaturedMembers objCityFeaturedMember = (FeaturedMembers)this.LoadControl("FeaturedMembers.ascx");
                    objCityFeaturedMember.RegionID = RegionSA.Instance.GetRandomZipCode(seoRegion.RegionID);
                    objCityFeaturedMember.RegionName = seoRegion.Description;
                    objCityFeaturedMember.Mode = DisplayMode.City;
                    plchCity.Controls.Add(objCityFeaturedMember);
                    break;
                case SEOPages.State:
                    
                    // Set content's placeholders  
                    this.plchState.Visible = true;
                    // set regionID
                    FeaturedMembers objStateFeaturedMember = (FeaturedMembers)this.LoadControl("FeaturedMembers.ascx");
                    objStateFeaturedMember.RegionID = RegionSA.Instance.GetSEOStatesPopularCity(seoRegion.RegionID);
                    objStateFeaturedMember.RegionName = seoRegion.Description;
                    objStateFeaturedMember.Mode = DisplayMode.State;
                    plchState.Controls.Add(objStateFeaturedMember);
                    // Top 20 cities in the state                                
                    ListSEORegions stateCities = (ListSEORegions)this.LoadControl("ListSEORegions.ascx");
                    stateCities.Columns = LayoutColumns.Two;
                    g.AddExpansionToken("state", seoRegion.Description);
                    stateCities.Title =  g.GetResource(this, "STATE_CITIES_TITLE");
                    stateCities.SEODataSource = RegionSA.Instance.GetSEOCitiesInState(seoRegion.RegionID);
                    stateCities.LinkType = DisplayMode.City;
                    plchState.Controls.Add(stateCities);
                    break;
                case SEOPages.Main:
                    
                    // Set Left Nav       
                    this._LeftNav.StatesVisibility(false);
                    this._LeftNav.USCitiesVisibility(false);
                    
                    // Set content's placeholder
                    // populating US states control
                    ListSEORegions lstseoStates = (ListSEORegions)this.LoadControl("ListSEORegions.ascx");
                    lstseoStates.Columns = LayoutColumns.Two;
                    lstseoStates.LinkType = DisplayMode.State;
                    lstseoStates.Suffix =  g.GetResource(this, "MAIN_STATE_LIST_SUFFIX");
                    lstseoStates.Title =  g.GetResource(this, "MAIN_STATE_LIST_TITLE");
                    lstseoStates.SEODataSource = Matchnet.Content.ServiceAdapters.RegionSA.Instance.GetSEOUSStates();
                    plchMain.Controls.Add(lstseoStates);
                    // populating Major US cities control
                    ListSEORegions lstseoUSCities = (ListSEORegions)this.LoadControl("ListSEORegions.ascx");
                    lstseoUSCities.Columns = LayoutColumns.Two;
                    lstseoUSCities.SuffixMode = SuffixType.State;
                    lstseoUSCities.LinkType = DisplayMode.City;
                    lstseoUSCities.Title = g.GetResource(this, "MAIN_CITY_LIST_TITLE");
                    lstseoUSCities.SEODataSource = Matchnet.Content.ServiceAdapters.RegionSA.Instance.GetSEOMajorUSCities();
                    plchMain.Controls.Add(lstseoUSCities);
                    this.plchMain.Visible = true;

                    // Date.CA static html dump control
                    if (g.Brand.BrandID == 1001)
                    {
                        DateCAStatic dataCAStatic = (DateCAStatic)this.LoadControl("DateCAStatic.ascx");
                        plchDataCA.Controls.Add(dataCAStatic);
                        this.plchDataCA.Visible = true;
                    }
                    break;
            }
           
        }
       /// <summary>
       ///          This method initializes Head control of Master page based on provided page parameter
       ///          Page Title, meta description of the page and Keywords are set in this method.
       /// </summary>
       /// <remarks>This method also sets the content title (ASP label control)to same value as pages' title.  </remarks>
       /// <param name="page"></param>
       /// <param name="seoRegion"></param>
        private void InitHead(SEOPages page,SEORegion seoRegion)
        {
             string title=string.Empty,
                   description = string.Empty,
                   keywords = string.Empty;
             try
             {

                 Spark.Web.Ui.Head head = (Spark.Web.Ui.Head)Page.Master.FindControl("_head");
                 if (head == null)
                 {
                     return;
                 }
                 // populating the variables

                 switch (page)
                 {
                     case SEOPages.City:
                         g.AddExpansionToken("city", seoRegion.Description);
                         g.AddExpansionToken("state", seoRegion.ParentDescription);
                         title = g.GetResource(this, "CITY_TITLE");
                         description = g.GetResource(this, "CITY_DESCRIPTION");
                         keywords = g.GetResource(this, "CITY_KEYWORDS");
                        
                         break;
                     case SEOPages.State:
                         g.AddExpansionToken("state", seoRegion.Description);
                         title = g.GetResource(this, "STATE_TITLE");
                         description = g.GetResource(this, "STATE_DESCRIPTION");
                         StringBuilder sbCities = new StringBuilder();
                         SEORegionCollection cities = Matchnet.Content.ServiceAdapters.RegionSA.Instance.GetSEOCitiesInState(seoRegion.RegionID);
                         if (cities != null)
                         {
                             foreach (SEORegion city in cities)
                             {
                                 sbCities.Append(string.Format(" {0} ", city.Description));
                             }
                         }
                         
                         g.AddExpansionToken("cities", sbCities.ToString());
                         keywords = g.GetResource(this, "STATE_KEYWORDS");
                        
                         break;
                     case SEOPages.Main:
                         title = g.GetResource(this,"MAIN_TITLE");
                         description =  g.GetResource(this, "MAIN_DESCRIPTION");
                         keywords  =g.GetResource(this, "MAIN_KEYWORDS") ;
                         
                         break;
                 }

                 // Initializing the Head control
                 head.Titlle = title;
                 head.Description = description;
                 head.Keywords = keywords;
                 // setting content title
                 this.lblTitle.Text = title;
                 g.OmniturePageName = title;
             } 
             catch(Exception ex)
             {
                 throw (ex);
             }
        }
        private void InitBreadCrumb(SEOPages page, SEORegion seoRegion)
        {
            Spark.Web.Ui.BreadCrumb breadCrumb = (Spark.Web.Ui.BreadCrumb)Page.Master.FindControl("_BreadCrumb");
            switch (page)
            {
                case SEOPages.City:

                    breadCrumb._state.Text = seoRegion.ParentDescription;
                    SEORegion seoState = new SEORegion(0, seoRegion.ParentDescription, seoRegion.ParentAbbrv,"","");
                    breadCrumb._state.NavigateUrl = SEOPageHelper.GetLink(Request.Url, seoState, DisplayMode.State);

                    breadCrumb._city.Text = seoRegion.Description;
                    breadCrumb._city.NavigateUrl = SEOPageHelper.GetLink(Request.Url, seoRegion, DisplayMode.City);
                   
                    break;
                case SEOPages.State:

                    breadCrumb._state.Text = seoRegion.Description;
                    breadCrumb._state.NavigateUrl =SEOPageHelper.GetLink(Request.Url, seoRegion, DisplayMode.State);
                    
                    break;
                case SEOPages.Main:
                    
                    break;
            }       
        }
        private void InitSEOText(SEOPages page, SEORegion seoRegion)
        {
            try
            {
                      
                switch (page)
                {
                    case SEOPages.City:
                        this._litSEOText.Text = g.GetResource(this, "CITY_SEOTEXT");
                        break;
                    case SEOPages.State:
                        this._litSEOText.Text = g.GetResource(this, "STATE_SEOTEXT_" + seoRegion.DescriptionAbbrv);
                        break;
                    case SEOPages.Main:
                        this._litSEOText.Text = g.GetResource(this, "MAIN_SEOTEXT");
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error populating SEO Text.", ex);
            }
        }
    }
}
