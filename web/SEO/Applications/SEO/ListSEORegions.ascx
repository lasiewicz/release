<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListSEORegions.ascx.cs" Inherits="Spark.Web.Applications.SEO.ListSEORegions" %>
<%@ Register TagPrefix="SparkUIElement" Namespace="Spark.Web.Framework.Ui.Elements" Assembly="SEO" %>

    <h3><asp:Label ID="_title" runat="server"></asp:Label></h3>
    <ul  id="_twoColumn" runat="server" class="states" >
        <asp:Repeater ID="rptrTwoColumn" runat="server" >
            <ItemTemplate  >        
                <li>
                    <SparkUIElement:Link id="lnkRegion0" runat="server" />
                </li>                
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <div style="clear:both"></div>
    
    <ul id="_oneColumn" runat="server" class="cities" >
        <asp:Repeater ID="rptrOneColumn" runat="server" >
            <ItemTemplate> 
                <li>
                    <SparkUIElement:Link id="lnkRegion" runat="server"  />
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
 
