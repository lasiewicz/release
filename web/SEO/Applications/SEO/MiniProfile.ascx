<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniProfile.ascx.cs" Inherits="Spark.Web.Applications.SEO.MiniProfile" %>
<%@ Register TagPrefix="SparkUIElement" Namespace="Spark.Web.Framework.Ui.Elements" Assembly="SEO" %>

<asp:PlaceHolder ID="plchCityDetail" runat="server"  Visible=false>
    <div class="userpic"><a id="_lnkUserImgCity" runat="server"><img id="_UserPicCity"  runat="server" src="../../img/noPhoto.gif"/></a></div>
    <div class="username"><SparkUIElement:Link ID="_userName" runat="server"/></div>    
    <div class="age"><strong><asp:Label ID="lblAge" runat="server"  Text="Age: "/><asp:Label ID="_age" runat="server"  /></strong><br /></div>
    <div class="aboutme"><strong><asp:Label ID="lblAboutMe" runat="server"  Text="About Me:"/></strong><asp:Literal ID="_AboutMe" runat="server"  /></div>
    <div class="profilelink"><SparkUIElement:Link ID="_lnkvwProfile" runat="server" Text="View Profile"/></div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="plchStateDetail" runat="server"  Visible=false> 
    <a id="_lnkUserImg" runat="server"><img id="_UserPicState" runat="server" src="../../img/noPhoto.gif" /></a>
    <SparkUIElement:Link ID="_stateUserName" runat="server" />    
</asp:PlaceHolder>
