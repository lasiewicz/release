<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeaturedMembers.ascx.cs" Inherits="Spark.Web.Applications.SEO.FeaturedMembers" %>
<%@ Register Src="MiniProfile.ascx" TagName="MiniProfile" TagPrefix="SparkApp" %>

<asp:PlaceHolder id="_plchCity" runat="server">
<h2><asp:Label ID="_CityFeaturedMemberTitle" runat="server"></asp:Label></h2>
    <ul id="seo_cityFeaturedMembers" >
        <asp:Repeater runat="server" ID="rptrCityFeaturedMember">
                <ItemTemplate>
                    <li>
                        <SparkApp:MiniProfile ID="MiniProfile1" runat="server"  Mode ="City"  Member=<%# Container.DataItem %>/>
                    </li>
                </ItemTemplate>
        </asp:Repeater>
    </ul>
    

</asp:PlaceHolder>

<asp:PlaceHolder ID="_plchstate" runat="server">
<div id="seo_stateFeaturedMembers" >
<h3><asp:Label id="_StateFeaturedMemberTitle" runat="server"></asp:Label></h3>
    <ul >
        <asp:Repeater runat="server" ID="rptrStateFeaturedMember">
                <ItemTemplate>
                    <li>
                        <SparkApp:MiniProfile ID="MiniProfile2" runat="server"  Mode= "State"  Member=<%# Container.DataItem %>/>
                    </li>
                </ItemTemplate>
        </asp:Repeater>

    </ul>
    <div style="clear: both;"></div>
</div>
</asp:PlaceHolder>