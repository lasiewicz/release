using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Matchnet.Member.ServiceAdapters;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;


namespace Spark.Web.Applications.SEO
{
    public partial class FeaturedMembers : Spark.Web.Framework.FrameworkControl
    {
        private DisplayMode _displayMode = DisplayMode.State; // 0 for state 1 for city 
        private int _regionID=223;   // The members are pulled from this region US=223
        private string _regionName;
        private const string _title = @"Featured Members in <RegionName>:";
        
        public DisplayMode Mode
        {
            get { return _displayMode; }
            set { _displayMode = value; }
        }
        public int RegionID
        {
            get { return _regionID; }
            set { _regionID = value; }
        }
        public string  RegionName
        {
            get { return _regionName; }
            set { _regionName = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand = ((Spark.Web.MasterPages.SEO)this.Page.Master).GContext.Brand;
            
            #region MOL member retrieval
            // use RegionID 3404470
            ArrayList memberIDList = MembersOnlineSA.Instance.GetMemberIDs("foo", brand.Site.Community.CommunityID, 0, RegionID, 18, 50, 2, SortFieldType.HasPhoto, SortDirectionType.Desc, 0, 5).ToArrayList();
            ArrayList members = MemberSA.Instance.GetMembers(memberIDList, MemberLoadFlags.None);
            #endregion
            this._plchCity.Visible = false;
            this._plchstate.Visible = false;

            if (members.Count > 0)
            {
                if (_displayMode == DisplayMode.State)
                {
                    this._plchstate.Visible = true;
                    this._StateFeaturedMemberTitle.Text = _title.Replace("<RegionName>", RegionName);

                    this.rptrStateFeaturedMember.DataSource = members;
                    this.rptrStateFeaturedMember.DataBind();
                    
                }
                else if (_displayMode == DisplayMode.City)
                {
                    this._plchCity.Visible = true;
                    this._CityFeaturedMemberTitle.Text = _title.Replace("<RegionName>", RegionName);
                    this.rptrCityFeaturedMember.DataSource = members;
                    this.rptrCityFeaturedMember.DataBind();
                }
                
            }
            
        }
    }
}