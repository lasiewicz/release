﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Spark.Web.Framework;
namespace Spark.Web.Controls
{
    public partial class Omniture : Spark.Web.Framework.FrameworkControl
    {

        private string s_AccountName = Constants.NULL_STRING;
        private string s_LinkInternalFilters = Constants.NULL_STRING;

        // Traffic Variables
        private string _pageName = Constants.NULL_STRING;
        private string _server = Constants.NULL_STRING;
        private string _jscriptFile = Constants.NULL_STRING;
       
        public string S_AccountName
        {
            get
            {
                return s_AccountName;
            }
            set
            {
                s_AccountName = value;
            }
        }
        public string S_LinkInternalFilters
        {
            get
            {
                return s_LinkInternalFilters;
            }
            set
            {
                s_LinkInternalFilters = value;
            }
        }
        public string PageName
        {
            get
            {
                return _pageName;
            }
            set
            {
                _pageName = value;
            }
        }

        public string Server
        {
            get
            {
                return _server;
            }
            set
            {
                _server = value;
            }
        }


        public string JScriptFile
        {
            get
            {
                return _jscriptFile;
            }
            set
            {
                _jscriptFile = value;
            }
        }
        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
               
                // Single Omniture Javascript file is used for all sites. s_AccountName variable name declared at the very top to make use
                // of dynamically generated values.
                s_AccountName = "spark" + g.Brand.Site.Name.Replace(".", string.Empty);

                s_LinkInternalFilters = "javascript:," + g.Brand.Site.Name;

                _jscriptFile = "http://www." + g.Brand.Site.Name.Trim() + "/analytics/javascript/omniture.js";
                _server = System.Environment.MachineName;

                string rawUrl = Request.RawUrl;

            
            }
            catch (Exception ex)
            {
                //g.ProcessException(ex);
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}