<%@ Register TagPrefix="sp" Namespace="Spark.Web.Framework.Ui.Elements" Assembly="SEO" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Head.ascx.cs" Inherits="Spark.Web.Ui.Head" %>
<head>
<title ><asp:Literal runat="server" ID="litTitle"  /></title>
<!-- 
	Host: <%= System.Environment.MachineName %>
 -->
<meta name="description" content="<%=Description%>">
<meta name="keywords" content="<%=Keywords%>">	
 
<link href="/CSS/SparkCommonNonHe.css" rel="stylesheet" type="text/css" />
<sp:Txt id="txtHead" runat="server" ResourceConstant="HEAD_CONTENT" ExpandImageTokens="false" />
 </head>

