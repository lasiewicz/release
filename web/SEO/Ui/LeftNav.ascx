<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftNav.ascx.cs" Inherits="Spark.Web.Ui.LeftNav" %>
<%@ Register Src="~/Controls/QuickSearch.ascx" TagName="QuickSearch" TagPrefix="SparkControls" %>
<%@ Register Src="~/Applications/SEO/ListSEORegions.ascx" TagName="ListSEORegions"  TagPrefix="SparkApp" %>
<%@ Register TagPrefix="sp" Namespace="Spark.Web.Framework.Ui.Elements" Assembly="SEO" %>
<table >
    <tr>
        <td>
            <SparkControls:QuickSearch ID="QuickSearch1" runat="server" />               
        </td>
    </tr>
    <tr>
        <td>
            <div id="seo_registerbanner">
                <sp:Txt ID="txtLeftnavBanner" runat="server" ResourceConstant="LEFT_NAV_BANNER" ExpandImageTokens="true" />    
            </div>
        </td>
    </tr>
    <tr>
        <td >
            <div class="seo_listbox">
            <SparkApp:ListSEORegions id="LstSEOUSStates" runat="server" Columns="Two"  SuffixMode="Fixed" LinkType="State" />
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="seo_listbox">
            <SparkApp:ListSEORegions id="LstSEOUSMajorCities" runat="server" Columns="One" SuffixMode="State" LinkType="City" />
            </div>
        </td>
    </tr>
</table>




