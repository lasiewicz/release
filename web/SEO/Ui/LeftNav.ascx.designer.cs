//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Spark.Web.Ui {
    
    
    public partial class LeftNav {
        
        /// <summary>
        /// QuickSearch1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Spark.Web.Controls.QuickSearch QuickSearch1;
        
        /// <summary>
        /// txtLeftnavBanner control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Spark.Web.Framework.Ui.Elements.Txt txtLeftnavBanner;
        
        /// <summary>
        /// LstSEOUSStates control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Spark.Web.Applications.SEO.ListSEORegions LstSEOUSStates;
        
        /// <summary>
        /// LstSEOUSMajorCities control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Spark.Web.Applications.SEO.ListSEORegions LstSEOUSMajorCities;
    }
}
