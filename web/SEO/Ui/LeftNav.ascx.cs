using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Content.ServiceAdapters;

namespace Spark.Web.Ui
{
    public partial class LeftNav : Framework.FrameworkControl
    {
        private const string STATE_TITLE = "States:";
        private const string CITY_TITLE = "Top 20 U.S. Cities:";
        protected void Page_Load(object sender, EventArgs e)
        {
            LstSEOUSStates.SEODataSource= RegionSA.Instance.GetSEOUSStates();
            LstSEOUSStates.Title = STATE_TITLE;

            LstSEOUSMajorCities.SEODataSource=RegionSA.Instance.GetSEOMajorUSCities();
            LstSEOUSMajorCities.Title = CITY_TITLE;
        }

        public void StatesVisibility(Boolean blnVisible)
        {
            LstSEOUSStates.Visible = blnVisible;
        }
        public void USCitiesVisibility(Boolean blnVisible)
        {
            LstSEOUSMajorCities.Visible = blnVisible;
        }
    }
}