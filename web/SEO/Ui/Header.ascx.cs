using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Spark.Web.Ui
{
    public partial class Header : System.Web.UI.UserControl
    {
        
        public string LogoPath()
        {
            return string.Format("{0}://{1}{2}", Context.Request.Url.Scheme, Context.Request.Url.Authority, "/img/spl_logo.gif");
        }
        public string SloganPath()
        {
            return string.Format("{0}://{1}{2}", Context.Request.Url.Scheme, Context.Request.Url.Authority, "/img/spl_slogan.gif");
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}