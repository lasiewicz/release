using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Spark.Web.Ui
{
    public partial class BreadCrumb : System.Web.UI.UserControl
    {
        private string _Separator=" > ";
        private const string ROOT_TEXT= "Home";
        private string _rootUrl = string.Empty;

        public System.Web.UI.WebControls.HyperLink _root = new HyperLink();
        public System.Web.UI.WebControls.HyperLink _state= new HyperLink();
        public System.Web.UI.WebControls.HyperLink _city = new HyperLink();

        public string RootUrl
        {
            get
            {
                return _rootUrl;
            }
            set
            {
                _rootUrl = value;
            }
        }

        public string Separator 
        {
            //return _Separator; 
            get { return _Separator; }
            set { _Separator = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            this._root.Text = ROOT_TEXT;
            this._root.NavigateUrl = RootUrl;
            this._root.CssClass = "breadHome";
            this._plchNodes.Controls.Add(this._root);

            if (this._state != null)
            {
                if (this._state.Text != string.Empty)
                {
                    AddSeparator();
                    this._state.CssClass = "breadCrumbPagesLink";
                    this._plchNodes.Controls.Add(this._state);
                }                 
            }
            if (_city != null)
            {
                if (this._city.Text!=string.Empty)
                {
                    AddSeparator();
                    this._plchNodes.Controls.Add(this._city);
                    this._city.CssClass = "breadCrumbPagesLink";
                }
            }
        }
        private void AddSeparator()
        {
            System.Web.UI.WebControls.Label separator1 = new Label();
            separator1.Text = Separator;
            this._plchNodes.Controls.Add(separator1); 
        }
    }
}
