using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Spark.Web.Ui
{
    public partial class Head : Framework.FrameworkControl
    {
        private string _Title=String.Empty;
        private string _Description = String.Empty;
        private string _Keywords = String.Empty;        

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
      
        #region Properties

        public string Titlle
        {
            get { return _Title; }
            set { _Title = value;
                  litTitle.Text = value;                  
            }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description=value; }            
        }
        public string Keywords
        {
            get { return _Keywords; }
            set { _Keywords = value; }
        }
        #endregion
   }
}