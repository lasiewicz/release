using System;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using System.Configuration;
using System.Web;

using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Spark.Web.Framework.Globalization;

namespace Spark.Web.Framework
{
    public class ContextGlobal
    {
        private Brand _Brand;
        private BrandAlias _BrandAlias;
        private StringDictionary _expansionTokens = new StringDictionary();
        private string _DesignTimeURL = "http://Search.americansingles.com/default.aspx";
        private string _OmniturePageName;
        #region Properties
        public Brand Brand
        {
            get
            {
                #region Commented out
                /*// It makes more sense to use just one Brand instead of having Brand and TargetBrand
                // since the latter already defaulted to Brand.
                if (IsAdmin && Member != null && _ImpersonateContext)
                {
                    if (_ImpersonateBrand != null)
                    {
                        // If changing to a brand that has a different culture, use the different culture.
                        if (Thread.CurrentThread.CurrentCulture.Name != _ImpersonateBrand.Site.CultureInfo.Name)
                        {
                            Thread.CurrentThread.CurrentCulture = _ImpersonateBrand.Site.CultureInfo;
                        }

                        return _ImpersonateBrand;
                    }
                }
                */
                /*
                // Switch back to original culture if necessary
                if (Thread.CurrentThread.CurrentCulture.Name != _Brand.Site.CultureInfo.Name)
                {
                    Thread.CurrentThread.CurrentCulture = _Brand.Site.CultureInfo;
                }
                */
                #endregion
                return _Brand;
            }
            set
            {
                _Brand = value;
                /*Thread.CurrentThread.CurrentCulture = _Brand.Site.CultureInfo;
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentUICulture;
                 * */
            }
        }
            public string OmniturePageName
            {
                get{return _OmniturePageName;}
                set {_OmniturePageName=value;}

            }
        
        #endregion

        /// <summary>
        /// Order of how brand is loaded
        /// 
        /// 3. Load by URI 
        /// </summary>
        public  ContextGlobal() 
        {
            InitializeBrand();
        }
        

        private void InitializeBrand()
        {
            _Brand = null;
            _BrandAlias = null;
            
            HttpContext context = HttpContext.Current;

            #region Commented out
            // Loading from querystring key
           /* if (context != null)
            {         
                #region 2. Load by BrandID or PLID or DomainAliasID
                int plid = Constants.NULL_INT;

                plid = Conversion.CInt(context.Request["BrandID"], Constants.NULL_INT);

                if (plid == Constants.NULL_INT)
                {
                    plid = Conversion.CInt(context.Request[WebConstants.URL_PARAMETER_NAME_DOMAINALIASID], Constants.NULL_INT);
                }

                if ((plid == Constants.NULL_INT) && (HttpContext.Current.Request[WebConstants.IMPERSONATE_BRANDID] == null))
                {
                    plid = Conversion.CInt(context.Request[WebConstants.URL_PARAMETER_NAME_BRANDALIASID], Constants.NULL_INT);
                }

                if (plid != Constants.NULL_INT)
                {
                    _Brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrands().GetBrand(plid);

                    if (_Brand == null)
                    {
                        _BrandAlias = BrandConfigSA.Instance.GetBrands().GetBrandAlias(plid);

                        if (_BrandAlias != null)
                        {
                            _Brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrands().GetBrand(_BrandAlias.BrandID);
                        }
                    }
                }
                #endregion
 
            }*/
            #endregion

            #region 3. Load by URI
            if (_Brand == null)
            {
                string uri = String.Empty;

                if (context != null)
                {
                    uri = context.Request.Url.Host;
                }
                else
                {
                    uri = new Uri(_DesignTimeURL).Host;
                }

                _Brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrands().GetBrand(uri);

                // BrandAlias URI
                if (_Brand == null)
                {
                    _BrandAlias = BrandConfigSA.Instance.GetBrands().GetBrandAlias(uri);

                    if (_BrandAlias != null)
                    {
                        _Brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrands().GetBrand(_BrandAlias.BrandID);

//                        _UseBrandAliasURI = true;
                    }
                }
            }
            #endregion

            // If no brand is found use AS brand as default(ported from content svc)
            if (_Brand == null)
            {
                _Brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrands().GetBrand(1001);
            }

            // This is to prevent redirecting to a brand's URI we don't own
            if ((_Brand.StatusMask & (int)Matchnet.Content.ValueObjects.BrandConfig.StatusType.CoBrand) ==
                (int)Matchnet.Content.ValueObjects.BrandConfig.StatusType.CoBrand)
            {
                return;
            }

            #region Commented out
            /*
            double dbl = Constants.NULL_DOUBLE;

            // Ignore same uri, ignore IP, ignore healthcheck
            if (!context.Request.Url.Host.ToLower().EndsWith(_Brand.Uri.ToLower()) &&
                !double.TryParse(context.Request.Url.Host.Replace(".", ""), System.Globalization.NumberStyles.Number, null, out dbl) &&
                !HttpContext.Current.Request.RawUrl.ToLower().Equals("/applications/health/healthcheck.aspx"))
            {
                StringBuilder sbUri = new StringBuilder("http://" + _Brand.Site.DefaultHost + "." + _Brand.Uri + context.Request.RawUrl);

                //If we are viewing a BrandAlias, include the BrandAliasID in the uri so the BrandAlias doesn't get lost in the redirect:
                if (_BrandAlias != null)
                {
                    sbUri.Append(context.Request.RawUrl.IndexOf("?") < 0 ? "?" : "&");
                    sbUri.Append(WebConstants.URL_PARAMETER_NAME_BRANDALIASID + "=" + _BrandAlias.BrandAliasID);
                }

                Transfer(sbUri.ToString());
            }*/
            #endregion
        }

        public void AddExpansionToken(string key, string value)
        {
            if (_expansionTokens.ContainsKey(key))
                _expansionTokens.Remove(key);

            _expansionTokens.Add(key, value);
        }

        public string GetResource(System.Web.UI.Page page, string strToken)
        {
            return Localizer.GetFormattedStringResource(strToken, page, _Brand, _expansionTokens, null);
        }

        public string GetResource(System.Web.UI.Page page, string strToken, bool expandImageTokens)
        {
            string value = Localizer.GetFormattedStringResource(strToken, page, _Brand, _expansionTokens, null);

            if (expandImageTokens)
                return Localizer.ExpandImageTokens(value, this.Brand);
            else
                return value;
        }

    }
}
