﻿using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace Spark.Web.Framework.Ui.Elements
{
    /// <summary>
    /// Summary description for Txt.
    /// </summary>
    [DefaultProperty("Resource"),
    ToolboxData("<{0}:Txt runat=server></{0}:Txt>")]
    public class Txt : WebControl
    {
        private ContextGlobal _g;
        private string _resourceConstant = string.Empty;
        private string _defaultResourceConstant = string.Empty;
        private bool _expandImageTokens = false;
        
        public Txt()
        {
            _g = new ContextGlobal();            
        }
        
        [Bindable(true), Category("Appearance"), DefaultValue("")]
        public string ResourceConstant
        {
            get { return _resourceConstant; }
            set { _resourceConstant = value; }
        }

        [Bindable(true), Category("Appearance"), DefaultValue(false)]
        public bool ExpandImageTokens
        {
            get { return _expandImageTokens; }
            set { _expandImageTokens = value; }
        }
        
        /// <summary> 
        /// Render this control to the output parameter specified.
        /// </summary>
        /// <param name="output"> The HTML writer to write out to </param>
        protected override void Render(HtmlTextWriter output)
        {
            RenderInnerContent(output);
        }

        private void RenderInnerContent(HtmlTextWriter output)
        {
            if(ResourceConstant != string.Empty)
            {
                output.Write(_g.GetResource(this.Page, ResourceConstant, ExpandImageTokens));
            }
        }
    }
}
