using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Spark.Web.Framework.Ui.Elements
{
    
    public class ImageEx : System.Web.UI.WebControls.Image
    {
        private string _navigateURL=string.Empty; // this string contains the javascript commande to in event of click on image; 

        public string NavigateURL
        {
            get { return _navigateURL; }
            set { _navigateURL = value; }
        }
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {

            if (!_navigateURL.Equals(string.Empty))
            {
                this.Attributes["onclick"] = _navigateURL;//string.Format("javascript:{0};",_navigateURL);
            }
            base.Render(writer);
        }
    }
}
