using System;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;


namespace Spark.Web.Framework
{

	/// <summary>
	/// This class is a collection of helper methods that will be refactored into a new "Photo" control when time permits.
	/// See Tom or Will for details.
	/// </summary>
	public class MemberPhotoHelper
	{
		//All static methods

		private MemberPhotoHelper()
		{
		}

		
		/// <summary>
		/// Types of photos, e.g., Full size, thumbnail, and maybe eventually tiny thumbnail size
		/// </summary>
		public enum PhotoType 
		{
			Full = 1,
			Thumbnail
		}


		#region Public Methods

		/// <summary>
		/// 
		/// </summary>
		/// <param name="photo"></param>
		/// <param name="brand"></param>
		/// <returns>True if the Photo is private and the Brand allows private photos.</returns>
		public static bool IsPrivatePhoto(Photo photo, Brand brand)
		{
			return (photo.IsPrivate && brand.GetStatusMaskValue(StatusType.PrivatePhotos));
		}

		/// <summary>
		/// Selects a "default" photo object to use as a thumbnail (or whatever), returning a private photo if there are no approved,
		/// non-private photos, and null if there are no approved photos.
		/// </summary>
		/// <param name="member"></param>
		/// <returns>The member's Photo or null if there is no photo.</returns>
		public static Photo GetPhoto(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
		{
			return GetPhoto(member, brand.Site.Community.CommunityID);
		}

		/// <summary>
		/// Selects a "default" photo object to use as a thumbnail (or whatever), returning a private photo if there are no approved,
		/// non-private photos, and null if there are no approved photos.
		/// </summary>
		/// <param name="member"></param>
		/// <returns>The member's Photo or null if there is no photo.</returns>
		public static Photo GetPhoto(Matchnet.Member.ServiceAdapters.Member member, Int32 communityID)
		{
			PhotoCommunity photos = member.GetPhotos(communityID);
			Photo photo;
			Photo retVal = null;

			for (byte photoNum = 0; photoNum < photos.Count; photoNum++)
			{
				photo = photos[photoNum];
				if (photo.IsApproved)
				{
					if (photo.IsPrivate)
					{
						retVal = photo;  //This private photo will get returned if no non-private approved photos are found
					}
					else
					{
						retVal = photo; //We found a good photo, get out
						break;
					}
				}
			}

			return retVal;
		}
        /*
		/// <summary>
		/// Returns an absolute URL to the approved Full size photo file (if any), or a thumbnail "no photo" image if no photo available, or a thumbnail size private photo image (if any)
		/// Used for passing to Userplane flash movie which requires a slightly larger size image than our thumbnail size, which
		/// is the only reason we pass them full size image instead of thumbnail.
		/// </summary>
		/// <param name="member"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		public static string GetPhotoURLForUserplane(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
		{
			// you might wonder why we don't use ThumbFileWebPath? I guess it's because our thumbnail image is smaller than Userplane expects, so we send the larger one and it scales down :(
			// This has unfortunate side effect of making the persons face often seem farther away when using the IM client.
			string url =  getPhotoURL(member, brand, WebConstants.IMAGE_PRIVATE_PHOTO_THUMB, WebConstants.IMAGE_NOPHOTO_THUMB, PhotoType.Full);
			
			//return the absolute URL (because this is used by flash movie)
			//for dev purposes, we cannot simply return HttpContext.Current.Request.Url.Host to userplane in the XML, 
			//as long as we are using a proxy for UP XML requests... (we need to return a publicly available URL that will return an image).
			string subDomainPrefix = brand.Site.DefaultHost;
			if (HttpContext.Current.Request.Url.Host.ToLower().IndexOf("stage.") > -1) 
			{
				subDomainPrefix = "stage";
			}

			if (!url.StartsWith("http://"))
			{
				url = "http://" + subDomainPrefix + "." + brand.Uri + url;
			}
			return url;

		}

		/// <summary>
		/// Returns the absolute url to the "default" approved thumbnail size photo file for a member (if any),  
		/// adjusts URL of thumbnail photo to use https proxy (to avoid non-secure content dialogs),
		/// or a relative to site root path to a tiny thumbnail "no photo" image background if no photo available, 
		/// or a relative to site root path to tiny thumbnail size private photo image (if any).
		/// For use in the DHTML notifier...
		/// </summary>
		/// <param name="member"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		public static string GetTinyThumbPhotoURL(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
		{
			return getPhotoURL(member, brand, WebConstants.IMAGE_PRIVATE_PHOTO_TINY_THUMB, WebConstants.IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB, PhotoType.Thumbnail);
		}

		/// <summary>
		/// Returns the absolute url to the "default" approved thumbnail size photo file for a member (if any), 
		/// adjusts URL of thumbnail photo to use https proxy (to avoid non-secure content dialogs),
		/// or returns a relative to site root path to a thumbnail "no photo" image background if no photo available, 
		/// or returns a relative to site root path to thumbnail size private photo image (if site suports private photos and it's the only one available).
		/// </summary>
		/// <param name="member"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		public static string GetThumbPhotoURL(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
		{
			return getPhotoURL(member, brand, WebConstants.IMAGE_PRIVATE_PHOTO_THUMB, WebConstants.IMAGE_NOPHOTO_THUMB, PhotoType.Thumbnail);

		}
		*/

		#region Keep For Future Use In A New Member Photo Control
	/*
		/// <summary>
		/// Returns the relative to site root path to the approved Full size photo file (if any), or a transparent gif if no photo available, or a full size private photo image (if any)
		/// Could be used by view profile
		/// </summary>
		/// <param name="member"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		public static string GetFullPhotoFilePath(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
		{
			return GetPhotoURL(member, brand, WebConstants.IMAGE_PRIVATE_PHOTO_FULL, "trans.gif", PhotoType.Full);

		}

		/// <summary>
		/// Returns the relative to site root path to the approved Full size photo file (if any), or a transparent gif if no photo available, or a full size private photo image (if any)
		/// Could be used by view profile
		/// </summary>
		/// <param name="photo"></param>
		/// <returns></returns>
		public static string GetFullPhotoFilePath(Photo photo, Brand brand) 
		{
			return GetPhotoURL(photo, brand, WebConstants.IMAGE_PRIVATE_PHOTO_FULL, "trans.gif", PhotoType.Full);
		}

		*/
	#endregion Keep For Future Use

		#endregion Public Methods

		#region Private Methods
        /*		
		/// <summary>
		/// Returns the absolute url to the "default" approved thumbnail or full size photo file for a member (if any), 
		/// and adjusts URL of to use https proxy (to avoid non-secure content dialogs - if necessary),
		/// and adjusts URL to use siteID param so it displays correct 404 image if file happens not to be found.
		/// If only approved photo found is "private" and the supplied brand supports private photos, returns a relative
		/// to site root path to the supplied private photo image (if any).
		/// If requested string value is null, returns a relative to site root path to the supplied "no photo" image.
		/// </summary>
		/// <param name="member">the member whose photo we're interested in.</param>
		/// <param name="brand">the current brand. Needed to check if private photos are turned "on" and to derive site id.</param>
		/// <param name="privatePhotoFile">the name of the file (e.g., "privatephoto.jpg") to return a url for if the only approved photo found is private and brand supports private photos.</param>
		/// <param name="noPhotoFile">the name of the file (e.g., "noPhoto.jpg") to return a url for if the supplied photo is not approved </param>
		/// <param name="photoType">specifies whether to get photo url from ThumbFileWebPath or FileWebPath</param>
		/// <returns></returns>
		private static string getPhotoURL(Matchnet.Member.ServiceAdapters.Member member, Brand brand, string privatePhotoFile, string noPhotoFile, PhotoType photoType )
		{
			Photo photo = MemberPhotoHelper.GetPhoto(member, brand);
			return getPhotoURL(photo, brand, privatePhotoFile, noPhotoFile, photoType);
		}

		/// <summary>
		/// Returns specific URL string of supplied photo "object/row" (either approved or not - does not check if approved, 
		/// just checks if ThumbFileWebPath or FileWebPath are not null).
		/// Returns the absolute url to the "default" approved thumbnail or full size photo file for a member (if any), 
		/// and adjusts URL of to use https proxy (to avoid non-secure content dialogs - if necessary),
		/// and adjusts URL to use siteID param so it displays correct 404 image if file happens not to be found.
		/// If photo supplied is "private," and supplied brand supports private photos, returns a relative to site root 
		/// path to the supplied private photo image (if any).
		/// If requested string value in photo object is null, returns a relative to site root path to the supplied "no photo" image.
		/// </summary>
		/// <param name="photo">the photo object "row" (approved or not) that you wish to return a url from</param>
		/// <param name="brand">the current brand. Needed to check if private photos are turned "on" and to derive site id</param>
		/// <param name="privatePhotoFile">the name of the file (e.g., "privatephoto.jpg") to return a url for if the supplied photo is private and brand suppports private photos</param>
		/// <param name="noPhotoFile">the name of the photo file to display if the supplied photo is not approved (e.g., "noPhoto.jpg")</param>
		/// <param name="photoType">specifies whether to get photo url from ThumbFileWebPath or FileWebPath</param>
		/// <returns></returns>
		private static string getPhotoURL(Photo photo, Brand brand, string privatePhotoFile, string noPhotoFile, PhotoType photoType )
		{
			string memberPhotoUrl = string.Empty;
			if (photo != null)
			{	
				if(MemberPhotoHelper.IsPrivatePhoto(photo, brand))
				{
					memberPhotoUrl = Spark.Web.Framework.Ui.Elements.Image.GetURLFromFilename(privatePhotoFile);
					return memberPhotoUrl;//no need to continue. Return relative to site root path to private photo file. We don't want to use proxy on non-absolute private photo URL. see 17305.
				}
				else
				{
					switch (photoType) 
					{
						case PhotoType.Full:
							if ( photo.FileWebPath != null) //17243 - some members have null FileWebPath but shouldn't 
							{
								memberPhotoUrl = photo.FileWebPath; // this is the url of the full size member photo
							}
							break;
						case PhotoType.Thumbnail:

							if ( photo.ThumbFileWebPath != null) // some members have null ThumbFileWebPath (before approval for example)
							{
								memberPhotoUrl = photo.ThumbFileWebPath;
							}
							break;
					}
				}
			}


			//if photo URL is still empty, use the default noPhoto file
			if (memberPhotoUrl == string.Empty) 
			{
				memberPhotoUrl = Spark.Web.Framework.Ui.Elements.Image.GetURLFromFilename(noPhotoFile);
			}
			else 
			{
				//append site id in case image is not available, we can show temp unavailable image in correct color and language
				memberPhotoUrl = appendSiteID(memberPhotoUrl,brand.Site.SiteID);

				//use a proxy that mimics https site to avoid "this page contains nonsecure items"
				memberPhotoUrl = useProxyImageIfUsingSecureConnection(memberPhotoUrl);

			}

			return memberPhotoUrl;
		}
        */
		/// <summary>
		///  Appends query string parameter to the end of member images 
		///  (e.g., http://photo9.matchnet.com/beta/2005/11/13/23/120582964.jpg?siteID=103)
		///  The purpose is that if image happens not to be available (not found - 404), then instead of showing a red x, 
		///  we show a "temporarily unavailable" thumbnail image. The siteID is not required in order to show the
		///  "temporarily unavailable" thumbnail image, however, it determines the correct "temp unavailable" image to use
		///  with correct color and language (or else default temp unavailable image will be used).
		/// </summary>
		/// <param name="url"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		private static string appendSiteID(string imageURL, int siteID)
		{
			// This must be appended to all photos that hit the file servers in order for the site-specific
			// "Temporarily Unavailable" images to appear.  Appending this to any other photos should not cause errors.
			return  imageURL + "?" + WebConstants.URL_PARAMETER_NAME_SITEID + "=" + siteID.ToString();
		}
        /*
		/// <summary>
		/// This is to defeat the "this page contains nonsecure items" dialog that appears when you open a https page.
		/// Instead of getting image from http url, we get image from aspx page on https site that acts as proxy for that image.
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		private static string useProxyImageIfUsingSecureConnection(string url)
		{
			if (FrameworkGlobals.IsSecureRequest())
			{
				return "/Applications/MemberProfile/MemberPhotoFile.aspx?Path=" + System.Web.HttpUtility.UrlEncode(url);
			}
			else 
			{
				return url;
			}
		}
		*/

		#endregion Private Methods


}
}
