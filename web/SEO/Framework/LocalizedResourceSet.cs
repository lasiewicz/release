﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.Web.Framework.Globalization
{
    /// <summary>
    /// LocalizedResourceSet.
    /// </summary>
    internal sealed class LocalizedResourceSet
    {
        private const string RESOURCES_SUBFOLDER = @"App_LocalResources";
        private const string ROOT_NAMESPACE_FOLDER = @"spark\web";

        #region LocalizedResourceSet.

        internal static ResourceValue GetResource(Control ctrl, Brand brand, string ResourceName)
        {
            Hashtable resourceCollection = GetResourceCollection(ctrl, brand.Site);
            if (resourceCollection.ContainsKey(ResourceName))
            {
                return resourceCollection[ResourceName] as ResourceValue;
            }
            else
            {
                // null means resource was not found!
                return null;
            }
        }


        private static Hashtable GetResourceCollection(Control ctrl, Site site)
        {
            Hashtable resourceCollection = ResourceCache.Instance.GetResourceCollection(ctrl, site);
            if (resourceCollection == null)
            {
                string resourceFilePath = ResourceCache.Instance.GetResxFilePath(ctrl, site);
                resourceCollection = LoadResourceCollection(resourceFilePath);
                ResourceCache.Instance.Insert(resourceCollection, ctrl, site);
            }
            return resourceCollection;
        }


        private static Hashtable LoadResourceCollection(string resourceFilePath)
        {
            Hashtable resourceCollection = new Hashtable();

            //TODO: Make this bomb and throw exception?
            if (!System.IO.File.Exists(resourceFilePath))
            {
                //#if DEBUG
                //				System.Diagnostics.Trace.WriteLine(string.Format("Resource file not found ({0}).", resourceFilePath));
                //#endif
                return resourceCollection;
            }

            try
            {
                using (ResXResourceReader reader = new ResXResourceReader(resourceFilePath))
                {
                    try
                    {
                        foreach (DictionaryEntry entry in reader)
                        {
                            try
                            {
                                resourceCollection.Add(entry.Key.ToString(), new ResourceValue(entry.Value.ToString()));
                            }
                            catch (Exception ex)
                            {
                                // This is to ignore object not found error when no proper <value/> is present within a <data/> tag
                            }
                        }
                        return resourceCollection;
                    }
                    catch (Exception ex)
                    {
                        ContextGlobal _g = (ContextGlobal)HttpContext.Current.Items["g"];
                        EventLog.WriteEntry("Spark.Web.Framework.LocalizedResourceSet", ex.Message, EventLogEntryType.Error);
                        return resourceCollection;
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing resources from " + resourceFilePath + ".", ex);
            }
        }

        #endregion

        #region ResourceCache

        public class ResourceCache
        {

            public readonly static ResourceCache Instance = new ResourceCache();
#if DEBUG
            private FileSystemWatcher _watcher;
#endif
            private Hashtable _ResxFileMap; //in order to delete cache for specific file, need a map of files
            private Hashtable _ResxKeys;

            private ResourceCache()
            {
                _ResxKeys = Hashtable.Synchronized(new Hashtable());
                _ResxFileMap = Hashtable.Synchronized(new Hashtable());

#if DEBUG

                _watcher = new FileSystemWatcher();
                _watcher.Path = HttpContext.Current.Server.MapPath("~/");
                _watcher.NotifyFilter = NotifyFilters.LastAccess;
                _watcher.Filter = "*.resx";
                _watcher.IncludeSubdirectories = true;
                _watcher.Changed += new FileSystemEventHandler(ResxChangedEventHandler);
                _watcher.EnableRaisingEvents = false;
#endif
            }


            public Hashtable GetResourceCollection(Control ctrl, Site site)
            {
                Hashtable siteLevel = null;
                Hashtable cultureLevel = null;

                IntPtr resxHandle = (ctrl == null) ? IntPtr.Zero : ctrl.GetType().TypeHandle.Value;

                siteLevel = _ResxKeys[resxHandle] as Hashtable;
                if (siteLevel == null) return null;

                cultureLevel = siteLevel[site.SiteID] as Hashtable;
                if (cultureLevel == null) return null;

                return cultureLevel[site.CultureInfo.LCID] as Hashtable;
            }


            public Hashtable GetResourceCollection(string resxSetKey, Site site)
            {
                Hashtable siteLevel = null;
                Hashtable cultureLevel = null;

                siteLevel = _ResxKeys[resxSetKey] as Hashtable;
                if (siteLevel == null) return null;

                cultureLevel = siteLevel[site.SiteID] as Hashtable;
                if (cultureLevel == null) return null;

                return cultureLevel[site.CultureInfo.LCID] as Hashtable;
            }


            public void Insert(Hashtable resourceCollection, Control ctrl, Site site)
            {
                Hashtable siteLevel = null;
                Hashtable cultureLevel = null;

                IntPtr resxHandle = (ctrl == null) ? IntPtr.Zero : ctrl.GetType().TypeHandle.Value;
                string resxFileName = GetResxFilePath(ctrl, site);
                _ResxFileMap[resxFileName] = resxHandle;
#if DEBUG
                // File event changes are only for resx editing support. So production should not worry about this.
                _watcher.EnableRaisingEvents = true;
                //System.Diagnostics.Trace.WriteLine("___LocalizedResourceSet.ResourceCache.Instance.Insert() " + resxHandle.ToString() + "|" + siteID.ToString() + "|" + CultureInfo.CurrentCulture.LCID +"|" + resxFileName);
#endif
                try
                {
                    if (_ResxKeys.ContainsKey(resxHandle))
                    {
                        siteLevel = _ResxKeys[resxHandle] as Hashtable;
                        if (siteLevel.ContainsKey(site.SiteID))
                        {
                            cultureLevel = siteLevel[site.SiteID] as Hashtable;
                            if (cultureLevel.ContainsKey(site.CultureInfo.LCID))
                            {
                                // overwrite it
                                cultureLevel[site.CultureInfo.LCID] = resourceCollection;
                            }
                            else
                            {
                                cultureLevel.Add(site.CultureInfo.LCID, resourceCollection);
                            }
                        }
                        else
                        {
                            siteLevel.Add(site.SiteID, Hashtable.Synchronized(new Hashtable()));
                            cultureLevel = siteLevel[site.SiteID] as Hashtable;
                            cultureLevel.Add(site.CultureInfo.LCID, resourceCollection);
                        }
                    }
                    else
                    {
                        _ResxKeys.Add(resxHandle, Hashtable.Synchronized(new Hashtable()));
                        siteLevel = _ResxKeys[resxHandle] as Hashtable;
                        siteLevel.Add(site.SiteID, Hashtable.Synchronized(new Hashtable()));
                        cultureLevel = siteLevel[site.SiteID] as Hashtable;
                        cultureLevel.Add(site.CultureInfo.LCID, resourceCollection);
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine(ex.ToString());
                }


            }


            // This method doesn't seem to be in use.
            public void Insert(Hashtable resourceCollection, string resxSetKey, Site site)
            {
                Hashtable siteLevel = null;
                Hashtable cultureLevel = null;
#if DEBUG
                _watcher.EnableRaisingEvents = true;
                //System.Diagnostics.Trace.WriteLine("___LocalizedResourceSet.ResourceCache.Instance.Insert() " + resxSetKey + "|" + siteID.ToString() + "|" + CultureInfo.CurrentCulture.LCID );
#endif
                try
                {
                    if (_ResxKeys.ContainsKey(resxSetKey))
                    {
                        siteLevel = _ResxKeys[resxSetKey] as Hashtable;
                        if (siteLevel.ContainsKey(site.SiteID))
                        {
                            cultureLevel = siteLevel[site.SiteID] as Hashtable;
                            if (cultureLevel.ContainsKey(site.CultureInfo.LCID))
                            {
                                // overwrite it
                                cultureLevel[site.CultureInfo.LCID] = resourceCollection;
                            }
                            else
                            {
                                cultureLevel.Add(site.CultureInfo.LCID, resourceCollection);
                            }
                        }
                        else
                        {
                            siteLevel.Add(site.SiteID, Hashtable.Synchronized(new Hashtable()));
                            cultureLevel = siteLevel[site.SiteID] as Hashtable;
                            cultureLevel.Add(site.CultureInfo.LCID, resourceCollection);
                        }
                    }
                    else
                    {
                        _ResxKeys.Add(resxSetKey, Hashtable.Synchronized(new Hashtable()));
                        siteLevel = _ResxKeys[resxSetKey] as Hashtable;
                        siteLevel.Add(site.SiteID, Hashtable.Synchronized(new Hashtable()));
                        cultureLevel = siteLevel[site.SiteID] as Hashtable;
                        cultureLevel.Add(site.CultureInfo.LCID, resourceCollection);
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine(ex.ToString());
                }
            }


            public string GetResxFilePath(System.Web.UI.Control ctrl, Site site)
            {
                /*
                 Physical files:
                [C:\Matchnet\Bedrock\Web\bedrock.matchnet.com\_resources\global.999.en-us.resx]			>>> _resources\global	>>> 999		>>> en-us	>>> resx
                [C:\Matchnet\Bedrock\Web\bedrock.matchnet.com\_resources\default.aspx.100.en-us.resx] 	>>> _resources\default	>>> aspx	>>> 100	>>> en-us	>>> resx
                [C:\matchnet\bedrock\web\bedrock.matchnet.com\applications\home\_resources\splash.ascx.100.en-us.resx] 

                Type names:
                [UserPreferences_ascx] (type.Name)
                [Matchnet.Web.Applications.Home.Controls] (type.BaseTtpe.Namespace)
                */
                // make _aspx .aspx
                string fileName;
                
                fileName = Localizer.ConvertTypeToResourcePath(ctrl.GetType());                

                return (Path.Combine(HttpContext.Current.Server.MapPath("~/"), fileName) +
                    "." + site.SiteID.ToString() +
                    "." + site.CultureInfo.Name +
                    ".resx").ToLower();
            }


#if DEBUG
            private void ResxChangedEventHandler(object source, FileSystemEventArgs e)
            {
                ///NOTE: The ASP.NET compiles .resx files when it loads the classes that they correspond to.
                ///A side effect of that is that the file is touched and raises a "Changed" event.
                ///Furtunately, it happens once, on load. So the cache is going to get cleared and re-loaded by us twice because of this, but 
                ///it won't deprecate ever afterrwards, and the ASP.NET does not recompile the resx files again.
                ///our own reading of the file does NOT raise a changed event, so no looping here.
                ///This whole deal of unloading cache is necessary because of the resource editor tool that needs to touch the file and expects
                ///that the next hit on the site will reflect the changes.
                ///
                ///Future cleanup is to actually use the compiled resx directly instead of the hashtable extracted.
                ///Look at ResourceSet class, inherit and overrid so that it picks up our file naming convention
                ///and then extend to have the ResourceValue instead of the streight string?

                //	System.Diagnostics.Trace.WriteLine("___Globalizer: [" +  e.FullPath + "] , " + e.Name + " raised event " + e.ChangeType);

                RemoveFromCache(e.FullPath.Replace((source as FileSystemWatcher).Path, ""), e.FullPath);
            }
#endif
            public void RemoveFromCache(string resourceFileName, string resourceFileFullPath)
            {
                ///NOTE: The ASP.NET compiles .resx files when it loads the classes that they correspond to.
                ///A side effect of that is that the file is touched and raises a "Changed" event.
                ///Furtunately, it happens once, on load. So the cache is going to get cleared and re-loaded by us twice because of this, but 
                ///it won't deprecate ever afterrwards, and the ASP.NET does not recompile the resx files again.
                ///our own reading of the file does NOT raise a changed event, so no looping here.
                ///This whole deal of unloading cache is necessary because of the resource editor tool that needs to touch the file and expects
                ///that the next hit on the site will reflect the changes.
                ///
                ///Future cleanup is to actually use the compiled resx directly instead of the hashtable extracted.
                ///Look at ResourceSet class, inherit and overrid so that it picks up our file naming convention
                ///and then extend to have the ResourceValue instead of the streight string?

                //	System.Diagnostics.Trace.WriteLine("___Globalizer: [" +  e.FullPath + "] , " + e.Name + " raised event " + e.ChangeType);

                Hashtable siteLevel = null;
                Hashtable cultureLevel = null;
                string resxSetKey;
                int siteID;
                CultureInfo ci;

                string[] pathParts = resourceFileName.Split('.');

                if (pathParts.Length == 5)
                {
                    // this is a regular resx file. the key is [0] + '.' + [1]
                    resxSetKey = pathParts[0] + "." + pathParts[1];
                    siteID = Int32.Parse(pathParts[2]);
                    ci = new CultureInfo(pathParts[3]);
                }
                else if (pathParts.Length == 4)
                {
                    /* SPECIAL CASE: "_Resources\Global"
                    [C:\Matchnet\Bedrock\Web\bedrock.matchnet.com\_resources\global.999.en-us.resx]			>>> _resources\global	>>> 999		>>> en-us	>>> resx
                    [C:\Matchnet\Bedrock\Web\bedrock.matchnet.com\_resources\default.aspx.100.en-us.resx] 	>>> _resources\default	>>> aspx	>>> 100	>>> en-us	>>> resx
                    */
                    resxSetKey = pathParts[0];
                    siteID = Int32.Parse(pathParts[1]);
                    ci = new CultureInfo(pathParts[2]);
                }
                else
                {
                    return;
                }

                object obj = _ResxFileMap[resourceFileFullPath.ToLower()];
                IntPtr resxHandle = (obj == null) ? IntPtr.Zero : (IntPtr)obj;
                siteLevel = _ResxKeys[resxHandle] as Hashtable;

                if (siteLevel != null)
                {
                    cultureLevel = siteLevel[siteID] as Hashtable;
                    if (cultureLevel != null)
                    {
                        cultureLevel.Remove(ci.LCID);
                    }
                }
            }


        }


        #endregion Resource Cache

        #region Resource Value
        internal class ResourceValue
        {
            bool _HasNamedToken;
            bool _HasPositionalToken;
            string _Value;

            public ResourceValue(string Value)
            {
                _Value = (Value == null) ? string.Empty : Value;
                _HasPositionalToken = (Value.IndexOf("%s") > -1);
                _HasNamedToken = (Value.IndexOf("((") > -1);
            }

            /// <summary>
            /// True if the resource value has (( in it anywhere double parens
            /// </summary>
            public bool HasNamedTokens
            {
                get { return _HasNamedToken; }
            }

            /// <summary>
            /// True if the resource value has %s in it anywhere
            /// </summary>
            public bool HasPositionalTokens
            {
                get { return _HasPositionalToken; }
            }

            /// <summary>
            /// The value of the resource
            /// </summary>
            public string Value
            {
                get { return _Value; }
            }
        }
        #endregion Resource Value

    }
}
