using System;

namespace Spark
{
    // Gender Mask
    /// <summary>
    /// 
    /// </summary>
    [Flags]
    public enum GenderMask : int
    {
        /// <summary> </summary>
        Male = 1,
        /// <summary> </summary>
        Female = 2,
        /// <summary> </summary>
        SeekingMale = 4,
        /// <summary> </summary>
        SeekingFemale = 8,
        /// <summary> </summary>
        MTF = 16,
        /// <summary> </summary>
        FTM = 32,
        /// <summary> </summary>
        SeekingMTF = 64,
        /// <summary> </summary>
        SeekingFTM = 128
    };


    /*
    public enum CurrencyType : int
    {
        None = 0,
        USDollar = 1,
        Euro = 2,
        CanadianDollar = 3,
        Pound = 4,
        AustralianDollar = 5,
        Shekels = 6
    }
    */


    /// <summary>
    /// 
    /// </summary>
    public enum DurationType : int
    {
        /// <summary> </summary>
        None = 0,
        /// <summary> </summary>
        Minute = 1,
        /// <summary> </summary>
        Hour = 2,
        /// <summary> </summary>
        Day = 3,
        /// <summary> </summary>
        Week = 4,
        /// <summary> </summary>
        Month = 5,
        /// <summary> </summary>
        Year = 6
    };


    /// <summary>
    /// Defines system-wide constants
    /// </summary>
    public sealed class Constants
    {
        private Constants()
        {
        }


        public enum PRTID : int
        {
            HotlistWhoseEmailedYou = 30,
            HotlistWhoFlirtedWithYou = 31,
            MemberServicesSubscribe = 32,
            MemberServicesSubscriptionPlansAndCosts = 33

        };

        public enum HotlistCatID : int
        {
            EmailedYou = -4,
            FlirtedWithYou = -3

        };


        public const string EVENT_SRC_WWW = "WWW";

        /// <summary>
        /// Value used to represent a NULL float
        /// </summary>
        public const float NULL_FLOAT = float.MinValue + (float)1;

        /// <summary>
        /// Value used to represent a NULL decimal
        /// </summary>
        public const decimal NULL_DECIMAL = decimal.MinValue + (decimal)1;

        /// <summary>
        /// Value used to represent a NULL double
        /// </summary>
        public const double NULL_DOUBLE = double.MinValue + (double)1;

        /// <summary>
        /// Value used to represent a NULL int
        /// </summary>
        public const int NULL_INT = int.MinValue + 1;

        /// <summary>
        /// Value used to represent a NULL long
        /// </summary>
        public const long NULL_LONG = long.MinValue + (long)1;

        /// <summary>
        /// Value used to represent a NULL string
        /// </summary>
        public const string NULL_STRING = null;

        /// <summary>
        /// Value used to represent a successful method execution
        /// </summary>
        public const int RETURN_OK = 0;

        /// <summary>
        /// GroupID of the Group that encompasses all personals Communities.
        /// </summary>
        public const int GROUP_PERSONALS = 8383;

        /// <summary> </summary>
        public const int MIN_AGE_RANGE = 18;

        /// <summary> </summary>
        public const int MAX_AGE_RANGE = 99;

        /// <summary> </summary>
        public const int APP_FREETEXTAPPROVAL = 525;

        // height & weight
        public const double HEIGHT_MIN = 137.16;    //these are in cm
        public const double HEIGHT_MAX = 243.84;
        public const double WEIGHT_MIN = 36288;  //these are in grams
        public const double WEIGHT_MAX = 181500;

        public const int LOCALE_US_ENGLISH = 1033;
    }
}
