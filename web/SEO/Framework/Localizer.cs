﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Resources;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.Web.Framework.Globalization
{
    public sealed class Localizer
    {
        private const string RESOURCES_SUBFOLDER = @"App_LocalResources";
        private const string ROOT_NAMESPACE_FOLDER = @"spark\web";
        private const string IMG_URL_ROOT = @"/img";
        
        private static readonly Localizer _instance = new Localizer();

        // Explicit static constructor to tell the compiler not to make the type as beforefieldinit.
        // This is for lazy loading.
        static Localizer()
        { }

        Localizer()
        { }

        public static Localizer Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///		For every token (key) value found in the source string, replace it with the corresponding
        ///		expansion (value).
        /// </summary>
        /// <param name="source"></param>
        /// <param name="tokenMap"></param>
        /// <returns></returns>
        public static string ExpandTokens(string source, StringDictionary tokenMap)
        {
            if (tokenMap == null)
            {
#if DEBUG
                System.Diagnostics.Trace.WriteLine("__Globalization.Localizer.ExpandTokens() empty tokenmap for " + source);
#endif
                return source;
            }

            string result = source;
            foreach (DictionaryEntry entry in tokenMap)
            {
                string key = "((" + entry.Key.ToString().ToUpper(CultureInfo.InvariantCulture) + "))";

                if (entry.Value == null)
                {
#if DEBUG
                    System.Diagnostics.Trace.WriteLine("__Globalization.Localizer.ExpandTokens() null value for key " + entry.Key);
#endif
                    return source;
                }

                result = result.Replace(key, entry.Value.ToString());
            }
            return result;
        }

        public static string ExpandImageTokens(string source, Brand brand)
        {
            int pos = source.IndexOf("((image:");
            int lastPos = 0;
            int endPos = 0;
            string fileName = string.Empty;
            string token = string.Empty;

            if (pos < 0)
            {
                return source;
            }

            StringBuilder builder = new StringBuilder();

            while (pos > -1)
            {

                builder.Append(source.Substring(lastPos, pos - lastPos));

                endPos = source.IndexOf("))", pos);

                if (endPos > -1)
                {
                    token = source.Substring(pos + 2, endPos - pos - 2);

                    int colon1 = token.IndexOf(":");
                    int colon2 = token.LastIndexOf(":");

                    if (colon1 != -1 && colon2 != -1)
                    {
                        fileName = token.Substring(colon2 + 1, token.Length - colon2 - 1);
                        builder.Append(GetURLFromFilename(fileName, brand));
                    }

                    lastPos = endPos + 2;
                    if (lastPos > source.Length)
                    {
                        break;
                    }
                    pos = source.IndexOf("((image:", lastPos);
                }
                else
                    break;

            }
            if (lastPos < source.Length)
            {
                builder.Append(source.Substring(lastPos));
            }

            return builder.ToString();
        }

        private static string GetURLFromFilename(string fileName, Brand brand)
        {
            string path = string.Empty;

            path = FileSrc(fileName, brand);

            
            return path;
        }

        private static string FileSrc(string filename, Brand brand)
        {
            string path = String.Empty;
            bool found = false;

            string imageRoot = "\\\\" + Dns.GetHostName() + "\\img";

            string[] delims = new string[] { "Brand", "Site", "Community", "p", "d", "l" };
            string[] vals = new string[] { GetPathFromUri(brand.Uri), GetPathFromUri(brand.Site.Name), brand.Site.Community.Name, brand.BrandID.ToString(), brand.Site.Community.CommunityID.ToString(), brand.Site.LanguageID.ToString() };
            for (int i = 0; i < delims.Length; i++)
            {
                path = @"\" + delims[i] + @"\" + vals[i] + @"\" + filename;
                if (System.IO.File.Exists(imageRoot + path))
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                path = @"\" + filename;
                found = System.IO.File.Exists(imageRoot + path);
                //Regardless of whether the image exists, we'll return a path including the filename.
                //This will be logged as a 404 in IIS logs.
            }
                        
            path = path.Replace(@"\", @"/");
            path = IMG_URL_ROOT + path;

            return path;
        }

        private static string GetPathFromUri(string pUri)
        {
            return pUri.Replace(".", "-");
        }

        /// <summary>
        ///		Retrieve a string resource, use it as format template, and populate with values from the supplied argument array.
        /// </summary>
        /// <param name="resourceName">Resource key.</param>
        /// <param name="caller">The class that the resource belongs to.</param>
        /// <param name="brand">Site brand name.</param>
        /// <param name="tokenMap">String expansion tokens (name/value pairs).</param>
        /// <param name="args">String replacement arguments.</param>
        /// <returns>
        ///		Returns a localized string or null if resource is not defined.
        /// </returns>
        internal static string GetFormattedStringResource(string resourceName, object caller, Brand brand, StringDictionary tokenMap, string[] args)
        {
            Control callingControl = GetCallingPageOrUserControl(caller);


            LocalizedResourceSet.ResourceValue resourceValue = LocalizedResourceSet.GetResource(callingControl, brand, resourceName);

            // tokenMap not found:
            if (tokenMap == null)
                return string.Empty;
            
            // resource not found:
            if (resourceValue == null)
                return string.Empty;
            
            // Resource found, value is empty.
            if (resourceValue.Value == string.Empty)
                return string.Empty;

            string formattedString = resourceValue.Value;

            // perform  replace "((NAME1))" with tokenMap["NAME1"].Value 
            if (resourceValue.HasNamedTokens)
                formattedString = ExpandTokens(formattedString, tokenMap);


            if (args == null || args.Length == 0 || !resourceValue.HasPositionalTokens)
            {
				return formattedString;
            }
            else
            {
                // replace occurrences of "%s" with values from the agrs array
                formattedString = Format(formattedString, args);
                return formattedString;
            }
        }

        internal static string ConvertTypeToResourcePath(Type type)
        {
            string typeName = type.Name.ToLower();

            string folderName = type.BaseType.Namespace.ToLower().Replace('.', '\\');

            // strip the root namespace from the folder name, since the file structure ignores it
            if (folderName.StartsWith(ROOT_NAMESPACE_FOLDER))
            {
                folderName = folderName.Substring(ROOT_NAMESPACE_FOLDER.Length + 1);
            }

            #region For 1.1/2.0 compatibility
            //In 1.1, typeName is of the form "searchareacode_ascx"
            //In 2.0, it's of the form "applications_home_controls_searchareacode_ascx"
            //The code below strips the extra junk from the 2.0 version of the type name
            if (typeName.StartsWith(folderName.Replace("\\", "_")))
            {
                typeName = typeName.Substring(folderName.Length + 1);
            }
            #endregion

            if (typeName.EndsWith("_aspx") || typeName.EndsWith("_ascx") || typeName.EndsWith("_asmx"))
            {
                typeName = typeName.Substring(0, typeName.Length - 5) + "." + typeName.Substring(typeName.Length - 4);
            }
            else
            {
                System.Diagnostics.Trace.WriteLine("Funny Type " + type);
            }

            return folderName + @"\" + RESOURCES_SUBFOLDER + @"\" + typeName;
        }

        /// <summary>
        ///		Try to determine the Page or UserControl that this resource request ultimately
        ///		comes from. Recurse up the Control hierarchy if necessary.
        /// </summary>
        /// <param name="caller"></param>
        /// <returns></returns>
        private static Control GetCallingPageOrUserControl(object caller)
        {
            if (null == caller || !(caller is Control))
            {
                return null;
            }

            if (!(caller is UserControl || caller is Page))
            {
                return GetCallingPageOrUserControl(((Control)caller).Parent);
            }

            return (Control)caller;
        }

        /// <summary>
        ///		Substitute occurrences of "%s" in the format string with values from the arguments array.
        /// </summary>
        /// <param name="source">Format string.</param>
        /// <param name="args">Arguments array.</param>
        /// <returns></returns>
        private static string Format(string source, string[] args)
        {
            if (args == null || 0 == args.Length)
            {
                return source;
            }

            int pos = source.IndexOf("%s");
            int lastPos = 0;
            int counter = 0;

            if (pos < 0)
            {
                return source;
            }

            // Preallocate buffer to strring length + some. StringBuilder re-allocates mem and initializes to an arbitrary length initially.
            // Why 32? Why Not?
            StringBuilder builder = new StringBuilder(source.Length + 32);

            while (pos > -1)
            {
                builder.Append(source.Substring(lastPos, pos - lastPos));
                if (counter < args.Length)
                {
                    builder.Append(args[counter]);
                    counter++;
                }
                else
                {
                    break;
                }
                lastPos = pos + 2;
                if (lastPos > source.Length)
                {
                    break;
                }
                pos = source.IndexOf("%s", lastPos);
            }
            if (lastPos < source.Length)
            {
                builder.Append(source.Substring(lastPos));
            }

            return builder.ToString();
        }

        
    }
}
