using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Spark.Web.Framework
{
    public class FrameworkControl : System.Web.UI.UserControl
    {
        private ContextGlobal _g = new ContextGlobal();

        public ContextGlobal g
        {
            get
            {
                return _g;
            }
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            base.Render(writer);
        }
    }
}
