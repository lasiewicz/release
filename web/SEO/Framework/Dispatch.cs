using System;
using System.Web;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Dispatch which is usedto handle URL re-writing in SEO pages application
/// </summary>
/// 
namespace Spark.Web.Framework
{
    public class Dispatch : IHttpModule
    {
        
        public Dispatch()
        {
        }

        public void Init(HttpApplication application)
        {
            application.BeginRequest += (new EventHandler(this.Application_BeginRequest));
            application.EndRequest += new EventHandler(application_EndRequest);
            application.Error += new EventHandler(this.Application_OnError);
        }

        private void Application_OnError(Object source, EventArgs e)
        {

            Exception ex = HttpContext.Current.Server.GetLastError();
            if (ex != null)
            {
                if (ex is HttpException)
                {
                    if (((HttpException)ex).GetHttpCode() == 404)
                    {
                        return;
                    }
                }

                //ContextExceptions _ContextExceptions = new ContextExceptions();
                //_ContextExceptions.LogException(ex);
            }

            HttpContext Context = HttpContext.Current;
            Context.RewritePath("~/Error.aspx");
            Context.ApplicationInstance.CompleteRequest();
        }

        private void Application_BeginRequest(Object source, EventArgs e)
        {
            SEOReWriting();            
        }

        public void Dispose()
        {
        }

        private void application_EndRequest(object sender, EventArgs e)
        {
            /*ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];
            if (g != null)
            {
                g.Context_EndRequest();
            }*/
        }
        /// <summary>
        /// This method is resopnsible for all URL re-writing logic that needs to take place for SEO pages application        
        /// <remarks>This method works along with IIS setting 1) Default.aspx is set as the default page 2)Custom error page for error 404 is also set to default.aspx</remarks>
        /// </summary>
        private void SEOReWriting()
        {
            // Set up local variables
            System.Diagnostics.Debug.WriteLine("Dispatch started");
            HttpContext Context = HttpContext.Current;
            string requestPath = Context.Request.Path.ToLower();
            string fullURL = Context.Request.Url.AbsoluteUri.ToLower();

            try
            {
                System.Diagnostics.Debug.WriteLine("1)"+fullURL);
                if (requestPath.EndsWith("css") || requestPath.EndsWith("png")    
                    || requestPath.EndsWith("gif") || requestPath.EndsWith("jpg") || requestPath.EndsWith("js")
                    || requestPath.EndsWith("htm"))
                {
                    return;
                }
                if (Context.Request.QueryString.Count <= 0 && !requestPath.EndsWith("seo.aspx"))
                {

                    int firstOccurance = requestPath.IndexOf("/");
                    int LastOccurance = requestPath.LastIndexOf("/");

                    if (firstOccurance < 0 || requestPath.EndsWith("default.aspx") || requestPath.Equals("/"))  // there's no '/' go to SEO.aspx
                    {
                       // Context.Items["PagePath"] = "/Applications/seo/seo.aspx";	//requestPath;
                      //  Context.Items["FullURL"] = fullURL + "/Applications/seo/seo.aspx";
                        System.Diagnostics.Debug.WriteLine("2) Rewriting to :" + "/Applications/seo/seo.aspx");
                        Context.RewritePath("/Applications/seo/seo.aspx",false);

                    }
                    else if (LastOccurance == firstOccurance)   // URL follows : "/state" pattern
                    {
                       // Context.Items["PagePath"] = "/Applications/seo/seo.aspx";
                       // Context.Items["FullURL"] = fullURL.Replace(requestPath, "/Applications/seo/seo.aspx?state=" + requestPath.Substring(1));
                        System.Diagnostics.Debug.WriteLine("3) Rewriting to :" + "/Applications/seo/seo.aspx?state=" + requestPath.Substring(1));
                        Context.RewritePath("/Applications/seo/seo.aspx?state=" + requestPath.Substring(1),false);
                    }
                    else if (LastOccurance > firstOccurance)
                    {
                        string strMiddle = requestPath.Substring(firstOccurance + 1, (LastOccurance - firstOccurance) - 1);
                        string strCity = requestPath.Substring(LastOccurance + 1, (requestPath.Length - 1) - LastOccurance);
                        int MiddleOccurance = strMiddle.IndexOf("/");

                        if (strCity.Length > 0)  // URL follows : "/state/city" pattern
                        {
                            string strNewReqpath = "/Applications/seo/seo.aspx?state=" + strMiddle + "&city=" + strCity;
                           // Context.Items["PagePath"] = "/Applications/seo/seo.aspx";
                          //  Context.Items["FullURL"] = fullURL.Replace(requestPath, strNewReqpath);
                            System.Diagnostics.Debug.WriteLine("4) Rewriting to :" + strNewReqpath);
                            Context.RewritePath(strNewReqpath,false);

                        }
                        else if (MiddleOccurance > -1) // URL follows : "/state/city/" pattern
                        {
                            strCity = requestPath.Substring(MiddleOccurance + 2, LastOccurance - MiddleOccurance - 2);
                            string strState = requestPath.Substring(firstOccurance + 1, MiddleOccurance - firstOccurance);
                            string strNewReqpath = "/Applications/seo/seo.aspx?state=" + strState + "&city=" + strCity;
                           // Context.Items["PagePath"] = "/Applications/seo/seo.aspx";
                           // Context.Items["FullURL"] = fullURL.Replace(requestPath, strNewReqpath);
                            System.Diagnostics.Debug.WriteLine("5) Rewriting to :" + strNewReqpath);
                            Context.RewritePath(strNewReqpath,false);

                        }
                        else // URL follows : "/state/" pattern
                        { 
                            string strNewReqpath = "/Applications/seo/seo.aspx?state=" + strMiddle;
                           // Context.Items["PagePath"] = "/Applications/seo/seo.aspx";	//requestPath;
                           // Context.Items["FullURL"] = fullURL.Replace(requestPath, strNewReqpath);
                            //string.Format("{0}://{1}{2}",Context.Request.Url.Scheme ,  Context.Request.Url.Authority,strNewReqpath)
                            System.Diagnostics.Debug.WriteLine("6)Rewriting to :" + strNewReqpath);
                            Context.RewritePath(strNewReqpath,false);
                        }
                    }
                }
                else
                {
                   // Context.Items["PagePath"] = requestPath;
                    //Context.Items["FullURL"] = fullURL; 
                    System.Diagnostics.Debug.WriteLine("7) Rewriting to :" + requestPath);
                    Context.RewritePath(requestPath,false);

                }
            }
            catch (HttpException httpEx)
            {
                System.Diagnostics.EventLog.WriteEntry("Spark.Web.Framework.Dispatch", httpEx.Message.ToString(), System.Diagnostics.EventLogEntryType.Error);
            }
        }
    }
}
