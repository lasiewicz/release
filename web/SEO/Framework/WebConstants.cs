
using System;

namespace Spark.Web.Framework
{
	/// <summary>
	/// Summary description for WebConstants.
	/// </summary>
	public class WebConstants
    {
        #region Member Attribute Name Constants
        public const string ATTRIBUTE_NAME_DISPLAYPHOTOSTOGUESTS = "DisplayPhotosToGuests";
        #endregion

        #region Legacy Constants

        // todo: these are all legacy references and can and should be removed
        public const string HTTPIM_ENCRYPT_KEY = "8ffez102";
        public const int PAGE_MEMBER_PROFILE_VIEW_PROFILE_MAIN = 7070;
        public const int PT_PROFILE = 4;


        public const int PT_POPUP_ONCE = 2; // this makes it a popup once, all links on this page don't won't keep state (e.g. home page)
        public const int EMAIL_VERIFIED = 4;	// some form of global status mask
        public const int APP_EMAIL = 27;	// application specific ?
        public const string USERNAME_VERIFICATION_ACCOUNT = "VerifyEmail";		// verification email account 
        public const string USERNAME_ACTIVATION_ACCOUNT = "Activations";		// activiation account 
        public const string USERNAME_SUPPORT_ACCOUNT = "comments";		// support account
        public const int PAGE_MEMBER_SERVICES_SUSPEND = 18020;
        public const string USERNAME_RETURN_ACCOUNT = "communications";
        public const string IMPERSONATE_MEMBERID = "impmid";
        public const string IMPERSONATE_BRANDID = "imppid";
        public const int ADMIN_NOTE_MAX_LENGTH = 3900;
        #endregion

		#region HTML Related Constants
		public const string HTML_FORM_ID = "__aspnetForm";

		public const string IMAGE_PRIVATE_PHOTO_FULL = "privatephoto200x250_ynm4.gif"; //composite of background and text, large size for use in member profile
		public const string IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB = "privatephoto80x104_ynm4.gif"; //background only, does not contain text, for use as thumbnail in searches and profiles (text appears on top).
		public const string IMAGE_PRIVATE_PHOTO_THUMB = "privatephoto80x104_ynm4.jpg"; //composite of background and text, medium size and needs to be a non-interlaced jpg/jpeg for use in Userplane flash movie (and emails)
		public const string IMAGE_PRIVATE_PHOTO_TINY_THUMB = "privatephoto43x56_ym4.gif"; //composite of background and text, small size for use in IM notifier

		public const string IMAGE_NOPHOTO_BACKGROUND_THUMB = "noPhoto.gif"; //background only, does not contain text, for use as thumbnail in searches and profiles (text appears on top).
		public const string IMAGE_NOPHOTO_THUMB = "noPhoto.jpg"; //composite of background and text, medium size and needs to be a non-interlaced jpg/jpeg for use in Userplane flash movie (and emails)
		public const string IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB = "noPhoto_small.gif"; //background only, does not contain text, small size for use in IM notifier

		#endregion
	
		#region Community ID enum
		// todo: remove or clean up if possible
		public enum COMMUNITY_ID : int
		{
			AmericanSingles = 1,
			Glimpse = 2,
			JDate = 3,
			Corp = 8,
			Cupid = 10,
			College = 12,
			Spark = 17
		}
		#endregion

		#region Site ID enum
		// todo: clean this up/remove if possible
		public enum SITE_ID : int
		{
			JDateCoIL = 4,
			MatchnetUK = 6,
			DateCA = 13,
			Cupid = 15,
			Spark = 100,
			AmericanSingles = 101,
			Glimpse = 102,
			JDate = 103,
			Matchnet = 108,
			CollegeLuv = 112,
		}
		#endregion

		#region BrandID enum
		// todo: clean this up/remove if possible
		public enum BRAND_ID : int
		{
			AmericanSingles = 1001,
			Glimpse = 1002,
			JDate = 1003,
			JDateCoIL = 1004,
			MatchnetUK = 1006,
			DateCA = 1013,
			Cupid = 1015,
			Matchnet = 10008,
			CollegeLuv = 10012,
			Spark = 1017,
			Nana = 1933
		}
		#endregion

		#region attribute option enums
		[Flags]
			public enum AttributeOptionHideMask : int
		{
			HideSearch = 1,
			HideHotLists = 2,
			HideMembersOnline = 4,
			HidePhotos = 8
		}

		[Flags]
			[Obsolete("Please use Matchnet.Member.ValueObjects.AttributeOptionMailboxPreference")]
			public enum AttributeOptionMailboxPreference
		{
			IncludeOriginalMessagesInReplies = 1,
			SaveCopiesOfSentMessages = 2
		}
		#endregion

		#region URL Parameter Names
		public const string URL_PARAMETER_NAME_LANDINGPAGEID = "lpid";
		public const string URL_PARAMETER_NAME_BANNERID = "bid";
		public const string URL_PARAMETER_NAME_BRANDALIASID = "plid";
		public const string URL_PARAMETER_NAME_AID = "aid";
		public const string URL_PARAMETER_NAME_PID = "pid";
		public const string URL_PARAMETER_NAME_SID = "sid";		
		public const string URL_PARAMETER_NAME_LUGGAGE = "Luggage";
		public const string URL_PARAMETER_NAME_PLANID = "PlanID";
		public const string URL_PARAMETER_NAME_DOMAINALIASID = "DomainAliasID";
		public const string URL_PARAMETER_NAME_PROMOTIONID = "PromotionID";
		public const string URL_PARAMETER_NAME_LGID = "LGID";
		public const string URL_PARAMETER_NAME_PRM = "PRM";
		public const string URL_PARAMETER_NAME_LAYOUTTEMPLATEID = "LayoutTemplateID";
		public const string URL_PARAMETER_NAME_ENTRYPOINT = "EntryPoint";
		public const string URL_PARAMETER_NAME_SHOWTITLE = "ShowTitle";
		public const string URL_PARAMETER_NAME_HIDELEFTNAV = "HideLeftNav";
		public const string URL_PARAMETER_NAME_SEARCHINITTAB = "SearchInitTab";
		public const string URL_PARAMETER_NAME_SEARCHORDERBY = "SearchOrderBy";
		public const string URL_PARAMETER_NAME_SEARCHTYPEID = "SearchTypeID";
		public const string URL_PARAMETER_NAME_COUNTRYREGIONID = "CountryRegionID";
		public const string URL_PARAMETER_NAME_FIVEDFT = "FiveDFT";
		public const string URL_PARAMETER_NAME_LOGON_PAGE_FLAG = "lgn";
		public const string URL_PARAMETER_NAME_SITEID = "siteID"; // formerly QSPARAM_SITEID = "?siteID=" used to show site specific temporarily unavailable images
		public const string URL_PARAMETER_NAME_MEMBERID = "MemberID"; //for viewprofile
		public const string URL_PARAMETER_NAME_SPARKWS_CONTEXT = "SparkWS"; // = true if it is SparkWS context - for API / SparkWS project.
		public const string URL_PARAMETER_NAME_SPARKWS_CYPHER_OUT = "SPWSCYPHER"; // carries a cypher on a redirect from login in web service API context.
		public const string URL_PARAMETER_NAME_DELETEKEY = "DeleteKey";
		public const string URL_PARAMETER_NAME_RESOURCEFILENAME = "ResourceFileName";
		public const string URL_PARAMETER_NAME_RESOURCEFILEFULLPATH = "ResourceFileFullPath";
		public const string URL_PARAMETER_NAME_IMAGEFILENAME = "ImageFileName";
		public const string URL_PARAMETER_NAME_CATEGORYID = "CategoryID";
		public const string URL_PARAMETER_NAME_MEMBERMAILID = "MemberMailID";

		#endregion
		 
		#region External URL
		public const string HTTP_ASTROLOGY_URL = "http://www.zdki.us/bin/taReportsw.dll/MakeReport?"; 
		#endregion

		#region Ad Regions
		public const string ADREGION_NONE = "none";
		public const string ADREGION_S1 = "SI";
		public const string ADREGION_S2 = "S2";
		public const string ADREGION_L1 = "L1";
		public const string ADREGION_M1 = "M1";
		public const string ADREGION_M2 = "M2";
		public const string ADREGION_M3 = "M3";
		public const string ADREGION_M4 = "M4";
		public const string ADREGION_A1 = "A1";
		public const string ADREGION_A2 = "A2";
		public const string ADREGION_A3 = "A3";
		public const string ADREGION_A4 = "A4";
		public const string ADREGION_V1 = "V1";
		public const string ADREGION_C1 = "C1";
		#endregion

		#region EncKeys
		public const string XOR_SALT_ADD_VALUE = "42";
		public const string SPARK_WS_CRYPT_KEY = "A877C90D";// used to encrypt session id for tranfer over to SparkWS.

		#endregion
	}
}
