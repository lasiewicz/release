﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Web.Mobile.CM.Framework
{
    public static class RestUrls
    {
        public static RestUrl VALIDATE_ACCESS_TOKEN = new RestUrl { Url = "/oauth2/accesstoken/validate/?access_token={accesstoken}&applicationId={applicationId}", Version = "version=V3.0" };
        public static RestUrl MEMBERS_ONLINE_COUNT = new RestUrl { Url = "/membersonline/count" };
        public static RestUrl MEMBERS_ONLINE_LIST = new RestUrl { Url = "/membersonline/list" };
        public static RestUrl MAIL_FOLDER = new RestUrl { Url = "/mail/folder/{folderId}/pagesize/{pageSize}/page/{pageNumber}/{filter}", Version = "version=V3.2" };
        public static RestUrl MAIL_MESSAGE = new RestUrl { Url = "/mail/message/{messageId}/{folderId}/(markAsRead}", Version = "version=V3.2" };
        public static RestUrl PROFILE_ATTRIBUTES = new RestUrl { Url = "/profile/attributeset/{attributeSetName}/{targetMemberId}", Version = "version=V3.3" };
        public static RestUrl HOTLIST = new RestUrl { Url = "/hotlist/{hotListCategory}/pagesize/{pageSize}/pagenumber/{page}", Version = "version=V3.2" };
        //public static RestUrl MATCH_RESULT = new RestUrl { Url = "/match/results/pagesize/{pageSize}/page/{page}", Version = "version=V3.0"};
        public static RestUrl MATCH_RESULT = new RestUrl { Url = "/match/results/pagesize/{pageSize}/page/{page}" , Version = "version=V3.0"};
        public static RestUrl MATCH_PREFERENCES = new RestUrl { Url = "/match/preferences", Version = "version=V3.0" };
        public static RestUrl SEARCH_RESULTS = new RestUrl { Url = "/search/results", Version = "version=V3.0" };
        public static RestUrl PHOTOS_LIST = new RestUrl { Url = "/profile/photos/{targetMemberId}", Version = "version=V3.0" };
        public static RestUrl POST_PAYMENT = new RestUrl { Url = "/subscription/PaymentUiRedirectData", Version = "version=V3.0" };
        public static RestUrl TRANSACTION_INFO = new RestUrl { Url = "/subscription/history", Version = "version=V3.0" };
        public static RestUrl SUBSCRIPTION_PLAN_STATUS = new RestUrl { Url = "/subscription/plan", Version = "version=V3.0" };
        public static RestUrl SUBSCRIPTION_PLAN_RESPONSE = new RestUrl { Url = "/subscription/plan", Version = "version=V3.0" };
        public static RestUrl ACCESS_PRIVILEGE_RESPONSE = new RestUrl { Url = "/subscription/accessprivileges", Version = "version=V3.3" };
        public static RestUrl DISABLE_FREE_TRIAL = new RestUrl { Url = "/subscription/freetrial/disable", Version = "version=V3.0" };
        public static RestUrl SELF_SUSPEND = new RestUrl { Url = "/profile/selfSuspend", Version = "version=V3.0" };
        public static RestUrl REACTIVATE_SELF_SUSPEND = new RestUrl { Url = "/profile/ReActivateSelfSuspend", Version = "version=V3.0" };
        public static RestUrl ATTRIBUTE_OPTIONS = new RestUrl { Url = "/content/attributes/options/{attributeName", Version = "version=V3.0" };
        public static RestUrl BILLING_HISTORY = new RestUrl { Url = "/subscription/getBillingHistory", Version = "version=V3.0" };
        public static RestUrl SETTING_VALUE = new RestUrl { Url = "/content/runtimesettingvalues/{setting}", Version = "version=V3.0" };
        public static RestUrl LOOKUP_BY_MEMBER_USERNAME = new RestUrl { Url = "/profile/lookupByMemberUsername/{attributeSetName}/{targetMemberUsername}", Version = "version=V3.0"};
    }
}

