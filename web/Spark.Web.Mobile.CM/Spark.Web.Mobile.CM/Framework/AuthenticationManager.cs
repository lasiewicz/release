﻿using System;
using System.Web;
using System.Configuration;
using System.Globalization;
using System.Collections.Generic;
using System.Net;

using System.Web.Mvc;

using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Framework;



using log4net;
using RestSharp;

using Spark.Web.Mobile.CM.Utilities;
using Spark.Common.RestConsumer.V2.Models;
using Spark.Common.RestConsumer.V2.Models.Mingle.OAuth2;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;

namespace Spark.Web.Mobile.CM.Framework
{
    public class AuthenticationManager
    {
        //private const string COOKIE_STATE_KEY = "mos_state";
        //private const string COOKIE_ISSUB_KEY = "mos_sub";
        //private const string COOKIE_EXPIRATION_KEY = "mos_exp";
        private const string QUERY_ACCESS_TOKEN_KEY = "accesstoken";
        private const string QUERY_STATE_KEY = "state";
        private const string QUERY_SCOPE_KEY = "scope";
        private const string QUERY_ISSUB_KEY = "issub";
        private const int SCOPE_LONG = 90; // In days
        private const int SCOPE_SHORT = 2; // In days

        private static readonly ILog _logger = LogManager.GetLogger(typeof(AuthenticationManager));
        //private HttpContext _context;

        public AuthenticationManager()
        {
            //_context = context;
        }

        /// <summary>
        /// Get login url to Spark Unified Authorization with MOS template.
        /// </summary>
        /// <returns>The url string to Spark Unified Authorization.</returns>
        public string GetLoginUrl()
        {
            // Build login url to SUA.
            var baseUrl = ConfigurationManager.AppSettings["SuaUrl"];
            var template = ConfigurationManager.AppSettings["SuaTemplate"];
            var redirectUrl = ConfigurationManager.AppSettings["RedirectUrl"];
            var clientId = ConfigurationManager.AppSettings["ApplicationId"];
            var state = Randomizer.Generate();
            var url = String.Format("{0}{1}?redirecturl={2}&clientid={3}&scope=long&state={4}&displaymode=full", 
                baseUrl, template, redirectUrl, clientId, state);

            // Save generated state for validation.
            SaveState(state);

            return url;
        }

        public bool IsSuaResponse()
        {
            HttpRequest request = HttpContext.Current.Request;
            if (!String.IsNullOrEmpty(request.QueryString[QUERY_ACCESS_TOKEN_KEY]) && !String.IsNullOrEmpty(request.QueryString[QUERY_STATE_KEY]))
                return true;

            return false;
        }

        public bool HasValidToken() 
        {
            var token = Cookies.GetValue(ApplicationContext.COOKIE_ACCESS_TOKEN_KEY);
            int memberId = 0;

            if (!String.IsNullOrEmpty(token) && Int32.TryParse(Cookies.GetValue(ApplicationContext.COOKIE_MEMBER_ID_KEY), out memberId) && memberId > 0)
                return true;

            return false;
        }

        public bool ValidateSuaResponse() 
        {
            HttpRequest request = HttpContext.Current.Request;
            var accessToken = request.QueryString[QUERY_ACCESS_TOKEN_KEY];
            var state = request.QueryString[QUERY_STATE_KEY];
            var issub = (bool.TrueString.Equals(request.QueryString[QUERY_ISSUB_KEY])) ? "Y" : "N";
            var cookieState = GetState();
            var scope = request.QueryString[QUERY_SCOPE_KEY];
            var duration = (String.Compare(scope, "long", true) == 0) ? 
                SCOPE_LONG : SCOPE_SHORT;
            var domain = Cookies.DefaultDomain;

            Cookies.Delete(ApplicationContext.COOKIE_STATE_KEY);

            // Compare the state to make sure that the redirect came from the originator.
            if (String.Compare(state, cookieState) != 0)
            {
                _logger.Error(String.Format("SUA state did not match with cookie state. (state: {0}, cookie: {1})", state, cookieState));
                return false;
            }

            // Validate the access token and get member id.
            int memberId;
            if (!ValidateAccessToken(accessToken, out memberId))
            {
                _logger.Error(String.Format("Could not validate access token. (token: {0})", accessToken));
                return false;
             
            }

            // Persist token, expiration and subscription status in cookie.
            DateTime expires = DateTime.Now.AddDays(duration);

            Cookies.Add(ApplicationContext.COOKIE_ACCESS_TOKEN_KEY, accessToken, expires, domain);
            Cookies.Add(ApplicationContext.COOKIE_EXPIRATION_KEY, expires.ToString(CultureInfo.InvariantCulture), expires, domain);
            Cookies.Add(ApplicationContext.COOKIE_ISSUB_KEY, issub, expires, domain);
            Cookies.Add(ApplicationContext.COOKIE_MEMBER_ID_KEY, memberId.ToString(), expires, domain);
            Cookies.Add(ApplicationContext.COOKIE_SELFSUSPEND, "false", expires, domain);

            return true;
        }

        public void InvalidateCookie() 
        {
            Cookies.Delete(ApplicationContext.COOKIE_ACCESS_TOKEN_KEY);
            Cookies.Delete(ApplicationContext.COOKIE_MEMBER_ID_KEY);
            Cookies.Delete(ApplicationContext.COOKIE_ISSUB_KEY);
            Cookies.Delete(ApplicationContext.COOKIE_EXPIRATION_KEY);
            Cookies.Delete(ApplicationContext.COOKIE_STATE_KEY);
        }

        private void SaveState(int state)
        {
            Cookies.Add(ApplicationContext.COOKIE_STATE_KEY, state.ToString(), DateTime.Now.AddHours(1), Cookies.DefaultDomain);
        }

        private string GetState()
        {
            var cookie = Cookies.GetValue(ApplicationContext.COOKIE_STATE_KEY, null);
            return cookie;
        }

        private bool ValidateAccessToken(string accessToken, out int memberId)
        {
            memberId = 0;

            var data = new Dictionary<string, string>();
            data.Add("accesstoken", accessToken);
            data.Add("applicationId", ConfigurationManager.AppSettings["ApplicationId"]);
            //data.Add("client_secret", ConfigurationManager.AppSettings["ClientSecret"]);

            var client = ApiClient.GetInstance(String.Empty);
            var response = client.Execute<ResponseWrapper<ValidatedAccessToken>>(RestUrls.VALIDATE_ACCESS_TOKEN, data, null, Method.POST) as ResponseWrapper<ValidatedAccessToken>;
            if (response != null && response.Code != (int)HttpStatusCode.Unauthorized)
            {
                //return the member suspend status 
                memberId = response.Data.MemberId;
                return true;
            }

            // Log any error(s) in case we need to investigate the root cause on why
            // the token validation was unsuccessful.
            if (response != null && response.Error != null)
            {
                _logger.Debug(String.Format("Unable to validate token for member id: {0} - {1}", memberId, response.Error.Message));
            }

            return false;
        }

        public bool ProfileIsSelfSuspend()
        {
            var isSelfSuspend = false;
            var memberId = Int32.Parse(Cookies.GetValue(ApplicationContext.COOKIE_MEMBER_ID_KEY));
            var accessToken = Cookies.GetValue(ApplicationContext.COOKIE_ACCESS_TOKEN_KEY);

            var data = new Dictionary<string, string>();
            data.Add("access_token", accessToken);

            var urlParams = new Dictionary<string, string>();
            urlParams.Add("attributeSetName", "FullProfile");
            urlParams.Add("targetMemberId", memberId.ToString());


            var client = ApiClient.GetInstance(String.Empty);
            var response = client.Execute<ResponseWrapper<FullProfile>>(RestUrls.PROFILE_ATTRIBUTES, urlParams, data, Method.GET);
            if (response != null && response.Code != (int)HttpStatusCode.Unauthorized)
            {
                isSelfSuspend = response.Data.SelfSuspendedFlag;
                if (isSelfSuspend) { Cookies.Add(ApplicationContext.COOKIE_SELFSUSPEND, "true", DateTime.Now.AddDays(SCOPE_LONG), Cookies.DefaultDomain); }
            }
            return isSelfSuspend;
        }
    }
}