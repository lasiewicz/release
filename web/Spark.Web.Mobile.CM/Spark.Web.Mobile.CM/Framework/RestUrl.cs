﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Web.Mobile.CM.Framework
{
    /// <summary>
    /// Class responsible for defining a url, and
    /// version. The apiclient will use this construct api requests
    /// </summary>
    public class RestUrl
    {
        public string Url { get; set; }
        public string Version { get; set; }
    }
}