﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Web.Mobile.CM.Framework
{
    /// <summary>
    /// Provides application scope or share data (such as constants, methods, etc.)
    /// Data that are shared from different classes should be placed here.
    /// </summary>
    public class ApplicationContext
    {
        public static string CONTEXT_SESSION_KEY = "mos_session";
        public static string COOKIE_ACCESS_TOKEN_KEY = "sua_at";
        public static string COOKIE_MEMBER_ID_KEY = "sua_mid";
        public static string COOKIE_ISSUB_KEY = "sua_sub";
        public static string COOKIE_STATE_KEY = "sua_s";
        public static string COOKIE_EXPIRATION_KEY = "sua_ate";
        public static string COOKIE_DOMAIN_KEY = "sua_d";
        public static string COOKIE_OMNI_LOGIN_KEY = "mos_omni_login";
        public static string COOKIE_SELFSUSPEND = "mos_ss";

        //public static Session GetSession
        //{
        //    get
        //    {
        //        return (Session)HttpContext.Current.Items[CONTEXT_SESSION_KEY];
        //    }
        //}
    }
}