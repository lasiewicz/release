﻿using System;

using RestSharp;
using RestSharp.Deserializers;
using Newtonsoft.Json;
using System.Globalization;

namespace Spark.Web.Mobile.CM.Framework
{
    /// <summary>
    /// Custom deserialization class for RestSharp. RestSharp deserialization logic ignores
    /// data attributes (DataMember) therefore some properties will not be assigned a value
    /// becuase it cannot match the JSON field with the actual object's property.
    /// </summary>
    public class RestDeserializer : IDeserializer
    {
        public string RootElement { get; set; }
        public string Namespace { get; set; }
        public string DateFormat { get; set; }
        //public CultureInfo Culture { get; set; }
 
        public RestDeserializer()
        {   
            //Culture = CultureInfo.InvariantCulture;
        }

        public T Deserialize<T>(IRestResponse response) 
        {
            var temp = JsonConvert.DeserializeObject<T>(response.Content);
            return temp;
        }
    }
}