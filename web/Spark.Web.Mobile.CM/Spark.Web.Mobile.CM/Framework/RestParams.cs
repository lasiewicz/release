﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Web.Mobile.CM.Framework
{
    public static class RestParams
    {
        public const string PAGE_SIZE = "pageSize";
        public const string PAGE = "page";
        public const string TARGET_MEMBER_ID = "targetMemberId";
    }
}