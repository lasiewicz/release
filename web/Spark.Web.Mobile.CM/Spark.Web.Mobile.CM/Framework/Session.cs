﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Web.Mobile.CM.Framework
{
    /// <summary>
    /// Class responsible for persisting required data while HTTP request
    /// is being process. This object once create will be added to the 
    /// HttpContext's Items property for use by other classes (controller, models, etc).
    /// </summary>
    public class Session
    {
        public int MemberId { get; set; }
        public bool IsSub { get; set; }
        public bool IsAjaxRequest { get; set; }
        public string AccessToken { get; set; }
    }
}