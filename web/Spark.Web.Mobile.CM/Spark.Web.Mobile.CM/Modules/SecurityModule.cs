﻿using System;
using System.Web;
using System.Configuration;

using log4net;

using Spark.Web.Mobile.CM.Utilities;
using Spark.Web.Mobile.CM.Framework;
using Spark.Web.Mobile.CM.Configurations;

namespace Spark.Web.Mobile.CM.Modules
{
    public class SecurityModule : IHttpModule
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(SecurityModule));
        private static readonly AllowedPageSection _allowedPageConfiguration = (AllowedPageSection)System.Configuration.ConfigurationManager.GetSection("allowedPage");
        private AuthenticationManager _authenticationManager = new AuthenticationManager();

        public void Dispose()
        {
            //clean-up code here.
        }

        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += new EventHandler(OnAuthenticateRequest);
            //context.BeginRequest += new EventHandler(OnBeginRequest);
        }

        public void OnAuthenticateRequest(Object source, EventArgs e)
        {
            if (HttpContext.Current.Request.Path.IndexOf(".") != -1 || HttpContext.Current.Request.Path.IndexOf("healthcheck") != -1)
                return;

            var url = HttpContext.Current.Request.Url.AbsolutePath;
            if (IsAllowed(url))
            {
                // If valid token exist, create the Session object since some pages,
                // required the Session when logged in.
                if (_authenticationManager.HasValidToken())
                    CreateSession();

                return;
            }

            var cookie = HttpContext.Current.Request.Cookies.Get(ApplicationContext.COOKIE_ACCESS_TOKEN_KEY);
            var memberIDCookie = HttpContext.Current.Request.Cookies.Get(ApplicationContext.COOKIE_MEMBER_ID_KEY);


            // Determine if cookie exist, if not authenticate user.
            if (cookie == null || String.IsNullOrEmpty(cookie.Value)) {
                Authenticate();
                // put 'M' for Manual login from SUA
                Cookies.Add(ApplicationContext.COOKIE_OMNI_LOGIN_KEY, "M");
                return;
            }
            else if (memberIDCookie == null || String.IsNullOrEmpty(memberIDCookie.Value))
            {
                //CM FWS and old mos don't set a MID cookie, so environment jumping causes 
                //a redirect loop. If they have a sua at but no memberID then authenticate them.
                Authenticate();
                // put 'M' for Manual login from SUA
                Cookies.Add(ApplicationContext.COOKIE_OMNI_LOGIN_KEY, "M");
                return; 
            }
            else if (_authenticationManager.HasValidToken())
            {
                // Have auth token and auto login
                Cookies.Add(ApplicationContext.COOKIE_OMNI_LOGIN_KEY, "A");
            }

            //check for self suspended profile, all api calls will fail with the exception of profile, validate and unsuspend
            //if they are a self suspend, redirect to unsuspend page
            if (!(HttpContext.Current.Request.Path == "/home/selfsuspend" || HttpContext.Current.Request.Path == "/settings/unsuspend" || HttpContext.Current.Request.Path == "/logout"))
            {
                if (_authenticationManager.ProfileIsSelfSuspend())
                {
                    RedirectSelfSuspend();
                }
            }

            CreateSession();
        }

        private void Authenticate()
        {
            var isAjaxRequest = IsAjaxRequest();
            var suaResponse = _authenticationManager.IsSuaResponse();

            if (!suaResponse && !isAjaxRequest)
                RedirectLogin();

            if (suaResponse && !_authenticationManager.ValidateSuaResponse())
            {                
                RedirectLogin();
            }                

            if (isAjaxRequest) {
                HandleAjaxAuthentication();

                return;
            }

            //check for self suspended profile, all api calls will fail with the exception of profile, validate and unsuspend
            //if they are a self suspend, redirect to unsuspend page
            if (_authenticationManager.ProfileIsSelfSuspend())
            {
                RedirectSelfSuspend();
            }

            CreateSession();
        }

        private void CreateSession() 
        {
            try
            {
                // Cookie has been created and set at this point. Pass session data to current context.  
                HttpContext.Current.Items[ApplicationContext.CONTEXT_SESSION_KEY] = new Session
                {
                    MemberId = Int32.Parse(Cookies.GetValue(ApplicationContext.COOKIE_MEMBER_ID_KEY)),
                    AccessToken = Cookies.GetValue(ApplicationContext.COOKIE_ACCESS_TOKEN_KEY),
                    IsSub = (String.Compare(Cookies.GetValue(ApplicationContext.COOKIE_ISSUB_KEY), "Y", true) == 0)
                };
            } 
            catch(Exception ex) 
            {
                // In case exception occur when creating session, log error then redirect user to SUA.
                _logger.Error("Unable to create session.", ex);
                RedirectLogin();
            }
        }

        private bool IsAllowed(string url)
        {
            var temp = (url.Substring(url.Length - 1, 1) == "/") ? url.Substring(0, url.LastIndexOf("/")) : url;

            foreach (AllowedPageElement page in _allowedPageConfiguration.Pages)
            {
                if (String.Compare(page.Url, temp) == 0)
                    return true;
            }

            return false;
        }

        private bool IsAjaxRequest()
        {
            return HttpContext.Current.Request.Headers["X-Requested-With"] != null && 
                HttpContext.Current.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
        }

        private void HandleAjaxAuthentication() 
        { 
        }

        private void RedirectLogin() 
        {
            HttpContext.Current.Response.Redirect(_authenticationManager.GetLoginUrl());
        }

        private void RedirectSelfSuspend()
        {
            HttpContext.Current.Response.Redirect("/home/selfsuspend");
        }
    }
}
