﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Web.Mobile.CM.Exceptions
{
    public class AuthorizationException : Exception
    {
        private int _errorCode;
        public int ErrorCode {
            get 
            {
                return _errorCode;
            }
            set
            {
                _errorCode = value; 
            }

        }

        public AuthorizationException(string message) : base(message)
        {

        }

        public AuthorizationException(int errorCode, string message)
            : base(message)
        {
            _errorCode = errorCode;
        }
    }
}