﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Web.Mobile.CM.Exceptions
{
    public class ApiException : Exception
    {
        public int HttpStatusCode { get; set; }
        public int ErrorCode { get; set; }
        public Type Type { get; set; }

        public ApiException(string message) : base(message)
        {

        }
    }
}