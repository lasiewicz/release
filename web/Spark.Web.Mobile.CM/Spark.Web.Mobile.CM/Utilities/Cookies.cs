﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Reflection;
using System.Configuration;
using System.ComponentModel;

namespace Spark.Web.Mobile.CM.Utilities
{
    /// <summary>
    /// Utility class use for manipulating HttpCookie.
    /// </summary>
    public static class Cookies
    {
        private const string DEFAULT_COOKIE_PATH = "/";

        private static readonly string _defaultCookieDomain = ConfigurationManager.AppSettings["Domain"];
        private static readonly DateTime _defaultCookieExpires = DateTime.Now.AddDays(30); // In days.

        public static DateTime DefaultExpires 
        {
            get 
            {
                return _defaultCookieExpires;
            }
        }

        public static string DefaultDomain 
        {
            get
            {
                return _defaultCookieDomain;
            }
        }

        /// <summary>
        /// Add new cookie to the collection.
        /// </summary>
        /// <param name="name">Name of cookie.</param>
        /// <param name="value">Value of the cookie.</param>
        /// <param name="expires">Expiration date of the cookie (in hours).</param>
        /// <param name="domain">Domain of the cookie.</param>
        /// <param name="path">Path of the cookie.</param>
        /// <returns>Newly created cookie.</returns>
        public static HttpCookie Add(string name, string value, DateTime? expires, string domain, string path)
        {
            HttpCookie cookie = new HttpCookie(name, value);

            if(expires != null)
                cookie.Expires = (DateTime)expires;
            if (!String.IsNullOrEmpty(domain))
                cookie.Domain = domain;
            if (!String.IsNullOrEmpty(path))
                cookie.Path = path;

            HttpContext.Current.Response.Cookies.Add(cookie);
            return cookie;
        }

        public static HttpCookie Add(string name, string value, DateTime? expires, string domain)
        {
            HttpCookie cookie = new HttpCookie(name, value);

            if (expires != null)
                cookie.Expires = (DateTime)expires;
            if (!String.IsNullOrEmpty(domain))
                cookie.Domain = domain;

            HttpContext.Current.Response.Cookies.Add(cookie);
            return cookie;
        }

        public static HttpCookie Add(string name, string value, DateTime expires)
        {
            return Add(name, value, expires, null, null);
        }

        public static HttpCookie Add(string name, string value)
        {
            return Add(name, value, null, null, null);
        }

        public static HttpCookie Add(string name, Dictionary<string, string> values, DateTime? expires, string domain, string path) 
        {
            if (values == null || values.Count == 0)
                return null;

            HttpCookie cookie = new HttpCookie(name);
            if (expires != null)
                cookie.Expires = (DateTime)expires;
            if (!String.IsNullOrEmpty(domain))
                cookie.Domain = domain;
            if (!String.IsNullOrEmpty(path))
                cookie.Path = path;

            foreach (var pair in values) 
                cookie.Values.Add(pair.Key, pair.Value);

            HttpContext.Current.Response.Cookies.Add(cookie);
            return cookie;
        }

        public static HttpCookie Add(string name, Dictionary<string, string> values, DateTime expires)
        {
            return Add(name, values, expires, null, null);
        }

        public static HttpCookie Add(string name, Dictionary<string, string> values)
        {
            return Add(name, values, null, null, null);
        }

        public static HttpCookie Add<T>(string name, T value, DateTime? expires, string domain, string path) 
        {
            HttpCookie cookie = new HttpCookie(name);
            if (expires != null)
                cookie.Expires = (DateTime)expires;
            if (!String.IsNullOrEmpty(domain))
                cookie.Domain = domain;
            if (!String.IsNullOrEmpty(path))
                cookie.Path = path;

            PropertyInfo[] props = value.GetType().GetProperties();
            foreach (var prop in props) 
            {
                var temp = prop.GetValue(value) ?? "";
                cookie.Values.Add(prop.Name, temp.ToString());
            }

            HttpContext.Current.Response.Cookies.Add(cookie);
            return cookie;
        }

        public static HttpCookie Add<T>(string name, T value, DateTime? expires)
        {
            return Add<T>(name, value, expires, null, null);
        }

        public static HttpCookie Add<T>(string name, T value)
        {
            return Add<T>(name, value, null, null, null);
        }

        public static string GetValue(string name) 
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
            return cookie != null ? cookie.Value : null;
        }

        public static string GetValue(string name, string defaultValue)
        {
            var temp = GetValue(name);
            return temp != null ? temp : defaultValue;
        }

        public static Dictionary<string, string> GetValues(string name)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
            if (cookie == null)
                return null;

            Dictionary<string, string> values;
            if (!cookie.Values.HasKeys())
                return null;
            
            values = new Dictionary<string, string>();
            foreach (var key in cookie.Values.AllKeys)
                values.Add(key, cookie.Values[key]);

            return values;
        }

        public static void Delete(string name)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
            if (cookie == null)
                return;

            cookie.Expires = DateTime.Now.AddDays(-10);
            cookie.Path = DEFAULT_COOKIE_PATH;
            cookie.Domain = DefaultDomain;
            cookie.Value = null;

            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void Update(string name, string value, string domain)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
            if (cookie == null)
                return;

            cookie.Path = DEFAULT_COOKIE_PATH;
            if(!String.IsNullOrEmpty(domain))
                cookie.Domain = domain;
            cookie.Value = value;

            //HttpContext.Current.Response.Cookies.Add(cookie);
            HttpContext.Current.Response.SetCookie(cookie);
        }

        public static void Update(string name, string value) 
        {
            Update(name, value, null);
        }
    }
}