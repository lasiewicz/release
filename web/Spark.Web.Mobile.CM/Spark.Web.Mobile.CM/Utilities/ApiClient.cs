﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using log4net;
using RestSharp;
using Spark.Web.Mobile.CM.Framework;

namespace Spark.Web.Mobile.CM.Utilities
{
    public class ApiClient
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(ApiClient));
        private static readonly ApiClient _instance = new ApiClient();

        private static string DefaultAPIVersion = "version=V3.0";

        private string _accessToken;

        public static ApiClient GetInstance(string accessToken) 
        {
            _instance._accessToken = accessToken;
            return _instance;
        }

        private ApiClient()
        {
        }

        public string Execute(RestUrl url, Dictionary<string, string> urlParams, Dictionary<string, string> data, Method method, bool addSecret = false)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["SparkApiUrl"]);
            var request = new RestRequest(UrlBuilder.Build(url.Url), method);
            var versionHeader = url.Version ?? DefaultAPIVersion;
            
            request.AddHeader("Accept", versionHeader);

            AddParameters(request, data);
            AddUrlSegments(request, urlParams);
            AddAccessToken(request, method);
            if (addSecret) AddAppIdAndSecret(request, method);

            SetSslIgnore();

            var response = client.Execute(request);
            return response.Content;
        }

        public T Execute<T>(RestUrl url, Dictionary<string, string> urlParams, Dictionary<string, string> data, Method method, bool addSecret = false) where T : new()
        {
            var client = new RestClient(ConfigurationManager.AppSettings["SparkApiUrl"]);
            var request = new RestRequest(UrlBuilder.Build(url.Url), method);
            var versionHeader = url.Version ?? DefaultAPIVersion;

            request.AddHeader("Accept", versionHeader);

            client.AddHandler("application/json", new RestDeserializer());

            AddParameters(request, data);
            AddUrlSegments(request, urlParams);
            AddAccessToken(request, method);
            if (addSecret) AddAppIdAndSecret(request, method);


            SetSslIgnore();

            // For debugging purpose only!!!
            _logger.Debug(client.BuildUri(request).AbsoluteUri);
            _logger.Debug("Version Header:" + versionHeader);

            var response = client.Execute<T>(request);

            _logger.Debug(String.Format("api response: {0}", response.Content));
            return response.Data;
        }

        public T ExecuteJson<T>(RestUrl url, Dictionary<string, string> urlParams, Object data, Method method, bool addSecret = false) where T : new()
        {
            var client = new RestClient(ConfigurationManager.AppSettings["SparkApiUrl"]);
            var request = new RestRequest(UrlBuilder.Build(url.Url), method);
            var versionHeader = url.Version ?? DefaultAPIVersion;

            request.AddHeader("Accept", versionHeader);
            request.AddHeader("Content-type", "application/json");

            client.AddHandler("application/json", new RestDeserializer());
           

            
            AddUrlSegments(request, urlParams);
            AddAccessToken(request, method);
            if (addSecret) AddAppIdAndSecret(request, method);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(data);

            SetSslIgnore();

            var response = client.Execute<T>(request);
            return response.Data;
        }

        private void AddParameters(RestRequest request, Dictionary<string, string> data) 
        {
            if (data == null)
                return;

            foreach (var param in data)
                request.AddParameter(param.Key, param.Value);
        }

        private void AddParametersJson(RestRequest request, string data)
        {
            if (data == null)
                return;
            request.AddParameter("text/json", data, ParameterType.RequestBody);
        }

        private void AddUrlSegments(RestRequest request, Dictionary<string, string> urlParams)
        {
            if (urlParams == null)
                return;

            foreach (var param in urlParams)
                request.AddUrlSegment(param.Key, param.Value);
        }

        private void AddAccessToken(RestRequest request, Method method) 
        {
            Parameter parameter = request.Parameters.Find(delegate(Parameter param) {
                return (String.Compare(param.Name, "access_token", true) == 0 || 
                    String.Compare(param.Name, "accesstoken", true) == 0);
            });

            if (parameter != null)
                return;

            request.AddParameter("access_token", _accessToken);
            if (method == Method.POST || method == Method.PUT)
            {
                var url = request.Resource + "?access_token={token}";

                request.Parameters.RemoveAt(request.Parameters.Count - 1);
                request.Resource = url;
                request.AddUrlSegment("token", _accessToken);
            }
        }

        private void SetSslIgnore()
        {
            // ignore ssl errors from using self ssl in dev/stage envs.
            if (ConfigurationManager.AppSettings["EnvironmentHost"] != "www" && ConfigurationManager.AppSettings["EnvironmentHost"] != "preprod")
            {
                ServicePointManager.ServerCertificateValidationCallback +=
                    (sender, certificate, chain, sslPolicyErrors) => true;
            }
        }

        private void AddAppIdAndSecret(RestRequest request, Method method)
        {
            var url = request.Resource + "?client_secret={clientSecret}&applicationId={appID}";
            request.Resource = url;
            request.AddUrlSegment("appID", ConfigurationManager.AppSettings["ApplicationId"]);
            request.AddUrlSegment("clientSecret", ConfigurationManager.AppSettings["ClientSecret"]);
        }
    }
}