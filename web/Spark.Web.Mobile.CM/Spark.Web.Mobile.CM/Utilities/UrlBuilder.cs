﻿using System;
using System.Configuration;
using System.Collections.Generic;

using Spark.Web.Mobile.CM.Framework;

namespace Spark.Web.Mobile.CM.Utilities
{
    public class UrlBuilder
    {
        //private static string _baseUrl = ConfigurationManager.AppSettings["SparkApiUrl"];
        private static string _baseFragment = "{brandId}";
        private static int _brandId = Int32.Parse(ConfigurationManager.AppSettings["BrandId"]);

        public static string Build(string urlFragment)
        {
            var url = _baseFragment.Replace("{brandId}", _brandId.ToString()) + urlFragment;
            //url = url + "?access_token={token}";

            return url;
        }
    }
}