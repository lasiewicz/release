﻿using System;
using System.Web;

namespace Spark.Web.Mobile.CM.Utilities
{
    public class Randomizer
    {
        public static int Generate()
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            return random.Next();
        }
    }
}