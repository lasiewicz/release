﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.Web.Mobile.CM.Framework;
using Spark.Web.Mobile.CM.Utilities;
using Spark.Web.Mobile.CM.ViewData;

namespace Spark.Web.Mobile.CM.Controllers
{
    [AllowAnonymous]
    public class HomeController : BaseController
    {
        private HomeViewData _viewData;
       

        public HomeController()
        {
            _viewData = new HomeViewData();
            _viewData.ShowNavPanel = IsAuthenticated;
        }

        public ActionResult SelfSuspend()
        {
            _viewData.PageId = "selfsuspend_page";
            _viewData.PageUrl = "/home/selfsuspend";
            _viewData.Title = "Self Suspended";
            _viewData.Session = Session;
            _viewData.ShowNavPanel = false;
            //_viewData.ShowFooter = false;
            //_viewData.ShowHeader = false;
            return View(_viewData);
        }

        public ActionResult ContactUs()
        {
            _viewData.PageId = "contactus_page";
            _viewData.PageUrl = "/home/contactus";
            _viewData.Title = "Contact Us";
            _viewData.Session = Session;
            return View(_viewData);
            
        }

        public ActionResult ImRedirect()
        {
            _viewData.PageId = "im_redirect";
            _viewData.PageUrl = "/home/imredirect";
            _viewData.Title = "IM Redirect";
            _viewData.Session = Session;
            return View(_viewData);
        }

        public ActionResult Safety()
        {
            _viewData.PageId = "safety_page";
            _viewData.PageUrl = "/home/safety";
            _viewData.Title = "Safety";
            _viewData.Session = Session;
            if (Cookies.GetValue(ApplicationContext.COOKIE_SELFSUSPEND) == "true") { _viewData.ShowNavPanel = false; }
            return View(_viewData);
        }

        public ActionResult Privacy()
        {
            _viewData.PageId = "privacy_page";
            _viewData.PageUrl = "/home/privacy";
            _viewData.Title = "Privacy";
            _viewData.Session = Session;
            return View(_viewData);
        }

        public ActionResult TermsOfService()
        {
            _viewData.PageId = "termsofservice_page";
            _viewData.PageUrl = "/home/termsofservice";
            _viewData.Title = "Terms of Service";
            _viewData.Session = Session;
            return View(_viewData);

        }

        public ActionResult CookiePolicy()
        {
            _viewData.PageId = "cookiepolicy_page";
            _viewData.PageUrl = "/home/cookiepolicy";
            _viewData.Title = "Terms of Service";
            _viewData.Session = Session;
            return View(_viewData);
        }

        //public ActionResult MobileApp()
        //{
        //    _viewData.PageId = "mobile_app_page";
        //    _viewData.PageUrl = "/mobile";
        //    _viewData.Title = "Mobile Apps";
        //    _viewData.Session = Session;
        //    return View(_viewData);
        //}
	}
}