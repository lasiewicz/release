﻿using System;
using System.Web.Mvc;

using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Framework;


namespace Spark.Web.Mobile.CM.Controllers
{
    [AllowAnonymous]
    public class LoginController : BaseController
    {
        private AuthenticationManager _authenticationManager = new AuthenticationManager();

        public ActionResult Login()
        {
            // Invalidate session cookie.
            _authenticationManager.InvalidateCookie();

            LoginViewData viewData = new LoginViewData();
            return View(viewData);
        }

        public void Logon()
        {
            if (!IsAuthenticated)
                 Logout();              
        }

        public void Logout()
        {
            // Invalidate session cookie then redirect to SUA.
            _authenticationManager.InvalidateCookie();
            Response.Redirect(_authenticationManager.GetLoginUrl());
        }
    }
}