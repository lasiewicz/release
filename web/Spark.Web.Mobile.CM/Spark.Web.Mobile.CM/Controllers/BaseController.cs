﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

using Spark.Web.Mobile.CM.Utilities;
using Spark.Web.Mobile.CM.Filters;
using Spark.Web.Mobile.CM.Framework;
using Spark.Web.Mobile.CM.Models;

namespace Spark.Web.Mobile.CM.Controllers
{
    [SessionRequired]
    public class BaseController : Controller
    {
        protected const int DEFAULT_PAGE_SIZE = 25;

        public int MembersOnlineCount 
        {
            get 
            {
                var model = new MembersOnlineModel();
                return model.GetMembersOnlineCount().Count;
            }
        }

        public new Session Session 
        {
            get
            {
                return (Session)HttpContext.Items[ApplicationContext.CONTEXT_SESSION_KEY];
            }
        }

        public bool IsAjaxRequest
        {
            get
            {
                return HttpContext.Request.IsAjaxRequest();
            }
        }

        public bool IsAuthenticated 
        {
            get 
            {
                var authenticationManager = new AuthenticationManager();
                return authenticationManager.HasValidToken();
            }
        }

        public string SiteUrl 
        {
            get 
            {
                return ConfigurationManager.AppSettings["SiteUrl"];
            }
        }

        public string MessageAttachmentURLPath
        {
            get
            {
                return ConfigurationManager.AppSettings["MessageAttachmentURLPath"];
            }
        }
	}
}