﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Models;
using Spark.Web.Mobile.CM.Filters;

namespace Spark.Web.Mobile.CM.Controllers
{
    [AuthorizationLogging]
    [ErrorLogging]
    public class MembersOnlineController : BaseController
    {

        [HttpGet]
        public ActionResult MembersOnline(int? page)
        {
            //var model = new MembersOnlineModel();
            //var membersOnline = model.GetMembersOnline(DEFAULT_PAGE_SIZE, page.GetValueOrDefault(1));

            MembersOnlineViewData viewData = new MembersOnlineViewData() { 
                CssClass = "mol-page",
                //Members = membersOnline.Members,
                PageId = "membersonline_page",
                PageSize = DEFAULT_PAGE_SIZE,
                PageUrl = "/membersonline",
                Session = Session,
                Title = "Members Online" 
            };

            return View(viewData);
        }
	}
}