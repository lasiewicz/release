﻿using System;
using System.Web.Mvc;

using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Models;
using Spark.Web.Mobile.CM.Filters;

namespace Spark.Web.Mobile.CM.Controllers
{
    [AuthorizationLogging]
    [ErrorLogging]
    public class MatchesController : BaseController
    {
        public ActionResult Matches(int? page)
        {
            //var model = new MatchesModel();
            //var results = model.GetMatches(DEFAULT_PAGE_SIZE, page.GetValueOrDefault(1));

            var viewData = new MatchesViewData() {
                PageId = "matches_page",
                PageSize = DEFAULT_PAGE_SIZE,
                //Results = results,
                Session = Session,
                Title = "Matches"
            };
            return View(viewData);
        }
	}
}