﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Spark.Web.Mobile.CM.Utilities;
using Spark.Web.Mobile.CM.ViewData;

namespace Spark.Web.Mobile.CM.Controllers
{
    public class ErrorController : BaseController
    {
        //private ErrorViewData _viewData = new ErrorViewData();
        public ActionResult Error() 
        {
            //_viewData.PageId = "error_page";
            //_viewData.PageSize = DEFAULT_PAGE_SIZE;
            //_viewData.PageUrl = "/error";
            ////_viewData.Session = Session;
            //_viewData.Title = "Error";

            //if (Session != null)
            //{
            //    _viewData.Session = Session;
            //}
            //else
            //{
            //    _viewData.ShowNavPanel = false;
            //    _viewData.ShowFooter = false;
            //    _viewData.ShowHeader = false;
            //}
            //return View(_viewData);
            return View();
        }

        public ActionResult PageNotFound() 
        {
             //return View(new ErrorViewData());
            return View();
                 
        }
	}
}