﻿using System;
using System.Web.Mvc;
using System.Web.WebSockets;
using Spark.Web.Mobile.CM.Models;
using Spark.Web.Mobile.CM.ViewData;

namespace Spark.Web.Mobile.CM.Controllers
{
    public class IMController : BaseController
    {
        public ActionResult InstantMessages()
        {
            var viewData = new IMViewData();

                viewData.PageId = "chat_page";
                viewData.Title = "Chats";

            return View(viewData);
        }
    }
}