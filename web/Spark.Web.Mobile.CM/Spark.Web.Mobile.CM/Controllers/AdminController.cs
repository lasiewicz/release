﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spark.Web.Mobile.CM.Controllers
{
    public class AdminController : Controller
    {
        private const string SERVER_DOWN_MESSAGE = "Matchnet Server Down";
        private const string SERVER_ENABLED_MESSAGE = "Matchnet Server Enabled";

        public ActionResult HealthCheck()
        {
            var serverStatus = SERVER_DOWN_MESSAGE;

            try
            {
                var filePath = Path.GetDirectoryName(Server.MapPath("/")) + @"\deploy.txt";
                var fileExists = System.IO.File.Exists(filePath);

                if (!fileExists)
                    serverStatus = SERVER_ENABLED_MESSAGE;
            }
            catch (Exception)
            {
                serverStatus = SERVER_DOWN_MESSAGE;
            }
            finally
            {
                Response.AddHeader("Health", serverStatus);
                Response.Write(serverStatus);
                Response.End();
            }

            return Content(serverStatus);
        }
	}
}