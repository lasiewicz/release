﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Web.UI.WebControls;
using System.Configuration;

using log4net;

using Spark.Web.Mobile.CM.ViewData;
using Spark.Common.RestConsumer.V2.Models.Mingle.Subscription;
using Spark.Common.RestConsumer.V2.Models.Mingle.GAM;
using Spark.Common.RestConsumer.V2.Models;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;
using Spark.Web.Mobile.CM.Models;
using Spark.Web.Mobile.CM.Framework;
using Spark.Common.RestConsumer.V2;
using Spark.Common.RestConsumer.V2.Models.Mingle.Search;
using Spark.Web.Mobile.CM.Filters;
using Spark.Web.Mobile.CM.Utilities;

namespace Spark.Web.Mobile.CM.Controllers
{
    [AuthorizationLogging]
    [ErrorLogging]
    public class SubscriptionController : BaseController
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(SubscriptionController));
        private SubscriptionViewData _viewData = new SubscriptionViewData();
        private ProfileModel _model = new ProfileModel();

        public SubscriptionController()
        {
        }

        [HttpGet]
        public ActionResult Subscribe(string prtid = null, int promoid = 0, string lastapplication = null)
        {
            var context = System.Web.HttpContext.Current;
            var browser = (context == null) ? null : context.Request.Browser;

            // If there is no member id, take user to error screen.
            if (Session.MemberId == 0)
            {
                return View("../Error/Error", new ErrorViewData()
                {
                    Session = Session
                });
            }

            var request = new PaymentUiPostDataRequest
            {
                PaymentUiPostDataType = "1",
                CancelUrl = SiteUrl.ToLower(),
                ConfirmationUrl = SiteUrl.ToLower() + "/subscription/confirm",
                DestinationUrl = SiteUrl.ToLower(),
                ReturnUrl = SiteUrl.ToLower(),
                ClientIp = Request.ServerVariables["REMOTE_ADDR"],
                //PrtTitle = "",
                //SrId = "",
                PrtId = prtid ?? "1249",
                OmnitureVariables = new Dictionary<string, string>(),
                MemberLevelTrackingLastApplication = lastapplication ?? "2",
                PaymentType = 1,
                PromoType = "1", //1: mobile, 0: FWS
               // TemplateId = 121  
            }; 

            if (promoid != null && promoid > 0)
            {
                request.PromoID = promoid;
            }

            //Special case for current android promo, since android payment is a webview.
            //This won't be needed at somepoint since android web view payment will just pass in last application
            _logger.Debug("Last Application:" + request.MemberLevelTrackingLastApplication);

            if (promoid == 4855)
            {
                request.MemberLevelTrackingLastApplication = "4";
            }

            if (browser != null)
            {
                request.MemberLevelTrackingIsMobileDevice = browser.IsMobileDevice.ToString().ToLower();
                request.MemberLevelTrackingIsTablet = (browser["is_tablet"] == null ? "false" : browser["is_tablet"].ToLower());
            }

            // Set Omniture data.
            _viewData.Session = Session;
            if (_viewData.CurrentUserProfile != null)
            {
                request.OmnitureVariables.Add("prop19", _viewData.CurrentUserProfile.Age.ToString());
                request.OmnitureVariables.Add("prop18", _viewData.CurrentUserProfile.Gender);
                request.OmnitureVariables.Add("prop27", _viewData.CurrentUserProfile.SubscriptionStatusGam);
                request.OmnitureVariables.Add("prop22", _viewData.CurrentUserProfile.SubscriptionStatus);
                request.OmnitureVariables.Add("prop21", _viewData.CurrentUserProfile.ZipCode);
                if (_viewData.CurrentUserProfile.RegistrationDate != null)
                {
                    var span = DateTime.Now - (DateTime)_viewData.CurrentUserProfile.RegistrationDate;
                    request.OmnitureVariables.Add("eVar45", span.Days.ToString());
                }


                request.OmnitureVariables.Add("prop23", _viewData.CurrentUserProfile.MemberId.ToString());
            }

            SubscriptionModel model = new SubscriptionModel();
            var paymentData = model.PostPayment(request);

            if (paymentData == null)
                throw new Exception("Unable to get payment data from Subcription.");

            _viewData.PaymentData = paymentData;

            return View(_viewData);
        }

        public ActionResult Premium(string prtid = null)
        {
            var context = System.Web.HttpContext.Current;
            var browser = (context == null) ? null : context.Request.Browser;

            // If there is no member id, take user to error screen.
            if (Session.MemberId == 0)
            {
                return View("../Error/Error", new ErrorViewData()
                {
                    Session = Session
                });
            }

            var request = new PaymentUiPostDataRequest
            {
                PaymentUiPostDataType = "3",
                CancelUrl = SiteUrl.ToLower(),
                ConfirmationUrl = SiteUrl.ToLower() + "/subscription/confirm",
                DestinationUrl = SiteUrl.ToLower(),
                ReturnUrl = SiteUrl.ToLower(),
                ClientIp = Request.ServerVariables["REMOTE_ADDR"],
               // PrtTitle = "",
                //SrId = "",
                PrtId = prtid ?? "1249",
                OmnitureVariables = new Dictionary<string, string>(),
                MemberLevelTrackingLastApplication = "2",
                PaymentType = 1,
                PromoType = "1"
            };

            if (browser != null)
            {
                request.MemberLevelTrackingIsMobileDevice = browser.IsMobileDevice.ToString().ToLower();
                request.MemberLevelTrackingIsTablet = (browser["is_tablet"] == null ? "false" : browser["is_tablet"].ToLower());
            }

            // Set Omniture data.
            _viewData.Session = Session;
            if (_viewData.CurrentUserProfile != null)
            {
                request.OmnitureVariables.Add("prop19", _viewData.CurrentUserProfile.Age.ToString());
                request.OmnitureVariables.Add("prop18", _viewData.CurrentUserProfile.Gender);
                request.OmnitureVariables.Add("prop27", _viewData.CurrentUserProfile.SubscriptionStatusGam);
                request.OmnitureVariables.Add("prop22", _viewData.CurrentUserProfile.SubscriptionStatus);
                request.OmnitureVariables.Add("prop21", _viewData.CurrentUserProfile.ZipCode);
                if (_viewData.CurrentUserProfile.RegistrationDate != null)
                {
                    var span = DateTime.Now - (DateTime)_viewData.CurrentUserProfile.RegistrationDate;
                    request.OmnitureVariables.Add("eVar45", span.Days.ToString());
                }


                request.OmnitureVariables.Add("prop23", _viewData.CurrentUserProfile.MemberId.ToString());
            }

            SubscriptionModel model = new SubscriptionModel();
            var paymentData = model.PostPayment(request);

            if (paymentData == null)
                throw new Exception("Unable to get payment data from Subcription.");

            _viewData.PaymentData = paymentData;

            return View(_viewData);
        }

        public ActionResult HistoryPage()
        {
            Cookies.Add("sofs", "1", null, ConfigurationManager.AppSettings["Domain"], "/");

            string baseUrl = ConfigurationManager.AppSettings["FullWebsiteUrl"];
            return Redirect(String.Format("{0}/Applications/Subscription/History.aspx", baseUrl));
        }

        public ActionResult Cancel(string subscriptionType = null, string enableOrDisable = "disable", int reasonIdOrPremType = 0, bool selfSuspend = false)
        {
            SubscriptionModel model = new SubscriptionModel();


            var subscriptionPlanStatus = model.GetSubscriptionPlanStatus();
            _viewData.SubscriptionPlanStatusData = subscriptionPlanStatus;

            _viewData.Session = Session;
            _viewData.PageId = "confirmation_page";



            if (subscriptionType == "basesubscriptionplan") //user wants to cancel base sub
            {
                var baseSub = new SubscriptionBase()
                {
                    IsEnabled = enableOrDisable == "enable",
                    RenewalRate = subscriptionPlanStatus.BaseSubscriptionPlan.RenewalRate,
                    CurrencyType = subscriptionPlanStatus.BaseSubscriptionPlan.CurrencyType,
                    RenewalDurationType = subscriptionPlanStatus.BaseSubscriptionPlan.RenewalDurationType
                };
                var premiumOption = new SubscriptionPremiumOption()
                {
                    PremiumSubscriptionType = reasonIdOrPremType,
                    IsEnabled = false
                };
                var premiumOptionsList = new List<SubscriptionPremiumOption>()
                {
                    premiumOption
                };
                var request = new SubscriptionPremiumChangeDataRequest()
                {
                    BaseSubscriptionPlan = baseSub,
                    IsSelfSuspended = selfSuspend,
                    //TerminationReasonId = null,
                    PremiumOptions = premiumOptionsList
                };
                var subscriptionPlanStatusChangeResponse = model.UpdateSubscriptionPlanStatus(request);
                _viewData.SubscriptionPlanChangeConfirmation = subscriptionPlanStatusChangeResponse;

                //call the api to get updated sub status post cancelation
                subscriptionPlanStatus = model.GetSubscriptionPlanStatus();
                _viewData.SubscriptionPlanStatusData = subscriptionPlanStatus;

            }

            if (subscriptionType == "premiumoption") //user wants to cancel autorenew on a premium option
            {
                var baseSub = new SubscriptionBase()
                {
                    IsEnabled = true,
                    RenewalRate = subscriptionPlanStatus.BaseSubscriptionPlan.RenewalRate,
                    CurrencyType = subscriptionPlanStatus.BaseSubscriptionPlan.CurrencyType,
                    RenewalDurationType = subscriptionPlanStatus.BaseSubscriptionPlan.RenewalDurationType
                };
                var premiumOption = new SubscriptionPremiumOption()
                {
                    PremiumSubscriptionType = reasonIdOrPremType,
                    IsEnabled = enableOrDisable == "enable"
                };
                var premiumOptionList = new List<SubscriptionPremiumOption>()
                {
                    premiumOption
                };
                var request = new SubscriptionPremiumChangeDataRequest()
                {
                    BaseSubscriptionPlan = baseSub,
                    PremiumOptions = premiumOptionList
                };
                var subscriptionPlanStatusChangeResponse = model.UpdateSubscriptionPlanStatus(request);


                //call the api to get updated sub status post cancelation
                subscriptionPlanStatus = model.GetSubscriptionPlanStatus();
                _viewData.SubscriptionPlanStatusData = subscriptionPlanStatus;
                _viewData.SubscriptionPlanChangeConfirmation = subscriptionPlanStatusChangeResponse;

            }

            if (subscriptionType == "freetrial" && subscriptionPlanStatus.IsFreeTrial) //user wants to cancel free trial
            {
                var disableFreeTrialResponse = model.DisableFreeTrial();
                _viewData.DisableFreeTrialResponse = disableFreeTrialResponse;
            }




            return View(_viewData);
        }

        [HttpGet]
        public ActionResult Confirm(string paymentType = "mos")
        {
            SubscriptionModel model = new SubscriptionModel();
            string viewName = "Confirm";
            //var transactionData = model.GetTransactionInfo(oid);

           

            var transactionData = model.GetTransactionHistory();
            if (transactionData == null)
            {
               // _viewData.TransactionHistoryData = new List<TransactionHistory>();
                throw new Exception("Unable to retrieve transaction data.");
            }


            // Since the member went through a successful subscription process, update the
            // cookie so it'll be marked as a paying member.
            Cookies.Update(ApplicationContext.COOKIE_ISSUB_KEY, "Y", Cookies.DefaultDomain);
            Session.IsSub = true;


            if (paymentType == "android")
            {
                _viewData.ShowNavPanel = false;
                _viewData.ShowFooter = false;
                _viewData.ShowHeader = false;
            }

            _viewData.PaymentType = paymentType;
            _viewData.Session = Session;
            _viewData.PageId = "confirmation_page";
            //_viewData.TransactionData = transactionData;

            _viewData.TransactionHistoryData = transactionData;


            //come back and uncomment this:
          //  var offset = DateTime.Now;
          //  _viewData.TransactionData.TransactionDate = offset;

            var profile = _model.GetFullProfile(Session.MemberId);
            _viewData.FullProfile = profile;

            var searchModel = new SearchModel();
            var results = searchModel.GetSearchResults(3, 1);
            _logger.Debug(String.Format("getting 3 members for the confirmation page:", results));

            if (results != null)
                _viewData.Members = results.Members;

            return View(viewName, _viewData);
        }
        public ActionResult AccountSettings()
        {
            _viewData = new SubscriptionViewData();
            _viewData.Session = Session;
            SubscriptionModel model = new SubscriptionModel();

            if (_viewData.CurrentUserProfile.SubscriptionStatusGam == "never_sub")
            {

            }
            else
            {
                var subscriptionPlanStatus = model.GetSubscriptionPlanStatus();
                _viewData.SubscriptionPlanStatusData = subscriptionPlanStatus;
                _viewData.CancelState = getCancelState(_viewData.CurrentUserProfile, subscriptionPlanStatus);
            }
            
            _viewData.PageId = "account_settings_page";
            _viewData.PageUrl = "/settings/account";
            _viewData.Title = "Account Settings";
            return View(_viewData);
        }

        public ActionResult IAP()
        {
            _viewData.Session = Session;
            _viewData.PageUrl = "/subscription/iap";
            _viewData.PageId = "iap_page";
            _viewData.Title = "IAP";

            return View(_viewData);
        }
        private string getCancelState(FullProfile profile, SubscriptionPlanStatus subscriptionPlan)
        {
            //possible states - noSub, wantsToRenew, wantToCancelSub, wantsToCancelFT
            var CancelState = "noSub";

            if (profile.AdTargeting.sub_status == "ex_sub")
            {
                //check for ex sub, who may have a plan with is enabled false, so instead check substatus gam
                CancelState = "noSub";
            }
            //check for activeSub - wantToCancelSub (sub not null and is active)
            else if (subscriptionPlan.BaseSubscriptionPlan != null && subscriptionPlan.BaseSubscriptionPlan.IsEnabled)
            {
                CancelState = "wantToCancelSub";
                //check for activeFT - wantsToCancelFT (freetrial not null and is active)
            }
            else if (subscriptionPlan.FreeTrialSubscriptionPlan != null && subscriptionPlan.FreeTrialSubscriptionPlan.IsEnabled)
            {
                CancelState = "wantsToCancelFT";
            }
            //check for inactive sub - wantsToRenew (sub not null and not active)
            else if (subscriptionPlan.BaseSubscriptionPlan != null && !subscriptionPlan.BaseSubscriptionPlan.IsEnabled)
            {
                CancelState = "wantsToRenew";
            }


            return CancelState;
        }
    }
}