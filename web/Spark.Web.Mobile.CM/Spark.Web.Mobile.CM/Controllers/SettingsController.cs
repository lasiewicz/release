﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Framework;

using System.Configuration;
using Spark.Web.Mobile.CM.Models;
using Spark.Web.Mobile.CM.Utilities;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;

namespace Spark.Web.Mobile.CM.Controllers
{
    public class SettingsController : BaseController
        {
        private SettingsViewData _viewData;
        public ActionResult Settings()
        {
            _viewData = new SettingsViewData();
            _viewData.ShowNavPanel = IsAuthenticated;
            _viewData.PageId = "settings_page";
            _viewData.PageUrl = "/settings";
            _viewData.Title = "Settings & Preferences";
            _viewData.Session = Session;
            return View(_viewData);
        }

        public ActionResult SpotlightSettings()
        {
            _viewData = new SettingsViewData();
            _viewData.PageId = "spotlight_settings_page";
            _viewData.PageUrl = "/settings/spotlight";
            _viewData.Title = "Spotlight Settings";
            _viewData.Session = Session;
            return View(_viewData);
        }

        public ActionResult MessageSettings() 
        {
            Cookies.Add("sofs", "1", null, ConfigurationManager.AppSettings["Domain"], "/");

            string baseUrl = ConfigurationManager.AppSettings["FullWebsiteUrl"];
            return Redirect(String.Format("{0}/Applications/Email/MessageSettings.aspx", baseUrl));
        }

        public ActionResult SelfSuspendSettings(int reasonId = -1)
        {
            SettingsModel model = new SettingsModel();
            _viewData = new SettingsViewData();
            //ProfileModel _model = new ProfileModel();
            //var memberId = Session.MemberId;
            //var profile = _model.GetFullProfile(memberId);
            if (reasonId >= 0)
            {
                var selfSuspendData = model.SelfSuspend(reasonId);
                _viewData.SelfSuspendData = selfSuspendData;  

                //redirect to suspend page
                return Redirect("/home/selfsuspend");
            }

            _viewData.Session = Session;
            _viewData.PageId = "selfsuspend_settings_page";
            _viewData.PageUrl = "/settings/suspend";

           
            return View(_viewData);

        }

        public ActionResult ReactivateSelfSuspend()
        {
            SettingsModel model = new SettingsModel();
            _viewData = new SettingsViewData();
            _viewData.Session = Session;
            var reactivateSelfSuspendCode = model.ReActivateSelfSuspended();
            _viewData.ReActivateSelfSuspendCode = reactivateSelfSuspendCode;
            Cookies.Update(ApplicationContext.COOKIE_SELFSUSPEND, "false", Cookies.DefaultDomain);
            //redirect to suspend page
            return Redirect("/home");
        }

 
        public ActionResult BlockedMembers()
        {
            _viewData = new SettingsViewData();
            _viewData.PageId = "blockedmembers_settings_page";
            _viewData.PageUrl = "/settings/blockedmembers";
            _viewData.Session = Session;

            return View(_viewData);

        }

        public ActionResult EmailAlerts()
        {
            _viewData = new SettingsViewData();
            _viewData.PageId = "emailalerts_settings_page";
            _viewData.PageUrl = "/settings/emailalerts";
            _viewData.Session = Session;

            return View(_viewData);
        }

       
	}
}   