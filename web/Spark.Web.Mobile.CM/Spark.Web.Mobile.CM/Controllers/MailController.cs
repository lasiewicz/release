﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Models;
using Spark.Common.RestConsumer.V2.Models.Mingle.Mail;
using Spark.Web.Mobile.CM.Filters;

namespace Spark.Web.Mobile.CM.Controllers
{
    [AuthorizationLogging]
    [ErrorLogging]
    public class MailController : BaseController
    {
        private MailViewData _viewData;

        public MailController()
        {
            _viewData = new MailViewData();
        }

        [HttpGet]
        public ActionResult Mail()
        {
            _viewData.Session = Session;
            _viewData.PageSize = DEFAULT_PAGE_SIZE;
            _viewData.PageId = "mail_page";
            _viewData.Title = "Mail";

            return View(_viewData);
        }

        [HttpGet]
        public ActionResult Chat()
        {
            _viewData.Session = Session;
            _viewData.PageSize = DEFAULT_PAGE_SIZE;
            _viewData.PageId = "chat_page";
            _viewData.Title = "Chats";

            return View(_viewData);
        }

        [HttpGet]
        public ActionResult ChatHistory(int memberId)
        {
            _viewData.Session = Session;
            _viewData.PageSize = DEFAULT_PAGE_SIZE;
            _viewData.PageId = "chat_history_page";
            _viewData.Title = "Chat History";

            _viewData.SentImMemberId = memberId;

            return View(_viewData);
        }

        [HttpGet]
        public ActionResult New()
        {
            MailViewData ViewData = new MailViewData() { Title = "Mail - New" };
            return View(ViewData);
        }

        public ActionResult Message(int? messageId)
        {
            var model = new MailModel();
            var message = model.GetMailMessage(messageId.GetValueOrDefault());

            _viewData.Message = message;
            /*_viewData.PageId = String.Format("{0}_{1}_{2}_page",
                ControllerContext.RouteData.Values["controller"].ToString().ToLower(),
                ControllerContext.RouteData.Values["action"].ToString().ToLower(),
                messageId);*/

            _viewData.PageId = "mail_message_page";
            _viewData.PageUrl = "/mail/message/" + messageId.GetValueOrDefault();
            _viewData.Session = Session;

            return View(_viewData);
        }


        private MailFolder GetMailFolder(int folder, int pageSize, int page) 
        {
            MailModel model = new MailModel();
            return model.GetMailFolder(folder, pageSize, page);
        }

        private string GetPageId() 
        {
            var values = ControllerContext.RouteData.Values;
            return String.Format("{0}_{1}_page", values["controller"].ToString().ToLower(), 
                values["action"].ToString().ToLower());
        }
	}
}