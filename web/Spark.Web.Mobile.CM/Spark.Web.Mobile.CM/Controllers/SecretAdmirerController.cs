﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Models;
using Spark.Web.Mobile.CM.Filters;

namespace Spark.Web.Mobile.CM.Controllers
{
    [AuthorizationLogging]
    [ErrorLogging]
    public class SecretAdmirerController : BaseController
    {
        private SecretAdmirerViewData _viewData = new SecretAdmirerViewData();

        public ActionResult SecretAdmirer(int? page)
        {
            //var model = new SecretAdmirerModel();
            //var results = model.GetSecretAdmirers(DEFAULT_PAGE_SIZE, page.GetValueOrDefault(1));
            _viewData.PageId = "secret_admirer_page";
            _viewData.PageUrl = "/secretadmirer";
            _viewData.Session = Session;

            return View(_viewData);
        }

        public ActionResult Settings()
        {
            _viewData.PageId = "secret_admirer_settings_page";
            _viewData.PageUrl = "/secretadmirer/settings";
            _viewData.Session = Session;

            return View(_viewData);
        }

        public ActionResult Results(string type)
        {
            _viewData.PageId = "secret_admirer_results_page";
            _viewData.PageUrl = "/secretadmirer/results/" + type;
            _viewData.ResultType = type;
            _viewData.Session = Session;
           //_viewData.PageCache = "true";

            return View(_viewData);
        }
	}
    
}