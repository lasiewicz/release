﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Models;
using Spark.Web.Mobile.CM.Filters;

namespace Spark.Web.Mobile.CM.Controllers
{
    [AuthorizationLogging]
    [ErrorLogging]
    public class HotListController : BaseController
    {
        public ActionResult Favorites(int? page)
        {
            var model = new HotListModel();
            var hotLists = model.GetHotList(HotListModel.HotListCategory.Default, 
                DEFAULT_PAGE_SIZE, page.GetValueOrDefault(1));

            HotListViewData viewData = new HotListViewData() { 
                HotLists = hotLists,
                PageId = "favorites_page",
                PageSize = DEFAULT_PAGE_SIZE,
                PageUrl = "/hotlist/favorites",
                Session = Session,
                Title = "Favorites" 
            };
            return View(viewData);
        }
	}
}