﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Models;
using Spark.Web.Mobile.CM.Filters;

namespace Spark.Web.Mobile.CM.Controllers
{
    [AuthorizationLogging]
    [ErrorLogging]
    public class SearchController : BaseController
    {
        private SearchModel _model = new SearchModel();
        private SearchViewData _viewData = new SearchViewData();

        public ActionResult Preferences()
        {
            var preferences = _model.GetMatchPreferences();
            _viewData.Session = Session;
            _viewData.PageId = "search_page";
            _viewData.Title = "Search";
            _viewData.Preferences = preferences;
            var matchPreferences = _model.GetFullMatchPreferences();
            _viewData.MatchPreferences = matchPreferences;

            return View(_viewData);
        }
	}
}