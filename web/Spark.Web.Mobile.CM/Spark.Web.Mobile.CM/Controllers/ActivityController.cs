﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Spark.Web.Mobile.CM.Utilities;
using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Filters;

namespace Spark.Web.Mobile.CM.Controllers
{
    [AuthorizationLogging]
    [ErrorLogging]
    public class ActivityController : BaseController
    {
        private ActivityViewData _viewData = new ActivityViewData();

        public ActionResult Activity(string type, string filter) 
        {
            _viewData.PageId = "activity_page";
            _viewData.PageSize = DEFAULT_PAGE_SIZE;
            _viewData.PageUrl = "/activity";
            _viewData.Session = Session;
            _viewData.Title = "Activities";

            return View(_viewData);
        }
	}
}