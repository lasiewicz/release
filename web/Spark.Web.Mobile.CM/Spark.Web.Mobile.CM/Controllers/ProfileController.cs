﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.Common.RestConsumer.V2.Models.Mingle.GAM;
using Spark.Common.RestConsumer.V2.Models.Mingle.Photos;
using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Models;
using Spark.Web.Mobile.CM.Framework;
using Spark.Web.Mobile.CM.Filters;


namespace Spark.Web.Mobile.CM.Controllers
{
    [AuthorizationLogging]
    [ErrorLogging]
    public class ProfileController : BaseController
    {
        private const int MAX_PHOTO_LIMIT = 12;

        private ProfileModel _model = new ProfileModel();
        private ProfileViewData _viewData;

        public ProfileController() 
        {
            _viewData = new ProfileViewData();
        }

        [HttpGet]
        public new ActionResult Profile(int? id)
        {
            string viewName = "Profile";
            string absolutePath = HttpContext.Request.Url.AbsolutePath;
            string pageUrl = "/profile";


            int memberId = id.GetValueOrDefault(0) != 0 ?
               id.GetValueOrDefault() : Session.MemberId;

            //// Determine if requested profile is user's own. If so, redirect the to
            //// MyProfile action instead.
            if (memberId == Session.MemberId)
            {
                viewName = "MyProfile";
                var matchPreferences = _model.GetMatchPreferences();
                _viewData.MatchPreferences = matchPreferences;
            }
            
            var profile = _model.GetFullProfile(memberId);

            _viewData.IsOwnProfile = (memberId == Session.MemberId);
            _viewData.PageId = "profile_page";
            _viewData.Profile = profile;
            _viewData.PageUrl = (memberId == Session.MemberId) ? pageUrl : String.Format("/profile/{0}", memberId.ToString());
            _viewData.Session = Session;
            _viewData.ShowCloseButton = absolutePath.Contains("static");
            _viewData.ShowFooter = !absolutePath.Contains("static");
            _viewData.ShowMembersOnlineButton = !absolutePath.Contains("static");
            _viewData.ShowNavPanel = !absolutePath.Contains("static");
            _viewData.Title = "Profile";
            _viewData.ShowSA = absolutePath.Contains("sa");

            if (String.Compare(viewName, "MyProfile") == 0)
            {
                _viewData.PageId = "myprofile_page";
                _viewData.Title = "My Profile";
            }

            return View(viewName, _viewData);
        }

        public new ActionResult ProfileUsername(string username)
        {
            string viewName = "Profile";
            string absolutePath = HttpContext.Request.Url.AbsolutePath;


            string userName = username;

            var profile = _model.GetProfileByUsername(userName);

            _viewData.IsOwnProfile = false;
            _viewData.PageId = "profile_page";
            _viewData.Profile = profile;
            _viewData.Session = Session;
            _viewData.Title = "Profile";

            return View(viewName, _viewData);
        }


        [HttpGet]
        public new ActionResult ReportAbuse(int? id)
        {
            string viewName = "ReportAbuse";
            string absolutePath = HttpContext.Request.Url.AbsolutePath;


            int memberId = id.GetValueOrDefault(0) != 0 ?
               id.GetValueOrDefault() : Session.MemberId;

            // Determine if requested profile is user's own. If so, redirect the to
            // MyProfile action instead.
            if (memberId == Session.MemberId)
            {
                Redirect("/profile");
            }

            var profile = _model.GetFullProfile(memberId);

            _viewData.IsOwnProfile = (memberId == Session.MemberId);
            _viewData.PageId = "report_abuse";
            _viewData.Profile = profile;
            _viewData.Session = Session;
            _viewData.Title = "Report";
            _viewData.ReportAbuseReasons = _model.GetAttributeOptions("ReportAbuseReason");

            return View(_viewData);
        }

        [HttpGet]
        public ActionResult Photos() 
        {
            var photos = _model.GetPhotos(Session.MemberId);
            if(photos != null){
                _viewData.Photos = _model.GetPhotos(Session.MemberId);
            }
            else
            {
                _viewData.Photos = new List<Photo>();
            }
           
            _viewData.PageId = "photos_page";
            _viewData.PageUrl = "/profile/photos";
            _viewData.Session = Session;
            _viewData.Title = "Photos";

            return View("MyPhotos", _viewData);
        }

        [HttpGet]
        public ActionResult Facebook()
        {
            _viewData.Session = Session;
            _viewData.PageId = "facebook_page";
            _viewData.PageUrl = "/profile/facebook-photos";
            _viewData.Title = "Facbook Photos";

            return View("FacebookPhotos",_viewData);
        }

	}
}