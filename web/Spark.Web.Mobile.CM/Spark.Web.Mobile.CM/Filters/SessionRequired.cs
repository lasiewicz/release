﻿using System;
using System.Web.Mvc;

using Spark.Web.Mobile.CM.Framework;
using Spark.Web.Mobile.CM.Exceptions;

namespace Spark.Web.Mobile.CM.Filters
{
    public class SessionRequired : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext context)
        {
            bool skipAuthorization = context.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) || 
                context.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);

            if (skipAuthorization)
                return;

            var session = (Spark.Web.Mobile.CM.Framework.Session)context.HttpContext.Items[ApplicationContext.CONTEXT_SESSION_KEY];
            if (session == null)
                throw new AuthorizationException("Session required.");
        }
    }
}