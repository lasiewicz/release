﻿using System;
using System.Web;
using System.Web.Mvc;

using log4net;
using Spark.Web.Mobile.CM.Exceptions;
using Spark.Web.Mobile.CM.Framework;
using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Controllers;


namespace Spark.Web.Mobile.CM.Filters
{
    public class AuthorizationLogging  : HandleErrorAttribute
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(ErrorLogging));

        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            if (context.ExceptionHandled  || !(exception is AuthorizationException))
                return;

            _logger.Debug(exception.Message);

            if (((AuthorizationException)exception).ErrorCode != 41020)
                RedirectToLogin(context);
            else 
                RedirectToSubscription(context);
        }

        private void RedirectToLogin(ExceptionContext context) 
        {
            var manager = new AuthenticationManager();
            var url = manager.GetLoginUrl();

            // Invalidate cookie so it can be updated once redirected from SUA.
            manager.InvalidateCookie();

            context.ExceptionHandled = true;
            context.HttpContext.Response.Clear();
            if (!context.HttpContext.Request.IsAjaxRequest())
            {
                context.HttpContext.Response.Redirect(url);
                return;
            }

            context.Result = CreateLoginResult();
        }

        private void RedirectToSubscription(ExceptionContext context) 
        {
            context.ExceptionHandled = true;
            context.HttpContext.Response.Clear();
            if (!context.HttpContext.Request.IsAjaxRequest())
            {
                context.HttpContext.Response.Redirect("/subscription");
                return;
            }
            
            // Return subscription page to user.
            context.Result = CreateSubscriptionResult((BaseController)context.Controller);       
        }

        private ActionResult CreateLoginResult() 
        {
            return new ViewResult
            {
                ViewName = "../login/login",
                ViewData = new ViewDataDictionary<LoginViewData>(new LoginViewData()),
            };
        }

        private ActionResult CreateSubscriptionResult(BaseController controller) 
        {
            return new ViewResult
            {
                ViewName = "../subscription/subscribe",
                ViewData = new ViewDataDictionary<SubscriptionViewData>(new SubscriptionViewData()),
            };       
        }
    }
}