﻿using System;
using System.Net;
using System.Web.Mvc;

using log4net;

using Spark.Web.Mobile.CM.ViewData;
using Spark.Web.Mobile.CM.Exceptions;
using Spark.Web.Mobile.CM.Controllers;

namespace Spark.Web.Mobile.CM.Filters
{
    public class ErrorLogging : HandleErrorAttribute
    {
        //private static readonly ILog _logger = LogManager.GetLogger(typeof(ErrorLogging));

        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            var type = typeof(ErrorLogging);
            if (context.ExceptionHandled || exception is AuthorizationException)
                return;
            if (exception is ApiException)
                type = ((ApiException)exception).Type;
               
            // Log exception.
            var logger = LogManager.GetLogger(type);
            logger.Error(exception.Message, exception);

            context.Result = CreateErrorResult(context);
            context.ExceptionHandled = true;
            context.HttpContext.Response.Clear();
            //context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            context.HttpContext.Response.TrySkipIisCustomErrors = true;
        }

        private ActionResult CreateErrorResult(ExceptionContext context) 
        {
            return new ViewResult
            {
                ViewName = "../Error/Error",
                ViewData = new ViewDataDictionary<ErrorViewData>(new ErrorViewData() { 
                    Exception = context.Exception,
                    PageId = "error_page",
                    Session = ((BaseController)context.Controller).Session
                }),
            };
        }
    }
}