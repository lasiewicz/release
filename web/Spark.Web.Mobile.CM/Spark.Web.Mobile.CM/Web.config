﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=301880
  -->
<configuration>
  <configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" />
    <section name="allowedPage" type="Spark.Web.Mobile.CM.Configurations.AllowedPageSection" />
    <!-- For more information on Entity Framework configuration, visit http://go.microsoft.com/fwlink/?LinkID=237468 -->
    <section name="entityFramework" type="System.Data.Entity.Internal.ConfigFile.EntityFrameworkSection, EntityFramework, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />
    <sectionGroup name="elmah">
      <section name="security" requirePermission="false" type="Elmah.SecuritySectionHandler, Elmah" />
      <section name="errorLog" requirePermission="false" type="Elmah.ErrorLogSectionHandler, Elmah" />
      <section name="errorMail" requirePermission="false" type="Elmah.ErrorMailSectionHandler, Elmah" />
      <section name="errorFilter" requirePermission="false" type="Elmah.ErrorFilterSectionHandler, Elmah" />
    </sectionGroup>
  </configSections>
  <log4net debug="true">
    <appender name="DebugAppender" type="log4net.Appender.DebugAppender">
      <immediateFlush value="true" />
      <layout type="log4net.Layout.SimpleLayout" />
    </appender>
    <appender name="RollingLogFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="c:\\logfiles\\spark.cm.mos.v2.log" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="10" />
      <maximumFileSize value="10MB" />
      <staticLogFileName value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5p %d %5rms %-22.22c{1} %-18.18M - %m%n" />
      </layout>
    </appender>
    <root>
      <level value="ALL" />
      <appender-ref ref="RollingLogFileAppender" />
      <appender-ref ref="DebugAppender" />
    </root>
  </log4net>
  <allowedPage>
    <pages>
      <add name="login" url="/login" />
      <add name="error" url="/error" />
      <add name="pagenotfound" url="/pagenotfound" />
      <add name="contactus" url="/home/contactus" />
      <add name="safety" url="/home/safety" />
      <add name="privacy" url="/home/privacy" />
      <add name="tos" url="/home/termsofservice" />
      <add name="toc" url="/home/termsandconditions" />
      <add name="cookiepolicy" url="/home/cookiepolicy" />
    </pages>
  </allowedPage>
  <appSettings>
    <add key="webpages:Version" value="3.0.0.0" />
    <add key="webpages:Enabled" value="false" />
    <add key="ClientValidationEnabled" value="true" />
    <add key="UnobtrusiveJavaScriptEnabled" value="true" />
    <!--<add key="ApplicationId" value="1003" />
    <add key="ClientSecret" value="642cAz0nTtMx9a/foVfaIphUqc4/X+cVH1Yritd1hV4=" />-->
    <add key="ApplicationId" value="1028" />
    <add key="ClientSecret" value="zUG8YkA2BMUFfJGklA58hy8/0+2vDZE0to4/rutIqPI=" />
    <add key="BrandId" value="9081" />
    <!--<add key="SparkApiUrl" value="https://api.atarsha.securemingle.dev/v3" />-->
    <!--use the one below-->
    <add key="SparkApiUrl" value="https://api-dev.spark.net/v3" />
    <!--not this one-->
    <add key="RedirectUrl" value="http://m.local.christian.dev/activity" />
    <add key="SuaUrl" value="http://accounts.local.spark.net/logon/" />
    <add key="FullWebsiteUrl" value="http://bhstage.christian.dev" />
    <add key="SuaTemplate" value="mosnewchristianminglecom" />
    <add key="Domain" value=".christian.dev" />
    <add key="Environment" value="dev" /> 
    <add key ="SiteUrl" value="http://m.local.christian.dev"/>
    <add key="NewChatUrl" value="http://static.bhstage.christian.dev" />
    <add key="TealiumUrl" value="//tags.tiqcdn.com/utag/spark/christianusmos/dev/utag.js"/>
    <add key="EjabberdServer" value="stage3-im.spark.net" />
    <add key="EjabberdPort" value="443" />
    <add key="EjabberdProtocol" value="https://" />
    <add key="EjabberdSiteDomain" value="bhstage.christian.dev" />
    <add key="ImApiUrl" value="https://api-dev.spark.net" />
    <add key="VersionCode" value="1.0"/>
    <add key="MessageAttachmentURLPath" value="http://s3.amazonaws.com/sparkmessageattachmentsdev" />
  </appSettings>
  <!--
    For a description of web.config changes see http://go.microsoft.com/fwlink/?LinkId=235367.

    The following attributes can be set on the <httpRuntime> tag.
      <system.Web>
        <httpRuntime targetFramework="4.5" />
      </system.Web>
  -->
  <system.web>
    <authentication mode="None" />
    <compilation debug="true" targetFramework="4.5" />
    <customErrors mode="RemoteOnly">
      <error statusCode="404" redirect="~/pagenotfound"/>
      <error statusCode="500" redirect="~/error"/>
    </customErrors>
    <httpRuntime />
    <pages controlRenderingCompatibilityVersion="4.0" />
    <httpModules>
      <add name="ErrorLog" type="Elmah.ErrorLogModule, Elmah" />
      <add name="ErrorMail" type="Elmah.ErrorMailModule, Elmah" />
      <add name="ErrorFilter" type="Elmah.ErrorFilterModule, Elmah" />
    </httpModules>
  </system.web>
  <system.webServer>
    <modules>
      <remove name="FormsAuthenticationModule" />
      <add name="SecurityModule" type="Spark.Web.Mobile.CM.Modules.SecurityModule" preCondition="managedHandler" />
      <add name="ErrorLog" type="Elmah.ErrorLogModule, Elmah" preCondition="managedHandler" />
      <add name="ErrorMail" type="Elmah.ErrorMailModule, Elmah" preCondition="managedHandler" />
      <add name="ErrorFilter" type="Elmah.ErrorFilterModule, Elmah" preCondition="managedHandler" />
    </modules>
    <validation validateIntegratedModeConfiguration="false" />
    <rewrite>
      <rules>
        <clear/>
        <rule name="profile-sa" stopProcessing="true">
          <match url="Applications/MemberProfile/ViewProfile.aspx"/>
          <conditions logicalGrouping="MatchAll" trackAllCaptures="false">
            <add input="{QUERY_STRING}" pattern="clickmemberid=(\d{0,12})"/>
          </conditions>
          <action type="Redirect" url="/profile/sa/{C:1}" redirectType="Found" appendQueryString="true" />
        </rule>
        <rule name="profile" stopProcessing="true">
          <match url="Applications/MemberProfile/ViewProfile.aspx"/>
          <conditions logicalGrouping="MatchAll" trackAllCaptures="false">
            <add input="{QUERY_STRING}" pattern="memberid=(\d{0,12})"/>
          </conditions>
          <action type="Redirect" url="/profile/{C:1}" redirectType="Found" appendQueryString="true" />
        </rule>
        <rule name="favorited-you" stopProcessing="true">
          <match url="Applications/HotList/View.aspx"/>
          <conditions logicalGrouping="MatchAny" trackAllCaptures="false">
            <add input="{QUERY_STRING}" pattern="CategoryID=-2"/>
          </conditions>
          <action type="Redirect" url="/activity" redirectType="Found" appendQueryString="true" />
        </rule>
        <rule name="inbox" stopProcessing="true">
          <match url="Applications/HotList/View.aspx"/>
          <conditions logicalGrouping="MatchAny" trackAllCaptures="false">
            <add input="{QUERY_STRING}" pattern="CategoryID=-4"/>
          </conditions>
          <action type="Redirect" url="/mail" redirectType="Found" appendQueryString="true" />
        </rule>
        <rule name="viewed-you" enabled="true" stopProcessing="true">
          <match url="Applications/HotList/View.aspx"/>
          <conditions logicalGrouping="MatchAny" trackAllCaptures="false">
            <add input="{QUERY_STRING}" pattern="categoryid=-1"/>
          </conditions>
          <action type="Redirect" url="/activity" redirectType="Found" appendQueryString="true" />
        </rule>
        <rule name="viewed-you-default" stopProcessing="true">
          <match url="default.asp"/>
          <conditions logicalGrouping="MatchAll" trackAllCaptures="false">
            <add input="{QUERY_STRING}" pattern="CategoryID=-1"/>
            <add input="{QUERY_STRING}" pattern="p=26000"/>
          </conditions>
          <action type="Redirect" url="/activity" logRewrittenUrl="true" redirectType="Found" appendQueryString="true" />
        </rule>
        <rule name="app-subscribe" stopProcessing="true">
          <match url="account/payment/app_login.html" />
          <conditions logicalGrouping="MatchAny" trackAllCaptures="false">
            <add input="{QUERY_STRING}" pattern="access_token=(.+)&amp;promoid=(.+)"/>
          </conditions>
          <action type="Redirect" url="subscription/subscribe/?promoid={C:2}&amp;lastapplication=4" appendQueryString="false" />
        </rule>
        <rule name="privacy" stopProcessing="true">
          <match url="Applications/Article/ArticleView.aspx"/>
          <conditions logicalGrouping="MatchAll" trackAllCaptures="false">
            <add input="{QUERY_STRING}" pattern="CategoryID=1948"/>
            <add input="{QUERY_STRING}" pattern="ArticleID=6498"/>
          </conditions>
          <action type="Redirect" url="/home/privacy" logRewrittenUrl="true" redirectType="Found" appendQueryString="true" />
        </rule>
        <rule name="safety" stopProcessing="true">
          <match url="Applications/Article/ArticleView.aspx"/>
          <conditions logicalGrouping="MatchAll" trackAllCaptures="false">
            <add input="{QUERY_STRING}" pattern="CategoryID=114"/>
          </conditions>
          <action type="Redirect" url="/home/safety" logRewrittenUrl="true" redirectType="Found" appendQueryString="true" />
        </rule>   
        <rule name="profile-default" patternSyntax="ECMAScript" stopProcessing="true">
          <match url="default.asp"/>
          <conditions logicalGrouping="MatchAll" trackAllCaptures="false">
            <add input="{QUERY_STRING}" pattern="memberid=(\d{1,12})"/>
          </conditions>
          <action type="Redirect" url="/profile/{C:1}" redirectType="Found" appendQueryString="true" />
        </rule>
        <rule name="mappings" stopProcessing="true">
          <match url=".*"/>
          <conditions>
            <add input="{JdateToMobileMap:{URL}}" pattern="(.+)"/>
          </conditions>
          <action type="Redirect" url="{C:1}" redirectType="Found"/>
        </rule>
        <rule name="landing-page" stopProcessing="true">
          <match url="(.*)"/>
          <conditions>
            <add input="{QUERY_STRING}" pattern="LPID=(\d{0,20})"/>
          </conditions>
          <action type="Redirect" url="http://www.jdate.com" redirectType="Found"/>
        </rule> 
      </rules>
      <rewriteMaps>
        <rewriteMap name="JdateToMobileMap">
          <add key="/Applications/MembersOnline/MembersOnline.aspx" value="/membersonline"/>
          <add key="/Applications/Registration/Registration.aspx" value="/registration/step1"/>
          <add key="/Applications/MemberProfile/RegistrationStep1.aspx" value="/registration/step1"/>
          <add key="/Applications/Registration/Landing1.aspx" value="/registration/step1"/>
          <add key="/Applications/Subscription/Subscribe.aspx" value="/subscription/subscribe"/>
          <add key="/Applications/Email/MailBox.aspx" value="/mail"/>
          <add key="/Applications/MemberProfile/MemberPhotoUpload.aspx" value="/profile/photos"/>
          <add key="/Applications/ContactUs/ContactUs.aspx" value="/home/contactus"/>
          <add key="/Applications/Search/SearchPreferences.aspx" value="/search"/>
          <add key="/Applications/Search/SearchResults.aspx" value="/search"/>
          <add key="/Applications/QuickSearch/QuickSearchResults.aspx" value="/search"/>
          <add key="/Applications/Home/Default.aspx" value="/activity"/>
          <add key="/Applications/MemberProfile/ViewProfile.aspx" value="/profile"/>
          <add key="/Applications/Logon/Logon.aspx" value="/logout"/>
          <add key="/Applications/Logon/Logoff.aspx" value="/logout"/>
          <add key="/Applications/HotList/View.aspx" value="/activity"/>
          <add key="/Applications/HotList/SlideShow.aspx" value="/secretadmirer"/>
          <add key="/Applications/Email/MessageSettings.aspx" value="/settings/message"/>
          <add key="/Applications/Subscription/History.aspx" value="/subscription/history/"/>
          <add key="/Applications/MemberServices/MemberServices.aspx?rc=YOUR_MEMBERSHIP_HAS_BEEN_SUSPENDED" value="/settings/unsuspend"/>
          
        </rewriteMap>
      </rewriteMaps>
    </rewrite>
  </system.webServer>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-5.0.0.0" newVersion="5.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-1.5.2.14234" newVersion="1.5.2.14234" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <entityFramework>
    <defaultConnectionFactory type="System.Data.Entity.Infrastructure.SqlConnectionFactory, EntityFramework" />
    <providers>
      <provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer" />
    </providers>
  </entityFramework>
  <elmah>
    <!--
        See http://code.google.com/p/elmah/wiki/SecuringErrorLogPages for 
        more information on remote access and securing ELMAH.
    -->
    <security allowRemoteAccess="false" />
  </elmah>
    <location path="elmah.axd" inheritInChildApplications="false">
      <system.web>
        <httpHandlers>
          <add verb="POST,GET,HEAD" path="elmah.axd" type="Elmah.ErrorLogPageFactory, Elmah" />
        </httpHandlers>
        <!-- 
          See http://code.google.com/p/elmah/wiki/SecuringErrorLogPages for 
          more information on using ASP.NET authorization securing ELMAH.

        <authorization>
          <allow roles="admin" />
          <deny users="*" />  
        </authorization>
        -->  
      </system.web>
      <system.webServer>
        <handlers>
          <add name="ELMAH" verb="POST,GET,HEAD" path="elmah.axd" type="Elmah.ErrorLogPageFactory, Elmah" preCondition="integratedMode" />
        </handlers>
      </system.webServer>
  </location>
</configuration>