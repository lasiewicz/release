﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using log4net;

using Spark.Web.Mobile.CM.Controllers;

namespace Spark.Web.Mobile.CM
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(MvcApplication));

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            log4net.Config.XmlConfigurator.Configure();
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
        }

        public void Application_Error(object sender, EventArgs e)
        {
            // Intercept all possible exceptions raised by the application.
            // In case exceptions are not handled directly from controllers, models or
            // thrown error.
            Exception ex = Server.GetLastError();
            _logger.Error(ex.Message, ex);
        }
    }
}
