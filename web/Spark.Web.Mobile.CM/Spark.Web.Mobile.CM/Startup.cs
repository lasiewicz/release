﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Spark.Web.Mobile.CM.Startup))]
namespace Spark.Web.Mobile.CM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
