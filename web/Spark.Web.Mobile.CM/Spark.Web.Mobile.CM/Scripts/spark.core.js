spark.api.client = (function () {
    //var AJAX_REST_AUTHORITY = "https://api.spark.net/V2";
    var AJAX_REST_AUTHORITY = "http://api.stgv3.spark.net/V2";
    //var AJAX_REST_AUTHORITY = "http: //api.local.spark.net/V2";
    var BRAND_ID = 1003;
    var loginData = {};
    var self = {};

    var defaultAjaxOptions = {
        beforeSend: function (xhr) {
            // Hack for Android 2.3.X devices (known Google issue).
            if (self.isAndroidGingerbread) {
                xhr.setRequestHeader("Content-Length", "");
            }
        },
        tryCount: 0,
        retryLimit: 3,
        dataType: "json"
    };

    // List of urls to REST API.
    var urlLookup = {
        fullProfile: { url: "/profile/attributeset/fullprofile/{targetMemberId}", method: "GET" },
        miniprofile: { url: "/profile/attributeset/miniprofile/{targetMemberId}/{memberId}", method: "GET" },
        attributeOptions: { url: "/content/attributes/options/{attribute}", method: "GET" },
        updateProfile: { url: "/profile/attributes", method: "PUT" },
        sendTease: { url: "/mail/tease", method: "POST" },
        getHotlist: { url: "/hotlist/{hotlist}/pagesize/{pageSize}/pagenumber/{pageNumber}", method: "GET" },
        members_online_count: { url: "/membersonline/count", method: "GET" },
        members_online: { url: "/membersonline/list", method: "GET" },
      
    };

    // Private method(s).
    function buildUrl(originalUrl, urlParams) {
        // Replace place-holder with url params.
        originalUrl = formatUrl(originalUrl, urlParams);

        // Build complete API url (add timestamp query-string to explicitly disable ajax caching).
        var url = AJAX_REST_AUTHORITY + "/brandId/" + BRAND_ID + originalUrl + "?access_token=" + loginData.accessToken + "&ts=" + new Date().getTime();
        return url;
    };

    function formatUrl(url, params) {
        for (key in params) {
            url = url.replace("{" + key + "}", params[key]);
        }

        return url;
    }

    function getLoginData() {
        //var tokenDomain = $.cookie("MOS_DOMAIN");

        return {
            "memberId": parseInt($.cookie("mos_id"), 10),
            "accessToken": $.cookie("mos_token"),
            "isPayingMember": $.cookie("mos_sub"),
            "accessExpiresTime": new Date($.cookie("mos_exp"))
        };
    };

    function isValidToken() {
        if (!loginData || !loginData.accessToken || !loginData.aaccessExpiresTime) {
            return false;
        }

        var nowUTC = getUTCTime();
        var accessExpiresUTC = loginData.accessExpiresTime;
        accessExpiresUTC.setHours(accessExpiresUTC.getHours() + 8); // adjust from pacific time
        if (accessExpiresUTC > nowUTC) {
            return true;
        }

        return false;
    };

    function getUTCTime(theTime) {
        if (!theTime) {
            theTime = new Date();
        }
        var utc = new Date(theTime.getTime() + theTime.getTimezoneOffset() * 60 * 1000);
        return utc;
    };

    function handleAjaxCallback(success, data, fn, context) {
        if (typeof fn === "function")
            fn(success, data, context);
    };

    // Public method(s).
    self.init = function (brandId, ajaxUrl, ajaxOptions) {
        BRAND_ID = brandId || BRAND_ID;
        AJAX_REST_AUTHORITY = ajaxUrl || AJAX_REST_AUTHORITY;
        loginData = getLoginData();

        if (typeof ajaxOptions !== "undefined")
            $.extend(defaultAjaxOptions, ajaxOptions);
        self.urls = urlLookup;
        self.isAndroidGingerbread = /Android 2.3/i.test(navigator.userAgent);
    };

    self.getLoginData = function () {
        return getLoginData();
    };

    self.callAPI = function (options, urlParams, data, callback) {
        var spec = {
            type: options.method,
            url: buildUrl(options.url, urlParams),
            //cache: false,
            data: data,
            success: function (response, other, other2) {
                handleAjaxCallback(true, response, callback, this);
            },
            error: function (xhr, statusText, error) {
                handleAjaxCallback(false, $.parseJSON(xhr.responseText), callback);
            }
        };

        if (options.method.toLowerCase() !== 'get' && data) {
            var count = 0;
            for (var key in data) if (data.hasOwnProperty(key)) {
                count++;
            }

            if (count > 0) {
                spec.contentType = "application/json";
                spec.data = JSON.stringify(data);
            }
        }

        $.extend(spec, defaultAjaxOptions);
        $.ajax(spec);
    };

    self.init();

    return self;
})();


spark.omniture = (function () {
    var self = {};
    var global = {};

    // Private function(s)
    function isWeekend(dayOfWeek) {
        if (dayOfWeek === 0 || dayOfWeek === 6) {
            return true;
        }

        return false;
    }

    function getTime(hours, minutes, ampm) {
        hours = hours % 12;
        hours = hours ? hours : 12;
        minutes = minutes - (minutes % 30);
        var temp = minutes == 0 ? "00" : minutes;

        return hours + ":" + temp + " " + ampm;
    }

    function getTimestamp(d) {
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var hours = d.getHours() + 1;
        var minutes = d.getMinutes();

        // Format: yyyy-MM-dd HH:mm
        return year + "-" + month + "-" + day + " " + hours + ":" + minutes;
    }

    function setDateTime(d) {
        var hours = d.getHours();
        var minutes = d.getMinutes();
        var ampm = (hours >= 12) ? "PM" : "AM";
        var options = {
            prop11: getTime(hours, minutes, ampm), // Hour of Day
            prop12: d.getDay(),  // Day of Week
            prop13: isWeekend(d.getDay()) ? "Weekend" : "Weekdays" // Weekday/Weekend
        };

        $.extend(global, options);
    }

    function clear(options) {
        for (var key in options) {
            delete s[key];
            //delete global[key];
        };
    }

    // Public function(s)
    self.init = function (status, gender, age, ethnicity, location, subscriberStatus, id) {
        var options = {
            eVar36: status, // i.e m.jdate.com
            prop18: gender,
            prop19: age,
            prop20: ethnicity,
            prop21: location,
            prop22: subscriberStatus,
            prop23: id
        };

        $.extend(global, options);
    };

    self.track = function (pageName, options) {
        if (!options)
            options = {};
        if (pageName)
            options.pageName = pageName;

        var currentDate = new Date();
        options.prop9 = getTimestamp(currentDate);
        options.prop10 = location.href;

        setDateTime(currentDate);

        $.extend(s, global, options);
        s.t();

        clear(options);
    };

    self.trackEvents = function (pageName, events, options) {
        if (!options)
            options = {};
        if (pageName)
            options.pageName = pageName;
        if (events)
            options.events = events;

        var currentDate = new Date();
        options.prop9 = getTimestamp(currentDate);
        options.prop10 = location.href;

        setDateTime(currentDate);

        $.extend(s, global, options);
        s.t();

        clear(options);
    };

    self.getGlobal = function () {
        return global;
    };

    self.removeEvar = function (num) {
        delete s["eVar" + num];
    };

    self.removeProp = function (num) {
        delete s["prop" + num];
    };

    //self.removeProperty = function (key) {
    //    delete s[key];
    //};

    self.setEvar = function (num, value) {
        s["eVar" + num] = value;
    };

    self.setProp = function (num, value) {
       s["prop" + num] = value;
    };

    return self;
})();