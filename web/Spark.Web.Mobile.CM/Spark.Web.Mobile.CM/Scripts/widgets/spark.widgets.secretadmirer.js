﻿(function ($) {
    $.widget("mobile.secretadmirer", $.mobile.widget, {
        options: {},
        contentHeight: 320,
        contentContainer: null,
        isLastPage: false,
        isLoading: false,
        isVoting: false,
        batchSize: 0,
       // pageNo: 1,
        preference: null,
        threshold: 20,
        swiper: null,

        _adjustHeight: function () {
            var self = this;
            var windowHeight = $(window).height();
            var contentHeight = (self.contentHeight + 60);
        },

        _appendSlides: function (ul) {
            var self = this;
            if (self.swiper === null)
                return;

            if (!self.isLastPage && !self.isLoading && self._loadSlides()) {
                spark.utils.common.showLoading("Please wait...");

                self.isLoading = true;
                self._getSecretAdmirers(function (success, response) {
                    spark.utils.common.hideLoading();

                    if (!success) {
                        self.swiper.swipeNext();
                        return;
                    }

                    self.threshold += 20;

                    
                    var members = response.data;
                    //var members = response.data.SlideshowMembers;
                    var length = members.length;
                    var width = $(window).width();

                    if (length < self.batchSize)
                        self.isLastPage = true;

                    // Append more slide(s).
                    $.each(members, function (index, item) {
                        var li = self._createSlide(item, width);

                        li.appendTo(ul);
                    });

                    if (self.isLastPage)
                        self._createNoResultSlide(ul, width);

                    self.swiper.reInit();
                    self.swiper.swipeNext();
                    self.isLoading = false;
                });
            } else {
                self.swiper.swipeNext();
            }
        },

        _create: function () {
            var self = this;
            var container = self.element;

            container.bind("click", function (e) {
                var o = e.target;
                if (!$(o).hasClass("secret-admirer"))
                    return;

                self._hide();
            });

            $(window).off("resize.secretadmirer").on("resize.secretadmirer", function () {
                self._resize();
            });

            container.hide();
        },

        _createNoResultSlide: function (ul, width) {
            var li = $("<li class=\"swiper-slide no-result\"><span>Oh no! There are no more members who match your SA preferences. Tap the button below to change your preferences.</span></li>");
            var searchButton = $("<a href=\"/secretadmirer/settings\" class=\"btn-blue\">Update Preferences</a>");

            searchButton.appendTo(li);

            li.css({ width: width });
            li.appendTo(ul);
        },

        _createSlide: function (item, width) {
            var self = this;
            var li = $("<li class=\"swiper-slide\" />");
            var h3 = $("<h3 />");
            var img = $("<img />");
            var profile = item.miniProfile;
            var photo = profile.defaultPhoto;
            var imageUrl = (photo) ? photo.fullPath : null;

            if (imageUrl) {
                img.bind("load", function (o) {
                    var e = o.currentTarget;
                    var dimension = self._getDimension(e.clientWidth, e.clientHeight, width, self.contentHeight);
                    var left = (width - dimension[0]) / 2;
                    var top = (self.contentHeight - dimension[1]) / 2;

                    $(e).attr("data-clientheight", e.clientHeight);
                    $(e).attr("data-clientwidth", e.clientWidth);
                    $(e).css({
                        width: dimension[0] + "px",
                        height: dimension[1] + "px",
                        top: top + "px",
                        left: left + "px",
                        visibility: "visible"
                    });
                });

                img.attr("src", imageUrl);
                img.css({ visibility: "hidden" });
                img.appendTo(li);
            }

            h3.on("click", function () {
                window.open("/profile/" + profile.id, "_" + profile.id);
            });
            h3.html("<span>" + profile.username + "</span><br/ ><span>" + profile.age + ", " + profile.location + "</span>");
            h3.appendTo(li);

            li.css({ width: width });
            li.attr("data-memberid", profile.id);

            return li;
        },

        _getDimension: function (width, height, maxWidth, maxHeight) {
            var ratio = 1;
            if (width > maxWidth || height > maxHeight) {
                ratio = Math.min(maxWidth / width, maxHeight / height);
            }

            return [(width * ratio), (height * ratio)];
        },

        _getSecretAdmirers: function (callback) {
            var self = this;
            var prefs = self.preference;

            prefs.IsNewPrefs = false;
            spark.views.admirer.getMemberSlideShow(self.batchSize, prefs, function (success, response) {
                callback(success, response);
            });
        },

        _hide: function () {
            var container = this.element;
            var content = container.find(":first-child");
            var height = 380;

            this.swiper = null;
            //content.animate({ top: "-" + height + "px" }, 200, "linear", function () {
            //    container.hide();
            //    content.empty();
            //});

            content.slideUp("fast", function () {
                container.hide();
                content.empty();
            });

            //$(window).off("resize.secretadmirer");
        },

        _hideButtons: function () {
            var self = this;
            var container = self.element;
            var content = container.find(":first-child");
            var buttonContainer = content.find("div.button-container");

            buttonContainer.hide();
        },

        _hideStamp: function () {
            var self = this;
            var container = self.element;
            var content = container.find(":first-child");
            var stamp = content.find("span.stamp");

            stamp.removeClass("yes")
                .removeClass("no")
                .removeClass("maybe");

            stamp.addClass("invisible");
        },

        _loadSlides: function () {
            var self = this;
            //var container = self.element;
            //var content = container.find(":first-child");
            //var swiperContainer = content.find("div.swiper-container");
            //var ul = swiperContainer.find("ul");
            var activeSlideIndex = (self.swiper.activeIndex + 1);

            if (self.isLastPage)
                return false;

            return (activeSlideIndex >= self.threshold);;
        },

        _resize: function () {
            var self = this;
            var container = self.element;
            var content = container.find(":first-child");
            var swiperContainer = content.find("div.swiper-container");
            var ul = swiperContainer.find("ul");
            var width = $(window).width();

            swiperContainer.css({
                height: self.contentHeight,
                width: width
            });

            ul.children("li").each(function () {
                var e = $(this);
                var img = e.find("img");
                var dimension = self._getDimension(img.attr("data-clientwidth"), img.attr("data-clientheight"), width, self.contentHeight);
                var left = (width - dimension[0]) / 2;
                var top = (self.contentHeight - dimension[1]) / 2;

                e.css({
                    width: width
                });

                img.css({
                    width: dimension[0] + "px",
                    height: dimension[1] + "px",
                    top: top + "px",
                    left: left + "px"
                });
            });
        },

        _showStamp: function (votingType) {
            var self = this;
            var container = self.element;
            var content = container.find(":first-child");
            var stamp = content.find("span.stamp");
            var width = content.width();
            var height = (content.height() - 60);
            var left = ((width - stamp.width()) / 2);
            var top = ((height - stamp.height()) / 2);
            var cssStamp = "yes";

            if (votingType === 2) {
                cssStamp = "no"
            } else if (votingType === 3) {
                cssStamp = "maybe"
            }

            stamp.css({
                left: left,
                top: top
            });

            stamp.removeClass("invisible")
                .addClass(cssStamp);
        },

        _saveYNM: function(memberId, isYes, isNo, isMaybe, pageName, omniName, callback) {
            spark.views.admirer.saveYNM(memberId, isYes, isNo, isMaybe, pageName, omniName, callback);
        },

        _voteYNM: function (votingType, callback) {
            var self = this;
            if (self.isVoting)
                return;

            var slide = self.swiper.activeSlide();
            var memberId = ($(slide).attr("data-memberid")) ?
                $(slide).attr("data-memberid") : slide.getData("memberid");

            self.isVoting = true;
            switch (votingType) {
                case 2: // No
                    self._saveYNM(memberId, false, true, false, "m_secretadmirer_overlay", "MOS Overlay", callback);
                    break;
                case 3: // Maybe
                    self._saveYNM(memberId, false, false, true, "m_secretadmirer_overlay", "MOS Overlay", callback);
                    break;
                default: // Yes
                    self._saveYNM(memberId, true, false, false, "m_secretadmirer_overlay", "MOS Overlay", callback);
                    break;
            }

            // Show vite stamp so user can see what action took place
            // i.e. YES, NO or MAYBE vote.
            self._showStamp(votingType);
        },

        show: function (members, prefs, batchSize) {

            var self = this;
            var container = self.element;
            var content = container.find(":first-child");
            var swiperContainer = $("<div class=\"swiper-container\" />");
            var ul = $("<ul class=\"swiper-wrapper\" />");
            var buttonContainer = $("<div class=\"button-container\" />");
            var yesButton = $("<a class=\"yes\" data-role=\"none\">YES</a>");
            var maybeButton = $("<a class=\"maybe middle\" data-role=\"none\">MAYBE</a>");
            var noButton = $("<a class=\"no\" data-role=\"none\">NO</a>");
            //var closeBar = $("<div class=\"close-bar\" />");
            var closeButton = $("<span class=\"btn-close\" />");
            var stamp = $("<span class=\"stamp invisible\" />");
            var settings = $('<div class="prefs-container"><a data-role="none" class="btn-sa-settings" href="/secretadmirer/settings" id="btn_sa_settings">Stats &amp; Preferences</a></div>')
            var width = $(window).width();
            var width = $(window).width();
            
            //self._adjustHeight();
            self.preference = prefs;
            self.batchSize = batchSize;
            self.threshold = 20;
            self.isLastPage = (members.length < batchSize);

            content.empty();
            swiperContainer.css({
                height: self.contentHeight,
                width: width
            });

            //closeBar.off().click(function () {
            //    self._hide();
            //});

            closeButton.off().click(function () {
                self._hide();
            });

            $.each(members, function (index, item) {
                var li = self._createSlide(item, width);
                li.appendTo(ul);
            });

            ul.appendTo(swiperContainer);

            //settingsButton.appendTo(buttonContainer);
            yesButton.appendTo(buttonContainer);
            maybeButton.appendTo(buttonContainer);
            noButton.appendTo(buttonContainer);

            swiperContainer.appendTo(content);
            buttonContainer.appendTo(content);
           // closeBar.appendTo(content);
            closeButton.appendTo(content);
            stamp.appendTo(content);
            settings.appendTo(content);


            self.contentContainer = content;
            if (members.length === 0) {
                self._hideButtons();
                self._createNoResultSlide(ul, width);
            } else if (self.isLastPage) {
                self._createNoResultSlide(ul, width);
            }

            // Initialize swiper
            self.swiper = swiperContainer.swiper({
                mode: 'horizontal',
                noSwiping: true,
                loop: false,
                onSlideNext: function (o) {
                    var activeSlide = $(o.activeSlide());
                    if ((activeSlide.hasClass("no-result")))
                        self._hideButtons();

                    self._hideStamp();
                    self.isVoting = false;
                }
            });

            yesButton.off().on("click", function () {
                self._voteYNM(1, function (success, response) {
                    self._appendSlides(ul);
                });
            });

            maybeButton.off().on("click", function () {
                self._voteYNM(3, function (success, response) {
                    self._appendSlides(ul);
                });
            });

            noButton.off().on("click", function () {
                self._voteYNM(2, function (success, response) {
                    self._appendSlides(ul);
                });
            });

            container.show();
            content.slideDown(350);
        }
    });

    $(document).bind("pagecreate create", function (e) {
        $(document).trigger("secretadmirerbeforecreate");
        $("div[data-type='secretadmirer']").each(function () {
            if (typeof ($(this).data('secretadmirer')) === "undefined") {
                $(this).secretadmirer();
            }
        });
    });
})(jQuery);