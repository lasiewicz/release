﻿spark.utils.httpclient = (function () {
    var self = {};

    var defaultOptions = {
        beforeSend: function (xhr) {
            // Hack for Android 2.3.X devices (known Google issue).
            if (self.isAndroidGingerbread) {
                xhr.setRequestHeader("Content-Length", "");
            }
        },
        tryCount: 0,    
        retryLimit: 3
    };

    // Private method(s).
    function buildUrl(originalUrl, urlParams) {
        // Replace place-holder with url params.
        originalUrl = formatUrl(originalUrl, urlParams);
        return originalUrl;
    };

    function formatUrl(url, params) {
        for (key in params) {
            url = url.replace("{" + key + "}", params[key]);
        }

        return url;
    }

    function handleResponse(success, data, fn, context) {
        if (typeof fn === "function")
            fn(success, data, context);
    };

    // Public method(s).
    self.init = function (options) {
        if (typeof options !== "undefined")
            $.extend(defaultOptions, options);

        self.isAndroidGingerbread = /Android 2.3/i.test(navigator.userAgent);
    };

    self.execute = function (url, urlParams, data, method, callback) {
        var spec = {
            type: method,
            url: buildUrl(url, urlParams),
            //cache: false,
            data: data,
            success: function (response, other, other2) {
                handleResponse(true, response, callback, this);
            },
            error: function (xhr, statusText, error) {
                handleResponse(false, $.parseJSON(xhr.responseText), callback);
            }
        };

        $.extend(spec, defaultOptions);
        $.ajax(spec);
    };

    self.init();

    return self;
})();
