﻿spark.utils.views = (function () {
    var self = {};

    // Private function(s).
    function appendToList(ul, data, itemBuilder, callback) {
        if($(ul).length === 0)
            return;
    
        // Remove loading footer.
        removeLoadingPanel(ul);

        // Show no results panel.
        /*if (data === null || data.length === 0) {
            $("<li class=\"no-results\"><p>No results found.</p></li>").appendTo(ul);
            if (typeof callback === "function")
                callback(true);

            return;
        }*/

        var lastIndex = (data.length - 1);
        $.each(data, function (index, item) {
            var li = itemBuilder(index, item);

            //if (index === lastIndex)
            //    li.addClass("last");

            li.appendTo(ul);
        });

        if (typeof callback === "function")
            callback(true);
    };

    function addNoResultsPanel(ul) {
        $("<li class=\"no-results\"><p>No results found.</p></li>").appendTo(ul);
    };

    function addLoadingPanel(ul) {
        if (!ul || ul.length === 0)
            return;

        if (ul.find("li.loading").length !== 0)
            return;

        // Add loading footer.
        var loadingPanel = $("<li class=\"loading\"><p>Loading...</p></li>");
        var spinner = new Spinner({
            lines: 15,
            length: 5,
            width: 2,
            radius: 5
        }).spin();

        loadingPanel.find("p").prepend(spinner.el);
        loadingPanel.appendTo(ul);
    };

    function clearList(ul) {
        if (!ul || ul.length === 0)
            return;

        ul.empty();
    };

    function removeItemBorder(ul) {
        ul.find("li:last-child").addClass("last");
    };

    function removeLoadingPanel(ul) {
        ul.find("li.loading").remove();
    };

    function removeNoResultsPanel(ul) {
        ul.find("li.no-results").remove();
    };

    function getBlockedUsers(memberId, targetMemberId) {
        var params = {
            hotlist: "hide",
            targetMemberId: targetMemberId,
            pageSize: 500,
            pageNumber: 1
        };
        
        spark.api.client.callAPI(spark.api.client.urls.hotlist, params, null, function (success, response) {
            
            if (!success) {
                //alert(response);
                return;
            }

            if (response.data.length > 0) {
                
            }


            //if (success && invites.length > 0) {
            //    var blockedUsers = {};
            //    var length = response.data.length;

            //    for (var j = 0; j < length; j++) {
            //        blockedUsers[response.data[j].miniProfile.id] = true;
            //    }

            //    length = invites.length;
            //    for (var i = 0; i < invites.length; i++) {
            //        var invite = invites[i];
            //        if (invite.SenderMemberId in blockedUsers) {
            //            invites.splice(i, 1);
            //            i--;
            //        }

            //    }
            //}
        });
    }

    // Public function(s).
    self.addLoadingPanel = addLoadingPanel;
    self.addNoResultsPanel = addNoResultsPanel;
    self.appendToList = appendToList;
    self.clearList = clearList;
    self.removeItemBorder = removeItemBorder;
    self.removeLoadingPanel = removeLoadingPanel;
    self.removeNoResultsPanel = removeNoResultsPanel;
    self.getBlockedUsers = getBlockedUsers;

    return self;
})();