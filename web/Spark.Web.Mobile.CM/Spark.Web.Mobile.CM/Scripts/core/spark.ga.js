﻿spark.ga = (function () {
    var self = {};
    var global = {};

    /*** Initialize Google Analytics Web Tracking ***/
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    /*** Setup GA Track ID ***/
    ga('create', 'UA-47473518-3', 'auto');

    if ($.cookie('refreshGA')) {
        $.removeCookie('refreshGA');
    } else {
        ga('send', 'pageview');
        ga('send', 'event', "Site", "Open");
    }

    // Tracking Events
    self.trackEventWith4Params = function (category, action, label, value) {
        ga('send', 'event', category, action, label, value);  // value is a number.
    };

    self.trackEventWith3Params = function (category, action, label) {
        ga('send', 'event', category, action, label);
    };

    self.trackEventWith2Params = function (category, action) {
        ga('send', 'event', category, action);
    };

    self.trackEventWithParams= function (category, action, params) {
        ga('send', 'event', category, action, params);
    };

    // Tracking Pages
    self.trackPageByDefault = function () {
        ga('send', 'pageview');
    };

    self.trackPageWithPath = function (path) {
        ga('send', 'pageview', path);
    };

    self.trackPageWith2Params = function (path, title) {
        ga('send', 'pageview', {
            'page': path,
            'title': title
        });
    };

    return self;
})();