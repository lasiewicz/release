spark.utils = (function () {
    var self = {};

    self.showGenericDialog = function (content, callback) {
        var dialog = $("#generic_dialog");
        //var okBtn = dialog.find("a.ok");
        //var titleBar = dialog.find("h4");
        var msg = dialog.find("p");

        dialog.trigger("create");
        dialog.popup({ tolerance: "15,5,15,5" });

        /*if (typeof func === "function") {
            okBtn.unbind("click").bind("click", function () {
                dialog.popup("close");

                func();
            });
        } else {
            okBtn.unbind("click").bind("click", function () {
                dialog.popup("close");
            });
        }*/

        // Setup title and message to display.
        //titleBar.text(title);
        if (typeof content === "string") {
            msg.text(content);
        } else {
            msg.html(content);
        }

        // Hack to disable closing popup when users tap outside.
        dialog.on({
            popupbeforeposition: function () {
                $('.ui-popup-screen').off();
            }
        });

        dialog.popup("open");
    };

    return self;
})();

spark.utils.httpclient = (function () {
    var self = {};

    var defaultOptions = {
        beforeSend: function (xhr) {
            // Hack for Android 2.3.X devices (known Google issue).
            if (self.isAndroidGingerbread) {
                xhr.setRequestHeader("Content-Length", "");
            }
        },
        tryCount: 0,    
        retryLimit: 3
    };

    // Private method(s).
    function buildUrl(originalUrl, urlParams) {
        // Replace place-holder with url params.
        originalUrl = formatUrl(originalUrl, urlParams);
        return originalUrl;
    };

    function formatUrl(url, params) {
        for (key in params) {
            url = url.replace("{" + key + "}", params[key]);
        }

        return url;
    }

    function handleResponse(success, data, fn, context) {
        if (typeof fn === "function")
            fn(success, data, context);
    };

    // Public method(s).
    self.init = function (options) {
        if (typeof options !== "undefined")
            $.extend(defaultOptions, options);

        self.isAndroidGingerbread = /Android 2.3/i.test(navigator.userAgent);
    };

    self.execute = function (url, urlParams, data, method, callback) {
        var spec = {
            type: method,
            url: buildUrl(url, urlParams),
            //cache: false,
            data: data,
            success: function (response, other, other2) {
                handleResponse(true, response, callback, this);
            },
            error: function (xhr, statusText, error) {
                handleResponse(false, $.parseJSON(xhr.responseText), callback);
            }
        };

        $.extend(spec, defaultOptions);
        $.ajax(spec);
    };

    self.init();

    return self;
})();
