﻿spark.views.im = (function () {
    var self = {};
    var dialog = null;
    var interval = (1000 * 20);
    var invites = [];
    var isLoading = false;
    var isActive = false;
    var removedInvites = [];
    var timerHandler;
    var chatServerName;
    var boshServer;
    var connection; // strophe connection
    var communityId;
 
    function closeInvite() {
        dialog.popup("close");
    };

    function connectToEjabberd(callback) {
        if (!communityId) {
            return; // unknown site
        }

        ownJid = spark.utils.common.getMemberId() + '-' + communityId + '@' + chatServerName;

        connection = new Strophe.Connection(boshServer);
        connection.connect(ownJid, spark.utils.common.getToken(), callback);
    };

    function filterBlockedInvites() {
        var params = {
            hotlist: "hide",
            targetMemberId: spark.utils.common.getMemberId(),
            pageSize: 500,
            pageNumber: 1
        };

        spark.api.client.callAPI(spark.api.client.urls.hotlist, params, null, function (success, response) {
            if (success && invites.length > 0) {
                var blockedUsers = {};
                var length = response.data.length;

                for (var j = 0; j < length; j++) {
                    blockedUsers[response.data[j].miniProfile.id] = true;
                }

                length = invites.length;
                for (var i = 0; i < invites.length; i++) {
                    var invite = invites[i];
                    if (invite.SenderMemberId in blockedUsers) {
                        invites.splice(i, 1);
                        i--;
                    }

                }
            }

            showInvite();
        });
    }

    function filterInvites(data) {
        var temp = [];
        var length = data.length;
        for (var i = 0; i < length; i++) {
            if (!isRemoved(data[i].ConversationKey))
                temp.push(data[i]);
        }

        return temp;
    };

    function getInvites() {
        closeInvite();
        spark.api.client.callAPI(spark.api.client.urls.getInvites, null, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            var data = response.data;
            if (data.length === 0)
                return;

            // Filter invitations in case we've already removed them from conversation.
            invites = filterInvites(response.data);
            filterBlockedInvites();
        });
    };

    function getRootDomain() { 
        var domainParts = document.domain.split('.');
        var last = domainParts.length - 1;
        var rootDomain;

        rootDomain = domainParts[last - 1] + '.' + domainParts[last];
        if (domainParts[last - 1] === "co") { // grab 3 parts: jdate co il, jdate co uk
            rootDomain = domainParts[last - 2] + '.' + domainParts[last - 1] + '.' + domainParts[last];
        }

        return rootDomain;
    };

    
    function handleIMWindowClosing(params) {
        var chatterJid = params.chatterMemberId + '-' + communityId + '@' + chatServerName;

        connectToEjabberd(function (status) {
            if (status === Strophe.Status.CONNECTED) {
                if (connection) {
                    connection.send($pres());
                    connection.send($pres({
                        to: chatterJid,
                        "type": "unavailable"
                    }));
                }
            } else if (status === Strophe.Status.CONNFAIL) {
                //spark.log.logError('Connection to chat server at ' + boshServer + ' failed.');
            } else if (status === Strophe.Status.AUTHFAIL) {
                //spark.log.logError('authorization with chat server failed');
            }
        });

        // if receiver never got online, trigger missed IM
        if (params.messageQueue && params.messageQueue.length > 0)
            sendMissedIM(params.messageQueue, params.chatterMemberId);

        spark.omniture.track("m_im_chat_window", { eVar58: params.chatDuration, eVar59: params.messageCount });
    };

    function hasActiveInvite() {
        return isActive;
    };

    function initializeDialog() {
        // Get handle to IM popup
        dialog = $("#im_invite_dialog");

        // Initialize IM Invite popup.
        dialog.trigger("create");
        dialog.popup({ tolerance: "15,5,15,5" });

        // Hack to disable closing popup when users tap outside.
        dialog.on({
            popupbeforeposition: function () {
                $('.ui-popup-screen').off();
            }
        });

        dialog.bind({
            popupafteropen: function () {
                isActive = true;
            },
            popupafterclose: function () {
                isActive = false;
            }
        });
    }

    function isRemoved(key) {
        var length = removedInvites.length;
        for (var i = 0; i < length; i++) {
            if (key === removedInvites[i])
                return true;
        }

        return false;
    };

    function openIM(url, chatterID) {
        window.open(url, '_' + chatterID);
    };

    function removeInvite(key) {
        var params = {
            ConversationKey: key
        };

        removedInvites.push(key);
        invites.splice((invites.length - 1), 1);

        spark.api.client.callAPI(spark.api.client.urls.removeInvite, {}, params, function (success, response) {
            // Do nothing.
        });
    };

    function sendEjabIMRejectedMessage(recipientMemberId, conversationTokenId) {
        var declineJid;
        var memberId = spark.utils.common.getMemberId();

        if (!conversationTokenId) {
            declineJid = memberId + '-' + communityId + '@' + chatServerName;
        } else {
            declineJid = recipientMemberId + '#' + memberId + '#' + conversationTokenId + '-' + communityId + '@' + chatServerName;
        }
        var recipientJid = recipientMemberId + '-' + communityId + '@' + chatServerName;
        connection = new Strophe.Connection(boshServer);
        connection.connect(declineJid, spark.utils.common.getToken(), function (status) {
            if (status === Strophe.Status.CONNECTED) {
                if (connection) {
                    connection.send($pres());
                    connection.send($pres({
                        to: recipientJid,
                        "type": "declined"
                    }));
                }
            } else if (status === Strophe.Status.CONNFAIL) {
                //spark.log.logError('Connection to chat server at ' + boshServer + ' failed.');
            } else if (status === Strophe.Status.AUTHFAIL) {
                //.log.logError('authorization with chat server failed');
            }
        });
    };

    function sendMissedIM(messageQueue, targetMemberId) {
        if (messageQueue.length == 0)
            return;

        var postData = {
            recipientMemberId: targetMemberId,
            messages: messageQueue
        };

        spark.api.client.callAPI(spark.api.client.url.missedIM, null, postData, function (success, data) {
            // Do nothing
        });
    };

    function showInvite() {
        var length = invites.length;
        if (length === 0 || hasActiveInvite())
            return;

        var invite = invites[length - 1];
        var senderId = invite.SenderMemberId;
        var key = invite.ConversationKey;
        var canMemberReply = invite.CanMemberReply;
        var conversationTokenId = invite.ConversationTokenId;

        spark.api.client.callAPI(spark.api.client.urls.miniprofile, { targetMemberId: senderId }, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            var data = response.data;
            var isSub = spark.utils.common.isSub();
            var img = $(dialog).find("img");
            var userName = $(dialog).find("span.username");
            var ageLocation = $(dialog).find("span.age-location");
            var noBtn = $(dialog).find("a.no");
            var yesBtn = $(dialog).find("a.yes");
            var imageUrl = (data.primaryPhoto !== null) ?
                data.primaryPhoto.thumbPath : (data.gender === "Male") ?
                    "/images/hd/img_silhouette_man@2x.png" : "/images/hd/img_silhouette_woman@2x.png";

            img.attr("src", imageUrl);
            img.off("click").on("click", function () {
                viewProfile("/profile/" + data.memberId);
            });

            userName.off("click").on("click", function () {
                viewProfile("/profile/" + data.memberId);
            });

            userName.text(data.username);
            var temp = "";
            if (data.age)
                temp = data.age;
            if (temp !== "" && data.location)
                temp += ", " + data.location;
            if (temp === "" && data.location)
                temp = data.location;
            ageLocation.text(temp);

            // Bind event to NO button
            noBtn.off("click").on("click", function () {
                removeInvite(key);
                sendEjabIMRejectedMessage(senderId, conversationTokenId);
                closeInvite();

                spark.omniture.trackEvents("m_im_invitation", "event69",
                    { eVar2: "m_im_invitation" });

                window.setTimeout(showInvite, 500);
            });

            if (!canMemberReply) {
                yesBtn.text("Upgrade");
            }

            yesBtn.unbind("click").bind("click", function (e) {
                removeInvite(key);
                closeInvite();

                var a = $(this);
                if (!canMemberReply) {
                    // Go to subscription page.
                    a.addClass("subscription");
                    a.attr("href", "/subscription/subscribe/2260");
                    a.attr("data-ajax", false);
                } else {
                    var chatUrl = a.attr("href");

                    spark.omniture.trackEvents("m_im_invitation", "event68",
                        { eVar2: "m_im_invitation" });

                    chatUrl += "?memberId=" + data.memberId + "&conversationTokenId=" + conversationTokenId;
                    openIM(chatUrl, senderId);
                    e.preventDefault();
                }

                // Show next invite so when user goes back to original
                // page the user can take action.
                setTimeout(showInvite, 500);
                //e.preventDefault();
            });

            // Show IM invite dialog.
            dialog.removeClass("hidden").show();
            dialog.popup("open");
        });
    };

    function viewProfile(url) {
        $.mobile.changePage(url);
    };

    self.sendEjabIMRejectedMessage = function(senderId, conversationTokenId) {
        sendEjabIMRejectedMessage(senderId, conversationTokenId);
    };

    self.init = function (chatServer) {
        var siteLookup = {
            "blacksingles": 24,
            "jdate": 3,
            "spark": 1,
            "bbwpersonalsplus": 23,
            "cupid": 10
        };

        var domain = getRootDomain();
        var communityName = domain.split('.')[0];
        communityId = siteLookup[communityName];

        boshServer = chatServer;
        var temp = document.createElement('a');
        temp.href = boshServer;
        chatServerName = temp.hostname;
            
        $(window).bind("message", function (event) {
            if (getRootDomain(event.originalEvent.origin) !== getRootDomain(window.location.href)) // TODO: Does not make sense?
                return;

            if (event.originalEvent.data.indexOf("chatterMemberId") !== -1) {
                var params = JSON.parse(event.originalEvent.data);
                handleIMWindowClosing(params);
            }
        });

        invites = [];
        removeInvites = [];

        initializeDialog();
        timerHandler = window.setInterval(getInvites, interval);
    };

    return self;
})();