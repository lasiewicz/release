﻿/// <reference path="../utils/spark.utils.min.js" />
spark.views.admirer = (function () {
    var self = {};
    var isLoading = false;
    var isLastPage = false;
    var isSaving = false;
    var isVoting = false;
    var cookieName = "mos_admirers_pref_" + spark.utils.common.getMemberId();
    var cookieExpires = 30;
    var preferences = {
        minAge: 18,
        maxAge: 29,
        maxDistance: "",
        location:"",
        showOnlyMembersWithPhotos: false
    };
    var batchSize = 25;
    var swiper = null;
    var threshold = 20;
    var pageInit = true;

    function adjustStatusBar(data) {
        if (!data)
            return;

        var bothYesCount = data.MutualYes;
        var yesCount = data.MembersClickedYes;
        var noCount = data.MembersClickedNo;
        var maybeCount = data.MembersClickedMaybe;
        var totalCount = (yesCount + noCount + maybeCount);
        var ul = $("ul.secret-admirer-stats");
        var width = (ul.width() - 126);
        var yesBar = ul.find("li.yes");
        var noBar = ul.find("li.no");
        var maybeBar = ul.find("li.maybe");
        var yesSpanBar = yesBar.find("a span:eq(1)");
        var noSpanBar = noBar.find("a span:eq(1)");
        var maybeSpanBar = maybeBar.find("a span:eq(1)");
        var animationSpeed = 350;

        $("li.bothyes").find("a span:eq(1)").text(bothYesCount);

        if (yesCount > 0) {
            yesSpanBar.animate({
                right: width - (width * (yesCount / totalCount)) + "px"
            }, {
                duration: animationSpeed,
                progress: function (animation, progress, remainingMs) {
                    yesSpanBar.text(Math.ceil(yesCount * progress));
                }
            });
        }

        if (maybeCount > 0) {
            maybeSpanBar.animate({
                right: width - (width * (maybeCount / totalCount)) + "px"
            }, {
                duration: animationSpeed,
                progress: function (animation, progress, remainingMs) {
                    maybeSpanBar.text(Math.ceil(maybeCount * progress));
                }
            });
        }

        if (noCount > 0) {
            noSpanBar.animate({
                right: width - (width * (noCount / totalCount)) + "px"
            }, {
                duration: animationSpeed,
                progress: function (animation, progress, remainingMs) {
                    noSpanBar.text(Math.ceil(noCount * progress));
                }
            });
        }
    };

    self.changeList = function () {
        var nextList = $('select.sa-results').val();
        var url = window.location.href;
        var thisList = $.trim(url.substring(url.lastIndexOf('/') + 1));

        window.location = "/secretadmirer/results/" + nextList;

        var pageName = "m_SA_" + thisList;
        var nextPage = "(m_SA_" + nextList + ")";
    };

    function appendToList(data) {      
        var ul = $("ul.scrollable");
        var lastIndex = (data.length - 1);

        ul.find("li.loading").remove();
        ul.find("li.last").removeClass("last");

        $.each(data, function (index, item) {
            var li = $("<li data-memberid=\"\" />");
            var a = $("<a />");
            var p = $("<p />");
            var img = $("<img />");
            var primary = $("<span class=\"primary\"/>");
            var secondary = $("<span class=\"secondary\"/>");
            var btn = $("<button class=\"change\" data-role=\"none\">Change</button>");
           
            a.attr("href", "/profile/" + item.miniProfile.id);

            //if (!item.miniProfile.thumbnailUrl) {
            //    var thumbnail = (item.miniProfile.gender === "Female") ? "/images/img_silhouette_woman.png" : "/images/img_silhouette_man.png";
            //} else {
            //     thumbnail = item.miniProfile.thumbnailUrl; 
            //}
            
            if ((!item.miniProfile.thumbnailUrl) || (item.miniProfile.selfSuspendedFlag == true) || (item.miniProfile.adminSuspendedFlag == true)) {
                var thumbnail = (item.miniProfile.gender === "Female") ? "/images/hd/img_silhouette_woman@2x.png" : "/images/hd/img_silhouette_man@2x.png";
            } else {
                thumbnail = item.miniProfile.thumbnailUrl;
            }
            if (item.miniProfile.adminSuspendedFlag == true) {
                a = $('<a class="adminSuspended"/>');
            }
            if (item.miniProfile.selfSuspendedFlag == true) {
                a = $('<a class="selfSuspended"/>');
            }


            img.attr("src", thumbnail).appendTo(a);
            primary.text(item.miniProfile.username).appendTo(p);

            var ageText = "";
            var locText = "";

            if (item.miniProfile.age != null) {
                ageText = item.miniProfile.age;
            }

            if (item.miniProfile.location != null) {
                locText = item.miniProfile.location;
                secondary.text(ageText + ", " + locText);
            } else {
                secondary.text(ageText);
            }

            secondary.appendTo(p);
            
            btn.appendTo(a);
            p.appendTo(a);
            a.appendTo(li);

            li.attr("data-memberId", item.miniProfile.id);
            if (index === lastIndex)
                li.addClass("last");
            if (item.miniProfile.isHighlighted) {
                p.addClass('member-highlight');
                li.addClass('member-highlight');
            }
            li.appendTo(ul);
        });

        if (data.length === 0)
            ul.append("<li class=\"no-results\"><p>No results found.</p></li>");

        isLoading = false;
    };

    function appendSlides(ul) {
   
        if (swiper === null)
            return;
        
        if (!isLastPage && !isLoading && loadSlides()) {
            spark.utils.common.showLoading("Please wait...");
            
            isLoading = true;
            var prefs = getPreference();


            prefs.IsNewPrefs = false;
            prefs.seekingGender = $("a#btn_sa_settings.btn-sa-settings").attr("data-seeking");
            //prefs.location = $("a#btn_sa_settings.btn-sa-settings").attr("data-zipcode");

            getMemberSlideShow(batchSize, prefs, function (success, response) {
                spark.utils.common.hideLoading();
                if (!success) {
                    swiper.swipeNext();
                    return;
                }

                threshold += 20;
                var members = response.data;
                var length = members.length;
                var width = $(window).width();

                
                if (length < batchSize)
                    isLastPage = true;

                // Append more slide(s).
                $.each(members, function (index, item) {
                    var li = createSlide(item, width);

                    li.appendTo(ul);
                });

                if (isLastPage)
                    createNoResultSlide(ul, width);

                swiper.reInit();
                swiper.swipeNext();
                isLoading = false;
            });
        } else {

            swiper.swipeNext();

        }
    };

    function createNoResultSlide(ul, width) {
        var li = $("<li class=\"swiper-slide no-result\"><span>Oh no! There are no more members who match your SA preferences. Tap the button below to change your preferences.</span></li>");

        li.css({ width: width });
        li.appendTo(ul);
    };

    function createSlide(item, width) {
        var self = this;
        var li = $("<li class=\"swiper-slide\" />");
        var h3 = $("<h3 />");
        var img = $("<img />");
        var profile = item.miniProfile;
        var photo = profile.defaultPhoto;
        var imageUrl = (photo) ? photo.fullPath : null;

        if (imageUrl) {
            img.bind("load", function (o) {
                var e = o.currentTarget;
                var dimension = getDimension(e.clientWidth, e.clientHeight, width, 320);
                var left = (width - dimension[0]) / 2;
                var top = (320 - dimension[1]) / 2;

                $(e).attr("data-clientheight", e.clientHeight);
                $(e).attr("data-clientwidth", e.clientWidth);
                $(e).css({
                    width: dimension[0] + "px",
                    height: dimension[1] + "px",
                    top: top + "px",
                    left: left + "px",
                    visibility: "visible"
                });
            });


            img.attr("src", imageUrl);
            img.css({ visibility: "hidden" });
            img.appendTo(li);
        }

        h3.on("click", function () {
            window.open("/profile/" + profile.id, "_" + profile.id);
        });
        h3.html("<span>" + profile.username + "</span><br/ ><span>" + profile.age + ", " + profile.location + "</span>");
        h3.appendTo(li);

        li.css({ width: width });
        li.attr("data-memberid", profile.id);

        return li;
    };

    function validateCityByZipCode(zipcode) {
        spark.api.client.callAPI(spark.api.client.urls.get_location, { zipCode: zipcode }, null, function (success, response) {
            if ((response.data == null || response.data == 0) || ($("input#postal_code").val() == "") || response.data[0].Description == "") {
                spark.utils.common.showGenericDialog("Please enter a valid Zip/Postal Code.", function () {
                    //self.text("Save Preferences");
                    $("a#btn_save_sa_prefs.btn-blue").text("Save Preferences");
                    isSaving = false;
                });
                return;
            }
            else {
                spark.utils.common.showMessageBar("Your preferences saved successfully", true, function () {
                    $("a#btn_save_sa_prefs.btn-blue").text("Save Preferences");
                    //self.text("Save Preferences");
                    isSaving = false;

                });

            }
        });
    }

    function getDimension(width, height, maxWidth, maxHeight) {
        var ratio = 1;
        if (width > maxWidth || height > maxHeight) {
            ratio = Math.min(maxWidth / width, maxHeight / height);
        }

        return [(width * ratio), (height * ratio)];
    };

    function getPreference() {
        var cookie = $.cookie(cookieName);
        if (!cookie)
            return null;

        var tempArr = cookie.split("&");
        var prefs = {};

        $.each(tempArr, function (index, item) {
            var length = item.indexOf("=");
            var key = item.substring(0, length);
            var value = item.substring(length + 1);

            prefs[key] = value;
        });
        return prefs;
    };

    function getMatchPreference(callback) {
        var prefs = getPreference();
        if (prefs !== null)
        {
            callback(true, {data: prefs});
            return;
        }

        spark.api.client.callAPI(spark.api.client.urls.get_preferences, null, null, function (success, response) {
            savePreferences(response.data);
            callback(success, response);
        });
    };

    function getMemberSlideShow(pageSize, prefs, callback) {
        var url = spark.api.client.urls.slideShowMembers;

        prefs.batchSize = pageSize;
        spark.api.client.callAPI(url, null, prefs, function (success, response) {
           callback(success, response);
        });
    };

    function getSecretAdmirers(pageSize, pageNo, prefs, callback) {
        var url = spark.api.client.urls.secretAdmirers;

        prefs.PageSize = pageSize;
        prefs.PageNumber = pageNo;

        spark.api.client.callAPI(url, null, prefs, function (success, response) {
            callback(success, response);
        });
    };

    function getSecretAdmirerResult(type) {
        // TODO: Add logic here...
    };

    function hideButtons() {
        $("div.button-container").fadeOut().addClass("hidden");
    };

    function hideStamp() {
        var content = $("div.secret-admirer-page");
        var stamp = content.find("span.stamp");

        stamp.removeClass("yes")
            .removeClass("no")
            .removeClass("maybe");

        stamp.addClass("invisible");
    };

    function initializeGame(members) {
        var ul = $("ul.swiper-wrapper");
        var swiperContainer = $("div.swiper-container");
        var yesButton = $("#btn_sa_yes");
        var maybeButton = $("#btn_sa_maybe");
        var noButton = $("#btn_sa_no");
        var width = $(window).width();

        //reset threshold and last page on init
        self.threshold = 20;
        self.isLastPage = false;

        swiperContainer.css({
            height:320,
            width: width
        });

        $.each(members, function (index, item) {
            var li = createSlide(item, width);
            li.appendTo(ul);
        });

        ul.find("li.loading").remove();

        if (members.length < batchSize) {
            self.isLastPage = true;
        }


        if (members.length === 0) {
            hideButtons();
            createNoResultSlide(ul, width);
        } else if (self.isLastPage) {
            createNoResultSlide(ul, width);
        }

        swiper = swiperContainer.swiper({
            mode: 'horizontal',
            noSwiping: true,
            loop: false,
            onSlideNext: function (o) {
                var activeSlide = $(o.activeSlide());
                if ((activeSlide.hasClass("no-result")))
                    hideButtons();

                hideStamp();
                isVoting = false;
            }
        });

        yesButton.off().on("click", function () {
            voteYNM(1, "m_secret_admirer", "MOS Nav", function (success, response) {
                appendSlides(ul);
            });
        });

        maybeButton.off().on("click", function () {
            voteYNM(3, "m_secret_admirer", "MOS Nav", function (success, response) {
                appendSlides(ul);
            });
        });

        noButton.off().on("click", function () {
            voteYNM(2, "m_secret_admirer", "MOS Nav", function (success, response) {
                appendSlides(ul);
            });
        });

        if(members.length > 0)
            $("div.button-container").fadeIn().removeClass("hidden");
    };

    function loadSlides() {
        var activeSlideIndex = (swiper.activeIndex + 1);
        
        if (isLastPage)
            return false;
        return (activeSlideIndex >= threshold);
    };

    function resizeGame() {
        var content = $("div.secret-admirer-page");
        var swiperContainer = content.find("div.swiper-container");
        var ul = swiperContainer.find("ul");
        var width = $(window).width();

        swiperContainer.css({
            height: 320,
            width: width
        });

        ul.children("li").each(function () {
            var e = $(this);
            var img = e.find("img");
            var dimension = getDimension(img.attr("data-clientwidth"), img.attr("data-clientheight"), width, 320);
            var left = (width - dimension[0]) / 2;
            var top = (320 - dimension[1]) / 2;

            e.css({
                width: width
            });

            img.css({
                width: dimension[0] + "px",
                height: dimension[1] + "px",
                top: top + "px",
                left: left + "px"
            });
        });
    };

    function savePreferences(prefs) {
        if (!prefs)
            return;

        preferences = prefs;

        var location = (preferences.location && preferences.location !== "null") ? preferences.location : "";
        var distance = (preferences.maxDistance !== null) ? preferences.maxDistance : "1000";
        var value = "showOnlyMembersWithPhotos=" + "false" + "&seekingGender=" + preferences.seekingGender + "&maxAge=" + prefs.maxAge + "&minAge=" + prefs.minAge + "&isNewPrefs=" + "true" + "&maxDistance=" + distance + "&batchSize=" + "25" + "&location=" + location.toUpperCase();

        $.cookie(cookieName, value, { path: "/", expires: cookieExpires });
    };

    function saveYNM(memberId, isYes, isNo, isMaybe, pageName, omniName, callback) {
        var params = {
            "Yes": isYes,
            "No": isNo,
            "Maybe": isMaybe,
            "AddMemberId": memberId
        };
        
        spark.api.client.callAPI(spark.api.client.urls.saveYNM, null, params, function (success, response) {
            callback(success, response);
        });

        var prop30 = "";
        if (params.Yes) {
            prop30 = "Y - " + omniName;
        } else if (params.No) {
            prop30 = "N - " + omniName;
        } else if (params.Maybe) {
            prop30 = "M - " + omniName;
        }
    };

    function setPreferences(prefs) {
        preferences = prefs;

        var ddMinAge = $("#min_age");
        var ddMaxAge = $("#max_age");
        var ddDistance = $("#distance");
        var postalCode = $("#postal_code");
       
        ddMinAge.find("select").val(preferences.minAge);
        ddMaxAge.find("select").val(preferences.maxAge);
        ddDistance.find("select").val(preferences.maxDistance);

        if (preferences.location !== "null") {
            postalCode.val(preferences.location);
        }
    };

    function showStamp(votingType) {
        var content = $("div.secret-admirer-page");
        var stamp = content.find("span.stamp");
        var width = content.width();
        var height = (content.height() - 60);
        var left = ((width - stamp.width()) / 2);
        var top = ((height - stamp.height()) / 2);
        var cssStamp = "yes";

        if (votingType === 2) {
            cssStamp = "no"
        } else if (votingType === 3) {
            cssStamp = "maybe"
        }

        stamp.css({
            left: left,
            top: top
        });

        stamp.removeClass("invisible")
            .addClass(cssStamp);
    };

    function voteYNM (votingType, pageName, omniName, callback) {
        if (isVoting)
            return;
        //var slide = swiper.activeSlide();
        //var memberId = ($(slide).attr("data-memberid")) ?
        //    $(slide).attr("data-memberid") : slide.getData("memberid");
        
       var memberId = $("li.swiper-slide.swiper-slide-visible.swiper-slide-active").attr("data-memberid");

        isVoting = true;
        switch (votingType) {
            case 2: // No
                saveYNM(memberId, false, true, false, pageName, omniName, callback);
                break;
            case 3: // Maybe
                saveYNM(memberId, false, false, true, pageName, omniName, callback);
                break;
            default: // Yes
                saveYNM(memberId, true, false, false, pageName, omniName, callback);
                break;
        }
        showStamp(votingType);
    };

    // TODO: Move to utility class
    function validateZipcode(zipcode) {
        var CNRegex = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;  // Validate either with or without " ", "-"
        var USRegex = /(^\d{5}$)|(^\d{5}-\d{4}$)/;  // Validate either 5 or 5+4 digits
        var isValid = false;

        if (zipcode.length == 5) {
            isValid = USRegex.exec(zipcode);
        } else if (zipcode.length == 6) {
            isValid = CNRegex.exec(zipcode);
        } else if (zipcode == "") {
            isValid = true;
        } else {
            return false;
        }

        return isValid;
    }

    self.getAdmirers = function () {
        if (isLoading || isLastPage)
            return;

        isLoading = true;

        var page = $("#secretadmirer_page");
        var url = spark.api.client.urls.secretAdmirers;
        var prefs = getPreference();
        var pageSize = page.attr("data-pagesize");
        var pageNo = Number(page.attr("data-pageno"));

        prefs.PageSize = pageSize;
        prefs.PageNumber = (pageNo + 1);
       
        // Persist current page number. This wil be used as reference to the
        // next page number.
        page.attr("data-pageno", (pageNo + 1));

        spark.api.client.callAPI(url, null, prefs, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            if (response.data.length < pageSize)
                isLastPage = true;

            appendToList(response.data);
        });
    };

    self.getResults = function() {
        spark.views.hotlist.getHotlistCount(function(data) {
            spark.views.admirer.resultsCount(data);
        });
    };

    self.resultsList = function (url) {
        if (isLoading || isLastPage)
            return;

        isLoading = true;

        var ul = $("ul.scrollable");
        ul.off().click(function (e) {
            var target = $(e.target);

            if (target.is("button")) {
                var li = target.parent().parent();
                var id = li.attr("data-memberid");
                var img = li.find("a img").attr("src");
                var username = li.find("a p span.primary").text();
                var ageLocation = li.find("a p span.secondary").text();

                spark.views.admirer.changeYNM(id, img, username, ageLocation);
                e.preventDefault();

                return;
            }
        });

        spark.utils.views.addLoadingPanel(ul);

        var page = $("#secret_admirer_results_page");
        var url = url;
        page.attr("data-pagesize", 25);
        var pageNo = page.attr("data-pageno");
        var pageSize = page.attr("data-pagesize");
        
        url(pageSize, pageNo, function (success, response) {
            // Persist current page number. This wil be used as reference to the
            // next page number.
            pageNo++;
            page.attr("data-pageno", pageNo);
            if (response.data.length < pageSize)
                isLastPage = true;

            appendToList(response.data);
        });
        
    };

    self.changeYNM = function(id, img, primary, secondary) {
        var dialog = $(".ynm_dialog");
        var yesButton = $(".yes");
        var maybeButton = $(".maybe");
        var noButton = $(".no");
        var page = $("select").val();
        dialog.trigger("create");
        dialog.popup({ tolerance: "15,5,15,5" });
        dialog.attr("data-memberid", id);
        dialog.find("img").attr("src", img);
        dialog.find("p span.primary").text(primary);
        dialog.find("p span.secondary").text(secondary);
        
        // Hack to disable closing popup when users tap outside.
        dialog.on({
            popupbeforeposition: function() {
                $('.ui-popup-screen').off();
            }
        });

        dialog.removeClass("hidden").show();
        dialog.popup("open");
        var omniName;

        if (page === "yes" || page === "bothyes") {
            omniName = "Y";
        } else if (page === "maybe") {
            omniName = "M";
        } else if (page === "no") {
            omniName = "N";
        }

        yesButton.off().on("click", function () { 
            saveYNM(id, true, false, false, "m_secret_admirer_results", "change from " + omniName, function (success, response) {
                dialog.popup("close");
                location.reload();
                });
            
        });

        maybeButton.off().on("click", function () {
            saveYNM(id, false, false, true, "m_secret_admirer_results", "change from " + omniName, function (success, response) {
                dialog.popup("close");
                location.reload();
            });
        });

        noButton.off().on("click", function () {
            saveYNM(id, false, true, false, "m_secret_admirer_results", "change from " + omniName, function (success, response) {
                dialog.popup("close");
                location.reload();
                });
        });
};

    self.resultsCount = function (data) {
        var yesCount = data.MembersClickedYes;
        var noCount = data.MembersClickedNo;
        var maybeCount = data.MembersClickedMaybe;
        var bothCount = data.MutualYes;
        var url = window.location.href;
        var value = $.trim(url.substring(url.lastIndexOf('/') + 1));

        if (value === "yes"){
            $(".page-header h2 span.ynm_count").text(yesCount);
            $("#ynm_sort_filter select option[value='yes']").prop("selected", true);
            spark.views.admirer.resultsList(spark.views.hotlist.getMembersClickedYes);

        } else if (value === "no") {
            $(".page-header h2 span.ynm_count").text(noCount);
            $("#ynm_sort_filter select option[value='no']").prop("selected", true);
            spark.views.admirer.resultsList(spark.views.hotlist.getMembersClickedNo);
        } else if (value === "maybe") {
            $(".page-header h2 span.ynm_count").text(maybeCount);
            $("#ynm_sort_filter select option[value='maybe']").prop("selected", true);
            spark.views.admirer.resultsList(spark.views.hotlist.getMembersClickedMaybe);

        } else if (value === "bothyes") {
            $(".page-header h2 span.ynm_count").text(bothCount);
            $("#ynm_sort_filter select option[value='bothyes']").prop("selected", true);
            spark.views.admirer.resultsList(spark.views.hotlist.getMutualYes);
        }
    };

    self.init = function () {
        $(window).off("resize.secretadmirerpage").on("resize.secretadmirerpage", function () {
            resizeGame();
        });

        getMatchPreference(function (success, response) {
            var prefs = response.data;
            prefs.IsNewPrefs = true;
            prefs.seekingGender = $("a#btn_sa_settings.btn-sa-settings").attr("data-seeking");
            //prefs.location = $("a#btn_sa_settings.btn-sa-settings").attr("data-zipcode");
            spark.views.admirer.getMemberSlideShow(batchSize, prefs, function (success, response) {
                var members = response.data;
                initializeGame(members);
            });
        });
    };

    self.initResults = function () {
        isLastPage = false;
        isLoading = false;

        var url = window.location.href;
        var value = $.trim(url.substring(url.lastIndexOf('/') + 1));
        $("#ynm_sort_filter select option[value='"+value+"']").prop("selected", true);

        spark.views.admirer.getResults();
    };

    self.initSettings = function () {
        spark.views.hotlist.getHotlistCount(function (data) {
            adjustStatusBar(data);
        });

        getMatchPreference(function (success, response) {
            if (!success)
                response.data = preferences;

            setPreferences(response.data);
            $("#btn_save_sa_prefs").off().click(function () {
                if (isSaving)
                    return;

                isSaving = true;
                var self = $(this);
                self.text("Saving...");

                var prefs = preferences;
                var ddMinAge = $("#min_age");
                var ddMaxAge = $("#max_age");
                var ddDistance = $("#distance");
                var postalCode = $("#postal_code");

                prefs.minAge = ddMinAge.find("select").val();
                prefs.maxAge = ddMaxAge.find("select").val();
                prefs.maxDistance = ddDistance.find("select").val();
                prefs.location = $("#postal_code").val();

                if (prefs.minAge > prefs.maxAge || prefs.minAge == "" || prefs.maxAge == "") {
                    spark.utils.common.showGenericDialog("Please select valid age range.", function () {
                        self.text("Save Preferences");
                        isSaving = false;
                    });
                    return;
                }

                if (prefs.location == "") {
                    spark.utils.common.showGenericDialog("Please enter a valid Zip/Postal Code.");
                }

                //else if (!validateZipcode(prefs.location)) {
                //    spark.utils.common.showGenericDialog("Please enter a valid Zip/Postal Code.", function () {
                //        self.text("Save Preferences");
                //        isSaving = false;
                //    });
                //    return;
                //}

                validateCityByZipCode(prefs.location);

                savePreferences(prefs);
                //spark.utils.common.showMessageBar("Your preferences saved successfully", true, function () {
                //    self.text("Save Preferences");
                //    isSaving = false;
                //});

                
            });
        });
    };

    self.getMatchPreference = getMatchPreference;
    self.getMemberSlideShow = getMemberSlideShow;
    self.getSecretAdmirers = getSecretAdmirers;
    self.saveYNM = saveYNM;
    return self;
})();

$(document).on("pageshow", "#secret_admirer_results_page", function () {
    spark.views.admirer.initResults();
    $(document).on("click", ".adminSuspended", function () {
        spark.utils.common.showGenericDialog('');
        $('#generic_dialog .content').html("<b>Profile Unavailable</b><br/><br/>This profile has been removed from the site.</br/>Try browsing more profiles!");
        $('#generic_dialog').addClass('high');
    });
    $(document).on("click", ".selfSuspended", function () {
        spark.utils.common.showGenericDialog('');
        $('#generic_dialog .content').html("<b>Profile Unavailable</b><br/><br/>This member either became a Success Story or temporarily removed their account.");
        $('#generic_dialog').addClass('high');
    });
   
});

$(document).on("pageshow", "#secret_admirer_page", function () {
    spark.views.admirer.init();
});

$(document).on("pageshow", "#secret_admirer_settings_page", function () {
    spark.views.admirer.initSettings();
});