﻿spark.views.matches = (function () {
    "use strict";
    var self = {};
    var memberList = [];
  
    var regionID = "";
    var sortType = 1;
    var resultPageNumber = 1;

    var showPreferencePage = false;
    var pageInit = true;
    var isLoading = false;
    var isLastPage = false;

    var gridHeight = 0;
    var gridWidth = 0;


    function setupButtonClickEvent() {
        $(document).on("tap", "li.block", function () {
            spark.utils.common.showGenericDialog("This profile is currently not available");
        });

        $("#matches_sort_filter").find("select").on("change", function () {

            $("#matches_list").find("li").remove();
            $("#matches_list").find("p").remove();
            memberList = [];
            isLastPage = false;
            sortType = parseInt(this.value);
            callAPIforMatchesData(1);

        });
    }

    function appendToList(data) {
        var ul = $("ul.scrollable");

        // Remove loading footer.
        ul.find("li.loading").remove();
        ul.find("p.no-results-text").remove();

        if (data.length == 0) {
            var p = $("<p />");
            p.text("No results found!");
            p.addClass("no-results-text");
            p.appendTo(ul);
        }

        $.each(data, function (index, item) {
            var li = $("<li data-memberid=\"" + item.id + "\" />");
            var a = $("<a />");
            var img = $("<img />");
            var p = $("<p />");
            var userName = $("<span class=\"primary\" />");
            var ageLocation = $("<span class=\"secondary\" />");
            var online = $("<span class=\"online\" />");
            var mPercentage = $("<span class = \"matches-label\" />");
            var imageUrl = (item.gender === "Female") ? "/images/hd/img_silhouette_woman@2x.png" : "/images/hd/img_silhouette_man@2x.png";

            a.attr("href", "/profile/" + item.id);
            if (item.blockContactByTarget || item.selfSuspendedFlag || item.adminSuspendedFlag) {
                li.addClass("block");
                a.attr("href", "#");
            }

            if (item.defaultPhoto && item.defaultPhoto.thumbPath !== "") {
                imageUrl = item.defaultPhoto.thumbPath;
            }
            else if (item.DefaultPhoto && item.DefaultPhoto.thumbPath !== "") {
                imageUrl = item.DefaultPhoto.thumbPath;
            }

            img.attr("src", imageUrl).appendTo(a);

            userName.text(item.username).appendTo(p);

            var ageText = "";
            var locText = "";

            if (item.age !== null) {
                ageText = item.age;
            }

            if (item.location !== null) {
                locText = item.location;
                ageLocation.text(ageText + ", " + locText).appendTo(p);
            }
            else {
                ageLocation.text(ageText).appendTo(p);
            }

            if (item.isOnline)
                online.appendTo(p);

            if (!ul.hasClass("grid-view")) {
                online.text("Online");
            } else {
                // If in grid mode, apply neccessary in-style to override the CSS class.
                li.css({
                    height: gridHeight + "px"
                });

                img.css({
                    width: gridWidth + "px",
                    height: gridHeight + "px"
                });
            }

            p.appendTo(a);

            if (item.matchRating != null) {
                //if (item.matchRating !== 0) {

                mPercentage.text(item.matchRating + "% match");
                mPercentage.appendTo(a);
                // }

            }
            if (item.isHighlighted) {
                p.addClass('member-highlight');
                li.addClass('member-highlight');
            }

            a.appendTo(li);
            li.appendTo(ul);

            memberList.push(item);
        });


        if (pageInit && spark.utils.common.loadViewToggleStatus() === "grid") {
            showGrid(ul);
            $("div#mos_christian_matches_list_top_320x50").addClass("hidden");
            $("div#mos_christian_matches_grid_top_320x50").removeClass("hidden");
        }

        isLoading = false;
        pageInit = false;
    }

    function initializeListToggle() {
        var ul = $("#matches_list");
        var toggle = $(".list-grid-toggle");
        var listButton = toggle.find("a.list");
        var gridButton = toggle.find("a.grid");
        //var gridClass = "grid-view";

        if (spark.utils.common.loadViewToggleStatus() === "grid") {
            $("#btn_grid_view").addClass("active");
            $("#btn_list_view").removeClass("active");
        }

        listButton.off().on("click", function (e) {
            showList(ul);
            $("div#mos_christian_matches_list_top_320x50").removeClass("hidden");
            $("div#mos_christian_matches_grid_top_320x50").addClass("hidden");
           
        });

        gridButton.off().on("click", function (e) {
            showGrid(ul);
            $("div#mos_christian_matches_list_top_320x50").addClass("hidden");
            $("div#mos_christian_matches_grid_top_320x50").removeClass("hidden");
        });
    };

    function callAPIforMatchesData(pageNo) {
        isLoading = true;

        var ul = $("ul.scrollable");
        var page = $("#matches_page");
        var url = spark.api.client.urls.matches;
        //var url = spark.api.client.urls.getSearchResults;
        var pageSize = page.attr("data-pagesize");
        
        var prefs = {
            searchOrderByType: sortType
        };

        var params = {
            pageSize: pageSize,
            pageNumber: pageNo
        };

        prefs.PageSize = page.attr("data-pagesize");
        prefs.PageNumber = (pageNo + 1);

        // Persist current page number. This wil be used as reference to the
        // next page number.

        // Add loading footer.
        spark.utils.views.addLoadingPanel(ul);

        window.setTimeout(function () {
            spark.api.client.callAPI(url, params, prefs, function (success, response) {
                if (!success) {
                    isLoading = false;
                    spark.utils.common.showError(response);
                    $("ul.scrollable").find("li.loading").remove();
                    return;
                }

                resultPageNumber++;
                page.attr("data-pageno", pageNo);

                if (response.data.length < pageSize)
                    isLastPage = true;

                appendToList(response.data.members);

            });
        }, spark.utils.common.getLoadingTime());
    }

    function resizeGridImage(ul) {
        var defaultImageWidth = 73;
        var defaultImageHeight = 95;
        var marginWidth = 1;
        var maxThreshold = 10;
        var imageWidth = defaultImageWidth + marginWidth;
        var ratio = 1;
        var screenWidth = $(window).width();
        var gridCount = Math.floor((screenWidth / imageWidth));
        var gridTotalWidth = (gridCount * imageWidth);
        var threshold = (screenWidth - gridTotalWidth);
        var padding = 0;

        if (threshold > maxThreshold) {
            gridCount = (gridCount + 1);
            gridWidth = Math.floor((screenWidth / gridCount)) - marginWidth;
            gridTotalWidth = (gridCount * (gridWidth + marginWidth));
            threshold = (screenWidth - gridTotalWidth);

            ratio = Math.round((gridWidth / imageWidth) * 100) / 100;
            gridHeight = Math.round((defaultImageHeight * ratio));
        }

        ul.css({ paddingLeft: 0 });
        if (threshold > 0) {
            padding = Math.round((threshold / 2)) - marginWidth;
            ul.css({
                paddingLeft: padding + "px"
            });
        }

        if (gridHeight !== 0 && gridWidth !== 0) {
            ul.find("li").css({
                height: gridHeight + "px"
            });

            ul.find("li > a > img").css({
                width: gridWidth + "px",
                height: gridHeight + "px"
            });

            ul.find("li > a > div.thumb").css({
                width: gridWidth + "px",
                height: gridHeight + "px"
            });
        }
    };

    function showGrid(ul) {
        if (ul.hasClass("grid-view"))
            return;


        if (ul.find("li").hasClass("loading")) {
            return;
        }

        ul.addClass("grid-view");
        $("#btn_grid_view").addClass("active");
        $("#btn_list_view").removeClass("active");
        spark.utils.common.setCookieForViewToggle("grid");

        resizeGridImage(ul);

        ul.hide();
        ul.find("span.online").text("");
        ul.fadeIn(350);

        setTimeout(function () {
            googletag.pubads().refresh([gpt_matches_grid_top]);
        }, 800);
    };

    function showList(ul) {
        if (!ul.hasClass("grid-view"))
            return;

        $("#btn_list_view").addClass("active");
        $("#btn_grid_view").removeClass("active");

        ul.removeAttr("style");
        ul.find("li").removeAttr("style");
        ul.find("li > a > img").removeAttr("style");
  
        ul.hide();
        ul.removeClass("grid-view");
        ul.find("span.online").text("Online");
        spark.utils.common.setCookieForViewToggle("list");
        ul.fadeIn(350);
        setTimeout(function () {
            googletag.pubads().refresh([gpt_matches_top]);
        }, 800);


    }


    self.getMatches = function () {
        var cachedData = $(document).data("cachedData");

        if (cachedData != null) {
            if (cachedData.cachedPage === "matches") {
                return;
            }
        }

        if (showPreferencePage) {
            return;
        }

        if (pageInit) {
            return;
        }

        if (isLoading || isLastPage)
            return;

        var pageNo = Number($("#matches_page").attr("data-pageno"));
        callAPIforMatchesData(++pageNo);
    };

    self.init = function () {
        pageInit = true;
        initializeListToggle();
        setupButtonClickEvent();

        var cachedData = $(document).data("cachedData");

        if (cachedData != null) {
            if (cachedData.cachedPage === "matches") {
                self.resumePageStatus();
            }
        }
        else {
            callAPIforMatchesData(1);
        }
    };

    self.resizeGrid = function (ul) {
        resizeGridImage(ul);
    };

    self.storeMembers = function () {
        var members = [];
        $("#matches_list").children("li").each(function () {
            var li = $(this);
            members.push({
                memberId: li.attr("data-memberid"),
                thumbnail: li.find("a img").attr("src"),
                userName: li.find(".primary").text(),
                match: li.find(".matches-label").text()
            });
        });

        // Cache data so that it can be retrieve from the Profile page.
        $(document).data("members", members);
        //$(document).data('spotlight', spotlight);

        var isGrid = false;
        var ul = $("#matches_list");
        if (ul.hasClass("grid-view")) {
            isGrid = true;
        }

        $(document).data("cachedData", {
            cachedPage: "matches",
            isGrid: isGrid,
            pageNumber: $("#matches_page").attr("data-pageno"),
            lastPage: isLastPage,
            locationID: regionID,
            offsetY: $(document).scrollTop(),
            membersData: memberList,
            //spotlight: spotlight,
        });
    }

    self.cleanCache = function () {
        $(document).removeData("members");
        $(document).removeData("cachedData");
    }

    self.resumePageStatus = function () {
        var cachedData = $(document).data("cachedData");

        $("#matches_page").attr("data-pageno", cachedData.pageNumber);
        isLastPage = cachedData.lastPage;
        isLoading = true;
        regionID = cachedData.locationID;
        memberList = [];
        //if (cachedData.spotlight) {
            //spark.views.search.buildSpotlight(cachedData.spotlight[0]);
        //}
        resultPageNumber++;
        appendToList(cachedData.membersData);
        $(document).scrollTop(cachedData.offsetY);
        
        //if (cachedData.isGrid) {
        //    var ul = $("#matches_list");
        //    showGrid(ul);
        //}

        setTimeout(function () {
            self.cleanCache();
        }, 500);
    }

    self.reset = function () {
        memberList = [];
        pageInit = true;
    }

    return self;
})();

$(document).on("pageshow", "#matches_page", function () {
    spark.views.matches.init();
    if (spark.utils.common.loadViewToggleStatus() == "list") {
        setTimeout(function () {
            googletag.pubads().refresh([gpt_matches_top]);
        }, 800);
    }
   if (spark.utils.common.loadViewToggleStatus() == "grid") {
        setTimeout(function () {
            googletag.pubads().refresh([gpt_matches_grid_top]);
        }, 800);
    }

    $(window).off("orientationchange").on("orientationchange", function (event) {
        var ul = $("#matches_list");

        if (ul && ul.hasClass("grid-view")) {
            spark.views.matches.resizeGrid(ul);
        }
    });
});

$(document).on("pagebeforehide", "#matches_page", function (e, ui) {
    //check if the next page is the profile page
    if (ui.nextPage.attr('id') === 'profile_page') {
        spark.views.matches.cleanCache();
        spark.views.matches.storeMembers();
    }
    else {
        spark.views.matches.cleanCache();
    }  
});

$(document).on("pagehide", "#matches_page", function () {
    spark.views.matches.reset();
});

$(document).on('tap', '#matches_list li a', function (e) {
    var a = $(this);

    //get a single item match % -- delete once multiple profiles are re-enabled
    $(document).removeData("match");
    var match = [];
    var li = $(this).parent("li");
    match.push({
            match: li.find(".matches-label").text()
    });
    $(document).data("match", match);
});
