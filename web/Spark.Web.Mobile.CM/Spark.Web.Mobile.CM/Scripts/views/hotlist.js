﻿spark.views.hotlist = (function () {
    var self = {};
    var isLoading = false;
    var isLastPage = false;

    var memberList = [];
    var pageInit = true;

    function appendToList(data) {
        var ul = $("ul.scrollable");

        // Remove loading footer.
        ul.find("li.loading").remove();

        if (data.length == 0) {
            var p = $("<p />");
            p.text("No results found!");
            p.addClass("no-results-text");
            p.appendTo(ul);
        }

        $.each(data, function (index, item) {
            var li = $("<li data-memberid=\"" + item.miniProfile.id + "\" />");
            var a = $("<a />");
            var img = $("<img />");
            var p = $("<p />");
            var userName = $("<span class=\"primary\" />");
            var ageLocation = $("<span class=\"secondary\" />");
            var online = $("<span class=\"online\" />");
            var imageUrl = (item.miniProfile.gender === "Male") ? "/images/hd/img_silhouette_man@2x.png" : "/images/hd/img_silhouette_woman@2x.png";

            a.attr("href", "/profile/" + item.miniProfile.id);

            //if (item.miniProfile.defaultPhoto != null) {
            //    if (item.miniProfile.defaultPhoto.thumbPath && item.miniProfile.defaultPhoto.thumbPath !== "") {
            //        imageUrl = item.miniProfile.defaultPhoto.thumbPath;
            //    }
            //}
             if (item.miniProfile.defaultPhoto != null) {
                 if (item.miniProfile.defaultPhoto.fullPath && item.miniProfile.defaultPhoto.thumbPath !== "") {
                     imageUrl = item.miniProfile.defaultPhoto.fullPath;
                }
            }

            img.attr("src", imageUrl).appendTo(a);
            userName.text(item.miniProfile.username).appendTo(p);
            ageLocation.text(item.miniProfile.age + ", " + item.miniProfile.location).appendTo(p);

            if (item.miniProfile.isOnline) {
                online.appendTo(p);
            }

            if (!ul.hasClass("grid-view")) {
                online.text("Online");
            } else {
                // If in grid mode, apply neccessary in-style to override the CSS class.
                li.css({
                    height: gridHeight + "px"
                });

                img.css({
                    width: gridWidth + "px",
                    height: gridHeight + "px"
                });
            }

            if (item.miniProfile.isHighlighted) {
                p.addClass('member-highlight');
                li.addClass('member-highlight');
            }

            p.appendTo(a);
            a.appendTo(li);
            li.appendTo(ul);
            
            memberList.push(item);
        });

        bindClickEvents();

        isLoading = false;
        pageInit = false;
    };

    function getFavoritedMe(pageSize, pageNo, callback) {
        getHotlist("added_you_to_favorites", pageSize, pageNo, callback);
    };

    function getFlirtedMe(pageSize, pageNo, callback) {
        getHotlist("smiles_received", pageSize, pageNo, callback);
    };

    function getEmailedMe(pageSize, pageNo, callback) {
        getHotlist("emails_received", pageSize, pageNo, callback);
    };

    function getMyEmailed(pageSize, pageNo, callback) {
        getHotlist("emails_sent", pageSize, pageNo, callback);
    };

    function getMyTeased(pageSize, pageNo, callback) {
        getHotlist("smiles_sent", pageSize, pageNo, callback);
    };

    function getMyIMed(pageSize, pageNo, callback) {
        getHotlist("ims_sent", pageSize, pageNo, callback);
    };

    function getMyFavorites(pageSize, pageNo, callback) {
        getHotlist("your_favorites", pageSize, pageNo, callback);
    };

    function getImedMe(pageSize, pageNo, callback) {
        getHotlist("ims_received", pageSize, pageNo, callback);
    };

    function getMyViewed(pageSize, pageNo, callback) {
        getHotlist("views_sent", pageSize, pageNo, callback);
    };

    function getMembersClickedNo(pageSize, pageNo, callback) {
        getHotlist("voted_no", pageSize, pageNo, callback);
    }

    function getMembersClickedMaybe(pageSize, pageNo, callback) {
        getHotlist("voted_maybe", pageSize, pageNo, callback);
    }

    function getMembersClickedYes(pageSize, pageNo, callback) {
        getHotlist("voted_yes", pageSize, pageNo, callback);
    }

    function getMutualYes(pageSize, pageNo, callback) {
        getHotlist("mutual_yes", pageSize, pageNo, callback);
    }

    function getHotlist(category, pageSize, pageNo, callback) {
        var url = spark.api.client.urls.hotlist;
        var params = {
            hotlist: category,
            pageSize: pageSize,
            pageNumber: pageNo
        };

        spark.api.client.callAPI(url, params, null, function (success, response) {
            callback(success, response);
        });
    };

    function getViewedMe(pageSize, pageNo, callback) {
        getHotlist("views_received", pageSize, pageNo, callback);
    };

    function getHotlistData(pageNo) {
        isLoading = true;

        var page = $("#favorites_page");
        var url = spark.api.client.urls.hotlist;
        //var pageNo = Number(page.attr("data-pageno"));
        var pageSize = page.attr("data-pagesize");
        var params = {
            hotlist: "your_favorites",
            pageSize: pageSize,
            pageNumber: pageNo
        };

        var ul = $("ul.scrollable");
        spark.utils.views.addLoadingPanel(ul);

        var interval = ul.find("li").length !== 0 ? spark.utils.common.getLoadingTime() : 0;

        window.setTimeout(function () {
            spark.api.client.callAPI(url, params, null, function (success, response) {
                if (!success) {
                    isLoading = false;
                    spark.utils.common.showError(response);
                    $("ul.scrollable").find("li.loading").remove();
                    return;
                }

                page.attr("data-pageno", pageNo);
                if (response.data.length < pageSize) {
                    isLastPage = true;
                }

                appendToList(response.data);
            });
        }, interval);
    }

    function bindClickEvents() {
        var li = $('#favorites_list li a');
        var memberId = spark.utils.common.getMemberId();
       
        li.off().on("click", function (e) {
            //e.preventDefault();
           //var targetMemberId = $(this).parent('li').attr('data-memberid');
           // spark.utils.views.getBlockedUsers(memberId, targetMemberId);
        });
    };

    self.getFavorites = function () {
        var cachedData = $(document).data("cachedData");

        if (cachedData != null) {
            if (cachedData.cachedPage == "favorites") {
                return;
            }
        }

        if (pageInit) {
            return;
        }

        if (isLoading || isLastPage)
            return;

        var pageNo = Number($("#favorites_page").attr("data-pageno"));
        getHotlistData(++pageNo);
    };

    self.getHotlistCount = function (callback) {
        
        spark.api.client.callAPI(spark.api.client.urls.hotlistCount, null, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            
            if(typeof callback === "function")
                callback(response.data);
        });
    };

    self.getFavoritedMe = getFavoritedMe;
    self.getFlirtedMe = getFlirtedMe;
    self.getEmailedMe = getEmailedMe;
    self.getViewedMe = getViewedMe;
    self.getImedMe = getImedMe;
    self.getMyEmailed = getMyEmailed;
    self.getMyViewed = getMyViewed;
    self.getMyTeased = getMyTeased;
    self.getMyIMed = getMyIMed;
    self.getMyFavorites = getMyFavorites;
    self.getMembersClickedYes = getMembersClickedYes;
    self.getMembersClickedNo = getMembersClickedNo;
    self.getMembersClickedMaybe = getMembersClickedMaybe;
    self.getMutualYes = getMutualYes;
    self.bindClickEvents = bindClickEvents;
  

    self.init = function () {
        isLastPage = false;
        pageInit = true;
        var cachedData = $(document).data("cachedData");

        if (cachedData != null) {
            if (cachedData.cachedPage == "favorites") {
                self.resumePageStatus();
            }
        }
        else {
            getHotlistData(1);
        }

        spark.views.hotlist.bindClickEvents();
    };

    self.isOnline = function() {
        var li = $("ul.scrollable li");
        li.each(function () {
            var id = $(this).attr("data-memberid");

            spark.api.client.callAPI(spark.api.client.urls.miniprofile, { targetMemberId: id }, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }
                if (response.data.isOnline) {
                    $("li[data-memberid="+id+"]").find("p span.online").show();
                }
            });

        });
    };
  
    self.storeMembers = function () {
        var members = [];
        var count = 1;
        $("#favorites_list").children("li").each(function () {
            var li = $(this);
            li.attr("data-index", count++);
            members.push({
                memberId: li.find("a").attr("href").replace("/profile/", ""),
                thumbnail: li.find("a img").attr("src"),
                userName: li.find("a span.primary").text(),
                slideNo: li.attr("data-index")
            });
        });

        // Cache data so that it can be retrieve from the Profile page.
        $(document).data("members", members);

        $(document).data("cachedData", {
            cachedPage: "favorites",
            pageNumber: $("#favorites_page").attr("data-pageno"),
            lastPage: isLastPage,
            offsetY: $(document).scrollTop(),
            membersData: memberList,
        });
    }

    self.cleanCache = function () {
        $(document).removeData("members");
        $(document).removeData("cachedData");
    }

    self.resumePageStatus = function () {
        var cachedData = $(document).data("cachedData");

        $("#favorites_page").attr("data-pageno", cachedData.pageNumber);
        isLastPage = cachedData.lastPage;
        isLoading = true;
        memberList = [];

        appendToList(cachedData.membersData);
        $(document).scrollTop(cachedData.offsetY);

        setTimeout(function () {
            self.cleanCache();
        }, 500);
    }

    self.reset = function () {
        memberList = [];
        pageInit = true;
    }

    return self;
})();

$(document).on("pageshow", "#favorites_page", function (e, ui) {
    //spark.views.hotlist.isOnline();
    spark.views.hotlist.init();
});
