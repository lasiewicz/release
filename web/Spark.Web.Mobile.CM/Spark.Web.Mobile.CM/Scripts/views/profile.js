﻿var initCheck = true;
spark.views.baseprofile = function () {
    var base = {};
    
    function addTabEvents() {
        var tabs = $(".profile-tabs");
        tabs.children("li").each(function () {
            var li = $(this);

            li.find("a").click(function (e) {
                var a = $(this);

                changeTab(a.attr("href"));
                e.preventDefault();
            });
        });
    }

    

    function changeTab(section) {

        var tab = "In My Own Words";
        var options;
        if (section.substring(1) == "my_details") {
            tab = "My Details";
        } else if (section.substring(1) == "my_ideal_match") {
            tab = "My Ideal Match";
        }
       
        var tabContents = $("[data-role='tabcontent']");
        var tabs = $(".profile-tabs");
        var selectedIndex = 0;

        tabs.children("li").each(function (index) {
            var li = $(this);
            var a = li.find("a");

            a.removeClass("active")
                .removeClass("border-left")
                .removeClass("border-right");
            if (a.attr("href") === section) {
                a.addClass("active");
                selectedIndex = index;
            }

            var parent = li.parent();
            switch (selectedIndex) {
                case 0:
                    parent.find("li:eq(1) a").addClass("border-left");
                    parent.find("li:eq(2) a").addClass("border-left");
                    break;
                case 1:
                    parent.find("li:eq(0) a").addClass("border-right");
                    parent.find("li:eq(2) a").addClass("border-left");
                    break;
                default:
                    parent.find("li:eq(0) a").addClass("border-right");
                    parent.find("li:eq(1) a").removeClass("border-left").addClass("border-right");
                    break;
            }
        });

        tabContents.each(function () {
            var tab = $(this);
            if (tab.attr("id") !== section.substring(1)) {
                tab.hide();
            } else {
                tab.show();

                var tabName = "In My Own Words";
                if (section.substring(1) == "my_details") {
                    tabName = "My Details";
                } else if (section.substring(1) == "my_ideal_match") {
                    tabName = "My Ideal Match";
                }

            }
        });
    }

    // Public method(s).
    base.addTabEvents = addTabEvents;
    base.getPhotos = getPhotos;
    //base.showProfile = showProfile;
    return base;
};

spark.views.profile = (function () {
    var self = {};

    self.init = function () {

        if ($(document).data("blockStatus")) {

        }
        spark.views.profile.addTabEvents();
        $("a[href='#in_my_own_words']").click();
        initCheck = false;
        $("#profile_page .prevButton").css({ "visibility": "visible" }).click(function () {
            if (history.length == 1) {
                window.close();
                spark.utils.common.showGenericDialog("Please close this window if it does not close automatically");
            } else {
                history.back(-1);
            }
        });

        //wire up click handlers
        $("li.block-btn a.ui-btn.ui-btn-inline.block.selected").on("tap", tapBlockSelected);
        $("a.ui-btn.ui-btn-inline.submit-btn").on("tap", submitBlockedProfile);
        $("a.ui-btn.ui-btn-inline.cancel-btn").on("tap", cancelBlock);


        // Bind event to close button (if present).
        $("#btn_header_close").off().click(function () {
            window.close();
            spark.utils.common.showGenericDialog("Please close this window if it does not close automatically");
        });
    };

    self.secretAdmirerGame = function () {

        function saveYNM(memberId, isYes, isNo, isMaybe, callback) {
            var params = {
                Yes: isYes,
                No: isNo,
                Maybe: isMaybe,
                AddMemberId: memberId
            };

            spark.api.client.callAPI(spark.api.client.urls.saveYNM, null, params, function(success, response) {
                callback(success, response);
            });
        }

        var url = window.location.pathname;
        var sa = url.substr(0, 11);
        if (sa === "/profile/sa"){
            if ($("#yesno").hasClass("yes") || $("#yesno").hasClass("yesBoth") || $("#yesno").hasClass("no") || $("#yesno").hasClass("maybe")) {
                $("#secret_admirer").hide();
            } else {
                $(".profile.secret-admirer").css("display","block");
            }
        
            var id = $(".first").attr("data-memberid");
            var yesButton = $("a.yes");
            var maybeButton = $("a.maybe");
            var noButton = $("a.no");
            var pageName = "m_profile";

            yesButton.off().on("click", function () {
                saveYNM(id, true, false, false, function (success, response) {
                    $(".profile.secret-admirer").fadeOut(400);
                    $("#yesno").addClass("yes");
                });
            });

            maybeButton.off().on("click", function () {
                saveYNM(id, false, false, true, function (success, response) {
                    $(".profile.secret-admirer").fadeOut(400);
                    $("#yesno").addClass("maybe");
                });
            });

            noButton.off().on("click", function () {
                saveYNM(id, false, true, false, function (success, response) {
                    $(".profile.secret-admirer").fadeOut(400);
                    $("#yesno").addClass("no");
                });
            });

            
        }
    };


    self.matches = function () {
        if ($(document).data("match"))
            $("h4.match").css({ "height": "18px" }).text($(document).data("match")[0].match);
        
    };

  self.showFlirtMessagesInCategory = function(index) {
      //click on li show category 
      $("div.flirts-container").addClass("hidden");
      $("div.flirts-category-expanded").removeClass("hidden");

      spark.api.client.callAPI(spark.api.client.urls.getAllTeases, null, null, function (success, response) {
          for (var i = 0; i < response.data.length ; i++) {
              var ul = $("ul.flirts-category");
              var recipientMemberId = $('.first').attr("data-memberid");
              if (response.data[index].Teases[i] != null) {
                  var teaseId = (response.data[index].Teases[i].ID);
                  var li = $("<li data-teaseId=\"" + teaseId + "\"  data-teaseCategoryId=\"" + response.data[index].Id + "\"  data-index=\"" + i + "\" data-recipientId=\"" + recipientMemberId + "\"/>");
                  li.text(response.data[index].Teases[i].Description);
                  li.appendTo(ul);
              }
          }

      });
    }

  self.showFlirtCategoryOptions = function () {
      $("div.flirts-container").removeClass("hidden");
      $("div.ui-content").parent().hide();
        spark.utils.common.hideFooter();

        spark.api.client.callAPI(spark.api.client.urls.getAllTeases, null, null, function (success, response) {
            for (var i = 0; i < response.data.length; i++) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                //var ul = $("ul.flirts-list");
                //var a = $("<a class='right-arrow' />");
                //var li = $("<li data-category=\"" + response.data[i].Description + "\" data-categoryId=\"" + response.data[i].Id + "\"  data-index=\"" + i + "\"/>");
                //var span = $("<span class='forward-arrow'></span>")

                //$(ul).append("<li data-category=\"" + response.data[i].Description + "\" data-categoryId=\"" + response.data[i].Id + "\"  data-index=\"" + i + "\" >" + response.data[i].Description + "<span class='right-arrow' data-category=\"" + response.data[i].Description + "\" data-categoryId=\"" + response.data[i].Id + "\"  data-index=\"" + i + "\"></span></li>");

                var ul = $("ul.flirts-list");
                var a = $("<a class='right-arrow' />");
                var li = $("<li data-category=\"" + response.data[i].Description + "\" data-categoryId=\"" + response.data[i].Id + "\"  data-index=\"" + i + "\"/>");
                var span = $("<span class='forward-arrow'></span>")

                $(ul).append("<li data-category=\"" + response.data[i].Description + "\" data-categoryId=\"" + response.data[i].Id + "\"  data-index=\"" + i + "\" >" + response.data[i].Description + "<span class='right-arrow' data-category=\"" + response.data[i].Description + "\" data-categoryId=\"" + response.data[i].Id + "\"  data-index=\"" + i + "\"></span></li>");

            }
        });
    }

    self.flirt = function (recipientMemberId, teaseId, teaseCategoryId, text) {
        spark.utils.common.showConfirm(text, function () {

            var postData = {
                recipientMemberId: recipientMemberId,
                teaseId: teaseId,
                teaseCategoryId: teaseCategoryId,
                body: ""
            };

            var url = spark.api.client.urls.sendTease;
            spark.api.client.callAPI(url, null, postData, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }
                else {
                    spark.utils.common.showMessageBar("Smile Sent!", true, function () {
                    });
                }
                setTimeout(function () {
                    window.location = "/profile/" + recipientMemberId + "";
                }, 3000);
            });
        });
    };

    
    self.favorite = function() {
        var profileId = $('.first').attr("data-memberid");
        $("#profile_page .icon-favorite").toggleClass('selected').removeClass('ui-btn-active');
        var params = {
            "hotListCategory": "favorite",
            "targetMemberId": profileId,
        }
        if ($("#profile_page .icon-favorite").hasClass('selected')) {
            $("#profile_page .icon-favorite").text('Unfavorite');

            spark.api.client.callAPI(spark.api.client.urls.addToHotList, params, null, function (success, response) {
                var fullProfile = [];
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }
            });

        } else {
            $("#profile_page .icon-favorite").text('Favorite');
            spark.api.client.callAPI(spark.api.client.urls.removeFromHotlist, params, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }
            });
        };
    };

    self.getYesNoMaybe = function() {
        //Get Secret Admirer Status from API
        var memberId = $('.first').attr("data-memberid");
        spark.api.client.callAPI(spark.api.client.urls.get_YNM, { addMemberId: memberId }, null, function(success, response) {
            var yesNoMaybe = [];
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            yesNoMaybe.push({ yesNoMaybe: response.data });
            $(document).data("yesNoMaybe", yesNoMaybe);
            var ynm = $(document).data("yesNoMaybe");

            //Assign img a class based on secret admirer status
            if (ynm[0].yesNoMaybe.Maybe === true) {
                $(".first span#yesno").removeClass("yes no").addClass("maybe");

               

            } else if (ynm[0].yesNoMaybe.MutualYes === true) {
                $(".first span#yesno").removeClass("no maybe").addClass("yesBoth");
             
            } else if (ynm[0].yesNoMaybe.No === true) {
                $(".first span#yesno").removeClass("yes maybe").addClass("no");
              
            } else if (ynm[0].yesNoMaybe.Yes === true) {
                $(".first span#yesno").removeClass("no maybe").addClass("yes");
              
            } else {
                $(".first span#yesno").removeClass("no yes maybe");
            }

            spark.views.profile.secretAdmirerGame();

        });

    };

    self.removeElement = function () {
        var profileId = $('.first').attr("data-memberid");
        if (!$("#profile_page .icon-favorite").hasClass('selected')) {
            var cachedData = $(document).data("cachedData");
            // If the cached data is from Favorites, then remove the correspond profile if user unfavorite it.
            // Note: this code is only work for single profile. If you change it to support swiping, this code here need to changed.
            if (cachedData != null) {
                if (cachedData.cachedPage == "favorites") {
                    $.each(cachedData.membersData, function (index, item) {
                        if (profileId == item.miniProfile.memberId) {
                            cachedData.membersData.splice(index, 1);
                            return false;
                        }
                    });
                    $(document).data("cachedData", cachedData);
                }
            }
        }
    };

    // Block or Report section

    function cacelOverLay(event) {
        $(".overlay").remove();
    }


    function tapBlockSelected(event) {
        $("div.block-popup").removeClass("disappear");
        $("div.profile-block-or-report").addClass("hide");
    }

    ////submit 
    function submitBlockedProfile(event) {
        $("div.block-popup").addClass("disappear");
        $("div.profile-block-or-report").removeClass("hide");

        if ($("p.block-one, p.block-two, p.block-three").hasClass("block")) {
            $("li.block-btn a.ui-btn.ui-btn-inline.block").addClass("selected");
            $("li.block-btn a.ui-btn.ui-btn-inline.block").text("Unblock");
        } else {
            $("li.block-btn a.ui-btn.ui-btn-inline.block").removeClass("selected");
            $("li.block-btn a.ui-btn.ui-btn-inline.block").text("Block");
        }
        blockUser();
    }

    $(":button:contains('Cancel')").click(function () {
        $(".overlay").remove();
    });

    $(document).on('tap', "li.block-btn a.ui-btn.ui-btn-inline.block", function (e) {
        e.preventDefault();
        $("div.block-popup").removeClass("disappear");
        $("div.profile-block-or-report").addClass("hide");
    });


     function blockUserByListName(listname) {
         var blockStatus = [];
        var memberId = $(".first").attr("data-memberid");
        var params = {
            hotListCategory: listname,
            targetMemberId: memberId,
        }

        spark.api.client.callAPI(spark.api.client.urls.addToHotList, params, null, function (success, response) {
            var fullProfile = [];
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

        });
        
    };

    function unblockUserByListName(listname) {

        var fullProfile = [];
        var memberId = $(".first").attr("data-memberid");
        var params = {
            hotListCategory: listname,
            targetMemberId: memberId,
        }
        spark.api.client.callAPI(spark.api.client.urls.removeFromHotlist, params, null, function (success, response) {
            var fullProfile = [];
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

        });

    };
    
    function blockUser() {
        if ($("p.block-one").hasClass("block")) {
            blockUserByListName("hide");
        } else {
           unblockUserByListName("hide");
        }

        if ($("p.block-two").hasClass("block")) {
            blockUserByListName("block");
        } else {
            unblockUserByListName("block");
        }
    };

   

    $(document).on('tap', "p.block-one, p.block-two, p.block-three", function (e) {
        e.preventDefault();
        $(this).toggleClass("block");
    });

    //cancel 
    //$(document).on('tap', "a.ui-btn.ui-btn-inline.cancel-btn", function (e) {
    //    e.preventDefault();
    //    $("div.block-popup").addClass("disappear");
    //    $("div.profile-block-or-report").removeClass("hide");
    //});

    function cancelBlock() {
        $("div.block-popup").addClass("disappear");
        $("div.profile-block-or-report").removeClass("hide");
    }


    self = $.extend(new spark.views.baseprofile(), self);
    //self.cancelBlock = cancelBlock;
    self.blockUser = blockUser;
    return self;
})();


spark.views.ReportAbuse = (function () {
    var self = {};
    var isInit = false;

    self.init = function () {
        if (isInit) {
            return;
        }
        //wire up click handlers
        $("ul.reason-list li p").on("tap", selectReason);
        $(".report-btn").on("tap", "a.report.enabled", postReportAbuse);
        $("textarea.report-concern").on("input", checkForComment);
        $(".report-btn a.cancel").on("tap", backToPrev);
        $("a.prevButton").on("tap", backToPrev);

    }

    function backToPrev() {
        history.back(-1);
    }

    function selectReason(event) {
        var alreadySelected = false;

        $(this).closest("li").hasClass('selected') ? alreadySelected = true : alreadySelected = false;
        $("ul.reason-list li").removeClass("selected");
        $(this).siblings("textarea").removeClass("enabled");
        $(".report-btn a.report").removeClass("enabled");

        if (alreadySelected == false) {
            $(this).closest("li").addClass("selected");
            $(this).siblings("textarea").addClass("enabled");
            if ($(this).siblings("textarea").length == 0) { $(".report-btn a.report").addClass("enabled"); }
        } else {
            $(".report-btn a.report").removeClass("enabled");
        }
    }

    function postReportAbuse(event) {
        var reasonID = $("ul.reason-list li.selected").data("attribute-option-id");
        var reasonValue = $("ul.reason-list li.selected").data("attribute-value");
        var userID = $("div.reported-user").attr("data-memberid");

        var comment ;

        if ($("ul.reason-list li.report-reason:nth-child(2)").hasClass("selected")) {
            comment = $("ul.reason-list li.report-reason:nth-child(2) > p").text();
        } else {
            comment = $("ul.reason-list li.selected textarea").val();
        }
       
        var params = {
            hotListCategory: "report",
            targetMemberId: userID,
            comment: comment
        }


        spark.api.client.callAPI(spark.api.client.urls.ReportAbuse, params, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            if (response.data == "Member has been reported already") {
                spark.utils.common.showGenericDialog("Member has been reported already.");
            } else {
                $(".report-abuse-content").addClass("hidden");
                $(".confirm-report-abuse").removeClass("hidden");
                $("body").scrollTop(0);
            }
        });
    }

    function checkForComment(event) {
        if ($(this).val().length > 0) {
            $(".report-btn a.report").addClass("enabled");
        } else {
            $(".report-btn a.report").removeClass("enabled");
        }

    }

    return self;
})();

var activeMemberId = 0;
function getPhotos(memberId) {
    spark.api.client.callAPI(spark.api.client.urls.photos, { targetMemberId: memberId }, null, function (success, response) {
        if (!success) {
            spark.utils.common.showError(response);
            return;
        }

        var photos = response.data;
        if (photos < 0) {

        } else {
            var photoList = [];
            for (var i = 0; i < photos.length; i++) {
                photoList.push({
                    imagePath: photos[i].fullPath,
                    caption: photos[i].caption,
                    thumbnail: photos[i].thumbPath
                });
            }

            var photoViewer = $("#photo_viewer");
            photoViewer.photoviewer("setBeforeHideListener", function () {
                $("div[data-role='content']").show();
            });

            photoViewer.photoviewer("setBeforeShowListener", function () {
                $("div[data-role='content']").hide();
            });

            photoViewer.photoviewer("show", photoList, 0);
        }
    });

}


function spaces(myList) {


    var mySplitedList = myList.toString();

    var newList = mySplitedList.replace(/,/g, ", ");

    return newList;

}

function getFullProfile(memberId) {
    $("span#yesno").removeClass("no yes maybe");
    spark.api.client.callAPI(spark.api.client.urls.fullProfile, { targetMemberId: memberId }, null, function (success, response) {
        var fullProfile = [];
        if (!success) {
            spark.utils.common.showError(response);
            return;
        }

        fullProfile.push({ fullProfile: response.data });
        $(document).data("fullProfile", fullProfile);
        if (fullProfile[0].fullProfile.isViewersFavorite == false) {
            $('.slide[member-id=' + memberId + '] .favorite a').removeClass("selected").text("Favorite");
        } else {
            $('.slide[member-id=' + memberId + '] .favorite a').addClass("selected").text("Unfavorite");
        }

        var p = fullProfile[0].fullProfile;
        for (var property in p) {
            if (p.hasOwnProperty(property)) {

                if (p[property] === null || p[property] < Array.length || p[property] === "") {
                    p[property] = "Not answered";
                }
            }
        }

        if (p.primaryPhoto === "Not answered" && p.gender === "Male") {
            profileImg = "/images/hd/img_silhouette_man@2x.png";
        } else if (p.primaryPhoto === "Not answered" && p.gender === "Female") {
            profileImg = "/images/hd/img_silhouette_woman@2x.png";
        } else {
            var profileImg = p.primaryPhoto.fullPath;
        }

        //Profile Details
        if (p.gender === "Female") {
            var gender = "woman";
        } else if (p.gender === "Male") {
            var gender = "man";
        }

        var status = p.maritalStatus;
        var seeking = p.seekingGender;
        var age = p.age;
        var location = p.location;
        var isOnline = p.isOnline;
        var lastLoggedIn = p.lastLoggedIn;
        var photoCount = p.photoCount;

        //In My Own Words
        var aboutMe = spark.messageconversion.htmlUnescape(p.aboutMe);
        var lookingFor = spaces(p.lookingFor);
        var perfectFirstDateEssay = p.perfectFirstDateEssay;
        var idealRelationshipEssay = p.idealRelationshipEssay;
        var learnFromThePastEssay = p.learnFromThePastEssay;
        var personalityTraits = spaces(p.personalityTraits);
        var likeGoingOutTo = spaces(p.likeGoingOutTo);
        var inMyFreeTimeILikeTo = spaces(p.inMyFreeTimeILikeTo);
        var physicalActivity = spaces(p.physicalActivity);
        var pets = spaces(p.pets);
        var favoriteMusic = spaces(p.favoriteMusic);
        var favoriteFoods = spaces(p.favoriteFoods);
        var likeToRead = spaces(p.likeToRead);

        //My Details
        var myHeightIn = p.heightCm * .393701;
        var myHeightFt = parseInt(myHeightIn / 12);
        var myHeightIns = parseInt(myHeightIn - (myHeightFt * 12));
        var myHeight = myHeightFt + "\'" + myHeightIns + '\"' + "(" + p.heightCm + " cm)";
        var myWeight = parseInt(p.weightGrams * .0022) + " lbs (" + parseInt(p.weightGrams * .001) + " kg)";
        var hair = p.hairColor;
        var eyes = p.eyeColor;
        var bodyStyle = p.bodyType;
        var relocate = p.relocation;
        var myChildren = p.children;
        var custody = p.custody;
        var wantKids = p.planOnChildren;
        var kosher = p.keepKosher;
        var synagogue = p.synagogueAttendance;
        var drink = p.drinkingHabits;
        var smoke = p.smokingHabits;
        var zodiac = p.zodiac;
        var activityLevel = p.activityLevel;
        var grewUpIn = p.grewUpIn;
        var ethnicity = p.ethnicity;
        var iSpeak = p.languagesSpoken;
        var studied = p.studiedOrInterestedIn;
        var education = p.educationLevel;
        var occupation = p.occupation;
        var occupationDesc = p.occupationDescription;
        var income = p.incomeLevel;
        var politics = p.politicalOrientation;
        var religion = p.religion;

        //My Ideal Match
        var lookFor = spaces(p.lookingFor);
        if (isNaN(p.matchMinAge) || isNaN(p.matchMaxAge)) {
            var ageRange = p.matchMinAge;
        } else {
            var ageRange = p.matchMinAge + " to " + p.matchMaxAge;
        }
        var relationshipStatus = p.matchMaritalStatus;
        var religiousBackground = spaces(p.religion);
        var edLevel = spaces(p.matchEducationLevel);
        var drinkingHabits = spaces(p.matchDrinkingHabits);
        var smokingHabits = spaces(p.matchSmokingHabits);

        //Profile Details
        $('.slide[member-id=' + memberId + '] .photos').attr("member-id", memberId);
        $('.slide[member-id=' + memberId + '] .photos img').attr("src", profileImg);

        getYesNoMaybe(memberId);
        //shorten username to 10

        $(".slide[member-id=" + memberId + "] .photos .photo-count b").text(photoCount);
        if (isNaN(photoCount)) {
            $('.slide[member-id=' + memberId + '] .photos .photo-count b').text("0");
        }

        $('.slide[member-id=' + memberId + '] .status').empty().text(status + " " + gender);
        if (status === "Not answered") {
            $('.slide[member-id=' + memberId + '] .status').empty().text(gender.substr(0, 1).toUpperCase() + gender.substr(1));
        }
        if (seeking === "Female") {
            seeking = "women";
            $('.slide[member-id=' + memberId + '] .seeking').empty().text("Seeking " + seeking);
        } else if (seeking === "Male") {
            seeking = "men";
            $('.slide[member-id=' + memberId + '] .seeking').empty().text("Seeking " + seeking);
        }
        $('.slide[member-id=' + memberId + '] .age').empty().text(age + " years old");
        $('.slide[member-id=' + memberId + '] .location').empty().text(location);
        var date = new Date(lastLoggedIn);
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var year = date.getFullYear();
        var newdate = month + "/" + day + "/" + year;
        

        $(".slide[member-id=" + memberId + "] .onlineStatus a.btn-mol").show();
        $(".slide[member-id=" + memberId + "] .onlineStatus .last-log").remove();
        if (spark.utils.common.isSub()) {
            $(".slide[member-id=" + memberId + "] a.chat-btn").attr("href", "#");
        } else {
            $(".slide[member-id=" + memberId + "] a.chat-btn").attr("href", "/subscription/subscribe/4167");
        }
        if (isOnline === "Not answered") {
            $(".slide[member-id=" + memberId + "] .onlineStatus a.btn-mol").hide();
            $(".slide[member-id=" + memberId + "] .onlineStatus").append("<p class='last-log' data-value='@Model.Profile.LastLoggedIn'>Last Login:<br/> " + newdate + "</p>");
        }

        //In My Own Words
        $('.slide[member-id=' + memberId + '] .aboutMe').empty().text(aboutMe);
        $('.slide[member-id=' + memberId + '] .lookingFor').empty().text(lookingFor);
        $('.slide[member-id=' + memberId + '] .perfectFirstDate').empty().text(perfectFirstDateEssay);
        $('.slide[member-id=' + memberId + '] .idealRelationship').empty().text(idealRelationshipEssay);
        $('.slide[member-id=' + memberId + '] .pastRelationships').empty().text(learnFromThePastEssay);
        $('.slide[member-id=' + memberId + '] .personalityTraits').empty().text(personalityTraits);
        $('.slide[member-id=' + memberId + '] .likeGoingOutTo').empty().text(likeGoingOutTo);
        $('.slide[member-id=' + memberId + '] .inMyFreeTime').empty().text(inMyFreeTimeILikeTo);
        $('.slide[member-id=' + memberId + '] .location').empty().text(location);
        $('.slide[member-id=' + memberId + '] .physicalActivity').empty().text(physicalActivity);
        $('.slide[member-id=' + memberId + '] .pets').empty().text(pets);
        $('.slide[member-id=' + memberId + '] .favMusic').empty().text(favoriteMusic);
        $('.slide[member-id=' + memberId + '] .favFood').empty().text(favoriteFoods);
        $('.slide[member-id=' + memberId + '] .likeToRead').empty().text(likeToRead);

        //My Details
        $('.slide[member-id=' + memberId + '] .myHeight').empty().text(myHeight);
        if (isNaN(p.heightCm)) {
            $('.slide[member-id=' + memberId + '] .myHeight').empty().text("Not answered");
        }
        $('.slide[member-id=' + memberId + '] .myWeight').empty().text(myWeight);
        if (isNaN(p.weightGrams)) {
            $('.slide[member-id=' + memberId + '] .myWeight').empty().text("Not answered");
        }
        $('.slide[member-id=' + memberId + '] .hair').empty().text(hair);
        $('.slide[member-id=' + memberId + '] .eyes').empty().text(eyes);
        $('.slide[member-id=' + memberId + '] .bodyStyle').empty().text(bodyStyle);
        $('.slide[member-id=' + memberId + '] .relocate').empty().text(relocate);
        $('.slide[member-id=' + memberId + '] .myChildren').empty().text(myChildren);
        $('.slide[member-id=' + memberId + '] .custody').empty().text(custody);
        $('.slide[member-id=' + memberId + '] .wantKids').empty().text(wantKids);
        $('.slide[member-id=' + memberId + '] .kosher').text(kosher);
        $('.slide[member-id=' + memberId + '] .synagogue').empty().text(synagogue);
        $('.slide[member-id=' + memberId + '] .drink').empty().text(drink);
        $('.slide[member-id=' + memberId + '] .zodiac').empty().text(zodiac);
        $('.slide[member-id=' + memberId + '] .activityLevel').empty().text(activityLevel);
        $('.slide[member-id=' + memberId + '] .grewUpIn').empty().text(grewUpIn);
        $('.slide[member-id=' + memberId + '] .ethnicity').empty().text(ethnicity);
        $('.slide[member-id=' + memberId + '] .iSpeak').empty().text(iSpeak);
        $('.slide[member-id=' + memberId + '] .studied').empty().text(studied);
        $('.slide[member-id=' + memberId + '] .education').empty().text(education);
        $('.slide[member-id=' + memberId + '] .occupation').empty().text(occupation);
        $('.slide[member-id=' + memberId + '] .occupationDesc').empty().text(occupationDesc);
        $('.slide[member-id=' + memberId + '] .income').empty().text(income);
        $('.slide[member-id=' + memberId + '] .politics').empty().text(politics);
        $('.slide[member-id=' + memberId + '] .religion').text(religion);
        $('.slide[member-id=' + memberId + '] .synagogue').empty().text(synagogue);
        $('.slide[member-id=' + memberId + '] .drink').empty().text(drink);
        $('.slide[member-id=' + memberId + '] .smoke').empty().text(smoke);
        $('.slide[member-id=' + memberId + '] .zodiac').empty().text(zodiac);
        $('.slide[member-id=' + memberId + '] .activityLevel').empty().text(activityLevel);

        //My Ideal Match
        $('.slide[member-id=' + memberId + '] .lookFor ').empty().text(lookFor);
        $('.slide[member-id=' + memberId + '] .ageRange').empty().text(ageRange);
        $('.slide[member-id=' + memberId + '] .relationshipStatus ').empty().text(relationshipStatus);
        $('.slide[member-id=' + memberId + '] .religiousBackground ').empty().text(religiousBackground);
        $('.slide[member-id=' + memberId + '] .edLevel ').empty().text(edLevel);
        $('.slide[member-id=' + memberId + '] .drinkingHabits ').empty().text(drinkingHabits);
        $('.slide[member-id=' + memberId + '] .smokingHabits ').empty().text(smokingHabits);

    });
}
function parseDate(dateString) {
    var d = new Date(dateString);

    var year = d.getUTCFullYear();
    var month = d.getUTCMonth();
    var date = d.getUTCDate();
    var monthString = convertMonToString(month);

    var returnDate = monthString + " " + date + ", " + year + " ";

    return returnDate;
}

function convertMonToString(month) {
    var monthString;
    //TODO:REFACTOR!
    if (month === "0") {
        monthString = "Jan";
    }
    else if (month === "1") {
        monthString = "Feb";
    }
    else if (month === "2") {
        monthString = "Mar";
    }
    else if (month === "3") {
        monthString = "Apr";
    }
    else if (month === "4") {
        monthString = "May";
    }
    else if (month === "5") {
        monthString = "Jun";
    }
    else if (month === "6") {
        monthString = "Jul";
    }
    else if (month === "7") {
        monthString = "Aug";
    }
    else if (month === "8") {
        monthString = "Sep";
    }
    else if (month === "9") {
        monthString = "Oct";
    }
    else if (month === "10") {
        monthString = "Nov";
    }
    else if (month === "11") {
        monthString = "Dec";
    }
    else {
        monthString = "Undefined";
    }

    return monthString;
}




// Bind event to Chat IM Button
$(document).on('tap', '#profile_page .onlineStatus a', function (e) {

});

// Bind event to Flirt navigation button.
$(document).on('tap', '#profile_page div.flirts-container > div', function (e) {
    e.preventDefault();
    var id = $("div.page-header > h2").attr("data-memberid");
    window.location = "/profile/" + id + "";
    //$("div.ui-content").parent().show();
    //spark.utils.common.showFooter();
    //$("div.flirts-container").addClass("hidden");
    
    //$("div.ui-content").parent().fadeIn(350);
});

/* Navigate back to the list of flirt categories*/
$(document).on('tap', '#profile_page div.flirts-category-expanded > div', function (e) {
    e.preventDefault();
    $("div.flirts-container").removeClass("hidden");
    $("div.flirts-category-expanded").addClass("hidden");
    $("ul.flirts-category li").remove();
});

/*send flirt*/
$(document).on('tap', '#profile_page ul.flirts-category li', function (e) {
    var recipientMemberId = $(this).attr("data-recipientId");
    var teaseId = $(this).attr("data-teaseId");
    var teaseCategoryId = $(this).attr("data-teaseCategoryId");
    var text = $(this).text();
   
    if (text.length > 130) {
        var newHeight = 150 + $(this).height();
        $("div#generic_confirm_dialog").css({
            "min-height": newHeight,
        })
    } else {
        $("div#generic_confirm_dialog").css({
            "min-height": "150px",
        });
    }
    spark.views.profile.flirt(recipientMemberId, teaseId, teaseCategoryId, text);
});

/*Select flirt category */
$(document).on('tap', '#profile_page ul.flirts-list li', function (e) {
    e.preventDefault();
    var index = $(this).attr("data-index");
    spark.views.profile.showFlirtMessagesInCategory(index);
});

/* Select Filrt(smile) Button  --> navigates to the list of flirts categories*/
$(document).on('tap', '#profile_page .flirt a', function (e) {
    //spark.views.profile.flirt();
    spark.views.profile.showFlirtCategoryOptions();
});

// Profile Favoriting functionality
$(document).on('tap', '#profile_page .icon-favorite', function (e) {
    e.preventDefault();
    spark.views.profile.favorite();
});

//photo slideshow overlay feature
$(document).on('tap', '#profile_page .profile-detail-container div.photos', function (e) {
    var memberId = $(".first").attr("data-memberid");
    if ($(this).find("span.photo-count b").text() === "0") {
        //do nothing
    } else {
        $("footer").hide();
        getPhotos(memberId);
    }

});

function getSelectedIndex(memberId) {
    var list = $(document).data("members");
    var length = 0;
    if (list) {
        length = list.length;
    }
    for (var i = 0; i < length; i++) {
        if (memberId === list[i].memberId) {

            return i;
        }
    }

    return 0;
}

function italicize() {

    $(".profile-content p").each(function () {
        $(this).css({ "font-style": "normal" });
    });

    if ($(".profile-content div p:contains('Not answered')").length > 0) {
        $(".profile-content > div > p:contains('Not answered')").each(function () {
            $(this).css({ "font-style": "italic" });
        });

    }
}

function getUsername(memberId) {
    spark.api.client.callAPI(spark.api.client.urls.fullProfile, { targetMemberId: memberId }, null, function (success, response) {
        var fullProfile = [];
        if (!success) {
            spark.utils.common.showError(response);
            return;
        }

        fullProfile.push({ fullProfile: response.data });
        var username = fullProfile[0].fullProfile.username;
        if (username.length > 15) {
            var shortName = username.slice(0, 15) + "...";
            $('.page-header h2').text(shortName);
        } else {
            $('.page-header h2').text(username);
        }
    });
}

function appendSwiperSlides(memberId, index) {
    var swiper = $(".swiper");
    var wrapper = swiper.find(".swiper-wrapper");

        var id = $(document).data("memberId");
        if (!id) {
            var url = $("#profile_page").attr("data-url");
            var memid = url.replace("/profile/", "");
        } else {
            memid = id[0].memberId;
        }

        $(".first").wrapInner("<div class='slide' member-id='" + memid + "'/>");
        getFullProfile(memid);
        getUsername(memid);
        italicize();
        $("#profile_page .prevButton").css({"visibility":"visible"}).click(function() {
            history.back(-1);
        });
        $("#profile_page .nextButton").css({"visibility":"hidden"});

    //}
    return;
}

function profileDetails(list, index) {
    var options = {};
    if (list[index].match) {
        $(".swiper-slide-active").find("h4.match").text(list[index].match);
    }

    $(".swiper-slide .slide").attr("member-id", list[index].memberId);
    $(".swiper-slide-active a[href='#in_my_own_words']").click();
    getFullProfile(list[index].memberId);
    getUsername(list[index].memberId);
    spark.views.profile.addTabEvents();
    italicize();
}

function showProfile(memberId) {
    var header = $(".page-header");
    var prevButton = header.find(".prevButton");
    var nextButton = header.find(".nextButton");
    var list = $(document).data("members");
    var index = getSelectedIndex(memberId);

    appendSwiperSlides(memberId, index);

  
    return;
}

$(document).on('tap', '#profile_page #btn_send', function (e) {
    //var memberId = ($(this).closest(".swiper-slide").attr("member-id"));
    var memberId = $(this).attr("data-memberid");
    var userName = $(".page-header h2").text();
    userName = userName.replace("SmilesMessages", "");

    showComposeMail(memberId, userName);
    
});

function showComposeMail(memberId, userName) {
    $("div.ui-content").parent().hide();
    spark.utils.common.hideFooter();
    var panel = $("#compose_mail");
    panel.removeClass("hidden").fadeIn(350);
    panel.find("#btn_cancel_mail").off().click(function () {
        panel.hide();
        spark.utils.common.showFooter();
        $("div.ui-content").parent().fadeIn(350);
    });

    panel.find("h4").text("To: " +userName);
    panel.find("#btn_send_mail").off().click(function () {
        var body = $.trim($("#body").val());
        var text = body.replace(/<\/?[^>]+>/ig, " ");

        body = text;

        var subject;
        if (body.length < 35) {
            subject = body.substr(0, 35);
        } else {
            subject = body;
        }
        
        var recipientMemberId = $("div.page-header > h2").attr("data-memberid");
        var originalMessageRepliedToId = "";

        spark.views.mail.sendMessage(recipientMemberId, subject, body, originalMessageRepliedToId, function (data) {
            $("#body").val("");
            $("#subject").val("");

            panel.hide();
            spark.utils.common.showFooter();
            $("div.ui-content").parent().fadeIn(350);

            spark.utils.common.showMessageBar("Your message has been sent", true);
        });
    });
}



$(document).on("pagebeforeshow", "#profile_page", function (e, ui) {
    spark.views.profile.getYesNoMaybe();
    if (ui.prevPage.attr('id') == 'matches_page') {
        spark.views.profile.matches();
    } else {
        $(document).removeData("match");
        $("h4.match").css({ "height": "0" });
    }
});
$(document).on("pageshow", "#report_abuse", function (e, ui) {
    spark.views.ReportAbuse.init();
});


$(document).on("pageshow", "#profile_page", function (e, ui) {
    spark.views.profile.init();
    italicize();
    
    setTimeout(function () {
        googletag.pubads().refresh([gpt_profile_top]);
    }, 750);
    spark.utils.common.hideLoading();
}); //end ready();

$(document).on("pagebeforehide", "#profile_page", function (e, ui) {
    spark.views.profile.removeElement();
    initCheck = true;
});