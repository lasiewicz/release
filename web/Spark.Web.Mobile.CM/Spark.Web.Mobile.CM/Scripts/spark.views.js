spark.views.activity = (function () {
    var self = {};

    self.init = function () {
    };

    self.showSecretAdmirer = function () {
        $("#secret_admirer").secretadmirer("show");
    };

    return self;
})();

$(document).on("pageshow", "#activity_page", function () {
    $("#btn_secret_admirer").click(function () {
        spark.views.activity.showSecretAdmirer();
    });
});

spark.views.mol = (function () {
    var self = {};

    self.updateCount = function () {
        spark.api.client.callAPI(spark.api.client.urls.members_online_count, null, null,
            function (success, response) {
                if (!success)
                    return;

                var id = $.mobile.activePage.attr("id");
                var data = response.data;
                var btn = $("#btn_mol_" + id);
                
                if (btn.length === 0)
                    return;

                btn.text(data.count + " Online");
            });
    };

    self.storeMembers = function () {
        var members = [];
        $("#mol_list").children("li").each(function () {
            var li = $(this);
            members.push({
                memberId: li.attr("data-memberid"),
                thumbnail: li.find("a img").attr("src"),
                userName: li.find("a span").text()
            });
        });

        // Cache data so that it can be retrieve from the Profile page.
        $(document).data("members", members);
    }

    return self;
})();

$(document).on("pagebeforehide", "#membersonline_page", function (e, ui) {
    spark.views.mol.storeMembers();
});

spark.views.baseprofile = function () {
    var base = {};

    function addTabEvents() {
        var tabs = $("#profile_tabs");
        tabs.children("li").each(function () {
            var li = $(this);

            li.find("a").click(function (e) {
                var a = $(this);

                changeTab(a.attr("href"));
                e.preventDefault();
            });
        });
    }

    function changeTab(section) {
        var tabContents = $("[data-role='tabcontent']");
        var tabs = $("#profile_tabs");
        var selectedIndex = 0;

        tabs.children("li").each(function (index) {
            var li = $(this);
            var a = li.find("a");

            a.removeClass("active")
                .removeClass("border-left")
                .removeClass("border-right");
            if (a.attr("href") === section) {
                a.addClass("active");
                selectedIndex = index;
            }

            var parent = li.parent();
            switch (selectedIndex) {
                case 0:
                    parent.find("li:eq(1) a").addClass("border-left");
                    parent.find("li:eq(2) a").addClass("border-left");
                    break;
                case 1:
                    parent.find("li:eq(0) a").addClass("border-right");
                    parent.find("li:eq(2) a").addClass("border-left");
                    break;
                default:
                    parent.find("li:eq(0) a").addClass("border-right");
                    parent.find("li:eq(1) a").removeClass("border-left").addClass("border-right");
                    break;
            }
        });

        tabContents.each(function () {
            var tab = $(this);
            if (tab.attr("id") !== section.substring(1)) {
                tab.hide();
            } else {
                tab.show();
            }

        });
    }

    function getPhotos(memberId, callback) {

    }

    // Public method(s).
    base.addTabEvents = addTabEvents;
    base.getPhotos = getPhotos;

    return base;
};

spark.views.profile = (function () {
    var self = {};

    self.init = function () {
        spark.views.profile.addTabEvents();

        $("a[href='#in_my_own_words']").click();
    };

    self = $.extend(new spark.views.baseprofile(), self);

    return self;
})();

$(document).on("pageshow", "#profile_page", function () {
    spark.views.profile.init();

    console.log($(document).data("members"));


    //shorten profile username if longer than 15 characters
    if ($('.page-header h2').text().length >= 15) {
        var shortName = $('.page-header h2').text().slice(0, 15) + "...";
        $('.page-header h2').text(shortName);
    }
});



spark.views.myprofile = (function () {
    var self = {};
    var editedFields = [];

    var attributeOptionsMap = {
        "personality": "personalityTrait",
        "i_enjoy": "leisureActivity",
        "i_like_to_go_to": "EntertainmentLocation",
        "favorite_physical_activities": "physicalActivity",
        "own_these_pets": "pets",
        "favorite_foods": "cuisine",
        "favorite_music": "music",
        "like_to_read": "reading",
        "marital_status": "maritalStatus",
        "hair": "hairColor",
        "eyes": "eyeColor",
        "body_style": "bodyType",
        "looking_for": "relationshipMask",
        "relationship_status": "maritalStatus",
        "religious_background": "jDateReligion",
        "education_level": "educationLevel",
        "drinking_habits": "drinkingHabits",
        "smoking_habits": "smokingHabits",
        "relocation": "relocateFlag",
        "children": "childrenCount",
        "custody_situation": "custody",
        //"planning_children": "planOnChildren",
        "keep_kosher": "keepKosher",
        "go_to_synagogue": "synagogueAttendance",
        "i_smoke": "smokingHabits",
        "i_drink": "drinkingHabits",
        "zodiac_sign": "zodiac",
        "activity_level": "activityLevel",
        "ethnicity": "jDateEthnicity",
        "languages": "languageMask",
        "education": "educationLevel",
        "occupation": "industryType",
        "annual_income": "incomeLevel",
        "political_orientation": "politicalOrientation",
        "my_religion": "jDateReligion"
    };

    var jsonMapping = {
        "marital_status": "maritalStatus",
        "gender_seeking": "seekingGender",
        "about_me": "aboutMe",
        "perfect_first_date": "perfectFirstDateEssay",
        "ideal_relationship": "idealRelationshipEssay",
        "past_relationships": "learnFromThePastEssay",
        "personality": "personalityTraits",
        "i_enjoy": "inMyFreeTimeILikeTo",
        "i_like_to_go_to": "likeGoingOutTo",
        "favorite_physical_activities": "physicalActivity",
        "own_these_pets": "pets",
        "favorite_foods": "favoriteFoods",
        "favorite_music": "favoriteMusic",
        "like_to_read": "likeToRead",
        "height": "heightCm",
        "weight": "weightGrams",
        "hair": "hairColor",
        "eyes": "eyeColor",
        "body_style": "bodyType",
        "relocation": "relocation",
        "children": "children",
        "custody_situation": "custody",
        "planning_children": "planOnChildren",
        "keep_kosher": "keepKosher",
        "go_to_synagogue": "synagogueAttendance",
        "i_smoke": "smokingHabits",
        "i_drink": "drinkingHabits",
        "activity_level": "activityLevel",
        "zodiac_sign": "zodiac",
        "grew_up_in": "grewUpIn",
        "studied": "studiedOrInterestedIn",
        "ethnicity": "jDateEthnicity",
        "languages": "languagesSpoken",
        "education": "educationLevel",
        "occupation": "occupation",
        "occupation_description": "occupationDescription",
        "annual_income": "incomeLevel",
        "political_orientation": "politicalOrientation",
        "my_religion": "jDateReligion",
        "looking_for": "lookingFor",
        "min_age_range": "matchMinAge",
        "max_age_range": "matchMaxAge",
        "relationship_status": "matchMaritalStatus",
        "religious_background": "matchJDateReligion",
        "education_level": "matchEducationLevel",
        "drinking_habits": "matchDrinkingHabits",
        "smoking_habits": "matchSmokingHabits",
        "user_name": "username"
    }

    function addEditField(e, hasChanged) {
        var id = e.attr("id");

        if ($.inArray(id, editedFields) === -1 && hasChanged) {
            editedFields.push(id);
        } else if (!hasChanged) {
            editedFields = $.grep(editedFields, function (value) {
                return value != id;
            });
        }
    }

    function getAttributeOptionName(key) {
        if (key in attributeOptionsMap)
            return attributeOptionsMap[key];

        return "";
    }

    function getMapping(key) {
        return jsonMapping[key];
    }

    function getMultiselectValues(key) {
        var o = $("#" + key + "_text");
        var arr = (o.attr("data-value")) ? o.attr("data-value").split(",") : [];
        var values = [];

        $.each(arr, function (index, value) {
            values.push($.trim(value));
        })

        return values;
    }

    function hideLoading() {
        $.mobile.loading("hide");
    }

    function processControls(form) {
        // Proces textarea field(s).
        var textbox = form.find("input[type='text']");
        textbox.keyup(function () {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());

            addEditField($(this), hasChanged);
        });

        textbox.on("paste", function () {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());

            addEditField($(this), hasChanged);
        })

        // Proces textarea field(s).
        var textarea = form.find("textarea");
        textarea.keyup(function () {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());

            addEditField($(this), hasChanged);
        });

        textarea.on("paste", function () {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());

            addEditField($(this), hasChanged);
        })

        // Process multi-select widget(s).
        var widgets = form.find("div[data-type='multiselect']");
        widgets.each(function () {
            var o = $(this);
            var id = o.attr("id");

            var values = getMultiselectValues(id);
            o.multiselect("createOptions", getAttributeOptionName(id), values);
            o.multiselect("setOnChangeListener", function (hasChanged) {
                addEditField(o, hasChanged);
            });
        });

        // Process custom dropdown(s).
        var dropdowns = form.find("div.dd-custom");
        dropdowns.each(function () {
            var o = $(this);
            var id = o.attr("id");
            var select = o.find("select");

            // Add onChange listener to dropdown.
            select.change(function () {
                $(this).find("option:selected").each(function () {
                    var hasChanged = ($.trim($("#" + id + "_text").attr("data-value")) != $.trim($(this).text()));

                    addEditField(o, hasChanged);
                });
            });

            if (select.find("option").length !== 0) {
                setSelected(select);

                return;
            }

            var attrName = getAttributeOptionName(id);
            if (!attrName || attrName.length === 0) {
                setSelected(o.find("select"));

                return;
            }

            // Get options for dropdown.
            spark.api.client.callAPI(spark.api.client.urls.attributeOptions, { attribute: attrName }, null, function (success, response) {
                if (!success)
                    return;

                $.each(response.data, function (key, value) {
                    var option = $("<option />");

                    option.attr("value", value["Value"]);
                    option.text(value["Description"]);
                    option.appendTo(select);
                });

                setSelected(select);
            });

        });
    }

    function saveProfile() {
        // Check if there are fields to save. If none, just return.
        if (editedFields.length === 0)
            return;

        var attributes = {};
        var attributesMulti = {};

        $.each(editedFields, function (index, item) {
            var e = $("#" + item);
            var type = e.attr("data-type");
            var values = [];

            if (type === "multiselect") {
                values = e.multiselect("getValues");
                for (var i = 0; i < values.length; i++) {
                    values[i] = +values[i];
                }

                attributesMulti[getMapping(item)] = values;
            } else if (e.hasClass("dd-custom") && e.attr("id") !== "body_style") {
                var value = Number(e.find("select").val());
                if (e.attr("id") == "gender_seeking")
                {
                    if (value === 9 || value === 10) {
                        value = "Female";
                    } else {
                        value = "Male";
                    }
                }

                attributes[getMapping(item)] = value;
            } else if (e.attr("id") === "body_style") {
                values.push(Number(e.find("select").val()));
                attributesMulti[getMapping(item)] = values;
            } else {
                attributes[getMapping(item)] = e.val();
            }
        });

        var params = {
            AttributesJson: JSON.stringify(attributes),
            AttributesMultiValueJson: JSON.stringify(attributesMulti)
        };

        spark.api.client.callAPI(spark.api.client.urls.updateProfile, null, params, function (success, response) {
            showLoading("Saving...");

            if (!success) {
                hideLoading();

                // TODO: Show generic popup (need creative input)
                alert("Unable to save data!");
                return;
            }

            updateDetail();
            hideLoading();
            showDetail();
        });
    }

    function setSelected(target) {
        var id = target.parent().attr("id");
        var temp = id;
        if (id === "min_age_range" || id === "max_age_range") {
            temp = "age_range";
        }

        var o = $("#" + temp + "_text");
        var value = o.attr("data-value");
        if (!value || $.trim(value.length) === 0)
            return;

        if (id === "min_age_range")
            value = value.split(",")[0];
        if (id === "max_age_range")
            value = value.split(",")[1];
        if (id === "gender_seeking")
        {
            var temp = value.split(",");
            var gender = temp[0].toLowerCase();

            if (gender === "male") {
                value = 9;

                if (temp[1].toLowerCase() === "male")
                    value = 5;
            } else if (gender === "female") {
                value = 6;

                if (temp[1].toLowerCase() === "female")
                    value = 10;
            }
        }

        var option = target.find("option").filter(function () {
            var found = ($(this).text() == "" + value);
            if (!found) {
                found = ($(this).val() == value);
            }

            return found;
        });

        if (option && option.length !== 0)
            option.prop("selected", true);
    }

    function showEdit(name) {
        showForm(name);

        var target = $("div.editable");
        var temp = $("div.readonly");

        temp.animate({
            left: -(temp.width()),
            position: "absolute"
        }, 250, "linear", function () {
            temp.css({
                position: "relative"
            }).addClass("hidden");
        });

        target.removeClass("hidden").css({
            left: target.width(),
            position: "absolute"
        }).animate({
            left: 0
        }, 250, "linear", function () {
            var height = (target.height() > $(window).height()) ? target.height() : $(window).height();
            target.css({
                //height: height,
                //background: "#EAEDF1",
                position: "relative"
            });

            window.scrollTo(0, 0);
        });
    }

    function showForm(name) {
        var container = $("fieldset.profile-edit-form").parent();

        container.css({
            minHeight: ($(document).height() - 41)
        });

        $("fieldset.profile-edit-form").each(function () {
            var item = $(this);
            if (item.attr("id") !== name) {
                item.addClass("hidden");
            } else {
                processControls(item);
                item.removeClass("hidden");
            }
        });
    }

    function showDetail() {
        var target = $("div.readonly");
        var temp = $("div.editable");

        temp.animate({
            left: temp.width(),
            position: "absolute"
        }, 250, "linear", function () {
            temp.css({
                position: "relative"
            }).addClass("hidden");
        });

        target.removeClass("hidden").css({
            left: -(target.width()),
            position: "absolute"
        }).animate({
            left: 0
        }, 250, "linear", function () {
            target.css({
                position: "relative"
            });

            editedFields = [];
            window.scrollTo(0, 0);
        });
    }

    function showLoading(msg, options) {
        var defaultOptions = {
            text: msg,
            textVisible: true
        };

        $.extend(defaultOptions, options);
        $.mobile.loading("show", defaultOptions);
    }

    function updateDetail() {
        $.each(editedFields, function (index, item) {
            var e = $("#" + item);
            var o = $("#" + item + "_text");
            var type = e.attr("data-type");

            if (type === "multiselect") {
                var values = e.multiselect("getTextValues");

                e.multiselect("setValues", values);
                o.attr("data-value", values.join(","));
                o.text(values.join(", "));
            } else if (item === "min_age_range" || item === "max_age_range") {
                var temp = [];
                temp.push($("#min_age_range").find("select").val());
                temp.push($("#max_age_range").find("select").val());

                o = $("#age_range_text");
                o.attr("data-value", temp.join(","));
                o.text(temp[0] + " to " + temp[1]);
            } else if (e.hasClass("dd-custom")) {
                o.attr("data-value", e.find("select").val());
                o.text(e.find("select option:selected").text());
            } else {
                o.text(e.val());
            }
        });
    }

    self.init = function () {
        spark.views.myprofile.addTabEvents();

        $("a[href='#in_my_own_words']").click();

        $(".profile-edit-icon").bind("click", function (e) {
            showEdit($(this).attr("data-form"));
        });

        $("#btn_edit_cancel").bind("click", showDetail);
        $("#btn_edit_save").bind("click", saveProfile);
    };

    self.showPhotoViewer = function () {
        $("#photo_viewer").photoviewer("show");
    };

    self = $.extend(new spark.views.baseprofile(), self);

    return self;
})();

$(document).on("pageshow", "#myprofile_page", function () {
    spark.views.myprofile.init();
});