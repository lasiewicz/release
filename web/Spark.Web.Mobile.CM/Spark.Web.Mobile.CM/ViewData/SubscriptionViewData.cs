﻿using System;
using Spark.Web.Mobile.CM.Controllers;
using Spark.Common.RestConsumer.V2.Models.Mingle;
using Spark.Common.RestConsumer.V2.Models.Mingle.Subscription;
using Spark.Common.RestConsumer.V2.Models.Mingle.Search;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;
using Spark.Common.RestConsumer.V2.Models.Mingle.GAM;
using System.Collections.Generic;
using System.Text.RegularExpressions;


namespace Spark.Web.Mobile.CM.ViewData
{
    public class SubscriptionViewData : BaseViewData
    {
        public PaymentUiPostData PaymentData { get; set; }
        public List<MiniProfile> Members { get; set; }
        public TransactionInfo TransactionData { get; set; }
        public SubscriptionPlanStatus SubscriptionPlanStatusData { get; set; }
        public List<SubscriptionPlanChangeConfirmation> SubscriptionPlanChangeConfirmation { get; set; }
        public Object DisableFreeTrialResponse { get; set; }
        public SubscriptionBase BaseSubscriptionPlan { get; set; }
        public FreeTrialSubscriptionPlan FreeTrialSubscriptionPlan { get; set; }
        public string PaymentType { get; set; }
        public List<TransactionHistory> TransactionHistoryData { get; set; }
        public FullProfile FullProfile { get; set; }
        //public AdTargeting AdTargeting { get; set; }
        public string CancelState { get; set; }
    
    }
}