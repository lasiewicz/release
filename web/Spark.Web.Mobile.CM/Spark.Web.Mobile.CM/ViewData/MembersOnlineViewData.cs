﻿using System;
using System.Collections.Generic;

using Spark.Common.RestConsumer.V2.Models.MembersOnline;
using Spark.Common.RestConsumer.V2.Models.Profile;

namespace Spark.Web.Mobile.CM.ViewData
{
    public class MembersOnlineViewData : BaseViewData
    {
        public List<MiniProfile> Members { get; set; }
    }
}