﻿using System;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Web;
using System.Net;
using System.Configuration;
using System.Collections.Generic;

using RestSharp;

using Spark.Common.RestConsumer.V2.Models;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;
using Spark.Common.RestConsumer.V2.Models.Mingle.Subscription;
using Spark.Common.RestConsumer.V2.Models.Mingle.Content;
using Spark.Web.Mobile.CM.Controllers;
using Spark.Web.Mobile.CM.Framework;
using Spark.Web.Mobile.CM.Utilities;
using Spark.Web.Mobile.CM.Exceptions;
using Spark.Web.Mobile.CM.Models;

namespace Spark.Web.Mobile.CM.ViewData
{
    public class BaseViewData
    {
        private FullProfile _profile;
        //private SubscriptionPlanStatus _subscriptionPlanStatus;
        private string _pageCache = "never";
        private bool _showFooter = true;
        private bool _showHeader = true;
        private bool _showCloseButton = false;
        private bool _showMembersOnlineButton = true;
        private bool _showNavPanel = true;

        public BaseViewData() {}

        public string CssClass { get; set; }

        public int BrandId
        {
            get
            {
                return Int32.Parse(ConfigurationManager.AppSettings["BrandId"]);
            }
        }

        public string Environment 
        {
            get
            {
                return ConfigurationManager.AppSettings["Environment"];
            }
        }

        public bool IsAuthenticated 
        {
            get 
            {
                var authenticationManager = new Spark.Web.Mobile.CM.Framework.AuthenticationManager();
                return authenticationManager.HasValidToken();
            }
        }

        public FullProfile CurrentUserProfile 
        {
            get 
            {
                if (_profile == null) {
                    _profile = GetFullProfile();
                }

                return _profile;
            }
        }

        public string PageId { get; set; }

        public string PageCache
        {
            get
            {
                return _pageCache;
            }
            set
            {
                _pageCache = value;
            }
        }

        public string PageUrl { get; set; }
        public string Title { get; set; }
        public bool IsAjaxRequest { get; set; }

        public string ResrUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["SparkApiUrl"];
            }
        }

        public string VersionCode
        {
            get
            {
                return ConfigurationManager.AppSettings["VersionCode"];
            }
        }


        public string EjabberdServer
        {
            get { return ConfigurationManager.AppSettings["EjabberdServer"]; }
        }

        public string EjabberdPort
        {
            get { return ConfigurationManager.AppSettings["EjabberdPort"]; }
        }

        public string EjabberdProtocol
        {
            get { return ConfigurationManager.AppSettings["EjabberdProtocol"]; }
        }

        public string EjabberdSiteDomain
        {
            get { return ConfigurationManager.AppSettings["EjabberdSiteDomain"]; }
        }

        public string ImApiUrl
        {
            get { return ConfigurationManager.AppSettings["ImApiUrl"]; }
        }

        public string SiteUrl
        {
            get { return ConfigurationManager.AppSettings["SiteUrl"]; }
        }

        public string NewChatURL
        {
            get { return ConfigurationManager.AppSettings["NewChatUrl"]; }
        }

        public string TealiumURL
        {
            get { return ConfigurationManager.AppSettings["TealiumUrl"]; }
        }

        public Session Session { get; set; }

        public bool Scrollable { get; set; }

        public bool ShowCloseButton 
        {
            get
            {
                return _showCloseButton;
            }
            set
            {
                _showCloseButton = value;
            }
        }

        public bool ShowFooter 
        {
            get
            {
                return _showFooter;
            }
            set
            {
                _showFooter = value;
            }
        }

        public bool ShowHeader
        {
            get
            {
                return _showHeader;
            }
            set
            {
                _showHeader = value;
            }
        }

        public bool ShowNavPanel 
        {
            get 
            {
                return _showNavPanel;
            }
            set 
            {
                _showNavPanel = value;
            }
        }

        public bool ShowMembersOnlineButton 
        {
            get
            {
                return _showMembersOnlineButton;
            }
            set
            {
                _showMembersOnlineButton = value;
            }
        }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public string PageTitle { get; set; }        


        private FullProfile GetFullProfile() 
        {

            var model = new ProfileModel();
            if (Session != null)
            {
                return model.GetFullProfile(Session.MemberId);
            }
            else
            {
                return new FullProfile();
            }
        }


        public string MessageAttachmentURLPath
        {
            get
            {
                return ConfigurationManager.AppSettings["MessageAttachmentURLPath"];
            }
        }
        
    }
}