﻿using System;
using System.Collections.Generic;

using Spark.Web.Mobile.CM.Controllers;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;
using Spark.Common.RestConsumer.V2.Models.Mingle.Photos;
using Spark.Common.RestConsumer.V2.Models.Mingle.Match;
using Spark.Common.RestConsumer.V2.Models.Mingle.GAM;
using Spark.Common.RestConsumer.V2.Models.Mingle.Content.AttributeData;

namespace Spark.Web.Mobile.CM.ViewData
{
    public class ProfileViewData : BaseViewData
    {
        public FullProfile Profile { get; set; }
        public List<Photo> Photos { get; set; }
        public MatchPreferences MatchPreferences { get; set; }
        //public AdTargeting AdTargeting { get; set; }
        public List<int> AgeOptions 
        {
            get
            {
                List<int> ageOptions = new List<int>();
                int minAge = 18;
                int maxAge = 99;

                for (int i = minAge; i <= maxAge; i++)
                    ageOptions.Add(i);

                return ageOptions;
            }
        }

        public bool IsOwnProfile { get; set; }

        public bool ShowSA { get; set; }

        public ProfileViewData() : base()
        {
        }
        public List<AttributeOption> ReportAbuseReasons { get; set; }
    }
}