﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;

using Spark.Web.Mobile.CM.Controllers;
using Spark.Common.RestConsumer.V2.Models.Mingle.Photos;

namespace Spark.Web.Mobile.CM.ViewData
{
    public class SettingsViewData : BaseViewData
    {
        public SelfSuspend SelfSuspendData { get; set; }
        public int ReActivateSelfSuspendCode { get; set; }
    }
}