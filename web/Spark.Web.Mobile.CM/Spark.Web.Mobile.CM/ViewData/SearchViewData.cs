﻿using System;
using System.Collections.Generic;

using Spark.Web.Mobile.CM.Controllers;
using Spark.Common.RestConsumer.V2.Models.Mingle.Search;
using Spark.Common.RestConsumer.V2.Models.Mingle.Match;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;
using Spark.Web.Mobile.CM.Framework;

namespace Spark.Web.Mobile.CM.ViewData
{
    public class SearchViewData : BaseViewData
    {
        public SearchResults Results { get; set; }
        public SearchPreferences Preferences { get; set; }
        public MatchPreferences MatchPreferences { get; set; }

    }
}