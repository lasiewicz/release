﻿using System;

namespace Spark.Web.Mobile.CM.ViewData
{
    public class ErrorViewData : BaseViewData
    {
        public Exception Exception { get; set; }
    }
}