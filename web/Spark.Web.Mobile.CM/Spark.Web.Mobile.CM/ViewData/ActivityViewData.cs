﻿using Spark.Web.Mobile.CM.Controllers;

namespace Spark.Web.Mobile.CM.ViewData
{
    public class ActivityViewData : BaseViewData
    {
        public string Type { get; set; }
        public string Filter { get; set; }

        public ActivityViewData() : base()
        {
        }
    }
}