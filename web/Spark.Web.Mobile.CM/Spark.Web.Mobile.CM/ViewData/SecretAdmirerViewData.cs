﻿using System;

using Spark.Web.Mobile.CM.Controllers;
using Spark.Common.RestConsumer.V2.Models.Mingle.Search;

namespace Spark.Web.Mobile.CM.ViewData
{
    public class SecretAdmirerViewData : BaseViewData
    {
        public SearchResults Results { get; set; }
        public string ResultType { get; set; }


        public SecretAdmirerViewData() : base() 
        {
        }
    }
}