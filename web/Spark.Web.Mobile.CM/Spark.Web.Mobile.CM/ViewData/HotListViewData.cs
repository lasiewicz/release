﻿using System;
using System.Collections.Generic;

using Spark.Web.Mobile.CM.Controllers;
using Spark.Common.RestConsumer.V2.Models.HotList;

namespace Spark.Web.Mobile.CM.ViewData
{
    public class HotListViewData : BaseViewData
    {
        public List<HotListEntry> HotLists { get; set; }

        public HotListViewData(): base()
        {
        }
    }
}