﻿using System;
using System.Web;

using Spark.Web.Mobile.CM.Controllers;
using Spark.Web.Mobile.CM.Framework;

namespace Spark.Web.Mobile.CM.ViewData
{
    public class LoginViewData
    {
        public string LoginUrl
        {
            get
            {
                var manager = new AuthenticationManager();
                return manager.GetLoginUrl();
            }
        }
    }
}