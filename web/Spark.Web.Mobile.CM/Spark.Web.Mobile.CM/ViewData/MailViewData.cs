﻿using System;

//using Spark.Common.RestConsumer.V2.Models.Mail;
using Spark.Common.RestConsumer.V2.Models.Mingle.Mail;
using Spark.Web.Mobile.CM.Controllers;

namespace Spark.Web.Mobile.CM.ViewData
{
    public class MailViewData : BaseViewData
    {
        public MailFolder MailFolder { get; set; }
        public MailMessage Message { get; set; }
        public int Folder { get; set; }
        public int SentImMemberId { get; set; }

        public MailViewData() : base()
        {
        }
    }
}