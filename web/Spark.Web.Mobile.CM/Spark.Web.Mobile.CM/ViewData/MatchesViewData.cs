﻿using System;

using Spark.Web.Mobile.CM.Controllers;
using Spark.Common.RestConsumer.V2.Models.Mingle.Search;

namespace Spark.Web.Mobile.CM.ViewData
{
    public class MatchesViewData : BaseViewData
    {
        public SearchResults Results { get; set; }
    }
}