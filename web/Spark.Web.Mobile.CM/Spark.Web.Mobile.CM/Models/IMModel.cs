﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Spark.Common.RestConsumer.V2.Models.Mingle.Mail;
using Spark.Web.Mobile.CM.Framework;
using Spark.Common.RestConsumer.V2.Models.Mingle.Content;

namespace Spark.Web.Mobile.CM.Models
{
    public class IMModel : BaseModel
    {

        public SettingValue GetIMSettingValue()
        {
            return GetSettingValue("USE_MOL_CONSOLIDATION_UI");
        }
    }
}