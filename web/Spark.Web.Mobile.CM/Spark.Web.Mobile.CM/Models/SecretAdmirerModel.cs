﻿using System;
using System.Collections.Generic;
using System.Globalization;

using Spark.Common.RestConsumer.V2.Models.Mingle.Search;
using Spark.Web.Mobile.CM.Framework;
using Spark.Web.Mobile.CM.Utilities;

namespace Spark.Web.Mobile.CM.Models
{
    public class SecretAdmirerModel : BaseSearchModel
    {
        private const string COOKIE_ADMIRERS_PREFERENCES_KEY = "mos_admirers_pref_";
        private const int COOKIE_ADMIRERS_EXPIRATIONS_DAYS = 365;

        public SearchResults GetSecretAdmirers(int pageSize, int page) 
        {
            var preferences = GetPreferences(Session.MemberId);
            var response = Post<SearchResults>(RestUrls.SEARCH_RESULTS, null, new Dictionary<string, string> {
                { "PageSize", pageSize.ToString() },
                { "PageNumber", page.ToString() },
                { "MinAge", preferences.MinAge.ToString(CultureInfo.InvariantCulture) },
                { "MaxAge", preferences.MaxAge.ToString(CultureInfo.InvariantCulture) },
            });

            return response.Data;
        }

        public SearchPreferences GetPreferences(int memberId)
        {
            var cookie = Cookies.GetValues(COOKIE_ADMIRERS_PREFERENCES_KEY + memberId);
            if (cookie == null)
            {
                var preferences = GetMatchPreferences();
                Cookies.Add<SearchPreferences>(String.Format("{0}{1}", COOKIE_ADMIRERS_PREFERENCES_KEY, "" + memberId),
                    preferences, DateTime.Now.AddDays(COOKIE_ADMIRERS_EXPIRATIONS_DAYS));

                return preferences;
            }

            return new SearchPreferences
            {
                MinAge = Convert.ToInt32(cookie["MinAge"]),
                MaxAge = Convert.ToInt32(cookie["MaxAge"]),
            };
        }
    }
}