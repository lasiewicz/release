﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Web.Mobile.JDate.V2.Models.Subscription
{
    public class SubscriptionConfirmationModel
    {
        public SubscriptionConfirmationModel(Spark.Common.RestConsumer.V2.Models.Subscription.TransactionInfo transactionInfo)
        {
            Username = transactionInfo.Username;
            ConfirmationNumber = transactionInfo.ConfirmationNumber;
            CreditCardType = transactionInfo.CreditCardType;
            MaskedCreditCardNumber = transactionInfo.MaskedCreditCardNumber;
            TransactionDate = transactionInfo.TransactionDate;
        }

        public string Username { get; set; }
        public string ConfirmationNumber { get; set; }
        public string CreditCardType { get; set; }
        public string MaskedCreditCardNumber { get; set; }
        public DateTime TransactionDate { get; set; }
        public string SubscriptionStatus { get; set; }

        public List<Spark.Common.RestConsumer.V2.Models.Profile.MiniProfile> SearchResultMembers;
    }
}