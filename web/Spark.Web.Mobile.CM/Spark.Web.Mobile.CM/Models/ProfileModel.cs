﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;

using Spark.Common.RestConsumer.V2.Models.Mingle.Content.AttributeData;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;
using Spark.Common.RestConsumer.V2.Models.Mingle.Photos;
using Spark.Common.RestConsumer.V2.Models.Mingle.Match;
using Spark.Common.RestConsumer.V2.Models.Mingle.GAM;
using Spark.Web.Mobile.CM.Framework;

namespace Spark.Web.Mobile.CM.Models
{
    public class ProfileModel : BaseModel
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(ProfileModel));

        public FullProfile GetFullProfile(int memberId) 
        {
            var response = Get<FullProfile>(RestUrls.PROFILE_ATTRIBUTES, new Dictionary<string, string> { 
                {"attributeSetName", "fullprofile"}  ,
                {RestParams.TARGET_MEMBER_ID, memberId.ToString()}
            }, null);

            return response.Data;
        }

        public FullProfile GetProfileByUsername(string userName)
        {
            var response = Get<FullProfile>(RestUrls.LOOKUP_BY_MEMBER_USERNAME, new Dictionary<string, string> { 
                {"attributeSetName", "fullprofile"}  ,
                {"targetMemberUsername", userName}
            }, null);

            return response.Data;
        }

        public MiniProfile GetMiniProfile(int memberId) 
        {
            _logger.Debug(String.Format("getting miniprofile for: {0}", memberId));
            var response = Get<MiniProfile>(RestUrls.PROFILE_ATTRIBUTES, new Dictionary<string, string> { 
                { "attributeSetName", "miniprofile" },
                { RestParams.TARGET_MEMBER_ID, memberId.ToString() }
            }, null);

            return response.Data;      
        }

        public List<Photo> GetPhotos(int memberId) 
        {
            var response = Get<List<Photo>>(RestUrls.PHOTOS_LIST, new Dictionary<string, string> { 
                {RestParams.TARGET_MEMBER_ID, memberId.ToString()}
            }, null);

            return response.Data;
        }

        public MatchPreferences GetMatchPreferences()
        {
            var response = Get<MatchPreferences>(RestUrls.MATCH_PREFERENCES, new Dictionary<string, string> { 
            }, null);

            return response.Data;
        }


        public List<AttributeOption> GetAttributeOptions(string optionType)
        {
            var response = Get<List<AttributeOption>>(RestUrls.ATTRIBUTE_OPTIONS, new Dictionary<string, string>
            {
                {"attributeName", optionType}
            }, null);

            return response.Data;
        } 
    }
}