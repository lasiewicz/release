﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;
using Spark.Common.RestConsumer.V2.Models;
using Spark.Web.Mobile.CM.Framework;
using Spark.Common.RestConsumer.V2.Models.MembersOnline;
using Spark.Web.Mobile.CM.Utilities;
using System.Globalization;
using Spark.Web.Mobile.CM.Exceptions;

namespace Spark.Web.Mobile.CM.Models
{
    public class MembersOnlineModel : BaseModel
    {
        private const string COOKIE_MOL_PREFERENCES_KEY = "mos_mol_pref_";
        private const int COOKIE_MOL_EXPIRATIONS_DAYS = 365;

        private static MembersOnlinePreferences _defaultPreferences = new MembersOnlinePreferences
        {
            SeekingGender = "",
            LanguageId = -2147483647,  // Any Language
            MinAge = 25,
            MaxAge = 40,
            RegionId = 223 // United States
        };

        public MembersOnlineResults GetMembersOnline(int pageSize, int page)
        {
            MembersOnlinePreferences preferences = GetPreferences(Session.MemberId);
            var data = new Dictionary<string, string>
            {
                {"sortType", "2"}, // 2 = insert date
                {"PageSize", pageSize.ToString(CultureInfo.InvariantCulture)},
                {"PageNumber", page.ToString(CultureInfo.InvariantCulture)},
                {"LanguageId", preferences.LanguageId.ToString(CultureInfo.InvariantCulture)},
                {"MinAge", preferences.MinAge.ToString(CultureInfo.InvariantCulture)},
                {"MaxAge", preferences.MaxAge.ToString(CultureInfo.InvariantCulture)},
                {"RegionId", preferences.RegionId.ToString(CultureInfo.InvariantCulture)}
            };

            if (!String.IsNullOrEmpty(preferences.SeekingGender))
                data.Add("SeekingGender", preferences.SeekingGender);

            var response = base.Get<MembersOnlineResults>(RestUrls.MEMBERS_ONLINE_LIST, null, data);
            return response.Data;
        }

        public MembersOnlineCount GetMembersOnlineCount() 
        {
            var response = Get<MembersOnlineCount>(RestUrls.MEMBERS_ONLINE_COUNT, null, null);
            return response.Data;
        }

        private MembersOnlinePreferences GetPreferences(int memberId) 
        {
            var cookie = Cookies.GetValues(COOKIE_MOL_PREFERENCES_KEY + memberId);
            if (cookie == null) 
            { 
                // If no cookie exist, create cookie and use default preferences.
                Cookies.Add<MembersOnlinePreferences>(String.Format("{0}{1}", COOKIE_MOL_PREFERENCES_KEY, "" + memberId), 
                    _defaultPreferences, DateTime.Now.AddDays(COOKIE_MOL_EXPIRATIONS_DAYS));

                return _defaultPreferences;
            }

            return new MembersOnlinePreferences {
                SeekingGender = cookie["SeekingGender"],
                LanguageId = Convert.ToInt32(cookie["LanguageId"]),
                MinAge = Convert.ToInt32(cookie["MinAge"]),
                MaxAge = Convert.ToInt32(cookie["MaxAge"]),
                RegionId = Convert.ToInt32(cookie["RegionId"])
            };
        }
    }
}