﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Spark.Common.RestConsumer.V2.Models.Mingle.Match;
using Spark.Common.RestConsumer.V2.Models.Mingle.Search;
using Spark.Web.Mobile.CM.Framework;


namespace Spark.Web.Mobile.CM.Models
{
    public class SearchModel : BaseSearchModel
    {
        /*private static readonly SearchPreferences _defaultPreferences = new SearchPreferences {
            Gender = "Male",
            SeekingGender = "Female",
            MinAge = 25,
            MaxAge = 40,
            ShowOnlyMembersWithPhotos = true,
            MaxDistance = 50
        };*/

        public SearchResults GetSearchResults(int pageSize, int page) 
        { 
            // Get search preference(s) to pass to search result API.
            var preferences = GetMatchPreferences();
            var data = new Dictionary<string, string> {
                { "PageSize", pageSize.ToString() },
                { "PageNumber", page.ToString() },
                { "Gender", preferences.Gender },
                { "SeekingGender", preferences.SeekingGender },
                { "MinAge", preferences.MinAge.ToString(CultureInfo.InvariantCulture) },
                { "MaxAge", preferences.MaxAge.ToString(CultureInfo.InvariantCulture) },
                { "MaxDistance", preferences.MaxDistance.ToString(CultureInfo.InvariantCulture) },
                { "ShowOnlyMembersWithPhotos", preferences.ShowOnlyMembersWithPhotos.ToString(CultureInfo.InvariantCulture) },
            };

            if (!String.IsNullOrEmpty(preferences.Location))
                data.Add("Location", preferences.Location);

            var response = Post<SearchResults>(RestUrls.SEARCH_RESULTS, null, data);

            return response.Data;
        }

        public MatchPreferences GetFullMatchPreferences()
        {
            var response = Get<MatchPreferences>(RestUrls.MATCH_PREFERENCES, new Dictionary<string, string>
            {
            }, null);

            return response.Data;
        }
    }
}