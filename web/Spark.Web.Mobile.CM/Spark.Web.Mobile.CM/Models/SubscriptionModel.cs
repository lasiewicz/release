﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using Spark.Web.Mobile.CM.Framework;
using Spark.Common.RestConsumer.V2.Models.Mingle.Subscription;
using Spark.Common.RestConsumer.V2.Models.Mingle.GAM;
using Spark.Common.RestConsumer.V2.Models;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;
using Newtonsoft.Json;

using log4net;

namespace Spark.Web.Mobile.CM.Models
{

    public class SubscriptionModel : BaseModel
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(SubscriptionModel));
        public PaymentUiPostData PostPayment(PaymentUiPostDataRequest data)
        {
            string prtid = null;
            var customprtid = prtid;


            var paymentDictionary = new Dictionary<string, string>
            {
                   {"PaymentUiPostDataType", data.PaymentUiPostDataType},
                   {"CancelUrl", data.CancelUrl},
                   {"ConfirmationUrl", data.ConfirmationUrl },
                   {"DestinationUrl", data.DestinationUrl },
                   {"ReturnUrl", data.ReturnUrl},
                   {"ClientIp", data.ClientIp},
                   {"PrtTitle", data.PrtTitle},
                   //{"SrId", data.SrId},
                   {"PrtId",data.PrtId},
                   {"MemberLevelTrackingLastApplication", data.MemberLevelTrackingLastApplication},
                   //{"MemberLevelTrackingIsMobileDevice", data.MemberLevelTrackingIsMobileDevice},
                   //{"MemberLevelTrackingIsTablet", data.MemberLevelTrackingIsTablet},
                   {"PromoID", data.PromoID.ToString()},
                   {"PromoType", data.PromoType },
                   {"TemplateId", data.TemplateId},
                  
            };

            paymentDictionary.Add("OmnitureVariablesJson", JsonConvert.SerializeObject(data.OmnitureVariables));

            _logger.Debug(String.Format("getting payment Dictionary for", paymentDictionary));

            var response = Post<PaymentUiPostData>(RestUrls.POST_PAYMENT, null, paymentDictionary);
            return response.Data;
        }

        public List<SubscriptionPlanChangeConfirmation> UpdateSubscriptionPlanStatus(Object data)
        {

            var subscriptionChangeResponse =
                PutJson<List<SubscriptionPlanChangeConfirmation>>(RestUrls.SUBSCRIPTION_PLAN_RESPONSE, null,
                 data);
            return subscriptionChangeResponse.Data;
        }

        public List<TransactionHistory> GetTransactionHistory()
        {
            var transactionResponse = Get<List<TransactionHistory>>(RestUrls.BILLING_HISTORY, null, null);
            _logger.Debug(String.Format("getting transactionresponse:", transactionResponse));
            return transactionResponse.Data;
        }

        public List<MiniProfile> SearchResultMembers;


        public FullProfile GetFullProfile(int memberId)
        {
            var response = Get<FullProfile>(RestUrls.PROFILE_ATTRIBUTES, new Dictionary<string, string> { 
                {"attributeSetName", "fullprofile"}  ,
                {RestParams.TARGET_MEMBER_ID, memberId.ToString()}
            }, null);

            return response.Data;
        }

        public SubscriptionPlanStatus GetSubscriptionPlanStatus()
        {
            var subscriptionResponse = Get<SubscriptionPlanStatus>(RestUrls.SUBSCRIPTION_PLAN_STATUS, null, null);

            return subscriptionResponse.Data;
        }


        public List<AccessPrivileges> GetAccessPrivileges()
        {
            var accessPrivilegeResponse = Get<List<AccessPrivileges>>(RestUrls.ACCESS_PRIVILEGE_RESPONSE, null, null);

            return accessPrivilegeResponse.Data;
        }

        //public List<TransactionHistory> GetTransactionHistory()
        //{
        //    var transactionHistoryResponse = Get<List<TransactionHistory>>(RestUrls.BILLING_HISTORY, null, null);

        //    return transactionHistoryResponse.Data;
        //}

        public Object DisableFreeTrial()
        {
            var disableFreeTrialResponse = PutJson<Object>(RestUrls.DISABLE_FREE_TRIAL, null, null);

            return disableFreeTrialResponse;
        }


    }


}