﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Common.RestConsumer.V2.Models.Mingle.Profile;

using Spark.Web.Mobile.CM.Framework;

namespace Spark.Web.Mobile.CM.Models
{
    public class SettingsModel : BaseModel
    {
        public int ReActivateSelfSuspended()
        {
            var reactivateSelfSuspend = PutJson<ReActivateSelfSuspend>(RestUrls.REACTIVATE_SELF_SUSPEND, null, null);
            return reactivateSelfSuspend.Code;
        }

        public SelfSuspend SelfSuspend(int reasonID)
        {
            var selfSuspendRequest = new SelfSuspendRequest();
            selfSuspendRequest.SuspendReasonId = reasonID;

            var selfSuspend = PutJson<SelfSuspend>(RestUrls.SELF_SUSPEND, null, selfSuspendRequest);
            return selfSuspend.Data;
        }
    }
}