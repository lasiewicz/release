﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RestSharp;

using Spark.Web.Mobile.CM.Utilities;
using Spark.Common.RestConsumer.V2.Models;
using Spark.Common.RestConsumer.V2.Models.Mingle;
using Spark.Common.RestConsumer.V2.Models.Mingle.Content;
using System.Net;
using Spark.Web.Mobile.CM.Exceptions;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Web.Mobile.CM.Framework;


namespace Spark.Web.Mobile.CM.Models
{
    public abstract class BaseModel
    {
        //private ApiClient _client = new ApiClient(Session.AccessToken);

        protected Spark.Web.Mobile.CM.Framework.Session Session
        {
            get
            {
                return (Spark.Web.Mobile.CM.Framework.Session)HttpContext.Current.Items[ApplicationContext.CONTEXT_SESSION_KEY];
            }
        }
        protected SettingValue GetSettingValue(String runtimeSettingType)
        {
            //talk to the runtime setting endpoint to get setting. The value returned in the object is a string so that it can be true/false or a more specific setting as needed
            var urlParams = new Dictionary<string, string>
            {
                { "settingName", runtimeSettingType },
            };

            var response = Get<SettingValue>(RestUrls.SETTING_VALUE, urlParams, null);
            return response.Data;

        }
        protected ResponseWrapper<T> Get<T>(RestUrl url, Dictionary<string, string> urlParams, Dictionary<string, string> data) where T : new()
        {
            return Execute<T>(url, urlParams, data, Method.GET);
        }

        protected ResponseWrapper<T> Post<T>(RestUrl url, Dictionary<string, string> urlParams, Dictionary<string, string> data) where T : new()
        {
            return Execute<T>(url, urlParams, data, Method.POST);
        }

        protected ResponseWrapper<T> PutJson<T>(RestUrl url, Dictionary<string, string> urlParams, Object data) where T : new()
        {
            return ExecuteJson<T>(url, urlParams, data, Method.PUT);
        }

        private ResponseWrapper<T> Execute<T>(RestUrl url, Dictionary<string, string> urlParams, Dictionary<string, string> data, Method method) 
        {
            var client = ApiClient.GetInstance(Session.AccessToken);
            var response = client.Execute<ResponseWrapper<T>>(url, urlParams, data, method);

            // Check to see if unauthorize call to API is attempted 
            // (could be access token required, expired, etc.)
            if (response.Code == (int)HttpStatusCode.Unauthorized)
                throw new AuthorizationException(response.Error.Code, ParseError(response.Error));
            if (response.Code != 0 && response.Code != (int)HttpStatusCode.OK)
                throw new ApiException(ParseError(response.Error))
                { 
                    ErrorCode = response.Error.Code,
                    HttpStatusCode = response.Code,
                    Type = this.GetType()
                };

            return response;
        }

        private ResponseWrapper<T> ExecuteJson<T>(RestUrl url, Dictionary<string, string> urlParams, Object data, Method method)
        {
            var client = ApiClient.GetInstance(Session.AccessToken);
            var response = client.ExecuteJson<ResponseWrapper<T>>(url, urlParams, data, method);

            // Check to see if unauthorize call to API is attempted 
            // (could be access token required, expired, etc.)
            if (response.Code == (int)HttpStatusCode.Unauthorized)
                throw new AuthorizationException(response.Error.Code, ParseError(response.Error));
            if (response.Code != 0 && response.Code != (int)HttpStatusCode.OK)
                throw new ApiException(ParseError(response.Error))
                {
                    ErrorCode = response.Error.Code,
                    HttpStatusCode = response.Code,
                    Type = this.GetType()
                };

            return response;
        }



        private string ParseError(Error error)
        {
            return String.Format("{0} - {1}", error.Code, error.Message);
        }
    }
}