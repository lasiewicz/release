﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Spark.Common.RestConsumer.V2.Models.Mingle.Search;
using Spark.Web.Mobile.CM.Framework;

namespace Spark.Web.Mobile.CM.Models
{
    public abstract class BaseSearchModel : BaseModel
    {
        public SearchPreferences GetMatchPreferences() 
        {
            var response = Get<SearchPreferences>(RestUrls.MATCH_PREFERENCES, null, null);
            return response.Data;
        }
    }
}