﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Spark.Common.RestConsumer.V2.Models.HotList;
using Spark.Web.Mobile.CM.Framework;

namespace Spark.Web.Mobile.CM.Models
{
    public class HotListModel : BaseModel
    {
        public enum HotListCategory 
        {
            Default = 1,
            WhoAddedYouToTheirFavorites = 2,
            WhoViewedYourProfile
        }

        public List<HotListEntry> GetHotList(HotListCategory category, int pageSize, int page) 
        {
            var response = Get<List<HotListEntry>>(RestUrls.HOTLIST, new Dictionary<string, string> { 
                { "hotListCategory", Enum.GetName(category.GetType(), category) },
                { RestParams.PAGE_SIZE, pageSize.ToString() },
                { RestParams.PAGE, page.ToString() }
            }, null);

            return response.Data;
        }
    }
}