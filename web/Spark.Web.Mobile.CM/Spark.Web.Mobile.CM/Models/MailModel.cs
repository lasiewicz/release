﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Spark.Common.RestConsumer.V2.Models.Mingle.Mail;
using Spark.Web.Mobile.CM.Framework;

namespace Spark.Web.Mobile.CM.Models
{
    public class MailModel : BaseModel
    {
        public MailFolder GetMailFolder(int folderId, int pageSize, int page)
        {
            var urlParams = new Dictionary<string, string>
            {
                { "folderId", folderId.ToString() },
                { RestParams.PAGE_SIZE, pageSize.ToString() },
                { RestParams.PAGE, page.ToString() }
            };

            var response = base.Get<MailFolder>(RestUrls.MAIL_FOLDER, urlParams, null);
            return response.Data;
        }

        public MailMessage GetMailMessage(int messageId)
        {
            var urlParams = new Dictionary<string, string>
            {
                { "messageId", messageId.ToString() },
               
            };

            //var data = new Dictionary<string, string> 
            //{
            //    {"memberId", ApplicationContext.GetSession.MemberId.ToString() }
            //};

            var response = base.Get<MailMessage>(RestUrls.MAIL_MESSAGE, urlParams, null);
            return response.Data;
        }
    }
}