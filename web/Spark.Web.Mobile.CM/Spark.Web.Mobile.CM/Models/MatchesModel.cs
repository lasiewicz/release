﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

using Spark.Common.RestConsumer.V2.Models.Mingle.Search;
using Spark.Web.Mobile.CM.Framework;
using Spark.Web.Mobile.CM.Utilities;

namespace Spark.Web.Mobile.CM.Models
{
    public class MatchesModel : BaseSearchModel
    {
        private const string COOKIE_MATCHES_PREFERENCES_KEY = "mos_matches_pref_";
        private const int COOKIE_MATCHES_EXPIRATIONS_DAYS = 365;

        public SearchResults GetMatches(int pageSize, int page)
        {
            var preferences = GetPreferences(Session.MemberId);
            var response = Get<SearchResults>(RestUrls.MATCH_RESULT, new Dictionary<string, string> { 
                    {RestParams.PAGE_SIZE, pageSize.ToString()},
                    {RestParams.PAGE, page.ToString()}
                }, 
           
                new Dictionary<string, string> { 
                    { "Gender", preferences.Gender },
                    { "SeekingGender", preferences.SeekingGender },
                    { "MinAge", preferences.MinAge.ToString(CultureInfo.InvariantCulture) },
                    { "MaxAge", preferences.MaxAge.ToString(CultureInfo.InvariantCulture) },
                    { "MaxDistance", preferences.MaxDistance.ToString(CultureInfo.InvariantCulture) },
                    { "Location", String.IsNullOrEmpty(preferences.Location) ? "" : preferences.Location },
                    { "ShowOnlyMembersWithPhotos", preferences.ShowOnlyMembersWithPhotos.ToString(CultureInfo.InvariantCulture) },
            });

            return response.Data;
        }

        public SearchPreferences GetPreferences(int memberId)
        {
            var cookie = Cookies.GetValues(COOKIE_MATCHES_PREFERENCES_KEY + memberId);
            if (cookie == null)
            {
                var preferences = GetMatchPreferences();
                Cookies.Add<SearchPreferences>(String.Format("{0}{1}", COOKIE_MATCHES_PREFERENCES_KEY, "" + memberId),
                    preferences, DateTime.Now.AddDays(COOKIE_MATCHES_EXPIRATIONS_DAYS));

                return preferences;
            }

            return new SearchPreferences
            {
                SeekingGender = cookie["SeekingGender"],
                Gender = cookie["Gender"],
                MinAge = Convert.ToInt32(cookie["MinAge"]),
                MaxAge = Convert.ToInt32(cookie["MaxAge"]),
                MaxDistance = Convert.ToInt32(cookie["MaxDistance"]),
                Location = cookie["Location"],
                ShowOnlyMembersWithPhotos = Convert.ToBoolean(cookie["ShowOnlyMembersWithPhotos"]),
            };
        }
    }
}