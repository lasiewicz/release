﻿using System;
using System.Configuration;

namespace Spark.Web.Mobile.CM.Configurations
{
    public class AllowedPageSection : ConfigurationSection
    {
        [ConfigurationProperty("pages", IsRequired = false)]
        public AllowedPageCollection Pages
        {
            get
            {
                return (AllowedPageCollection)this["pages"];
            }
            set
            {
                this["pages"] = value;
            }
        }
    }
}