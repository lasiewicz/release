﻿using System;
using System.Configuration;

namespace Spark.Web.Mobile.CM.Configurations
{
    [ConfigurationCollection(typeof(AllowedPageElement))]
    public class AllowedPageCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new AllowedPageElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((AllowedPageElement)element).Name;
        }
    }
}