﻿using System.Web;
using System.Web.Mvc;

using Spark.Web.Mobile.CM.Filters;

namespace Spark.Web.Mobile.CM
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new SessionRequired());
            //filters.Add(new AuthorizationLogging());
            //filters.Add(new ErrorLogging());
        }
    }
}
