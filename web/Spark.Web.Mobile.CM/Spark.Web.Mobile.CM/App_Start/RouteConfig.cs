﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.History;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Spark.Web.Mobile.CM
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("favicon.ico");
            routes.IgnoreRoute("robots.txt");

            // Admin
            routes.MapRoute(
                "healthcheck", "admin/healthcheck",
                new { controller = "Admin", action = "HealthCheck" },
                new { httpMethod = new HttpMethodConstraint("Get") });

            // Activty
            routes.MapRoute("activity", "activity",
                new { controller = "Activity", action = "Activity" });
            routes.MapRoute("activity-home", "home",
                new { controller = "Activity", action = "Activity" });

            // Error
            routes.MapRoute("error", "error",
                new { controller = "Error", action = "Error" });
            routes.MapRoute("error-pagenotfound", "pagenotfound",
                new { controller = "Error", action = "PageNotFound" });

            //Self Suspended
            routes.MapRoute("selfsuspend", "selfsuspend",
                new { controller = "Home", action = "SelfSuspend" });

            // Hotlist
            routes.MapRoute("hotlist_favorites", "hotlist/favorites",
                new { controller = "HotList", action = "Favorites" });

            routes.MapRoute("im", "im",
                new { controller = "IM", action = "InstantMessages" });

            // Login
            routes.MapRoute("login", "login",
                new { controller = "Login", action = "Login" });
            routes.MapRoute("logout", "logout",
                 new { controller = "Login", action = "Logout" });
            routes.MapRoute("logon", "logon/logon",
                new { controller = "Login", action = "Logon" });

            // Mail
            routes.MapRoute("mail", "mail",
                new { controller = "Mail", action = "Mail" });
            //routes.MapRoute("mail_message", "mail/message/{messageId}",
            //    new { controller = "Mail", action = "Message", messageId = UrlParameter.Optional });
            routes.MapRoute("chats", "chats",
               new { controller = "Mail", action = "Chat" });
            routes.MapRoute("chat-history", "chats/history/{memberId}",
                 new { controller = "Mail", action = "ChatHistory", memberId = UrlParameter.Optional });
    

            // Matches
            routes.MapRoute("matches", "matches/{page}",
                new { controller = "Matches", 
                    action = "Matches", 
                    page = UrlParameter.Optional });

            // Members Online
            routes.MapRoute("members_online", "membersonline/{page}",
                new { controller = "MembersOnline", 
                    action = "MembersOnline", 
                    page = UrlParameter.Optional });

            // Profile 
            routes.MapRoute("profile", "profile/{id}",
                new { controller = "Profile", action = "Profile", id = UrlParameter.Optional },
                new { id = @"\d+" });
            routes.MapRoute("profile-static", "profile/static/{id}",
                new { controller = "Profile", action = "Profile", id = UrlParameter.Optional },
                new { id = @"\d+" });
            routes.MapRoute("profile-sa", "profile/sa/{id}",
                new { controller = "Profile", action = "Profile", id = UrlParameter.Optional },
                new { id = @"\d+" });
            routes.MapRoute("profile-username", "profile/username/{username}",
                new { controller = "Profile", action = "ProfileUsername", username = UrlParameter.Optional });  
            routes.MapRoute("profile-photos", "profile/photos",
                new { controller = "Profile", action = "Photos"});
            routes.MapRoute("profile-default", "profile",
                new { controller = "Profile", action = "Profile", id = UrlParameter.Optional });
            routes.MapRoute("profile-reportabuse", "profile/report/{id}",
             new { controller = "Profile", action = "ReportAbuse", id = UrlParameter.Optional },
             new { id = @"\d+" });
            //routes.MapRoute("profile-photos-facebook", "profile/facebook-photos",
            //    new { controller = "Profile", action = "Facebook" });

            // Home
            routes.MapRoute("contactus", "home/contactus",
                new { controller = "Home", action = "ContactUs" });
            routes.MapRoute("safety", "home/safety", 
                new  { controller = "Home", action = "Safety" });
            routes.MapRoute("privacy", "home/privacy",
                new { controller = "Home", action = "Privacy" });
            routes.MapRoute("termsofservice", "home/termsofservice",
                new { controller = "Home", action = "TermsOfService" });
            routes.MapRoute("termsandconditions", "home/termsandconditions",
                new { controller = "Home", action = "TermsOfService" });
            routes.MapRoute("cookie", "home/cookiepolicy",
                new { controller = "Home", action = "CookiePolicy" });
            routes.MapRoute("imredirect", "home/imredirect",
                new { controller = "Home", action = "ImRedirect" });
            //routes.MapRoute("mobile", "mobile",
            //    new { controller = "Home", action = "MobileApp" });

            // Search
            routes.MapRoute("search_results", "search/results",
                new { controller = "Search", action = "Results" });
            routes.MapRoute("search_preferences", "search/preferences",
                new { controller = "Search", action = "Preferences"});
            routes.MapRoute("search_default", "search",
                new { controller = "Search", action = "Preferences" }); // Default route for Search

            // Secret Admirer
            routes.MapRoute("secret_admirer", "secretadmirer",
                new { controller = "SecretAdmirer", action = "SecretAdmirer" });
            routes.MapRoute("secret_admirer_settings", "secretadmirer/settings",
                new { controller = "SecretAdmirer", action = "Settings" });
            routes.MapRoute("secret_admirer_results", "secretadmirer/results/{type}",
                new { controller = "SecretAdmirer", action = "Results", type = UrlParameter.Optional });
            //routes.MapRoute("secret_admirer_results", "secretadmirer/results",
              //  new { controller = "SecretAdmirer", action = "Results" });

            // Settings
            routes.MapRoute("settings", "settings",
                new { controller = "Settings", action = "Settings" });
            routes.MapRoute("settings-message", "settings/message",
                new { controller = "Settings", action = "MessageSettings" });
            routes.MapRoute("settings-unsuspend", "settings/unsuspend",
                new { controller = "Settings", action = "ReactivateSelfSuspend" });
            routes.MapRoute("settings-selfsuspend", "settings/suspend/{reasonid}",
                new { controller = "Settings", action = "SelfSuspendSettings", reasonId = UrlParameter.Optional});
            routes.MapRoute("settings-spotlight", "settings/spotlight",
                new { controller = "Settings", action = "SpotlightSettings" });
            routes.MapRoute("settings-account", "settings/account",
                new { controller = "Subscription", action = "AccountSettings" });

            routes.MapRoute("settings-blockedmembers", "settings/blockedmembers",
                new { controller = "Settings", action = "BlockedMembers" });
            routes.MapRoute("settings-emailalerts", "settings/emailalerts",
               new { controller = "Settings", action = "EmailAlerts" });

            // Subscription
            routes.MapRoute("subscribe", "subscription/subscribe/{prtid}/{promoid}/{lastapplication}",
               new { controller = "Subscription", action = "Subscribe", prtid = UrlParameter.Optional, promoid = UrlParameter.Optional, lastapplication = UrlParameter.Optional });
            routes.MapRoute("confirm", "subscription/confirm/{paymentType}",
                            new { controller = "Subscription", action = "Confirm", paymentType = UrlParameter.Optional });
            routes.MapRoute("history", "subscription/history",
                new {controller = "Subscription", action = "HistoryPage"});
            routes.MapRoute("premium", "subscription/premium/{prtid}",
                new { controller = "Subscription", action = "Premium", prtid = UrlParameter.Optional });
            routes.MapRoute("cancel", "subscription/cancel/{subscriptionType}/{enableOrDisable}/{reasonIdOrPremType}",
                new { controller = "Subscription", action = "Cancel", subscriptionType = UrlParameter.Optional, enableOrDisable = UrlParameter.Optional, reasonIdOrPremType = UrlParameter.Optional });
         
            // Default route (catch all)
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Activity", action = "Activity" });
        }
    }
}
