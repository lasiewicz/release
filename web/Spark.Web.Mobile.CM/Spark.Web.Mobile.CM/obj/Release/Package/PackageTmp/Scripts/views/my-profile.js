﻿spark.views.myprofile = (function () {
    var self = {};
    var editedFields = [];
    var reviewFields = [];

    /*
        This is a mapping between what will be sent back to the server on update
        and what is called through the get attribute options endpoint.
        The first value is what will be returned to the server, 
        The second value is what attribute type will be called to get the options.

    */


    var attributeOptionsMap = {
        // Basic Info
        "gender":"gender",
        "user_name": "username",
        "marital_status": "marital_status",
        "birthdate": "birthdate",
        "relationship_status": "marital_status",
        "ministry_displayname": "ministry_displayname",

        //Introduction
        "looking_for": "greeting",
        "perfect_first_date": "first_date",
        "past_relationships": "past_relationships",
        "christian_meaning": "christian_meaning",
        "christian_how_long": "christian_how_long",
        "five_years": "five_years",
        "favorite_scripture": "favorite_scripture",
        "anything_else": "anythingelse",

        //About me
        "height": "height",
        "body_type": "body_type",
        "hair": "hair",
        "eyes": "eyes",
        "marital_status": "marital_status",
        "about_children": "about_children",
        "children": "children",
        "children_at_home": "children_at_home",
        "smoke": "smoke",
        "drink": "drink",
        "religion": "religion",
        "religion2": "religion2",
        "church_activity": "church_activity",
        "education": "education",
        "occupation": "occupation",
        "ethnicity": "ethnicity",

        // Fun Facts - in the About Me section
        "self_description": "self_description",
        "favorite_music": "music",
        "favorite_musicians": "favorite_musicians",
        "languages": "language",
        "movie": "movie",
        "favorite_movies": "favorite_movies",
        "favorite_tv": "favorite_tv",
        "outdoors": "outdoors",
        "indoors": "indoors",
        "punctual": "punctual",
        "trendy": "trendy",
        "great_trip": "great_trip",
        "favorite_foods": "food",
        "favorite_restaurants": "favorite_restaurants",
        "political": "political",
        "schools_attended": "schools_attended",

        //My Match
        "age_range": "age",
        "min_age_range": "age",
        "max_age_range": "age",
        "wants_distance":"distance",
        "wants_height": "height",
        "wants_min_height": "height",
        "wants_max_height": "height",
        "wants_body_type": "body_type",
        "wants_education": "education",
        "wants_marital_status": "marital_status",
        "wants_religion": "religion",
        "wants_church_activity": "church_activity",
        "wants_ethnicity": "ethnicity",
        "wants_smoke": "smoke",
        "wants_drink": "drink",
        "bodyTypeImportance": "bodyTypeImportance",
        "religionImportance": "religionImportance",
        "educationImportance": "educationImportance",
        "maritalStatusImportance": "maritalStatusImportance",
        "religionImportance": "religionImportance",
        "churchActivityImportance": "churchActivityImportance",
        "ethnicityImportance": "ethnicityImportance",
        "smokeImportance": "smokeImportance",
        "drinkImportance": "drinkImportance",

        //Misc attributes that are not included
        "looks": "looks",
        "mentality": "mentality",
        "living_quarters": "living_quarters",
        "prefer_to_live": "prefer_to_live",
        "planning": "planning",
        "season": "season",
       // "greeting": "greeting",
    };


    /* 
        This is a mapping between what comes back from the match preferences call and
    */

    var jsonMapping = {
        //Basic Info
        "gender":"gender",
        "user_name": "username",
        "marital_status": "marital_status",
        "gender_seeking": "seekingGender",
        "birthdate": "birthdate",

        //Introduction
        "looking_for": "greeting",
        "perfect_first_date": "first_date",
        "past_relationships": "past_relationships",
        "christian_meaning": "christian_meaning",
        "christian_how_long": "christian_how_long",
        "five_years": "five_years",
        "favorite_scripture": "favorite_scripture",
        "anything_else": "anythingelse",

        //About Me
        "height": "height",
        "body_type": "body_type",
        "hair": "hair",
        "eyes": "eyes",
        "marital_status": "marital_status",
        "about_children": "about_children",
        "children": "children",
        "children_at_home": "children_at_home",
        "smoke": "smoke",
        "drink": "drink",
        "religion": "religion",
        "religion2": "religion2",
        "church_activity": "church_activity",
        "education": "education",
        "occupation": "occupation",
        "ethnicity": "ethnicity",

        // Fun Facts - in the About Me section
        "self_description": "self_description",
        "favorite_music": "music",
        "favorite_musicians": "favorite_musicians",
        "languages": "language",
        "movie": "movie",
        "favorite_movies": "favorite_movies",
        "favorite_tv": "favorite_tv",
        "outdoors": "outdoors",
        "indoors": "indoors",
        "punctual": "punctual",
        "trendy": "trendy",
        "great_trip": "great_trip",
        "favorite_foods": "food",
        "favorite_restaurants": "favorite_restaurants",
        "political": "political",
        "schools_attended": "schools_attended",

        //My Match
        "age_range": "wants_age",
        "min_age_range": "minAge",
        "max_age_range": "maxAge",
        "wants_distance": "maxDistance",
        "wants_min_height": "minHeight",
        "wants_max_height": "maxHeight",
        "wants_body_type": "bodyType",
        "wants_height": "height",
        "wants_education": "education",
        "wants_marital_status": "maritalStatus",
        "wants_religion": "religion",
        "wants_church_activity": "churchActivity",
        "wants_ethnicity": "ethnicity",
        "wants_smoke": "smoke",
        "wants_drink": "drink",
        "bodyTypeImportance": "bodyTypeImportance",
        "religionImportance": "religionImportance",
        "educationImportance": "educationImportance",
        "maritalStatusImportance": "maritalStatusImportance",
        "religionImportance": "religionImportance",
        "churchActivityImportance": "churchActivityImportance",
        "ethnicityImportance": "ethnicityImportance",
        "smokeImportance": "smokeImportance",
        "drinkImportance": "drinkImportance",
        

        //Misc attributes that are not included
        "looks": "looks",
        "mentality": "mentality",
        "living_quarters": "living_quarters",
        "prefer_to_live": "prefer_to_live",
        "planning": "planning",
        "season": "season",
        //"greeting": "greeting"

    }

    var tabMapping = {
        "user_name": "Basic Info",
        "marital_status": "Basic Info",
        "birthdate": "Basic Info",

        "looking_for": "Introduction",
        "perfect_first_date": "Introduction",
        "past_relationships": "Introduction",
        "christian_meaning": "Introduction",
        "christian_how_long": "Introduction",
        "five_years": "Introduction",
        "favorite_scripture": "Introduction",
        "anything_else": "Introduction",

        "greeting": "Introduction",
        "anythingelse": "Introduction",
        "christian_meaning": "Introduction",
        "christian_how_long": "Introduction",
        "five_years": "Introduction",
        "favorite_scripture": "Introduction",
        "perfect_first_date": "Introduction",
        "ideal_relationship": "Introduction",
        "past_relationships": "Introduction",


        "height": "About Me",
        "body_type": "About Me",
        "hair": "About Me",
        "eyes": "About Me",
        "marital_status": "About Me",
        "about_children": "About Me",
        "children": "About Me",
        "children_at_home": "About Me",
        "smoke": "About Me",
        "drink": "About Me",
        "religion": "About Me",
        "religion2": "About Me",
        "church_activity": "About Me",
        "education": "About Me",
        "occupation": "About Me",
        "ethnicity": "About Me",

        "self_description": "About Me",
        "favorite_music": "About Me",
        "favorite_musicians": "About Me",
        "languages": "About Me",
        "movie": "About Me",
        "favorite_movies": "About Me",
        "favorite_tv": "About Me",
        "outdoors": "About Me",
        "indoors": "About Me",
        "punctual": "About Me",
        "trendy": "About Me",
        "great_trip": "About Me",
        "favorite_foods": "About Me",
        "favorite_restaurants": "About Me",
        "political": "About Me",
        "schools_attended": "About Me",



        //My Match
        "age_range": "My Match",
        "min_age_range": "My Match",
        "max_age_range": "My Match",
        "wants_distance": "My Match",
        "wants_min_height": "My Match",
        "wants_max_height": "My Match",
        "wants_body_type": "My Match",
        "wants_height": "My Match",
        "wants_education": "My Match",
        "wants_marital_status": "My Match",
        "wants_religion": "My Match",
        "wants_church_activity": "My Match",
        "wants_ethnicity": "My Match",
        "wants_smoke": "My Match",
        "wants_drink": "My Match",
        "bodyTypeImportance": "My Match",
        "ethnicityImportance": "My Match",
        "educationImportance": "My Match",
        "maritalStatusImportance": "My Match",
        "religionImportance": "My Match",
        "churchActivityImportance": "My Match",
        "smokeImportance": "My Match",
        "drinkImportance": "My Match"

       
    }

    var mappingOwnWordsIndex = {
        "about_me": 1,
        "perfect_first_date": 2,
        "ideal_relationship": 3,
        "past_relationships": 4,
        "personality": 5,
        "i_enjoy": 6,
        "i_like_to_go_to": 7,
        "favorite_physical_activities": 8,
        "own_these_pets": 9,
        "favorite_foods": 10,
        "favorite_music": 11,
        "like_to_read": 12
    }

    var pageNameLookup = {
        "basic_info": "m_edit_basic_info",
        "my_details": "m_edit_my_details",
        "my_own_words": "m_edit_my_own_words"
    };
  

    function addEditField(e, hasChanged) {     
        var id = e.attr("id");

        if ($.inArray(id, editedFields) === -1 && hasChanged) {
            editedFields.push(id);

            if (e.attr("data-review"))
                reviewFields.push(id);

        } else if (!hasChanged) {
            editedFields = $.grep(editedFields, function (value) {
                return value != id;
            });

            reviewFields = $.grep(reviewFields, function (value) {
                return value != id;
            });
        }
    }

    function getAttributeOptionName(key) {
        
        if (key in attributeOptionsMap)
            return attributeOptionsMap[key];
       

        return "";
    }

    function getMapping(key) {
        return jsonMapping[key];
    }

    function getTabSession(key) {
        return tabMapping[key];
    }

    function getEditIndex(key) {
        return mappingOwnWordsIndex[key];
    }

    function getMultiselectValues(key) {
        
        var o = $("#" + key + "_text");
        var arr = (o.attr("data-value")) ? o.attr("data-value").split(",") : [];
        var values = [];
        $.each(arr, function(index, value) {
            values.push($.trim(value));
            });
      
        return values;
    }

    function hideLoading() {
        $.mobile.loading("hide");
    }


    function cancelProfileChanges(form) {

        // Proces textarea field(s).
        var textbox = form.find("input[type='text']");
        $(textbox).each(function (index, element) {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());
            if (hasChanged) {

                var input = $(this);
                input.val(input.data("original-value"));

                editedFields = $.grep(editedFields, function (value) {
                    return value != id;
                });

                reviewFields = $.grep(reviewFields, function (value) {
                    return value != id;
                });
            }

        }); 

        // Proces textarea field(s)
        var textarea = form.find("textarea");
        $(textarea).each(function (index, element) {
            var id = $(this).attr("id");

            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());
            if (hasChanged) {

                var input = $(this);
                input.val(input.data("original-value"));

               // remove from editFields array
                editedFields = $.grep(editedFields, function (value) {
                    return value != id;
                });

                reviewFields = $.grep(reviewFields, function (value) {
                    return value != id;
                });
            }
        }); 


        var customInputs = form.find("div[date-api]");
        $(customInputs).each(function (index, element) {
            var o = $(this);
            var id = o.attr("id");
            var apivalue = new Date(o.attr("date-api"));
            var inputValue = (o.find("input#bday").val());
            var convertToDate = new Date(inputValue);
            var value = convertToDate.toISOString();
            var hasChanged = (value != apivalue);

            if (hasChanged) {
                //revert back to original birth date 
                var input = $("input#bday");
                //input.val(input.data("original-value"));
                //input.val(input.data("value"));
                editedFields = $.grep(editedFields, function (value) {
                    return value != id;
                });
            }

        });

        var widgets = form.find("div[data-type='multiselect']");
        $(widgets).each(function (index, element) {
            var o = $(this);
            var id = o.attr("id");
            var values = getMultiselectValues(id);
           
            o.multiselect("createOptions", getAttributeOptionName(id), values);

            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());
            if (hasChanged) {
                var input = $(this);
                input.val(input.data("value"));

                editedFields = $.grep(editedFields, function (value) {
                    return value != id;
                });
            }
        }); 

        // Process custom dropdown(s).
        var dropdowns = form.find("div.dd-custom");
        dropdowns.each(function () {
            var o = $(this);
            var id = o.attr("id");
            var select = o.find("select");

            var hasChanged = ($.trim($("#" + id + "_text").attr("data-value")) != $.trim($(this).text()));
            if (hasChanged) {

                editedFields = $.grep(editedFields, function (value) {
                    return value != id;
                });
            }

        });

    } // ends cancelProfileChangesForm


    function processControls(form) {
        // Proces textarea field(s).
        var textbox = form.find("input[type='text']");
        textbox.keyup(function () {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());
            addEditField($(this), hasChanged);
        });

        textbox.on("paste", function() {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());
            addEditField($(this), hasChanged);
        });

        // Proces textarea field(s).
        var textarea = form.find("textarea");
        textarea.keyup(function () {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());
            addEditField($(this), hasChanged);
        });

        textarea.on("paste", function() {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());

            addEditField($(this), hasChanged);
        });


        //Process custom birthdate selection input  
        var customInputs = form.find("div[date-api]");
        customInputs.on("change", function () {
            var o = $(this);
            var id = o.attr("id");
           
            var apivalue = new Date (o.attr("date-api"));
             // var value = new Date(o.text());
            var inputValue = (o.find("input#bday").val());
            var convertToDate = new Date(inputValue);
            $("input#bday").attr("value",(inputValue));
            if ($("input#bday").val() === "") {
                spark.utils.common.showGenericDialog("Date of birth cannot be blank.");

                var input = $("input#bday");
                input.val(input.data("original-value"));
            }

            //var value = convertToDate.toISOString();
            var value = inputValue;
            var hasChanged = (value != apivalue);
            addEditField(o, hasChanged);
    }); 


        // Process multi-select widget(s).
        var widgets = form.find("div[data-type='multiselect']");
        widgets.each(function () {
            var o = $(this);
            var id = o.attr("id");
            
            var values = getMultiselectValues(id);
            o.multiselect("createOptions", getAttributeOptionName(id), values);
            o.multiselect("setOnChangeListener", function (hasChanged) {
                addEditField(o, hasChanged);
            });
        });

        // Process custom dropdown(s).
        var dropdowns = form.find("div.dd-custom");
        dropdowns.each(function () {
            var o = $(this);
            var id = o.attr("id");
            var select = o.find("select");
            // Add onChange listener to dropdown.
            select.change(function () {
                var self = $(this);

                self.find("option:selected").each(function () {
                    var hasChanged = false;
                    var values = [];

                    hasChanged = ($.trim($("#" + id + "_text").attr("data-value")) != $.trim($(this).text()));
                    addEditField(o, hasChanged);
                });
            });
            // we have options do not need to make an API call if options are already there
            if (select.find("option").length !== 0) {
                setSelected(select);

                return;
            }

            //we don't have options yet, lets call the mapping to find out what options to ask the api for
            var attrName = getAttributeOptionName(id);

            //no attribute mapping, use data in page and break
            if (!attrName || attrName.length === 0) {
                setSelected(o.find("select"));

                return;
            }
           
            // Get options for dropdown.
            spark.api.client.callAPI(spark.api.client.urls.attributeOptions, { attributeName: attrName }, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

          
                if (response.data != null) {
                    $.each(response.data, function (key, value) {
                        if (value["Value"] > -1) {
                            var option = $("<option />");

                            option.attr("value", value["Value"]);
                            option.text(value["Description"]);
                            option.appendTo(select);
                        }
                    });

                }

                setSelected(select);
            });

        });
    }

    function saveProfile() {
        // Check if there are fields to save. If none, just return.
        if (editedFields.length === 0) {
            return;
        }
            

        var attributes = {};
        var attributesMulti = {};
        var errorMessage = "";


        $.each(editedFields, function (index, item) {
            var e = $("#" + item);
            var type = e.attr("data-type");
            var values = [];
            var id = e.attr("id");


            if (type === "multiselect") {
                values = e.multiselect("getValues");
                for (var i = 0; i < values.length; i++) {
                    values[i] = +values[i];
                }

                attributesMulti[getMapping(item)] = values;

            } else if (e.hasClass("dd-custom") && id !== "body_style") {
                var value = Number(e.find("select").val());

                if (id === "birthdate") {
                    //convert back to Date('yyyy-MM-ddTHH:mm:ss.fffZ') format
                    var inputValue = ($("input#bday").val());
                    var convertToDate = new Date(inputValue);                   
                    var newBirthDate = convertToDate.toISOString();
                    //value = newBirthDate;
                    
                    // var res = inputValue.replace("-", ",");
                    value = inputValue.split('-').map(Number);
                    $("input#bday").attr("value", inputValue);

                }

                if (id === "min_age_range") {
                    var maxAge = $("#max_age_range").find("select");
                    if (value > maxAge.val() && errorMessage === "") {
                        errorMessage = "Minimum age cannot be greater than maximum. Please select valid age value.";
                    }
                } else if (id === "max_age_range") {
                    var minAge = $("#min_age_range").find("select");
                    if (value < minAge.val() && errorMessage === "") {
                        errorMessage = "Maximun age cannot be less than minimum. Please select valid age value.";
                    }
                } else if (id === "marital_status") {
                    var marital = $("#marital_status").find("select");
                    if (marital.val() === "-2147483647") {
                        errorMessage = "Marital Status cannot be left blank.";
                    }
                } 
 
                attributes[getMapping(item)] = value;
            } else if (e.attr("id") === "body_style") {
                values.push(Number(e.find("select").val()));
                attributesMulti[getMapping(item)] = values;
            } else {
                attributes[getMapping(item)] = e.val();
            }
        });

        // Chek if there are any error(s) before saving. If so, show message
        // and abort saving until user fix issue.
        if (errorMessage.length > 0) {
            spark.utils.common.showGenericDialog(errorMessage);
            return;
        }

        //attributesMulti["smoke"] = [0, 1];
       

        var params = {
            attributesJson: JSON.stringify(attributes),
            attributesMultiValueJson: JSON.stringify(attributesMulti)
            //attributesMultiValueJson : JSON.stringify(smoke)
        };

        $.each(editedFields, function (index, item) {
            var e = $("#" + item);
            var id = e.attr("id");

            if (e.hasClass("myMatch")) {
                var myMatchParams = {};
                if (e.hasClass("multiselect")) {
                    SaveMyIdealMatch(attributesMulti);
                } else {
                    SaveMyIdealMatch(attributes);
                }
            }
          else  if (id === "user_name") {
                //use the update username method instead of updateProfile
                var username = $("input#user_name").val();
                saveUserName(username);
            } else {
                //use updateProfile for all other attributes
              spark.api.client.callAPI(spark.api.client.urls.updateProfile, null, params, function (success, response) {
                showLoading("Saving...");

            if (!success) {
              
                hideLoading();

                var error = response.error;
                var message = (error) ? error.message : "We're unable to save your profile.";
               
                var s = message;
                if (s.indexOf("Invalid value for Birthdate") > -1) {
                    //message === " Invalid value for Birthdate.  Must be between ages of 18 and 99.";
                    showErrorDialog("Invalid value for Birthdate.  Must be between ages of 18 and 99.");
                } else {
                    showErrorDialog(message);
                }

                return;
            }

            updateDetail();
            hideLoading();
            showReviewDialog(function () {
                showDetail();
                editedFields = [];
            });

            $("div#birthdate").removeClass("selected");

             }); /// ends updateprofile api call

            }
        });

    }

    function setSelected(target) {

        //looks for an option matching the data-value of the id, if found, selects that option
        var id = target.parent().attr("id");
        var temp = id;
        if (id === "min_age_range" || id === "max_age_range") {
            temp = "age_range";
        }

        if (id === "wants_min_height" || id === "wants_max_height") {
            temp = "wants_height";
        }

        var o = $("#" + temp + "_text");
        var value = o.attr("data-value");

        if (!value || $.trim(value.length) === 0)
            return;

        if (id === "min_age_range")
            value = value.split(",")[0];
        if (id === "max_age_range")
            value = value.split(",")[1];


        if (id === "wants_min_height")
            value = value.split(",")[0];

        if (id === "wants_max_height")
            value = value.split(",")[1];

        var option = target.find("option").filter(function () {
            var found = ($(this).text() == "" + value);
            if (!found) {
                found = ($(this).val() == value);
            }
            return found;
        });

        if (option && option.length !== 0)
            option.prop("selected", true);
    }

    function SaveMyIdealMatch(params) {
        //put_preferences
        spark.api.client.callAPI(spark.api.client.urls.put_preferences, null, params, function (success, response) {
            showLoading("Saving...");
            if (!success) {

                hideLoading();

                var error = response.error;
                var message = (error) ? error.message : "We're unable to save your profile.";
                showErrorDialog(message);

                return;
            }

            updateDetail();
            hideLoading();
            showReviewDialog(function () {
                showDetail();
                editedFields = [];
            });
        })
    }

    function showDetail() {
 
        var target = $("div.readonly");
        var temp = $("div.editable");

        $("div.ui-body-c").removeClass("profile-edit-bg");

        $("div#birthdate").removeClass("selected");

        temp.hide();
        target.fadeIn();
        $("#footer").show();

        cancelProfileChanges(temp);
    }

    function showEdit(name, callback) {
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        //window.scrollTo(0, 0);
        showForm(name);
        var target = $("div.editable");
        var temp = $("div.readonly");

        spark.utils.common.hideFooter();
        temp.hide();
        target.fadeIn();

        if (name === "physical_info_form") {
            $("textarea#occupation").val($("p#occupation_text").text());
            $("textarea#occupation").text($("p#occupation_text").text());
        }

       else if (name == "background_form") {
            var textArray = ["self_description", "favorite_musicians", "favorite_movies", "favorite_tv", "great_trip", "favorite_restaurants", "schools_attended"]
            for (var i = 0; i < textArray.length; i++) {
                if ($('textarea#' + textArray[i] + '').val() === "") {
                    var p = $('p#' + textArray[i] + '_text');
                    if (p === "") {
                        $('textarea#' + textArray[i] + '').val($('textarea#' + textArray[i] + '').text());
                        $('textarea#' + textArray[i] + '').text($('textarea#' + textArray[i] + '').text());
                    } else {
                        $('textarea#' + textArray[i] + '').val(p.text());
                        $('textarea#' + textArray[i] + '').text(p.text());
                    }
                   
                }
            }
        }
        
    }

    function showForm(name) {
        $("div.ui-body-c").addClass("profile-edit-bg");
        
        var form;

        $(".profile-edit-form, .profile-manage-photos").each(function () {
            var temp = $(this);
           
            if (temp.attr("id") !== name) {
                temp.addClass("hidden");
            } else {
                if (name !== "manage_photos") {
                    processControls(temp);
                }

                temp.removeClass("hidden");
                form = temp;
            }

        });
        return form;
    }

    function showErrorDialog(message) {
        spark.utils.common.showGenericDialog(message);

    }

    function showLoading(msg, options) {
        var defaultOptions = {
            text: msg,
            textVisible: true
        };

        $.extend(defaultOptions, options);
        $.mobile.loading("show", defaultOptions);
    }

    function showReviewDialog(callback)
    {
        if (reviewFields.length !== 0) {
            reviewFields = [];
            spark.utils.common.showGenericDialog("Your new essay is being approved by Customer Care and will be posted shortly.", callback);

        } else {
            callback();
        }
    }

    function showUsernameReviewDialog(callback)
    {
        if (reviewFields.length !== 0) {
            reviewFields = [];
            spark.utils.common.showGenericDialog("Your new Username is being approved by Customer Care and will be posted shortly.", callback);

        } else {
            callback();
        }
    }

    function updateDetail() {
        $.each(editedFields, function (index, item) {
            var e = $("#" + item);
            var o = $("#" + item + "_text");
            var type = e.attr("data-type");

            if (type === "multiselect") {
                var values = e.multiselect("getTextValues");

                e.multiselect("setValues", values);
                o.attr("data-value", values.join(","));
                o.text(values.join(", "));
            } else if (item === "min_age_range" || item === "max_age_range") {
                var temp = [];
                temp.push($("#min_age_range").find("select").val());
                temp.push($("#max_age_range").find("select").val());

                o = $("#age_range_text");
                o.attr("data-value", temp.join(","));
                o.text(temp[0] + " to " + temp[1]);
            } else if (e.hasClass("dd-custom")) {
                o.attr("data-value", e.find("select").val());
                o.text(e.find("select option:selected").text());
                if (o.attr("id") === "gender_text") {
                    var gender = $("div#gender option:selected").text();
                    gender = gender === "Female" ? "Woman" : "Man"; 

                    var maritalStatus = $("div#marital_status option:selected").text();
                    if (maritalStatus === "Single - never been married") {
                        $("h4#marital_status_text").text("");
                        $("h4#marital_status_text").text("Single" + " " + gender);
                    } else {
                        $("h4#marital_status_text").text("");
                        $("h4#marital_status_text").text(maritalStatus + " " + gender);
                    }    

                }
                if (o.attr("id") === "marital_status_text") {
                    // full string 
                    var fullString = $("h4#marital_status_text").text();
                    // get first word (Status)
                    var currentStatus = $("h4#marital_status_text").text();
                    
                    if (currentStatus == "Single - never been married") {
                        currentStatus = currentStatus.substring(0, currentStatus.indexOf('-'));
                    } else {
                        currentStatus = currentStatus.split(" ")[0];
                    }
                    
                    //updated status
                    var updatedStatus = $("div#marital_status.dd-custom :selected").text();
                    if (updatedStatus == "Single - never been married") {
                        updatedStatus = updatedStatus.substring(0, updatedStatus.indexOf('-'));
                    }
                    //now find and replace
                    if ($("h4#marital_status_text").attr('data-gender') === "Female") {
                        //o.text(fullString.replace(currentStatus, updatedStatus) + " " +  "Woman");
                        o.text(updatedStatus + " " + "Woman");
                    } else {
                        //o.text(fullString.replace(currentStatus, updatedStatus) + " " + "Man");
                        o.text(updatedStatus + " " + "Man");
                    }
                   
                } 
                if (o.attr("id") === "birthdate_text") {
                    // get current age by selecting the first two characters in the string 
                    var ageString = $("h4#edit_birthdate_text").text().substring(0, 2);
                    // get the text from the update date string
                    var inputValue = ($("input#bday").val());
                    var newDateString = new Date(inputValue);
                    //convert it into a date object 
                    var convertNewDate = newDateString.toISOString();
                    // get the new age by using getAge function & now replace the old age with the new age
                    o.text($("h4#edit_birthdate_text").text().replace(ageString, getAge(convertNewDate)) + "   " + "  years old");
                }

                if (item === "wants_min_height" || item === "wants_max_height") {
                    
                    var temp = [];
                    temp.push($("#wants_min_height").find("select").val());
                    temp.push($("#wants_max_height").find("select").val());

                    o = $("#wants_height_text");
                    o.attr("data-value", temp.join(","));
                    var minHeightText = $("select.minHeight option:selected").text();
                    var maxHeightText = $("select.maxHeight option:selected").text();
                    o.text('Between ' + minHeightText + ' and ' + maxHeightText);

                }

            } else {
                o.text(e.val());
                var id = o.attr('id');
            }

        });
    }


    function updateUsername() {
            if ($("fieldset#user_name_form.profile-edit-form")) {
                var username = $("input#user_name").val();
                 saveUserName(username);
            }
    }
    

    function saveUserName(username) {
        var memberId = $("h2#user_name_text.page-header.profile-page-header").attr('data-memberid');

       var params = {
            UserName: username,
            MemberId: memberId,           
       }
      

        spark.api.client.callAPI(spark.api.client.urls.updateUsername, null, params, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            } else {
                updateDetail();
                hideLoading();
                //showDetail();

                //spark.utils.common.showGenericDialog("Your username is pending approval");
                var h2 = $("h2#user_name_text.page-header.profile-page-header");
                var a = $("<a class='btn-edit' data-role='none' />");
                // $("h2#user_name_text").text($("input#user_name").val());
                var span = $("<span class='btn-edit' style='float:right; margin:10px -16px 0px 0px'/>");
                 $("h2#user_name_text").attr("data-value", ($("input#user_name").val()));
                //a.prependTo(h2).text();

                 span.appendTo(h2).text();
                 showUsernameReviewDialog(function () {
                     showDetail();
                     editedFields = [];
                 });

                 $("a[href^=tel]").attr('data-role', 'none');

                 $("span.username > a.ui-link").on("click", function (e) {
                     e.preventDefault();
                 });

                 $(".btn-edit").on("click", function (e) {
                     var name = $(this).parent().attr("data-form");
                     var buttonContainer = $("#profile_edit_buttons");
                     buttonContainer.show();
                     showEdit(name);
                 }); 

            }
          
        }); 

    }


    function getMyFullProfile() {
        var memberId = $("h2#user_name_text.page-header.profile-page-header").attr('data-memberid');
        spark.api.client.callAPI(spark.api.client.urls.fullProfile, { targetMemberId: memberId }, null, function(success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            var date = new Date(response.data.birthdate);
            var birthdate = ((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear());

            $("div#edit_birthdate.dd-custom").text(birthdate);
            $("div#edit_birthdate.dd-custom").attr('data-value', response.data.birthdate);
            $("div#edit_birthdate.dd-custom").attr('date-api', response.data.birthdate);

            var div = $("div#birthdate.dd-custom");
            var input = $("<input type='date' name='bday' id='bday' data-role='none' class=' ui-input-text ui-body-c' style='border-radius:0px;margin-top:-8px' data-original-value=\"" + formatDate(birthdate) + "\"  value=\"" + formatDate(birthdate) + "\"  >");

            input.appendTo(div);
            $("div#birthdate").attr('data-value', response.data.birthdate);
            $("div#birthdate").attr('date-api', response.data.birthdate);


            var name = $("<span class=\"username\"/>");
            name.text(($("h2#user_name_text").text()));


        });

    }


    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    function getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    $(document).on("tap", "input#bday", function () {
        $("div#birthdate").addClass("selected");

    });

    function getMyMatchData() {

    }

    self.init = function () {
        spark.views.myprofile.addTabEvents();

        $("a[href='#in_my_own_words']").click();

        $(".btn-edit").on("click", function (e) {
            var name = $(this).parent().attr("data-form");
            var buttonContainer = $("#profile_edit_buttons");

            buttonContainer.show();
            showEdit(name);

        });
        $("#btn_edit_cancel").on("click", function (e) {
            var input = $("input#bday");
            input.val(input.data("original-value"));

            showDetail();
        });

        //$("#btn_edit_cancel").on("click", showDetail);
        $("#btn_edit_save").on("click", saveProfile);
   
        
        getMyFullProfile();
        $("div.date-container").hide();

        //if ($("div").attr("data-type") == "multiselect") {
        //    if($("div.multiselect"))
        //}
    };


    self.showPhotoViewer = function () {
        $("#photo_viewer").photoviewer("show");
    };

    self = $.extend(new spark.views.baseprofile(), self);

    return self;
})();


$(document).on("pageshow", "#myprofile_page", function () {
    spark.views.myprofile.init();
    $('input').slider();
});

$(document).on('tap', '#myprofile_page #edit-photo a', function (e) {
    var a = $(this);
});