﻿spark.views.photos = (function () {
    var self = {};
    var photoList = [];

    function backButton() {
        history.back(-1);
    }

    function bindEvents(ul) {
        ul.off("click").on("click", function (e) {
            var target = $(e.target);
            var li;

            if (target.is("img")) {
                showPhotoViewer(target.attr("data-index"));
            } else if (target.is("p") && target.hasClass("use-main")) {
                li = target.parent();

                flagPhotoAsMain(li.attr("data-photoid"));
            } else if (target.is("button")) {
                li = target.parent();
                deletePhoto(li.attr("data-photoid"));
            } else if (target.is("span")) {
                showCaption(target);
            }
        });
    };

    function deletePhoto(photoId) {
        spark.utils.common.showConfirm("Are you sure you want to delete this photo? ", function () {
            spark.utils.common.showLoading("Deleting...");

            spark.api.client.callAPI(spark.api.client.urls.deletePhoto, { photoId: photoId }, null, function (success, response) {
                spark.utils.common.hideLoading();

                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                showPhotos(function () {
                    spark.utils.common.showMessageBar("Your photo has been deleted", true);
                });

            }, null);
        });
    };

    function flagPhotoAsMain(id) {

        spark.api.client.callAPI(spark.api.client.urls.makeDefaultPhoto,{ photoId: id },null, function (success, response) {
            if (!success) {
                var error = response.error;
                if (error.message.length > 0) {
                    spark.utils.common.showGenericDialog(error.message);
                }

                return;
            }         
            showPhotos();
        });

    };

    function getPhotos(callback) {
        var ul = $("#photos_list");
        ul.find("li:gt(0)").remove();

        photoList = [];
        spark.api.client.callAPI(spark.api.client.urls.photos, { targetMemberId: spark.utils.common.getMemberId() }, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            ul.find("li:gt(0)").remove();
            bindEvents(ul);

            var photos = response.data;
            var length = photos.length;

            $.each(photos, function (i, item) {
                var li = $("<li />");
                var img = $("<img />");
                var temp = $("<p>Pending<br/>Approval</p>");
                var useMain = $("<p class=\"use-main\">Use As<br/>Main</p>");
                var isMain = $("<p class=\"main\">Main<br/>Photo</p>");
                var caption = $("<span />");
                var deleteButton = $("<button class=\"delete\"></button>");


                img.attr("data-index", i);
                img.prop("src", item.fullPath);
                img.appendTo(li);
                if (!item.isPhotoApproved) {
                    li.addClass("unapproved");
                    temp.appendTo(li);
                } else if (item.isApprovedForMain && !item.IsMain) {
                    li.addClass("usemain");
                    useMain.appendTo(li);
                } else if (item.isApprovedForMain && item.IsMain) {
                    li.addClass("currentmain");
                    isMain.appendTo(li);
                }


                deleteButton.addClass("trash");
                deleteButton.appendTo(li);



                var defaultCaption = "{ Tap here to edit caption }";
                caption.addClass("caption");
                caption.text(item.caption || defaultCaption);
                caption.appendTo(li);


                li.attr("data-photoid", item.fullId);
                li.appendTo(ul);

                if (i === (length - 1)) {
                    spark.utils.views.removeItemBorder(ul);
                }

                // Store the photos in a list so we can re-use the same data on
                // the PhotoViewer widget.
                photoList.push({
                    imagePath: item.fullPath,
                    caption: item.caption,
                    thumbnail: item.thumbPath
                });
            });

            // Update the uploaded count.
            var addPhotoBar = ul.find("li:first-child");
            addPhotoBar.find("div").removeClass("disabled");
            addPhotoBar.find("h5:first-child").text("Add new photo");
            addPhotoBar.find("input[type='file']").prop("disabled", false);

            var photoUploadCount = addPhotoBar.find("div h5:eq(1)");
            photoUploadCount.text(photos.length + " of 8 uploaded")

            // Check if user exceeded the maximum upload allowed.
            if (photos.length >= 8) {
                addPhotoBar.find("div").addClass("disabled");
                addPhotoBar.find("h5:first-child").text("8 of 8 photos uploaded.");
                addPhotoBar.find("h5:eq(1)").text("Delete some to add more.");
                addPhotoBar.find("input[type='file']").prop("disabled", true);
            }

            //spark.utils.common.hideLoading();
            if (photos.length === 0)
                ul.append("<li class=\"no-results\"><p>No results found.</p></li>");

            callback();
        });
    }

    function showPreview() {
        var maxWidth = $(window).outerWidth();
        $("#photo_preview_panel").show();
        $("#photo_list_panel").hide();
        spark.utils.common.hideFooter();

        $("#photo_canvas img").css({
            maxWidth: maxWidth,
            height: "auto"
        });
    }



    function encodeImageFileAsURL(input) {
        if (input.files && input.files[0]) {
            var FR = new FileReader();
            var fileSizeLimit = 5000000;
            var supportedFileType = /\.(gif|jpg|jpeg|tiff|png)$/i;
            var regex = new RegExp(supportedFileType);
            var preview = $("#photo_preview_panel");
            var uploadButton = preview.find("#btn_upload_photo");
            var cancelButton = preview.find("#btn_cancel_upload");
            var srcData;
            var newImage;
            var imgData;
            var filename = input.files[0].name;
            var dataToSend = {};

            
            if (input.files[0].size > fileSizeLimit) {
                spark.utils.common.showGenericDialog("File size too large. Image max size is 5 MB.");
                return;
            }

            if (regex.test(filename) === false) {
                spark.utils.common.showGenericDialog("Filetype is not supported.");
                return;
            }
           

            FR.onload = function (fileLoadedEvent) {
                srcData = fileLoadedEvent.target.result; // <--- data: base64
                newImage = document.createElement('img');

                newImage.src = srcData;
                document.getElementById("photo_canvas").innerHTML = newImage.outerHTML;
                srcData = srcData.replace('data:image/jpeg;base64,', '');
               
                showPreview();

                $(uploadButton).click(function () {
                    imgData = srcData;

                    dataToSend.photoFile = imgData;
                    dataToSend.fileName = filename;
                    dataToSend.caption = "";
                    spark.utils.common.showLoading();

                    spark.api.client.callAPI(spark.api.client.urls.upload_photo, null, dataToSend, function(success, response) {
                        if (!success) {
                            spark.utils.common.hideLoading();
                            var message = "Unable to upload image.";
                            spark.utils.common.showGenericDialog(message, function() {
                                showPhotos(function () {
                                    $("#photo_canvas").empty();
                                    $("#photo_preview_panel").hide();
                                    $("#photo_list_panel").fadeIn();
                                });
                            });
                        } else {
                            window.setTimeout(function() {
                                spark.utils.common.hideLoading();
                                showPhotos(function () {
                                    $("#photo_canvas").empty();
                                    $("#photo_preview_panel").hide();
                                    $("#photo_list_panel").fadeIn();
                                });
                            }, 1500);
                        }
                    });

                });

                $(cancelButton).click(function() {
                    showPhotos(function () {
                        $("#photo_canvas").empty();
                        $("#photo_preview_panel").hide();
                        $("#photo_list_panel").fadeIn();
                    });
                });
            };
            FR.readAsDataURL(input.files[0]);
        }
    }

    function showCaption(target) {
        var listPanel = $("#photo_list_panel");
        var panel = $("#photo_caption_panel");
        var cancelButton = $("#btn_cancel_caption");
        var saveButton = $("#btn_save_caption");
        var textarea = $("#caption");
        var caption = target.text().indexOf("Tap here to edit caption") === -1 ? target.text() : "";
        var textCounter = $(".char-counter");
        var count = 0;
        var temp = "20 characters left";

        listPanel.hide();

        textarea.val(caption);
        textCounter.text(temp);

        if (caption.length > 0) {
            count = (20 - caption.length);
            temp = (count > 1) ? count + " characters left" : count + " character left";

            textCounter.text(temp);
        }

        function updateTextCounter(target) {
            var text = target.val();

            count = (20 - target.val().length);
            temp = (count > 1) ? count + " characters left" : count + " character left";

            textCounter.text(temp);
        }

        /*textarea.keyup(function (e) {
            updateTextCounter($(this));
        });*/

        textarea.on("input paste", function() {
            updateTextCounter($(this));
        });

        cancelButton.unbind("click").bind("click", function () {
            showPhotos(function () {
                panel.hide();
                listPanel.fadeIn();
            });
        });

        saveButton.unbind("click").one("click", function () {
            var photoId = target.parent().attr("data-photoid");
            updatePhotoCaption(photoId, textarea.val());
            // updatePhotoCaption(photoId);
        });

        panel.fadeIn().removeClass("hidden");
    }

    function showPhotos(callback) {
        //spark.utils.common.showLoading("Loading...");
        var ul = $("#photos_list");

        ul.find("li.no-results").remove();

        getPhotos(function () {
            if (typeof callback === "function")
                callback();
        });
    }

    function showPhotoViewer(index) {
        if (photoList.length === 0)
            return;

        var photoViewer = $("#photo_viewer");
        photoViewer.photoviewer("setBeforeHideListener", function () {
            $("div[data-role='content']").show();
        });

        photoViewer.photoviewer("setBeforeShowListener", function () {
            $("div[data-role='content']").hide();
        });

        photoViewer.photoviewer("show", photoList, index);
    }

    function updatePhotoCaption(id, caption) {
        spark.api.client.callAPI(spark.api.client.urls.updatePhotoCaption, { photoId: id }, { caption: caption }, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            window.setTimeout(function () {
                showPhotos(function() {
                    $("#photo_caption_panel").hide();
                    $("#photo_list_panel").fadeIn();
                });
            }, 500);
        });
    };

    self.init = function () {
        $("#photoFiles").change(function () {
            encodeImageFileAsURL(this);
        });
        photoList = [];

        var ul = $("#photos_list");
        ul.children("li:gt(0)").each(function () {
            var li = $(this);
            var img = li.find("img");

            photoList.push({
                imagePath: img.attr("data-fullpath"),
                caption: img.attr("alt"),
                thumbnail: img.attr("src")
            });
        });

        bindEvents(ul);
        showPhotos();
        $(document).on("tap", "a#btn_back.back-button", function() {
            backButton();
        });
    };

    return self;
})();

$(document).on("pagebeforeshow", "#photos_page", function (e, ui) {
    //if (window.location.href.indexOf('reload') === -1) {
    //    window.location.replace(window.location.href + '?reload');
    //}
});

$(document).on("pagehide", "#photos_page", function (e, ui) {
    $("#photo_canvas").empty();
});
$(document).on("pageshow", "#photos_page", function (e, ui) {
    var page = ui.prevPage;
    if (page.attr("id") === "myprofile_page") {
        $("#btn_back").removeClass("hidden");
    }

    spark.views.photos.init();
    //$(window).off("orientationchange").on("orientationchange", function (event) {
    //    var previewPanel = $("#photo_preview_panel");
    //});
});

