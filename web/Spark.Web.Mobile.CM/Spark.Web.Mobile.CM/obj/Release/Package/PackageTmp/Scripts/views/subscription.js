﻿spark.views.subscription = (function () {
    "use strict";

    var self = {};

    // Resize the images
    function resizeImage() {
        var screenWidth = $(window).width() - 20;
        var maxImageWidth = 107;
        var totalItemWidth = (maxImageWidth * 3);
        var imageWidth;
        var padding = 0;

        $("ul.generic-list-large").find("li img").css({
            backgroundColor: "#000"
        });

        if (totalItemWidth > screenWidth) {
            imageWidth = Math.floor(screenWidth / 3) - 1;
            $("ul.generic-list-large").find("li img").css({
                width: imageWidth,
                height: "auto"
            });

            totalItemWidth = ((imageWidth + 1) * 3);
        }

        padding = Math.floor((screenWidth - totalItemWidth) / 2);
        $("ul.generic-list-large").css({ paddingLeft: padding });

    };

    $(document).on('tap', "a.premium.Cancel", function (e) {
        e.preventDefault();

        if ($('div.page-content').hasClass("iap")) {
            //self.showIAPCancel();
            window.location = '/subscription/iap';
            return;
        }
        $("div#ConfirmCancel").removeClass("hide");
        $("div#subscription-data").addClass("hide");
        var addOnType = $(this).attr('data-addon');
        $("span#addontype").html(addOnType);
        var addOnID = $(this).attr('data-addon-id');
        //$('a.continue-cancel').attr('href', '/subscription/cancel/premiumoption/disable/' + addOnID + '/true/');
        $('a.continue-cancel').attr('data-addon-id', addOnID);

    });

    $(document).on('tap', "a.stopcancel", function (e) {
        e.preventDefault();
        $("div#self-suspend-option").addClass("hide");
        $("div#ConfirmCancel").addClass("hide");
        $("div#subscription-data").removeClass("hide");
        $("div.checkbox").removeClass("selected")
    });

    $(document).on('tap', '.continue-cancel.ui-link', function (e) {
        var addOnID = parseInt($(this).attr('data-addon-id'));

        var premiumSubscriptionType = parseInt(addOnID);
        console.log(addOnID, typeof (addOnID), premiumSubscriptionType, typeof (premiumSubscriptionType));

        spark.api.client.callAPI(spark.api.client.urls.UpdateSubscriptionPlan, null, { "PremiumOptions": [{ "PremiumSubscriptionType": premiumSubscriptionType, "IsEnabled": false }] }, function (success, response) {
            console.log(response.data);
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            else {
               // window.location = "/subscription/cancel";
                //spark.utils.common.showGenericDialog("Your subscription has been canceled");
                $("div#ConfirmCancel").addClass("hide");
                $("div#subscription-data").removeClass("hide");
            }
        });

    });


    $(document).on('tap', "a.cancel-base-sub", function (e) {
     
        e.preventDefault();
        if ($('div.page-content').hasClass("iap")) {
            window.location = '/subscription/iap';
            //$('a.cancel-base-sub').attr('href', '/subscription/iap');
            // self.showIAPCancel();
            return;
        }


        //spark.api.client.callAPI(spark.api.client.urls.fullProfile, { targetMemberId: 1127798303 }, null, function (success, response) {
        //    console.log("profile:", response.data)
        //    if (!success) {
        //        spark.utils.common.showError(response);
        //        return;
        //    }
        //}); 


        spark.api.client.callAPI(spark.api.client.urls.UpdateSubscriptionPlan, null, { "BaseSubscriptionPlan": { "IsEnabled": false }, "PremiumOptions": [] }, function (success, response) {
            console.log(response.data);
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            else {
                spark.utils.common.showGenericDialog("Your subscription has been canceled");
                $("div.base-sub-reactivate").removeClass("hide");
                $("div#subscription-data").addClass("hide");
                $("div.page-header > h2").text("Reactivate");
            }
        });

    });

    $(document).on('tap', "a.reactivate-sub-btn", function (e) {
        //console.log($(this).attr('data-cancel-reason'));
        e.preventDefault();

        spark.api.client.callAPI(spark.api.client.urls.UpdateSubscriptionPlan, null, { "BaseSubscriptionPlan": { "IsEnabled": true }, "PremiumOptions": [] }, function (success, response) {
            console.log(response.data);
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            else {
                $("div.base-sub-reactivate").addClass("hide");
                $("div#subscription-data").removeClass("hide");
                $("div.page-header > h2").text("Cancel Auto-Renew");
                window.location = "/subscription/cancel";
            }
        });
    });


    $(document).on('tap', "div#cancelReason ul.list-of-reasons li", function (e) {
        e.preventDefault();
        //hide x21
        $("div#cancelReason").addClass("hide");
        //$("div#ConfirmCancel").removeClass("hide");
        //show x22
        $("div#self-suspend-option").removeClass("hide");

        var cancelReason = $(this).attr('data-cancel-reason');
        $("span#addontype").html("my subscription");
        //hide x21 show x22;
        //$('a.continue-cancel').attr('href', '/subscription/cancel/basesubscriptionplan/disable/' + cancelReason);

        $('a.continue-cancel-auto-renewal').attr('data-cancel-reason', cancelReason);
    });

    $(document).on('tap', "div.checkbox", function (e) {
        e.preventDefault();
        $(this).toggleClass("selected");
    })

    //x22
    $(document).on('tap', "a.continue-cancel-auto-renewal", function (e) {
        e.preventDefault();
        //hide x22 
        $("div#self-suspend-option").addClass("hide");
        //show x25
        $("div#ConfirmCancel").removeClass("hide");
        //append URL with cancel reason ID + selfSuspend status
        var cancelReason = $(this).attr('data-cancel-reason');
        //$(this).attr('href', '/subscription/cancel/basesubscriptionplan/disable/' + cancelReason + '/true');
        if ($("div#remove.checkbox").hasClass("selected")) {
           // $('a.continue-cancel').attr('href', '/subscription/cancel/basesubscriptionplan/disable/' + cancelReason + '/true');
        } else {
           // $('a.continue-cancel').attr('href', '/subscription/cancel/basesubscriptionplan/disable/false');
        }

    });

    self.showIAPCancel = function () {
        $('div#subscription-data').addClass('hide');
        // $('div#iapCancel').removeClass('hide');
    };

    $(document).on("tap", "a.prevButton-btn.ui-link", function () {
        if (!($("div#subscription-data").hasClass("hide")) || !($("div.base-sub-reactivate").hasClass("hide"))) {
            $(this).attr("href", '/settings/account/');
        }
        //else if (!($("div.base-sub-reactivate").hasClass("hide"))){
        //    $("div.base-sub-reactivate").addClass("hide");
        //}

        else {
            $("div#subscription-data").removeClass("hide")
            $("div#cancelReason").addClass("hide");
            $("div#ConfirmCancel").addClass("hide");
            $("div.base-sub-reactivate").addClass("hide");
        }
        $("div.checkbox").removeClass("selected");
        $("div#self-suspend-option").addClass("hide");

    });

    function isBundle() {
        var i = $("div.premium-option-type").length;
        var p = $("<p class=\"prem-divider\"></span>");
        var bundle = $("div.premium-option-type")
        p.text("This will ALSO cancel your premium add-on features below.");
        if (i > 0 && (!($("div.premium-option-type")).hasClass("isBundle"))) {
            p.appendTo($("p.premium-cancel-warning "));
            //$("p.premium-cancel-warning ");
        }
    }

    function resizeCheckBox() {
        var iphone5 = navigator.userAgent.match(/iPhone/i) !== null && window.screen.height == (568);
        var iphone4 = navigator.userAgent.match(/iPhone/i) !== null && window.screen.height == (960 / 2);
        if (iphone4 || iphone5) {
            $("div#self-suspend-option ul li").css("font-size", "12.5px");
        }
    }

    //self-suspend

    // Public functions(s).
    self.init = function () {
        resizeImage();
        isBundle();
        resizeCheckBox();
    };

    self.resize = resizeImage;

    return self;


})();

$(document).on("pageshow", "#confirmation_page", function () {
    spark.views.subscription.init();

    if (screen.height < 600) {
        $("div#self-suspend-option ul li").css("font-size", "12px");
    }

    $(window).off("orientationchange").on("orientationchange", function (event) {
        spark.views.subscription.resize();
    });
})