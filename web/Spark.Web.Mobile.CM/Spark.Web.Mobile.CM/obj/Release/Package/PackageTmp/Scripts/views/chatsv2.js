﻿spark.views.chats = (function () {
    var self = {};
    var isLoading = false;
    var incomingLastPage = false;
    var outgoingLastPage = false;
    var firstLoad = true;
    var memberList = [];
    var pageInit = true;

    function appendToList(data) {
        var ul = $("ul.scrollable");
        // Remove loading footer.
        ul.find("li.loading").remove();

        if (data.length == 0) {
            var p = $("<p />");
            p.text("No results found!");
            p.addClass("no-results-text");
            p.appendTo(ul);
        }


        $.each(data, function (index, item) {
            var imageUrl = (item.miniProfile.gender === "Male") ? "/images/hd/img_silhouette_man@2x.png" : "/images/hd/img_silhouette_woman@2x.png";

            if (item.miniProfile.defaultPhoto != null) {
                if (item.miniProfile.defaultPhoto.thumbPath && item.miniProfile.defaultPhoto.fullPath !== "") {
                    imageUrl = item.miniProfile.defaultPhoto.fullPath;
                }
            }

            var isSub = (spark.utils.common.isSub()) ? "" : "false";
            var isSubHref = (spark.utils.common.isSub()) ? '/chats/history/' + item.miniProfile.id + '' : "/subscription/subscribe/4164";
            var memberHightlight = item.miniProfile.isHighlighted ? 'member-highlight' : '';

            var blockedMember = (item.miniProfile.blockContact ) ? "blockedContact" : "";
           
            // item.miniProfile.blockContactByTarget)

            var imButton;
            var online;
            var chatLinks;
           
            if (item.miniProfile.isOnline) {
                if (item.miniProfile.blockContact || item.miniProfile.blockContactByTarget) {
                    online = "online";
                } else if (item.miniProfile.selfSuspendedFlag || item.miniProfile.adminSuspendedFlag) {
                    online = "";
                } else {
                    online = "online";
                    chatLinks = 'target=  "_' + item.miniProfile.username + '" data-role="none"  data-im-params= "username=' + item.miniProfile.username + '&mobile=true" data-im-windowname="im_' + item.miniProfile.username + '"'
                    imButton = "im-button";
                    isSubHref = "#";
                }
            }

            var offline = !(item.miniProfile.isOnline) ? "offline" : "";


            $('<li data-memberid="' + item.miniProfile.id + '" category="' + item.category + '" data-gender="' + item.miniProfile.gender + '" data-name="' + item.miniProfile.username + '"  class="' + memberHightlight + ' ' + blockedMember + ' ' + offline + '"><a   href=' + isSubHref + ' ' + chatLinks + ' class="' + online + ' ' + imButton + '"><img src= "' + imageUrl + '"/><p class="' + memberHightlight + '"><abbr class="timeago">' + jQuery.timeago(item.actionDate) + '</abbr><span class="primary">' + item.miniProfile.username + '</span><span class="secondary ' + online + '">' + item.miniProfile.age + ", " + item.miniProfile.location + '</span></p></a></li>').appendTo(ul);


            memberList.push(item);

        });


        isLoading = false;
        pageInit = false;
    };




    //send chat invite popup
    $(document).on('tap', "a.im-button", function (e) {
        e.preventDefault();
        SparkPresence.open_im_window($(this).attr("data-im-params"), $(this).attr("data-im-windowname"));

    });


    $(document).on('tap', 'li.blockedContact', function () {
        var username = $(this).attr("data-name");
        var gender = ($(this).attr("data-gender") === "Male") ? "his" : "her";
        spark.utils.common.showGenericDialog('');
        $('#generic_dialog .content').html('<b>Sorry!</b><br/>You can\'t communicate with ' + username + ' because you\'ve blocked ' + gender + '.');
        $('#generic_dialog > p.content').css("margin-top", "-16px");
        $('#generic_dialog').addClass('high');
    });


    function arrayUnique(input) {
        var a = input.slice(); //why? changes length from 5 to 18 how?
        for (var i = 0; i < (a.length) ; i++) {
            for (var j = i + 1; j < a.length; j++) {
                if (a[i].miniProfile.id === a[j].miniProfile.id) {

                    // compare dates then remove older date
                    var idate = new Date(a[i].actionDate).getTime();
                    var jdate = new Date(a[j].actionDate).getTime();

                    if (idate > jdate) {
                        //keep the newest one :idate
                        a.splice(j--, 1);
                    } else {
                        //remove i 
                        a.splice(i, 1);
                        //update j value
                        j = i;
                    }
                }
            }
        }
        return a;

    };


    function mergeHotLists() {

        if (isLoading)
            return;
        isLoading = true;
        var incoming = [];
        var outgoing = [];
        var combineList = [];

        var page = $("#chat_page");
        var url = spark.api.client.urls.hotlist;
        var incomingPageNo = Number(page.attr("data-pageno"));
        var outgoingPageNo = Number(page.attr("data-pageno-two"));
        var pageSize = page.attr("data-pagesize");

        var incomingParams = {
            hotlist: "ims_received",
            pageSize: pageSize,
            pageNumber: incomingPageNo
        }

        var outgoingParams = {
            hotlist: "ims_sent",
            pageSize: pageSize,
            pageNumber: outgoingPageNo
        }

        var ul = $("ul.scrollable");
        spark.utils.views.addLoadingPanel(ul);

        var interval = ul.find("li").length !== 0 ? spark.utils.common.getLoadingTime() : 0;

        window.setTimeout(function () {
            spark.api.client.callAPI(url, incomingParams, null, function (success, response) {
                if (!success) {
                    isLoading = false;
                    spark.utils.common.showError(response);
                    $("ul.scrollable").find("li.loading").remove();
                    return;
                }
                page.attr("data-pageno", incomingPageNo);

                if (response.data.length < pageSize) {
                    incomingLastPage = true;
                }

                incoming = response.data;
                //appendToList(incoming);

                spark.api.client.callAPI(url, outgoingParams, null, function (success, response) {
                    if (!success) {
                        isLoading = false;
                        spark.utils.common.showError(response);
                        $("ul.scrollable").find("li.loading").remove();
                        return;
                    }

                    page.attr("data-pageno-two", outgoingPageNo);
                    if (response.data.length < pageSize) {
                        outgoingLastPage = true;
                    }

                    outgoing = response.data;
                    // appendToList(outgoing);

                    // Merges both arrays and gets unique items              
                    memberList = arrayUnique(incoming.concat(outgoing));
                    memberList.sort(function (a, b) {
                        var adate = new Date(a.actionDate).getTime();
                        var bdate = new Date(b.actionDate).getTime();
                        return bdate - adate;
                    });

                    // if both true show return ; else append to list 
                    if (incomingLastPage && outgoingLastPage && !firstLoad) {
                        $('.loading').hide();
                        return;
                    } else {
                        //build memberList; 
                        incomingPageNo++;
                        outgoingPageNo++;
                        page.attr("data-pageno", incomingPageNo);
                        page.attr("data-pageno-two", outgoingPageNo);
                        appendToList(memberList);
                        firstLoad = false;
                    }
                });
            });



        }, interval);

        isLoading = false;
        isLastPage = false;

    };



    self.firstLoad = firstLoad;


    self.init = function () {
        isLastPage = false;
        pageInit = true;

        var cachedData = $(document).data("cachedData");
        if (cachedData != null) {
            if (cachedData.cachedPage === "chats") {
                //spark.views.chats.resumePageStatus();
            }
        }
        else {
            mergeHotLists();
            pageInit = false;
        }

    };

    self.isOnline = function () {
        var li = $("ul.scrollable li");
        li.each(function () {
            var id = $(this).attr("data-memberid");

            spark.api.client.callAPI(spark.api.client.urls.miniprofile, { targetMemberId: id }, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }
                if (response.data.isOnline) {
                    $("li[data-memberid=" + id + "]").find("p span.online").show();
                }
            });

        });
    };


    self.storeMembers = function () {
        var members = [];
        var count = 1;
        $("#chats_list").children("li").each(function () {
            var li = $(this);
            li.attr("data-index", count++);
            members.push({
                memberId: li.find("a").attr("href").replace("/profile/", ""),
                thumbnail: li.find("a img").attr("src"),
                userName: li.find("a span.primary").text(),
                slideNo: li.attr("data-index")
            });
        });

        // Cache data so that it can be retrieve from the Profile page.
        $(document).data("members", members);

        $(document).data("cachedData", {
            cachedPage: "chats",
            pageNumber: $("#chat_page").attr("data-pageno"),
            lastPage: isLastPage,
            offsetY: $(document).scrollTop(),
            membersData: memberList,
        });

    }

    self.cleanCache = function () {
        $(document).removeData("members");
        $(document).removeData("cachedData");
    }

    self.resumePageStatus = function () {
        var cachedData = $(document).data("cachedData");

        $("#chat_page").attr("data-pageno", cachedData.pageNumber);
        isLastPage = cachedData.lastPage;
        isLoading = true;
        memberList = [];

        appendToList(cachedData.membersData);
        $(document).scrollTop(cachedData.offsetY);

        setTimeout(function () {
            self.cleanCache();
        }, 500);
    }

    self.reset = function () {
        memberList = [];
        pageInit = true;
        firstLoad = true;
    }

    return self;
})();
spark.views.chatHistory = (function () {
    // VARS
    // @page int the number of message list page we are on
    // @totalItems int the total number of messages
    // @isLastPage bool whether or not we can load more pages
    // @messages an array of messages
    // @targetMember int the MemberId of the user who's chat history you are viewing


    var totalItems;
    var isLastPage;
    var targetMember;
    var messageIds;
    var messages;

    var page;
    var firstLoad;
    var self = {};

    ////////////
    // PARAMS 
    // @pageNum an int representing which page to query the folder api for
    // Returns an array of message objects

    function getFolderMessageList(page) {
        var params = {
            folderId: -4,
            pageSize: pageSize,
            pageNumber: page,
            filter: "im"
        };

        spark.api.client.callAPI(spark.api.client.urls.messages, params, null, function (success, response) {
            if (success) {
                //set the new page # and update is last page
                //page = page++;
                totalItems = response.data.totalMessages;

                isLastPage = (page * pageSize) >= totalItems;

                if ((isLastPage) && ($("#see-more").length > 0)) {
                    $("li#see-more").remove();
                    spark.utils.common.hideLoading();
                }

                var messageList = response.data.messageList.filter(function (value) {
                    return (value.sender.id === targetMember || value.recipient.id === targetMember);
                });


                messageIds = messageList.map(function (obj) {
                    return obj.id;
                });

                if (messageIds.length !== 0) {
                    getMessages(messageIds);
                }
                else {
                    if (firstLoad) {
                        loadMoreMessages();
                    } else {
                        spark.utils.common.hideLoading();
                        return;
                    }
                }


            } else {
                spark.utils.common.showError(response);
                return;
            }
        });
    }

    //////////
    // params
    // @messageIds an array of message ids to query the api for
    // returns an array of message objects

    function getMessages(messageIds) {
        var totalResponses = 0;
        for (var i = 0; i < messageIds.length; i++) {

            spark.api.client.callAPI(spark.api.client.urls.message, { "messageId": messageIds[i], "folderId": "-4", "markAsRead": 1 }, null, function (success, response) {

                if (success) {
                    var ims = response.data.imMessages;
                    messages = messages.concat(ims);
                    totalResponses++;

                    if (totalResponses === messageIds.length) {
                        sortMessages(messages);
                        displayMessages(messages);
                        spark.utils.common.hideLoading();

                        $("#top").css({ 'height': $("#chats_history").height() - 450 });
                        scrollToBottom();
                    }
                }

            });

        };

    }

    function sortMessages(messages) {
        messages.sort(function (a, b) {
            var adate = new Date(a.sentTime);
            var bdate = new Date(b.sentTime);
            return bdate - adate;
        });
    }

    function displayMessages(data) {
        $.each(data, function (index, item) {

            var personIdentifier = (item.fromMemberId !== targetMember) ? "me" : "other";
            var bubble = (item.fromMemberId !== targetMember) ? "btm-right" : "btm-left";
            var imgPresence = (item.fromMemberId !== targetMember) ? "hidden" : "show";
            var imageUrl = $("div.profile-panel > a > img").attr('src');

            var date = new Date(item.sentTime);
            var imTimeStamp = ((date.getMonth() + 1) + '/' + date.getDate() + '/' + (date.getFullYear().toString()).substr(2, 2) + '  ' + '' + formatAMPM(date));

            $('<li class="chat_message ' + personIdentifier + '"><a href="#"><img class="' + imgPresence + '" src="' + imageUrl + '"/><p><span id="' + personIdentifier + '" class="imMessage talk-bubble tri-right round ' + bubble + '">' + spark.messageconversion.convertCustomTagToHtml(item.message) + '</span><span class="imTimeStamp">' + imTimeStamp + '</span></li>').prependTo('#chats_history');
            $("span.spr").text('');

        });

        if (data.length > 20) {
            $("#chats_history li").eq(19).prevAll().hide().addClass('toggleable');
            $('<li class="more">show more</li>').prependTo("#chats_history");
        } else {
            if (!isLastPage) {
                $('<li id="see-more">show more</li>').prependTo('#chats_history');
            }
        }

    }

    function displayMoreMessages() {
        if ($("#chats_history li.more").hasClass('less')) {
            $("#chats_history li.more").text('show more').removeClass('less');
            $("#chats_history li").removeClass('see-more');
            $("#see-more").remove();
        } else {
            $("#chats_history li.more").addClass('less');
            $("#chats_history li.more").text('show less').addClass('less');
            if (!isLastPage && $("#chats_history li.more").hasClass("less") && $("#see-more").length < 1) {
                $('<li id="see-more">show more</li>').prependTo('#chats_history');
            }
        }
        $("#chats_history li.more").siblings('li.toggleable').slideToggle();
    }


    function formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }


    function loadMoreMessages() {
        if (!isLastPage) {
            page = Number($("#chat_history_page").attr("data-pageno")) + 1;
            getFolderMessageList(page);
            spark.utils.common.showLoading();
        }
    }


    function addFavorite() {
        spark.api.client.callAPI(spark.api.client.urls.addToHotList, { "hotListCategory": "favorite", "targetMemberId": targetMember }, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            $('.fave').removeClass('fave-off').addClass('fave-on');

        });
    }

    function removeFavorite() {
        spark.api.client.callAPI(spark.api.client.urls.removeFromHotlist, { "hotListCategory": "favorite", "targetMemberId": targetMember }, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            $('.fave').removeClass('fave-on').addClass('fave-off')

        });
    }

    function blockUser() {
        var text = "Are you sure you want to block this member?";
        spark.utils.common.showConfirm(text, function () {
            spark.api.client.callAPI(spark.api.client.urls.addToHotList, { "hotListCategory": "block", "targetMemberId": targetMember }, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }
                $('.block').removeClass('block-off').addClass('block-on');
            });
        });
    }

    function unBlockUser() {
        spark.api.client.callAPI(spark.api.client.urls.removeFromHotlist, { "hotListCategory": "block", "targetMemberId": targetMember }, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            $('.block').removeClass('block-on').addClass('block-off')
        });

    }



    function bindClickEvents() {
        $(document).on('click', '#see-more', function () {
            loadMoreMessages();
        });

        $('#chats_history').on('click', '.more', function () {
            displayMoreMessages();
        });


        $(document).on('tap', '.fave.fave-off', function () {
            addFavorite();
        });

        $(document).on('tap', '.fave.fave-on', function () {
            removeFavorite();
        });

        $(document).on('tap', '.block.block-off', function () {
            blockUser();
        });

        $(document).on('tap', '.block.block-on', function () {
            unBlockUser();
        });


        $(document).on('tap', '.adminSuspended', function () {
            spark.utils.common.showGenericDialog('');
            $('#generic_dialog .content').html("<b>Profile Unavailable </b></br/>This profile has been removed from the site.Try browsing more profiles!");
            $('#generic_dialog').addClass('lift');
        });

        $(document).on('tap', '.selfSuspended', function () {
            var username = $(this).find("span.primary").text();
            var gender = ($(this).find("div.float-left").attr("data-gender") === "Male") ? "his" : "her";
            spark.utils.common.showGenericDialog('');
            $('#generic_dialog .content').html('<b>Sorry!</b><br/>'+username+' either became a Success Story or temporarily removed '+ gender +' account.');
            $('#generic_dialog').addClass('lift');

        });

        $(document).on("click", ".blockContactByTarget", function () {
            var username = $(this).find("span.primary").text();
            spark.utils.common.showGenericDialog('');
            $('#generic_dialog .content').html('<b>Sorry!</b><br/>' + username + ' is currently unavailable at this time');
            $('#generic_dialog').addClass('lift');
        });
    }


    function calculateLastPage(pageNum, pageSize, totalItems) {
        return (pageNum * pageSize) >= totalItems;
    }

    //// Create the profile panel at the top of the page
    function getProfilePanel(id) {
        spark.api.client.callAPI(spark.api.client.urls.miniprofile, { targetMemberId: id }, null, function (success, item) {
            if (!success) {
                profilePanelForSelfSuspendedUser();
            }
            createProfilePanel(item.data);
        });
    };

    function profilePanelForSelfSuspendedUser() {
        var incoming = [];
        var outgoing = [];
        var merge = [];
        spark.api.client.callAPI(spark.api.client.urls.hotlist, { hotlist: "ims_received", pageSize: 25, pageNumber: 1 }, null, function (success, response) {
            incoming = response.data;

            spark.api.client.callAPI(spark.api.client.urls.hotlist, { hotlist: "ims_sent", pageSize: 25, pageNumber: 1 }, null, function (success, response) {
                outgoing = response.data;
                merge = incoming.concat(outgoing);
                var mergeAgain = [];
                for (var i = 0; i < merge.length; i++) {
                    if (targetMember === merge[i].miniProfile.id) {
                        mergeAgain.push(merge[i].miniProfile);
                    }
                }
                createProfilePanel(mergeAgain[0]);
            });
        });
    }

    function createProfilePanel(data) {
        var blockContactByTarget = (data.blockContactByTarget) ? "blockedByTarget" : "";
        var favClass = (data.isViewersFavorite) ? "fave-on" : "fave-off";
        var blockClass = (data.blockContact) ? "block-on" : "block-off";
        var imageUrl = (data.gender === "Male") ? "/images/hd/img_silhouette_man@2x.png" : "/images/hd/img_silhouette_woman@2x.png";
        var seekingText = (data.gender === "Female") ? "Woman seeking a Man" : "Man seeking a Woman";

        var a = $("<a />");
        a.attr("href", "/profile/" + targetMember);
        a.attr("data-id", targetMember);

        if (data.selfSuspendedFlag) {
            a.addClass("selfSuspended");
            a.attr("href", "#");
        } else if (data.adminSuspendedFlag) {
            a.addClass("adminSuspended");
            a.attr("href", "#");
        } else if (data.blockContactByTarget) {
            a.addClass("blockContactByTarget");
            a.attr("href", "#");
        }


        if (data.primaryPhoto != null) {
            if (data.primaryPhoto.fullPath && data.primaryPhoto.thumbPath !== "") {
                imageUrl = data.primaryPhoto.fullPath;
            }
        }


        $('<img class="ui-grid-c float-left" src="' + imageUrl + '"/><div class="float-left" data-gender="' + data.gender + '"><span class="primary ui-grid-solo" >' + data.username + '</span><span class="secondary ui-grid-solo">' + seekingText + '</span><span class="location ui-grid-solo">' + data.location + '</span></div></a><a title="Favorite Member" class="' + favClass + ' float-right fave" id="favorite_symbol"></a><a class="float-right block ' + blockClass + '">').appendTo(a);
        a.appendTo('.profile-panel');
    }

    function scrollToBottom() {
        document.getElementById('bottom').scrollIntoView();
    };



    self.init = function () {

        messages = [];
        page = 1;
        pageSize = 25;
        targetMember = parseInt($('.target-id').val());

        getProfilePanel(targetMember);
        getFolderMessageList(page);
        spark.utils.common.showLoading();
        bindClickEvents();
        firstLoad = true;
    }
    return self;
})();
$(document).on("pagebeforehide", "#chat_page", function (e, ui) {
    //check if the next page is the profile page
    if (ui.nextPage.attr('id') == 'chat_page') {
        spark.views.chats.cleanCache();
        spark.views.chats.storeMembers();
    }
    else {
        spark.views.chats.cleanCache();
    }
});

$(document).on("pagebeforeshow", "#chat_page", function (e, ui) {
    if (navigator.userAgent.match(/(iPad|iPhone|iPod touch);.*CPU.*OS 7_\d/i)) {
        if (window.location.href.indexOf('reload') == -1) {
            window.location.replace(window.location.href + '?reload');
        }
    }

});

$(document).on("pageshow", "#chat_page", function (e, ui) {
    spark.views.chats.init();
});

$(document).on("pagehide", "#chat_page", function () {
    spark.views.chats.reset();

});

function viewProfile(url) {
    // $.mobile.changePage(url);
};
$(document).on("pageshow", "#chat_history_page", function (e, ui) {
    spark.views.chatHistory.init();
   
});