﻿spark.omniture = (function () {
    var self = {};
    var global = {};

    // Private function(s)
    /* 
    function isWeekend(dayOfWeek) {
        if (dayOfWeek === 0 || dayOfWeek === 6) {
            return true;
        }

        return false;
    }

   function getTime(hours, minutes, ampm) {
        hours = hours % 12;
        hours = hours ? hours : 12;
        minutes = minutes - (minutes % 30);
        var temp = minutes == 0 ? "00" : minutes;

        return hours + ":" + temp + " " + ampm;
    }

    function getTimestamp(d) {
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var hours = d.getHours() + 1;
        var minutes = d.getMinutes();

        // Format: yyyy-MM-dd HH:mm
        return year + "-" + month + "-" + day + " " + hours + ":" + minutes;
    }

    function setDateTime(d) {
        var hours = d.getHours();
        var minutes = d.getMinutes();
        var ampm = (hours >= 12) ? "PM" : "AM";
        var options = {
            prop11: getTime(hours, minutes, ampm), // Hour of Day
            prop12: d.getDay(),  // Day of Week
            prop13: isWeekend(d.getDay()) ? "Weekend" : "Weekdays" // Weekday/Weekend
        };

        $.extend(global, options);
    }
    */
    function clear(options) {
        for (var key in options) {
            delete s[key];
            //delete global[key];
        };
    }

    // Public function(s)
    self.init = function (status, gender, age, ethnicity, location, subscriptionType, subscriberStatus, id, prm, lgid, eid) {
        var prop27;
        if (subscriptionType === "premium") {
            prop27 = "Renewing";
        } else {
            prop27 = subscriptionType;
        }

        var options = {
            eVar36: status, // i.e m.christianmingle.com
            prop18: gender,
            prop19: age,
            prop20: ethnicity,
            prop21: location,
            prop27: prop27,
            prop22: subscriberStatus,
            prop23: id,
            eVar11: prm,
            eVar13: lgid,
            eVar12: eid

    };

        $.extend(global, options);
    };

    self.track = function (pageName, options) {
        if (!options)
            options = {};
        if (pageName)
            options.pageName = pageName;

        //var currentDate = new Date();
        //options.prop9 = getTimestamp(currentDate);
        options.prop10 = location.href;

        //setDateTime(currentDate);

        $.extend(s, global, options);
        s.t();

        clear(options);
    };

    self.trackEvents = function (pageName, events, options) {
        
        if ($.cookie('refreshOmni')) {
            $.removeCookie('refreshOmni');
        } else {
            if (!options)
                options = {};
            if (pageName)
                options.pageName = pageName;
            if (events)
                options.events = events;

            //var currentDate = new Date();
            //options.prop9 = getTimestamp(currentDate);
            options.prop10 = location.href;
            //options.prop27 = prop27;
            //setDateTime(currentDate);

            $.extend(s, global, options);
            s.t();

            clear(options);
        }
    };

    self.getGlobal = function () {
        return global;
    };

    self.removeEvar = function (num) {
        delete s["eVar" + num];
    };

    self.removeProp = function (num) {
        delete s["prop" + num];
    };

    //self.removeProperty = function (key) {
    //    delete s[key];
    //};

    self.setEvar = function (num, value) {
        s["eVar" + num] = value;
    };

    self.setProp = function (num, value) {
       s["prop" + num] = value;
    };

    return self;
})();