﻿spark.views.mail = (function () {
    var self = {},
        activeFolderId = -4,
        messageList = {
            inbox: [],
            sent: [],
            trash: []
        },
        isLoading = false,
        isLastPage = false,
        swiper = null,
        currentDate = new Date(),
        offsetFromTop = 0,
        accessCheck = false,
        allAccess,
        readReceipt,
        basicSubscription,
        page,
        totalItems,
        pageSize = 20;


    function addTabEvents() {
        var tabs = $("ul.mail-tabs");
        tabs.children("li").each(function () {
            var li = $(this);

            // Bind event to anchor link(s).
            li.find("a").click(function (e) {
                var a = $(this);
                var folderId;
                if ((a.attr("href") !== "/mail/folders")) {
                    folderId = getFolderId(a.attr("href"));
                }

                $("#mail_page").attr("data-pageno", 1);
                isLoading = false;
                isLastPage = false;
                
                
                if (!$(this).hasClass("active")) {
                    page = 1;
                    changeTabs(folderId);
                }
                e.preventDefault();
            });
        });
    }

    function addLabel(today, message, ul) {
        var d = new Date(message.insertDate);
        var diff = today.getTime() - d.getTime();
        var seconds = Math.floor((diff / 1000));
        var minutes = Math.floor((seconds / 60));
        var hours = Math.floor((minutes / 60));
        var days = Math.floor((hours / 24));

        if (days === 1 && ul.find("li.yesterday").length === 0) {
            $("<li class=\"label yesterday\">Yesterday</li>").appendTo(ul);
        }
        if ((days > 1 && days <= 7) && ul.find("li.week").length === 0) {
            $("<li class=\"label week\">1 week ago</li>").appendTo(ul);
        }
        if ((days > 7 && days <= 31) && ul.find("li.month").length === 0) {
            $("<li class=\"label month\">1 month ago</li>").appendTo(ul);
        }
        if (days > 31 && ul.find("li.old").length === 0) {
            $("<li class=\"label old\">Old messages</li>").appendTo(ul);
        }
    }

  

    function appendToList(messages, folderId) {
        var ul = $("#mail_list");

        var lastIndex = (messages.length - 1);
        // Remove loading footer.
        ul.find("li.loading").remove();
        ul.find("li.last").removeClass("last");
        spark.utils.common.hideLoading();
        //isLoading = false;

        $.each(messages, function (index, item) {
            var member = (folderId !== -2) ? item.sender : item.recipient;
            var li = $("<li  data-messageId=\"" + messages[index].id + "\"/>");
            var a = $("<a data-role=\"none\"/>");
            var img = $("<img />");
            var p = $("<p />");
            var userName = $("<span class=\"primary\" />");
            var mailType = $("<span class=\"secondary\" />");
            var icon = $("<span class=\"mail-icon\" />");
            var openDate;
            var folder;
            var photo = member.defaultPhoto;

            //if (folderId === -2) {
            //    folder = "sent";
            //}

            var subject = item.body;


            var shortSub;
            if (subject) {
                if (subject.indexOf("&#039;") === -1 || subject.indexOf("&#039;") >= 0) {
                    subject = subject.replace(/&#039;/g, "\'");
                }

                if (subject.indexOf("---Original Message---") >= 0) {
                    subject = subject.substr(0, subject.indexOf("---Original Message---"));
                }
                if (subject.length >= 35) {
                    shortSub = subject.substr(0, 35) + "...";
                } else {
                    shortSub = subject;
                }
                
                if (shortSub.indexOf('<br') === -1 || shortSub.indexOf('<br') >= 0) {
                    shortSub = shortSub.split("<br")[0];
                }
                
            }

           
            var thumbnail = (member.gender === "Female") ?
                "/images/hd/img_silhouette_woman@2x.png" : "/images/hd/img_silhouette_man@2x.png";

            addLabel(currentDate, item, ul);

            //a.attr("href", "/mail/message/" + item.id);
            a.attr("href", "#");

            if (photo && photo.fullPath && photo.thumbPath.length > 0) {
                thumbnail = photo.fullPath;
            }



            img.attr("src", thumbnail);
            img.appendTo(a);

            icon.appendTo(a);
            icon.addClass(item.mailType.toLowerCase());
            
            if (basicSubscription === "BasicSubscription") {
            
            }
            
            if (item.mailType === "mail") {
                if (folderId === -4 && item.recipient.subscriptionStatus === "unpaid") {
                    mailType.text("Subscribe to read your email");
                }
                else if (item.subject === "") {
                    mailType.text(shortSub);
                } else if (item.subject !== "" && item.subject.length <= 35) {
                    mailType.text(item.subject);
                } else {
                    var mailsub = item.subject.substr(0, 35) + "...";
                    mailType.text(mailsub);
                }
            }
            else if (item.mailType === "smile") {
                mailType.text("Smile");
            }
            else if (item.mailType === "im") {
                mailType.text("Missed IM");
            }
            else {
                mailType.text(item.mailType);
            }

            if (item.messageStatus === 8) {
                if ((a).hasClass("read")) {
                    (a).removeClass("read");
                }

            } else if (item.messageStatus === 1 || item.messageStatus === 2) {
                if (!(a).hasClass("read")) {
                    ((a).addClass("read"));
                }
                
            }

            userName.text(member.username);
            userName.appendTo(p);

            mailType.appendTo(p);

            if (member.isOnline) {
                userName.addClass("online");
            }

            p.appendTo(a);
            a.appendTo(li);

            li.click(function (e) {
                var messageId = $(this).attr("data-messageId");
                var a = $(this).find("a");
                var href = a.attr("href");
                var username = $(this).find("span.primary").text();
                var gender = ($(this).attr("data-gender") === "Male") ? "his" : "her";

                if (item.recipient.subscriptionStatus === "unpaid") {
                    window.location.href = "/subscription/subscribe/4161";
                    return;
                }

                if ((member.blockContactByTarget) || (member.blockContact) || (member.selfSuspendedFlag) || (member.adminSuspendedFlag)) {
                    spark.utils.common.showGenericDialog('');
                    $('#generic_dialog').addClass('lift');
                    e.preventDefault();
                }
                if (member.blockContact) {
                    $('#generic_dialog .content').html('<b>Sorry!</b><br/>You can\'t communicate with ' + username + ' because you\'ve blocked ' + gender + '.');
                    return;
                }
                else if (member.blockContactByTarget) {
                    $('#generic_dialog .content').html('<b>Sorry!</b><br/>' + username + ' is currently unavailable at this time');
                    return;
                } else if (member.selfSuspendedFlag) {
                    $('#generic_dialog .content').html('<b>Sorry!</b><br/>' + username + ' either became a Success Story or temporarily removed ' + gender + ' account.');
                    return;
                } else if (member.adminSuspendedFlag) {
                    spark.utils.common.showGenericDialog("This profile is currently not available");
                    return;
                }

                else {
                    showMessage(messageId);
                    e.preventDefault();
                }

            });

            if (index === lastIndex) {
                li.addClass("last");
            }
            li.appendTo(ul);
            storeMessages(folderId, item);
        });

        if (messages.length === 0 && page === 1) {
            ul.append("<li class=\"no-results\"><p>No results found.</p></li>");
        }

        //isLoading = false;
        isLastPage = (page * pageSize) >= totalItems;

        if (!isLastPage) {
          //  loadMoreMessages(folderId);
        }

    }

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            if (!isLastPage) {
                loadMoreMessages(activeFolderId);
            }
        }

    });

    function loadMoreMessages(folderId) {
            page = page + 1;
            isLastPage = (page * pageSize) >= totalItems;
            var ul =  $("#mail_list");
            getMessages(folderId);
    }

    function appendSwiperSlides() {
        var swiper = $("#message_swiper");
        var wrapper = swiper.find(".swiper-wrapper");
        var list = getMessageList();

        $.each(list, function (index, item) {
            var slider = $("<div class=\"swiper-slide\" />");
            var container = $("<div class=\"message-container\" />");

            //container.attr("data-index", index);
            container.appendTo(slider);
            slider.appendTo(wrapper);
        });
    }

    function changeTabs(folderId) {
        var tabs = $("ul.mail-tabs");
        var list = $("#mail_list");
        var selectedIndex = 0;


        if (activeFolderId !== folderId) {
            list.empty();
            messageList = {
                inbox: [],
                sent: [],
                trash: []
            };
        }

        tabs.children("li").each(function (index) {
            var li = $(this);
            var a = li.find("a");

            a.removeClass("active")
                .removeClass("border-left")
                .removeClass("border-right");

            if (getFolderId(a.attr("href")) === folderId) {
                a.addClass("active");
            }


            var parent = li.parent();
            switch (folderId) {
            case -4:
                parent.find("li:eq(1) a").addClass("border-left");
                parent.find("li:eq(2) a").addClass("border-left");
                $("div.deletedMessageDiv").removeClass("hide");
                break;
            case -2:
                parent.find("li:eq(0) a").addClass("border-right");
                parent.find("li:eq(2) a").addClass("border-left");
                $("div.deletedMessageDiv").addClass("hide");
                break;
            default:
                parent.find("li:eq(0) a").addClass("border-right");
                parent.find("li:eq(1) a").removeClass("border-left").addClass("border-right");
                $("div.deletedMessageDiv").addClass("hide");
                break;
            }
        });

        $("div.folder-message-container").addClass("hidden");
        activeFolderId = folderId;

        $("ul#mail_list").removeClass("hidden");
        getMessages(folderId);

    }


    function createMessagePanel(message, index) {
        var swiper = $("#message_swiper");
        var slide = swiper.find(".swiper-wrapper").children(":eq(" + index + ")");
        var container = slide.children(".message-container");

        // We've already processed the slide, so just ignore further
        // processing.
        if (container.attr("data-processed")) {
            return;
        }

        var member = (activeFolderId !== -2) ? message.sender : message.recipient;
        var blockContact = (member.blockContact) ? "blockContact" : "";
        var blockContactByTarget = (member.blockContactByTarget) ? "blockContactByTarget" : "";
        var adminSuspended = (member.adminSuspendedFlag) ? "adminSuspended" : "";
        var selfSuspended = (member.selfSuspendedFlag) ? "selfSuspended" : "";
        var profileContainer = $('<div class="profile-container '+ adminSuspended+' '+ blockContact+' '+ blockContactByTarget+' '+ selfSuspended+'" data-memberId="' + member.id + '" data-gender="'+ member.gender +'"/>');
        var profileDetailContainer = $("<p  />");
        var img = $("<img />");
        var userName = $("<span class=\"username\" />");
        var age = $("<span class=\"age\" />");
        var location = $("<span class=\"location\" />");
        var messageDate = $("<span class=\"date\" />");
        var messageBody = $("<p />");
        var trash = $("<span class=\"trash\" data-folderId = \"" + message.memberFolderId + "\" data-messageId= \"" + message.id + "\" />");
        var imageUrl = (member.gender === "Male") ? "/images/hd/img_silhouette_man@2x.png" : "/images/hd/img_silhouette_woman@2x.png";
        var body = (message.body) ? message.body.replace(/\n/g, '<br />') : "";
        var photo = member.defaultPhoto;

        userName.text(member.username);
        age.text(member.age + " years old");
        location.text(member.location);

        messageDate.text(spark.utils.common.parseDate(message.insertDate, 'GMT'));
        messageBody.addClass(message.mailType.toLowerCase());
        // messageBody.html(body);
        if (photo && photo.fullPath && photo.thumbPath.length > 0) {
            imageUrl = photo.fullPath;
        }

        trash.attr("data-messageid", message.id);

        img.prop("src", imageUrl);
        img.appendTo(profileContainer);
        profileContainer.click(function () {
            if (member.blockContactByTarget || member.adminSuspendedFlag || member.selfSuspendedFlag) {
                spark.utils.common.showGenericDialog("This profile is currently not available");
            } 
            
            else {
                $.mobile.changePage("/profile/" + member.id);
            } 
        });

        userName.appendTo(profileDetailContainer);
        age.appendTo(profileDetailContainer);
        location.appendTo(profileDetailContainer);
        messageDate.appendTo(messageBody);
        trash.appendTo(messageBody);

        profileDetailContainer.appendTo(profileContainer);
        profileContainer.appendTo(container);
        messageBody.appendTo(container);

        container.attr("data-processed", "true");

        // Get message detail.
        getMessageDetail(message.id, message.memberFolderId, true, function(data) {

            body = (data.body) ? data.body.replace(/\n/g, '<br />') : "";
            if (data.body === null && data.imMessages.length > 0) {
                for (var i = 0; i < data.imMessages.length; i++) {
                    var identifier = (data.imMessages[i].fromMemberId === $("div.nav-header").attr("data-myid")) ? "me" : "other";
                    var imgPresence = (data.imMessages[i].fromMemberId === $("div.nav-header").attr("data-myid")) ? "hidden" : "show";
                    var bubble = (data.imMessages[i].fromMemberId === $("div.nav-header").attr("data-myid")) ? "btm-right" : "btm-left";
                    var safari = '';

                    if ((navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1) || navigator.userAgent.match(/Mobi/)) {
                        safari = 'saf';
                    }

                    var sentTime = data.imMessages[i].sentTime;
                    var sentTimeToDate = new Date(sentTime);
                    var convertToLocal = sentTimeToDate.toLocaleString();
                    var imTimeStamp = convertToLocal;
                    imTimeStamp = imTimeStamp.replace(',', '');
                    var ul = $("<ul id='missedim-inbox' class='scrollable generic-list-small' />");

                    $('<li  class="chat_message ' + identifier + ' ' + safari + '"><a href="#"><img class="' + imgPresence + '" src="' + imageUrl + '"/><p><span id="' + identifier + '" class="imMessage talk-bubble tri-right round ' + bubble + '">' + spark.messageconversion.convertCustomTagToHtml(spark.messageconversion.htmlDecode(data.imMessages[i].message)) + '</span><span class="imTimeStamp">' + imTimeStamp + '</span></li>').appendTo(ul);

                    $("span.spr").text('');

                    ul.appendTo(messageBody);
                }
            } else if (data.mailType === "FreeTextComment") {

                body = data.body.toString();
                var txtRemove = body.substring(0, body.indexOf("{"));
                body = txtRemove;
                messageBody.html(body);

            }
            
            else if (data.mailType === "card") {
                var src = data.cardImage,
                    flashSrc = data.cardFlash,
                    img = $('<img class="card-img">');

                img.attr('src', src);
                messageBody.append(img);
                messageBody.append(body);

            } else {
                messageBody.html(body);
            }
 
            messageDate.appendTo(messageBody);
            trash.appendTo(messageBody);
            //move.appendTo(messageBody);

            setTimeout(function () {
                resizeSwiper(index);
            }, 500);
        });
    }


    function deleteMessage(messageId) {
        spark.utils.common.showConfirm("Are you sure you want to delete this message?", function () {

            var postData = {
                "MessagesToDelete": [
                  {
                      MessageId: messageId,
                      CurrentFolderId: $("span.trash").attr("data-folderid")
                  },
                ]
            };

                spark.api.client.callAPI(spark.api.client.urls.deleteMessage, null, postData, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                removeSlide(swiper.activeIndex);
                spark.utils.common.showMessageBar("Message has been deleted", true);
            });
        });
    }

    function dispose() {
        activeFolderId = 0;
        messageList = {
            inbox: [],
            sent: [],
            folders: [],
            trash: []
        };
        swiper = null;
    }


    function getFolderId(path) {
        if (path.indexOf("inbox") > 0) {
            return -4;
        }
        if (path.indexOf("sent") > 0) {
            return -2;
        }
        if (path.indexOf("trash") > 0) {
            return -1;
        }
    }

    function getMessage(messageId) {
        var messages = getMessageList();
        var length = messages.length;

        return messages[getSelectedIndex(messageId)];
    }

    function getMessageDetail(messageId, folderId, markAsRead, callback) {
        spark.api.client.callAPI(spark.api.client.urls.message, { messageId: messageId, folderId: folderId, markAsRead: 1 }, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                //console.log("error from gett message detail", response);
                return;
            }
            callback(response.data);
        });
    }

    function getSelectedIndex(messageId) {
        var messages = getMessageList();
        var length = messages.length;

        for (var i = 0; i < length; i++) {
            if (messageId === messages[i].id) {
                return i;
            }
        }

        return 0;
    }


     function getMessages(folderId, callback) {
        var ul = $("#mail_list");

        var params = {
            folderId: folderId,
            pageSize: pageSize,
            pageNumber: page,
            filter: "all"
        }

        spark.utils.views.addLoadingPanel(ul);

        // Make API call to retrieve messages.
        spark.api.client.callAPI(spark.api.client.urls.messages, params, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                //console.log("getMessages", response);
                return;
            }
            
            totalItems = response.data.totalMessages;

            isLastPage = (page * pageSize) >= totalItems;
            isLoading = true;
           appendToList(response.data.messageList, folderId);
           if (typeof callback === "function")
               callback();

        });

        if (!isLastPage) {
           // spark.utils.common.hideLoading();
        }
    }

    function getMessageList() {
        switch (activeFolderId) {
            case -2:
                return messageList["sent"];
            case -1:
                return messageList["trash"];
            default:
                return messageList["inbox"];  
        }
    };

    function gotoMessage(index) {
        var messages = getMessageList();
        var message = messages[index];

        createMessagePanel(message, index);
        resizeSwiper(index);
    };

    function getButtonText() {
        switch (activeFolderId) {
            case -2:
                return "Sent";
            case -1:
                return "Trash";
            case -4:
                return "Inbox";
           
        }
    }; 

    function hideReply() {
        $("div.reply").hide();
    };

    function removeSlide(index) {
        var messages = getMessageList();
        var wrapper = $(swiper.container).find(".swiper-wrapper");

        wrapper.children("div:eq(" + index + ")").remove();

        // Remove message from cache array.
        messages.splice(index, 1);

        // Check if there are more records in the last, if not then
        // take users back to the list page.
        var length = messages.length;
        if (length === 0) {
            showList();
            return;
        }

        // Check if last element from list is deleted, if so move back the
        // index to the last element (of the new array).
        if (index === length)
            --index;

        // Create message panel to show
        createMessagePanel(messages[index], index);
        
        // Re-initialize the swiper.
        swiper.reInit();
        swiper.swipeTo(index, 100);
    }

    function resizeSwiper(index) {
        var swiperContainer = $(swiper.container);
        var swiperWrapper = swiperContainer.find(".swiper-wrapper");
        var slide = swiperWrapper.find(".swiper-slide:eq(" + index + ")");

        var height = slide.find(".message-container").height();
        $(swiper.container).css({
            height: height + "px"
        });
    }

    function reply(callback) {
        var messages = getMessageList();
        var originalMessage = messages[swiper.activeIndex];
        var recipientId = $("div.profile-container").attr("data-memberid");;
        var subject = "RE: " + originalMessage.subject;
        var body = $("#message_body").val() ;
        var originalMessageID = originalMessage.id;

        if (originalMessage.mailType != "MissedIM" && originalMessage.body) {
             body = $("#message_body").val() + "\n\n" + "---Original Message---" + "\n\n" + originalMessage.body;
        } else if (originalMessage.mailType != "MissedIM" && !originalMessage.body) {
            var originalBody = $("p.email").text();
            body = $("#message_body").val() + "\n\n" + "---Original Message---" + "\n\n" + originalBody;
        }
        
        var postData = {
            recipientMemberId: recipientId,
            subject: subject,
            body: body,
            originalMessageRepliedToId : originalMessageID,
            mailType: "mail",
            isDraft : ""
        };
        var url = spark.api.client.urls.sendMessage;
        if ($("#message_body").val().length === 0) {
            spark.utils.common.showGenericDialog("Message body is required. Please type a message to send.");
            return;
        }


        spark.api.client.callAPI(url, null, postData, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            callback(response.data);
        });
    };

    function showList() {
        $("#message_detail_panel").fadeOut(350, function () {
            var swiperWrapper = $(swiper.container).find(".swiper-wrapper");
            swiperWrapper.empty();

            // Clear list to fetch fresh data.
            $("#mail_list").empty();
            messageList = {
                inbox: [],
                sent: [],
                trash: []
            };

            $("#mail_page").attr("data-pageno", 1);

           isLoading = false;
            isLastPage = false;
            page = 1;

            //console.log("activeFolderId", activeFolderId);

            getMessages(activeFolderId, function () {
                var panel = $("#message_list_panel");
                panel.fadeIn();
                $(document).scrollTop(offsetFromTop);
            });

        });
    }

    function showMessage(messageId) {
        offsetFromTop = $(window).scrollTop();
        $("#message_list_panel").addClass("hidden").hide();
        $(document).scrollTop(0);

        var header = $(".mail-message-header");
        var backButton = header.find(".back-button");
        var prevButton = header.find(".prev");
        var nextButton = header.find(".next");
        var sendButton = $("#btn_send");
        var div = $("div#message_list_panel");
       
        backButton.text(getButtonText());

        backButton.unbind("click").click(function () {
            $("#mail_page").attr("data-pageno", 1);
               spark.utils.common.showLoading();
                showList();
                showReply();

        });

        prevButton.unbind("click").bind("click", function () {
            swiper.swipePrev();
        });

        nextButton.unbind("click").bind("click", function () {
            swiper.swipeNext();
        });

        sendButton.unbind("click").bind("click", function () {
            reply(function () {
                spark.utils.common.showMessageBar("Message sent successfully", true);
                $("#message_body").val("");

                $.mobile.silentScroll(0);
               // setTimeout(window.location = "/mail", 7000);
               
            });
        });

        if (activeFolderId === -1)
            hideReply();
        appendSwiperSlides();

        var panel = $("#message_detail_panel");
        var message = getMessage(messageId);
        var index = getSelectedIndex(messageId);
        createMessagePanel(message, index);
        panel.fadeIn(500, function () {
            panel.removeClass("hidden");
        });

        if (swiper === null) {
            swiper = $("#message_swiper").swiper({
                initialSlide: index,
                loop: false,
                mode: "horizontal",
                speed: 600,
                onSlidePrev: function () {
                    gotoMessage(swiper.activeIndex);
                },
                onSlideNext: function () {
                    gotoMessage(swiper.activeIndex);
                }
            });

            $(swiper.container).click(function (e) {
                var target = $(e.target);
                if (target.is("span") && target.hasClass("trash")) {
                    var messageId = target.attr("data-messageid");
                    deleteMessage(messageId)
                }
            });

            $(swiper.container).css({
                clear: "both",
                width: "100%"
            });

            return;
        }

        // Since swiper widget is initialized, just re-initialize then go to
        // selected index.
        swiper.reInit();
        swiper.swipeTo(index, 100);

        $("img").load(function () {
            resizeSwiper(index);
        });
    };

    function showReply() {
        $("div.reply").show();
    };


    function storeMessages(folderId, message) {
        switch (folderId) {
            case -2:
                messageList["sent"].push(message);
                return;
            case -1:
                messageList["trash"].push(message);
                return;

            default:
                messageList["inbox"].push(message);
        }
    };

    self.getActiveFolder = function () {
        return activeFolderId;
    };

    self.getMessages = function (folderId) {
        getMessages(folderId);
    };

    
    //center ads
   self.centerGPTAds = function() {
        var screenWidth = $(window).width();
        var iframeWidth = $(".gpt-ads-top div iframe").width();
        var marginLeft = ((screenWidth - iframeWidth) / 2);

        if (iframeWidth < screenWidth) {
            $(".gpt-ads-top > div").css('marginLeft', marginLeft);
        }
    };

    self.getMessageDetail = function (messageId, folderId, markAsRead, callback) {
        getMessageDetail(messageId, folderId, markAsRead, callback);
    };


    self.init = function () {

        swiper = null;
        addTabEvents();
        activeFolderId = -4;

        // Get user's inbox (default).
        $("a[href='/mail/inbox']").click();
        self.centerGPTAds;
        

        var mailTabs = $("div#message_list_panel.page-content");
        var messageDiv = $("<div class='deletedMessageDiv'  />");
        messageDiv.text("Messages from members who are no longer active may have been removed");
        mailTabs.before(messageDiv);


        page = 1;
    };

    self.sendMessage = function (recipientMemberId, subject, body, originalMessageRepliedToId, callback) {
        var url = spark.api.client.urls.sendMessage;
        var postData = {
            recipientMemberId: recipientMemberId,
            subject: subject,
            body: body,
            originalMessageRepliedToId: originalMessageRepliedToId,
            mailType: "mail",
            isDraft: ""
        };

        if ($.trim(subject).length === 0) {
            spark.utils.common.showGenericDialog("Subject is required. Please type a subject.");
            return;
        }

        if ($.trim(body).length === 0) {
            spark.utils.common.showGenericDialog("Message body is required. Please type a message to send.");
            return;
        }

        spark.api.client.callAPI(url, null, postData, function (success, response) {
            if (!success) {
                //spark.utils.common.showGenericDialog("There was a problem sending your message", true);
                //spark.utils.common.showError(response);
                if (response.error.code === 40017) {
                    spark.utils.common.showGenericDialog("It takes 2-4 hours to activate email privileges after subscribing. Please try again later.", true);
                } else {
                    spark.utils.common.showError(response);
                }
                return;
            }
            callback(response.data);
        });
    };

    return self;
})();

$(document).on("pageshow", "#mail_page", function () {
    spark.views.mail.init();

    $(window).off("orientationchange").on("orientationchange", function (event) {
        spark.views.mail.centerGPTAds();
    });
    setTimeout(function () {
        googletag.pubads().refresh([gpt_inbox_top]);
    }, 750);
});


$(document).on("pageshow", "#im_redirect", function () {
        var senderId;
        var recipientId;
        var conversationTokenId;
        var requestType;
        var messageId;
        function parseUri(str) {

            var parser = document.createElement('a');
            parser.href = str;
            $("span.hostname").text(" " + parser.hostname);
            $("span.page").text(" " + parser.pathname);
            query = parser.search;

            //parses the uri queries and creates individual keys and values

            $(query.substring(1, query.length).split('&' || '?')).each(function (k, qstring) {
                key = qstring.split('=')[0];
                val = qstring.split('=')[1];

                if (key === "senderMemeberId") {
                    senderId = val;
                } else if (key === "conversationTokenId") {
                    conversationTokenId = val;
                } else if (key === "recipientMemberId") {
                    recipientId = val;
                } else if (key === "requestType") {
                    requestType = val;
                } else if (key === "messageId") {
                    messageId = val;
                }
            });  //end .each()
        }; //end parseUri

        var url = window.location.search;
        parseUri(url);

        if (requestType == 0) {
            spark.views.im.sendEjabIMRejectedMessage(senderId, conversationTokenId);
         }
        if (requestType == 1) {
            spark.views.mail.getMessageDetail(messageId, true, function (data) {
              
                if (!data) {
                    spark.utils.common.showGenericDialog("there is no message");
                }
                var container = $("div#imredirect");
                var body = (data.body) ? data.body.replace(/\n/g, '<br />') : "";
                var messageBody = $("<p />");
                    if (data.mailType === "MissedIM" && body.indexOf("sparktag") > 0) {
                        var rebuiltHtml = spark.messageconversion.convertCustomTagToHtml(spark.messageconversion.htmlDecode(body));
                        container.html(rebuiltHtml);
                        var images = container.find("img");

                        $.each(images, function(index, item) {
                            var img = $(item);
                            img.css({
                                width: "100%",
                                height: "auto"
                            });
                            img.parent("a").removeAttr("href");
                        });
                        messageBody.text("");
                        messageBody.prepend(container.html());
                    } else {
                        container.html(body);
                    }
            });
        }
});