﻿spark.views.blockedmembers = (function () {
    var self = {};
    var isLoading = false;
    var isLastPage = false;

    var memberList = [];
    var pageInit = true;

    var incomingLastPage = false;
    var outgoingLastPage = false;

    function appendToList(data) {
        var ul = $("ul.scrollable");

        // Remove loading footer.
        ul.find("li.loading").remove();

        if (data.length == 0) {
            var p = $("<p />");
            p.text("No results found!");
            p.addClass("no-results-text");
            //p.appendTo(ul);
        }

        $.each(data, function (index, item) {
            var li = $("<li data-memberid=\"" + item.miniProfile.id + "\" />");
            var a = $("<a />");
            var img = $("<img />");
            var p = $("<p />");
            var userName = $("<span class=\"primary\" />");
            var ageLocation = $("<span class=\"secondary\" />");
            var online = $("<span class=\"online\" />");
            var imageUrl = (item.miniProfile.gender === "Male") ? "/images/hd/img_silhouette_man@2x.png" : "/images/hd/img_silhouette_woman@2x.png";

            //a.attr("href", "/profile/" + item.miniProfile.id);


            if (item.miniProfile.defaultPhoto != null) {
                if (item.miniProfile.defaultPhoto.thumbPath && item.miniProfile.defaultPhoto.fullPath !== "") {
                    imageUrl = item.miniProfile.defaultPhoto.fullPath;
                }
            }

            img.attr("src", imageUrl).appendTo(a);
            userName.text(item.miniProfile.username).appendTo(p);
            ageLocation.text(item.miniProfile.age + ", " + item.miniProfile.location).appendTo(p);

            if (item.miniProfile.isOnline) {
                online.appendTo(p);
            }

            if (!ul.hasClass("grid-view")) {
                online.text("Online");
            } else {
                // If in grid mode, apply neccessary in-style to override the CSS class.
                li.css({
                    height: gridHeight + "px"
                });

                img.css({
                    width: gridWidth + "px",
                    height: gridHeight + "px"
                });
            }

            if (item.miniProfile.isHighlighted) {
                p.addClass('member-highlight');
                li.addClass('member-highlight');
            }

            p.appendTo(a);
            a.appendTo(li);
            li.appendTo(ul);

            memberList.push(item);
        });

        bindClickEvents();

        isLoading = false;
        pageInit = false;
    };

 
    function getHotlist(category, pageSize, pageNo, callback) {
        var url = spark.api.client.urls.hotlist;
        var params = {
            hotlist: category,
            pageSize: pageSize,
            pageNumber: pageNo
        };

        spark.api.client.callAPI(url, params, null, function (success, response) {
            if (callback) {
                callback(success, response);
            }
            
        });

    };

    function getMiniProfile(memberId) {
        spark.api.client.callAPI(spark.api.client.urls.fullProfile, { targetMemberId: memberId }, null, function (success, response) {
            var fullProfile = [];
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
        });
   }
   
    function filter(input) {
        a = input;
        for (var i = 0; i < (a.length); i++) {
            for (var j = i + 1; j < a.length; j++) {
                if (a[i].miniProfile.id === a[j].miniProfile.id) {
                    var index = a.indexOf(a[i].miniProfile.id);
                    var index2 = a.indexOf(a[j].miniProfile.id);
                    return a;
                }
            }
        }
    }
    function arrayUnique(input) {
        var a = input.slice(); //why? changes length from 5 to 18 how?
        for (var i = 0; i < (a.length) ; i++) {
            for (var j = i + 1; j < a.length; j++) {
                if (a[i].miniProfile.id === a[j].miniProfile.id) {

                    var idate = a.indexOf(a[i].miniProfile.id);
                    var jdate = a.indexOf(a[j].miniProfile.id);

                    // compare dates then remove older date
                    //var idate = new Date(a[i].actionDate).getTime();
                    //var jdate = new Date(a[j].actionDate).getTime();
                    //var idate = a[i].index;
                    //var jdate = a[j].index;
                    if (idate > jdate) {
                    //   // keep the newest one :idate
                       a.splice(j--, 1);
                    } else {
                       // remove i 
                        a.splice(i, 1);
                       // update j value
                        j = i;
                    }
                }
            }
        }
       return a;
     
    };

    function displayNoResults() {
        var ul = $("ul.scrollable");
        var p = $("<p />");
        p.text("No results found!");
        p.addClass("no-results-text");
       

        if ((getHotlist("block", 25, 1) == 0) && ((getHotlist("hide", 25, 1) == 0))) {
            p.appendTo(ul);
        } else {
        }
    }

    function mergeHotLists(pageNo) {
        var url = spark.api.client.urls.hotlist;
        var blockParams = {
            hotlist: "block",
            pageSize: 25,
            pageNumber: pageNo
        };

        var hideParams = {
            hotlist: "hide",
            pageSize: 25,
            pageNumber: pageNo
        }

        var block = [];
        var hide = [];
        var combinedList = [];

        var ul = $("ul.scrollable");
        spark.utils.views.addLoadingPanel(ul);

        var interval = ul.find("li").length !== 0 ? spark.utils.common.getLoadingTime() : 0;
        window.setTimeout(function () {
        
            spark.api.client.callAPI(url, blockParams, null, function (success, response) {
                block = response.data;
                spark.api.client.callAPI(url, hideParams, null, function (success, response) {

                    hide = response.data;


                    combinedList = arrayUnique(block.concat(hide));

                    appendToList(combinedList);


                });
            });

        }, interval);

    }



    function getHotlistData(pageNo , category ) {
        isLoading = true;

        var membersList = [];
        var blockedMembers = [];
        var hiddenMembers = [];

        var page = $("#blockedmembers_settings_page");
        var url = spark.api.client.urls.hotlist;
        //var pageNo = Number(page.attr("data-pageno"));
        var pageSize = page.attr("data-pagesize");

       
        var params = {
            hotlist: category,
            pageSize: 25,
            pageNumber: pageNo
        }
    

        var ul = $("ul.scrollable");
        spark.utils.views.addLoadingPanel(ul);

        var interval = ul.find("li").length !== 0 ? spark.utils.common.getLoadingTime() : 0;

        window.setTimeout(function () {
            spark.api.client.callAPI(url, params, null, function (success, response) {
                if (!success) {
                    isLoading = false;
                    spark.utils.common.showError(response);
                    $("ul.scrollable").find("li.loading").remove();
                    return;
                }
                page.attr("data-pageno", pageNo);
                if (response.data.length < pageSize) {
                    isLastPage = true;
                }
                appendToList(response.data);
            });// ends blockParams

        }, interval);
    }


    function displayBlockOrHide(memberId) {
        spark.api.client.callAPI(spark.api.client.urls.miniprofile, { targetMemberId: memberId }, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            if ((response.data.hideSearch) || (response.data.hideSearchByTarget)) {
                $("div.block-popup > p.block-one").addClass("block");
            }
            if ((response.data.blockContact) || (response.data.blockContactByTarget)) {
                $("div.block-popup > p.block-two").addClass("block");
            }
        });
    }

    

    function bindClickEvents() {
        var li = $('#blockedmembers_list li a');
        //var memberId = spark.utils.common.getMemberId();


        li.off().on("click", function (e) {
            var memberId = $(this).parent('li').attr('data-memberid');
            $("div.block-popup").toggleClass("disappear");
            $("div.block-popup").attr("data-memberId", memberId);
            displayBlockOrHide(memberId);
        });

        //submit 
        $(document).on('tap', "a.ui-btn.ui-btn-inline.submit-btn", function (e) {
            e.preventDefault();
            $("div.block-popup").addClass("disappear");
            $("div.profile-block-or-report").removeClass("hide");

            if ($("p.block-one, p.block-two, p.block-three").hasClass("block")) {
                $("li.block-btn a.ui-btn.ui-btn-inline.block").addClass("selected");
                $("li.block-btn a.ui-btn.ui-btn-inline.block").text("Unblock");
            } else {
                $("li.block-btn a.ui-btn.ui-btn-inline.block").removeClass("selected");
                $("li.block-btn a.ui-btn.ui-btn-inline.block").text("Block");
            }
            blockUser();
        });

    };

    function blockUserByListName(listname) {
        var blockStatus = []; 
        var memberId = $("div.block-popup").attr("data-memberId");
        var params = {
            hotListCategory: listname,
            targetMemberId: memberId,
        }

        spark.api.client.callAPI(spark.api.client.urls.addToHotList, params, null, function (success, response) {
            var fullProfile = [];
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
        });

    };

    function unblockUserByListName(listname) {
        var fullProfile = [];
        var memberId = $("div.block-popup").attr("data-memberId");
        var params = {
            hotListCategory: listname,
            targetMemberId: memberId,
        }
        spark.api.client.callAPI(spark.api.client.urls.removeFromHotlist, params, null, function (success, response) {
            var fullProfile = [];
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            window.location = "/settings/blockedmembers";
        });

    };

    function blockUser() {
        if ($("p.block-one").hasClass("block")) {
            blockUserByListName("hide");
        } else {
            unblockUserByListName("hide");
        }

        if ($("p.block-two").hasClass("block")) {
            blockUserByListName("block");
        } else {
            unblockUserByListName("block");
        }
        
        //window.location reload?
    };

    function cancelBlock() {
        $("div.block-popup").addClass("disappear");
        $("div.profile-block-or-report").removeClass("hide");
    }

    self.getFavorites = function () {
        var cachedData = $(document).data("cachedData");

        if (cachedData != null) {
            if (cachedData.cachedPage == "blockedmembers") {
                return;
            }
        }

        if (pageInit) {
            return;
        }

        if (isLoading || isLastPage)
            return;

        var pageNo = Number($("#blockedmembers_settings_page").attr("data-pageno"));
        //getHotlistData(++pageNo, "block");
        //getHotlistData(++pageNo, "hide");
       mergeHotLists(++pageNo);

    };


    self.init = function () {
        isLastPage = false;
        pageInit = true;
        var cachedData = $(document).data("cachedData");

        if (cachedData != null) {
            if (cachedData.cachedPage == "blockedmembers") {
                self.resumePageStatus();
            }
        }
        else {
            //getHotlistData(1, "block");
            //getHotlistData(1, "hide");
        }

        mergeHotLists(1);
        displayNoResults();

        $("a.ui-btn.ui-btn-inline.cancel-btn").on("tap", cancelBlock);

        spark.views.hotlist.bindClickEvents();
    };


    self.displayNoResults = displayNoResults;

    self.isOnline = function () {
        var li = $("ul.scrollable li");
        li.each(function () {
            var id = $(this).attr("data-memberid");

            spark.api.client.callAPI(spark.api.client.urls.miniprofile, { targetMemberId: id }, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }
                if (response.data.isOnline) {
                    $("li[data-memberid=" + id + "]").find("p span.online").show();
                }
            });

        });
    };

    self.storeMembers = function () {
        var members = [];
        var count = 1;
        $("#blockedmembers_list").children("li").each(function () {
            var li = $(this);
            li.attr("data-index", count++);
            members.push({
                memberId: li.find("a").attr("href").replace("/profile/", ""),
                thumbnail: li.find("a img").attr("src"),
                userName: li.find("a span.primary").text(),
                slideNo: li.attr("data-index")
            });
        });

        // Cache data so that it can be retrieve from the Profile page.
        $(document).data("members", members);

        $(document).data("cachedData", {
            cachedPage: "blockedmembers",
            pageNumber: $("#blockedmembers_settings_page").attr("data-pageno"),
            lastPage: isLastPage,
            offsetY: $(document).scrollTop(),
            membersData: memberList,
        });


    }

    self.cleanCache = function () {
        $(document).removeData("members");
        $(document).removeData("cachedData");
    }

    self.resumePageStatus = function () {
        var cachedData = $(document).data("cachedData");

        $("#blockedmembers_settings_page").attr("data-pageno", cachedData.pageNumber);
        isLastPage = cachedData.lastPage;
        isLoading = true;
        memberList = [];

        appendToList(cachedData.membersData);
        $(document).scrollTop(cachedData.offsetY);

        setTimeout(function () {
            self.cleanCache();
        }, 500);
    }

    self.reset = function () {
        memberList = [];
        pageInit = true;
    }

    return self;
})();
$(document).on("pagebeforehide", "#blockedmembers_settings_page", function (e, ui) {
    //check if the next page is the profile page
    if (ui.nextPage.attr('id') == 'profile_page') {
        spark.views.blockedmembers.cleanCache();
        spark.views.blockedmembers.storeMembers();
    }
    else {
        spark.views.blockedmembers.cleanCache();
    }
});

$(document).on("pageshow", "#blockedmembers_settings_page", function (e, ui) {
    //spark.views.hotlist.isOnline();
    // spark.views.hotlist.init();
    spark.views.blockedmembers.init();

});

$(document).on("pagehide", "#blockedmembers_settings_page", function () {
    spark.views.blockedmembers.reset();
});