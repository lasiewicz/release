﻿module.exports = function (grunt) {
    grunt.initConfig({
        concat: {
            options: {
                separator: grunt.util.linefeed + grunt.util.linefeed
            },
            core: {
                src: ['scripts/core/spark.api.client.js',
                    'scripts/core/spark.ga.js',
                   // 'scripts/core/spark.omniture.js'
                ],
                dest: 'scripts/core/spark.core.js'
            },
            views: {
                src: ['scripts/views/activity.js',
                    'scripts/views/admirer.js',
                    'scripts/views/hotlist.js',
                    'scripts/views/im.js',
                    'scripts/views/mail.js',
                    'scripts/views/matches.js',
                    'scripts/views/message.js',
                    'scripts/views/mol.js',
                    'scripts/views/profile.js',
                    'scripts/views/my-profile.js',
                    'scripts/views/photos.js',
                    'scripts/views/search.js'
                ],
                dest: 'scripts/views/spark.views.js'
            },
            widgets: {
                src: [
                    'scripts/widgets/spark.widgets.multiselect.js',
                    'scripts/widgets/spark.widgets.photoviewer.js'
                ],
                dest: 'scripts/widgets/spark.widgets.js'
            },
            css: {
                src: ['css/common.css', 'css/controls.css'],
                dest: 'css/spark.css'
            },
            css_views: {
                src: ['css/views/activity.css',
                    'css/views/home.css',
                    'css/views/mail.css',
                    'css/views/match.css',
                    'css/views/message.css',
                    'css/views/misc.css',
                    'css/views/mol.css',
                    'css/views/photos.css',
                    'css/views/profile.css',
                    'css/views/search.css'
                ],
                dest: 'css/views/spark.views.css'
            },
            css_widgets: {
                src: ['css/widgets/spark.widgets.multiselect.css',
                    'css/widgets/spark.widgets.photoviewer.css'
                ],
                dest: 'css/widgets/spark.widgets.css'
            }
        },

        uglify: {
            my_target: {
                files: {
                    'scripts/global.min.js': ['scripts/global.js'],
                    'scripts/core/spark.core.min.js': ['scripts/core/spark.core.js'],
                    'scripts/utils/spark.utils.min.js': ['scripts/utils/spark.utils.common.js'],
                    'scripts/views/spark.views.min.js': ['scripts/views/spark.views.js'],
                    'scripts/widgets/spark.widgets.min.js': ['scripts/widgets/spark.widgets.js']
                }
            }
        },

        cssmin: {
            minify: {
                files: {
                    'css/normalize.min.css': ['css/normalize.css'],
                    'css/spark.min.css': ['css/spark.css'],
                    'css/vendors/idangerous.swiper.min.css': ['css/vendors/idangerous.swiper.css'],
                    'css/views/spark.views.min.css': ['css/views/spark.views.css'],
                    'css/widgets/spark.widgets.min.css': ['css/widgets/spark.widgets.css']
                }
            }
        },

        watch: {
            scripts: {
                files: ['Scripts/core/*.js',
                    'Scripts/utils/*.js',
                    'Scripts/views/*.js'
                ],
                tasks: 'concat'
            },
            css: {
                files: ['Css/views/*.css',
                    'Css/widgets/*.css'],
                tasks: 'concat'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['watch']);
};