﻿spark.views.message = (function () {
    var self = {};

    function bindEvents() {
        var backButton = $("button.back-button");
        var replyButton = $("#btn_send");
        var deleteButton = $("span.trash");
        var profileContainer = $("div.profile-container");

        backButton.off().click(function () {
            window.history.back();
        });

        profileContainer.off().click(function (e) {
            $.mobile.changePage("/profile/" + $(this).attr("data-memberid"));
        });

        replyButton.off().click(function () {
            var recipientId = $("#recipient").val();
            var subject = "RE: " + $("#subject").val();
            var body = $("#message_body").val();
            var postData = {
                RecipientMemberId: recipientId,
                Subject: subject,
                Body: body,
                MailType: "Email"
            };
            var url = spark.api.client.urls.sendMessage;

            if (body.length === 0) {
                spark.utils.common.showGenericDialog("Message body is required. Please type a message to send.");
                return;
            }

            spark.api.client.callAPI(url, null, postData, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                $("#message_body").val("");
                spark.utils.common.showMessageBar("Message sent successfully", true);

            });
        });

        deleteButton.off().click(function () {
            deleteMessage();
        });

        convertDatesToLocal();

    };

    function convertDatesToLocal() {
        $('p.email span.date').each(function () {
            var pstDate = $(this).html();
            $(this).html(spark.utils.common.parseDate(pstDate, 'GMT'));
        });
    }

    function deleteMessage() {
        spark.utils.common.showConfirm("Are you sure you want to delete this message?", function () {
            var params = {
                currentFolderId: $("#folder_id").val(),
                messageId: $("#message_id").val()
            };

            spark.api.client.callAPI(spark.api.client.urls.deleteMessage, params, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                window.history.back();
                spark.utils.common.showMessageBar("Message has been deleted", true);
            });
        });
    };

    function reply() {

    };

    self.init = function () {
        bindEvents();
    };

    return self;
})();

$(document).on("pageshow", "#mail_message_page", function () {
    spark.views.message.init();
});