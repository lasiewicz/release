﻿(function ($) {
    $.widget("mobile.multiselect", $.mobile.widget, {
        hasChangedFlag: false,
        options: {
            attributeName: "",
            delimeter: ",",
            items: [],
            onChange: function (hasChanged) { },
            values: []
        },
        id: null,
        ignoreList: [],

        _create: function () {
            var self = this;
            var container = self.element;
            var attrName = container.attr("data-attrname");
            var values = (container.attr("data-values")) ? container.attr("data-values").split(self.options.delimeter) : null;
            self.id = container.attr("id");
            self.ignoreList = container.attr("data-ignore") ?  container.attr("data-ignore").split(",") : [];

            if (!values || !(values instanceof Array))
                values = self.options.values;

            container.addClass("multiselect");
            if (attrName && attrName !== "") {
                self._getOptions(attrName, function (success, response) {
                    if (!success)
                        return;

                    self._createOptions(response.data, values);
                });

                return;
            }

            self._createOptions(self.options.items, values);
        },
        
        _createOptions: function (data, values) {
            var self = this;
            if (data.length === 0)
                return;

            if (!(values instanceof Array))
                values = [];

            var ul = $("<ul></ul>");
            ul.bind("click", function (e) {
                var target = $(e.target);
                if (target.prop("tagName").toLowerCase() === "span") {
                    if (target.hasClass("checked")) {
                        target.removeClass("checked");
                        self._removeValue(target.attr("data-value"));
                    } else {
                        target.addClass("checked");
                        self._setValue(target.attr("data-value"));
                    }
                }
            });

            var temp = [];
            $.each(data, function (key, value) {
                if (value['Description'] == 'ATTOPT_250_BLANK') {
                    value['Description'] = '';
                }

                if (value['Description'] === "")
                    return;

                var li = $("<li />");
                var cb = $("<span />");
                var p = $("<p />").text(value["Description"]);

                cb.attr("data-value", value["Value"]);
                if ($.inArray(Number(value["Value"]), values) !== -1) {
                    cb.addClass("checked");
                    temp.push(value["Value"]);
                }

                cb.appendTo(li);
                p.appendTo(li);
                li.appendTo(ul);
            });

            if (temp.length !== 0) {
                self.element.attr("data-values", temp.join());
                self.options.values = temp;
            }

            ul.appendTo(this.element);
        },


        _getOptions: function (attrName, callback) {
            if (!attrName || attrName.length === 0)
                callback(false, null);

            spark.api.client.callAPI(spark.api.client.urls.attributeOptions, { attributeName: attrName }, null, function (success, response) {
                callback(success, response);

            });
        },

        _getValuesFromText: function (values) {
            var self = this;
            var items = self.element.find("ul li");
            var temp = [];
          
            items.each(function () {
                var o = $(this);
                var value = o.find("p").text();

                if ($.inArray(value, values) !== -1) {
                    temp.push(Number(o.find("span").attr("data-value")));
                }

            });

            return temp;

        },

        _hasChanged: function (values) {
            var options = this.options;
            var flag = this.hasChangedFlag;

            if (options.values.length !== values.length) {
                if (!flag) {
                    options.onChange(true);
                    this.hasChangedFlag = true;
                }

                return;
            }

            var length = options.values.length;
            for (var i = 0; i < length; i++) {
                if ($.inArray(options.values[i], values) !== -1) {
                    if (!flag) {
                        options.onChange(true);
                        this.hasChangedFlag = true;
                    }

                    return;
                }
            }

            this.hasChangedFlag = false;
            options.onChange(false);
        },

        _removeValue: function (value) {
            var e = this.element;
            var options = this.options;
            var arr;
            if (e.attr("data-values")) {
                arr = e.attr("data-values").split(options.delimeter);
            }
           // var arr = e.attr("data-values").split(options.delimeter);
            var values = [];

            for (var i = 0; i < arr.length; i++) {
                if(arr[i] !== value)
                    values.push(arr[i]);
            }
            
            this._hasChanged(values);
            e.attr("data-values", values.join());
        },
        
        _setSelected: function (values) {
            var self = this;
            var temp = [];

            values = self._getValuesFromText(values);
            self.element.find("ul li span:first-child").each(function () {
                var cb = $(this);
                var value = cb.attr("data-value");

                cb.removeClass("checked");
                if ($.inArray(Number(value), values) !== -1) {
                    cb.addClass("checked");
                    temp.push(Number(value));
                }
            });

            self.options.values = temp;
        },

        _setValue: function(value) {
            var e = this.element;
            var options = this.options;
            var values = [];

            if (e.attr("data-values") !== undefined && e.attr("data-values") !== "") {
                var arr = e.attr("data-values").split(options.delimeter);
                for (var i = 0; i < arr.length; i++)
                    values.push(arr[i]);
            }

            values.push(value);
            this._hasChanged(values)
            e.attr("data-values", values.join());
        },

        getValues: function () {
            var e = this.element;
            if (e.attr("data-values") !== undefined && e.attr("data-values") !== "")
                return e.attr("data-values").split(this.options.delimeter);
            return [];
        },

        getTextValues: function () {
            var temp = [];
            this.element.find("ul li span.checked").each(function () {
                var li = $(this).parent();

                temp.push(li.find("p").text());
            });

            return temp;
        },

        createOptions: function (name, values) {
            var self = this;
            var arr = values;
            var temp = [];
            if (self.element.find("ul li").length > 0) {
                self._setSelected(values);
                return;
            }


            self._getOptions(name, function (success, response) {

                if (!success)
                    return;

                var data = response.data;

                if (self.ignoreList.length > 0) {
                    // Filter the result(s) to remove unwanted item(s).
                    data = $.grep(data, function (item, index) {
                        if ($.inArray(item["Value"] + "", self.ignoreList) === -1) {
                            return true;
                        }
                    });
                }

                $.each(data, function (key, value) {
                    if ($.inArray(value["Description"], arr) !== -1) {
                        temp.push(value["Value"]);
                    }
                });

                self._createOptions(data, temp);
   
            });
        },

        setOnChangeListener: function (callback)
        {
            this.options.onChange = callback;
        },

        setValues: function (values) {
            this.hasChangedFlag = false;
            this._setSelected(values);
        }
    });

    // Initialize multi-select widgets automatically.
    $(document).bind("pagecreate create", function (e) {
        $(document).trigger("multiselectbeforecreate");
        $("div[data-type='multiselect']").each(function () {
            if (typeof ($(this).data('multiselect')) === "undefined") {
                $(this).multiselect();
            }
        });
    });
})(jQuery);