(function ($) {
    $.widget("mobile.multiselect", $.mobile.widget, {
        hasChangedFlag: false,
        options: {
            attributeName: "",
            delimeter: ",",
            items: [],
            onChange: function (hasChanged) { },
            values: []
        },

        _create: function () {
            var self = this;
            var container = self.element;
            var attrName = container.attr("data-attrname");
            var values = (container.attr("data-values")) ? container.attr("data-values").split(self.options.delimeter) : null;

            if (!values || !(values instanceof Array))
                values = self.options.values;

            container.addClass("multiselect");
            if (attrName && attrName !== "") {
                self._getOptions(attrName, function (success, response) {
                    if (!success)
                        return;

                    self._createOptions(response.data, values);
                });

                return;
            }

            self._createOptions(self.options.items, values);
        },
        
        _createOptions: function (data, values) {
            var self = this;
            if (data.length === 0)
                return;

            if (!(values instanceof Array))
                values = [];

            var ul = $("<ul></ul>");
            ul.bind("click", function (e) {
                var target = $(e.target);

                if (target.prop("tagName").toLowerCase() === "span") {
                    if (target.hasClass("checked")) {
                        target.removeClass("checked");
                        self._removeValue(target.attr("data-value"));
                     } else {
                        target.addClass("checked");
                        self._setValue(target.attr("data-value"));
                     }
                }
            });

            var temp = [];
            $.each(data, function (key, value) {
                if (value['Description'] == 'ATTOPT_250_BLANK') {
                    value['Description'] = '';
                }

                if (value['Description'] === "")
                    return;

                var li = $("<li />");
                var cb = $("<span />");
                var p = $("<p />").text(value["Description"]);

                cb.attr("data-value", value["Value"]);
                if ($.inArray(Number(value["Value"]), values) !== -1) {
                    cb.addClass("checked");
                    temp.push(value["Value"]);
                }

                cb.appendTo(li);
                p.appendTo(li);
                li.appendTo(ul);
            });

            if (temp.length !== 0) {
                self.element.attr("data-values", temp.join());
                self.options.values = temp;
            }

            ul.appendTo(this.element);
        },

        _getOptions: function (attrName, callback) {
            if (!attrName || attrName.length === 0)
                callback(false, null);

            spark.api.client.callAPI(spark.api.client.urls.attributeOptions, { attributeName: attrName }, null, function (success, response) {
                callback(success, response);
                console.log("response", response.data);
            });
        },

        _getValuesFromText: function (values) {
            var self = this;
            var items = self.element.find("ul li");
            var temp = [];

            items.each(function () {
                var o = $(this);
                var value = o.find("p").text();

                if ($.inArray(value, values) !== -1) {
                    temp.push(Number(o.find("span").attr("data-value")));
                }
            });

            return temp;
        },

        _hasChanged: function (values) {
            var options = this.options;
            var flag = this.hasChangedFlag;

            if (options.values.length !== values.length) {
                if (!flag) {
                    options.onChange(true);
                    this.hasChangedFlag = true;
                }

                return;
            }

            var length = options.values.length;
            for (var i = 0; i < length; i++) {
                if ($.inArray(options.values[i], values) !== -1) {
                    if (!flag) {
                        options.onChange(true);
                        this.hasChangedFlag = true;
                    }

                    return;
                }
            }

            this.hasChangedFlag = false;
            options.onChange(false);
        },

        _removeValue: function (value) {
            var e = this.element;
            var options = this.options;
            var arr = e.attr("data-values").split(options.delimeter);
            var values = [];

            for (var i = 0; i < arr.length; i++) {
                if(arr[i] !== value)
                    values.push(arr[i]);
            }
            
            this._hasChanged(values);
            e.attr("data-values", values.join());
        },
        
        _setSelected: function (values) {
            var self = this;
            var temp = [];

            values = self._getValuesFromText(values);
            self.element.find("ul li span:first-child").each(function () {
                var cb = $(this);
                var value = cb.attr("data-value");

                cb.removeClass("checked");
                if ($.inArray(Number(value), values) !== -1) {
                    cb.addClass("checked");
                    temp.push(Number(value));
                }
            });

            self.options.values = temp;
        },

        _setValue: function(value) {
            var e = this.element;
            var options = this.options;
            var values = [];
            if (e.attr("data-values") !== undefined && e.attr("data-values") !== "") {
                var arr = e.attr("data-values").split(options.delimeter);
                for (var i = 0; i < arr.length; i++)
                    values.push(arr[i]);
            }

            values.push(value);

            this._hasChanged(values)
            e.attr("data-values", values.join());
        },

        getValues: function () {
            var e = this.element;
            if (e.attr("data-values") !== undefined && e.attr("data-values") !== "")
                return e.attr("data-values").split(this.options.delimeter);

            return [];
        },

        getTextValues: function () {
            var temp = [];
            this.element.find("ul li span.checked").each(function () {
                var li = $(this).parent();

                temp.push(li.find("p").text());
            });

            return temp;
        },

        createOptions: function (name, values) {
            var self = this;
            var arr = values;
            var temp = [];

            if (self.element.find("ul li").length > 0) {
                self._setSelected(values);
                return;
            }

            self._getOptions(name, function (success, response) {
                if (!success)
                    return;

                var data = response.data;
                $.each(data, function (key, value) {
                    if ($.inArray(value["Description"], arr) !== -1) {
                        temp.push(value["Value"]);
                    }
                });

                self._createOptions(data, temp);
            });
        },

        setOnChangeListener: function (callback)
        {
            this.options.onChange = callback;
        },

        setValues: function (values) {
            this.hasChangedFlag = false;
            this._setSelected(values);
        }
    });

    // Initialize multi-select widgets automatically.
    $(document).bind("pagecreate create", function (e) {
        $(document).trigger("multiselectbeforecreate");
        $("div[data-type='multiselect']").each(function () {
            if (typeof ($(this).data('multiselect')) === "undefined") {
                $(this).multiselect();
            }
        });
    });
})(jQuery);

(function ($) {
    $.widget("mobile.photoviewer", $.mobile.widget, {
        controlVisible: true,
        options: {
            onBeforeHide: function () { },
            onBeforeShow: function () { }
        },
        swiper: null,

        _create: function () {
            var self = this;
            var container = self.element;

            container.bind("click", function (e) {
                var o = e.target;
                if (!$(o).hasClass("photo-viewer"))
                    return;

                container.hide();
            });

            $(window).resize(function () {
                self._resize();
            });

            container.hide();
        },

        _getDimension: function (width, height, maxWidth, maxHeight) {
            var ratio = 1;

            if (width > maxWidth || height > maxHeight) {
                ratio = Math.min(maxWidth / width, maxHeight / height);
            }

            return [(width * ratio), (height * ratio)];
        },

        _hide: function () {
            if (typeof this.options.onBeforeHide === "function")
                this.options.onBeforeHide();

            var container = this.element;
            container.fadeOut(350);
        },

        _resize: function () {
            var self = this;
            var parent = this.element;
            var swiperContainer = parent.find(".swiper-container");
            var prevButton = $("a.prev");
            var nextButton = $("a.next");
            var ul = parent.find("ul.swiper-wrapper");
            var width = $(window).width();
            var height = ($(window).height());

            prevButton.css({
                top: ((height - prevButton.height()) / 2) + "px"
            });

            nextButton.css({
                top: ((height - prevButton.height()) / 2) + "px"
            });

            swiperContainer.css({
                height: height,
                width: width
            });

            ul.children("li").each(function () {
                var e = $(this);
                var img = e.find("img");
                var dimension = self._getDimension(img.attr("data-clientwidth"), img.attr("data-clientheight"), width, height);
                var left = (width - dimension[0]) / 2;
                var top = (height - dimension[1]) / 2;

                e.css({
                    height: height,
                    position: "relative",
                    width: width
                });

                img.css({
                    width: dimension[0] + "px",
                    height: dimension[1] + "px",
                    top: top + "px",
                    left: left + "px"
                });
            });
        },

        setBeforeHideListener: function (callback) {
            this.options.onBeforeHide = callback;
        },

        setBeforeShowListener: function (callback) {
            this.options.onBeforeShow = callback;
        },

        show: function (photos, selectedIndex) {
            var self = this;
            if (typeof self.options.onBeforeShow === "function")
                self.options.onBeforeShow();

            var parent = this.element;
            //var container = $("<div class=\"container\" />");
            var swiperContainer = $("<div class=\"swiper-container\" />");
            var ul = $("<ul class=\"swiper-wrapper\" />");
            //var buttonContainer = $("<div class=\"button-container\" />");
            var prevButton = $("<a id=\"btn_prev_photo\" class=\"prev\" />");
            var nextButton = $("<a id=\"btn_next_photo\" class=\"next\" />");
            //var playButton = $("<button class=\"play\" />");
            var closeButton = $("<a id=\"btn_close_photo\" class=\"close\" />");
            var width = $(window).width();
            var height = $(window).height();

            parent.empty();

            swiperContainer.css({
                height: height,
                width: width
            });

            var length = photos.length;
            $.each(photos, function (index, item) {
                var li = $("<li class=\"swiper-slide\" />");
                var img = $("<img />");
                var div = $("<div />")
                var caption = $("<p class=\"caption\" />");
                var photoCounter = $("<div class=\"counter\"/>");

                li.css({
                    height: height,
                    position: "relative",
                    width: width
                });

                img.bind("load", function (o) {
                    var e = o.currentTarget;
                    var dimension = self._getDimension(e.clientWidth, e.clientHeight, width, height);
                    var left = (width - dimension[0]) / 2;
                    var top = (height - dimension[1]) / 2;

                    $(e).attr("data-clientheight", e.clientHeight);
                    $(e).attr("data-clientwidth", e.clientWidth);
                    $(e).css({
                        width: dimension[0] + "px",
                        height: dimension[1] + "px",
                        top: top + "px",
                        left: left + "px",
                        visibility: "visible"
                    });
                });

                img.attr("id", "photo_" + index);
                img.attr("src", item.imagePath);
                img.css({ visibility: "hidden" });

                caption.text(item.caption);
                photoCounter.text((index + 1) + " of " + length);

                caption.appendTo(div);
                photoCounter.appendTo(div);

                img.appendTo(li);
                div.appendTo(li);
                li.appendTo(ul);
            });

            /*ul.off("click").on("click", function (e) {
                var target = $(e.target);

                if (target.is("li") || target.is("img"))
                    self.toggleControl(target);
            });*/
            ul.appendTo(swiperContainer);

            prevButton.css({
                top: ((height - prevButton.height()) / 2) + "px"
            });

            nextButton.css({
                top: ((height - prevButton.height()) / 2) + "px"
            });

            closeButton.appendTo(parent);
            prevButton.appendTo(parent);
            //playButton.appendTo(parent);
            nextButton.appendTo(parent);
            swiperContainer.appendTo(parent);
            //buttonContainer.appendTo(parent);

            // Initialize swiper
            self.swiper = swiperContainer.swiper({
                //autoplay: 2500,
                //autoplayStopOnLast: true,
                /*cssWidthAndHeight: false,*/
                initialSlide: selectedIndex,
                mode: 'horizontal',
                loop: false,
                onSlideChangeEnd: function (o, directioin) {
                    //if (o.activeIndex === (photos.length - 1))
                }
            });

            // Stop auto-play so can manually start it.
            //swiper.stopAutoplay();
            closeButton.bind("click", function () {
                self._hide();
                $("footer").show();
            });

            prevButton.bind("click", function () {
                self.swiper.swipePrev();
            });

            //playButton.bind("click", function () {
            //    swiper.startAutoplay();
            //});

            nextButton.bind("click", function () {
                self.swiper.swipeNext();
            });

            //container.appendTo(parent);
            parent.fadeIn(350);
        },

        toggleControl: function (target) {
            if (this.controlVisible) {
                $("#btn_prev_photo").fadeOut();
                $("#btn_next_photo").fadeOut();
                $("#btn_close_photo").fadeOut();

                target.find("> div").fadeOut();

                this.controlVisible = false;
            } else {
                $("#btn_prev_photo").fadeIn();
                $("#btn_next_photo").fadeIn();
                $("#btn_close_photo").fadeIn();

                target.find("> div").fadeIn();

                this.controlVisible = true;
            }
        }
    });

    $(document).bind("pagecreate create", function (e) {
        $(document).trigger("photoviewerbeforecreate");
        $("div[data-type='photoviewer']").each(function () {
            if (typeof ($(this).data('photoviewer')) === "undefined") {
                $(this).photoviewer();
            }
        });
    });
})(jQuery);