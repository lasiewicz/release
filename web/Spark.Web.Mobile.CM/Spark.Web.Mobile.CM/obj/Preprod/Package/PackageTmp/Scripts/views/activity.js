﻿spark.views.activity = (function () {
    var self = {};
    var activityList = [];
    var activeType = "views";
    var currentDate = new Date();
    var page;
    var pageSize = 20;
    var preferences = null;
    var pageNo = 1;
    var isLoading = false;
    var isLastPage = false;
    var isNewPrefs = true;
    var initialLoad = false;
    var ul = $("#activity_list");
    function addNoResult(ul) {
        ul.append("<li class=\"no-results activity-no-results\"><p>What are you waiting for?<br /> <span>Start searching and communicating right now</span></p></li>");
    };

    function applyFilter(type) {
        var ul = $("#activity_list");
        var filterType = getFilterType(type);
        var temp = [];

        ul.empty();
        if (activityList !== null) {
            temp = $.grep(activityList, function (item, index) {
                return (item.NotificationType === filterType || filterType === 0);
            });
        }

        appendToList(temp);
    };

    function addLabel(today, activity, ul) {
        var d = new Date(activity.actionDate);
        var diff = today.getTime() - d.getTime();
        var seconds = Math.floor((diff / 1000));
        var minutes = Math.floor((seconds / 60));
        var hours = Math.floor((minutes / 60));
        var days = Math.floor((hours / 24));

        if (days === 1 && ul.find("li.yesterday").length === 0)
            $("<li class=\"label yesterday\">Yesterday</li>").appendTo(ul);
        if ((days > 1 && days <= 7) && ul.find("li.week").length === 0)
            $("<li class=\"label week\">1 week ago</li>").appendTo(ul);
        if ((days > 7 && days <= 31) && ul.find("li.month").length === 0)
            $("<li class=\"label month\">1 month ago</li>").appendTo(ul);
        if (days > 31 && ul.find("li.old").length === 0)
            $("<li class=\"label old\">Old activities</li>").appendTo(ul);
    };



    var hotListOptionsMap = {
        "views": "views_received",
        "chats": "ims_received",
        "smiles": "smiles_received",
        "email": "emails_received",
        "favorites": "added_you_to_favorites",
        "viewed": "views_sent",
        "chatted": "ims_sent",
        "smiled": "smiles_sent",
        "emailed": "emails_sent",
        "favorited": "your_favorites"
    };

    
    function getAttributeOptionName(obj,property) {
        return obj[property];
    };

    function getHotListOptionName(property) {
        return hotListOptionsMap[property];
    };

    function getHotlistCategory(category) {
        if (isLoading || isLastPage)
            return;

        var ul = $("#activity_list");
        isLoading = true;
        pageNo = Number(page.attr("data-pageno"));
        page.attr("data-pageno", (pageNo + 1));

        var url = spark.api.client.urls.hotlist;
        var params = {
            hotlist: category,
            pageSize: pageSize,
            pageNumber: pageNo
        };

        spark.utils.views.addLoadingPanel(ul);
        spark.api.client.callAPI(url, params, null, function (success, response) {
            if (!success) {
                spark.utils.views.removeLoadingPanel(ul);

                return;
            }

            var length = response.data.length;
            if (length < pageSize)
                isLastPage = true;

           
            var activityTab = $("#activity_tabs li a.active").attr("href");
            activityTab= activityTab.substring(activityTab.indexOf("/") + 1);

          

            spark.utils.views.appendToList(ul, response.data, function (index, item) {
                var profile = item.miniProfile;
                var li = $("<li />");
                var a = $("<a data-role=\"none\" />");
                var img = $("<img />");
                var p = $("<p />");
                var userName = $("<span class=\"primary\" />");
                var isSub = spark.utils.common.isSub();
                var photo = profile.defaultPhoto;
                var thumbnail = (profile.gender === "Female") ?
                    "/images/hd/img_silhouette_woman@2x.png" : "/images/hd/img_silhouette_man@2x.png";

                addLabel(currentDate, item, ul);

                if (item.miniProfile.blockContactByTarget || item.miniProfile.selfSuspendedFlag || item.miniProfile.adminSuspendedFlag) {
                    li.addClass("block");
                    a.attr("href", "#");
                } else if ((activityTab === "chats" || activityTab === "chatted" || activityTab === "email" || activityTab === "emailed")) {
                    a.attr("href", "/mail");
                } else {
                    a.attr("href", "/profile/" + profile.id);
                }

                if (photo && photo.thumbPath && photo.thumbPath.length > 0) {
                    thumbnail = photo.thumbPath;
                }

                img.attr("src", thumbnail);
                img.appendTo(a);
                var username = shortname(profile.username);

                var attributeOptionsMap = {
                    "views": username + " viewed your profile",
                    "chats": username + " sent you a chat request",
                    "smiles": username + " sent you a smile",
                    "email": username + " sent you a message",
                    "favorites": username + "  added you to their favorites",
                    "viewed": "You viewed  "+ username +"'s profile",
                    "chatted": 'You sent ' + username + ' a chat request',
                    "smiled": 'You sent ' + username + ' a smile',
                    "emailed": 'You sent ' + username + ' an email',
                    "favorited": 'You added ' + username  + '  to your favorites'

                };

                userName.text(getAttributeOptionName(attributeOptionsMap, activityTab));
                userName.appendTo(p);

                p.appendTo(a);
                a.appendTo(li);
                if (profile.isHighlighted)
                    li.addClass('member-highlight');
                return li;
            }, function () {
                isLoading = false;
                if (length === 0 && ul.find("li").length === 0) {
                    addNoResult(ul);
                } else if (isLastPage) {
                    spark.utils.views.removeItemBorder(ul);
                }
                //$('li a.'+activityTab+' span.badge').hide();
            });
        });
    };


    function blockContact(username, gender) {
        spark.utils.common.showGenericDialog('');
        $('#generic_dialog').addClass('lift');
        $('#generic_dialog .content').html('<b>Sorry!</b><br/>You can\'t communicate with ' + username + ' because you\'ve blocked ' + gender + '.');
    };

    function blockContactByTarget(username) {
        spark.utils.common.showGenericDialog('');
        $('#generic_dialog .content').html('<b>Sorry!</b><br/>' + username + ' is currently unavailable at this time');
        $('#generic_dialog').addClass('lift');
    };

    function selfSuspended(username, gender) {
        spark.utils.common.showGenericDialog('');
        $('#generic_dialog .content').html('<b>Sorry!</b><br/>' + username + ' either became a Success Story or temporarily removed ' + gender + ' account.');
        $('#generic_dialog').addClass('lift');
    };

    function adminSuspended() {
        spark.utils.common.showGenericDialog('');
        $('#generic_dialog .content').html("<b>Profile Unavailable </b></br/>This profile has been removed from the site.Try browsing more profiles!");
        $('#generic_dialog').addClass('lift');
    };

    function bindEvents() {
        var refreshButton = $("#btn_refresh");
        var incomingButton = $("#btn_incoming_refresh");
        var outgoingButton = $("#btn_outgoing_refresh");

        refreshButton.off().click(function () {
            refresh();
        });

        outgoingButton.off().click(function () {
            switchOutgoing();
        });

        incomingButton.off().click(function () {
            switchIncoming();
        });

        $(document).on("tap", "li.blockContact", function (username, gender) {
            username = $(this).find("span.primary").attr("data-name");
            gender = ($(this).find("span.primary").attr("data-gender") === "Male" ? "his" : "her");
            blockContact(username, gender);
        });

        $(document).on("tap", "li.blockContactByTarget", function (username) {
            username = $(this).find("span.primary").attr("data-name");
            blockContactByTarget(username);
        });

        $(document).on("tap", "li.selfSuspended", function (username, gender) {
            username = $(this).find("span.primary").attr("data-name");
            gender = ($(this).find("span.primary").attr("data-gender") === "Male" ? "his" : "her");
            selfSuspended(username, gender);
        });

        $(document).on('tap', 'li.adminSuspended', function () {
            adminSuspended();
        });


    }; 
    //bindTabEvents;



    function bindTabEvents() {
        var tabs = $("#activity_tabs");

        tabs.off().click(function (e) {
            var target = $(e.target);
            var notificationType;

            if (target.is("a") || target.is("span")) {
                notificationType = target.attr("href");
                if (typeof notificationType === "undefined")
                    notificationType = target.parent().attr("href");

                notificationType = notificationType.substring(notificationType.indexOf("/") + 1);
                spark.utils.common.isPayingMember($("#activity_tabs").data("memberid"));

                if (!spark.utils.common.isPayingMember($("#activity_tabs").data("memberid"))) {
                    if (notificationType === "email" || notificationType === "chats" || notificationType === "chatted" || notificationType === "emailed") {
                        window.location.href = "/subscription/subscribe/4161";
                    } else {
                        changeTab(notificationType);
                    }

                } else {
                    changeTab(notificationType);
                }
                

            }
            e.preventDefault();
        });
    };

    function shortname(name) {
        var username = name;
        if (username.length > 13) {
            var shortName = username.slice(0, 13) + "...";
            return shortName;
        } else {
            return username;
        }
    }

    function clearHotListCounts(category) {
        spark.api.client.callAPI(spark.api.client.urls.eventSeen, null, { "hotListCategory": category }, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
        });
    };

    function changeTab(type) {
        var tabs = $("#activity_tabs");

        pageSize = page.attr("data-pagesize");
        pageNo = 1;
        page.attr("data-pageno", pageNo);


        isLoading = false;
        isLastPage = false;


        activeType = type;
        if (type === "viewed") {
            activeType = "views";
        } else if (type === "smiled") {
            activeType = "smiles";
        } else if (type === "emailed") {
            activeType = "email";
        } else if (type === "favorited") {
            activeType = "favorites";
        } else if (type === "chatted") {
            activeType = "chats";
        }

        tabs.find("li a").removeClass("active");
        tabs.find("li a." + activeType).addClass("active");

        getHotlist(type);
    };


 

    function getFilterType(type) {
        if (type === "smiles")
            return 2;
        if (type === "email")
            return 3;
        if (type === "favorites")
            return 4;
        if (type === "views")
            return 1;

        return 0;
    };

    function getHotlist(type, scrolling) {
        var ul = $("#activity_list");
        var hotlistCategory;
        if (!scrolling)
            spark.utils.views.clearList(ul);

        var category = getHotListOptionName(type);
        getHotlistCategory(category);

         clearHotListCounts(getHotListOptionName(type));

        showBadge();
    };


    function ignoreNotification(type) {
        if (type !== 1 && type !== 2 && type !== 3 && type !== 4)
            return true;

        return false;
    };

    function toggleActivityButtons() {
        var refreshButton = $("#btn_refresh");
        var outgoingButton = $("#btn_outgoing_refresh");

        refreshButton.toggleClass("unselected");
        outgoingButton.toggleClass("unselected");
    }

    function refresh() {
        var activeTab = $(".active").attr("href");
        activeType = activeTab.substring(activeTab.indexOf("/") + 1);

        pageSize = page.attr("data-pagesize");
        pageNo = 1;
        page.attr("data-pageno", pageNo);

        isLoading = false;
        isLastPage = false;
        getHotlist(activeType);
    };

    function switchIncoming() {
        var incomingButton = $("#btn_incoming_refresh");
        var outgoingButton = $("#btn_outgoing_refresh");

        incomingButton.removeClass("unselected");
        outgoingButton.addClass("unselected");

        var tabs = $("#activity_tabs li a");

        tabs.each(function () {
            if ($(this).hasClass("views")) {
                $(this).attr("href", "activity/views");
            } else if ($(this).hasClass("smiles")) {
                $(this).attr("href", "activity/smiles");
            } else if ($(this).hasClass("email")) {
                $(this).attr("href", "activity/email");
            } else if ($(this).hasClass("favorites")) {
                $(this).attr("href", "activity/favorites");
            } else if ($(this).hasClass("chats")) {
                $(this).attr("href", "activity/chats");
            }
        });
        showBadge();

        var activeTab = $(".active").attr("href");
        activeType = activeTab.substring(activeTab.indexOf("/") + 1);

        pageSize = page.attr("data-pagesize");
        pageNo = 1;
        page.attr("data-pageno", pageNo);

        isLoading = false;
        isLastPage = false;
        getHotlist(activeType);
        bindTabEvents();
    }

    function switchOutgoing() {
        var incomingButton = $("#btn_incoming_refresh");
        var outgoingButton = $("#btn_outgoing_refresh");

        incomingButton.addClass("unselected");
        outgoingButton.removeClass("unselected");

        var tabs = $("#activity_tabs li a");

        tabs.each(function () {
            if ($(this).hasClass("views")) {
                $(this).attr("href", "activity/viewed");
            } else if ($(this).hasClass("smiles")) {
                $(this).attr("href", "activity/smiled");
            } else if ($(this).hasClass("email")) {
                $(this).attr("href", "activity/emailed");
            } else if ($(this).hasClass("favorites")) {
                $(this).attr("href", "activity/favorited");
            } else if ($(this).hasClass("chats")) {
                $(this).attr("href", "activity/chatted");
            }
        });

        var activeTab = $(".active").attr("href");
        activeType = activeTab.substring(activeTab.indexOf("/") + 1);

        pageSize = page.attr("data-pagesize");
        pageNo = 1;
        page.attr("data-pageno", pageNo);

        isLoading = false;
        isLastPage = false;
        getHotlist(activeType);

        bindTabEvents();
        $("li a span.badge").hide();
    };


    function showBadge() {
        var tabs = $("#activity_tabs");
        var count;

        spark.api.client.callAPI(spark.api.client.urls.hotlistCount, null, null, function (success, response) {
            if (!success) {
                return;
            }

                var counts = response.data;
                if (counts.viewedMyProfileNew > 0) {
                    tabs.find("li a.views span:eq(1)").text(counts.viewedMyProfileNew).show();
                } else if (counts.viewedMyProfileNew === 0 && $("a.views > span.badge").text() !== "") {
                    setTimeout(function () {
                        tabs.find("li a.views span:eq(1)").css("display", "none");
                    }, 3000);
                }

                if (counts.addedMeToTheirFavoritesNew > 0) {
                    tabs.find("li a.favorites span:eq(1)").text(counts.addedMeToTheirFavoritesNew).show();
                } else if (counts.addedMeToTheirFavoritesNew === 0 && $("a.favorites > span.badge").text() !== "") {
                    setTimeout(function () {
                        tabs.find("li a.favorites span:eq(1)").css("display", "none");
                    }, 3000);
                }
                if (counts.Inbox.MissedIM > 0) {
                    tabs.find("li a.chats span:eq(1)").text(counts.Inbox.MissedIM).show();
                } else if (counts.Inbox.MissedIM === 0 && $("a.chats > span.badge").text() !== "") {
                    setTimeout(function () {
                        tabs.find("li a.chats span:eq(1)").css("display", "none");
                    }, 3000);
                }

                if (counts.Inbox.Email > 0) {
                    tabs.find("li a.email span:eq(1)").text(counts.Inbox.Email).show();
                } else if (counts.Inbox.Email === 0 && $("a.email > span.badge").text() !== "") {
                    setTimeout(function () {
                        tabs.find("li a.email span:eq(1)").css("display", "none");
                    }, 3000);
                }


                if (counts.Inbox.Tease > 0) {
                    tabs.find("li a.smiles span:eq(1)").text(counts.Inbox.Tease).show();
                } else if (counts.Inbox.Tease === 0 && $("a.smiles > span.badge").text() !== "") {
                    setTimeout(function () {
                        tabs.find("li a.smiles span:eq(1)").css("display", "none");
                    }, 3000);
                }

            return count;
        });
    };

    //dynamically resize the overlay
    function resizeOverlay() {
        var screenWidth = $(window).width();
        var iframeWidth = $(".gpt-container-overlay div iframe").width();
        var marginLeft = ((screenWidth - iframeWidth) / 2);
        var leftBtn = (iframeWidth - 56);
        var elClose = $('<div id="gpt-closeButton" class="gpt-closeButton"></div>');

        if (iframeWidth < screenWidth) {
            $(".gpt-container-overlay").addClass("blackout");
            $(".gpt-container-overlay div:nth-child(1)").css('marginLeft', marginLeft);
            $(".gpt-container-overlay div:nth-child(1)").append(elClose);
            $("div#gpt-closeButton.gpt-closeButton").css('left', leftBtn);
        }
    };

    self.init = function () {
        bindEvents();
        bindTabEvents();
        resizeOverlay();

        showBadge();
        initialLoad = true;
        isNewPrefs = true;
        page = $("#activity_page");
        preferences = null;

        $("a.views").click();
    };

    self.resize = resizeOverlay;

    self.showSecretAdmirer = function () {
        $('#activity_page .activity_list').hide();
        spark.utils.common.showLoading("Loading...");

        spark.views.admirer.getMatchPreference(function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            var batchSize = 25;
            var data = response.data;
            var prefs = {
                minAge: data.minAge,
                maxAge: data.maxAge,
                maxDistance: data.maxDistance,
                location: (data.location && data.location !== "null") ? data.location : "",
                isNewPrefs: isNewPrefs
            };

            preferences = prefs;
            spark.views.admirer.getMemberSlideShow(batchSize, preferences, function (success, response) {
                var members = response.data;

                isNewPrefs = false;
                spark.utils.common.hideLoading();
                $("#secret_admirer").secretadmirer("show", members, preferences, batchSize);
            });
        });
    };

    self.activeType = activeType;
    self.getHotlist = getHotlist;

    return self;
})();

$(document).on("pageshow", "#activity_page", function () {
    spark.views.activity.init();
    spark.views.admirer.init();
    setTimeout(function () {
        googletag.pubads().refresh([outofpage]);
        googletag.pubads().refresh([gpt_activity_top]);
    }, 900);

    $(".gpt-container-overlay").removeClass("blackout");

    $("#btn_secret_admirer").click(function () {
        spark.views.activity.showSecretAdmirer();
    });

    $(document).on('click', '#secret_admirer .btn-close', function () {
        $('#activity_page .activity_list').show();
    });


    //close the overlay
    $('body').click(function () {
        $('.gpt-container-overlay').hide();
    });

    if (screen.height < 500) {
        $("div#phone.phone").css("height", "246px");
    }

    // resize the overlay on orietnation change
    $(window).off("orientationchange").on("orientationchange", function (event) {
        spark.views.activity.resize();
    });
});