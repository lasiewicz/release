﻿spark.views.settings = (function() {
    var self = {};
    var location;
    function getRegionId(regionId){
        var params = {
            "languageID": "1",
            "regionID": regionId
        };

        spark.api.client.callAPI(spark.api.client.urls.getRegionHierarchy, params, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
          

            if (response.data.City != null) {
                location = response.data.City.Description + ", " + response.data.State.Abbreviation;
                $(".seeking-location").text(location);
            }

            if (response.data.Country != null) {
                $(".edit-city").text(response.data.Country.Description);
            }

            if (response.data.PostalCode != null) {
                $(".edit-zip").text(response.data.PostalCode.Description);
                $("#zip_code_form input#postal_code").val(response.data.PostalCode.Description).attr('placeholder', response.data.PostalCode.Description);
            }
              
        });
    }

 
    function bindClickEvents() {

        $(".location-toggle").off().on('click', function () {
            $('#location-prefs').show();
            $("#spotlight-prefs").hide();
        });


        $(".spotlight.prevButton").off().on('click', function () {
            $('#location-prefs').hide();
            $("#spotlight-prefs").show();
        });

        $('.spotlight-display li').on('click', function () {
            //showEdit("basic_info_form");
        });
        $('li.edit-age').on('click', function () {
            showEdit("age_range_form");
        });

        $('.spotlight-location li.distance').on('click', function () {
            showEdit("distance_form");
        });
        $('.spotlight-location li.zip').on('click', function () {
            showEdit("zip_code_form");
        });
        $('ul.suspend-reason li').on('tap', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $('ul.suspend-reason li').removeClass('selected');
                $(this).addClass('selected');
            }
        });

        //$('div#account-prefs.self-suspend a#back_btn.back-button.ui-link').on("tap", function () {
        //    alert("yo");
        //});

        $('a.remove-profile').on("tap", removeProfile);
        $('div#account-prefs.self-suspend a#back_btn.back-button.ui-link').on("tap", backButton);
        $("a.remove-profile-from-story").on("tap", removeProfileWithStory);
     

        $('div#removing-profile-terms ul li a.continue').on('tap', function (e) {
            e.preventDefault();
            if (!$('span.checkbox.remove-terms').hasClass('checked')) {
                spark.utils.common.showGenericDialog("You must accept the terms to continue");

            } else {
                $("div#removing-profile-terms").addClass("hidden");
                $("div#removing-profile-reason").removeClass("hidden");
            }
           

        });

        $('span.checkbox.remove-terms').on('tap', function() {
            $(this).toggleClass('checked');
        });

        $('div#need_improvement ul li').on('tap', function () {
           
            var li = $("div#need_improvement ul li");
            $(this).find('span').toggleClass('checked');

            $("#need_improvement ul li span.checked").each(function (index, element) {
                var arr = [];
            });

            for (var i = 0; i < $("#need_improvement ul li span.checked").length; i++) {
                var arr = [];
                arr.push( $("#need_improvement ul li span.checked").attr("data-value"));
                //$("div#need_improvement").attr("data-values", arr);
            }
        });

        $('div#need_improvement ul li:last-child').on('tap', function () {
            $("textarea#other_explanation").toggleClass("hidden");

        });


        $('ul#need_improvement li a.remove-profile').on('tap', function (e) {
        });

        $('div#share_story ul li:first-child').on('tap', function () {
            $("textarea#story").toggleClass("hidden");
        });

        $("div#share_story ul li").on("tap", function () {
            var li = $(this).find('span').toggleClass('checked');
            var index = $(this).find('span').attr("data-index");

            if ($("div#share_story ul li span.checked").length > 1) {
                $("div#share_story ul li span.checked:not([data-index='" + index + "'])").removeClass("checked");
                
                if ((!$("textarea#story").hasClass("hidden"))) {
                    if (index !== 0) {
                        $("textarea#story").addClass("hidden");
                    }
                   
                }

            }
            
        });

       
    } // ends bind click events

    $('select.explainReason').on('change', function () {
        if (this.value === 7) {
            $("textarea#explainReason").removeClass("hidden");
        }
        else {
            if (!$("textarea#explainReason").hasClass("hidden")) {
                $("textarea#explainReason").addClass("hidden");
            } 
        }
    });

    function removeProfile() {
        //check for selected reason, if not show
        //e.preventDefault();
        var suspendReasonId = $("div#removing-profile option:selected").val();
        var explainReason = $("textarea#explainReason").val();
        var improveIds = [];
        var explainImprovements = $("textarea#other_explanation").val();
        var likelyhoodCount = $("select.likelyhoodCount option:selected").val();
        var shareStory = $("div#share_story ul li span.checked").attr("data-value");
        var story = $("textarea#story").val();

        var dataList = $("#need_improvement ul li span.checked").map(function () {
            return $(this).data("value");
        }).get();
        dataList.join(',');
        improveIds = dataList;

        var params = {
            suspendReasonId: suspendReasonId,
            explainReason: explainReason,
            improveIds: improveIds,
            explainImprovements: explainImprovements,
            likelyhoodCount: likelyhoodCount

        }


        if (suspendReasonId === "" || likelyhoodCount === "" || ($("div#need_improvement ul li span.checked").length < 1)) {
            var errorString = "Please select a reason for removing your profile.";
            spark.utils.common.showGenericDialog(errorString);

        }


        if (suspendReasonId === "1" || suspendReasonId === "2") {
            if (likelyhoodCount == "" || ($("div#need_improvement ul li span.checked").length < 1)) {
                spark.utils.common.showGenericDialog("Please select a reason for removing your profile.");
            } else {
                $('div#removing-profile-reason').addClass("hidden");
                $('div#shareStory').removeClass("hidden");
                $('html, body').animate({ scrollTop: 0 }, 'fast');
            }
           
        } else {
            spark.api.client.callAPI(spark.api.client.urls.selfSuspend, null, params, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }
                var memberId = $("div.page-header").attr("data-memberid");
                spark.api.client.callAPI(spark.api.client.urls.fullProfile, { targetMemberId: memberId }, null, function (success, response) {
                    if (!success) {
                        spark.utils.common.showError(response);
                    }
                })

                window.location = "/home/selfsuspend";
            });

        }

        if ($("div#need_improvement ul li span.checked").length < 1 ) {

        }

    }
    function removeProfileWithStory() {
        var suspendReasonId = $("div#removing-profile option:selected").val();
        var explainReason = $("textarea#explainReason").val();
        var improveIds = [];
        var explainImprovements = $("textarea#other_explanation").val();
        var likelyhoodCount = $("select.likelyhoodCount option:selected").val();
        var shareStory = $("div#share_story ul li span.checked").attr("data-value");
        var story = $("textarea#story").val();

        var params = {
            suspendReasonId: suspendReasonId,
            explainReason: explainReason,
            improveIds: improveIds,
            explainImprovements: explainImprovements,
            likelyhoodCount: likelyhoodCount,
            shareStory: shareStory,
            story:story
        }

        if ($("div#share_story ul li span.checked").length < 1) {
            spark.utils.common.showGenericDialog("Please select one");
        } else {
            spark.api.client.callAPI(spark.api.client.urls.selfSuspend, null, params, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                } else {
                    
                }
                window.location = "/home/selfsuspend";
            });

            //window.location = "/home/selfsuspend";
        }

    }

    function backButton() {

        if ((!$("div#removing-profile-terms").hasClass("hidden"))) {
            window.location = '/settings/account';
        }

       else if (!($("div#removing-profile-reason").hasClass("hidden"))) {
           $("div#removing-profile-reason").addClass("hidden");
           $("div#removing-profile-terms").removeClass("hidden");
       }
       else if (!($("div#shareStory").hasClass("hidden"))) {
           $("div#shareStory").addClass("hidden");
           $("div#removing-profile-reason").removeClass("hidden");
       }

    }

    function showSpotlightSettings() {
        spark.api.client.callAPI(spark.api.client.urls.accessPrivileges, null, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            for (var i = 0; i < response.data.length; i++) {
                if (response.data[i].name === "SpotlightMember") {
                    var d = new Date();
                    var today = d.toISOString();
           
                    if (today < response.data[i].endDateUtc ) {
                        $('.premium-settings').show();
                    }

                }
            }
        });
    }

    function getSpotlightSettings() {
        spark.api.client.callAPI(spark.api.client.urls.accessPrivileges, null, null, function (success, response) {
            for (var i = 0; i < response.data.length; i++) {
                if (response.data[i].name === "SpotlightMember") {

                    spark.api.client.callAPI(spark.api.client.urls.getSpotlightSettings, null, null, function (success, response) {

                        var spotlightSettings;
                        if (response.data == null && !response.data.IsSpotlightEnabled) {
                            return;
                        } else {
                            spotlightSettings = response.data;
                            var seekingGender;
                            var seekingAgeMin = spotlightSettings.MinAge;
                            var seekingAgeMax = spotlightSettings.MaxAge;
                            var seekingDistance = spotlightSettings.Distance;
                            getRegionId(response.data.ZipCode);

                            if (spotlightSettings.SeekingGender === "Male") {
                                seekingGender = "Men";
                            } else {
                                seekingGender = "Women";
                            }

                            $(".seeking-gender").attr("data-value", seekingGender);
                            $(".seeking-age").text(seekingAgeMin + ' - ' + seekingAgeMax);
                            $(".selectedMinAge").val(seekingAgeMin).text(seekingAgeMin);
                            $(".selectedMaxAge").val(seekingAgeMax).text(seekingAgeMax);
                            $(".edit-distance").text(seekingDistance);

                        }

                    });

                }
            }
        });
    };

    function showEdit(name, callback) {

        window.scrollTo(0, 0);
        var form = showForm(name);
        var target = $("div.editable");
        var temp = $("div.readonly");

        spark.utils.common.hideFooter();
        temp.hide();
        target.fadeIn();
    }

    function showDetail() {
        var target = $("div.readonly");
        var temp = $("div.editable");

        $("div.ui-body-c").removeClass("profile-edit-bg");

        temp.hide();
        target.fadeIn();
        spark.utils.common.showFooter();
    }


    function showLoading(msg, options) {
        var defaultOptions = {
            text: msg,
            textVisible: true
        };

        $.extend(defaultOptions, options);
        $.mobile.loading("show", defaultOptions);
    }

    function hideLoading() {
        $.mobile.loading("hide");
    }


    function getLocationByZipCode(local, callback) {
      
        if (local !== "") {
            var params = {
                zipCode: local
            };

            spark.api.client.callAPI(spark.api.client.urls.get_location, params, null, function (success, response) {
                if (!success) {
                    isLoading = false;
                    spark.utils.common.showGenericDialog(response);
                    return;
                }
                
                if (response.data.length === 0)
                {
                    spark.utils.common.showGenericDialog("Invalid Zip Code");
                    return;
                }

                if (typeof callback === "function")
                    callback(response.data[0].Id);

                return;
            });
        }
    }

    function saveSpotlightPrefs() {

        var errorMessage = "";
        var minAge = $("#min_age_range select option:selected").val();
        var maxAge = $("#max_age_range select option:selected").val();
        var distance = $('#distance_form select option:selected').val();
        var zip = $("#zip_code_form input#postal_code").val();
        var region;

        if (!spark.views.search.validateZipcode(zip)) {
            spark.utils.common.showGenericDialog("Invalid Zip code!");
            return;
        } else {
            var params = {
                "languageID": "1",
                "regionID": zip
            };

            spark.api.client.callAPI(spark.api.client.urls.getRegionHierarchy, params, null, function (success, response) {
                region = response.data;
                if (minAge > maxAge) {
                    errorMessage = "Minimum age cannot be greater than maximum age";
                }

                // Check if there are any error(s) before saving. If so, show message
                // and abort saving until user fix issue.
                if (errorMessage.length > 0) {
                    spark.utils.common.showGenericDialog(errorMessage);
                    return;
                }
                var params = {
                    "CityId": region.City.Id,
                    "CountryId": region.Country.Id,
                    "Distance": distance,
                    IsSpotlightEnabled: true,
                    "MaxAge": maxAge,
                    "MinAge": minAge,
                    "RegionId": region.City.Id,
                    "ZipCode": zip
                };

 


                spark.api.client.callAPI(spark.api.client.urls.updateSpotlightSettings, null, params, function (success, response) {

                    showLoading("Saving...");

                    if (!success) {
                        var error = response.error;
                        var message = (error) ? error.message : "We're unable to save your profile.";
                        showErrorDialog(message);

                        return;
                    }

                    $(".seeking-age").text(minAge + ' - ' + maxAge);
                    $(".selectedMinAge").val(minAge).text(minAge);
                    $(".selectedMaxAge").val(maxAge).text(maxAge);
                    $(".edit-distance").text(distance);
                    getRegionId(zip);
                    hideLoading();
                    showDetail();

                });
            });
        }
    }

    function showForm(name) {
        $("div.ui-body-c").addClass("profile-edit-bg");

        var form;

        $(".spotlight-settings-edit-form").each(function () {
            var temp = $(this);

            if (temp.attr("id") !== name) {
                temp.addClass("hidden");
            } else {
                temp.removeClass("hidden");
                form = temp;
            }
        });
        return form;
    }


    function getEmailAlertSettings() {
        spark.api.client.callAPI(spark.api.client.urls.getEmailSettings, null, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }


            var emailAlertSettings = response.data.emailAlertSettings;
            var emailSetting = $(".settings-emailalerts ul#emailAlerts li").map(function () {
                return $(this).data("settings");
            }).get();
            emailSetting.join(',');

            
            for (var i = 0; i < emailSetting.length; i++) {
                $.each(emailAlertSettings, function (j, val) {
                    if (emailSetting[i] === j) {
                        if (val === true) {
                            $('.settings-emailalerts ul li[data-settings="' + emailSetting[i] + '"]').addClass('selected');
                        }
                    }
                });
            }


            var newsLetterSetting = $(".settings-emailalerts ul.newsletter li").map(function () {
                return $(this).data("settings");
            }).get();
            var newsletterAlertSetting = response.data.newsLetterSettings;
            newsLetterSetting.join(',');

            for (var i = 0; i < newsLetterSetting.length; i++) {
                $.each(newsletterAlertSetting, function (j, val) {
                    if (newsLetterSetting[i] === j) {
                        if (val === true) {
                            $('.settings-emailalerts ul.newsletter li[data-settings="' + newsLetterSetting[i] + '"]').addClass('selected');
                        }
                    }
                });
            }
        });
    }

    function selectEmailAlert() {
        $(this).toggleClass('selected');
        getEmailAlertParams();
    }



    function getEmailAlertParams() {
        spark.api.client.callAPI(spark.api.client.urls.getEmailSettings, null, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            var params = {
                "emailAlertSettings": {
                    //matches block
                    "shouldNotifySecretAdmirerAlerts": $("ul.matches li:first-child").hasClass("selected") ? true : false,
                    "shouldNotifyMatches": $("ul.matches li:nth-child(2)").hasClass("selected") ? true : false,
                    //communication block
                    "shouldNotifyEmailAlert": $("ul.communication li:first-child").hasClass("selected") ? true : false,
                    "shouldNotifySmiles": $("ul.communication li:nth-child(2)").hasClass("selected") ? true : false,
                    "shouldNotifyIMNotifications": $("ul.communication li:last-child").hasClass("selected") ? true : false,
                },
                //newsletter block
                "newsLetterSettings": {
                    "shouldSendAnnouncementsOffers": $("ul.newsletter li:last-child").hasClass("selected") ? true : false,
                    "shouldSendNewsletters": $("ul.newsletter li:nth-child(2)").hasClass("selected") ? true : false,
                    "shouldSendPartnerOffers": $("ul.newsletter li:first-child").hasClass("selected") ? true : false,
                },
                "messageSettings": []
            };


            spark.api.client.callAPI(spark.api.client.urls.updateEmailSettings, null, params, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }
            });

        });
    };

    self.init = function () {
        showSpotlightSettings();
        getSpotlightSettings();
    };

    self.spotlightInit = function () {
        getSpotlightSettings();
        bindClickEvents();
        $('.editable').hide();

        $("#btn_edit_cancel").on("click", showDetail);
        $("#btn_edit_save").on("click", saveSpotlightPrefs);
    };

    self.suspendInit = function() {
        bindClickEvents();
    }

    self.emailAlertsInit = function () {
        $(".settings-emailalerts ul li").on("tap", selectEmailAlert);
        getEmailAlertSettings();
    }

    return self;
})();

$(document).on("pageshow", "#spotlight_settings_page", function () {
    spark.views.settings.spotlightInit();
});

$(document).on("pageshow", "#settings_page", function () {
    spark.views.settings.init();
});

$(document).on("pageshow", "#selfsuspend_settings_page", function () {
    spark.views.settings.suspendInit();
    $("div.footer-container").addClass("hidden");
});

$(document).on("pageshow", "#emailalerts_settings_page", function () {
    spark.views.settings.emailAlertsInit();
});