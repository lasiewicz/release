﻿spark.views.search = (function () {
    "use strict";
    var self = {};
    var resultPageSize = 25;
    var resultPageNumber = 1;
    var isLoading = false;
    var isLastPage = true;
    var showResultPage = false;
    var pageInit = true;

    var gender = "male";
    var seeking = "female";
    var photoOnly = false;
    var location = "";
    var regionID = "";
    var zipCode = "";
    var distance = 5;
    var minAge = 18;
    var maxAge = 29;
    var sortType = 3;
    var prefCookieID = "mos_search_pref_" + spark.utils.common.getMemberId();
    var memberList = [];
    var editedFields = [];
    var reviewFields = [];

    var gridHeight = 0;
    var gridWidth = 0;

    var minHeight;
    var maxHeight;

    //mutliselect attributes
    var ethnicity;
    var bodyType;
    var education =[] ;
    var maritalStatus = [];
    var smoke;
    var drink ;
    var religion ;



    var attributeOptionsMap = {
        "wants_height": "height",
        "wants_min_height": "height",
        "wants_max_height": "height",
        "height": "height",
        "ethnicity": "ethnicity",
        "education": "education",
        "wants_body_type": "body_type",
        "body_type": "body_type",
        "smoke": "smoke",
        "drink": "drink",
        "religion": "religion",
        "new_since": "new_since",
        "recently_updated": "recently_updated",
        "marital_status": "marital_status",
        "min_age": "age"
    }

    function getMatchPreferences() {
        if ($.cookie(prefCookieID) != null) {
            var cookie = $.cookie(prefCookieID);
            var tempArr = cookie.split("&");
            var prefs = {};
            var religion = [];

            $.each(tempArr, function (index, item) {
                var length = item.indexOf("=");
                var key = item.substring(0, length);
                var value = item.substring(length + 1);

                prefs[key] = value;
            });

            gender = prefs.gender;
            seeking = prefs.seeking;
            minAge = prefs.minAge;
            maxAge = prefs.maxAge;
            distance = prefs.distance;
            location = prefs.location;
            minHeight = prefs.minHeight;
            maxHeight = prefs.maxHeight;
            ethnicity = prefs.ethnicity;
            bodyType = prefs.bodyType;
            smoke = prefs.smoke;
            drink = prefs.drink;
            maritalStatus = prefs.maritalStatus;
            education = prefs.education;

            if (prefs.sortType != null) {
                sortType = parseInt(prefs.sortType);
            }

            if (prefs.photoOnly === "true") {
                photoOnly = true;
            }
            else {
                photoOnly = false;
            }

            setupDefaultPref();
        }

        else {

            spark.api.client.callAPI(spark.api.client.urls.get_preferences, null, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }


                var data = response.data;

                if (data.gender === "Female") {
                    gender = "female";
                }
                else {
                    gender = "male";
                }

                if (data.seekingGender === "Male") {
                    seeking = "male";
                }
                else {
                    seeking = "female";
                }

                if (data.minAge != null) {
                    minAge = data.minAge;
                }

                if (data.maxAge != null) {
                    maxAge = data.maxAge;
                }

                if (data.location != null) {
                    location = data.location;
                }

                if (data.maxDistance != null) {
                    distance = data.maxDistance;
                }

                if (data.showOnlyMembersWithPhotos != null) {
                    photoOnly = data.showOnlyMembersWithPhotos;
                }

                if (data.minHeight != null) {
                    minHeight = data.minHeight;
                }

                if (data.maxHeight != null) {
                    maxHeight = data.maxHeight;
                }

                if (data.ethnicity != null) {
                    ethnicity = data.ethnicity;
                }

                if (data.bodyType != null) {
                    bodyType = data.bodyType;
                }

                if (data.smoke != null) {
                    smoke = data.smoke;
                }

                if (data.drink != null) {
                    drink = data.drink;
                }

                if (data.maritalStatus != null) {
                    maritalStatus = data.maritalStatus;
                }

                if (data.education != null) {
                    education = data.education;
                }

                if( data.religion != null){
                    religion = data.religion;
                }

                setupDefaultPref();
                setupCookie();
            });
        }
    }

    function setupDefaultPref() {
            var genderToggle = $("#gender");
            var genderSeekingToggle = $("#gender_seeking");
            var ddMinAge = $("#min_age");
            var ddMaxAge = $("#max_age");
            var ddDistance = $("#distance");
            var postalCode = $("#postal_code");
            var cbPhotos = $("#picture_only");
            var sortFilter = $("#result_sort_filter");
            var ddMinHeight = $("#wants_min_height");
            var ddMaxHeight = $("#wants_max_height");
            // ms is for multiselect
            var msEthnicity = $("#ethnicity");
            var msBodyType = $("#body_type");
            var msEducation = $("#education");
            var msMaritalStatus = $("#marital_status");

            // Set gender value.
            genderToggle.find("a.female").removeClass("selected");
            genderToggle.find("a.male").removeClass("selected");
            if (gender === "female") {
                genderToggle.find("a.female").addClass("selected");
            } else {
                genderToggle.find("a.male").addClass("selected");
            }

            // Set seeking gender.
            genderSeekingToggle.find("a.female").removeClass("selected");
            genderSeekingToggle.find("a.male").removeClass("selected");

            if (seeking === "female") {
                genderSeekingToggle.find("a.female").addClass("selected");
            } else {
                genderSeekingToggle.find("a.male").addClass("selected");
            }

            // Set age range, distance and postal/zip code value.
            ddMinAge.find("select").val(minAge);
            ddMaxAge.find("select").val(maxAge);
            ddDistance.find("select").val(distance);
            postalCode.val(location);
            sortFilter.find("select").val(sortType);

            ddMinHeight.find("select").val(minHeight);
            ddMaxHeight.find("select").val(maxHeight);

            //TODO:setup multiselect widget
            msEthnicity.find("span.checked").attr("data-value");
            msEducation.find("span.checked").attr("data-value");
            msBodyType.find("span.checked").attr("data-value");
            msMaritalStatus.find("span.checked").attr("data-value");

            // TODO: Create custom widget.
            cbPhotos.removeClass("checked");

            if (photoOnly)
                cbPhotos.addClass("checked");

    }

    function setupButtonClickEvent() {
        var genderToggle = $("#gender");
        var genderSeekingToggle = $("#gender_seeking");
        var ddMinAge = $("#min_age");
        var ddMaxAge = $("#max_age");
        var ddDistance = $("#distance");
        var cbPhotos = $("#picture_only");
        var ddMinHeight = $("#wants_min_height");
        var ddMaxHeight = $("#wants_max_height");
        var msBodyType = $("#body_type");
        var msEthnicity = $("#ethnicity");
        var msMaritalStatus = $("#marital_status");
        var msEducation = $("#education");

        // TODO: Create widget to abstract functionality.
        genderToggle.off("click").on("click", function (e) {
            var self = $(this);
            var target = $(e.target);

            target.addClass("selected");
            if (target.hasClass("female")) {
                self.find("a.male").removeClass("selected");
                gender = "female";
            }

            if (target.hasClass("male")) {     
                self.find("a.female").removeClass("selected");
                gender = "male";
            }
        });

        genderSeekingToggle.off("click").on("click", function (e) {
            var self = $(this);
            var target = $(e.target);

            target.addClass("selected");
            if (target.hasClass("female")) {
                self.find("a.male").removeClass("selected");
                seeking = "female";
            }

            if (target.hasClass("male")) {
                self.find("a.female").removeClass("selected");
                seeking = "male";
            }
        });

        cbPhotos.off().on("click", function (e) {
            var self = $(this);
            if (self.hasClass("checked")) {
                self.removeClass("checked");
                photoOnly = false;
            } else {
                self.addClass("checked");
                photoOnly = true;
            }
        });

        ddMinAge.find("select").on("change", function () {
            minAge = this.value;
        });

        ddMaxAge.find("select").on("change", function () {
            maxAge = this.value;
        });

        ddDistance.find("select").on("change", function () {
            distance = this.value;
        });

        ddMinHeight.find("select").on("change", function () {
            minHeight = this.value;
        });

        ddMaxHeight.find("select").on("change", function () {
            maxHeight = this.value;
        });

       
        msBodyType.find("span.checked").on("change", function () {
            bodyType = this.value;
        });

        msEthnicity.find("span.checked").on("change", function () {
            ethnicity = this.value;
        });

        msMaritalStatus.find("span.checked").on("change", function () {
            maritalStatus = this.value;
        });

        msEducation.find("span.checked").on("change", function() {
            education = this.value;
        });

        $("#preferences-search-btn").click(function () {
            if (minAge > maxAge || minAge === "" || maxAge === "") {
                spark.utils.common.showGenericDialog("Please select the right age range!");
                return;
            }

            //location = $("#preferences-zipcode-textarea").val();
            location = $("#postal_code").val();
            if (location === "") {
                $(".search-result-page").removeClass("hidden");
                $(".search-preferences-page").addClass("hidden");
                $(".search-result-page").fadeIn(350);
                getSearchResults();
            } else {
                    getCitiesByZipCode(location);
            }
        });


        $("a#result-back-btn.back-button").click(function () {
            showResultPage = false;
          

            $(".search-result-page").fadeOut(350, function () {
                memberList = [];
                $(".search-preferences-page").removeClass("hidden");
                $(".search-result-page").addClass("hidden");
                $("#search-result-list").find("li").remove();
                $("#search-result-list").find("p").remove();

              //  $(".more-search-options li").addClass("hidden");
                $("div.search-preferences-options").addClass("hidden");

            });            
        });

        $("#result_sort_filter").find("select").on("change", function () {
            $("#search-result-list").find("li").remove();
            $("#search-result-list").find("p").remove();
            memberList = [];
            isLastPage = false;
            resultPageNumber = 1;
            sortType = parseInt(this.value);
            getSearchResults();
        });


        $("#memberlookup-find").click(function () {
            var memberID = $("#lookupContent").val();
          
            if (!/^[a-zA-Z0-9]+$/.test(memberID)) {
                spark.utils.common.showGenericDialog("Please only use alphanumeric characters when searching for members.", $("p.content").css("padding","21px 10px"));
                return;
            } else {
                lookupProfile(memberID);
                return true;
            }

        });

        $(".more-options-section a").on("click", showMoreOptionsList);
        $("div.search-panel.editable > h2.page-header").on("click", backToMoreOptionsList);
        $("a#back-to-search.back-button").on("click", backToSearch);
        $("ul.more-search-options li").on("click", showMoreOptionsItem);
        $("li.block").on("click", profileBlockSuspendCheck);


    }// ends the setUpButtonClickEvent function

    function profileBlockSuspendCheck() {
        spark.utils.common.showGenericDialog("This profile is currently not available");
    }

    function getCitiesByZipCode(zipCode) {
        spark.api.client.callAPI(spark.api.client.urls.get_location, { zipCode: zipCode }, null, function (success, response) {
            if (response.data === null || response.data === 0 || response.data[0].Description === "") {
                spark.utils.common.showGenericDialog("Invalid Zip code!");
            } else {
                setupCookie();

                memberList = [];
                showResultPage = true;
                isLastPage = false;
                resultPageNumber = 1;
                getSearchResults();

                $(".search-result-page").removeClass("hidden");
                $(".search-preferences-page").addClass("hidden");
                $(".search-result-page").fadeIn(350);
            }

        });
    }

    function showMoreOptionsList() {
        $(".search-result-page").addClass("hidden");
        $(".search-preferences-page").addClass("hidden");
        $("div.search-preferences-options").removeClass("hidden");
        showMoreOptions();
    }

    function showMoreOptionsItem() {
        $("div.search-preferences-options").addClass("hidden");
        $("div.search-panel.editable").removeClass("hidden");
        // $("div.more-search-options").addClass("hidden");
        var name = $(this).attr("data-form");
        var legend = $(this).text();
        var buttonContainer = $("#search_edit_buttons");
        $("div.search-edit").removeClass("hidden");
        buttonContainer.show();
        showEdit(name);

        $("div.search-panel.editable > h2.page-header").text(legend);
        var a = $("<a id=\"search-back-btn \" class=\"back-button \" />");
        a.appendTo($("div.search-panel.editable > h2.page-header"));
        $("div.search-panel.editable > h2.page-header").removeClass("hidden");

        var id = name;
        id = id.replace('_form', '');
        var dataValues = $('div#' + id + '').attr("data-values");

       
        if ($('div#' + id + '').attr("data-type") === "multiselect") {
            if ((dataValues)) {
                var multivalueArray = [];
                multivalueArray = $('div#' + id + '').attr("data-values").split(',');
                for (var i = 0; i < multivalueArray.length; i++) {
                    $('div#' + id + '.multiselect.search').find("[data-value='" + multivalueArray[i] + "']").addClass("checked");
                }
            }
        }
    }

    function backToMoreOptionsList() {
        // alert("back to more options list");
        var values = [];
        $(".multiselect.search li").each(function (index, element) {
            if ($(this).find("span").hasClass("checked")) {
                values.push($(element).find("span").attr('data-value'));
                $(this).closest("div").attr("data-values", values);
            }

        });

        $('.search-preferences-options').removeClass('hidden');
        $("div.search-panel.editable").addClass("hidden");
        $("footer#footer.footer").show();
    }

    function backToSearch() {
        $("div.search-preferences-options.page-content").addClass('hidden');
        $("div.search-preferences-page.page-content").removeClass('hidden');
        $("footer#footer.footer").show();
    }

    $(document).on('tap', ".multiselect.search li", function () {
        $(this).toggleClass("checked");
        $(this).find("span").toggleClass("checked");
    });


    $(document).on('tap', "a#btn_edit_save", function (e) {
        getSearchResults();
        $(".search-result-page").addClass("hidden");
        $(".search-preferences-page").addClass("hidden");
        $("div.search-preferences-options").removeClass("hidden");
        $("div.search-edit").addClass("hidden");
    });



    function showEdit(name, callback) {

        window.scrollTo(0, 0);
        var form = showForm(name);
        var target = $("div.editable");
        

        spark.utils.common.hideFooter();
        
        target.fadeIn();
    }

    function showForm(name) {
        //$("div.ui-body-c").addClass("profile-edit-bg");

        var form;

        $(".search-edit-form").each(function () {
            var temp = $(this);

            if (temp.attr("id") !== name) {
                temp.addClass("hidden");
            } else {
                processControls(temp);

                temp.removeClass("hidden");
                form = temp;
            }

        });
        return form;
    }


    function processControls(form) {
        // Proces textarea field(s).
        var textbox = form.find("input[type='text']");
        textbox.keyup(function () {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());
            addEditField($(this), hasChanged);
        });

        textbox.on("paste", function() {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());
            addEditField($(this), hasChanged);
        });

        // Proces textarea field(s).
        var textarea = form.find("textarea");
        textarea.keyup(function () {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());
            addEditField($(this), hasChanged);
        });

        textarea.on("paste", function() {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());

            addEditField($(this), hasChanged);
        });

        // Process multi-select widget(s).
        var widgets = form.find("div[data-type='multiselect']");
        widgets.each(function () {
            var o = $(this);
            var id = o.attr("id");
            o.addClass("search");
            var values = getMultiselectValues(id);
            o.multiselect("createOptions", getAttributeOptionName(id), values);
            o.multiselect("setOnChangeListener", function (hasChanged) {
                //alert("has changed");
                addEditField(o, hasChanged);
            });
        });

        // Process custom dropdown(s).
        var dropdowns = form.find("div.dd-custom");
        dropdowns.each(function () {
            var o = $(this);
            var id = o.attr("id");
            var select = o.find("select");

            // Add onChange listener to dropdown.
            select.change(function () {
                var self = $(this);

                self.find("option:selected").each(function () {
                    var hasChanged = false;
                    var values = [];

                    //if (id !== "min_age_range" && id !== "max_age_range") {
                    hasChanged = ($.trim($("#" + id + "_text").attr("data-value")) != $.trim($(this).text()));
                    addEditField(o, hasChanged);
                });
            });

            if (select.find("option").length !== 0) {
                setSelected(select);

                return;
            }

           var attrName = getAttributeOptionName(id);
            if (!attrName || attrName.length === 0) {
                setSelected(o.find("select"));

                return;
            }

            // Get options for dropdown.
            spark.api.client.callAPI(spark.api.client.urls.attributeOptions, { attributeName: attrName }, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                if (response.data != null) {
                    $.each(response.data, function (key, value) {
                        var option = $("<option />");

                        option.attr("value", value["Value"]);
                        option.text(value["Description"]);
                        option.appendTo(select);
                    });
                }
                setSelected(select);
            });
        });
    }


    function getAttributeOptionName(key) {
        if (key in attributeOptionsMap)
            return attributeOptionsMap[key];
        return "";
    }

    function addEditField(e, hasChanged) {
        var id = e.attr("id");

        if ($.inArray(id, editedFields) === -1 && hasChanged) {
            editedFields.push(id);

            if (e.attr("data-review"))
                reviewFields.push(id);

        } else if (!hasChanged) {
            editedFields = $.grep(editedFields, function (value) {
                return value != id;
            });

            reviewFields = $.grep(reviewFields, function (value) {
                return value != id;
            });
        }
    }
  
    function getMultiselectValues(key) {
        //var o = $("#" + key + "_text");
        var o = $("#" + key + "_form");
        var arr = (o.attr("data-value")) ? o.attr("data-value").split(",") : [];
        var values = [];
        $.each(arr, function (index, value) {
            values.push($.trim(value));
        });

        return values;
    }

    function setSelected(target) {
        var id = target.parent().attr("id");
        var temp = id;
        if (id === "min_age_range" || id === "max_age_range") {
            temp = "age_range";
        }

        if (id === "wants_min_height" || id === "wants_max_height") {
            temp = "wants_height";
        }


        var o = $("#" + temp);
        var value = o.attr("data-value");

        if (!value || $.trim(value.length) === 0)
            return;

        if (id === "min_age_range")
            value = value.split(",")[0];
        if (id === "max_age_range")
            value = value.split(",")[1];
        if (id === "gender_seeking") {
            var temp = value.split(",");
            var gender = temp[0].toLowerCase();

            if (gender === "male") {
                value = 9;

                if (temp[1].toLowerCase() === "male")
                    value = 5;
            } else if (gender === "female") {
                value = 6;

                if (temp[1].toLowerCase() === "female")
                    value = 10;
            }
        }

        if (id === "wants_min_height")
            value = value.split(",")[0];
   

        if (id === "wants_max_height")
            value = value.split(",")[1];
        

        var option = target.find("option").filter(function () {
            var found = ($(this).text() == "" + value);
            if (!found) {
                found = ($(this).val() == value);
            }
            return found;
        });

        if (option && option.length !== 0)
            option.prop("selected", true);
    }


    function lookupProfile(memberID) {
        var params;
        var api;

        if (isNaN(memberID)) {
            api = spark.api.client.urls.lookupByUsername;
            params = {
                //attributeSetName: "fullprofile",
                attributeSetName: "miniprofile",
                targetMemberUsername: memberID
            }
        }
        else {
            var api = spark.api.client.urls.fullProfile;
            var params = {
                targetMemberId: Number(memberID)
            };
        }

        spark.utils.common.showLoading();
        
        window.setTimeout(function () {
            spark.api.client.callAPI(api, params, null, function (success, response) {
                spark.utils.common.hideLoading();
                spark.views.search.cleanCache();
                if (!success) {
                    if (response.error.code === 40001) {
                        spark.utils.common.showGenericDialog("Member not found.");
                    } else {
                        spark.utils.common.showError(response);
                    }
                    return;
                }

                if (response.data.selfSuspendedFlag != null && response.data.selfSuspendedFlag === true) {
                    spark.utils.common.showGenericDialog("The member is not available.");
                    return;
                }
                if (response.data.adminSuspendedFlag != null && response.data.adminSuspendedFlag === true) {
                    spark.utils.common.showGenericDialog("The member is not available.");
                    return;
                }

                if (response.data.blockContactByTarget != null && response.data.blockContactByTarget === true) {
                    spark.utils.common.showGenericDialog("The member is not available.");
                    return;
                }

                if (response.data.blockSearchByTarget != null && response.data.blockSearchByTarget === true) {
                    spark.utils.common.showGenericDialog("The member is not available.");
                    return;
                }

                if (response.data.blockContact != null && response.data.blockContact === true) {
                    spark.utils.common.showGenericDialog("The member is not available.");
                    return;
                }

                if (response.data.blockSearch != null && response.data.blockSearch === true) {
                    spark.utils.common.showGenericDialog("The member is not available.");
                    return;
                }


                var members = [];
                members.push({
                    memberId: response.data.memberId,
                    thumbnail: response.data.thumbnail,
                    userName: response.data.username
                });

                $(document).data("members", members);

                $.mobile.changePage("/profile/" + response.data.memberId);
            });
        }, 2000);
    }


    function getSearchResults() {

        isLoading = true;

        var AdvancedPreferencesJson = {};

        if (($("div#ethnicity").attr("data-values")) != undefined) {
            var ethnicityArray = [];
            ethnicityArray = ($("div#ethnicity").attr("data-values")).split(",");
            AdvancedPreferencesJson.ethnicity = ethnicityArray;
        }

        if (($("div#marital_status").attr("data-values")) != undefined) {
            var maritalStatusArray = [];
            maritalStatusArray = ($("div#marital_status").attr("data-values")).split(",");
            AdvancedPreferencesJson.maritalStatus = maritalStatusArray;
        }

        if (($("div#body_type").attr("data-values")) != undefined) {
            var bodyTypeArray = [];
            bodyTypeArray = ($("div#body_type").attr("data-values")).split(",");
            AdvancedPreferencesJson.bodyType = bodyTypeArray;
        }

        if (($("div#education").attr("data-values")) != undefined) {
            var educationArray = [];
            educationArray = ($("div#education").attr("data-values")).split(",");
            AdvancedPreferencesJson.educationLevel = educationArray;
        }

        if (($("div#drink").attr("data-values")) != undefined) {
            var drinkArray = [];
            drinkArray = ($("div#drink").attr("data-values")).split(",");
            AdvancedPreferencesJson.drinkingHabits = drinkArray;
        }

        if (($("div#smoke").attr("data-values")) != undefined) {
            var smokeArray = [];
            smokeArray = ($("div#smoke").attr("data-values")).split(",");
            AdvancedPreferencesJson.smokingHabits = smokeArray;
        }

        if (($("div#religion").attr("data-values")) != undefined) {
            var religionArray = [];
            religionArray = ($("div#religion").attr("data-values")).split(",");
            AdvancedPreferencesJson.religion2 = religionArray;
        }

        if ($("div#new_since.dd-custom :selected").val() !== "") {
            AdvancedPreferencesJson.joinedSince = [$("div#new_since.dd-custom :selected").val()];
        }

        if ($("div#recently_updated.dd-custom :selected").val() !== "") {
            AdvancedPreferencesJson.updatedSince = [$("div#recently_updated.dd-custom :selected").val()];
        }

        if ($("#postal_code").val() != undefined && $("#postal_code").val().length > 0) {
            zipCode = $("#postal_code").val();
        }

        var minHeightValue = $("div#wants_min_height.between :selected").val();
        var maxHeightValue = $("div#wants_max_height.and :selected").val();

        var ul = $("#search-result-list");
        var params = {
            pageSize: resultPageSize,
            pageNumber: resultPageNumber,
            gender: gender,
            seekingGender: seeking,
            minAge: minAge,
            maxAge: maxAge,  
            maxDistance: distance,
            showOnlyMembersWithPhotos: photoOnly,
            searchOrderByType: sortType,
            minHeight : minHeightValue,
            maxHeight : maxHeightValue,
            AdvancedPreferencesJson: JSON.stringify(AdvancedPreferencesJson)
        };

        if ($("#postal_code").val() != undefined && $("#postal_code").val().length > 0) {
            params.postal_code = $("#postal_code").val();
        }

        /* Sort Type*/
        //JoinDate = 1, LastLogonDate = 2, Proximity = 3, Popularity = 4,
        //ColorCode = 5, KeywordRelevance = 6, MutualMatch = 7

        var interval = ul.find("li").length !== 0 ? spark.utils.common.getLoadingTime() : 0;

        // Add loading footer.
        spark.utils.views.addLoadingPanel(ul);
        window.setTimeout(function () {
            spark.api.client.callAPI(spark.api.client.urls.getSearchResults, null, params, function (success, response) {
                if (!success) {
                    isLoading = false;
                    spark.utils.common.showError(response);
                    $("#search-result-list").find("li.loading").remove();
                    return;
                }
                if (response.data.Spotlight != null) {
                    if (response.data.Spotlight && resultPageNumber <= 1) {
                        buildSpotlight(response.data.Spotlight);
                        ul.find("li")[0] = response.data.Spotlight;
                    }
                }


                resultPageNumber++;
                var data = response.data;
                if (data.members.length < resultPageSize) {
                    isLastPage = true;
                }

                appendToList(data.members);
            });
        }, interval);
    }

    function appendToList(members) {
        var ul = $("#search-result-list");

        // Remove loading footer.
        ul.find("li.loading").remove();

        if (members.length == 0) {
            var p = $("<p />");
            p.text("No results found!")
            p.addClass("no-results-text");
            p.appendTo(ul);
        }

        $.each(members, function (index, item) {
            var li = $("<li data-memberid=\"" + item.id + "\" />");
            var a = $("<a data-role=\"none\"/>");
            var img = $("<img />");
            var span1 = $("<span />");
            var span2 = $("<span />");
            var span3 = $("<span />");
            //var mPercentage = $("<span class = \"matches-label\" />");
            var p = $("<p />");
            var br = $("<br />");
            var imageUrl = (item.gender === "Female") ? "/images/hd/img_silhouette_woman@2x.png" : "/images/hd/img_silhouette_man@2x.png";

            a.attr("href", "/profile/" + item.id);

            if (item.blockContactByTarget || item.selfSuspendedFlag || item.adminSuspendedFlag) {
                li.addClass("block");
                a.attr("href", "#");
            }

            if (item.defaultPhoto && item.defaultPhoto.thumbPath !== "") {
                imageUrl = item.defaultPhoto.thumbPath;
            }
            else if (item.DefaultPhoto && item.DefaultPhoto.thumbPath !== "") {
                imageUrl = item.DefaultPhoto.thumbPath;
            }

            img.attr("src", imageUrl).appendTo(a);

           

            span1.text(item.username);
            span1.addClass("primary");
            span1.appendTo(p);

            var ageText = "";
            var locText = "";

            if (item.age != null) {
                ageText = item.age;
            }

            if (item.location != null) {
                locText = item.location;
                span2.text(ageText + ", " + locText);
            }
            else {
                span2.text(ageText);
            }
            
            span2.addClass("secondary");
            span2.appendTo(p);

            if (item.isOnline === true) {
                span3.addClass("online");
                span3.appendTo(p);
            }

            if (!ul.hasClass("grid-view")) {
                span3.text("Online");
            } else {
               
                // If in grid mode, apply neccessary in-style to override the CSS class.
                li.css({
                    height: gridHeight + "px"
                });
                
                img.css({
                    width: gridWidth + "px",
                    height: gridHeight + "px"
                });
            }

            p.appendTo(a);

            //if (item.matchRating != null) {
            //    mPercentage.text(item.matchRating + "% match");
            //    mPercentage.appendTo(a);
            //}

            if (item.isHighlighted) {
                p.addClass('member-highlight');
                li.addClass('member-highlight');
            }


            a.appendTo(li);
            li.appendTo(ul);
            memberList.push(item);

        });

        if (pageInit && spark.utils.common.loadViewToggleStatus() === "grid") {
            showGrid(ul);
            $("div#mos_christian_searchresults_list_top_320x50").addClass("search_hide");
            $("div#mos_christian_searchresults_grid_top_320x50").removeClass("search_hide");
        }

        isLoading = false;
        pageInit = false;
        //$(document).data("members", memberList);

        //$("ul#search-result-list").append($('<li />').append($("<p />", { 'class':'no-results-text','text': 'There are no more results' })));

    }

    function buildSpotlight(spotlight) {
        if (spotlight != null) {

            spark.api.client.callAPI(spark.api.client.urls.fullProfile, { targetMemberId: spotlight.memberId }, null, function (success, response) {
                spotlight = response.data;

                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }


                var ul = $("ul.generic-list-large");
                var li = $("<li data-memberid=\"" + spotlight.memberId + "\" class='spotlight'/>");
                var a = $("<a data-role=\"none\"/>");
                var img = $("<div />");
                var title = $("<span class='title'>Spotlight Member</span>");
                var span1 = $("<span />");
                var span2 = $("<span />");
                var span3 = $("<span />");
                var span4 = $("<span />");
                var p = $("<p class='grid-view'/>");
                var p2 = $("<span class='list-view'/>");
                var br = $("<br />");
                var thumbnail = (spotlight.gender === "Female") ? "/images/hd/img_silhouette_woman@2x.png" : "/images/hd/img_silhouette_man@2x.png";

                if (spotlight.defaultPhoto && spotlight.defaultPhoto.thumbPath !== "") {
                    thumbnail = spotlight.defaultPhoto.fullPath;
                }
                else if (spotlight.DefaultPhoto && spotlight.DefaultPhoto.thumbPath !== "") {
                    thumbnail = spotlight.DefaultPhoto.fullPath;
                }

                img.addClass("thumb").css({
                    "background-image": "url(" + thumbnail + ")",
                }).attr('data-img-src', thumbnail);
                img.appendTo(a);
                title.appendTo(p);
                span1.text(spotlight.username);
                span1.addClass("primary");
                span1.appendTo(p);

                var ageText = "";
                var locText = "";

                if (spotlight.age != null) {
                    ageText = spotlight.age + " years old";
                    span2.text(ageText + ", " + spotlight.gender);
                }

                if (spotlight.location != null) {
                    locText = spotlight.location;
                    span4.text(locText);
                }

                span4.text(spotlight.location);
                span2.addClass("secondary age").attr('data-age', spotlight.age).attr('data-gender', spotlight.gender);
                span2.appendTo(p);
                span4.addClass("secondary location").appendTo(p);
                p2.appendTo(p);
                if (spotlight.isOnline === true) {
                    span3.addClass("online");
                    span3.appendTo(p);
                }

                p2.text(spotlight.age + ", " + locText);

                if (!ul.hasClass("grid-view")) {
                    span3.text("Online");
                    span2.hide();
                    span4.hide();
                    p2.show();
                } else {
                    // If in grid mode, apply neccessary in-style to override the CSS class.
                    li.css({
                        width: "100%",
                        height: "106px",
                        display: "block"
                    });
                    p2.hide();
                }

                a.attr("href", "/profile/" + spotlight.memberId);

                p.appendTo(a);
                a.appendTo(li);
               // li.appendTo(ul);
                (ul).prepend(li);

                //$('ul#search-result-list').prepend($('ul#search-result-list li:last'));

            });
        }
    }

    function initializeListToggle() {
        var ul = $("#search-result-list");
        var toggle = $(".list-grid-toggle");
        var listButton = toggle.find("a.list");
        var gridButton = toggle.find("a.grid");
        //var gridClass = "grid-view";

        if (spark.utils.common.loadViewToggleStatus() === "grid") {
            $("#btn_grid_view").addClass("active");
            $("#btn_list_view").removeClass("active");
        }

        listButton.off().on("click", function (e) {
            showList(ul);
            $("div#mos_christian_searchresults_list_top_320x50").removeClass("search_hide");
            $("div#mos_christian_searchresults_grid_top_320x50").addClass("search_hide");
        });

        gridButton.off().on("click", function (e) {
            showGrid(ul);
            $("div#mos_christian_searchresults_list_top_320x50").addClass("search_hide");
            $("div#mos_christian_searchresults_grid_top_320x50").removeClass("search_hide");
        });
    };

    function resizeGridImage(ul) {
        var defaultImageWidth = 73;
        var defaultImageHeight = 95;
        var marginWidth = 1;
        var maxThreshold = 10;
        var imageWidth = defaultImageWidth + marginWidth;
        var ratio = 1;
        var screenWidth = $(window).width();
        var gridCount = Math.floor((screenWidth / imageWidth));
        var gridTotalWidth = (gridCount * imageWidth);
        var threshold = (screenWidth - gridTotalWidth);
        var padding = 0;

        if (threshold > maxThreshold) {
            gridCount = (gridCount + 1);
            gridWidth = Math.floor((screenWidth / gridCount)) - marginWidth;
            gridTotalWidth = (gridCount * (gridWidth + marginWidth));
            threshold = (screenWidth - gridTotalWidth);

            ratio = Math.round((gridWidth / imageWidth) * 100) / 100;
            gridHeight = Math.round((defaultImageHeight * ratio));
        }

        ul.css({ paddingLeft: 0 });
        if (threshold > 0) {
            padding = Math.round((threshold / 2)) - marginWidth;
            ul.css({
                paddingLeft: padding + "px"
            });
        }

        if (gridHeight !== 0 && gridWidth !== 0) {
            ul.find("li").css({
                height: gridHeight + "px"
            });

            ul.find("li > a > img").css({
                width: gridWidth + "px",
                height: gridHeight + "px"
            });

            ul.find("li > a > div.thumb").css({
                width: gridWidth + "px",
                height: gridHeight + "px"
            });
        }
    };


    function setupCookie() {
        $.cookie(prefCookieID, "gender=" + gender + "&seeking=" + seeking + "&minAge=" + minAge + "&maxAge=" + maxAge + "&distance=" + distance + "&location=" + location + "&photoOnly=" + photoOnly + "&minHeight=" + minHeight + "&maxHeight=" + maxHeight + "&ethnicity=" + ethnicity + "&bodyType=" + bodyType + "&education=" + education + "&maritalStatus=" + maritalStatus + "&smoke=" + smoke + "&drink=" + drink + "&sortType=" + sortType, { expires: 20 });
    }

    function showGrid(ul) {
        if (ul.hasClass("grid-view"))
            return;

        if (ul.find("li").hasClass("loading")) {
            return;
            
        }

        ul.addClass("grid-view");
        $("#btn_grid_view").addClass("active");
        $("#btn_list_view").removeClass("active");
        spark.utils.common.setCookieForViewToggle("grid");

        resizeGridImage(ul);

        ul.hide();
        ul.find("span.online").text("");
        $("li.spotlight span.list-view").hide();
        $("li.spotlight span.secondary").show();
        ul.fadeIn(350);
        setTimeout(function () {
            googletag.pubads().refresh([gpt_results_grid_top]);
        }, 800);

    };

    function showList(ul) {
        if (!ul.hasClass("grid-view"))
            return;

        $("#btn_list_view").addClass("active");
        $("#btn_grid_view").removeClass("active");

        ul.find("li").removeAttr("style");
        ul.find("li > a > img").removeAttr("style");

        ul.hide();
        ul.removeClass("grid-view");
        ul.find("span.online").text("Online");
        $("li.spotlight span.list-view").show();
        $("li.spotlight span.secondary").hide();
        spark.utils.common.setCookieForViewToggle("list");
        ul.fadeIn(350);
        setTimeout(function () {
            googletag.pubads().refresh([gpt_results_top]);
        }, 800);

    }

    function validateZipcode(zipcode) {
        var CNRegex = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;  // Validate either with or without " ", "-"
        var USRegex = /(^\d{5}$)|(^\d{5}-\d{4}$)/;  // Validate either 5 or 5+4 digits
        var isValid = false;

        if (zipcode.length === 5) {
            isValid = USRegex.exec(zipcode);
        }
        else if (zipcode.length === 6) {
            isValid = CNRegex.exec(zipcode);
        }
        else if (zipcode === "") {
            isValid = true;
        }
        else {
            return false;
        }

        return isValid;
    }

    function changeTabs(tabID) {
        var tabs = $("ul.search-tabs");
        tabs.children("li").each(function () {
            var li = $(this);
            var a = li.find("a");

            if (a.text() === tabID) {
                if (tabID === "Member Lookup") {
                    a.addClass("active").removeClass("border-left");
                    $("#tab-1").addClass("hidden");
                    $("#tab-2").removeClass("hidden");
                }
                else if (a.text() === "Search") {
                    a.addClass("active").removeClass("border-right");
                    $("#tab-1").removeClass("hidden");
                    $("#tab-2").addClass("hidden");

                }
            }
            else {
                if (a.text() === "Member Lookup") {
                    a.addClass("border-left").removeClass("active");
                } else if (a.text() === "Search") {
                    a.addClass("border-right").removeClass("active");
                }
            }
        });
    }

    function showMoreOptions(e) {
        e.preventDefault();
        $('.search-preferences-options').removeClass('hidden');

    }



    $(document).on("tap", "li.block", function () {
        spark.utils.common.showGenericDialog("This profile is currently not available");
    });

    self.init = function () {
        pageInit = true;
     
        var tabs = $("ul.search-tabs");
        tabs.children("li").each(function () {
            var li = $(this);

            // Bind event to anchor link(s).
            li.find("a").click(function (e) {
                var a = $(this);
                var tabID = a.text();

                changeTabs(tabID);
                e.preventDefault();
            });
        });

        initializeListToggle();

        setupButtonClickEvent();
        getMatchPreferences();
    };

    self.resizeGrid = function (ul) {
        resizeGridImage(ul);
    };

    self.saveCookie = function () {
        setupCookie();
    }

    self.getMoreSearchResult = function () {
        if (!showResultPage)
            return;

        if (isLoading || isLastPage)
            return;

        getSearchResults();
    }

    self.storeMembers = function () {
        var members = [];
        var spotlight = [];
        spark.views.search.cleanCache();
        $("#search-result-list").children("li").each(function () {
            var li = $(this);
            //Cache the Spotlight Profile into a separate array
            if (li.hasClass('spotlight')) {
                spotlight.push({
                    memberId: li.attr("data-memberid"),
                    defaultPhoto: {
                        thumbPath: li.find("a .thumb").attr("data-img-src"),
                    },
                    username: li.find(".primary").text(),
                    age: li.find(".secondary.age").attr('data-age'),
                    gender: li.find(".secondary.age").attr('data-gender'),
                    location: li.find(".secondary.location").text(),
                });
            }else {
            members.push({
                memberId: li.attr("data-memberid"),
                thumbnail: li.find("a img").attr("src"),
                userName: li.find(".primary").text(),
            });
            }
        });

        // Cache data so that it can be retrieve from the Profile page.
        $(document).data('spotlight', spotlight);
        $(document).data("members", members);

        var isGrid = false;
        var ul = $("#search-result-list");
        if (ul.hasClass("grid-view")) {
            isGrid = true;
        }

        if (showResultPage) {
            $(document).data("cachedData", {
                cachedPage: "search_result",
                isGrid: isGrid,
                pageNumber: resultPageNumber,
                lastPage: isLastPage,
                loading: isLoading,
                showResult: showResultPage,
                locationID: regionID,
                offsetY: $(document).scrollTop(),
                membersData: memberList,
                spotlight: spotlight,

        });


        }
        else {
            $(document).data("cachedData", {
                cachedPage: "search_Lookup",
                showResult: showResultPage,
                lookupName: $("#lookupContent").val(),
            });
        }
    }

    self.cleanCache = function () {
        $(document).removeData("members");
        $(document).removeData("cachedData");
    }

    self.resumePageStatus = function () {
        var cachedData = $(document).data("cachedData");

        if (cachedData != null) {
            if (cachedData.cachedPage == "search_result") {
                resultPageNumber = cachedData.pageNumber;
                isLastPage = cachedData.lastPage;
                isLoading = cachedData.loading;
                showResultPage = cachedData.showResult;
                regionID = cachedData.locationID;
                memberList = [];

                if (cachedData.spotlight) {
                    buildSpotlight(cachedData.spotlight[0]);
                }
                
                //$(".search-preferences-page").addClass("hidden");
                $(".search-result-page").fadeIn(10, function () {
                    appendToList(cachedData.membersData);

                    $(".search-result-page").removeClass("hidden");
                    $(document).scrollTop(cachedData.offsetY);
                });

                return;
            }
            else if (cachedData.cachedPage == "search_Lookup") {
                $(".search-preferences-page").removeClass("hidden");
                changeTabs("Member Lookup");
                $("#lookupContent").val(cachedData.lookupName);
            }
        }
        else {
            $(".search-preferences-page").removeClass("hidden");
        }
    }

    self.reset = function () {
        memberList = [];
        pageInit = true;
    }

    self.validateZipcode = validateZipcode;
    self.buildSpotlight = buildSpotlight;
    return self;
})();


$(document).on("pageshow", "#search_page", function () {
    $(window).off("orientationchange").on("orientationchange", function (event) {
        var ul = $("#search-result-list");

        if (ul && ul.hasClass("grid-view")) {
             spark.views.search.resizeGrid(ul);
        }
    });

    spark.views.search.init();
    spark.views.search.resumePageStatus();
    
    if (spark.utils.common.loadViewToggleStatus() === "list") {
        setTimeout(function () {
            googletag.pubads().refresh([gpt_results_top]);
        }, 800);
    }
    if (spark.utils.common.loadViewToggleStatus() === "grid") {
        setTimeout(function () {
            googletag.pubads().refresh([gpt_results_grid_top]);
        }, 800);
    }
});

$(document).on("pagehide", "#search_page", function () {
    spark.views.search.saveCookie();
    spark.views.search.reset();
});

$(document).on("pagebeforehide", "#search_page", function (e, ui) {
    //check if the next page is the profile page
    if (ui.nextPage.attr('id') === 'profile_page') {
        spark.views.search.storeMembers();
    }
    else {
        spark.views.search.cleanCache();
    }
});