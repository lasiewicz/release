﻿spark.api.client = (function () {
    //this is the one we are currently using:
    var AJAX_REST_AUTHORITY;


    var BRAND_ID = 9081;
    var loginData = {};
    var self = {};

    var defaultAjaxOptions = {
        beforeSend: function (xhr) {
            // Hack for Android 2.3.X devices (known Google issue).
            if (self.isAndroidGingerbread) {
                xhr.setRequestHeader("Content-Length", "");
            }
            //xhr.setRequestHeader("Accept", "version=V3.0");
        },
        tryCount: 0,
        retryLimit: 3,
        dataType: "json"
    };

    // List of urls to REST API.
    var urlLookup = {
        fullProfile: {
            url: "/profile/attributeset/fullprofile/{targetMemberId}", method: "GET", headers: { Accept: "version=V3.3" }
        },
        miniprofile: { url: "/profile/attributeset/miniprofile/{targetMemberId}", method: "GET" , headers: { Accept: "version=V3.3" }},
        attributeOptions: { url: "/content/attributes/options/{attributeName}", method: "GET", headers: { Accept: "version=V3.0" } },
        attributeOptionCollection: { url: "/content/attributes/optionscollection", method: "POST", headers: { Accept: "version=V3.0" } },
        updateProfile: { url: "/profile/attributes", method: "PUT", headers: { Accept: "version=V3.1" } },
        updateUsername: { url: "/profile/username", method: "PUT" },
        getAllTeases: { url: "/content/teases", method: "GET", headers: { Accept: "version=V3.0" } },
        sendTease: { url: "/mail/tease", method: "POST", headers: { Accept: "version=V3.0" } },
        eventSeen: {url:"/hotlist/eventseen/", method: "POST", headers: {Accept: "version=V3.0"}},
        hotlist: { url: "/hotlist/{hotlist}/pagesize/{pageSize}/pagenumber/{pageNumber}", method: "GET", headers: { Accept: "version=V3.2" } },
        members_online_count: { url: "/membersonline/count", method: "GET" },
        membersOnline: { url: "/membersonline/list", method: "GET" },
        upload_photo: { url: "/profile/photos/uploadjson", method: "POST", headers: { Accept: "version=V3.0" } },
        photos: { url: "/profile/photos/{targetMemberId}", method: "GET", headers: { Accept: "version=V3.0" } },
        updatePhotoCaption: { url: "/profile/photos/{photoId}", method: "PUT", headers: { Accept: "version=V3.0" } },
        makeDefaultPhoto: { url: "/profile/photos/makedefault/{photoId}", method: "PUT", headers: { Accept: "version=V3.0" } },
        deletePhoto: { url: "/profile/photos/delete/{photoId}", method: "DELETE", headers: { Accept: "version=V3.0" } },
        mailFolders: { url: "/mail/folders", method: "GET", headers: { Accept: "version=V3.0" } },
        message: { url: "/mail/message/{messageId}/{folderId}/{markAsRead}", method: "GET", headers: { Accept: "version=V3.2" } },
        messages: { url: "/mail/folder/{folderId}/pagesize/{pageSize}/page/{pageNumber}/{filter}", method: "GET", headers: { Accept: "version=V3.2" } },
        sendMessage: { url: "/mail/message", method: "POST", headers: { Accept: "version=V3.2" } },
        deleteMessage: { url: "/mail/messages", method: "POST", headers: { Accept: "version=V3.2" } },
        moveMailMessage: { url: "/mail/message", method: "PUT", headers: { Accept: "version=V3.0" } },
        get_YNM: { url: "/hotlist/ynmvote/targetMemberId/{addMemberId}", method: "GET", headers: { Accept: "version=V3.0" } },
        saveYNM: { url: "/hotlist/ynmvote", method: "POST", headers: { Accept: "version=V3.0" } },
        matches: { url: "/match/results/pagesize/{pageSize}/page/{pageNumber}", method: "GET", headers: { Accept: "version=V3.0" } },
        secretAdmirers: { url: "/memberslideshow/members", method: "GET" },
        get_preferences: { url: "/match/preferences", method: "GET", headers: { Accept: "version=V3.0" } },
        put_preferences: { url: "/match/preferences", method: "PUT", headers: { Accept: "version=V3.0" } },
        sendInstantMessage: { url: "application/{applicationId}/instantmessenger/saveIM", method: "POST" },
        getSearchResults: { url: "/search/results", method: "POST", headers: { Accept: "version=V3.0" } },
        getInvites: { url: "/instantmessenger/CheckInvite", method: "GET" },
        removeInvite: { url: "/instantmessenger/invite", method: "DELETE" },
        missedIM: { url: "/instantmessenger/missedIM", method: "POST" },
        get_location: { url: "/content/region/citiesbyzipcode/{zipCode}", method: "GET", headers: { Accept: "version=V3.0" } },
        lookupByUsername: { url: "/profile/lookupByMemberUsername/{attributeSetName}/{targetMemberUsername}", method: "GET" },
        lookupByMemberID: { url: "/profile/lookupByMemberId/{attributeSetName}/{targetMemberId}", method: "GET", headers: { Accept: "version=V3.0" } },
        activities: { url: "/startNum/{startNum}/pageSize/{pageSize}/usernotifications", method: "GET" },
        hotlistCount: { url: "/hotlist/counts", method: "GET", headers: { Accept: "version=V3.3" } },
        unreadMailCount: { url: "/mail/unreadcount", method: "GET", headers: { Accept: "version=V3.0" } },
        postFavorite: { url: "/hotlist/{hotlistCategory}/targetmemberid/{memberId}", method: "POST", headers: { Accept: "version=V3.0" } },
        deleteFavorite: { url: "/hotlist/favorite/targetmemberid/{targetMemberId}", method: "DELETE" },
        slideShowMembers: { url: "/memberslideshow/members", method: "GET" },
        accessPrivileges: { url: "/subscription/accessprivileges", method: "GET", headers: { Accept: "version=V3.3" } },
        getRuntimeSettingValue: { url: "/content/runtimesettingvalue/{settingName}", method: "GET" },
        getSpotlightSettings: { url: "/premium/spotlightsettings", method: "GET", headers: { Accept: "version=V3.0" } },
        updateSpotlightSettings: { url: "/premium/spotlightsettings", method: "POST", headers: { Accept: "version=V3.0" } },
        // getRegionHierarchy: { url: "/content/region/hierarchy/2/{regionId}", method: "GET" },
        getRegionHierarchy: { url: "/content/region/hierarchy/{languageID}/{regionID}", method: "GET", headers: { Accept: "version=V3.0" } },
        addToHotList: { url: "/hotlist/{hotListCategory}/targetmemberid/{targetMemberId}", method: "POST", headers: { Accept: "version=V3.0" } },
        removeFromHotlist: { url: "/hotlist/{hotListCategory}/targetmemberid/{targetMemberId}", method: "DELETE", headers: { Accept: "version=V3.0" } },
        UpdateSubscriptionPlan: { url: "/subscription/plan", method: "PUT", headers: { Accept: "version=V3.0" } },
        GetSubscriptionPlanStatus: { url: "/subscription/plan", method: "GET", headers: { Accept: "version=V3.0" } },
        GetIsMobileAppSubscriber: { url: "/subscription/ismobileappsubscriber/{memberId}", method: "GET" },
        selfSuspend: { url: "/profile/selfSuspend", method: "POST", headers: { Accept: "version=V3.0" } },
        reActivateSelfSuspend: { url: "/profile/ReActivateSelfSuspend", method: "POST", headers: { Accept: "version=V3.0" } },
        ReportAbuse: { url: "/hotlist/report/targetmemberid/{memberId}", method: "POST", headers: { Accept: "version=V3.0" } },
        getEmailSettings: { url: "/profile/emailsettings", method: "GET", headers: { Accept: "version=V3.0" } },
        updateEmailSettings: { url: "/profile/emailsettings", method: "PUT", headers: { Accept: "version=V3.0" } },

};

    // Private method(s).
    function buildUrl(originalUrl, urlParams) {
        // Replace place-holder with url params.
        originalUrl = formatUrl(originalUrl, urlParams);

        // Build complete API url (add timestamp query-string to explicitly disable ajax caching).
        var url = AJAX_REST_AUTHORITY + "/" + BRAND_ID + originalUrl + "?access_token=" + loginData.accessToken + "&ts=" + new Date().getTime();
        return url;
    };

    function formatUrl(url, params) {
        if (!params)
            return url;

        for (key in params) {
            url = url.replace("{" + key + "}", params[key]);
        }

        return url;
    }

    function getLoginData() {
        return {
            "memberId": parseInt($.cookie("sua_mid"), 10),
            "accessToken": $.cookie("sua_at"),
            "isPayingMember": $.cookie("sua_sub"),
            "accessExpiresTime": new Date($.cookie("sua_ate"))
        };
    };

    function isValidToken() {
        if (!loginData || !loginData.accessToken || !loginData.aaccessExpiresTime) {
            return false;
        }

        var nowUTC = getUTCTime();
        var accessExpiresUTC = loginData.accessExpiresTime;
        accessExpiresUTC.setHours(accessExpiresUTC.getHours() + 8); // adjust from pacific time
        if (accessExpiresUTC > nowUTC) {
            return true;
        }

        return false;
    };

    function getUTCTime(theTime) {
        if (!theTime) {
            theTime = new Date();
        }
        var utc = new Date(theTime.getTime() + theTime.getTimezoneOffset() * 60 * 1000);
        return utc;
    };

    function handleAjaxCallback(success, data, fn, context) {
        if (typeof fn === "function")
            fn(success, data, context);
    };

    // Public method(s).
    self.buildUrl = buildUrl;

    self.init = function (brandId, ajaxUrl, ajaxOptions) {
        BRAND_ID = brandId || BRAND_ID;
        AJAX_REST_AUTHORITY = ajaxUrl || AJAX_REST_AUTHORITY;
        loginData = getLoginData();

        if (typeof ajaxOptions !== "undefined")
            $.extend(defaultAjaxOptions, ajaxOptions);

        self.urls = urlLookup;
        self.isAndroidGingerbread = /Android 2.3/i.test(navigator.userAgent);
    };

    self.getLoginData = function () {
        return getLoginData();
    };

    self.callAPI = function (options, urlParams, data, callback) {
        var spec = {
            type: options.method,
            url: buildUrl(options.url, urlParams),
            data: data,
            success: function (response, other, other2) {
                handleAjaxCallback(true, response, callback, this);
            },
            error: function (xhr, statusText, error) {
                handleAjaxCallback(false, $.parseJSON(xhr.responseText), callback);
            }
        };

        if (options.headers !== null) {
            spec.headers = options.headers;
        }

        if (options.method.toLowerCase() !== 'get' && data) {
            var count = 0;
            for (var key in data) if (data.hasOwnProperty(key)) {
                count++;
            }

            if (count > 0) {
                spec.contentType = "application/json";
                spec.data = JSON.stringify(data);
            }
        }
        
        $.extend(spec, defaultAjaxOptions);
        console.log("url:" + spec.url);

        $.ajax(spec);


    };

    self.init();

    return self;
})();
