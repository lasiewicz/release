spark.api.client = (function () {
    //var AJAX_REST_AUTHORITY = "https://api.spark.net/V2";
   // var AJAX_REST_AUTHORITY = "http://api.stgv3.spark.net/V2";
    var AJAX_REST_AUTHORITY =  "https://api-dev.securemingle.com";
    //var AJAX_REST_AUTHORITY = "http: //api.local.spark.net/V2";
    var BRAND_ID = 1003;
    var loginData = {};
    var self = {};

    var defaultAjaxOptions = {
        beforeSend: function (xhr) {
            // Hack for Android 2.3.X devices (known Google issue).
            if (self.isAndroidGingerbread) {
                xhr.setRequestHeader("Content-Length", "");
            }
        },
        tryCount: 0,
        retryLimit: 3,
        dataType: "json"
    };

    // List of urls to REST API.
    var urlLookup = {
        fullProfile: { url: "/profile/attributeset/fullprofile/{targetMemberId}", method: "GET" },
        miniprofile: { url: "/profile/attributeset/miniprofile/{targetMemberId}", method: "GET" },
        attributeOptions: { url: "/content/attributes/options/{attributeName}", method: "GET" },
        updateProfile: { url: "/profile/attributes", method: "PUT" },
        sendTease: { url: "/mail/tease", method: "POST" },
        hotlist: { url: "/hotlist/{hotlist}/pagesize/{pageSize}/pagenumber/{pageNumber}", method: "GET" },
        members_online_count: { url: "/membersonline/count", method: "GET" },
        membersOnline: { url: "/membersonline/list", method: "GET" },
        upload_photo: { url: "/profile/photos/uploadform", method: "POST" },
        photos: { url: "/profile/photos/{targetMemberId}", method: "GET" },
        updatePhotoCaption: { url: "/profile/photos/{memberPhotoId}", method: "PUT" },
        deletePhoto: { url: "/profile/photos/delete/{memberPhotoId}", method: "DELETE" },
        message: { url: "/mail/message/{messageListId}", method: "GET" },
        messages: { url: "/mail/folder/{folderId}/pagesize/{pageSize}/page/{pageNumber}", method: "GET" },
        sendMessage: { url: "/mail/message", method: "POST" },
        deleteMessage: { url: "/mail/message/folder/{currentFolderId}/message/{messageId}", method: "DELETE" },
        get_YNM: { url: "/hotlist/ynmvote/targetMemberId/{addMemberId}", method: "GET" },
        matches: { url: "/match/results/pagesize/{pageSize}/page/{pageNumber}", method: "GET", headers: { Accept: "version=V2.1" } },
        secretAdmirers: { url: "/search/secretadmirer", method: "POST" },
        get_preferences: { url: "/match/preferences", method: "GET" },
        put_preferences: { url: "/match/preferences", method: "PUT" },
        sendInstantMessage: { url: "application/{applicationId}/instantmessenger/saveIM", method: "POST" },
        getSearchResults: { url: "/search/results", method: "POST", headers:{ Accept: "version=V2.1" } },
        getInvites: { url: "/instantmessenger/invites", method: "GET" },
        removeInvite: { url: "/instantmessenger/invite", method: "DELETE" },
        missedIM: { url: "/instantmessenger/missedIM", method: "POST" },
        get_location: { url: "/content/region/citiesbyzipcode/{zipCode}", method: "GET" },
        lookupByUsername: { url: "/profile/lookupByMemberUsername/{attributeSetName}/{targetMemberUsername}", method: "GET" },
        lookupByMemberID: { url: "/profile/lookupByMemberId/{attributeSetName}/{targetMemberId}", method: "GET", headers: { Accept: "version=V3.0" } },
        activities: { url: "/startNum/{startNum}/pageSize/{pageSize}/usernotifications", method: "GET" },
        hotlistCount: { url: "/hotlist/counts", method: "GET" },
        postFavorite: { url: "/hotlist/default/targetmemberid/{targetmemberid}", method: "POST" },
        deleteFavorite: { url: "/hotlist/favoritemember/{favoriteMemberId}", method: "DELETE" }
    };

    // Private method(s).
    function buildUrl(originalUrl, urlParams) {
        // Replace place-holder with url params.
        originalUrl = formatUrl(originalUrl, urlParams);

        // Build complete API url (add timestamp query-string to explicitly disable ajax caching).
        var url = AJAX_REST_AUTHORITY + "/brandId/" + BRAND_ID + originalUrl + "?access_token=" + loginData.accessToken + "&ts=" + new Date().getTime();
        return url;
    };

    function formatUrl(url, params) {
        if (!params)
            return url;

        for (key in params) {
            url = url.replace("{" + key + "}", params[key]);
        }

        return url;
    }

    function getLoginData() {
        return {
            "memberId": parseInt($.cookie("sua_mid"), 10),
            "accessToken": $.cookie("sua_at"),
            "isPayingMember": $.cookie("sua_sub"),
            "accessExpiresTime": new Date($.cookie("sua_ate"))
        };
    };

    function isValidToken() {
        if (!loginData || !loginData.accessToken || !loginData.aaccessExpiresTime) {
            return false;
        }

        var nowUTC = getUTCTime();
        var accessExpiresUTC = loginData.accessExpiresTime;
        accessExpiresUTC.setHours(accessExpiresUTC.getHours() + 8); // adjust from pacific time
        if (accessExpiresUTC > nowUTC) {
            return true;
        }

        return false;
    };

    function getUTCTime(theTime) {
        if (!theTime) {
            theTime = new Date();
        }
        var utc = new Date(theTime.getTime() + theTime.getTimezoneOffset() * 60 * 1000);
        return utc;
    };

    function handleAjaxCallback(success, data, fn, context) {
        if (typeof fn === "function")
            fn(success, data, context);
    };

    // Public method(s).
    self.buildUrl = buildUrl;

    self.init = function (brandId, ajaxUrl, ajaxOptions) {
        BRAND_ID = brandId || BRAND_ID;
        AJAX_REST_AUTHORITY = ajaxUrl || AJAX_REST_AUTHORITY;
        loginData = getLoginData();

        if (typeof ajaxOptions !== "undefined")
            $.extend(defaultAjaxOptions, ajaxOptions);

        self.urls = urlLookup;
        self.isAndroidGingerbread = /Android 2.3/i.test(navigator.userAgent);
    };

    self.getLoginData = function () {
        return getLoginData();
    };

    self.callAPI = function (options, urlParams, data, callback) {
        var spec = {
            type: options.method,
            url: buildUrl(options.url, urlParams),
            //cache: false,
            data: data,
            success: function (response, other, other2) {
                handleAjaxCallback(true, response, callback, this);
            },
            error: function (xhr, statusText, error) {
                handleAjaxCallback(false, $.parseJSON(xhr.responseText), callback);
            },
        };

        if (options.headers !== null) {
            spec.headers = options.headers;
        }

        if (options.method.toLowerCase() !== 'get' && data) {
            var count = 0;
            for (var key in data) if (data.hasOwnProperty(key)) {
                count++;
            }

            if (count > 0) {
                spec.contentType = "application/json";
                spec.data = JSON.stringify(data);
            }
        }

        $.extend(spec, defaultAjaxOptions);
        $.ajax(spec);
    };

    self.init();

    return self;
})();


