﻿// Spark namespace definition.
var spark = spark || {};
spark.api = spark.api || {};
spark.utils = spark.utils || {};
spark.views = spark.views || {};

function iOSversion() {
    if (/iP(hone|od|ad)/.test(navigator.platform)) {
        // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
        var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
        return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
    }
}

// Handle infinite scrolling for pages that are scrollable.
//$(window).scroll(function() {
$(document).on("scrollstop", function (e) {
    var page = $.mobile.activePage;
    var isScrollable = (page.attr("data-scrollable") === "true");

    if (!isScrollable)
        return;
    
    var ver = iOSversion();
    var offsetFromTop = $(window).scrollTop();
    var bottomBound = $(document).height() - $(window).height() - 197;

    if (ver != null) {
        if (ver[0] < 7) {
            bottomBound -= 60;
        }
        else if (ver[0] >= 7) {
            bottomBound -= 69;
        }
    }

    if (bottomBound < 0)
        return;

    if (offsetFromTop >= bottomBound) {
        var activePage = $.mobile.activePage.attr('id');

        // Add event listener(s) below for pages that has infinite scrolling enabled.
        if (activePage === "activity_page") {
            var activeTab = $(".active").attr("href");
            var type = activeTab.substring(activeTab.indexOf("/") + 1);
            spark.views.activity.getHotlist(type, true);
            //spark.views.activity.getHotlist(spark.views.activity.activeType, true);
        } else if (activePage === "membersonline_page") {
            spark.views.mol.getMembersOnline();
        } else if (activePage === "favorites_page") {
            spark.views.hotlist.getFavorites();
        } else if (activePage === "matches_page") {
            spark.views.matches.getMatches();
        } else if (activePage === "secret_admirer_results_page") {
            spark.views.admirer.getResults();
        } else if (activePage === "search_page") {
            spark.views.search.getMoreSearchResult();
        } else if (activePage === "chat_page") {
            spark.views.chats.getFavorites();
        } else if (activePage === "mail_page") {
            var folderId = spark.views.mail.getActiveFolder();
            if (folderId > 0)
                spark.views.mail.getMessages(folderId);
        }
    }
});

//$(document).ready(function() {

//});

//$(document).on('tap', '.generic-list-large li a', function (e) {
//    var a = $(this);
//    var memberId = [];
//    memberId.push({
//        memberId: a.attr("href").replace("/profile/", "")
//    });

//    $(document).data("members", memberId);
//});

// Handle general page show event.
$(document).on("pageshow", function (e, ui) {
    spark.views.mol.updateCount();
    
    //back button
    $("h2.page-header.home-header").click(function () {
        window.history.back();
    });

    //update copyright year
    $('span#copyright-year').html(new Date().getFullYear());


    // Manually change page so panel will close first and then
    // change page.

    $("#nav_panel").click(function (e) {
        var target = $(e.target);
        var href;

        if (target.is("a")) {
            $("#nav_panel").panel("close");
            $(document).removeData("members");
            $(document).removeData("cachedData");

            href = target.attr("href");
            window.setTimeout(function () {

                if (target.hasClass("download")) {
                    if (spark.utils.common.iOSversion()) {
                        window.location.assign("https://itunes.apple.com/us/app/christianmingle-christian/id941317572?mt=8");
                    }
                    else if (spark.utils.common.isAndroid()) {
                        window.location.assign("https://play.google.com/store/apps/details?id=com.spark.christianmingle&hl=en");
                    }
                    $('.ui-loader').hide();
                }

                if (href.indexOf("subscription/subscribe") === 1 || href.indexOf("subscription/premium") === 1 ) {
                    window.location.href = href;
                } else {
                    $.mobile.changePage(href);
                    
                }
            }, 300);

            e.preventDefault();
        }
    });

    var minHeight = $(window).height() - $('.nav-header').height() - 197;

    //Detect Android Device
    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1;
    //var isAndroid = navigator.userAgent.toLowerCase().indexOf("android");

    if (isAndroid) {
        minHeight += 10;
    }

    $('.page-content').css('min-height', minHeight + 'px');

    // Bind click event to Full Site link.
    $("#fullsite_link").off().click(function (e) {
        // Set cookie to stay on full website.
        $.cookie("sofs", 1, { expires: null, path: '/', domain: 'christianmingle.com' });
    });
    
    //show interstitial, download link and toaster if users are on ios
    if (iOSversion() || spark.utils.common.isAndroid()) {
        $(".nav-panel-menu li a.download").show();
        spark.utils.common.openInAppPop();
    }

    //deeplink checking
    spark.utils.common.openNativeApp();

});

$(document).on("pagehide", function (event, ui) {
    var page = $(event.target);

    if (page.attr("data-cache") == "never") {
        page.remove();
    };
});


$(document).on('tap', 'div.nav-header a.btn-mol', function (e) {
    var a = $(this);
});

$(document).on("pageshow", "#selfsuspend_page", function () {
    $(".nav-header").find("a").removeAttr("href");
});

$(document).on("pageshow", "#sub_send", function () {
    $("#cm_app_pop").hide();
    var memberId = spark.utils.common.getMemberId();
    spark.utils.common.isIAP(memberId);
 });

