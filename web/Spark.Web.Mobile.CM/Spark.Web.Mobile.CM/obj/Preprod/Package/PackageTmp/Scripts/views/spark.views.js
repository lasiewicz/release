spark.views.activity = (function () {
    var self = {};
    var activityList = [];
    var activeType = "all";
    var currentDate = new Date();
    
    function applyFilter(type) {
        var ul = $("#activity_list");
        var filterType = getFilterType(type);
        var temp = [];

        ul.empty();
        if (activityList !== null) {
            temp = $.grep(activityList, function (item, index) {
                return (item.NotificationType === filterType || filterType === 0);
            });
        }

        appendToList(temp);
    };

    function addLabel(today, activity, ul) {
        var d = new Date(activity.Created);
        var diff = today.getTime() - d.getTime();
        var seconds = Math.floor((diff / 1000));
        var minutes = Math.floor((seconds / 60));
        var hours = Math.floor((minutes / 60));
        var days = Math.floor((hours / 24));

        if (days === 1 && ul.find("li.yesterday").length === 0)
            $("<li class=\"label yesterday\">Yesterday</li>").appendTo(ul);
        if ((days > 1 && days <= 7) && ul.find("li.week").length === 0)
            $("<li class=\"label week\">1 week ago</li>").appendTo(ul);
        if ((days > 7 && days <= 31) && ul.find("li.month").length === 0)
            $("<li class=\"label month\">1 month ago</li>").appendTo(ul);
        if (days > 31 && ul.find("li.old").length === 0)
            $("<li class=\"label old\">Old activities</li>").appendTo(ul);
    };

    function appendToList(activities) {
        var ul = $("#activity_list");

        // Remove loading footer.
        ul.find("li.loading").remove();
        if (activities === null || activities.length === 0) {
            $("<li class=\"no-results\"><p>No results found.</p></li>").appendTo(ul);

            return;
        }

        var lastIndex = (activities.length - 1);
        var list;

        // Filter notifications.
        list = $.grep(activities, function (item, index) {
            return ignoreNotification(item.NotificationType);
        }, true);


        lastIndex = list.length - 1;
        $.each(list, function (index, item) {
            var notificationType = item.NotificationType;
            var li = $("<li />");
            var a = $("<a data-role=\"none\" data-notification-type=\"" + notificationType + "\" />");
            var img = $("<img />");
            var p = $("<p />")
            var userName = $("<span class=\"primary\" />");
            var isSub = spark.utils.common.isSub();
            var text = "";

            //var mailType = $("<span class=\"secondary\" />");
            //var icon = $("<span class=\"mail-icon\" />");
            //var member = (folderId !== 3) ? item.sender : item.recipient;
            var thumbnail = item.ThumbnailFile;

            addLabel(currentDate, item, ul);

            if (notificationType === 1 || notificationType === 4) {
                a.attr("href", "/profile/" + item.CreatorId);
            } else if ((notificationType === 2) && isSub || (notificationType === 3 && isSub)) {
                a.attr("href", "/mail/message/" + item.DataPoints.MAIL_MESSAGE_ID);
            } else if (notificationType === 2 && !isSub) {
                a.attr("data-ajax", false);
                a.attr("href", "/subscription/subscribe/4164");
            } else if (notificationType === 3 && !isSub) {
                a.attr("data-ajax", false);
                a.attr("href", "/subscription/subscribe/4163");
            }

            if (!isSub && notificationType === 2) {
                thumbnail = "../../images/icon_nophoto.png";
            } else if (!isSub && notificationType === 3) {
                thumbnail = "../../images/icon_nophoto.png";
            } else if (thumbnail !== null && thumbnail === "/images/hd/img_silhouette_man@2x.png") {
                thumbnail = "/images/hd/img_silhouette_man@2x.png";
            } else if (thumbnail !== null && thumbnail === "/images/hd/img_silhouette_woman@2x.png") {
                thumbnail = "/images/hd/img_silhouette_woman@2x.png";
            }

            img.attr("src", thumbnail);
            img.appendTo(a);

            //icon.appendTo(a);
            //icon.addClass(item.mailType.toLowerCase());
            //if (item.messageStatus !== 8) {
            //    a.addClass("read");
            //}

            text = item.MemberName + " " + getText(notificationType);
            if ((!isSub && notificationType === 2) || (!isSub && notificationType === 3))
                text = text.replace(item.MemberName, "Someone");

            userName.text(text);
            userName.appendTo(p);

            //mailType.text(item.mailType);
            //mailType.appendTo(p);

            //if (member.isOnline) {
            //    userName.addClass("online");
            //}

            p.appendTo(a);
            a.appendTo(li);

            if (index === lastIndex)
                li.addClass("last");

            li.appendTo(ul);
        });

        if (list.length === 0)
            ul.append("<li class=\"no-results\"><p>No results found.</p></li>");
    };

    function bindEvents() {
        var refreshButton = $("#btn_refresh");

        refreshButton.off().click(function () {
            refresh();
        });
    };

    function bindTabEvents() {
        var tabs = $("#activity_tabs");

        tabs.off().click(function (e) {
            var target = $(e.target);
            var notificationType;

            if (target.is("a") || target.is("span")) {
                notificationType = target.attr("href");
                if (typeof notificationType === "undefined")
                    notificationType = target.parent().attr("href");

                notificationType = notificationType.substring(notificationType.indexOf("/") + 1);


                if (!spark.utils.common.isSub()) {
                    if (notificationType == "email") {
                        window.location.href = "/subscription/subscribe/4161";
                    }
                    else if (notificationType == "flirts") {
                        window.location.href = "/subscription/subscribe/4162";
                    }
                    else {
                        changeTab(notificationType);
                    }                    
                }
                else {
                    changeTab(notificationType);
                }
            }

            e.preventDefault();
        });
    };

    function changeTab(type) {
        var tabs = $("#activity_tabs");
        
        tabs.find("li a").removeClass("active");
        tabs.find("li a." + type).addClass("active");

        activeType = type;
        applyFilter(type);
    };

    function getActivities(filter) {
        var ul = $("#activity_list");

        // Add loading footer.
        var loadingPanel = $("<li class=\"loading\"><p>Loading...</p></li>");
        var spinner = new Spinner({
            lines: 15,
            length: 5,
            width: 2,
            radius: 5
        }).spin();

        loadingPanel.find("p").prepend(spinner.el);
        loadingPanel.appendTo(ul);

        spark.api.client.callAPI(spark.api.client.urls.activities, {
            startNum: 1,
            pageSize: 50
        }, null, function (success, response) {
            if (!success) {
                var error = response.error;
                if (response.code === 401) {
                    spark.utils.common.redirectLogin();
                }

                return;
            }

            activityList = response.data
            if (filter) {
                applyFilter(filter);

                return;
            }

            appendToList(activityList);
        });
    };

    function getFilterType(type) {
        if (type === "flirts")
            return 2;
        if (type === "email")
            return 3;
        if (type === "favorites")
            return 4;
        if (type === "views")
            return 1;

        return 0;
    };

    function getText(type) {
        if (type === 1)
            return "viewed your profile";
        if (type === 2)
            return "sent you a flirt";
        if (type === 3)
            return "sent you a message";
        if (type === 4)
            return "added you to their favorite";

        return "";
    };

    function ignoreNotification(type) {
        if (type !== 1 && type !== 2 && type !== 3 && type !== 4)
            return true;

        return false;
    };

    function refresh() {
        var ul = $("#activity_list");

        ul.empty();
        spark.views.hotlist.getHotlistCount(function (data) {
            showBadge(data);
            getActivities(activeType);
        });
    };

    function showBadge(counts) {
        var tabs = $("#activity_tabs");
        var totalCount = 0;

        // Show favorites badge.
        if (counts.addedMeToTheirFavoritesNew > 0) {
            totalCount += counts.addedMeToTheirFavoritesNew;
            tabs.find("li a.favorites span:eq(1)").text(counts.addedMeToTheirFavoritesNew).show();
        }

        // Show viewed you badge.
        if (counts.viewedMyProfileNew > 0) {
            totalCount += counts.viewedMyProfileNew;
            tabs.find("li a.views span:eq(1)").text(counts.viewedMyProfileNew).show();
        }

        if (totalCount > 0) {
            tabs.find("li a.all span:eq(1)").text(totalCount).show();
        }
    };

    self.init = function () {
        bindEvents();
        bindTabEvents();
        getActivities();

        spark.views.hotlist.getHotlistCount(function (data) {
            showBadge(data);
        });
    };

    self.showSecretAdmirer = function () {
        $("#secret_admirer").secretadmirer("show");
    };

    return self;
})();

$(document).on("pagebeforehide", "#activity_page", function (e, ui) {

});

$(document).on("pageshow", "#activity_page", function () {
    var pageName = 'activity page - incoming';
    var options = {
        eVar2: pageName
    };

    spark.omniture.trackEvents(pageName, "event2", options);
    spark.views.activity.init();

    $("#btn_secret_admirer").click(function () {
        spark.views.activity.showSecretAdmirer();
    });
});

spark.views.admirer = (function () {
    var self = {};
    var isLoading = false;
    var isLastPage = false;

    function appendToList(data) {
        var ul = $("ul.scrollable");

        $.each(data, function (index, item) {
            var li = $("<li data-memberid=\"\" />");
            var a = $("<a />");
            var img = $("<img />");
            var span = $("<span />")
            var imageUrl = (item.gender === "Male") ? "/images/hd/img_silhouette_man@2x.png" : "/images/hd/img_silhouette_woman@2x.png";

            a.attr("href", "/profile/" + item.id);
            //if (item.defaultPhoto && item.defaultPhoto.thumbPath !== "") {
            //    imageUrl = item.defaultPhoto.thumbPath;
            //}

            img.attr("src", imageUrl).appendTo(a);
            span.text(item.username).appendTo(a);

            a.appendTo(li);
            li.appendTo(ul);
        });

        isLoading = false;
    }

    function getPreference() {
        var cookie = $.cookie("mos_admirers_pref_" + spark.utils.common.getMemberId());
        var tempArr = cookie.split("&");
        var prefs = {};

        $.each(tempArr, function (index, item) {
            var length = item.indexOf("=");
            var key = item.substring(0, length);
            var value = item.substring(length + 1);

            prefs[key] = value;
        });

        return prefs;
    }

    self.getAdmirers = function () {
        if (isLoading || isLastPage)
            return;

        isLoading = true;

        var page = $("#secretadmirer_page");
        var url = spark.api.client.urls.secretAdmirers;
        var prefs = getPreference();
        var pageSize = page.attr("data-pagesize");
        var pageNo = Number(page.attr("data-pageno"));

        prefs.PageSize = pageSize;
        prefs.PageNumber = (pageNo + 1);

        // Persist current page number. This wil be used as reference to the
        // next page number.
        page.attr("data-pageno", (pageNo + 1));

        spark.api.client.callAPI(url, null, prefs, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            if (response.data.length < pageSize)
                isLastPage = true;

            appendToList(response.data.Members);
        });
    };

    self.init = function () {

    };

    return self;
})();

spark.views.hotlist = (function () {
    var self = {};
    var isLoading = false;
    var isLastPage = false;

    function appendToList(data) {
        var ul = $("ul.scrollable");
        $.each(data, function (index, item) {
            var li = $("<li data-memberid=\"\" />");
            var a = $("<a />");
            var img = $("<img />");
            var p = $("<p />");
            var userName = $("<span class=\"primary\" />");
            var ageLocation = $("<span class=\"secondary\" />");
            var online = $("<span class=\"online\" />");
            var imageUrl = (item.miniProfile.gender === "Male") ? "/images/hd/img_silhouette_man@2x.png" : "/images/hd/img_silhouette_woman@2x.png";

            a.attr("href", "/profile/" + item.miniProfile.id);
            if (item.miniProfile.thumbnailUrl && item.miniProfile.thumbnailUrl !== "") {
                imageUrl = item.miniProfile.thumbnailUrl;
            }

            img.attr("src", imageUrl).appendTo(a);
            userName.text(item.username).appendTo(p);
            ageLocation.text(item.age + ", " + item.location).appendTo(p);

            if (!ul.hasClass("grid-view")) {
                online.text("Online");
            } else {
                // If in grid mode, apply neccessary in-style to override the CSS class.
                li.css({
                    height: gridHeight + "px"
                });

                img.css({
                    width: gridWidth + "px",
                    height: gridHeight + "px"
                });
            }

            online.appendTo(p);

            p.appendTo(a);
            a.appendTo(li);
            li.appendTo(ul);
        });

        isLoading = false;
    };

    self.getFavorites = function () {
        if (isLoading || isLastPage)
            return;

        isLoading = true;

        var page = $("#hotlist_page");
        var url = spark.api.client.urls.hotlist;
        var pageNo = Number(page.attr("data-pageno"));
        var pageSize = page.attr("data-pagesize");
        var params = {
            hotlist: "default",
            pageSize: pageSize,
            pageNumber: (pageNo + 1)
        };
        
        // Persist current page number. This wil be used as reference to the
        // next page number.
        page.attr("data-pageno", (pageNo + 1));

        spark.api.client.callAPI(url, params, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            if (response.data.length < pageSize)
                isLastPage = true;

            appendToList(response.data);
        });
    };

    self.getHotlistCount = function (callback) {
        spark.api.client.callAPI(spark.api.client.urls.hotlistCount, null, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            
            if(typeof callback === "function")
                callback(response.data);
        });
    };

    self.init = function () {
        
    };
  
   self.storeMembers = function () {
       var members = [];
       var count = 1;
        $("#favorites_list").children("li").each(function () {
            var li = $(this);
            li.attr("data-index", count++);
            members.push({
                memberId: li.find("a").attr("href").replace("/profile/", ""),
                thumbnail: li.find("a img").attr("src"),
                userName: li.find("a span.primary").text(),
                slideNo: li.attr("data-index")
            });
        }); 

        // Cache data so that it can be retrieve from the Profile page.
        $(document).data("members", members);
  
   }

   self.cleanCache = function () {
       $(document).removeData("members");
       $(document).removeData("cachedData");
   }

    return self;
})();

$(document).on("pagebeforehide", "#favorites_page", function (e, ui) {
    //check if the next page is the profile page
    if (ui.nextPage.attr('id') == 'profile_page') {
        spark.views.hotlist.storeMembers();
    }
    else {
        spark.views.hotlist.cleanCache();
    }
});

$(document).on("pageshow", "#favorites_page", function (e, ui) {

});

spark.views.im = (function () {
    var self = {};
    var dialog = null;
    var interval = (1000 * 20);
    var invites = [];
    var isLoading = false;
    var isActive = false;
    var removedInvites = [];
    var timerHandler;
    var chatServerName;
    var boshServer;
    var connection; // strophe connection
    var communityId;
 
    function closeInvite() {
        dialog.popup("close");
    };

    function connectToEjabberd(callback) {
        if (!communityId) {
            return; // unknown site
        }

        ownJid = spark.utils.common.getMemberId() + '-' + communityId + '@' + chatServerName;

        connection = new Strophe.Connection(boshServer);
        connection.connect(ownJid, spark.utils.common.getToken(), callback);
    };

    function filterBlockedInvites() {
        var params = {
            hotlist: "hide",
            targetMemberId: spark.utils.common.getMemberId(),
            pageSize: 500,
            pageNumber: 1
        };

        spark.api.client.callAPI(spark.api.client.urls.hotlist, params, null, function (success, response) {
            if (success && invites.length > 0) {
                var blockedUsers = {};
                var length = response.data.length;

                for (var j = 0; j < length; j++) {
                    blockedUsers[response.data[j].miniProfile.id] = true;
                }

                length = invites.length;
                for (var i = 0; i < invites.length; i++) {
                    var invite = invites[i];
                    if (invite.SenderMemberId in blockedUsers) {
                        invites.splice(i, 1);
                        i--;
                    }

                }
            }

            showInvite();
        });
    }

    function filterInvites(data) {
        var temp = [];
        var length = data.length;
        for (var i = 0; i < length; i++) {
            if (!isRemoved(data[i].ConversationKey))
                temp.push(data[i]);
        }

        return temp;
    };

    function getInvites() {
        closeInvite();
        spark.api.client.callAPI(spark.api.client.urls.getInvites, null, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            var data = response.data;
            if (data.length === 0)
                return;

            // Filter invitations in case we've already removed them from conversation.
            invites = filterInvites(response.data);
            filterBlockedInvites();
        });
    };

    function getRootDomain() { 
        var domainParts = document.domain.split('.');
        var last = domainParts.length - 1;
        var rootDomain;

        rootDomain = domainParts[last - 1] + '.' + domainParts[last];
        if (domainParts[last - 1] === "co") { // grab 3 parts: jdate co il, jdate co uk
            rootDomain = domainParts[last - 2] + '.' + domainParts[last - 1] + '.' + domainParts[last];
        }

        return rootDomain;
    };

    
    function handleIMWindowClosing(params) {
        var chatterJid = params.chatterMemberId + '-' + communityId + '@' + chatServerName;

        connectToEjabberd(function (status) {
            if (status === Strophe.Status.CONNECTED) {
                if (connection) {
                    connection.send($pres());
                    connection.send($pres({
                        to: chatterJid,
                        "type": "unavailable"
                    }));
                }
            } else if (status === Strophe.Status.CONNFAIL) {
                //spark.log.logError('Connection to chat server at ' + boshServer + ' failed.');
            } else if (status === Strophe.Status.AUTHFAIL) {
                //spark.log.logError('authorization with chat server failed');
            }
        });

        // if receiver never got online, trigger missed IM
        if (params.messageQueue && params.messageQueue.length > 0)
            sendMissedIM(params.messageQueue, params.chatterMemberId);

    };

    function hasActiveInvite() {
        return isActive;
    };

    function initializeDialog() {
        // Get handle to IM popup
        dialog = $("#im_invite_dialog");

        // Initialize IM Invite popup.
        dialog.trigger("create");
        dialog.popup({ tolerance: "15,5,15,5" });

        // Hack to disable closing popup when users tap outside.
        dialog.on({
            popupbeforeposition: function () {
                $('.ui-popup-screen').off();
            }
        });

        dialog.bind({
            popupafteropen: function () {
                isActive = true;
            },
            popupafterclose: function () {
                isActive = false;
            }
        });
    }

    function isRemoved(key) {
        var length = removedInvites.length;
        for (var i = 0; i < length; i++) {
            if (key === removedInvites[i])
                return true;
        }

        return false;
    };

    function openIM(url, chatterID) {
        window.open(url, '_' + chatterID);
    };

    function removeInvite(key) {
        var params = {
            ConversationKey: key
        };

        removedInvites.push(key);
        invites.splice((invites.length - 1), 1);

        spark.api.client.callAPI(spark.api.client.urls.removeInvite, {}, params, function (success, response) {
            // Do nothing.
        });
    };

    function sendEjabIMRejectedMessage(recipientMemberId, conversationTokenId) {
        var declineJid;
        var memberId = spark.utils.common.getMemberId();

        if (!conversationTokenId) {
            declineJid = memberId + '-' + communityId + '@' + chatServerName;
        } else {
            declineJid = recipientMemberId + '#' + memberId + '#' + conversationTokenId + '-' + communityId + '@' + chatServerName;
        }
        var recipientJid = recipientMemberId + '-' + communityId + '@' + chatServerName;
        connection = new Strophe.Connection(boshServer);
        connection.connect(declineJid, spark.utils.common.getToken(), function (status) {
            if (status === Strophe.Status.CONNECTED) {
                if (connection) {
                    connection.send($pres());
                    connection.send($pres({
                        to: recipientJid,
                        "type": "declined"
                    }));
                }
            } else if (status === Strophe.Status.CONNFAIL) {
                //spark.log.logError('Connection to chat server at ' + boshServer + ' failed.');
            } else if (status === Strophe.Status.AUTHFAIL) {
                //.log.logError('authorization with chat server failed');
            }
        });
    };

    function sendMissedIM(messageQueue, targetMemberId) {
        if (messageQueue.length == 0)
            return;

        var postData = {
            recipientMemberId: targetMemberId,
            messages: messageQueue
        };

        spark.api.client.callAPI(spark.api.client.url.missedIM, null, postData, function (success, data) {
            // Do nothing
        });
    };

    function showInvite() {
        var length = invites.length;
        if (length === 0 || hasActiveInvite())
            return;

        var invite = invites[length - 1];
        var senderId = invite.SenderMemberId;
        var key = invite.ConversationKey;
        var canMemberReply = invite.CanMemberReply;
        var conversationTokenId = invite.ConversationTokenId;

        spark.api.client.callAPI(spark.api.client.urls.miniprofile, { targetMemberId: senderId }, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            var data = response.data;
            var isSub = spark.utils.common.isSub();
            var img = $(dialog).find("img");
            var userName = $(dialog).find("span.username");
            var ageLocation = $(dialog).find("span.age-location");
            var noBtn = $(dialog).find("a.no");
            var yesBtn = $(dialog).find("a.yes");
            var imageUrl = (data.primaryPhoto !== null) ?
                data.primaryPhoto.thumbPath : (data.gender === "Male") ?
                    "/images/hd/img_silhouette_man@2x.png" : "/images/hd/img_silhouette_woman@2x.png";

            img.attr("src", imageUrl);
            img.off("click").on("click", function () {
                viewProfile("/profile/" + data.memberId);
            });

            userName.off("click").on("click", function () {
                viewProfile("/profile/" + data.memberId);
            });

            userName.text(data.username);
            var temp = "";
            if (data.age)
                temp = data.age;
            if (temp !== "" && data.location)
                temp += ", " + data.location;
            if (temp === "" && data.location)
                temp = data.location;
            ageLocation.text(temp);

            // Bind event to NO button
            noBtn.off("click").on("click", function () {
                removeInvite(key);
                sendEjabIMRejectedMessage(senderId, conversationTokenId);
                closeInvite();

                //spark.omniture.trackEvents("m_im_invitation", "event69",
                  //  { eVar2: "m_im_invitation" });

                window.setTimeout(showInvite, 500);
            });

            if (!canMemberReply) {
                yesBtn.text("Upgrade");
            }

            yesBtn.unbind("click").bind("click", function (e) {
                removeInvite(key);
                closeInvite();

                var a = $(this);
                if (!canMemberReply) {
                    // Go to subscription page.
                    a.addClass("subscription");
                    a.attr("href", "/subscription/subscribe/2260");
                } else {
                    var chatUrl = a.attr("href");

                    chatUrl += "?memberId=" + data.memberId + "&conversationTokenId=" + conversationTokenId;
                    openIM(chatUrl, senderId);
                }


                // Show next invite so when user goes back to original
                // page the user can take action.
                setTimeout(showInvite, 500);
                e.preventDefault();
            });

            // Show IM invite dialog.
            dialog.removeClass("hidden").show();
            dialog.popup("open");
        });
    };

    function viewProfile(url) {
        $.mobile.changePage(url);
    };

    self.init = function (chatServer) {
        var siteLookup = {
            "blacksingles": 24,
            "jdate": 3,
            "spark": 1,
            "bbwpersonalsplus": 23,
            "cupid": 10
        };

        var domain = getRootDomain();
        var communityName = domain.split('.')[0];
        communityId = siteLookup[communityName];

        boshServer = chatServer;
        var temp = document.createElement('a');
        temp.href = boshServer;
        chatServerName = temp.hostname;
            
        $(window).bind("message", function (event) {
            if (getRootDomain(event.originalEvent.origin) !== getRootDomain(window.location.href)) // TODO: Does not make sense?
                return;

            if (event.originalEvent.data.indexOf("chatterMemberId") !== -1) {
                var params = JSON.parse(event.originalEvent.data);
                handleIMWindowClosing(params);
            }
        });

        invites = [];
        removeInvites = [];

        initializeDialog();
        timerHandler = window.setInterval(getInvites, interval);
    };

    return self;
})();

spark.views.mail = (function () {
    var self = {};
    var activeFolderId = 0;
    var messageList = {
        inbox: [],
        sent: [],
        trash: []
    };
    var isLoading = false;
    var isLastPage = false;
    var swiper = null;
    var currentDate = new Date();
    var offsetFromTop = 0;

    function addTabEvents() {
        var tabs = $("ul.mail-tabs");
        tabs.children("li").each(function () {
            var li = $(this);

            // Bind event to anchor link(s).
            li.find("a").click(function (e) {
                var a = $(this);
                var folderId = getFolderId(a.attr("href"));

                $("#mail_page").attr("data-pageno", 1);
                isLoading = false;
                isLastPage = false;

                changeTabs(folderId);
                e.preventDefault();
            });
        });
    };

    function addLabel(today, message, ul) {
        var d = new Date(message.insertDate);
        var diff = today.getTime() - d.getTime();
        var seconds = Math.floor((diff / 1000));
        var minutes = Math.floor((seconds / 60));
        var hours = Math.floor((minutes / 60));
        var days = Math.floor((hours / 24));

        if (days === 1 && ul.find("li.yesterday").length === 0) 
            $("<li class=\"label yesterday\">Yesterday</li>").appendTo(ul);
        if ((days > 1 &&  days <= 7) && ul.find("li.week").length === 0) 
            $("<li class=\"label week\">1 week ago</li>").appendTo(ul);
        if ((days > 7 && days <= 31) && ul.find("li.month").length === 0) 
            $("<li class=\"label month\">1 month ago</li>").appendTo(ul);
        if (days > 31 && ul.find("li.old").length === 0) 
            $("<li class=\"label old\">Old messages</li>").appendTo(ul);
    };

    function appendToList(messages, folderId) {
        var ul = $("#mail_list");
        var lastIndex = (messages.length - 1);

        // Remove loading footer.
        ul.find("li.loading").remove();

        $.each(messages, function (index, item) {
            var li = $("<li />");
            var a = $("<a data-role=\"none\"/>");
            var img = $("<img />");
            var p = $("<p />")
            var userName = $("<span class=\"primary\" />");
            var mailType = $("<span class=\"secondary\" />");
            var icon = $("<span class=\"mail-icon\" />");
            var member = (folderId !== 3) ? item.sender : item.recipient;
            var thumbnail = (member.gender === "Female") ?
                "/images/img_silhouette_woman.png" : "/images/hd/img_silhouette_woman@2x.png";

            addLabel(currentDate, item, ul);
                
            a.attr("href", "/mail/message/" + item.id);

            if (member.thumbnailUrl !== null) {
                thumbnail = member.thumbnailUrl;
            }

            img.attr("src", thumbnail);
            img.appendTo(a);

            icon.appendTo(a);
            icon.addClass(item.mailType.toLowerCase());
            if (item.messageStatus !== 8) {
                a.addClass("read");
            }

            userName.text(member.username);
            userName.appendTo(p);

            mailType.text(item.mailType);
            mailType.appendTo(p);

            if (member.isOnline) {
                userName.addClass("online");
            }

            p.appendTo(a);
            a.appendTo(li);

            li.click(function (e) {
                var a = $(this).find("a");
                var href = a.attr("href");
                var messageId = href.substring(href.lastIndexOf("/") + 1);

                showMessage(messageId);
                e.preventDefault();
            });

            if (index === lastIndex)
                li.addClass("last");

            li.appendTo(ul);

            storeMessages(folderId, item);
        });

        if (messages.length === 0)
            ul.append("<li class=\"no-results\"><p>No results found.</p></li>");

    }

    function appendSwiperSlides() {
        var swiper = $("#message_swiper");
        var wrapper = swiper.find(".swiper-wrapper");
        var list = getMessageList();

        $.each(list, function (index, item) {
            var slider = $("<div class=\"swiper-slide\" />");
            var container = $("<div class=\"message-container\" />");

            //container.attr("data-index", index);
            container.appendTo(slider);
            slider.appendTo(wrapper);
        });
    }   

    function changeTabs(folderId) {
        var tabs = $("ul.mail-tabs");
        var list = $("#mail_list");
        var selectedIndex = 0;

        if (activeFolderId !== folderId) {
            //list.find("li.loading").removeClass("hidden");
            //list.find("li:gt(0)").remove();
            list.empty();
            messageList = {
                inbox: [],
                sent: [],
                trash: []
            };
        }

        tabs.children("li").each(function (index) {
            var li = $(this);
            var a = li.find("a");

            a.removeClass("active")
                .removeClass("border-left")
                .removeClass("border-right");

            if (getFolderId(a.attr("href")) === folderId) {
                a.addClass("active");
            }
            
            var parent = li.parent();
            switch (folderId) {
                case 1:
                    parent.find("li:eq(1) a").addClass("border-left");
                    parent.find("li:eq(2) a").addClass("border-left");
                    break;
                case 3:
                    parent.find("li:eq(0) a").addClass("border-right");
                    parent.find("li:eq(2) a").addClass("border-left");
                    break;
                default:
                    parent.find("li:eq(0) a").addClass("border-right");
                    parent.find("li:eq(1) a").removeClass("border-left").addClass("border-right");
                    break;
            }
        });

        activeFolderId = folderId;
        getMessages(folderId);
    }

    function convertMonthToString(month) {
        switch(month) {
            case 0:
                return "Jan";
            case 1:
                return "Feb";
            case 2:
                return "Mar";
            case 3:
                return "Apr";
            case 4:
                return "May";
            case 5:
                return "Jun";
            case 6:
                return "Jul";
            case 7:
                return "Aug";
            case 8:
                return "Sep";
            case 9:
                return "Oct";
            case 10:
                return "Nov";
            case 11:
                return "Dec";
        }
    }

    function createMessagePanel(message, index) {
        var swiper = $("#message_swiper");
        var slide = swiper.find(".swiper-wrapper").children(":eq(" + index + ")");
        var container = slide.children(".message-container");

        // We've already processed the slide, so just ignore further
        // processing.
        if (container.attr("data-processed")) {
            return;
        }

        var profileContainer = $("<div class=\"profile-container\" />");
        var profileDetailContainer = $("<p />");
        //var imageContainer = $("<span />");
        var img = $("<img />");
        var userName = $("<span class=\"username\" />");
        var age = $("<span class=\"age\" />");
        var location = $("<span class=\"location\" />");
        var messageDate = $("<span class=\"date\" />");
        var messageBody = $("<p />");
        var trash = $("<span class=\"trash\" />")
        var member = (activeFolderId !== 3) ? message.sender : message.recipient;
        var imageUrl = (member.gender === "Male") ? "/images/hd/img_silhouette_man@2x.png" : "/images/hd/img_silhouette_woman@2x.png";
        var body = (message.body) ? message.body.replace(/\n/g, '<br />') : "";

        userName.text(member.username);
        age.text(member.age + " years old");
        location.text(member.location);

        messageDate.text(parseDate(message.insertDate));
        messageBody.addClass(message.mailType.toLowerCase());
        messageBody.html(body);

        if (member.thumbnailUrl !== null) {
            imageUrl = member.thumbnailUrl;
        }

        trash.attr("data-messageid", message.id);

        img.prop("src", imageUrl);
        img.appendTo(profileContainer);
        profileContainer.click(function () {
            $.mobile.changePage("/profile/" + member.id);
        });

        userName.appendTo(profileDetailContainer);
        age.appendTo(profileDetailContainer);
        location.appendTo(profileDetailContainer);
        messageDate.appendTo(messageBody);
        trash.appendTo(messageBody);

        profileDetailContainer.appendTo(profileContainer);
        profileContainer.appendTo(container);
        messageBody.appendTo(container);

        container.attr("data-processed", "true");
        // Get message detail.
        getMessageDetail(message.id, function (data) {
            body = (data.body) ? data.body.replace(/\n/g, '<br />') : "";
            messageBody.html(body);

            messageDate.appendTo(messageBody);
            trash.appendTo(messageBody);
        });
    }

    function deleteMessage(messageId) {
        spark.utils.common.showConfirm("Are you sure you want to delete this message?", function () {
            var params = {
                currentFolderId: activeFolderId,
                messageId: messageId
            };

            spark.api.client.callAPI(spark.api.client.urls.deleteMessage, params, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                removeSlide(swiper.activeIndex);
                spark.utils.common.showMessageBar("Message has been deleted", true);
            });
        });
    }

    function dispose() {
        activeFolderId = 0;
        messageList = {
            inbox: [],
            sent: [],
            trash: []
        };
        swiper = null;
    }

    function getFolderId(path) {
        if (path.indexOf("inbox") > 0)
            return 1;
        if (path.indexOf("sent") > 0)
            return 3;
        if (path.indexOf("trash") > 0)
            return 4;
    }

    function getMessage(messageId) {        
        var messages = getMessageList();
        var length = messages.length;
        
        return messages[getSelectedIndex(messageId)];
    }

    function getMessageDetail(messageId, callback) {
        spark.api.client.callAPI(spark.api.client.urls.message, { messageListId: messageId }, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }
            callback(response.data);
        });
    }

    function getSelectedIndex(messageId) {
        var messages = getMessageList();
        var length = messages.length;
 
        for (var i = 0; i < length; i++) {
            if (messageId == messages[i].id) {
                return i;
            }
        }

        return 0;
    }

    function getMessages(folderId, callback) {
        if (isLoading || isLastPage)
            return;

        isLoading = true;

        var ul = $("#mail_list");
        var page = $("#mail_page");
        var pageSize = page.attr("data-pagesize");
        var pageNo = Number(page.attr("data-pageno"));
        var params = {
            folderId: folderId,
            pageSize: pageSize,
            pageNumber: (ul.find("li").length === 0) ? pageNo : (pageNo + 1)
        }

        // Add loading footer.
        var loadingPanel = spark.utils.common.loadingFooter();
        loadingPanel.appendTo(ul);

        // Persist current page number. This wil be used as reference to the
        // next page number.
        page.attr("data-pageno", params.pageNumber);

        // Make API call to retrieve messages.
        spark.api.client.callAPI(spark.api.client.urls.messages, params, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            if (response.data.messageList.length < pageSize)
                isLastPage = true;

            appendToList(response.data.messageList, folderId);
            if (typeof callback === "function")
                callback();
        });
    }

    function getMessageList() {
        switch (activeFolderId) {
            case 3:
                return messageList["sent"];
            case 4:
                return messageList["trash"];
            default:
                return messageList["inbox"];
        }
    };

    function gotoMessage(index) {
        var messages = getMessageList();
        var message = messages[index];

        createMessagePanel(message, index);
        resizeSwiper(index);
    };

    function getButtonText() {
        switch (activeFolderId) {
            case 3:
                return "Sent";
            case 4:
                return "Trash";
            default:
                return "Inbox";
        }
    };

    function hideReply() {
        $("div.reply").hide();
    };

    function removeSlide(index) {
        var messages = getMessageList();
        var wrapper = $(swiper.container).find(".swiper-wrapper");

        wrapper.children("div:eq(" + index + ")").remove();

        // Remove message from cache array.
        messages.splice(index, 1);

        // Check if there are more records in the last, if not then
        // take users back to the list page.
        var length = messages.length;
        if (length === 0) {
            showList();
            return;
        }

        // Check if last element from list is deleted, if so move back the
        // index to the last element (of the new array).
        if (index === length)
            --index;

        // Create message panel to show
        createMessagePanel(messages[index], index);

        // Re-initialize the swiper.
        swiper.reInit();
        swiper.swipeTo(index, 100);
    }

    function resizeSwiper(index) {
        var swiperContainer = $(swiper.container);
        var swiperWrapper = swiperContainer.find(".swiper-wrapper");
        var slide = swiperWrapper.find(".swiper-slide:eq(" + index + ")");

        var height = slide.find(".message-container").height();
        $(swiper.container).css({
            height: height + "px"
        });
    }

    function reply(callback) {
        var messages = getMessageList();
        var originalMessage = messages[swiper.activeIndex];
        var recipientId = originalMessage.sender.id;
        var subject = "RE: " + originalMessage.subject;
        var body = $("#message_body").val();
        var postData = {
            RecipientMemberId: recipientId,
            Subject: subject,
            Body: body,
            MailType: "Email"
        };
        var url = spark.api.client.urls.sendMessage;

        if (body.length === 0) {
            spark.utils.common.showGenericDialog("Message body is required. Please type a message to send.");
            return;
        }

        spark.api.client.callAPI(url, null, postData, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            callback(response.data);
        });
    };

    function showList() {
        $("#message_detail_panel").fadeOut(350, function () {
            var swiperWrapper = $(swiper.container).find(".swiper-wrapper");
            swiperWrapper.empty();

            // Clear list to fetch fresh data.
            $("#mail_list").empty();
            messageList = {
                inbox: [],
                sent: [],
                trash: []
            };

            $("#mail_page").attr("data-pageno", 1);
            isLoading = false;
            isLastPage = false;

            getMessages(activeFolderId, function () {
                var panel = $("#message_list_panel");
                panel.fadeIn();
                $(document).scrollTop(offsetFromTop);
            });
        });
    }

    function showMessage(messageId) {
        offsetFromTop = $(window).scrollTop();
        $("#message_list_panel").addClass("hidden").hide();
        $(document).scrollTop(0);

        var header = $(".mail-message-header");
        var backButton = header.find(".back-button");
        var prevButton = header.find(".prev");
        var nextButton = header.find(".next");
        var sendButton = $("#btn_send");

        backButton.text(getButtonText());
        backButton.unbind("click").click(function () {
            showList();
            showReply();
        });

        prevButton.unbind("click").bind("click", function () {
            swiper.swipePrev();
        });

        nextButton.unbind("click").bind("click", function () {
            swiper.swipeNext();
        });

        sendButton.unbind("click").bind("click", function () {
            reply(function () {
                spark.utils.common.showMessageBar("Message sent successfully", true);
                $("#message_body").val("");
            });
        });

        if (activeFolderId === 4)
            hideReply();
        appendSwiperSlides();

        var panel = $("#message_detail_panel");
        var message = getMessage(messageId);
        var index = getSelectedIndex(messageId);

        createMessagePanel(message, index);
        panel.fadeIn(500, function () {
            panel.removeClass("hidden");
        });

        if (swiper === null) {
            swiper = $("#message_swiper").swiper({
                initialSlide: index,
                loop: false,
                mode: "horizontal",
                speed: 600,
                onSlidePrev: function () {
                    gotoMessage(swiper.activeIndex);
                },
                onSlideNext: function () {
                    gotoMessage(swiper.activeIndex);
                }
            });

            $(swiper.container).click(function (e) {
                var target = $(e.target);
                if (target.is("span") && target.hasClass("trash")) {
                    var messageId = target.attr("data-messageid");
                    deleteMessage(messageId)
                }
            });

            $(swiper.container).css({
                clear: "both",
                width: "100%"
            });

            return;
        }

        // Since swiper widget is initialized, just re-initialize then go to
        // selected index.
        swiper.reInit();
        swiper.swipeTo(index, 100);
    };

    function showReply() {
        $("div.reply").show();
    };

    function parseDate(dateString) {
        var d = new Date(dateString);

        var year = d.getUTCFullYear();
        var month = d.getUTCMonth();
        var date = d.getUTCDate();
        var hour = d.getUTCHours();
        var minute = d.getUTCMinutes();
        var monthString = convertMonthToString(month);

        var dayOrNight = "AM";
        if (hour == 12) {
            dayOrNight = "PM";
        }

        if (hour > 12) {
            dayOrNight = "PM";
            hour = hour % 12;
        }

        return monthString + " " + date + ", " + year + " " + hour + ":" + minute + " " + dayOrNight;
    }

    function storeMessages(folderId, message) {
        switch (folderId) {
            case 3:
                messageList["sent"].push(message);
                return;
            case 4:
                messageList["trash"].push(message);
                return;
            default:
                messageList["inbox"].push(message);
        }
    };

    self.getActiveFolder = function () {
        return activeFolderId;
    };

    self.getMessages = function (folderId) {
        getMessages(folderId);
    };

    self.init = function () {
        swiper = null;

        addTabEvents();
        activeFolderId = 0;

        // Get user's inbox (default).
        $("a[href='/mail/inbox']").click();
    };

    self.sendMessage = function (memberId, subject, body, callback) {
        var url = spark.api.client.urls.sendMessage;
        var postData = {
            RecipientMemberId: memberId,
            Body: body,
            Subject: subject,
            MailType: "Email"
        };
        
        if ($.trim(subject).length === 0) {
            spark.utils.common.showGenericDialog("Subject is required. Please type a subject.");
            return;
        }

        if ($.trim(body).length === 0) {
            spark.utils.common.showGenericDialog("Message body is required. Please type a message to send.");
            return;
        }

        spark.api.client.callAPI(url, null, postData, function (success, response) {
            if (!success) {
                //spark.utils.common.showGenericDialog("There was a problem sending your message", true);
                spark.utils.common.showError(response);
                return;
            }

            callback(response.data);
        });

    };

    return self;
})();

$(document).on("pageshow", "#mail_page", function () {
    var pageName = 'messages';
    var options = {
        eVar2: pageName
    };
    spark.omniture.trackEvents(pageName, "event2", options);
    spark.views.mail.init();
});


spark.views.matches = (function () {
    "use strict";
    var self = {};
    var memberList = [];
  
    var gender = "Male";
    var seeking = "Female";
    var photoOnly = false;
    var jewishOnly = true;
    var location = "";
    var regionID = "";
    var distance = 5;
    var minAge = 18;
    var maxAge = 29;
    var keyword = "";

    var showPreferencePage = false;
    var pageInit = true;
    var isLoading = false;
    var isLastPage = false;
    var prefCookieID = "mos_matches_pref_" + spark.utils.common.getMemberId();

    var gridHeight = 0;
    var gridWidth = 0;

    function setupCookie() {
        var isMale = true;
        var isFemale = false;
        var seekingMale = false;
        var seekingFemale = true;

        if (gender == "Female") {
            isMale = false;
            isFemale = true;
        }

        if (seeking == "Male") {
            seekingMale = true;
            seekingFemale = false
        }

        $.cookie(prefCookieID, "MemberId=" + spark.utils.common.getMemberId() + "&Gender=" + gender + "&SeekingGender=" + seeking + "&MinAge=" + minAge + "&MaxAge=" + maxAge + "&MaxDistance=" + distance + "&Location=" + location + "&ShowOnlyMembersWithPhotos=" + photoOnly + "&Keyword=" + keyword + "&IsMale=" + isMale + "&IsFemale=" + isFemale + "&SeekingMale=" + seekingMale + "&SeekingFemale=" + seekingFemale);
    }

    function validateZipcode(zipcode) {
        var CNRegex = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;  // Validate either with or without " ", "-"
        var USRegex = /(^\d{5}$)|(^\d{5}-\d{4}$)/;  // Validate either 5 or 5+4 digits
        var isValid = false;

        if (zipcode.length == 5) {
            isValid = USRegex.exec(zipcode);
        }
        else if (zipcode.length == 6) {
            isValid = CNRegex.exec(zipcode);
        }
        else if (zipcode == "") {
            isValid = true;
        }
        else {
            return false;
        }

        return isValid;
    }

    function setupButtonClickEvent() {
        var genderToggle = $("#gender");
        var genderSeekingToggle = $("#gender_seeking");
        var ddMinAge = $("#min_age");
        var ddMaxAge = $("#max_age");
        var ddDistance = $("#distance");
        var cbPhotos = $("#picture_only");
        var cbJewish = $("#jewish_only");

        // TODO: Create widget to abstract functionality.
        genderToggle.off("click").on("click", function (e) {
            var self = $(this);
            var target = $(e.target);

            target.addClass("selected");
            if (target.hasClass("female")) {
                self.find("a.male").removeClass("selected");
                gender = "Female";
            }

            if (target.hasClass("male")) {
                self.find("a.female").removeClass("selected");
                gender = "Male";
            }
        });

        genderSeekingToggle.off("click").on("click", function (e) {
            var self = $(this);
            var target = $(e.target);

            target.addClass("selected");
            if (target.hasClass("female")) {
                self.find("a.male").removeClass("selected");
                seeking = "Female";
            }

            if (target.hasClass("male")) {
                self.find("a.female").removeClass("selected");
                seeking = "Male";
            }
        });

        ddMinAge.find("select").on("change", function () {
            minAge = this.value;
        });

        ddMaxAge.find("select").on("change", function () {
            maxAge = this.value;
        });

        ddDistance.find("select").on("change", function () {
            distance = this.value;
        });


        cbPhotos.off().on("click", function (e) {
            var self = $(this);
            if (self.hasClass("checked")) {
                self.removeClass("checked");
                photoOnly = false;
            } else {
                self.addClass("checked");
                photoOnly = true;
            }
        });

        cbJewish.off().on("click", function (e) {
            var self = $(this);
            if (self.hasClass("checked")) {
                self.removeClass("checked");
                jewishOnly = false;
            } else {
                self.addClass("checked");
                jewishOnly = true;
            }
        });

        $("#match-edit-btn").click(function () {
            showPreferencePage = true;
            $(".match-main-page").addClass("hidden");
            $(".match-preferences-page").removeClass("hidden");
            $(".match-preferences-page").fadeIn(350);

            getPreference();
        });

        $("#save-btn").click(function () {
            minAge = ddMinAge.find("select").val();
            maxAge = ddMaxAge.find("select").val();
   
            if (minAge > maxAge || minAge == "" || maxAge == "") {
                spark.utils.common.showGenericDialog("Please select the right age range!");
                return;
            }

            //location = $("#preferences-zipcode-textarea").val();
            location = $("#postal_code").val();
            keyword = $("#keyword").val();

            if (validateZipcode(location)) {
                setupCookie();
                getPreference();

                $(".match-preferences-page").fadeOut(350, function () {
                    $("#matches_list").find("li").remove();
                    $("#matches_list").find("p").remove();

                    isLastPage = false;
                    showPreferencePage = false;
                    $(".match-main-page").removeClass("hidden");
                    $(".match-preferences-page").addClass("hidden");

                    //callAPIforMatchesData(1);
                    getLocationByZipCode();
                });
            }
            else {
                spark.utils.common.showGenericDialog("Invalid Zip code!");
            }            
        });

        $("#cancel-btn").click(function () {
            $(".match-preferences-page").fadeOut(350, function () {
                showPreferencePage = false;
                $(".match-main-page").removeClass("hidden");
                $(".match-preferences-page").addClass("hidden");
            });
        });
    }

    function appendToList(data) {
        var ul = $("ul.scrollable");

        // Remove loading footer.
        ul.find("li.loading").remove();

        if (data.length == 0) {
            var p = $("<p />");
            p.text("No results found!")
            p.addClass("no-results-text");
            p.appendTo(ul);
        }

        $.each(data, function (index, item) {
            var li = $("<li data-memberid=\"" + item.memberId + "\" />");
            var a = $("<a />");
            var img = $("<img />");
            var p = $("<p />");
            var userName = $("<span class=\"primary\" />");
            var ageLocation = $("<span class=\"secondary\" />");
            var online = $("<span class=\"online\" />");
            var mPercentage = $("<span class = \"matches-label\" />");
            var imageUrl = (item.gender === "Female") ? "/images/hd/img_silhouette_woman@2x.png" : "/images/hd/img_silhouette_man@2x.png";

            a.attr("href", "/profile/" + item.memberId);
            if (item.defaultPhoto && item.defaultPhoto.thumbPath !== "") {
                imageUrl = item.defaultPhoto.fullPath;
            }
            else if (item.DefaultPhoto && item.DefaultPhoto.thumbPath !== "") {
                imageUrl = item.DefaultPhoto.fullPath;
            }

            img.attr("src", imageUrl).appendTo(a);
            userName.text(item.username).appendTo(p);

            var ageText = "";
            var locText = "";

            if (item.age !== null) {
                ageText = item.age;
            }

            if (item.location !== null) {
                locText = item.location;
                ageLocation.text(ageText + ", " + locText).appendTo(p);
            }
            else {
                ageLocation.text(ageText).appendTo(p);
            }

            if (item.isOnline)
                online.text("Online").appendTo(p);

            if (!ul.hasClass("grid-view")) {
                online.text("Online");
            } else {
                // If in grid mode, apply neccessary in-style to override the CSS class.
                li.css({
                    height: gridHeight + "px"
                });

                img.css({
                    width: gridWidth + "px",
                    height: gridHeight + "px"
                });
            }
            
            p.appendTo(a);
            
            if (item.matchRating != null) {
                mPercentage.text(item.matchRating + "% match");
                mPercentage.appendTo(a);
            }

            a.appendTo(li);
            li.appendTo(ul);

            memberList.push(item);
        });

        if (pageInit && spark.utils.common.loadViewToggleStatus() === "grid") {
            showGrid(ul);
        }

        isLoading = false;
        pageInit = false;
    }

    function getPreference() {
        if ($.cookie(prefCookieID) != null) {
            var cookie = $.cookie(prefCookieID);
            var tempArr = cookie.split("&");
            var prefs = {};

            $.each(tempArr, function (index, item) {
                var length = item.indexOf("=");
                var key = item.substring(0, length);
                var value = item.substring(length + 1);

                prefs[key] = value;
            });

            gender = prefs.Gender;
            seeking = prefs.SeekingGender;
            minAge = prefs.MinAge;
            maxAge = prefs.MaxAge;
            distance = prefs.MaxDistance;
            location = prefs.Location;
            keyword = prefs.Keyword;

            if (prefs.ShowOnlyMembersWithPhotos.toLowerCase() == "true") {
                photoOnly = true;
            }
            else {
                photoOnly = false;
            }

            setupDefaultPref();
        }
        else {
            spark.api.client.callAPI(spark.api.client.urls.get_preferences, null, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }


                var data = response.data;

                if (data.gender == "Female") {
                    seeking = "female";
                }
                else {
                    seeking = "male";
                }

                if (data.seekingGender == "Male") {
                    gender = "male";
                }
                else {
                    gender = "female";
                }

                if (data.minAge != null) {
                    minAge = data.minAge;
                }

                if (data.maxAge != null) {
                    maxAge = data.maxAge;
                }

                if (data.location != null) {
                    location = data.location;
                }

                if (data.maxDistance != null) {
                    distance = data.maxDistance;
                }

                if (data.showOnlyMembersWithPhotos != null) {
                    photoOnly = data.showOnlyMembersWithPhotos;
                }

                var cachedData = $(document).data("cachedData");

                if (cachedData == null) {
                    if (pageInit) {
                        callAPIforMatchesData(1);
                    }
                }

                setupDefaultPref();
                setupCookie();
            });
        }
    }

    function setupDefaultPref() {
        var genderToggle = $("#gender");
        var genderSeekingToggle = $("#gender_seeking");
        var ddMinAge = $("#min_age");
        var ddMaxAge = $("#max_age");
        var ddDistance = $("#distance");
        var postalCode = $("#postal_code");
        var keywordText = $("#keyword");
        var cbPhotos = $("#picture_only");
        var cbJewish = $("#jewish_only");

        // Set gender value.
        genderToggle.find("a.female").removeClass("selected");
        genderToggle.find("a.male").removeClass("selected");
        if (gender.toLocaleLowerCase() === "female") {
            genderToggle.find("a.female").addClass("selected");
        } else {
            genderToggle.find("a.male").addClass("selected");
        }

        // Set seeking gender.
        genderSeekingToggle.find("a.female").removeClass("selected");
        genderSeekingToggle.find("a.male").removeClass("selected");
        if (seeking.toLocaleLowerCase() === "female") {
            genderSeekingToggle.find("a.female").addClass("selected");
        } else {
            genderSeekingToggle.find("a.male").addClass("selected");
        }

        // Set age range, distance and postal/zip code value.
        ddMinAge.find("select").val(minAge);
        ddMaxAge.find("select").val(maxAge);
        ddDistance.find("select").val(distance);
        postalCode.val(location);
        keywordText.val(keyword);

        cbPhotos.removeClass("checked");
        cbJewish.removeClass("checked");
        if (photoOnly)
            cbPhotos.addClass("checked");
        if (jewishOnly)
            cbJewish.addClass("checked");
    }

    function getLocationByZipCode() {
        isLoading = true;

        if (location !== "") {
            var params = {
                zipCode: location
            };

            spark.api.client.callAPI(spark.api.client.urls.get_location, params, null, function (success, response) {
                if (!success) {
                    isLoading = false;
                    spark.utils.common.showError(response);
                    return;
                }

                regionID = response.data[0].Id;
                //callAPIforPutPref();
                callAPIforMatchesData(1);
            });
        }
        else {
            regionID = "";
            //callAPIforPutPref();
            callAPIforMatchesData(1);
        }
    }

    function initializeListToggle() {
        var ul = $("#matches_list");
        var toggle = $(".list-grid-toggle");
        var listButton = toggle.find("a.list");
        var gridButton = toggle.find("a.grid");
        //var gridClass = "grid-view";

        if (spark.utils.common.loadViewToggleStatus() === "grid") {
            $("#btn_grid_view").addClass("active");
            $("#btn_list_view").removeClass("active");
        }

        listButton.off().on("click", function (e) {
            showList(ul);
        });

        gridButton.off().on("click", function (e) {
            showGrid(ul);
        });
    };

    function callAPIforMatchesData(pageNo) {
        isLoading = true;

        var ul = $("ul.scrollable");
        var page = $("#matches_page");
        //var url = spark.api.client.urls.matches;
        var url = spark.api.client.urls.getSearchResults;
        var pageSize = page.attr("data-pagesize");
        
        var prefs = {
            pageSize: pageSize,
            pageNumber: pageNo,
            Gender: seeking,
            SeekingGender: gender,
            MinAge: minAge,
            MaxAge: maxAge,
            Location: regionID,
            MaxDistance: distance,
            ShowOnlyMembersWithPhotos: photoOnly,
            searchOrderByType: 7,
            searchType: 4,
            searchEntryPointType: 1,
            Keywords:keyword,
        };

        //var params = {
        //    pageSize: pageSize,
        //    pageNumber: pageNo
        //};

        //prefs.PageSize = page.attr("data-pagesize");
        //prefs.PageNumber = (pageNo + 1);

        // Persist current page number. This wil be used as reference to the
        // next page number.

        // Add loading footer.
        var loadingPanel = spark.utils.common.loadingFooter();
        loadingPanel.appendTo(ul);
        //spark.utils.common.showLoading();

        window.setTimeout(function () {
            spark.api.client.callAPI(url, null, prefs, function (success, response) {
                //spark.utils.common.hideLoading();

                if (!success) {
                    isLoading = false;
                    spark.utils.common.showError(response);
                    return;
                }


                page.attr("data-pageno", pageNo);

                if (response.data.Members.length < pageSize)
                    isLastPage = true;

                appendToList(response.data.Members);

            });
        }, 2000);
    }

    function callAPIforPutPref() {
        var url = spark.api.client.urls.put_preferences;
        var prefs = {
            Gender: seeking,
            SeekingGender: gender,
            MinAge: minAge,
            MaxAge: maxAge,
            Location: regionID,
            MaxDistance: distance,
            ShowOnlyMembersWithPhotos: photoOnly,
        };

        
        spark.api.client.callAPI(url, null, prefs, function (success, response) {
            if (!success) {
                isLoading = false;
                spark.utils.common.showError(response);
                return;
            }

            callAPIforMatchesData(1);
        });
    }

    function resizeGridImage(ul) {
        var defaultImageWidth = 106;
        var defaultImageHeight = 136;
        var marginWidth = 1;
        var maxThreshold = 10;
        var imageWidth = defaultImageWidth + marginWidth;
        var ratio = 1;
        //var gridWidth = 0;
        //var gridHeight = 0;
        var screenHeight = $(window).height();
        var screenWidth = $(window).width();
        var gridCount = Math.floor((screenWidth / imageWidth));
        var gridTotalWidth = (gridCount * imageWidth)
        var threshold = (screenWidth - gridTotalWidth);
        var padding = 0;

        if (threshold > maxThreshold) {
            gridCount = (gridCount + 1);
            gridWidth = Math.floor((screenWidth / gridCount)) - marginWidth;
            gridTotalWidth = (gridCount * (gridWidth + marginWidth));
            threshold = (screenWidth - gridTotalWidth);

            ratio = Math.round((gridWidth / imageWidth) * 100) / 100;
            gridHeight = Math.round((defaultImageHeight * ratio));
        }

        ul.css({ paddingLeft: 0 });
        if (threshold > 0) {
            padding = Math.round((threshold / 2)) - marginWidth;
            ul.css({
                paddingLeft: padding + "px"
            });
        }

        if (gridHeight !== 0 && gridWidth !== 0) {
            ul.find("li").css({
                height: gridHeight + "px"
            });

            ul.find("li > a > img").css({
                width: gridWidth + "px",
                height: gridHeight + "px"
            });
        }
    };

    function showGrid(ul) {
        if (ul.hasClass("grid-view"))
            return;

        if (ul.find("li").hasClass("loading")) {
            return;
        }

        ul.addClass("grid-view");
        $("#btn_grid_view").addClass("active");
        $("#btn_list_view").removeClass("active");
        spark.utils.common.setCookieForViewToggle("grid");

        resizeGridImage(ul);

        ul.hide();
        ul.find("span.online").text("");
        ul.fadeIn(350);
    };

    function showList(ul) {
        if (!ul.hasClass("grid-view"))
            return;

        $("#btn_list_view").addClass("active");
        $("#btn_grid_view").removeClass("active");

        ul.removeAttr("style");
        ul.find("li").removeAttr("style");
        ul.find("li > a > img").removeAttr("style");

        ul.hide();
        ul.removeClass("grid-view");
        ul.find("span.online").text("Online");
        spark.utils.common.setCookieForViewToggle("list");
        ul.fadeIn(350);
    }

    self.getMatches = function () {
        var cachedData = $(document).data("cachedData");

        if (cachedData != null) {
            if (cachedData.cachedPage == "matches") {
                return;
            }
        }

        if (showPreferencePage) {
            return;
        }

        if (pageInit) {
            return;
        }

        if (isLoading || isLastPage)
            return;

        var pageNo = Number($("#matches_page").attr("data-pageno"));
        callAPIforMatchesData(++pageNo);
    };

    self.init = function () {
        initializeListToggle();
        setupButtonClickEvent();
        getPreference();

        var cachedData = $(document).data("cachedData");

        if (cachedData != null) {
            if (cachedData.cachedPage == "matches") {
                self.resumePageStatus();
            }
        }
        else {
            if ($.cookie(prefCookieID) != null) {
                callAPIforMatchesData(1);
            }
        }
    };

    self.resizeGrid = function (ul) {
        resizeGridImage(ul);
    };

    self.storeMembers = function () {
        var members = [];
        $("#matches_list").children("li").each(function () {
            var li = $(this);
            members.push({
                memberId: li.attr("data-memberid"),
                thumbnail: li.find("a img").attr("src"),
                userName: li.find(".primary").text(),
                match: li.find(".matches-label").text()
        });
        });

        // Cache data so that it can be retrieve from the Profile page.
        $(document).data("members", members);

        var isGrid = false;
        var ul = $("#matches_list");
        if (ul.hasClass("grid-view")) {
            isGrid = true;
        }

        $(document).data("cachedData", {
            cachedPage: "matches",
            isGrid: isGrid,
            pageNumber: $("#matches_page").attr("data-pageno"),
            lastPage: isLastPage,
            locationID: regionID,
            offsetY: $(document).scrollTop(),
            membersData: memberList,
        });
    }

    self.cleanCache = function () {
        $(document).removeData("members");
        $(document).removeData("cachedData");
    }

    self.resumePageStatus = function () {
        var cachedData = $(document).data("cachedData");

        $("#matches_page").attr("data-pageno", cachedData.pageNumber);
        isLastPage = cachedData.lastPage;
        isLoading = true;
        regionID = cachedData.locationID;
        memberList = [];

        appendToList(cachedData.membersData);
        $(document).scrollTop(cachedData.offsetY);

        //if (cachedData.isGrid) {
        //    var ul = $("#matches_list");
        //    showGrid(ul);
        //}

        setTimeout(function () {
            self.cleanCache();
        }, 500);
    }

    self.reset = function () {
        pageInit = true;
    }

    return self;
})();

$(document).on("pageshow", "#matches_page", function () {
    spark.views.matches.init();

    $(window).off("orientationchange").on("orientationchange", function (event) {
        var ul = $("#matches_list");

        if (ul && ul.hasClass("grid-view")) {
            spark.views.matches.resizeGrid(ul);
  
        }
    });
});

$(document).on("pagebeforehide", "#matches_page", function (e, ui) {
    //check if the next page is the profile page
    if (ui.nextPage.attr('id') == 'profile_page') {
        spark.views.matches.storeMembers();       
    }
    else {
        spark.views.matches.cleanCache();
    }  
});

$(document).on("pagehide", "#matches_page", function () {
    spark.views.matches.reset();
});

spark.views.message = (function () {
    var self = {};

    function bindEvents() {
        var backButton = $("button.back-button");
        var replyButton = $("#btn_send");
        var deleteButton = $("span.trash");

        backButton.off().click(function () {
            window.history.back();
        });

        replyButton.off().click(function () {
            var recipientId = $("#recipient").val();
            var subject = "RE: " + $("#subject").val();
            var body = $("#message_body").val();
            var postData = {
                RecipientMemberId: recipientId,
                Subject: subject,
                Body: body,
                MailType: "Email"
            };
            var url = spark.api.client.urls.sendMessage;

            if (body.length === 0) {
                spark.utils.common.showGenericDialog("Message body is required. Please type a message to send.");
                return;
            }

            spark.api.client.callAPI(url, null, postData, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                $("#message_body").val("");
                spark.utils.common.showMessageBar("Message sent successfully", true);
            });
        });

        deleteButton.off().click(function () {
            deleteMessage();
        });
    };

    function deleteMessage() {
        spark.utils.common.showConfirm("Are you sure you want to delete this message?", function () {
            var params = {
                currentFolderId: $("#folder_id").val(),
                messageId: $("#message_id").val()
            };

            spark.api.client.callAPI(spark.api.client.urls.deleteMessage, params, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                window.history.back();
                spark.utils.common.showMessageBar("Message has been deleted", true);
            });
        });
    };

    function reply() {

    };

    self.init = function () {
        bindEvents();
    };

    return self;
})();

$(document).on("pageshow", "#mail_message_page", function () {
    spark.views.message.init();
});

spark.views.mol = (function() {
    "use strict";
    var self = {};
    var pageInit = true;
    var isLoading = false;
    var isLastPage = false;
    var gridWidth = 0;
    var gridHeight = 0;
    var showPreferencePage = false;
    var molCookieID = "mos_mol_pref_" + spark.utils.common.getMemberId();

    // Define Language ID
    var anylangID = -2147483647;
    var ArabicID = 2048;
    var BengaliID = 4096;
    var BulgarianID = 8192;
    var ChineseID = 16384;
    var CzechID = 32768;
    var DutchID = 1024;
    var EnglishID = 2;
    var FinnishID = 65536;
    var FrenchID = 8;
    var GermanID = 32;
    var HebrewID = 262144;
    var HindiID = 524288;
    var ItalianID = 128;
    var JapaneseID = 1048576;
    var KoreanID = 2097152;
    var MalayID = 4194304;
    var NorwegianID = 8388608;
    var PersianID = 512;
    var PolishID = 16777216;
    var PortugueseID = 33554432;
    var RomanianID = 67108864;
    var RussianID = 64;
    var SpanishID = 16;
    var SwedishID = 134217728;
    var TagalogID = 268435456;
    var ThaiID = 536870912;
    var UrduID = 1073741824;
    var VietnameseID = 4;
    var YiddishID = 256;
    var OtherID = 1;

    // Define Region ID
    var anyRegion = -2147483647;
    //var myRegionID = ;
    var canadaID = 38;
    var israelID = 105;
    var UKID = 222;
    var USID = 223;

    var gender = "Male";
    var language = anylangID;
    var region = USID;
    var minAge = 18;
    var maxAge = 29;
    var sortType = 2;

    function appendToList(members) {
        var ul = $("ul.scrollable");

        // Remove loading footer.
        ul.find("li.loading").remove();

        $.each(members, function(index, item) {
            if (item.isOnline) {
                var li = $("<li data-memberid=\"" + item.memberId + "\" />");
                var a = $("<a />");
                var img = $("<img />");
                var p = $("<p />");
                var userName = $("<span class=\"primary\" />");
                var ageLocation = $("<span class=\"secondary\" />");
                var online = $("<span class=\"online\" />");
                var imageUrl = (item.gender === "Male") ? "/images/hd/img_silhouette_man@2x.png" : "/images/hd/img_silhouette_woman@2x.png";

                a.attr("href", "/profile/" + item.memberId);
                if (item.primaryPhoto && item.primaryPhoto.thumbPath !== "") {
                    imageUrl = item.primaryPhoto.thumbPath;
                }

                img.attr("src", imageUrl).appendTo(a);
                userName.text(item.username).appendTo(p);

                var ageText = "";
                var locText = "";

                if (item.age != null) {
                    ageText = item.age;
                }

                if (item.location != null) {
                    locText = item.location;
                    ageLocation.text(ageText + ", " + locText).appendTo(p);
                } else {
                    ageLocation.text(ageText).appendTo(p);
                }
                if (item.isOnline) {
                    if (!ul.hasClass("grid-view")) {
                        online.text("Online");
                    } else {
                        // If in grid mode, apply neccessary in-style to override the CSS class.
                        li.css({
                            height: gridHeight + "px"
                        });

                        img.css({
                            width: gridWidth + "px",
                            height: gridHeight + "px"
                        });
                    }

                    online.appendTo(p);
                }

                p.appendTo(a);
                a.appendTo(li);
                li.appendTo(ul);
            }
        });

        if (pageInit && spark.utils.common.loadViewToggleStatus() === "grid") {
            showGrid(ul);
        }

        isLoading = false;
        pageInit = false;
    }


    function getPreference()
    {
        var prefs = {};

        if ($.cookie(molCookieID) != null) {
            var cookie = $.cookie(molCookieID);
            var tempArr = cookie.split("&");    

            $.each(tempArr, function (index, item) {
                var length = item.indexOf("=");
                var key = item.substring(0, length);
                var value = item.substring(length + 1);

                prefs[key] = value;
            });
        }
        else {
            prefs["SeekingGender"] = "";
            prefs["MinAge"] = 25;
            prefs["MaxAge"] = 40;
            prefs["RegionId"] = 223;
            prefs["LanguageId"] = -2147483647;
            //prefs["SortType"] = 1;
        }        

        return prefs;
    }

    function setupCookie() {
        $.cookie(molCookieID, "SeekingGender=" + gender + "&MinAge=" + minAge + "&MaxAge=" + maxAge + "&RegionId=" + region + "&LanguageId=" + language);
    }

    self.getMembersOnline = function () {
        if (pageInit) {
            return;
        }

        if (isLoading || isLastPage)
            return;

        var pageNo = Number($("#membersonline_page").attr("data-pageno"));

        loadMemberList(++pageNo);
    };

    function loadMemberList(pageNo) {
        isLoading = true;

        var ul = $("ul.scrollable");
        var page = $("#membersonline_page");
        var url = spark.api.client.urls.membersOnline;
        var prefs = getPreference();
        var pageSize = page.attr("data-pagesize");        

        prefs.PageSize = pageSize;
        prefs.PageNumber = pageNo;
        prefs.SortType = sortType;

        if (prefs.SeekingGender === "")
            delete prefs.SeekingGender; // Remove data empty parameter(s). Will cause API to throw exception.

        // Persist current page number. This wil be used as reference to the
        // next page number.

        // Add loading footer.
        var loadingPanel = spark.utils.common.loadingFooter();
        loadingPanel.appendTo(ul);

        window.setTimeout(function () {
            spark.api.client.callAPI(url, null, prefs, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                page.attr("data-pageno", prefs.PageNumber);

                if (response.data.Members.length < pageSize) {
                    isLastPage = true;
                }

                appendToList(response.data.Members);
            });
        }, 2000);
    }

    function initializeListToggle() {
        var ul = $("#mol_list");
        var toggle = $(".list-grid-toggle");
        var listButton = toggle.find("a.list");
        var gridButton = toggle.find("a.grid");
        //var gridClass = "grid-view";

        if (spark.utils.common.loadViewToggleStatus() === "grid") {
            $("#btn_grid_view").addClass("active");
            $("#btn_list_view").removeClass("active");
        }

        listButton.off().on("click", function (e) {
            showList(ul);
        });

        gridButton.off().on("click", function (e) {
            showGrid(ul);
        });
    };

    function resizeGridImage(ul) {
        var defaultImageWidth = 106;
        var defaultImageHeight = 136;
        var marginWidth = 1;
        var maxThreshold = 10;
        var imageWidth = defaultImageWidth + marginWidth;
        var ratio = 1;
        //var gridWidth = 0;
        //var gridHeight = 0;
        var screenHeight = $(window).height();
        var screenWidth = $(window).width();
        var gridCount = Math.floor((screenWidth / imageWidth));
        var gridTotalWidth = (gridCount * imageWidth)
        var threshold = (screenWidth - gridTotalWidth);
        var padding = 0;

        if (threshold > maxThreshold) {
            gridCount = (gridCount + 1);
            gridWidth = Math.floor((screenWidth / gridCount)) - marginWidth;
            gridTotalWidth = (gridCount * (gridWidth + marginWidth));
            threshold = (screenWidth - gridTotalWidth);

            ratio = Math.round((gridWidth / imageWidth) * 100) / 100;
            gridHeight = Math.round((defaultImageHeight * ratio));
        }

        ul.css({ paddingLeft: 0 });
        if (threshold > 0) {
            padding = Math.round((threshold / 2)) - marginWidth;
            ul.css({
                paddingLeft: padding + "px"
            });
        }

        if (gridHeight !== 0 && gridWidth !== 0) {
            ul.find("li").css({
                width: gridWidth + "px",
                height: gridHeight + "px"
            });

            ul.find("li > a > img").css({
                width: gridWidth + "px",
                height: gridHeight + "px"
            });
        }
    };

    function showGrid(ul) {
        if (ul.hasClass("grid-view"))
            return;

        if (ul.find("li").hasClass("loading")) {
            return;
        }

        ul.addClass("grid-view");
        $("#btn_grid_view").addClass("active");
        $("#btn_list_view").removeClass("active");
        spark.utils.common.setCookieForViewToggle("grid");

        resizeGridImage(ul);

        ul.hide();
        ul.find("span.online").text("");
        ul.fadeIn(350);
    };

    function showList(ul) {
        if (!ul.hasClass("grid-view"))
            return;

        $("#btn_list_view").addClass("active");
        $("#btn_grid_view").removeClass("active");

        ul.find("li").removeAttr("style");
        ul.find("li > a > img").removeAttr("style");

        ul.hide();
        ul.removeClass("grid-view");
        ul.find("span.online").text("Online");
        spark.utils.common.setCookieForViewToggle("list");
        ul.fadeIn(350);
    }

    function setupButtonClickEvent() {
        var genderToggle = $("#gender");
        var ddMinAge = $("#min_age");
        var ddMaxAge = $("#max_age");
        var ddRegion = $("#region");
        var ddLanguage = $("#language");

        genderToggle.off("click").on("click", function (e) {
            var self = $(this);
            var target = $(e.target);

            target.addClass("selected");
            if (target.hasClass("female")) {
                self.find("a.male").removeClass("selected");
                self.find("a.all").removeClass("selected");
                gender = "Female";
            }

            if (target.hasClass("male")) {
                self.find("a.female").removeClass("selected");
                self.find("a.all").removeClass("selected");
                gender = "Male";
            }

            if (target.hasClass("all")) {
                self.find("a.male").removeClass("selected");
                self.find("a.female").removeClass("selected");
                gender = "";
            }
        });

        ddMinAge.find("select").on("change", function () {
            minAge = this.value;
        });

        ddMaxAge.find("select").on("change", function () {
            maxAge = this.value;
        });

        ddRegion.find("select").on("change", function () {
            region = this.value;
        });

        ddLanguage.find("select").on("change", function () {
            language = this.value;
        });

        $("#mol-edit-btn").click(function () {
            showPreferencePage = true;
            $(".mol-main-page").addClass("hidden");
            $(".mol-preferences-page").removeClass("hidden");
            $(".mol-preferences-page").fadeIn(350);
            initializePref()
        });

        $("#save-btn").click(function () {
            if (minAge > maxAge || minAge == "" || maxAge == "") {
                spark.utils.common.showGenericDialog("Please select the right age range!");
                return;
            }

            setupCookie();

            $(".mol-preferences-page").fadeOut(350, function () {
                $("#mol_list").find("li").remove();
                $("#mol_list").find("p").remove();

                showPreferencePage = false;
                isLastPage = false;
                isLoading = true;

                $(".mol-main-page").removeClass("hidden");
                $(".mol-preferences-page").addClass("hidden");

                loadMemberList(1);
            });
        });

        $("#cancel-btn").click(function () {
            $(".mol-preferences-page").fadeOut(350, function () {
                showPreferencePage = false;
                $(".mol-main-page").removeClass("hidden");
                $(".mol-preferences-page").addClass("hidden");
            });
        });

        $("#mol_sort_filter").find("select").on("change", function () {
            $("#mol_list").find("li").remove();
            $("#mol_list").find("p").remove();
            isLastPage = false;
            sortType = parseInt(this.value);
            loadMemberList(1);
        });
    }

    function initializePref() {
        var genderToggle = $("#gender");
        var ddMinAge = $("#min_age");
        var ddMaxAge = $("#max_age");
        var ddRegion = $("#region");
        var ddLanguage = $("#language");
        var prefs = getPreference();

        gender = prefs.SeekingGender;
        minAge = prefs.MinAge;
        maxAge = prefs.MaxAge;
        region = prefs.RegionId;
        language = prefs.LanguageId;

        if (gender === "") {
            genderToggle.find(".all").addClass("selected");
            genderToggle.find(".male").removeClass("selected");
            genderToggle.find(".female").removeClass("selected");
        }
        else if (gender.toLowerCase() == "female") {
            genderToggle.find(".female").addClass("selected");
            genderToggle.find(".male").removeClass("selected");
            genderToggle.find(".all").removeClass("selected");
        }
        else if (gender.toLowerCase() == "male") {
            genderToggle.find(".male").addClass("selected");
            genderToggle.find(".female").removeClass("selected");
            genderToggle.find(".all").removeClass("selected");
        }

        ddMinAge.find("select").val(minAge);
        ddMaxAge.find("select").val(maxAge);
        ddRegion.find("select").val(region);
        ddLanguage.find("select").val(language);
    }

    self.init = function () {
        loadMemberList(1);
        initializeListToggle();
        setupButtonClickEvent();
    };

    self.resizeGrid = function (ul) {
        resizeGridImage(ul);
    };

    self.updateCount = function () {
        spark.api.client.callAPI(spark.api.client.urls.members_online_count, null, null,
            function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                var id = $.mobile.activePage.attr("id");
                var data = response.data;
                var btn = $("#btn_mol_" + id);
                
                if (btn.length === 0)
                    return;

                btn.text(data.count + " Online");

                $("#molnumber").text(data.count);
            });
    };

    self.storeMembers = function () {
        var members = [];
        $("#mol_list").children("li").each(function () {
            var li = $(this);
            members.push({
                memberId: li.attr("data-memberid"),
                thumbnail: li.find("a img").attr("src"),
                userName: li.find("a span.primary").text()
            });
        });

        // Cache data so that it can be retrieve from the Profile page.
        $(document).data("members", members);
    }

    self.cleanCache = function () {
        $(document).removeData("members");
        $(document).removeData("cachedData");
    }

    self.reset = function () {
        pageInit = true;
    }

    return self;
})();


$(document).on("pagebeforehide", "#membersonline_page", function (e, ui) {
    //check if the next page is the profile page
    if (ui.nextPage.attr('id') == 'profile_page') {
        spark.views.mol.storeMembers();
    }
    else {
        spark.views.mol.cleanCache();
    }    
});

$(document).on("pageshow", "#membersonline_page", function (e, ui) {
    $(window).off("orientationchange").on("orientationchange", function (event) {
        var ul = $("#mol_list");

        if (ul && ul.hasClass("grid-view")) {
            spark.views.mol.resizeGrid(ul);
        }
    });


    spark.views.mol.init();
    spark.views.mol.storeMembers();
});

$(document).on("pagehide", "#membersonline_page", function () {
    spark.views.mol.reset();
});


spark.views.baseprofile = function () {
    var base = {};

    function addTabEvents() {
        var tabs = $(".profile-tabs");
        tabs.children("li").each(function () {
            var li = $(this);

            li.find("a").click(function (e) {
                var a = $(this);

                changeTab(a.attr("href"));
                e.preventDefault();
            });
        });
    }

    function changeTab(section) {
        var tabContents = $("[data-role='tabcontent']");
        var tabs = $(".profile-tabs");
        var selectedIndex = 0;

        tabs.children("li").each(function (index) {
            var li = $(this);
            var a = li.find("a");

            a.removeClass("active")
                .removeClass("border-left")
                .removeClass("border-right");
            if (a.attr("href") === section) {
                a.addClass("active");
                selectedIndex = index;
            }

            var parent = li.parent();
            switch (selectedIndex) {
                case 0:
                    parent.find("li:eq(1) a").addClass("border-left");
                    parent.find("li:eq(2) a").addClass("border-left");
                    break;
                case 1:
                    parent.find("li:eq(0) a").addClass("border-right");
                    parent.find("li:eq(2) a").addClass("border-left");
                    break;
                default:
                    parent.find("li:eq(0) a").addClass("border-right");
                    parent.find("li:eq(1) a").removeClass("border-left").addClass("border-right");
                    break;
            }
        });

        tabContents.each(function () {
            var tab = $(this);
            if (tab.attr("id") !== section.substring(1)) {
                tab.hide();
            } else {
                tab.show();
            }

        });
    }

    // Public method(s).
    base.addTabEvents = addTabEvents;
    base.getPhotos = getPhotos;
    base.showProfile = showProfile;
    base.italicize = italicize;
    return base;
};

spark.views.profile = (function () {
    var self = {};

    self.init = function () {
        var memberId = $(document).data("memberId");
        if (!memberId) {
            var url = $("#profile_page").attr("data-url");
            memberId = url.replace("/profile/", "");
        }
        if (memberId) {
            spark.views.profile.showProfile(memberId[0].memberId);
            spark.views.profile.italicize();
        }

        spark.views.profile.addTabEvents();

        $("a[href='#in_my_own_words']").click();

    };

    self = $.extend(new spark.views.baseprofile(), self);

    return self;
})();

var activeMemberId = 0;
function getPhotos(memberId) {
    spark.api.client.callAPI(spark.api.client.urls.photos, { targetMemberId: memberId }, null, function (success, response) {
        if (!success) {
            spark.utils.common.showError(response);
            return;
        }

        var photos = response.data;
        if (photos < 0) {

        } else {
            var photoList = [];
            for (var i = 0; i < photos.length; i++) {
                photoList.push({
                    imagePath: photos[i].fullPath,
                    caption: photos[i].caption,
                    thumbnail: photos[i].thumbPath
                });
            }

            var photoViewer = $("#photo_viewer");
            photoViewer.photoviewer("setBeforeHideListener", function () {
                $("div[data-role='content']").show();
            });

            photoViewer.photoviewer("setBeforeShowListener", function () {
                $("div[data-role='content']").hide();
            });

            photoViewer.photoviewer("show", photoList, 0);
        }
    });

}


function spaces(myList) {


    var mySplitedList = myList.toString();

    var newList = mySplitedList.replace(/,/g , ", ");

    return newList;

}

function getFullProfile(memberId) {
    spark.api.client.callAPI(spark.api.client.urls.fullProfile, { targetMemberId: memberId }, null, function (success, response) {
        var fullProfile = [];
        if (!success) {
            spark.utils.common.showError(response);
            return;
        }

        fullProfile.push({ fullProfile: response.data });
        $(document).data("fullProfile", fullProfile);

        if (fullProfile[0].fullProfile.isViewersFavorite == false) {
            $('.slide[member-id='+memberId+'] .favorite a').removeClass("selected").text("Favorite");
        } else {
            $('.slide[member-id='+memberId+'] .favorite a').addClass("selected").text("Unfavorite");
        }

        var p = fullProfile[0].fullProfile;
        for (var property in p) {
            if (p.hasOwnProperty(property)) {
                
                if (p[property] === null || p[property] < Array.length || p[property] === "") {
                    p[property] = "Not answered";
                }
            }
        }

        if (p.primaryPhoto === "Not answered" && p.gender === "Male") {
            profileImg = "/images/hd/img_silhouette_man@2x.png";
        } else if (p.primaryPhoto === "Not answered" && p.gender === "Female") {
            profileImg = "/images/hd/img_silhouette_woman@2x.png";
        } else {
            var profileImg = p.primaryPhoto.fullPath;
        }

        //Profile Details
        if (p.gender === "Female") {
            var gender = "woman";
        } else if (p.gender === "Male") {
            var gender = "man";
        }
        var status = p.maritalStatus;
        var seeking = p.seekingGender;
        var age = p.age;
        var location = p.location;
        var isOnline = p.isOnline;
        var lastLoggedIn = p.lastLoggedIn;
        var photoCount = p.photoCount;



        //In My Own Words
        var aboutMe = p.aboutMe;
        var lookingFor = spaces(p.lookingFor);
        var perfectFirstDateEssay = p.perfectFirstDateEssay;
        var idealRelationshipEssay = p.idealRelationshipEssay;
        var learnFromThePastEssay = p.learnFromThePastEssay;
        var personalityTraits = spaces(p.personalityTraits);
        var likeGoingOutTo = spaces(p.likeGoingOutTo);
        var inMyFreeTimeILikeTo = spaces(p.inMyFreeTimeILikeTo);
        var physicalActivity = spaces(p.physicalActivity);
        var pets = spaces(p.pets);
        var favoriteMusic = spaces(p.favoriteMusic);
        var favoriteFoods = spaces(p.favoriteFoods);
        var likeToRead = spaces(p.likeToRead);

        //My Details
        var myHeightIn = p.heightCm * .393701;
        var myHeightFt = parseInt(myHeightIn / 12);
        var myHeightIns = parseInt(myHeightIn - (myHeightFt * 12));
        var myHeight = myHeightFt + "\'" + myHeightIns + '\"' + "(" + p.heightCm + " cm)";
        var myWeight = parseInt(p.weightGrams * .0022) + " lbs (" + parseInt(p.weightGrams * .001) + " kg)";
        var hair = p.hairColor;
        var eyes = p.eyeColor;
        var bodyStyle = p.bodyType;
        var relocate = p.relocation;
        var myChildren = p.children;
        var custody = p.custody;
        var wantKids = p.planOnChildren;
        var kosher = p.keepKosher;
        var synagogue = p.synagogueAttendance;
        var drink = p.drinkingHabits;
        var smoke = p.smokingHabits;
        var zodiac = p.zodiac;
        var activityLevel = p.activityLevel;
        var grewUpIn = p.grewUpIn;
        var ethnicity = p.ethnicity;
        var iSpeak = p.languagesSpoken;
        var studied = p.studiedOrInterestedIn;
        var education = p.educationLevel;
        var occupation = p.occupation;
        var occupationDesc = p.occupationDescription;
        var income = p.incomeLevel;
        var politics = p.politicalOrientation;
        var religion = p.religion;

        //My Ideal Match
        var lookFor = spaces(p.lookingFor);
        if (isNaN(p.matchMinAge) || isNaN(p.matchMaxAge)) {
            var ageRange =  p.matchMinAge ;
        } else
        {
            var ageRange = p.matchMinAge + " to " + p.matchMaxAge;
        }
        var relationshipStatus = p.matchMaritalStatus;
        var religiousBackground = spaces(p.religion);
        var edLevel = spaces(p.matchEducationLevel);
        var drinkingHabits = spaces(p.matchDrinkingHabits);
        var smokingHabits = spaces(p.matchSmokingHabits);

        //Profile Details
        $('.slide[member-id=' + memberId + '] .photos').attr("member-id", memberId);
        $('.slide[member-id=' + memberId + '] .photos img').attr("src", profileImg);
        //shorten username to 10
        
        $(".slide[member-id="+memberId+"] .photos .photo-count b").text(photoCount);
        if (isNaN(photoCount)) {
            $('.slide[member-id='+memberId+'] .photos .photo-count b').text("0");
        }

        $('.slide[member-id=' + memberId + '] .status').empty().text(status + " " + gender);
        if (status === "Not answered") {
            $('.slide[member-id=' + memberId + '] .status').empty().text(gender.substr(0, 1).toUpperCase() + gender.substr(1));
        }
        if (seeking === "Female") {
            seeking = "women";
            $('.slide[member-id=' + memberId + '] .seeking').empty().text("Seeking " + seeking);
        } else if (seeking === "Male") {
            seeking = "men";    
            $('.slide[member-id=' + memberId + '] .seeking').empty().text("Seeking " + seeking);
        }
        $('.slide[member-id=' + memberId + '] .age').empty().text(age + " years old");
        $('.slide[member-id=' + memberId + '] .location').empty().text(location);

     
        var date = new Date(lastLoggedIn);
        var month = date.getUTCMonth() + 1;
        var day = date.getUTCDate();
        var year = date.getUTCFullYear();
        var newdate = month + "/" + day + "/" + year;

        $(".slide[member-id="+memberId+"] .onlineStatus a.btn-mol").show();
        $(".slide[member-id="+memberId+"] .onlineStatus .last-log").remove();
        $(".slide[member-id="+memberId+"] a.chat-btn").attr("href", chatUrl + memberId);
       
        if (isOnline === "Not answered") {
            $(".slide[member-id="+memberId+"] .onlineStatus a.btn-mol").hide();
            $(".slide[member-id="+memberId+"] .onlineStatus").append("<p class='last-log'>Last Login:<br/> " + newdate + "</p>");
        }

        //In My Own Words
        $('.slide[member-id='+memberId+'] .aboutMe').empty().text(aboutMe);
        $('.slide[member-id=' + memberId + '] .lookingFor').empty().text(lookingFor);
        $('.slide[member-id=' + memberId + '] .perfectFirstDate').empty().text(perfectFirstDateEssay);
        $('.slide[member-id=' + memberId + '] .idealRelationship').empty().text(idealRelationshipEssay);
        $('.slide[member-id=' + memberId + '] .pastRelationships').empty().text(learnFromThePastEssay);
        $('.slide[member-id=' + memberId + '] .personalityTraits').empty().text(personalityTraits);
        $('.slide[member-id=' + memberId + '] .likeGoingOutTo').empty().text(likeGoingOutTo);
        $('.slide[member-id=' + memberId + '] .inMyFreeTime').empty().text(inMyFreeTimeILikeTo);
        $('.slide[member-id=' + memberId + '] .location').empty().text(location);
        $('.slide[member-id=' + memberId + '] .physicalActivity').empty().text(physicalActivity);
        $('.slide[member-id=' + memberId + '] .pets').empty().text(pets);
        $('.slide[member-id=' + memberId + '] .favMusic').empty().text(favoriteMusic);
        $('.slide[member-id=' + memberId + '] .favFood').empty().text(favoriteFoods);
        $('.slide[member-id=' + memberId + '] .likeToRead').empty().text(likeToRead);

        //My Details
        $('.slide[member-id=' + memberId + '] .myHeight').empty().text(myHeight);
        if (isNaN(p.heightCm)) {
            $('.slide[member-id=' + memberId + '] .myHeight').empty().text("Not answered");
        }
        $('.slide[member-id=' + memberId + '] .myWeight').empty().text(myWeight);
        if (isNaN(p.weightGrams)) {
            $('.slide[member-id=' + memberId + '] .myWeight').empty().text("Not answered");
        }
        $('.slide[member-id=' + memberId + '] .hair').empty().text(hair);
        $('.slide[member-id=' + memberId + '] .eyes').empty().text(eyes);
        $('.slide[member-id='+memberId+'] .bodyStyle').empty().text(bodyStyle);
        $('.slide[member-id='+memberId+'] .relocate').empty().text(relocate);
        $('.slide[member-id='+memberId+'] .myChildren').empty().text(myChildren);
        $('.slide[member-id='+memberId+'] .custody').empty().text(custody);
        $('.slide[member-id='+memberId+'] .wantKids').empty().text(wantKids);
        $('.slide[member-id='+memberId+'] .kosher').text(kosher);
        $('.slide[member-id='+memberId+'] .synagogue').empty().text(synagogue);
        $('.slide[member-id='+memberId+'] .drink').empty().text(drink);
        $('.slide[member-id='+memberId+'] .zodiac').empty().text(zodiac);
        $('.slide[member-id='+memberId+'] .activityLevel').empty().text(activityLevel);
        $('.slide[member-id='+memberId+'] .grewUpIn').empty().text(grewUpIn);
        $('.slide[member-id='+memberId+'] .ethnicity').empty().text(ethnicity);
        $('.slide[member-id='+memberId+'] .iSpeak').empty().text(iSpeak);
        $('.slide[member-id='+memberId+'] .studied').empty().text(studied);
        $('.slide[member-id='+memberId+'] .education').empty().text(education);
        $('.slide[member-id='+memberId+'] .occupation').empty().text(occupation);
        $('.slide[member-id='+memberId+'] .occupationDesc').empty().text(occupationDesc);
        $('.slide[member-id='+memberId+'] .income').empty().text(income);
        $('.slide[member-id='+memberId+'] .politics').empty().text(politics);
        $('.slide[member-id='+memberId+'] .religion').text(religion);
        $('.slide[member-id='+memberId+'] .synagogue').empty().text(synagogue);
        $('.slide[member-id='+memberId+'] .drink').empty().text(drink);
        $('.slide[member-id='+memberId+'] .smoke').empty().text(smoke);
        $('.slide[member-id='+memberId+'] .zodiac').empty().text(zodiac);
        $('.slide[member-id='+memberId+'] .activityLevel').empty().text(activityLevel);

        //My Ideal Match
        $('.slide[member-id='+memberId+'] .lookFor ').empty().text(lookFor);
        $('.slide[member-id='+memberId+'] .ageRange').empty().text(ageRange);
        $('.slide[member-id='+memberId+'] .relationshipStatus ').empty().text(relationshipStatus);
        $('.slide[member-id='+memberId+'] .religiousBackground ').empty().text(religiousBackground);
        $('.slide[member-id='+memberId+'] .edLevel ').empty().text(edLevel);
        $('.slide[member-id='+memberId+'] .drinkingHabits ').empty().text(drinkingHabits);
        $('.slide[member-id='+memberId+'] .smokingHabits ').empty().text(smokingHabits);

    });   
}
function parseDate(dateString) {
    var d = new Date(dateString);

    var year = d.getUTCFullYear();
    var month = d.getUTCMonth();
    var date = d.getUTCDate();
    var monthString = convertMonToString(month);

    var returnDate = monthString + " " + date + ", " + year + " ";

    return returnDate;
}

function convertMonToString(month) {
    var monthString;

    if (month == "0") {
        monthString = "Jan";
    }
    else if (month == "1") {
        monthString = "Feb";
    }
    else if (month == "2") {
        monthString = "Mar";
    }
    else if (month == "3") {
        monthString = "Apr";
    }
    else if (month == "4") {
        monthString = "May";
    }
    else if (month == "5") {
        monthString = "Jun";
    }
    else if (month == "6") {
        monthString = "Jul";
    }
    else if (month == "7") {
        monthString = "Aug";
    }
    else if (month == "8") {
        monthString = "Sep";
    }
    else if (month == "9") {
        monthString = "Oct";
    }
    else if (month == "10") {
        monthString = "Nov";
    }
    else if (month == "11") {
        monthString = "Dec";
    }
    else {
        monthString = "Undefined";
    }

    return monthString;
}

// Bind event to Flirt navigation button.
$(document).on('tap', '#profile_page .flirt a', function (e) {
    var recipientMemberId = $('.swiper-slide-active').find(".slide").attr("member-id");
    var userName = $(".page-header h2").text();

    var postData = {
        RecipientMemberId: recipientMemberId,
        TeaseId: 494, // Tease_Casual_Mobile_1
        TeaseCategoryId: 66, // Casual
        Body: "I'm sending you a Flirt to get the conversation started. Contact me and let's get to know each other!"
    };


    spark.api.client.callAPI(spark.api.client.urls.sendTease, {}, postData, function (success, response) {
        var msg = "Flirt sent to " + userName;

        if (!success) {
            var data = response ? response.Result : null;
            msg = "You've already sent a flirt to this user.";
            if (data && data.error) {
                var code = data.error.code;
                switch (code) {
                    case 40019: // Exceeded maximum allowed.
                        msg = "You have exceeded maximum flirt allowed.";
                        break;
                    default:
                        msg = "Your flirt was not able to be sent.";
                }
            }
        } else {

        }
        spark.utils.common.showGenericDialog(msg, null);
    });
});

function getYesNoMaybe(memberId) {
    //Get Secret Admirer Status from API
    spark.api.client.callAPI(spark.api.client.urls.get_YNM, { addMemberId: memberId }, null, function (success, response) {
        var yesNoMaybe = [];
        if (!success) {
            spark.utils.common.showError(response);
            return;
        }

        yesNoMaybe.push({ yesNoMaybe: response.data });
        $(document).data("yesNoMaybe", yesNoMaybe);
        var ynm = $(document).data("yesNoMaybe");

        //Assign img a class based on secret admirer status
        if (ynm[0].yesNoMaybe.Maybe == true) {
            $(".swiper-slide[member-id=" + memberId + "]").find("span#yesno").addClass("maybe");
                      
        } else if (ynm[0].yesNoMaybe.MutualYes == true) {
            $(".swiper-slide[member-id=" + memberId + "]").find("span#yesno").addClass("yesBoth");

        } else if (ynm[0].yesNoMaybe.No == true) {
            $(".swiper-slide[member-id=" + memberId + "]").find("span#yesno").addClass("no");

        } else if (ynm[0].yesNoMaybe.Yes == true) {
            $(".swiper-slide[member-id=" + memberId + "]").find("span#yesno").addClass("yes");

        }
    });

}

// Profile Favoriting functionality
$(document).on('tap', '#profile_page .icon-favorite', function (e) {
    e.preventDefault();
    var icon = $(this);
    var profileId = icon.closest('.slide').attr("member-id");
    icon.toggleClass('selected').removeClass('ui-btn-active');
    if (icon.hasClass('selected')) {
        icon.text('Unfavorite');
        spark.api.client.callAPI(spark.api.client.urls.postFavorite, null, { targetMemberId: profileId }, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

        });
    } else {
        $(this).text('Favorite');
        spark.api.client.callAPI(spark.api.client.urls.deleteFavorite, { favoriteMemberId: profileId }, null, function(success, response) {
          if (!success) {
              spark.utils.common.showError(response);
              return;
          }

        });
    };
});

//photo slideshow overlay feature
$(document).on('tap', '#profile_page .profile-detail-container div.photos', function (e) {
    var memberId = $(this).attr("member-id");
    //gets photos for user from API
    if ($(this).find("span.photo-count b").text() === "0") {
        //do nothing
    } else {
        $("footer").hide();
        getPhotos(memberId);
    }

});

function getSelectedIndex(memberId) {
    var molData = $(document).data("members");
    var length = 0;
    if (molData) {
         length = molData.length;
    }
    for (var i = 0; i < length; i++) {
        if (memberId == molData[i].memberId) {
            return i;
        }
    }

    return 0;
}

function italicize()
{
    $(".profile-content div p:contains('Not answered')").each(function () {
        $(this).css({ "font-style": "italic" });
    });
}

function getUsername(memberId) {
    spark.api.client.callAPI(spark.api.client.urls.fullProfile, { targetMemberId: memberId }, null, function(success, response) {
        var fullProfile = [];
        if (!success) {
            spark.utils.common.showError(response);
            return;
        }

        fullProfile.push({ fullProfile: response.data });
        var username = fullProfile[0].fullProfile.username;
        if (username.length > 10) {
            var shortName = username.slice(0, 10) + "...";
            $('.page-header h2').text(shortName);
        } else {
            $('.page-header h2').text(username);
        }
    });
    }


function appendSwiperSlides() {
    
    var swiper = $(".swiper");
    var wrapper = swiper.find(".swiper-wrapper");
    var list = $(document).data("members");
    if (list && list.length > 0) {
        
        $.each(list, function(index, item) {
            var slider = $("<div class=\"swiper-slide\"  />");
            var newSlide = $("<div class='slide' member-id='" + item.memberId + "'>");
            var slide = $(".first").html();
            newSlide.html(slide);
            if (item.match){
                newSlide.find("h4.match").text(item.match);
            }
            newSlide.appendTo(slider);
            slider.appendTo(wrapper);
        });
        $(".first").remove();
        $(".swiper-slide").each(function() {
            var li = $(this);
            var id = li.find(".slide").attr("member-id");
            getFullProfile(id);
            italicize();
            getYesNoMaybe(id);
        });
        if (list.length === 1) {
            $("#profile_page .prevButton, #profile_page .nextButton").hide();
        }
 
    } else {
        var id = $(document).data("memberId");
        
        if (!id) {
            var url = $("#profile_page").attr("data-url");
            var memberId = url.replace("/profile/", "");
        } else {
             memberId = id[0].memberId;
        }
        

        $(".first").wrapInner("<div class='slide' member-id='" + memberId + "'/>");
        getFullProfile($.trim(memberId));
        getYesNoMaybe(memberId);
        italicize();
        $("#profile_page .prevButton, #profile_page .nextButton").hide();

    }
}


function showProfile(memberId) {

    var swiper = $(".swiper");
    var header = $(".page-header");
    var prevButton = header.find(".prevButton");
    var nextButton = header.find(".nextButton");
    var list = $(document).data("members");
    prevButton.unbind("click").bind("click", function () {
        swiper.swipePrev();
    });

    nextButton.unbind("click").bind("click", function () {
        swiper.swipeNext();
    });

    appendSwiperSlides();
    italicize();
    getUsername(memberId);

    var index = getSelectedIndex(memberId);

        var swiper = $(".swiper").swiper({
            initialSlide: index,
            loop: false,
            mode: "horizontal",
            speed: 600,
            onInit: function(swiper) {
                italicize();
            },
            //swipe right functionality
            onSlideNext: function (swiper) {
                var name = $(".swiper-slide-active").find(".slide").attr("member-id");
                $(".swiper-slide-active a[href='#in_my_own_words']").click();
                index++;
                $('.prevButton').css({ "visibility": "visible" });
                if (index == list.length - 1) {
                    $('.nextButton').css({ "visibility": "hidden" });
                }

                activeMemberId = name;
                getUsername(name);
                italicize();
            },

            //swipe left functionality
            onSlidePrev: function (swiper) {
                var name = $(".swiper-slide-active").find(".slide").attr("member-id");
                $(".swiper-slide-active a[href='#in_my_own_words']").click();
                index--;
                $('.nextButton').css({ "visibility": "visible" });
                //pageName.text(molData[counter].username);
                if (index == 0) {
                    $('.prevButton').css({ "visibility": "hidden" });
                }

                activeMemberId = name;
                getUsername(name);
                italicize();
            }
        });

        $(swiper.container).css({
            clear: "both",
            width: "100%"
        });
    // Since swiper widget is initialized, just re-initialize then go to
    // selected index.
    italicize();
        swiper.reInit();
        swiper.swipeTo(index, 100);
  
        return;
    }

$(document).on('tap', '#profile_page #btn_send', function (e) {
    if (spark.utils.common.isSub()) {
        var memberId = ($(this).closest(".swiper-slide").attr("member-id"));
        var userName = $(".page-header h2").text();

        if (activeMemberId === 0)
            activeMemberId = $("#btn_send").attr("data-memberid");

        showComposeMail(activeMemberId, userName);
    }
    else {
        window.location.href = "/subscription/subscribe/4168";
    }
});

function showComposeMail(memberId, userName) {
    $("div.ui-content").parent().hide();
    spark.utils.common.hideFooter();

    var panel = $("#compose_mail");
    panel.removeClass("hidden").fadeIn(350);
    panel.find("#btn_cancel_mail").off().click(function () {
        panel.hide();

        spark.utils.common.showFooter();
        $("div.ui-content").parent().fadeIn(350);
    });

    panel.find("h4").text("To: " + userName);
    panel.find("#btn_send_mail").off().click(function () {
        var body = $.trim($("#body").val());
        var subject = $.trim($("#subject").val());

        spark.views.mail.sendMessage(memberId, subject, body, function (data) {
            $("#body").val("");
            $("#subject").val("");

            panel.hide();
            spark.utils.common.showFooter();
            $("div.ui-content").parent().fadeIn(350);

            spark.utils.common.showMessageBar("Your message has been sent", true);
        });
    });
}

$(document).on("pageshow", "#profile_page", function () {
    spark.views.profile.init();
    setTimeout(function () {
        googletag.pubads().refresh([gpt_profile_top]);
    }, 500);
    spark.utils.common.hideLoading();
}); //end ready();


spark.views.myprofile = (function () {
    var self = {};
    var editedFields = [];
    var reviewFields = [];

    var attributeOptionsMap = {
        "personality": "personalityTrait",
        "i_enjoy": "leisureActivity",
        "i_like_to_go_to": "EntertainmentLocation",
        "favorite_physical_activities": "physicalActivity",
        "own_these_pets": "pets",
        "favorite_foods": "cuisine",
        "favorite_music": "music",
        "like_to_read": "reading",
        "marital_status": "maritalStatus",
        "hair": "hairColor",
        "eyes": "eyeColor",
        "body_style": "bodyType",
        "looking_for": "relationshipMask",
        "relationship_status": "maritalStatus",
        "religious_background": "religion",
        "education_level": "educationLevel",
        "drinking_habits": "drinkingHabits",
        "smoking_habits": "smokingHabits",
        "relocation": "relocateFlag",
        "children": "childrenCount",
        "custody_situation": "custody",
        //"planning_children": "planOnChildren",
        "keep_kosher": "keepKosher",
        "go_to_synagogue": "synagogueAttendance",
        "i_smoke": "smokingHabits",
        "i_drink": "drinkingHabits",
        "zodiac_sign": "zodiac",
        "activity_level": "activityLevel",
        "ethnicity": "ethnicity",
        "languages": "languageMask",
        "education": "educationLevel",
        "occupation": "industryType",
        "annual_income": "incomeLevel",
        "political_orientation": "politicalOrientation",
        "my_religion": "religion"
    };

    var jsonMapping = {
        "marital_status": "maritalStatus",
        "gender_seeking": "seekingGender",
        "about_me": "aboutMe",
        "perfect_first_date": "perfectFirstDateEssay",
        "ideal_relationship": "idealRelationshipEssay",
        "past_relationships": "learnFromThePastEssay",
        "personality": "personalityTraits",
        "i_enjoy": "inMyFreeTimeILikeTo",
        "i_like_to_go_to": "likeGoingOutTo",
        "favorite_physical_activities": "physicalActivity",
        "own_these_pets": "pets",
        "favorite_foods": "favoriteFoods",
        "favorite_music": "favoriteMusic",
        "like_to_read": "likeToRead",
        "height": "heightCm",
        "weight": "weightGrams",
        "hair": "hairColor",
        "eyes": "eyeColor",
        "body_style": "bodyType",
        "relocation": "relocation",
        "children": "children",
        "custody_situation": "custody",
        "planning_children": "planOnChildren",
        "keep_kosher": "keepKosher",
        "go_to_synagogue": "synagogueAttendance",
        "i_smoke": "smokingHabits",
        "i_drink": "drinkingHabits",
        "activity_level": "activityLevel",
        "zodiac_sign": "zodiac",
        "grew_up_in": "grewUpIn",
        "studied": "studiedOrInterestedIn",
        "ethnicity": "jDateEthnicity",
        "languages": "languagesSpoken",
        "education": "educationLevel",
        "occupation": "occupation",
        "occupation_description": "occupationDescription",
        "annual_income": "incomeLevel",
        "political_orientation": "politicalOrientation",
        "my_religion": "religion",
        "looking_for": "lookingFor",
        "min_age_range": "matchMinAge",
        "max_age_range": "matchMaxAge",
        "relationship_status": "matchMaritalStatus",
        "religious_background": "religion",
        "education_level": "matchEducationLevel",
        "drinking_habits": "matchDrinkingHabits",
        "smoking_habits": "matchSmokingHabits",
        "user_name": "username"
    }

    var pageNameLookup = {
        "basic_info": "m_edit_basic_info",
        "my_details": "m_edit_my_details",
        "my_own_words": "m_edit_my_own_words"
    };
  

    function addEditField(e, hasChanged) {
        var id = e.attr("id");

        if ($.inArray(id, editedFields) === -1 && hasChanged) {
            editedFields.push(id);

            if (e.attr("data-review"))
                reviewFields.push(id);

        } else if (!hasChanged) {
            editedFields = $.grep(editedFields, function (value) {
                return value != id;
            });

            reviewFields = $.grep(reviewFields, function (value) {
                return value != id;
            });
        }
    }

    function getAttributeOptionName(key) {
        if (key in attributeOptionsMap)
            return attributeOptionsMap[key];

        return "";
    }

    function getMapping(key) {
        return jsonMapping[key];
    }

    function getMultiselectValues(key) {
        var o = $("#" + key + "_text");
        var arr = (o.attr("data-value")) ? o.attr("data-value").split(",") : [];
        var values = [];

        $.each(arr, function (index, value) {
            values.push($.trim(value));
        })

        return values;
    }

    function hideLoading() {
        $.mobile.loading("hide");
    }

    function processControls(form) {
        // Proces textarea field(s).
        var textbox = form.find("input[type='text']");
        textbox.keyup(function () {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());

            addEditField($(this), hasChanged);
        });

        textbox.on("paste", function () {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());

            addEditField($(this), hasChanged);
        })

        // Proces textarea field(s).
        var textarea = form.find("textarea");
        textarea.keyup(function () {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());

            addEditField($(this), hasChanged);
        });

        textarea.on("paste", function () {
            var id = $(this).attr("id");
            var hasChanged = ($("#" + id + "_text").attr("data-value") !== $(this).val());

            addEditField($(this), hasChanged);
        })

        // Process multi-select widget(s).
        var widgets = form.find("div[data-type='multiselect']");
        widgets.each(function () {
            var o = $(this);
            var id = o.attr("id");

            var values = getMultiselectValues(id);
            o.multiselect("createOptions", getAttributeOptionName(id), values);
            o.multiselect("setOnChangeListener", function (hasChanged) {
                addEditField(o, hasChanged);
            });
        });

        // Process custom dropdown(s).
        var dropdowns = form.find("div.dd-custom");
        dropdowns.each(function () {
            var o = $(this);
            var id = o.attr("id");
            var select = o.find("select");

            // Add onChange listener to dropdown.
            select.change(function () {
                var self = $(this);

                self.find("option:selected").each(function () {
                    var hasChanged = false;
                    var values = [];

                    //if (id !== "min_age_range" && id !== "max_age_range") {
                        hasChanged = ($.trim($("#" + id + "_text").attr("data-value")) != $.trim($(this).text()));
                    //} else {
                     //   values = $("#age_range_text").attr("data-value").split(",");
                    //}

                    // Make sure that the minimum age is not greater than the maximum.
                    /*if (id === "min_age_range") {
                        var value = select.val() !== "" ? Number(select.val()) : 0;
                        var maxAge = $("#max_age_range").find("select");

                        hasChanged = value !== values[0];
                        if (value > maxAge.val()) {
                            select.val(values[0]);
                            hasChanged = false;

                            spark.utils.common.showGenericDialog("Minimum age cannot be greater than maximum. Please select valid age value.");
                        } 
                    }

                    // Make sure that the maximum age is not less than the minimum.
                    if (id === "max_age_range") {
                        var value = select.val() !== "" ? Number(select.val()) : 0;
                        var minAge = $("#min_age_range").find("select");

                        hasChanged = value !== values[1];
                        if (select.val() < minAge.val()) {
                            select.val(values[1]);
                            hasChanged = false;

                            spark.utils.common.showGenericDialog("Maximun age cannot be less than minimum. Please select valid age value.");
                        }
                    }*/

                    addEditField(o, hasChanged);
                });
            });

            if (select.find("option").length !== 0) {
                setSelected(select);

                return;
            }

            var attrName = getAttributeOptionName(id);
            if (!attrName || attrName.length === 0) {
                setSelected(o.find("select"));

                return;
            }

            // Get options for dropdown.
            spark.api.client.callAPI(spark.api.client.urls.attributeOptions, { attributeName: attrName }, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                $.each(response.data, function (key, value) {
                    var option = $("<option />");

                    option.attr("value", value["Value"]);
                    option.text(value["Description"]);
                    option.appendTo(select);
                });

                setSelected(select);
            });

        });
    }

    function saveProfile() {
        // Check if there are fields to save. If none, just return.
        if (editedFields.length === 0)
            return;

        var attributes = {};
        var attributesMulti = {};
        var errorMessage = "";

        $.each(editedFields, function (index, item) {
            var e = $("#" + item);
            var type = e.attr("data-type");
            var values = [];
            var id = e.attr("id");

            if (type === "multiselect") {
                values = e.multiselect("getValues");
                for (var i = 0; i < values.length; i++) {
                    values[i] = +values[i];
                }

                attributesMulti[getMapping(item)] = values;
            } else if (e.hasClass("dd-custom") && id !== "body_style") {
                var value = Number(e.find("select").val());
                if (id === "gender_seeking")
                {
                    if (value === 9 || value === 10) {
                        value = "Female";
                    } else {
                        value = "Male";
                    }
                }

                if (id === "min_age_range") {
                    var maxAge = $("#max_age_range").find("select");
                    if (value > maxAge.val() && errorMessage === "") {
                        errorMessage = "Minimum age cannot be greater than maximum. Please select valid age value.";
                    }
                } else if (id === "max_age_range") {
                    var minAge = $("#min_age_range").find("select");
                    if (value < minAge.val() && errorMessage === "") {
                        errorMessage = "Maximun age cannot be less than minimum. Please select valid age value.";
                    }
                }
                   
                attributes[getMapping(item)] = value;
            } else if (e.attr("id") === "body_style") {
                values.push(Number(e.find("select").val()));
                attributesMulti[getMapping(item)] = values;
            } else {
                attributes[getMapping(item)] = e.val();
            }
        });

        // Chek if there are any error(s) before saving. If so, show message
        // and abort saving until user fix issue.
        if (errorMessage.length > 0) {
            spark.utils.common.showGenericDialog(errorMessage);

            return;
        }

        var params = {
            AttributesJson: JSON.stringify(attributes),
            AttributesMultiValueJson: JSON.stringify(attributesMulti)
        };

        spark.api.client.callAPI(spark.api.client.urls.updateProfile, null, params, function (success, response) {
            showLoading("Saving...");

            if (!success) {
                hideLoading();

                var error = response.error;
                var message = (error) ? error.message : "We're unable to save your profile.";
                showErrorDialog(message);

                return;
            }
            updateDetail();
            hideLoading();
            showReviewDialog(function () {
                showDetail();
                
            });
        });

       
    }

    //function createAttributeSectionLookup() {
    //    // make a lookup table to find which section (tab) an attribute was in
    //    // depending on the section, omniture will be called once for the whole section, or once per attribute (essays)
    //    if (self.attributeSectionlookup) {
    //        return;
    //    }
    //    var attLookup = {};
    //    var attributeName;
    //    for (var sectionSelector in jsonMapping) {
    //        var section = jsonMapping[sectionSelector];
    //        
    //        for (var attributeSelector in section) {
    //            attributeName = section[attributeSelector];
    //            
    //            attLookup[attributeName] = sectionSelector;
                
    //            if (attributeOptionsMap[attributeName]) {
    //                attributeName = attributeOptionsMap[attributeName];
    //                attLookup[attributeName] = sectionSelector;
    //            }
    //        }
    //    }
    //    self.attributeSectionlookup = attLookup;
    //}
    
   

    function setSelected(target) {
        var id = target.parent().attr("id");
        var temp = id;
        if (id === "min_age_range" || id === "max_age_range") {
            temp = "age_range";
        }

        var o = $("#" + temp + "_text");
        var value = o.attr("data-value");
        if (!value || $.trim(value.length) === 0)
            return;

        if (id === "min_age_range")
            value = value.split(",")[0];
        if (id === "max_age_range")
            value = value.split(",")[1];
        if (id === "gender_seeking")
        {
            var temp = value.split(",");
            var gender = temp[0].toLowerCase();

            if (gender === "male") {
                value = 9;

                if (temp[1].toLowerCase() === "male")
                    value = 5;
            } else if (gender === "female") {
                value = 6;

                if (temp[1].toLowerCase() === "female")
                    value = 10;
            }
        }

        var option = target.find("option").filter(function () {
            var found = ($(this).text() == "" + value);
            if (!found) {
                found = ($(this).val() == value);
            }

            return found;
        });

        if (option && option.length !== 0)
            option.prop("selected", true);
    }

    function showDetail() {
        var target = $("div.readonly");
        var temp = $("div.editable");

        $("div.ui-body-c").removeClass("profile-edit-bg");

        temp.hide();
        target.fadeIn();
    }

    function showEdit(name, callback) {
        var form = showForm(name);
        var target = $("div.editable");
        var temp = $("div.readonly");

        spark.utils.common.hideFooter();
        temp.hide();
        target.fadeIn();
    }

    function showForm(name) {
        $("div.ui-body-c").addClass("profile-edit-bg");

        var form;
        $(".profile-edit-form, .profile-manage-photos").each(function () {
            var temp = $(this);

            if (temp.attr("id") !== name) {
                temp.addClass("hidden");
            } else {
                if (name !== "manage_photos")
                    processControls(temp);

                temp.removeClass("hidden");
                form = temp;
            }
        });

        return form;
    }

    function showErrorDialog(message) {
        spark.utils.common.showGenericDialog(message);
    }

    function showLoading(msg, options) {
        var defaultOptions = {
            text: msg,
            textVisible: true
        };

        $.extend(defaultOptions, options);
        $.mobile.loading("show", defaultOptions);
    }

    function showReviewDialog(callback)
    {
        if (reviewFields.length !== 0) {
            reviewFields = [];
            spark.utils.common.showGenericDialog("Your new essay is being approved by Customer Care and will be posted shortly.", callback);
        } else {
            callback();
        }
    }

    function updateDetail() {
        $.each(editedFields, function (index, item) {
            var e = $("#" + item);
            var o = $("#" + item + "_text");
            var type = e.attr("data-type");

            if (type === "multiselect") {
                var values = e.multiselect("getTextValues");

                e.multiselect("setValues", values);
                o.attr("data-value", values.join(","));
                o.text(values.join(", "));
            } else if (item === "min_age_range" || item === "max_age_range") { 
                var temp = [];
                temp.push($("#min_age_range").find("select").val());
                temp.push($("#max_age_range").find("select").val());

                o = $("#age_range_text");
                o.attr("data-value", temp.join(","));
                o.text(temp[0] + " to " + temp[1]);
            } else if (e.hasClass("dd-custom")) {
                o.attr("data-value", e.find("select").val());

                o.text(e.find("select option:selected").text());
                if (o.attr("id") === "gender_seeking_text") {
                    o.text(e.find("select option:selected").text().replace("Woman ", "").replace("Man ", ""));
                } 
            } else {
                o.text(e.val());
            }
        });
    }

    self.init = function () {
        spark.views.myprofile.addTabEvents();

        $("a[href='#in_my_own_words']").click();

        $(".btn-edit").on("click", function (e) {
            var name = $(this).parent().attr("data-form");
            var buttonContainer = $("#profile_edit_buttons");

            buttonContainer.show();
            showEdit(name);
        });

        $("#btn_edit_cancel").on("click", showDetail);
        $("#btn_edit_save").on("click", saveProfile);
    };

    self.showPhotoViewer = function () {
        $("#photo_viewer").photoviewer("show");
    };

    self = $.extend(new spark.views.baseprofile(), self);

    return self;
})();

$(document).on("pageshow", "#myprofile_page", function () {
    spark.views.myprofile.init();
});

spark.views.photos = (function () {
    var self = {};
    var photoList = [];

    function bindEvents(ul) {
        ul.off("click").on("click", function (e) {
            var target = $(e.target);
            var li;

            if (target.is("img")) {
                showPhotoViewer(target.attr("data-index"));
            }

            if (target.is("p") && target.hasClass("use-main")) {
                li = target.parent();

                flagPhotoAsMain(li.attr("data-photoid"), true);
            }

            if (target.is("button")) {
                li = target.parent();
                deletePhoto(li.attr("data-photoid"));
            }

            if (target.is("span")) {
                showCaption(target);
            }
        });
    };

    function deletePhoto(photoId) {
        spark.utils.common.showConfirm("Are you sure you want to delete this photo? ", function () {
            spark.api.client.callAPI(spark.api.client.urls.deletePhoto, { memberPhotoId: photoId }, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                showPhotos(function () {
                    spark.utils.common.showMessageBar("Your photo has been deleted", true);
                });
            }, null);
        });
    };

    function flagPhotoAsMain(id, isMain) {
        var params = {
            IsMain: true
        };

        spark.api.client.callAPI(spark.api.client.urls.updatePhotoCaption, { memberPhotoId: id }, params, function (success, response) {
            if (!success) {
                var error = response.error;
                if (error.message.length > 0) {
                    spark.utils.common.showGenericDialog(error.message);
                }

                return;
            }

            showPhotos();
        });

    };

    function getPhotos(callback) {
        var ul = $("#photos_list");
        ul.find("li:gt(0)").remove();

        photoList = [];
        spark.api.client.callAPI(spark.api.client.urls.photos, { targetMemberId: spark.utils.common.getMemberId() }, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            ul.find("li:gt(0)").remove();
            bindEvents(ul);

            var photos = response.data;
            $.each(photos, function (i, item) {
                var li = $("<li />");
                var img = $("<img />");
                var temp = $("<p>Pending<br/>Approval</p>");
                var useMain = $("<p class=\"use-main\">Use As<br/>Main</p>");
                //var span = $("<span />");
                var caption = $("<span />");
                //var mainButton = $("<button>Use as Main</button>");
                var deleteButton = $("<button class=\"delete\"></button>");

                img.attr("data-index", i);
                img.prop("src", item.fullPath);
                img.appendTo(li);
                if (!item.isPhotoApproved) {
                    li.addClass("unapproved");
                    temp.appendTo(li);
                } else if(item.isPhotoApproved && !item.IsMain) {
                    li.addClass("usemain");
                    useMain.appendTo(li);
                }

                deleteButton.addClass("trash");
                //deleteButton.data("photo_id", item.memberPhotoId);
                deleteButton.appendTo(li)

                //if(!item.isPhotoApproved) 
                //span.text();
                //span.appendTo(li);

                var defaultCaption = "{ Tap here to edit caption }";
                caption.addClass("caption");
                //caption.data("photo_id", item.memberPhotoId);
                caption.text(item.caption || defaultCaption);
                caption.appendTo(li);

                //mainButton.appendTo(li)

                li.attr("data-photoid", item.memberPhotoId);
                li.appendTo(ul);

                // Store the photos in a list so we can re-use the same data on
                // the PhotoViewer widget.
                photoList.push({
                    imagePath: item.fullPath,
                    caption: item.caption,
                    thumbnail: item.thumbPath
                });
            });

            // Update the uploaded count.
            var addPhotoBar = ul.find("li:first-child");
            addPhotoBar.find("div").removeClass("disabled");
            addPhotoBar.find("h5:first-child").text("Add new photo");
            addPhotoBar.find("input[type='file']").prop("disabled", false);

            var photoUploadCount = addPhotoBar.find("div h5:eq(1)");
            photoUploadCount.text(photos.length + " of 12 uploaded")

            // Check if user exceeded the maximum upload allowed.
            if (photos.length >= 12) {
                addPhotoBar.find("div").addClass("disabled");
                addPhotoBar.find("h5:first-child").text("12 of 12 photos uploaded.");
                addPhotoBar.find("h5:eq(1)").text("Delete some to add more.");
                addPhotoBar.find("input[type='file']").prop("disabled", true);
            }
            
            //spark.utils.common.hideLoading();
            if (photos.length === 0)
                ul.append("<li class=\"no-results\"><p>No results found.</p></li>");

            callback();
        });
    }

    function initializeFileUploader() {
        var url = spark.api.client.buildUrl(spark.api.client.urls.upload_photo.url);

        // Initialize file uploader.
        $("#fileupload").fileupload({
            add: function (e, data) {
                var fileSizeLimit = 5000000;
                var supportedFileType = /\.(gif|jpg|jpeg|tiff|png)$/i;
                var regex = new RegExp(supportedFileType);
                var file = data.files[0];

                if (!regex.test(file.name)) {
                    spark.utils.common.showGenericDialog("File type not supported.");
                    return;
                }

                if (file.size > fileSizeLimit) {
                    spark.utils.common.showGenericDialog("File size too large. Image max size is 5 MB.");
                    return;
                }

                //data.submit();
                showPreview(e, data);
            },
            done: function (e, data) {
                spark.utils.common.hideLoading();

                showPhotos(function () {
                    $("#photo_preview_panel").hide();
                    $("#photo_list_panel").fadeIn();
                });
            },
            fail: function (e, data) {
                var xhr = data.jqXHR;
                var json = $.parseJSON(xhr.responseText);
                var message = "Unable to upload image.";

                if (json) {
                    message = json.error.message;
                }

                spark.utils.common.hideLoading();
                spark.utils.common.showGenericDialog(message, function () {
                    showPhotos(function () {
                        $("#photo_preview_panel").hide();
                        $("#photo_list_panel").fadeIn();
                    });
                });
            },
            send: function (e, data) {
                // If browser does not support XHR, add logic below to handle any process contain
                // in the "done" or any callback non-XHR do not trigger.
                if (data.dataType.indexOf('iframe') >= 0) {
                    window.setTimeout(function () {
                        spark.utils.common.hideLoading();
                        showPhotos(function () {
                            $("#photo_preview_panel").hide();
                            $("#photo_list_panel").fadeIn();
                        });
                    }, 1500);
                }
            },
            url: url,
            dataType: "json",
            maxNumberOfFiles: 1,
            autoUpload: false
        });
    }

    function showCaption(target) {
        var listPanel = $("#photo_list_panel");
        var panel = $("#photo_caption_panel");
        var cancelButton = $("#btn_cancel_caption");
        var saveButton = $("#btn_save_caption");
        var textarea = $("#caption");
        var caption = target.text().indexOf("Tap here to edit caption") === -1 ? target.text() : "";
        var textCounter = $(".char-counter");
        var count = 0;
        var temp = "100 characters left";

        listPanel.hide();

        textarea.val(caption);
        textCounter.text(temp);

        if (caption.length > 0) {
            count = (100 - caption.length);
            temp = (count > 1) ? count + " characters left" : count + " character left";

            textCounter.text(temp);
        }

        function updateTextCounter(target) {
            var text = target.val();

            count = (100 - target.val().length);
            temp = (count > 1) ? count + " characters left" : count + " character left";

            textCounter.text(temp);
        }

        /*textarea.keyup(function (e) {
            updateTextCounter($(this));
        });*/

        textarea.on("input paste", function () {
            updateTextCounter($(this));
        })

        cancelButton.unbind("click").bind("click", function () {
            showPhotos(function () {
                panel.hide();
                listPanel.fadeIn();
            });
        });

        saveButton.unbind("click").one("click", function () {
            var photoId = target.parent().attr("data-photoid");
            updatePhotoCaption(photoId, textarea.val());
        });

        panel.fadeIn().removeClass("hidden");
    }

    function showPhotos(callback) {
        //spark.utils.common.showLoading("Loading...");
        var ul = $("#photos_list");

        ul.find("li.no-results").remove();

        getPhotos(function () {
            if(typeof callback === "function")
                callback();
        });
    }

    function showPhotoViewer(index) {
        if (photoList.length === 0)
            return;

        var photoViewer = $("#photo_viewer");
        photoViewer.photoviewer("setBeforeHideListener", function () {
            $("div[data-role='content']").show();
        });

        photoViewer.photoviewer("setBeforeShowListener", function () {
            $("div[data-role='content']").hide();
        });

        photoViewer.photoviewer("show", photoList, index);
    }

    function showPreview(e, data) {
        var listPanel = $("#photo_list_panel");
        var preview = $("#photo_preview_panel");
        var cancelButton = preview.find("#btn_cancel_upload");
        var uploadButton = preview.find("#btn_upload_photo");
        //var canvas = document.getElementById("photo_canvas");
        var canvas = $("#photo_canvas");
        var maxWidth = $(window).outerWidth();
        var maxHeight = $(window).outerHeight();

        listPanel.hide();
        spark.utils.common.hideFooter();

        try {
            //var context = canvas.getContext("2d");
            var file = data.files[0];

            canvas.find("img").remove();

            var loadingImage = loadImage(file, function (img) {
                //var width = img.width;
                //var height = img.height;
                //var ratio = 1;

                //canvas.width = maxWidth;
                //canvas.height = maxHeight - 44;

                //if (width > maxWidth || height > maxHeight) {
                //if(width > maxWidth) {
                //    ratio = Math.min(maxWidth / width, maxHeight / height);
                //}

                //width = (width * ratio);
                //height = (height * ratio);

                $(img).css({
                    maxWidth: maxWidth,
                    height: "auto"
                });
                $(img).appendTo(canvas);

                //var left = (canvas.width - width) / 2;
                //var top = (canvas.height - height) / 2;
                //var radian = Math.PI / 180;

                //context.save();
                //context.translate(left, top);
                //context.drawImage(img, 0, 0, width, height);
                //context.restore();

                // Get image meta data and try to extract EXIF data so we can figure out the orientation
                // if available.
                /*loadImage.parseMetaData(file, function (data) {
                    if (data.exif) {
                        var radian = Math.PI / 180;
                        var orientation = data.exif.get("Orientation");

                        // If orientation is right-side up, then we don't need to rotate image.
                        if (typeof orientation === "undefined" || orientation === 1)
                            return;

                        // Save current canvas context then clear canvas to re-draw the
                        // rotated image.
                        context.save();
                        context.clearRect(0,0,maxWidth,maxHeight);

                        switch (orientation) {
                            case 3:

                                break;
                            case 6:
                                context.translate(left, top);
                                context.translate(width/2, height/2);
                                context.rotate(90*radian);
                                context.drawImage(img, -width / 2, -height / 2, width, height);
                                break;
                        }

                        //context.restore();
                    }
                });*/
            });

            // In case the browser doesn't support image preview.
            if (!loadingImage) {
                //if (canvas) $(canvas).remove();

                var p = $("<p />");
                p.text(file.name);
                p.appendTo(canvas);
            }

        } catch (err) {

        }

        cancelButton.unbind("click").bind("click", function () {
            showPhotos(function () {
                preview.hide();

                spark.utils.common.showFooter();
                listPanel.fadeIn();
            });
        });

        uploadButton.unbind("click").one("click", function () {
            spark.utils.common.showLoading("Uploading...");

            data.formData = [{ name: "caption", value: "" }];
            data.submit();
        });

        preview.fadeIn().removeClass("hidden");
    };

    function updatePhotoCaption(id, caption) {
        spark.api.client.callAPI(spark.api.client.urls.updatePhotoCaption, { memberPhotoId: id }, { caption: caption }, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            window.setTimeout(function () {
                showPhotos(function () {
                    $("#photo_caption_panel").hide();
                    $("#photo_list_panel").fadeIn();
                })
            }, 500);
        });
    };

    self.init = function () {
        initializeFileUploader();

        var ul = $("#photos_list");
        ul.children("li:gt(0)").each(function () {
            var li = $(this);
            var img = li.find("img");

            photoList.push({
                imagePath: img.attr("data-fullpath"),
                caption: img.attr("alt"),
                thumbnail: img.attr("src")
            });
        });

        bindEvents(ul);
        //showPhotos();
    };

    return self;
})();

$(document).on("pageshow", "#photos_page", function (e, ui) {
    var page = ui.prevPage;
    if (page.attr("id") === "myprofile_page") {
        $("#btn_back").removeClass("hidden");
    }


    spark.views.photos.init();
    //$(window).off("orientationchange").on("orientationchange", function (event) {
    //    var previewPanel = $("#photo_preview_panel");
    //});
});

spark.views.search = (function () {
    "use strict";
    var self = {};
    var resultPageSize = 25;
    var resultPageNumber = 1;
    var isLoading = false;
    var isLastPage = true;
    var showResultPage = false;
    var pageInit = true;

    var gender = "male";
    var seeking = "female";
    var photoOnly = false;
    var jewishOnly = true;
    var location = "";
    var regionID = "";
    var distance = 5;
    var minAge = 18;
    var maxAge = 29;
    var sortType = 3;
    var prefCookieID = "mos_search_pref_" + spark.utils.common.getMemberId();
    var memberList = [];

    var gridHeight = 0;
    var gridWidth = 0;

    function getMatchPreferences() {
        if ($.cookie(prefCookieID) != null) {
            var cookie = $.cookie(prefCookieID);
            var tempArr = cookie.split("&");
            var prefs = {};

            $.each(tempArr, function (index, item) {
                var length = item.indexOf("=");
                var key = item.substring(0, length);
                var value = item.substring(length + 1);

                prefs[key] = value;
            });

            gender = prefs.gender;
            seeking = prefs.seeking;
            minAge = prefs.minAge;
            maxAge = prefs.maxAge;
            distance = prefs.distance;
            location = prefs.location;

            if (prefs.sortType != null) {
                sortType = parseInt(prefs.sortType);
            }

            if (prefs.photoOnly == "true") {
                photoOnly = true;
            }
            else {
                photoOnly = false;
            }
            
            if (prefs.jewishOnly == "true") {
                jewishOnly = true;
            }
            else {
                jewishOnly = false;
            }            

            setupDefaultPref();
        }
        else {
            spark.api.client.callAPI(spark.api.client.urls.get_preferences, null, null, function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

            
                var data = response.data;

                if (data.gender == "Female") {
                    seeking = "female";
                }
                else {
                    seeking = "male";
                }

                if (data.seekingGender == "Male") {
                    gender = "male";
                }
                else {
                    gender = "female";
                }

                if (data.minAge != null) {
                    minAge = data.minAge;
                }

                if (data.maxAge != null) {
                    maxAge = data.maxAge;
                }

                if (data.location != null) {
                    location = data.location;
                }

                if (data.maxDistance != null) {
                    distance = data.maxDistance;
                }

                if (data.showOnlyMembersWithPhotos != null) {
                    photoOnly = data.showOnlyMembersWithPhotos;
                }

                setupDefaultPref();
                setupCookie();
            });
        }
    }

    function setupDefaultPref() {
        var genderToggle = $("#gender");
        var genderSeekingToggle = $("#gender_seeking");
        var ddMinAge = $("#min_age");
        var ddMaxAge = $("#max_age");
        var ddDistance = $("#distance");
        var postalCode = $("#postal_code");
        var cbPhotos = $("#picture_only");
        var cbJewish = $("#jewish_only");
        var sortFilter = $("#result_sort_filter");

        // Set gender value.
        genderToggle.find("a.female").removeClass("selected");
        genderToggle.find("a.male").removeClass("selected");
        if (gender === "female") {
            genderToggle.find("a.female").addClass("selected");
        } else {
            genderToggle.find("a.male").addClass("selected");
        }

        // Set seeking gender.
        genderSeekingToggle.find("a.female").removeClass("selected");
        genderSeekingToggle.find("a.male").removeClass("selected");
        if (seeking === "female") {
            genderSeekingToggle.find("a.female").addClass("selected");
        } else {
            genderSeekingToggle.find("a.male").addClass("selected");
        }

        // Set age range, distance and postal/zip code value.
        ddMinAge.find("select").val(minAge);
        ddMaxAge.find("select").val(maxAge);
        ddDistance.find("select").val(distance);
        postalCode.val(location);        
        sortFilter.find("select").val(sortType);

        // Set checkbox value for photos and jeweish only.
        // TODO: Create custom widget.
        cbPhotos.removeClass("checked");
        cbJewish.removeClass("checked");
        if (photoOnly)
            cbPhotos.addClass("checked");
        if (jewishOnly)
            cbJewish.addClass("checked");
    }

    function setupButtonClickEvent() {
        var genderToggle = $("#gender");
        var genderSeekingToggle = $("#gender_seeking");
        var ddMinAge = $("#min_age");
        var ddMaxAge = $("#max_age");
        var ddDistance = $("#distance");
        var cbPhotos = $("#picture_only");
        var cbJewish = $("#jewish_only");

        // TODO: Create widget to abstract functionality.
        genderToggle.off("click").on("click", function (e) {
            var self = $(this);
            var target = $(e.target);

            target.addClass("selected");
            if (target.hasClass("female")) {
                self.find("a.male").removeClass("selected");
                gender = "female";
            }

            if (target.hasClass("male")) {     
                self.find("a.female").removeClass("selected");
                gender = "male";
            }
        });

        genderSeekingToggle.off("click").on("click", function (e) {
            var self = $(this);
            var target = $(e.target);

            target.addClass("selected");
            if (target.hasClass("female")) {
                self.find("a.male").removeClass("selected");
                seeking = "female";
            }

            if (target.hasClass("male")) {
                self.find("a.female").removeClass("selected");
                seeking = "male";
            }
        });

        cbPhotos.off().on("click", function (e) {
            var self = $(this);
            if (self.hasClass("checked")) {
                self.removeClass("checked");
                photoOnly = false;
            } else {
                self.addClass("checked");
                photoOnly = true;
            }
        });

        cbJewish.off().on("click", function (e) {
            var self = $(this);
            if (self.hasClass("checked")) {
                self.removeClass("checked");
                jewishOnly = false;
            } else {
                self.addClass("checked");
                jewishOnly = true;
            }
        });

        ddMinAge.find("select").on("change", function () {
            minAge = this.value;
        });

        ddMaxAge.find("select").on("change", function () {
            maxAge = this.value;
        });

        ddDistance.find("select").on("change", function () {
            distance = this.value;
        });

        $("#preferences-search-btn").click(function () {
            //minAge = ddMinAge.find("select").val();
            //maxAge = ddMaxAge.find("select").val();

            if (minAge > maxAge || minAge == "" || maxAge == "") {
                spark.utils.common.showGenericDialog("Please select the right age range!");
                return;
            }

            //location = $("#preferences-zipcode-textarea").val();
            location = $("#postal_code").val();
            if (validateZipcode(location)) {
                setupCookie();

                memberList = [];
                showResultPage = true;
                isLastPage = false;
                resultPageNumber = 1;
                //getSearchResults();
                getLocationByZipCode();

                $(".search-result-page").removeClass("hidden");
                $(".search-preferences-page").addClass("hidden");
                $(".search-result-page").fadeIn(350);
            }
            else {
                spark.utils.common.showGenericDialog("Invalid Zip code!");
            }
        });

        $("#result-back-btn").click(function () {
            showResultPage = false;
            $(".search-result-page").fadeOut(350, function () {
                memberList = [];
                $(".search-preferences-page").removeClass("hidden");
                $(".search-result-page").addClass("hidden");
                $("#search-result-list").find("li").remove();
                $("#search-result-list").find("p").remove();
            });            
        });

        $("#result_sort_filter").find("select").on("change", function () {
            $("#search-result-list").find("li").remove();
            $("#search-result-list").find("p").remove();
            isLastPage = false;
            resultPageNumber = 1;
            sortType = parseInt(this.value);
            getSearchResults();
        });

        $("#memberlookup-find").click(function () {
            var memberID = $("#lookupContent").val();
            lookupProfile(memberID);
            //memberList = [];
            //showResultPage = true;
            //isLastPage = false;
            //resultPageNumber = 1;

            //$(".search-result-page").removeClass("hidden");
            //$(".search-preferences-page").addClass("hidden");
            //$(".search-result-page").fadeIn(350);
        });
    }

    function lookupProfile(memberID) {
        var params;
        var api;

        if (isNaN(memberID)) {
            api = spark.api.client.urls.lookupByUsername;
            params = {
                //attributeSetName: "fullprofile",
                attributeSetName: "miniprofile",
                targetMemberUsername: memberID
            }
        }
        else {
            var api = spark.api.client.urls.fullProfile;
            var params = {
                targetMemberId: Number(memberID)
            };
        }

        spark.utils.common.showLoading();
        window.setTimeout(function () {
            spark.api.client.callAPI(api, params, null, function (success, response) {
                spark.utils.common.hideLoading();

                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                $.mobile.changePage("/profile/" + response.data.memberId);
                //appendToList(data.Members);
            });
        }, 2000);
    }

    function getLocationByZipCode() {
        isLoading = true;

        if (location !== "") {
            var params = {
                zipCode: location
            };

            spark.api.client.callAPI(spark.api.client.urls.get_location, params, null, function (success, response) {
                if (!success) {
                    isLoading = false;
                    spark.utils.common.showError(response);
                    return;
                }

                regionID = response.data[0].Id;
                getSearchResults();
            });            
        }
        else {
            regionID = "";
            getSearchResults();
        }
    }

    function getSearchResults() {
        isLoading = true;

        var ul = $("#search-result-list");
        var params = {
            PageSize: resultPageSize,
            PageNumber: resultPageNumber,
            Gender: seeking,
            SeekingGender: gender,
            MinAge: minAge,
            MaxAge: maxAge,
            Location: regionID,
            MaxDistance: distance,
            ShowOnlyMembersWithPhotos: photoOnly,
            searchOrderByType: sortType,
            searchEntryPointType: 3,
            searchType: 12,
        };

        /* Sort Type*/
        //JoinDate = 1, LastLogonDate = 2, Proximity = 3, Popularity = 4,
        //ColorCode = 5, KeywordRelevance = 6, MutualMatch = 7

        var interval = ul.find("li").length !== 0 ? 2000 : 0;

        // Add loading footer.
        var loadingPanel = spark.utils.common.loadingFooter();
        loadingPanel.appendTo(ul);

        window.setTimeout(function () {
            spark.api.client.callAPI(spark.api.client.urls.getSearchResults, null, params, function (success, response) {
                if (!success) {
                    isLoading = false;
                    spark.utils.common.showError(response);
                    return;
                }

                resultPageNumber++;
                var data = response.data;

                if (data.Members.length < resultPageSize) {
                    isLastPage = true;
                }

                appendToList(data.Members);
            });
        }, interval);
    }

    function appendToList(members) {
        var ul = $("#search-result-list");

        // Remove loading footer.
        ul.find("li.loading").remove();

        if (members.length == 0) {
            var p = $("<p />");
            p.text("No results found!")
            p.addClass("no-results-text");
            p.appendTo(ul);
        }

        $.each(members, function (index, item) {
            var li = $("<li data-memberid=\"" + item.memberId + "\" />");
            var a = $("<a data-role=\"none\"/>");
            var img = $("<img />");
            var span1 = $("<span />");
            var span2 = $("<span />");
            var span3 = $("<span />");
            //var mPercentage = $("<span class = \"matches-label\" />");
            var p = $("<p />");
            var br = $("<br />");
            var thumbnail = (item.gender === "Female") ? "/images/hd/img_silhouette_woman@2x.png" : "/images/hd/img_silhouette_man@2x.png";

            a.attr("href", "/profile/" + item.memberId);

            if (item.defaultPhoto && item.defaultPhoto.thumbPath !== "") {
                thumbnail = item.defaultPhoto.fullPath;
            }
            else if (item.DefaultPhoto && item.DefaultPhoto.thumbPath !== "") {
                thumbnail = item.DefaultPhoto.fullPath;
            }

            img.attr("src", thumbnail);
            img.appendTo(a);

            span1.text(item.username);
            span1.addClass("primary");
            span1.appendTo(p);

            var ageText = "";
            var locText = "";

            if (item.age != null) {
                ageText = item.age;
            }

            if (item.location != null) {
                locText = item.location;
                span2.text(ageText + ", " + locText);
            }
            else {
                span2.text(ageText);
            }
            
            span2.addClass("secondary");
            span2.appendTo(p);

            if (item.isOnline === true) {
                span3.text("Online");
                span3.addClass("online");
                span3.appendTo(p);
            }

            if (!ul.hasClass("grid-view")) {
                span3.text("Online");
            } else {
                // If in grid mode, apply neccessary in-style to override the CSS class.
                li.css({
                    height: gridHeight + "px"
                });

                img.css({
                    width: gridWidth + "px",
                    height: gridHeight + "px"
                });
            }

            p.appendTo(a);

            //if (item.matchRating != null) {
            //    mPercentage.text(item.matchRating + "% match");
            //    mPercentage.appendTo(a);
            //}

            a.appendTo(li);
            li.appendTo(ul);
            memberList.push(item);
           
        });

        if (pageInit && spark.utils.common.loadViewToggleStatus() === "grid") {
            showGrid(ul);
        }

        isLoading = false;
        pageInit = false;
        //$(document).data("members", memberList);

        //$("ul#search-result-list").append($('<li />').append($("<p />", { 'class':'no-results-text','text': 'There are no more results' })));
    }

    function initializeListToggle() {
        var ul = $("#search-result-list");
        var toggle = $(".list-grid-toggle");
        var listButton = toggle.find("a.list");
        var gridButton = toggle.find("a.grid");
        //var gridClass = "grid-view";

        if (spark.utils.common.loadViewToggleStatus() === "grid") {
            $("#btn_grid_view").addClass("active");
            $("#btn_list_view").removeClass("active");
        }

        listButton.off().on("click", function (e) {
            showList(ul);
        });

        gridButton.off().on("click", function (e) {
            showGrid(ul);
        });
    };

    function resizeGridImage(ul) {
        var defaultImageWidth = 106;
        var defaultImageHeight = 136;
        var marginWidth = 1;
        var maxThreshold = 10;
        var imageWidth = defaultImageWidth + marginWidth;
        var ratio = 1;
        //var gridWidth = 0;
        //var gridHeight = 0;
        var screenHeight = $(window).height();
        var screenWidth = $(window).width();
        var gridCount = Math.floor((screenWidth / imageWidth));
        var gridTotalWidth = (gridCount * imageWidth)
        var threshold = (screenWidth - gridTotalWidth);
        var padding = 0;

        if (threshold > maxThreshold) {
            gridCount = (gridCount + 1);
            gridWidth = Math.floor((screenWidth / gridCount)) - marginWidth;
            gridTotalWidth = (gridCount * (gridWidth + marginWidth));
            threshold = (screenWidth - gridTotalWidth);

            ratio = Math.round((gridWidth / imageWidth) * 100) / 100;
            gridHeight = Math.round((defaultImageHeight * ratio));
        }

        ul.css({ paddingLeft: 0 });
        if (threshold > 0) {
            padding = Math.round((threshold / 2)) - marginWidth;
            ul.css({
                paddingLeft: padding + "px"
            });
        }

        if (gridHeight !== 0 && gridWidth !== 0) {
            ul.find("li").css({
                height: gridHeight + "px"
            });

            ul.find("li > a > img").css({
                width: gridWidth + "px",
                height: gridHeight + "px"
            });
        }
    };

    function setupCookie() {
        $.cookie(prefCookieID, "gender=" + gender + "&seeking=" + seeking + "&minAge=" + minAge + "&maxAge=" + maxAge + "&distance=" + distance + "&location=" + location + "&photoOnly=" + photoOnly + "&jewishOnly=" + jewishOnly + "&sortType=" + sortType, { expires: 20 });
    }

    function showGrid(ul) {
        if (ul.hasClass("grid-view"))
            return;

        if (ul.find("li").hasClass("loading")) {
            return;
        }

        ul.addClass("grid-view");
        $("#btn_grid_view").addClass("active");
        $("#btn_list_view").removeClass("active");
        spark.utils.common.setCookieForViewToggle("grid");

        resizeGridImage(ul);

        ul.hide();
        ul.find("span.online").text("");
        ul.fadeIn(350);
    };

    function showList(ul) {
        if (!ul.hasClass("grid-view"))
            return;

        $("#btn_list_view").addClass("active");
        $("#btn_grid_view").removeClass("active");

        ul.find("li").removeAttr("style");
        ul.find("li > a > img").removeAttr("style");

        ul.hide();
        ul.removeClass("grid-view");
        ul.find("span.online").text("Online");
        spark.utils.common.setCookieForViewToggle("list");
        ul.fadeIn(350);
    }

    function validateZipcode(zipcode) {
        var CNRegex = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;  // Validate either with or without " ", "-"
        var USRegex = /(^\d{5}$)|(^\d{5}-\d{4}$)/;  // Validate either 5 or 5+4 digits
        var isValid = false;

        if (zipcode.length == 5) {
            isValid = USRegex.exec(zipcode);
        }
        else if (zipcode.length == 6) {
            isValid = CNRegex.exec(zipcode);
        }
        else if (zipcode == "") {
            isValid = true;
        }
        else {
            return false;
        }

        return isValid;
    }

    function changeTabs(tabID) {
        var tabs = $("ul.search-tabs");
        tabs.children("li").each(function () {
            var li = $(this);
            var a = li.find("a");

            if (a.text() === tabID) {
                if (tabID === "Member Lookup") {
                    a.addClass("active").removeClass("border-left");
                    $("#tab-1").addClass("hidden");
                    $("#tab-2").removeClass("hidden");
                }
                else if (a.text() === "Search") {
                    a.addClass("active").removeClass("border-right");
                    $("#tab-1").removeClass("hidden");
                    $("#tab-2").addClass("hidden");
                }
            }
            else {
                if (a.text() === "Member Lookup") {
                    a.addClass("border-left").removeClass("active");
                } else if (a.text() === "Search") {
                    a.addClass("border-right").removeClass("active");
                }
            }
        });
    }

    self.init = function () {
        //$(".search-preferences-page").tabs({
        //    beforeActivate: function (e, ui) {
        //        var newTab = ui.newTab;
        //        var oldTab = ui.oldTab;

        //        var a = newTab.find("a");
        //        if (a.text() === "Member Lookup") {
        //            a.addClass("active").removeClass("border-left");
        //            a = oldTab.find("a");
        //            a.addClass("border-right").removeClass("active");
        //        } else if (a.text() === "Search") {
        //            a.addClass("active").removeClass("border-right");
        //            a = oldTab.find("a");
        //            a.addClass("border-left").removeClass("active");
        //        }
        //    }
        //});
        var tabs = $("ul.search-tabs");
        tabs.children("li").each(function () {
            var li = $(this);

            // Bind event to anchor link(s).
            li.find("a").click(function (e) {
                var a = $(this);
                var tabID = a.text();

                changeTabs(tabID);
                e.preventDefault();
            });
        });

        initializeListToggle();

        setupButtonClickEvent();
        getMatchPreferences();
    };

    self.resizeGrid = function (ul) {
        resizeGridImage(ul);
    };

    self.saveCookie = function () {
        showResultPage = false;
        memberList = [];
        $(".search-preferences-page").removeClass("hidden");
        $(".search-result-page").addClass("hidden");
        $("#search-result-list").find("li").remove();

        setupCookie();
    }

    self.getMoreSearchResult = function () {
        if (!showResultPage)
            return;

        if (isLoading || isLastPage)
            return;

        getSearchResults();
    }

    self.storeMembers = function () {
        var members = [];
        spark.views.search.cleanCache();
        $("#search-result-list").children("li").each(function () {
            var li = $(this);
            members.push({
                memberId: li.attr("data-memberid"),
                thumbnail: li.find("a img").attr("src"),
                userName: li.find(".primary").text()
            });
        });

        // Cache data so that it can be retrieve from the Profile page.
        $(document).data("members", members);

        var isGrid = false;
        var ul = $("#search-result-list");
        if (ul.hasClass("grid-view")) {
            isGrid = true;
        }

        if (showResultPage) {
            $(document).data("cachedData", {
                cachedPage: "search_result",
                isGrid: isGrid,
                pageNumber: resultPageNumber,
                lastPage: isLastPage,
                loading: isLoading,
                showResult: showResultPage,
                locationID: regionID,
                offsetY: $(document).scrollTop(),
                membersData: memberList,
            });
        }
        else {
            $(document).data("cachedData", {
                cachedPage: "search_Lookup",
                showResult: showResultPage,
                lookupName: $("#lookupContent").val(),
            });
        }        
    }

    self.cleanCache = function () {
        $(document).removeData("members");
        $(document).removeData("cachedData");
    }

    self.resumePageStatus = function () {
        var cachedData = $(document).data("cachedData");

        if (cachedData != null) {
            if (cachedData.cachedPage == "search_result") {
                resultPageNumber = cachedData.pageNumber;
                isLastPage = cachedData.lastPage;
                isLoading = cachedData.loading;
                showResultPage = cachedData.showResult;
                regionID = cachedData.locationID;
                memberList = [];

                $(".search-preferences-page").addClass("hidden");
                $(".search-result-page").fadeIn(10, function () {
                    appendToList(cachedData.membersData);

                    $(".search-result-page").removeClass("hidden");
                    $(document).scrollTop(cachedData.offsetY);
                });
            }
            else if (cachedData.cachedPage == "search_Lookup") {
                changeTabs("Member Lookup");
                $("#lookupContent").val(cachedData.lookupName);
            }
        }
    }
    
    self.reset = function () {
        pageInit = true;
    }

    return self;
})();

$(document).on("pageshow", "#search_page", function () {
    $(window).off("orientationchange").on("orientationchange", function (event) {
        var ul = $("#search-result-list");

        if (ul && ul.hasClass("grid-view")) {
            spark.views.search.resizeGrid(ul);
        }
    });

    spark.views.search.init();
    spark.views.search.resumePageStatus();
});

$(document).on("pagehide", "#search_page", function () {
    spark.views.search.saveCookie();
    spark.views.search.reset();
});

$(document).on("pagebeforehide", "#search_page", function (e, ui) {
    //check if the next page is the profile page
    if (ui.nextPage.attr('id') == 'profile_page') {
        spark.views.search.storeMembers();
    }
    else {
        spark.views.search.cleanCache();
    }
});