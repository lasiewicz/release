﻿spark.views.mol = (function() {
    "use strict";
    var self = {};
    var memberList = [];

    var pageInit = true;
    var isLoading = false;
    var isLastPage = false;
    var gridWidth = 0;
    var gridHeight = 0;
    var molCookieID = "mos_mol_pref_" + spark.utils.common.getMemberId();
 


    function appendToList(members) {
        var ul = $("ul.scrollable");

        // Remove loading footer.
        ul.find("li.loading").remove();

        $.each(members, function (index, item) {
            if (item.isOnline && (item.id !== ($("div.nav-header").attr("data-myid")))) {
                
                var li = $("<li data-memberid=\"" + item.id + "\" />");
                var a = $("<a />");
                var img = $("<img />");
                var p = $("<p />");
                var userName = $("<span class=\"primary\" />");
                var ageLocation = $("<span class=\"secondary\" />");
                var online = $("<span class=\"online\" />");
                var imageUrl = (item.gender === "Male") ? "/images/hd/img_silhouette_man@2x.png" : "/images/hd/img_silhouette_woman@2x.png";

                a.attr("href", "/profile/" + item.id);
                if (item.defaultPhoto && item.defaultPhoto.thumbPath !== "") {
                    imageUrl = item.defaultPhoto.thumbPath;
                }

                img.attr("src", imageUrl).appendTo(a);
                userName.text(item.username).appendTo(p);

                var ageText = "";
                var locText = "";

                if (item.age != null) {
                    ageText = item.age;
                }

                if (item.location != null) {
                    locText = item.location;
                    ageLocation.text(ageText + ", " + locText).appendTo(p);
                } else {
                    ageLocation.text(ageText).appendTo(p);
                }
                if (item.isOnline) {
                    if (!ul.hasClass("grid-view")) {
                        online.text("Online");
                    } else {
                        // If in grid mode, apply neccessary in-style to override the CSS class.
                        li.css({
                            height: gridHeight + "px"
                        });

                        img.css({
                            width: gridWidth + "px",
                            height: gridHeight + "px"
                        });
                    }
                    online.appendTo(p);
                }

                if (item.isHighlighted) {
                    li.addClass('member-highlight');
                    p.addClass('member-highlight');
                }

                p.appendTo(a);
                a.appendTo(li);
                li.appendTo(ul);

                memberList.push(item);

            }
        });

        if (pageInit && spark.utils.common.loadViewToggleStatus() === "grid") {
            showGrid(ul);
            $("div#mos_christian_membersonline_list_top_320x50").addClass("mol_hide");
            $("div#mos_christian_membersonline_grid_top_320x50").removeClass("mol_hide");
        }

        isLoading = false;
        pageInit = false;
    }



    function getPreference()
    {
        var prefs = {};

        if ($.cookie(molCookieID) != null) {
            var cookie = $.cookie(molCookieID);
            var tempArr = cookie.split("&");    

            $.each(tempArr, function (index, item) {
                var length = item.indexOf("=");
                var key = item.substring(0, length);
                var value = item.substring(length + 1);

                prefs[key] = value;
            });
        }
        else {
            prefs["seekingGender"] = $(".gender").attr("data-value");
            prefs["minAge"] = 25;
            prefs["maxAge"] = 40;
            prefs["sortType"] = 1;
        }

        gender = prefs.SeekingGender;
        minAge = prefs.MinAge;
        maxAge = prefs.MaxAge;


        if (prefs.sortType != null) {
            sortType = parseInt(prefs.sortType);
        }

        return prefs;
    }

    self.getMembersOnline = function () {
        if (pageInit) {
            return;
        }

        if (isLoading || isLastPage)
            return;

        var pageNo = Number($("#membersonline_page").attr("data-pageno"));
        loadMemberList(++pageNo);
    };

    function loadMemberList(pageNo) {
        isLoading = true;

        var ul = $("ul.scrollable");
        var page = $("#membersonline_page");
        var url = spark.api.client.urls.membersOnline;
        var pageSize = page.attr("data-pagesize");
        var prefs = {};
        var sortType = $("#mol_sort_filter select").val();

        if ($.cookie(molCookieID) != null) {
            var cookie = $.cookie(molCookieID);
            var tempArr = cookie.split("&");

            $.each(tempArr, function (index, item) {
                var length = item.indexOf("=");
                var key = item.substring(0, length);
                var value = item.substring(length + 1);

                prefs[key] = value;
            });
        }
        else {
            //call api for match prefs
            var prefsURL = spark.api.client.urls.get_preferences;
            spark.api.client.callAPI(prefsURL, null, prefs, function(success, response) {
                if (!success) {
                    //show error
                    spark.utils.common.showError(response);
                } else {
                    //setup prefs cookie
                    $.cookie(molCookieID, "seekingGender=" + response.data.seekingGender + "&minAge=" + response.data.minAge + "&maxAge=" + response.data.maxAge);

                    prefs.seekingGender = response.data.seekingGender;
                    prefs.minAge = response.data.minAge;
                    prefs.maxAge = response.data.maxAge;
                    
                }
            });
        }

        prefs.pageSize = pageSize;
        prefs.pageNumber = pageNo;
        prefs.sortType = sortType;


        // Add loading footer.
        spark.utils.views.addLoadingPanel(ul);

        window.setTimeout(function () {
            spark.api.client.callAPI(url, null, prefs, function (success, response) {
                if (!success) {
                    isLoading = false;
                    spark.utils.common.showError(response);
                    $("ul.scrollable").find("li.loading").remove();
                    return;
                }

                page.attr("data-pageno", prefs.PageNumber);

                if (response.data.members.length < pageSize) {
                    isLastPage = true;
                }

                appendToList(response.data.members);
            });
        }, spark.utils.common.getLoadingTime());
    }

    function initializeListToggle() {
        var ul = $("#mol_list");
        var toggle = $(".list-grid-toggle");
        var listButton = toggle.find("a.list");
        var gridButton = toggle.find("a.grid");
        //var gridClass = "grid-view";

        if (spark.utils.common.loadViewToggleStatus() === "grid") {
            $("#btn_grid_view").addClass("active");
            $("#btn_list_view").removeClass("active");
        }

        listButton.off().on("click", function (e) {
            showList(ul);
            //$("divmos_christian_membersonline_list_top_320x50").removeAttr("style", "display:block");
            $("div#mos_christian_membersonline_list_top_320x50").removeClass("mol_hide");
            $("div#mos_christian_membersonline_grid_top_320x50").addClass("mol_hide");
        });

        gridButton.off().on("click", function (e) {
            showGrid(ul);
            $("div#mos_christian_membersonline_list_top_320x50").addClass("mol_hide");
            $("div#mos_christian_membersonline_grid_top_320x50").removeClass("mol_hide");
        });
    };

    function resizeGridImage(ul) {
        var defaultImageWidth = 106;
        var defaultImageHeight = 136;
        var marginWidth = 1;
        var maxThreshold = 10;
        var imageWidth = defaultImageWidth + marginWidth;
        var ratio = 1;
        //var gridWidth = 0;
        //var gridHeight = 0;
        var screenHeight = $(window).height();
        var screenWidth = $(window).width();
        var gridCount = Math.floor((screenWidth / imageWidth));
        var gridTotalWidth = (gridCount * imageWidth)
        var threshold = (screenWidth - gridTotalWidth);
        var padding = 0;

        if (threshold > maxThreshold) {
            gridCount = (gridCount + 1);
            gridWidth = Math.floor((screenWidth / gridCount)) - marginWidth;
            gridTotalWidth = (gridCount * (gridWidth + marginWidth));
            threshold = (screenWidth - gridTotalWidth);

            ratio = Math.round((gridWidth / imageWidth) * 100) / 100;
            gridHeight = Math.round((defaultImageHeight * ratio));
        }

        ul.css({ paddingLeft: 0 });
        if (threshold > 0) {
            padding = Math.round((threshold / 2)) - marginWidth;
            ul.css({
                paddingLeft: padding + "px"
            });
        }

        if (gridHeight !== 0 && gridWidth !== 0) {
            ul.find("li").css({
                width: gridWidth + "px",
                height: gridHeight + "px"
            });

            ul.find("li > a > img").css({
                width: gridWidth + "px",
                height: gridHeight + "px"
            });
        }
    };

    function showGrid(ul) {
        if (ul.hasClass("grid-view"))
            return;

        if (ul.find("li").hasClass("loading")) {
            return;
        }

        ul.addClass("grid-view");
        $("#btn_grid_view").addClass("active");
        $("#btn_list_view").removeClass("active");
        spark.utils.common.setCookieForViewToggle("grid");

        resizeGridImage(ul);

        ul.hide();
        ul.find("span.online").text("");
        ul.fadeIn(350);
        setTimeout(function () {
            googletag.pubads().refresh([gpt_mol_grid_top]);
        }, 800);

    };

    function showList(ul) {
        if (!ul.hasClass("grid-view"))
            return;

        $("#btn_list_view").addClass("active");
        $("#btn_grid_view").removeClass("active");

        ul.find("li").removeAttr("style");
        ul.find("li > a > img").removeAttr("style");

        ul.hide();
        ul.removeClass("grid-view");
        ul.find("span.online").text("Online");
        spark.utils.common.setCookieForViewToggle("list");
        ul.fadeIn(350);
        setTimeout(function () {
            googletag.pubads().refresh([gpt_mol_top]);
        }, 800);

    }

    function setupButtonClickEvent() {
        $("#mol_sort_filter").find("select").on("change", function () {
            $("#mol_list").find("li").remove();
            $("#mol_list").find("p").remove();
            memberList = [];
            isLastPage = false;

            loadMemberList(1);
        });
    }



    self.init = function () {
        pageInit = true;

        loadMemberList(1);
        initializeListToggle();
        setupButtonClickEvent();
    };

    self.resizeGrid = function (ul) {
        resizeGridImage(ul);
    };

    self.updateCount = function () {
        spark.api.client.callAPI(spark.api.client.urls.members_online_count, null, null,
            function (success, response) {
                if (!success) {
                    spark.utils.common.showError(response);
                    return;
                }

                var id = $.mobile.activePage.attr("id");
                var data = response.data;
                var btn = $("#btn_mol_" + id);
                
                if (btn.length === 0)
                    return;

                btn.text(data.count + " Online");

                $("#molnumber").text(data.count);
            });
    };

    self.reset = function () {
        memberList = [];
        pageInit = true;
    }

    return self;
})();

$(document).on("pageshow", "#membersonline_page", function (e, ui) {
    $(window).off("orientationchange").on("orientationchange", function (event) {
        var ul = $("#mol_list");
        if (ul && ul.hasClass("grid-view")) {
            spark.views.mol.resizeGrid(ul);
        }
    });
       
    spark.views.mol.init();
    if (spark.utils.common.loadViewToggleStatus() === "list") {
        setTimeout(function () {
            googletag.pubads().refresh([gpt_mol_top]);
        }, 800);
    }
    if (spark.utils.common.loadViewToggleStatus() === "grid") {
        setTimeout(function () {
            googletag.pubads().refresh([gpt_mol_grid_top]);
        }, 800);
    }
    $('.nav-header .btn-mol').hide();
});

$(document).on("pagehide", "#membersonline_page", function () {
    spark.views.mol.reset();
    $('.nav-header .btn-mol').show();
});
