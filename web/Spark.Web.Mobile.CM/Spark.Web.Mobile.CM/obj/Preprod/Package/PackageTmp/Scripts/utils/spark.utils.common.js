﻿var spark = spark || {};
spark.api = spark.api || {};
spark.utils = spark.utils || {};
spark.views = spark.views || {};

spark.utils.common = (function() {
    var self = {};

    self.getLoadingTime = function() {
        return 1000;
    };


    self.parseDate = function (dateString, timeZone) {
        var dateToLocal;
        if (timeZone.toUpperCase() === ("PST")) {
           
            
            //PST try to determine if this utc offset it 7 or 8 hours
            var PacificTimeZone = self.isDateDST(dateString) ? 'GMT-0700' : 'GMT-0800';
            var a = dateString.split(/[^0-9]/);

            var parsedDate = a[1] + '/' + a[2] + '/' + a[0] + ' ' + a[3] + ':' + a[4] + ":" + a[5] + " " + PacificTimeZone;
            dateToLocal = new Date(parsedDate);
        } else {
            //UTC
            dateToLocal = new Date(dateString);
        }

        var year = dateToLocal.getFullYear();
        var month = dateToLocal.getMonth();
        var date = dateToLocal.getDate();
        var hour = dateToLocal.getHours();
        var minute = dateToLocal.getMinutes();
        var monthString = self.convertMonthToString(month);
        if (minute <= 9) {
            minute = "0" + minute;
        }
        var dayOrNight = "AM";
        if (hour === 12) {
            dayOrNight = "PM";
        }

        if (hour > 12) {
            dayOrNight = "PM";
            hour = hour % 12;
        }

        return monthString + " " + date + ", " + year + " " + hour + ":" + minute + " " + dayOrNight;
    }
    
    self.isDateDST = function(date) {

        //check the dates offset against a date we know is non dst.
        var nonDstOffset = new Date('1/1/2015').getTimezoneOffset();
        var currentOffset = new Date(date).getTimezoneOffset();
        return nonDstOffset > currentOffset;
    };

    self.convertMonthToString = function(month) {
        switch(month) {
            case 0:
                return "Jan";
            case 1:
                return "Feb";
            case 2:
                return "Mar";
            case 3:
                return "Apr";
            case 4:
                return "May";
            case 5:
                return "Jun";
            case 6:
                return "Jul";
            case 7:
                return "Aug";
            case 8:
                return "Sep";
            case 9:
                return "Oct";
            case 10:
                return "Nov";
            case 11:
                return "Dec";
        }
    }

    self.getMemberId = function() {
        if ($.cookie("sua_mid"))
            return $.cookie("sua_mid");

        return 0;
    };

    self.getToken = function() {
        if ($.cookie("sua_at"))
            return $.cookie("sua_at");

        return "";
    };

    self.hideFooter = function() {
        $("#footer").hide();
    };

    self.hideLoading = function() {
        $.mobile.loading("hide");
    };

    self.hideMessageBar = function() {
        var messageBar = $("#message_bar");

        messageBar.text("");
        messageBar.fadeOut("slow");
    };

    self.isSub = function() {
        if ($.cookie("sua_sub"))
            return ($.cookie("sua_sub") === "Y");

        return false;
    };

    self.logout = function(redirectUrl) {
        // Force user to logout.
        window.location.href = "/logout";
    };

    self.redirectLogin = function() {
        $.mobile.changePage("/login");
    };

    self.showConfirm = function(content, okCallback, cancelCallback) {
        var dialog = $("#generic_confirm_dialog");
        var okButton = dialog.find("a.ok");
        var cancelButton = dialog.find("a.cancel");
        var msg = dialog.find("p");

        dialog.trigger("create");
        dialog.popup({ tolerance: "15,5,15,5" });

        okButton.unbind("click").click(function() {
            dialog.popup("close");
            if (typeof okCallback === "function")
                okCallback();
        });

        cancelButton.unbind("click").click(function() {
            dialog.popup("close");
            if (typeof cancelCallback === "function")
                cancelCallback();
        });

        if (typeof content === "string") {
            msg.text(content);
        } else {
            msg.html(content);
        }

        // Hack to disable closing popup when users tap outside.
        dialog.on({
            popupbeforeposition: function() {
                $('.ui-popup-screen').off();
            }
        });

        dialog.removeClass("hidden").show();
        dialog.popup("open");
    };

    self.showFooter = function() {
        $("#footer").show();
    };

    self.showError = function(response, callback) {
        if (response.code === 401) {
            self.logout(response.code);
            self.redirectLogin();
        } else {
            var error = response.error;
            var env = window.env || "production";

            var errorMsg = "Oops! Something seems to have gone wrong.";
            if (env === "production" || env === "preprod") {
                error.message = errorMsg;
            }

            if (error && error.message.length > 0) {
                self.showGenericDialog(error.message);
            }
        }
    };

    self.showGenericDialog = function(content, callback) {
        var dialog = $("#generic_dialog");
        var okBtn = dialog.find("a.ok");
        var msg = dialog.find("p");

        dialog.trigger("create");
        dialog.popup({ tolerance: "15,5,15,5" });

        okBtn.unbind("click").bind("click", function() {
            dialog.popup("close");
            if (typeof callback === "function")
                callback();
        });

        // Setup title and message to display.
        if (typeof content === "string") {
            msg.text(content);
        } else {
            msg.html(content);
        }

        // Hack to disable closing popup when users tap outside.
        dialog.on({
            popupbeforeposition: function() {
                $('.ui-popup-screen').off();
            }
        });

        dialog.removeClass("hidden").show();
        dialog.popup("open");
    };

    self.showLoading = function (msg, options) {
        var defaultOptions = {
            text: msg,
            textVisible: true,
            textonly: true,
            theme:"a"
        };

        $.extend(defaultOptions, options);
        $.mobile.loading("show", defaultOptions);
    };

    self.showMessageBar = function(message, autoHide, callback) {
        var messageBar = $("#message_bar");

        messageBar.text(message);
        messageBar.fadeIn("slow", function() {
            if (autoHide)
                window.setTimeout(self.hideMessageBar, 3000);
            if (typeof callback === "function")
                callback();
        });
    };

    self.loadViewToggleStatus = function() {
        var cookieID = "mos_view_toggle_" + self.getMemberId();

        if ($.cookie(cookieID) != null) {
            return $.cookie(cookieID);
        } else {
            $.cookie(cookieID, "list");
            return "list";
        }
    };

    self.setCookieForViewToggle = function(status) {
        var cookieID = "mos_view_toggle_" + self.getMemberId();
        $.cookie(cookieID, status);
    };

    self.trackAuthStatus = function(mode) {
        var events;
        switch (mode) {
        case "M": // Manual
            events = "event3";
            break;
        case "A": // Auto-Login
            events = "event20";
            break;
        default:
            events = "";
        }
    };

    self.isIAP = function(memberId) {
        var api = spark.api.client.urls.fullProfile;
        var params = {
            targetMemberId: Number(memberId)
        };

        spark.api.client.callAPI(api, params, null, function(success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            if (response.data.isIAPPayingMember === true) {
               
            } else {
                document.forms[0].submit();
            }
        });
    };

    self.iOSversion = function () {
        if (/iP(hone|od|ad)/.test(navigator.platform)) {
            // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
            var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
            return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
        }
    };
    // detect Android
    self.isAndroid = function () {
        var ua = navigator.userAgent.toLowerCase();
        var isAndroid = ua.indexOf("android") > -1;
        return isAndroid;
    };
    self.openInAppPop = function () {
       
        function closePopUp() {
            $(document).unbind('touchmove').bind('touchmove', function(e) {
                return true;
            });

            $("#cm_app_pop").fadeOut(800, function() {
                //set cookie to track this as a refresh so analytics wont get douple pageviews
                $.cookie('refreshGA', '1');
                $.cookie('refreshOmni', '1');
                location.reload();
            });
            $("body").removeClass("no-scroll");
            $.cookie('downloadPopUp', 1);

        }

        function appendSmartBanner() {
            if ($("meta[name='apple-itunes-app']").length > 0) {
                $("meta[name='apple-itunes-app']").attr("content", "app-id=941317572, app-argument=spark-cm:/" + window.location.pathname + "");
                showSmartBanner(false);
            } else {
                $('head').append('<meta name="apple-itunes-app" content="app-id=941317572, app-argument=spark-cm:/' + window.location.pathname + '">');
                showSmartBanner(true);
            }
            

        }

        function showSmartBanner(initialLoad) {
            $.mobile.defaultHomeScroll = 0;

            //smart banner fix for ios 8, not needed on less than 8;
            if (isiOs8()) {
                //if inital load, wait for meta tag to be added to page
                if (initialLoad === true) {
                    $(window).load(function() {
                        window.setTimeout(function() {
                            $("html, body").animate({ scrollTop: -100 }, 'fast');
                        }, 0);
                    });
                } else {
                    //page has already loaded, smart banner is already there, we just need to show it
                    window.setTimeout(function () {
                        $("html, body").animate({ scrollTop: -100 }, 'fast');
                    }, 0);
                }
            }
        }

        function isiOs8() {
            return navigator.userAgent.match(/(iPhone|iPod touch);.*CPU.*OS 8_\d/i);
        }

        var cookieSet = $.cookie("downloadPopUp");
        if ($('#imredirect').length === 0) {
            if (typeof cookieSet === "undefined") {
                
                $("#cm_app_pop").css('display', 'block');
                $("body").addClass('no-scroll');
                $(document).bind('touchmove', function(e) {
                    e.preventDefault();
                });
                if ($(window).height() < 480) {
                    $(".app-pop .phone").css({ "position": "relative" });
                }

            } else {
                appendSmartBanner();
                //showSmartBanner();
            }
        } else {
            $("#cm_app_pop").hide();
            return;
        }




        $("a.download-link").prop("href", "spark-cm://" + window.location.pathname).off().on("click", function (e) {
            e.preventDefault();

            var timeout;
            var appstore = "https://play.google.com/store/apps/details?id=com.spark.christianmingle&hl=en";
            if (spark.utils.common.iOSversion()) {
                appstore = "itms-apps://itunes.apple.com/us/app/christianmingle-christian/id941317572?mt=8";
            }
            function preventPopup() {
                clearTimeout(timeout);
                timeout = null;
                closePopUp();
                window.removeEventListener('pagehide', preventPopup);
            }

            $('<iframe />')
            .attr('src', $(this).prop("href"))
            .attr('style', 'display:none;')
            .appendTo('body');

            timeout = setTimeout(function () {
                document.location = appstore;
                closePopUp();
            }, 15);
            window.addEventListener('pagehide', preventPopup);



        });

        function getPath() {
            if (window.location.pathname === "/activity") {
                return "/activity/incoming/WhoViewedYourProfile";
            } else {
                return window.location.pathname;
            }
        }

        $("a.continue-link").off().on("click", function () {
            closePopUp();

        });

    };

    self.isPayingMember = function (memberId) {
        var api = spark.api.client.urls.fullProfile;
        var params = {
            targetMemberId: Number(memberId)
        };

        spark.api.client.callAPI(api, params, null, function (success, response) {
            if (!success) {
                spark.utils.common.showError(response);
                return;
            }

            if (response.data.isPayingMember) {
                return true;
            };
        });
    };

    self.openNativeApp = function (){
        //this should be an array of platforms and schemas. Currently only iphone
        var mobileAppPlatforms = [['iPhone', 'spark-cm://']];
        //test for the different platforms
        var platformIndex = -1;
         for (var i = 0; i < mobileAppPlatforms.length; i++){
           var userRegex = new RegExp(mobileAppPlatforms[i][0], "g"); 
           var inUserAgent = userRegex.test( navigator.userAgent );
           if(inUserAgent){
              platformIndex = i;
              break;
           }
         }
         if (platformIndex > -1 && getParameterByName('eid')) {
             //build URL
             var deeplink = mobileAppPlatforms[platformIndex][1];
             var path = location.pathname[0] == '/' ? location.pathname.substr(location.pathname.indexOf("/") + 1) : location.pathname;
             //deeplinks on ios match mobile links
             path = path === "activity" ? "activity/incoming/WhoViewedYourProfile" : path;

             deeplink += path;

             var timeout;

             openApp();


         }
         
         function preventPopup() {
             clearTimeout(timeout);
             timeout = null;
             window.removeEventListener('pagehide', preventPopup);
         }

         function openApp() {
             $('<iframe />')
             .attr('src', deeplink)
             .attr('style', 'display:none;')
             .appendTo('body');

             timeout = setTimeout(function () {
                 //cookies to not cause analytics hits on refresh
                 $.cookie('refreshGA', '1');
                 $.cookie('refreshOmni', '1');
                 window.location = removeParameter(window.location.href, 'eid');
             }, 100);
             window.addEventListener('pagehide', preventPopup);
         }

         function removeParameter(url, parameter) {
             var urlparts= url.split('?');

             if (urlparts.length>=2)
             {
                 var urlBase=urlparts.shift(); //get first part, and remove from array
                 var queryString=urlparts.join("?"); //join it back up

                 var prefix = encodeURIComponent(parameter)+'=';
                 var pars = queryString.split(/[&;]/g);
                 for (var i= pars.length; i-->0;)               //reverse iteration as may be destructive
                     if (pars[i].lastIndexOf(prefix, 0)!==-1)   //idiom for string.startsWith
                         pars.splice(i, 1);
                 url = urlBase + '?' + pars.join('&');
                 
                 if (url[url.length - 1] == '?') {    //remove extra question mark
                     url = url.substring(0, url.length - 1);
                 }

             }
             return url;
         }

         function getParameterByName(name) {
             name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
             var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                 results = regex.exec(location.search);
             return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
         }

       


    }


    return self;
})();