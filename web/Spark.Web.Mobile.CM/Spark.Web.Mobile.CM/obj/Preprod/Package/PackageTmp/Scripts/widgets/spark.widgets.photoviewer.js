﻿(function ($) {
    $.widget("mobile.photoviewer", $.mobile.widget, {
        controlVisible: true,
        options: {
            onBeforeHide: function () { },
            onBeforeShow: function () { }
        },
        swiper: null,

        _create: function () {
            var self = this;
            var container = self.element;

            container.bind("click", function (e) {
                var o = e.target;
                if (!$(o).hasClass("photo-viewer"))
                    return;

                container.hide();
            });

            $(window).resize(function () {
                self._resize();
            });

            container.hide();
        },

        _getDimension: function (width, height, maxWidth, maxHeight) {
            var ratio = 1;

            if (width > maxWidth || height > maxHeight) {
                ratio = Math.min(maxWidth / width, maxHeight / height);
            }

            return [(width * ratio), (height * ratio)];
        },

        _hide: function () {
            if (typeof this.options.onBeforeHide === "function")
                this.options.onBeforeHide();

            var container = this.element;
            container.fadeOut(350);
        },

        _resize: function () {
            var self = this;
            var parent = this.element;
            var swiperContainer = parent.find(".swiper-container");
            var prevButton = $("a.prev");
            var nextButton = $("a.next");
            var ul = parent.find("ul.swiper-wrapper");
            var width = $(window).width();
            var height = ($(window).height());

            prevButton.css({
                top: ((height - prevButton.height()) / 2) + "px"
            });

            nextButton.css({
                top: ((height - prevButton.height()) / 2) + "px"
            });

            swiperContainer.css({
                height: height,
                width: width
            });

            ul.children("li").each(function () {
                var e = $(this);
                var img = e.find("img");
                var dimension = self._getDimension(img.attr("data-clientwidth"), img.attr("data-clientheight"), width, height);
                var left = (width - dimension[0]) / 2;
                var top = (height - dimension[1]) / 2;

                e.css({
                    height: height,
                    position: "relative",
                    width: width
                });

                img.css({
                    width: dimension[0] + "px",
                    height: dimension[1] + "px",
                    top: top + "px",
                    left: left + "px"
                });
            });
        },

        setBeforeHideListener: function (callback) {
            this.options.onBeforeHide = callback;
        },

        setBeforeShowListener: function (callback) {
            this.options.onBeforeShow = callback;
        },

        show: function (photos, selectedIndex) {
            var self = this;
            if (typeof self.options.onBeforeShow === "function")
                self.options.onBeforeShow();

            var parent = this.element;
            //var container = $("<div class=\"container\" />");
            var swiperContainer = $("<div class=\"swiper-container\" />");
            var ul = $("<ul class=\"swiper-wrapper\" />");
            //var buttonContainer = $("<div class=\"button-container\" />");
            var prevButton = $("<a id=\"btn_prev_photo\" class=\"prev hidden\" />");
            var nextButton = $("<a id=\"btn_next_photo\" class=\"next\" />");
            //var playButton = $("<button class=\"play\" />");
            var closeButton = $("<a id=\"btn_close_photo\" class=\"close\" />");
            var width = $(window).width();
            var height = $(window).height();

            parent.empty();

            swiperContainer.css({
                height: height,
                width: width
            });

            var length = photos.length;
            $.each(photos, function (index, item) {
                var li = $("<li class=\"swiper-slide\" />");
                var img = $("<img />");
                var div = $("<div />")
                var caption = $("<p class=\"caption\" />");
                var photoCounter = $("<div class=\"counter\"/>");

                li.css({
                    height: height,
                    position: "relative",
                    width: width
                });

                img.bind("load", function (o) {
                    var e = o.currentTarget;
                    var dimension = self._getDimension(e.clientWidth, e.clientHeight, width, height);
                    var left = (width - dimension[0]) / 2;
                    var top = (height - dimension[1]) / 2;

                    $(e).attr("data-clientheight", e.clientHeight);
                    $(e).attr("data-clientwidth", e.clientWidth);
                    $(e).css({
                        width: dimension[0] + "px",
                        height: dimension[1] + "px",
                        top: top + "px",
                        left: left + "px",
                        visibility: "visible"
                    });
                });

                img.attr("id", "photo_" + index);
                img.attr("src", item.imagePath);
                img.css({ visibility: "hidden" });

                var captionText = (item.caption) ? item.caption : "";
                caption.text(captionText);
                photoCounter.text((index + 1) + " of " + length);
       
                caption.appendTo(div);
                photoCounter.appendTo(div);

                img.appendTo(li);
                div.appendTo(li);
                li.appendTo(ul);

            });

            /*ul.off("click").on("click", function (e) {
                var target = $(e.target);

                if (target.is("li") || target.is("img"))
                    self.toggleControl(target);
            });*/
            ul.appendTo(swiperContainer);

            if (Number(selectedIndex) > 0) {
                prevButton.removeClass("hidden");
            }

            prevButton.css({
                top: ((height - prevButton.height()) / 2) + "px"
            });

            if (length === 1 || Number(selectedIndex) === (length - 1)) {
                nextButton.addClass("hidden");
            }

            nextButton.css({
                top: ((height - prevButton.height()) / 2) + "px"
            });

            closeButton.appendTo(parent);
            prevButton.appendTo(parent);
            //playButton.appendTo(parent);
            nextButton.appendTo(parent);
            swiperContainer.appendTo(parent);
            //buttonContainer.appendTo(parent);

            // Initialize swiper
            self.swiper = swiperContainer.swiper({
                //autoplay: 2500,
                //autoplayStopOnLast: true,
                /*cssWidthAndHeight: false,*/
                initialSlide: selectedIndex,
                mode: 'horizontal',
                loop: false,
                onSlideChangeEnd: function (o, direction) {
                    var activeIndex = o.activeIndex;
                    if (direction === "prev") {
                        if (activeIndex === 0)
                            prevButton.addClass("hidden");

                        nextButton.removeClass("hidden");
                    } else {
                        if (activeIndex === (length - 1)) 
                            nextButton.addClass("hidden");

                        prevButton.removeClass("hidden");
                    }
                }
            });

            // Stop auto-play so can manually start it.
            //swiper.stopAutoplay();
            closeButton.bind("click", function () {
                self._hide();
                $("footer").show();
            });

            prevButton.bind("click", function () {
                self.swiper.swipePrev();
            });

            //playButton.bind("click", function () {
            //    swiper.startAutoplay();
            //});

            nextButton.bind("click", function () {
                self.swiper.swipeNext();
            });

            //container.appendTo(parent);
            parent.fadeIn(350);
        },

        toggleControl: function (target) {
            if (this.controlVisible) {
                $("#btn_prev_photo").fadeOut();
                $("#btn_next_photo").fadeOut();
                $("#btn_close_photo").fadeOut();

                target.find("> div").fadeOut();

                this.controlVisible = false;
            } else {
                $("#btn_prev_photo").fadeIn();
                $("#btn_next_photo").fadeIn();
                $("#btn_close_photo").fadeIn();

                target.find("> div").fadeIn();

                this.controlVisible = true;
            }
        }
    });

    $(document).bind("pagecreate create", function (e) {
        $(document).trigger("photoviewerbeforecreate");
        $("div[data-type='photoviewer']").each(function () {
            if (typeof ($(this).data('photoviewer')) === "undefined") {
                $(this).photoviewer();
            }
        });
    });
})(jQuery);