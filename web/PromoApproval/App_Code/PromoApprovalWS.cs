﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Transactions;

/// <summary>
/// Summary description for PromoApproval
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class PromoApprovalWS : System.Web.Services.WebService {

    public PromoApprovalWS () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    /// <summary>
    /// Saves a new promo approval request.
    /// </summary>
    /// <param name="adminEmailAddress"></param>
    /// <param name="brandID"></param>
    /// <param name="adminMemberID"></param>
    /// <param name="brandName"></param>
    /// <param name="promoApprovalStatus">1=Pending, 2=Approved, 3=Rejected. This call should always be pending.</param>
    /// <param name="promoID"></param>
    [WebMethod]
    public void SavePromoApproval(
            int promoID,
            int adminMemberID,
            string adminEmailAddress,
            int brandID,
            string brandName,
            short promoApprovalStatus
        ) 
    {
        try
        {
            if (promoApprovalStatus != (int)PromoApprovalStatus.Pending)
            {
                throw new Exception("Only promo approval status of pending is accepted in this call.");
            }

            // Create a new approval queue for each approver for a given brand.
            PromoApprovalDataClassesDataContext database;
            database = new PromoApprovalDataClassesDataContext(ConfigurationManager.ConnectionStrings["ApprovalConnectionString"].ConnectionString);
            
            List<BrandApprover_ListResult> brandApprovers = database.BrandApprover_List().ToList();
            
            List<BrandApprover_ListResult> filteredBrandApprovers = 
                                            (from BrandApprover_ListResult b in brandApprovers
                                            where b.BrandID == brandID
                                            select b).ToList();
            if (filteredBrandApprovers.Count == 0)
            {
                throw new Exception("No approvers under this brand mapped in the database.");
            }
            
            int existingCount = 
                (from PromoApproval_ListResult p in database.PromoApproval_List(null).ToList()
                 where p.PromoID == promoID select p).ToList().Count;

            if (existingCount > 0)
            {
                System.Diagnostics.EventLog.WriteEntry("PromoApproval", "This promo(" + promoID.ToString()
                + ") already exists.");
                return;
            }

            TransactionOptions options = new TransactionOptions();
            options.Timeout = new TimeSpan(0, 0, 20);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
            {
                foreach (BrandApprover_ListResult brandApprover in filteredBrandApprovers)
                {
                    database.PromoApproval_Save(
                        null,
                        promoID,
                        brandApprover.ApproverMemberID,
                        brandApprover.ApproverEmailAddress,
                        adminMemberID,
                        adminEmailAddress,
                        brandID,
                        brandName,
                        promoApprovalStatus);

                    SendEmailNotification(database, promoID, brandID, brandApprover);
                }

                scope.Complete();
            }
        }
        catch (Exception ex)
        {
            System.Diagnostics.EventLog.WriteEntry("PromoApproval", "PromoID:" + promoID.ToString() +
                ", BrandID:" + brandID.ToString() + ", AdminMemberID:" + 
                adminMemberID.ToString() + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
            throw new Exception("Error in Saving Promo Approval.", ex);
        }
    }

    private void SendEmailNotification(PromoApprovalDataClassesDataContext database, int promoID, int brandID, BrandApprover_ListResult brandApprover)
    {
        try
        {
            if (database == null)
                throw new Exception("Database Context cannot be null.");

            if (brandApprover == null)
                throw new Exception("Brand Approver cannot be null.");

            SparkWS.Messaging.Messaging messagingWS = new SparkWS.Messaging.Messaging();

            SparkWS.Messaging.ExternalMailRequest request = new SparkWS.Messaging.ExternalMailRequest();

            string viewURL = string.Format(ConfigurationSettings.AppSettings["ViewPromoURL"], promoID, brandID);

            // Interal Corp Mail Type
            request.ExternalMailTypeID = 200;
            request.MemberID = Convert.ToInt32(brandApprover.ApproverMemberID);
            request.SiteID = Convert.ToInt32(GetSiteID(brandID, database));

            string[] values = new string[5];
            values[0] = ConfigurationSettings.AppSettings["FromEmailAddress"].ToString();
            values[1] = brandApprover.ApproverEmailAddress;
            values[2] = "Promo" + promoID.ToString() + " is awaiting your approval." + Environment.NewLine +
                        "Click here to see details of the promotion: " + viewURL + Environment.NewLine +
                        "To approve or reject the promo, go here: " + ConfigurationSettings.AppSettings["URL"].ToString();
            values[3] = "Promo Approver";
            values[4] = "Promo Approval Request. " + promoID.ToString();
            request.Values = values;
            request.Version = "1.0.0";

            SparkWS.Messaging.ApiAccessHeader header = new SparkWS.Messaging.ApiAccessHeader();
            header.Key = ConfigurationSettings.AppSettings["SparkWSKey"].ToString();// Super User

            messagingWS.ApiAccessHeaderValue = header;

            try
            {
                messagingWS.SendExternalMail(request);
            }
            catch (Exception ex)
            {
                throw new Exception("Error sending external mail ws request.", ex);
            }
        }
        catch (Exception ex)
        {
            System.Diagnostics.EventLog.WriteEntry("PromoApproval", ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
            throw new Exception("Error in sending email notification.", ex);
        }
    }

    private int? GetSiteID(int brandID, PromoApprovalDataClassesDataContext database)
    {
        try
        {
            int? siteID = null;

            List<BrandSite_ListResult> brandSites = database.BrandSite_List().ToList();

            foreach (BrandSite_ListResult brandSite in brandSites)
            {
                if (brandSite.BrandID == brandID)
                {
                    siteID = brandSite.SiteID;
                    break;
                }
            }

            return siteID;
        }
        catch (Exception ex)
        {
            throw new Exception("Error getting site ID.", ex);
        }
    }
    
}

