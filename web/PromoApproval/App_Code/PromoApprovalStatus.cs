﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public enum PromoApprovalStatus
{
    Pending = 1,
    Approved = 2,
    Rejected = 3,
    Cancelled = 4
}
