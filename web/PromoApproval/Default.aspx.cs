﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Configuration;
using System.Transactions;

public partial class _Default : System.Web.UI.Page 
{
    PromoApprovalDataClassesDataContext database;
    private const string SPARKWS_TICKETKEY = "02F3D63058C14469BA587400A0FDC776";
 
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            database = new PromoApprovalDataClassesDataContext(ConfigurationManager.ConnectionStrings["ApprovalConnectionString"].ConnectionString);
            // This ensures no object caching.
            database.ObjectTrackingEnabled = false;

            if (GetBrandApprover() == null)
            {
                LabelNotification.Text = User.Identity.Name + " is not allowed on this page.";
                return;
            }

            if (!IsPostBack)
            {
                PopulateGridViewPromoApprovals();
            }
        }
        catch (Exception ex)
        {
            PanelError.Visible = true;
            LabelError.Text = "Please report the following error. " + ex.ToString();
            System.Diagnostics.EventLog.WriteEntry("PromoAproval", ex.ToString());
        }
    }

    private void PopulateGridViewPromoApprovals()
    {
        List<PromoApproval_ListResult> promoApprovals =
            database.PromoApproval_List(GetBrandApprover().ApproverMemberID).ToList();

        List<PromoApproval_ListResult> pendingPromoApprovals = (from PromoApproval_ListResult p in promoApprovals
                                                                where (PromoApprovalStatus)p.PromoApprovalStatus ==
                                                                PromoApprovalStatus.Pending
                                                                select p).ToList();

        if (pendingPromoApprovals.Count == 0)
        {
            LabelNotification.Text += "There are no more pending items.";
        }

        GridViewPromoApprovals.DataSource = pendingPromoApprovals;
        GridViewPromoApprovals.DataBind();

    }

    private BrandApprover_ListResult GetBrandApprover()
    {
        List<BrandApprover_ListResult> brandApprovers = database.BrandApprover_List().ToList();
        List<BrandApprover_ListResult> filteredBrandApprovers = (from BrandApprover_ListResult b in brandApprovers
                                                          where b.ApproverDomainAccount == User.Identity.Name.ToLower()
                                                          select b).ToList();
        if (filteredBrandApprovers.Count == 0)
            return null;

        BrandApprover_ListResult filteredBrandApprover = filteredBrandApprovers.First();
        return filteredBrandApprover;
    }

    public void GridViewPromoApprovals_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int promoApprovalID, promoID, approverMemberID, adminMemberID, brandID;
            string approverMemberEmailAddress, adminMemberEmailAddress, brandName;
            DateTime insertDateTime, updateDateTime;
            PromoApprovalStatus promoApprovalStatus;

            promoApprovalID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "PromoApprovalID"));
            promoID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "PromoID"));
            approverMemberID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ApproverMemberID"));
            approverMemberEmailAddress = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ApproverMemberEmailAddress"));
            adminMemberID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "AdminMemberID"));
            adminMemberEmailAddress = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "AdminMemberEmailAddress"));
            brandID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "BrandID"));
            brandName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "BrandName"));
            promoApprovalStatus = (PromoApprovalStatus)Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "PromoApprovalStatus"));
            insertDateTime = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "InsertDateTime"));
            updateDateTime = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "UpdateDateTime"));

            Button buttonApprove = (Button)e.Row.FindControl("ButtonApprove");
            buttonApprove.CommandArgument = promoApprovalID.ToString();

            Button buttonReject = (Button)e.Row.FindControl("ButtonReject");
            buttonReject.CommandArgument = promoApprovalID.ToString();

            HyperLink link = new HyperLink();
            link.NavigateUrl = string.Format(ConfigurationSettings.AppSettings["ViewPromoURL"], promoID, brandID);
            link.Target = "_blank";
            link.Text = promoID.ToString();
            link.ToolTip = "View promo detail";

            e.Row.Cells[2].Controls.Add(link);
            e.Row.Cells[3].Text = approverMemberEmailAddress.ToString() + "(" + approverMemberID.ToString() + ")";
            e.Row.Cells[4].Text = adminMemberEmailAddress.ToString() + "(" + adminMemberID.ToString() + ")";
            e.Row.Cells[5].Text = brandName.ToString() + "(" + brandID.ToString() + ")";
            e.Row.Cells[6].Text = promoApprovalStatus.ToString();
            e.Row.Cells[7].Text = insertDateTime.ToString("MM/dd/yy HH':'mm tt");
            e.Row.Cells[8].Text = updateDateTime.ToString("MM/dd/yy HH':'mm tt");
        }
    }

    public void GridViewPromoApprovals_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "Approve":
                    Approve(Convert.ToInt32(e.CommandArgument));
                    break;

                case "Reject":
                    Reject(Convert.ToInt32(e.CommandArgument));
                    break;
            }

            if (e.CommandName == "ViewPromoDetail")
            {
                LinkButton button = e.CommandSource as LinkButton;
            }
        }
        catch (Exception ex)
        {
            PanelError.Visible = true;
            LabelError.Text = "Please report the following error. " + ex.ToString();
            System.Diagnostics.EventLog.WriteEntry("PromoAproval", ex.ToString());
        }
    }

    private void Approve(int promoApprovalID)
    {
        CompleteApproval(promoApprovalID, PromoApprovalStatus.Approved);
    }

    private void Reject(int promoApprovalID)
    {
        CompleteApproval(promoApprovalID, PromoApprovalStatus.Rejected);
    }

    private void CompleteApproval(int promoApprovalID, PromoApprovalStatus promoApprovalStatus)
    {
        TransactionOptions options = new TransactionOptions();
        options.Timeout = new TimeSpan(0, 0, 60);

        using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
        {
            PromoApproval_ListResult promoApprovalListResult =
                UpdatePromoApprovals(promoApprovalID, GetBrandApprover().ApproverMemberID, promoApprovalStatus);
           
            // If All Items have been approved Make a web service call back to Bedrock to update the status
            if (!HasPending(promoApprovalListResult.PromoID))
            {
                SaveSparkWSPromoApproval(promoApprovalListResult);
                SendEmailNotification(promoApprovalListResult);
                LabelNotification.Text =
                    "Promo(" + promoApprovalListResult.PromoID.ToString() + ") has been updated in production to status, "
                    + ((PromoApprovalStatus)promoApprovalListResult.PromoApprovalStatus).ToString() + System.Environment.NewLine;
            }
            
            scope.Complete();
            scope.Dispose();
        }

        PopulateGridViewPromoApprovals();
        
    }

    /// <summary>
    /// Returns true if there are any pending items given a promo.
    /// </summary>
    /// <returns></returns>
    private bool HasPending(int promoID)
    {
        List<PromoApproval_ListResult> promoApprovals = database.PromoApproval_List(null).ToList();
        
        List<PromoApproval_ListResult> pending =
                        (from PromoApproval_ListResult p in promoApprovals
                         where p.PromoID == promoID
                         && ((PromoApprovalStatus)p.PromoApprovalStatus) == PromoApprovalStatus.Pending
                         select p).ToList();

        if (pending.Count > 0)
        {
            return true;
        }

        return false;
    }


    /// <summary>
    /// If an update is made, other promo approvals are set to cancelled  status.
    /// </summary>
    /// <param name="promoApprovalID"></param>
    /// <param name="approverMemberID"></param>
    /// <param name="promoApprovalStatus"></param>
    /// <returns></returns>
    private PromoApproval_ListResult UpdatePromoApprovals(int promoApprovalID, int approverMemberID, 
                                                        PromoApprovalStatus promoApprovalStatus)
    {
        List<PromoApproval_ListResult> promoApprovals = database.PromoApproval_List(approverMemberID).ToList();

        PromoApproval_ListResult promoApprovalListResult = (from PromoApproval_ListResult p in promoApprovals
                                                            where p.PromoApprovalID == promoApprovalID
                                                            select p).First();

        // Only make an update when there are pending items.
        if (!HasPending(promoApprovalListResult.PromoID))
        {
            return null;
        }

        promoApprovalListResult.PromoApprovalStatus = (short)promoApprovalStatus;

        SavePromoApproval(promoApprovalListResult);

        // Get the latest collection from db.
        promoApprovals = database.PromoApproval_List(null).ToList();

        List<PromoApproval_ListResult> pending =
                (from PromoApproval_ListResult p in promoApprovals
                 where p.PromoID == promoApprovalListResult.PromoID
                 && ((PromoApprovalStatus)p.PromoApprovalStatus) == PromoApprovalStatus.Pending
                 select p).ToList();

        foreach (PromoApproval_ListResult pendingItem in pending)
        {
            pendingItem.PromoApprovalStatus = (short)PromoApprovalStatus.Cancelled;
            SavePromoApproval(pendingItem);
        }

        return promoApprovalListResult;
    }

    private void SavePromoApproval(PromoApproval_ListResult promoApprovalListResult)
    {
        database.PromoApproval_Save(
            promoApprovalListResult.PromoApprovalID,
            promoApprovalListResult.PromoID,
            promoApprovalListResult.ApproverMemberID,
            promoApprovalListResult.ApproverMemberEmailAddress,
            promoApprovalListResult.AdminMemberID,
            promoApprovalListResult.ApproverMemberEmailAddress,
            promoApprovalListResult.BrandID,
            promoApprovalListResult.BrandName,
            promoApprovalListResult.PromoApprovalStatus);

    }

    private void SaveSparkWSPromoApproval(PromoApproval_ListResult promoApprovalListResult)
    {
        SparkWS.Subscription.Subscription subscriptionWS = new SparkWS.Subscription.Subscription();

        subscriptionWS.SavePromoApproval(promoApprovalListResult.PromoID,
            promoApprovalListResult.BrandID,
            promoApprovalListResult.PromoApprovalStatus,
            SPARKWS_TICKETKEY);
    }

    private void SendEmailNotification(PromoApproval_ListResult promoApprovalListResult)
    {
        SparkWS.Messaging.Messaging messagingWS = new SparkWS.Messaging.Messaging();

        SparkWS.Messaging.ExternalMailRequest request = new SparkWS.Messaging.ExternalMailRequest();

        List<BrandApprover_ListResult> brandApprovers = database.BrandApprover_List().ToList();

        List<BrandApprover_ListResult> filteredBrandApprovers =
                                (from BrandApprover_ListResult b in brandApprovers
                                 where b.BrandID == promoApprovalListResult.BrandID
                                 select b).ToList();

        string viewURL = string.Format(ConfigurationSettings.AppSettings["ViewPromoURL"], promoApprovalListResult.PromoID, promoApprovalListResult.BrandID);
        
        string allApprovers = string.Empty;

        foreach (BrandApprover_ListResult brandApprover in brandApprovers)
        {
            allApprovers += " " + brandApprover.ApproverDomainAccount.ToString();
        }

        foreach (BrandApprover_ListResult brandApprover in filteredBrandApprovers)
        {
            // Interal Corp Mail Type
            request.ExternalMailTypeID = 200;
            request.MemberID = Convert.ToInt32(brandApprover.ApproverMemberID);
            request.SiteID = (int)GetSiteID(promoApprovalListResult.BrandID);
            string[] values = new string[5];
            values[0] = @"wlee@spark.net";
            values[1] = brandApprover.ApproverEmailAddress;
            values[2] = "Promo " + promoApprovalListResult.PromoID.ToString() + 
                " has been " + ((PromoApprovalStatus)promoApprovalListResult.PromoApprovalStatus).ToString()  + " by " + allApprovers + ". It is now live in production." +
                Environment.NewLine + viewURL;
            values[3] = "Promo Approver";
            values[4] = "Promo Approved. " + promoApprovalListResult.PromoID.ToString();
            request.Values = values;
            request.Version = "1.0.0";

            SparkWS.Messaging.ApiAccessHeader header = new SparkWS.Messaging.ApiAccessHeader();
            header.Key = "02F3D63058C14469BA587400A0FDC776"; // Super User

            messagingWS.ApiAccessHeaderValue = header;

            messagingWS.SendExternalMail(request);
        }
    }

    private int? GetSiteID(int brandID)
    {
        int? siteID = null;

        List<BrandSite_ListResult> brandSites = database.BrandSite_List().ToList();

        foreach (BrandSite_ListResult brandSite in brandSites)
        {
            if (brandSite.BrandID == brandID)
            {
                siteID = brandSite.SiteID;
                break;
            }
        }

        return siteID;
    }
}
