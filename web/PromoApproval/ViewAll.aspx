﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewAll.aspx.cs" Inherits="ViewAll" MasterPageFile="~/MasterPage.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
    <div class="filterbox">
        Brand Approver:
        <asp:DropDownList runat="server" ID="DropDownListBrandApprover">
        </asp:DropDownList>
        Promo ID:
        <asp:TextBox runat="server" ID="TextBoxPromoID"></asp:TextBox>
        <asp:Button runat="server" ID="ButtonFilter"  
            onclick="ButtonFilter_Click" Text="Filter" />
    </div>
    <asp:GridView ID="GridViewAll" runat="server" AllowPaging="True" 
        PageSize = "10"
        PagerSettings-Mode="NumericFirstLast"
        PagerSettings-FirstPageText="First"
        PagerSettings-LastPageText="Last"
        AllowSorting="True" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" 
        BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical" 
        OnPageIndexChanging="GridViewAll_OnPageIndexChanging"
        OnRowDataBound="GridViewPromoApprovals_OnRowDataBound"
        OnSorting="GridViewAll_Sorting">
        <RowStyle BackColor="#F7F7DE" />
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />        
    </asp:GridView>
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>