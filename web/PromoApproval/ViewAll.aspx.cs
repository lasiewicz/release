﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Reflection;

public partial class ViewAll : System.Web.UI.Page
{
    PromoApprovalDataClassesDataContext database;

    protected void Page_Load(object sender, EventArgs e)
    {
        database = new PromoApprovalDataClassesDataContext(ConfigurationManager.ConnectionStrings["ApprovalConnectionString"].ConnectionString);

        if (!IsPostBack)
        {
            PopulateDropDowns();
            PopulateGridViewAll();
        }
    }

    private void PopulateDropDowns()
    {
        List<BrandApprover_ListResult> brandApprovers = database.BrandApprover_List().ToList();

        DropDownListBrandApprover.DataTextField = "ApproverDomainAccount";
        DropDownListBrandApprover.DataValueField = "ApproverMemberID";

        BrandApprover_ListResult selectAll = new BrandApprover_ListResult();
        selectAll.ApproverMemberID = 0;
        selectAll.ApproverDomainAccount = "All";
        brandApprovers.Insert(0, selectAll);

        var distinctBrandApprovers = (from BrandApprover_ListResult b in brandApprovers
                                      select new { b.ApproverDomainAccount, b.ApproverMemberID }).Distinct();

        DropDownListBrandApprover.DataSource = distinctBrandApprovers;
        DropDownListBrandApprover.DataBind();         
    }

    private List<PromoApproval_ListResult> GetDataSource()
    {
        List<PromoApproval_ListResult> promoApprovals;

        int brandApprover = Convert.ToInt32(DropDownListBrandApprover.SelectedValue.ToString());

        if (brandApprover == 0)
        {
            promoApprovals = database.PromoApproval_List(null).ToList();
        }
        else
        {
            promoApprovals = database.PromoApproval_List(brandApprover).ToList();
        }

        if (TextBoxPromoID.Text != String.Empty)
        {
            promoApprovals =  (from PromoApproval_ListResult p in promoApprovals
                                     where p.PromoID == Convert.ToInt32(TextBoxPromoID.Text) select p).ToList();
        }

        return promoApprovals;
    }

    private void PopulateGridViewAll()
    {
        PopulateGridViewAll(GetDataSource());
    }

    private void PopulateGridViewAll(List<PromoApproval_ListResult> promoApprovals)
    {
        GridViewAll.DataSource = promoApprovals;
        GridViewAll.DataBind();
    }

    protected void GridViewAll_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      GridViewAll.PageIndex = e.NewPageIndex;
      PopulateGridViewAll();
    }

    public void GridViewPromoApprovals_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int promoApprovalID, promoID, approverMemberID, adminMemberID, brandID;
            string approverMemberEmailAddress, adminMemberEmailAddress, brandName;
            DateTime insertDateTime, updateDateTime;
            PromoApprovalStatus promoApprovalStatus;

            promoApprovalID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "PromoApprovalID"));
            promoID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "PromoID"));
            approverMemberID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ApproverMemberID"));
            approverMemberEmailAddress = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ApproverMemberEmailAddress"));
            adminMemberID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "AdminMemberID"));
            adminMemberEmailAddress = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "AdminMemberEmailAddress"));
            brandID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "BrandID"));
            brandName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "BrandName"));
            promoApprovalStatus = (PromoApprovalStatus)Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "PromoApprovalStatus"));
            insertDateTime = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "InsertDateTime"));
            updateDateTime = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "UpdateDateTime"));

            HyperLink link = new HyperLink();
            link.NavigateUrl = string.Format(ConfigurationSettings.AppSettings["ViewPromoURL"], promoID, brandID);
            link.Target = "_blank";
            link.Text = promoID.ToString();
            link.ToolTip = "View promo detail";

            e.Row.Cells[1].Controls.Add(link);
            e.Row.Cells[8].Text = promoApprovalStatus.ToString();
            e.Row.Cells[9].Text = insertDateTime.ToString("MM/dd/yy HH':'mm tt");
            e.Row.Cells[10].Text = updateDateTime.ToString("MM/dd/yy HH':'mm tt");
        }
    }

    protected void GridViewAll_Sorting(object sender, GridViewSortEventArgs e)
    {
        SortDirection sortDirection;

        if (ViewState["_Direction"] != null)
        {
            sortDirection = (SortDirection)ViewState["_Direction"];
            if (sortDirection == SortDirection.Ascending)
                sortDirection = SortDirection.Descending;
            else if (sortDirection == SortDirection.Descending)
                sortDirection = SortDirection.Ascending;
        }
        else
        {
            sortDirection = SortDirection.Descending;
        }

        List<PromoApproval_ListResult> promoApprovals = GetDataSource();
        GridViewAll.DataSource = promoApprovals;

        promoApprovals.Sort(delegate(PromoApproval_ListResult p1, PromoApproval_ListResult p2)
        {
            PropertyInfo propertyInfo1 = p1.GetType().GetProperty(e.SortExpression.ToString());
            PropertyInfo propertyInfo2 = p2.GetType().GetProperty(e.SortExpression.ToString());

            object value1, value2;

            if (sortDirection == SortDirection.Ascending)
            {
                value1 = propertyInfo1.GetValue(p1, null);
                value2 = propertyInfo1.GetValue(p2, null);
            }
            else
            {
                value1 = propertyInfo1.GetValue(p2, null);
                value2 = propertyInfo1.GetValue(p1, null);
            }
            
            switch (propertyInfo1.PropertyType.Name)
            {
                case "String":
                    return Convert.ToString(value1).CompareTo(Convert.ToString(value2));
                case "Int32":
                case "Int16":
                case "Integer":
                    return Convert.ToInt32(value1).CompareTo(Convert.ToInt32(value2));
                case "DateTime":
                    return Convert.ToDateTime(value1).CompareTo(Convert.ToDateTime(value2));
            }

            // shouldn't hit
            return int.MinValue;
        });

        //if (sortDirection == SortDirection.Descending)
        //{
        //    promoApprovals.Sort((PromoApproval_ListResult p1, PromoApproval_ListResult p2)
        //        => p2.PromoApprovalID.CompareTo(p1.PromoApprovalID));
        //}

        ViewState["_Direction"] = sortDirection;

        GridViewAll.DataSource = promoApprovals;
        GridViewAll.DataBind();
    }

    private string ConvertSortDirectionToSql(SortDirection sortDirection)
    {
        switch (sortDirection)
        {
            case SortDirection.Ascending:
                return "ASC";

            case SortDirection.Descending:
                return "DESC";
        }

        return null;
    }

    protected void ButtonFilter_Click(object sender, EventArgs e)
    {
        PopulateGridViewAll();
    }
}
