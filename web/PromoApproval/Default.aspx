﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" EnableEventValidation="true" MasterPageFile="~/MasterPage.master"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

    <asp:Panel ID="PanelError" runat="server" CssClass="panelerror" Visible="false">
        <asp:Label ID="LabelError" runat="server" CssClass="labelerror"></asp:Label>
    </asp:Panel>
    <div><asp:Label ID="LabelInformation" runat="server" Text="" class="labellarge"></asp:Label></div>
    <div><asp:Label ID="LabelNotification" runat="server" Text="" class="labellarge labelnotification"></asp:Label></div>
    
    <asp:GridView ID="GridViewPromoApprovals" runat="server"
            OnRowDataBound="GridViewPromoApprovals_OnRowDataBound"
            OnRowCommand="GridViewPromoApprovals_OnRowCommand"
            AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" 
            BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" 
            GridLines="Vertical">
            <RowStyle BackColor="#F7F7DE" />
            <Columns>
                <asp:TemplateField HeaderText="Approve">
                    <ItemTemplate>
                        <cc1:ConfirmButtonExtender 
                            ID="ConfirmButtonExtenderApprove" 
                            DisplayModalPopupID="ModalPopupExtenderApprove"
                            TargetControlID ="ButtonApprove"
                            runat="server">
                        </cc1:ConfirmButtonExtender>
                        <cc1:ModalPopupExtender
                            ID="ModalPopupExtenderApprove"
                            CancelControlID="ButtonApproveCancel"
                            OkControlID="ButtonApproveOk"
                            TargetControlID="ButtonApprove"
                            PopupControlID="PanelApprove"
                            BackgroundCssClass="modalbackground"
                            runat="server">
                        </cc1:ModalPopupExtender>
                        <asp:Panel ID="PanelApprove" runat="server" CssClass="modal" style="display:none;">
                            <p><img src="./Images/warning.jpg" alt="warning" align="middle" class="imagewarning"/>
                            Are you sure you want to approve?</p>
                            <asp:Button ID="ButtonApproveOk" runat="server" CssClass="buttonok"  Text="OK" />
                            <asp:Button ID="ButtonApproveCancel" runat="server" CssClass="buttoncancel" Text="Cancel" />
                        </asp:Panel>
                        <asp:Button ID="ButtonApprove" CssClass="button buttonapprove" runat="server" 
                        Text="Approve" CommandName="Approve"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Reject">
                    <ItemTemplate>
                        <cc1:ConfirmButtonExtender 
                            ID="ConfirmButtonExtenderReject" 
                            DisplayModalPopupID="ModalPopupExtenderReject"
                            TargetControlID ="ButtonReject"
                            runat="server">
                        </cc1:ConfirmButtonExtender>
                        <cc1:ModalPopupExtender
                            ID="ModalPopupExtenderReject"
                            CancelControlID="ButtonRejectCancel"
                            OkControlID="ButtonRejectOk"
                            TargetControlID="ButtonReject"
                            PopupControlID="PanelReject"
                            BackgroundCssClass="modalbackground"
                            runat="server"></cc1:ModalPopupExtender>
                        <asp:Panel ID="PanelReject" runat="server" CssClass="modal" style="display:none;">
                            <p>
                                <img src="./Images/warning.jpg" alt="warning" align="middle" class="imagewarning"/>
                                Are you sure you want to reject?
                            </p>
                            <asp:Button ID="ButtonRejectOk" runat="server" CssClass="buttonok" Text="OK" />
                            <asp:Button ID="ButtonRejectCancel" runat="server" CssClass="buttoncancel" Text="Cancel" />
                        </asp:Panel> 
                        <asp:Button ID="ButtonReject" CssClass="button buttonreject" runat="server" 
                        Text="Reject" CommandName="Reject"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PromoID"></asp:TemplateField>
                <asp:TemplateField HeaderText="Approver"></asp:TemplateField>
                <asp:TemplateField HeaderText="Submitted By"></asp:TemplateField>
                <asp:TemplateField HeaderText="Brand"></asp:TemplateField>
                <asp:TemplateField HeaderText="Status"></asp:TemplateField>
                <asp:TemplateField HeaderText="Updated"></asp:TemplateField>
                <asp:TemplateField HeaderText="Inserted"></asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    
</ContentTemplate></asp:UpdatePanel>
    
    <asp:UpdateProgress runat="server" ID="UpdateProgressPleaeWait"
     AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="center pleasewait">
                <br /><br />
                <img src="./Images/pleasewait.gif" alt="pleasewait" align="middle"/> 
                <br /><br />
                Please Wait ... 
            </div>                   
        </ProgressTemplate>
    </asp:UpdateProgress>
    
</asp:Content>