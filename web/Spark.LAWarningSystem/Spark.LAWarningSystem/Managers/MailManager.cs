﻿using System.Net;
using System.Net.Mail;

namespace Spark.LAWarningSystem.Managers
{
    public class MailManager
    {
        public static readonly MailManager Singleton = new MailManager();

        private MailManager()
        {
        }

        public void SendMail(string toAddress, string mailMessage, string mailSubject)
        {
            SmtpClient client = new SmtpClient("smtp.matchnet.com");
            client.Port = 587;
            client.EnableSsl = false;
            client.Timeout = 100000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential();
            MailMessage msg = new MailMessage();
            msg.To.Add(toAddress);
            msg.From = new MailAddress("laws.alerts@spark.net");
            msg.Subject = mailSubject;
            msg.Body = mailMessage;
//            Attachment data = new Attachment(contentStream, contentType);
//            msg.Attachments.Add(data);
            client.Send(msg);
        }

    }
}