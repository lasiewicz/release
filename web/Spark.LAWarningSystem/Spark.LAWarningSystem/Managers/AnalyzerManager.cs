﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Caching;
using System.Xml.Serialization;
using com.espertech.esper.client;
using com.espertech.esper.compat.collections;
using log4net;
using Spark.LAWarningSystem.Models;
using Configuration = com.espertech.esper.client.Configuration;
using EPStatement = com.espertech.esper.client.EPStatement;

namespace Spark.LAWarningSystem.Managers
{
    public class AnalyzerManager : IDisposable
    {
        private const string EPSTATEMENTS_CACHE_KEY = "EPStatements";
        private static readonly object _lockObject = new object();
        private static Cache _cache = HttpRuntime.Cache;
        public static readonly AnalyzerManager Singleton = new AnalyzerManager();
        private string _emailAddress;
        private Configuration _config;
        private EPServiceProvider _epService;
        private Thread Init;
        private bool _isRunning = false;
        private static ILog _log = null;

        public static ILog Log
        {
            set { _log = value; }
            get
            {
                if (null == _log)
                {
                    _log = LogManager.GetLogger(typeof(AnalyzerManager));
                }
                return _log;
            }
        }

        private EPRuntime EpRuntime
        {
            get { return _epService.EPRuntime; }
        }

        private AnalyzerManager()
        {
            _emailAddress=ConfigurationManager.AppSettings["laws.email.addess"];
            Initialize();
            _isRunning = true;
            Init = new Thread(new ThreadStart(InitCheck));
            Init.Start();
        }

        public void InitCheck()
        {
            while (_isRunning)
            {
                Thread.Sleep(30000);
                if (null == _cache.Get(EPSTATEMENTS_CACHE_KEY))
                {
                    Initialize();
                }
            }
        }

        public void Initialize()
        {
            try
            {
                lock (_lockObject)
                {
                    //This call internally depend on log4net, 
                    //will throw an error if log4net cannot be loaded 
                    EPServiceProviderManager.PurgeDefaultProvider();
                    if (null == _config)
                    {
                        _config = new Configuration();
                        _config.AddImport<WebRequestEvent>();
                        _config.AddNamespaceImport<WebRequestEvent>();
                        _config.AddEventType<WebRequestEvent>();
                        _config.EngineDefaults.ThreadingConfig.IsListenerDispatchPreserveOrder = false;
                        _config.EngineDefaults.ThreadingConfig.IsInsertIntoDispatchPreserveOrder = false;
                        _config.EngineDefaults.ViewResourcesConfig.IsShareViews = false;
                    }
                    if (null == _epService)
                    {
                        _epService = EPServiceProviderManager.GetDefaultProvider(_config);
                    }
                    _epService.Initialize();

                    EPStatements statements = _cache.Get(EPSTATEMENTS_CACHE_KEY) as EPStatements;
                    if (null == statements)
                    {
                        var xmlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Managers/XML/EPLStatements.xml");
                        TextReader textReader=null;
                        try
                        {
                            textReader = new StreamReader(xmlFile);
                            XmlSerializer deserializer = new XmlSerializer(typeof (EPStatements));
                            statements = deserializer.Deserialize(textReader) as EPStatements;
                        }
                        finally
                        {
                            if (null != textReader)
                            {
                                textReader.Close();
                                textReader.Dispose();
                            }    
                        }
                        
                        _cache.Insert(EPSTATEMENTS_CACHE_KEY, statements, new CacheDependency(xmlFile));
                    }

                    foreach (var epStatement in statements.Statements)
                    {
                        var epExpression = epStatement.EPExpression;
                        var epMessage = epStatement.EPMessage;
                        var epSubject = epStatement.EPSubject;
                        SetupEPLAndEvent(epExpression.Format, 
                            epExpression.GetArgs(), 
                            epMessage.Format,
                            epMessage.GetArgs(), 
                            epSubject.Format, 
                            epSubject.GetArgs());
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e.Message, e);
                throw e;
            }
        }

        private void SetupEPLAndEvent(string expressionFormat, object [] expArgs, string messageFormat, object [] messageArgParams, string subjectFormat, object [] subjectArgParams)
        {
            EPStatement statement = _epService.EPAdministrator.CreateEPL(string.Format(expressionFormat, expArgs));
            statement.Events += delegate(object sender, UpdateEventArgs e)
            {
                if (null != e.NewEvents && e.NewEvents.Length > 0)
                {
                    EventBean evt = e.NewEvents[0];
                    var messageArgs = CreateObjectArrayFromEventBean(evt, messageArgParams);
                    string message = string.Format(messageFormat, messageArgs);
                    Log.Info(message);
                    var subjectArgs = CreateObjectArrayFromEventBean(evt, subjectArgParams);
                    MailManager.Singleton.SendMail(_emailAddress, message, string.Format(subjectFormat, subjectArgs));
                }
            };
        }

        private object[] CreateObjectArrayFromEventBean(EventBean evt, object[] argParams)
        {
            object[] arrayFromEventBean = new object[argParams.Length];
            int i = 0;
            foreach (object argParam in argParams)
            {
                object o = null;
                if (argParam is string)
                {
                    try
                    {
                        o = evt.Get((string) argParam);
                    }
                    catch (Exception ex)
                    {
                        o = ((Dictionary<string, object>) evt.Underlying).Get((string) argParam);
                    }
                }
                else
                {
                    o = argParam;
                }

                if (null != o)
                {
                    arrayFromEventBean[i] = o;
                    i++;
                }
            }
            return arrayFromEventBean;
        }

        public void SendEvent(object evt)
        {
            EpRuntime.SendEvent(evt);
        }

        public void Dispose()
        {
            _isRunning = false;
            _cache.Remove(EPSTATEMENTS_CACHE_KEY);
            _cache = null;
            _epService.Dispose();
            Init.Abort();
        }
    }

    public class WebRequestEvent
    {
        public const string LOG_FORMAT = "OriginIP:{0}, ClientIP:{1}, ServerName:{2}, Url:{3}, Http Status Code:{4}, Http SubStatus Code:{5}, Http Verb:{6}, App ID:{7}, Member ID: {8}, Request Time:{9}ms";

        private string _originIp;
        private string _clientIp;
        private string _serverName;
        private string _url;
        private int _httpStatusCode;
        private int _httpSubStatusCode;
        private string _httpVerb;
        private int _applicationId;
        private int _memberId;
        private int _requestTimeInMillis;

        public string OriginIp
        {
            get { return _originIp; }
            set { _originIp = value; }
        }

        public string ClientIp
        {
            get { return _clientIp; }
            set { _clientIp = value; }
        }

        public string ServerName
        {
            get { return _serverName; }
            set { _serverName = value; }
        }

        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        public int HttpStatusCode
        {
            get { return _httpStatusCode; }
            set { _httpStatusCode = value; }
        }

        public int HttpSubStatusCode
        {
            get { return _httpSubStatusCode; }
            set { _httpSubStatusCode = value; }
        }

        public string HttpVerb
        {
            get { return _httpVerb; }
            set { _httpVerb = value; }
        }

        public int ApplicationId
        {
            get { return _applicationId; }
            set { _applicationId = value; }
        }

        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        public int RequestTimeInMillis
        {
            get { return _requestTimeInMillis; }
            set { _requestTimeInMillis = value; }
        }

        public WebRequestEvent() { }

        public WebRequestEvent(int applicationId, string clientIp, string serverName, int httpStatusCode, int httpSubStatusCode, string httpVerb, int memberId, string originIp, int requestTimeInMillis, string url)
        {
            _applicationId = applicationId;
            _clientIp = clientIp;
            _serverName = serverName;
            _httpStatusCode = httpStatusCode;
            _httpSubStatusCode = httpSubStatusCode;
            _httpVerb = httpVerb;
            _memberId = memberId;
            _originIp = originIp;
            _requestTimeInMillis = requestTimeInMillis;
            _url = url;
        }

        public override string ToString()
        {
            return string.Format(LOG_FORMAT, OriginIp, ClientIp, ServerName, Url, HttpStatusCode, HttpSubStatusCode, HttpVerb, ApplicationId, MemberId, RequestTimeInMillis);
        }
    }
}