﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using com.espertech.esper.client;

namespace Spark.LAWarningSystem.Models
{
    [XmlRoot(DataType = "EPStatments", ElementName = "EPStatments")]
    public class EPStatements
    {
        [XmlElement("EPStatement")]
        public List<EPStatement> Statements { get; set; }
    }

    public class EPStatement
    {
        public EPExpression EPExpression { get; set; }
        public EPMessage EPMessage { get; set; }
        public EPSubject EPSubject { get; set; }
    }

    public class EPExpression
    {
        [XmlAttribute(AttributeName = "format")]
        public string Format { get; set; }
        [XmlElement(ElementName = "ExpressionArg")]
        public List<EPExpressionArg> EpExpressionArgs { get; set; }

        public object[] GetArgs()
        {
            object[] objects = new object[EpExpressionArgs.Count];
            for (int i = 0; i < EpExpressionArgs.Count; i++)
            {
                var arg = EpExpressionArgs[i].ExpressionArg;
                try
                {
                    int num;
                    if (Int32.TryParse(arg.Trim(), out num))
                    {
                        objects[i] = num;
                    }
                    else
                    {
                        objects[i] = arg;
                    }
                }
                catch (Exception e)
                {
                    objects[i] = arg;
                }
            }
            return objects;
        }
    }

    [Serializable]
    public class EPExpressionArg
    {
        [XmlAttribute(AttributeName = "order")]
        public int Order { get; set; }
        [XmlText()]
        public string ExpressionArg { get; set; }
    }

    public class EPMessage
    {
        [XmlAttribute(AttributeName = "format")]
        public string Format { get; set; }
        [XmlElement(ElementName = "MessageArg")]
        public List<EPMessageArg> EpMessageArgs { get; set; }
        public object[] GetArgs()
        {
            object[] objects = new object[EpMessageArgs.Count];
            for (int i = 0; i < EpMessageArgs.Count; i++)
            {
                var arg = EpMessageArgs[i].MessageArg;
                try
                {
                    int num;
                    if (Int32.TryParse(arg.Trim(), out num))
                    {
                        objects[i] = num;
                    }
                    else
                    {
                        objects[i] = arg;
                    }
                }
                catch (Exception e)
                {
                    objects[i] = arg;
                }
            }
            return objects;
        }

    }

    [Serializable]
    public class EPMessageArg
    {
        [XmlAttribute(AttributeName = "order")]
        public int Order { get; set; }
        [XmlText()]
        public string MessageArg { get; set; }
    }

    public class EPSubject
    {
        [XmlAttribute(AttributeName = "format")]
        public string Format { get; set; }
        [XmlElement(ElementName = "SubjectArg")]
        public List<EPSubjectArg> EpSubjectArgs { get; set; }
        public object[] GetArgs()
        {
            object[] objects = new object[EpSubjectArgs.Count];
            for (int i = 0; i < EpSubjectArgs.Count; i++)
            {
                var arg = EpSubjectArgs[i].SubjectArg;
                try
                {
                    int num;
                    if (Int32.TryParse(arg.Trim(), out num))
                    {
                        objects[i] = num;
                    }
                    else
                    {
                        objects[i] = arg;
                    }
                }
                catch (Exception e)
                {
                    objects[i] = arg;
                }
            }
            return objects;
        }

    }

    [Serializable]
    public class EPSubjectArg
    {
        [XmlAttribute(AttributeName = "order")]
        public int Order { get; set; }
        [XmlText()]
        public string SubjectArg { get; set; }
    }

}