﻿using System.Runtime.Serialization;
using System.Web.Http;
using System.Web.Http.Results;
using log4net;
using Spark.LAWarningSystem.Managers;

namespace Spark.LAWarningSystem.Controllers
{
    public class AnalyzerController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AnalyzerController));

        [Route("api/processevent")]
        [HttpPost]
        public JsonResult<WebRequestEvent> ProcessEvent(WebRequestEventRequest webRequest)
        {
            WebRequestEvent webRequestEvent = new WebRequestEvent(webRequest.ApplicationId, webRequest.ClientIp, webRequest.ServerName, webRequest.HttpStatusCode,
                webRequest.HttpSubStatusCode, webRequest.HttpVerb, webRequest.MemberId, webRequest.OriginIp,
                webRequest.RequestTimeInMillis, webRequest.Url);
            Log.Info(webRequest.ToString());
            AnalyzerManager.Singleton.SendEvent(webRequestEvent);
            return Json(webRequestEvent);
        }
    }

    [DataContract(Name = "WebRequestEventRequest")]
    public class WebRequestEventRequest
    {
        public const string LOG_FORMAT = "OriginIP:{0}, ClientIP:{1}, Url:{2}, Http Status Code:{3}, Http SubStatus Code:{4}, Http Verb:{5}, App ID:{6}, Member ID: {7}, Request Time:{8}ms";

        [DataMember(Name = "originIP")]
        public string OriginIp
        { get; set; }

        [DataMember(Name = "clientIP")]
        public string ClientIp
        { get; set; }

        [DataMember(Name = "serverName")]
        public string ServerName
        { get; set; }

        [DataMember(Name = "url")]
        public string Url
        { get; set; }

        [DataMember(Name = "httpStatusCode")]
        public int HttpStatusCode
        { get; set; }

        [DataMember(Name = "httpSubStatusCode")]
        public int HttpSubStatusCode
        { get; set; }

        [DataMember(Name = "httpVerb")]
        public string HttpVerb
        { get; set; }

        [DataMember(Name = "appId")]
        public int ApplicationId
        { get; set; }

        [DataMember(Name = "memberId")]
        public int MemberId
        { get; set; }

        [DataMember(Name = "reqTimeInMillis")]
        public int RequestTimeInMillis
        { get; set; }

        public override string ToString()
        {
            return string.Format(LOG_FORMAT, OriginIp, ClientIp, Url, HttpStatusCode, HttpSubStatusCode, HttpVerb, ApplicationId, MemberId, RequestTimeInMillis);
        }
    }

}
