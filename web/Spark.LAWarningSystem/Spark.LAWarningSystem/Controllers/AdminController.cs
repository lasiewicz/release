﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using log4net;

namespace Spark.LAWarningSystem.Controllers
{
    public class AdminController : ApiController
    {
        private const string MatchnetServerDown = "Matchnet Server Down";
        private const string MatchnetServerEnabled = "Matchnet Server Enabled";
        private static readonly ILog Log = LogManager.GetLogger(typeof(AdminController));

        [Route("admin/healthcheck")]
        [System.Web.Http.HttpGet]
        public string GetHealthCheck()
        {
            var serverStatus = MatchnetServerDown;

            try
            {
                var server = (HttpContext.Current == null) ? null : HttpContext.Current.Server;
                var filePath = Path.GetDirectoryName(server.MapPath("/")) + @"\deploy.txt";
                var fileExists = System.IO.File.Exists(filePath);

                if (!fileExists)
                {
                    serverStatus = MatchnetServerEnabled;
                }
            }
            catch
            {
                serverStatus = MatchnetServerDown;
            }
            finally
            {
                var response = (HttpContext.Current == null) ? null : HttpContext.Current.Response;
                response.AddHeader("Health", serverStatus);
                //                Response.Write(serverStatus);
                //                Response.End();
            }

            return serverStatus;
        }
    }
}