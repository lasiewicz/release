﻿using System;
using System.Collections.Generic;
using System.Threading;
using log4net;
using NUnit.Framework;
using Spark.LAWarningSystem.Managers;

namespace Spark.LAWarningSystem.Tests
{
    [TestFixture]
    public class LoadTests
    {
        public Random random = new Random();

        [TestFixtureSetUp]
        public void Setup()
        {
            AnalyzerManager.Log = new MockLogger();
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            AnalyzerManager.Log = null;
        }

        [Test]
        public void SendWebRequests()
        {            
            Stack<string> logMessages = ((MockLogger)AnalyzerManager.Log).logMessages;
            for (int i = 0; i < 100; i++)
            {
                var webRequestEvent = CreateWebRequestEvent();
                AnalyzerManager.Singleton.SendEvent(webRequestEvent);
                Console.WriteLine("Req Millis at {0} was {1}ms, ClientIp: {2}, Time:{3}", i, webRequestEvent.RequestTimeInMillis, webRequestEvent.ClientIp, DateTime.Now.ToLongTimeString());
                Thread.Sleep(random.Next(1000));
                while (logMessages.Count > 0)
                {
                    Console.WriteLine(logMessages.Pop());
                }

                if (i>29 && i%30 == 0)
                {
                    Console.WriteLine("----------------------------------------------");
                    Console.WriteLine("Thread sleep for 30 secs to clear data window.");
                    Console.WriteLine("----------------------------------------------");
                    Thread.Sleep(30000);
                }
            }
        }

        [Test]
        public void SendThreadedWebRequests()
        {
            Stack<string> logMessages = ((MockLogger)AnalyzerManager.Log).logMessages;
            for (int i = 0; i < 1000; i++)
            {
                FireAndForget(o =>
                {
                    var webRequestEvent = CreateWebRequestEvent();
                    AnalyzerManager.Singleton.SendEvent(webRequestEvent);
                    Console.WriteLine("Req Millis at {0} was {1}ms, ClientIp: {2}, Time:{3}", i, webRequestEvent.RequestTimeInMillis, webRequestEvent.ClientIp, DateTime.Now.ToLongTimeString());
                    Thread.Sleep(random.Next(1000));
                    while (logMessages.Count > 0)
                    {
                        Console.WriteLine(logMessages.Pop());
                    }
                }, "SendThreadedWebRequests","Failed test!");
            }
            Thread.Sleep(300000);
        }

        public static bool FireAndForget(WaitCallback callback, string app, string failureLogMessage)
        {
            return ThreadPool.QueueUserWorkItem(o =>
            {
                try
                {
                    callback(o);
                }
                catch (Exception e)
                {
                    Console.WriteLine(app + ": " + failureLogMessage, e);
                }
            });
        }

        [Test]
        public void SendMail()
        {
            MailManager.Singleton.SendMail("arodriguez@spark.net","Unit test SendMail()","Unit Test Subject SendMail()");
        }

        private WebRequestEvent CreateWebRequestEvent()
        {
            int requestTimeInMillis = random.Next(22);
            WebRequestEvent webRequestEvent = new WebRequestEvent();
            webRequestEvent.ApplicationId = 1003;
            switch (requestTimeInMillis%4)
            {
                case 0:
                    webRequestEvent.OriginIp = "192.168.1.4";
                    webRequestEvent.ClientIp = "64.16.82.20";
                    break;
                case 1:
                    webRequestEvent.OriginIp = "2.55.139.156";
                    webRequestEvent.ClientIp = "64.16.82.20";
                    break;
                case 2:
                    webRequestEvent.OriginIp = "87.68.234.175";
                    webRequestEvent.ClientIp = "64.16.82.22";
                    break;
                case 3:
                    webRequestEvent.OriginIp = "24.234.195.249";
                    webRequestEvent.ClientIp = "64.16.82.23";
                    break;
                default:
                    webRequestEvent.OriginIp = "192.168.1.4";
                    webRequestEvent.ClientIp = "64.16.82.20";
                    break;
            }
            webRequestEvent.HttpStatusCode = 200;
            webRequestEvent.HttpSubStatusCode = 0;
            webRequestEvent.HttpVerb = "GET";
            webRequestEvent.MemberId = 100004579;
            webRequestEvent.RequestTimeInMillis = requestTimeInMillis;
            return webRequestEvent;
        }
    }

    public class MockLogger : ILog
    {

        public Stack<string> logMessages = new Stack<string>();
        #region ILog Members
        private void PushMessage(string item)
        {
            logMessages.Push(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.ff") + " -- " + item);
        }

        public void Debug(object message, Exception exception)
        {
            PushMessage(message.ToString() + ": " + exception.ToString());
        }

        public void Debug(object message)
        {
            PushMessage(message.ToString());
        }


        public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            PushMessage(string.Format(provider, format, args));
        }

        public void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
            PushMessage(string.Format(format, arg0, arg1, arg2));
        }

        public void DebugFormat(string format, object arg0, object arg1)
        {
            PushMessage(string.Format(format, arg0, arg1));
        }

        public void DebugFormat(string format, object arg0)
        {
            PushMessage(string.Format(format, arg0));
        }

        public void DebugFormat(string format, params object[] args)
        {
            PushMessage(string.Format(format, args));
        }

        public void Error(object message, Exception exception)
        {
            PushMessage(message.ToString() + ": " + exception.ToString());
        }

        public void Error(object message)
        {
            PushMessage(message.ToString());
        }

        public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            PushMessage(string.Format(provider, format, args));
        }

        public void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
            PushMessage(string.Format(format, arg0, arg1, arg2));
        }

        public void ErrorFormat(string format, object arg0, object arg1)
        {
            PushMessage(string.Format(format, arg0, arg1));
        }

        public void ErrorFormat(string format, object arg0)
        {
            PushMessage(string.Format(format, arg0));
        }

        public void ErrorFormat(string format, params object[] args)
        {
            PushMessage(string.Format(format, args));
        }

        public void Fatal(object message, Exception exception)
        {
            PushMessage(message.ToString() + ": " + exception.ToString());
        }

        public void Fatal(object message)
        {
            PushMessage(message.ToString());
        }

        public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            PushMessage(string.Format(provider, format, args));
        }

        public void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
            throw new NotImplementedException();
        }

        public void FatalFormat(string format, object arg0, object arg1)
        {
            throw new NotImplementedException();
        }

        public void FatalFormat(string format, object arg0)
        {
            throw new NotImplementedException();
        }

        public void FatalFormat(string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void Info(object message, Exception exception)
        {
            PushMessage(message.ToString() +": "+ exception.ToString());
        }

        public void Info(object message)
        {
            PushMessage(message.ToString());
        }

        public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            PushMessage(string.Format(provider, format, args));
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
            PushMessage(string.Format(format, arg0, arg1, arg2));
        }

        public void InfoFormat(string format, object arg0, object arg1)
        {
            PushMessage(string.Format(format, arg0, arg1));
        }

        public void InfoFormat(string format, object arg0)
        {
            PushMessage(string.Format(format, arg0));
        }

        public void InfoFormat(string format, params object[] args)
        {
            PushMessage(string.Format(format, args));
        }

        public bool IsDebugEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsErrorEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsFatalEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsInfoEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsWarnEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public void Warn(object message, Exception exception)
        {
            throw new NotImplementedException();
        }

        public void Warn(object message)
        {
            throw new NotImplementedException();
        }

        public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
            throw new NotImplementedException();
        }

        public void WarnFormat(string format, object arg0, object arg1)
        {
            throw new NotImplementedException();
        }

        public void WarnFormat(string format, object arg0)
        {
            throw new NotImplementedException();
        }

        public void WarnFormat(string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ILoggerWrapper Members

        public log4net.Core.ILogger Logger
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }

}
