using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Matchnet.PhotoFile.Web
{
	public class NoPhoto : System.Web.UI.Page
	{
		private const String QSPARAM_SITEID = "siteID";
		private const String DEFAULT_SITEID = "101";
		private readonly static Regex isNumericRegex = new Regex(@"^\d+$");

		private void Page_Load(object sender, System.EventArgs e)
		{
			String siteID = DEFAULT_SITEID;

			// The RawURL will be in the format:  /Spark.NoPhoto/NoPhoto.aspx?404;http://dev.americansingles.com:80/fileroot1/2004/06/10/13/30142.jpg?siteID=102
			// Need to parse out QSPARAM_SITEID (siteID).
			String rawURL = Request.RawUrl;

				
			Int32 startIndex = rawURL.IndexOf(";");
			if (startIndex != -1)
			{
				rawURL = rawURL.Substring(startIndex + 1);
			}

			if (rawURL.IndexOf("/alpha/") > -1 || rawURL.IndexOf("/beta/") > -1)
			{
				translatePhotoPath(rawURL);
			}

			startIndex = rawURL.IndexOf(QSPARAM_SITEID);

			if (startIndex != -1)
			{	
				Int32 endIndex = rawURL.IndexOf("&", startIndex);
				startIndex += QSPARAM_SITEID.Length + 1;
	
				if (endIndex == -1)
					siteID = rawURL.Substring(startIndex);
				else
					siteID = rawURL.Substring(startIndex, endIndex);
			}

			Response.ContentType = "image/gif";
			Response.WriteFile(GetNoPhotoImagePath(siteID));		
		}


		private string GetNoPhotoImagePath(String siteID)
		{
			string imgPath = Server.MapPath("NoPhotoImgs/NoPhoto_" + siteID + ".gif");

			if (!File.Exists(imgPath))
			{
				imgPath = Server.MapPath("NoPhotoImgs/NoPhoto_" + DEFAULT_SITEID + ".gif");
			}
  
			return imgPath;
		}


		private static Boolean IsNumeric(String value)
		{
			Match match = isNumericRegex.Match(value);

			return match.Success;
		}


		private void translatePhotoPath(string url)
		{
			Uri uri = new Uri(url);
			string path = uri.AbsolutePath;

			if (path.IndexOf("/Photo") > -1)
			{
				return;
			}

			switch (uri.Host.ToLower())
			{
				case "photo0.matchnet.com":
					Response.Redirect("http://photo0.matchnet.com/Photo" + path.Substring(1));
					break;

				case "photo1.matchnet.com":
					Response.Redirect("http://photo1.matchnet.com/Photo" + path.Substring(1).Replace("alpha", "1000").Replace("beta", "1100"));
					break;

				case "photo2.matchnet.com":
					Response.Redirect("http://photo2.matchnet.com/Photo" + path.Substring(1).Replace("alpha", "2000").Replace("beta", "2100"));
					break;

				case "photo3.matchnet.com":
					Response.Redirect("http://photo3.matchnet.com/Photo" + path.Substring(1).Replace("alpha", "3000").Replace("beta", "3100"));
					break;

				case "photo4.matchnet.com":
					Response.Redirect("http://photo4.matchnet.com/Photo" + path.Substring(1).Replace("alpha", "4000").Replace("beta", "4100"));
					break;

				case "photo5.matchnet.com":
					Response.Redirect("http://photo5.matchnet.com/Photo" + path.Substring(1).Replace("alpha", "5000").Replace("beta", "5100"));
					break;

				case "photo6.matchnet.com":
					Response.Redirect("http://photo6.matchnet.com/Photo" + path.Substring(1).Replace("alpha", "6000").Replace("beta", "6100"));
					break;

				case "photo7.matchnet.com":
					Response.Redirect("http://photo7.matchnet.com/Photo" + path.Substring(1).Replace("alpha", "7000").Replace("beta", "7100"));
					break;

				case "photo8.matchnet.com":
					Response.Redirect("http://photo8.matchnet.com/Photo" + path.Substring(1).Replace("alpha", "8000").Replace("beta", "8100"));
					break;

				case "photo9.matchnet.com":
					Response.Redirect("http://photo9.matchnet.com/Photo" + path.Substring(1).Replace("alpha", "9000").Replace("beta", "9100"));
					break;

				case "photo10.matchnet.com":
					Response.Redirect("http://photo10.matchnet.com/Photo" + path.Substring(1).Replace("alpha", "10000").Replace("beta", "10100"));
					break;

				case "photo11.matchnet.com":
					Response.Redirect("http://photo11.matchnet.com/Photo" + path.Substring(1).Replace("alpha", "11000").Replace("beta", "11100"));
					break;

				case "photo12.matchnet.com":
					Response.Redirect("http://photo12.matchnet.com/Photo" + path.Substring(1).Replace("alpha", "12000").Replace("beta", "12100"));
					break;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
