using System;
using System.IO;
using System.Messaging;
using System.Threading;

using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.File;
using Matchnet.PhotoFile.ServiceDefinitions;
using Matchnet.PhotoFile.ValueObjects;

using Spark.Logging;

namespace Matchnet.PhotoFile.BusinessLogic
{
	internal class QueueManager
	{
		private const Int32 SLEEP_TIME_DEFAULT = 10000;
		private const Int32 SLEEP_TIME_MAX = 180000;

		private bool _runnable = false;
		private Thread _workerThread;
		private MessageQueue _queue;
        private const string CLASS_NAME = "QueueManager";

		public QueueManager(string queuePath)
		{
			_queue = new MessageQueue(queuePath);
			_queue.Formatter = new BinaryMessageFormatter();
			System.Diagnostics.Trace.WriteLine("__QueueManager(" + queuePath + ")");
		}

		public void Start()
		{
			if (_workerThread != null)
			{
				throw new Exception("_workerThread already exists.");
			}

			_workerThread = new Thread(new ThreadStart(workCycle));
			_runnable = true;
			_workerThread.Start();
		}


		public void SignalStop()
		{
			_runnable = false;
			System.Diagnostics.Trace.WriteLine("__signalstop " + _queue.Path);
		}


		public void Stop()
		{
			_runnable = false;
			_workerThread.Join();
			_workerThread = null;
			System.Diagnostics.Trace.WriteLine("__stop " + _queue.Path);
		}


		private void workCycle()
		{
			TimeSpan timeout = new TimeSpan(0, 0 , 2);
			Int32 sleepTime = SLEEP_TIME_DEFAULT;
            string traceMessage = string.Empty;

			while (_runnable)
			{
//				System.Diagnostics.Trace.WriteLine("__workCycle " + _queue.Path + ", " + System.Environment.UserName);
				System.Messaging.Message message = null;
				MessageQueueTransaction tran = new MessageQueueTransaction();
				tran.Begin();

				try
				{
					try
					{
						message = _queue.Receive(timeout, tran);
					}
					catch (MessageQueueException mEx)
					{
						if (mEx.Message != "Timeout for the requested operation has expired.")
						{
							throw mEx;
						}
					}

					if (message != null)
					{
						switch (message.Body.GetType().Name)
						{
							case "CopyPointer":
								CopyPointer copyPointer = message.Body as CopyPointer;
                                traceMessage = string.Format("workCycle() received CopyPointer Src: {0}, Dest: {1}", copyPointer.SrcPath, copyPointer.DestPath);
                                TraceLog(traceMessage);
								try
								{
									if (System.IO.File.Exists(copyPointer.SrcPath))
									{
										FileInfo info = new FileInfo(copyPointer.DestPath);
										Directory.CreateDirectory(info.Directory.ToString());
										System.IO.File.Copy(copyPointer.SrcPath, copyPointer.DestPath, true);
										System.Diagnostics.Trace.WriteLine("__copy: " + copyPointer.SrcPath + " > " + copyPointer.DestPath);
                                        traceMessage = string.Format("workCycle() CopyPointer copy Src: {0}, Dest: {1}", copyPointer.SrcPath, copyPointer.DestPath);
                                        TraceLog(traceMessage);

                                        bool fileExists = System.IO.File.Exists(copyPointer.DestPath);
                                        traceMessage = string.Format("CopyPointer() File Verification Result. File: {0} Exists: {1} Size: {2}", copyPointer.DestPath, fileExists.ToString(), info.Length.ToString());
                                        TraceLog(traceMessage);

									}
                                    else
                                    {
                                        traceMessage = string.Format("workCycle() CopyPointer No SRC found! Src: {0}, Dest: {1}", copyPointer.SrcPath, copyPointer.DestPath);
                                        TraceLog(traceMessage);
                                    }
								}
								catch (Exception ex)
								{
									System.Diagnostics.Trace.WriteLine("Error copying file (src: " + copyPointer.SrcPath + ", dest: " + copyPointer.DestPath + ").");
                                    RollingFileLogger.Instance.LogException(Matchnet.PhotoFile.ValueObjects.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, ex);
                                    throw new Exception("Error copying file (src: " + copyPointer.SrcPath + ", dest: " + copyPointer.DestPath + ").", ex);
								}
								break;

							case "DeletePointer":
								DeletePointer deletePointer = message.Body as DeletePointer;
								try
								{
									if (System.IO.File.Exists(deletePointer.Path))
									{
										System.IO.File.Delete(deletePointer.Path);

                                        traceMessage = string.Format("CopyPointer() File Deleted. File: {0} ", deletePointer.Path);
                                        TraceLog(traceMessage);
										System.Diagnostics.Trace.WriteLine("__delete: " + deletePointer.Path);
									}
									else
									{
                                        traceMessage = string.Format("CopyPointer() File for deletion does not exist. File: {0} ", deletePointer.Path);
                                        TraceLog(traceMessage);
                                        System.Diagnostics.Trace.WriteLine("__delete (not exists): " + deletePointer.Path);
									}
								}
								catch (Exception ex)
								{
									System.Diagnostics.Trace.WriteLine("Error deleting file (path: " + deletePointer.Path + ").");
									throw new Exception("Error deleting file (path: " + deletePointer.Path + ").", ex);
								}
								break;

							default:
								throw new Exception("Message object not recognized: " + message.Body.GetType().Name);
						}

						sleepTime = SLEEP_TIME_DEFAULT;
					}

					tran.Commit();
				}
				catch (Exception ex)
				{
					System.Diagnostics.Trace.WriteLine("__" + ex.ToString());
					new ServiceBoundaryException(Matchnet.PhotoFile.ValueObjects.ServiceConstants.SERVICE_NAME,
						"Error processing queue item.",
						ex);

					try
					{
						if (message != null)
						{
							_queue.Send(message.Body, tran);
							tran.Commit();
						}
					}
					catch (Exception reQueueEx)
					{
						tran.Abort();
						new ServiceBoundaryException(Matchnet.PhotoFile.ValueObjects.ServiceConstants.SERVICE_NAME,
							"Error re-queueing file operation.",
							reQueueEx);

					}

					if (sleepTime < SLEEP_TIME_MAX)
					{
						sleepTime = sleepTime * 2;
					}

					Thread.Sleep(sleepTime);
				}
			}
		}

        private void TraceLog(string message)
        {
            RollingFileLogger.Instance.LogInfoMessage(Matchnet.PhotoFile.ValueObjects.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, message, null);
        }
	}
}
