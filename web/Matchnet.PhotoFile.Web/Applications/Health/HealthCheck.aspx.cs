using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;


namespace Matchnet.PhotoFile.Web.Applications.Health
{
	public class HealthCheck : System.Web.UI.Page
	{
		string MATCHNET_SERVER_DOWN = "Matchnet Server Down";
		string MATCHNET_SERVER_ENABLED = "Matchnet Server Enabled";

		private void Page_Load(object sender, System.EventArgs e)
		{
			string ServerStatus = MATCHNET_SERVER_DOWN;

			try
			{
				if(isEnabled())
				{
					ServerStatus = MATCHNET_SERVER_ENABLED;
				}
			}
			catch (Exception ex)
			{
				ServerStatus = MATCHNET_SERVER_DOWN;
			}
			finally
			{
				Response.AddHeader("Health", ServerStatus);
				Response.Write(ServerStatus);
				Response.End();
			}

		}


		private bool isEnabled()
		{
			string filePath = Path.GetDirectoryName(Server.MapPath("/")) + @"\deploy.txt";

			if (!System.IO.File.Exists(filePath))
			{
				return true;
			}

			return false;
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
