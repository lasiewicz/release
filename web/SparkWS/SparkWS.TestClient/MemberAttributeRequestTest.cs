using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.ObjectModel;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.IO;
using System.Xml;
namespace SparkWS.TestClient
{
    public delegate void LogResults(string file,string url,string resXML,DateTime start,DateTime end);
    public delegate void LogAllResults(string file, int iterations,DateTime start, DateTime end);
    [TestClass()]
    public class MemberAttributeRequestTest
    {
        public  event LogResults EventResults;
        public  event LogAllResults EventAllResults;
        private TestContext testContextInstance;

        public int Iterations = 1;
        public string URL = "";
        public string Logfile = "";
        public bool LogResults;
        private Collection<string> URLColl = null;

        private bool collectionFlag;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        public MemberAttributeRequestTest(Collection<string>url, bool log, string logfile)
        {
            URLColl = url;
            collectionFlag = true;
            if (log && !String.IsNullOrEmpty(logfile))
            {
                ResultsHandler handler = new ResultsHandler();
                EventResults += new LogResults(handler.OnResults);
                EventAllResults += new LogAllResults(handler.OnAllResults);
                Logfile = logfile;
                LogResults = log;
            }
           
        }

        public MemberAttributeRequestTest(int iterations, string url,bool log, string logfile)
        {
            if (iterations > 0)
                Iterations = iterations;
            URL = url;
            collectionFlag = false;
            if (log && !String.IsNullOrEmpty(logfile))
            {
                ResultsHandler handler = new ResultsHandler();
                EventResults += new LogResults(handler.OnResults);
                EventAllResults += new LogAllResults(handler.OnAllResults);
                LogResults = log;
                Logfile = logfile;
            }
        }
        public  void workCycle()
        {
            int numcycles;
            string xml = "";

            if (collectionFlag)
            {
                numcycles = URLColl.Count;
                Iterations = numcycles;
            }
            else
                numcycles = Iterations;
            DateTime testStart = DateTime.Now;
            for (int i = 0; i < numcycles; i++)
            {
                string url = "";
                if (collectionFlag)
                    url = URLColl[i];
                else
                {
                    url = URL;
                   
                }
                DateTime start = DateTime.Now;
                xml = SendHttpRequest(url);
                DateTime end = DateTime.Now;
                if (LogResults)
                    EventResults(Logfile, url, xml, start, end);
            }
            DateTime testEnd = DateTime.Now;
            EventAllResults(Logfile, Iterations, testStart,testEnd);
        }

        [TestMethod()]
        public string SendHttpRequest(string url)
        {
            string xml = "";

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            
                using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
                {
                    using (Stream stream = resp.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        { xml = reader.ReadToEnd(); }
                       
                    }
                }
                return xml;
     
        }
              
    }

    public class MemberInfo
    {
        public int MemberID;
        public int SiteID;
    }

    public class ResultsHandler
    {
        public void OnResults(string file, string url, string resXML, DateTime start, DateTime end)
        {
            try
            {
                bool xmlGood = CheckXML(resXML);
                using (TextWriter writer = new StreamWriter(file, true))
                {
                    TimeSpan t = new TimeSpan(end.Ticks - start.Ticks);
                    writer.NewLine = "\r\n";
                    writer.WriteLine(url);
                    writer.WriteLine(resXML);
                    writer.WriteLine("XML checked:" + xmlGood.ToString());
                    string interval = String.Format("Start:{0:t} - End:{1:t}, Duration: {2} (ms)", start, end, t.TotalMilliseconds);
                    writer.WriteLine(interval);
                    writer.WriteLine("");
                    writer.WriteLine("===============================================================");
                    writer.WriteLine("");
                }
            }
            catch (Exception ex)
            { }
        }

        public void OnAllResults(string file,  int iter,DateTime start, DateTime end)
        {
            try
            {
                using (TextWriter writer = new StreamWriter(file, true))
                {
                    TimeSpan t = new TimeSpan(end.Ticks - start.Ticks);
                    writer.NewLine = "\r\n";
                    writer.WriteLine("");
                    writer.WriteLine("");
                    writer.WriteLine("Test Completed");
                    writer.WriteLine(String.Format("Iterations:{0}",iter));
                    string interval = String.Format("Started:{0:t} - Ended:{1:t}, Duration: {2} (sec)", start, end, t.TotalSeconds);
                    writer.WriteLine(String.Format(interval));
                    writer.WriteLine("");
                    writer.WriteLine("===============================================================");
                    writer.WriteLine("");
                }
            }
            catch (Exception ex)
            { }
        }

        private bool CheckXML(string xml)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}