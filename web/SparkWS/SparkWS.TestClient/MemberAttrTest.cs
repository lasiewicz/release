using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;

namespace SparkWS.TestClient
{
    public partial class MemberAttrTest : Form
    {
        public string FileName = "";
        public string LogPath = "";
        public Collection<string> urls = new Collection<string>();
        public MemberAttrTest()
        {
            InitializeComponent();
        }

     
        private void button1_Click(object sender, EventArgs e)
        {   
            openFileDialog1 = new OpenFileDialog();
            openFileDialog1.ShowDialog();
            FileName = openFileDialog1.FileName;
            txtFileName.Text = FileName;
            using(TextReader reader=new StreamReader(FileName))
            {
                urls.Clear();
                string res=reader.ReadLine();
               while(res !=null)
               {
                   if (!String.IsNullOrEmpty(res))
                   { urls.Add(res); }
                    res = reader.ReadLine();
               }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

           
            int threadNum = Int32.Parse(txtThreadNum.Text);
            for (int i=0;i< threadNum;i++)
            {
                string logfile = txtLogPath.Text + "_thread" + i.ToString() + ".log";
                MemberAttributeRequestTest test = new MemberAttributeRequestTest(urls, chkLog.Checked, logfile);
                ThreadStart threadDelegate = new ThreadStart(test.workCycle);
                Thread t = new Thread(threadDelegate);
                t.Start();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string xml="";
            MemberAttributeRequestTest test = new MemberAttributeRequestTest(urls,chkLog.Checked,"");
            foreach(string url in urls)
                xml+=test.SendHttpRequest(url)+ "\n";

            txtXML.Text = xml;

        }

        private void txtLogPath_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1 = new FolderBrowserDialog();
            folderBrowserDialog1.ShowDialog();
            txtLogPath.Text = folderBrowserDialog1.SelectedPath;
        }

      
        }


    
}