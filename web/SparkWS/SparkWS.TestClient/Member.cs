using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;


using System.Web.Services.Protocols;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.SparkWS;

using SparkWS.TestClient.wsMember;

namespace SparkWS.TestClient
{
    public partial class Member : Form
    {

        public const string TicketKey1 = "02F3D63058C14469BA587400A0FDC776"; // superuser, nonblocked
        public const string TicketKey2 = "238711C489714B22A0AA2635AEAAA9A0"; // Affiliate, nonblocked
        public const string TicketKey3 = "8231322C93C14289B4FADB0443FC0F25"; // Affiliate, blcoked
        public const string TicketKey4 = "D4E36B58E6EB4F25852DC76F90894ECA"; // 3rd party developer e-dologic for premium jdate.co.il israel office
        public const string TicketKey5 = "D66B6588935646BC8C0888CCBB16959B"; // 3rd party developer next-in for cupid / jdate.co.il israel office
        public const string TicketKey6 = "E4738D6AA98047D2B279818BBC54B8B6"; // MingleMatch.
        public const int ProfileSectionID6 = 5; // MingleMatch.
        public const int MEMBER_ID = 100021171;
        public Member()
        {
            InitializeComponent();
        }

        private void btnGetMember_Click(object sender, EventArgs e)
        {
            ApiAccessHeader hdr = new ApiAccessHeader();
		   wsMember.Member member = new wsMember.Member();
            try
            {
                int i=0;
              string emailaddress="";
              string username="";
             int gender;

			hdr.Key = txtKey.Text;
			member.ApiAccessHeaderValue = hdr;
            wsMember.ProfileSectionRequest req = new wsMember.ProfileSectionRequest();
            req.Version = "1.0.0";
            req.MemberID=Int32.TryParse(  txtMemberID.Text, out i)?Int32.Parse(  txtMemberID.Text):MEMBER_ID ;
            req.ProfileSectionID=Int32.TryParse(  txtProfileSectionId.Text,out i)?Int32.Parse(  txtProfileSectionId.Text):ProfileSectionID6;
			req.MemberSessionID="";

            wsMember.ProfileSection ps1 = member.GetMember(req);
            int ilen = ps1.Profile.Values.GetLength(0);

           //Matchnet.Content.ServiceAdapters.AttributeMetadataSA.Instance.GetAttributes().GetAttribute(12).Name;
            for (int k = 0; k < ilen; k++)
            {
                Object val = ps1.Profile.Values[k];
                if (val is wsMember.MemberAttributeOfString)
                {
                    wsMember.MemberAttributeOfString attr = (wsMember.MemberAttributeOfString)val;

                    switch (attr.AttributeID)
                    {
                        case 12:
                            {
                                emailaddress=attr.Value;
                                break;
                            }
                        case 302:
                            {
                                username=attr.Value;
                                break;
                            }
                        default:
                            {
                                break;
                            }

                    }
                    
                }
                else if (val is wsMember.MemberAttributeOfInt32)
                {
                    wsMember.MemberAttributeOfInt32 attr = (wsMember.MemberAttributeOfInt32)val;
                    switch (attr.AttributeID)
                    {
                        case 69:
                            {
                                gender = attr.Value;
                                break;
                            }
                         }
                }

            }

            }
            catch (SoapException ex)
            {
          
                string msg = ex.Message;
                string source = ex.Source;

            }
            finally { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MemberAttrTest f = new MemberAttrTest();
            f.Show();
        }
    }
}