﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

using SparkWS.Test.wsSearch;

namespace SparkWS.Test
{  
    /// <summary>
    ///This is a test class for SearchTest and is intended
    ///to contain all SearchTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SearchTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SearchByAreaCode
        ///</summary>
        [TestMethod()]
        public void SearchByMemberSearchPreferencesTest()
        {
            ApiAccessHeader header = new ApiAccessHeader();
            //header.Key = "02F3D63058C14469BA587400A0FDC776"; // Super User
            header.Key = "e6f401c0638611dbbd130800200c9a66"; // IL

            Search target = new Search();

            target.ApiAccessHeaderValue = header;

            SearchByMemberSearchPreferencesRequest request = new SearchByMemberSearchPreferencesRequest();

            request.MemberID = 16000206;
            request.BrandID = 1003;
            request.OrderBy = "";
            request.PageSize = 10;
            request.StartRow = 1;
            request.Version = "1.0.0";

            SearchByMemberSearchPreferencesResponse response = target.SearchByMemberSearchPreferences(request);

            MiniProfile[] miniProfiles = response.MiniProfiles;

            foreach (MiniProfile miniProfile in miniProfiles)
            {
                System.Diagnostics.Trace.WriteLine("SearchByMemberSearchPreferences UserName:" + miniProfile.UserName);
            }

            Assert.IsTrue(miniProfiles.Length > 1, "SparkWS.Test.wsSearch.SearchByMemberSearchPreferences did not return the expected value.");
        }

        /// <summary>
        ///A test for SearchByAreaCode
        ///</summary>
        [TestMethod()]
        public void SearchByAreaCodeVisitorTest()
        {
            ApiAccessHeader header = new ApiAccessHeader();
            //header.Key = "02F3D63058C14469BA587400A0FDC776"; // Super User
            header.Key = "e6f401c0638611dbbd130800200c9a66"; // IL

            Search target = new Search();

            target.ApiAccessHeaderValue = header;

            SearchByAreaCodeRequest request = new SearchByAreaCodeRequest();

            request.AgeFrom = 18;
            request.AgeTo = 99;
            request.AreaCodes = new string[] { "408", "213" };
            request.BrandID = 1003;
            request.Country = "USA";
            request.Gender = "Male";
            request.OrderBy = "";
            request.PageSize = 10;
            request.SeekingGender = "Female";
            request.StartRow = 1;
            request.Version = "1.0.0";

            SearchByAreaCodeResponse response = target.SearchByAreaCode(request);

            MiniProfile[] miniProfiles = response.MiniProfiles;

            foreach (MiniProfile miniProfile in miniProfiles)
            {
                System.Diagnostics.Trace.WriteLine("SearchByAreaCode UserName:" + miniProfile.UserName);
            }

            Assert.IsTrue(miniProfiles.Length > 1, "SparkWS.Test.wsSearch.SearchByAreaCodeVisitor did not return the expected value.");
        }
    }
}
