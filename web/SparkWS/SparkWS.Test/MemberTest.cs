﻿using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using SparkWS.Test.wsMember;

namespace SparkWS.Test
{


    /// <summary>
    ///This is a test class for MemberTest and is intended
    ///to contain all MemberTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MemberTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod()]
        public void AuthenticateTest()
        {
            ApiAccessHeader header = new ApiAccessHeader();
            //header.Key = "02F3D63058C14469BA587400A0FDC776"; // Super User
            header.Key = "e6f401c0638611dbbd130800200c9a66"; // IL

            Member target = new Member();

            target.ApiAccessHeaderValue = header;

            AuthenticationRequest request = new AuthenticationRequest();

            request.Version = "1.0.0";
            request.EmailAddress = "wlee@spark.net";
            request.Password = "1234";

            AuthenticationResult result = target.Authenticate(request);

            Assert.IsTrue(result.IsAuthenticated == true, "SparkWS.Test.wsMember.Member.Authenticate did not return the expected value.");
        }

        /// <summary>
        ///A test for GetMiniProfile
        ///</summary>
        [TestMethod()]
        public void GetMiniProfileTest()
        {
            ApiAccessHeader header = new ApiAccessHeader();
            //header.Key = "02F3D63058C14469BA587400A0FDC776"; // Super User
            //header.Key = "e6f401c0638611dbbd130800200c9a66"; // IL
            header.Key = "02F3D63058C14469BA587400A0FDC776"; //oren
            Member target = new Member();

            target.ApiAccessHeaderValue = header;

            GetMiniProfileRequest request = new GetMiniProfileRequest();
            request.BrandID = 1003;
            request.Version = "1.0.0";
            // request.MemberID = 16000206;
            request.MemberID = 0;
            request.EncryptedMemberID = "GU3FnvqxeeLz 4wr32CS11vu RrDaPWq";


            GetMiniProfileResponse actual;

            actual = target.GetMiniProfile(request);

            System.Diagnostics.Trace.WriteLine("UserName:" + actual.MiniProfile.UserName);

            // Checking against Const_Null which is default value for Total.
            Assert.IsTrue(actual.MiniProfile.Age > 0, "SparkWS.Test.wsMember.Member.GetMiniProfileTest did not return the expected value.");
            Assert.IsTrue(!string.IsNullOrEmpty(actual.MiniProfile.Email), "SparkWS.Test.wsMember.Member.GetMiniProfileTest did not return the expected value.");
            Assert.IsTrue(System.DateTime.MinValue.CompareTo(actual.MiniProfile.DateOfBirth) != 0, "SparkWS.Test.wsMember.Member.GetMiniProfileTest did not return the expected value.");
        }

        [TestMethod()]
        public void GetMemberUsernameByPropertyTest()
        {
            ApiAccessHeader header = new ApiAccessHeader();
            header.Key = "E4738D6AA98047D2B279818BBC54B8B6"; // mingle on prod and dev

            Member target = new Member(); // Member web service
            target.ApiAccessHeaderValue = header;

            ProfileSectionRequest request = new ProfileSectionRequest();
            request.ProfileSectionID = 5;
            request.Version = "1.0.0";
            request.MemberID = 111688108;
            var sessionIdStringFromCookies = "yFTbv4CEzhKfLmp%2bjM0mCzv47FurA5kCBc3VMhIL0TGtJmLNQ6jOYMfIJVloQGAhmDuw3ZJm1CrRIP6b9QWZUhBEH4B%2fVug%2fhPxpUGYbsc8%3d";
            //request.MemberSessionID = HttpUtility.UrlDecode(sessionIdStringFromCookies);

            ProfileSection result = target.GetMemberUsernameByProperty(request);

            string userName = string.Empty;
            foreach (object memberAttr in result.Profile.Values)
            {
                if (memberAttr is MemberAttributeOfString)
                {
                    MemberAttributeOfString memberAttrStr = (MemberAttributeOfString)memberAttr;
                    if (memberAttrStr.AttributeID == 302)
                        userName = memberAttrStr.Value;
                }
            }

            Assert.IsTrue(userName.ToLower() == "mikecho", "Make sure Mike didn't change his username recently : )");
        }

        [TestMethod()]
        public void GetMembersUsernamesByPropertyTest()
        {
            ApiAccessHeader header = new ApiAccessHeader();
            header.Key = "E4738D6AA98047D2B279818BBC54B8B6"; // Super User

            Member target = new Member(); // Member web service
            target.ApiAccessHeaderValue = header;

            ProfileSectionRequest request1 = new ProfileSectionRequest();
            request1.ProfileSectionID = 5;
            request1.Version = "1.0.0";
            request1.MemberID = 100066873;

            ProfileSectionRequest request2 = new ProfileSectionRequest();
            request2.ProfileSectionID = 5;
            request2.Version = "1.0.0";
            request2.MemberID = 16000206;

            ProfileSectionRequest[] requests = new ProfileSectionRequest[2];
            requests[0] = request1;
            requests[1] = request2;

            ProfileSectionsResult result = target.GetMembersUsernamesByProperty(requests);

            string[] usernames = new string[2];
            int i = 0;

            if (result != null && result.ProfileSections.Length == 2)
            {
                foreach (ProfileSection pSection in result.ProfileSections)
                {
                    foreach (object memberAttr in pSection.Profile.Values)
                    {
                        if (memberAttr is MemberAttributeOfString)
                        {
                            MemberAttributeOfString memberAttrStr = (MemberAttributeOfString)memberAttr;
                            if (memberAttrStr.AttributeID == 302)
                            {
                                usernames[i] = memberAttrStr.Value;
                                i++;
                            }
                        }
                    }
                }
            } 

            Assert.IsTrue(usernames[0].ToLower() == "mikecho", "Make sure Mike didn't change his username recently : )");
            Assert.IsTrue(usernames[1].ToLower() == "pleasework13", "Make sure Won didn't change his username recently : )");
        }

        [TestMethod()]
        public void SubscribeToMessmo()
        {
            ApiAccessHeader header = new ApiAccessHeader();
            header.Key = "348bf4e9d34845719658403258487d3f"; // Messmo key

            Member target = new Member();
            target.ApiAccessHeaderValue = header;
            MessmoSubscribeRequest request = new MessmoSubscribeRequest();
            request.Version = "1.0.0";
            request.ChannelNumber = "53283";
            request.SubscriberId = "1ddf3218-36cb-47dd-bf28-0115b35d1ea9";

            MessmoSubscribeResponse response = null;

            try
            {
                response = target.SubscribeToMessmo(request);
            }
            catch (System.Exception ex)
            {
                string test = ex.Message;
            }

            Assert.IsTrue(response.Status.ToLower() == "success", "SubscribeToMessmo call for Messmo's use failed");
        }

        [TestMethod()]
        public void MessmoSendUserTrans()
        {
            ApiAccessHeader header = new ApiAccessHeader();
            header.Key = "348bf4e9d34845719658403258487d3f"; // Messmo key

            Member target = new Member();
            target.ApiAccessHeaderValue = header;
            MessmoSendUserTransRequest request = new MessmoSendUserTransRequest();
            request.Version = "1.0.0";
            request.ChannelNumber = "53283";
            request.Status = "GetValidUntil";
            request.SubscriberId = "1ddf3218-36cb-47dd-bf28-0115b35d1ea9";

            MessmoSendUserTransResponse response = null;

            try
            {
                response = target.MessmoSendUserTrans(request);
            }
            catch (System.Exception ex)
            {
                string test = ex.Message;
            }

            Assert.IsTrue(response.Status.ToLower() == "success", "MessmoSendUserTrans call for Messmo's use failed");
        }
    }
}
