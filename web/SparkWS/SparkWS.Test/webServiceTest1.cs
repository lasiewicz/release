using System;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows;
using System.Windows.Forms;

using SparkWS.Test.wsMember;
using System.Web.Services.Protocols;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.SparkWS;


namespace SparkWS.Test
{
   /// <summary>
   /// Summary description for webServiceTest1
   /// </summary>
   [TestClass]
   public class webServiceTest1
   {
	  public const string TicketKey1 = "02F3D63058C14469BA587400A0FDC776"; // superuser, nonblocked
	  public const string TicketKey2 = "238711C489714B22A0AA2635AEAAA9A0"; // Affiliate, nonblocked
	  public const string TicketKey3 = "8231322C93C14289B4FADB0443FC0F25"; // Affiliate, blcoked
	  public const string TicketKey4 = "D4E36B58E6EB4F25852DC76F90894ECA"; // 3rd party developer e-dologic for premium jdate.co.il israel office
	  public const string TicketKey5 = "D66B6588935646BC8C0888CCBB16959B"; // 3rd party developer next-in for cupid / jdate.co.il israel office
      public const string TicketKey6 = "E4738D6AA98047D2B279818BBC54B8B6"; // MingleMatch.
      public const int ProfileSectionID6 = 5; // MingleMatch.
       public const int MEMBER_ID = 100021171;
       // You can get the session key by logging in, then viewing the source on any page that contains the header
       // and searching for "Message Boards" and getting the value for SPWSCYPHER.
       public static readonly string MEMBER_SESSIONID = System.Web.HttpUtility.UrlDecode("I78gFJO15I%2f7Oz9ZK3a2uJFdvkYjsKcJtwbEu%2bF3rXNYuIohzUhvZ2vOIhRKYJjSc5WYtaRFIkw%2f8D4TY4RHq4%2foTK73hldw2YPqXPw7GCM%3d");

	  public webServiceTest1()
	  {
		 //
		 // TODO: Add constructor logic here
		 //
	  }

	  #region Additional test attributes
	  //
	  // You can use the following additional attributes as you write your tests:
	  //
	  // Use ClassInitialize to run code before running the first test in the class
	  // [ClassInitialize()]
	  // public static void MyClassInitialize(TestContext testContext) { }
	  //
	  // Use ClassCleanup to run code after all tests in a class have run
	  // [ClassCleanup()]
	  // public static void MyClassCleanup() { }
	  //
	  // Use TestInitialize to run code before running each test 
	  // [TestInitialize()]
	  // public void MyTestInitialize() { }
	  //
	  // Use TestCleanup to run code after each test has run
	  // [TestCleanup()]
	  // public void MyTestCleanup() { }
	  //
	  #endregion


	  [TestMethod]
	  public void tstVariedAccess()
	  {
		 ApiAccessHeader hdr = new ApiAccessHeader();
		 wsMember.Member member = new wsMember.Member();
		 
		 try // this succeeds
		 {
			hdr.Key = TicketKey1;
			member.ApiAccessHeaderValue = hdr;
			string result = member.Ping();
			Trace.WriteLine(result.ToString());
		 }
		 catch (SoapException soex)
		 {
			Console.WriteLine(soex.ToString());
		 }
		 try // this fails. the GetMember requires a better than Anyone roll
		 {
            SparkWS.Test.wsMember.ProfileSectionRequest request = new SparkWS.Test.wsMember.ProfileSectionRequest();

            request.Version = "1.0.0";

            request.MemberID = MEMBER_ID;
            request.ProfileSectionID = 1;

            SparkWS.Test.wsMember.ProfileSection ps1 = member.GetMember(request);
            Trace.WriteLine(ps1.ToString());
		 }
		 catch (SoapException soex)
		 {
			Console.WriteLine(soex.ToString());
		 }

         //try // this succeeds, this time not even re-creating the permission. it's cached somehow.
         //{
         //   hdr.Key = TicketKey3;
         //   member.ApiAccessHeaderValue = hdr;
         //   string result = member.Ping();
         //   Trace.WriteLine(result.ToString());
         //}
         //catch (SoapException soex)
         //{
         //   Console.WriteLine(soex.ToString());
         //}

	  }

	  [TestMethod]
	  public void tstPing()
	  {
		 ApiAccessHeader hdr = new ApiAccessHeader();
		 wsMember.Member member = new wsMember.Member();

		 try
		 {
			hdr.Key = TicketKey1;
			member.ApiAccessHeaderValue = hdr;
			string result = member.Ping();
			Trace.WriteLine(result.ToString());
		 }
		 catch (SoapException soex)
		 {
			Console.WriteLine(soex.ToString());
		 }

		 //try
		 //{
		 //   hdr.Key = TicketKey3;
		 //   member.ApiAccessHeaderValue = hdr;
		 //   string result = member.Ping();
		 //   Trace.WriteLine(result.ToString());
		 //}
		 //catch (SoapException soex)
		 //{
		 //   Console.WriteLine(soex.ToString());
		 //}

		 //try
		 //{
		 //   hdr.Key = TicketKey5;
		 //   member.ApiAccessHeaderValue = hdr;
		 //   for (int i = 0; i < 100; i++)
		 //   {
		 //      string result = member.Ping();
		 //      Trace.WriteLine(i.ToString() + ") " + result);
		 //   }
		 //}
		 //catch (SoapException soex)
		 //{
		 //   Console.WriteLine(soex.ToString());
		 //}
	  }

	  [TestMethod]
	  public void tstGetMember1()
	  {
		 getMember(TicketKey1);
	  }

	  [TestMethod]
	  public void tstGetMember2()
	  {
		 getMember(TicketKey2);
	  }

	  [TestMethod]
	  public void tstGetMember3()
	  {
		 getMember(TicketKey3);
	  }

	  [TestMethod]
	  public void tstGetMember4()
	  {
		 getMember(TicketKey4);
	  }

       [TestMethod]
       public void tstGetMember6()
       {
           getMember(TicketKey6, ProfileSectionID6);
       }

       private void getMember(string TicketToUse)
       {
           getMember(TicketToUse, 1);
       }

	  private void getMember(string TicketToUse, int profileSectionID)
      {
		 try
		 {
			ApiAccessHeader hdr = new ApiAccessHeader();
			hdr.Key = TicketToUse;
			SparkWS.Test.wsMember.ProfileSectionRequest request = new SparkWS.Test.wsMember.ProfileSectionRequest();

			request.MemberID = MEMBER_ID;
             if (MEMBER_SESSIONID != "")
                request.MemberSessionID = MEMBER_SESSIONID;

            request.ProfileSectionID = profileSectionID;

			request.Version = "1.0.0";

			wsMember.Member member = new wsMember.Member();
			member.ApiAccessHeaderValue = hdr;
			
             SparkWS.Test.wsMember.ProfileSection result = member.GetMember(request);

			Trace.WriteLine(result.Profile.ToString());

            Assert.IsTrue(result.Profile.Values.Length > 0);
		 }
		 catch (SoapException soex)
		 {
			Trace.WriteLine(soex.ToString());
		 }
	  }

	  [TestMethod]
	  public void tstGetMembers1() { getMembers(TicketKey1); }

	  private void getMembers(string TicketToUse)
	  {
		 try
		 {
			ApiAccessHeader hdr = new ApiAccessHeader();
			hdr.Key = TicketToUse;
			SparkWS.Test.wsMember.ProfileSectionRequest[] requests = new SparkWS.Test.wsMember.ProfileSectionRequest[3];
			SparkWS.Test.wsMember.ProfileSectionRequest request;
			
			request = new SparkWS.Test.wsMember.ProfileSectionRequest();
			request.Version = "1.0.0";
			request.MemberID = 10518;
			request.ProfileSectionID = 1;
			requests[0] = request;
	
			request = new SparkWS.Test.wsMember.ProfileSectionRequest();
			request.Version = "1.0.0";
			request.MemberID = 100766;
			request.ProfileSectionID = 1;
			requests[1] = request;

			request = new SparkWS.Test.wsMember.ProfileSectionRequest();
			request.Version = "1.0.0";
			request.MemberID = 100767;
			request.ProfileSectionID = 1;
			requests[2] = request;


			wsMember.Member member = new wsMember.Member();
			member.ApiAccessHeaderValue = hdr;
			SparkWS.Test.wsMember.ProfileSectionsResult result = member.GetMembers(requests);
			foreach( SparkWS.Test.wsMember.ProfileSection ps in result.ProfileSections)
			   Trace.WriteLine( (ps == null)? "NULL": ps.MemberID.ToString());

			foreach( string s in result.Errors)
			   Trace.WriteLine( (s == null)? "NULL": s);

		 }
		 catch (SoapException soex)
		 {
			Trace.WriteLine(soex.ToString());
		 }
	  }

	  [TestMethod]
	  public void tstAuthenticate()
	  {
		 try
		 {
			ApiAccessHeader hdr = new ApiAccessHeader();
            hdr.Key = TicketKey1;
			AuthenticationRequest request = new AuthenticationRequest();
			request.Password = "123456";
			request.EmailAddress = "nuri@spark.net";

			wsMember.Member member = new wsMember.Member();
			member.ApiAccessHeaderValue = hdr;
			AuthenticationResult result = member.Authenticate(request);
			Trace.WriteLine(result.ToString());
		 }
		 catch (SoapException soex)
		 {
			Console.WriteLine(soex.ToString());
		 }
	  }

	  [TestMethod]
	  public void tstMembersOnline()
	  {
		 try
		 {
			int result = 0;
			ApiAccessHeader hdr = new ApiAccessHeader();
			hdr.Key = TicketKey1;
			MembersOnlineCountRequest request = new MembersOnlineCountRequest();
			request.CommunityID = 1;
			request.Version = "1.0.0";

			wsMember.Member member = new wsMember.Member();
			member.ApiAccessHeaderValue = hdr;
			result = member.GetMembersOnlineCount(request);
			Trace.WriteLine(result.ToString());

			request.CommunityID = 3;
			result = member.GetMembersOnlineCount(request);
			Trace.WriteLine(result.ToString());


		 }
		 catch (SoapException soex)
		 {
			Console.WriteLine(soex.ToString());
		 }
	  }

	  [TestMethod]
	  public void tstSetExternalVideoURL()
	  {
		 try
		 {
			ApiAccessHeader hdr = new ApiAccessHeader();
			hdr.Key = TicketKey1;
			ExternalVideoUrlRequest request = new ExternalVideoUrlRequest();
			request.SiteID = 15;
			request.MemberID = 100766;
			request.URL = "http://video.google.com/videoplay?docid=262909448508012229";

			wsMember.Member member = new wsMember.Member();
			member.ApiAccessHeaderValue = hdr;
			member.SetExternalVideoURL(request);
			Trace.WriteLine("no exception encountered");
		 }
		 catch (SoapException soex)
		 {
			Console.WriteLine(soex.ToString());
		 }
	  }

	  [TestMethod]
	  public void tstAccessTicketSA()
	  {
		 AccessTicket at;
		 at = AccessTicketSA.Instance.GetAccessTicket(TicketKey1); Console.WriteLine(at.Key + " has ID: " + at.AccessTicketID);
		 at = AccessTicketSA.Instance.GetAccessTicket(TicketKey2); Console.WriteLine(at.Key + " has ID: " + at.AccessTicketID);
		 at = AccessTicketSA.Instance.GetAccessTicket(TicketKey3); Console.WriteLine(at.Key + " has ID: " + at.AccessTicketID);
		 at = AccessTicketSA.Instance.GetAccessTicket(TicketKey4); Console.WriteLine(at.Key + " has ID: " + at.AccessTicketID);
	  }


   }
}
