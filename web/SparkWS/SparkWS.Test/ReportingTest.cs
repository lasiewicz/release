﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using SparkWS.Test.wsReporting;

namespace SparkWS.Test
{
    [TestClass()]
    public class ReportingTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestMethod()]
        public void UpdateReporting()
        {
            ApiAccessHeader header = new ApiAccessHeader();
            header.Key = "348bf4e9d34845719658403258487d3f"; // Messmo key

            Reporting target = new Reporting();
            target.ApiAccessHeaderValue = header;
            MessmoReportingRequest request = new MessmoReportingRequest();
            request.Version = "1.0.0";
            request.ChannelNumber = "53283";
            request.Event = "ACTION";
            request.TimeStamp = "23/09/2009 12:09:34";
            //request.Taggle = "16000206::100066873::1::3::601395916";
            request.Taggle = "100004243::100066873::1::3::659950570";
            request.ActionType = "READ";
            request.SmartMessageTag = "Inbox::pleasework13";

            MessmoReportingResponse response = null;
            try
            {
                response = target.UpdateReporting(request);
            }
            catch (Exception ex)
            {
                string test = ex.Message;
            }

            Assert.IsTrue(response.Status.ToLower() == "ok", "UpdateReporting call for Messmo's use failed");
        }
    }
}
