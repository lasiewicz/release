using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SparkWS.Test.wsMember;

namespace SparkWS.Test
{
    /// <summary>
    /// Summary description for GetMember
    /// </summary>
    [TestClass] 
    public class GetMember
    {
        public GetMember()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void GetMemberData()
        {
            //declaring the service
            wsMember.Member memberService = new wsMember.Member();


            //setting the header key
            wsMember.ApiAccessHeader header = new SparkWS.Test.wsMember.ApiAccessHeader();
            header.Key = "e6f401c0638611dbbd130800200c9a66";
            memberService.ApiAccessHeaderValue = header;

            //prepare the request for a member id
            wsMember.MemberIDRequest memberIDRequest = new MemberIDRequest();
            memberIDRequest.Email = "mhristoforov@spark.net";
            memberIDRequest.Version = "1.0.0";

            //get the member id
            wsMember.MemberIDResponse memberIDResponse = memberService.GetMemberID(memberIDRequest);


            //prepare the profile section request
            wsMember.ProfileSectionRequest profileSectionRequest = new ProfileSectionRequest();
            profileSectionRequest.MemberID = memberIDResponse.MemberID;
            //for cupid = 8 / jdate = 7
            profileSectionRequest.ProfileSectionID = 7;
            profileSectionRequest.Version = "1.0.0";

            wsMember.ProfileSection profileSectionResult = memberService.GetMember(profileSectionRequest);

            memberService.Dispose();
        }
    }
}
