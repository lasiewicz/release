<%@ Application Language="C#"  %>
<%@Import Namespace="System.Web" %>
<%@ Import Namespace="log4net" %>


<script RunAt="server">

    ILog Logger = LogManager.GetLogger(typeof(HttpApplication));
   void Application_Start(object sender, EventArgs e)
   {
      
	  System.Threading.Thread.AllocateNamedDataSlot(WebServiceBase.REMOTE_CLIENT_IP_KEY);
	  System.Threading.Thread.AllocateNamedDataSlot(ApiAccessHeader.TICKET_KEY);

	  string enableMingleNotification = System.Web.Configuration.WebConfigurationManager.AppSettings["EnableMingleNotification"];

      if (enableMingleNotification == "true")
      {
          global::MingleNotificationBL mingleNotification = new MingleNotificationBL(System.Web.Configuration.WebConfigurationManager.AppSettings["MingleNotificationURL"]);
          
          Application["MingleNotificationService"] = mingleNotification;
          mingleNotification.Start();
      }

      log4net.Config.DOMConfigurator.Configure();

   }

   void Application_End(object sender, EventArgs e)
   {
	  System.Threading.Thread.FreeNamedDataSlot(WebServiceBase.REMOTE_CLIENT_IP_KEY);
	  System.Threading.Thread.FreeNamedDataSlot(ApiAccessHeader.TICKET_KEY);
	  try
	  {
		 string enableMingleNotification = System.Web.Configuration.WebConfigurationManager.AppSettings["EnableMingleNotification"];
         if (enableMingleNotification == "true" || Application["MingleNotificationService"] != null)
         {
             MingleNotificationBL mingleNotification = Application["MingleNotificationService"] as MingleNotificationBL;
             Application.Remove("MingleNotificationService");
             mingleNotification.Dispose();
         }
	  }
	  catch (Exception ex)
	  {
		 System.Diagnostics.Debug.WriteLine(ex.ToString());
	  }
   }

   void Application_Error(object sender, EventArgs e)
   {
	  System.Diagnostics.Debug.WriteLine(sender.ToString(), e.ToString());
   }

   void Session_Start(object sender, EventArgs e)
   {
	  // Code that runs when a new session is started
   }

   void Session_End(object sender, EventArgs e)
   {
	  // Code that runs when a session ends. 
	  // Note: The Session_End event is raised only when the sessionstate mode
	  // is set to InProc in the Web.config file. If session mode is set to StateServer 
	  // or SQLServer, the event is not raised.

   }

   void Application_BeginRequest(object sender, EventArgs e)
   {
       string enableLogging = System.Web.Configuration.WebConfigurationManager.AppSettings["EnableLogging"];
       if (enableLogging == "true")
       {
           byte[] inputStream = new byte[HttpContext.Current.Request.ContentLength];
           HttpContext.Current.Request.InputStream.Read(inputStream, 0, inputStream.Length);
           HttpContext.Current.Request.InputStream.Position = 0;
           string requestString = ASCIIEncoding.ASCII.GetString(inputStream);
           Logger.InfoFormat("IP: {0} {1} - {2}", HttpContext.Current.Request.UserHostAddress,
                             HttpContext.Current.Request.UserHostName, requestString);
       }
   }

   void Application_EndRequest(Object sender, EventArgs e)
   {
       PaymentProfileServiceAdapter.CloseProxyInstance();
       PurchaseServiceAdapter.CloseProxyInstance();

       Spark.Common.Adapter.OrderHistoryServiceWebAdapter.CloseProxyInstance();
       Spark.Common.Adapter.AccessServiceWebAdapter.CloseProxyInstance();
       Spark.Common.Adapter.RenewalServiceWebAdapter.CloseProxyInstance();
       Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();
   }
   
       
</script>

