﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Adapter for interfacing with Purchase Service Proxy
/// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
/// 
/// Type: Static; this adapter does not need to be instantiated, but it's CloseProxyInstance() should be called at the 
/// end of the web request (e.g. in Global.asax: Application_EndRequest)
/// </summary>
public class PurchaseServiceAdapter
{
    public static PurchaseServiceClient GetProxyInstance()
    {
        PurchaseServiceClient _Client = null;
        try
        {

            if (HttpContext.Current.Items["PurchaseServiceClient"] != null)
            {
                _Client = (PurchaseServiceClient)HttpContext.Current.Items["PurchaseServiceClient"];

                if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                {
                    _Client = new PurchaseServiceClient();
                    HttpContext.Current.Items["PurchaseServiceClient"] = _Client;
                }
                else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                {
                    _Client.Abort();
                    _Client = new PurchaseServiceClient();
                    HttpContext.Current.Items["PurchaseServiceClient"] = _Client;
                }
            }
            else
            {
                _Client = new PurchaseServiceClient();
                HttpContext.Current.Items["PurchaseServiceClient"] = _Client;
            }


        }
        catch (Exception ex)
        {
            string s = ex.Message;
        }

        return _Client;

    }

    public static void CloseProxyInstance()
    {
        PurchaseServiceClient _Client;
        if (HttpContext.Current.Items["PurchaseServiceClient"] != null)
        {
            _Client = (PurchaseServiceClient)HttpContext.Current.Items["PurchaseServiceClient"];

            try
            {
                _Client.Close();
            }
            catch (Exception ex)
            {
                _Client.Abort();
            }
            finally
            {
                HttpContext.Current.Items.Remove("PurchaseServiceClient");
            }
        }
    }
}
