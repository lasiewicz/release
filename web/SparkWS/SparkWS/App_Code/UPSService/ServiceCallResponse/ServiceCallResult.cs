﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for ServiceCallResult
/// </summary>
public struct ServiceCallResult
{
    private string response;

    public string Response
    {
        get { return response; }
        set { response = value; }
    }
    private string responseCode;

    public string ResponseCode
    {
        get { return responseCode; }
        set { responseCode = value; }
    }
    private string responseMessage;

    public string ResponseMessage
    {
        get { return responseMessage; }
        set { responseMessage = value; }
    }
    private Dictionary<string, object> responseValues;

    public Dictionary<string, object> ResponseValues
    {
        get { return responseValues; }
        set { responseValues = value; }
    }
}

