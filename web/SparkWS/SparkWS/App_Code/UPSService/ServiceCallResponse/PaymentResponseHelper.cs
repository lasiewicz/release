﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;

/// <summary>
/// Summary description for PaymentResponseHelper
/// </summary>
public class PaymentResponseHelper
{
    public static ServiceCallResult LoadResultObject(string data)
    {
        ServiceCallResult result = new ServiceCallResult();
        Dictionary<string, object> values = new Dictionary<string, object>();
        result.ResponseValues = values;

        XmlDocument doc = new XmlDocument();
        doc.InnerXml = data;

        XmlNodeList node = doc.GetElementsByTagName("InternalResponse");

        result.ResponseCode = node[0].FirstChild.InnerText;
        result.ResponseMessage = node[0].FirstChild.NextSibling.InnerText;

        if (result.ResponseCode == "0")
        {
            //node = doc.GetElementsByTagName("ResponseValues");
            //Dictionary<string, string> responsevalues = DesrializeGatewayResponse(node);
            //result.responseValues.Add("responseTime", responsevalues["responseTime"]);
            //result.responseValues.Add("litleTxnId", responsevalues["litleTxnId"]);
            if (doc.GetElementsByTagName("FirstName").Count > 0)
            {
                result.ResponseValues.Add("FirstName", doc.GetElementsByTagName("FirstName")[0].InnerText);
            }
            if (doc.GetElementsByTagName("LastName").Count > 0)
            {
                result.ResponseValues.Add("LastName", doc.GetElementsByTagName("LastName")[0].InnerText);
            }
            if (doc.GetElementsByTagName("PaymentType").Count > 0)
            {
                result.ResponseValues.Add("PaymentType", doc.GetElementsByTagName("PaymentType")[0].InnerText);
            }
            if (doc.GetElementsByTagName("CardType").Count > 0)
            {
                string cardType = doc.GetElementsByTagName("CardType")[0].InnerText;
                if (cardType.ToUpper() == "MASTERCARD")
                {
                    cardType = "MasterCard";
                }
                result.ResponseValues.Add("CardType", cardType);
            }
            if (doc.GetElementsByTagName("CreditCardNumber").Count > 0)
            {
                result.ResponseValues.Add("CreditCardNumber", doc.GetElementsByTagName("CreditCardNumber")[0].InnerText);
            }
            if (doc.GetElementsByTagName("BankAccountNumber").Count > 0)
            {
                result.ResponseValues.Add("BankAccountNumber", doc.GetElementsByTagName("BankAccountNumber")[0].InnerText);
            }
            if (doc.GetElementsByTagName("BillingAgreementID").Count > 0)
            {
                result.ResponseValues.Add("BillingAgreementID", doc.GetElementsByTagName("BillingAgreementID")[0].InnerText);
            }
        }
        result.ResponseValues.Add("xml", data);
        return result;
    }

    private static Dictionary<string, string> DesrializeGatewayResponse(XmlNodeList nodes)
    {

        Dictionary<string, string> list = new Dictionary<string, string>();
        foreach (XmlNode node in nodes[0].ChildNodes)
        {
            if (!list.ContainsKey(node.FirstChild.InnerText))
                list.Add(node.FirstChild.InnerText, node.FirstChild.NextSibling.InnerText);
        }

        return list;
    }
}
