using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Security.Permissions;
using System.Xml.Serialization;
using System.Xml;
using System.Threading;

using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Configuration.ValueObjects;
using System.Reflection;
using Matchnet.List.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Exceptions;

/// <summary>
/// 
/// </summary>
[WebService(Namespace = "http://ExternalMail.SparkWS.com/", Description = "For sending Spark's external mail items")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Messaging : WebServiceBase
{

    public Messaging()
    {
    }

    [WebMethod]
    public string MarkMessageReadPixel(string uid)
    {
        string result = string.Empty;
        try
        {
            result = MessagingBL.MarkMessageReadPixel(uid);
            return result;
        }
        catch (Exception ex)
        {
            WebBoundaryException webEx = new WebBoundaryException("Error in web method", ex);
            return result;
        }

        return result;
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate | ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Subsidiary))]
    public ExternalMailResult SendExternalMail(ExternalMailRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 600);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            ExternalMailResult result = MessagingBL.SendExternalMail(request, key);

            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.ExternalMail.SendExternalMail request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.ExternalMail.SendExternalMail unable to send external mail.", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)( ApiAccessRoll.Developer | ApiAccessRoll.Internal ))]
    public InternalMailResult SendInternalMail(InternalMailRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 600);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            InternalMailResult result = MessagingBL.SendInternalMail(request, key);

            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.ExternalMail.SendInternalMail request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.ExternalMail.SendInternalMail unable to send external mail.", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal))]
    public EcardResult SendInternalEcard(InternalEcardRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 300);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            EcardResult result = MessagingBL.SendInternalEcard(request, key);
            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.Mail.SendInternalEcard request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.Mail.SendInternalEcard unable to send internal ecard.", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal))]
    public EcardResult SendExternalEcard(ExternalEcardRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 300);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            EcardResult result =  MessagingBL.SendExternalEcard(request, key);
            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.Mail.SendInternalEcard request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.Mail.SendInternalEcard unable to send internal ecard.", SoapException.ServerFaultCode, ex);
        }
    }


    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal))]
    public MarkMessageReadResult MarkMessageRead(MarkMessageReadRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 200);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            MarkMessageReadResult result = MessagingBL.MarkMessageRead(request, key);

            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.ExternalMail.MarkMessageRead request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.ExternalMail.MarkMessageRead unable to mark message as read.", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod(Description = "For sending member's internal emails.")]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal))]
    public SendEmailResponse SendEmail(SendEmailRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 600);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            SendEmailResponse result = MessagingBL.SendEmail(request, key);

            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.ExternalMail.SendEmail request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.ExternalMail.SendEmail unable to send email.", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod(Description = "For retrieving member's internal email items.")]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal))]
    public GetEmailResponse GetEmail(GetEmailRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 600);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            GetEmailResponse response = MessagingBL.GetEmail(request, key);

            return response;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.ExternalMail.GetEmail request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.ExternalMail.GetEmail unable to send email.", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate | ApiAccessRoll.Developer | ApiAccessRoll.Internal))]
    public MessmoMessageResponse SendMessmoMessage(MessmoMessageRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 600);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            MessmoMessageResponse result = MessagingBL.SendMessmoMessage(request, key);

            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.ExternalMail.SendMessmoMessage request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.ExternalMail.SendMessmoMessage unable to send email.", SoapException.ServerFaultCode, ex);
        }

    }

    
}

