using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Security.Permissions;
using System.Xml.Serialization;
using System.Xml;
using System.Threading;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Exceptions;
using System.Reflection;

[WebService(Namespace = "http://Member.SparkWS.com/", Description = "Exchanges information about a member in the Spark network websites.")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Member : WebServiceBase
{
    public Member()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)ApiAccessRoll.Anyone)]
    public string Ping()
    {
        try
        {
            ThrottleBL.Instance.Ding(accessHeader.Key, "Ping", 6);
        }
        catch
        {
            throw new SoapException("Ping called too often.", SoapException.ClientFaultCode);
        }

        try
        {
            return accessHeader.Key + ": " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.Member.Ping exception occured while pinging", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal))]
    public MemberIDResponse GetMemberIDByUserNameAndCommunityID(MemberIDRequest request)
    {
        if (request.Version != "1.0.0")
            throw new SoapException("GetMember() [request] version not supported." + request.Version.ToString(), SoapException.ClientFaultCode);

        try
        {
            ThrottleBL.Instance.Ding(accessHeader.Key, "GetMember", 3000);
            //ThrottleBL.Instance.Ding(accessHeader.Key, "GetMember", 600);
        }
        catch
        {
            ServiceUtil.LogEvent("WWW", "GetMember called too often.", System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("GetMember called too often.", SoapException.ClientFaultCode);
        }

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";
        try
        {
            MemberIDResponse result = MemberBL.GetMemberIDByUserNameAndCommunityID(request, key);
            return result;
        }
        catch (SparkWSClientException ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMember request error: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMember request error", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMember unable to get member data: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMember unable to get member data", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal))]
    public MemberIDResponse GetMemberID(MemberIDRequest request)
    {
        if (request.Version != "1.0.0")
            throw new SoapException("GetMember() [request] version not supported." + request.Version.ToString(), SoapException.ClientFaultCode);

        try
        {
            ThrottleBL.Instance.Ding(accessHeader.Key, "GetMember", 3000);
            //ThrottleBL.Instance.Ding(accessHeader.Key, "GetMember", 600);
        }
        catch
        {
            ServiceUtil.LogEvent("WWW", "GetMember called too often.", System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("GetMember called too often.", SoapException.ClientFaultCode);
        }

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";
        try
        {
            MemberIDResponse result = MemberBL.GetMemberID(request, key);
            return result;
        }
        catch (SparkWSClientException ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMember request error: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMember request error", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMember unable to get member data: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMember unable to get member data", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate | ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Subsidiary))]
    public ProfileSection GetMember(ProfileSectionRequest request)
    {
        if (request.Version != "1.0.0")
            throw new SoapException("GetMember() [request] version not supported." + request.Version.ToString(), SoapException.ClientFaultCode);

        try
        {
            ThrottleBL.Instance.Ding(accessHeader.Key, "GetMember", 600);
        }
        catch
        {
            ServiceUtil.LogEvent("WWW", "GetMember called too often.", System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("GetMember called too often.", SoapException.ClientFaultCode);
        }

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";
        try
        {
            ProfileSection result = MemberBL.GetProfileSection(request, key);
            return result;
        }
        catch (SparkWSClientException ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMember request error: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMember request error", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMember unable to get member data: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMember unable to get member data", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate | ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Subsidiary))]
    public ProfileSection GetMemberUsernameByProperty(ProfileSectionRequest request)
    {
        if (request.Version != "1.0.0")
            throw new SoapException("GetMemberUsernameByProperty() [request] version not supported." + request.Version.ToString(), SoapException.ClientFaultCode);

        try
        {
            ThrottleBL.Instance.Ding(accessHeader.Key, "GetMemberUsernameByProperty", 3000);
            //ThrottleBL.Instance.Ding(accessHeader.Key, "GetMemberUsernameByProperty", 600);
        }
        catch
        {
            ServiceUtil.LogEvent("WWW", "GetMemberUsernameByProperty called too often.", System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("GetMemberUsernameByProperty called too often.", SoapException.ClientFaultCode);
        }

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";
        try
        {
            ProfileSection result = MemberBL.GetProfileSectionUsernameByProperty(request, key);
            return result;
        }
        catch (SparkWSClientException ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMemberUsernameByProperty request error: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMemberUsernameByProperty request error", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMemberUsernameByProperty unable to get member data: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMemberUsernameByProperty unable to get member data", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate | ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Subsidiary))]
    public ProfileSectionsResult GetMembers(ProfileSectionRequest[] requests)
    {
        if (requests == null || requests.Length == 0)
            throw new SoapException("GetMembers() requests[] is null or empty.", SoapException.ClientFaultCode);

        for (int i = 0; i < requests.Length; i++)
            if (requests[i].Version != "1.0.0")
                throw new SoapException("GetMembers() [request] version not supported." + requests[i].Version.ToString(), SoapException.ClientFaultCode);

        try
        {
            ThrottleBL.Instance.Ding(accessHeader.Key, "GetMembers", 100);
        }
        catch
        {
            ServiceUtil.LogEvent("WWW", "GetMembers called too often.", System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("GetMembers called too often.", SoapException.ClientFaultCode);
        }

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";
        ProfileSectionsResult result = new ProfileSectionsResult(requests.Length);

        for (int i = 0; i < requests.Length; i++)
        {
            try
            {
                result.ProfileSections[i] = MemberBL.GetProfileSection(requests[i], key);
            }
            catch (SparkWSClientException ex)
            {
                ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMembers request error: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
                result.Errors[i] = "SparkWS.Member.GetMembers request error" + ex.Message;
            }
            catch (Exception ex)
            {
                ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMembers unable to get member data: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
                result.Errors[i] = "SparkWS.Member.GetMembers unable to get member data" + ex.Message;
            }
        }
        return result;

    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate | ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Subsidiary))]
    public ProfileSectionsResult GetMembersUsernamesByProperty(ProfileSectionRequest[] requests)
    {
        if (requests == null || requests.Length == 0)
            throw new SoapException("GetMembersUsernamesByProperty() requests[] is null or empty.", SoapException.ClientFaultCode);

        for (int i = 0; i < requests.Length; i++)
            if (requests[i].Version != "1.0.0")
                throw new SoapException("GetMembersUsernamesByProperty() [request] version not supported." + requests[i].Version.ToString(), SoapException.ClientFaultCode);

        try
        {
            ThrottleBL.Instance.Ding(accessHeader.Key, "GetMembersUsernamesByProperty", 3000);
            //ThrottleBL.Instance.Ding(accessHeader.Key, "GetMembersUsernamesByProperty", 100);
        }
        catch
        {
            ServiceUtil.LogEvent("WWW", "GetMembersUsernamesByProperty called too often.", System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("GetMembersUsernamesByProperty called too often.", SoapException.ClientFaultCode);
        }

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";
        ProfileSectionsResult result = new ProfileSectionsResult(requests.Length);

        for (int i = 0; i < requests.Length; i++)
        {
            try
            {
                result.ProfileSections[i] = MemberBL.GetProfileSectionUsernameByProperty(requests[i], key);
            }
            catch (SparkWSClientException ex)
            {
                ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMembersUsernamesByProperty request error: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
                result.Errors[i] = "SparkWS.Member.GetMembersUsernamesByProperty request error" + ex.Message;
            }
            catch (Exception ex)
            {
                ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMembersUsernamesByProperty unable to get member data: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
                result.Errors[i] = "SparkWS.Member.GetMembersUsernamesByProperty unable to get member data" + ex.Message;
            }
        }
        return result;

    }


    /// <summary>
    /// This method available only to spark networks internal applications. Outside hosted or other API users need to use session 
    /// in a profile section request, because they can't and shouldn't have access or prompt a user for their password.
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Internal | ApiAccessRoll.Developer | ApiAccessRoll.Affiliate))]
    public AuthenticationResult Authenticate(AuthenticationRequest request)
    {
        if (request.Version != "1.0.0")
            throw new SoapException("Authenticate() [request] version not supported." + request.Version.ToString(), SoapException.ClientFaultCode);

        try
        {
            string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

            AuthenticationResult result = (request.BrandId > 0) ? MemberBL.Authenticate(request.EmailAddress, request.Password, request.BrandId, request.Privileges, key)
                                            : MemberBL.Authenticate(request.EmailAddress, request.Password, request.Privileges, key);
            return result;
        }
        catch (Exception ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.Authenticate exception occured while authenticating: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.Authenticate exception occured while authenticating", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Internal | ApiAccessRoll.Developer | ApiAccessRoll.Subsidiary))]
    public int GetMembersOnlineCount(MembersOnlineCountRequest request)
    {
        int result = 0;

        if (request.Version != "1.0.0")
            throw new SoapException("GetMembersOnlineCount() [request] version not supported." + request.Version.ToString(), SoapException.ClientFaultCode);

        try
        {
            ThrottleBL.Instance.Ding(accessHeader.Key, "GetMembersOnlineCount", 3);
        }
        catch
        {
            ServiceUtil.LogEvent("WWW", "GetMembersOnlineCount called too often.", System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("GetMembersOnlineCount called too often.", SoapException.ClientFaultCode);
        }

        try
        {
            result = MembersOnlineBL.GetMembersOnlineCount(request);
        }
        catch (Exception ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMembersOnlineCount exception for communityID " + request.CommunityID.ToString() + " Ex: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMembersOnlineCount exception for communityID " + request.CommunityID.ToString(), SoapException.ServerFaultCode, ex);
        }
        return result;
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Internal | ApiAccessRoll.Developer | ApiAccessRoll.Subsidiary))]
    public void SetExternalVideoURL(ExternalVideoUrlRequest request)
    {
        if (request.Version != "1.0.0")
            throw new SoapException("SetExternalVideoURL() [request] version not supported." + request.Version.ToString(), SoapException.ClientFaultCode);

        try
        {
            ThrottleBL.Instance.Ding(accessHeader.Key, "SetExternalVideoURL", 12);
        }
        catch
        {
            ServiceUtil.LogEvent("WWW", "SetExternalVideoURL called too often.", System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SetExternalVideoURL called too often.", SoapException.ClientFaultCode);
        }
        try
        {
            int accessTicketID = AccessTicketBL.GetAccessTicket(accessHeader.Key).AccessTicketID;
            if (accessTicketID < 1)
                throw new Exception("Invalid access ticket ID");

            MemberBL.SetVideoClipURL(request, accessTicketID);
        }
        catch (Exception ex)
        {
            ServiceUtil.LogEvent("WWW", "SetExternalVideoURL exception: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SetExternalVideoURL exception", SoapException.ClientFaultCode, ex);
        }
    }

    [WebMethod]
    public void GetMemberAttributes(string accessticket, int siteid, int memberid)
    {
        MemberAttributeResult m = null;
        string xml = "";
        try
        {
            xml = MemberBL.GetMemberAttributes(accessticket, siteid, memberid, 0);
            System.Web.HttpContext.Current.Response.Write(xml);
        }
        catch (Exception ex)
        {
            m = new MemberAttributeResult();
            m.MemberID = memberid;
            m.SiteID = siteid;
            System.Web.HttpContext.Current.Response.Write(m.ToXMLString(ReturnCodes.retSystemError, "System Error"));
            WebBoundaryException webex = new WebBoundaryException("Error in web method", ex);

        }

    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Affiliate))]
    public string GetZoozamenData(string accessticket, int siteid, int memberid)
    {
        MemberAttributeResult m = null;
        string xml = "";
        try
        {
            xml = MemberBL.GetZoozamenMemberAttributes(accessticket, siteid, memberid, 0);
            //System.Web.HttpContext.Current.Response.Write(xml);
        }
        catch (Exception ex)
        {
            m = new MemberAttributeResult();
            m.MemberID = memberid;
            m.SiteID = siteid;
            //System.Web.HttpContext.Current.Response.Write(m.ToXMLString(ReturnCodes.retSystemError,"System Error"));
            WebBoundaryException webex = new WebBoundaryException("Error in web method", ex);

            xml = m.ToXMLString(ReturnCodes.retSystemError, "System Error");
        }

        return xml;
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Affiliate))]
    public int UpdateMemberFraudStatus(int memberid, int siteid, int success)
    {
        int returnValue = 0;
        
        MemberAttributeResult m = null;
        try
        {
            returnValue = MemberBL.UpdateMemberFraudStatus(memberid, siteid, success);
        }
        catch (Exception ex)
        {
            m = new MemberAttributeResult();
            m.MemberID = memberid;
            m.SiteID = siteid;
            System.Web.HttpContext.Current.Response.Write(m.ToXMLString(ReturnCodes.retSystemError, "System Error"));
            WebBoundaryException webex = new WebBoundaryException("Error in web method", ex);
        }
        return returnValue;

    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Affiliate))]
    public int AdminSuspendMember(int memberid, int siteid)
    {
        int returnValue = 0;

        MemberAttributeResult m = null;
        try
        {
            returnValue = MemberBL.AdminSuspendMember(memberid, siteid);
        }
        catch (Exception ex)
        {
            m = new MemberAttributeResult();
            m.MemberID = memberid;
            m.SiteID = siteid;
            System.Web.HttpContext.Current.Response.Write(m.ToXMLString(ReturnCodes.retSystemError, "System Error"));
            WebBoundaryException webex = new WebBoundaryException("Error in web method AdminSuspendMember()", ex);
        }
        return returnValue;
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Affiliate))]
    public int AdminSuspendMemberForROLF(int memberid, int siteid, int stepID, string stepDescription)
    {
        int returnValue = 0;

        MemberAttributeResult m = null;
        try
        {
            returnValue = MemberBL.AdminSuspendMemberForROLF(memberid, siteid, stepID, stepDescription);
        }
        catch (Exception ex)
        {
            m = new MemberAttributeResult();
            m.MemberID = memberid;
            m.SiteID = siteid;
            System.Web.HttpContext.Current.Response.Write(m.ToXMLString(ReturnCodes.retSystemError, "System Error"));
            WebBoundaryException webex = new WebBoundaryException("Error in web method AdminSuspendMemberForROLF", ex);
        }
        return returnValue;

    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Affiliate))]
    public int AdminSuspendMemberWithReason(int memberid, int siteid, int adminReasonID)
    {
        int returnValue = 0;

        MemberAttributeResult m = null;
        try
        {
            returnValue = MemberBL.AdminSuspendMemberWithReason(memberid, siteid, adminReasonID);
        }
        catch (Exception ex)
        {
            m = new MemberAttributeResult();
            m.MemberID = memberid;
            m.SiteID = siteid;
            System.Web.HttpContext.Current.Response.Write(m.ToXMLString(ReturnCodes.retSystemError, "System Error"));
            WebBoundaryException webex = new WebBoundaryException("Error in web method AdminSuspendMemberWithReason", ex);
        }
        return returnValue;
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Affiliate))]
    public GetMiniProfileResponse GetMiniProfile(GetMiniProfileRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 600);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            GetMiniProfileResponse response = MemberBL.GetMiniProfile(request, key);

            return response;
        }
        catch (SparkWSClientException ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMiniProfile request error: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMiniProfile request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMiniProfile unable to get miniprofile: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMiniProfile unable to get miniprofile.", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate | ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Subsidiary))]
    public DemographicData GetDemographicData(DemographicDataRequest request)
    {
        if (request.Version != "1.0.0")
            throw new SoapException("GetDemographicData() [request] version not supported." + request.Version.ToString(), SoapException.ClientFaultCode);

        try
        {
            ThrottleBL.Instance.Ding(accessHeader.Key, "GetDemographicData", 600);
        }
        catch
        {
            ServiceUtil.LogEvent("WWW", "GetDemographicData called too often.", System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("GetDemographicData called too often.", SoapException.ClientFaultCode);
        }

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";
        try
        {
            DemographicData result = MemberBL.GetDemographicData(request, key);
            return result;
        }
        catch (SparkWSClientException ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMember request error: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMember request error", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetMember unable to get member data: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetMember unable to get member data", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Internal | ApiAccessRoll.Developer | ApiAccessRoll.Affiliate))]
    public ABScenarioResult GetABScenario(ABScenarioRequest request)
    {

        try
        {
            return MemberBL.GetABScenario(request);
        }
        catch (Exception ex)
        {
            ServiceUtil.LogEvent("WWW", "SparkWS.Member.GetABScenario exception occured while getting AB scenario: " + ex.Message, System.Diagnostics.EventLogEntryType.Warning);
            throw new SoapException("SparkWS.Member.GetABScenario exception occured while getting AB scenario", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate | ApiAccessRoll.Developer | ApiAccessRoll.Internal))]
    public MessmoSubscribeResponse SubscribeToMessmo(MessmoSubscribeRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 600);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            MessmoSubscribeResponse result = MemberBL.SubscribeToMessmo(request, key);

            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.Member.SubscribeToMessmo request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.Member.SubscribeToMessmo unable to update member's Messmo subscription status.", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate | ApiAccessRoll.Developer | ApiAccessRoll.Internal))]
    public MessmoSendUserTransResponse MessmoSendUserTrans(MessmoSendUserTransRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 600);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            MessmoSendUserTransResponse result = MemberBL.MessmoSendUserTrans(request, key);

            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.Member.MessmoSendUserTrans request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.Member.MessmoSendUserTrans unable to send member's subscription end date.", SoapException.ServerFaultCode, ex);
        }
    }
}
