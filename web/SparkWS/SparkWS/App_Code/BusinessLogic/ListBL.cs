﻿using System;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Handles list operations such as finding whehther a member
/// is on someones hotlist, adding a member to a hotlist, checking
/// ignore lists, and more.
/// </summary>
public class ListBL
{
    public ListBL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static ListSaveResult AddListMember(AddListMemberRequest request, string ticketKey)
    {
        //checks the access ticket reputability
        AccessTicket ticket = AccessTicketBL.GetAccessTicket(ticketKey);
        if (!isInternalUser(ticket.AccessRoll))
        {
            throw new Exception("This method is not available for general use.");
        }

        //check if the user is not on the ignore list
        ListSaveResult listSaveResult = ListSA.Instance.AddListMember((HotListCategory)request.HotListCategoryID, request.CommunityID, request.SiteID,
            request.MemberID, request.TargetMemberID, request.Comment, Matchnet.Constants.NULL_INT, false, false);

        return listSaveResult;
    }

    private static bool isInternalUser(ApiAccessRoll roll)
    {
        return ((roll & (ApiAccessRoll.Internal | ApiAccessRoll.Developer | ApiAccessRoll.SuperUser)) > 0);
    }
}
