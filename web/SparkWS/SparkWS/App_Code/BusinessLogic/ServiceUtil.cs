﻿using System;
using System.Reflection;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Caching;
using System.Collections;
using System.Web.Services.Protocols;
using System.Diagnostics;
/// <summary>
/// Gives as some methods to validate web service calls
/// </summary>
public static class ServiceUtil
{

    /// <summary>
    /// There are times where we only care about getting the right communityId but we need to pass in brand for it to execute correctly.
    /// Use this method only if you don't care about getting the right brand.
    /// </summary>
    /// <param name="communityId"></param>
    /// <returns></returns>
    internal static Matchnet.Content.ValueObjects.BrandConfig.Brand GetAnyBrandOfCommunity(int communityId)
    {
        Matchnet.Content.ValueObjects.BrandConfig.Brands brands = BrandConfigSA.Instance.GetBrandsByCommunity(communityId);
        Matchnet.Content.ValueObjects.BrandConfig.Brand theBrand = null;

        foreach (Matchnet.Content.ValueObjects.BrandConfig.Brand brand in brands)
        {
            theBrand = brand;
            break;
        }

        return theBrand;
    }

     /// <summary>
    /// checks whether a particular method is of the correct version
    /// </summary>
    /// <param name="currentRequestVersion">the current version of the method</param>
    /// <param name="versionedVO">the versionedVO inherited object that has a version assigned</param>
    internal static void ValidateVersion(MethodBase methodInfo, string currentRequestVersion, VersionedVO versionedVO)
    {
        if (!versionedVO.Version.Equals(currentRequestVersion))
        {
            throw new SoapException(methodInfo.DeclaringType.Name + "." + methodInfo.Name + " [request] version not supported." + versionedVO.Version.ToString(), SoapException.ClientFaultCode);
        }
    }

    /// <summary>
    /// checks whehther a particular key is exceeding the amount of time
    /// a method can be called
    /// </summary>
    /// <param name="methodInfo">the method info 
    /// (get by using System.Reflection.MethodInfo.GetCurrentMethodInfo()</param>
    /// <param name="key">the key that is trying to call the method</param>
    /// <param name="treshold">how many times can this exist</param>
    internal static void CheckTrottle(MethodBase methodInfo, string key, int treshold)
    {
        try
        {
            ThrottleBL.Instance.Ding(key, methodInfo.DeclaringType.Name + "." + methodInfo.Name, treshold);
        }
        catch
        {
            throw new SoapException(methodInfo.DeclaringType.Name + "." + methodInfo.Name + " called too often.", SoapException.ClientFaultCode);
        }
    }

    internal static Matchnet.Content.ValueObjects.BrandConfig.Site GetSite(int siteID)
    {
          Matchnet.Content.ValueObjects.BrandConfig.Site site=null;
         try
         {
            Sites sites = getSites();
            if (sites != null)
            { site = sites[siteID]; }
            return site;
          }
      
         catch(Exception ex)
          { throw (ex); }

     }

  
    internal static string GetAttributeName(int id)
    {
       string res="";
        try
        {
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attr = null;
            attr = GetAttribute(id);
            if (attr != null)
                res = attr.Name;

            return res;
        }

        catch (Exception ex)
        { throw (ex); }

    }

    internal static Matchnet.Content.ValueObjects.AttributeMetadata.Attribute GetAttribute(int id)
    {
        Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attr=null;
        try
        {
            Matchnet.Content.ValueObjects.AttributeMetadata.Attributes attrcoll = getAttributesMetadata();
            if (attrcoll != null)
            {
                 attr = attrcoll.GetAttribute(id);
            }
            return attr;
        }

        catch (Exception ex)
        { throw (ex); }

    }

    internal static bool ValidMessmoChannelNumber(string channelNumber)
    {
        string channelNumberFromDB = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MESSMO_CHANNEL_NUMBER");

        return (channelNumber == channelNumberFromDB);
    }

    private static Sites getSites()
    {
        Sites sites = null;
         try{
                    sites=(Sites)Cache.Instance.Get( Sites.SPARKWS_SITES_CACHE_KEY);
                    Matchnet.Content.ValueObjects.BrandConfig.Sites brandsites = null; 
                    if(sites==null)
                    { brandsites = BrandConfigSA.Instance.GetSites();}
                         if (brandsites != null)
                         {
                             sites = new Sites();
                             foreach (Matchnet.Content.ValueObjects.BrandConfig.Site s in brandsites)
                             {  sites.Add(s); }
                             Cache.Instance.Insert(sites);
                         }
                         return sites;
           }
           catch(Exception ex)
           { throw (ex); }
    }

    private static Matchnet.Content.ValueObjects.AttributeMetadata.Attributes getAttributesMetadata()
    {
        //AttributeMapCollection mapcoll = null;
        Matchnet.Content.ValueObjects.AttributeMetadata.Attributes attrcoll = null;
        try
        {
            //string cachekey = Matchnet.Content.ValueObjects.AttributeMetadata.Attributes.CACHE_KEY;
            //attrcoll = (Matchnet.Content.ValueObjects.AttributeMetadata.Attributes)Cache.Instance.Get(cachekey);
            //if (attrcoll != null)
            //    return attrcoll;
             attrcoll = AttributeMetadataSA.Instance.GetAttributes();
            
            //{Cache.Instance.Insert(attrcoll); }
            return attrcoll;
        }
        catch (Exception ex)
        { throw (ex); }
    }

    public static void LogEvent(string source, string message, EventLogEntryType type)
    {
        try
        {
            EventLog.WriteEntry(source, message, type);
        }
        catch (Exception ex)
        { }
    }

   
}
