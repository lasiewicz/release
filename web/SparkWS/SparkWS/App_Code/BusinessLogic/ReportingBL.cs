﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.ActivityRecording.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Content.ServiceAdapters;

/// <summary>
/// Used to send reporting data to Spark. This class takes reporting metrics and places the data in appopriate places. Initially
/// written for the Messmo JDate Mobile project. Can be and should be extended later for other reporting needs.
/// </summary>
public class ReportingBL
{

    #region Public static methods
    public static MessmoReportingResponse UpdateReporting(MessmoReportingRequest request, string ticketKey)
    {
#if DEBUG
        Debug.WriteLine("UpdateReporting() Messmo sent the following data: " + request.GetValuesString());
        ServiceUtil.LogEvent("WWW", "UpdateReporting() Request received from Messmo--> " + request.GetValuesString(), EventLogEntryType.Information);
#endif

        bool reportThisAction = true;

        #region Taggle Parsing
        int fromMemberID, toMemberID, groupID, messageListID;
        Matchnet.HTTPMessaging.Constants.MessmoMessageType msgType = Matchnet.HTTPMessaging.Constants.MessmoMessageType.None;

        Matchnet.HTTPMessaging.Utilities.ParseMessmoTaggleValue(request.Taggle, out fromMemberID, out toMemberID, out msgType,
            out groupID, out messageListID);

        int siteID = 103;
        #endregion
        
        Matchnet.ActivityRecording.ValueObjects.Types.ActionType actionType = GetActionType(request.ActionType);

        if (actionType == Matchnet.ActivityRecording.ValueObjects.Types.ActionType.None)
        {
            throw new SparkWSClientException("This action is not supported");
        }

        string eventString = request.Event.ToLower();
        if (eventString == "action")
        {
            // check to see if the ActionType is one of "view full profile" or "read a message"
            // if so, action within Bedrock is required before sending this data to our reporting DB
            if (actionType == Matchnet.ActivityRecording.ValueObjects.Types.ActionType.ViewProfile)
            { 
                // update the List with this
                ListSaveResult saveResult = ListSA.Instance.AddListMember(HotListCategory.MembersYouViewed, groupID, siteID, toMemberID, fromMemberID, string.Empty, Matchnet.Constants.NULL_INT,
                    true, false);

                if (saveResult.Status != ListActionStatus.Success)
                {
                    throw new Exception("Unable to update the list.");
                }
            }
            else if (actionType == Matchnet.ActivityRecording.ValueObjects.Types.ActionType.ReadMessage)
            {
                // Only perform mark as read if this is a real message on our side.  Messmo considers everything as as message
                // (i.e. someone hotlisted you), so make sure it's a real message.
                if (msgType == Matchnet.HTTPMessaging.Constants.MessmoMessageType.Ecard ||
                    msgType == Matchnet.HTTPMessaging.Constants.MessmoMessageType.Email ||
                    msgType == Matchnet.HTTPMessaging.Constants.MessmoMessageType.Flirt ||
                    msgType == Matchnet.HTTPMessaging.Constants.MessmoMessageType.MissedIM)
                {
                    // mark the message as read within our site
                    ArrayList messageIDList = new ArrayList(1);
                    messageIDList.Add(messageListID);

                    bool markReadResult = EmailMessageSA.Instance.MarkRead(toMemberID, groupID, true, messageIDList);

                    // This is really the proper call for when viewing a message. The same call is used in the web application.
                    // Just marking a message was *not* updating Message Opendate column which resulted in Message Viewed Icon in SentBox from not showing up.
                    // The brand that we are sending in here is for UserName purpose only. We only need to get the community right.
                    
                    EmailMessageSA.Instance.RetrieveMessage(groupID, toMemberID, messageListID, String.Empty, Direction.None, true, ServiceUtil.GetAnyBrandOfCommunity(groupID));
                }
                else
                {
                    // If Messmo sends us a Mark as Read command for an entity that's not a message on our end, we don't even want to
                    // include this in our reports.
                    reportThisAction = false;
                }
            }
        }
        else if (eventString == "setting")
        {
            // todo: placeholder for phase 2            
            throw new SparkWSClientException("This EVENT is not supported.");
        }
        else
        {
            throw new SparkWSClientException("This EVENT is not supported.");
        }

        if (reportThisAction)
        {
            // Log this activity to the DB
            ActivityRecordingSA.Instance.RecordActivity(toMemberID, fromMemberID, siteID, actionType, Matchnet.ActivityRecording.ValueObjects.Types.CallingSystem.Messmo, request.GetValuesString());
        }

        return new MessmoReportingResponse("1.0.0", "OK");        
    }
    #endregion

    private static Matchnet.ActivityRecording.ValueObjects.Types.ActionType GetActionType(string messmoActionTypeString)
    {
        Matchnet.ActivityRecording.ValueObjects.Types.ActionType actionType = Matchnet.ActivityRecording.ValueObjects.Types.ActionType.None;

        switch (messmoActionTypeString.ToLower())
        {
            case "read":
                actionType = Matchnet.ActivityRecording.ValueObjects.Types.ActionType.ReadMessage;
                break;
            case "delete":
                actionType = Matchnet.ActivityRecording.ValueObjects.Types.ActionType.DeleteMessage;
                break;
            case "reply":
                actionType = Matchnet.ActivityRecording.ValueObjects.Types.ActionType.ReplyToMessage;
                break;
            case "send":
                actionType = Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendAMessage;
                break;
            case "login":
                actionType = Matchnet.ActivityRecording.ValueObjects.Types.ActionType.Login;
                break;
            case "logout":
                actionType = Matchnet.ActivityRecording.ValueObjects.Types.ActionType.Logout;
                break;
            case "profile":
                actionType = Matchnet.ActivityRecording.ValueObjects.Types.ActionType.ViewProfile;
                break;
        }

        return actionType;
    }
}
