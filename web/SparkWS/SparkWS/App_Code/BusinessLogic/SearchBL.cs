﻿using System;
using System.Collections.Generic;
using System.Collections;

using Matchnet;

using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;

using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceAdapters;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;

using Matchnet.Configuration.ValueObjects.SparkWS;

public class SearchBL
{
    /// <summary>
    /// Static Class
    /// </summary>
    private SearchBL()
    {
    }

    public static SearchByAreaCodeResponse SearchByAreaCode(SearchByAreaCodeRequest request, string ticketKey)
    {
        #region AccessTicket Validation

        AccessTicket ticket = AccessTicketBL.GetAccessTicket(ticketKey);

        if (!isInternalUser(ticket.AccessRoll))
        {
            throw new System.Exception("This method is not available for general use.");
        }

        #endregion

        #region Argument Validation

        if (request.PageSize > 50)
            throw new ArgumentOutOfRangeException("PageSize", "Page size cannot be more than 50.");

        if (request.StartRow < 1)
            throw new ArgumentOutOfRangeException("StartRow", "Start row cannot be less than 1.");

        if (request.AreaCodes.Length > 4)
            throw new ArgumentOutOfRangeException("AreaCodes", "Only up 4 area codes are supported.");

        if ((request.Gender.ToLower() != "male") && (request.Gender.ToLower() != "female"))
        {
            throw new ArgumentOutOfRangeException("Gender", "Gender must be \"Male\" or \"Female\".");
        }

        if ((request.SeekingGender.ToLower() != "male") && (request.SeekingGender.ToLower() != "female") && (request.SeekingGender.ToLower() != "both"))
        {
            throw new ArgumentOutOfRangeException("SeekingGender", "SeekingGender must be \"Male\" or \"Female\".");
        }

        if ((request.AgeFrom < 18) || (request.AgeFrom > 99))
        {
            throw new ArgumentOutOfRangeException("AgeFrom", "Age from must be between 18 and 99.");
        }

        if ((request.AgeTo < 18) || (request.AgeTo > 99))
        {
            throw new ArgumentOutOfRangeException("AgeTo", "Age to must be between 18 and 99.");
        }

        if (string.IsNullOrEmpty(request.Country))
        {
            throw new ArgumentNullException("Country name cannot be empty.");
        }

        #endregion

        Brand brand = BrandConfigSA.Instance.GetBrandByID(request.BrandID);
        SearchPreferenceCollection searchPreferenceCollection = new SearchPreferenceCollection();

        #region SearchType

        searchPreferenceCollection["SearchTypeID"] = ((int)SearchTypeID.AreaCode).ToString();

        #endregion

        #region OrderBy

        QuerySorting orderBy;

        // Set default value to popularity.
        if (string.IsNullOrEmpty(request.OrderBy))
        {
            orderBy = QuerySorting.Popularity;
        }
        else
        {
            orderBy = (QuerySorting)Enum.Parse(typeof(QuerySorting), request.OrderBy);
        }

        searchPreferenceCollection["SearchOrderBy"] = ((int)orderBy).ToString();

        #endregion

        #region CountyRegionID

        if (!string.IsNullOrEmpty(request.Country))
        {
            int countryRegionID = Constants.NULL_INT;

            RegionCollection regionCollection = RegionSA.Instance.RetrieveCountries();

            foreach (Region region in regionCollection)
            {
                if (region.Description == request.Country)
                {
                    countryRegionID = region.RegionID;
                    break;
                }
            }

            if (countryRegionID == Constants.NULL_INT)
            {
                throw new Exception("This country does not exist in the Region database. " + request.Country);
            }

            searchPreferenceCollection.Add("CountryRegionID", countryRegionID.ToString());
            searchPreferenceCollection.Add("RegionID", countryRegionID.ToString());
        }

        #endregion

        #region AreaCodes

        if (request.AreaCodes.Length != 0)
        {
            for (int i = 0; i < request.AreaCodes.Length; i++)
            {
                searchPreferenceCollection.Add("AreaCode" + (i + 1), request.AreaCodes[i]);

                if (i > 3)
                {
                    throw new ArgumentOutOfRangeException("Only up to 4 area codes are supported in search");
                }
            }
        }

        #endregion

        #region Distance

        ///TODO: add Distance to the request class.
        searchPreferenceCollection.Add("Distance", "60");

        #endregion

        #region Age Range

        if (request.AgeFrom != 0 && request.AgeTo != 0)
        {
            searchPreferenceCollection.Add("MinAge", request.AgeFrom.ToString());
            searchPreferenceCollection.Add("MaxAge", request.AgeTo.ToString());
        }

        #endregion

        #region HasPhotoFlag

        if (request.HasPhotoFlag != Constants.NULL_INT)
        {
            searchPreferenceCollection.Add("HasPhotoFlag", request.HasPhotoFlag.ToString());
        }

        #endregion


        // returned value
        List<MiniProfile> miniProfiles = new List<MiniProfile>();

        #region GenderMask

        if (request.SeekingGender != "both")
        {
            if (!string.IsNullOrEmpty(request.Gender) && !string.IsNullOrEmpty(request.SeekingGender))
            {

                GenderMask gender = (GenderMask)Enum.Parse(typeof(GenderMask), request.Gender);
                GenderMask seekingGender = (GenderMask)Enum.Parse(typeof(GenderMask), request.SeekingGender) == GenderMask.Male ? GenderMask.SeekingMale : GenderMask.SeekingFemale;

                int genderMask = Matchnet.GenderUtils.FlipMaskIfHeterosexual((int)gender + (int)seekingGender);

                searchPreferenceCollection.Add("GenderMask", genderMask.ToString());
            }

            MatchnetQueryResults searchResults = MemberSearchSA.Instance.Search(searchPreferenceCollection, brand.Site.Community.CommunityID, brand.Site.SiteID, request.StartRow, request.PageSize);

            ArrayList searchMembers = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);

            foreach (Matchnet.Member.ServiceAdapters.Member searchMember in searchMembers)
            {
                GetMiniProfileResponse response = MemberBL.GetMiniProfile(new GetMiniProfileRequest("1.0.0", searchMember.MemberID, request.BrandID), ticketKey);
                miniProfiles.Add(response.MiniProfile);
            }
        }
        else
        {
            // Get males
            searchPreferenceCollection.Add("GenderMask", "9");
            MatchnetQueryResults searchResults = MemberSearchSA.Instance.Search(searchPreferenceCollection, brand.Site.Community.CommunityID, brand.Site.SiteID, request.StartRow, request.PageSize / 2);

            ArrayList searchMembers = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);

            foreach (Matchnet.Member.ServiceAdapters.Member searchMember in searchMembers)
            {
                GetMiniProfileResponse response = MemberBL.GetMiniProfile(new GetMiniProfileRequest("1.0.0", searchMember.MemberID, request.BrandID), ticketKey);
                miniProfiles.Add(response.MiniProfile);
            }

            // Get females
            searchPreferenceCollection["GenderMask"] = "6";

            searchResults = MemberSearchSA.Instance.Search(searchPreferenceCollection, brand.Site.Community.CommunityID, brand.Site.SiteID, request.StartRow, request.PageSize / 2);
            searchMembers = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);

            foreach (Matchnet.Member.ServiceAdapters.Member searchMember in searchMembers)
            {
                GetMiniProfileResponse response = MemberBL.GetMiniProfile(new GetMiniProfileRequest("1.0.0", searchMember.MemberID, request.BrandID), ticketKey);
                miniProfiles.Add(response.MiniProfile);
            }
        }

        #endregion


        return new SearchByAreaCodeResponse("1.0.0", miniProfiles);
    }

    public static SearchByMemberSearchPreferencesResponse SearchByMemberSearchPreferences(SearchByMemberSearchPreferencesRequest request, string ticketKey)
    {
        #region AccessTicket Validation

        AccessTicket ticket = AccessTicketBL.GetAccessTicket(ticketKey);

        if (!isInternalUser(ticket.AccessRoll))
        {
            throw new System.Exception("This method is not available for general use.");
        }

        #endregion

        #region Argument Validation

        if (request.MemberID == 0 || request.MemberID == Constants.NULL_INT)
            throw new ArgumentOutOfRangeException("MemberID", "Member ID cannot be 0 or null.");

        if (request.PageSize > 50)
            throw new ArgumentOutOfRangeException("PageSize", "Page size cannot be more than 50.");

        if (request.StartRow < 1)
            throw new ArgumentOutOfRangeException("StartRow", "Start row cannot be less than 1.");

        #endregion

        #region Populate required objects for search.

        Brand brand = BrandConfigSA.Instance.GetBrandByID(request.BrandID);

        Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(request.MemberID, MemberLoadFlags.None);
        if (member == null)
            throw new Exception("This member does not exist. MemberID=" + request.MemberID.ToString());

        SearchPreferenceCollection searchPreferenceCollection = SearchPreferencesSA.Instance.GetSearchPreferences(member.MemberID, brand.Site.Community.CommunityID);
        if (searchPreferenceCollection == null)
            throw new Exception("This's member's search preference collection returned null. MemberID=" + request.MemberID.ToString());

        #endregion

        #region OrderBy

        QuerySorting orderBy;

        // Set default value to popularity.
        if (string.IsNullOrEmpty(request.OrderBy))
        {
            orderBy = QuerySorting.Popularity;
        }
        else
        {
            orderBy = (QuerySorting)Enum.Parse(typeof(QuerySorting), request.OrderBy);
        }

        searchPreferenceCollection["SearchOrderBy"] = ((int)orderBy).ToString();

        #endregion

        MatchnetQueryResults searchResults = MemberSearchSA.Instance.Search(searchPreferenceCollection, brand.Site.Community.CommunityID, brand.Site.SiteID, request.StartRow, request.PageSize);

        ArrayList searchMembers = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);

        List<MiniProfile> miniProfiles = new List<MiniProfile>();

        foreach (Matchnet.Member.ServiceAdapters.Member searchMember in searchMembers)
        {
            GetMiniProfileResponse response = MemberBL.GetMiniProfile(new GetMiniProfileRequest("1.0.0", searchMember.MemberID, request.BrandID), ticketKey);
            miniProfiles.Add(response.MiniProfile);
        }

        return new SearchByMemberSearchPreferencesResponse("1.0.0", miniProfiles);
    }

    #region Private Methods

    private static bool isInternalUser(ApiAccessRoll roll)
    {
        return ((roll & (ApiAccessRoll.Internal | ApiAccessRoll.Developer | ApiAccessRoll.SuperUser)) > 0);
    }

    #endregion

}
