using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using System.Diagnostics;


/// <summary>
/// Ecapsulates logic to send messages across Spark's networks
/// </summary>
public class MessagingBL
{
    #region Public Static Methods

    /// <summary>
    /// marks a message as read in a particular user's inbox
    /// </summary>
    /// <param name="request">the request to mark a message as read</param>
    /// <param name="ticketKey">the access key that requests that</param>
    /// <returns>a markmessage read result that specifies whether the
    /// message was succesfully marked as read</returns>
    public static MarkMessageReadResult MarkMessageRead(MarkMessageReadRequest request, string ticketKey)
    {
        //checks the ticket
        AccessTicket ticket = AccessTicketBL.GetAccessTicket(ticketKey);

        if (!isInternalUser(ticket.AccessRoll))
        {
            throw new System.Exception("This method is not available for general use.");
        }

        //the message ID to invalidate
        ArrayList messageIDList = new ArrayList (1);
        messageIDList.Add (request.MessageID);

        bool markReadResult = EmailMessageSA.Instance.MarkRead(request.MemberID, request.CommunityID, true, messageIDList);

        // This is really the proper call for when viewing a message. The same call is used in the web application.
        // Just marking a message was *not* updating Message Opendate column which resulted in Message Viewed Icon in SentBox from not showing up.
        EmailMessageSA.Instance.RetrieveMessage(request.CommunityID, request.MemberID, request.MessageID, String.Empty, Direction.None, true,
            ServiceUtil.GetAnyBrandOfCommunity(request.CommunityID));

        return new MarkMessageReadResult("1.0.0", markReadResult);
    }

    public static string MarkMessageReadPixel(string pUid)
    {
        bool isValid = true;
        string decrypted;

        try
        {
            // after decrypting the value, it should be messagelistid_memberid_communityid
            decrypted = Matchnet.Security.Crypto.Decrypt(MemberBL.SPARK_WS_CRYPT_KEY, pUid);
        }
        catch (Exception ex)
        {
            ServiceUtil.LogEvent("WWW", "Hack attempt with invalid uid passed to MarkMessageReadPixel() " + ex.Message, EventLogEntryType.Error);
            return "Error";
        }

        // split and check length is correct
        string[] split = decrypted.Split(new char[] { '_' });
        if (split.Length != 3)
            isValid = false;

        if (isValid)
        {
            int messageListId = Constants.NULL_INT;
            int memberId = Constants.NULL_INT;
            int communityId = Constants.NULL_INT;

            // make sure they are all numbers
            isValid = int.TryParse(split[0], out messageListId);

            if (isValid)
                isValid = int.TryParse(split[1], out memberId);

            if (isValid)
                isValid = int.TryParse(split[2], out communityId);

            if (isValid)
            {
                EmailMessage emailMessage = EmailMessageSA.Instance.RetrieveMessage(communityId, memberId, messageListId, String.Empty, Direction.None, true,
                    ServiceUtil.GetAnyBrandOfCommunity(communityId));
                isValid = (emailMessage != null);
            }
        }

        if (isValid)
        {
            return "OK";
        }
        else
        {
            return "Error";
        }
    }

    /// <summary>
    /// sends an internal email from a particular user to a particular user in the same community
    /// </summary>
    /// <param name="request">the request to send</param>
    /// <param name="ticketKey">the access key to check for privileges</param>
    /// <returns>a result whether the mail was sent correctly or not, that
    /// would also include the messageID so it can be later on set to read or deleted
    /// The result would also let you know whether the sender was on the reciever's ignore list.
    /// if this is true then the message would fail and won't be send</returns>
    public static InternalMailResult SendInternalMail(InternalMailRequest request, string ticketKey)
    {
        //checks the access ticket reputability
        AccessTicket ticket = AccessTicketBL.GetAccessTicket(ticketKey);
        if (!isInternalUser(ticket.AccessRoll))
        {
            throw new Exception("This method is not available for general use.");
        }

        //check if the user is not on the ignore list
        Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(request.ToMemberID);
        if (list.IsHotListed(HotListCategory.IgnoreList, request.CommunityID, request.FromMemberID))
        {
            return new InternalMailResult("1.0.0", -1, -1, false, true);
        }

        Matchnet.Member.ServiceAdapters.Member member = null;

        //get the member that is sending the message so we can check
        //whehther his message needs to be saved in his sent messages folder
        try
        {
            member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(request.FromMemberID, MemberLoadFlags.None);
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("MessagingBL.SendInternalEcard() unable to get sender", ex);
        }

        //get the brand ID for this member
        int brandID = Constants.NULL_INT;
        bool saveInSentFolder = false;

        try
        {
            member.GetLastLogonDate(request.CommunityID, out brandID);
            //if we fail to get the brandID
            if (brandID == Constants.NULL_INT)
            {
                throw new SparkWSClientException(string.Format("Unable to get the brandID for member {0} with community ID of {1}", request.FromMemberID.ToString(), request.CommunityID.ToString()));
            }

            //we get the brand
            Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

            AttributeOptionMailboxPreference mailboxMask = (AttributeOptionMailboxPreference)member.GetAttributeInt(brand, CachedMember.ATTRIBUTEID_MAILBOXPREFERENCE);
            saveInSentFolder = ((mailboxMask & AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) == AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) ? true : false;
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("SendExternalEcard failed because last logon date or mailbox preferences could not be retrieved for member.", ex);
        }

        //create the new message
        MessageSave message = new MessageSave(request.FromMemberID, request.ToMemberID, request.CommunityID, request.SiteID, MailType.ECard, request.Subject, request.Body);
       
        //send the message
        MessageSendResult result = EmailMessageSA.Instance.SendMessage(message, saveInSentFolder, Matchnet.Constants.NULL_INT, true);


        //we check whehther the message was succesfully sent
        //and return the approporiate result
        if (result.Status == MessageSendStatus.Success)
        {
            return new InternalMailResult("1.0.0", result.RecipientMessageListID, result.SenderMessageListID , true, false);
        }
        else
        {
            //if the message errored out
            return new InternalMailResult("1.0.0", Matchnet.Constants.NULL_INT, Matchnet.Constants.NULL_INT, false, false);
        }

        
    }

    public static EcardResult SendInternalEcard(InternalEcardRequest request, string ticketKey)
    {
        //checks the access ticket reputability
        AccessTicket ticket = AccessTicketBL.GetAccessTicket(ticketKey);
        if (!isInternalUser(ticket.AccessRoll))
        {
            throw new SparkWSClientException("This method is not available for general use.");
        }

        if (IsOnIgnoreList(request.FromMember, request.ToMember, request.CommunityID))
        {
            return new EcardResult(false, EcardSendResult.SenderOnIgnoreList);
        }

        try
        {
            // Do not check quota against member with "E-cards Administrator Privilege". This is to enable mass sending of E-cards for travel & events. 03/18/08
            if (!MemberPrivilegeSA.Instance.HasPrivilege(request.FromMember, 156))
            {
                //check whether the user is at his quota
                if (QuotaSA.Instance.IsAtQuota(request.CommunityID, request.FromMember, Matchnet.Content.ValueObjects.Quotas.QuotaType.SentECard))
                {
                    return new EcardResult(false, EcardSendResult.DailyQuotaReached);
                }
            }

            InternalMailRequest internalMailRequest = new InternalMailRequest("1.0.0", request.FromMember,
                request.ToMember, request.CommunityID, request.SiteID, "Ecard", request.EcardInfo.Url, 9);

            InternalMailResult mailResult = MessagingBL.SendInternalMail(internalMailRequest, ticketKey);


            EcardResult result = null;

            if (mailResult.Success)
            {
                QuotaSA.Instance.AddQuotaItem(request.CommunityID, request.FromMember, Matchnet.Content.ValueObjects.Quotas.QuotaType.SentECard);

                //sets the add for reciever to the people i've sent ecard to list
                AddListMemberRequest addListMemberRecieve = new AddListMemberRequest("1.0.0",
                    -24, request.CommunityID, request.SiteID, request.FromMember, request.ToMember, false, "");

                //sets the add for sender to the people that have sent me ecard to list
                AddListMemberRequest addListMemberSend = new AddListMemberRequest("1.0.0",
                    -25, request.CommunityID, request.SiteID, request.ToMember, request.FromMember, false, "");

                //do the adds
                ListSaveResult listSaveRecieve = ListBL.AddListMember(addListMemberRecieve, ticketKey);
                ListSaveResult listSaveSend = ListBL.AddListMember(addListMemberSend, ticketKey);

                //set the result
                result = new EcardResult(true, EcardSendResult.Success);
                result.MessageID = mailResult.MessageID;

                //mail alert code to alert the reciever they have a new ecard
                #region Load member from member service
                Matchnet.Member.ServiceAdapters.Member member = null;


                //get the member
                try
                {
                    member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(request.ToMember, MemberLoadFlags.None);
                }
                catch (Exception ex)
                {
                    throw new SparkWSClientException("MessagingBL.SendInternalEcard() unable to get reciever", ex);
                }

                //get the brand ID for this member
                int brandID = Constants.NULL_INT;

                try
                {
                    member.GetLastLogonDate(request.CommunityID, out brandID);
                }
                catch (Exception ex)
                {
                    throw new SparkWSClientException("SendExternalEcard failed because last logon date could not be retrieved for member.", ex);
                }

                //if we fail to get the brandID
                if (brandID == Constants.NULL_INT)
                {
                    throw new SparkWSClientException(string.Format("Unable to get the brandID for member {0} with community ID of {1}", request.FromMember, request.CommunityID));
                }

                //check whether the member is suspended
                if (member.IsEligibleForEmail(request.CommunityID))
                {
                    ExternalMailSA.Instance.SendEcardToMember(brandID, request.SiteID, request.FromMember,
                        request.EcardInfo.Url, request.EcardInfo.ThumbUrl, request.ToMember, DateTime.Now);
                }

                #endregion
            }
            else
            {
                result = new EcardResult(false, EcardSendResult.SenderOnIgnoreList);
            }

            return result;
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("The card could not be sent.", ex);
        }
    }

    /// <summary>
    /// checks whether a paritucular member is on another member's ignore list
    /// </summary>
    /// <param name="senderMemberID">the member ID of the ignore list owner</param>
    /// <param name="recieverMemberID">the member ID of the people to check for</param>
    /// <param name="communityID">the community ID to which the list belongs</param>
    /// <returns>true/false whether the member is on the list</returns>
    private static bool IsOnIgnoreList(int senderMemberID, int recieverMemberID, int communityID)
    {
        //checks whehther the sender is on the recievers ignore list
        Matchnet.List.ServiceAdapters.List recieverList = null;
        bool isOnIgnoreList = false;

        try
        {
            recieverList = ListSA.Instance.GetList(recieverMemberID);
            isOnIgnoreList = recieverList.IsHotListed(HotListCategory.IgnoreList, communityID, senderMemberID);
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("Failed to determine whether the ecard sender is on the recievers ignore list.", ex);
        }

        return isOnIgnoreList;
    }

    /// <summary>
    /// sends an external ecard mail message to a particualar email
    /// </summary>
    /// <param name="request">a request with all the parameters</param>
    /// <param name="ticketKey">the ticket key invoking this method</param>
    /// <returns>a result otline the success/faulire of the operation</returns>
    public static EcardResult SendExternalEcard(ExternalEcardRequest request, string ticketKey)
    {
        //checks the access ticket reputability
        AccessTicket ticket = AccessTicketBL.GetAccessTicket(ticketKey);
        if (!isInternalUser(ticket.AccessRoll))
        {
            throw new Exception("This method is not available for general use.");
        }

        //check whether the email is valid
        if (!IsEmail(request.ToEmail))
        {
            return new EcardResult(false, EcardSendResult.InvalidRecipientEmail);
        }

        //check if the sender has reached his dailly email quota
        if (QuotaSA.Instance.IsAtQuota(request.CommunityID, request.FromMember, Matchnet.Content.ValueObjects.Quotas.QuotaType.SentECard))
        {
            return new EcardResult(false, EcardSendResult.DailyQuotaReached);
        }

        #region Load member from member service
        Matchnet.Member.ServiceAdapters.Member member = null;


        //get the member
        try
        {
            member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(request.FromMember, MemberLoadFlags.None);
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("MessagingBL.SendExternalEcard() unable to get member", ex);
        }

        //get the brand ID for this member
        int brandID = Constants.NULL_INT;

        try
        {
            member.GetLastLogonDate(request.CommunityID, out brandID);
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("SendExternalEcard failed because last logon date could not be retrieved for member.", ex);
        }

        //if we fail to get the brandID
        if (brandID == Constants.NULL_INT)
        {
            throw new SparkWSClientException(string.Format("Unable to get the brandID for member {0} with community ID of {1}", request.FromMember, request.CommunityID));
        }

        #endregion




        //send the external ecard
        if (ExternalMailSA.Instance.SendEcardToFriend(brandID, request.SiteID, request.FromMember, request.EcardInfo.Url,
            request.EcardInfo.ThumbUrl, request.ToEmail, DateTime.Now))
        {
            QuotaSA.Instance.AddQuotaItem(request.CommunityID, request.FromMember, Matchnet.Content.ValueObjects.Quotas.QuotaType.SentECard);
            return new EcardResult(true, EcardSendResult.Success);
        }

        return new EcardResult(false, EcardSendResult.InvalidRecipientEmail);
    }

    /// <summary>
    /// checks whether a given string is an email
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    private static bool IsEmail(string value)
    {
        if (value.IndexOf("@") > 0 && value.LastIndexOf(".") > value.IndexOf("@"))
            return true;

        return false;
    
    }


    public static ExternalMailResult SendExternalMail(ExternalMailRequest request, string ticketKey)
    {
        Matchnet.Member.ServiceAdapters.Member member = null;
        ExternalMailResult result = null;
        int communityID = Matchnet.Constants.NULL_INT;
        int brandID = Matchnet.Constants.NULL_INT;

        AccessTicket tkt = AccessTicketBL.GetAccessTicket(ticketKey);

        if (!isInternalUser(tkt.AccessRoll))
        {
            throw new Exception("This method is not available for general use.");
        }

        #region Load member from member service

        try
        {
            member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(request.MemberID, MemberLoadFlags.None);
        }
        catch (Exception ex)
        {
            throw new Exception("MessagingBL.SendExternalMail() unable to get member", ex);
        }

        #endregion

        #region Determine CommunityID given SiteID

        Matchnet.Content.ValueObjects.BrandConfig.Sites sites = BrandConfigSA.Instance.GetSites();
        
        Site site = null;

        foreach (Site s in sites)
        {
            if (s.SiteID == request.SiteID)
            {
                site = s;
                break;
            }
        }

        if (site == null)
        {
            result = new ExternalMailResult(false, "Unknown SiteID.");
            return result;
        }

        communityID = site.Community.CommunityID;

        #endregion

        #region Don't send if member's suspended.

        if (!member.IsEligibleForEmail(communityID))
        {
            result = new ExternalMailResult(false, "Not eligible for email");
            return result;
        }

        #endregion

        // Retrieve BrandID
        member.GetLastLogonDate(communityID, out brandID);

        if (brandID == Constants.NULL_INT)
        {
            throw new Exception("BrandID not found. Member(" + member.MemberID.ToString() + ") has never logged into this brand.");
        }
        ///TODO: implement database configuration so access ticket IDs can be tied in with email types
        #region Determine which ExternalMail service method to invoke and call.

        try
        {
            switch ((int)request.ExternalMailTypeID)
            {
                case (int)EmailType.MessageBoardInstantNotification:
                    ExternalMailSA.Instance.SendMessageBoardInstantNotification(brandID, request.SiteID, request.MemberID, request.Values[2], request.Values[3], request.Values[4], Convert.ToInt32(request.Values[5]), request.Values[6]);
                    break;

                case (int)EmailType.MessageBoardDailyUpdates:
                    ExternalMailSA.Instance.SendMessageBoardDailyUpdates(brandID, request.SiteID, request.MemberID, request.Values[2], request.Values[3], Convert.ToInt32(request.Values[4]), Convert.ToInt32(request.Values[5]), Convert.ToInt32(request.Values[6]), Convert.ToInt32(request.Values[7]), request.Values[8]);
                    break;

                case (int)EmailType.InternalCorpMail:
                    ExternalMailSA.Instance.SendInternalCorpMail(brandID,
                        request.Values[0],
                        request.Values[1],
                        request.Values[2],
                        request.Values[3],
                        request.Values[4]);
                    break;
                case (int)EmailType.SendFriendReferral:
                    ExternalMailSA.Instance.SendFriendReferralEmail(
                        request.Values[0],
                        request.Values[1],
                        request.Values[2],
                        request.Values[3],
                        request.Values[4],
                        "",
                        request.MemberID,
                        brandID);
                    break;
                default:
                    result = new ExternalMailResult(false, "Unknown ExternalMailTypeID");
                    return result;

            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error sending email to Spark's external mail service. [ExternalMailTypeID: " + request.ExternalMailTypeID.ToString() + ", MemberID:" + member.MemberID.ToString() + "], error:" + ex.Message, ex);
        }

        #endregion

        result = new ExternalMailResult(true, "");

        return result;
    }

    private static bool isInternalUser(ApiAccessRoll roll)
    {
        return ((roll & (ApiAccessRoll.Internal | ApiAccessRoll.Developer | ApiAccessRoll.SuperUser)) > 0);
    }

    /// <summary>
    /// gets a community ID given a site ID
    /// </summary>
    /// <param name="siteID">the siteID to use</param>
    /// <returns>the community ID</returns>
    private static int GetCommunityID(int siteID)
    {
        Site site = BrandConfigSA.Instance.GetBrands().GetSite(siteID);
        return site.Community.CommunityID;
    }

    /// <summary>
    /// For sending onsite(internal) email to another community member.
    /// 
    /// This method and its classes are very close to SendInternalEmail method, but from examination, that method was created 
    /// specifically for E-card. Keeping it safe by separating it out.
    /// </summary>
    /// <param name="request"></param>
    /// <param name="ticketKey"></param>
    /// <returns></returns>
    public static SendEmailResponse SendEmail(SendEmailRequest request, string ticketKey)
    {
        //checks the access ticket reputability
        AccessTicket ticket = AccessTicketBL.GetAccessTicket(ticketKey);
        if (!isInternalUser(ticket.AccessRoll))
        {
            throw new Exception("This method is not available for general use.");
        }

        // Get Brand info
        Brand brand = BrandConfigSA.Instance.GetBrandByID(request.BrandID);
        
        // Check if the user is not on the ignore list
        Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(request.ToMemberID);
        if (list.IsHotListed(HotListCategory.IgnoreList, brand.Site.Community.CommunityID, request.FromMemberID))
        {
            return new SendEmailResponse("1.0.0", "Failed", Constants.NULL_INT, Constants.NULL_INT, true);
        }

        Matchnet.Member.ServiceAdapters.Member member = null;

        //get the member that is sending the message so we can check
        //whehther his message needs to be saved in his sent messages folder
        try
        {
            member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(request.FromMemberID, MemberLoadFlags.None);
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("MessagingBL.SendEmail() unable to get sender", ex);
        }

        //get the brand ID for this member
        int brandID = Constants.NULL_INT;
        bool saveInSentFolder = false;

        try
        {
            member.GetLastLogonDate(brand.Site.Community.CommunityID, out brandID);
            //if we fail to get the brandID
            if (brandID == Constants.NULL_INT)
            {
                throw new SparkWSClientException(string.Format("Unable to get the brandID for member {0} with community ID of {1}", request.FromMemberID.ToString(), brand.Site.Community.CommunityID.ToString()));
            }

            //we get the brand
            Brand memberBrand = BrandConfigSA.Instance.GetBrandByID(brandID);

            AttributeOptionMailboxPreference mailboxMask = (AttributeOptionMailboxPreference)member.GetAttributeInt(memberBrand, CachedMember.ATTRIBUTEID_MAILBOXPREFERENCE);
            saveInSentFolder = ((mailboxMask & AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) == AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) ? true : false;
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("MessagingBL.SendEmail() failed because last logon date or mailbox preferences could not be retrieved for member.", ex);
        }

        //create the new message
        MessageSave message = new MessageSave(request.FromMemberID, request.ToMemberID, brand.Site.Community.CommunityID, brand.Site.SiteID, MailType.Email, request.Subject, request.Body);

        //send the message
        MessageSendResult result = EmailMessageSA.Instance.SendMessage(message, saveInSentFolder, Matchnet.Constants.NULL_INT, true);

        //we check whehther the message was succesfully sent
        //and return the approporiate result
        if (result.Status == MessageSendStatus.Success)
        {
            return new SendEmailResponse("1.0.0", "Success", result.RecipientMessageListID, result.SenderMessageListID, false);
        }
        else
        {
            return new SendEmailResponse("1.0.0", "Failed", result.RecipientMessageListID, result.SenderMessageListID, false);
        }
    }

    /// <summary>
    /// For retrieving emails from a member's Inbox.
    /// 
    /// A collection of Emali object is returned given a MemberID and a BrandID
    /// </summary>
    /// <param name="request"></param>
    /// <param name="ticketKey"></param>
    /// <returns></returns>
    public static GetEmailResponse GetEmail(GetEmailRequest request, string ticketKey)
    {
        //checks the access ticket reputability
        AccessTicket ticket = AccessTicketBL.GetAccessTicket(ticketKey);
        if (!isInternalUser(ticket.AccessRoll))
        {
            throw new Exception("This method is not available for general use.");
        }

        // Get Brand info
        Brand brand = BrandConfigSA.Instance.GetBrandByID(request.BrandID);

        #region Get Member, let's see if this memberID's good.

        Matchnet.Member.ServiceAdapters.Member member = null;

        //get the member that is sending the message so we can check
        //whehther his message needs to be saved in his sent messages folder
        try
        {
            member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(request.MemberID, MemberLoadFlags.None);
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("MessagingBL.GetEmail() unable to get member", ex);
        }

        #endregion

        int total = EmailMessageSA.Instance.RetrieveMessages(request.MemberID, brand.Site.Community.CommunityID, (int)SystemFolders.Inbox, brand.Site.SiteID).Count;

        ICollection emailMessageCollection = EmailMessageSA.Instance.RetrieveMessages(request.MemberID, brand.Site.Community.CommunityID, (int)SystemFolders.Inbox, request.StartRow, request.PageSize, "insertdate asc", brand);

        List<Email> emails = new List<Email>();

        foreach (EmailMessage emailMessage in emailMessageCollection)
        {            
            // Collection call above does not return message body.. so here's another call..
            EmailMessage emailMessage2 = EmailMessageSA.Instance.RetrieveMessage(brand.Site.Community.CommunityID, request.MemberID, emailMessage.MemberMailID, "insertdate asc", Direction.None, false, brand);

            Email email = new Email("1.0.0", emailMessage.Subject, emailMessage2.Message.MessageBody, emailMessage.InsertDate.ToString("g"), emailMessage.FromMemberID, emailMessage.FromUserName, emailMessage.ToMemberID, emailMessage.ToUserName, ((MailType)emailMessage.MailTypeID).ToString(), emailMessage.StatusMask.ToString(), emailMessage.OpenDate.ToString("g"));

            emails.Add(email);
        }

        return new GetEmailResponse("1.0.0", emails, total);
    }

    /// <summary>
    /// This method was written for Messmo to use. This method will send an internal mail and then send the same message to
    /// the member's phone if the member has this feature enabled. Since Messmo is only supported on JDate.com, this method only
    /// works for JDate.com for now.
    /// </summary>
    /// <param name="request"></param>
    /// <param name="ticketKey"></param>
    /// <returns></returns>
    public static MessmoMessageResponse SendMessmoMessage(MessmoMessageRequest request, string ticketKey)
    {
#if DEBUG
        Debug.WriteLine("SendMessmoMessage() Request received from Messmo--> " + request.GetValuesString());
        ServiceUtil.LogEvent("WWW", "SendMessmoMessage() Request received from Messmo--> " + request.GetValuesString(), EventLogEntryType.Information);
#endif
                
        if (!ServiceUtil.ValidMessmoChannelNumber(request.ChannelNumber))
        {
            throw new SparkWSClientException("This channel number does not exist: " + request.ChannelNumber);
        }

        // We only have support for JDATE
        // if we are going to support more brands and cross-brand situations, revisit all lines of code that use this variable
        Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);

        #region Taggle Parsing
        // Taggle format:
        // senderMemberId::receivingMemberId::MessmoMessageType::GroupIDForMessage::OriginalMessageID
        // Since all messages coming from Messmo are replies, member specified by the receivingMemberId is the one who
        // is performing the action (in this case, sending an internal email)

        int fromMemberId, toMemberId, groupIdForOriginalMessage, originalMessageListId;
        Matchnet.HTTPMessaging.Constants.MessmoMessageType msgType;
                
        Matchnet.HTTPMessaging.Utilities.ParseMessmoTaggleValue(request.Taggle, out toMemberId, out fromMemberId, out msgType,
            out groupIdForOriginalMessage, out originalMessageListId);

        if (fromMemberId == Constants.NULL_INT)
        {
            throw new SparkWSClientException("Invalid Taggle: " + request.Taggle);
        }        
        #endregion

        #region Ignore List Check & Save Mail in Sent Folder option check
        // Ignore list check
        Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(toMemberId);
        if (list.IsHotListed(HotListCategory.IgnoreList, brand.Site.Community.CommunityID, fromMemberId))
        {
            return new MessmoMessageResponse("1.0.0", "Failed", Constants.NULL_INT, Constants.NULL_INT, true);
        }
        
        // Check sender's "Save Mail in Sent Folder" option
        Matchnet.Member.ServiceAdapters.Member member = null;
        try
        {
            member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(fromMemberId, MemberLoadFlags.None);
        }
        catch (Exception ex)
        {
            throw new Exception("MessagingBL.SendMessage() unable to get sender", ex);
        }

        bool saveInSentFolder = false;
        try
        {
            AttributeOptionMailboxPreference mailboxMask = (AttributeOptionMailboxPreference)member.GetAttributeInt(brand, CachedMember.ATTRIBUTEID_MAILBOXPREFERENCE);
   
            saveInSentFolder = ((mailboxMask & AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) == AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) ? true : false;
        }
        catch (Exception ex)
        {
            throw new Exception("MessagingBL.SendMessage() unable to retrieve mailbox preferences", ex);
        }
        #endregion

        // Create a new message to send
        // TODO:
        // -Figure out the logic to determine the message title. For email type, just append "RE:" to the original title
        #region Message Subject Determination
        string messageSubject = string.Empty;
        if (msgType == Matchnet.HTTPMessaging.Constants.MessmoMessageType.Email ||
            msgType == Matchnet.HTTPMessaging.Constants.MessmoMessageType.Flirt ||
            msgType == Matchnet.HTTPMessaging.Constants.MessmoMessageType.Ecard ||
            msgType == Matchnet.HTTPMessaging.Constants.MessmoMessageType.MissedIM)
        {
            EmailMessage originalMsg = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveMessage(fromMemberId, groupIdForOriginalMessage, originalMessageListId);
            if (originalMsg != null)
            {
                string tempSubject = originalMsg.Subject;
                if (tempSubject.ToLower().Substring(0, 3) == "re:")
                    messageSubject = tempSubject;
                else
                    messageSubject = "RE: " + tempSubject;
            }
            else
            {
                throw new Exception("MessagingBL.SendMessage() cannot find the original message information");
            }
        }
        else
        {
            messageSubject = Matchnet.HTTPMessaging.Utilities.GetMessageSubject(msgType);

            // this should be Constants.NULL_INT already from the Taggle value for non-email messages, but just to be safe
            originalMessageListId = Constants.NULL_INT;
        }
        #endregion

        MessageSave message = new MessageSave(fromMemberId, toMemberId, brand.Site.Community.CommunityID, brand.Site.SiteID, MailType.Email, messageSubject, request.Message);
        // Use the timestamp sent in as the message insert date instead of the default (which is DateTime.Now)
        message.InsertDate = Matchnet.HTTPMessaging.Utilities.ConvertMessmoTimestampToDateTime(request.TimeStamp, true);

        // Send the message
        MessageSendResult result = EmailMessageSA.Instance.SendMessage(message, saveInSentFolder, originalMessageListId, true);

        // Prepare the response
        if (result.Status == MessageSendStatus.Success)
        {
            return new MessmoMessageResponse("1.0.0", "Success", result.RecipientMessageListID, result.SenderMessageListID, false);
        }
        else
        {
            return new MessmoMessageResponse("1.0.0", "Failed", result.RecipientMessageListID, result.SenderMessageListID, false);
        }

    }

    

    #endregion  
}
