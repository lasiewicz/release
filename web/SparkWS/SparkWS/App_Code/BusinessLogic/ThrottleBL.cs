using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;

/// <summary>
/// Summary description for ThrottleBL
/// </summary>
public class ThrottleBL
{
   public static readonly ThrottleBL Instance = new ThrottleBL();
   public const double BASE_INTERVAL_SEC = 60; // 1 minute interval
   private System.Timers.Timer _Timer = new System.Timers.Timer();

   private Dictionary<string, Dictionary<string, ThrottleInfo>> _Accessors = new Dictionary<string, Dictionary<string, ThrottleInfo>>();


   private ThrottleBL()
   {
	  _Timer.Interval = BASE_INTERVAL_SEC * 1000;
	  _Timer.AutoReset = false;
	  _Timer.Elapsed += new System.Timers.ElapsedEventHandler(_Timer_Elapsed);
	  _Timer.Start();
   }

   void _Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
   {
	  lock (_Accessors)
	  {
		 foreach (string key in _Accessors.Keys)
		 {
			foreach (string ctx in _Accessors[key].Keys)
			{
			   (_Accessors[key][ctx]).Count = 0;
			}
		 }
	  }
	  _Timer.Start();
   }

/// <summary>
/// Increments the hit rate counter for the ticketKey in the context, and throws an exception if rate exceeds threshold
/// </summary>
/// <param name="ticketKey">Represents the caller/ client that calls this method</param>
/// <param name="context">The function name which called this throttle. Each context has it's own counter.</param>
   /// <param name="threshold">The number of calls per BASE_INTERVAL allowed.</param>
   public void Ding(string ticketKey, string context, long threshold)
   {
       lock (_Accessors)
       {
           if (_Accessors.ContainsKey(ticketKey) == false)
           {
               _Accessors[ticketKey] = new Dictionary<string, ThrottleInfo>();
               _Accessors[ticketKey][context] = new ThrottleInfo(1);
           }
           else if (_Accessors[ticketKey].ContainsKey(context) == false)
           {
               _Accessors[ticketKey][context] = new ThrottleInfo(1);
           }
           else
           {
               ThrottleInfo ti = _Accessors[ticketKey][context];
               long ticks = DateTime.Now.Ticks;
               ti.Count++;

               if (ti.Count > threshold /*|| ti.LockoutTicks > ticks*/)
               {
                   //lock (_Accessors)
                   //{
                   //    ti.LockoutTicks = ticks + TimeSpan.FromMinutes(10).Ticks;
                   //}
                   throw new Exception("Clinet exceeded allowed access rate!");
               }
           }
       }
   }

   protected class ThrottleInfo
   {
	  public long LockoutTicks;
	  public long Count;
	  public ThrottleInfo(long count)
	  {
		 Count = count;
		 LockoutTicks = 0;
	  }
   }
}
