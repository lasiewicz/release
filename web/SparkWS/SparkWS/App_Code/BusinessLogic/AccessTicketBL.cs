using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

using Matchnet.Caching;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Exceptions;


/// <summary>
/// Business logic for access ticket manipulation.
/// Access ticket encapsulates the permissions base and carries with it configuration specific to an accessing client.
/// </summary>
public class AccessTicketBL
{
    public static AccessTicket GetAccessTicket(string ticketKey)
   {
	  return AccessTicketSA.Instance.GetAccessTicket(ticketKey);
   }

    public static bool ValidAccess(AccessTicket ticket)
    {
        bool res = false;
        string ticketString = "";
        string remoteIP = "";
        try
        {

            if (ticket == null)
                return false;

            if (ticket.IsBlocked)
                return false;

            LocalDataStoreSlot clientIP = Thread.GetNamedDataSlot(WebServiceBase.REMOTE_CLIENT_IP_KEY);
            object obj = Thread.GetData(clientIP);
            remoteIP = (obj == null) ? "Unknown IP" : obj.ToString();
            if (!String.IsNullOrEmpty(remoteIP))
            {
                ServiceUtil.LogEvent("WWW", string.Format("SparkWS Remote Client IP: {0}, Ticket Key: {1}", remoteIP, ticket.Key), System.Diagnostics.EventLogEntryType.Information);
            }
            for (int i = 0; i < ticket.RemoteHosts.Length; i++)
            {
                if (remoteIP.StartsWith(ticket.RemoteHosts[i]))
                { res = true; break; }
            }

            if (res)
            {
                ServiceUtil.LogEvent("WWW", string.Format("SparkWS Valid Ticket Access for Remote Client IP: {0}, Ticket Key: {1}", remoteIP, ticket.Key), System.Diagnostics.EventLogEntryType.Information);
            }
            else
            {
                ServiceUtil.LogEvent("WWW", string.Format("SparkWS Invalid Ticket Access for Remote Client IP: {0}, Ticket Key: {1}", remoteIP, ticket.Key), System.Diagnostics.EventLogEntryType.Warning);
            }
            
            return res;
        }

        catch (Exception ex)
        {

            ticketString = DumpTicket(ticket);

            WebBoundaryException webExc = new WebBoundaryException("System error validating AccessTicket:\r\n" + ticketString + "\r\nRemote IP: " + remoteIP, ex);

            return false;
        }

    }

   public static string DumpTicket(AccessTicket tkt)
    {
        string msg = "";
        try
        {
            if (tkt == null)
                return "null ticket";

            msg = "Ticket: " + tkt.Key + "\r\n";

            string ips = "Allowed IPs:\r\n";

            if (tkt.RemoteHosts == null)
            { ips = "none\r\n"; }
            else
            {
                for (int i = 0; i < tkt.RemoteHosts.Length; i++)
                { ips += tkt.RemoteHosts[i] + "\r\n"; }
            }
            string profiles = "ProfileSection:\r\n";
            if (tkt.ProfileSectionDefinitions == null)
            { profiles = "none\r\n"; }
            else
            {
               IDictionary dictDefinitions = (IDictionary)tkt.ProfileSectionDefinitions;
                foreach(int key in dictDefinitions.Keys)
                {
                    ProfileSectionDefinition definition = (ProfileSectionDefinition)dictDefinitions[key];

                    profiles += "id:" + definition.ProfileSectionID + "\r\ncache key:" + definition.GetCacheKey() + "\r\nattributes:\r\n";
                    if (definition.AttributeDefinitions == null)
                    { profiles += "none\r\n"; }
                    else
                    {
                        for (int j = 0; j < definition.AttributeDefinitions.Length; j++)
                        {
                            MemberAttributeParameter mpa = definition.AttributeDefinitions[j];
                            profiles += "id: " + mpa.AttributeID + "\r\n";
                        }
                    }
                }
            }

            msg += ips + profiles;
            return msg;
        }
        catch (Exception ex)
        {
            return msg;
        }
    }
}
