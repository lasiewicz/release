﻿using System;

using System.Collections.Generic;
using System.Net;
using System.Threading;



/// <summary>
/// Used to  notify Mingle of cache invalidation for a member.
/// 
/// 
/// You can call the method InvalidateMember() in order to enque
/// a InvalidateMember web call that will alert Mingle of the change.
/// 
/// This works like a service and needs to be Start() and Stop()
/// by calling the neccessary methods. The class is
/// marked disposable and you should not lose reference of it before
/// calling the Dispose() or Stop() methods.
/// </summary>
public class MingleNotificationBL : IDisposable
{
    private readonly string notificationURL;

    /// <summary>
    /// flag whether the notification services has started
    /// </summary>
    private bool notificationServiceStarted = false;


    private Thread executingThread = null;

    /// <summary>
    /// holds all pending member ids
    /// </summary>
    private Queue<int> pendingMemberInvalidations = null;

    /// <summary>
    /// holds all currently running invalidation requests
    /// </summary>
    private List<MemberInvalidationRequest> runningInvalidations = null;

    /// <summary>
    /// creates a new mingle notification service
    /// </summary>
    /// <param name="notificationURL">the notification URL
    /// to which we should submit the values</param>
    public MingleNotificationBL(string notificationURL)
    {
        this.notificationURL = notificationURL;
    }

    public void Start()
    {
        //set up the thread
        executingThread = new Thread(new ThreadStart(WorkCycle));

        //set up the invalidation request collections
        pendingMemberInvalidations = new Queue<int>(40);
        runningInvalidations = new List<MemberInvalidationRequest>(40);

        //set it to true
        notificationServiceStarted = true;
        executingThread.Start();
    }

    /// <summary>
    /// Stops all further web requests, and clear the current queues
    /// </summary>
    public void Stop()
    {
        this.Dispose();
    }

    /// <summary>
    /// invalidates a member out of mingle's cache
    /// </summary>
    /// <param name="memberID">the member ID to invalidate</param>
    public void InvalidateMember(int memberID)
    {
        //check if the service is running
        if (notificationServiceStarted == false)
        {
            return;
        }

        //some data checking which if it fails we just return
        if (memberID < 0)
        {
            return;
        }

        lock (pendingMemberInvalidations)
        {
            pendingMemberInvalidations.Enqueue(memberID);
        }
    }

    /// <summary>
    /// the url that is being notified at mingle
    /// </summary>
    public string NotificationURL
    {
        get { return this.notificationURL; }
    }

    #region IDisposable Members

    public void Dispose()
    {
        //stop processing
        notificationServiceStarted = false;

        //clear the data
        executingThread.Abort();
        pendingMemberInvalidations.Clear();
        runningInvalidations.Clear();

    }

    #endregion

    /// <summary>
    /// this is called by the exec thread so it does the blunt of the work
    /// </summary>
    private void WorkCycle()
    {
        while (true)
        {

            //just in case. we don't want this to go in some crazy infinitive loop if something weird
            //happends and we loose the thread.
            if (notificationServiceStarted == false)
            {
                break;
            }

            int memberID = -1;
            try
            {
                //if we are running a bit hot, then we wait a bit before we span any new requests
                if (runningInvalidations.Count > 40 || pendingMemberInvalidations.Count == 0)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                }


                //locking pending invalidations. we don't want to deque on 0 items.
                lock (pendingMemberInvalidations)
                {
                    if (pendingMemberInvalidations.Count > 0)
                    {
                        //otherwise we get as an invalidation request
                        memberID = pendingMemberInvalidations.Dequeue();
                    }
                }

                if (memberID > 0)
                {
                    //we create the meber invalidation request
                    MemberInvalidationRequest invalidationRequest = new MemberInvalidationRequest(memberID, notificationURL);

                    //add it to the running invalidations so we can keep track of it
                    lock (runningInvalidations)
                    {
                        runningInvalidations.Add(invalidationRequest);
                    }


                    //start the request
                    invalidationRequest.Start(new OnInvalidationComplete(InvalidationComplete));
                }
            }
            catch
            {
                //if there is an error here then we stop and restart the service
                Stop();
                Start();
            }
        }

    }

    /// <summary>
    /// handles completed invalidation request
    /// </summary>
    /// <param name="completedInvalidationRequest">the completed invalidation</param>
    private void InvalidationComplete(MemberInvalidationRequest completedInvalidationRequest)
    {
        lock (runningInvalidations)
        {
            runningInvalidations.Remove(completedInvalidationRequest);
        }
    }
}
