using System;
using System.Security;
using System.Security.Permissions;
using System.Security.Principal;
using System.Reflection;
using System.Diagnostics;
using System.Threading;
using Matchnet.Configuration.ValueObjects.SparkWS;


/// <summary>
/// Declerative attribute for controlling security / access to a method
/// </summary>
[AttributeUsageAttribute(AttributeTargets.Method, AllowMultiple = true)]
public class ApiPermissionAttribute : CodeAccessSecurityAttribute
{
   private bool _IsUnrestricted = false;
   private ApiAccessRoll _accessGroup = ApiAccessRoll.Anyone;
   private string _TicketKey;
   private AccessTicket _Ticket;
   private string _RemoteHostIP;

   // implemented but ignored for our purposes.
   public new bool Unrestricted
   {
	  get { return _IsUnrestricted; }
	  set { _IsUnrestricted = value; }
   }

   public int APIAccessGroup
   {
	  get { return (int)_accessGroup; }
	  set { _accessGroup = (ApiAccessRoll)value; }
   }

   public ApiAccessHeader AccessHeader
   {
	  set { _TicketKey = value.Key; }
	  get { return null; }
   }

   /// <summary>
   /// The API Access Ticket key.
   /// </summary>
   public string TicketKey
   {
	  set { _TicketKey = value; }
	  get { return _TicketKey; }
   }

   public ApiPermissionAttribute(SecurityAction action) : base(action) { }


   /// <summary>
   /// Allow access IFF:
   /// not blocked
   ///	 and
   /// remote host IP is allowed for this ticket
   ///	 and
   /// access roll defined is anyone OR an access roll defined matches the client's identity
   /// </summary>
   /// <returns></returns>
   private bool IsAllowed()
   {

#if DEBUG
	  Trace.WriteLine("Accessor is " + _TicketKey + " reuesting access to a method requiring " + _accessGroup.ToString());
#endif

	  if (_Ticket.IsBlocked == true) return false;


	  // if access is NOT anyone, and access roll doesn't match, fail.
	  if (_accessGroup != ApiAccessRoll.Anyone &&
		   ((_Ticket.AccessRoll & _accessGroup) == 0)) return false;


	  for (int i = 0; i < _Ticket.RemoteHosts.Length; i++)
	  {
		 ///TODO: Get access to remote IP of client to check against allowed hosts
		 if (_RemoteHostIP.StartsWith(_Ticket.RemoteHosts[i]))
			return true;
	  }

	  return false;
   }

   /// <summary>
   /// This override implements the permission creation based on the ticket key which is stored in 
   /// thread local storage. The string is loaded into the thread's data slot by the WebServiceBase 
   /// class, because access to the API header is available there.
   /// </summary>
   /// <remarks>This code gets called by the framework, not explicit from our implementation.</remarks>
   /// <returns>An IPermission object</returns>
   public override IPermission CreatePermission()
   {
	  LocalDataStoreSlot ldss;
	  Object obj;

	  ldss = Thread.GetNamedDataSlot(ApiAccessHeader.TICKET_KEY);
	  obj = Thread.GetData(ldss);
	  _TicketKey = (obj == null) ? AccessTicket.DEFAULT_TICKET : obj.ToString();

	  ldss = Thread.GetNamedDataSlot(WebServiceBase.REMOTE_CLIENT_IP_KEY);
	  obj = Thread.GetData(ldss);
	  _RemoteHostIP = (obj == null)? "Unknown IP" : obj.ToString();

	  _Ticket = AccessTicketBL.GetAccessTicket(_TicketKey);
	  if (_Ticket == null || _TicketKey == AccessTicket.DEFAULT_TICKET)
		 _Ticket = new AccessTicket();

	  if (!IsAllowed())
		 throw new SecurityException("Accessor's identity does not permit access to this method [" + _TicketKey + "] @ " + _RemoteHostIP);

	  return new ApiCodeAccessPermission(PermissionState.None);
   }

   #region ApiCodeAccessPermission



   /// <summary>
   /// ApiCodeAccessPermission
   /// </summary>
   [Serializable()]
   public class ApiCodeAccessPermission : CodeAccessPermission, IUnrestrictedPermission
   {
	  private bool _IsUnrestricted;
	  private string _Name;

	  public ApiCodeAccessPermission(PermissionState state)
	  {
		 //if (state == PermissionState.None)
		 //{
		 //    _Name = "";
		 //}
		 //else
		 //{
		 //    if (state == PermissionState.Unrestricted)
		 //    {
		 //        throw new ArgumentException("Unrestricted state is not allowed for identity permissions.");
		 //    }
		 //    else
		 //    {
		 //        throw new ArgumentException("Invalid permission state.");
		 //    }
		 //}
	  }

	  public string Name
	  {
		 get { return _Name; }
		 set { _Name = value; }
	  }

	  #region IUnrestrictedPermission Members

	  bool IUnrestrictedPermission.IsUnrestricted()
	  {
		 return _IsUnrestricted;
	  }

	  #endregion


	  public override IPermission Copy()
	  {
		 return null;
	  }

	  public override void FromXml(SecurityElement element)
	  {
		 // The following code for unrestricted permission is only included as an example for
		 // permissions that allow the unrestricted state. It is of no value for this permission.
		 String elUnrestricted = element.Attribute("Unrestricted");
		 if (null != elUnrestricted)
		 {
			_IsUnrestricted = bool.Parse(elUnrestricted);
			return;
		 }

		 String elName = element.Attribute("Name");
		 _Name = (elName == null) ? null : elName;

	  }

	  public override IPermission Intersect(IPermission target)
	  {
		 System.Diagnostics.Trace.WriteLine("************* Entering Intersect *********************");
		 if (target == null)
		 {
			return null;
		 }
#if DEBUG
        System.Diagnostics.Trace.WriteLine ("This is = " + (( ApiCodeAccessPermission)this).Name);
        System.Diagnostics.Trace.WriteLine ("Target is " + (( ApiCodeAccessPermission)target)._Name);
#endif
		 if (!(target is ApiCodeAccessPermission))
		 {
			throw new ArgumentException(String.Format("Argument is wrong type.", this.GetType().FullName));
		 }

		 ApiCodeAccessPermission operand = (ApiCodeAccessPermission)target;

		 if (operand.IsSubsetOf(this))
		 {
			return operand.Copy();
		 }
		 else
		 {
			if (this.IsSubsetOf(operand))
			{
			   return this.Copy();
			}
			else
			{
			   return null;
			}
		 }
	  }


	  public override bool IsSubsetOf(IPermission target)
	  {
#if DEBUG
            System.Diagnostics.Trace.WriteLine ("************* Entering IsSubsetOf *********************");
#endif
		 if (target == null)
		 {
			System.Diagnostics.Trace.WriteLine("IsSubsetOf: target == null");
			return false;
		 }
#if DEBUG

            System.Diagnostics.Trace.WriteLine ("This is = " + (( ApiCodeAccessPermission)this).Name);
            System.Diagnostics.Trace.WriteLine ("Target is " + (( ApiCodeAccessPermission)target)._Name);
#endif
		 try
		 {
			ApiCodeAccessPermission operand = (ApiCodeAccessPermission)target;

			// The following check for unrestricted permission is only included as an example for
			// permissions that allow the unrestricted state. It is of no value for this permission.
			if (true == operand._IsUnrestricted)
			{
			   return true;
			}
			else if (true == this._IsUnrestricted)
			{
			   return false;
			}

			if (this._Name != null)
			{
			   if (operand._Name == null) return false;

			   if (this._Name == "") return true;
			}

			if (this._Name.Equals(operand._Name)) return true;
			else
			{
			   // Check for wild card character '*'.
			   int i = operand._Name.LastIndexOf("*");

			   if (i > 0)
			   {
				  string prefix = operand._Name.Substring(0, i);

				  if (this._Name.StartsWith(prefix))
				  {
					 return true;
				  }
			   }
			}

			return false;
		 }
		 catch (InvalidCastException)
		 {
			throw new ArgumentException(String.Format("Argument_WrongType", this.GetType().FullName));
		 }

	  }

	  public override SecurityElement ToXml()
	  {
		 // Use the SecurityElement class to encode the permission to XML.
		 SecurityElement esd = new SecurityElement("IPermission");
		 String name = typeof(ApiCodeAccessPermission).AssemblyQualifiedName;
		 esd.AddAttribute("class", name);
		 esd.AddAttribute("version", "1.0");

		 // The following code for unrestricted permission is only included as an example for
		 // permissions that allow the unrestricted state. It is of no value for this permission.
		 if (_IsUnrestricted)
		 {
			esd.AddAttribute("Unrestricted", true.ToString());
		 }
		 if (_Name != null)
		 {
			esd.AddAttribute("Name", _Name);
		 }

		 return esd;

	  }
   }
}

   #endregion

