using System;
using Matchnet.Session.ServiceAdapters;


/// <summary>
/// Summary description for MembersOnlineBL
/// </summary>
public class MembersOnlineBL
{
   public static int GetMembersOnlineCount(MembersOnlineCountRequest request)
   {
	  int result = 0;
	  result = SessionSA.Instance.GetSessionCount(request.CommunityID);
	  return result;
   }
}
