
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters.Admin;
using Matchnet.Purchase.ValueObjects;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects;
using System.Text;
using System.Collections.Generic;
using Spark.Common.Adapter;


/// <summary>
/// Summary description for SubscriptionBL
/// </summary>
/// 
public class SubscriptionBL
{

    public static bool TrialPayConfirmation(Int32 memberID, Int32 siteID, string ticketKey)
    {
        Int32 intResult = 0;
        bool blnResult = false;

        Int32 adminMemberID;
        Int32 duration;
        Int32 planID = Constants.NULL_INT;

        Int32 transactionReasonID = 0;
        try
        {

            System.Diagnostics.Trace.WriteLine("SubscriptionBL.cs - TrialPayConfirmation started.");

            adminMemberID = Int32.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("TRIALPAY_ADMIN_ACCOUNT"));
            if (adminMemberID <= 0)
            {
                throw new SparkWSClientException("SubscriptionBL.cs - TrialPayConfirmation error. Can not retrieve Admin memberID.");
            }
            duration = Int32.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("TRIALPAY_ADJUST_DAYS"));

            #region Obsolete - Legacy TrialPay Adjustment
            //MemberSub ms = PurchaseSA.Instance.GetSubscription(memberID, siteID);

            //if (ms != null && ms.PaymentType == PaymentType.CreditCard)
            //{
            //    planID = ms.PlanID;
            //}

            //intResult = PurchaseAdminSA.Instance.UpdateSubscription(memberID,
            //    siteID,
            //    adminMemberID,
            //    duration,
            //    Matchnet.DurationType.Day,
            //    Matchnet.Purchase.ValueObjects.TranType.TrialPayAdjustment,
            //    transactionReasonID,
            //    false,
            //    planID);
            //System.Diagnostics.Debug.WriteLine("SubscriptionBL.cs - TrialPayConfirmation result=" + intResult.ToString());
            //if (intResult == Matchnet.Constants.RETURN_OK)
            //{
            //    blnResult = true;
            //}
            #endregion

            List<int> PrivilegeTypeIDs = new List<int>();
            PrivilegeTypeIDs.Add((int)Spark.Common.AccessService.PrivilegeType.BasicSubscription);

            //Update Unified Access
            Spark.Common.AccessService.AccessServiceClient accessServiceCient = AccessServiceWebAdapter.GetProxyInstanceForBedrock();
            Spark.Common.AccessService.AccessResponse accessResponse = accessServiceCient.AdjustTrialPayPrivilege(memberID, PrivilegeTypeIDs.ToArray(), duration, (int)DurationType.Day, adminMemberID, "", transactionReasonID, siteID, 1);
            if (accessResponse.InternalResponse.responsecode != "0")
            {
                ServiceUtil.LogEvent("WWW", "TrialPayConfirmation(" + memberID.ToString() + "," + siteID.ToString() + "). Access Response: " + accessResponse.InternalResponse.responsecode.ToString() + ", " + accessResponse.InternalResponse.responsemessage + ", " + accessResponse.message, System.Diagnostics.EventLogEntryType.Warning);
            }
            else
            {
                ServiceUtil.LogEvent("WWW", "TrialPayConfirmation(" + memberID.ToString() + "," + siteID.ToString() + "). Access privilege successfully updated.", System.Diagnostics.EventLogEntryType.Information);
                blnResult = true;
                
            }

        }
        catch (Exception ex)
        {
            if (!String.IsNullOrEmpty(ex.Message.ToString()))
            {
                ServiceUtil.LogEvent("WWW", "TrialPayConfirmation(" + memberID.ToString() + "," + siteID.ToString() + "). Error message:" + ex.Message.ToString(), System.Diagnostics.EventLogEntryType.Error);
            }
            throw new SparkWSClientException("SubscriptionBL.cs - TrialPayConfirmation error. ", ex);
        }
        return blnResult;
    }

    public static bool UpdateSubscription(Int32 memberID, Int32 siteID, int subscriptionInfoID, int accessTicketID)
    {
        bool blnResult = false;
        int intResult = 0;

        try
        {
            Matchnet.Configuration.ValueObjects.SparkWS.SubscriptionInfo subInfoID = AccessTicketSA.Instance.GetSubscriptionInfo(accessTicketID, subscriptionInfoID);

            intResult = PurchaseAdminSA.Instance.UpdateSubscription(memberID, siteID, subInfoID.AdminMemberID, subInfoID.Duration, (DurationType)subInfoID.DurationTypeID,
                                                                    (TranType)subInfoID.TranTypeID, subInfoID.TransactionReasonID, subInfoID.ReOpenFlag, subInfoID.PlanID);
            if (intResult == Matchnet.Constants.RETURN_OK)
            {
                blnResult = true;
            }
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("SubscriptionBL.cs - UpdateSubscription error. ", ex);
        }
        return blnResult;
    }

    /// <summary>
    /// Called by the promoapproval system once all items for a given promo has been approved.
    /// </summary>
    /// <param name="promoID"></param>
    /// <param name="brandID"></param>
    /// <param name="approvalStatus">1 - Pending, 2 - Approved, 3 - Rejected</param>
    public static void SavePromoApproval(Int32 promoID, Int32 brandID, Int16 approvalStatus)
    {
        try
        {
            PromoEngineSA.Instance.SavePromoApproval(promoID, brandID, approvalStatus);
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("SubscriptionBL.cs - ApprovePromo error. ", ex);
        }
    }

    #region Encore related
    public static MatchbackCollection GetMatbackRequest()
    {
        try
        {
            return PurchaseSA.Instance.GetMatbackRequest();
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("SubscriptionBL.cs - GetMatbackRequest error. ", ex);
        }
    }

    public static int StartMatchbackFileGeneration(string outputFileName)
    {
        try
        {
            return PurchaseSA.Instance.StartMatchbackFileGeneration(outputFileName);
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("SubscriptionBL.cs - StartMatchbackFileGeneration error. ", ex);
        }
    }

    public static void InsertMatchbackFile(byte[] outputFileContent, int lineID)
    {
        try
        {
            PurchaseSA.Instance.InsertMatchbackFile(outputFileContent, lineID);
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("SubscriptionBL.cs - InsertMatchbackFile error. ", ex);
        }
    }

    public static void MarkMatchbackRequestAsComplete(MatchbackCollection mbCol)
    {
        try
        {
            PurchaseSA.Instance.MarkMatchbackRequestAsComplete(mbCol);
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("SubscriptionBL.cs - MarkMatchbackRequestAsComplete error. ", ex);
        }
    }

    public static string GetOutputCreditSalesFileName()
    {
        StringBuilder sb = new StringBuilder();
        string companyString = "SPAR";

        if (RuntimeSettings.GetSetting("ENCORE_OUTPUT_FILE_MODE").ToLower() == "prod")
        {
            sb.Append(companyString + "WEB");

            DateTime now = DateTime.Now;

            // xxxxWEBCCYYMMDDHHMMss is the file name format
            sb.Append(now.Year.ToString() + PrependZero(now.Month.ToString(), 2) + PrependZero(now.Day.ToString(), 2)
                + PrependZero(now.Hour.ToString(), 2) + PrependZero(now.Minute.ToString(), 2) + PrependZero(now.Second.ToString(), 2));
        }
        else
        {
            // xxxxWEBTEST## is the file name format for testing
            // We could have an overlap of ## but let's just do random function
            Random random = new Random();
            int number = random.Next(99);
            sb.Append(companyString + "WEBTEST" + PrependZero(number.ToString(), 2));
        }

        sb.Append(".DAT");

        return sb.ToString();
    }

    public static string PrependZero(string valueString, int supposeLength)
    {
        int diff = supposeLength - valueString.Length;
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < diff; i++)
        {
            sb.Append("0");
        }
        sb.Append(valueString);

        return sb.ToString();
    }

    #endregion
}
