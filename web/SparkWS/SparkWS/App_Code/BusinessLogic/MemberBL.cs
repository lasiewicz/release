using System;
using System.Collections;
using System.Collections.Generic;

using Matchnet.Configuration.ServiceAdapters;

using Matchnet.Configuration.ServiceAdapters.Analitics;
//using Matchnet.Configuration.ServiceAdapters.ana;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Exceptions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Security;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Member.ValueObjects.Privilege;
using System.Text;
using Matchnet.MemberPhotoHelper;
using Spark.CloudStorage;
using Spark.SAL;
using Matchnet.OmnitureHelper;
using Matchnet.ActivityRecording.ServiceAdapters;
using Spark.Common.OrderHistoryService;
using Spark.Common.Adapter;
using Matchnet.Email.ServiceAdapters;


 public enum PhotoType
{
    Full = 1,
    Thumbnail
}


/// <summary>
/// Allows for varios member operation such as finding,
/// login, and updates
/// </summary>
public class MemberBL
{
    public const string SPARK_WS_CRYPT_KEY = "A877C90D";
    private const int MEMBER_ATTRIBUTE_EXTERNAL_VIDEO_URL = 110;
    private const int MEMBER_ATTRIBUTEID_USERNAME = 302;
    private const int MEMBER_ATTRIBUTEID_PASSEDFRAUDCHECK = 571; // this id is actually different for dev versus prod
    private const string MEMBER_ATTRIBUTENAME_PASSEDFRAUDCHECK = "PassedFraudCheckSite";

    /// <summary>
    /// gets a member ID by their username and community. MPR-1898
    /// </summary>
    /// <param name="request">the request containing the username and the community ID</param>
    /// <param name="ticketKey">the ticket key that is requesting the memberID</param>
    public static MemberIDResponse GetMemberIDByUserNameAndCommunityID(MemberIDRequest request, string ticketKey)
    {
        AccessTicket accessTicket = AccessTicketBL.GetAccessTicket(ticketKey);
        if (accessTicket.ProfileSectionDefinitions == null || accessTicket.ProfileSectionDefinitions.Count == 0)
            throw new Exception(ticketKey + " has no section definitions.");

        int memberID = 0;

        System.Diagnostics.Trace.WriteLine("GetMemberIDByUserNameAndCommunityID called: username:" + request.Username
    + ", communityID:" + request.CommunityID);

        try
        {
            if (request.Username != string.Empty && request.CommunityID != int.MinValue)
            {
                memberID = MemberSA.Instance.GetMemberID(request.Username, request.CommunityID);
            }
            else
            {
                throw new SparkWSClientException("Request properties username and communityID are invalid");
            }
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("problem finding user by username", ex);
        }

        System.Diagnostics.Trace.WriteLine("GetMemberIDByUserNameAndCommunityID call completed: username:" + request.Username
            + ", communityID:" + request.CommunityID + ", memberID:" + memberID.ToString()) ;

        return new MemberIDResponse("1.0.0", memberID);
    }

    /// <summary>
    /// gets a member ID by their username
    /// </summary>
    /// <param name="request">the request containing the username</param>
    /// <param name="ticketKey">the ticket key that is requesting the memberID</param>
    public static MemberIDResponse GetMemberID(MemberIDRequest request, string ticketKey)
    {
        AccessTicket accessTicket = AccessTicketBL.GetAccessTicket(ticketKey);
        if (accessTicket.ProfileSectionDefinitions == null || accessTicket.ProfileSectionDefinitions.Count == 0)
            throw new Exception(ticketKey + " has no section definitions.");

        int memberID = 0;

        try
        {
            if (request.Username != string.Empty)
            {
                memberID = MemberSA.Instance.GetMemberID(request.Username);
            }
            else
            {
                memberID = MemberSA.Instance.GetMemberIDByEmail(request.Email);
            }
        }
        catch (Exception ex)
        {
            throw new SparkWSClientException("problem finding user by username", ex);
        }

        return new MemberIDResponse("1.0.0", memberID);
    }

    /// <summary>
    /// Retrives a member's miniprofile information. Originally requested by IL, but can be used for other sites as well.
    /// All attribute values are returned as literal locale string so there is no need to do any lookups.
    /// This would require no profile section configuration.
    /// </summary>
    /// <param name="request"></param>
    /// <param name="ticketKey"></param>
    /// <returns></returns>
    public static GetMiniProfileResponse GetMiniProfile(GetMiniProfileRequest request, string ticketKey)
    {
        #region Access Ticket Validation

        AccessTicket accessTicket = AccessTicketBL.GetAccessTicket(ticketKey);
        if (accessTicket.ProfileSectionDefinitions == null || accessTicket.ProfileSectionDefinitions.Count == 0)
            throw new Exception(ticketKey + " has no section definitions.");

        #endregion

        Brand brand = BrandConfigSA.Instance.GetBrandByID(request.BrandID);

        if (request.MemberID == 0)
        {
            if (!string.IsNullOrEmpty(request.EncryptedMemberID))
            {
                request.MemberID = Int32.Parse(Matchnet.Security.Crypto.Decrypt(SPARK_WS_CRYPT_KEY, request.EncryptedMemberID));
            }
        }

        Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(request.MemberID, MemberLoadFlags.None);

        #region UserName

        string userName;
        userName = member.GetUserName(brand);
        
        #endregion

        #region Age

        int age = 0;
        DateTime birthDate = member.GetAttributeDate(brand, "Birthdate");

        if (birthDate != DateTime.MinValue)
        {
            age = (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
        }

        #endregion

        #region Gender

        string gender = getGender(member.GetAttributeInt(brand, "GenderMask")) == 1 ? "Male" : "Female";

        #endregion

        #region SeekingGender

        string seekingGender = getSeekingGender(member.GetAttributeInt(brand, "GenderMask")) == 1 ? "Male" : "Female";

        #endregion

        #region ThumbPhotoURL

        string thumbPhotoURL = string.Empty;
        string fullThumbPhotoURL = string.Empty;

        Photo photo = member.GetDefaultPhoto(brand.Site.Community.CommunityID, true);

        if(photo != null)
        {
            fullThumbPhotoURL = getFullPhotoPath(photo, PhotoType.Thumbnail, brand);
            thumbPhotoURL = photo.ThumbFileWebPath; 
        }

        #endregion

        #region Region - City, State, Country

        RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(member.GetAttributeInt(brand, "RegionID"), brand.Site.LanguageID);

        string city = string.Empty;
        string state = string.Empty;
        string country = string.Empty;

        city = regionLanguage.CityName;
        state = regionLanguage.StateAbbreviation;
        country = regionLanguage.CountryAbbreviation;

        #endregion

        #region LastLogonDateTime

        string lastLastLogonDateTime = Convert.ToString(member.GetAttributeDate(brand, "BrandLastLogonDate"));

        #endregion

        #region Seeking Ages

        int ageSeekingFrom = member.GetAttributeInt(brand, "DesiredMinAge");
        int ageSeekingTo = member.GetAttributeInt(brand, "DesiredMaxAge");

        #endregion

        #region IsSubscriber

        bool isSubscriber = member.IsPayingMember(brand.Site.SiteID);

        #endregion

        #region Phone Number

        string phoneNumber = member.GetAttributeText(brand, "Phone");

        #endregion

        #region Email

        string email = member.GetAttributeText(brand, "EmailAddress");

        #endregion

        #region DateOfBirth

        DateTime dateOfBirth = member.GetAttributeDate(brand, "Birthdate");

        #endregion


        MiniProfile miniProfile = new MiniProfile("1.0.0", userName, age, gender, seekingGender, thumbPhotoURL, city, state, country, lastLastLogonDateTime, ageSeekingFrom, ageSeekingTo, isSubscriber, phoneNumber, email, dateOfBirth, fullThumbPhotoURL);

        return new GetMiniProfileResponse("1.0.0", miniProfile);
    }

    public static ProfileSection GetProfileSectionUsernameByProperty(ProfileSectionRequest request, string ticketKey)
    {
        ProfileSection retSection = GetProfileSection(request, ticketKey);

        if (retSection != null)
        {
            Matchnet.Member.ServiceAdapters.Member member = null;
            try
            {
                member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(retSection.MemberID, MemberLoadFlags.None);
            }
            catch (Exception ex)
            {
                throw new Exception("MemberBL.GetProfileSectionUsernameByProperty() unable to get member", ex);
            }

            Brand brand = null;
            int communityID;
            int brandID;
            
            switch (request.ProfileSectionID)
            {
                case 4:
                    communityID = 1;
                    break;
                case 5:
                    communityID = 3;
                    break;
                case 24:
                    communityID = 10;
                    break;
                case 30:
                    communityID = 21;
                    break;
                case 32:
                    communityID = 23;
                    break;
                case 34:
                    communityID = 24;
                    break;
                default:
                    throw new Exception("No community mapping found for this profile section ID value - " + request.ProfileSectionID);
            }
            member.GetLastLogonDate(communityID, out brandID);
            brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            retSection.Profile.Values.Add(new MemberAttributeOfString(
                new MemberAttributeParameter(302, AttributeDataType.String, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID),
                member.GetUserName(brand)));

            //ServiceUtil.LogEvent("WWW", member.GetUserName(brand), System.Diagnostics.EventLogEntryType.Information);
        }

        return retSection;
    }

    public static ProfileSection GetProfileSection(ProfileSectionRequest request, string ticketKey)
    {
        int memberID = 0;
        int sessionMemberID = 0;
        bool isValidatedSessionMemebr = false;

        AccessTicket tkt = AccessTicketBL.GetAccessTicket(ticketKey);

        // Get \ validate profile section ID request for this accessor
        if (tkt.ProfileSectionDefinitions == null || tkt.ProfileSectionDefinitions.Count == 0)
            throw new Exception(ticketKey + " has no section definitions ");

        ProfileSectionDefinition definition = tkt.ProfileSectionDefinitions[request.ProfileSectionID];
        if (definition == null)
            throw new Exception(ticketKey + " not permitted(1) for section " + request.ProfileSectionID.ToString());

        memberID = request.MemberID;

        if (request.MemberSessionID != string.Empty)
        {
            // decrypt MemberSessionID
            try
            {
                string sessionID = Crypto.Decrypt(SPARK_WS_CRYPT_KEY, request.MemberSessionID);
                sessionMemberID = SessionSA.Instance.GetMemberID(sessionID);
                if (sessionMemberID > 0)
                {
                    isValidatedSessionMemebr = true;
                    memberID = sessionMemberID;
                }
            }
            catch (Exception ex)
            {
                throw new SparkWSClientException("problem decyphering sessionid", ex);
            }
        }

        ProfileSection result = new ProfileSection(definition.AttributeDefinitions.Length);
        result.ProfileSectionID = definition.ProfileSectionID;

        result.MemberID = memberID;

        Matchnet.Member.ServiceAdapters.Member member = null;
        try
        {
            member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
        }
        catch (Exception ex)
        {
            throw new Exception("MemberBL.GetProfileSection() unable to get member. MemberID:" + memberID.ToString()
                +", BrandID:" + request.BrandID.ToString() + ", ProfileSectionID:" + request.ProfileSectionID.ToString(), ex);
        }

        populateMemberAttributes(member, definition, ref result, isValidatedSessionMemebr, tkt);

        populateMemberPhotos(member, definition, ref result, isValidatedSessionMemebr, tkt);

        populateMemberInfo(member, definition, ref result, isValidatedSessionMemebr, tkt);

        return result;
    }

    /// <summary>
    /// This method was originally implemented for HurryDate. It was then turned into returning an XML string in
    /// the next method. To get member attributes, GetProfileSection call should be used.
    /// </summary>
    /// <param name="request"></param>
    /// <param name="ticketKey"></param>
    /// <returns></returns>
    public static ProfileSection GetMemberAttributes(ProfileSectionRequest request, string ticketKey)
    {
        int memberID = 0;
        int sessionMemberID = 0;
        bool isValidatedSessionMemebr = false;

        AccessTicket tkt = AccessTicketBL.GetAccessTicket(ticketKey);

        // Get \ validate profile section ID request for this accessor
        if (tkt.ProfileSectionDefinitions == null || tkt.ProfileSectionDefinitions.Count == 0)
            throw new Exception(ticketKey + " has no section definitions ");

        ProfileSectionDefinition definition = tkt.ProfileSectionDefinitions[request.ProfileSectionID];
        if (definition == null)
            throw new Exception(ticketKey + " not permitted(1) for section " + request.ProfileSectionID.ToString());

        memberID = request.MemberID;

        ProfileSection result = new ProfileSection(definition.AttributeDefinitions.Length);
        result.ProfileSectionID = definition.ProfileSectionID;

        result.MemberID = memberID;

        Matchnet.Member.ServiceAdapters.Member member = null;
        try
        {
            member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
        }
        catch (Exception ex)
        {
            throw new Exception("MemberBL.GetProfileSection() unable to get member", ex);
        }

        int globalStatusMask = member.GetAttributeInt(0, 0, 0, 268, 0); // global status mask
        if ((globalStatusMask & 1) != 0)  // Bad emails are now allowed.
            throw new Exception("MemberBL.GetProfileSection() member not accassible");

        populateMemberAttributes(member, definition, ref result, isValidatedSessionMemebr, tkt);

        return result;
    }

    public static string GetMemberAttributes(string ticketKey, int siteid, int memberid, int profileSectionID)
    {
        ProfileSectionDefinition definition = null;
        MemberAttributeResult attr = null;
        string remoteIP = System.Web.HttpContext.Current.Request.UserHostAddress;
        const string EXCEPTION_FORMAT_STRING = "Exception happened in SparkWS \r\nRequest:\r\nRemote Host{0}\r\nTicket:{1}\r\nMemberID:{2}\r\nSiteID:{3}\r\n";

        string exceptionString = String.Format(EXCEPTION_FORMAT_STRING, remoteIP, ticketKey, memberid, siteid);
        string ticketLog = "";
        try
        {
            attr = new MemberAttributeResult();

            if (!attr.Init(siteid, memberid))
            { return attr.ToXMLString(ReturnCodes.retValidationAccess, String.Format("Invalid siteID ({0}) or memberID ({1}) in request", siteid, memberid)); }

            AccessTicket tkt = AccessTicketBL.GetAccessTicket(ticketKey);

            if (!AccessTicketBL.ValidAccess(tkt))
            {
                ticketLog = AccessTicketBL.DumpTicket(tkt);
                ServiceUtil.LogEvent("WWW", exceptionString + "Ticket key is not permitted:\r\n" + ticketLog, System.Diagnostics.EventLogEntryType.Warning);
                return attr.ToXMLString(ReturnCodes.retValidationAccess, String.Format("Ticket key {0} is not permitted for " + remoteIP, ticketKey));
            }

            ticketLog = AccessTicketBL.DumpTicket(tkt);
            // Get \ validate profile section ID request for this accessor
            if (tkt.ProfileSectionDefinitions == null || tkt.ProfileSectionDefinitions.Count == 0)
            {
                ServiceUtil.LogEvent("WWW", exceptionString + "Ticket key is not configured:\r\n" + ticketLog, System.Diagnostics.EventLogEntryType.Warning);
                return attr.ToXMLString(ReturnCodes.retValidationAccess, String.Format("Ticket key {0} is not configured", ticketKey));
            }

            //in case in the future ProfileSection ID will be passed, for now it will be 0 and only 1 section per ticket    
            if (profileSectionID == 0)
            {  //get first one          
                IDictionary dictDefinitions = (IDictionary)tkt.ProfileSectionDefinitions;
                foreach (int key in dictDefinitions.Keys)
                { definition = (ProfileSectionDefinition)dictDefinitions[key]; break; }
            }
            else
            { definition = tkt.ProfileSectionDefinitions[profileSectionID]; }

            if (definition == null)
            {
                ServiceUtil.LogEvent("WWW", exceptionString + "Ticket key is not configured:\r\n" + ticketLog, System.Diagnostics.EventLogEntryType.Warning);
                return attr.ToXMLString(ReturnCodes.retValidationAccess, String.Format("Ticket key {0} is not configured", ticketKey));
            }

            Matchnet.Member.ServiceAdapters.Member member = null;
            try
            {
                member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
            }
            catch (Exception ex)
            { return attr.ToXMLString(ReturnCodes.retSystemError, String.Format("Unable to get member for id {0} and siteid {1}", memberid, siteid)); }

            if (member == null)
            { return attr.ToXMLString(ReturnCodes.retValidationMember, String.Format("Unable to get member for id {0} and siteid {1}", memberid, siteid)); }

            if (member.EmailAddress == null)
            { return attr.ToXMLString(ReturnCodes.retValidationMember, String.Format("Unable to get member for id {0} and siteid {1}", memberid, siteid)); }

            int globalStatusMask = member.GetAttributeInt(0, 0, 0, 268, 0); // global status mask
            if ((globalStatusMask & 1) != 0)  // Bad emails are now allowed.
            { return attr.ToXMLString(ReturnCodes.retValidationAccess, String.Format("Member  for id {0} and siteid {1} is not accessible", memberid, siteid)); }

            if (member.IsPayingMember(siteid))
            {
                //Matchnet.Purchase.ValueObjects.MemberTran tran = getMemberSubscribtion(siteid, memberid);
                OrderInfo orderInfo = getMemberSubscriptionUPS(siteid, memberid);
                populateSubscribtionAttributes(orderInfo, ref attr);
            }
            else
            { attr.AddAttribute("subscriptionstatus", MemberAttributeResult.SUB_STATUS_REGISTERED.ToString()); }

            populateMemberAttributes(member, definition, ref attr);

            return attr.ToXMLString();
        }
        catch (Exception ex)
        {
            WebBoundaryException webExc = new WebBoundaryException(exceptionString + ticketLog, ex);
            return attr.ToXMLString(ReturnCodes.retSystemError, "System Error");
        }
    }

    public static string GetZoozamenMemberAttributes(string ticketKey, int siteid, int memberid, int profileSectionID)
    {
        ProfileSectionDefinition definition = null;
        MemberAttributeResult attr = null;
        string remoteIP = System.Web.HttpContext.Current.Request.UserHostAddress;
        const string EXCEPTION_FORMAT_STRING = "Exception happened in SparkWS \r\nRequest:\r\nRemote Host{0}\r\nTicket:{1}\r\nMemberID:{2}\r\nSiteID:{3}\r\n";

        string exceptionString = String.Format(EXCEPTION_FORMAT_STRING, remoteIP, ticketKey, memberid, siteid);
        string ticketLog = "";
        try
        {
            attr = new MemberAttributeResult();

            if (!attr.Init(siteid, memberid))
            { return attr.ToXMLString(ReturnCodes.retValidationAccess, String.Format("Invalid siteID ({0}) or memberID ({1}) in request", siteid, memberid)); }

            AccessTicket tkt = AccessTicketBL.GetAccessTicket(ticketKey);

            if (!AccessTicketBL.ValidAccess(tkt))
            {
                ticketLog = AccessTicketBL.DumpTicket(tkt);
                ServiceUtil.LogEvent("WWW", exceptionString + "Ticket key is not permitted:\r\n" + ticketLog, System.Diagnostics.EventLogEntryType.Warning);
                return attr.ToXMLString(ReturnCodes.retValidationAccess, String.Format("Ticket key {0} is not permitted", ticketKey));
            }

            ticketLog = AccessTicketBL.DumpTicket(tkt);
            // Get \ validate profile section ID request for this accessor
            if (tkt.ProfileSectionDefinitions == null || tkt.ProfileSectionDefinitions.Count == 0)
            {
                ServiceUtil.LogEvent("WWW", exceptionString + "Ticket key is not configured:\r\n" + ticketLog, System.Diagnostics.EventLogEntryType.Warning);
                return attr.ToXMLString(ReturnCodes.retValidationAccess, String.Format("Ticket key {0} is not configured", ticketKey));
            }

            //in case in the future ProfileSection ID will be passed, for now it will be 0 and only 1 section per ticket    
            if (profileSectionID == 0)
            {  //get first one          
                IDictionary dictDefinitions = (IDictionary)tkt.ProfileSectionDefinitions;
                foreach (int key in dictDefinitions.Keys)
                { definition = (ProfileSectionDefinition)dictDefinitions[key]; break; }
            }
            else
            { definition = tkt.ProfileSectionDefinitions[profileSectionID]; }

            if (definition == null)
            {
                ServiceUtil.LogEvent("WWW", exceptionString + "Ticket key is not configured:\r\n" + ticketLog, System.Diagnostics.EventLogEntryType.Warning);
                return attr.ToXMLString(ReturnCodes.retValidationAccess, String.Format("Ticket key {0} is not configured", ticketKey));
            }

            Matchnet.Member.ServiceAdapters.Member member = null;
            try
            {
                member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
            }
            catch (Exception ex)
            { return attr.ToXMLString(ReturnCodes.retSystemError, String.Format("Unable to get member for id {0} and siteid {1}", memberid, siteid)); }

            if (member == null)
            { return attr.ToXMLString(ReturnCodes.retValidationMember, String.Format("Unable to get member for id {0} and siteid {1}", memberid, siteid)); }

            if (member.EmailAddress == null)
            { return attr.ToXMLString(ReturnCodes.retValidationMember, String.Format("Unable to get member for id {0} and siteid {1}", memberid, siteid)); }

            int globalStatusMask = member.GetAttributeInt(0, 0, 0, 268, 0); // global status mask
            if ((globalStatusMask & 1) != 0)  // Bad emails are now allowed.
            { return attr.ToXMLString(ReturnCodes.retValidationAccess, String.Format("Member  for id {0} and siteid {1} is not accessible", memberid, siteid)); }

            // GET MEMBER SUBSCRIPTION DATA  
            if (member.IsPayingMember(siteid))
            {
                //Matchnet.Purchase.ValueObjects.MemberTran tran = getMemberSubscribtion(siteid, memberid);
                //Matchnet.Purchase.ValueObjects.MemberSub membersub = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetSubscription(memberid, siteid);

                OrderInfo orderInfo = getMemberSubscriptionUPS(siteid, memberid);
                Spark.Common.RenewalService.RenewalSubscription renewalSub = Spark.Common.Adapter.RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(memberid, siteid);

                populateZoozamenSubscribtionAttributes(orderInfo, renewalSub, ref attr);
            }
            else
            {
                attr.AddAttribute("subscriptionstatus", "None");
            }

            // GET MEMBER ATTRIBUTES DATA  
            populateZoozamenMemberAttributes(member, definition, ref attr);

            Matchnet.Content.ValueObjects.BrandConfig.Brands brands = BrandConfigSA.Instance.GetBrandsBySite(siteid);
            foreach (Matchnet.Content.ValueObjects.BrandConfig.Brand brand in brands)
            {
                if (brand.BrandID == 1004)
                {
                    // GET MEMBER PHOTO DATA
                    attr.AddAttribute("mainphotourl", MemberPhotoHelper.GetPhotoURLForUserplane(member, brand));

                    // GET MEMBER SEARCH PREFERENCES DATA
                    MemberSearch _memberSearch = MemberSearchCollection.Load(member.MemberID, brand).PrimarySearch;

                    attr.AddAttribute("searchingwithin", Convert.ToString(_memberSearch.Distance));
                }
            }



            return attr.ToXMLString();
        }
        catch (Exception ex)
        {
            WebBoundaryException webExc = new WebBoundaryException(exceptionString + ticketLog, ex);
            return attr.ToXMLString(ReturnCodes.retSystemError, "System Error");
        }
    }

    public static DemographicData GetDemographicData(DemographicDataRequest request, string ticketKey)
    {
        int memberID = 0;
        int sessionMemberID = 0;
        bool isValidatedSessionMemebr = false;

        AccessTicket tkt = AccessTicketBL.GetAccessTicket(ticketKey);

        // Get \ validate profile section ID request for this accessor
        if (tkt.ProfileSectionDefinitions == null || tkt.ProfileSectionDefinitions.Count == 0)
            throw new Exception(ticketKey + " has no section definitions ");

        ProfileSectionDefinition definition = tkt.ProfileSectionDefinitions[request.ProfileSectionID];
        if (definition == null)
            throw new Exception(ticketKey + " not permitted(1) for section " + request.ProfileSectionID.ToString());

        memberID = request.MemberID;

        if (request.MemberSessionID != null && request.MemberSessionID != string.Empty)
        {
            // decrypt MemberSessionID
            try
            {
                string sessionID = Crypto.Decrypt(SPARK_WS_CRYPT_KEY, request.MemberSessionID);
                sessionMemberID = SessionSA.Instance.GetMemberID(sessionID);
                if (sessionMemberID > 0)
                {
                    isValidatedSessionMemebr = true;
                    memberID = sessionMemberID;
                }
            }
            catch (Exception ex)
            {
                throw new SparkWSClientException("problem decyphering sessionid", ex);
            }
        }

        DemographicData result = new DemographicData(definition.AttributeDefinitions.Length);
        result.ProfileSectionID = definition.ProfileSectionID;

        result.MemberID = memberID;

        Matchnet.Member.ServiceAdapters.Member member = null;
        try
        {
            member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
        }
        catch (Exception ex)
        {
            throw new Exception("MemberBL.GetProfileSection() unable to get member", ex);
        }

        populateDemographicDataAttributes(member, definition, request.CommunityID, request.SiteID, request.BrandID, ref result);

        if (member.IsPayingMember(request.SiteID))
        {
            result.SubscriberStatus = "Subscriber";
        }
        else
        {
            result.SubscriberStatus = "Registered";
        }

        result.ProfileCompletionPercentage = GetProfileCompetionPercentage(member, Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(request.BrandID));

        return result;
    }

    private static void populateMemberAttributes(Matchnet.Member.ServiceAdapters.Member member, ProfileSectionDefinition definition, ref ProfileSection result, bool isValidatedSessionMemebr, AccessTicket ticket)
    {
        for (int i = 0; i < definition.AttributeDefinitions.Length; i++)
        {
            MemberAttributeParameter mpa = definition.AttributeDefinitions[i];
            switch (mpa.DataType)
            {
                case AttributeDataType.Boolean:
                    result.Profile.Add(new MemberAttributeOfBoolean(mpa,
                                        member.GetAttributeBool(
                                        mpa.CommunityID,
                                        mpa.SiteID,
                                        mpa.BrandID,
                                        mpa.AttributeID
                                        )));

                    break;

                case AttributeDataType.Int:
                    int intValue = member.GetAttributeInt(
                                                mpa.CommunityID,
                                                mpa.SiteID,
                                                mpa.BrandID,
                                                mpa.AttributeID,
                                                Int32.MinValue
                                                );
                    if (mpa.AttributeID == 122) // region ID -> turn it into a string
                    {
                        RegionLanguage rl = RegionSA.Instance.RetrievePopulatedHierarchy(intValue, mpa.LanguageID);
                        string regionString =
                              rl.CountryName + "\t" +
                              rl.StateDescription + "\t" +
                              rl.CityName + "\t";
                        result.Profile.Add(new MemberAttributeOfArrayOfString(
                                                new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.String, mpa.CommunityID, mpa.SiteID, mpa.BrandID, mpa.LanguageID),
                                                new string[] { rl.CountryName, rl.StateDescription, rl.CityName }));

                    }
                    else
                    {
                        result.Profile.Add(new MemberAttributeOfInt32(
                                                mpa,
                                                intValue
                                                ));
                    }
                    break;
                case AttributeDataType.String:
                    try
                    {
                      if (mpa.AttributeID != 302)
                            result.Profile.Add(new MemberAttributeOfString(mpa,
                                                member.GetAttributeText(mpa.CommunityID,
                                                mpa.SiteID,
                                                mpa.BrandID,
                                                mpa.LanguageID,
                                                mpa.AttributeID,
                                                string.Empty)));
 
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error in populating member attribute text.", ex);
                    }

                    break;
                case AttributeDataType.Date:
                    DateTime dtValue = member.GetAttributeDate(mpa.CommunityID,
                                        mpa.SiteID,
                                        mpa.BrandID,
                                        mpa.AttributeID,
                                        DateTime.MinValue);
                    if (mpa.AttributeID == 70) // birth date : convert to an age, add as an Int attribute
                    {

                        result.Profile.Add(new MemberAttributeOfInt32(
                           new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.Int, mpa.CommunityID, mpa.SiteID, mpa.BrandID, mpa.LanguageID)
                           , (int)((double)DateTime.Now.Subtract(dtValue).Days / 365.25)));
                    }
                    else
                    {
                        result.Profile.Add(new MemberAttributeOfDateTime(mpa, dtValue));
                    }

                    break;
                default:
                    throw new Exception("Bunk Attribute Definition attr " + mpa.AttributeID.ToString());
            }
        }
    }

    private static void populateDemographicDataAttributes(Matchnet.Member.ServiceAdapters.Member member, ProfileSectionDefinition definition, int communityID, int siteID, int brandID, ref DemographicData result)
    {
        Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attr = null;
        Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);

        for (int i = 0; i < definition.AttributeDefinitions.Length; i++)
        {
            MemberAttributeParameter mpa = definition.AttributeDefinitions[i];

            int attrID = mpa.AttributeID;
            attr = ServiceUtil.GetAttribute(attrID);
            if (attr == null)
            {
                return;
            }

            string attributeName = attr.Name;

            switch (attr.DataType)
            {
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Bit:
                    bool val;
                    val = member.GetAttributeBool(communityID, siteID, brandID, mpa.AttributeID);

                    result.Profile.Add(new MemberAttributeOfBoolean(new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.Boolean, communityID, siteID, brandID, mpa.LanguageID),
                                        member.GetAttributeBool(
                                        communityID,
                                        siteID,
                                        brandID,
                                        mpa.AttributeID
                                        )));

                    break;

                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Number:
                    int intValue = member.GetAttributeInt(communityID, siteID, brandID, mpa.AttributeID, Int32.MinValue);

                    if (mpa.AttributeID == 122) // region ID -> turn it into a string
                    {
                        RegionLanguage rl = RegionSA.Instance.RetrievePopulatedHierarchy(intValue, mpa.LanguageID);
                        string regionString =
                              rl.CountryName + "\t" +
                              rl.StateDescription + "\t" +
                              rl.CityName + "\t";
                        result.Profile.Add(new MemberAttributeOfArrayOfString(
                                                new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.String, communityID, siteID, brandID, mpa.LanguageID),
                                                new string[] { rl.CountryName, rl.StateDescription, rl.CityName }));

                        result.Location = OmnitureHelper.GetRegionString(member, brand, brand.Site.LanguageID, false, true, false);
                    }
                    else if (mpa.AttributeID == 88
                        || mpa.AttributeID == 362)
                    {
                        string ethnicityValue = String.Empty;
                        /*
                        string attributeName = String.Empty;
                        if (mpa.AttributeID == 88)
                        {
                            attributeName = "Ethnicity";
                        }
                        else if (mpa.AttributeID == 362)
                        {
                            attributeName = "JDateEthnicity";
                        }
                        */

                        AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, communityID, siteID, brandID);
                        if (attributeOptionCollection == null)
                        {
                            result.Profile.Add(new MemberAttributeOfString(new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.String, communityID, siteID, brandID, mpa.LanguageID),
                                                String.Empty));

                            result.Ethnicity = String.Empty;
                        }
                        else
                        {
                            AttributeOption attributeOption = attributeOptionCollection[intValue];
                            if (attributeOption == null)
                            {
                                result.Profile.Add(new MemberAttributeOfString(new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.String, communityID, siteID, brandID, mpa.LanguageID),
                                                    String.Empty));

                                result.Ethnicity = String.Empty;
                            }
                            else
                            {
                                result.Profile.Add(new MemberAttributeOfString(new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.String, communityID, siteID, brandID, mpa.LanguageID),
                                                    attributeOption.Description));

                                result.Ethnicity = attributeOption.Description;
                            }
                        }
                    }
                    else
                    {
                        result.Profile.Add(new MemberAttributeOfInt32(
                                                new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.Int, communityID, siteID, brandID, mpa.LanguageID),
                                                intValue
                                                ));
                    }
                    break;
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Text:
                    result.Profile.Add(new MemberAttributeOfString(new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.String, communityID, siteID, brandID, mpa.LanguageID),
                                        member.GetAttributeText(communityID,
                                        siteID,
                                        brandID,
                                        mpa.LanguageID,
                                        mpa.AttributeID,
                                        string.Empty)));

                    break;
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Date:
                    DateTime dtValue = member.GetAttributeDate(communityID,
                                        siteID,
                                        brandID,
                                        mpa.AttributeID,
                                        DateTime.MinValue);
                    if (mpa.AttributeID == 70) // birth date : convert to an age, add as an Int attribute
                    {

                        result.Profile.Add(new MemberAttributeOfInt32(
                           new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.Int, communityID, siteID, brandID, mpa.LanguageID)
                           , (int)((double)DateTime.Now.Subtract(dtValue).Days / 365.25)));

                        result.Age = OmnitureHelper.GetAge(dtValue);
                    }
                    else
                    {
                        result.Profile.Add(new MemberAttributeOfDateTime(new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.Date, communityID, siteID, brandID, mpa.LanguageID), dtValue));
                    }

                    break;
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Mask:
                    int mask = member.GetAttributeInt(communityID, siteID, brandID, mpa.AttributeID, Int32.MinValue);

                    if (mpa.AttributeID == MemberAttributeResult.ATTRID_GENDERMASK)
                    {
                        int gender = getGender(mask);
                        string strGender = String.Empty;
                        if (gender == (int)MemberAttributeResult.GENDER_MALE)
                        {
                            strGender = "Male";
                        }
                        else
                        {
                            strGender = "Female";
                        }

                        int seekinggender = getSeekingGender(mask);
                        string strSeekingGender = String.Empty;
                        if (seekinggender == (int)MemberAttributeResult.GENDER_SEEK_MALE)
                        {
                            strSeekingGender = "Seeking Male";
                        }
                        else
                        {
                            strSeekingGender = "Seeking Female";
                        }

                        result.Profile.Add(new MemberAttributeOfArrayOfString(
                                                new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.String, communityID, siteID, brandID, mpa.LanguageID),
                                                new string[] { strGender, strSeekingGender }));

                        result.Gender = OmnitureHelper.GetGender(mask);
                    }
                    else
                    {
                        result.Profile.Add(new MemberAttributeOfString(new MemberAttributeParameter(mpa.AttributeID, AttributeDataType.String, communityID, siteID, brandID, mpa.LanguageID), mask.ToString()));
                    }

                    break;
                default:
                    throw new Exception("Bunk Attribute Definition attr " + mpa.AttributeID.ToString());
            }
        }
    }

    private static void populateMemberAttributes(Matchnet.Member.ServiceAdapters.Member member, ProfileSectionDefinition definition, ref MemberAttributeResult result)
    {
        int communityid;
        int siteid;
        int brandid;
        Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attr = null;
        communityid = result.CommunityID;
        siteid = result.SiteID;
        for (int i = 0; i < definition.AttributeDefinitions.Length; i++)
        {
            MemberAttributeParameter mpa = definition.AttributeDefinitions[i];
            int attrID = mpa.AttributeID;
            attr = ServiceUtil.GetAttribute(attrID);
            if (attr == null)
                return;
            switch (attr.DataType)
            {
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Bit:
                    {
                        bool val;
                        val = member.GetAttributeBool(communityid, siteid, mpa.BrandID, mpa.AttributeID);
                        result.AddAttribute(attr.Name, val.ToString());
                        break;
                    }
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Number:
                    {
                        int intValue = member.GetAttributeInt(communityid, siteid, mpa.BrandID, mpa.AttributeID, Int32.MinValue);
                        if (intValue != Int32.MinValue)
                        {
                            string val = "";
                            if (mpa.AttributeID == 122) // region ID -> turn it into a zipcode
                            {
                                RegionLanguage rl = RegionSA.Instance.RetrievePopulatedHierarchy(intValue, mpa.LanguageID);
                                string zip = rl.PostalCode;
                                result.AddAttribute("zipcode", zip);
                            }
                            else
                            {
                                result.AddAttribute(attr.Name, val.ToString());
                            }

                        }
                        break;
                    }
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Text:
                    {
                        string val = member.GetAttributeText(communityid, siteid, mpa.BrandID, mpa.LanguageID, mpa.AttributeID, string.Empty);
                        if (!String.IsNullOrEmpty(val))
                        { result.AddAttribute(attr.Name, val); }
                        break;
                    }
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Date:
                    {
                        DateTime dtValue = member.GetAttributeDate(communityid, siteid, mpa.BrandID, mpa.AttributeID, DateTime.MinValue);
                        if (dtValue != DateTime.MinValue)
                        {
                            string val = String.Format("{0:d}", dtValue);
                            if (mpa.AttributeID == MemberAttributeResult.ATTRID_BDATE)
                            { result.AddAttribute("dateofbirth", val); }
                            else
                            { result.AddAttribute(attr.Name, val); }
                        }
                        break;
                    }
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Mask:
                    {
                        int mask = member.GetAttributeInt(communityid, siteid, mpa.BrandID, attrID, Int32.MinValue);
                        if (mask != Int32.MinValue)
                        {
                            if (attrID == MemberAttributeResult.ATTRID_GENDERMASK)
                            {
                                int gender = getGender(mask);
                                int seekinggender = getSeekingGender(mask);

                                result.AddAttribute("gender", gender.ToString());
                                result.AddAttribute("seekinggender", seekinggender.ToString());

                            }
                            else
                            { result.AddAttribute(attr.Name, mask.ToString()); }
                        }
                        break;
                    }

                default:
                    throw new Exception("Bunk Attribute Definition attr " + mpa.AttributeID.ToString());
            }
        }
    }

    private static void populateZoozamenMemberAttributes(Matchnet.Member.ServiceAdapters.Member member, ProfileSectionDefinition definition, ref MemberAttributeResult result)
    {
        int communityid;
        int siteid;
        int brandid;
        Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attr = null;
        communityid = result.CommunityID;
        siteid = result.SiteID;
        for (int i = 0; i < definition.AttributeDefinitions.Length; i++)
        {
            MemberAttributeParameter mpa = definition.AttributeDefinitions[i];
            int attrID = mpa.AttributeID;
            attr = ServiceUtil.GetAttribute(attrID);
            if (attr == null)
                return;
            switch (attr.DataType)
            {
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Bit:
                    {
                        bool val;
                        val = member.GetAttributeBool(communityid, siteid, mpa.BrandID, mpa.AttributeID);
                        result.AddAttribute(attr.Name, val.ToString());
                        break;
                    }
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Number:
                    {
                        int intValue = member.GetAttributeInt(communityid, siteid, mpa.BrandID, mpa.AttributeID, Int32.MinValue);
                        if (intValue != Int32.MinValue)
                        {
                            string val = "";
                            if (mpa.AttributeID == 122) // region ID -> turn it into a zipcode
                            {
                                RegionLanguage rl = RegionSA.Instance.RetrievePopulatedHierarchy(intValue, mpa.LanguageID);
                                string zip = rl.PostalCode;
                                string city = rl.CityName;
                                string state = rl.StateDescription;
                                string country = rl.CountryName;

                                if (zip != null)
                                {
                                    result.AddAttribute("zipcode", zip);
                                }

                                if (city != null)
                                {
                                    result.AddAttribute("city", city);
                                }

                                if (state != null)
                                {
                                    result.AddAttribute("state", state);
                                }

                                if (country != null)
                                {
                                    result.AddAttribute("country", country);
                                }
                            }
                            else if (attrID == MemberAttributeResult.ATTRID_EDUCATION
                                || attrID == MemberAttributeResult.ATTRID_SMOKING)
                            {
                                result.AddAttribute(attr.Name + "description", GetMaskContent(attr.Name, intValue, communityid, siteid, mpa.BrandID));
                            }
                            else
                            {
                                result.AddAttribute(attr.Name, intValue.ToString());
                            }

                        }
                        break;
                    }
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Text:
                    {
                        string val = member.GetAttributeText(communityid, siteid, mpa.BrandID, mpa.LanguageID, mpa.AttributeID, string.Empty);
                        if (!String.IsNullOrEmpty(val))
                        { result.AddAttribute(attr.Name, val); }
                        break;
                    }
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Date:
                    {
                        DateTime dtValue = member.GetAttributeDate(communityid, siteid, mpa.BrandID, mpa.AttributeID, DateTime.MinValue);
                        if (dtValue != DateTime.MinValue)
                        {
                            string val = String.Format("{0:d}", dtValue);
                            if (mpa.AttributeID == MemberAttributeResult.ATTRID_BDATE)
                            {
                                result.AddAttribute("dateofbirth", val);

                                try
                                {
                                    DateTime birthDate = Convert.ToDateTime(val);

                                    if (birthDate == DateTime.MinValue)
                                    {
                                        result.AddAttribute("age", "0");
                                    }
                                    else
                                    {
                                        result.AddAttribute("age", Convert.ToString((int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25)));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    break;
                                }
                            }
                            else
                            { result.AddAttribute(attr.Name, val); }
                        }
                        break;
                    }
                case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Mask:
                    {
                        int mask = member.GetAttributeInt(communityid, siteid, mpa.BrandID, attrID, Int32.MinValue);
                        if (mask != Int32.MinValue)
                        {
                            if (attrID == MemberAttributeResult.ATTRID_GENDERMASK)
                            {
                                int gender = getGender(mask);
                                int seekinggender = getSeekingGender(mask);

                                //result.AddAttribute("genderdescription", GetMaskContent(attr.Name, mask, communityid, siteid, mpa.BrandID));

                                result.AddAttribute("gender", GetMaskContent(attr.Name, gender, communityid, siteid, mpa.BrandID));
                                result.AddAttribute("seekinggender", GetMaskContent(attr.Name, seekinggender, communityid, siteid, mpa.BrandID));

                                //result.AddAttribute("gender", gender.ToString());
                                //result.AddAttribute("seekinggender", seekinggender.ToString());
                            }
                            else
                            { result.AddAttribute(attr.Name, mask.ToString()); }
                        }
                        break;
                    }

                default:
                    throw new Exception("Bunk Attribute Definition attr " + mpa.AttributeID.ToString());
            }
        }
    }

    private static string GetMaskContent(string attributeName, int maskValue, int communityID, int siteID, int brandID)
    {
        AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, communityID, siteID, brandID);

        if (attributeOptionCollection == null)
            return string.Empty;

        StringBuilder sb = new StringBuilder();

        foreach (AttributeOption attributeOption in attributeOptionCollection)
        {
            if (attributeOption.MaskContains(maskValue))
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }
                sb.Append(attributeOption.Description);
            }
        }

        return sb.ToString();
    }

    private static void populateSubscribtionAttributes(Matchnet.Purchase.ValueObjects.MemberTran mt, ref MemberAttributeResult result)
    {
        int subscriptionDuration;
        int subscriptionStatus;
        int membertranId;
        try
        {
            //there is no good member transaction -> registered memeber
            if (mt == null)
            {
                result.AddAttribute("subscriptionstatus", MemberAttributeResult.SUB_STATUS_REGISTERED.ToString());
                return;
            }

            //there is a good member transaction -> subscriber, get transaction details
            subscriptionStatus = MemberAttributeResult.SUB_STATUS_SUBSCRIBER;
            membertranId = mt.MemberTranID;

            switch (mt.DurationType)
            {
                case Matchnet.DurationType.Month:
                    subscriptionDuration = mt.Duration;
                    break;

                case Matchnet.DurationType.Year:
                    subscriptionDuration = 6;
                    break;

                default:
                    //less than 1 month
                    subscriptionDuration = 1;
                    break;
            }

            result.AddAttribute("subscriptionstatus", subscriptionStatus.ToString());
            result.AddAttribute("subscriptionduration", subscriptionDuration.ToString());
            result.AddAttribute("transactionid", membertranId.ToString());
            result.AddAttribute("transactiondate", string.Format("{0:d}", mt.InsertDate));

        }
        catch (Exception ex)
        { throw (ex); }
    }

    private static void populateSubscribtionAttributes(OrderInfo orderInfo, ref MemberAttributeResult result)
    {
        int subscriptionDuration;
        int subscriptionStatus;
        int membertranId;
        try
        {
            //there is no good member transaction -> registered memeber
            if (orderInfo == null)
            {
                result.AddAttribute("subscriptionstatus", MemberAttributeResult.SUB_STATUS_REGISTERED.ToString());
                return;
            }

            //there is a good member transaction -> subscriber, get transaction details
            subscriptionStatus = MemberAttributeResult.SUB_STATUS_SUBSCRIBER;
            membertranId = orderInfo.OrderID;

            OrderDetailInfo odi = orderInfo.OrderDetail[0];

            switch ((Matchnet.DurationType)Enum.Parse(typeof(Matchnet.DurationType), odi.DurationTypeID.ToString()))
            {
                case Matchnet.DurationType.Month:
                    subscriptionDuration = odi.Duration;
                    break;

                case Matchnet.DurationType.Year:
                    subscriptionDuration = 6;
                    break;

                default:
                    //less than 1 month
                    subscriptionDuration = 1;
                    break;
            }

            result.AddAttribute("subscriptionstatus", subscriptionStatus.ToString());
            result.AddAttribute("subscriptionduration", subscriptionDuration.ToString());
            result.AddAttribute("transactionid", membertranId.ToString());
            result.AddAttribute("transactiondate", string.Format("{0:d}", orderInfo.InsertDateInPST));

        }
        catch (Exception ex)
        { throw (ex); }
    }

    private static void populateZoozamenSubscribtionAttributes(Matchnet.Purchase.ValueObjects.MemberTran mt, Matchnet.Purchase.ValueObjects.MemberSub membersub, ref MemberAttributeResult result)
    {
        int subscriptionDuration;
        int subscriptionStatus;
        int membertranId;
        Matchnet.Purchase.ValueObjects.CurrencyType currencyType;

        string durationType = String.Empty;

        try
        {
            //there is no good member transaction -> registered memeber
            if (mt == null)
            {
                result.AddAttribute("subscriptionstatus", MemberAttributeResult.SUB_STATUS_REGISTERED.ToString());
                return;
            }

            //there is a good member transaction -> subscriber, get transaction details
            subscriptionStatus = MemberAttributeResult.SUB_STATUS_SUBSCRIBER;
            membertranId = mt.MemberTranID;

            switch (mt.DurationType)
            {
                case Matchnet.DurationType.Minute:
                    durationType = "minute";
                    break;

                case Matchnet.DurationType.Hour:
                    durationType = "hour";
                    break;

                case Matchnet.DurationType.Day:
                    durationType = "day";
                    break;

                case Matchnet.DurationType.Week:
                    durationType = "week";
                    break;

                case Matchnet.DurationType.Month:
                    subscriptionDuration = mt.Duration;
                    durationType = "month";
                    break;

                case Matchnet.DurationType.Year:
                    subscriptionDuration = 6;
                    durationType = "year";
                    break;

                default:
                    //less than 1 month
                    subscriptionDuration = 1;
                    break;
            }


            result.AddAttribute("subscriptionstatus", subscriptionStatus.ToString());
            result.AddAttribute("subscriptionduration", mt.Duration.ToString());
            result.AddAttribute("transactionid", membertranId.ToString());

            result.AddAttribute("subscriptiondurationlength", mt.Duration.ToString());
            result.AddAttribute("subscriptiondurationdescription", durationType);

            Matchnet.Purchase.ValueObjects.Plan plan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(mt.PlanID, 1004);
            if (plan != null)
            {
                result.AddAttribute("subscriptioninitialcost", plan.InitialCost.ToString());
                result.AddAttribute("subscriptionrenewalcost", plan.RenewCost.ToString());

                if ((plan.PlanTypeMask & Matchnet.Purchase.ValueObjects.PlanType.PremiumPlan) == Matchnet.Purchase.ValueObjects.PlanType.PremiumPlan)
                {
                    result.AddAttribute("subscriptionispremiumplus", "true");
                }
                else
                {
                    result.AddAttribute("subscriptionispremiumplus", "false");
                }
            }

            currencyType = mt.CurrencyType;
            result.AddAttribute("subscriptioncurrencytype", currencyType.ToString());

            result.AddAttribute("subscriptionexpirationdate", membersub.RenewDate.ToString());
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    private static void populateZoozamenSubscribtionAttributes(OrderInfo orderInfo, Spark.Common.RenewalService.RenewalSubscription renewalSub, ref MemberAttributeResult result)
    {
        int subscriptionDuration;
        int subscriptionStatus;
        int membertranId;
        Matchnet.Purchase.ValueObjects.CurrencyType currencyType;

        string durationType = String.Empty;

        try
        {
            //there is no good member transaction -> registered memeber
            if (orderInfo == null)
            {
                result.AddAttribute("subscriptionstatus", MemberAttributeResult.SUB_STATUS_REGISTERED.ToString());
                return;
            }

            //there is a good member transaction -> subscriber, get transaction details
            subscriptionStatus = MemberAttributeResult.SUB_STATUS_SUBSCRIBER;
            membertranId = orderInfo.OrderID;

            OrderDetailInfo odi = orderInfo.OrderDetail[0];

            switch ((Matchnet.DurationType)Enum.Parse(typeof(Matchnet.DurationType), odi.DurationTypeID.ToString()))
            {
                case Matchnet.DurationType.Minute:
                    durationType = "minute";
                    break;

                case Matchnet.DurationType.Hour:
                    durationType = "hour";
                    break;

                case Matchnet.DurationType.Day:
                    durationType = "day";
                    break;

                case Matchnet.DurationType.Week:
                    durationType = "week";
                    break;

                case Matchnet.DurationType.Month:
                    subscriptionDuration = odi.Duration;
                    durationType = "month";
                    break;

                case Matchnet.DurationType.Year:
                    subscriptionDuration = 6;
                    durationType = "year";
                    break;

                default:
                    //less than 1 month
                    subscriptionDuration = 1;
                    break;
            }


            result.AddAttribute("subscriptionstatus", subscriptionStatus.ToString());
            result.AddAttribute("subscriptionduration", odi.Duration.ToString());
            result.AddAttribute("transactionid", membertranId.ToString());

            result.AddAttribute("subscriptiondurationlength", odi.Duration.ToString());
            result.AddAttribute("subscriptiondurationdescription", durationType);

            if (renewalSub != null)
            {
                Matchnet.Purchase.ValueObjects.Plan plan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, 1004);
                if (plan != null)
                {
                    result.AddAttribute("subscriptioninitialcost", plan.InitialCost.ToString());
                    result.AddAttribute("subscriptionrenewalcost", plan.RenewCost.ToString());

                    if ((plan.PlanTypeMask & Matchnet.Purchase.ValueObjects.PlanType.PremiumPlan) == Matchnet.Purchase.ValueObjects.PlanType.PremiumPlan)
                    {
                        result.AddAttribute("subscriptionispremiumplus", "true");
                    }
                    else
                    {
                        result.AddAttribute("subscriptionispremiumplus", "false");
                    }
                }
            }
            currencyType = (Matchnet.Purchase.ValueObjects.CurrencyType)Enum.Parse(typeof(Matchnet.Purchase.ValueObjects.CurrencyType), orderInfo.CurrencyID.ToString());
            result.AddAttribute("subscriptioncurrencytype", currencyType.ToString());

            if (renewalSub != null)
            {
                result.AddAttribute("subscriptionexpirationdate", renewalSub.RenewalDatePST.ToString());
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    private static void populateMemberPhotos(Matchnet.Member.ServiceAdapters.Member member, ProfileSectionDefinition definition, ref ProfileSection result, bool isValidatedSessionMemebr, AccessTicket ticket)
    {
        if (definition.PhotoCommunityIDList == null || definition.PhotoCommunityIDList.Length == 0)
            return;

        List<MemberPhoto> photos = new List<MemberPhoto>(definition.PhotoCommunityIDList.Length * 4);
        

        for (int communityID = 0; communityID < definition.PhotoCommunityIDList.Length; communityID++)
        {
            PhotoCommunity communityPhotos = member.GetPhotos(definition.PhotoCommunityIDList[communityID]);
            communityPhotos.StripHostFromPaths();
            for (byte idx = 0; idx < communityPhotos.Count; idx++)
            {
                Photo p = communityPhotos[idx];
                if (p.IsApproved && (!p.IsPrivate || isInternalUser(ticket.AccessRoll)))
                {
                    int brandID;
                    member.GetLastLogonDate(definition.PhotoCommunityIDList[communityID], out brandID);
                    Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

                    string fullPhotoURL = getFullPhotoPath(p, PhotoType.Full, brand);
                    string fullThumbnailURL = getFullPhotoPath(p, PhotoType.Thumbnail, brand);
                    
                    // check to see if we got both URLs back before adding the photo
                    if(!string.IsNullOrEmpty(fullPhotoURL) && !string.IsNullOrEmpty(fullThumbnailURL))
                    {
                        MemberPhoto mp = new MemberPhoto(definition.PhotoCommunityIDList[communityID], p.FileWebPath, p.ThumbFileWebPath,
                        p.IsPrivate, !member.GetAttributeBool(definition.PhotoCommunityIDList[communityID], 0, 0, 76),
                        p.ListOrder, fullPhotoURL, fullThumbnailURL);
                        photos.Add(mp);
                    }
                    
                }
            }
        }

        result.Photos = new MemberPhoto[photos.Count];
        photos.CopyTo(result.Photos);
    }

    private static void populateMemberInfo(Matchnet.Member.ServiceAdapters.Member member, ProfileSectionDefinition definition, ref ProfileSection result, bool isValidatedSessionMemebr, AccessTicket ticket)
    {
        MemberInfo mi = new MemberInfo();
        bool isInternalUser = MemberBL.isInternalUser(ticket.AccessRoll);
        if (definition.CommunityIDList != null && definition.CommunityIDList.Length > 0)
        {
            //mi.Communities = new List<MemberCommunityInfo>(definition.CommunityIDList.Length);
            List<MemberCommunityInfo> communityList = new List<MemberCommunityInfo>(definition.CommunityIDList.Length);
            if (isValidatedSessionMemebr || isInternalUser)
            {
                int[] memberCommunities = member.GetCommunityIDList();
                foreach (int definedCommunityID in definition.CommunityIDList)
                {
                    foreach (int existsCommunityID in memberCommunities)
                    {
                        if (definedCommunityID == existsCommunityID)
                        {
                            bool isSelfSuspended = member.GetAttributeBool(existsCommunityID, 0, 0, 270);
                            // show to external clients only non self suspended ones.
                            if (!isSelfSuspended || (isSelfSuspended && isInternalUser))
                            {
                                communityList.Add(new MemberCommunityInfo(existsCommunityID, isSelfSuspended));
                            }
                            break;
                        }
                    }
                }
            }

            mi.Communities = new MemberCommunityInfo[communityList.Count];
            communityList.CopyTo(mi.Communities);
        }

        if (definition.SiteIDList != null && definition.SiteIDList.Length > 0)
        {
            List<MemberSiteInfo> siteList = new List<MemberSiteInfo>(definition.SiteIDList.Length);
            //mi.Sites = new List<MemberSiteInfo>(definition.SiteIDList.Length);
            if (isValidatedSessionMemebr || isInternalUser)
            {
                int[] memberSites = member.GetSiteIDList();
                foreach (int definedSiteID in definition.SiteIDList)
                {
                    foreach (int existsSiteID in memberSites)
                    {
                        if (definedSiteID == existsSiteID)
                        {
                            DateTime subscriptionExpirationDate = getSubscriptionExpirationDate(member, existsSiteID);
                            MemberSiteInfo memSiteInfo = new MemberSiteInfo(existsSiteID, subscriptionExpirationDate);
                            
                            bool passedFraudCheck = member.GetAttributeBool(Matchnet.Constants.NULL_INT, existsSiteID,
                                                    Matchnet.Constants.NULL_INT, "PassedFraudCheckSite");
                            memSiteInfo.PassedFraudCheck = passedFraudCheck;

                            siteList.Add(memSiteInfo);

                            break;
                        }
                    }
                }
            }
            mi.Sites = new MemberSiteInfo[siteList.Count];
            siteList.CopyTo(mi.Sites);
        }

        result.Membership = mi;
    }

    /// <summary>
    /// Authenticate an user given their email addtress and password.
    /// It also shares user privilege information as well.
    /// </summary>
    /// <param name="emailAddress">the email address of the user to authenticate</param>
    /// <param name="password">the password</param>
    /// <param name="privilegeIDs">an array of ints</param>
    /// <param name="ticketKey">the ticket key requesting this method</param>
    /// <returns>Whehther the user was authenticated by the system</returns>
    public static AuthenticationResult Authenticate(string emailAddress, string password, int[] privilegeIDs, string ticketKey)
    {
        AccessTicket ticket = AccessTicketBL.GetAccessTicket(ticketKey);


        //check if theuser is internal
        if (!isInternalUser(ticket.AccessRoll))
        {
            throw new Exception("Authentication service not available for general use");
        }

        try
        {
            System.Collections.Generic.List<PrivilegeResult> privilegeResults = new List<PrivilegeResult>();


            Matchnet.Member.ValueObjects.AuthenticationResult ar = MemberSA.Instance.Authenticate(emailAddress, password);

            if (ar.Status == AuthenticationStatus.Authenticated)
            {
                //we get the member privileges
                MemberPrivilegeCollection memberPrivilegeCollection = MemberPrivilegeSA.Instance.GetMemberPrivileges(ar.MemberID);

                //start to check every privilege against the member privilege collection
                if (privilegeIDs != null)
                {

                    foreach (int privilegeID in privilegeIDs)
                    {
                        privilegeResults.Add(new PrivilegeResult(privilegeID, memberPrivilegeCollection.HasPrivilege(privilegeID)));
                    }
                }

                return new AuthenticationResult(ar.MemberID, true, privilegeResults.ToArray());
            }
            else
            {
                return new AuthenticationResult(0, false, privilegeResults.ToArray());
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Failed authentication call for " + emailAddress, ex);
        }

    }

    /// <summary>
    /// Authenticate an user given their email addtress, password and communityId.
    /// It also shares user privilege information as well.
    /// </summary>
    /// <param name="emailAddress">the email address of the user to authenticate</param>
    /// <param name="password">the password</param>
    /// <param name="brandId">the brandId</param>
    /// <param name="privilegeIDs">an array of ints</param>
    /// <param name="ticketKey">the ticket key requesting this method</param>
    /// <returns>Whehther the user was authenticated by the system</returns>
    public static AuthenticationResult Authenticate(string emailAddress, string password, int brandId, int[] privilegeIDs, string ticketKey)
    {
        AccessTicket ticket = AccessTicketBL.GetAccessTicket(ticketKey);

        //check if theuser is internal
        if (!isInternalUser(ticket.AccessRoll))
        {
            throw new Exception("Authentication service not available for general use");
        }

        try
        {
            System.Collections.Generic.List<PrivilegeResult> privilegeResults = new List<PrivilegeResult>();

            Brand brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            Matchnet.Member.ValueObjects.AuthenticationResult ar = MemberSA.Instance.Authenticate(brand, emailAddress, password);

            if (ar.Status == AuthenticationStatus.Authenticated)
            {
                //we get the member privileges
                MemberPrivilegeCollection memberPrivilegeCollection = MemberPrivilegeSA.Instance.GetMemberPrivileges(ar.MemberID);

                //start to check every privilege against the member privilege collection
                if (privilegeIDs != null)
                {

                    foreach (int privilegeID in privilegeIDs)
                    {
                        privilegeResults.Add(new PrivilegeResult(privilegeID, memberPrivilegeCollection.HasPrivilege(privilegeID)));
                    }
                }

                return new AuthenticationResult(ar.MemberID, true, privilegeResults.ToArray());
            }
            else
            {
                return new AuthenticationResult(0, false, privilegeResults.ToArray());
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Failed authentication call for " + emailAddress, ex);
        }

    }

    public static void SetVideoClipURL(ExternalVideoUrlRequest request, int ticketID)
    {

        Matchnet.Member.ServiceAdapters.Member member = null;
        int communityID = 0;
        try
        {
            member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(request.MemberID, MemberLoadFlags.None);
        }
        catch (Exception ex)
        {
            throw new Exception("Member.SetVideoClipURL unable to get member", ex);
        }
        if (!member.IsSiteMember(request.SiteID))
        {
            throw new Exception("Member.SetVideoClipURL invalid request(1)");
        }


        Matchnet.Content.ValueObjects.BrandConfig.Sites sites = BrandConfigSA.Instance.GetSites();
        for (int i = 0; i < sites.Count; i++)
        {
            if (sites[i].SiteID == request.SiteID)
            {
                communityID = sites[i].Community.CommunityID;
                break;
            }
        }
        if (communityID == 0)
        {
            throw new Exception("Member.SetVideoClipURL invalid request(2)");
        }

        try
        {
            member.SetAttributeText(communityID,
               request.SiteID,
               0,
               2, // English - URL's are always english.
               MEMBER_ATTRIBUTE_EXTERNAL_VIDEO_URL,
               ((request.URL == string.Empty) ? null : request.URL),
               TextStatusType.Human);

            MemberSA.Instance.SaveMember(member);
        }
        catch (Exception ex)
        {
            throw new Exception("Member.SetVideoClipURL failed to save data", ex);
        }
        try
        {
            AdminSA.Instance.AdminActionLogInsert(
                     request.MemberID,
                     communityID,
                     (int)AdminAction.AlteredExternalVideoLinkURL,
                     ticketID,
                     AdminMemberIDType.SparkWSTicketID);
        }
        catch (Exception ex)
        {
            throw new Exception("Member.SetVideoClipURL failed to save log action:\t"
                              + request.MemberID.ToString()
                              + "\t" + communityID.ToString()
                              + "\t" + request.URL, ex);
        }
    }

    public static int UpdateMemberFraudStatus(int memberid, int siteid, int success)
    {
        int returnValue = 0;
        try
        {
            Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
            //Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(1003);
            //member.SetAttributeInt(brand, MEMBER_ATTRIBUTEID_PASSEDFRAUDCHECK, success);
            member.SetAttributeInt(0, siteid, 0, MEMBER_ATTRIBUTENAME_PASSEDFRAUDCHECK, success);
            Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
            returnValue = 1;

            if (success == 1)
            {
                // enable emails sent by this member now
                Brands brands = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandsBySite(siteid);

                int communityID = 0;
                foreach (Brand brand in brands)
                {
                    communityID = brand.Site.Community.CommunityID;
                    break;
                }

                if (communityID != 0)
                    EmailMessageSA.Instance.MarkOffFraudHoldSentByMember(memberid, communityID, siteid);
            }
           
        }
        catch (Exception ex)
        {
            throw new Exception("Member.UpdateMemberFraudStatus(" + memberid.ToString() + "," + siteid.ToString() + "," + success.ToString() + ") failed to update: " + ex);
        }

        return returnValue;
    }

    public static int AdminSuspendMember(int memberid, int siteid)
    {
        int returnValue = 0;
        try
        {
            GlobalStatusMask globalStatusMask = (GlobalStatusMask)Matchnet.Constants.NULL_INT;
            Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);

            globalStatusMask = (GlobalStatusMask)member.GetAttributeInt(0, siteid, 0, "GlobalStatusMask", 0);
            globalStatusMask = globalStatusMask | GlobalStatusMask.AdminSuspended;

            member.SetAttributeInt(0, siteid, 0, "GlobalStatusMask", (int)globalStatusMask);
            Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);

            AdminSA.Instance.AdminActionLogInsert(
                    member.MemberID
                    , Matchnet.Constants.GROUP_PERSONALS
                    , (Int32)AdminAction.AdminSuspendMember
                    , -1);

            returnValue = 1;
        }
        catch (Exception ex)
        {
            throw new Exception("Member.AdminSuspendMember failed to update: " + ex);
        }

        return returnValue;
    }

    public static int AdminSuspendMemberWithReason(int memberid, int siteid, int adminReasonID)
    {
        int returnValue = 0;
        try
        {
            GlobalStatusMask globalStatusMask = (GlobalStatusMask)Matchnet.Constants.NULL_INT;
            Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);

            globalStatusMask = (GlobalStatusMask)member.GetAttributeInt(0, siteid, 0, "GlobalStatusMask", 0);
            globalStatusMask = globalStatusMask | GlobalStatusMask.AdminSuspended;

            member.SetAttributeInt(0, siteid, 0, "GlobalStatusMask", (int)globalStatusMask);
            member.SetAttributeInt(0, siteid, 0, "LastSuspendedAdminReasonID", adminReasonID);
            Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);

            AdminSA.Instance.AdminActionLogInsert(member.MemberID,
                        Matchnet.Constants.GROUP_PERSONALS
                        , (Int32)AdminAction.AdminSuspendMember
                        , -1
                        , AdminMemberIDType.AdminProfileMemberID
                        , (AdminActionReasonID)adminReasonID);

            returnValue = 1;
        }
        catch (Exception ex)
        {
            throw new Exception("Member.AdminSuspendMemberWithReason failed to update: " + ex);
        }

        return returnValue;
    }

    public static int AdminSuspendMemberForROLF(int memberid, int siteid, int stepID, string stepDescription)
    {
        int returnValue = 0;
        try
        {
            ///hardcoding this for english because the description will always be in english
            int languageID = 2;

            GlobalStatusMask globalStatusMask = (GlobalStatusMask)Matchnet.Constants.NULL_INT;
            Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);

            globalStatusMask = (GlobalStatusMask)member.GetAttributeInt(0, siteid, 0, "GlobalStatusMask", 0);
            globalStatusMask = globalStatusMask | GlobalStatusMask.AdminSuspended;
                        
            member.SetAttributeInt(0, siteid, 0, "GlobalStatusMask", (int)globalStatusMask);
            member.SetAttributeInt(0, siteid, 0, "LastSuspendedAdminReasonID", (Int32)AdminActionReasonID.ROLF);
            member.SetAttributeInt(0, siteid, 0, "ROLFScore", stepID);
            member.SetAttributeText(0, siteid, 0, languageID, "ROLFScoreDescription", stepDescription, TextStatusType.Auto);

            Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);

            AdminSA.Instance.AdminActionLogInsert(member.MemberID,
                        Matchnet.Constants.GROUP_PERSONALS
                        , (Int32)AdminAction.AdminSuspendMember
                        , -1
                        , AdminMemberIDType.AdminProfileMemberID
                        , AdminActionReasonID.ROLF);

            returnValue = 1;
        }
        catch (Exception ex)
        {
            throw new Exception("Member.AdminSuspendMemberForROLF failed to update: " + ex);
        }

        return returnValue;
    }

    public static ABScenarioResult GetABScenario(ABScenarioRequest request)
    {
        string exceptionString = "MemberID:{0}, CommunityID:{1}, SiteID: {2}";
        string scenarioName = "Scenario: {0}\r\n";
        string scenarioString = "";
        try
        {
            if (request == null)
                throw new Exception("Received null request");

            exceptionString = String.Format(exceptionString, request.MemberID, request.CommunityID, request.SiteID);

            if (request.MemberID <= 0)
                throw new Exception("Invalid MemberID, should be positive integer. MemberID=" + request.MemberID.ToString());
            if (request.Scenarios == null || request.Scenarios.Count == 0)
            {
                throw new Exception("Invalid Scenarios list. Should contain at list 1 scenario");
            }

            List<ABScenario> scenarios = new List<ABScenario>();
            for (int i = 0; i < request.Scenarios.Count; i++)
            {
                scenarioString = scenarioString + String.Format(scenarioName, request.Scenarios[i]);
                AnaliticsScenarioBase scenario = new AnaliticsScenarioBase(request.Scenarios[i], request.CommunityID, request.SiteID, request.MemberID);
                Scenario scen = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetAnaliticsScenario(scenario);
                scenarios.Add(new ABScenario(request.Scenarios[i], scen));
            }
            ABScenarioResult result = new ABScenarioResult(request.CommunityID, request.SiteID, request.MemberID);
            result.Scenarios = scenarios;

            return result;
        }
        catch (Exception ex)
        {
            WebBoundaryException webExc = new WebBoundaryException(exceptionString + scenarioString, ex);
            throw webExc;
        }

    }

    #region Encore related
    /// <summary>
    /// This method checks the member attribute used for recording ClientOrderID against the member's email address.
    /// </summary>
    /// <param name="clientOrderID"></param>
    /// <param name="memberEmailAddress"></param>
    /// <param name="memberID"'></param>
    /// <param name="brandID"></param>
    /// <returns></returns>
    public static bool IsClientOrderIDValid(string clientOrderID, string memberEmailAddress, out int memberID, out int brandID, out int siteID)
    {
        brandID = Matchnet.Constants.NULL_INT;
        memberID = Matchnet.Constants.NULL_INT;
        siteID = Matchnet.Constants.NULL_INT;
        string memberAttrValue = null;
        bool ret = false;

        // don't even bother if this is a bad row. skip to the next record
        if (!(clientOrderID == null || memberEmailAddress == null))
        {
            // bad ClientOrderID format. skip this record
            if (clientOrderID.Length == 15)
            {
                memberID = MemberSA.Instance.GetMemberIDByEmail(memberEmailAddress);

                if (memberID != Matchnet.Constants.NULL_INT)
                {
                    // get the brandId from the ClientOrderID
                    string brandIdString = clientOrderID.Substring(10, 5);
                    if (brandIdString[0] == '0')
                        brandIdString = brandIdString.Substring(1, 4);

                    if (int.TryParse(brandIdString, out brandID))
                    {
                        Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                        Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);
                        if (brand != null && brand.Site != null)
                        {
                            siteID = brand.Site.SiteID;
                            memberAttrValue = member.GetAttributeText(brand, "EncoreClientOrderID", string.Empty);
                            ret = memberAttrValue.ToUpper() == clientOrderID.ToUpper();
                        }
                    }
                }
            }
        }

        return ret;
    }

    public static void UpdateEncoreMatchbackDate(int brandID, int memberID)
    {
        Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

        // We need to record EncoreMatchbackDate.
        Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID,
            Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
        member.SetAttributeDate(brand, "EncoreMatchbackDate", DateTime.Now);
        MemberSA.Instance.SaveMember(member);
    }

    #endregion


    #region Messmo-related
    /// <summary>
    /// Messmo API reference 2.22.  Messmo issues this call when they want to know a member's subscription end date.
    /// We reply to this request asynchronously.
    /// </summary>
    /// <param name="request"></param>
    /// <param name="ticketKey"></param>
    /// <returns></returns>
    public static MessmoSendUserTransResponse MessmoSendUserTrans(MessmoSendUserTransRequest request, string ticketKey)
    {

#if DEBUG
        System.Diagnostics.Debug.WriteLine("MessmoSendUserTrans() Request received from Messmo--> " + request.GetValuesString());
        ServiceUtil.LogEvent("WWW", "MessmoSendUserTrans() Request received from Messmo--> " + request.GetValuesString(), System.Diagnostics.EventLogEntryType.Information);
#endif
        
        if(!ServiceUtil.ValidMessmoChannelNumber(request.ChannelNumber))
        {
            throw new SparkWSClientException("This channel number does not exist: " + request.ChannelNumber);
        }

        const string STATUS_STOP_SMS = "userstopsms";

        if (!(request.Status.ToLower() == "getvaliduntil" || request.Status.ToLower() == STATUS_STOP_SMS))
        {
            throw new SparkWSClientException("This Status is not supported: " + request.Status);
        }

        // We only have support for JDATE.  Since our messaging system is community-based, Messmo is technically enabled for
        // all sites within JDATE community.  The sign-up page is only enabled on JDATE.com though.
        int communityID = 3;
        int brandID = 1003;
        int siteID = 103;

        // Get the MemberID by Messmo Subscriber ID        
        int memberID = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMemberIDByMessmoID(request.SubscriberId, communityID);

        if (memberID != Matchnet.Constants.NULL_INT)
        {
            Matchnet.Member.ServiceAdapters.Member member = null;

            try
            {
                member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                if (request.Status.ToLower() == STATUS_STOP_SMS)
                {
                     ActivityRecordingSA.Instance.RecordActivity(memberID, memberID, siteID,
                         Matchnet.ActivityRecording.ValueObjects.Types.ActionType.StopSMS,
                         Matchnet.ActivityRecording.ValueObjects.Types.CallingSystem.Messmo, request.GetValuesString());                  
                }
                else
                {
                    DateTime setDate = member.GetAttributeDate(communityID, siteID, brandID, "SubscriptionExpirationDate", DateTime.MinValue);
                    Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMessmoUserTransStatusRequest(brandID, DateTime.Now, request.SubscriberId,
                        Matchnet.HTTPMessaging.Constants.MESSMO_USER_TRANS_STATUS_SET_VALID_UNTIL, setDate);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("MessagingBL.MessmoSendUserTrans() unable to retrieve the member (MemberID: " + memberID.ToString() + ")", ex);
            }

            return new MessmoSendUserTransResponse("1.0.0", "Success");
        }
        else
        {
            return new MessmoSendUserTransResponse("1.0.0", "Failed");
        }        
    }

    /// <summary>
    /// This method is written for Messmo to use. This sets the MessmoCapable member attribute to true. Only enabled for
    /// JDate.com for now.
    /// </summary>
    /// <param name="request"></param>
    /// <param name="ticketKey"></param>
    /// <returns></returns>
    public static MessmoSubscribeResponse SubscribeToMessmo(MessmoSubscribeRequest request, string ticketKey)
    {
#if DEBUG
        System.Diagnostics.Debug.WriteLine("SubscribeToMessmo() Request received from Messmo--> " + request.GetValuesString());
        ServiceUtil.LogEvent("WWW", "SubscribeToMessmo() Request received from Messmo--> " + request.GetValuesString(), System.Diagnostics.EventLogEntryType.Information);
#endif

        //System.Diagnostics.Trace.WriteLine("__SubscribeToMessmo() MessmoSubscriberId: " + request.GetValuesString());
        //ServiceUtil.LogEvent("WWW", "__SubscribeToMessmo() MessmoSubscriberId: " + request.GetValuesString(), System.Diagnostics.EventLogEntryType.Information);

        if (!ServiceUtil.ValidMessmoChannelNumber(request.ChannelNumber))
        {
            throw new SparkWSClientException("This channel number does not exist: " + request.ChannelNumber);
        }

        // We only have support for JDATE.  Since our messaging system is community-based, Messmo is technically enabled for
        // all sites within JDATE community.  The sign-up page is only enabled on JDATE.com though.
        int communityID = 3;

        // Get the MemberID by Messmo Subscriber ID        
        int memberID = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMemberIDByMessmoID(request.SubscriberId, communityID);

        if (memberID != Matchnet.Constants.NULL_INT)
        {
            Matchnet.Member.ServiceAdapters.Member member = null;

            try
            {
                member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                int messmoCapable = member.GetAttributeInt(communityID, Matchnet.Constants.NULL_INT, Matchnet.Constants.NULL_INT, "MessmoCapable", 0);

                // Only take action if the MessmoCapable is in PENDING.  If it's not in PENDING, something must have happened
                // in between now and when the member first subscribed to Messmo.
                if (messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Pending)
                {
                    member.SetAttributeInt(communityID, Matchnet.Constants.NULL_INT, Matchnet.Constants.NULL_INT, "MessmoCapable", 1);
                    Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("MessagingBL.SubscribeToMessmo() unable to retrieve the member (MemberID: " + memberID.ToString() + ")", ex);
            }

            return new MessmoSubscribeResponse("1.0.0", "Success");
        }
        else
        {
            return new MessmoSubscribeResponse("1.0.0", "Failed");
        }

    }
    #endregion

    #region helpers

    private static DateTime getSubscriptionExpirationDate(Matchnet.Member.ServiceAdapters.Member member, int siteID)
    {
        DateTime result = DateTime.MinValue;
        //result = member.GetAttributeDate(0, siteID, 0, "SubscriptionExpirationDate", DateTime.MinValue);
        Spark.Common.AccessService.AccessPrivilege ap = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.BasicSubscription, 0, siteID, 0);
        if (ap != null)
        {
            result = ap.EndDatePST;
        }

        #region Toma Testing
        //if (result == DateTime.MinValue || result < DateTime.Now)
        //{
        //    try
        //    {
        //        DateTime temp1 = member.GetAttributeDate(Matchnet.Constants.NULL_INT, siteID, Matchnet.Constants.NULL_INT, "SubscriptionExpirationDate", DateTime.MinValue);
        //        DateTime temp2 = member.GetAttributeDate(Matchnet.Constants.NULL_INT, siteID, Matchnet.Constants.NULL_INT, "SubscriptionExpirationDate", DateTime.MinValue, false);
        //        Spark.Common.AccessService.AccessPrivilege ap2 = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.BasicSubscription, 0, siteID, 0);
        //        DateTime temp3 = DateTime.MinValue;
        //        if (ap2 != null)
        //        {
        //            temp3 = ap2.EndDatePST;
        //        }

        //        ServiceUtil.LogEvent("WWW", "getSubscriptionExpirationDate(" + member.MemberID.ToString() + "," + siteID.ToString() + "). Result: " + result.ToString() + ", Temp1: " + temp1.ToString() + ", Temp2: " + temp2.ToString() + ", Temp3: " + temp3.ToString(), System.Diagnostics.EventLogEntryType.Information);
        //    }
        //    catch (Exception ex)
        //    {
        //        ServiceUtil.LogEvent("WWW", "getSubscriptionExpirationDate(" + member.MemberID.ToString() + "," + siteID.ToString() + "). " + ex.Message, System.Diagnostics.EventLogEntryType.Information);
        //    }
        //}
        #endregion
        return result;
    }

    private static bool isInternalUser(ApiAccessRoll roll)
    {
        return ((roll & (ApiAccessRoll.Internal | ApiAccessRoll.Developer | ApiAccessRoll.SuperUser)) > 0);
    }

    private static int getGender(int mask)
    {

        if ((mask & MemberAttributeResult.GENDER_FEMALE) == MemberAttributeResult.GENDER_FEMALE)
        { return MemberAttributeResult.GENDER_FEMALE; }
        if ((mask & MemberAttributeResult.GENDER_MALE) == MemberAttributeResult.GENDER_MALE)
        { return MemberAttributeResult.GENDER_MALE; }

        return 0;
    }

    private static int getSeekingGender(int mask)
    {

        if ((mask & MemberAttributeResult.GENDER_SEEK_FEMALE) == MemberAttributeResult.GENDER_SEEK_FEMALE)
        { return MemberAttributeResult.GENDER_FEMALE; }
        if ((mask & MemberAttributeResult.GENDER_SEEK_MALE) == MemberAttributeResult.GENDER_SEEK_MALE)
        { return MemberAttributeResult.GENDER_MALE; }

        return 0;
    }

    private static Matchnet.Purchase.ValueObjects.MemberTran getMemberSubscribtion(int siteid, int memberid)
    {
        Matchnet.Purchase.ValueObjects.MemberTran tran = null;
        try
        {
            Matchnet.Purchase.ValueObjects.MemberTranCollection mtc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetMemberTransactions(memberid, siteid);
            foreach (Matchnet.Purchase.ValueObjects.MemberTran mt in mtc)
            {
                if (mt.SiteID == siteid && mt.TranType == Matchnet.Purchase.ValueObjects.TranType.InitialBuy && mt.MemberTranStatus == Matchnet.Purchase.ValueObjects.MemberTranStatus.Success)
                {
                    tran = mt;
                    break;
                }
            }
            return tran;
        }
        catch (Exception ex)
        { throw (ex); }

    }

    private static OrderInfo getMemberSubscriptionUPS(int siteid, int memberid)
    {
        OrderInfo tran = null;
        try
        {
            OrderInfo[] arrOrderInfo = OrderHistoryServiceWebAdapter.GetProxyInstanceForBedrock().GetMemberOrderHistoryByMemberID(memberid, siteid, 50, 1);
            if (arrOrderInfo != null && arrOrderInfo.Length > 0)
            {
                foreach (OrderInfo orderInfo in arrOrderInfo)
                {
                    if (orderInfo.OrderStatusID == 2)
                    {
                        if (orderInfo.OrderDetail != null && orderInfo.OrderDetail.Length > 0)
                        {
                            bool isInitialPurchase = false;
                            foreach (OrderDetailInfo odi in orderInfo.OrderDetail)
                            {
                                if (odi.OrderTypeID == 1)
                                {
                                    isInitialPurchase = true;
                                    break;
                                }
                            }

                            if (isInitialPurchase)
                            {
                                tran = orderInfo;
                                break;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { tran = null; }

        return tran;

    }

    private static string GetProfileCompetionPercentage(Matchnet.Member.ServiceAdapters.Member member, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
    {
        bool hasPhoto = false;
        int currentStep;
        int completedSteps = 0;
        int stepValue = 1;
        if (member.GetPhotos(brand.Site.Community.CommunityID).Count > 0)
        {
            hasPhoto = true;
        }
        string regsteps = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("REGISTRATION_STEPS", brand.Site.Community.CommunityID, brand.Site.SiteID);
        int stepCount = Matchnet.Conversion.CInt(regsteps) + 1; //add 1 for photo upload page
        int completedStepMask = member.GetAttributeInt(brand, "NextRegistrationActionPageID");
        for (currentStep = 1; currentStep <= stepCount; currentStep++)
        {
            if (currentStep != stepCount)
            {
                //regular steps
                if ((completedStepMask & stepValue) == stepValue)
                {
                    completedSteps++;
                }
            }
            else
            {
                //photoupload
                if (hasPhoto)
                {
                    completedSteps++;
                }
            }

            stepValue = stepValue * 2;
        }

        return Convert.ToString(Convert.ToInt32((double)completedSteps / (double)stepCount * 100).ToString() + "%");
    }

    private static string getFullPhotoPath(Photo photo, PhotoType photoType, Brand brand)
    {
        string memberPhotoUrl = string.Empty;
        bool showPhotosFromCloud = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD"));

        switch (photoType)
        {
            case PhotoType.Full:
                if (photo.FileWebPath != null) //17243 - some members have null FileWebPath but shouldn't 
                {
                    memberPhotoUrl = showPhotosFromCloud ? photo.FileCloudPath : photo.FileWebPath;
                }
                break;
            case PhotoType.Thumbnail:
                if (photo.ThumbFileWebPath != null) // some members have null ThumbFileWebPath (before approval for example)
                {
                    memberPhotoUrl = showPhotosFromCloud ? photo.ThumbFileCloudPath : photo.ThumbFileWebPath;
                }
                break;
        }

        if (showPhotosFromCloud)
        {
            if (!string.IsNullOrEmpty(memberPhotoUrl))
            {
                Client cloudClient = new Client(brand.Site.Community.CommunityID, brand.Site.SiteID,
                                                RuntimeSettings.Instance);
                memberPhotoUrl = cloudClient.GetFullCloudImagePath(memberPhotoUrl, FileType.MemberPhoto, false);
            }
        }
        else
        {
            memberPhotoUrl = "http://" + brand.Site.DefaultHost + "." + brand.Uri + memberPhotoUrl;
        }

        return memberPhotoUrl;
    }

    #endregion helpers
}
