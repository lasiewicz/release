using System;
using System.Security;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;


/// <summary>
/// Summary description for WebServiceBase
/// </summary>
public class WebServiceBase : WebService
{
   public ApiAccessHeader accessHeader;
   public const string REMOTE_CLIENT_IP_KEY = "RemoteClientHost";

   public WebServiceBase()
   {
	  string remoteClientHostIP = HttpContext.Current.Request.Headers["client-ip"];
	  if (remoteClientHostIP == null)
		 remoteClientHostIP = HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];

	  Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(remoteClientHostIP), null);
	  Thread.SetData(Thread.GetNamedDataSlot(REMOTE_CLIENT_IP_KEY), remoteClientHostIP);
   }
}
