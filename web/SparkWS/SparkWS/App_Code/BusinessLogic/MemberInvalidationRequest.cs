﻿using System;
using System.Net;

internal delegate void OnInvalidationComplete(MemberInvalidationRequest completedInvalidationRequest);

/// <summary>
/// holds currently executing invalidation requests
/// </summary>
internal class MemberInvalidationRequest : IDisposable
{
    private readonly string notificationURL;

    /// <summary>
    /// the current http request to the server
    /// </summary>
    private HttpWebRequest webRequest;

    private HttpWebResponse webResponse;

    /// <summary>
    /// the async result that is used to keep track of how the request is going.
    /// </summary>
    private IAsyncResult asyncResult;

    /// <summary>
    /// the invalidation args on which this request is acting on
    /// </summary>
    private int memberID;

    /// <summary>
    /// whether the request has been fired.
    /// </summary>
    private bool started;

    private OnInvalidationComplete onInvalidationComplete;

    /// <summary>
    /// creates a new invalidation request which can be fired
    /// to a particular notifucatio url
    /// </summary>
    /// <param name="invalidationArgs">the invalidation args that hold the user ID, and site ID for which
    /// the invalidation will fire</param>
    /// <param name="notificationURL">the notification URL to which an HTTP request will be submitted.
    /// it should have {0} and {1} which correspond to memberID and siteID respectively</param>
    public MemberInvalidationRequest(int memberID, string notificationURL)
    {
        //set the init values for the private fields.
        this.started = false;
        this.memberID = memberID;
        this.notificationURL = notificationURL;

        //set up the web request
        string destinationURL = string.Format(notificationURL, MemberID.ToString());

        webRequest = WebRequest.Create(destinationURL) as HttpWebRequest;
        webRequest.ContentType = "application/x-www-form-urlencoded";
        webRequest.Method = "GET";
        webRequest.KeepAlive = false;
    }

    /// <summary>
    /// starts this request
    /// </summary>
    public void Start()
    {
        asyncResult = webRequest.BeginGetResponse(new AsyncCallback(OnComplete), null);
    }

    /// <summary>
    /// starts this request, and supplies a method to call after the invalidation has been completed.
    /// should be used when we want to do some post-completing outside of this class.
    /// </summary>
    /// <param name="invalidationCompleteHandler">the handler to be called when the invalidation is complete</param>
    public void Start(OnInvalidationComplete invalidationCompleteHandler)
    {
        this.onInvalidationComplete = invalidationCompleteHandler;
        webRequest.BeginGetResponse(new AsyncCallback(OnComplete), null);
    }

    /// <summary>
    /// stops this request
    /// </summary>
    public void Stop()
    {
        Dispose();
    }

    /// <summary>
    /// called whenever an invalidation has completed succesfully
    /// </summary>
    private void OnComplete(IAsyncResult asyncResult)
    {
        try
        {
            webResponse = webRequest.EndGetResponse(asyncResult) as HttpWebResponse;
            webResponse.Close();
        }
        catch (Exception ex)
        {
            Dispose();
        }
        finally
        {
            onInvalidationComplete(this);
        }
    }

    /// <summary>
    /// gets if this request has been started
    /// </summary>
    internal bool Started
    {
        get { return started; }
    }

    /// <summary>
    /// gets if the request has completed succesfully
    /// </summary>
    internal bool Completed
    {
        get
        {
            if (started)
            {
                return asyncResult.CompletedSynchronously;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// returns the current http web request
    /// </summary>
    internal HttpWebRequest HttpWebRequest
    {
        get { return webRequest; }
    }

    /// <summary>
    /// gets the current async result
    /// </summary>
    internal IAsyncResult AsyncResult
    {
        get { return asyncResult; }
    }

    /// <summary>
    /// the invalidation args for this request
    /// </summary>
    internal int MemberID
    {
        get { return memberID; }
    }

    #region IDisposable Members

    public void Dispose()
    {
        //stops the current request
        webRequest.Abort();
        //onInvalidationComplete(this);
    }

    #endregion
}

