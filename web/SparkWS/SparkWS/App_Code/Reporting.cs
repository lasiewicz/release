﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Security.Permissions;
using Matchnet.Configuration.ValueObjects.SparkWS;
using System.Reflection;

/// <summary>
/// Web service primarily written for Messmo, but it should be extended to support others if it needs to be.
/// </summary>
[WebService(Namespace = "http://Reporting.SparkWS.com/", Description="For sending reporting data to Spark")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Reporting : WebServiceBase
{
    public Reporting()
    { }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate | ApiAccessRoll.Developer | ApiAccessRoll.Internal))]
    public MessmoReportingResponse UpdateReporting(MessmoReportingRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 2000);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            MessmoReportingResponse result = ReportingBL.UpdateReporting(request, key);

            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.Reporting.UpdateReporting request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.Reporting.UpdateReporting unable to log the reporting data.", SoapException.ServerFaultCode, ex);
        }

    }
}

