﻿using System;
using Matchnet.List.ValueObjects;
using System.Security.Permissions;
using System.Web.Services;
using Matchnet.Configuration.ValueObjects.SparkWS;
using System.Web.Services.Protocols;
using System.Reflection;


/// <summary>
/// Summary description for List
/// </summary>
[WebService(Namespace = "http://List.SparkWS.com/", Description = "Exchanges information about a member in the Spark network websites.")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class List : WebServiceBase
{

    public List()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Developer | ApiAccessRoll.Internal))]
    public ListSaveResult AddListMember(AddListMemberRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 200);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";
        try
        {
            ListSaveResult result = ListBL.AddListMember(request, key);
            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.List.AddListMember request error", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.List.AddListMember unable to get member data", SoapException.ServerFaultCode, ex);
        }
    }

}

