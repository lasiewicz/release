
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Security.Permissions;
using System.Xml.Serialization;


using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Purchase.ValueObjects;
using System.Collections.Generic;
using Spark.Common.Adapter;
using Spark.Common.OrderHistoryService;
//using Matchnet.Exceptions;


/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://Subscription.SparkWS.com/", Description = "Exchanges information about a member subscription status in the Spark network websites.")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Subscription : WebServiceBase
{

    public Subscription()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    /*
    [WebMethod]
    // [SoapHeader("accessHeader")]
    // [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate))]
     public bool TrialPayConfirmationObj( TrialPayNotification request)
     {
         bool result = false;
         bool ConfirmationResult=false;
         // Check for object version 
         try
         {

             ConfirmationResult = SubscriptionBL.TrialPayConfirmation(request.MemberID, request.SiteID, accessHeader.Key);
         }
         catch (SoapException soapEx)
         {
             throw new SoapException(string.Format("SparkWS.Subscription.TrialPayConfirmation exception. MemberID={0}, SiteID={1}", request.MemberID.ToString(), request.SiteID.ToString()), SoapException.ServerFaultCode, soapEx);
         }
         finally
         {
             result = ConfirmationResult;
         }
         return result;
     }*/
    [WebMethod]
    //[SoapHeader("accessHeader")]
    //[ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate))]
    public bool TrialPayConfirmation(string strInput, string ticketkey)
    {
        bool result = false;
        bool ConfirmationResult = false;
        Int32 memberid = 0;
        Int32 siteid = 0;

        // Check for object version 

        //System.Diagnostics.EventLog.WriteEntry("Subscription.asmx", string.Format("Subscription.asmx - TrialPayConfirmation started.\n  memberid={0} , siteid={1}, ticketkey{2}", memberid.ToString(), siteid.ToString(), ticketkey), System.Diagnostics.EventLogEntryType.Warning); 
        System.Diagnostics.Trace.WriteLine(string.Format("Subscription.asmx - TrialPayConfirmation started.\n  memberid={0} , siteid={1}, ticketkey{2}", memberid.ToString(), siteid.ToString(), ticketkey));
        ServiceUtil.LogEvent("WWW", string.Format("Subscription.asmx - TrialPayConfirmation started.\n  strInput={0} , ticketkey={1}", strInput, ticketkey), System.Diagnostics.EventLogEntryType.Information);

        #region AccessTicket_Verification
        AccessTicket accessTicket = AccessTicketBL.GetAccessTicket(ticketkey);
        if (AccessTicketBL.ValidAccess(accessTicket))
        {
            if (accessTicket.ProfileSectionDefinitions == null || accessTicket.ProfileSectionDefinitions.Count == 0)
                throw new Exception(ticketkey + " has no section definitions.");
            if (accessTicket.AccessRoll != ApiAccessRoll.Affiliate)
                throw new Exception(ticketkey + " has no Affiliate Access Roll.");
        }
        else
        {
            throw new Exception(ticketkey + " Invalid Access key.");
        }
        #endregion

        try
        {
            if (!strInput.Equals(string.Empty))
            {
                string[] StrParams = strInput.Split(new Char[] { ';' });
                if (StrParams.Length >= 2)
                {
                    memberid = Int32.Parse(StrParams[0]);
                    siteid = Int32.Parse(StrParams[1]);

                    ServiceUtil.LogEvent("WWW", string.Format("Subscription.asmx - TrialPayConfirmation Before Calling Access Privilege Update \n  memberid={0} , siteid={1}, ticketkey={2}", memberid.ToString(), siteid.ToString(), ticketkey), System.Diagnostics.EventLogEntryType.Information);

                    ConfirmationResult = SubscriptionBL.TrialPayConfirmation(memberid, siteid, ticketkey);
                    System.Diagnostics.Debug.WriteLine("Subscription.asmx - TrialPayConfirmation result=" + ConfirmationResult.ToString());
                }
            }
            else
            {
                ServiceUtil.LogEvent("WWW", string.Format("Subscription.asmx - TrialPayConfirmation Invalid strInput: {0}", strInput), System.Diagnostics.EventLogEntryType.Error);
                throw new Exception("SparkWS.Subscription.TrialPayConfirmation exception. Invalid input string (memberid;siteid).");
            }
        }
        catch (SoapException soapEx)
        {
            throw new SoapException(string.Format("SparkWS.Subscription.TrialPayConfirmation exception. MemberID={0}, SiteID={1}", memberid.ToString(), siteid.ToString()), SoapException.ServerFaultCode, soapEx);
        }
        finally
        {
            result = ConfirmationResult;
        }
        return result;
    }

    [WebMethod]
    public bool TrialPayConfirmationEncrypted(string encryptedMemberID, Int32 siteID, string ticketkey)
    {
        string memberID = Matchnet.Security.Crypto.Decrypt(MemberBL.SPARK_WS_CRYPT_KEY, encryptedMemberID);
        return TrialPayConfirmation(memberID + ";" + siteID.ToString(), ticketkey);
    }

    [WebMethod]
    public bool UpdateSubscription(int memberID, int siteID, int subscriptionInfoID, string ticketkey)
    {
        #region AccessTicket_Verification
        AccessTicket accessTicket = AccessTicketBL.GetAccessTicket(ticketkey);
        if (AccessTicketBL.ValidAccess(accessTicket))
        {
            if (accessTicket.ProfileSectionDefinitions == null || accessTicket.ProfileSectionDefinitions.Count == 0)
                throw new Exception(ticketkey + " has no section definitions.");
            if (accessTicket.AccessRoll != ApiAccessRoll.Affiliate)
                throw new Exception(ticketkey + " has no Affiliate Access Roll.");
        }
        else
        {
            throw new Exception(ticketkey + " Invalid Access key.");
        }
        #endregion

        
        return SubscriptionBL.UpdateSubscription(memberID, siteID, subscriptionInfoID, accessTicket.AccessTicketID);
    }

    [WebMethod]
    public bool UpdateSubscriptionEncrypted(string encryptedMemberID, int siteID, int subscriptionInfoID, string ticketkey)
    {
        string strMemberID = Matchnet.Security.Crypto.Decrypt(MemberBL.SPARK_WS_CRYPT_KEY, encryptedMemberID);
        int memberID = int.Parse(strMemberID);
        return UpdateSubscription(memberID, siteID, subscriptionInfoID, ticketkey);
    }

    [WebMethod]
    public void SavePromoApproval(int promoID, int brandID, int approvalStatus, string ticketkey)
    {
        AccessTicket accessTicket = AccessTicketBL.GetAccessTicket(ticketkey);
        if (AccessTicketBL.ValidAccess(accessTicket))
        {
            SubscriptionBL.SavePromoApproval(promoID, brandID, (Int16)approvalStatus);
        }
        else
        {
            throw new Exception(ticketkey + " Invalid Access key.");
        }   
    }

    /// <summary>
    /// Coordinates encore processing between Matchnet and UPS
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public int GenerateEncryptedSalesFile(string ticketkey)
    {
        OrderHistoryManagerClient oClient = OrderHistoryServiceWebAdapter.GetProxyInstanceForBedrock();
        PurchaseServiceClient pClient = PurchaseServiceAdapter.GetProxyInstance();
        spark.net.paymentwrapper.PaymentWrapperServiceClient pwClient = PaymentWrapperAdapter.GetProxyInstance();

        AccessTicket accessTicket = AccessTicketBL.GetAccessTicket(ticketkey);
        if (AccessTicketBL.ValidAccess(accessTicket))
        {
            try
            {
                List<Spark.Purchase.ValueObjects.EncoreMember> EncoreMemberList = new List<Spark.Purchase.ValueObjects.EncoreMember>();

                bool isTesting = false;
                if (isTesting)
                {
                    // Create list of members to generate sales file
                    Spark.Purchase.ValueObjects.EncoreMember encoreMember = new Spark.Purchase.ValueObjects.EncoreMember();
                    encoreMember.CallingSystemID = 103;
                    encoreMember.CustomerID = 100004276;
                    encoreMember.Email = "test@test.net";
                    encoreMember.Filler = "";
                    encoreMember.MailCode = "90210";
                    encoreMember.OrderDate = DateTime.Now.ToShortDateString();
                    encoreMember.VariableField = "100004276_1003";

                    EncoreMemberList.Add(encoreMember);

                    // Generate Encrypted Sales File
                    byte[] outputFileContent = PaymentProfileServiceAdapter.GetProxyInstance().GenerateEncoreEncryptedSalesFile(EncoreMemberList.ToArray());
                    string hero = "Hail the hero!";
                }
                else
                {
                    #region Prepare the Sales Records into a DataTable
                    MatchbackCollection mbCol = SubscriptionBL.GetMatbackRequest();
                    foreach (Matchback mb in mbCol)
                    {
                        int memberID = Matchnet.Constants.NULL_INT;
                        int brandID;
                        int siteID;

                        // Skip records that do not have the valid ClientOrderID
                        if (MemberBL.IsClientOrderIDValid(mb.ClientOrderID, mb.Email, out memberID, out brandID, out siteID))
                        {
                            MemberBL.UpdateEncoreMatchbackDate(brandID, memberID);

                            // Create list of members to generate sales file
                            Spark.Purchase.ValueObjects.EncoreMember encoreMember = new Spark.Purchase.ValueObjects.EncoreMember();
                            encoreMember.CallingSystemID = siteID;
                            encoreMember.CustomerID = memberID;
                            encoreMember.Email = mb.Email;
                            encoreMember.Filler = mb.Filler;
                            encoreMember.MailCode = mb.MailCode;
                            encoreMember.OrderDate = mb.OrderDate;
                            encoreMember.VariableField = memberID.ToString() + "_" + brandID.ToString();

                            EncoreMemberList.Add(encoreMember);
                        }
                        else
                        {
                            string message = string.Format("GenerateEncryptedSalesFile(): ClientOrderID sent from Encore does not match member's attribute value - MemberID: {0}, Email: {1}, ClientOrderID: {2}",
                                memberID.ToString(), mb.Email, mb.ClientOrderID);

                            //EventLog.WriteEntry(Matchnet.Purchase.ValueObjects.ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
                            ServiceUtil.LogEvent("WWW", message, System.Diagnostics.EventLogEntryType.Warning);
                        }
                    }

                    #endregion

                    string outputFileName = SubscriptionBL.GetOutputCreditSalesFileName();
                    int lineID = SubscriptionBL.StartMatchbackFileGeneration(outputFileName);

                    // Generate Encrypted Sales File
                    byte[] outputFileContent = PaymentProfileServiceAdapter.GetProxyInstance().GenerateEncoreEncryptedSalesFile(EncoreMemberList.ToArray());
                    //string nothing = "nothing";
                    // Put the file into the DB
                    SubscriptionBL.InsertMatchbackFile(outputFileContent, lineID);

                    // Update the matchback records as processed
                    SubscriptionBL.MarkMatchbackRequestAsComplete(mbCol);
                }

                return EncoreMemberList.Count;
            }
            catch (Exception ex)
            {
                ServiceUtil.LogEvent("WWW", "GenerateEncryptedSalesFile(): " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                return Matchnet.Constants.NULL_INT;
            }
        }
        else
        {
            throw new Exception(ticketkey + "GenerateEncryptedSalesFile(): Invalid Access key.");
        }   
    }
}

