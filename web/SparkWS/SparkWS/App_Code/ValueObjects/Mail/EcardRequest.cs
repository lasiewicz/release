﻿using System;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// a base class encapsulating an send ecard request.
/// only the recipient info is not specified and when inheriting
/// this is what you will usually be extended
/// </summary>
[Serializable]
public abstract class EcardRequest : VersionedVO
{
    private EcardInfo ecardInfo = null;

    /// <summary>
    /// id of the member that is sending the card
    /// </summary>
    private int fromMember = 0;

    /// <summary>
    /// the community id for which this card is intended
    /// </summary>
    private int communityID = 0;

    /// <summary>
    /// the site ID from which we are sending the ecard
    /// </summary>
    private int siteID = 0;

    /// <summary>
    /// the id of the member sending the card
    /// </summary>
    public int FromMember
    {
        get { return fromMember; }
        set { fromMember = value; }
    }

    /// <summary>
    /// the id of the community we are sending the ecard to
    /// </summary>
    public int CommunityID
    {
        get { return communityID; }
        set { communityID = value; }
    }

    /// <summary>
    /// the site id where this card is being sent
    /// </summary>
    public int SiteID
    {
        get { return siteID; }
        set { siteID = value; }
    }

    /// <summary>
    /// card info and description
    /// </summary>
    public EcardInfo EcardInfo
    {
        get { return ecardInfo; }
        set
        {
            if (value == null)
            {
                throw new SparkWSClientException("The card info cannot be null when sending an ecard request");
            }

            ecardInfo = value;
        }
    }

    public EcardRequest(string version, int fromMember, int communityID, int siteID, EcardInfo ecardInfo)
        :base(version)
    {
        this.fromMember = fromMember;
        this.communityID = communityID;
        this.siteID = siteID;
        //just so we make sure the card exists
        EcardInfo = ecardInfo;
    }

    protected EcardRequest():base("")
    { }
}
