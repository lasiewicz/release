﻿using System;
using System.Data;
using System.Configuration;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Used to make a request to send an ecard from one user to another
/// </summary>
[Serializable]
public class InternalEcardRequest:EcardRequest
{
    private InternalEcardRequest()
    { }

    /// <summary>
    /// the member to which the ecard is sent
    /// </summary>
    private int toMember;

    /// <summary>
    /// get/set the member to which the ecard should be sent
    /// </summary>
    public int ToMember
    {
        get { return toMember; }
        set { toMember = value; }
    }

    public InternalEcardRequest(string version, int toMember, int fromMember, int communityID, int siteID, EcardInfo ecardInfo)
        :base(version, fromMember, communityID, siteID, ecardInfo)
    {
        this.toMember = toMember;
    }

}

