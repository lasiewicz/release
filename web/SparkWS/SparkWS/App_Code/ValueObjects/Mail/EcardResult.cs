﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// The result of sending an ecard
/// </summary>
[Serializable]
public class EcardResult:VersionedVO
{
    private bool success = false;
    private EcardSendResult detailedSendResult;

    public EcardResult(bool success, EcardSendResult detailedSendResult):base("1.0.0")
    {
        this.detailedSendResult = detailedSendResult;
        this.success = success;
    }

    private EcardResult()
        : base("")
    { }

    /// <summary>
    /// get/set whether the request was succesfull or not
    /// </summary>
    public bool Success
    {
        get { return success; }
        set { success = value; }
    }

    /// <summary>
    /// detail description of the send result
    /// usefull for when the operation fails
    /// </summary>
    public EcardSendResult DetailedSendResult
    {
        get { return detailedSendResult; }
        set { detailedSendResult = value; }
    }

    /// <summary>
    /// the id of the message that was sent to the user
    /// when an internal ecard has been sent
    /// </summary>
    private int messageID;

    /// <summary>
    /// the id of the message sehnt to the user when he recieved
    /// an internal ecard. this should later be used with the
    /// MarkMessageAsRead
    /// </summary>
    public int MessageID
    {
        get { return messageID; }
        set { messageID = value; }
    }
}


/// <summary>
/// the result to a try to send an ecard
/// </summary>
public enum EcardSendResult
{ 
    InvalidSender,
    InvalidRecipient,
    InvalidRecipientEmail,
    DailyQuotaReached,
    SenderOnIgnoreList,
    Success
}
