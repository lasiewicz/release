﻿using System;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Represents a result of marking a message as read
/// </summary>
[Serializable()]
public class MarkMessageReadResult : VersionedVO
{
    /// <summary>
    /// the id of the member for which we have to set a message as read
    /// </summary>
    private bool success= false;

    public MarkMessageReadResult()
        : base("0.0.0")
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary>
    /// Creates a new markmessagereadresult to be used to notify if a message was
    /// succesfully marked as read
    /// </summary>
    /// <param name="version">the version of the request</param>
    /// <param name="success">whether marking the message as read was succesfull</param>
    public MarkMessageReadResult(string version, bool success)
        :
        base(version)
    {
        this.success = success;
    }

    /// <summary>
    /// whether the message was marked read succesfully
    /// </summary>
    public bool Success
    {
        get { return success; }
        set { success = value; }
    }

}
