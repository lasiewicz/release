﻿using System;
using System.Collections.Generic;
using System.Web;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;

/// <summary>
/// Summary description for MessmoSubscribeResponse
/// </summary>
[Serializable]
public class MessmoSubscribeResponse : VersionedVO
{
    private string _status = Constants.NULL_STRING;

    public string Status
    {
        get { return _status; }
        set { _status = value; }
    }

    public MessmoSubscribeResponse() : base("0.0.0")
    {
        
    }

    public MessmoSubscribeResponse(string version, string status)
        : base(version)
    {
        _status = status;
    }
}
