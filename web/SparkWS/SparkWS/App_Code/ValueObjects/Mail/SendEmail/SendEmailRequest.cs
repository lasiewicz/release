using System;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;
/// <summary>
/// Instance of this class is returned as a result of SendEmail call.
/// </summary>
[Serializable()]
public class SendEmailRequest : VersionedVO
{
    #region Private Variables
    
    private int fromMemberID = Constants.NULL_INT;
    private int toMemberID = Constants.NULL_INT;
    private string body = Constants.NULL_STRING;
    private string subject = Constants.NULL_STRING;
    private int brandID = Constants.NULL_INT;

    #endregion

    #region Public Properties

    public int FromMemberID 
    {
        get
        {
            return fromMemberID;
        }
        set
        {
            fromMemberID = value;
        }
    }
    public int ToMemberID 
    {
        get
        {
            return toMemberID;
        }
        set
        {
            toMemberID = value;
        }
    }
    public string Body 
    {
        get
        {
            return body;
        }
        set
        {
            body = value;
        }
    }
    public string Subject
    {
        get
        {
           return subject;
        }
        set
        {
            subject = value;
        }
    }
    public int BrandID
    {
        get
        {
            return brandID;
        }
        set
        {
            brandID = value;
        }
    }

    #endregion

    #region Constructors

    public SendEmailRequest() : base("0.0.0")
    {
    }

    /// <summary>
    /// Constructor for SendEmailRequest
    /// </summary>
    /// <param name="version">Version of the class</param>
    /// <param name="fromMemberID">Sender's Member ID</param>
    /// <param name="toMemberID">Recipient's Member ID</param>
    /// <param name="brandID">Brand's ID</param>
    /// <param name="subject">Subject of the message</param>
    /// <param name="body">Body of the message</param>
    public SendEmailRequest(string version, int fromMemberID, int toMemberID, int brandID, string subject, string body) : base(version)
    {
        this.fromMemberID = fromMemberID;
        this.toMemberID = toMemberID;
        this.brandID = brandID;
        this.subject = subject;
        this.body = body;
    }

    #endregion
}
