using System;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;

/// <summary>
/// Instance of this class is required for making SendEmail method call.
/// </summary>
[Serializable()]
public class SendEmailResponse : VersionedVO
{
    #region Private Variables

    private string status = Constants.NULL_STRING;
    private int messageID = Constants.NULL_INT;
    private int senderMessageID = Constants.NULL_INT;
    private bool onIgnoreList = false;

    #endregion

    #region Public Properties

    public string Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }
    public int MessageID
    {
        get
        {
            return messageID;
        }
        set
        {
            messageID = value;
        }
    }
    public int SenderMessageID
    {
        get
        {
            return senderMessageID;
        }
        set
        {
            senderMessageID = value;
        }
    }
    public bool OnIgnoreList
    {
        get
        {
            return onIgnoreList;
        }
        set
        {
            onIgnoreList = value;
        }
    }

    #endregion

    public SendEmailResponse() : base("0.0.0")
    {
    }

    /// <summary>
    /// Constructor for SendEmailResponse
    /// </summary>
    /// <param name="status">Returns "Success" or "Failed".</param>
    /// <param name="messageID">Message ID of the recipient.</param>
    /// <param name="senderMessageID">Message ID of the sender.</param>
    public SendEmailResponse(string version, string status, int messageID, int senderMessageID, bool onIgnoreList) : base(version)
    {
        this.status = status;
        this.messageID = messageID;
        this.senderMessageID = senderMessageID;
        this.onIgnoreList = onIgnoreList;
    }
}
