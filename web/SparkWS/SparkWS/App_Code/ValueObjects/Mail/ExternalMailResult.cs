using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;

/// <summary>
/// 
/// </summary>
[Serializable]
public class ExternalMailResult : VersionedVO
{
    #region Private Variables

    private bool sent = false;
    private string reason = string.Empty;

    #endregion

    #region Public Properties

    public bool Sent
    {
        get { return sent; }
        set { sent = value; }
    }

    public string Reason
    {
        get { return reason; }
        set { reason = value; }
    }

    #endregion

    #region Constructors

    /// <summary>
    /// For serialization.
    /// </summary>
    public ExternalMailResult() : base("1.0.0.0")
    {
    }

    /// <summary>
    /// Returns a result from sending an email through Spark's external mail service.
    /// </summary>
    /// <param name="sent">True is a email queue is successfully sent to Spark's external mail service.</param>
    /// <param name="reason">Reason for non successful send call to Spark's external mail service.</param>
    public ExternalMailResult(bool sent, string reason): base("1.0.0")
    {
        this.sent = sent;
        this.reason = reason;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <returns>Sent status and reason.</returns>
    public override string ToString()
    {
        return "Sent: " + sent.ToString() + ", Reason: " + reason;
    }
}