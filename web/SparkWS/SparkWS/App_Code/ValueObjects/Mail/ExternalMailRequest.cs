using System;

using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.ExternalMail.ValueObjects.Impulse;

/// <summary>
/// 
/// </summary>
public class ExternalMailRequest : VersionedVO
{
    #region Private Variables

    private int externalMailTypeID;
    private string[] values;
    private int memberID;
    private int siteID;

    #endregion

    #region Public Properties

    public int ExternalMailTypeID
    {
        get { return externalMailTypeID; }
        set { externalMailTypeID = value; }
    }

    public string[] Values
    {
        get { return values; }
        set { values = value; }
    }
    public int MemberID
    {
        get { return memberID; }
        set { memberID = value; }
    }

    public int SiteID
    {
        get { return siteID; }
        set { siteID = value; }
    }
    #endregion

    #region Constructors

    /// <summary>
    /// For serialization.
    /// </summary>
    public ExternalMailRequest() : base("0.0.0")
    {
    }

    public ExternalMailRequest(string version, int memberID, int siteID, int externalMailTypeID, string[] values) : base(version)
    {
        this.memberID = memberID;
        this.siteID = siteID;
        this.externalMailTypeID = externalMailTypeID;
        this.values = values;
    }

    #endregion
}
