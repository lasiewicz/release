﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;

/// <summary>
/// Summary description for MessmoSubscribeRequest
/// </summary>
[Serializable]
public class MessmoSubscribeRequest : VersionedVO
{
    #region Private Members
    private string _command = Constants.NULL_STRING;
    private string _messmoKey = Constants.NULL_STRING;
    private string _channelNumber = Constants.NULL_STRING;
    private string _timestamp = Constants.NULL_STRING;
    private int _sequence = Constants.NULL_INT;
    private string _subscriberId = Constants.NULL_STRING;
    private string _message = Constants.NULL_STRING;
    private string _apiVersion = Constants.NULL_STRING;
    #endregion

    #region Properties
    public string Command
    {
        get { return _command; }
        set { _command = value; }
    }
    public string MessmoKey
    {
        get { return _messmoKey; }
        set { _messmoKey = value; }
    }
    public string ChannelNumber
    {
        get { return _channelNumber; }
        set { _channelNumber = value; }
    }
    public string TimeStamp
    {
        get { return _timestamp; }
        set { _timestamp = value; }
    }
    public int Sequence
    {
        get { return _sequence; }
        set { _sequence = value; }
    }
    public string SubscriberId
    {
        get { return _subscriberId; }
        set { _subscriberId = value; }
    }
    public string Message
    {
        get { return _message; }
        set { _message = value; }
    }
    public string ApiVersion
    {
        get { return _apiVersion; }
        set { _apiVersion = value; }
    }
    #endregion

    public MessmoSubscribeRequest() : base ("0.0.0")
    {
        
    }

    public MessmoSubscribeRequest(string version, string command, string messmoKey, string channelNumber, string timestamp,
        int sequence, string subscriberId, string message, string apiVersion) : base(version)
    {
        _command = command;
        _messmoKey = messmoKey;
        _channelNumber = channelNumber;
        _timestamp = timestamp;
        _sequence = sequence;
        _subscriberId = subscriberId;
        _message = message;
        _apiVersion = apiVersion;
    }

    public string GetValuesString()
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("Command: " + _command);
        sb.Append(" MessmoKey: " + _messmoKey);
        sb.Append(" ChannelNumber: " + _channelNumber);
        sb.Append(" Timestamp: " + _timestamp);
        sb.Append(" Sequence: " + _sequence);
        sb.Append(" SubscriberID: " + _subscriberId);
        sb.Append(" Message: " + _message);
        sb.Append(" APIVersion: " + _apiVersion);


        return sb.ToString();
        
    }


}
