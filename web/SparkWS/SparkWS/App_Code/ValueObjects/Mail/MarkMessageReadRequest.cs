﻿using System;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Represents a request to mark an internal message as read
/// </summary>
[Serializable()]
public class MarkMessageReadRequest:VersionedVO
{
    /// <summary>
    /// the id of the member for which we have to set a message as read
    /// </summary>
    private int memberID = -1;

    /// <summary>
    /// the ID of the message to set as read
    /// </summary>
    private int messageID = -1;

    /// <summary>
    /// the ID of the community to set the message as read
    /// </summary>
    private int communityID = -1;


    public MarkMessageReadRequest():base("0.0.0")
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary>
    /// creates a new markmessagereadrequest
    /// </summary>
    /// <param name="version">the version of the request</param>
    /// <param name="memeberID">the member ID for this request</param>
    /// <param name="communityID">the community where the memeber/message resuides</param>
    /// <param name="messageID">the id of the message to mark as read</param>
    public MarkMessageReadRequest(string version, int memberID, int communityID, int messageID):
        base(version)
    {
        this.memberID = memberID;
        this.communityID = communityID;
        this.messageID = messageID;
    }

    /// <summary>
    /// get/set the id of the member we want to mark a message as read
    /// </summary>
    public int MemberID
    {
        get { return memberID; }
        set { memberID = value; }
    }

    /// <summary>
    /// get/set the id of the message to mark as read
    /// </summary>
    public int MessageID
    {
        get { return messageID; }
        set { messageID = value; }
    }

    /// <summary>
    /// get/set the community where the message/member resides
    /// </summary>
    public int CommunityID
    {
        get { return communityID; }
        set { communityID = value; }
    }

    
}
