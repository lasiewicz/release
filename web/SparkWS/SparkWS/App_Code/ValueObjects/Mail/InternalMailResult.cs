﻿using System;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Result to an internal mail request
/// </summary>
[Serializable()]
public class InternalMailResult:VersionedVO
{
    /// <summary>
    /// whether the mail was successfully sent
    /// </summary>
    private bool success = false;

    /// <summary>
    /// whether one of the reciever has placed the sender on an ignore list
    /// </summary>
    private bool onIgnoreList = false;

    /// <summary>
    /// the ID that was assigned if the message was sent succesfully
    /// </summary>
    private int messageID = -1;

    /// <summary>
    /// the ID that was assigned for the sender message ID(sent inbox)
    /// </summary>
    private int senderMessageID = -1;

    public InternalMailResult():base("0.0.0")
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary>
    /// creates a new internalmailresult
    /// </summary>
    /// <param name="version">the version of the result</param>
    /// <param name="messageID">the message ID that was assigned.
    /// it will be used later on to mark the message as read later on</param>
    /// <param name="success">whether it was succesfull</param>
    /// <param name="onIgnoreList">whether the reciever has placed the sender on their ignore list
    /// this can be True if the Success was false</param>
    public InternalMailResult(string version, int messageID, int senderMessageID, bool success, bool onIgnoreList)
        :base(version)
    {
        this.senderMessageID = senderMessageID;
        this.success = success;
        this.onIgnoreList = onIgnoreList;
        this.messageID = messageID;
    }

    /// <summary>
    /// get/set whether the internal message was sent succesfully
    /// </summary>
    public bool Success
    {
        get { return success; }
        set { success = value; }
    }

    /// <summary>
    /// get/set whether the sender is on the recievers ignore list.
    /// if this is true the message will fail to send.
    /// </summary>
    public bool OnIgnoreList
    {
        get { return onIgnoreList; }
        set { onIgnoreList = value; }
    }

    /// <summary>
    /// get/set the message ID assigned to this message if it was succesfull
    /// </summary>
    public int MessageID
    {
        get { return messageID;  }
        set { messageID = value; }
    }


    /// <summary>
    /// the sent inbox message id for the sender
    /// </summary>
    public int SenderMessageID
    {
        get { return senderMessageID; }
        set { senderMessageID = value; }
    }

}
