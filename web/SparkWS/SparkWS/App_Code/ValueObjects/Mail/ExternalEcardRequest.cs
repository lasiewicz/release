﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

/// <summary>
/// Used to make a request to send an ecard from one member to an external email adress
/// </summary>
[Serializable]
public class ExternalEcardRequest : EcardRequest
{
    /// <summary>
    /// the email the card should be send to
    /// </summary>
    private string toEmail;

    /// <summary>
    /// get/set the email the card should be send to
    /// </summary>
    public string ToEmail
    {
        get { return toEmail; }
        set
        {
            if (isEmail(value))
            {
                toEmail = value;
            }
            else
            {
                throw new SparkWSClientException("The email you are trying to send an ecard to is invalid");
            }

        }
    }

    public ExternalEcardRequest(string version, string toEmail, int fromMember, int communityID, int siteID, EcardInfo ecardInfo)
        : base(version, fromMember, communityID, siteID, ecardInfo)
    {
        ToEmail = toEmail;
    }

    /// <summary>
    /// for serialization use
    /// </summary>
    private ExternalEcardRequest()
    { }

    /// <summary>
    /// checks whether a particular string is an email
    /// </summary>
    /// <param name="inputEmail">the string to check</param>
    /// <returns>true/false whether the email is real</returns>
    private static bool isEmail(string inputEmail)
    {
        string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
              @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
              @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        Regex re = new Regex(strRegex);
        if (re.IsMatch(inputEmail))
            return (true);
        else
            return (false);
    }
}
