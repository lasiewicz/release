using System;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;

/// <summary>
/// Instance of this class is returned as a result of SendEmail call.
/// </summary>
[Serializable()]
public class Email : VersionedVO
{
    #region Private Variables

    private string subject;
    private string body;
    private string createdDateTime;
    private int fromMemberID;
    private string fromUserName;
    private int toMemberID;
    private string toUserName;
    private string mailType;
    private string status;
    private string openDateTime;

    #endregion

    #region Public Properties

    public string Subject
    {
        get
        {
            return subject;
        }
        set
        {
            subject = value;
        }
    }
    public string Body
    {
        get
        {
            return body;
        }
        set
        {
            body = value;
        }
    }
    public string CreatedDateTime
    {
        get
        {
            return createdDateTime;
        }
        set
        {
            createdDateTime = value;
        }
    }
    public int FromMemberID
    {
        get
        {
            return fromMemberID;
        }
        set
        {
            fromMemberID = value;
        }
    }
    public string FromUserName
    {
        get
        {
            return fromUserName;
        }
        set
        {
            fromUserName = value;
        }
    }
    public int ToMemberID
    {
        get
        {
            return toMemberID;
        }
        set
        {
            toMemberID = value;
        }
    }
    public string ToUserName
    {
        get
        {
            return toUserName;
        }
        set
        {
            toUserName = value;
        }
    }
    public string MailType
    {
        get
        {
            return mailType;
        }
        set
        {
            mailType = value;
        }
    }
    public string Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }
    public string OpenDateTime
    {
        get
        {
            return openDateTime;
        }
        set
        {
            openDateTime = value;
        }
    }

    #endregion

    #region Constructors

    public Email() : base("0.0.0")
    {
    }

    public Email(string version, string subject, string body, string createdDateTime, int fromMemberID, string fromUserName, int toMemberID, string toUserName, string mailType, string status, string openDateTime) : base(version)
    {
        this.subject = subject;
        this.body = body;
        this.createdDateTime = createdDateTime;
        this.fromMemberID = fromMemberID;
        this.fromUserName = fromUserName;
        this.toMemberID = toMemberID;
        this.toUserName = toUserName;
        this.mailType = mailType;
        this.status = status;
        this.openDateTime = openDateTime;
    }

    #endregion
}
