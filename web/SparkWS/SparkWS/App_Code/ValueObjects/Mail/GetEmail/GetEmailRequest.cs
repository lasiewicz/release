using System;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;
using System.Collections.Generic;

/// <summary>
/// Instance of this class is used as a request to GetEmail method.
/// </summary>
[Serializable()]
public class GetEmailRequest : VersionedVO
{
    #region Private Variables

    private int memberID = Constants.NULL_INT;
    private int brandID = Constants.NULL_INT;
    private int startRow = Constants.NULL_INT;
    private int pageSize = Constants.NULL_INT;

    #endregion

    #region Public Properties

    public int MemberID
    {
        get
        {
            return memberID ;
        }
        set
        {
            memberID = value;
        }
    }
    public int BrandID
    {
        get
        {
            return brandID;
        }
        set
        {
            brandID = value;
        }
    }
    public int StartRow
    {
        get
        {
            return startRow;
        }
        set
        {
            startRow = value;
        }
    }
    public int PageSize
    {
        get
        {
            return pageSize;
        }
        set
        {
            pageSize = value;
        }
    }

    #endregion

    #region Constructors

    public GetEmailRequest()
        : base("0.0.0")
    {
    }

    public GetEmailRequest(string version, int memberID, int brandID, int startRow, int pageSize)
        : base(version)
    {
        this.memberID = memberID;
        this.brandID = brandID;
        this.startRow = startRow;
        this.pageSize = pageSize;
    }

    #endregion
}
