using System;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;
using System.Collections.Generic;

/// <summary>
/// Instance of this class is returned as a result of GetEmail call.
/// </summary>
[Serializable()]
public class GetEmailResponse : VersionedVO
{
    #region Private Variables

    private List<Email> emails = null;
    private int total = Constants.NULL_INT;

    #endregion

    #region Public Properties

    public List<Email> Emails
    {
        get
        {
            return emails;
        }
        set
        {
            emails = value;
        }
    }
    public int Total
    {
        get
        {
            return total;
        }
        set
        {
            total = value;
        }
    }

    #endregion

    #region Constructors

    public GetEmailResponse()
        : base("0.0.0")
    {
    }

    public GetEmailResponse(string version, List<Email> emails, int total)
        : base(version)
    {
        this.emails = emails;
        this.total = total;
    }

    #endregion
}
