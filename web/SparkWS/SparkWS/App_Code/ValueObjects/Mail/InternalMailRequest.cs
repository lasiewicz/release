﻿using System;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Represents an internal mail request
/// </summary>
[Serializable()]
public class InternalMailRequest:VersionedVO
{

    #region "Private Fields"
    /// <summary>
    /// the member from which this message originates
    /// </summary>
    private int fromMemberID = -1;

    /// <summary>
    /// the member that should recieve the message
    /// </summary>
    private int toMemberID = -1;

    /// <summary>
    /// the body of the message
    /// </summary>
    private string body = string.Empty;

    /// <summary>
    /// the message's subject
    /// </summary>
    private string subject = string.Empty ;

    /// <summary>
    /// the type of the message
    /// </summary>
    private int messageType = 0;

    /// <summary>
    /// the community for which this message is intendet
    /// </summary>
    private int communityID = 0;

    /// <summary>
    /// the site where this message was initiated
    /// </summary>
    private int siteID = 0;

    #endregion

    #region "Public Fields"
    /// <summary>
    /// get/set the member from which this message originates
    /// </summary>
    public int FromMemberID
    {
        get { return fromMemberID; }
        set { fromMemberID = value; }
    }

    /// <summary>
    /// get/set the member that should recieve the message
    /// </summary>
    public int ToMemberID
    {
        get { return toMemberID; }
        set { toMemberID = value; }
    }

    /// <summary>
    /// get/set the body of the message
    /// </summary>
    public string Body
    {
        get { return body; }
        set { body = value; }
    }

    /// <summary>
    /// get/set the message's subject
    /// </summary>
    public string Subject
    {
        get { return subject; }
        set { subject = value; }
    }

    /// <summary>
    /// get/set the community ID to which this message should be sent
    /// </summary>
    public int CommunityID
    {
        get { return communityID; }
        set { communityID = value; }
    }

    /// <summary>
    /// get/set the site ID where this message was initiated  
    /// </summary>
    public int SiteID
    {
        get { return siteID; }
        set { siteID = value; }
    }

    #endregion


    #region "Constructors"

    public InternalMailRequest():base("0.0.0")
    {

    }

    /// <summary>
    /// fully initializable constructor
    /// </summary>
    /// <param name="version">the version of this class</param>
    /// <param name="fromMemberID">the sender's ID</param>
    /// <param name="toMemberID">the reciever's ID</param>
    /// <param name="communityID">the communityID for which this message is intended</param>
    /// <param name="subject">the subject of the message</param>
    /// <param name="body">the body of the message</param>
    /// <param name="messageType">the type of message</param>
    public InternalMailRequest(string version, int fromMemberID, int toMemberID, int communityID, string subject, string body, int messageType):
        base(version)
    {

        //set the initial variables for the class
        this.fromMemberID = fromMemberID;
        this.toMemberID = toMemberID;
        this.communityID = communityID;
        this.subject = subject;
        this.body = body;
        this.messageType = messageType;
    }

    /// <summary>
    /// fully initializable constructor
    /// </summary>
    /// <param name="version">the version of this class</param>
    /// <param name="fromMemberID">the sender's ID</param>
    /// <param name="toMemberID">the reciever's ID</param>
    /// <param name="communityID">the communityID for which this message is intended</param>
    /// <param name="siteID">the siteID of where this message was initiated</param>
    /// <param name="subject">the subject of the message</param>
    /// <param name="body">the body of the message</param>
    /// <param name="messageType">the type of message</param>
    public InternalMailRequest(string version, int fromMemberID, int toMemberID, int communityID, int siteID, string subject, string body, int messageType)
        :
        base(version)
    {

        //set the initial variables for the class
        this.fromMemberID = fromMemberID;
        this.toMemberID = toMemberID;
        this.communityID = communityID;
        this.siteID = siteID;
        this.subject = subject;
        this.body = body;
        this.messageType = messageType;
    }

    #endregion

}
