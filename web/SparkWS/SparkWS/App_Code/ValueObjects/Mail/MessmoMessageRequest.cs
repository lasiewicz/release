﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;

/// <summary>
/// Summary description for MessmoMessageRequest
/// </summary>
[Serializable]
public class MessmoMessageRequest : VersionedVO
{
    #region Private Members
    private string _command = Constants.NULL_STRING;
    private string _channelNumber = Constants.NULL_STRING;
    private string _timestamp = Constants.NULL_STRING;
    private int _sequence = Constants.NULL_INT;
    private string _subscriberId = Constants.NULL_STRING;
    private string _message = Constants.NULL_STRING;
    private string _attachmentUrl = Constants.NULL_STRING;
    private string _attachmentType = Constants.NULL_STRING;
    private string _userOriginatedType = Constants.NULL_STRING;
    private string _taggle = Constants.NULL_STRING;
    private string _supplementary = Constants.NULL_STRING;
    private bool _suspend = false;
    private string _votingId = Constants.NULL_STRING;
    private string _apiVersion = Constants.NULL_STRING;
    #endregion

    #region Public Properties
    public string Command
    {
        get { return _command; }
        set { _command = value; }
    }
    public string ChannelNumber
    {
        get { return _channelNumber; }
        set { _channelNumber = value; }
    }
    public string TimeStamp
    {
        get { return _timestamp; }
        set { _timestamp = value; }
    }
    public int Sequence
    {
        get { return _sequence; }
        set { _sequence = value; }
    }
    public string SubscriberId
    {
        get { return _subscriberId; }
        set { _subscriberId = value; }
    }
    public string Message
    {
        get { return _message; }
        set { _message = value; }
    }
    public string AttachmentUrl
    {
        get { return _attachmentUrl; }
        set { _attachmentUrl = value; }
    }
    public string AttachmentType
    {
        get { return _attachmentType; }
        set { _attachmentType = value; }
    }
    public string UserOriginatedType
    {
        get { return _userOriginatedType; }
        set { _userOriginatedType = value; }
    }
    public string Taggle
    {
        get { return _taggle; }
        set { _taggle = value; }
    }
    public string Supplementary
    {
        get { return _supplementary; }
        set { _supplementary = value; }
    }
    public bool Suspend
    {
        get { return _suspend; }
        set { _suspend = value; }
    }
    public string VotingID
    {
        get { return _votingId; }
        set { _votingId = value; }
    }
    public string ApiVersion
    {
        get { return _apiVersion; }
        set { _apiVersion = value; }
    }
    #endregion

    public MessmoMessageRequest() : base("0.0.0")
    {
        
    }

    public MessmoMessageRequest(string version, string command, string channelNumber, string timeStamp, int sequence,
        string subscriberID, string message, string attachmentUrl, string attachmentType, string userOriginatedType,
        string taggle, string supplementary, bool suspend, string votingId, string apiVersion)
        : base(version)
    {
        _command = command;
        _channelNumber = channelNumber;
        _timestamp = timeStamp;
        _sequence = sequence;
        _subscriberId = subscriberID;
        _message = message;
        _attachmentUrl = attachmentUrl;
        _attachmentType = attachmentType;
        _userOriginatedType = userOriginatedType;
        _taggle = taggle;
        _supplementary = supplementary;
        _suspend = suspend;
        _votingId = votingId;
        _apiVersion = apiVersion;
    }

    public string GetValuesString()
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("Command: " + _command);
        sb.Append(" Channel number: " + _channelNumber);
        sb.Append(" Timestamp: " + _timestamp);
        sb.Append(" Sequence: " + _sequence);
        sb.Append(" SubscriberId: " + _subscriberId);
        sb.Append(" Message: " + _message);
        sb.Append(" AttachmentUrl: " + _attachmentUrl);
        sb.Append(" AttachmentType: " + _attachmentType);
        sb.Append(" UserOrginatedType: " + _userOriginatedType);
        sb.Append(" Taggle: " + _taggle);
        sb.Append(" Supplementary: " + _supplementary);
        sb.Append(" Suspend: " + _suspend);
        sb.Append(" VotingId: " + _votingId);
        sb.Append(" APIVersion: " + _apiVersion);
        
        return sb.ToString();
    }
}
