﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ValueObjects.SparkWS;


/// <summary>
/// Holds ecard info such as absolute URL, category and so forth
/// </summary>
[Serializable]
public class EcardInfo:VersionedVO
{
    /// <summary>
    /// the id of the card 
    /// </summary>
    private int id;

    /// <summary>
    /// the url where the card is located
    /// </summary>
    private string url;

    /// <summary>
    /// the thumb url for this card
    /// </summary>
    private string thumbUrl;

    /// <summary>
    /// the category to which this card belongs
    /// </summary>
    private string category;

    /// <summary>
    /// the name of the card being send
    /// </summary>
    private string name;

    /// <summary>
    /// get/set the ID of the card
    /// </summary>
    public int Id
    {
        get { return id; }
        set { id = value; }
    }

    /// <summary>
    /// get/set the card url where the card can be viewed
    /// </summary>
    public string Url
    {
        get { return url; }
        set
        {
            if (value.Trim().Length == 0)
            {
                throw new SparkWSClientException("The card URL cannot be empty.");
            }

            url = value;
        }
    }

    /// <summary>
    /// get/set the card's thumb url where the card can be viewed
    /// </summary>
    public string ThumbUrl
    {
        get { return thumbUrl; }
        set
        {
            if (value.Trim().Length == 0)
            {
                throw new SparkWSClientException("The card's thumb URL cannot be empty.");
            }

            thumbUrl = value;
        }
    }

    /// <summary>
    /// get/set the card's category string representation
    /// </summary>
    public string Category
    {
        get { return category; }
        set { category = value; }
    }

    /// <summary>
    /// get/set the name of the card
    /// </summary>
    public string Name
    {
        get { return name; }
        set
        {
            if (value.Trim().Length == 0)
            {
                throw new SparkWSClientException("The card's name cannot be empty");
            }

            name = value;
        }
    }

    /// <summary>
    /// creates a new representation of a card
    /// </summary>
    /// <param name="id">the unique identifier of the card</param>
    /// <param name="url">the url of the card</param>
    /// <param name="thumbUrl">the thumb URL for this card</param>
    /// <param name="name">the name of the card</param>
    /// <param name="category">the category of the card</param>
    /// <param name="version">the version of this info object</param>
    public EcardInfo(string version, int id, string url, string thumbUrl, string name, string category):base(version)
    {
        this.id = id;
        Url = url;
        Name = name;
        Category = category;
        ThumbUrl = thumbUrl;
    }

    private EcardInfo()
        : base("")
    { }
}
