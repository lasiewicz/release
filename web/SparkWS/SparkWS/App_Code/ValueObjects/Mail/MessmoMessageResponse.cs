﻿using System;
using System.Collections.Generic;
using System.Web;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;

/// <summary>
/// Summary description for MessmoMessageResponse
/// </summary>
[Serializable()]
public class MessmoMessageResponse : VersionedVO
{
    #region Private Members
    private string _status = Constants.NULL_STRING;
    private int _messageID = Constants.NULL_INT;
    private int _senderMessageID = Constants.NULL_INT;
    private bool _onIgnoreList = false;
    #endregion

    #region Properties
    public string Status
    {
        get
        {
            return _status;
        }
        set
        {
            _status = value;
        }
    }
    public int MessageID
    {
        get
        {
            return _messageID;
        }
        set
        {
            _messageID = value;
        }
    }
    public int SenderMessageID
    {
        get
        {
            return _senderMessageID;
        }
        set
        {
            _senderMessageID = value;
        }
    }
    public bool OnIgnoreList
    {
        get
        {
            return _onIgnoreList;
        }
        set
        {
            _onIgnoreList = value;
        }
    }
    #endregion

    public MessmoMessageResponse() : base("0.0.0")
    {
        
    }

    public MessmoMessageResponse(string version, string status, int messageId, int senderMessageId, bool onIgnoreList)
        : base(version)
    {
        _status = status;
        _messageID = messageId;
        _senderMessageID = senderMessageId;
        _onIgnoreList = onIgnoreList;
    }
}
