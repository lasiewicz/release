using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Security.Permissions;
using System.Xml.Serialization;
using System.Xml;


using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Requests to set the external video URL for a memebr of a community to the value supplied.
/// </summary>
[Serializable()]
public class ExternalVideoUrlRequest: VersionedVO
{
   /// <summary>
   /// The MemberID of the person on in the Spark network.
   /// </summary>
   public int MemberID;
   /// <summary>
   /// The site to which this URL should be attached.
   /// </summary>
   public int SiteID;
   /// <summary>
   /// The URL of the external video media.
   /// </summary>
   public string URL;
   
   /// <summary>
   /// Constructor
   /// </summary>
   public ExternalVideoUrlRequest() : base("0.0.0") { }
   /// <summary>
   /// Constructor
   /// </summary>
   /// <param name="version">Current supported version is "1.0.0"</param>
   /// <param name="memberID">The MemberID of the person on in the Spark network.</param>
   /// <param name="siteID">The site to which this URL should be attached.</param>
   /// <param name="url">The URL of the external video media.</param>
   public ExternalVideoUrlRequest(string version, int memberID, int siteID, string url) : this() 
   {
	  MemberID = memberID;
	  SiteID = siteID;
	  URL = url;
   }
}
