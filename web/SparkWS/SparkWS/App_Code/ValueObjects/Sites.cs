using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Matchnet;
/// <summary>
/// Summary description for Sites
/// </summary>
[Serializable]
public class Sites :  KeyedCollection<int, Matchnet.Content.ValueObjects.BrandConfig.Site>,  IValueObject, ICacheable
{
    public const string SPARKWS_SITES_CACHE_KEY = "~SPARKWSALLSITES";

    #region class variables
    private int _CacheTTLSeconds;
    private CacheItemPriorityLevel _CachePriority;
    private string _CacheKey;
    #endregion

    public Sites(): base() { }
    

    protected override int GetKeyForItem(Matchnet.Content.ValueObjects.BrandConfig.Site s)
    {
        return s.SiteID;
    }

    #region ICacheable Members

      public int CacheTTLSeconds
    {
        get
        {
            return _CacheTTLSeconds;
        }
        set
        {
            _CacheTTLSeconds = value;
        }
    }

    public Matchnet.CacheItemMode CacheMode
    {
        get
        {
            return CacheItemMode.Absolute;
        }
    }

    public Matchnet.CacheItemPriorityLevel CachePriority
    {
        get
        {
            return _CachePriority;
        }
        set
        {
            _CachePriority = value;
        }
    }

    public string GetCacheKey()
    {
        return SPARKWS_SITES_CACHE_KEY;
    }



    #endregion
}
