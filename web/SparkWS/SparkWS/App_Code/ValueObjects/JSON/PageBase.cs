﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Threading;
using System.Security.Principal;

/// <summary>
/// Summary description for PageBase
/// </summary>
public class PageBase : System.Web.UI.Page
{
    public const string REMOTE_CLIENT_IP_KEY = "RemoteClientHost";

    protected override void OnInit(EventArgs e)
    {
        string remoteClientHostIP = HttpContext.Current.Request.Headers["client-ip"];
        if (remoteClientHostIP == null)
            remoteClientHostIP = HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];

        Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(remoteClientHostIP), null);
        Thread.SetData(Thread.GetNamedDataSlot(REMOTE_CLIENT_IP_KEY), remoteClientHostIP);

        base.OnInit(e);
    }
}
