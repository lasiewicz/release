﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// JSON messaging request
/// </summary>
[Serializable]
public class MessageRequest : RequestBase
{
    #region Private Variables

    private int externalMailTypeID;
    private string[] values;
    private int memberID;
    private int siteID;

    #endregion

    #region Public Properties

    public int ExternalMailTypeID
    {
        get { return externalMailTypeID; }
        set { externalMailTypeID = value; }
    }

    public string[] Values
    {
        get { return values; }
        set { values = value; }
    }
    public int MemberID
    {
        get { return memberID; }
        set { memberID = value; }
    }

    public int SiteID
    {
        get { return siteID; }
        set { siteID = value; }
    }
    #endregion

	public MessageRequest()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}
