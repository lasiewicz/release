﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// JSON base request
/// </summary>
[Serializable]
public class RequestBase
{
    private string accessKey;
    private string action;

    public string AccessKey { get { return accessKey; } set { accessKey = value; } }
    public string Action { get { return action; } set { action = value; } }

	public RequestBase()
	{
        accessKey = "";
        action = JSONAction.None.ToString();
	}
}

public enum JSONAction : int
{
    None = 0,
    SendExternalMail = 1
}
