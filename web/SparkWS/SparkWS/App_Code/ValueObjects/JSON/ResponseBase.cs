﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// JSON base response
/// </summary>
[Serializable]
public class ResponseBase
{
    private string status;
    private string statusMessage;

    public string Status { get { return status; } set { status = value; } }
    public string StatusMessage { get { return statusMessage; } set { statusMessage = value; } }

    public ResponseBase()
    {
        Status = JSONStatus.Success.ToString();
        StatusMessage = "";
    }
}

public enum JSONStatus : int
{
    Success = 2,
    Failed = 3
}
