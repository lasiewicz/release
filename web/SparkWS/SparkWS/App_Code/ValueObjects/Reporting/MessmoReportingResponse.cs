﻿using System;
using System.Collections.Generic;
using System.Web;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Summary description for MessmoReportingResponse
/// </summary>
[Serializable]
public class MessmoReportingResponse : VersionedVO
{
    private string m_status;

    public string Status
    {
        get { return m_status; }
        set { m_status = value; }
    }

    public MessmoReportingResponse() : base("0.0.0")
    {
        
    }

    public MessmoReportingResponse(string version, string status)
        : base(version)
    {
        this.Status = status;
    }
}
