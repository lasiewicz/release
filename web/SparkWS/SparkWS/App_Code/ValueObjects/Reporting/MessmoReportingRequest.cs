﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Summary description for MessmoReportingRequest
/// </summary>
[Serializable]
public class MessmoReportingRequest : VersionedVO
{
    #region Private Members
    private string m_channelNumber = string.Empty;
    private string m_event = string.Empty;
    private string m_timestamp = string.Empty;
    private string m_taggle = string.Empty;
    private string m_actionType = string.Empty;
    private string m_smartMessageTag = string.Empty;
    private string m_messageStyle = string.Empty;
    private string m_alertType = string.Empty;
    #endregion

    #region Properties
    public string ChannelNumber
    {
        get { return m_channelNumber; }
        set { m_channelNumber = value; }
    }
    public string Event
    {
        get { return m_event; }
        set { m_event = value; }
    }
    public string TimeStamp
    {
        get { return m_timestamp; }
        set { m_timestamp = value; }
    }
    public string Taggle
    {
        get { return m_taggle; }
        set { m_taggle = value; }
    }
    public string  ActionType
    {
        get { return m_actionType; }
        set { m_actionType = value; }
    }
    public string SmartMessageTag
    {
        get { return m_smartMessageTag; }
        set { m_smartMessageTag = value; }
    }
    public string MessageStyle
    {
        get { return m_messageStyle; }
        set { m_messageStyle = value; }
    }
    public string  AlertType
    {
        get { return m_alertType; }
        set { m_alertType = value; }
    }
    #endregion

    public MessmoReportingRequest() : base("0.0.0")
    {
        
    }

    public MessmoReportingRequest(string version, string aEvent, string timestamp, string taggle, string actiontype,
        string smartmessagetag, string messagestyle, string alerttype) : base (version)
    {
        this.Event = aEvent;
        this.TimeStamp = timestamp;
        this.Taggle = taggle;
        this.ActionType = actiontype;
        this.SmartMessageTag = smartmessagetag;
        this.MessageStyle = messagestyle;
        this.AlertType = alerttype;
    }

    public string GetValuesString()
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("Channel Number: " + m_channelNumber);
        sb.Append(" Event: " + m_event);
        sb.Append(" Timestamp: " + m_timestamp);
        sb.Append(" Taggle: " + m_taggle);
        sb.Append(" ActionType: " + m_actionType);
        sb.Append(" SmartMessageTag: " + m_smartMessageTag);
        sb.Append(" MessageStyle: " + m_messageStyle);
        sb.Append(" AlertType: " + m_alertType);
        
        return sb.ToString();
    }   
}
