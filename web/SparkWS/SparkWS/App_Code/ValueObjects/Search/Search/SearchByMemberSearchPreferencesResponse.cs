﻿using System;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;
using System.Collections.Generic;

[Serializable()]
public class SearchByMemberSearchPreferencesResponse : VersionedVO
{
    #region Private Variables

    private List<MiniProfile> miniProfiles;

    #endregion

    #region Public Properties

    public List<MiniProfile> MiniProfiles
    {
        get
        {
            return miniProfiles;
        }
        set
        {
            miniProfiles = value;
        }
    }

    #endregion

    public SearchByMemberSearchPreferencesResponse()
        : base("0.0.0")
    {
    }

    public SearchByMemberSearchPreferencesResponse(string version, List<MiniProfile> miniProfiles)
        : base(version)
    {
        this.miniProfiles = miniProfiles;
    }
}
