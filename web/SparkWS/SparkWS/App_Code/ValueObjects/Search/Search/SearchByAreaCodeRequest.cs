﻿using System;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;
/// <summary>
/// Instance of this class is required as a request for Search call.
/// </summary>
[Serializable()]
public class SearchByAreaCodeRequest : VersionedVO
{
    #region Private Variables

    private int brandID = Constants.NULL_INT;
    private int pageSize = Constants.NULL_INT;
    private int startRow = Constants.NULL_INT;
    private string[] areaCodes = new string[4];
    private string gender = Constants.NULL_STRING;
    private string seekingGender = Constants.NULL_STRING;
    private int ageFrom = Constants.NULL_INT;
    private int ageTo = Constants.NULL_INT;
    private string country = Constants.NULL_STRING;
    private string orderBy = Constants.NULL_STRING;
    private int hasPhotoFlag = Constants.NULL_INT;

    #endregion

    #region Public Properties

    public int BrandID
    {
        get
        {
            return brandID;
        }
        set
        {
            brandID = value;
        }
    }
    public int PageSize
    {
        get
        {
            return pageSize;
        }
        set
        {
            pageSize = value;
        }
    }
    public int StartRow
    {
        get
        {
            return startRow;
        }
        set
        {
            startRow = value;
        }
    }
    public string[] AreaCodes
    {
        get
        {
            return areaCodes;
        }
        set
        {
            areaCodes = value;
        }
    }
    public string Gender
    {
        get
        {
            return gender;
        }
        set
        {
            gender = value;
        }
    }
    public string SeekingGender
    {
        get
        {
            return seekingGender;
        }
        set
        {
            seekingGender = value;
        }
    }
    public int AgeFrom
    {
        get
        {
            return ageFrom;
        }
        set
        {
            ageFrom = value;
        }
    }
    public int AgeTo
    {
        get
        {
            return ageTo;
        }
        set
        {
            ageTo = value;
        }
    }
    public string Country
    {
        get
        {
            return country;
        }
        set
        {
            country = value;
        }
    }
    public string OrderBy
    {
        get
        {
            return orderBy;
        }
        set
        {
            orderBy = value;
        }
    }
    public int HasPhotoFlag
    {
        get
        {
            return hasPhotoFlag;
        }
        set
        {
            hasPhotoFlag = value;
        }
    }

    #endregion

    #region Constructors

    public SearchByAreaCodeRequest()
        : base("0.0.0")
    {
    }

    /// <summary>
    /// Constructor for SearchByAreaCodeRequest
    /// 
    /// Exceptions will be thrown if any argument range does not meet the requirement.
    /// </summary>
    /// <param name="version">Version</param>
    /// <param name="brandID">BrandID</param>
    /// <param name="pageSize">Max is 50.</param>
    /// <param name="startRow">Minimum value is 1.</param>
    /// <param name="areaCodes">Max is 4.</param>
    /// <param name="gender">"Male", "Female"</param>
    /// <param name="seekingGender">"Male", "Female"</param>
    /// <param name="ageFrom">18 - 99</param>
    /// <param name="ageTo">18 - 99</param>
    /// <param name="country">Country name in English. "USA", "Israel", "Canada", etc...</param>
    /// <param name="orderBy">Optional. "Popularity", "LastLogonDate", "JoinDate", "Proximity". Popularity is default.</param>
    public SearchByAreaCodeRequest(string version, int brandID, int pageSize, int startRow, string[] areaCodes, string gender, string seekingGender, int ageFrom, int ageTo, string country, string orderBy)
        : base(version)
    {
        this.brandID = brandID;
        this.pageSize = pageSize;
        this.startRow = startRow;
        this.areaCodes = areaCodes;
        this.gender = gender;
        this.seekingGender = seekingGender;
        this.ageFrom = ageFrom;
        this.ageTo = ageTo;
        this.country = country;
        this.orderBy = orderBy;
    }

    public SearchByAreaCodeRequest(string version, int brandID, int pageSize, int startRow, string[] areaCodes, string gender, string seekingGender, int ageFrom, int ageTo, string country, string orderBy, int hasPhotoFlag)
        : base(version)
    {
        this.brandID = brandID;
        this.pageSize = pageSize;
        this.startRow = startRow;
        this.areaCodes = areaCodes;
        this.gender = gender;
        this.seekingGender = seekingGender;
        this.ageFrom = ageFrom;
        this.ageTo = ageTo;
        this.country = country;
        this.orderBy = orderBy;
        this.hasPhotoFlag = hasPhotoFlag;
    }

    #endregion
}
