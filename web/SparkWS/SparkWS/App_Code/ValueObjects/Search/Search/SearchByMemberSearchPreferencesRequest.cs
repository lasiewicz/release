﻿using System;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;
/// <summary>
/// Instance of this class is required as a request for Search call.
/// </summary>
[Serializable()]
public class SearchByMemberSearchPreferencesRequest : VersionedVO
{
    #region Private Variables

    private int memberID = Constants.NULL_INT;
    private int brandID = Constants.NULL_INT;
    private int pageSize = Constants.NULL_INT;
    private int startRow = Constants.NULL_INT;
    private string orderBy = Constants.NULL_STRING;

    #endregion

    #region Public Properties

    public int MemberID
    {
        get
        {
            return memberID;
        }
        set
        {
            memberID = value;
        }
    }
    public int BrandID
    {
        get
        {
            return brandID;
        }
        set
        {
            brandID = value;
        }
    }
    public int PageSize
    {
        get
        {
            return pageSize;
        }
        set
        {
            pageSize = value;
        }
    }
    public int StartRow
    {
        get
        {
            return startRow;
        }
        set
        {
            startRow = value;
        }
    }
    public string OrderBy
    {
        get
        {
            return orderBy;
        }
        set
        {
            orderBy = value;
        }
    }

    #endregion

    #region Constructors

    public SearchByMemberSearchPreferencesRequest()
        : base("0.0.0")
    {
    }

    /// <summary>
    /// Constructor for SearchByAreaCodeRequest
    /// </summary>
    /// <param name="version">Version</param>
    /// <param name="memberID">MemberID.</param>
    /// <param name="brandID">BrandID</param>
    /// <param name="pageSize">Max is 50.</param>
    /// <param name="startRow">Minimum value is 1.</param>
    /// <param name="orderBy">Optional. "Popularity", "LastLogonDate", "JoinDate", "Proximity". Popularity is default.</param>
    public SearchByMemberSearchPreferencesRequest(string version, int memberID, int brandID, int pageSize, int startRow, string orderBy)
        : base(version)
    {
        this.memberID = MemberID;
        this.brandID = brandID;
        this.pageSize = pageSize;
        this.startRow = startRow;
        this.orderBy = orderBy;
    }

    #endregion
}
