﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Reprsents a request to add to a list
/// such as hotlist, viewed you, emailed you and so forth
/// </summary>
[Serializable()]
public class AddListMemberRequest:VersionedVO
{
    #region "Private variables"
    /// <summary>
    /// the category ID which will be added
    /// </summary>
    private int hotListCategoryID = int.MinValue;


    /// <summary>
    /// the community ID in which list it should be added
    /// </summary>
    private int communityID = int.MinValue;


    /// <summary>
    /// the site ID for which list it should be added
    /// </summary>
    private int siteID = int.MinValue;


    /// <summary>
    /// the memberID which should be added to
    /// </summary>
    private int memberID = int.MinValue;

    /// <summary>
    /// the member to add to the list
    /// </summary>
    private int targetMemberID = int.MinValue;


    /// <summary>
    /// any comments for this addition
    /// </summary>
    private string comment = string.Empty;

    private bool reciprocal = false;

    #endregion

    #region Constructors
    public AddListMemberRequest():
        base("0.0.0")
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary>
    /// inits a new add list member request
    /// </summary>
    /// <param name="hotListCategoryID">the ID of the list to add to</param>
    /// <param name="communityID">the communityID of the list</param>
    /// <param name="siteID">the site ID of the list</param>
    /// <param name="memberID">the memberID that is doing the add</param>
    /// <param name="targetMemberID">the target member ID to add to the list</param>
    /// <param name="comment">any comments that are required</param>
    public AddListMemberRequest(string version, int hotListCategoryID, int communityID, int siteID, int memberID, int targetMemberID, bool reciprocal, string comment):
        base(version)
    {
        this.hotListCategoryID = hotListCategoryID;
        this.communityID = communityID;
        this.siteID = siteID;
        this.memberID = memberID;
        this.targetMemberID = targetMemberID;
        this.comment = comment;
        this.reciprocal = reciprocal;
    }
    #endregion

    #region Public Properties


    /// <summary>
    /// get/set the hot list category ID for this request
    /// </summary>
    public int HotListCategoryID
    {
        get { return hotListCategoryID; }
        set { hotListCategoryID = value; }
    }

    /// <summary>
    /// get/set the community ID under which this list exists
    /// </summary>
    public int CommunityID
    {
        get { return communityID; }
        set { communityID = value; }
    }

    /// <summary>
    /// get/set the site ID under which the list belongs
    /// </summary>
    public int SiteID
    {
        get { return siteID; }
        set { siteID = value; }
    }

    /// <summary>
    /// get/set the ID of the member that will have additions to the list
    /// </summary>
    public int MemberID
    {
        get { return memberID; }
        set { memberID = value; }
    }

    /// <summary>
    /// get/set the ID of the member that is being added to the list
    /// </summary>
    public int TargetMemberID
    {
        get { return targetMemberID; }
        set { targetMemberID = value; }
    }

    /// <summary>
    /// whether it will go both ways
    /// </summary>
    public bool Reciprocal
    {
        get { return reciprocal; }
        set { reciprocal = value; }
    }

    /// <summary>
    /// the comment for this attribute
    /// </summary>
    public string Comment
    {
        get { return comment; }
        set { comment = value; }
    }

   


    #endregion
}
