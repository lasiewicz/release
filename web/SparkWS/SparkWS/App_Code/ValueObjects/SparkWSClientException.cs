using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;

/// <summary>
/// SparkWSClientException is the exception that should be thrown for exceptions which represent client side errors, 
/// such as bad data passed to the API etc.
/// </summary>
public class SparkWSClientException : Exception
{
   // Summary:
   //     Initializes a new instance of the System.Exception class.
   public SparkWSClientException() : base() { }
   //
   // Summary:
   //     Initializes a new instance of the System.Exception class with a specified
   //     error message.
   //
   // Parameters:
   //   message:
   //     The message that describes the error.
   public SparkWSClientException(string message) : base(message) { }
   //
   // Summary:
   //     Initializes a new instance of the System.Exception class with serialized
   //     data.
   //
   // Parameters:
   //   context:
   //     The System.Runtime.Serialization.StreamingContext that contains contextual
   //     information about the source or destination.
   //
   //   info:
   //     The System.Runtime.Serialization.SerializationInfo that holds the serialized
   //     object data about the exception being thrown.
   //
   // Exceptions:
   //   System.Runtime.Serialization.SerializationException:
   //     The class name is null or System.Exception.HResult is zero (0).
   //
   //   System.ArgumentNullException:
   //     The info parameter is null.
   protected SparkWSClientException(SerializationInfo info, StreamingContext context) : base(info, context) { }
   //
   // Summary:
   //     Initializes a new instance of the System.Exception class with a specified
   //     error message and a reference to the inner exception that is the cause of
   //     this exception.
   //
   // Parameters:
   //   message:
   //     The error message that explains the reason for the exception.
   //
   //   innerException:
   //     The exception that is the cause of the current exception, or a null reference
   //     (Nothing in Visual Basic) if no inner exception is specified.
   public SparkWSClientException(string message, Exception innerException) : base(message, innerException) { }


}
