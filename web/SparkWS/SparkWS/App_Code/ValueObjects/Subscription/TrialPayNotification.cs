using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Summary description for TrialPayNotification
/// </summary>
public class TrialPayNotification: VersionedVO 
{
    public Int32 MemberID;
    public Int32 SiteID;

    public TrialPayNotification(): base("0.0.1")
    {
        
    }
    public TrialPayNotification(string version): base(version)
    {

    }

    public TrialPayNotification(string version,Int32 memberid, Int32 siteid )
        : this(version)
    {
        this.MemberID = memberid;
        this.SiteID = siteid;

    }
    public TrialPayNotification(Int32 memberid, Int32 siteid)
        : this("0.0.1")
    {
        this.MemberID = memberid;
        this.SiteID = siteid;

    }
    
}
