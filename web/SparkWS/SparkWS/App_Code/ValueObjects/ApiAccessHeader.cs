using System;
using System.Web.Services.Protocols;
using System.Security;
using System.Security.Principal;
using System.Threading;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Summary description for ApiAccessHeader
/// </summary>

public class ApiAccessHeader : SoapHeader
{
   public static string TICKET_KEY = "TicketKey";
   private string _Key = AccessTicket.DEFAULT_TICKET;
   public string Key
   {
	  get { 
		 return _Key; 
	  }
	  set
	  {
		 _Key = (value == string.Empty) ? AccessTicket.DEFAULT_TICKET : value;
		 Thread.SetData(Thread.GetNamedDataSlot(TICKET_KEY), _Key);
	  }
   }
}
