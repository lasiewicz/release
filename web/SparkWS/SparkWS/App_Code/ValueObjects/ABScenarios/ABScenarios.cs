﻿using System;
using System.Data;
using System.Configuration;

using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.SparkWS;
using System.Xml.Serialization;
using Matchnet.Configuration.ServiceAdapters.Analitics;
/// <summary>
/// Summary description for ABScenarios
/// </summary>

[Serializable()]
public class ABScenarioRequest 
{
    int _memberID;
    int _communityID;
    int _siteID;
    List<string> _scenarios;

    public int MemberID
    {
        get { return _memberID; }
        set { _memberID = value; }

    }

    public int CommunityID
    {
        get { return _communityID; }
        set { _communityID = value; }

    }

    public int SiteID
    {
        get { return _siteID; }
        set { _siteID = value; }

    }

[XmlArray("Scenarios"),XmlArrayItem("Name")]
    public List<string> Scenarios
    {
        get { return _scenarios; }
        set { _scenarios = value; }

    }

    public ABScenarioRequest() { }
    public ABScenarioRequest(int communityid, int siteid, int memberid)
    {
        _memberID = _memberID;
        _communityID = communityid;
        _siteID = siteid;
     }

    public void AddScenario(string s)
    {
        if (_scenarios == null && s != null)
            _scenarios = new List<string>();

        if (s != null)
            _scenarios.Add(s);

    }
}


[Serializable()]
public class ABScenarioResult
{
    int _memberID;
    int _communityID;
    int _siteID;
    List<ABScenario> _scenarios;

    public int MemberID
    {
        get { return _memberID; }
        set { _memberID = value; }

    }

    public int CommunityID
    {
        get { return _communityID; }
        set { _communityID = value; }

    }

    public int SiteID
    {
        get { return _siteID; }
        set { _siteID = value; }

    }

    public List<ABScenario> Scenarios
    {
        get { return _scenarios; }
        set { _scenarios = value; }

    }

    public ABScenarioResult() { }
    public ABScenarioResult(int communityid, int siteid, int memberid)
    {
        _memberID = memberid;
        _communityID = communityid;
        _siteID = siteid;
    }

    public void AddScenario(ABScenario s)
    {
        if (_scenarios == null && s != null)
            _scenarios = new List<ABScenario>();

        if (s != null)
            _scenarios.Add(s);

    }
}
[Serializable()]
[XmlInclude(typeof(Scenario))]
public class ABScenario 
{
    string _name;

    Scenario _abScenario = Scenario.A;
    public string Name
    {
        get { return _name; }
        set { _name = value; }

    }

    public Scenario Scenario
    {
        get { return _abScenario; }
        set { _abScenario = value; }

    }
    public ABScenario() { }

    public ABScenario(string name, Scenario abScenario)
    {
        _name = name;
        _abScenario = abScenario;
    }


}
