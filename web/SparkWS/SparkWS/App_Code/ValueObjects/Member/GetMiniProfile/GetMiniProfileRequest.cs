using System;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;
/// <summary>
/// Instance of this class is required as a request for GetMiniProfile call.
/// </summary>
[Serializable()]
public class GetMiniProfileRequest : VersionedVO
{
    #region Private Variables

    private int memberID = Constants.NULL_INT;
    private int brandID = Constants.NULL_INT;
    private string encryptedMemberID = Constants.NULL_STRING;

    #endregion

    #region Public Properties

    public int MemberID
    {
        get
        {
            return memberID;
        }
        set
        {
            memberID = value;
        }
    }
    public int BrandID
    {
        get
        {
            return brandID;
        }
        set
        {
            brandID = value;
        }
    }
    public string EncryptedMemberID
    {
        get
        {
            return encryptedMemberID;
        }
        set
        {
            encryptedMemberID = value;
        }
    }
    #endregion

    #region Constructors

    public GetMiniProfileRequest()
        : base("0.0.0")
    {
    }

    /// <summary>
    /// Constructor for MiniProfile
    /// </summary>
    /// <param name="version"></param>
    /// <param name="memberID">Member's ID</param>
    /// <param name="brandID">Brand ID</param>
    public GetMiniProfileRequest(string version, int memberID, int brandID)
        : base(version)
    {
        this.memberID = memberID;
        this.brandID = brandID;
    }

    #endregion
}
