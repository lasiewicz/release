using System;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;

/// <summary>
/// Instance of this class is required for making GetMiniProfile method call.
/// </summary>
[Serializable()]
public class GetMiniProfileResponse : VersionedVO
{
    #region Private Variables

    private MiniProfile miniProfile;

    #endregion

    #region Public Properties

    public MiniProfile MiniProfile
    {
        get
        {
            return miniProfile;
        }
        set
        {
            miniProfile = value;
        }
    }

    #endregion

    public GetMiniProfileResponse()
        : base("0.0.0")
    {
    }

    public GetMiniProfileResponse(string version, MiniProfile miniProfile)
        : base(version)
    {
        this.miniProfile = miniProfile;
    }
}
