using System;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;

/// <summary>
/// Instance of this class is returned as a result of GetMiniProfile call.
/// </summary>
[Serializable()]
public class MiniProfile : VersionedVO
{
    #region Private Variables

    private string userName = Constants.NULL_STRING;
    private int age = Constants.NULL_INT;
    private string gender = Constants.NULL_STRING;
    private string seekingGender = Constants.NULL_STRING;
    private string thumbPhotoURL = Constants.NULL_STRING;
    private string city = Constants.NULL_STRING;
    private string state = Constants.NULL_STRING;
    private string country = Constants.NULL_STRING;
    private string lastLogonDateTime = Constants.NULL_STRING;
    private int ageSeekingFrom = Constants.NULL_INT;
    private int ageSeekingTo = Constants.NULL_INT;
    private bool isSubscriber = false;
    private string phoneNumber = Constants.NULL_STRING;
    private string email = Constants.NULL_STRING;
    private DateTime dateOfBirth = DateTime.MinValue;
    private string fullThumbPhotoURL = Constants.NULL_STRING;

    #endregion

    #region Public Properties

    public string UserName
    {
        get
        {
            return userName;
        }
        set
        {
            userName = value;
        }
    }
    public int Age
    {
        get
        {
            return age;
        }
        set
        {
            age = value;
        }
    }
    public string Gender
    {
        get
        {
            return gender;
        }
        set
        {
            gender = value;
        }
    }
    public string SeekingGender
    {
        get
        {
            return seekingGender;
        }
        set
        {
            seekingGender = value;
        }
    }
    public string ThumbPhotoURL
    {
        get
        {
            return thumbPhotoURL;
        }
        set
        {
            thumbPhotoURL = value;
        }
    }

    public string FullThumbPhotoURL
    {
        get
        {
            return fullThumbPhotoURL;
        }
        set
        {
            fullThumbPhotoURL = value;
        }
    }
    
    public string City
    {
        get
        {
            return city;
        }
        set
        {
            city = value;
        }
    }
    public string State
    {
        get
        {
            return state;
        }
        set
        {
            state = value;
        }
    }
    public string Country
    {
        get
        {
            return country;
        }
        set
        {
            country = value;
        }
    }
    public string LastLogonDateTime
    {
        get
        {
            return lastLogonDateTime;
        }
        set
        {
            lastLogonDateTime = value;
        }
    }
    public int AgeSeekingFrom
    {
        get
        {
            return ageSeekingFrom;
        }
        set
        {
            ageSeekingFrom = value;
        }
    }
    public int AgeSeekingTo
    {
        get
        {
            return ageSeekingTo;
        }
        set
        {
            ageSeekingTo = value;
        }
    }
    public bool IsSubscriber
    {
        get
        {
            return isSubscriber;
        }
        set
        {
            isSubscriber = value;
        }
    }
    public string PhoneNumber
    {
        get
        {
            return phoneNumber;
        }
        set
        {
            phoneNumber = value;
        }
    }
    public string Email
    {
        get
        {
            return email;
        }
        set
        {
            email = value;
        }
    }
    public DateTime DateOfBirth
    {
        get
        {
            return dateOfBirth;
        }
        set
        {
            dateOfBirth = value;
        }
    }


    #endregion

    #region Constructors

    public MiniProfile()
        : base("0.0.0")
    {
    }

    public MiniProfile(string version, string userName, int age, string gender, string seekingGender, string thumbPhotoURL, string city, string state, string country, string lastLogonDateTime, int ageSeekingFrom, int ageSeekingTo, bool isSubscriber, string phoneNumber, string email, DateTime dateOfBirth)
        : base(version)
    {
        this.userName = userName;
        this.age = age;
        this.gender = gender;
        this.seekingGender = seekingGender;
        this.thumbPhotoURL = thumbPhotoURL;
        this.city = city;
        this.state = state;
        this.country = country;
        this.lastLogonDateTime = lastLogonDateTime;
        this.ageSeekingFrom = ageSeekingFrom;
        this.ageSeekingTo = ageSeekingTo;
        this.isSubscriber = isSubscriber;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
    }

    public MiniProfile(string version, string userName, int age, string gender, string seekingGender, string thumbPhotoURL,
        string city, string state, string country, string lastLogonDateTime, int ageSeekingFrom, int ageSeekingTo,
        bool isSubscriber, string phoneNumber, string email, DateTime dateOfBirth, string fullThumbPhotoURL)
        : this(version, userName, age, gender, seekingGender, thumbPhotoURL, city, state, country, lastLogonDateTime, 
            ageSeekingFrom, ageSeekingTo, isSubscriber, phoneNumber, email, dateOfBirth)
    {
        this.fullThumbPhotoURL = fullThumbPhotoURL;
    }

    #endregion
}
