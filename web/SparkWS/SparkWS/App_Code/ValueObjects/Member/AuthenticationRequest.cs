using System;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Encapsulates a request for authentication of a member
/// </summary>
public class AuthenticationRequest: VersionedVO 
{
   /// <summary>
   /// The email address of the person to use for authentication
   /// </summary>
   public string EmailAddress;
   /// <summary>
   /// The password to use for authentication
   /// </summary>
   public string Password;
   /// <summary>
   /// Brand the member belongs to
   /// </summary>
   public int BrandId;

    /// <summary>
    /// The priviligies that we want to see if this user
    /// has
    /// </summary>
    public int[] Privileges;

   internal AuthenticationRequest() : base("0.0.0")
   {
   }

   public AuthenticationRequest(string version)
	  : base(version)
   {
   }

   public AuthenticationRequest(string version, string emailAddress, string password) : this(version) {
	  EmailAddress = emailAddress;
	  Password = password;
   }

    public AuthenticationRequest(string version, string emailAddress, string password, int[] privileges)
        :this(version, emailAddress, password)
    {
        this.Privileges = privileges;
    }

    public AuthenticationRequest(string version, string emailAddress, string password, int brandId)
        : this(version)
    {
        EmailAddress = emailAddress;
        Password = password;
        BrandId = brandId;
    }

    public AuthenticationRequest(string version, string emailAddress, string password, int brandId, int[] privileges)
        : this(version, emailAddress, password, brandId)
    {
        this.Privileges = privileges;
    }
}
