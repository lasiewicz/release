﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Summary description for MemberIDResponse
/// </summary>
public class MemberIDResponse:VersionedVO
{
    /// <summary>
    /// the memberID
    /// </summary>
    public int MemberID = 0;

    internal MemberIDResponse()
        : base("0.0.0")
    { }

    public MemberIDResponse(string version)
        : base(version)
    { }

    public MemberIDResponse(string version, int memberID)
        :this(version)
    {
        MemberID = memberID;
    }
}
