using System;

/// <summary>
/// Encapsulates the parameters required for session expiration at Mingle
/// </summary>
public class InvalidateSessionArgs
{
   public int SiteID;
   public string SessionID;

   public InvalidateSessionArgs(string sessionID, int siteID)
   {
	  this.SessionID = sessionID;
	  this.SiteID = siteID;
   }

   public override string ToString()
   {
	  return SessionID + ":" + SiteID.ToString();
   }
}
