﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Summary description for MemberIDRequest
/// </summary>
public class MemberIDRequest:VersionedVO
{
    /// <summary>
    /// the username which we need to find the memberID of
    /// </summary>
    public string Username = string.Empty;

    /// <summary>
    /// the emails which we need to find the memberID of
    /// </summary>
    public string Email = string.Empty;

    /// <summary>
    /// Optional field for getting username from the right community
    /// </summary>
    public int CommunityID = int.MinValue;

    internal MemberIDRequest()
        : base("0.0.0")
    { }

    public MemberIDRequest(string version)
        : base(version)
    { }

    /// <summary>
    /// inits a new memberID request
    /// </summary>
    /// <param name="userName">the username by which to look up</param>
    public MemberIDRequest(string version, string userName):this(version)
    {
        Username = userName;
    }

    public MemberIDRequest(string version, string userName, int communityID)
        : this(version)
    {
        Username = userName;
        CommunityID = communityID;
    }
}
