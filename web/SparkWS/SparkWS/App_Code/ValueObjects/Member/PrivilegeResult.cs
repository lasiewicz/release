﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// represents a result for whether an user have certain privileges.
/// </summary>
[Serializable()]
public class PrivilegeResult
{
    /// <summary>
    /// the privilege ID
    /// </summary>
    public int ID;

    /// <summary>
    /// whether the there is a privilege for this ID
    /// </summary>
    public bool HasPrivilige;

    /// <summary>
    /// serialization needed
    /// </summary>
    public PrivilegeResult()
    { }

    /// <summary>
    /// short declaration
    /// </summary>
    /// <param name="id">the ID of the privilege we are recording for</param>
    /// <param name="hasPrivilge">true/false whether the privilege exists for the user</param>
    public PrivilegeResult(int id, bool hasPrivilge)
    {
        this.ID = id;
        this.HasPrivilige = hasPrivilge;
    }
}
