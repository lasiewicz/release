﻿using System;
using System.Collections.Generic;
using System.Web;

using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Summary description for DemographicDataRequest
/// </summary>
public class DemographicDataRequest : VersionedVO
{
    #region Private Variables

    private int _memberID;
    private string _memberSessionID;
    private int _profileSectionID;
    private int _communityID;
    private int _siteID;
    private int _brandID;

    #endregion

    #region Public Properties

    public int MemberID
    {
        get { return this._memberID; }
        set { this._memberID = value; }
    }

    public string MemberSessionID
    {
        get { return this._memberSessionID; }
        set { this._memberSessionID = value; }
    }

    public int ProfileSectionID
    {
        get { return this._profileSectionID; }
        set { this._profileSectionID = value; }
    }

    public int CommunityID
    {
        get { return this._communityID; }
        set { this._communityID = value; }
    }

    public int SiteID
    {
        get { return this._siteID; }
        set { this._siteID = value; }
    }

    public int BrandID
    {
        get { return this._brandID; }
        set { this._brandID = value; }
    }

    #endregion

    #region Constructors

    /// <summary>
    /// For serialization.
    /// </summary>
    public DemographicDataRequest()
        : base("0.0.0")
    {
    }

    public DemographicDataRequest(string version, int memberID, string memberSessionID, int profileSectionID, int communityID, int siteID, int brandID)
        : base(version)
    {
        this._memberID = memberID;
        this._memberSessionID = memberSessionID;
        this._profileSectionID = profileSectionID;
        this._communityID = communityID;
        this._siteID = siteID;
        this._brandID = brandID;
    }

    #endregion
}
