﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml.Serialization;

using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Summary description for DemographicData
/// </summary>
[Serializable()]
public class DemographicData : VersionedVO
{
    /// <summary>
    /// The ID of this instance.
    /// </summary>
    /// <remarks>The ID of a profile section is associated with a unique consumer. 
    /// If 2 consumers are interested in the same section, a definition needs to 
    /// be cloned, assigned a new ID and assigned to the new consumer.</remarks>
    public int ProfileSectionID;

    /// <summary>
    /// The member's ID
    /// </summary>
    public int MemberID;

    /// <summary>
    /// The collection of values for this section.
    /// </summary>
    [XmlIgnore]
    public MemberAttributes Profile;
    //private MemberAttributes _profile;

    /// <summary>
    /// Gender
    /// </summary>
    public string Gender;

    /// <summary>
    /// Age
    /// </summary>
    public int Age;

    /// <summary>
    /// Location
    /// </summary>
    public string Location;

    /// <summary>
    /// Ethnicity
    /// </summary>
    public string Ethnicity;

    /// <summary>
    /// Subscriber status
    /// </summary>
    public string SubscriberStatus;

    /// <summary>
    /// Profile percentage completion
    /// </summary>
    public string ProfileCompletionPercentage;

    //public MemberAttributes Profile
    //{
    //    get
    //    {
    //        return this._profile;
    //    }
    //    set
    //    {
    //        this._profile = value;
    //    }
    //}

    /// <summary>
    /// Constructor
    /// </summary>
    public DemographicData()
        : base("1.0.0")
    {

    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="capacity">The number of expected items in the section. 
    /// Use this constructor as much as possible.</param>
    public DemographicData(int capacity)
        : this()
    {
        Profile = new MemberAttributes(capacity);
    }
}
