using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;

using Matchnet.Configuration.ValueObjects.SparkWS;
using System.Xml;


/// <summary>
/// Encapsulates a list of profile sections and a list of exceptions corresponding to the retreival of the individual profiles in the profile sections list.
/// </summary>
[Serializable()]
[XmlInclude(typeof(ProfileSection)), 
XmlInclude(typeof(ProfileSection[])),
XmlInclude(typeof(string[]))
]
public class ProfileSectionsResult : VersionedVO
{
   /// <summary>
   /// An ordered list where each element is in the request position and contains the profile section defined.
   /// </summary>
   public ProfileSection[] ProfileSections;
   /// <summary>
   /// An ordered list of error strings, if any exist.
   /// </summary>
   public string[] Errors;

   public ProfileSectionsResult() : base("1.0.0") { }
   public ProfileSectionsResult(int capacity)
	  : base("1.0.0")
   {
	  ProfileSections = new ProfileSection[capacity];
	  Errors = new string[capacity];
   }
}
