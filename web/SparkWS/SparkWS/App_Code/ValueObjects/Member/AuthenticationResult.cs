using System;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.SparkWS;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

/// <summary>
/// Summary description for AuthenticationResult
/// </summary>
[Serializable()]
public class AuthenticationResult: VersionedVO
{
    /// <summary>
    /// whether the user is authenticated
    /// </summary>
   public bool IsAuthenticated;

    /// <summary>
    /// the member ID of the user
    /// </summary>
   public int MemberID;

    /// <summary>
    /// priviliges information
    /// </summary>
    public PrivilegeResult[] Privileges;

   public AuthenticationResult(): base("1.0.0")   {
   }

   public AuthenticationResult(int memberID, bool isAuthenticated) : base("1.0.0") {
	  IsAuthenticated = isAuthenticated;
	  MemberID = memberID;
   }

    public AuthenticationResult(int memberID, bool isAuthenticated, PrivilegeResult[] privileges)
        : this(memberID, isAuthenticated)
    {
        this.Privileges = privileges;
    }

   public override string ToString()
   {
	  return "MemberID: " + MemberID.ToString() + " Auth = " + IsAuthenticated.ToString();
   }
}
