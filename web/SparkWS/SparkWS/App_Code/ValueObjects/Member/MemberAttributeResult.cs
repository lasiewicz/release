using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.Collections;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Content.ValueObjects.BrandConfig;
using System.Xml;

 public enum ReturnCodes
    {
         retSuccess=0,
         retValidation=100,
         retValidationAccess,
         retValidationMember,
         retSystemError=200

    }


/// <summary>
/// Encapsulates a list of profile sections and a list of exceptions corresponding to the retreival of the individual profiles in the profile sections list.
/// </summary>
[Serializable()]
[XmlInclude(typeof(ProfileAttribute)),
XmlInclude(typeof(ProfileAttribute[])),
XmlInclude(typeof(string[]))
]
public class MemberAttributeResult
{
    
    public const int  ATTRID_EADDRESS=12;
    public const int  ATTRID_BDATE = 70;
    public const int ATTRID_REGIONID = 122;
    public const int ATTRID_GENDERMASK = 69;
    public const int ATTRID_USERNAME = 302;
    public const int ATTRID_EDUCATION = 89;
    public const int ATTRID_SMOKING = 92;

    public const int GENDER_MALE = 1;
    public const int GENDER_FEMALE = 2;

    public const int GENDER_SEEK_MALE = 4;
    public const int GENDER_SEEK_FEMALE = 8;

    public const int SUB_STATUS_NOVALUE = 0;
    public const int SUB_STATUS_SUBSCRIBER = 1;
    public const int SUB_STATUS_REGISTERED = 2;
    int memberid;
    int siteid;
    int communityid;
    ReturnCodes returncode=ReturnCodes.retSuccess;
    string returndesc="Success";
    ArrayList attributes;

       public MemberAttributeResult()
    {
      
      }
    #region properties
   
    public Int32 MemberID
    {
        get { return memberid; }
        set { memberid = value; }
    }

    public Int32 SiteID
    {
        get { return siteid; }
        set {siteid = value; }
         }

    public Int32 CommunityID
    {
        get { return communityid; }
        set { communityid = value; }
    }

    public  ArrayList Attributes
    {
        get { return attributes; }
    }

    public ReturnCodes ReturnCode
    {
        get { return returncode; }
        set {returncode=value; }
    }

    public string ReturnDescription
    {
        get { return returndesc; }
        set { returndesc = value; }
    }

 #endregion

    #region public methods
    public bool Init(int siteID, int memberID)
    {
        bool res = true;
        try
        {
            memberid = memberID;
            siteid = siteID;

            if (siteid <= 0)
                return false;

            if (memberid <= 0)
                return false;

            Matchnet.Content.ValueObjects.BrandConfig.Site s = ServiceUtil.GetSite(siteid);
            if (s != null)
            { communityid = s.Community.CommunityID; }
            else { return false; }

            return res;
        }
        catch (Exception ex)
        { throw (ex); }

    }
    public void AddAttribute( string name, string val)
    {
        if (attributes == null)
        { attributes = new ArrayList(); }
        if(!String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(val))
            attributes.Add(new ProfileAttribute( name.ToLower(), val));
    }

  
    public string ToXMLString()
    {
        string ret="";
        try
        {
             if (returncode == 0)
            {
                ret = "<Member>";
                ret += getRequiredNodesString(returncode, returndesc);
                if (attributes != null)
                {
                    for (int i = 0; i <= attributes.Count - 1; i++)
                    {
                        ProfileAttribute a = (ProfileAttribute)attributes[i];
                        ret += getNodeString(a.name, a.val);
                    }
                }
                ret += "</Member>";
            }
            else
            {
               ret= handleError(returncode, returndesc);
            }
            return ret;
        }
        catch (Exception ex)
        {
            return handleError(ReturnCodes.retSystemError, ex.Message);
        }

    }

    public string ToXMLString(ReturnCodes code, string desc)
    {
        returncode = code;
        returndesc = desc;
        return ToXMLString();
    }
    #endregion

    #region helper methods

      
    private string handleError(ReturnCodes code, string desc)
    {
        string description=desc;
        if (String.IsNullOrEmpty(description))
        {
            switch (code)
            {
                case ReturnCodes.retValidation:
                    {   description = "Validation Error";break;  }
                case ReturnCodes.retValidationAccess:
                    {   description = "Invalid accesskey"; break; }
                case ReturnCodes.retValidationMember:
                    {   description = "Member/SiteID not found";  break;  }
                case ReturnCodes.retSystemError:
                    {   description = "System Error"; break; }
                default:
                    {   code = ReturnCodes.retSystemError;  description = "Unknown error";  break;  }
            }
        }
        return "<Member>" + getRequiredNodesString(code, description) + "</Member>";
    }
    
    private string getRequiredNodesString( ReturnCodes code, string desc)
    {
        string ret = "";
        returncode = code;
        if (!String.IsNullOrEmpty(desc))
            returndesc = desc.Trim();
        else
            returndesc = "";

        ret += getNodeString("returncode", returncode);
        ret += getNodeString("returndescription", returndesc);
        ret += getNodeString("siteid", siteid);
        ret += getNodeString("memberid", memberid);


        return ret;
    }
    
    private string getNodeString(string nodeName, int value)
    {string ret = "";
    try
    {
        string val = value.ToString();
        ret = getNodeString(nodeName, val);
        return ret;
    }
    catch (Exception ex)
    { return ret; }
      

    }
    
    private string getNodeString(string nodeName, ReturnCodes value)
    {
        string ret = "";
        try
        {
            int val = 0;
            val = Convert.ToInt32(value);
           ret = getNodeString(nodeName, val);
            return ret;
        }
        catch (Exception ex)
        { return ret; }
    }
    
    private string getNodeString(string nodeName, string value)
    {
        string ret="";
        if (!String.IsNullOrEmpty(value) && !String.IsNullOrEmpty(nodeName))
            ret = "<" + nodeName.Trim() + ">" + value.Trim() + "</" + nodeName.Trim() + ">";

        return ret;

    }

    #endregion

}

public class ProfileAttribute
{
    public string name;
    public string val;

    public ProfileAttribute()
    { }
    public ProfileAttribute( string attrName, string attrValue)
    {
     name = attrName;
      val = attrValue;
    }
}
