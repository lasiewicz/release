using System;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.SparkWS;

/// <summary>
/// Encapsulates a request for the number of members online.
/// </summary>
public class MembersOnlineCountRequest: VersionedVO 
{
   /// <summary>
   /// The Community ID for which to get the count of current members online.
   /// </summary>
   public int CommunityID;

   /// <summary>
   /// Constructor
   /// </summary>
   internal MembersOnlineCountRequest()
	  : base("0.0.0")
   {
   }

   /// <summary>
   /// Constructor
   /// </summary>
   /// <param name="version">current supported version is "1.0.0"</param>
   public MembersOnlineCountRequest(string version)
	  : base(version)
   {
   }

   /// <summary>
   /// Constructor
   /// </summary>
   /// <param name="version">current supported version is "1.0.0"</param>
   /// <param name="communityID">The Community ID for which to get the count of current members online.</param>
   public MembersOnlineCountRequest(string version, int communityID) : this(version) {
	  CommunityID = communityID;
   }
}

