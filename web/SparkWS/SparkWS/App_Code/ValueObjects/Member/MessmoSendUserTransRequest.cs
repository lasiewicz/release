﻿using System;
using System.Collections.Generic;
using System.Web;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet;
using System.Text;

/// <summary>
/// Summary description for MessmoSendUserTransRequest
/// </summary>
/// 
[Serializable]
public class MessmoSendUserTransRequest : VersionedVO
{
    #region Private Members
    private string _command = Constants.NULL_STRING;
    private string _messmoKey = Constants.NULL_STRING;
    private string _channelNumber = Constants.NULL_STRING;
    private string _timestamp = Constants.NULL_STRING;
    private string _subscriberId = Constants.NULL_STRING;
    private string _status = Constants.NULL_STRING;
    private string _getDate = Constants.NULL_STRING;
    private string _apiVersion = Constants.NULL_STRING;
    #endregion

    #region Properties
    public string Command
    {
        get { return _command; }
        set { _command = value; }
    }
    public string MessmoKey
    {
        get { return _messmoKey; }
        set { _messmoKey = value; }
    }
    public string ChannelNumber
    {
        get { return _channelNumber; }
        set { _channelNumber = value; }
    }
    public string TimeStamp
    {
        get { return _timestamp; }
        set { _timestamp = value; }
    }
    public string SubscriberId
    {
        get { return _subscriberId; }
        set { _subscriberId = value; }
    }
    public string Status
    {
        get { return _status; }
        set { _status = value; }
    }
    public string GetDate
    {
        get { return _getDate; }
        set { _getDate = value; }
    }
    public string ApiVersion
    {
        get { return _apiVersion; }
        set { _apiVersion = value; }
    }
    #endregion

    public MessmoSendUserTransRequest()
        : base("0.0.0")
    {
        
    }


    public MessmoSendUserTransRequest(string version, string command, string messmoKey, string channelNumber, string timestamp,
        string subscriberId, string status, string getDate, string apiVersion) : base(version)
    {
        _command = command;
        _messmoKey = messmoKey;
        _channelNumber = channelNumber;
        _timestamp = timestamp;
        _subscriberId = subscriberId;
        _status = status;
        _getDate = getDate;
        _apiVersion = apiVersion;
    }

    public string GetValuesString()
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("Command: " + _command);
        sb.Append(" MessmoKey: " + _messmoKey);
        sb.Append(" ChannelNumber: " + _channelNumber);
        sb.Append(" Timestamp: " + _timestamp);
        sb.Append(" SubscriberID: " + _subscriberId);
        sb.Append(" Status: " + _status);
        sb.Append(" SetDate: " + _getDate);
        sb.Append(" APIVersion: " + _apiVersion);


        return sb.ToString();

    }
}
