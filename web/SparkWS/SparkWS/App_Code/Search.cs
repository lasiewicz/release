﻿using System;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

using System.Security.Permissions;
using Matchnet.Configuration.ValueObjects.SparkWS;
using System.Reflection;

[WebService(Namespace = "http://Search.SparkWS.com/", Description = "This service offers various types of search on Spark BH sites.")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Search : WebServiceBase {

    public Search() 
    {
    }

    [WebMethod(Description = "Performs search based on stored member search preferences. All parameters inside the request object are required.")]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate | ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Subsidiary))]
    public SearchByMemberSearchPreferencesResponse SearchByMemberSearchPreferences(SearchByMemberSearchPreferencesRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 600);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            SearchByMemberSearchPreferencesResponse result = SearchBL.SearchByMemberSearchPreferences(request, key);

            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.Search.SearchByMemberSearchPreferences request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.Search.SearchByMemberSearchPreferences unable to search by member search preferences.", SoapException.ServerFaultCode, ex);
        }
    }

    [WebMethod(Description = "Performs visitor area code search. All parameters inside the request object are required.")]
    [SoapHeader("accessHeader")]
    [ApiPermission(SecurityAction.Demand, APIAccessGroup = (int)(ApiAccessRoll.Affiliate | ApiAccessRoll.Developer | ApiAccessRoll.Internal | ApiAccessRoll.Subsidiary))]
    public SearchByAreaCodeResponse SearchByAreaCode(SearchByAreaCodeRequest request)
    {
        ServiceUtil.ValidateVersion(MethodInfo.GetCurrentMethod(), "1.0.0", request);
        ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), accessHeader.Key, 600);

        string key = (accessHeader != null) ? accessHeader.Key : "FAKE";

        try
        {
            SearchByAreaCodeResponse result = SearchBL.SearchByAreaCode(request, key);

            return result;
        }
        catch (SparkWSClientException ex)
        {
            throw new SoapException("SparkWS.Search.SearchByAreaCode request error.", SoapException.ClientFaultCode, ex);
        }
        catch (Exception ex)
        {
            throw new SoapException("SparkWS.Search.SearchByAreaCode unable to search by areacode(s).", SoapException.ServerFaultCode, ex);
        }
    }
}

