using System;
using System.Drawing;
using System.Web;
using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

public partial class CacheManager : System.Web.UI.Page
{
   
   protected void Page_Load(object sender, EventArgs e)
   {
       string key = Request["DeleteKey"];

       if (key != null)
       {
           try
           {
               object obj = Matchnet.Caching.Cache.Instance.Remove(key);

               if (obj != null)
               {
                   if (obj is Matchnet.Member.ValueObjects.CachedMember)
                   {
                       Matchnet.Member.ValueObjects.CachedMember member = obj as Matchnet.Member.ValueObjects.CachedMember;

                       if (Application["MingleNotificationService"] != null)
                       {
                           MingleNotificationBL notificationBL = Application["MingleNotificationService"] as MingleNotificationBL;
                           notificationBL.InvalidateMember(member.MemberID);
                       }

                   }
               }

           }
           catch(Exception ex)
           {
               throw new SparkWSClientException("cache could not be emptied", ex);
           }

       }
   }
}
