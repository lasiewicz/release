﻿<%@ WebHandler Language="C#" Class="HealthCheck" %>

using System;
using System.Web;
using System.IO;
using Matchnet.Configuration.ServiceAdapters;

public class HealthCheck : IHttpHandler {
     
    
    private const string MATCHNET_SERVER_DOWN = "Matchnet Server Down";
    private const string MATCHNET_SERVER_ENABLED = "Matchnet Server Enabled";

    private HttpContext context = null;
        
    public void ProcessRequest (HttpContext context) {
        this.context = context;
        string serverStatus = MATCHNET_SERVER_DOWN;
           
        //outputs the headers that came in
        if (context.Request["Headers"] != null)
        {
            foreach (string key in context.Request.Headers.AllKeys)
            { 
                context.Response.Write (key + ":" + context.Request.Headers[key] + "<br />");
            }
        }

        if (Enabled)
        {
            serverStatus = MATCHNET_SERVER_ENABLED;
        }
        
        
        context.Response.ContentType = "text/plain";
        context.Response.AddHeader("Health", serverStatus);
        context.Response.Write(serverStatus);
        //context.Response.Write(System.Environment.MachineName.ToString ());
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    /// <summary>
    /// gets whehther the server is enabled or not
    /// </summary>
    private bool Enabled
    {
        get
        {
            string filePath = Path.GetDirectoryName(context.Server.MapPath("/")) + @"\deploy.txt";

            bool fileExists = File.Exists(filePath);
            bool isEnabled = RuntimeSettings.IsServerEnabled();

            if (!fileExists && isEnabled)
            {
                return true;
            }

            return false;
        }
    }

}