﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Spark Messaging JSON-based API for utilizing External Mail
/// </summary>
public partial class JSON_SparkMessaging : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!String.IsNullOrEmpty(Request["MessageRequest"]))
            {
                //get posted json data
                string jsonMessageRequest = Request["MessageRequest"];
                MessageRequest messageRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<MessageRequest>(jsonMessageRequest);

                //get AccessKey
                ApiAccessHeader apiAccessHeader = new ApiAccessHeader();
                if (!String.IsNullOrEmpty(messageRequest.AccessKey))
                {
                    apiAccessHeader.Key = messageRequest.AccessKey;
                }
                else
                {
                    throw new Exception("Missing access key");
                }

                //process (access key validated in BL)
                //ServiceUtil.CheckTrottle(MethodInfo.GetCurrentMethod(), apiAccessHeader.Key, 600);
                ExternalMailRequest externalMailRequest = new ExternalMailRequest();
                externalMailRequest.ExternalMailTypeID = messageRequest.ExternalMailTypeID;
                externalMailRequest.MemberID = messageRequest.MemberID;
                externalMailRequest.SiteID = messageRequest.SiteID;
                externalMailRequest.Values = messageRequest.Values;
                ExternalMailResult externalMailResult = MessagingBL.SendExternalMail(externalMailRequest, apiAccessHeader.Key);

                if (externalMailResult == null || externalMailResult.Sent)
                {
                    //json success response
                    MessageResponse response = new MessageResponse();
                    response.Status = JSONStatus.Success.ToString();
                    response.StatusMessage = "Email sent successfully";
                    Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(response));
                }
                else
                {
                    throw new Exception("External Mail service returned a failure. Reason: " + externalMailResult.Reason);
                }
            }
            else
            {
                throw new Exception("MessageRequest key not found");
            }
        }
        catch (Exception ex)
        {
            //json failed response
            MessageResponse response = new MessageResponse();
            response.Status = JSONStatus.Failed.ToString();
            response.StatusMessage = "Error. " + ex.Message;
            Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(response));
        }

    }
}
