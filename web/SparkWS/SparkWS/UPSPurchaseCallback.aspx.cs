﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet;
using Matchnet.Purchase.ServiceAdapters;
using Newtonsoft.Json;
using Spark.Common.UPS;
using Spark.Common;
using System.Net;
using System.Collections.Specialized;
using Spark.Common.Adapter;
using Spark.Common.OrderHistoryService;

public partial class UPSPurchaseCallback : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string OrderID = Constants.NULL_STRING;

        try
        {
            //get posted data
            OrderID = Request.Form["OrderID"];
            string Version = Request.Form["Version"];
            string ResponseCode = Request.Form["ResponseCode"];
            string ResponseMessage = Request.Form["ResponseMessage"];
            string TransactionTypeID = Request.Form["TransactionTypeID"];
            string CustomerID = Request.Form["CustomerID"];
            string CallingSystemID = Request.Form["CallingSystemID"];
            string CallingSystemCustomData = Request.Form["CallingSystemCustomData"];
            PaymentType orderPaymentType = PaymentType.None;

            if (OrderID != "")
            {
                int orderID = Convert.ToInt32(Request.Form["OrderID"]);

                // Call OrderHistory to get order data
                OrderInfo orderInfo = OrderHistoryServiceWebAdapter.GetProxyInstanceForBedrock().GetOrderInfo(orderID);
                if (orderInfo != null)
                {
                    // Check if the legacy data already exists for this order
                    NameValueCollection legacyDataCollection = PurchaseSA.Instance.UPSGetMemberPaymentByOrderID(orderInfo.CustomerID, orderID, orderInfo.CallingSystemID);
                    if (legacyDataCollection["MemberTranID"] != null)
                    {
                        // Legacy data already exists for this order so do not add another legacy data  
                        Response.StatusDescription = "true";
                        return;
                    }

                    int referenceOrderID = orderInfo.ReferenceOrderID;
                    int orderCommonResponseCode = orderInfo.InternalResponseStatusID;
                    int orderMemberTranStatusID = orderInfo.OrderStatusID;
                    int orderChargeID = Constants.NULL_INT;
                    int orderDetailDurationTypeID = Constants.NULL_INT;
                    int orderDetailDuration = Constants.NULL_INT;
                    DateTime orderInsertDateInPST = orderInfo.InsertDateInPST;
                    List<int> arrDistinctOrderPackageID = new List<int>();
                    bool containsColorAnalysis = false;

                    // Get the transaction type to determine which existing purchase method to call
                    List<spark.net.purchase.TransactionType> arrOrderTransactionTypes = new List<spark.net.purchase.TransactionType>();
                    foreach (OrderDetailInfo orderDetailInfo in orderInfo.OrderDetail)
                    {
                        if (orderDetailInfo.OrderTypeID != Constants.NULL_INT)
                        {
                            spark.net.purchase.TransactionType orderDetailTransactionType = (spark.net.purchase.TransactionType)orderDetailInfo.OrderTypeID;
                            if (!arrOrderTransactionTypes.Contains(orderDetailTransactionType))
                            {
                                arrOrderTransactionTypes.Add(orderDetailTransactionType);
                            }

                            // An initial purchase order contains multiple order detail items in a package
                            // Each order detail has a duration type and duration
                            // The rule for UPS integration is that all the order details for an order must
                            // have the same duration type and must have the same duration
                            // For example, if the package contains a subscription and highlighted profile, and
                            // the subscription is for 1 month, than the highlighted profile must also be 1 month  
                            // Similarly, a credit order can have different duration type and duration for the individual
                            // order details.  But the rule is that changing the duration type and duration must be the same
                            // for all order details when doing a credit.  
                            // For orders that also contains a discount, do not use the duration and duration type for 
                            // the discount package since the duration is 0 and the duration type is None.  Use the 
                            // duration and the duration type of the primary package.  
                            if ((orderDetailDurationTypeID == Constants.NULL_INT && orderDetailDuration == Constants.NULL_INT)
                                && (orderDetailInfo.DiscountID <= 0))
                            {
                                // If the orderDetailDurationTypeID and orderDetailDuration were not yet set and the order detail
                                // is not a discount package than set the orderDetailDurationTypeID and orderDetailDuration
                                orderDetailDurationTypeID = orderDetailInfo.DurationTypeID;
                                // The initial duration for an order detail is not set to Constants.NULL_INT but instead set to
                                // System.Int32.MinValue.  However, a comparison to Constants.NULL_INT is used rather than 
                                // System.Int32.MinValue in the legacy purchase service.  So if the duration is initialized to
                                // System.Int32.MinValue in UPS, use Constants.NULL_INT instead for the initial value.  This
                                // is required for setting a proper start and end date in the legacy purchase service.   
                                orderDetailDuration = (orderDetailInfo.Duration == System.Int32.MinValue ? Constants.NULL_INT : orderDetailInfo.Duration);
                            }
                            else if (orderDetailDurationTypeID == 0)
                            {
                                // If orderDetailDurationTypeID has been set to none 
                                // Can occur if the first order detail was for a remaining credit package 
                                // A remaining credit package has a DurationTypeID of 0  
                                if (orderDetailInfo.DurationTypeID > 0)
                                {
                                    // Set orderDetailDurationTypeID and orderDetailDuration to actual durations
                                    // if they exits
                                    // This is the case when there is a order detail for a remaining package where
                                    // the DurationTypeID is 0 and the Duration is 0 for this order detail
                                    orderDetailDurationTypeID = orderDetailInfo.DurationTypeID;
                                    orderDetailDuration = (orderDetailInfo.Duration == System.Int32.MinValue ? Constants.NULL_INT : orderDetailInfo.Duration);
                                }
                            }

                            if (orderDetailInfo.ItemID == 1003)
                            {
                                containsColorAnalysis = true;
                            }
                        }

                        if (!arrDistinctOrderPackageID.Contains(orderDetailInfo.PackageID))
                        {
                            arrDistinctOrderPackageID.Add(orderDetailInfo.PackageID);
                        }
                    }

                    // If the OrderTypeID in the order details has an adjustment, than process this transaction 
                    // as an administrative adjustment                      
                    if (arrOrderTransactionTypes.Contains(spark.net.purchase.TransactionType.Adjustment)
                        && orderInfo.TotalAmount <= 0)
                    {
                        ProcessAdministrativeAdjustments(orderInfo);
                        Response.StatusDescription = "true";
                        return;
                    }
                    else if (arrOrderTransactionTypes.Contains(spark.net.purchase.TransactionType.AdditionalNonSubscriptionPurchase)
                        && containsColorAnalysis == true)
                    {
                        // Do not insert legacy data for color analysis purchases  
                        Response.StatusDescription = "true";
                        return;
                    }

                    spark.net.paymentwrapper.PaymentWrapperServiceClient paymentWrapperProxy = PaymentWrapperAdapter.GetProxyInstance();
                    string paymentResponse = paymentWrapperProxy.GetObfuscatedPaymentProfile(orderInfo.UserPaymentGuid, Convert.ToString(orderInfo.CallingSystemID));
                    ServiceCallResult serviceCallResult = PaymentResponseHelper.LoadResultObject(paymentResponse);
                    if (serviceCallResult.ResponseCode == "0")
                    {
                        orderPaymentType = (PaymentType)Enum.Parse(typeof(PaymentType), Convert.ToString(serviceCallResult.ResponseValues["PaymentType"]));

                        int orderMemberID = orderInfo.CustomerID;
                        int orderAdminMemberID = orderInfo.AdminUserID;
                        //int orderPlanID = orderInfo.OrderDetail[0].PackageID;
                        int orderDiscountID = Constants.NULL_INT;
                        int orderSiteID = orderInfo.CallingSystemID;
                        int orderBrandID = Constants.NULL_INT;
                        Matchnet.Purchase.ValueObjects.CreditCardType orderCreditCardType = 0;
                        string lastFourAccountNumber = Constants.NULL_STRING;
                        int orderConversionMemberID = Constants.NULL_INT;
                        int orderSourceID = Constants.NULL_INT;
                        int orderPurchaseReasonTypeID = Constants.NULL_INT;
                        string orderIPAddress = Constants.NULL_STRING;
                        int orderPromoID = Constants.NULL_INT;
                        decimal orderUICreditAmount = Constants.NULL_DECIMAL;
                        Matchnet.Purchase.ValueObjects.PurchaseMode orderPurchaseMode = Matchnet.Purchase.ValueObjects.PurchaseMode.None;
                        //decimal orderUICreditAmount = legacyPurchaseData.Data.LegacyDataInfo.UICreditAmount;
                        //Matchnet.Purchase.ValueObjects.PurchaseMode orderPurchaseMode = (Matchnet.Purchase.ValueObjects.PurchaseMode)legacyPurchaseData.Data.LegacyDataInfo.PurchaseMode;
                        bool orderReusePreviousPayment = false;
                        int orderUPSLegacyDataID = orderInfo.UPSLegacyDataID;
                        string orderFirstName = Constants.NULL_STRING;
                        string orderLastName = Constants.NULL_STRING;
                        //int orderReferenceMemberTranID = Constants.NULL_INT;
                        int orderReferenceMemberPaymentID = Constants.NULL_INT;
                        //int orderReferencePlanID = Constants.NULL_INT;
                        Matchnet.Purchase.ValueObjects.CreditCardType orderReferenceOrderCreditCardType = 0;
                        decimal orderAmount = orderInfo.TotalAmount;
                        Int32 orderDuration = orderDetailDuration;
                        Matchnet.DurationType orderDurationType = (Matchnet.DurationType)orderDetailDurationTypeID;
                        Matchnet.Purchase.ValueObjects.CurrencyType orderCurrencyType = (Matchnet.Purchase.ValueObjects.CurrencyType)orderInfo.CurrencyID;

                        //Matchnet.Purchase.ValueObjects.MemberSub ms = PurchaseSA.Instance.GetSubscription(100066132, 103);

                        Matchnet.Purchase.ValueObjects.TranType legacyTranType = 0;
                        Matchnet.Purchase.ValueObjects.OriginalPurchaseActionTypeForUPS originalPurchaseActionTypeForUPS = Matchnet.Purchase.ValueObjects.OriginalPurchaseActionTypeForUPS.None;

                        if ((arrOrderTransactionTypes.Contains(spark.net.purchase.TransactionType.InitialSubscriptionPurchase)
                                || arrOrderTransactionTypes.Contains(spark.net.purchase.TransactionType.AdditionalNonSubscriptionPurchase)
                                || arrOrderTransactionTypes.Contains(spark.net.purchase.TransactionType.AdditionalSubscriptionPurchase)
                                || arrOrderTransactionTypes.Contains(spark.net.purchase.TransactionType.GiftRedeem)
                            )
                            &&
                            (orderPaymentType == PaymentType.CreditCard
                                || orderPaymentType == PaymentType.Check
                                || orderPaymentType == PaymentType.PayPalLitle
                                || orderPaymentType == PaymentType.ElectronicFundsTransfer
                                || orderPaymentType == PaymentType.Manual
                                || orderPaymentType == PaymentType.PaymentReceived
                                || orderPaymentType == PaymentType.PaypalDirect
                                )
                            )
                        {
                            // Need to get the UPSLegacyDataID from the OrderID
                            string compressedlegacyDataInJSON = PurchaseSA.Instance.UPSLegacyDataGet(orderUPSLegacyDataID);

                            if (String.IsNullOrEmpty(compressedlegacyDataInJSON))
                            {
                                orderSiteID = orderInfo.CallingSystemID;
                            }
                            else
                            {
                                string legacyDataInJSON = StringExtension.Decompress(compressedlegacyDataInJSON);
                                //string legacyDataInJSON = PurchaseSA.Instance.UPSLegacyDataGet(100032);

                                if (legacyDataInJSON != null)
                                {
                                    PaymentJson legacyPurchaseData = JsonConvert.DeserializeObject<PaymentJson>(legacyDataInJSON);

                                    orderSiteID = legacyPurchaseData.Data.LegacyDataInfo.SiteID;
                                    orderBrandID = legacyPurchaseData.Data.LegacyDataInfo.BrandID;
                                    orderConversionMemberID = legacyPurchaseData.Data.LegacyDataInfo.ConversionMemberID;
                                    orderSourceID = legacyPurchaseData.Data.LegacyDataInfo.SourceID;
                                    orderPurchaseReasonTypeID = legacyPurchaseData.Data.LegacyDataInfo.PurchaseReasonTypeID;
                                    orderIPAddress = Convert.ToString(legacyPurchaseData.Data.MemberInfo.IPAddress);
                                    orderPromoID = legacyPurchaseData.Data.LegacyDataInfo.PromoID;
                                    orderReusePreviousPayment = (legacyPurchaseData.Data.LegacyDataInfo.ReUsePreviousPayment > 0 ? true : false);

                                    // Get the credit amount and the purchase mode from the PackageID of the order
                                    // The PackageID is the same as the PlanID that was purchased by the member  
                                    // However an order can have multiple packages including remaining credit package, 
                                    // discount package, and a la carte packages
                                    // First check to see if any of packages purchased in the order is valid for a credit
                                    // If so, than the PurchaseMode for this entire order will be an upgrade or swap and
                                    // the PurchaseMode and unused credit amount needs to be saved with the transaction
                                    // If no unused credits were used in the order, than the PurchaseMode will be set to 
                                    // the first PurchaseMode found that did not have a PurchaseMode of None
                                    Matchnet.Purchase.ValueObjects.PurchaseMode checkPurchaseMode = Matchnet.Purchase.ValueObjects.PurchaseMode.None;
                                    foreach (PaymentJsonPackage paymentJSONPackage in legacyPurchaseData.Data.Packages)
                                    {
                                        if (arrDistinctOrderPackageID.Contains(paymentJSONPackage.ID))
                                        {
                                            checkPurchaseMode = (Matchnet.Purchase.ValueObjects.PurchaseMode)Enum.Parse(typeof(Matchnet.Purchase.ValueObjects.PurchaseMode), Convert.ToString(paymentJSONPackage.Items["PurchaseMode"]));
                                            if (Matchnet.Purchase.ValueObjects.Plan.IsPurchaseModeValidForCredit(checkPurchaseMode))
                                            {
                                                orderPurchaseMode = checkPurchaseMode;
                                                orderUICreditAmount = Convert.ToDecimal(paymentJSONPackage.Items["CreditAmount"]);
                                                break;
                                            }
                                            else
                                            {
                                                if (orderPurchaseMode == Matchnet.Purchase.ValueObjects.PurchaseMode.None)
                                                {
                                                    orderPurchaseMode = checkPurchaseMode;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    throw new ApplicationException("Called PurchaseSA.Instance.UPSLegacyDataGet() but the legacy data does not exists for OrderID " + orderID + ", Callback was not completed");
                                }

                            }

                            if (orderPaymentType == PaymentType.CreditCard)
                            {
                                // Get the ChargeID from the authorization 
                                // Do not use the ChargeID from the charge  
                                foreach (OrderUserPaymentInfo orderUserPaymentInfo in orderInfo.OrderUserPayment)
                                {
                                    // ChargeTypeID of 2 is Authorization
                                    if (orderUserPaymentInfo.ChargeTypeID == 2)
                                    {
                                        orderChargeID = orderUserPaymentInfo.ChargeID;
                                    }
                                }

                                orderCreditCardType = (Matchnet.Purchase.ValueObjects.CreditCardType)Enum.Parse(typeof(Matchnet.Purchase.ValueObjects.CreditCardType), Convert.ToString(serviceCallResult.ResponseValues["CardType"]));
                                if (serviceCallResult.ResponseValues.ContainsKey("CreditCardNumber"))
                                {
                                    lastFourAccountNumber = Convert.ToString(serviceCallResult.ResponseValues["CreditCardNumber"]);
                                }
                                else
                                {
                                    lastFourAccountNumber = "xxxx";
                                }
                                orderFirstName = Convert.ToString(serviceCallResult.ResponseValues["FirstName"]);
                                orderLastName = Convert.ToString(serviceCallResult.ResponseValues["LastName"]);

                                legacyTranType = Matchnet.Purchase.ValueObjects.TranType.InitialBuy;
                                originalPurchaseActionTypeForUPS = Matchnet.Purchase.ValueObjects.OriginalPurchaseActionTypeForUPS.BeginCreditCardPurchase;
                            }
                            else if (orderPaymentType == PaymentType.Check
                                     || orderPaymentType == PaymentType.ElectronicFundsTransfer
                                     || orderPaymentType == PaymentType.Manual
                                     || orderPaymentType == PaymentType.PaymentReceived
                                     || orderPaymentType == PaymentType.PaypalDirect)
                            {
                                // Get the ChargeID from the authorization 
                                // Do not use the ChargeID from the charge  
                                foreach (OrderUserPaymentInfo orderUserPaymentInfo in orderInfo.OrderUserPayment)
                                {
                                    // ChargeTypeID of 2 is Authorization
                                    if (orderUserPaymentInfo.ChargeTypeID == 2)
                                    {
                                        orderChargeID = orderUserPaymentInfo.ChargeID;
                                    }
                                }

                                if (serviceCallResult.ResponseValues.ContainsKey("BankAccountNumber"))
                                {
                                    lastFourAccountNumber = Convert.ToString(serviceCallResult.ResponseValues["BankAccountNumber"]);
                                }
                                else
                                {
                                    lastFourAccountNumber = "xxxx";
                                }
                                orderFirstName = Convert.ToString(serviceCallResult.ResponseValues["FirstName"]);
                                orderLastName = Convert.ToString(serviceCallResult.ResponseValues["LastName"]);

                                legacyTranType = Matchnet.Purchase.ValueObjects.TranType.InitialBuy;
                                originalPurchaseActionTypeForUPS = Matchnet.Purchase.ValueObjects.OriginalPurchaseActionTypeForUPS.BeginCheckPurchase;
                            }
                            else if (orderPaymentType == PaymentType.PayPalLitle)
                            {
                                // Get the ChargeID from the authorization 
                                // Do not use the ChargeID from the charge  
                                foreach (OrderUserPaymentInfo orderUserPaymentInfo in orderInfo.OrderUserPayment)
                                {
                                    // ChargeTypeID of 2 is Authorization
                                    if (orderUserPaymentInfo.ChargeTypeID == 2)
                                    {
                                        orderChargeID = orderUserPaymentInfo.ChargeID;
                                    }
                                }

                                if (serviceCallResult.ResponseValues.ContainsKey("LastFourAccountNumber"))
                                {
                                    lastFourAccountNumber = Convert.ToString(serviceCallResult.ResponseValues["LastFourAccountNumber"]);
                                }
                                else
                                {
                                    lastFourAccountNumber = "xxxx";
                                }
                                orderFirstName = Convert.ToString(serviceCallResult.ResponseValues["FirstName"]);
                                orderLastName = Convert.ToString(serviceCallResult.ResponseValues["LastName"]);

                                legacyTranType = Matchnet.Purchase.ValueObjects.TranType.InitialBuy;
                                originalPurchaseActionTypeForUPS = Matchnet.Purchase.ValueObjects.OriginalPurchaseActionTypeForUPS.BeginPaypalPurchase;
                            }
                        }
                        else if (arrOrderTransactionTypes.Contains(spark.net.purchase.TransactionType.Credit)
                                || arrOrderTransactionTypes.Contains(spark.net.purchase.TransactionType.Void))
                        {
                            // Get the ChargeID from the authorization 
                            // Do not use the ChargeID from the charge  
                            foreach (OrderUserPaymentInfo orderUserPaymentInfo in orderInfo.OrderUserPayment)
                            {
                                // ChargeTypeID of 3 is Credit
                                // ChargeTypeID of 4 is Void
                                if (orderUserPaymentInfo.ChargeTypeID == 3
                                    || orderUserPaymentInfo.ChargeTypeID == 4)
                                {
                                    orderChargeID = orderUserPaymentInfo.ChargeID;
                                }
                            }

                            // The siteID, referenceMemberTranID, referenceMemberPaymentID, and the referencePlanID
                            // will be retrieved inside the purchase service by getting the details of the original
                            // transaction from an OrderID  
                            if (orderPaymentType == PaymentType.CreditCard)
                            {
                                orderReferenceOrderCreditCardType = (Matchnet.Purchase.ValueObjects.CreditCardType)Enum.Parse(typeof(Matchnet.Purchase.ValueObjects.CreditCardType), Convert.ToString(serviceCallResult.ResponseValues["CardType"]));
                            }
                            orderFirstName = Convert.ToString(serviceCallResult.ResponseValues["FirstName"]);
                            orderLastName = Convert.ToString(serviceCallResult.ResponseValues["LastName"]);

                            legacyTranType = Matchnet.Purchase.ValueObjects.TranType.AdministrativeAdjustment;
                            originalPurchaseActionTypeForUPS = Matchnet.Purchase.ValueObjects.OriginalPurchaseActionTypeForUPS.BeginCredit;

                            // There are no time adjustments allowed for credits  
                            //orderDuration = 0;
                            //orderDurationType = DurationType.None;

                            //ET-720, 12282009 TL, time adjustments are now allowed through credit tool
                            if (orderDuration != Constants.NULL_INT)
                            {
                                //durations should always be negative for credits/voids
                                orderDuration = -(Math.Abs(orderDuration));
                            }
                            else
                            {
                                orderDuration = 0;
                                orderDurationType = DurationType.None;
                            }
                        }
                        else if (arrOrderTransactionTypes.Contains(spark.net.purchase.TransactionType.Renewal))
                        {
                            orderSiteID = orderInfo.CallingSystemID;

                            // Get the ChargeID from the authorization 
                            // Do not use the ChargeID from the charge  
                            foreach (OrderUserPaymentInfo orderUserPaymentInfo in orderInfo.OrderUserPayment)
                            {
                                // ChargeTypeID of 2 is Authorization
                                // A renewal does both an authorization and a charge  
                                if (orderUserPaymentInfo.ChargeTypeID == 2)
                                {
                                    orderChargeID = orderUserPaymentInfo.ChargeID;
                                }
                            }

                            if (orderPaymentType == PaymentType.CreditCard)
                            {
                                orderCreditCardType = (Matchnet.Purchase.ValueObjects.CreditCardType)Enum.Parse(typeof(Matchnet.Purchase.ValueObjects.CreditCardType), Convert.ToString(serviceCallResult.ResponseValues["CardType"]));
                            }
                            orderFirstName = Convert.ToString(serviceCallResult.ResponseValues["FirstName"]);
                            orderLastName = Convert.ToString(serviceCallResult.ResponseValues["LastName"]);

                            legacyTranType = Matchnet.Purchase.ValueObjects.TranType.Renewal;
                            originalPurchaseActionTypeForUPS = Matchnet.Purchase.ValueObjects.OriginalPurchaseActionTypeForUPS.BeginRenewal;
                        }

                        // Call the existing purchase service to update the legacy data  
                        int newLegacyMemberTranID = PurchaseSA.Instance.UPSProcessLegacyData(orderMemberID,
                                                            orderAdminMemberID,
                                                            arrDistinctOrderPackageID.ToArray(),
                                                            orderDiscountID,
                                                            orderSiteID,
                                                            orderBrandID,
                                                            orderCreditCardType,
                                                            orderConversionMemberID,
                                                            orderSourceID,
                                                            orderPurchaseReasonTypeID,
                                                            orderIPAddress,
                                                            orderPromoID,
                                                            orderUICreditAmount,
                                                            orderPurchaseMode,
                                                            orderReusePreviousPayment,
                                                            orderFirstName,
                                                            orderLastName,
                                                            legacyTranType,
                                                            orderReferenceMemberPaymentID,
                                                            orderReferenceOrderCreditCardType,
                                                            orderAmount,
                                                            orderDuration,
                                                            orderDurationType,
                                                            orderCurrencyType,
                                                            lastFourAccountNumber,
                                                            orderID,
                                                            referenceOrderID,
                                                            orderCommonResponseCode,
                                                            orderMemberTranStatusID,
                                                            orderInsertDateInPST,
                                                            orderChargeID,
                                                            originalPurchaseActionTypeForUPS,
                                                            (int)orderPaymentType);
                        if (newLegacyMemberTranID <= 0)
                        {
                            throw new ApplicationException("Called PurchaseSA.UPSProcessLegacyData() but the legacy purchase data was not inserted for OrderID " + OrderID + ", Callback was not completed");
                        }
                    }
                    else
                    {
                        throw new ApplicationException("Called PaymentWrapper.GetObfuscatedPaymentProfile() but the payment profile could not be found for UserPaymentGUID " + Convert.ToString(orderInfo.UserPaymentGuid) + " and CallingSystemID " + Convert.ToString(orderInfo.CallingSystemID) + " and OrderID " + OrderID + ", Callback was not completed");
                    }
                }
                else
                {
                    throw new ApplicationException("Called OrderHistory.GetOrderInfo() but the OrderInfo does not exists for OrderID " + OrderID + ", Callback was not completed");
                }

                //Response.Headers.Add("CallbackStatus", "1");
                //Response.SubStatusCode = 1212;
                Response.StatusDescription = "true";
            }
        }
        catch (Exception ex)
        {
            if (!String.IsNullOrEmpty(OrderID))
            {
                ServiceUtil.LogEvent("WWW", "UPS Purchase Callback Error for OrderID " + OrderID + ", Error message: " + ex.Message.ToString(), System.Diagnostics.EventLogEntryType.Error);
            }
            else
            {
                ServiceUtil.LogEvent("WWW", "UPS Purchase Callback Error, Error message: " + ex.Message.ToString(), System.Diagnostics.EventLogEntryType.Error);
            }

            //Response.Headers.Add("CallbackStatus", "0");
            Response.StatusDescription = "false";
        }
    }

    private int ProcessAdministrativeAdjustments(OrderInfo orderInfo)
    {
        int orderMemberID = orderInfo.CustomerID;
        int orderAdminMemberID = orderInfo.AdminUserID;

        int orderDetailDurationTypeID = Constants.NULL_INT;
        int orderDetailDuration = Constants.NULL_INT;
        List<string> arrDistinctPrivileges = new List<string>();

        foreach (OrderDetailInfo orderDetailInfo in orderInfo.OrderDetail)
        {
            if (orderDetailInfo.OrderTypeID != Constants.NULL_INT)
            {
                // An initial purchase order contains multiple order detail items in a package
                // Each order detail has a duration type and duration
                // The rule for UPS integration is that all the order details for an order must
                // have the same duration type and must have the same duration
                // For example, if the package contains a subscription and highlighted profile, and
                // the subscription is for 1 month, than the highlighted profile must also be 1 month  
                // Similarly, a credit order can have different duration type and duration for the individual
                // order details.  But the rule is that changing the duration type and duration must be the same
                // for all order details when doing a credit.  
                // For orders that also contains a discount, do not use the duration and duration type for 
                // the discount package since the duration is 0 and the duration type is None.  Use the 
                // duration and the duration type of the primary package.  
                if ((orderDetailDurationTypeID == Constants.NULL_INT && orderDetailDuration == Constants.NULL_INT)
                    && (orderDetailInfo.DiscountID <= 0))
                {
                    // If the orderDetailDurationTypeID and orderDetailDuration were not yet set and the order detail
                    // is not a discount package than set the orderDetailDurationTypeID and orderDetailDuration
                    orderDetailDurationTypeID = orderDetailInfo.DurationTypeID;
                    // The initial duration for an order detail is not set to Constants.NULL_INT but instead set to
                    // System.Int32.MinValue.  However, a comparison to Constants.NULL_INT is used rather than 
                    // System.Int32.MinValue in the legacy purchase service.  So if the duration is initialized to
                    // System.Int32.MinValue in UPS, use Constants.NULL_INT instead for the initial value.  This
                    // is required for setting a proper start and end date in the legacy purchase service.   
                    orderDetailDuration = (orderDetailInfo.Duration == System.Int32.MinValue ? Constants.NULL_INT : orderDetailInfo.Duration);
                }
                else if (orderDetailDurationTypeID == 0)
                {
                    // If orderDetailDurationTypeID has been set to none 
                    // Can occur if the first order detail was for a remaining credit package 
                    // A remaining credit package has a DurationTypeID of 0  
                    if (orderDetailInfo.DurationTypeID > 0)
                    {
                        // Set orderDetailDurationTypeID and orderDetailDuration to actual durations
                        // if they exits
                        // This is the case when there is a order detail for a remaining package where
                        // the DurationTypeID is 0 and the Duration is 0 for this order detail
                        orderDetailDurationTypeID = orderDetailInfo.DurationTypeID;
                        orderDetailDuration = (orderDetailInfo.Duration == System.Int32.MinValue ? Constants.NULL_INT : orderDetailInfo.Duration);
                    }
                }
            }

            if (!arrDistinctPrivileges.Contains(orderDetailInfo.ItemDescription))
            {
                arrDistinctPrivileges.Add(orderDetailInfo.ItemDescription);
            }
        }

        Matchnet.DurationType orderDurationType = (Matchnet.DurationType)orderDetailDurationTypeID;
        int orderDuration = orderDetailDuration;
        int timeAdjustOverrideType = 0;
        foreach (string item in arrDistinctPrivileges)
        {
            switch (item)
            {
                case "Basic Subscription":
                    timeAdjustOverrideType += 1;
                    break;
                case "Highlighted Service":
                    timeAdjustOverrideType += 2;
                    break;
                case "SpotLight Service":
                    timeAdjustOverrideType += 4;
                    break;
                default:
                    break;
            }
        }

        List<int> arrDistinctOrderPackageID = new List<int>();
        arrDistinctOrderPackageID.Add(0);

        int orderTimeAdjustOverrideTypeID = orderInfo.PromoID;
        int orderID = orderInfo.OrderID;
        int orderCommonResponseCode = orderInfo.InternalResponseStatusID;
        int orderMemberTranStatusID = orderInfo.OrderStatusID;
        DateTime orderInsertDateInPST = orderInfo.InsertDateInPST;
        Matchnet.Purchase.ValueObjects.TranType legacyTranType = Matchnet.Purchase.ValueObjects.TranType.AdministrativeAdjustment;
        Matchnet.Purchase.ValueObjects.OriginalPurchaseActionTypeForUPS originalPurchaseActionTypeForUPS = Matchnet.Purchase.ValueObjects.OriginalPurchaseActionTypeForUPS.BeginUpdateSubscription;
        PaymentType orderPaymentType = PaymentType.None;

        int orderDiscountID = Constants.NULL_INT;
        int orderSiteID = orderInfo.CallingSystemID;
        int orderBrandID = Constants.NULL_INT;
        Matchnet.Purchase.ValueObjects.CreditCardType orderCreditCardType = 0;
        string lastFourAccountNumber = Constants.NULL_STRING;
        int orderConversionMemberID = Constants.NULL_INT;
        int orderSourceID = Constants.NULL_INT;
        int orderPurchaseReasonTypeID = Constants.NULL_INT;
        string orderIPAddress = Constants.NULL_STRING;
        int orderPromoID = timeAdjustOverrideType;
        decimal orderUICreditAmount = Constants.NULL_DECIMAL;
        Matchnet.Purchase.ValueObjects.PurchaseMode orderPurchaseMode = Matchnet.Purchase.ValueObjects.PurchaseMode.None;
        bool orderReusePreviousPayment = false;
        int orderUPSLegacyDataID = orderInfo.UPSLegacyDataID;
        string orderFirstName = Constants.NULL_STRING;
        string orderLastName = Constants.NULL_STRING;
        int orderReferenceMemberPaymentID = Constants.NULL_INT;
        Matchnet.Purchase.ValueObjects.CreditCardType orderReferenceOrderCreditCardType = 0;
        decimal orderAmount = orderInfo.TotalAmount;
        Matchnet.Purchase.ValueObjects.CurrencyType orderCurrencyType = (Matchnet.Purchase.ValueObjects.CurrencyType)orderInfo.CurrencyID;
        int referenceOrderID = Constants.NULL_INT;
        int orderChargeID = Constants.NULL_INT;

        // Call the existing purchase service to update the legacy data  
        int newLegacyMemberTranID = PurchaseSA.Instance.UPSProcessLegacyData(orderMemberID,
                                            orderAdminMemberID,
                                            arrDistinctOrderPackageID.ToArray(),
                                            orderDiscountID,
                                            orderSiteID,
                                            orderBrandID,
                                            orderCreditCardType,
                                            orderConversionMemberID,
                                            orderSourceID,
                                            orderPurchaseReasonTypeID,
                                            orderIPAddress,
                                            orderPromoID,
                                            orderUICreditAmount,
                                            orderPurchaseMode,
                                            orderReusePreviousPayment,
                                            orderFirstName,
                                            orderLastName,
                                            legacyTranType,
                                            orderReferenceMemberPaymentID,
                                            orderReferenceOrderCreditCardType,
                                            orderAmount,
                                            orderDuration,
                                            orderDurationType,
                                            orderCurrencyType,
                                            lastFourAccountNumber,
                                            orderID,
                                            referenceOrderID,
                                            orderCommonResponseCode,
                                            orderMemberTranStatusID,
                                            orderInsertDateInPST,
                                            orderChargeID,
                                            originalPurchaseActionTypeForUPS,
                                            (int)orderPaymentType);
        if (newLegacyMemberTranID <= 0)
        {
            throw new ApplicationException("Called PurchaseSA.UPSProcessLegacyData() but the legacy purchase data was not inserted for OrderID " + orderID + ", Callback was not completed");
        }

        return newLegacyMemberTranID;
    }
}
