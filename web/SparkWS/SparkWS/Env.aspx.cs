using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Matchnet.Security;

public partial class Env : System.Web.UI.Page
{
   protected void Page_Load(object sender, EventArgs e)
   {
      if (Application.Contents["NotificationCounter"] == null)
      {
          Application.Contents["NotificationCounter"] = 0;
      }

      if (Request.QueryString["MemberID"] != null)
      {
          if (Request.QueryString["MemberID"].ToString () != "-1")
          {
            Application["NotificationCounter"] = (int)Application["NotificationCounter"] + 1;
          }
      }

      if (Request.QueryString["NotificationCounter"] != null)
      { 
        Response.Write (Application.Contents ["NotificationCounter"].ToString ());
      }



	  Response.Write("=====================================<br>\n");
	  foreach (string key in Request.QueryString.AllKeys)
	  {
		 Response.Write(key + " = " + Request.QueryString[key] + "<br>\n");
	  }
	  Response.Write("=====================================<br>\n");
	  foreach (string key in Request.Form.AllKeys)
	  {
		 Response.Write(key + " = " + Request.Form[key] + "<br>\n");
	  }
	  Response.Write("=====================================<br>\n");
	  foreach (string key in Request.ServerVariables.AllKeys)
	  {
		 if (key.StartsWith("REMOTE") || key.StartsWith("HTTP_"))
			Response.Write(key + " = " + Request.ServerVariables[key] + "<br>\n");
	  }



   }
   //protected void Button1_Click(object sender, EventArgs e)
   //{
   //   try
   //   {
   //      ThrottleBL.Instance.Ding(Request.ServerVariables["REMOTE_HOST"]);
   //   }
   //   catch (Exception ex) { 
   //      Label1.Text = ex.ToString();
   //   }
   //}
}
