using System;
using System.Web;

namespace Matchnet.Web.FrameworkNew
{
	public class Dispatch : IHttpModule
	{
		public Dispatch()
		{
		}

		public void Init(HttpApplication application) 
		{
			application.BeginRequest += (new EventHandler(this.Application_BeginRequest));
		}

		private void Application_BeginRequest(Object source, EventArgs e) 
		{
			HttpContext Context = HttpContext.Current;
			string requestPath = Context.Request.Path.ToLower();

			if (!requestPath.EndsWith(".aspx") || 
				requestPath.EndsWith("404form.aspx") ||
				requestPath.EndsWith("tag-744.aspx") ||
				requestPath.EndsWith("get_aspx_ver.aspx") )  // This is a special page used by dev studio
			{
				return;
			}

			Context.RewritePath("/BasePage.aspx");
		}


		public void Dispose() 
		{
		}
	}
}
