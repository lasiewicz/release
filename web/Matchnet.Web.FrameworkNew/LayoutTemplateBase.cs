using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Session.ValueObjects;


namespace Matchnet.Web.FrameworkNew
{
	public class LayoutTemplateBase : ControlBase
	{
		private ControlBase _appControl;
		protected PlaceHolder AppControlPlaceholder;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (_appControl != null)
			{
				AppControlPlaceholder.Controls.Add(_appControl);
			}
		}


		public ControlBase AppControl
		{
			get
			{
				return _appControl;
			}
			set
			{
				_appControl = value;
			}
		}


		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}

		
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
	}
}
