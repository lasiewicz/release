using System;

using Matchnet.Content;

namespace Matchnet.Web.FrameworkNew
{
	public enum NotificationType: int
	{
		Normal,
		Error
	}

	public class Notification
	{
		private NotificationType _NotifyType;
		private int _ResourceID = Matchnet.Lib.ConstantsTemp.RESOURCE_NOT_FOUND;
		private string[] _Args;
		private string _Message= "";

		public Notification(NotificationType notifyType)
		{
			_NotifyType = notifyType;
			_Message = "undefined";
		}

		// Constructors that use ResourceConstants
		public Notification(NotificationType notifyType, string resourceConstant)
		{
			_NotifyType = notifyType;
			_ResourceID = Translator.GetResourceID(resourceConstant);
		}

		public Notification(NotificationType notifyType, string resourceConstant, string[] args)
		{
			_NotifyType = notifyType;
			_ResourceID = Translator.GetResourceID(resourceConstant);
			_Args = args;
		}

		// Constructurs that use ResourceIDs
		public Notification(NotificationType notifyType, int resourceID)
		{
			_NotifyType = notifyType;
			_ResourceID = resourceID;
		}

		public Notification(NotificationType notifyType, int resourceID, string[] args)
		{
			_NotifyType = notifyType;
			_ResourceID = resourceID;
			_Args = args;
		}

		public override bool Equals(object obj)
		{
			if (obj == null) return false;

			if (this.GetType() != obj.GetType()) return false;

			// safe because of the GetType check
			Notification notification = (Notification)obj;

			// use this pattern to compare value members
			if (!ResourceID.Equals(notification.ResourceID)) return false;
			if (!Message.Equals(notification.Message)) return false;
			
			// need to do something for args.
			if (Args != null) 
			{
				if (notification.Args == null) return false;
				if (Args.Length != notification.Args.Length) return false;

				for (int i = 0; i < Args.Length; i++) 
				{
					if (Args[i] != notification.Args[i]) return false;
				}
			} 
			else 
			{
				if (notification.Args != null) return false;
			}
			return true;
		}

		public override int GetHashCode()
		{
			return _ResourceID.GetHashCode();
		}

		public NotificationType NotifyType
		{
			get
			{
				return _NotifyType;
			}
		}

		public int ResourceID
		{
			get
			{
				return _ResourceID;
			}
		}

		public string[] Args
		{
			get
			{
				return _Args;
			}
		}

		public string Message
		{
			get
			{
				return _Message;
			}
			set
			{
				_Message = value;
			}

		}
	}
}
