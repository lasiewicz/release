using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Resources;
using System.Globalization;
using System.Threading;

using Matchnet.Session.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;


namespace Matchnet.Web.FrameworkNew
{
	public class ControlBase : System.Web.UI.UserControl
	{
		private ResourceManager _resourceManager;
		private BasePage _basePage = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
		}

		
		public string GetResourceValue(string key)
		{
			/*
			if (_resourceManager == null)
			{
				Thread.CurrentThread.CurrentCulture = Brand.Site.CultureInfo;
				Thread.CurrentThread.CurrentUICulture = Brand.Site.CultureInfo;

				Type myType = this.GetType();
				string baseName = myType.BaseType.Namespace + "." + myType.Name.Replace("_", ".") + "." + this.Brand.Site.SiteID;
				_resourceManager = new ResourceManager(baseName, myType.BaseType.Assembly);
				_resourceManager.GetResourceSet(Brand.Site.CultureInfo, false, false);
			}

			return _resourceManager.GetString(key);
			*/

			return basePage.GetResourceValue(key);
		}


		private BasePage basePage
		{
			get
			{
				if (_basePage == null)
				{
					_basePage = this.Page as BasePage;
				}
				return _basePage;
			}
		}


		public Matchnet.Content.Translator Translator
		{
			get
			{
				return basePage.Translator;
			}
		}


		public void Transfer(string url)
		{
			try
			{
				HttpContext.Current.Response.Redirect(url);
			}
			catch (ThreadAbortException)
			{
				// f it, yo;
			}
		}

		public UserSession UserSession
		{
			get
			{
				return basePage.UserSession;
			}
		}


		public Brand CurrentBrand
		{
			get
			{
				return basePage.CurrentBrand;
			}
		}


		public Matchnet.Content.ServiceAdapters.Page CurrentPage
		{
			get
			{
				return basePage.CurrentPage;
			}
		}


		public bool IsDevelopmentMode
		{
			get
			{
				//todo:gp
				return true;
			}
		}

		public int ProcessException(Exception ex)
		{
			return basePage.ProcessException(ex);
		}


		public Notifications Notifications
		{
			get
			{
				return basePage.Notifications;
			}
		}


		public string GetDestinationURL()
		{
			return basePage.GetDestinationURL();
		}


		public void DestinationURLRedirect()
		{
			basePage.DestinationURLRedirect();
		}


		public Matchnet.Member.ServiceAdapters.Member CurrentMember
		{
			get
			{
				return basePage.CurrentMember;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
