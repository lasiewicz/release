using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;


namespace Matchnet.Web.FrameworkNew
{
	public class BasePage : System.Web.UI.Page
	{
		private UserSession _userSession;
		private Brand _brand;
		private Matchnet.Content.ServiceAdapters.Page _page;
		private const string SESSIONITEM_BRANDID = "BrandID";
		private Matchnet.Content.Translator _translator;
		private Notifications _notifications;
		private Matchnet.Member.ServiceAdapters.Member _member;

		private void Page_Load(object sender, System.EventArgs e)
		{
			//session
			_userSession = SessionSA.Instance.GetSession(true);


			//brand
			int brandID = _userSession.GetInt(SESSIONITEM_BRANDID, Constants.NULL_INT);
			if (brandID != Constants.NULL_INT)
			{
				_brand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);
			}
			else
			{
				_brand = BrandConfigSA.Instance.GetBrands().GetBrand(new Uri(Request.Url.ToString()).Host);
				_userSession.Add(SESSIONITEM_BRANDID, _brand.BrandID, SessionPropertyLifetime.TemporaryDurable);
			}


			//page
			string pagePath = new UriBuilder("http://foo" + HttpContext.Current.Request.RawUrl).Path;

			if (pagePath.ToLower().Equals("/default.aspx"))
			{
				if (_userSession.GetInt("MemberID") > 0) //todo: shove a constant in here once i can reference the proper assembly
				{
					pagePath = "/Applications/Home/Home.aspx";
				}
				else
				{
					pagePath = "/Applications/Home/Splash.aspx";
				}

			}

			System.Diagnostics.Trace.WriteLine(pagePath);
			_page = PageConfigSA.Instance.GetPage(_brand.Site.SiteID, pagePath);

			if (_page != null)
			{
				LayoutTemplateBase layoutTemplate = (LayoutTemplateBase)LoadControl(_page.GetFullLayoutTemplatePath());

				System.Diagnostics.Trace.WriteLine(_page.GetFullControlPath());
				layoutTemplate.AppControl = (ControlBase)LoadControl(_page.GetFullControlPath());
				this.Controls.Add(layoutTemplate);
			}
			else
			{
				System.Diagnostics.Trace.WriteLine(pagePath);
				//todo:gp real 404
				Response.Write("404, meng");
			}
		}


		public Matchnet.Content.Translator Translator
		{
			get
			{
				if (_translator == null)
				{
					_translator = new Matchnet.Content.Translator(Matchnet.CachingTemp.Cache.GetInstance());
				}

				return _translator;
			}
		}

		public UserSession UserSession
		{
			get
			{
				return _userSession;
			}
		}


		public Brand CurrentBrand
		{
			get
			{
				return _brand;
			}
		}


		public Matchnet.Content.ServiceAdapters.Page CurrentPage
		{
			get
			{
				return _page;
			}
		}


		public int ProcessException(Exception ex)
		{
			//todo:gp
			//return _ContextExceptions.LogException(ex);
			System.Diagnostics.Trace.WriteLine(ex.ToString());
			return 0;
		}


		public string GetResourceValue(string key)
		{
			return GetResourceValue(key, new string[]{}, false);
		}


		public string GetResourceValue(string key, string[] args)
		{
			return GetResourceValue(key, args, false);
		}


		public string GetResourceValue(string key, string[] args, bool replaceImageTokens)
		{
			//todo:gp ?replaceImageTokens
			return Translator.Value(key, CurrentBrand.Site.LanguageID, CurrentBrand.BrandID, args);
		}


		public string GetResourceValue(int resourceID)
		{
			return GetResourceValue(resourceID, new string[]{}, false);
		}


		public string GetResourceValue(int resourceID, string[] args)
		{
			return GetResourceValue(resourceID, args, false);
		}


		public string GetResourceValue(int resourceID, string[] args, bool replaceImageTokens)
		{
			//todo:gp ?replaceImageTokens
			return Translator.Value(resourceID, CurrentBrand.Site.LanguageID, CurrentBrand.BrandID, args);
		}


		public Notifications Notifications
		{
			get
			{
				if (_notifications == null)
				{
					_notifications = new Notifications();
				}

				return _notifications;
			}
		}

		
		public string GetDestinationURL()
		{
			//todo:gp
			return "";
		}


		public void DestinationURLRedirect()
		{
			//todo:gp
		}


		public Matchnet.Member.ServiceAdapters.Member CurrentMember
		{
			get
			{
				if (_member == null && UserSession.GetInt("MemberID") > 0)
				{
					_member = MemberSA.Instance.GetMember(_brand.Site.Community.CommunityID,
						_brand.BrandID,
						_brand.Site.LanguageID,
						_userSession.GetInt("MemberID"),
						MemberLoadFlags.IsSelf);
				}

				return _member;
			}
		}

		
		private void Page_Unload(object sender, EventArgs e)
		{
			SessionSA.Instance.SaveSession(_userSession, Constants.NULL_STRING, true);
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
