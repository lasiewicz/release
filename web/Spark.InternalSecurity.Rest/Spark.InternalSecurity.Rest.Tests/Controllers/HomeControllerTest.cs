﻿using System;
using System.IdentityModel.Protocols.WSTrust;
using System.Security.Claims;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spark.InternalSecurity.Rest;
using Spark.InternalSecurity.Rest.Controllers;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
//using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Tokens;
using Spark.InternalSecurity.ServiceAdapter;

namespace Spark.InternalSecurity.Rest.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {

        public const string SymmetricKey =
          "YQBiAGMAZABlAGYAZwBoAGkAagBrAGwAbQBuAG8AcABxAHIAcwB0AHUAdgB3AHgAeQB6ADAAMQAyADMANAA1AA==";

        [TestMethod]
        public void Index()
        {

            //var res = GetTokenSignedWithSharedKey(null,null,"http://www.example.com");
            var principal = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, "bsmith"),
                new Claim(ClaimTypes.GivenName, "Bob"),
                new Claim(ClaimTypes.Surname, "Smith"),
                new Claim(ClaimTypes.PrimaryGroupSid, "S-1212-23232-1"),
                new Claim(ClaimTypes.GroupSid, "S-233203054-3434-21"),
                new Claim(ClaimTypes.GroupSid, "S-233203054-3434-22")
            });
            var res = JwtHelper.GetTokenSignedWithSharedKey(principal, "test-1", "http://api.spark.net", "Spark Networks", 60000, SymmetricKey);
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }

        private string GetTokenSignedWithSharedKey(ClaimsIdentity principal, string audience, string symmetricKeyToUse = null)
        {

            

            var key = Convert.FromBase64String(SymmetricKey);
            //if (symmetricKeyToUse != null)
            //{
            //    key = Convert.FromBase64String(symmetricKeyToUse);
            //}

            var credentials = new SigningCredentials(
                new InMemorySymmetricSecurityKey(key),
                "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
                "http://www.w3.org/2001/04/xmlenc#sha256");

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, "bsmith"),
                    new Claim(ClaimTypes.GivenName, "Bob"),
                    new Claim(ClaimTypes.Surname, "Smith"),
                    new Claim(ClaimTypes.Role, "Customer Service")
                }),

                //Subject = Subject,
                TokenIssuerName = "corp",
                AppliesToAddress = audience,
                SigningCredentials = credentials,
                Lifetime = new Lifetime(DateTime.Now,DateTime.Now.AddYears(1))
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }

    }
}
