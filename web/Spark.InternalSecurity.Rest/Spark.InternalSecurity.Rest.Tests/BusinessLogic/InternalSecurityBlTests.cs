﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Caching;
using Matchnet.Caching.CacheStrategies;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spark.InternalSecurity.BusinessLogic;
using Spark.InternalSecurity.ValueObjects;

namespace Spark.InternalSecurity.Rest.Tests.BusinessLogic
{
    [TestClass]
    public class InternalSecurityBlTests
    {

        readonly InternalSecurityBl _bl = new InternalSecurityBl(new InMemoryDatabaseFacade(),new CacheStrategy(new NoCacheBasicProvider()));

        [TestMethod]
        public void AppDomainTest()
        {
            var domainId = _bl.GetAppDomainId("test");
            Assert.AreEqual(domainId,1);

            domainId = _bl.GetAppDomainId("non-existing");
            Assert.AreEqual(domainId, 0);
        }

        [TestMethod]
        public void ClaminsHavePrivilegeTest()
        {
            Assert.IsTrue(_bl.ClaimsHavePrivilege(1,new List<string>() {"claim1"},1));
            Assert.IsFalse(_bl.ClaimsHavePrivilege(1, new List<string>() { "claim1" }, 3));
            Assert.IsTrue(_bl.ClaimsHavePrivilege(1, new List<string>() { "claim1" }, 2));
            Assert.IsTrue(_bl.ClaimsHavePrivilege(1, new List<string>() { "claim4" }, 5));
            Assert.IsTrue(_bl.ClaimsHavePrivilege(1, new List<string>() { "claim1" }, 5)); // Role In Role
        }
        [TestMethod]
        public void ClaimsCanDoActionTest()
        {
            Assert.IsTrue(_bl.ClaimsCanDoAction(1, new List<string>() { "claim1" }, new ResourceAction("resource","action1")));
            Assert.IsFalse(_bl.ClaimsCanDoAction(1, new List<string>() { "claim1" }, new ResourceAction("resource", "action3")));
            Assert.IsTrue(_bl.ClaimsCanDoAction(1, new List<string>() { "claim1" }, new ResourceAction("resource", "action2")));
            Assert.IsTrue(_bl.ClaimsCanDoAction(1, new List<string>() { "claim4" }, new ResourceAction("resource", "action5")));
            Assert.IsTrue(_bl.ClaimsCanDoAction(1, new List<string>() { "claim1" }, new ResourceAction("resource", "action5"))); // Role In Role
        }
    }
}
