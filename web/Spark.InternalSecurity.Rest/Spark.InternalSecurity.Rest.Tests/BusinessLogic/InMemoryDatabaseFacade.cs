﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using Matchnet.BaseClasses;
using Spark.InternalSecurity.BusinessLogic;
using Spark.InternalSecurity.ValueObjects;

namespace Spark.InternalSecurity.Rest.Tests.BusinessLogic
{
    class InMemoryDatabaseFacade:IDatabaseFacade
    {
        public InMemoryDatabaseFacade()
        {
            _apiClients = SeedApiClients();
            _rolesFromClaims = SeedRolesFromClaims();
            _childRoles = SeedChildRoles();
            _privileges = SeedPrivileges();
            _rolePrivileges = SeecRolePrivileges();
        }

        private List<ApiClient> _apiClients;

        List<ApiClient> SeedApiClients()
        {
            return new List<ApiClient>()
            {
                new ApiClient() {AppDomainId = 1, Name = "Test Client", ClientId = "test"}
            };

        }

        private List<ItemMapping<string, int>> _rolesFromClaims;
        List<ItemMapping<string, int>> SeedRolesFromClaims()
        {
            return new List<ItemMapping<string, int>>()
            {
                new ItemMapping<string, int>("claim1",1),
                new ItemMapping<string, int>("claim1",2),
                new ItemMapping<string, int>("claim2",2),
                new ItemMapping<string, int>("claim2",3),
                new ItemMapping<string, int>("claim3",4),
                new ItemMapping<string, int>("claim4",2),
                new ItemMapping<string, int>("claim4",5),
                new ItemMapping<string, int>("claim5",6),
            };
        }

        private List<ItemMapping<int, int>> _childRoles;

        List<ItemMapping<int, int>> SeedChildRoles()
        {
            return new List<ItemMapping<int, int>>()
            {
                new ItemMapping<int, int>(1,1),
                new ItemMapping<int, int>(2,2),
                new ItemMapping<int, int>(3,3),
                new ItemMapping<int, int>(4,4),
                new ItemMapping<int, int>(5,5),
                new ItemMapping<int, int>(6,6),

                new ItemMapping<int, int>(1,5),
                new ItemMapping<int, int>(5,6),
            };
        }

        class Privilege
        {
            public int Id { get; set; }
            public string Action { get; set; }
            public string Resource { get; set; }
        }

        private List<Privilege> _privileges;

        List<Privilege> SeedPrivileges()
        {
            return new List<Privilege>()
            {
                new Privilege(){Id = 1,Resource = "resource", Action = "action1"},
                new Privilege(){Id = 2,Resource = "resource", Action = "action2"},
                new Privilege(){Id = 3,Resource = "resource", Action = "action3"},
                new Privilege(){Id = 4,Resource = "resource2", Action = "action3"},
                new Privilege(){Id = 5,Resource = "resource", Action = "action5"},
                new Privilege(){Id = 6,Resource = "resource", Action = "action6"},
            };
        }

        private List<ItemMapping<int, int>> _rolePrivileges;

        List<ItemMapping<int, int>> SeecRolePrivileges()
        {
            return new List<ItemMapping<int, int>>()
            {
                new ItemMapping<int, int>(1,1),
                new ItemMapping<int, int>(2,2),
                new ItemMapping<int, int>(3,3),
                new ItemMapping<int, int>(4,4),
                new ItemMapping<int, int>(5,5),
                new ItemMapping<int, int>(6,6),
            };
        }

        public ApiClient GetApiClientFromDatabase(string clientId)
        {
            return _apiClients.FirstOrDefault(x => x.ClientId == clientId);
        }

        public List<ItemMapping<string, int>> GetRolesFromDatabase(int appDomain, List<string> claims)
        {
            return _rolesFromClaims.Where(x => claims.Contains(x.Key)).ToList();
        }

        public List<ItemMapping<int, int>> GetChildRolesFromDatabase(List<int> roles)
        {
            return _childRoles.Where(x => roles.Contains(x.Key)).ToList();
        }

        public List<ItemMapping<int, ResourceAction>> GetResourceActionsByRolesFromDatabase(List<int> roles)
        {
            var ret = new List<ItemMapping<int, ResourceAction>>();

            foreach (var role in roles)
            {
                var privilegeIds = _rolePrivileges.Where(x => x.Key == role).Select( x=> x.Value).ToList();
                var privileges = _privileges.Where(x => privilegeIds.Contains(x.Id));
                ret.AddRange(
                    privileges.Select(privilege => new ItemMapping<int, ResourceAction>(role, new ResourceAction(privilege.Resource, privilege.Action))));
            }
            return ret;
        }

        public List<ItemMapping<int, int>> GetPrivilegesByRolesFromDatabase(List<int> roles)
        {
            var ret = new List<ItemMapping<int, int>>();

            foreach (var role in roles)
            {
                var privilegeIds = _rolePrivileges.Where(x => x.Key == role).Select(x => x.Value).ToList();
                var privileges = _privileges.Where(x => privilegeIds.Contains(x.Id));
                ret.AddRange(
                    privileges.Select(privilege => new ItemMapping<int, int>(role, privilege.Id)));
            }
            return ret;
        }
    }
}
