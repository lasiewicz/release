﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.InternalSecurity.ValueObjects
{
    public class ResourceAction
    {
        public ResourceAction()
        {
            
        }

        public ResourceAction(string resource, string action)
        {
            Resource = resource;
            Action = action;
        }
        
        public string Resource { get; set; }
        public string Action { get; set; }

        public bool ValueEqual(ResourceAction otherAction)
        {
            return Resource == otherAction.Resource && Action == otherAction.Action;
        }
    }
}
