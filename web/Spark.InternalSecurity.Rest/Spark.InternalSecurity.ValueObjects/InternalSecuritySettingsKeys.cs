﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.InternalSecurity.ValueObjects
{
    public sealed class InternalSecuritySettingsKeys
    {
        public const string CertificateName = "INTERNAL_SECURITY_CERT_NAME";
        public const string CertificateIssuedBy = "INTERNAL_SECURITY_CERT_ISSUED_BY";
        public const string Audience = "INTERNAL_SECURITY_AUDIENCE";
        public const string TokenExpirationInMinutes = "INTERNAL_SECURITY_TOKEN_EXPIRATION_IN_MINUTES";
        public const string SecurityApiRoot = "InternalSecurityEndpointUrl";
        public const string CachExpirationInSeconds = "INTERNAL_SECURITY_CACHE_EXPIRATION_IN_SECONDS";
    }
}
