﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Spark.InternalSecurity.ValueObjects
{
    public class ClaimsKeys
    {
        public const string UserName = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
        public const string UserClaim = "http://schemas.microsoft.com/ws/2008/06/identity/claims/primarysid";
        public const string PrimaryGroupClaim = "http://schemas.microsoft.com/ws/2008/06/identity/claims/primarygroupsid";
        public const string GroupClaim = "http://schemas.microsoft.com/ws/2008/06/identity/claims/groupsid";
        public const string ClientClaim = "http://schemas.spark.net/ws/2015/07/rest/claims/clientid";

    }
}
