﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet;
using Matchnet.BaseClasses;

namespace Spark.InternalSecurity.ValueObjects
{
    public class ApiClient: BaseCacheable
    {
        public ApiClient()
            : base(CacheItemMode.Absolute, CacheItemPriorityLevel.Default, 3600) {}

        public string ClientId { get; set; }
        public string Name { get; set; }
        public int AppDomainId { get; set; }

        public override string GetCacheKey()
        {
            return GetCacheKey(ClientId);
        }

        public static string GetCacheKey(string id)
        {
            return "~ApiClient^" + id;
        }
    }
}
