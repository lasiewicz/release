﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Matchnet.Configuration.ServiceAdapters;
using Spark.InternalSecurity.ValueObjects;

namespace Spark.InternalSecurity.ServiceAdapter
{
    public class JwtHelper
    {
        private readonly static ILog Logger = LogManager.GetLogger("UserAuthorizationController");
        public static string GetTokenSignedWithSharedKey(
            ClaimsIdentity principal, 
            string clientId,
            string audience, 
            string tokenIssuer,
            int expireInMinutes = 60,
            string symmetricKeyToUse = null)
        {
            // The original ClaimsIdentity is protected (read-only), so we need to create a new instance.
            var newClaims = principal.Claims.ToList();
            newClaims.Add(new Claim(ClaimsKeys.ClientClaim,clientId));
            
            var newPrincipal = new ClaimsIdentity(newClaims);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = newPrincipal,
                TokenIssuerName = tokenIssuer,
                AppliesToAddress = audience,
                SigningCredentials = GetCertificateCredentials(),
                Lifetime = new Lifetime(DateTime.Now, DateTime.Now.AddMinutes(expireInMinutes))
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }

        static SigningCredentials GetCertificateCredentials()
        {
             var store = new X509Store(StoreName.My,StoreLocation.LocalMachine);
            try
            {
                store.Open(OpenFlags.ReadOnly);
                var certificateName = RuntimeSettings.GetSetting(InternalSecuritySettingsKeys.CertificateName);
                var certificate = store.Certificates.Find(X509FindType.FindBySubjectName, certificateName, false)[0];
                return new X509SigningCredentials(certificate);

            }
            finally
            {
                store.Close();
            }
  
        }

    }
}
