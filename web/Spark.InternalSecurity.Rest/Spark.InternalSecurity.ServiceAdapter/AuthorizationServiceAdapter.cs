﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Configuration.ServiceAdapters;
using RestSharp;
using Spark.InternalSecurity.ValueObjects;

namespace Spark.InternalSecurity.ServiceAdapter
{
    public class AuthorizationServiceAdapter
    {
        // No singleton        
        private ClaimsIdentity _claimsIdentity;
        private string _clientId;

        public AuthorizationServiceAdapter(ClaimsIdentity claimsIdentity, string clientId)
        {
            _claimsIdentity = claimsIdentity;
            _clientId = clientId;
            
        }

        string GetSecurityBaseUrl()
        {
            return ConfigurationManager.AppSettings[InternalSecuritySettingsKeys.SecurityApiRoot];
        }

        public string GetJwtToken()
        {
            var expireInMinutes = GetExpirationInMinutes();
            var certificateName = RuntimeSettings.GetSetting(InternalSecuritySettingsKeys.CertificateName);
            var issuer = RuntimeSettings.GetSetting(InternalSecuritySettingsKeys.CertificateIssuedBy);
            var audience = RuntimeSettings.GetSetting(InternalSecuritySettingsKeys.Audience);

            return JwtHelper.GetTokenSignedWithSharedKey(_claimsIdentity, _clientId, audience, issuer, expireInMinutes, certificateName);
        }

        int GetExpirationInMinutes()
        {
            var expireInMinutesStr = RuntimeSettings.GetSetting(InternalSecuritySettingsKeys.CachExpirationInSeconds);
            var expireInMinutes = 0;
            int.TryParse(expireInMinutesStr, out expireInMinutes);
            return expireInMinutes/60;
        }

        public bool HasPrivilege(int privilegeId)
        {
            var restClient = new RestClient(GetSecurityBaseUrl());
            var restRequest = new RestRequest("CurrentUser/Authorization/Privilege/{privilegeId}/Exists", Method.GET);
            restRequest.AddHeader("Authorization", "Bearer " + GetJwtToken());
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddParameter("privilegeId", privilegeId, ParameterType.UrlSegment);
            var response = restClient.Execute(restRequest);
            if (response.StatusCode != HttpStatusCode.OK)
                return false;
            bool res;
            bool.TryParse(response.Content, out res);
            return res;
        }

        public bool CanDoAction(string resource, string action)
        {
            var restClient = new RestClient(GetSecurityBaseUrl());
            var restRequest = new RestRequest("CurrentUser/Authorization/Resource/{resource}/Action/{action}", Method.GET);
            restRequest.AddHeader("Authorization", "Bearer " + GetJwtToken());
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddParameter("resource", resource, ParameterType.UrlSegment);
            restRequest.AddParameter("action", action, ParameterType.UrlSegment);
            var response = restClient.Execute(restRequest);
            if (response.StatusCode != HttpStatusCode.OK)
                return false;
            bool res;
            bool.TryParse(response.Content, out res);
            return res;
        }

    }
}
