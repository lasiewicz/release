﻿using System.Web;
using System.Web.Mvc;

namespace Spark.InternalSecurity.Rest
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
