﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using JwtAuthForWebAPI;
using log4net;
using Matchnet.Configuration.ServiceAdapters;
using Spark.InternalSecurity.ValueObjects;
using WebGrease.Activities;

namespace Spark.InternalSecurity.Rest
{
    public static class WebApiConfig
    {
        private readonly static ILog Logger = LogManager.GetLogger("WebApiConfig.Register");
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var builder = new SecurityTokenBuilder();
            
            var configReader = new ConfigurationReader();

            log4net.Config.DOMConfigurator.Configure(); 
            Logger.Info("AppStarted");

            var certificateName = RuntimeSettings.GetSetting(InternalSecuritySettingsKeys.CertificateName);
            var issuer = RuntimeSettings.GetSetting(InternalSecuritySettingsKeys.CertificateIssuedBy);
            var audience = RuntimeSettings.GetSetting(InternalSecuritySettingsKeys.Audience);
            
           

            var jwtHandler = new JwtAuthenticationMessageHandler
            {
                AllowedAudience = audience,
                Issuer = issuer,
                SigningToken = builder.CreateFromCertificate("CN=" + certificateName)
            };
            config.MessageHandlers.Add(jwtHandler);
        }
    }
}
