﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.InternalSecurity.Rest.InternalObjects
{
    class ParsedClaimsIdentity
    {
        public ParsedClaimsIdentity(string clientId, List<string> groups)
        {
            ClientId = clientId;
            Groups = groups;
        }

        public string ClientId { get; private set; }
        public List<string> Groups { get; private set; }
    }
}