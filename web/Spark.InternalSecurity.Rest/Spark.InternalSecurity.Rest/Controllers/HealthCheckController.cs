﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web;

namespace Spark.InternalSecurity.Rest.Controllers
{
    public class HealthCheckController : ApiController
    {
        private const string MatchnetServerDown = "Matchnet Server Down";
        private const string MatchnetServerEnabled = "Matchnet Server Enabled";

        [Route("HealthCheck")]
        public HttpResponseMessage Get()
        {
            var serverStatus = MatchnetServerDown;

            try
            {
                var filePath = Path.GetDirectoryName(System.Web.Hosting.HostingEnvironment.MapPath("~/")) + @"\deploy.txt";
                var fileExists = System.IO.File.Exists(filePath);

                if (!fileExists)
                {
                    serverStatus = MatchnetServerEnabled;
                }
            }
            catch
            {
                serverStatus = MatchnetServerDown;
            }
            finally
            {
                HttpContext.Current.Response.AppendHeader("Health", serverStatus);
            }
            return new HttpResponseMessage() {Content = new StringContent(serverStatus)};
        }
    }
}