﻿
using System;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using log4net;
using Spark.InternalSecurity.BusinessLogic;
using Spark.InternalSecurity.Rest.Filters;
using Spark.InternalSecurity.Rest.Helpers;
using Spark.InternalSecurity.ValueObjects;


namespace Spark.InternalSecurity.Rest.Controllers
{
    //[RequireHttps]
    [RoutePrefix("CurrentUser/Authorization")]
    public class UserAuthorizationController : ApiController
    {
        private readonly static ILog Logger = LogManager.GetLogger("UserAuthorizationController");
        [HttpGet]
        [Authorize]
        [Route("Privilege/{privilegeId}/Exists")]
        public bool HasPrivilege(int privilegeId)
        {
            try
            {
                var identity = ClaimsHelper.ParseClaimsIdentity((ClaimsIdentity) HttpContext.Current.User.Identity);
                var appDomain = InternalSecurityBl.Instance.GetAppDomainId(identity.ClientId);
                var ret = InternalSecurityBl.Instance.ClaimsHavePrivilege(appDomain, identity.Groups, privilegeId);
                Logger.Debug(string.Format("HasPrivilege privilegeId:{0} result: {1}", privilegeId, ret));
                return ret;
            }
            catch (Exception e)
            {
                Logger.Error("Exception: " + e.Message, e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        [Route("Resource/{resource}/Action/{resourceAction}")]
        public bool CanDoAction(string resource, string resourceAction)
        {
            try
            {
                Logger.Debug("CanDoAction is called.");
                var identity = ClaimsHelper.ParseClaimsIdentity((ClaimsIdentity)HttpContext.Current.User.Identity);
                var appDomain = InternalSecurityBl.Instance.GetAppDomainId(identity.ClientId);
                var ret = InternalSecurityBl.Instance.ClaimsCanDoAction(appDomain, identity.Groups, new ResourceAction(resource, resourceAction));
                Logger.Debug(string.Format("CanDoAction resource:{0} action: {1} result:{2}", resource,resourceAction, ret));
                return ret;
            }
            catch (Exception e)
            {
                Logger.Error("Exception: " + e.Message, e);
                throw;
            }
        }

    }
}
