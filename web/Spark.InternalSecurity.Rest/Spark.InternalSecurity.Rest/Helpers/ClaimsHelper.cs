﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Spark.InternalSecurity.Rest.InternalObjects;
using Spark.InternalSecurity.ValueObjects;

namespace Spark.InternalSecurity.Rest.Helpers
{
    static class ClaimsHelper
    {
        public static ParsedClaimsIdentity ParseClaimsIdentity(ClaimsIdentity claimsIdentity)
        {
            var groups = new List<string>();
            var clientId = string.Empty;
            foreach (var claim in claimsIdentity.Claims)
            {
                if(claim.Type == ClaimsKeys.PrimaryGroupClaim || claim.Type == ClaimsKeys.GroupClaim)
                    groups.Add(claim.Value);
                else if (claim.Type == ClaimsKeys.ClientClaim)
                    clientId = claim.Value;
            }
            return new ParsedClaimsIdentity(clientId,groups);
        }
    }
}