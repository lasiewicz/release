﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.BaseClasses;
using Matchnet.Configuration.ServiceAdapters;
using Spark.InternalSecurity.ValueObjects;

namespace Spark.InternalSecurity.BusinessLogic.InternalObjects
{
    class Role2ResourceActionsMapping:CacheableListMapping<int,ResourceAction>
    {
        public Role2ResourceActionsMapping(int key, List<ResourceAction> value)
            : base(key, value)
        {
            CacheTTLSeconds = int.Parse(RuntimeSettings.GetSetting(InternalSecuritySettingsKeys.CachExpirationInSeconds));
        }

        public Role2ResourceActionsMapping(int key, List<ResourceAction> value, int cacheTtlSeconds)
            : base(key, value, cacheTtlSeconds: cacheTtlSeconds)
        {
            
        }

        protected override string GetCacheKeyFromId(int id)
        {
            return GetCacheKey(id);
        }

        public static string GetCacheKey(int id)
        {
            return "~Role2ResourceActionsMapping^" + id;
        }

    }
}
