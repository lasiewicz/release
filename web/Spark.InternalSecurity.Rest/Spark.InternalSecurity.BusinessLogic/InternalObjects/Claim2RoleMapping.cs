﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.InternalSecurity.BusinessLogic.InternalObjects
{
    class Claim2RoleMapping
    {
        public string ObjectSid { get; set; }
        public int RoleId { get; set; }
    }
}
