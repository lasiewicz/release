﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.InternalSecurity.BusinessLogic.InternalObjects
{
    class Role2PrivilegeMapping
    {
        public int RoleId { get; set; }
        public int PrivilegeId { get; set; }
    }
}
