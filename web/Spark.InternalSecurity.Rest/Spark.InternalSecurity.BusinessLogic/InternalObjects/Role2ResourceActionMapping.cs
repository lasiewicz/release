﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spark.InternalSecurity.ValueObjects;

namespace Spark.InternalSecurity.BusinessLogic.InternalObjects
{
    class Role2ResourceActionMapping
    {
        public int RoleId { get; set; }
        public ResourceAction ResourceAction { get; set; }
    }
}
