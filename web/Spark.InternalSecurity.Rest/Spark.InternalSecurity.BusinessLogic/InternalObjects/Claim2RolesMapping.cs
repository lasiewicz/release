﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.BaseClasses;
using Matchnet.Configuration.ServiceAdapters;
using Spark.InternalSecurity.ValueObjects;

namespace Spark.InternalSecurity.BusinessLogic.InternalObjects
{
    class Claim2RolesMapping:CacheableListMapping<string,int>
    {
        public Claim2RolesMapping(string key, List<int> value) 
            : base(key, value)
        {
            CacheTTLSeconds = int.Parse(RuntimeSettings.GetSetting(InternalSecuritySettingsKeys.CachExpirationInSeconds));
        }

        public Claim2RolesMapping(string key, List<int> value, int cacheTtlSeconds)
            : base(key, value, cacheTtlSeconds: cacheTtlSeconds)
        {
            
        }

        protected override string GetCacheKeyFromId(string id)
        {
            return GetCacheKey(id);
        }

        public static string GetCacheKey(string id)
        {
            return "~Claim2RolesMapping^" + id;
        }

    }
}
