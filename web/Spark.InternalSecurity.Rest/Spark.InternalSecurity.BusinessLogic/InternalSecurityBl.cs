﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Matchnet.BaseClasses;
using Matchnet.Caching;
using Matchnet.Caching.CacheStrategies;
using Matchnet.Data;
using Spark.InternalSecurity.BusinessLogic.InternalObjects;
using Spark.InternalSecurity.ValueObjects;

namespace Spark.InternalSecurity.BusinessLogic
{
    public class InternalSecurityBl
    {
        /// <summary>
        /// Default constructor, is used in singleton. 
        /// Binds to default implementation of IDatabaseFacade.
        /// </summary>
        public InternalSecurityBl()
        {
            _databaseFacade = DatabaseFacade.Instance;
            _cache = new CacheStrategy(new MatchnetCacheBasicProvider());
        }

        /// <summary>
        /// The injection is used for testing.
        /// </summary>
        /// <param name="databaseFacade"></param>
        public InternalSecurityBl(
            IDatabaseFacade  databaseFacade,
            CacheStrategy cacheStrategy

            )
        {
            _databaseFacade = databaseFacade;
            _cache = cacheStrategy;
        }

        readonly IDatabaseFacade _databaseFacade;
        private readonly CacheStrategy _cache;
        //private readonly GetValuesByKeys<TKey, TValue> sss;         


        public static InternalSecurityBl Instance  = new InternalSecurityBl();

        private delegate List<TValue> GetValuesByKeys<TKey, TValue>(List<TKey> keys,
            Func<List<TKey>, List<ItemMapping<TKey, TValue>>> dbFetcher,
            Func<TKey, List<TValue>, CacheableListMapping<TKey, TValue>> mappingInfoFactory,
            Func<TKey, string> sourceKeyProvider
            );


        public bool ClaimsHavePrivilege(int appDomain, List<string> claims, int privilege)
        {
            var roles = GetRolesByClaims(appDomain, claims); 
            var flatRoles = FlattenRoles(roles);
            return RolesHavePrivileges(flatRoles, privilege);
        }

        public bool ClaimsCanDoAction(int appDomain, List<string> claims, ResourceAction action)
        {
            var roles = GetRolesByClaims(appDomain, claims); 
            var flatRoles = FlattenRoles(roles);
            return RolesCanDoActions(flatRoles, action);
        }

        public int GetAppDomainId(string clientId)
        {
            var client =  _cache.GetValueByKey(clientId,
                value => _databaseFacade.GetApiClientFromDatabase(value),
                ApiClient.GetCacheKey
                );
            if (client == null)
                return 0;
            return client.AppDomainId;
        }

        bool RolesHavePrivileges(List<int> roles, int privilege)
        {
            return _cache.GetValuesByKeys(roles,
                value => _databaseFacade.GetPrivilegesByRolesFromDatabase(value),
                (key, value) => new Role2PrivilegesMapping(key, value),
                Role2PrivilegesMapping.GetCacheKey)
                .Exists(x => x == privilege);
        }

        bool RolesCanDoActions(List<int> roles, ResourceAction action)
        {
            return _cache.GetValuesByKeys(roles,
                value => _databaseFacade.GetResourceActionsByRolesFromDatabase(value),
                (key, value) => new Role2ResourceActionsMapping(key, value),
                 Role2ResourceActionsMapping.GetCacheKey)
                .Exists(value => value != null && value.ValueEqual(action));
        }

        private List<int> GetRolesByClaims(int appDomain, List<string> claims)
        {
            return _cache.GetValuesByKeys(claims,
                value => _databaseFacade.GetRolesFromDatabase(appDomain, value),
                (key, value) => new Claim2RolesMapping(key, value),
                Claim2RolesMapping.GetCacheKey);
        }

        private List<int> FlattenRoles(List<int> roles)
        {
            return _cache.GetValuesByKeys(roles,
                value => _databaseFacade.GetChildRolesFromDatabase(value),
                (key, value) => new Role2RolesMapping(key, value),
                Role2RolesMapping.GetCacheKey);
        }

    }
}
