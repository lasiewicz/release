﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.BaseClasses;
using Matchnet.Data;
using Spark.InternalSecurity.BusinessLogic.InternalObjects;
using Spark.InternalSecurity.ValueObjects;

namespace Spark.InternalSecurity.BusinessLogic
{
    class DatabaseFacade : IDatabaseFacade
    {
        public static DatabaseFacade Instance = new DatabaseFacade();

        public ApiClient GetApiClientFromDatabase(string clientId)
        {
            var command = new Command("mnInternalSecurity", "dbo.up_Client_Get", 0);
            command.AddParameter("@ClientID", SqlDbType.VarChar, ParameterDirection.Input, clientId);

            using (var reader = Client.Instance.ExecuteReader(command))
            {
                ApiClient ret = null;
                while (reader.Read())
                {
                    ret= new ApiClient() {ClientId =  reader.GetString(0),Name = reader.GetString(1), AppDomainId = reader.GetInt32(2)};
                }
                return ret;
            }
        }


        public List<ItemMapping<string,int>> GetRolesFromDatabase(int appDomain, List<string> claims)
        {
            var command = new Command("mnInternalSecurity", "dbo.up_Get_Roles_By_Claims", 0);
            command.AddParameter("@AppDomainID", SqlDbType.Int, ParameterDirection.Input, appDomain);
            command.AddParameter("@Claims", SqlDbType.Structured, ParameterDirection.Input, GetTable(claims,"ClaimID"));

            using (var reader = Client.Instance.ExecuteReader(command))
            {
                var ret = new List<ItemMapping<string, int>>();
                while(reader.Read())
                {
                    ret.Add(new ItemMapping<string, int>(reader.GetString(0), reader.GetInt32(1)));
                }
                return ret;
            }
        }

        public List<ItemMapping<int,int>> GetChildRolesFromDatabase(List<int> roles)
        {
            var command = new Command("mnInternalSecurity", "dbo.up_Get_Flat_Roles", 0);
            command.AddParameter("@RootRoleIDs", SqlDbType.Structured, ParameterDirection.Input, GetTable(roles, "RoleID"));

            using (var reader = Client.Instance.ExecuteReader(command))
            {
                var ret = new List<ItemMapping<int, int>>();
                while (reader.Read())
                {
                    ret.Add(new ItemMapping<int, int>(reader.GetInt32(0), reader.GetInt32(1) ));
                }
                return ret;
            }
        }

        public List<ItemMapping<int,ResourceAction>> GetResourceActionsByRolesFromDatabase(List<int> roles)
        {
            var command = new Command("mnInternalSecurity", "dbo.up_Get_Resource_Actions_By_Roles", 0);
            command.AddParameter("@RootRoleIDs", SqlDbType.Structured, ParameterDirection.Input, GetTable(roles, "RoleID"));

            using (var reader = Client.Instance.ExecuteReader(command))
            {
                var ret = new List<ItemMapping<int, ResourceAction>>();
                while (reader.Read())
                {
                    ret.Add(new ItemMapping<int, ResourceAction>(
                        reader.GetInt32(0),
                        new ResourceAction(reader.GetString(1), reader.GetString(2))  
                    ));
                }
                return ret;
            }            
        }

        public List<ItemMapping<int,int>> GetPrivilegesByRolesFromDatabase(List<int> roles)
        {
            var command = new Command("mnInternalSecurity", "dbo.up_Get_Privileges_By_Roles", 0);
            command.AddParameter("@RootRoleIDs", SqlDbType.Structured, ParameterDirection.Input, GetTable(roles, "RoleID"));

            using (var reader = Client.Instance.ExecuteReader(command))
            {
                var ret = new List<ItemMapping<int, int>>();
                while (reader.Read())
                {
                    ret.Add(new ItemMapping<int, int>(reader.GetInt32(0), reader.GetInt32(1) ));
                }
                return ret;
            }
        }

        private DataTable GetTable<T>(List<T> items, string keyName)
        {
            var ret = new DataTable();
            ret.Columns.Add(keyName, typeof(T));
            foreach (var role in items)
            {
                ret.Rows.Add(role);
            }
            return ret;
        }


    }
}
