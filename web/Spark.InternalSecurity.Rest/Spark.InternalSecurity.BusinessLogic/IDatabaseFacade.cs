﻿using System.Collections.Generic;
using Matchnet.BaseClasses;
using Spark.InternalSecurity.ValueObjects;

namespace Spark.InternalSecurity.BusinessLogic
{
    public interface IDatabaseFacade
    {
        ApiClient GetApiClientFromDatabase(string clientId);
        List<ItemMapping<string,int>> GetRolesFromDatabase(int appDomain, List<string> claims);
        List<ItemMapping<int,int>> GetChildRolesFromDatabase(List<int> roles);
        List<ItemMapping<int,ResourceAction>> GetResourceActionsByRolesFromDatabase(List<int> roles);
        List<ItemMapping<int,int>> GetPrivilegesByRolesFromDatabase(List<int> roles);
    }
}