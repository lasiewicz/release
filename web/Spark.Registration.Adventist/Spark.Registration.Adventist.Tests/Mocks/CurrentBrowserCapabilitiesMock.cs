﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Web;

namespace Spark.Registration.Adventist.Tests.Mocks
{
    public class CurrentBrowserCapabilitiesMock : ICurrentBrowserCapabilities
    {
        #region ICurrentBrowserCapabilities Members
        private readonly Dictionary<string, string> _properties = new Dictionary<string, string>();

        public string Platform { get; set; }

        public int MajorVersion { get; set; }
        
        public bool IsMobileDevice { get; set; }
        
        public string MobileDeviceManufacturer { get; set; }
        
        public string MobileDeviceModel { get; set; }
        
        public int ScreenPixelsHeight { get; set; }
        
        public int ScreenPixelsWidth { get; set; }
        
        public string this[string index]
        {
            get 
            { 
                if(_properties.Keys.Contains(index.ToLower()))
                {
                    return _properties[index];
                }

                return null;
            }

            set
            {
                if(_properties.Keys.Contains(index.ToLower()))
                {
                    _properties[index.ToLower()] = value;
                }
                else
                {
                    _properties.Add(index.ToLower(), value);
                }
            }
        }

        #endregion
    }
}
