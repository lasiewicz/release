﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Authentication.OAuth.V2;
using Spark.Common.RestConsumer.V2.Models.OAuth2;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Interfaces;

namespace Spark.Registration.Adventist.Tests.Mocks
{
    public class LoginManagerMock: ILoginManager
    {

        #region ILoginManager Members

        public OAuthTokens FetchTokensAndSaveToCookies(string email, string password, bool isPasswordEncrypted, bool useRememberMeExpirationTime, string oauthCookieDomain, int brandId)
        {
            return new OAuthTokens { Success = true };
        }

        public OAuthTokens GetTokensFromCookies()
        {
            return new OAuthTokens { Success = true };
        }

        public OAuthTokens GetTokensViaPassword(string email, string password, bool isPasswordEncrypted, int brandId)
        {
            return new OAuthTokens { Success = true };
        }

        public OAuthTokens GetTokensViaRefresh(int memberId, string refreshToken, int brandId)
        {
            return new OAuthTokens { Success = true };
        }

        public void RemoveTokenCookies(string oauthCookieDomain)
        {
            
        }

        public void SaveTokensToCookies(System.Web.HttpResponse response, OAuthTokens tokens, bool useRmemeberMeMinutes, string email, string oauthCookieDomain)
        {
            
        }

        public void SetSubscriberCookie(bool isSubscriber, string oauthCookieDomain)
        {
            
        }

        public bool UserPreviouslyLoggedIn()
        {
            return false;
        }

        public OAuthTokens FetchTokensAndSaveToCookies(string email, string password, bool passwordIsEncrypted, bool useRememberMeExpirationTime, string environment, string brandDomain, int brandId)
        {
            return new OAuthTokens { Success = true };
        }

        public void SaveTokensToCookies(System.Web.HttpResponse response, OAuthTokens tokens, bool useRememeberMeMinutes, string email, string environment, string brandDomain)
        {
            
        }

        #endregion



        public OAuthTokens FetchTokensAndSaveToCookies(string email, string password, bool passwordIsEncrypted, bool useRememberMeExpirationTime, string environment, string brandDomain, int brandId, int applicationId, string clientSecret)
        {
            throw new NotImplementedException();
        }

        public OAuthTokens GetTokensViaPassword(string email, string password, bool passwordIsEncrypted, int brandId, int applicationId, string clientSecret, out Common.RestConsumer.V2.Models.ResponseDetail responseDetail)
        {
            throw new NotImplementedException();
        }

        public OAuthTokens GetTokensViaRefresh(int memberId, string refreshToken, int brandId, int applicationId, string clientSecret)
        {
            throw new NotImplementedException();
        }
    }
}
