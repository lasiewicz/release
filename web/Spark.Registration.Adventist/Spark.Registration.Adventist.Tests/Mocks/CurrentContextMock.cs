﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Web;

namespace Spark.Registration.Adventist.Tests.Mocks
{
    public class CurrentContextMock: ICurrentContext
    {
        #region ICurrentContext Members

        private Dictionary<string, object> _items = new Dictionary<string, object>();
        private bool _isNull;

        public IDictionary Items
        {
            get { return _items; }
        }

        public bool IsNull
        {
            get { return _isNull; }
            set { _isNull = value; }
        }
    
        #endregion

        public void SetItems(Dictionary<string, object> items)
        {
            _items = items;
        }
    }
}
