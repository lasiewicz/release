﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Registration.Adventist.Interfaces;
using System.Collections.Specialized;
using Spark.Registration.Adventist.Interfaces.Persistence;

namespace Spark.Registration.Adventist.Tests.Mocks
{
    public class RegistrationValuePersistenceMock : IRegistrationValuePersistence
    {
        private Dictionary<string, string> _persistedValues = new Dictionary<string, string>();
        private Dictionary<string, string> _persistedAttributes = new Dictionary<string, string>();


        public bool Persisted { get; set; }
        #region ITemporaryPersistence Members

        public string this[string index]
        {
            get
            {
                if (_persistedValues.ContainsKey(index))
                {
                    return _persistedValues[index];
                }
                else if(_persistedAttributes.ContainsKey(index))
                {
                    return _persistedAttributes[index];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string Name
        {
            get { return "mock"; }
        }

        public DateTime Expires
        {
            get { return DateTime.MinValue; }
            set{ }
        }

        public NameValueCollection Values
        {
            get
            {
                NameValueCollection values = new NameValueCollection();

                foreach(var item in _persistedValues)
                {
                    values.Add(item.Key, item.Value);
                }

                return values;
            }
        }

        public bool HasValues
        {
            get { return _persistedValues.Count > 0 || _persistedAttributes.Count > 0; }
        }

        public void Persist()
        {
            Persisted = true;
        }

        public void Clear()
        {
            _persistedValues = new Dictionary<string, string>();
            _persistedAttributes = new Dictionary<string, string>();
        }

        public void ClearRegistrationValues()
        {
            _persistedAttributes = new Dictionary<string, string>();
        }

        public void RemoveValue(string name)
        {
            if(_persistedValues.ContainsKey(name))
            {
                _persistedValues.Remove(name);
            }
            if (_persistedAttributes.ContainsKey(name))
            {
                _persistedAttributes.Remove(name);
            }
        }

        public void AddValue(string name, string value)
        {
            if(_persistedValues.ContainsKey(name))
            {
                _persistedValues[name] = value;
            }
            else
            {
                _persistedValues.Add(name, value);
            }
        }

        public void AddAttributeValue(string name, string value)
        {
            if (_persistedAttributes.ContainsKey(name))
            {
                _persistedAttributes[name] = value;
            }
            else
            {
                _persistedAttributes.Add(name, value);
            }
        }

        public Dictionary<string, string> GetAttributeValues()
        {
            return _persistedAttributes;
        }

        #endregion

    }
}
