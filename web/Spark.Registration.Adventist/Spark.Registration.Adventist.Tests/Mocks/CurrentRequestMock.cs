﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Web;

namespace Spark.Registration.Adventist.Tests.Mocks
{
    public class CurrentRequestMock : ICurrentRequest
    {
        #region ICurrentRequest Members

        private NameValueCollection _qs = new NameValueCollection();
        private NameValueCollection _form = new NameValueCollection();
        private NameValueCollection _main = new NameValueCollection();
        private HttpCookieCollection _cookies  = new HttpCookieCollection();
        private ICurrentBrowserCapabilities _browser;
        private Uri _uri = new Uri("http://www.jdate.com/applications/home/default.aspx");
        private string _rawURL = "http://www.jdate.com/applications/home/default.aspx";
        private string _urlReferrer = "http://www.jdate.com/applications/home/default.aspx";
        private string _clientIP = "192.168.3.1";

        public NameValueCollection QueryString
        {
            get { return _qs; }
        }

        public NameValueCollection Form
        {
            get { return _form; }
        }

        public string this[string index]
        {
            get
            {
                string value = string.Empty;
                if (_main.AllKeys.Contains(index))
                {
                    value = _main[index];
                }
                return value;
            }
        }

        public Uri Url
        {
            get { return _uri; }
            set { _uri = value; }
        }

        public NameValueCollection Params
        {
            get { return _main; }
        }

        public HttpCookieCollection Cookies
        {
            get { return _cookies; }
        }

        public ICurrentBrowserCapabilities Browser
        {
            get { return _browser; }
            set { _browser = value; }
        }

        public string RawUrl
        {
            get { return _rawURL; }
        }

        public string UrlReferrer
        {
            get { return _urlReferrer; }
            set { _urlReferrer = value; }
        }

        public string ClientIP
        {
            get { return _clientIP; }
        }

        public void SetURL(Uri uri)
        {
            _uri = uri;
        }

        public void SetRawURL(string rawURL)
        {
            _rawURL = rawURL;
        }

        public void SetURLReferrer(string urlReferrer)
        {
            _urlReferrer = urlReferrer;
        }

        public void SetClientIP(string setClientIP)
        {
            _clientIP = setClientIP;
        }

        public void AddQSParameter(string name, string value)
        {
            if (!_qs.AllKeys.Contains(name))
            {
                _qs.Add(name, value);
            }
            else
            {
                _qs[name] = value;
            }
            AddRequestParameter(name, value);
        }

        public void AddFormParameter(string name, string value)
        {
            if (!_form.AllKeys.Contains(name))
            {
                _form.Add(name, value);
            }
            else
            {
                _form[name] = value;
            }
            AddRequestParameter(name, value);
        }

        public void AddRequestParameter(string name, string value)
        {
            if (!_main.AllKeys.Contains(name))
            {
                _main.Add(name, value);
            }
            else
            {
                _main[name] = value;
            }
        }

        public void AddCookie(HttpCookie cookie)
        {
             _cookies.Add(cookie);
        }

        #endregion


        public bool IsAjaxRequest
        {
            get { throw new NotImplementedException(); }
        }

        public string UserAgent
        {
            get { throw new NotImplementedException(); }
        }
    }
}
