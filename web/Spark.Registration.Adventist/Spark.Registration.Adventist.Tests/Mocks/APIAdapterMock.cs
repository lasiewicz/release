﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Spark.Common.RestConsumer.V2.Models.Content.BrandConfig;
using Spark.Common.RestConsumer.V2.Models.Content.Promotion;
using Spark.Common.RestConsumer.V2.Models.Content.Region;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Content.RegistrationActivityRecording;
using Spark.Common.RestConsumer.V2.Models.Member;
using Spark.Common.RestConsumer.V2.Models.MingleMigration;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Adapters;

namespace Spark.Registration.Adventist.Tests.Mocks
{
    public class APIAdapterMock : IApiAdapter
    {
        private ExtendedRegisterResult _extendedRegisterResult;
        private List<Scenario> _scenarios = new List<Scenario>();
        private ExtendedRegisterRequest _extendedRegisterRequest;
        private List<string> _pixels;
        private string _randomScenarioName;
        private RegistrationRecaptureInfo _recaptureInfo;
        private int _sessionTrackingId;

        public int CreatedBedrockSessionMemberID { get; private set; }
        public string CreatedBedrockSessionSessionID { get; private set; }

        #region IAPIAdapter Members

        public ExtendedRegisterResult RegisterExtended(ExtendedRegisterRequest register)
        {
            _extendedRegisterRequest = register;
            return _extendedRegisterResult;
        }

        public void RecordRegistrationStart(string registrationSessionID, string formFactor, int applicationID, int scenarioID, int ipAddress)
        {
           
        }

        public void RecordRegistrationStart(string registrationSessionID, string formFactor, int applicationID, int scenarioID, int ipAddress, System.Web.HttpRequestBase httpRequestBase = null)
        {

        }

        public string RecordRegistrationStep(string registrationSessionID, int stepID, Dictionary<string, object> stepDetails, Dictionary<string, string> allRegFields, string emailAddress, string recaptureID)
        {
            return string.Empty;
        }

        public RegistrationRecaptureInfo GetRegistrationRecpatureInfo(string recaptureID)
        {
            return _recaptureInfo;
        }

        public PromotionMappingResult MapMapURLToPromo(string referrer)
        {
            return new PromotionMappingResult();
        }

        public string CreateBedrockSession(int memberId)
        {
            return "randomsessionid";
        }

        public void CreateBedrockSession(int memberId, string sessionId)
        {
            CreatedBedrockSessionMemberID  = memberId;
            CreatedBedrockSessionSessionID = sessionId;
        }

        public int GetMemberForSession(string sessionId)
        {
            return 0;
        }

        public List<Country> GetCountries()
        {
            return new List<Country>{new Country{Id=1, Description="USA" } };
        }

        public List<Region> GetRegionsForZip(string zipCode)
        {
            return new List<Region> { new Region { Id = 1, Description = "USA" } };
        }

        public List<State> GetStatesForCountry(int countryRegionId)
        {
            return new List<State> { new State { Id = 1, Description = "Michigan" } };
        }

        public List<City> GetCitiesForCountryState(int countryRegionId, int stateRegionId)
        {
            return new List<City> { new City { Id = 1, Description = "Los Angeles" } };
        }

        public List<Scenario> GetScenarios()
        {
            return _scenarios;
        }

        public string GetRandomScenarioName(DeviceType deviceType)
        {
            if (!string.IsNullOrEmpty(_randomScenarioName))
            {
                return _randomScenarioName;
            }
            
            if(_scenarios.Count > 0)
            {
                return _scenarios[0].Name;
            }

            return "none";
        }

        public int GetMembersOnlineCount()
        {
            return 1;
        }

        public List<string> GetPixels(string memberId, string luggageId, string promotionId, string referralURL, string pageName)
        {
            return _pixels;
        }

        public int CreateSessionTrackingRequest(string sessionId, int promotionId, string luggageId, int bannerId, string clientIP, string referrerURL, 
            string landingPageId, string landingPageTestId)
        {
            return _sessionTrackingId;
        }

        public int CreateSessionTrackingRequest(string sessionId, int promotionId, string luggageId, int bannerId,
                                         string clientIP, string referrerURL)
        {
            return _sessionTrackingId;
        }

        #endregion

        public void SetExtendedRegisterResult(ExtendedRegisterResult result)
        {
            _extendedRegisterResult = result;
        }

        public void SetScenarios(List<Scenario> scenarios)
        {
            _scenarios = scenarios;
        }

        public void SetPixels(List<string> pixels)
        {
            _pixels = pixels;
        }

        public ExtendedRegisterRequest GetExtendedRegisterRequest()
        {
            return _extendedRegisterRequest;
        }
        
        public void SetRandomScenarioName(string randomScenarioName)
        {
            _randomScenarioName = randomScenarioName;
        }

        public void SetRegistrationRecpatureInfo(RegistrationRecaptureInfo recaptureInfo)
        {
            _recaptureInfo = recaptureInfo;
        }

        public void SetSessionTrackingId(int sessionTrackingId)
        {
            _sessionTrackingId = sessionTrackingId;
        }

        public bool ValidateRegistrationAttribute(string attributeName, string attributeValue)
        {
            return true;
        }

        public Brand GetBrand()
        {
            return new Brand { Id = Convert.ToInt32(ConfigurationManager.AppSettings["BrandId"]) };
        }

        public Dictionary<string, object> MapToMingleAttributes(MingleAttributeMappingRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
