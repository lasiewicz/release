﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Adapters;

namespace Spark.Registration.Adventist.Tests.Mocks
{
    public class MOSSessionAdapterMock : IMOSSessionAdapter
    {
        #region IMOSSessionAdapter Members

        public Session CreatedSession { get; private set; }
        public string CreatedSessionID { get; private set; }

        public void CreateSession(Session session, string sessionId)
        {
            CreatedSession = session;
            CreatedSessionID = sessionId;
        }

        #endregion
    }
}
