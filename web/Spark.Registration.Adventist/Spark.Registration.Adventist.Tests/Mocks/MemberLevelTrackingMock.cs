﻿using Spark.Common.MemberLevelTracking;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Tracking;
using Spark.Registration.Adventist.Interfaces.Web;

namespace Spark.Registration.Adventist.Tests.Mocks
{
    public class MemberLevelTrackingMock : IMemberLevelTracking
    {
        private string _os = string.Empty;
        private string _formFactor = string.Empty;
        private bool _isMobile;
        private bool _isTablet;
        private DeviceType _deviceType = DeviceType.Desktop;
        private Application _application = Application.FullSite;
        
        #region IMemberLevelTracking Members

        public string GetOs(ICurrentRequest request)
        {
            return _os;
        }

        public string GetFormFactor(ICurrentRequest request)
        {
            return _formFactor;
        }

        public bool IsMobileDevice(ICurrentRequest request)
        {
            return _isMobile;
        }

        public bool IsTablet(ICurrentRequest request)
        {
            return _isTablet;
        }

        public DeviceType GetDeviceType(ICurrentRequest request, Session userSession)
        {
            return _deviceType;
        }

        public Application GetApplication(ICurrentRequest request, Session userSession)
        {
            return _application;
        }

        #endregion

        public void SetOs(string os)
        {
            _os = os;
        }

        public void SetFormFactor(string formFactor)
        {
            _formFactor = formFactor;
        }

        public void SetIsMobile(bool isMobile)
        {
            _isMobile = isMobile;
        }

        public void SetIsTablet(bool isTablet)
        {
            _isTablet = isTablet;
        }

        public void SetDeviceType(DeviceType deviceType)
        {
            _deviceType = deviceType;
        }

        public void SetApplication(Application application)
        {
            _application = application;
        }

    }
}
