﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Scenarios;

namespace Spark.Registration.Adventist.Tests.Mocks
{
    public class ScenarioLoaderMock: IScenarioLoader

    {
        private List<Scenario> _scenarios = new List<Scenario>();
        private Scenario _currentScenario;
        private string _randomScenarioName = string.Empty;
        
        #region IScenarioLoader Members

        public string GetRandomScenarioName()
        {
            return _randomScenarioName;
        }

        public List<Scenario> GetScenarios()
        {
            return _scenarios;
        }

        public Scenario GetNamedScenario(string name)
        {
            Scenario scenario = (from Scenario s in _scenarios where s.Name.ToLower() == name.ToLower() select s).FirstOrDefault();
            return scenario;
        }

        public Scenario GetCurrentScenario()
        {
            return _currentScenario;
        }

        #endregion

        public void SetScenarios(List<Scenario> scenarios)
        {
            _scenarios = scenarios;
        }

        public void SetCurrentScenario(Scenario scenario)
        {
            _currentScenario = scenario;
        }

        public void SetRandomScenario(string scenarioName)
        {
            _randomScenarioName = scenarioName;
        }
    }
}
