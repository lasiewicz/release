﻿using System.Web;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Web;

namespace Spark.Registration.Adventist.Tests.Mocks
{
    public class CurrentResponseMock: ICurrentResponse
    {
        private readonly HttpCookieCollection _cookies = new HttpCookieCollection();
        
        #region ICurrentResponse Members

        public HttpCookieCollection Cookies
        {
            get { return _cookies; }
        }

        public void AppendCookie(HttpCookie cookie)
        {
           _cookies.Add(cookie);
        }

        public void SetCookie(HttpCookie cookie)
        {
            _cookies.Add(cookie);
        }

        #endregion
    }
}
