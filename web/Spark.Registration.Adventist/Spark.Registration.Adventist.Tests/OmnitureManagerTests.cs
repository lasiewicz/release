﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Framework.Analytics;
using Spark.Registration.Adventist.Managers;
using Spark.Registration.Adventist.Tests.Mocks;
using Spark.Registration.Dictionaries.Constants;

namespace Spark.Registration.Adventist.Tests
{
    public class OmnitureManagerTests
    {
        [Test]
        public void TestGetOmnitureStepModelSpecific()
        {
            var omnitureManager = GetBaseOmnitureManager();

            var stepModel = omnitureManager.GetStepOmnitureModel(1, "myregsessionid");
            Assert.IsNotNull(stepModel, "Null stepmodel");
            Assert.IsNotNull(stepModel.Variables, "Null omniture variables");
            Assert.IsTrue(stepModel.Variables["prop72"] == "Scenario1", "prop72 not set with pagename");
            Assert.IsTrue(stepModel.Variables["prop73"] == "1", "prop73 not set with step number");
            Assert.IsTrue(stepModel.Variables["prop74"] == "myregsessionid", "prop74 not set with sessionid");
        }

        [Test]
        public void TestRegStartEventFiredCorrectly()
        {
            var omnitureManager = GetBaseOmnitureManager();

            var stepModel = omnitureManager.GetStepOmnitureModel(1, "myregsessionid");
            Assert.IsTrue(stepModel.Variables["events"] == "event7", "Event7 not present");

            omnitureManager = GetBaseOmnitureManager(); 
            stepModel = omnitureManager.GetStepOmnitureModel(2, "myregsessionid");
            Assert.IsTrue(stepModel.Variables["events"] == "", "Event7 present and shouldn't be");
        }

        [Test]
        [ExpectedException]
        public void TestNullScenarioThrowsException()
        {
            var currentContext = new CurrentContextMock();
            currentContext.Items.Add("Omniture", new Omniture());
            var currentRequest = new CurrentRequestMock();
            currentRequest.SetRawURL("/registration");
            currentRequest.SetURL(new Uri("http://www.jdate.com/?prm=123"));
            currentRequest.SetURLReferrer("www.google.com");
            
            var omnitureManager = new OmnitureManager("registration", currentRequest, null)
            {
                UserSession = new Session(),
                Persistence = new RegistrationValuePersistenceMock(),
                CurrentContext = currentContext
            };
        }

        [Test]
        public void TestPagenameAndURLSetCorrectlyForBedrock()
        {
            var omnitureManager = GetBaseOmnitureManager();

            var stepModel = omnitureManager.GetStepOmnitureModel(1, "myregsessionid");
            Assert.IsTrue(stepModel.PageName == "registration", "Pagename not set correctly");
            Assert.IsTrue(stepModel.PageUrl == "/registration", "Page url not set correctly");
        }

        [Test]
        public void TestPagenameAndURLSetCorrectlyForMOS()
        {
            var currentContext = new CurrentContextMock();
            currentContext.Items.Add("Omniture", new Omniture());
            var currentRequest = new CurrentRequestMock();
            currentRequest.SetRawURL("/registration");
            currentRequest.SetURL(new Uri("http://www.jdate.com/?prm=123"));
            currentRequest.SetURLReferrer("www.google.com");
            var scenario = new Scenario { ExternalSessionType = ExternalSessionType.MOS, Name = "Scenario1" };

            var omnitureManager = new OmnitureManager("registration", currentRequest, scenario)
            {
                UserSession = new Session(),
                Persistence = new RegistrationValuePersistenceMock(),
                CurrentContext = currentContext
            };

            var stepModel = omnitureManager.GetStepOmnitureModel(1, "myregsessionid");
            Assert.IsTrue(stepModel.PageName == ConfigurationManager.AppSettings["MOSOmniturePagenamePrefix"] + "registration", "Pagename not set correctly");
            Assert.IsTrue(stepModel.PageUrl == "/registration", "Page url not set correctly");
        }

        [Test]
        public void TestCommonVariables()
        {
            var omnitureManager = GetBaseOmnitureManager();
            omnitureManager.UserSession[SessionConstants.PromotionId] = 55020;
            omnitureManager.UserSession[SessionConstants.EID] = "email";
            omnitureManager.UserSession[SessionConstants.Luggage] = "lug";
            omnitureManager.UserSession[SessionConstants.LandingPageID] = 12345;
            omnitureManager.UserSession[SessionConstants.Refcd] = "ref";
            omnitureManager.UserSession[SessionConstants.TSACR] = "tsacr";

            var stepModel = omnitureManager.GetStepOmnitureModel(1, "myregsessionid");

            Assert.IsTrue(stepModel.Variables["prop10"] == "/?prm=123");
            Assert.IsTrue(stepModel.Variables["prop29"] == "www.google.com");
            Assert.IsTrue(stepModel.Variables["eVar48"] == "Scenario1");
            Assert.IsTrue(stepModel.Variables["eVar11"] == "55020");
            Assert.IsTrue(stepModel.Variables["campaign"] == "55020");
            Assert.IsTrue(stepModel.Variables["eVar12"] == "email");
            Assert.IsTrue(stepModel.Variables["eVar13"] == "lug");
            Assert.IsTrue(stepModel.Variables["eVar30"] == "12345");
            Assert.IsTrue(stepModel.Variables["eVar38"] == "ref");
            Assert.IsTrue(stepModel.Variables["eVar39"] == "tsacr");
        }

        [Test]
        public void TestPageModel()
        {
            var omnitureManager = GetBaseOmnitureManager();
            omnitureManager.UserSession.MemberId = 123456;
            omnitureManager.UserSession[SessionConstants.MemberGendermask] = 1;
            omnitureManager.UserSession[SessionConstants.MemberBirthdate] = DateTime.Now.AddYears(-25).ToString();
            omnitureManager.UserSession[SessionConstants.MemberRegionZipCode] = "90028";

            var pageModel = omnitureManager.GetOmnitureModel();
            Assert.IsTrue(pageModel.Variables["prop18"] == "Male");
            Assert.IsTrue(pageModel.Variables["prop19"] == "25");
            Assert.IsTrue(pageModel.Variables["prop21"] == "90028");
            Assert.IsTrue(pageModel.Variables["prop22"] == "Registered");
            Assert.IsTrue(pageModel.Variables["prop23"] == "123456");

        }

        private OmnitureManager GetBaseOmnitureManager()
        {
            var currentContext = new CurrentContextMock();
            currentContext.Items.Add("Omniture", new Omniture());
            var currentRequest = new CurrentRequestMock();
            currentRequest.SetRawURL("/registration");
            currentRequest.SetURL(new Uri("http://www.jdate.com/?prm=123"));
            currentRequest.SetURLReferrer("www.google.com");
            var scenario = new Scenario { ExternalSessionType = ExternalSessionType.Bedrock, Name = "Scenario1" };

            var omnitureManager = new OmnitureManager("registration", currentRequest, scenario)
            {
                UserSession = new Session(),
                Persistence = new RegistrationValuePersistenceMock(),
                CurrentContext = currentContext
            };

            return omnitureManager;
        }
    }
}
