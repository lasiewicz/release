﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Framework.Helpers;
using Spark.Registration.Adventist.Managers;
using Spark.Registration.Adventist.Tests.Mocks;

namespace Spark.Registration.Adventist.Tests
{
    public class ExternalSessionHelperTests
    {
        [Test]
        public void TestCreateExternalSessionBedrock()
        {
            var cookieManager = new CookieManager
                                    {
                                        CurrentRequest = new CurrentRequestMock(),
                                        CurrentResponse = new CurrentResponseMock()
                                    };
            var apiAdapter = new APIAdapterMock();
            var session = new Session();
            session["test"] = "testvalue";
            session.MemberId = 123456;
            var bedrockSessionCookieName = cookieManager.GetBedrockSessionCookieName();

            var externalSessionHelper = new ExternalSessionHelper(cookieManager, apiAdapter);
            externalSessionHelper.CreateExternalSession(ExternalSessionType.Bedrock, session, "sessionid");

            Assert.IsTrue(apiAdapter.CreatedBedrockSessionMemberID == 123456);
            Assert.IsTrue(apiAdapter.CreatedBedrockSessionSessionID == "sessionid");
            Assert.IsNotNull(cookieManager.CurrentResponse.Cookies[bedrockSessionCookieName]);
            Assert.IsNotNull(cookieManager.CurrentResponse.Cookies[bedrockSessionCookieName].Value == "sessionid");
        }

        [Test]
        public void TestCreateExternalSessionMOS()
        {
            var mosSessionAdapterMock = new MOSSessionAdapterMock();
            var cookieManager = new CookieManager
            {
                CurrentRequest = new CurrentRequestMock(),
                CurrentResponse = new CurrentResponseMock()
            };
            var apiAdapter = new APIAdapterMock();
            var session = new Session();
            session["test"] = "testvalue";
            session.MemberId = 123456;

            var externalSessionHelper = new ExternalSessionHelper(cookieManager, apiAdapter);
            externalSessionHelper.MOSSessionAdapter = mosSessionAdapterMock;
            externalSessionHelper.CreateExternalSession(ExternalSessionType.MOS, session, "sessionid");

            Assert.IsNotNull(mosSessionAdapterMock.CreatedSession);
            Assert.IsTrue(mosSessionAdapterMock.CreatedSession["test"].ToString() == "testvalue");
            Assert.IsTrue(mosSessionAdapterMock.CreatedSessionID == "sessionid");
        }
}
}
