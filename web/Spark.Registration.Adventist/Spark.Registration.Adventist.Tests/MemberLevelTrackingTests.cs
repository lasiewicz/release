﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using NUnit.Framework;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Framework.Helpers;
using Spark.Registration.Adventist.Tests.Mocks;
using Spark.Registration.Dictionaries.Constants;

namespace Spark.Registration.Adventist.Tests
{
    public class MemberLevelTrackingTests
    {
        [Test]
        public void TestGetOS()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock {Platform = "Windows", MajorVersion = 8};
            var requestMock = new CurrentRequestMock{Browser = browserMock};

            var memberlevelTracking = new MemberLevelTracking();
            string os = memberlevelTracking.GetOs(requestMock);
            Assert.IsTrue(os == "Windows 8");
        }

        [Test]
        public void TestIsMobile()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock  {IsMobileDevice = true };
            var requestMock = new CurrentRequestMock { Browser = browserMock };

            var memberlevelTracking = new MemberLevelTracking();
            bool isMobile = memberlevelTracking.IsMobileDevice(requestMock);
            Assert.IsTrue(isMobile);

            browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = false };
            requestMock = new CurrentRequestMock { Browser = browserMock };
            isMobile = memberlevelTracking.IsMobileDevice(requestMock);
            Assert.IsFalse(isMobile);
        }

        [Test]
        public void TestIsTablet()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock {IsMobileDevice = true};
            browserMock["is_tablet"] = "true";
            var requestMock = new CurrentRequestMock { Browser = browserMock };

            //tablet property set true
            var memberlevelTracking = new MemberLevelTracking();
            bool isTablet = memberlevelTracking.IsTablet(requestMock);
            Assert.IsTrue(isTablet);

            //tablet property set false
            browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = true };
            browserMock["is_tablet"] = "false";
            requestMock = new CurrentRequestMock { Browser = browserMock };
            isTablet = memberlevelTracking.IsTablet(requestMock);
            Assert.IsFalse(isTablet);

            //tablet property empty
            requestMock = new CurrentRequestMock { Browser = new CurrentBrowserCapabilitiesMock() };
            isTablet = memberlevelTracking.IsTablet(requestMock);
            Assert.IsFalse(isTablet);
        }

        [Test]
        public void TextGetDeviceDesktop()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = false };
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            var session = new Session();

            var memberlevelTracking = new MemberLevelTracking();
            var deviceType = memberlevelTracking.GetDeviceType(requestMock, session);
            Assert.IsTrue(deviceType == DeviceType.Desktop);
        }

        [Test]
        public void TextGetDeviceHandheld()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = true };
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            var session = new Session();

            var memberlevelTracking = new MemberLevelTracking();
            var deviceType = memberlevelTracking.GetDeviceType(requestMock, session);
            Assert.IsTrue(deviceType == DeviceType.Handheld);
        }

        [Test]
        public void TextGetDeviceTablet()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = true };
            browserMock["is_tablet"] = "true";
            var requestMock = new CurrentRequestMock { Browser = browserMock };

            var memberlevelTracking = new MemberLevelTracking();
            var session = new Session();
            var deviceType = memberlevelTracking.GetDeviceType(requestMock, session);
            Assert.IsTrue(deviceType == DeviceType.Tablet);

            browserMock.IsMobileDevice = false;
            requestMock = new CurrentRequestMock { Browser = browserMock };
            memberlevelTracking = new MemberLevelTracking();
            deviceType = memberlevelTracking.GetDeviceType(requestMock, session);
            Assert.IsTrue(deviceType == DeviceType.Tablet);
        }

        [Test]
        public void TestSessionChecked()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = true };
            browserMock["is_tablet"] = "true";
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            var session = new Session();
            session[SessionConstants.DeviceType] = DeviceType.Desktop;

            var memberlevelTracking = new MemberLevelTracking();
            DeviceType deviceType = memberlevelTracking.GetDeviceType(requestMock, session);
            Assert.IsTrue(deviceType == DeviceType.Desktop);
        }

        [Test]
        public void TestQuerystringParameterRespected()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = true };
            browserMock["is_tablet"] = "true";
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            requestMock.QueryString.Add(URLConstants.DeviceType, DeviceType.Desktop.ToString("d"));
            var session = new Session();

            var memberlevelTracking = new MemberLevelTracking();
            DeviceType deviceType = memberlevelTracking.GetDeviceType(requestMock, session);
            Assert.IsTrue(deviceType == DeviceType.Desktop);
        }

        [Test]
        public void TestInvalidQuerystringParameterHandled()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = true };
            browserMock["is_tablet"] = "true";
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            requestMock.QueryString.Add(URLConstants.DeviceType, "6");
            var session = new Session();

            var memberlevelTracking = new MemberLevelTracking();
            DeviceType deviceType = memberlevelTracking.GetDeviceType(requestMock, session);
            Assert.IsTrue(deviceType == DeviceType.Tablet);
        }
        
    }
}
