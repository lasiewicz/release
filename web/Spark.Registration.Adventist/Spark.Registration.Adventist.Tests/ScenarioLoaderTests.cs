﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Framework;
using Spark.Registration.Adventist.Tests.Mocks;
using Spark.Registration.Dictionaries.Constants;

namespace Spark.Registration.Adventist.Tests
{
    public class ScenarioLoaderTests
    {
        [Test]
        public void TestGetScenariosBasic()
        {
            var adapterMock = new APIAdapterMock();
            adapterMock.SetScenarios(GenerateScenariosWithDeviceTypes());

            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            var scenarios = scenarioLoader.GetScenarios();
            Assert.IsTrue(scenarios.Count == 3);
        }
        
        [Test]
        public void TestGetNamedScenarioBasic()
        {
            var adapterMock = new APIAdapterMock();
            adapterMock.SetScenarios(GenerateScenariosWithDeviceTypes());

            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            var scenario = scenarioLoader.GetNamedScenario("scenario1");
            Assert.IsNotNull(scenario);
            Assert.IsTrue(scenario.Name == "scenario1");
        }

        [Test]
        public void TestSingleScenarioOfEachTypeReturned()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = true };
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            var adapterMock = new APIAdapterMock();

            adapterMock.SetScenarios(GenerateScenariosWithDeviceTypes());
            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            scenarioLoader.CurrentRequest = requestMock;
            scenarioLoader.Persistence = new RegistrationValuePersistenceMock();

            //Handheld
            var randomScenario = scenarioLoader.GetRandomScenarioName();
            Assert.IsTrue(randomScenario == "scenario2");

            //Desktop
            (requestMock.Browser as CurrentBrowserCapabilitiesMock).IsMobileDevice = false;
            scenarioLoader.UserSession = new Session();
            randomScenario = scenarioLoader.GetRandomScenarioName();
            Assert.IsTrue(randomScenario == "scenario1");

            //Tablet
            (requestMock.Browser as CurrentBrowserCapabilitiesMock)["is_tablet"] = "true";
            scenarioLoader.UserSession = new Session();
            randomScenario = scenarioLoader.GetRandomScenarioName();
            Assert.IsTrue(randomScenario == "scenario3");
        }

        [Test]
        public void TestMultipleScenariosOfSingleTypeCallsAPIRandom()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = false };
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            var adapterMock = new APIAdapterMock();

            adapterMock.SetScenarios(GenerateThreeDesktopScenarios());
            adapterMock.SetRandomScenarioName("scenario3");

            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            scenarioLoader.CurrentRequest = requestMock;
            scenarioLoader.UserSession = new Session();
            scenarioLoader.Persistence = new RegistrationValuePersistenceMock();

            var randomScenario = scenarioLoader.GetRandomScenarioName();
            Assert.IsTrue(randomScenario == "scenario3");
        }

        [Test]
        public void TestDesktopScenarioReturnedIfNoScenarioExistsForDeviceType()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = true };
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            var adapterMock = new APIAdapterMock();

            adapterMock.SetScenarios(GenerateThreeDesktopScenarios());
            adapterMock.SetRandomScenarioName("scenario3");

            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            scenarioLoader.CurrentRequest = requestMock;
            scenarioLoader.UserSession = new Session();
            scenarioLoader.Persistence = new RegistrationValuePersistenceMock();

            var randomScenario = scenarioLoader.GetRandomScenarioName();
            Assert.IsTrue(randomScenario == "scenario3");
        }

        [Test]
        public void TestControlScenarioReturnedWhenFromLandingPage()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = false };
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            var adapterMock = new APIAdapterMock();

            adapterMock.SetScenarios(GenerateThreeDesktopScenarios());
            adapterMock.SetRandomScenarioName("scenario3");

            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            scenarioLoader.CurrentRequest = requestMock;
            scenarioLoader.UserSession[SessionConstants.RequestFromLandingPage] = true;
            scenarioLoader.Persistence = new RegistrationValuePersistenceMock();
            
            var randomScenario = scenarioLoader.GetRandomScenarioName();
            Assert.IsTrue(randomScenario == "scenario2");
        }

        [Test]
        public void TestScenarioStillReturnedIfNoControlSetButRequested()
        {
            var scenarios = new List<Scenario>();
            scenarios.Add(new Scenario { DeviceType = DeviceType.Desktop, ID = 1, Name = "scenario1" });
            scenarios.Add(new Scenario { DeviceType = DeviceType.Desktop, ID = 2, Name = "scenario2" });
            scenarios.Add(new Scenario { DeviceType = DeviceType.Desktop, ID = 3, Name = "scenario3" });

            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = false };
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            var adapterMock = new APIAdapterMock();

            adapterMock.SetScenarios(scenarios);
            adapterMock.SetRandomScenarioName("scenario3");

            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            scenarioLoader.CurrentRequest = requestMock;
            scenarioLoader.UserSession[SessionConstants.RequestFromLandingPage] = true;
            scenarioLoader.Persistence = new RegistrationValuePersistenceMock();

            var randomScenario = scenarioLoader.GetRandomScenarioName();
            Assert.IsTrue(randomScenario == "scenario3");
        }

        [Test]
        public void TestGetRandomScenarioSetsDeviceTypeSession()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = true };
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            var adapterMock = new APIAdapterMock();

            adapterMock.SetScenarios(GenerateThreeDesktopScenarios());
            adapterMock.SetRandomScenarioName("scenario3");

            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            scenarioLoader.CurrentRequest = requestMock;
            scenarioLoader.Persistence = new RegistrationValuePersistenceMock();

            var randomScenario = scenarioLoader.GetRandomScenarioName();
            Assert.IsTrue( (DeviceType)scenarioLoader.UserSession[SessionConstants.DeviceType] == DeviceType.Handheld);
        }

        [Test]
        public void TestGetCurrentScenarioFromPersistence()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = false };
            var persistenceMock = new RegistrationValuePersistenceMock();
            persistenceMock.AddValue(PersistenceConstants.ScenarioName, "scenario2");
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            var adapterMock = new APIAdapterMock();

            adapterMock.SetScenarios(GenerateThreeDesktopScenarios());

            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            scenarioLoader.CurrentRequest = requestMock;
            scenarioLoader.Persistence = persistenceMock;

            var scenario = scenarioLoader.GetCurrentScenario();
            Assert.IsNotNull(scenario);
            Assert.IsTrue(scenario.Name == "scenario2");
        }

        [Test]
        public void TestGetScenarioFromRequest()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = false };
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            requestMock.AddRequestParameter(URLConstants.ScenarioName, "scenario2");
            var adapterMock = new APIAdapterMock();

            adapterMock.SetScenarios(GenerateThreeDesktopScenarios());

            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            scenarioLoader.CurrentRequest = requestMock;
            scenarioLoader.Persistence = new RegistrationValuePersistenceMock();

            var scenario = scenarioLoader.GetCurrentScenario();
            Assert.IsNotNull(scenario);
            Assert.IsTrue(scenario.Name == "scenario2");
        }

        [Test]
        public void TestInvalidScenarioInRequestAndPersistence()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = false };
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            requestMock.AddRequestParameter(URLConstants.ScenarioName, "scenario6");
            var persistenceMock = new RegistrationValuePersistenceMock();
            persistenceMock.AddValue(PersistenceConstants.ScenarioName, "scenario7");
            var adapterMock = new APIAdapterMock();

            adapterMock.SetScenarios(GenerateThreeDesktopScenarios());
            adapterMock.SetRandomScenarioName("scenario3");

            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            scenarioLoader.CurrentRequest = requestMock;
            scenarioLoader.Persistence = persistenceMock;

            var scenario = scenarioLoader.GetCurrentScenario();
            Assert.IsNotNull(scenario);
            Assert.IsTrue(scenario.Name == "scenario3");
        }

        [Test]
        [ExpectedException]
        public void TestExceptionThrowIfScenarioNotFound()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = false };
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            requestMock.AddRequestParameter(URLConstants.ScenarioName, "scenario6");
            var persistenceMock = new RegistrationValuePersistenceMock();
            persistenceMock.AddValue(PersistenceConstants.ScenarioName, "scenario7");
            var adapterMock = new APIAdapterMock();

            //set random scenario name to one that doesn't exist
            adapterMock.SetScenarios(GenerateThreeDesktopScenarios());
            adapterMock.SetRandomScenarioName("scenario10");

            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            scenarioLoader.CurrentRequest = requestMock;
            scenarioLoader.Persistence = persistenceMock;

            var scenario = scenarioLoader.GetCurrentScenario();
        }

        [Test]
        public void TestScenarioNameSetInPersistence()
        {
            var browserMock = new CurrentBrowserCapabilitiesMock { IsMobileDevice = false };
            var requestMock = new CurrentRequestMock { Browser = browserMock };
            var persistenceMock = new RegistrationValuePersistenceMock();
            var adapterMock = new APIAdapterMock();

            //set random scenario name to one that doesn't exist
            adapterMock.SetScenarios(GenerateThreeDesktopScenarios());
            adapterMock.SetRandomScenarioName("scenario3");

            var scenarioLoader = new APIScenarioLoader(adapterMock, true);
            scenarioLoader.CurrentRequest = requestMock;
            scenarioLoader.Persistence = persistenceMock;

            var scenario = scenarioLoader.GetCurrentScenario();
            Assert.IsTrue(persistenceMock[PersistenceConstants.ScenarioName] == "scenario3");
        }

        private List<Scenario> GenerateScenariosWithDeviceTypes()
        {
            var scenarios = new List<Scenario>();
            scenarios.Add(new Scenario {DeviceType = DeviceType.Desktop, ID = 1, Name = "scenario1"});
            scenarios.Add(new Scenario { DeviceType = DeviceType.Handheld, ID = 2, Name = "scenario2" });
            scenarios.Add(new Scenario { DeviceType = DeviceType.Tablet, ID = 3, Name = "scenario3" });
            
            return scenarios;
        }

        private List<Scenario> GenerateThreeDesktopScenarios()
        {
            var scenarios = new List<Scenario>();
            scenarios.Add(new Scenario { DeviceType = DeviceType.Desktop, ID = 1, Name = "scenario1"});
            scenarios.Add(new Scenario { DeviceType = DeviceType.Desktop, ID = 2, Name = "scenario2", IsControl=true });
            scenarios.Add(new Scenario { DeviceType = DeviceType.Desktop, ID = 3, Name = "scenario3" });

            return scenarios;
        }
    }
}
