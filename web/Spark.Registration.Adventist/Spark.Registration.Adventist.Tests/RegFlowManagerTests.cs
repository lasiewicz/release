﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.Authentication.OAuth.V2;
using Spark.Common.RestConsumer.V2.Models.Content.AttributeData;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Member;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Framework;
using Spark.Registration.Adventist.Framework.HTTPContextWrappers;
using Spark.Registration.Adventist.Framework.Helpers;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Adapters;
using Spark.Registration.Adventist.Interfaces.Persistence;
using Spark.Registration.Adventist.Interfaces.Scenarios;
using Spark.Registration.Adventist.Interfaces.Tracking;
using Spark.Registration.Adventist.Interfaces.Web;
using Spark.Registration.Adventist.Managers;
using Spark.Registration.Adventist.Models;
using Spark.Registration.Adventist.Tests.Mocks;
using Spark.Registration.Adventist.Framework.Analytics;
using Spark.Registration.Dictionaries.Constants;

namespace Spark.Registration.Adventist.Tests
{
    [TestFixture]
    public class RegFlowManagerTests
    {
        private List<Scenario> _scenarios;
        private IRegistrationValuePersistence _persistence;
        private ICurrentRequest _request;
        private ICurrentResponse _response;
        private Session _session;
        private IApiAdapter _apiAdapter;
        private ICurrentContext _context;
        private IScenarioLoader _scenarioLoader;
        private IMemberLevelTracking _memberLevelTracking;
        private ILoginManager _loginManager;
        private RegistrationFlowManager _flowManager;
        
        [SetUp]
        public void Setup()
        {
            _scenarios = GetScenarios();
            _persistence = new RegistrationValuePersistenceMock();
            _request = new CurrentRequestMock();
            _response = new CurrentResponseMock();
            _context = new CurrentContextMock();
            _session = new Session();
            _apiAdapter = new APIAdapterMock();
            _scenarioLoader = new ScenarioLoaderMock();
            _memberLevelTracking = new MemberLevelTrackingMock();
            _loginManager = new LoginManagerMock();

            (_apiAdapter as APIAdapterMock).SetScenarios(_scenarios);
            (_scenarioLoader as ScenarioLoaderMock).SetScenarios(_scenarios);
            (_scenarioLoader as ScenarioLoaderMock).SetCurrentScenario(_scenarios[0]);
            (_scenarioLoader as ScenarioLoaderMock).SetRandomScenario(_scenarios[1].Name);

            (_request as CurrentRequestMock).Url = new Uri("http://reg.local.jdate.com/Registration");

            _context.Items.Add("UserSession", _session);
            _context.Items.Add("Omniture", new Omniture());


            var omnitureManager = new OmnitureManager("Registration", _request, _scenarios[0]);
            omnitureManager.CurrentContext = _context;
            omnitureManager.CurrentRequest = _request;
            omnitureManager.CurrentResponse = _response;
            omnitureManager.Persistence = _persistence;
            omnitureManager.UserSession = _session;

            var cookieManager = new CookieManager();
            cookieManager.CurrentContext = _context;
            cookieManager.CurrentRequest = _request;
            cookieManager.CurrentResponse = _response;
            cookieManager.Persistence = _persistence;
            cookieManager.UserSession = _session;

            _flowManager = new RegistrationFlowManager(_scenarioLoader, _request);

            _flowManager.OmnitureManagerInstance = omnitureManager;
            _flowManager.CookieManagerInstance = cookieManager;
            _flowManager.ApiAdapter = _apiAdapter;
            _flowManager.CurrentContext = _context;
            _flowManager.CurrentRequest = _request;
            _flowManager.CurrentResponse = _response;
            _flowManager.MemberLevelTracking = _memberLevelTracking;
            _flowManager.Persistence = _persistence;
            _flowManager.ScenarioLoader = _scenarioLoader;
            _flowManager.LoginManagerInstance = _loginManager;
            _flowManager.UserSession = _session;
        }

        [Test]
        public void TestGetFirstStep()
        {
            var currentStep = _flowManager.GetCurrentStep();
            Assert.IsTrue(currentStep.CurrentStepNumber == 1);
            
            //verify that the correct step came back, with the right number of controls, and empty
             Assert.IsTrue(currentStep.Step.PopulatedControls.Count == 2);
             Assert.IsTrue(currentStep.Step.PopulatedControls[0].Control.Name == _scenarios[0].Steps[0].Controls[0].Name);
            Assert.IsTrue(currentStep.Step.PopulatedControls[0].SelectedValue == string.Empty);
            Assert.IsTrue(currentStep.Step.PopulatedControls[1].Control.Name == _scenarios[0].Steps[0].Controls[1].Name);
            Assert.IsTrue(currentStep.Step.PopulatedControls[1].SelectedValue == string.Empty);
        }

        [Test]
        public void TestGetNextStep()
        {
            _flowManager.Persistence.AddValue(PersistenceConstants.CurrentStep, "1");
            _flowManager.Persistence.AddAttributeValue("EmailAddress", "mmishkoff@spark.net");
            _flowManager.Persistence.AddAttributeValue("UserName", "mmishkoff");

            var currentStep = _flowManager.GetNextStep();
            Assert.IsTrue(currentStep.CurrentStepNumber == 2);

            //verify that the correct step came back, with the right number of controls, and empty
            Assert.IsTrue(currentStep.Step.PopulatedControls.Count == 3);
            Assert.IsTrue(currentStep.Step.PopulatedControls[0].Control.Name == _scenarios[0].Steps[1].Controls[0].Name);
            Assert.IsTrue(currentStep.Step.PopulatedControls[0].SelectedValue == string.Empty);
            Assert.IsTrue(currentStep.Step.PopulatedControls[1].Control.Name == _scenarios[0].Steps[1].Controls[1].Name);
            Assert.IsTrue(currentStep.Step.PopulatedControls[1].SelectedValue == string.Empty);
            Assert.IsTrue(currentStep.Step.PopulatedControls[2].Control.Name == _scenarios[0].Steps[1].Controls[2].Name);
            Assert.IsTrue(currentStep.Step.PopulatedControls[2].SelectedValue == string.Empty);
        }

        [Test]
        public void TestGetNextStepWillNotGoPastEnd()
        {
            _flowManager.Persistence.AddValue(PersistenceConstants.CurrentStep, "2");
            _flowManager.Persistence.AddAttributeValue("EmailAddress", "mmishkoff@spark.net");
            _flowManager.Persistence.AddAttributeValue("UserName", "mmishkoff");
            _flowManager.Persistence.AddAttributeValue("smokingHabits", "2");
            _flowManager.Persistence.AddAttributeValue("gendermask", "1");
            _flowManager.Persistence.AddAttributeValue("birthdate", "05/09/75");
            
            var currentStep = _flowManager.GetNextStep();
            Assert.IsTrue(currentStep.CurrentStepNumber == 2);
        }

        [Test]
        public void TestGetPreviousStep()
        {
            _flowManager.Persistence.AddValue(PersistenceConstants.CurrentStep, "2");
            var currentStep = _flowManager.GetPreviousStep();
            Assert.IsTrue(currentStep.CurrentStepNumber == 1);

            //verify that the correct step came back, with the right number of controls, and empty
            Assert.IsTrue(currentStep.Step.PopulatedControls.Count == 2);
            Assert.IsTrue(currentStep.Step.PopulatedControls[0].Control.Name == _scenarios[0].Steps[0].Controls[0].Name);
            Assert.IsTrue(currentStep.Step.PopulatedControls[0].SelectedValue == string.Empty);
            Assert.IsTrue(currentStep.Step.PopulatedControls[1].Control.Name == _scenarios[0].Steps[0].Controls[1].Name);
            Assert.IsTrue(currentStep.Step.PopulatedControls[1].SelectedValue == string.Empty);
        }

        [Test]
        public void TestGetCurrentStepCorrect()
        {
            _flowManager.Persistence.AddValue(PersistenceConstants.CurrentStep, "2");
            _flowManager.Persistence.AddAttributeValue("EmailAddress", "mmishkoff@spark.net");
            _flowManager.Persistence.AddAttributeValue("UserName", "mmishkoff");
            var currentStep = _flowManager.GetCurrentStep();
            Assert.IsTrue(currentStep.CurrentStepNumber == 2);
        }

        [Test]
        public void TestGetCurrentStepWillNotGoTwoPastLastCompletedStep()
        {
            _flowManager.Persistence.AddValue(PersistenceConstants.CurrentStep, "2");
            var currentStep = _flowManager.GetCurrentStep();
            Assert.IsTrue(currentStep.CurrentStepNumber == 1);
        }

        [Test]
        public void TestFirstStepOmniture()
        {
            _flowManager.Persistence.AddValue(PersistenceConstants.SessionID, "SESSIONID");
            var currentStep = _flowManager.GetCurrentStep();

            Assert.IsTrue(currentStep.OmnitureData.Variables["prop72"] == _scenarios[0].Name);
            Assert.IsTrue(currentStep.OmnitureData.Variables["prop73"] == "1");
            Assert.IsTrue(currentStep.OmnitureData.Variables["prop74"] == "SESSIONID");
            Assert.IsTrue(currentStep.OmnitureData.Variables["events"] == "event7");
        }

        [Test]
        public void TestGetPreviousStepWillNotGoPastBeginning()
        {
            _flowManager.Persistence.AddValue(PersistenceConstants.CurrentStep, "1");
            var currentStep = _flowManager.GetPreviousStep();
            Assert.IsTrue(currentStep.CurrentStepNumber == 1);
        }

        [Test]
        public void TestSubmitStep()
        {
            var values = new List<NameValuePair>();
            values.Add(new NameValuePair("emailaddress", "mmishkoff@spark.net"));
            values.Add(new NameValuePair("username", "mmishkoff"));
            _persistence.AddValue(PersistenceConstants.CurrentStep, "1");

            var response = _flowManager.SubmitStep(values);
            var attributes = _flowManager.Persistence.GetAttributeValues();
            Assert.IsTrue(response.Status == UpdateRegistrationStatus.Success);
            Assert.IsTrue(attributes.Count == 2);
            Assert.IsTrue(attributes["emailaddress"] == "mmishkoff@spark.net");
            Assert.IsTrue(attributes["username"] == "mmishkoff");
        }

        [Test]
        public void TestCompleteScenarioRun()
        {
            var omnitureManager = new OmnitureManager("Registration", _request, _scenarios[1]);
            omnitureManager.CurrentContext = _context;
            omnitureManager.CurrentRequest = _request;
            omnitureManager.CurrentResponse = _response;
            omnitureManager.Persistence = _persistence;
            omnitureManager.UserSession = _session;

            var cookieManager = new CookieManager();
            cookieManager.CurrentContext = _context;
            cookieManager.CurrentRequest = _request;
            cookieManager.CurrentResponse = _response;
            cookieManager.Persistence = _persistence;
            cookieManager.UserSession = _session;

            var scenarioLoader = new ScenarioLoaderMock();
            scenarioLoader.SetCurrentScenario(_scenarios[1]); //three steps

            var adapter = new APIAdapterMock();
            adapter.SetExtendedRegisterResult(new ExtendedRegisterResult { MemberId = 123, RegisterStatus = "Success", Username = "mmishkoff" });

            var flowManager = new RegistrationFlowManager(scenarioLoader, _request);
            flowManager.Persistence = new RegistrationValuePersistenceMock();
            flowManager.CurrentContext = _context;
            flowManager.CurrentResponse = _response;
            flowManager.MemberLevelTracking = _memberLevelTracking;
            flowManager.LoginManagerInstance = _loginManager;
            flowManager.UserSession = new Session();
            flowManager.OmnitureManagerInstance = omnitureManager;
            flowManager.CookieManagerInstance = cookieManager;
            flowManager.ApiAdapter = adapter;

            var currentStep = flowManager.GetCurrentStep();
            Assert.IsTrue(currentStep.CurrentStepNumber == 1);

            Assert.IsTrue(currentStep.Step.PopulatedControls.Count == 1);
            Assert.IsTrue(currentStep.Step.PopulatedControls[0].Control.Name == _scenarios[1].Steps[0].Controls[0].Name);
            Assert.IsTrue(currentStep.Step.PopulatedControls[0].SelectedValue == string.Empty);

            var valuesToSubmit = new List<NameValuePair>();
            valuesToSubmit.Add(new NameValuePair("smokingHabits", "2"));

            var submitResponse = flowManager.SubmitStep(valuesToSubmit);
            Assert.IsTrue(submitResponse.Status == UpdateRegistrationStatus.Success);

            var attributeValues = flowManager.Persistence.GetAttributeValues();
            Assert.IsTrue(attributeValues["smokingHabits"] == "2");
            Assert.IsTrue(flowManager.Persistence[PersistenceConstants.LastCompletedStep] == "1");
            Assert.IsTrue(flowManager.Persistence[PersistenceConstants.CurrentStep] == "1");

            currentStep = flowManager.GetNextStep();
            Assert.IsTrue(flowManager.Persistence[PersistenceConstants.CurrentStep] == "2");
            Assert.IsTrue(currentStep.Step.PopulatedControls.Count == 1);
            Assert.IsTrue(currentStep.Step.PopulatedControls[0].Control.Name == _scenarios[1].Steps[1].Controls[0].Name);
            Assert.IsTrue(currentStep.Step.PopulatedControls[0].SelectedValue == string.Empty);

            valuesToSubmit = new List<NameValuePair>();
            valuesToSubmit.Add(new NameValuePair("educationLevel", "2"));

            submitResponse = flowManager.SubmitStep(valuesToSubmit);
            Assert.IsTrue(submitResponse.Status == UpdateRegistrationStatus.Success);

            attributeValues = flowManager.Persistence.GetAttributeValues();
            Assert.IsTrue(attributeValues["educationLevel"] == "2");
            Assert.IsTrue(flowManager.Persistence[PersistenceConstants.LastCompletedStep] == "2");
            Assert.IsTrue(flowManager.Persistence[PersistenceConstants.CurrentStep] == "2");

            currentStep = flowManager.GetNextStep();
            Assert.IsTrue(flowManager.Persistence[PersistenceConstants.CurrentStep] == "3");
            Assert.IsTrue(currentStep.Step.PopulatedControls.Count == 1);
            Assert.IsTrue(currentStep.Step.PopulatedControls[0].Control.Name == _scenarios[1].Steps[2].Controls[0].Name);
            Assert.IsTrue(currentStep.Step.PopulatedControls[0].SelectedValue == string.Empty);

            valuesToSubmit = new List<NameValuePair>();
            valuesToSubmit.Add(new NameValuePair("EmailAddress", "mmishkoff@spark.net"));

            submitResponse = flowManager.SubmitStep(valuesToSubmit);
            Assert.IsTrue(submitResponse.Status == UpdateRegistrationStatus.Registered);

        }

        [Test]
        public void TestRegisterNewUser()
        {
            //this is just a basic test to be sure that a registration will go through with no errors
            var values = new List<NameValuePair>();
            values.Add(new NameValuePair("emailaddress", "mmishkoff@spark.net"));
            values.Add(new NameValuePair("username", "mmishkoff"));
            values.Add(new NameValuePair("gendermask", "1"));
            values.Add(new NameValuePair("birthdate", "05/09/1975"));
            _flowManager.Persistence.AddValue(PersistenceConstants.RegionZipCode, "90028");
            _flowManager.Persistence.AddValue(PersistenceConstants.SessionID, "SESSIONID");
            _flowManager.Persistence.AddValue(PersistenceConstants.CurrentStep, "2");
            _flowManager.UserSession[SessionConstants.PromotionId] = 55020;
            _flowManager.UserSession[SessionConstants.Luggage] = "lug";
            _flowManager.UserSession[SessionConstants.Refcd] = "ref";
            _flowManager.UserSession[SessionConstants.BannerId] = "123";
            (_apiAdapter as APIAdapterMock).SetExtendedRegisterResult(new ExtendedRegisterResult { MemberId = 123, RegisterStatus = "Success", Username = "mmishkoff" });
            
            var response = _flowManager.SubmitStep(values);
            Assert.IsTrue(response.Status == UpdateRegistrationStatus.Registered);
        }

        [Test]
        public void TestRegisterNewUserSession()
        {
            //this tests that everything that should go into session after a reg call does
            var values = new List<NameValuePair>();
            values.Add(new NameValuePair("emailaddress", "mmishkoff@spark.net"));
            values.Add(new NameValuePair("username", "mmishkoff"));
            values.Add(new NameValuePair("gendermask", "1"));
            values.Add(new NameValuePair("birthdate", "05/09/1975"));
            _flowManager.Persistence.AddValue(PersistenceConstants.RegionZipCode, "90028");
            _flowManager.Persistence.AddValue(PersistenceConstants.SessionID, "SESSIONID");
            _flowManager.Persistence.AddValue(PersistenceConstants.CurrentStep, "2");
            _flowManager.UserSession[SessionConstants.PromotionId] = 55020;
            _flowManager.UserSession[SessionConstants.Luggage] = "lug";
            _flowManager.UserSession[SessionConstants.Refcd] = "ref";
            _flowManager.UserSession[SessionConstants.BannerId] = "123";
            (_apiAdapter as APIAdapterMock).SetExtendedRegisterResult(new ExtendedRegisterResult { MemberId = 123, RegisterStatus = "Success", Username = "mmishkoff" });

            var response = _flowManager.SubmitStep(values);
            Assert.IsTrue(_flowManager.UserSession[SessionConstants.MemberGendermask].ToString() == "1");
            Assert.IsTrue(_flowManager.UserSession[SessionConstants.MemberBirthdate].ToString() == "05/09/1975");
            Assert.IsTrue(_flowManager.UserSession[SessionConstants.MemberRegionZipCode].ToString() == "90028");
            Assert.IsTrue(_flowManager.UserSession.MemberId.ToString() == "123");
            Assert.IsTrue(Convert.ToBoolean(_flowManager.UserSession[SessionConstants.GenerateRegistrationPersistence]) == false);
            
        }

        [Test]
        public void TestAllRegistrationAttributesSet()
        {
            //this tests that everything that should go into session after a reg call does
            var values = new List<NameValuePair>();
            values.Add(new NameValuePair("emailaddress", "mmishkoff@spark.net"));
            values.Add(new NameValuePair("username", "mmishkoff"));
            values.Add(new NameValuePair("gendermask", "1"));
            values.Add(new NameValuePair("birthdate", "05/09/1975"));
            _flowManager.Persistence.AddValue(PersistenceConstants.RegionZipCode, "90028");
            _flowManager.Persistence.AddValue(PersistenceConstants.SessionID, "SESSIONID");
            _flowManager.Persistence.AddValue(PersistenceConstants.CurrentStep, "2");
            _flowManager.UserSession[SessionConstants.PromotionId] = 55020;
            _flowManager.UserSession[SessionConstants.Luggage] = "lug";
            _flowManager.UserSession[SessionConstants.Refcd] = "ref";
            _flowManager.UserSession[SessionConstants.BannerId] = "123";
            (_apiAdapter as APIAdapterMock).SetExtendedRegisterResult(new ExtendedRegisterResult { MemberId = 123, RegisterStatus = "Success", Username = "mmishkoff" });

            var response = _flowManager.SubmitStep(values);

            ExtendedRegisterRequest regRequest = (_apiAdapter as APIAdapterMock).GetExtendedRegisterRequest();
            Assert.IsTrue(regRequest != null);
            Assert.IsTrue(Convert.ToInt32(regRequest.AttributeData[AttributeConstants.PromotionID]) == 55020);
            Assert.IsTrue(regRequest.AttributeData[AttributeConstants.Luggage].ToString() == "lug");
            Assert.IsTrue(regRequest.AttributeData[AttributeConstants.Refcd].ToString() == "ref");
            Assert.IsTrue(regRequest.AttributeData[AttributeConstants.BannerId].ToString() == "123");
            Assert.IsTrue(regRequest.AttributeData[AttributeConstants.GenderMask].ToString() == "1");
            Assert.IsTrue(regRequest.AttributeData[AttributeConstants.Birthdate].ToString() == "05/09/1975");
            Assert.IsTrue(regRequest.AttributeData[AttributeConstants.RegsistrationSessionID].ToString() == "SESSIONID");
            Assert.IsTrue(regRequest.AttributeData[AttributeConstants.RegistrationScenarioID].ToString() == _scenarios[0].ID.ToString());
            Assert.IsTrue(regRequest.AttributeData[AttributeConstants.TrackingRegApplication].ToString() == "1");

            Assert.IsTrue(regRequest.UserName == "mmishkoff");
            Assert.IsTrue(regRequest.EmailAddress == "mmishkoff@spark.net");
            
        }

        [Test]
        public void TestMissingRegionCityID()
        {
            //this tests that everything that should go into session after a reg call does
            var values = new List<NameValuePair>();
            values.Add(new NameValuePair("regioncountryid", "223"));
            values.Add(new NameValuePair("regionzipcode", "10010"));
            
            var response = _flowManager.SubmitStep(values);
            Assert.IsTrue(response.Status == UpdateRegistrationStatus.Failure);
            Assert.IsTrue(response.ErrorMessages != null && response.ErrorMessages.Count >0);
        }

        [Test]
        public void TestValidRegionSubmissionPassesSubmit()
        {
            //this tests that everything that should go into session after a reg call does
            var values = new List<NameValuePair>();
            values.Add(new NameValuePair("regioncountryid", "223"));
            values.Add(new NameValuePair("regionzipcode", "10010"));
            values.Add(new NameValuePair("regioncityid", "3466314"));
            
            var response = _flowManager.SubmitStep(values);
            Assert.IsTrue(response.Status == UpdateRegistrationStatus.Success);
        }


        [Test]
        public void TestGetNextStepWillNotGoPastLastCompletedStep()
        {
            var omnitureManager = new OmnitureManager("Registration", _request, _scenarios[1]);
            omnitureManager.CurrentContext = _context;
            omnitureManager.CurrentRequest = _request;
            omnitureManager.CurrentResponse = _response;
            omnitureManager.Persistence = _persistence;
            omnitureManager.UserSession = _session;

            var cookieManager = new CookieManager();
            cookieManager.CurrentContext = _context;
            cookieManager.CurrentRequest = _request;
            cookieManager.CurrentResponse = _response;
            cookieManager.Persistence = _persistence;
            cookieManager.UserSession = _session;
            
            
            var scenarioLoader = new ScenarioLoaderMock();
            scenarioLoader.SetCurrentScenario(_scenarios[1]); //three steps
            
            var flowManager = new RegistrationFlowManager(scenarioLoader, _request);
            flowManager.Persistence =new RegistrationValuePersistenceMock();
            flowManager.CurrentContext = _context;
            flowManager.CurrentResponse = _response;
            flowManager.MemberLevelTracking = _memberLevelTracking;
            flowManager.LoginManagerInstance = _loginManager;
            flowManager.UserSession = new Session();
            flowManager.OmnitureManagerInstance = omnitureManager;
            flowManager.CookieManagerInstance = cookieManager;
           
            flowManager.Persistence.AddValue(PersistenceConstants.CurrentStep, "3");
            flowManager.Persistence.AddAttributeValue("smokingHabits", "2");

            var currentStep = flowManager.GetNextStep();
            Assert.IsTrue(currentStep.CurrentStepNumber == 2);
        }

        private List<Scenario> GetScenarios()
        {
            
                var smokingOptions = new List<AttributeOption>
                                                           {
                                                               {new AttributeOption{AttributeOptionId = 74260, Description=string.Empty, ListOrder=1, Value=0}},
                                                               {new AttributeOption{AttributeOptionId = 74261, Description="Non-Smoker", ListOrder=2, Value=2}},
                                                               {new AttributeOption{AttributeOptionId = 74262, Description="Occasionally", ListOrder=3, Value=4}},
                                                               {new AttributeOption{AttributeOptionId = 74263, Description="Regularly", ListOrder=4, Value=8}},
                                                               {new AttributeOption{AttributeOptionId = 105452, Description="Trying to quit", ListOrder=5, Value=16}}
                                                           };

                var educationOptions = new List<AttributeOption>
                                                           {
                                                               {new AttributeOption{AttributeOptionId = 75972, Description=string.Empty, ListOrder=10, Value=0}},
                                                               {new AttributeOption{AttributeOptionId = 75790, Description="Elementary", ListOrder=20, Value=2}},
                                                               {new AttributeOption{AttributeOptionId = 74250, Description="High School", ListOrder=30, Value=4}},
                                                               {new AttributeOption{AttributeOptionId = 74251, Description="Some College", ListOrder=40, Value=8}}
                                                           };


                var gendermaskOptions = new List<AttributeOption>
                                                           {
                                                               {new AttributeOption{AttributeOptionId = 0, Description="Man seeking woman", ListOrder=10, Value=1}},
                                                               {new AttributeOption{AttributeOptionId = 0, Description="Woman seeking man", ListOrder=20, Value=2}},
                                                               {new AttributeOption{AttributeOptionId = 0, Description="Man seeking man", ListOrder=30, Value=3}},
                                                               {new AttributeOption{AttributeOptionId = 0, Description="Woman seeking woman", ListOrder=40, Value=4}}
                                                           };

                var smokingControl = new RegControl
                    {
                        Name = "smokingHabits",
                        DataType = "Number",
                        IsAttribute = true,
                        IsMultiValue = false,
                        ControlDisplayType = 1,
                        Required = true,
                        RequiredErrorMessage = "Can you just give us a little something? This field is required so you kind of have to!",
                        Label = "Your smoking habits",
                        EnableAutoAdvance = false,
                        AllowZeroValue = false,
                        Options = smokingOptions
                    };

                var educationControl = new RegControl
                    {
                        Name = "educationLevel",
                        DataType = "Number",
                        IsAttribute = true,
                        IsMultiValue = false,
                        ControlDisplayType = 1,
                        Required = true,
                        RequiredErrorMessage = "Can you just give us a little something? This field is required so you kind of have to!",
                        Label = "Your education level",
                        EnableAutoAdvance = false,
                        AllowZeroValue = false,
                        Options = educationOptions
                    };

                var emailControl = new RegControl
                    {
                        Name = "EmailAddress",
                        DataType = "Text",
                        IsAttribute = true,
                        IsMultiValue = false,
                        ControlDisplayType = 4,
                        Required = true,
                        RequiredErrorMessage = "Can you just give us a little something? This field is required so you kind of have to!",
                        Label = "Email address",
                        EnableAutoAdvance = false,
                        AllowZeroValue = false
                    };

                var userNameControl = new RegControl
                {
                    Name = "UserName",
                    DataType = "Text",
                    IsAttribute = true,
                    IsMultiValue = false,
                    ControlDisplayType = 4,
                    Required = true,
                    RequiredErrorMessage = "Can you just give us a little something? This field is required so you kind of have to!",
                    Label = "User Name",
                    EnableAutoAdvance = false,
                    AllowZeroValue = false
                };

                var gendermaskControl = new RegControl
                {
                    Name = "gendermask",
                    DataType = "Mask",
                    IsAttribute = true,
                    IsMultiValue = false,
                    ControlDisplayType = 12,
                    Required = true,
                    RequiredErrorMessage = "Can you just give us a little something? This field is required so you kind of have to!",
                    Label = "You are a: ",
                    EnableAutoAdvance = false,
                    AllowZeroValue = false
                };

                var birthdateControl = new RegControl
                {
                    Name = "birthdate",
                    DataType = "Date",
                    IsAttribute = true,
                    IsMultiValue = false,
                    ControlDisplayType = 9,
                    Required = true,
                    RequiredErrorMessage = "Can you just give us a little something? This field is required so you kind of have to!",
                    Label = "Birth Date: ",
                    EnableAutoAdvance = false,
                    AllowZeroValue = false
                };

            var s1Step1 = new Step
                               {
                                  Controls = new List<RegControl>{emailControl, userNameControl},
                                  CSSClass = "class",
                                  Order=1, 
                                  TipText = "tip1"
                               };

            var s1Step2 = new Step
                               {
                                   Controls = new List<RegControl> { gendermaskControl, birthdateControl, smokingControl },
                                   CSSClass = "class",
                                   Order = 2,
                                   TipText = "tip2"
                               };

            var s2Step1 = new Step
            {
                Controls = new List<RegControl> { smokingControl },
                CSSClass = "class",
                Order = 1,
                TipText = "tip1"
            };

            var s2Step2 = new Step
            {
                Controls = new List<RegControl> { educationControl },
                CSSClass = "class",
                Order = 2,
                TipText = "tip2"
            };

            var s2Step3 = new Step
            {
                Controls = new List<RegControl> { emailControl },
                CSSClass = "class",
                Order = 3,
                TipText = "tip3"
            };

            var s1 = new Scenario
                              {
                                  ConfirmationTemplate = "confirmation1",
                                  ID = 1,
                                  IsControl = true,
                                  Name = "Scenario1",
                                  RegistrationTemplate = "registration1",
                                  SplashTemplate = "Splash1",
                                  Steps = new List<Step> {s1Step1, s1Step2}
                              };

            var s2 = new Scenario
                    {
                        ConfirmationTemplate = "confirmation1",
                        ID = 2,
                        IsControl = true,
                        Name = "Scenario2",
                        RegistrationTemplate = "registration1",
                        SplashTemplate = "Splash1",
                        Steps = new List<Step> { s2Step1, s2Step2, s2Step3 }
                    };

            var scenarios = new List<Scenario>{s1, s2};
            return scenarios;
        }
    }
}
