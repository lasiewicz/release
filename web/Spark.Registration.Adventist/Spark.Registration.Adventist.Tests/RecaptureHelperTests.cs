﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.Common.RestConsumer.V2.Models.Content.AttributeData;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Content.RegistrationActivityRecording;
using Spark.Registration.Adventist.Framework.Helpers;
using Spark.Registration.Adventist.Tests.Mocks;
using Spark.Registration.Dictionaries.Constants;

namespace Spark.Registration.Adventist.Tests
{
    public class RecaptureHelperTests
    {
        [Test]
        public void TestLoadPersistenceHappyPath()
        {
            var recaptureInfo = new RegistrationRecaptureInfo();
            recaptureInfo.RegistrationFields = new Dictionary<string, string>();
            recaptureInfo.RegistrationFields.Add("gendermask", "1");
            recaptureInfo.RegistrationFields.Add("emailaddress", "mmishkoff@spark.net");
            recaptureInfo.RegistrationFields.Add(PersistenceConstants.RegionCountryID, "223");
            recaptureInfo.RegistrationFields.Add(PersistenceConstants.RegionStateID, "2");
            recaptureInfo.RegistrationFields.Add(PersistenceConstants.RegionCityID, "21345");
            recaptureInfo.RegistrationFields.Add(PersistenceConstants.RegionZipCode, "90028");
            recaptureInfo.RegistrationFields.Add(PersistenceConstants.SessionID, "1234567890");
            recaptureInfo.RegistrationFields.Add(PersistenceConstants.RegistrationStartRecorded, "true");

            var persistence = new RegistrationValuePersistenceMock();

            var apiAdapterMock = new APIAdapterMock();
            apiAdapterMock.SetRegistrationRecpatureInfo(recaptureInfo);

            var scenarios = GetScenarios();
            var currentScenario = scenarios[0];

            var recaptureHelper = new RecaptureHelper();
            recaptureHelper.LoadPersistenceFromRecapture(persistence, apiAdapterMock, "12345", currentScenario,scenarios);

            Assert.IsTrue(persistence[PersistenceConstants.RegionCountryID] == "223");
            Assert.IsTrue(persistence[PersistenceConstants.RegionStateID] == "2");
            Assert.IsTrue(persistence[PersistenceConstants.RegionCityID] == "21345");
            Assert.IsTrue(persistence[PersistenceConstants.RegionZipCode] == "90028");
            Assert.IsTrue(persistence[PersistenceConstants.SessionID] == "1234567890");
            Assert.IsTrue(persistence[PersistenceConstants.RegistrationStartRecorded] == "true");

            Assert.IsTrue(persistence["gendermask"] == "1");
            Assert.IsTrue(persistence["emailaddress"] == "mmishkoff@spark.net");
        }

        [Test]
        public void TestScenarioSetIfPresentInRecapture()
        {
            var scenarios = GetScenarios();
            var currentScenario = scenarios[0];
            
            var recaptureInfo = new RegistrationRecaptureInfo();
            recaptureInfo.RegistrationFields = new Dictionary<string, string>();
            recaptureInfo.RegistrationFields.Add("gendermask", "1");
            recaptureInfo.RegistrationFields.Add(PersistenceConstants.ScenarioName, scenarios[1].Name);

            var persistence = new RegistrationValuePersistenceMock();

            var apiAdapterMock = new APIAdapterMock();
            apiAdapterMock.SetRegistrationRecpatureInfo(recaptureInfo);
            
            var recaptureHelper = new RecaptureHelper();
            recaptureHelper.LoadPersistenceFromRecapture(persistence, apiAdapterMock, "12345", currentScenario, scenarios);

            Assert.IsTrue(persistence[PersistenceConstants.ScenarioName] == scenarios[1].Name);
        }

        [Test]
        public void TestScenarioNotSetToCurrentIfPresentInRecaptureButDoesntExist()
        {
            var scenarios = GetScenarios();
            var currentScenario = scenarios[0];

            var recaptureInfo = new RegistrationRecaptureInfo();
            recaptureInfo.RegistrationFields = new Dictionary<string, string>();
            recaptureInfo.RegistrationFields.Add("gendermask", "1");
            recaptureInfo.RegistrationFields.Add(PersistenceConstants.ScenarioName, "NonExistantScenario");

            var persistence = new RegistrationValuePersistenceMock();

            var apiAdapterMock = new APIAdapterMock();
            apiAdapterMock.SetRegistrationRecpatureInfo(recaptureInfo);
            
            var recaptureHelper = new RecaptureHelper();
            recaptureHelper.LoadPersistenceFromRecapture(persistence, apiAdapterMock, "12345", currentScenario,
                                                         scenarios);

            Assert.IsTrue(string.IsNullOrEmpty(persistence[PersistenceConstants.ScenarioName]));
        }

        [Test]
        public void TestCurrentAndLastCompletedStepSetCorrectlyBackToOne()
        {
            var scenarios = GetScenarios();
            var currentScenario = scenarios[0];

            var recaptureInfo = new RegistrationRecaptureInfo();
            recaptureInfo.RegistrationFields = new Dictionary<string, string>();
            recaptureInfo.RegistrationFields.Add(PersistenceConstants.CurrentStep, "3");
            recaptureInfo.RegistrationFields.Add(PersistenceConstants.LastCompletedStep, "2");
            
            var persistence = new RegistrationValuePersistenceMock();

            var apiAdapterMock = new APIAdapterMock();
            apiAdapterMock.SetRegistrationRecpatureInfo(recaptureInfo);

            var recaptureHelper = new RecaptureHelper();
            recaptureHelper.LoadPersistenceFromRecapture(persistence, apiAdapterMock, "12345", currentScenario,
                                                         scenarios);

            Assert.IsTrue(persistence[PersistenceConstants.CurrentStep] == "1");
            Assert.IsTrue(persistence[PersistenceConstants.LastCompletedStep] == "1");
        }

        [Test]
        public void TestNoExceptionThrownIfRecaptureInfoIsNullOrFieldsNull()
        {
            var scenarios = GetScenarios();
            var currentScenario = scenarios[0];

            var recaptureInfo = new RegistrationRecaptureInfo();

            var persistence = new RegistrationValuePersistenceMock();

            var apiAdapterMock = new APIAdapterMock();
            apiAdapterMock.SetRegistrationRecpatureInfo(null);

            var recaptureHelper = new RecaptureHelper();
            recaptureHelper.LoadPersistenceFromRecapture(persistence, apiAdapterMock, "12345", currentScenario,
                                                         scenarios);

            apiAdapterMock.SetRegistrationRecpatureInfo(recaptureInfo);
            recaptureHelper.LoadPersistenceFromRecapture(persistence, apiAdapterMock, "12345", currentScenario,
                                                         scenarios);

            //no asserts, this is a successful test if the code just doesn't blow up
        }

        private List<Scenario> GetScenarios()
        {

            var smokingOptions = new List<AttributeOption>
                                                           {
                                                               {new AttributeOption{AttributeOptionId = 74260, Description=string.Empty, ListOrder=1, Value=0}},
                                                               {new AttributeOption{AttributeOptionId = 74261, Description="Non-Smoker", ListOrder=2, Value=2}},
                                                               {new AttributeOption{AttributeOptionId = 74262, Description="Occasionally", ListOrder=3, Value=4}},
                                                               {new AttributeOption{AttributeOptionId = 74263, Description="Regularly", ListOrder=4, Value=8}},
                                                               {new AttributeOption{AttributeOptionId = 105452, Description="Trying to quit", ListOrder=5, Value=16}}
                                                           };

            var educationOptions = new List<AttributeOption>
                                                           {
                                                               {new AttributeOption{AttributeOptionId = 75972, Description=string.Empty, ListOrder=10, Value=0}},
                                                               {new AttributeOption{AttributeOptionId = 75790, Description="Elementary", ListOrder=20, Value=2}},
                                                               {new AttributeOption{AttributeOptionId = 74250, Description="High School", ListOrder=30, Value=4}},
                                                               {new AttributeOption{AttributeOptionId = 74251, Description="Some College", ListOrder=40, Value=8}}
                                                           };


            var gendermaskOptions = new List<AttributeOption>
                                                           {
                                                               {new AttributeOption{AttributeOptionId = 0, Description="Man seeking woman", ListOrder=10, Value=1}},
                                                               {new AttributeOption{AttributeOptionId = 0, Description="Woman seeking man", ListOrder=20, Value=2}},
                                                               {new AttributeOption{AttributeOptionId = 0, Description="Man seeking man", ListOrder=30, Value=3}},
                                                               {new AttributeOption{AttributeOptionId = 0, Description="Woman seeking woman", ListOrder=40, Value=4}}
                                                           };

            var smokingControl = new RegControl
            {
                Name = "smokingHabits",
                DataType = "Number",
                IsAttribute = true,
                IsMultiValue = false,
                ControlDisplayType = 1,
                Required = true,
                RequiredErrorMessage = "Can you just give us a little something? This field is required so you kind of have to!",
                Label = "Your smoking habits",
                EnableAutoAdvance = false,
                AllowZeroValue = false,
                Options = smokingOptions
            };

            var educationControl = new RegControl
            {
                Name = "educationLevel",
                DataType = "Number",
                IsAttribute = true,
                IsMultiValue = false,
                ControlDisplayType = 1,
                Required = true,
                RequiredErrorMessage = "Can you just give us a little something? This field is required so you kind of have to!",
                Label = "Your education level",
                EnableAutoAdvance = false,
                AllowZeroValue = false,
                Options = educationOptions
            };

            var emailControl = new RegControl
            {
                Name = "EmailAddress",
                DataType = "Text",
                IsAttribute = true,
                IsMultiValue = false,
                ControlDisplayType = 4,
                Required = true,
                RequiredErrorMessage = "Can you just give us a little something? This field is required so you kind of have to!",
                Label = "Email address",
                EnableAutoAdvance = false,
                AllowZeroValue = false
            };

            var userNameControl = new RegControl
            {
                Name = "UserName",
                DataType = "Text",
                IsAttribute = true,
                IsMultiValue = false,
                ControlDisplayType = 4,
                Required = true,
                RequiredErrorMessage = "Can you just give us a little something? This field is required so you kind of have to!",
                Label = "User Name",
                EnableAutoAdvance = false,
                AllowZeroValue = false
            };

            var gendermaskControl = new RegControl
            {
                Name = "gendermask",
                DataType = "Mask",
                IsAttribute = true,
                IsMultiValue = false,
                ControlDisplayType = 12,
                Required = true,
                RequiredErrorMessage = "Can you just give us a little something? This field is required so you kind of have to!",
                Label = "You are a: ",
                EnableAutoAdvance = false,
                AllowZeroValue = false
            };

            var birthdateControl = new RegControl
            {
                Name = "birthdate",
                DataType = "Date",
                IsAttribute = true,
                IsMultiValue = false,
                ControlDisplayType = 9,
                Required = true,
                RequiredErrorMessage = "Can you just give us a little something? This field is required so you kind of have to!",
                Label = "Birth Date: ",
                EnableAutoAdvance = false,
                AllowZeroValue = false
            };

            var s1Step1 = new Step
            {
                Controls = new List<RegControl> { emailControl, userNameControl },
                CSSClass = "class",
                Order = 1,
                TipText = "tip1"
            };

            var s1Step2 = new Step
            {
                Controls = new List<RegControl> { gendermaskControl, birthdateControl, smokingControl },
                CSSClass = "class",
                Order = 2,
                TipText = "tip2"
            };

            var s2Step1 = new Step
            {
                Controls = new List<RegControl> { smokingControl },
                CSSClass = "class",
                Order = 1,
                TipText = "tip1"
            };

            var s2Step2 = new Step
            {
                Controls = new List<RegControl> { educationControl },
                CSSClass = "class",
                Order = 2,
                TipText = "tip2"
            };

            var s2Step3 = new Step
            {
                Controls = new List<RegControl> { emailControl },
                CSSClass = "class",
                Order = 3,
                TipText = "tip3"
            };

            var s1 = new Scenario
            {
                ConfirmationTemplate = "confirmation1",
                ID = 1,
                IsControl = true,
                Name = "Scenario1",
                RegistrationTemplate = "registration1",
                SplashTemplate = "Splash1",
                Steps = new List<Step> { s1Step1, s1Step2 }
            };

            var s2 = new Scenario
            {
                ConfirmationTemplate = "confirmation1",
                ID = 2,
                IsControl = true,
                Name = "Scenario2",
                RegistrationTemplate = "registration1",
                SplashTemplate = "Splash1",
                Steps = new List<Step> { s2Step1, s2Step2, s2Step3 }
            };

            var scenarios = new List<Scenario> { s1, s2 };
            return scenarios;
        }
    }
}
