﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.Common.RestConsumer.V2.Models.Content.AttributeData;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Registration.Adventist.Framework.Helpers;
using Spark.Registration.Adventist.Tests.Mocks;

namespace Spark.Registration.Adventist.Tests
{
    public class ScenarioHelperTests
    {
        [Test]
        public void TestGetCompletedStepWorksHappyPath()
        {
            var scenario = GetScenario();
            var persistence = new RegistrationValuePersistenceMock();
            persistence.AddAttributeValue("smokingHabits", "2");
            persistence.AddAttributeValue("educationLevel", "2");

            var scenarioHelper = new ScenarioHelper(scenario);
            int lastCompletedStep = scenarioHelper.GetLastCompletedStep(persistence);
            Assert.IsTrue(lastCompletedStep == 2);

            persistence = new RegistrationValuePersistenceMock();
            persistence.AddAttributeValue("smokingHabits", "2");

            lastCompletedStep = scenarioHelper.GetLastCompletedStep(persistence);
            Assert.IsTrue(lastCompletedStep == 1);

            persistence = new RegistrationValuePersistenceMock();
            persistence.AddAttributeValue("smokingHabits", "2");
            persistence.AddAttributeValue("educationLevel", "2");
            persistence.AddAttributeValue("EmailAddress", "mmishkoff@spark.net");
            
            lastCompletedStep = scenarioHelper.GetLastCompletedStep(persistence);
            Assert.IsTrue(lastCompletedStep == 3);
        }

        [Test]
        public void TestGetCompletedStepCatchesStepWithEmptyValue()
        {
            var scenario = GetScenario();
            var persistence = new RegistrationValuePersistenceMock();

            persistence.AddAttributeValue("smokingHabits", "2");
            persistence.AddAttributeValue("educationLevel", "");
            persistence.AddAttributeValue("EmailAddress", "mmishkoff@spark.net");

            var scenarioHelper = new ScenarioHelper(scenario);
            int lastCompletedStep = scenarioHelper.GetLastCompletedStep(persistence);

            Assert.IsTrue(lastCompletedStep == 1);
        }

        [Test]
        public void TestGetCompletedStepCatchesStepNotSupplied()
        {
            var scenario = GetScenario();
            var persistence = new RegistrationValuePersistenceMock();

            //this is for step 3, but should come back 0 because step 1 wasn't supplied
            persistence.AddAttributeValue("EmailAddress", "mmishkoff@spark.net");

            var scenarioHelper = new ScenarioHelper(scenario);
            int lastCompletedStep = scenarioHelper.GetLastCompletedStep(persistence);

            Assert.IsTrue(lastCompletedStep == 0);
        }

        [Test]
        public void TestGetCompletedStepWorksWithNoAttributes()
        {
            var scenario = GetScenario();
            var persistence = new RegistrationValuePersistenceMock();

            var scenarioHelper = new ScenarioHelper(scenario);
            int lastCompletedStep = scenarioHelper.GetLastCompletedStep(persistence);

            Assert.IsTrue(lastCompletedStep == 0);
        }

        [Test]
        public void GetCompletedStepWorksWithStepsMissingOrderSequence()
        {
            var scenario = GetScenario();
            scenario.Steps[0].Order = 1;
            scenario.Steps[1].Order = 3;
            scenario.Steps[2].Order = 4;

            var persistence = new RegistrationValuePersistenceMock();
            persistence.AddAttributeValue("smokingHabits", "2");
            persistence.AddAttributeValue("educationLevel", "2");
            persistence.AddAttributeValue("smokingHabits", "2");

            var scenarioHelper = new ScenarioHelper(scenario);
            int lastCompletedStep = scenarioHelper.GetLastCompletedStep(persistence);
            Assert.IsTrue(lastCompletedStep == 2);
        }

        [Test]
        public void GetCompletedStepWorksWithStepsOutOfOrderInScenario()
        {
            var scenario = GetScenario();
            var step1 = scenario.Steps[0];
            var step2 = scenario.Steps[1];
            var step3 = scenario.Steps[2];

            scenario.Steps[0] = step2;
            scenario.Steps[1] = step1;
            scenario.Steps[2] = step3;

            var persistence = new RegistrationValuePersistenceMock();
            persistence.AddAttributeValue("smokingHabits", "2");
            persistence.AddAttributeValue("educationLevel", "2");
            persistence.AddAttributeValue("smokingHabits", "2");

            var scenarioHelper = new ScenarioHelper(scenario);
            int lastCompletedStep = scenarioHelper.GetLastCompletedStep(persistence);
            Assert.IsTrue(lastCompletedStep == 2);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void TestGetCompletedStepWorksCatchesNullScenario()
        {
            var scenarioHelper = new ScenarioHelper(null);
        }

        private Scenario GetScenario()
        {

            var smokingOptions = new List<AttributeOption>
                                     {
                                         {
                                             new AttributeOption
                                                 {
                                                     AttributeOptionId = 74260,
                                                     Description = string.Empty,
                                                     ListOrder = 1,
                                                     Value = 0
                                                 }
                                             },
                                         {
                                             new AttributeOption
                                                 {
                                                     AttributeOptionId = 74261,
                                                     Description = "Non-Smoker",
                                                     ListOrder = 2,
                                                     Value = 2
                                                 }
                                             },
                                         {
                                             new AttributeOption
                                                 {
                                                     AttributeOptionId = 74262,
                                                     Description = "Occasionally",
                                                     ListOrder = 3,
                                                     Value = 4
                                                 }
                                             },
                                         {
                                             new AttributeOption
                                                 {
                                                     AttributeOptionId = 74263,
                                                     Description = "Regularly",
                                                     ListOrder = 4,
                                                     Value = 8
                                                 }
                                             },
                                         {
                                             new AttributeOption
                                                 {
                                                     AttributeOptionId = 105452,
                                                     Description = "Trying to quit",
                                                     ListOrder = 5,
                                                     Value = 16
                                                 }
                                             }
                                     };

            var educationOptions = new List<AttributeOption>
                                       {
                                           {
                                               new AttributeOption
                                                   {
                                                       AttributeOptionId = 75972,
                                                       Description = string.Empty,
                                                       ListOrder = 10,
                                                       Value = 0
                                                   }
                                               },
                                           {
                                               new AttributeOption
                                                   {
                                                       AttributeOptionId = 75790,
                                                       Description = "Elementary",
                                                       ListOrder = 20,
                                                       Value = 2
                                                   }
                                               },
                                           {
                                               new AttributeOption
                                                   {
                                                       AttributeOptionId = 74250,
                                                       Description = "High School",
                                                       ListOrder = 30,
                                                       Value = 4
                                                   }
                                               },
                                           {
                                               new AttributeOption
                                                   {
                                                       AttributeOptionId = 74251,
                                                       Description = "Some College",
                                                       ListOrder = 40,
                                                       Value = 8
                                                   }
                                               }
                                       };


            var gendermaskOptions = new List<AttributeOption>
                                        {
                                            {
                                                new AttributeOption
                                                    {
                                                        AttributeOptionId = 0,
                                                        Description = "Man seeking woman",
                                                        ListOrder = 10,
                                                        Value = 1
                                                    }
                                                },
                                            {
                                                new AttributeOption
                                                    {
                                                        AttributeOptionId = 0,
                                                        Description = "Woman seeking man",
                                                        ListOrder = 20,
                                                        Value = 2
                                                    }
                                                },
                                            {
                                                new AttributeOption
                                                    {
                                                        AttributeOptionId = 0,
                                                        Description = "Man seeking man",
                                                        ListOrder = 30,
                                                        Value = 3
                                                    }
                                                },
                                            {
                                                new AttributeOption
                                                    {
                                                        AttributeOptionId = 0,
                                                        Description = "Woman seeking woman",
                                                        ListOrder = 40,
                                                        Value = 4
                                                    }
                                                }
                                        };

            var smokingControl = new RegControl
                                     {
                                         Name = "smokingHabits",
                                         DataType = "Number",
                                         IsAttribute = true,
                                         IsMultiValue = false,
                                         ControlDisplayType = 1,
                                         Required = true,
                                         RequiredErrorMessage =
                                             "Can you just give us a little something? This field is required so you kind of have to!",
                                         Label = "Your smoking habits",
                                         EnableAutoAdvance = false,
                                         AllowZeroValue = false,
                                         Options = smokingOptions
                                     };

            var educationControl = new RegControl
                                       {
                                           Name = "educationLevel",
                                           DataType = "Number",
                                           IsAttribute = true,
                                           IsMultiValue = false,
                                           ControlDisplayType = 1,
                                           Required = true,
                                           RequiredErrorMessage =
                                               "Can you just give us a little something? This field is required so you kind of have to!",
                                           Label = "Your education level",
                                           EnableAutoAdvance = false,
                                           AllowZeroValue = false,
                                           Options = educationOptions
                                       };

            var emailControl = new RegControl
                                   {
                                       Name = "EmailAddress",
                                       DataType = "Text",
                                       IsAttribute = true,
                                       IsMultiValue = false,
                                       ControlDisplayType = 4,
                                       Required = true,
                                       RequiredErrorMessage =
                                           "Can you just give us a little something? This field is required so you kind of have to!",
                                       Label = "Email address",
                                       EnableAutoAdvance = false,
                                       AllowZeroValue = false
                                   };

            var userNameControl = new RegControl
                                      {
                                          Name = "UserName",
                                          DataType = "Text",
                                          IsAttribute = true,
                                          IsMultiValue = false,
                                          ControlDisplayType = 4,
                                          Required = true,
                                          RequiredErrorMessage =
                                              "Can you just give us a little something? This field is required so you kind of have to!",
                                          Label = "User Name",
                                          EnableAutoAdvance = false,
                                          AllowZeroValue = false
                                      };

            var gendermaskControl = new RegControl
                                        {
                                            Name = "gendermask",
                                            DataType = "Mask",
                                            IsAttribute = true,
                                            IsMultiValue = false,
                                            ControlDisplayType = 12,
                                            Required = true,
                                            RequiredErrorMessage =
                                                "Can you just give us a little something? This field is required so you kind of have to!",
                                            Label = "You are a: ",
                                            EnableAutoAdvance = false,
                                            AllowZeroValue = false
                                        };

            var birthdateControl = new RegControl
                                       {
                                           Name = "birthdate",
                                           DataType = "Date",
                                           IsAttribute = true,
                                           IsMultiValue = false,
                                           ControlDisplayType = 9,
                                           Required = true,
                                           RequiredErrorMessage =
                                               "Can you just give us a little something? This field is required so you kind of have to!",
                                           Label = "Birth Date: ",
                                           EnableAutoAdvance = false,
                                           AllowZeroValue = false
                                       };

            var s1Step1 = new Step
                              {
                                  Controls = new List<RegControl> {emailControl, userNameControl},
                                  CSSClass = "class",
                                  Order = 1,
                                  TipText = "tip1"
                              };

            var s1Step2 = new Step
                              {
                                  Controls = new List<RegControl> {gendermaskControl, birthdateControl, smokingControl},
                                  CSSClass = "class",
                                  Order = 2,
                                  TipText = "tip2"
                              };

            var step1 = new Step
                            {
                                Controls = new List<RegControl> {smokingControl},
                                CSSClass = "class",
                                Order = 1,
                                TipText = "tip1"
                            };

            var step2 = new Step
                            {
                                Controls = new List<RegControl> {educationControl},
                                CSSClass = "class",
                                Order = 2,
                                TipText = "tip2"
                            };

            var step3 = new Step
                            {
                                Controls = new List<RegControl> {emailControl},
                                CSSClass = "class",
                                Order = 3,
                                TipText = "tip3"
                            };

            var scenario = new Scenario
                               {
                                   ConfirmationTemplate = "confirmation1",
                                   ID = 1,
                                   IsControl = true,
                                   Name = "Scenario1",
                                   RegistrationTemplate = "registration1",
                                   SplashTemplate = "Splash1",
                                   Steps = new List<Step> {step1, step2, step3}
                               };


            return scenario;
        }
    }
}
