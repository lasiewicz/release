﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Framework;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Web;
using Spark.Registration.Adventist.Managers;
using Spark.Registration.Adventist.Models;
using Spark.Registration.Adventist.Tests.Mocks;
using Spark.Registration.Dictionaries.Constants;

namespace Spark.Registration.Adventist.Tests
{
    [TestFixture]
    public class PixelTests
    {
        [Test]
        public void TestBasicGetPixels()
        {
            ICurrentRequest request = new CurrentRequestMock();
            Session session = new Session();
            
            session.MemberId= 100068787;
            session[SessionConstants.Luggage] = "lgid";
            session[SessionConstants.PromotionId] = "prmed";
            session[SessionConstants.Referrer] = "www.jdate.com";

            PixelManager manager = new PixelManager();
            manager.CurrentRequest = request;
            manager.ApiAdapter = new APIAdapterMock();
            manager.UserSession = session;
            (manager.ApiAdapter as APIAdapterMock).SetPixels(new List<string> { "pixel1", "pixel2" });


            List<string> pixels = manager.RetrievePixels("registrationconfirmation");
            Assert.IsNotNull(pixels, "pixels are empty");
            Assert.IsTrue(pixels.Count == 2, "not all pixels returned");
            Assert.IsTrue(pixels[0] == "pixel1" && pixels[1] == "pixel2", "pixels altered");
        }

        [Test]
        public void TestPixelsFiredEventSetInSesion()
        {
            ICurrentRequest request = new CurrentRequestMock();
            Session session = new Session();

            session[SessionConstants.MemberID] = 100068787;
            session[SessionConstants.Luggage] = "lgid";
            session[SessionConstants.PromotionId] = "prmed";
            session[SessionConstants.Referrer] = "www.jdate.com";

            PixelManager manager = new PixelManager();
            manager.CurrentRequest = request;
            manager.ApiAdapter = new APIAdapterMock();
            manager.UserSession = session;
            List<string> pixels = manager.RetrievePixels("registrationconfirmation");

            Assert.IsTrue(Convert.ToBoolean(session[SessionConstants.ConfirmationPixelsFired]),"pixels fired event not set in session"  );
        }

        [Test]
        public void TestPixelsNotFiredWhenSessionVariableSet()
        {
            ICurrentRequest request = new CurrentRequestMock();
            Session session = new Session();

            session[SessionConstants.MemberID] = 100068787;
            session[SessionConstants.Luggage] = "lgid";
            session[SessionConstants.PromotionId] = "prmed";
            session[SessionConstants.Referrer] = "www.jdate.com";
            session[SessionConstants.ConfirmationPixelsFired] = true;

            PixelManager manager = new PixelManager();
            manager.CurrentRequest = request;
            manager.ApiAdapter = new APIAdapterMock();
            manager.UserSession = session;
            (manager.ApiAdapter as APIAdapterMock).SetPixels(new List<string> { "pixel1", "pixel2" });
            List<string> pixels = manager.RetrievePixels("registrationconfirmation");

            Assert.IsTrue(pixels == null, "pixels returned even though fired session variable set");
        }
    }
}
