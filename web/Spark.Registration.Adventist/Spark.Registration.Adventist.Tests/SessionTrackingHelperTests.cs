﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Framework.Helpers;
using Spark.Registration.Adventist.Tests.Mocks;
using Spark.Registration.Dictionaries.Constants;

namespace Spark.Registration.Adventist.Tests
{
    public class SessionTrackingHelperTests
    {
        [Test]
        public void TestCreateAndPersistSessionTrackingHappyPath()
        {
            var persistence = new RegistrationValuePersistenceMock();
            var session = new Session();

            persistence.AddValue(PersistenceConstants.SessionID, "sessionid");
            session[SessionConstants.PromotionId] = 55020;
            session[SessionConstants.Luggage] = "lug";
            session[SessionConstants.Referrer] = "www.google.com";
            
            var apiAdapterMock = new APIAdapterMock();
            apiAdapterMock.SetSessionTrackingId(12345);

            var sessionTrackerHelper = new SessionTrackingHelper();
            sessionTrackerHelper.CreateAndPersistSessionTracking(persistence, apiAdapterMock, session, "192.168.2.1");

            Assert.IsTrue(persistence[PersistenceConstants.SessionTrackingID] == "12345");
        }

        [Test]
        public void TestCreateAndPersistSessionWithMissingSessionParams()
        {
            var persistence = new RegistrationValuePersistenceMock();
            var session = new Session();

            persistence.AddValue(PersistenceConstants.SessionID, "sessionid");
            var apiAdapterMock = new APIAdapterMock();
            apiAdapterMock.SetSessionTrackingId(12345);

            var sessionTrackerHelper = new SessionTrackingHelper();
            sessionTrackerHelper.CreateAndPersistSessionTracking(persistence, apiAdapterMock, session, "192.168.2.1");

            Assert.IsTrue(persistence[PersistenceConstants.SessionTrackingID] == "12345");
        }
    }
}
