﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ServiceAdapters;
using Matchnet.EmailNotifier.ServiceAdapters;
using Matchnet.EmailNotifier.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using NUnit.Framework;
using Spark.Common.RestConsumer.V2;
using Spark.Common.RestConsumer.V2.Models;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Content.RegistrationActivityRecording;
using Spark.Common.RestConsumer.V2.Models.Member;
using Spark.Registration.Adventist.Framework;
using Spark.Registration.Adventist.Managers;
using Spark.Registration.Adventist.Models;

namespace Spark.Registration.Adventist.Tests
{
    [TestFixture]
    public class APITests
    {
        private  Dictionary<string, string> UrlParams { get; set; }
        private Dictionary<string, string> QueryParams { get; set; }
        
        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            UrlParams = new Dictionary<string, string> { { "applicationId", RegistrationConfiguration.ApplicationId.ToString() } };
            QueryParams = new Dictionary<string, string> { { "client_secret", RegistrationConfiguration.ClientSecret }, { "applicationId", RegistrationConfiguration.ApplicationId.ToString() } };
        }
        
        [Test]
        public void TestGetScenariosBasic()
        {
            var adapter = GetMockAdapter();

            List<Scenario> scenarios = adapter.GetScenarios();
            Assert.IsNotNull(scenarios, "Null scenarios returned");
            Assert.IsTrue(scenarios.Count > 0, "No scenarios returned");
        }

        private static ApiAdapter GetMockAdapter()
        {
            return new ApiAdapter(
                RegistrationConfiguration.ApplicationId.ToString(),
                RegistrationConfiguration.ClientSecret,
                null,
                RestConsumer.Instance);
        }

        [Test]
        public void TestGetRandomScenarioNameBasic()
        {
            var adapter = GetMockAdapter();

            var randomScenarioName = adapter.GetRandomScenarioName(DeviceType.Desktop);
            Assert.IsNotNullOrEmpty(randomScenarioName, "Empty random scenario name returned");
        }

        [Test]
        public void TestRecordRegistrationStart()
        {
            var regSessionID = Guid.NewGuid();
            
            var registrationStart = new RegistrationStart
            {
                RegistrationSessionID = regSessionID.ToString(),
                ApplicationID = 1,
                FormFactor = "<DeviceProperties><IsMobileDevice>false</IsMobileDevice><IsTablet>false</IsTablet><Manufacturer>Unknown</Manufacturer><Model>Unknown</Model></DeviceProperties>",
                ScenarioID = 1,
                IPAddress = 16777343,
                TimeStamp = DateTime.Now
            };

            Console.WriteLine("TestRecordRegistrationStart session guid: " + regSessionID.ToString());
            ResponseDetail responseDetail = new ResponseDetail();
            var result = RestConsumer.Instance.Post<RegistrationRecordingResult, RegistrationStart>(UrlParams, registrationStart, out responseDetail, QueryParams);
            Assert.IsTrue(result.Recorded, "start event not recorded");
        }

        [Test]
        public void TestRecordRegistrationStepWithVerifyRecapture()
        {
            bool isRecaptureEnabled = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_REGISTRATION_CAPTURE", 3, 103));

            if(isRecaptureEnabled)
            {
                var regSessionID = Guid.NewGuid();
                var email = regSessionID.ToString() + "@spark.net";
                var stepDetails = new Dictionary<string, object>{ {"gendermask", 1} };

                var registrationStep = new RegistrationStepWithRecaptureInfo
                {
                    RegistrationSessionID = regSessionID.ToString(),
                    StepID = 1,
                    StepDetails = stepDetails,
                    TimeStamp = DateTime.Now,
                    BrandID = 1003,
                    AllRegFields = stepDetails,
                    EmailAddress = email,
                    RecaptureID = string.Empty
                };

                ResponseDetail responseDetail = new ResponseDetail();
                var response = RestConsumer.Instance.Post<RegistrationRecordingResultWithRecaptureInfo, RegistrationStepWithRecaptureInfo>(UrlParams, registrationStep, out responseDetail ,QueryParams);
                Assert.IsTrue(response.Recorded, "step event not recorded");

                var ev = WCFEmailNotifierSA.Instance.GetScheduledEvent(response.RecaptureID);
                Assert.IsNotNull(ev, "RegRecapture event not found");
                Assert.IsTrue(ev.EmailAddress == email, "email address on RegRecapture event doesn't match");
                Assert.IsTrue(ev.ScheduleDetails.Details["gendermask"].ToString() == "1", "gendermask attribute not returned with event");
            }
        }

        [Test]
        public void TestRegistration()
        {
            var regSessionID = Guid.NewGuid().ToString();
            var email = regSessionID + "@spark.net";

            var attributes = new Dictionary<string, object>();
            attributes.Add("gendermask", 3);
            attributes.Add("FirstName", "Matt");
            attributes.Add("children", 2);
            attributes.Add("JDateReligion", 2);
            attributes.Add("JDateEthnicity", 8);
            attributes.Add("PromotionID", 49098);
            attributes.Add("RegionId", 9805694);
            attributes.Add("RegistrationScenarioID", 1);
            attributes.Add("lookingFor", 1);
            attributes.Add("MaritalStatus", 2);
            attributes.Add("SmokingHabits", 4);
            attributes.Add("DrinkingHabits", 2);
            attributes.Add("planOnChildren", "yes");
            attributes.Add("heightCm", 191);
            attributes.Add("occupation", 1073741824);
            attributes.Add("NewsletterMask", 4);
            attributes.Add("SynagogueAttendance", 16);
            attributes.Add("Birthdate", DateTime.Parse("05/09/1975"));

            var registerRequest = new ExtendedRegisterRequest
                                      {
                                          RegistrationSessionID = regSessionID,
                                          EmailAddress = email,
                                          UserName = "mmishkoff",
                                          Password = "1234",
                                          IpAddress = "192.168.0.1",
                                          AttributeData = attributes
                                      };


            var adapter = GetMockAdapter();
            var registerResult = adapter.RegisterExtended(registerRequest);
            Assert.IsTrue(registerResult.RegisterStatus == "Success", "Registration failed");

            var member = MemberSA.Instance.GetMember(registerResult.MemberId, MemberLoadFlags.IngoreSACache);
            Assert.IsNotNull(member, "Member is null");
            Assert.IsTrue(member.EmailAddress == email);

            var brand = BrandConfigSA.Instance.GetBrandByID(1003);

            //supplied attributes
            Assert.IsTrue(member.GetAttributeInt(brand, "gendermask") == 5);
            Assert.IsTrue(member.GetAttributeInt(brand, "ChildrenCount") == 2);
            Assert.IsTrue(member.GetAttributeInt(brand, "JDateReligion") == 2);
            Assert.IsTrue(member.GetAttributeInt(brand, "JDateEthnicity") == 8);
            Assert.IsTrue(member.GetAttributeInt(brand, "PromotionID") == 49098);
            Assert.IsTrue(member.GetAttributeInt(brand, "RegionId") == 9805694);
            Assert.IsTrue(member.GetAttributeInt(brand, "RegistrationScenarioID") == 1);
            Assert.IsTrue(member.GetAttributeInt(brand, "MaritalStatus") == 2);
            Assert.IsTrue(member.GetAttributeInt(brand, "SmokingHabits") == 4);
            Assert.IsTrue(member.GetAttributeInt(brand, "DrinkingHabits") == 2);
            Assert.IsTrue(member.GetAttributeInt(brand, "MoreChildrenFlag") == 1);
            Assert.IsTrue(member.GetAttributeInt(brand, "Height") == 191);
            Assert.IsTrue(member.GetAttributeInt(brand, "IndustryType") == 1073741824);
            Assert.IsTrue(member.GetAttributeInt(brand, "NewsletterMask") == 4);
            Assert.IsTrue(member.GetAttributeInt(brand, "SynagogueAttendance") == 16);
            Assert.IsTrue(member.GetAttributeDate(brand, "Birthdate") == DateTime.Parse("05/09/1975"));
            Assert.IsTrue(member.GetAttributeText(brand, "SiteFirstName") == "Matt");
            Assert.IsTrue(member.GetAttributeText(brand, "RegsistrationSessionID") == regSessionID);
        }

        [Test]
        public void TestGetRegistrationRecapture()
        {
            var email = Guid.NewGuid().ToString() + "@spark.net";
            var scheduledEvent = new ScheduledEvent
                                     {
                                         EmailAddress = email,
                                         ScheduleName = "registration",
                                         GroupID = 1003,
                                         ScheduleDetails = new ScheduleDetail()
                                     };
            scheduledEvent.ScheduleDetails.Add("gendermask", "1");
            scheduledEvent.ScheduleDetails.Add("RegionId", "9805694");
            
            var savedEvent = WCFEmailNotifierSA.Instance.SaveScheduledEvent(scheduledEvent);

            var apiAdapter = GetMockAdapter();
            var retrievedEvent = apiAdapter.GetRegistrationRecpatureInfo(savedEvent.ScheduleGUID);
            Assert.IsNotNull(retrievedEvent, "ScheduledEvent not returned");
            Assert.IsTrue(retrievedEvent.RegistrationFields["gendermask"] == "1", "retrieved event gendermask does not match");
            Assert.IsTrue(retrievedEvent.RegistrationFields["RegionId"] == "9805694", "retrieved event regionid does not match");
        }

        [Test]
        public void TestDefaultPromoMapping()
        {
            var apiAdapter = GetMockAdapter();
            var mappingResult = apiAdapter.MapMapURLToPromo("www.jdate.com");
            Assert.IsNotNull(mappingResult, "mapping result null");
            Assert.IsTrue(mappingResult.PromotionID > 0, "no promotionId returned with mapping result");
            Assert.IsTrue(mappingResult.LuggageID != string.Empty, "no luggageid returned with mapping result");
        }

        [Test]
        public void TestCreateBedrockSession()
        {
            var sessionId = Guid.NewGuid().ToString();
            var apiAdapter = GetMockAdapter();
            apiAdapter.CreateBedrockSession(100171322, sessionId);

            var bedrockSession = Matchnet.Session.ServiceAdapters.SessionSA.Instance.GetSession(sessionId);
            Assert.IsNotNull(bedrockSession, "null session retrieved from bedrock");
            Assert.IsTrue(bedrockSession["MemberID"] == "100171322", "session memberid does not match");
            Assert.IsTrue(bedrockSession.Key.ToString() == sessionId, "session key does not match");
        }

        [Test]
        public void TestGetMemberForSession()
        {
            var apiAdapter = GetMockAdapter();
            var sessionID = apiAdapter.CreateBedrockSession(100171322);
            Assert.IsTrue(sessionID != string.Empty, "Blank sessionid returned from bedrock");
            var memberID = apiAdapter.GetMemberForSession(sessionID);
            Assert.IsTrue(memberID == 100171322, "memberid for session does not match");
        }

        [Test]
        public void TestCreateSessionTrackingRequest()
        {
            var apiAdapter = GetMockAdapter();
            var sessionId = Guid.NewGuid().ToString();

            int sessionTrackingID = apiAdapter.CreateSessionTrackingRequest(sessionId, 55020, "lug", 123, "192.168.2.1", "www.google.com", "456", "789");
            Assert.IsTrue(sessionTrackingID >0);
        }
    }
}
