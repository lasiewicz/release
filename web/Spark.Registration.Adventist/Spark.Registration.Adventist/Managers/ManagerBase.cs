﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.RestConsumer.V2.Models.Content.BrandConfig;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Framework;
using Spark.Registration.Adventist.Framework.Analytics;
using Spark.Registration.Adventist.Framework.Helpers;
using Spark.Registration.Adventist.Framework.HTTPContextWrappers;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Adapters;
using Spark.Registration.Adventist.Interfaces.Persistence;
using Spark.Registration.Adventist.Interfaces.Web;
using Spark.Registration.Adventist.Models;

namespace Spark.Registration.Adventist.Managers
{
    public class ManagerBase
    {
        private IRegistrationValuePersistence _persistence;
        private ICurrentRequest _request;
        private ICurrentResponse _response;
        private IApiAdapter _apiAdapter;
        private ICurrentContext _context;
        private Omniture _omniture;
        private Session _session;
        private ISettingsSA _settingsService;

        public Session UserSession
        {
            get
            {
                if (!CurrentContext.IsNull && CurrentContext.Items["UserSession"] != null)
                {
                    _session = _session ?? CurrentContext.Items["UserSession"] as Session;
                }

                //session should never be null, better for it to be empty
                _session = _session ?? new Session();

                return _session;
            }

            set { _session = value; }
        }

        public IRegistrationValuePersistence Persistence
        {
            get
            {
                _persistence = _persistence ?? UnityHelper.Resolver.GetService(typeof(IRegistrationValuePersistence)) as IRegistrationValuePersistence;
                return _persistence;
            }
            set { _persistence = value; }
        }

        public ICurrentRequest CurrentRequest
        {
            get
            {
                _request = _request ?? UnityHelper.Resolver.GetService(typeof(ICurrentRequest)) as ICurrentRequest;
                return _request;
            }
            set { _request = value; }
        }

        public ICurrentResponse CurrentResponse
        {
            get
            {
                _response = _response ?? UnityHelper.Resolver.GetService(typeof(ICurrentResponse)) as ICurrentResponse;
                return _response;
            }
            set { _response = value; }
        }

        public ICurrentContext CurrentContext
        {
            get
            {
                _context = _context ?? UnityHelper.Resolver.GetService(typeof(ICurrentContext)) as ICurrentContext;
                return _context;
            }
            set { _context = value; }
        }

        public IApiAdapter ApiAdapter
        {
            get
            {
                _apiAdapter = _apiAdapter ?? UnityHelper.Resolver.GetService(typeof(IApiAdapter)) as IApiAdapter;
                return _apiAdapter;
            }
            set { _apiAdapter = value; }
        }

        
        protected Omniture Omniture
        {
            get
            {
                _omniture = CurrentContext.Items["Omniture"] as Omniture;

                if (_omniture == null)
                {
                    _omniture = new Omniture
                    {
                        Variables = new Dictionary<string, string>()
                    };

                    CurrentContext.Items["Omniture"] = _omniture;
                }

                return _omniture;
            }
        }

        public ISettingsSA SettingsService
        {
            get { return _settingsService ?? (_settingsService = RuntimeSettings.Instance); }
            set { _settingsService = value; }
        }

        
        public bool IsSuaLogonEnabled(DeviceType deviceType)
        {
            //for handhelds, use mos sua setting
            string suaSetting = (DeviceType.Handheld == deviceType) ? "MOS_SUA_OAUTH_ENABLED" : "SUA_OAUTH_ENABLED";
            Brand brand = RegistrationConfiguration.Brand;
            return SettingsService.SettingExistsFromSingleton(suaSetting, brand.Site.Community.Id, brand.Site.Id) 
                && SettingsService.GetSettingFromSingleton(suaSetting, brand.Site.Community.Id, brand.Site.Id) == Boolean.TrueString.ToLower();
        }
    }
}
