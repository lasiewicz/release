﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using log4net;
using Spark.Registration.Adventist.Framework.Helpers;
using Spark.Registration.Adventist.Interfaces.Adapters;
using Spark.Registration.Models.Web;

namespace Spark.Registration.Adventist.Managers
{
    public class ValidationManager : ManagerBase
    {
        public bool ValidateRegistrationAttributeValue(string attributeName, string attributeValue)
        {
            return ApiAdapter.ValidateRegistrationAttribute(attributeName, attributeValue);
        }


        public bool ValidateMingleRegistrationAttributeValue(string theAttributeName, string theAttributeValue)
        {
            var isExists = true;

            // get a new client
            var registrationAdapter = UnityHelper.Resolver.GetService(typeof(IMingleRegistrationAdapter)) as IMingleRegistrationAdapter;

           
            if (registrationAdapter != null)
            {
                // validate with Mingle API
                MingleValidationRequest request = new MingleValidationRequest { attributeName = theAttributeName.ToLower(), attributeValue = theAttributeValue.ToLower() };
                isExists = registrationAdapter.ValidateExtended(request);
            }

            return isExists;
        }


    }
}