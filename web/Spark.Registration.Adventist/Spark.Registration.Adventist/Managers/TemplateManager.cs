﻿using Spark.Registration.Adventist.Framework;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Scenarios;

namespace Spark.Registration.Adventist.Managers
{
    public class TemplateManager
    {
        private IScenarioLoader _scenarioLoader;
        
        public IScenarioLoader ScenarioLoader
        {
            get { return _scenarioLoader ?? (_scenarioLoader = new APIScenarioLoader()); }
            set { _scenarioLoader = value; }
        }
        
        public string GetErrorTemplateViewPath()
        {
            return "~/Views/Error/Error.cshtml";
        }

        public string GetSplashTemplateViewPath()
        {
            var scenario = ScenarioLoader.GetCurrentScenario();
            return "~/Views/Splash/" + scenario.SplashTemplate + ".cshtml";
        }

        public string GetConfirmationTemplateViewPath(string scenarioName)
        {
            var scenario = ScenarioLoader.GetNamedScenario(scenarioName);
            if(scenario == null)
            {
                //couldn't find scenario
                return string.Empty;
            }

            return "~/Views/Confirmation/" + scenario.ConfirmationTemplate + ".cshtml";
        }

        public string GetRegistrationTemplateViewPath()
        {
            var scenario = ScenarioLoader.GetCurrentScenario();
            return "~/Views/Registration/" + scenario.RegistrationTemplate + ".cshtml";
        }
    }
}