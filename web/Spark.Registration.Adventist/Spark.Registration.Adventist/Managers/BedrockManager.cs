﻿
namespace Spark.Registration.Adventist.Managers
{
    public class BedrockManager: ManagerBase
    {
        public bool ShouldRedirectUserToBedrock()
        {
            var cookieManager = new CookieManager(); 
            
            var shouldRedirect = cookieManager.BedrockAutoLogonCookiesSet();

            if (!shouldRedirect)
            {
                var bedrockSid = cookieManager.GetSessionFromBedrockCookie();
                if (!string.IsNullOrEmpty(bedrockSid))
                {
                    var memberId = ApiAdapter.GetMemberForSession(bedrockSid);
                    if (memberId >0)
                    {
                        shouldRedirect = true;
                    }

                }
            }

            return shouldRedirect;
        }
    }
}