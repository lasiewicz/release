﻿using System;

using System.Web.Caching;
using Spark.Registration.Adventist.Models;
using Cache = Matchnet.Caching.Cache;

namespace Spark.Registration.Adventist.Managers
{
    public class MembersOnlineManager: ManagerBase
    {
        public int GetMembersOnlineCount()
        {
            int count;
            
            var cachedCount = Cache.Instance.Get(CachedMOLCount.CACHE_KEY) as CachedMOLCount;

            if(cachedCount != null)
            {
                count = cachedCount.Count;
            }
            else
            {
                count = ApiAdapter.GetMembersOnlineCount(); 
                cachedCount = new CachedMOLCount(count);
                Cache.Instance.Add(CachedMOLCount.CACHE_KEY, cachedCount, null, DateTime.Now.AddSeconds(60), System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);
            }

            return count;
        }
    }
}