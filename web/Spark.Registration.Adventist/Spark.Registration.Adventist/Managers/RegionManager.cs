﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Caching;
using Spark.Common.RestConsumer.V2.Models.Content.Region;
using Spark.Registration.Adventist.Models;
using log4net;
using Spark.Registration.Dictionaries.Constants;
using Cache = Matchnet.Caching.Cache;
using Spark.Registration.Adventist.Framework.Helpers;
using Spark.Registration.Adventist.Interfaces.Adapters;
using Spark.Registration.Models.Web;

namespace Spark.Registration.Adventist.Managers
{
    public enum Countries
    {
        Canada = 42,
        France = 83,
        Israel = 114,
        UK = 235,
        USA = 1 
    }

    public enum RegionCacheType
    {
        Countries = 0,
        CitiesByZip=1,
        CitiesByRegion=2,
        StatesByRegion=3
    }

    public class RegionManager : ManagerBase
    {
        private static volatile object _cacheLocker = new object();
        private static readonly ILog Log = LogManager.GetLogger(typeof(RegistrationFlowManager));

        public List<Spark.Registration.Models.Web.Region> GetCountries()
        {
            List<Spark.Registration.Models.Web.Region> countries = null;

            CachedRegionData cachedData = GetCachedData();
            if(cachedData.Countries.Count == 0)
            {

                // get a new client
                var registrationAdapter = UnityHelper.Resolver.GetService(typeof(IMingleRegistrationAdapter)) as IMingleRegistrationAdapter;


                if (registrationAdapter != null)
                {
                    countries = registrationAdapter.GetCountries();
                    var usa = countries.Where(c => c.Id == (int)Countries.USA).First();
                    var canada = countries.Where(c => c.Id == (int)Countries.Canada).First();
                    countries.Remove(usa);
                    countries.Remove(canada);
                    countries.Insert(0, usa);
                    countries.Insert(1, canada);

                    var undefined = countries.Where(c => c.Description.ToLower() == "undefined").FirstOrDefault();
                    if (undefined != null)
                    {
                        countries.Remove(undefined);
                    }

                    CacheData(cachedData, RegionCacheType.Countries, null, countries);
                }
            }
            else
            {
                 countries = cachedData.Countries;
            }

            return countries;
        }

        public List<Spark.Registration.Models.Web.Region> GetCitiesForZipCode(string zipCode)
        {
            List<Spark.Registration.Models.Web.Region> cities;

            CachedRegionData cachedData = GetCachedData();
            if (!cachedData.CitiesByZipCode.Keys.Contains(zipCode))
            {
                cities = new List<Spark.Registration.Models.Web.Region>();
                
                var regions = ApiAdapter.GetRegionsForZip(zipCode);

                foreach (Spark.Common.RestConsumer.V2.Models.Content.Region.Region r in regions)
                {
                    cities.Add(new Registration.Models.Web.Region { Id = r.Id, Description = r.Description });
                }

                CacheData(cachedData, RegionCacheType.CitiesByZip, zipCode, cities);
            }
            else
            {
                cities = cachedData.CitiesByZipCode[zipCode];
            }
            return cities;
        }

        public List<Spark.Registration.Models.Web.Region> GetStates(int countryRegionId)
        {
            List<Spark.Registration.Models.Web.Region> states = null; 

            CachedRegionData cachedData = GetCachedData();
            if (!cachedData.StatesByRegionID.Keys.Contains(countryRegionId))
            {
                // get a new client
                var registrationAdapter = UnityHelper.Resolver.GetService(typeof(IMingleRegistrationAdapter)) as IMingleRegistrationAdapter;

                states = registrationAdapter.GetStatesForCountry(countryRegionId);

                if (states != null && states.Count > 0)
                {
                    int depth = states[0].Depth;
                    
                    if(depth == 3)
                    {
                        //These are cities, not states, so cache them in the city cache and return null
                        var cities = new List<City>();
                        cities.AddRange(from State state in states
                                             select new City
                                             {
                                                 Description = state.Description,
                                                 Id = state.Id, 
                                                 Depth = state.Depth
                                             });
                        
                        CacheData(cachedData, RegionCacheType.CitiesByRegion, countryRegionId, cities);
                        return null;
                    }

                    CacheData(cachedData, RegionCacheType.StatesByRegion, countryRegionId, states);
                }
            }
            else
            {
                 states = cachedData.StatesByRegionID[countryRegionId];
            }

            return states;
        }

        public List<Spark.Registration.Models.Web.Region> GetCities(int countryRegionId, int stateRegionId)
        {
            List<Spark.Registration.Models.Web.Region> cities = null;

            CachedRegionData cachedData = GetCachedData();

            int operativeRegionID = stateRegionId > 0 ? stateRegionId : countryRegionId;

            if (!cachedData.CitiesByRegionID.Keys.Contains(operativeRegionID))
            {

                // get a new client
                var registrationAdapter = UnityHelper.Resolver.GetService(typeof(IMingleRegistrationAdapter)) as IMingleRegistrationAdapter;

                cities = registrationAdapter.GetCitiesForCountryState(countryRegionId, stateRegionId);  // ApiAdapter.GetCitiesForCountryState(countryRegionId, stateRegionId);


                if (cities != null)
                {
                    CacheData(cachedData, RegionCacheType.CitiesByRegion, operativeRegionID, cities);
                }
            }
            else
            {
                cities = cachedData.CitiesByRegionID[operativeRegionID];
            }

            return cities;
        }

        public List<Spark.Registration.Models.Web.Region> GetCitiesContainingString(int countryRegionId, int stateRegionId, string substring)
        {
            List<Spark.Registration.Models.Web.Region> cities = GetCities(countryRegionId, stateRegionId);

            if(cities != null)
            {
                cities = cities.Where(c => c.Description.ToLower().Contains(substring.ToLower())).ToList();
            }

            return cities;
        }

        public RegionData PopulateRegionDataFromPersistence()
        {
            var regionData = new RegionData { Countries = GetCountries() };
            
            if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.RegionCountryID]))
            {
                regionData.CountryRegionID = Convert.ToInt32(Persistence[PersistenceConstants.RegionCountryID]);
            }
            else
            {
                //not set yet, so set to USA
                regionData.CountryRegionID = (int)Countries.USA;
            }

            if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.RegionStateID]))
            {
                regionData.StateRegionID = Convert.ToInt32(Persistence[PersistenceConstants.RegionStateID]);
                regionData.States = GetStates(regionData.CountryRegionID);
                regionData.Cities = GetCities(regionData.CountryRegionID, regionData.StateRegionID);
            }

            if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.RegionZipCode]))
            {
                regionData.ZipCode = Persistence[PersistenceConstants.RegionZipCode];
                regionData.Cities = GetCitiesForZipCode(regionData.ZipCode);

                if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.RegionCityID]))
                {
                    regionData.CityRegionID = Convert.ToInt32(Persistence[PersistenceConstants.RegionCityID]);
                }
            }
            else if (!string.IsNullOrEmpty(Persistence[PersistenceConstants.RegionCityID]))
            {
                regionData.CityRegionID = Convert.ToInt32(Persistence[PersistenceConstants.RegionCityID]);

            }

            return regionData;
        }
        
        private CachedRegionData GetCachedData()
        {
            CachedRegionData cachedData = Cache.Instance.Get(CachedRegionData.CACHE_KEY) as CachedRegionData ?? new CachedRegionData();

            if (cachedData.CitiesByRegionID == null) cachedData.CitiesByRegionID = new Dictionary<int, List<Spark.Registration.Models.Web.Region>>();
            if (cachedData.CitiesByZipCode == null) cachedData.CitiesByZipCode = new Dictionary<string, List<Spark.Registration.Models.Web.Region>>();
            if (cachedData.StatesByRegionID == null) cachedData.StatesByRegionID = new Dictionary<int, List<Spark.Registration.Models.Web.Region>>();
            if (cachedData.Countries == null) cachedData.Countries = new List<Spark.Registration.Models.Web.Region>();
            
            return cachedData;
        }
        
        private void CacheData(CachedRegionData data, RegionCacheType regionCacheType, object cacheKey, object cacheData)
        {
            lock (_cacheLocker)
            {

                try
                {
                    switch (regionCacheType)
                    {
                        case RegionCacheType.CitiesByRegion:
                            if (!data.CitiesByRegionID.Keys.Contains((int)cacheKey))
                            {
                                data.CitiesByRegionID.Add((int)cacheKey, cacheData as List<Spark.Registration.Models.Web.Region>);
                            }
                            break;
                        case RegionCacheType.CitiesByZip:
                            if (!data.CitiesByZipCode.Keys.Contains(cacheKey.ToString()))
                            {
                                data.CitiesByZipCode.Add(cacheKey.ToString(), cacheData as List<Spark.Registration.Models.Web.Region>);
                            }
                            break;
                        case RegionCacheType.StatesByRegion:
                            if (!data.StatesByRegionID.Keys.Contains((int)cacheKey))
                            {
                                data.StatesByRegionID.Add((int)cacheKey, cacheData as List<Spark.Registration.Models.Web.Region>);
                            }
                            break;

                        case RegionCacheType.Countries:
                            data.Countries = cacheData as List<Spark.Registration.Models.Web.Region>;
                            break;
                    }

                    Cache.Instance.Add(CachedRegionData.CACHE_KEY, data, null, DateTime.Now.AddHours(24),
                                       System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Error caching region data. CacheType: {0} CacheKey: {1}", regionCacheType, cacheKey), ex);
                }
            }
        }
    }
}
