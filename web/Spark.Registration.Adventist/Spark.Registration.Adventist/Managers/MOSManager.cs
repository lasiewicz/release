﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Spark.Authentication.OAuth.V2;
using Spark.Common.RestConsumer.V2.Models.Content.BrandConfig;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.OAuth2;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Framework;
using Spark.Registration.Adventist.Framework.HTTPContextWrappers;
using Spark.Registration.Adventist.Framework.Helpers;
using Spark.Registration.Adventist.Interfaces.Adapters;
using Spark.Registration.Adventist.Interfaces.Tracking;
using Spark.Registration.Adventist.Models;
using Spark.Registration.Dictionaries.Constants;

namespace Spark.Registration.Adventist.Managers
{
    public class MOSManager : ManagerBase
    {
        public bool ShouldRedirectToMOS(Session session)
        {
            var cookieManager = new CookieManager();
            var shouldRedirect = false;
            var memberLevelTracking =
                UnityHelper.Resolver.GetService(typeof (IMemberLevelTracking)) as IMemberLevelTracking;

            DeviceType deviceType = memberLevelTracking.GetDeviceType(CurrentRequest, session);
            if (deviceType == DeviceType.Handheld)
            {               
                OAuthTokens tokens;
                if (IsSuaLogonEnabled(deviceType))
                {
                    tokens = SuaLogonManager.GetTokensFromCookies();
                }
                else
                {
                    tokens = LoginManager.Instance.GetTokensFromCookies();                    
                }

                if (!String.IsNullOrEmpty(tokens.AccessToken) && tokens.ExpiresIn > 60 && tokens.MemberId > 0) // token is there and not about to expire
                {
                    shouldRedirect = true;
                }
                if(cookieManager.CheckForSofsCookie())
                {
                    //if 'sofs' cookie exists, load the scenario for FWS instead of MOS.
                    shouldRedirect = false;
                    var persistence = new RegistrationPersistence();
                    var apiAdapter = UnityHelper.Resolver.GetService(typeof(IApiAdapter)) as IApiAdapter;
                    string scenarioname = apiAdapter.GetRandomScenarioName(DeviceType.Desktop);
                    persistence[PersistenceConstants.ScenarioName] = scenarioname;
                }
            }

            return shouldRedirect;
        }
    }
}