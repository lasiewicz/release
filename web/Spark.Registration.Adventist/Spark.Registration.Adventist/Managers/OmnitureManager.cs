﻿using System;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Registration.Adventist.Framework.Helpers;
using Spark.Registration.Adventist.Interfaces.Web;
using Spark.Registration.Adventist.Models;
using Spark.Registration.Adventist.Models.ViewModels;
using Spark.Registration.Dictionaries.Constants;

namespace Spark.Registration.Adventist.Managers
{
    public class OmnitureManager: ManagerBase
    {
        private readonly string _pageName;
        private readonly Scenario _scenario;
        
        public OmnitureManager(string pageName, ICurrentRequest currentRequest, Scenario scenario)
        {
            if (scenario == null)
            {
                throw new ArgumentException("Null scenario passed into OmnitureManager");
            }
            
            _scenario = scenario;
            
            if(scenario.ExternalSessionType == ExternalSessionType.MOS)
            {
                _pageName = RegistrationConfiguration.MOSOmniturePagenamePrefix + pageName;
            }
            else
            {
                _pageName = pageName;
            }
            
            CurrentRequest = currentRequest;
        }

        public OmnitureViewModel GetStepOmnitureModel(int stepID, string sessionID)
        {
            SetCommonVariables();

            Omniture.SetVariable("prop72", _scenario.Name);
            Omniture.SetVariable("prop73", stepID.ToString());
            Omniture.SetVariable("prop74", sessionID);
            //For each reg step, porp10 value should be /registration - stepId
            Omniture.SetVariable("prop10", string.Format("/{0} - {1}", _pageName, stepID));

            //set reg start event on first step
            Omniture.SetVariable("events", stepID == 1 ? "event7" : "");
            
            var viewModel = new OmnitureViewModel
            {
                Variables = Omniture.Variables,
                // If this var isn't used, let's populate it with the action name.
                PageName = _pageName,
                Server = Environment.MachineName,
                PageUrl = CurrentRequest.RawUrl
            };

            return viewModel;
        }

        public OmnitureViewModel GetOmnitureModel()
        {
            SetCommonVariables();

            if (UserSession.MemberId > 0)
            {
                Omniture.SetVariable("prop18",
                                        MemberHelper.GetGenderDescription(
                                            UserSession[SessionConstants.MemberGendermask].ToString()));
                Omniture.SetVariable("prop19",
                                        MemberHelper.GetAge(UserSession[SessionConstants.MemberBirthdate].ToString()).
                                            ToString());
                if (UserSession[SessionConstants.MemberRegionZipCode] != null)
                {
                    Omniture.SetVariable("prop21", UserSession[SessionConstants.MemberRegionZipCode].ToString());
                }
                Omniture.SetVariable("prop22", "Registered");
                Omniture.SetVariable("prop23", UserSession.MemberId.ToString());
            }
            
            var viewModel = new OmnitureViewModel
            {
                Variables = Omniture.Variables,
                // If this var isn't used, let's populate it with the action name.
                PageName = _pageName,
                Server = Environment.MachineName,
                PageUrl = CurrentRequest.RawUrl
            };

            return viewModel;
        }

        private void SetCommonVariables()
        {
            Omniture.SetVariable("prop10", CurrentRequest.Url.PathAndQuery);
            Omniture.SetVariable("prop29", CurrentRequest.UrlReferrer);
            Omniture.SetVariable("eVar48", _scenario.Name);

            if (UserSession["OmnitureFirstPageLoadTime"] == null)
            {
                Omniture.SetVariable("prop9", String.Format("{0:yyyy-MM-dd HH:mm}", DateTime.Now));
                UserSession["OmnitureFirstPageLoadTime"] = "true";
            }

            var hostName = _scenario.ExternalSessionType == ExternalSessionType.Bedrock ? RegistrationConfiguration.BedrockHost : RegistrationConfiguration.MOSHost;
            Omniture.SetVariable("eVar36", hostName);
            
            if (UserSession[SessionConstants.PromotionId] != null)
            {
                Omniture.SetVariable("eVar11", UserSession[SessionConstants.PromotionId].ToString());
                Omniture.SetVariable("campaign", UserSession[SessionConstants.PromotionId].ToString());
            }

            if (UserSession[SessionConstants.EID] != null)
            {
                Omniture.SetVariable("eVar12", UserSession[SessionConstants.EID].ToString());
            }

            if (UserSession[SessionConstants.Luggage] != null)
            {
                Omniture.SetVariable("eVar13", UserSession[SessionConstants.Luggage].ToString());
            }

            if (UserSession[SessionConstants.LandingPageID] != null)
            {
                Omniture.SetVariable("eVar30", UserSession[SessionConstants.LandingPageID].ToString());
            }

            if (UserSession[SessionConstants.Refcd] != null)
            {
                Omniture.SetVariable("eVar38", UserSession[SessionConstants.Refcd].ToString());
            }

            if (UserSession[SessionConstants.TSACR] != null)
            {
                Omniture.SetVariable("eVar39", UserSession[SessionConstants.TSACR].ToString());
            }

            if (UserSession[SessionConstants.Omnivar] != null)
            {
                //it's ok to overwrite evar30 - Omnivar gets precedence over lpid
                Omniture.SetVariable("eVar30", UserSession[SessionConstants.Omnivar].ToString());
            }
            
        }
    }
}
