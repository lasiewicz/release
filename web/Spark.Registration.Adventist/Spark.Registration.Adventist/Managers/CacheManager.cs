﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Caching;
using Matchnet.Caching.CacheBuster;
using Spark.Registration.Adventist.Models;

namespace Spark.Registration.Adventist.Managers
{
    public class CacheManager
    {
        public bool  ClearScenariosCache()
        {
            var cacheBuster = new CacheBuster();
            return cacheBuster.BustCache(CacheableScenarios.CACHE_KEY);
        }
    }
}