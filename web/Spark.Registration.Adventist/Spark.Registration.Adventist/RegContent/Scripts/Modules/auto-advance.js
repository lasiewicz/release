﻿/*
|--------------------------------------------------------------------------
| Form step auto-advance
|--------------------------------------------------------------------------
|
|
|
*/

// reg scenario
var autoAdvance = {
    'config': {
        'container': '#container',
        'elementsAuto': '[type=radio], [type=checkbox], select',
        'elementsIgnored': '',
        'autoFunction': function(){
            // There is no default function for autoAdvance. Please pass in a function during init.
        }
    },
    'init': function (config) {
        // This code is for handling any updates/additions to 'config' that get passed in during init().
        if (config && typeof (config) == 'object') {
            $.extend(autoAdvance.config, config);
        }

        // cache container
        autoAdvance['$cont'] = $(autoAdvance.config.container);

        // bind events
        autoAdvance.bindAutoAdvance();
        autoAdvance.bindKeyDownOff();
        autoAdvance.bindClick();

        // subscribe to the formReady event fired by reg-scenario. This event gets fired when the step forms are ready in the dom.
        $(autoAdvance.config.container).on( "formReady", function(event, data){
            if (data.formEvent === "init" || data.formEvent === "saved" || data.formEvent === "auto-advance-init") {
                // autoAdvance.currentFormItems();
                // cache jQuery form objects, and add a data attribute to autoAdvance form-items
                autoAdvance.$autoAdvanceItems = autoAdvance.$cont.find('.auto-advance').data('changed', false);
                autoAdvance.$formItems = autoAdvance.$cont.find('.form-item');

                // set autoAdvance data flag on container as activated, for pausing if arrow keys are used
                autoAdvance.$cont.data('autoAdvance', 'activated');
            }
        });
    },
    'bindKeyDownOff': function(){
        autoAdvance.$cont.on('keydown.autoAdvance', '.auto-advance', function (event) {
            var $target = $(event.target);

            if(event.which === 37 || event.which === 38 || event.which === 39 || event.which === 40){
                autoAdvance.$cont.data('autoAdvance', 'paused');
            }
        });
    },
    'bindClick': function(){
        autoAdvance.$cont.on('click.autoAdvance', '.auto-advance', function (event) {
            var $target = $(event.target);
         
            if(autoAdvance.$cont.data('autoAdvance') === 'paused'){
                autoAdvance.$cont.data('autoAdvance', 'activated');
                $target.trigger('change');
            }
        });
    },
    'bindAutoAdvance': function(){
        autoAdvance.$cont.on('change.autoAdvance', '.auto-advance', function (event) {
            var $target = $(event.target);

            $target.closest('.form-item').data('changed', true);

            if(autoAdvance.$cont.data('autoAdvance') === 'activated'){
                // use jQuery deferred method to call autoAdvance.verifyCurrentFormItems()
                $.when(autoAdvance.verifyCurrentFormItems()).then(function(valid){
                    if(valid){
                        // setTimeout used to give the user a visual clue that the form has been selected (otherwise the next form step will load)
                        setTimeout(function() {
                            autoAdvance.config.autoFunction();
                        }, 100);
                    }
                });
            }
            
        });
    },
    'verifyCurrentFormItems': function(){
        var formItemChanged = [];
        var allAuto = autoAdvance.$formItems.length === autoAdvance.$autoAdvanceItems.length;

        // check if all form items are auto-advance, and if so, verify each one has changed
        if(allAuto){
            autoAdvance.$autoAdvanceItems.each(function(){
                if($(this).data('changed') === true ){
                    formItemChanged.push(true);
                } else if ($(this).data('changed') === 'undefined') {
                    formItemChanged.push(false);
                }
            });
        }

        return _.all(formItemChanged, function(item){
            return item === true;
        });
    },
    'destroy': function () {
        autoAdvance.$cont.off(".autoAdvance");
    }
};