/*
|--------------------------------------------------------------------------
| Registration scenario renderer
|--------------------------------------------------------------------------
|
| Depedencies: jQuery, mustache.js, underscore, spin.js, LAB.js, counter plugin, region-picker.js, webshims
| Code pattern: Object litteral pattern: http://rmurphey.com/blog/2009/10/15/using-objects-to-organize-your-code/
|
| regFlow is responsible for retrieving metadata from the api, modifying it where needed, rendering it out to the appropriate html template(s), collecting
| any entered data, and submitting it to the api. The API has three calls that simplify the step navigation, NextStep, PreviousStep and CurrentStep. Those
| api calls are bound to the appropriate buttons.
|
| The following sections may be of use (to find, search for quadruple **** plus section title):
| ****SubmitStep data manipulations: For changing the data right before it gets saved to the api
| ****Validation error message data preparation: prepares validation data for render
| ****ControlDisplayType customizations: for customizing data based on template type (such as adding selected attribute to a previously selected option)
|
| Useful arrays:
| 'currentStepItems': Built off of the name attribute. Useful for targeting specific form items in the rendered step. For example, with this array it is easy
| bind a character counter to about me only if about me is present in the step. This saves on downloading/executing unnecessary code. See currentStepEvents()
| for examples of usage.
|
| 'currentStepTemplates': Built off of the ControlDisplayType attribute. Usage is the same as 'currentStepItems' except it is for executing code targeting specific | types of templates (such as all select dropdowns).
*/

// custom webshims validation
$.webshims.addCustomValidityRule('alphanumeric', function (elem, val) {
    if (!val || !elem.getAttribute('data-alphanumeric')) { return; }
    var reg = /^[a-z0-9]+$/i;
    return !reg.test(val);
}, 'Proper value required.');

// custom webshims validation error messages
$.webshims.validityMessages['en'] = {
    typeMismatch: {
        email: 'Proper value required.',
        url: 'Proper value required.',
        number: 'Proper value required.',
        date: 'Proper value required.',
        time: 'Proper value required.',
        range: 'Proper value required.',
        "datetime-local": 'Proper value required.'
    },
    rangeUnderflow: 'Proper value required.',
    rangeOverflow: 'Proper value required.',
    stepMismatch: 'Proper value required.',
    tooLong: 'Proper value required.',

    patternMismatch: 'Proper value required.',
    valueMissing: 'Required.'
};
$.webshims.customErrorMessages["group-required"][''] = "Required.";

// reg scenario
var regFlow = {
    'config': {
        'autoAdvance': false,
        'emailAPIValidation': true, // checks against the attribute api for pre-registered emails
        'usernameAPIValidation': true, // checks against the attribute api for pre-registered usenames
        'removeZeroValues': false, // removed zero based values that act as blank placeholder for dropdowns
        'selectSizeMax': 8, // sets the maximum Size on select tags
        'dataObj': {},
        'savedBirthdayString': "",
        'container': '[role="content"]',
        'steps': 0,
        'currentStep': 1,
        'currentStepblocked': false,
        'currentStepItems': [],
        'currentStepTemplates': [],
        'progress': 0,
        '$tipsContainer': $('[role=content-secondary]'),
        'headerTextID': 'step-display-header',
        'nextButton': '[role=next]',
        'prevButton': '[role=previous]',
        'contButton': '.btn-continue',
        'scriptPath': '../../RegContent/Scripts',
        '$titleElement': $('#regFlowTitle'),
        'titleTextDefault': 'Create Your Profile &mdash; Basics',
        'msgDateDoesNotExist': 'Look again! Please choose a valid date because the one you just picked does not exist.',
        'msgDateTooYoung': 'You must be over 18 years of age to register.',
        'msgDateTooOld': 'You must be between the ages of 18-99 register.',
        'fieldsetHtml': ''
    },
    'init': function (config) {
        // This code is for handling any updates/additions to 'config' that get passed in during init().
        if (config && typeof (config) == 'object') {
            $.extend(regFlow.config, config);
        }

        //cache dom items
        regFlow.config.container = $('#' + regFlow.config.container);

        //get first step json
        regFlow.getStepJSON('CurrentStep');

        // event bindings
        regFlow.config.container.on('keypress.textInputs', 'input[type=text], input[type=password], input[type=email]', function (event) {
            if (event.type === "keypress" && event.which === 13) {
                $(this).blur().checkValidity();
                event.preventDefault();
                regFlow.nextStep();
            }
        });

        regFlow.config.container.on('click.nextButton', regFlow.config.nextButton, function () {
            regFlow.nextStep();
        });
        regFlow.config.container.on('click.contButton', regFlow.config.contButton, function () {
            regFlow.nextStep();
        });
        regFlow.config.container.on('click.prevButton', regFlow.config.prevButton, function () {
            regFlow.prevStep();
        });
    },
    'nextStep': function () {
        regFlow.checkStepValidity('NextStep');
    },
    'prevStep': function () {
        regFlow.getStepJSON('PreviousStep');
    },
    'validateAttributeApi': function (value, attribute) {
        return $.ajax({
            type: "GET",
            url: "/Registration/ValidateAttributeValue",
            data: { 'AttributeName': attribute, 'AttributeValue': value },
            dataType: 'json',
            cache: false
        });
    },
    'validateMingleAttributeValueApi': function (value, attribute) {
        return $.ajax({
            type: "GET",
            url: "/Registration/ValidateMingleAttributeValue",
            data: { 'AttributeName': attribute, 'AttributeValue': value },
            dataType: 'json',
            cache: false
        });
    },
    'checkStepValidity': function (action) {
        // First we validate the form with webshims. If checkValidity passes, the code in the callback is run.
        if ($('form').checkValidity()) {
            // if any form items have special validation, run them
            // email address api
            if (_.include(regFlow.config.currentStepItems, "emailaddress") && regFlow.config.emailAPIValidation === true) {
                var $idemailaddress = $('#idemailaddress');
                $('#btnContinue, [role=next]').addClass('disabled').attr('disabled', 'disabled');
                $('#formItem_emailaddress').find('.form-input').spin('tiny-over');

                // using jQuery's deferred object, we wait for the validateAttributeApi() function to check the api
                $.when(regFlow.validateAttributeApi($idemailaddress.val(), 'EmailAddress'))
                    .then(function (data) {
                        $('#btnContinue, [role=next]').removeClass('disabled').removeAttr('disabled');
                        $('#formItem_emailaddress').find('.form-input').spin(false);

                        if (data.AttributeValueExists === true) {
                            $idemailaddress.removeClass('form-ui-valid').addClass('form-ui-invalid');
                            $.webshims.validityAlert.showFor($idemailaddress, $idemailaddress.data('errormessage').api);
                            return false;
                        } else {
                            regFlow.saveStep(action);
                        }
                    });

                // regionid validations
                // this section could use a refactoring as it is a nested mess
            } else if (_.include(regFlow.config.currentStepItems, "regionid")) {

                if (regionPicker.config.isAutoComplete && regionPicker.config.isAutoCompleteValid) {
                    // if autocomplete is valid, check step validity
                    regFlow.saveStep(action);
                } else if (regionPicker.config.isAutoComplete && !regionPicker.config.isAutoCompleteValid) {
                    // if autocomplete fails, alert the user to validate
                    if ($('#regionCitiesByRegionId').val() !== "") {
                        $(spark).trigger("uiMessaging", [{
                            type: "autocomplete error",
                            content: regionPicker.config.acMsgNoResultsOnChange,
                            selector: "#regionCitiesByRegionId"
                        }]);
                    } else {
                        regFlow.saveStep(action);
                    }
                } else {
                    // if no autocomplete, check validity
                    regFlow.saveStep(action);
                }
                // if no special form elements are present, and checkValidity() passes, execute saveStep()
            } // user name api
            else if (_.include(regFlow.config.currentStepItems, "username") && regFlow.config.usernameAPIValidation === true)
            {
                var $idusername = $('#idusername');
                if (/^[a-zA-Z0-9]{4,26}$/.test($idusername.val()) == false)
                {
                    $idusername.removeClass('form-ui-valid').addClass('form-ui-invalid');
                    $.webshims.validityAlert.showFor($idusername, 'Username must be 4 or more characters and can not contain any special characters. Username must be 26 characters or less');
                    return false;
                }
                else
                {
                    $('#btnContinue, [role=next]').addClass('disabled').attr('disabled', 'disabled');
                    $('#formItem_username').find('.form-input').spin('tiny-over');

                    $.when(regFlow.validateMingleAttributeValueApi($idusername.val(), 'user_username')) //make sure to see that docs https://confluence.matchnet.com/display/SOFTENG/ValidateRegistrationAttribute+API+call to see what attributes names are accepted by the mingle api
                   .then(function (data) {
                       $('#btnContinue, [role=next]').removeClass('disabled').removeAttr('disabled');
                       $('#formItem_username').find('.form-input').spin(false);

                       if (data.AttributeValueExists === true) {
                           $idusername.removeClass('form-ui-valid').addClass('form-ui-invalid');
                           $.webshims.validityAlert.showFor($idusername, $idusername.data('errormessage').api);
                           return false;
                       } else {

                           if (_.include(regFlow.config.currentStepItems, "password")) {
                               //lets do the basical password check first
                               var passwordVal = $('#idpassword').val();
                               if (passwordVal !== "") {
                                   if (/^(?=.*[0-9])[a-zA-Z0-9]{8,16}$/.test(passwordVal) == false || passwordVal == $idusername.val()) {
                                       $('#idpassword').removeClass('form-ui-valid').addClass('form-ui-invalid');
                                       $.webshims.validityAlert.showFor($('#idpassword'), ' Your password does not meet our security requirements. Passwords must be between 8 and 16 characters, and must include at least 1 letter and 1 number, not match your username.');
                                       return false;
                                   }
                                   else
                                       regFlow.saveStep(action);
                               }
                           }
                           else
                               regFlow.saveStep(action);
                       }
                   });
                }
            }
            else if (_.include(regFlow.config.currentStepItems, "firstname") || _.include(regFlow.config.currentStepItems, "lastname"))
            {

                var $idfirstname = $('#idfirstname');
                var $idlastname = $('#idlastname');

                if ($idfirstname)
                {
                    if (/^[a-zA-Z]+$/.test($idfirstname.val()) == false)
                    {
                        $idfirstname.removeClass('form-ui-valid').addClass('form-ui-invalid');
                        $.webshims.validityAlert.showFor($idfirstname, 'First name cannot contain any numbers or special characters');
                        return false;
                    }
                }

                if ($idlastname)
                {
                    if (/^[a-zA-Z]+$/.test($idlastname.val()) == false)
                    {
                        $idlastname.removeClass('form-ui-valid').addClass('form-ui-invalid');
                        $.webshims.validityAlert.showFor($idlastname, 'Last name cannot contain any numbers or special characters');
                        return false;
                    }
                    
                }   
            
                regFlow.saveStep(action);
                
            }
            else {
                regFlow.saveStep(action);
            }
        }
    },
    'saveStep': function (action) {
        // ****SubmitStep data manipulations
        var stepData = $("form").find('input, textarea, select').not('.template-15 :checkbox').serializeArray(),
            checklistData = {},
            stepData2 = [],
            birthdayObj = {},
            lookingforSum = [];

        // make step data first letter uppercase as required by SubmitStep api
        for (var i = 0; i < stepData.length; i++) {
            var item = { Name: stepData[i].name, Value: stepData[i].value };

            // create birthday fields object
            if (stepData[i].name === "birthdayMonth") {
                birthdayObj[stepData[i].name] = stepData[i].value;
            }
            if (stepData[i].name === "birthdayDay") {
                birthdayObj[stepData[i].name] = stepData[i].value;
            }
            if (stepData[i].name === "birthdayYear") {
                birthdayObj[stepData[i].name] = stepData[i].value;
            }

            stepData2.push(item);
        }

        // if there are any checklists in the step
        if (_.include(regFlow.config.currentStepTemplates, 15)) {
            regFlow.config.container.find('.template-15').each(function () {
                var data = $(this).find(':checkbox').serializeArray();
                var checklistSum = 0;

                if (_.isEmpty(data) === false) {

                    _.each(data, function (el, item, list) {
                        checklistSum += parseInt(el.value, 10);
                    });

                    checklistData = { Name: data[0].name, Value: checklistSum.toString() };
                }
            });

            stepData2.push(checklistData);
        }

        // if there are any checkboxes not checked return value as "off"
        if (_.include(regFlow.config.currentStepTemplates, 16) || _.include(regFlow.config.currentStepTemplates, 2)) {
            regFlow.config.container.find('[type=checkbox]:not(:checked)').each(function (index, element) {
                var name = $(element).attr('name'),
                    checkObj = { Name: name, Value: "off" };
                stepData2.push(checkObj);
            });
        }

        //if birthday field object is not empty, create string and add to step data
        if (_.isEmpty(birthdayObj) === false) {
            var birthdayString = birthdayObj.birthdayMonth + '/' + birthdayObj.birthdayDay + '/' + birthdayObj.birthdayYear;
            stepData2.push({ Name: "BirthDate", Value: birthdayString });

            // cache birthdate into config
            regFlow.config.savedBirthdayString = birthdayString;
        }

        // console.group("saved step data");
        //     console.log(stepData2);
        //     console.log(JSON.stringify(stepData2));
        // console.groupEnd();

        $.ajax({
            type: "POST",
            url: "/Registration/SubmitStep",
            //data: stepData,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(stepData2),
            dataType: 'json',
            beforeSend: function () {
                regFlow.prepareStepForRender();
            }
        }).done(function (msg) {
            // console.group("SubmitStep response object");
            //     console.log( msg );
            // console.groupEnd();

            if (msg.Status === 0) {
                // get step based on action
                regFlow.getStepJSON(action);

                // trigger formSaved on step save
                $(regFlow.config.container).trigger("formReady", [{ formEvent: "saved"}]);
            }
            if (msg.Status === 1) {
                // console.log(JSON.stringify(msg));

                var errorObj = {},
                    errorMessagesHtml;

                // put error messages in an object
                errorObj.messages = _.values(msg.ErrorMessages);
                errorMessagesHtml = Mustache.to_html("{{#messages}}<div>{{.}}</div>{{/messages}}", errorObj);

                // reload current step with error message, callback added for error message
                regFlow.getStepJSON('CurrentStep', function () {
                    $(spark).trigger("uiMessaging", [{
                        type: "message error",
                        content: errorMessagesHtml,
                        selector: "[role=content] > h1"
                    }]);
                });
            }
            if (msg.Status === 2 && regFlow.config.currentStep === regFlow.config.steps) {
                // redirect to reg confirmation page: /Confirmation/
                //Append any querystring to the confirmation page
                window.location = "/Confirmation/";
                if (window.location.href.indexOf('?') > 0) {
                    var queryString = window.location.href.slice(window.location.href.indexOf('?') + 1);
                    if (queryString)
                        window.location = "/Confirmation/" + "?" + queryString;
                }
            }
        });
    },
    'getStepJSON': function (action, callback) {
        // Action is either NextStep, PreviousStep, or CurrentStep
        regFlow.prepareStepForRender();

        $.ajax({
            url: '/Registration/' + action,
            dataType: 'json',
            success: function (data) {
                regFlow.config.currentStep = data.CurrentStepNumber;
                regFlow.config.steps = data.TotalStepCount;

                regFlow.renderStep(data);
                if (typeof callback !== "undefined") {
                    callback.call(data);
                }
            },
            cache: false
        });
    },
    'renderStep': function (dataObj) {
        // clear global error messages from incoming step
        if (regFlow.config.currentStep === regFlow.config.steps) {
            // last step, don't clear error
        } else {
            // not last step. turn messaging off
            $(spark).trigger("uiMessaging", [{ selector: "[role=content] > h1", off: true}]);
        }

        // ui updates
        regFlow.updateProgress();
        regFlow.controlButtons();

        var $regFlowFieldset = $('#regFlow').find('#mainFormContainer');

        _.each(dataObj.Step.PopulatedControls, function (el, item, list) {
            var template = $("#form-" + el.Control.ControlDisplayType + "-tpl").html();
            var errorMessageObj = {};

            //convert Name to lower case
            el.Control.Name = el.Control.Name.toLowerCase();

            // build currentStepItems array for applying custom code when specific form item is present
            // for example, see AboutMe text area counter bound in currentStepEvents
            regFlow.config.currentStepItems.push(el.Control.Name);
            // console.group("currentStepItems");
            //     console.log(regFlow.config.currentStepItems);
            // console.groupEnd();

            // build currentStepTemplates array for applying custom code when form item type is present
            regFlow.config.currentStepTemplates.push(el.Control.ControlDisplayType);
            // console.group("currentStepTemplates");
            //     console.log(regFlow.config.currentStepTemplates);
            // console.groupEnd();

            // Loop through Controls
            _.each(el, function (control, item, list) {
                // if EnableAutoAdvance is true, add class to CSSClass property
                // this will need to be updated if another property is used to add an additional css class to the CSSClass property so nothing gets overwritten
                if (control.EnableAutoAdvance === true) {
                    control["CSSClass"] = "auto-advance";
                    regFlow.config.autoAdvance = true;
                }
            });

            // ****Validation error message data preparation
            // Types:
            //      1 MinLength
            //      2 MaxLength
            //      3 RegEx
            //      4 AlphaNumeric
            el.Control["ErrorMessages"] = {};
            if (el.Control.RequiredErrorMessage) {
                el.Control.ErrorMessages["valueMissing"] = el.Control.RequiredErrorMessage;
            }
            if (_.isEmpty(el.Control.Validations) === false) {
                _.each(el.Control.Validations, function (v, item, list) {
                    // console.log(v.ErrorMessage);
                    switch (v.Type) {
                        // MinLength        
                        case 1:
                            v["AttributeName"] = "data-minlength";
                            el.Control.ErrorMessages["tooShort"] = v.ErrorMessage;
                            break;
                        // MaxLength        
                        case 2:
                            //
                            v["AttributeName"] = "maxlength";
                            el.Control.ErrorMessages["tooLong"] = v.ErrorMessage;
                            break;
                        // Pattern        
                        case 3:
                            v["AttributeName"] = "pattern";
                            el.Control.ErrorMessages["patternMismatch"] = v.ErrorMessage;
                            break;
                        // AlphaNumeric        
                        case 4:
                            //
                            v["AttributeName"] = "data-alphanumeric";
                            // // make sure this is set to true so the attribute gets properly added to the element in the template
                            v.Value = "true";
                            el.Control.ErrorMessages["alphanumeric"] = v.ErrorMessage;
                            break;
                        // API attribute test        
                        case 5:
                            // api call to test for previously registered email address and usernames
                            v["AttributeName"] = "data-api";
                            v.Value = "true";
                            el.Control.ErrorMessages["api"] = v.ErrorMessage;
                            break;
                    }
                });
            }
            el.Control.ErrorMessages = JSON.stringify(el.Control.ErrorMessages);

            // ****ControlDisplayType customizations
            // perform data manipulations, loading of custom scripts, etc here
            // Created:
            //     DropdownList=1
            //     CheckBox=2
            //     TextBox=4
            //     TextArea=5
            //     Password=6
            //     Region=8
            //     DateOfBirth=9
            //     File=10
            //     ListBox=12
            //     Info=14
            // Not created:
            //     Captcha=3
            //     RadioList=7
            //     ColorCode=11
            //     RegionAjax=13
            // console.log(el.Control.ControlDisplayType);
            switch (el.Control.ControlDisplayType) {
                // parse data for templates with select options        
                case 1:
                case 7:
                case 12:
                    if (regFlow.config.removeZeroValues === true) {
                        // remove any empty options from the object, otherwise a blank option will appear in the select dropdown
                        _.each(el.Control.Options, function (op, item, list) {
                            if (op.Value <= 0 && op.Description === "") {
                                el.Control.Options.splice(item, 1);
                            }
                        });
                    }
                    _.each(el.Control.Options, function (op, item, list) {

                        var optionValue = op.Value;

                        // convert optionValue to string for comparison to SelectedValue
                        optionValue = '' + optionValue;

                        if (el.SelectedValue === optionValue) {
                            op['Selected'] = true;
                        }

                        // set all zero values to empty "" for client side validation.
                        if (op.Value <= 0 && op.Description === "") {
                            op.Value = "";
                        }

                        // add Size attribute to each Control
                        if (el.Control.Options.length > regFlow.config.selectSizeMax) {
                            el.Control['Size'] = regFlow.config.selectSizeMax;
                        } else if (el.Control.Options.length === 3) {

                            el.Control['Size'] = 4; //hack fix for tablet senario
                        }
                        else
                            el.Control['Size'] = el.Control.Options.length;
                    });
                    break;
                // region picker data        
                case 8:
                    // dataObj.Step.RegionData.CountryRegionID = 1;

                    // convert Id's to strings for consistency with data
                    dataObj.Step.RegionData.CountryRegionID = dataObj.Step.RegionData.CountryRegionID.toString();
                    dataObj.Step.RegionData.StateRegionID = dataObj.Step.RegionData.StateRegionID.toString();
                    dataObj.Step.RegionData.CityRegionID = dataObj.Step.RegionData.CityRegionID.toString();
                    dataObj.Step.RegionData.RegionID = dataObj.Step.RegionData.RegionID.toString();

                    // test data for USA
                    // dataObj.Step.RegionData.CountryRegionID = "1";
                    // dataObj.Step.RegionData.ZipCode = "90025";

                    // test data for Canada
                    // dataObj.Step.RegionData.CountryRegionID = "42";
                    // dataObj.Step.RegionData.ZipCode = "A0A 0A0";

                    // set Selected as true on selected attributes if one exists
                    _.find(dataObj.Step.RegionData.Countries, function (el) {
                        if (el.Id === Number(dataObj.Step.RegionData.CountryRegionID)) {
                            return el["Selected"] = true;
                        }
                    });
                    _.find(dataObj.Step.RegionData.Cities, function (el) {
                        if (el.Id === Number(dataObj.Step.RegionData.CityRegionID)) {
                            return el["Selected"] = true;
                        }
                    });
                    _.find(dataObj.Step.RegionData.States, function (el) {
                        if (el.Id === Number(dataObj.Step.RegionData.StateRegionID)) {
                            return el["Selected"] = true;
                        }
                    });

                    // move the region data to the same level that Options live for consistency
                    el.Control.RegionData = dataObj.Step.RegionData;

                    // remove the original RegionData for cleanup
                    delete dataObj.Step.RegionData;

                    // update the global dataObj with the new dataObj
                    regFlow.config.dataObj = dataObj;

                    break;
                case 9:
                    regFlow.config.savedBirthdayString = el.SelectedValue;
                    break;
                // checklist        
                case 15:
                    var checklistTotal = 0;

                    _.each(el.Control.Options, function (op, item, list) {
                        // add the name to the Options objects for templating
                        op["Name"] = el.Control.Name;

                        // if SelectedValue comes back, parse and add to option as checked attribute
                        if (el.Control.SelectedValue !== "") {
                            if ((op.Value & el.SelectedValue) === op.Value) {
                                op["Selected"] = true;
                            }
                        }
                    });
                    break;
                default:
                    // do nothing
            }
            regFlow.config.fieldsetHtml += Mustache.to_html(template, el);
        });

        regFlow.preStepRender(dataObj);

        regFlow.config.container.find('#regFlow').spin(false);
        regFlow.config.container.find('.btn-continue').show();
        regFlow.config.currentStepblocked = false;

        $regFlowFieldset.html(regFlow.config.fieldsetHtml);

        regFlow.currentStepEvents();

        if (dataObj.OmnitureData && dataObj.OmnitureData.Variables) {
            spark.tracking.addPageName(dataObj.OmnitureData.PageName, true);
            _.each(dataObj.OmnitureData.Variables, function (ov, item, list) {
                if (item) {
                    if (item.indexOf("event") > -1) {
                        spark.tracking.addEvent(ov, true);
                    }
                    else if (item.indexOf("prop") > -1) {
                        var prop = item.substring(4);
                        spark.tracking.addProp(prop, ov, true);
                    }
                    else if (item.indexOf("eVar") > -1) {
                        var evar = item.substring(4);
                        spark.tracking.addEvar(evar, ov, true);
                    }
                }
            });
            spark.tracking.track();
        }
    },
    'controlButtons': function () {
        //disable all buttons buttons
        $(regFlow.config.nextButton).add(regFlow.config.prevButton).addClass('disabled').attr('disabled', 'disabled');

        // enable next button based on step completion
        if (regFlow.config.currentStep < regFlow.config.steps) {
            $(regFlow.config.nextButton).removeClass('disabled').removeAttr('disabled');
        }
        // enable previous as long as step is not the first
        if (regFlow.config.currentStep > 1) {
            $(regFlow.config.prevButton).removeClass('disabled').removeAttr('disabled');
        }
    },
    'updateProgress': function () {
        // calculate progress bar percentage. subtract 1 step from total because that step has not actually been completed (the first step should be at zero percent completion)
        regFlow.config.progress = (regFlow.config.currentStep / regFlow.config.steps * 100) - (1 / regFlow.config.steps * 100);

        $('#progress-position').css('width', regFlow.config.progress + '%').text(regFlow.config.progress + '%');
    },
    'prepareStepForRender': function () {
        if (regFlow.config.currentStepblocked !== true) {
            regFlow.config.currentStepblocked = true;
            $(regFlow.config.nextButton).addClass('disabled').attr('disabled', 'disabled');
            $(regFlow.config.prevButton).addClass('disabled').attr('disabled', 'disabled');
            regFlow.config.container.find('#regFlow').spin('large', '#96b1cc');
            regFlow.config.container.find('.btn-continue').hide();
            regFlow.config.container.find('fieldset').empty();

            //clear current step item array, template array, fieldsetHtml
            regFlow.config.currentStepItems = [];
            regFlow.config.currentStepTemplates = [];
            regFlow.config.fieldsetHtml = "";
        }
    },
    'preStepRender': function (dataObj) {
        // inject title metadata into registration step H1
        if (dataObj.Step.TitleText !== "" && typeof dataObj.Step.TitleText !== "undefined") {
            regFlow.config.$titleElement.html(dataObj.Step.TitleText);
        } else {
            regFlow.config.$titleElement.html(regFlow.config.titleTextDefault);
        }

        // inject tips metadata into tips container
        if (!dataObj.Step.TipText) {
            regFlow.config.$tipsContainer.find('.tips').hide();
        }
        else {
            regFlow.config.$tipsContainer.find('.tips').show();
            regFlow.config.$tipsContainer.find('.tips .tips-content').html(dataObj.Step.TipText);
        }

        //add step header text into template html
        if (dataObj.Step.HeaderText !== "" && typeof dataObj.Step.HeaderText !== "undefined") {
            var headerTextHtml = "<div id='" + regFlow.config.headerTextID + "' class='header-text header-text-" + regFlow.config.currentStep + "'>" + dataObj.Step.HeaderText + "</div>";
            regFlow.config.fieldsetHtml = headerTextHtml + regFlow.config.fieldsetHtml;
        }
    },
    'currentStepEvents': function () {
        // initialize auto advance module
        // if(dataObj.EnableAutoAdvance === true && typeof autoAdvance === "undefined"){
        if (regFlow.config.autoAdvance === true && typeof autoAdvance === "undefined") {
            // set remove zero values to true
            regFlow.config.removeZeroValues = true;

            $LAB.script(function () {
                if (typeof autoAdvance == "undefined") return regFlow.config.scriptPath + "/Modules/auto-advance.js";
            }).wait(function () {
                autoAdvance.init({
                    container: regFlow.config.container,
                    autoFunction: function () {
                        regFlow.nextStep();
                    }
                });
                // trigger formReady on auto-advance-init
                $(regFlow.config.container).trigger("formReady", [{ formEvent: "auto-advance-init"}]);
            });
        }

        // Add character counts to aboutme textarea
        if (_.include(regFlow.config.currentStepItems, "aboutme")) {


            //clearing out the aboutme section. 
            if ($('#idaboutme').val().match(/^[a-zA-Z0-9]+/)) {
                $('#idaboutme').val($.trim($('#idaboutme').val()))   //trim 
            }
            else {
                $('#idaboutme').val('');
            }


            $LAB.script(function () {
                if (typeof $.fn.simplyCountable == "undefined") return regFlow.config.scriptPath + "/Libs/jQuery-plugins/jquery.simplyCountable.js";
            }).wait(function () {
                $('#idaboutme').simplyCountable({
                    counter: '.count-counter',
                    countType: 'characters',
                    maxCount: 50,
                    countDirection: 'up',
                    overClass: 'count-reached'
                });
            });
        }

        // Parse birthdate string and set defaults in dropdowns
        if (_.include(regFlow.config.currentStepItems, "birthdate")) {
            if (regFlow.config.savedBirthdayString !== "") {
                var birthdayArray = regFlow.config.savedBirthdayString.split("/");
                $("#birthdayMonth option[value=" + birthdayArray[0] + "]").attr("selected", "selected");
                $("#birthdayDay option[value=" + birthdayArray[1] + "]").attr("selected", "selected");
                $("#birthdayYear option[value=" + birthdayArray[2] + "]").attr("selected", "selected");
            }
            //load moment.js, if not already loaded, validate birthdate on change
            $LAB.script(function () {
                if (typeof Moment == "undefined") return regFlow.config.scriptPath + "/Libs/moment.1.6.2.min.js";
            }).wait(function () {
                $('#birthdayMonth,#birthdayDay,#birthdayYear').on('change', function () {
                    var $month = $('#birthdayMonth'),
                        $day = $('#birthdayDay'),
                        $year = $('#birthdayYear');

                    if ($month.val() !== "" && $day.val() !== "" && $year.val() !== "") {
                        var birthdayString = $month.val() + '/' + $('#birthdayDay').val() + '/' + $('#birthdayYear').val(),
                            birthday = moment(birthdayString, 'MM-DD-YYYY'),
                            now = new Date(),
                            age = moment(now).diff(birthday, 'years', true);

                        if (birthday.isValid() === false) {
                            // reset day to prevent submitting failed birthdate
                            $('#birthdayDay').val('');

                            //trigger error message
                            $.webshims.validityAlert.showFor($('#birthdayDay'), regFlow.config.msgDateDoesNotExist);
                        }

                        if (age < 18) {
                            // reset day to prevent submitting failed birthdate, blur() forces style update to invalid
                            $('#birthdayDay').val('').blur();
                            $('#birthdayMonth').val('').blur();
                            $('#birthdayYear').val('').blur();

                            //trigger error message
                            $.webshims.validityAlert.showFor($('#birthdayMonth'), regFlow.config.msgDateTooYoung);
                        }

                        if (age >= 100) {
                            // reset day to prevent submitting failed birthdate, blur() forces style update to invalid
                            $('#birthdayDay').val('').blur();
                            $('#birthdayMonth').val('').blur();
                            $('#birthdayYear').val('').blur();

                            //trigger error message
                            $.webshims.validityAlert.showFor($('#birthdayMonth'), regFlow.config.msgDateTooOld);
                        }
                    }
                });
            });
        }

        // Initialize region picker
        // console.log(regFlow.config.currentStepItems);
        if (_.include(regFlow.config.currentStepItems, "regionid")) {
            if (typeof regionPicker == "undefined") {
                $LAB
                .script(regFlow.config.scriptPath + "/Modules/region-picker.js")
                .wait(function () {
                    regionPicker.init({
                        container: "#regFlow",
                        incomingData: regFlow.config.dataObj
                    });
                });
            } else {
                // if the region-picker is already initialized, all we need to do is mimic the data update in regionPicker.init, and run initialRender()
                // update data
                _.each(regFlow.config.dataObj.Step.PopulatedControls, function (el, item, list) {
                    if (el.Control.RegionData) {
                        regionPicker.regionData = el.Control.RegionData;
                    }
                });

                // run renderInitial step
                regionPicker.initialRender();
            }
        }

        // Group-required template specific customizations
        if (_.include(regFlow.config.currentStepTemplates, 15)) {

            // re-trigger group-required validations
            $('.group-required').trigger('refreshCustomValidityRules');

            // Put group-required custom error message position after last checkbox instead of first
            $('.template-15').each(function () {
                var $group = $(this);

                $group.find('.group-required').bind('firstinvalid', function (e) {
                    $.webshims.validityAlert.showFor($group.find('.group-required:last'));
                    return false;
                });
            });


            $('.group-required').css('box-shadow', 'none');




        }

        // Bind autoGrow only if inputs are on step
        if (_.include(regFlow.config.currentStepTemplates, 4)) {
            $LAB.script(function () {
                if (typeof $.fn.autoGrowInput == "undefined") return regFlow.config.scriptPath + "/Libs/jQuery-plugins/jquery.autoGrow.min.js";
            }).wait(function () {
                $('input[type=text]').autoGrowInput({
                    comfortZone: 20,
                    minWidth: 0,
                    maxWidth: 390
                });
                $('input[type=text]').trigger('update');
            });
        }

        // set focus on first element in step
        regFlow.config.container.find('input, select, textarea').filter(':first').focus();

        // trigger formReady on step load
        $(regFlow.config.container).trigger("formReady", [{ formEvent: "formDomReady"}]);
    }
};

regFlow.init();
