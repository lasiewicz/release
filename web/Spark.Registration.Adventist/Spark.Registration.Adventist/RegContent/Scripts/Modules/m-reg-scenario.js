﻿/*
|--------------------------------------------------------------------------
| Mobile registration scenario renderer
|--------------------------------------------------------------------------
|
| Depedencies: jQuery, mustache.js, underscore, counter plugin, region-picker.js, webshims
| Code pattern: Object litteral pattern: http://rmurphey.com/blog/2009/10/15/using-objects-to-organize-your-code/
|
| regFlow is responsible for retrieving metadata from the api, modifying it where needed, rendering it out to the appropriate html template(s), collecting
| any entered data, and submitting it to the api. The API has three calls that simplify the step navigation, NextStep, PreviousStep and CurrentStep. Those
| api calls are bound to the appropriate buttons.
|
| The following sections may be of use (to find, search for quadruple **** plus section title):
| ****SubmitStep data manipulations: For changing the data right before it gets saved to the api
| ****Validation error message data preparation: prepares validation data for render
| ****ControlDisplayType customizations: for customizing data based on template type (such as adding selected attribute to a previously selected option)
|
| Useful arrays:
| 'currentStepItems': Built off of the name attribute. Useful for targeting specific form items in the rendered step. For example, with this array it is easy
| bind a character counter to about me only if about me is present in the step. This saves on downloading/executing unnecessary code. See currentStepEvents()
| for examples of usage.
|
| 'currentStepTemplates': Built off of the ControlDisplayType attribute. Usage is the same a s 'currentStepItems' except it is for executing code targeting specific | types of templates (such as all select dropdowns).
*/



adjustProgressBarUp = function () {
    var str = $("h2#regFlowTitle").text();
    var pattern = /[0-9]+/;
    var matches = str.match(pattern);
    var percent = Math.floor(Number(matches[0]) + 1) / 11 * 100;
    $("h2#regFlowTitle").css("width", percent + "%");
};

adjustProgressBarDown = function () {
    var str = $("h2#regFlowTitle").text();
    var pattern = /[0-9]+/;
    var matches = str.match(pattern);
    var percent = Math.floor(Number(matches[0]) - 1) / 11 * 100;
    $("h2#regFlowTitle").css("width", percent + "%");
};

// custom webshims validation
$.webshims.addCustomValidityRule('alphanumeric', function(elem, val){
    if(!val || !elem.getAttribute('data-alphanumeric')){return;}
    var reg = /^[a-z0-9]+$/i;
    return !reg.test( val );
}, 'Proper value required.');

// custom webshims validation error messages
$.webshims.validityMessages['en'] = {
    typeMismatch: {
        email: 'Proper value required.',
        url: 'Proper value required.',
        number: 'Proper value required.',
        date: 'Proper value required.',
        time: 'Proper value required.',
        range: 'Proper value required.',
        "datetime-local": 'Proper value required.'
    },
    rangeUnderflow: 'Proper value required.',
    rangeOverflow: 'Proper value required.',
    stepMismatch: 'Proper value required.',
    tooLong: 'Proper value required.',
    
    patternMismatch: 'Proper value required.',
    valueMissing: 'Required.'
};
$.webshims.customErrorMessages["group-required"][''] = "Required.";

// reg scenario
var regFlow = {
    'config': {
        'autoAdvance': false,
        'emailAPIValidation': true, // checks against the attribute api for pre-registered emails
        'removeZeroValues': true, // removed zero based values that act as blank placeholder for dropdowns
        'selectSizeMax': 8, // sets the maximum Size on select tags
        'dataObj': {},
        'savedBirthdayString': "",
        'container': '[data-role="content"]',
        'steps': 0,
        'stepControls': '#stepControls',
        // 'initialRender': true,
        'currentStep': 1,
        'currentStepblocked': false,
        'currentStepItems': [],
        'currentStepTemplates': [],
        'nextButton': '#formContinue',
        'prevButton': '#formPrevious',
        'scriptPath': '../../RegContent/Scripts',
        '$titleElement': $('#regFlowTitle'),
        'titleTextDefault': 'Create Your Profile &mdash; Basics',
        'titleTemplate': "Registration - <%= currentStep %> of <%= totalSteps %>",
        'msgDateDoesNotExist': 'Look again! Please choose a valid date because the one you just picked does not exist.',
        'msgDateTooYoung': 'You must be over 18 years of age to register.',
        'msgDateTooOld': 'You must be between the ages of 18-99 register.',
        'fieldsetHtml': ''
    },
    'init': function (config) {
        // This code is for handling any updates/additions to 'config' that get passed in during init().
        if (config && typeof (config) == 'object') {
            $.extend(regFlow.config, config);
        }

        //cache dom items
        regFlow.config.container = $('#' + regFlow.config.container);

        //get first step json
        regFlow.getStepJSON('CurrentStep');

        // event bindings
        regFlow.config.container.on('click.nextButton', regFlow.config.nextButton, function () {
            regFlow.nextStep();
        });
        regFlow.config.container.on('click.prevButton', regFlow.config.prevButton, function () {
            regFlow.prevStep();
        });
    },
    'nextStep': function(){
        regFlow.checkStepValidity('NextStep');
        
    },
    'prevStep': function(){
        regFlow.getStepJSON('PreviousStep');
        adjustProgressBarDown();
    },
    'validateAttributeApi': function(value, attribute){
        return $.ajax({
            type: "GET",
            url: "/Registration/ValidateAttributeValue",
            data: { 'AttributeName': attribute, 'AttributeValue': value },
            dataType: 'json'
        });
    },

    'validate': function (container) {
        var id = container.attr("id");
        var action = "NextStep";

        // Validate Gender Seeking section (Page 1)
        if (id === "formItem_newslettermask") {
            var parent = container.parent();
            container = parent.find("#formItem_termsandconditions");
            if (container.find("#idtermsandconditions").prop('checked')) {
                regFlow.saveStep(action);
            } else {
                $.webshims.validityAlert.showFor(container.find("#idtermsandconditions"), "Can you just give us a little something? This field is required so you kind of have to!");
            }

        } else if (id === "formItem_regionid") {
            // Validate ZipCode
            var zipCode = $("#regionzipcode");
            var pattern = zipCode.attr("pattern");
            if ($.trim(zipCode.val()) !== "" && zipCode.val().match(pattern)) {
                // Validate email address.
                var email = $("#idemailaddress");

                pattern = email.attr("pattern");
                if ($.trim(email.val()) !== "") {
                    if (email.val().match(pattern)) {
                        $.get("/Registration/ValidateAttributeValue", { 'AttributeName': "EmailAddress", 'AttributeValue': email.val() }, function (data) {
                            if (data.AttributeValueExists) {
                                $.webshims.validityAlert.showFor(email, email.data('errormessage').api);
                            } else {
                                regFlow.saveStep(action);
                            }
                        }, "json").fail(function () {

                        });
                    } else {
                        $.webshims.validityAlert.showFor(email, email.data('errormessage').patternMismatch);
                    }
                } else {
                    $.webshims.validityAlert.showFor(email, email.data('errormessage').valueMissing);
                }
            } else {
                $.webshims.validityAlert.showFor(zipCode, zipCode.data('errormessage'));
            }
        } else if (id === "formItem_username") {
            // Validate username
            var username = $("#idusername");
            if ($.trim(username.val()) !== "") {
                if (/^[a-z0-9]+$/i.test(username.val())) {
                    if (username.val().length > 3) {
                        // Validate password
                        var password = $("#idpassword");
                        if ($.trim(password.val()) !== "") {
                            if (/^[a-z0-9]+$/i.test(password.val())) {
                                if (password.val().length > 3) {
                                    // Validate birthdate
                                    var birthDate = $("#idbirthdate");
                                    if (birthDate.length === 0) {
                                        regFlow.saveStep(action);

                                        return;
                                    }

                                    if ($.trim(birthDate.val()) !== "") {
                                        var minDate = new Date(birthDate.attr("min"));
                                        var maxDate = new Date(birthDate.attr("max"));
                                        var temp = new Date(birthDate.val());

                                        if (temp < minDate || temp > maxDate) {
                                            $.webshims.validityAlert.showFor(birthDate, "Proper value required.");
                                        } else {
                                            regFlow.saveStep(action);
                                        }
                                    } else {
                                        $.webshims.validityAlert.showFor(birthDate, birthDate.data('errormessage').valueMissing);
                                    }

                                } else {
                                    $.webshims.validityAlert.showFor(password, password.data('errormessage').tooShort);
                                }
                            } else {
                                $.webshims.validityAlert.showFor(password, password.data('errormessage').alphanumeric);
                            }
                        } else {
                            $.webshims.validityAlert.showFor(password, password.data('errormessage').valueMissing);
                        }
                    } else {
                        $.webshims.validityAlert.showFor(username, username.data('errormessage').tooShort);
                    }
                } else {
                    $.webshims.validityAlert.showFor(username, username.data('errormessage').alphanumeric);
                }
            } else {
                $.webshims.validityAlert.showFor(username, username.data('errormessage').valueMissing);
            }
        } else if (id === "formItem_lookingfor") {
            if (container.find("input[type='checkbox']:checked").length === 0) {
                $.webshims.validityAlert.showFor($("#lookingfor2"), "Can you just give us a little something? This field is required so you kind of have to!");
            } else {
                regFlow.saveStep(action);
            }
        } else {
            // Validate radio button group.
            var e = container.find("input[type='radio']:eq(0)");

            if (container.find("input[type='radio']:checked").length === 0) {
                $.webshims.validityAlert.showFor(e, "Can you just give us a little something? This field is required so you kind of have to!");
            } else {
                regFlow.saveStep(action);
            }
        }
    },

    'showError': function (section, e) {
        var errorMessage;

        if (section === "gendermask") {
            errorMessage = e.attr("data-errormessage");
        }

        $.webshims.validityAlert.showFor(e, errorMessage);
    },

    'checkStepValidity' : function(action){
        var frm = $("#regFlow");
        var container = frm.find("div:first-child > div");

        regFlow.validate(container);
    },
    'saveStep': function (action) {
        // if step is empty, don't try to send to server
        if(regFlow.config.currentStepItems.length === 0){
            return false;
        }

        // ****SubmitStep data manipulations
        var stepData = $("form").find('input, textarea, select').not('.template-15 :checkbox, .template-16 :checkbox, .template-2 :checkbox').serializeArray(),
            checklistData = {},
            stepData2 = [],
            birthdayObj = {},
            lookingforSum = [];

        // make step data first letter uppercase as required by SubmitStep api
        for (var i = 0; i < stepData.length; i++) {
            var item = { Name: stepData[i].name, Value: stepData[i].value };

            // create birthday fields object
            if (stepData[i].name === "birthdayMonth") {
                birthdayObj[stepData[i].name] = stepData[i].value;
            }
            if (stepData[i].name === "birthdayDay") {
                birthdayObj[stepData[i].name] = stepData[i].value;
            }
            if (stepData[i].name === "birthdayYear") {
                birthdayObj[stepData[i].name] = stepData[i].value;
            }

            // checking the answer for gendermask and storing it for later use in the MobileConfirmation page. 
            if (stepData[i].name === "gendermask") {
                sessionStorage.setItem("gendermask", stepData[i].value);
            }

            stepData2.push(item);
        }

        // if there are any checklists in the step
        if (_.include(regFlow.config.currentStepTemplates, 15)) {
            regFlow.config.container.find('.template-15').each(function () {
                var data = $(this).find(':checkbox').serializeArray();
                var checklistSum = 0;

                if (_.isEmpty(data) === false) {

                    _.each(data, function (el, item, list) {
                        checklistSum += parseInt(el.value, 10);
                    });

                    checklistData = { Name: data[0].name, Value: checklistSum.toString() };
                }
            });

            stepData2.push(checklistData);
        }

        // if there are any checkboxes not checked return value as "off"
        if (_.include(regFlow.config.currentStepTemplates, 16) || _.include(regFlow.config.currentStepTemplates, 2)) {
            regFlow.config.container.find('[type=checkbox]').each(function (index, element) {
                var name = $(this).attr('name'),
                    checkObj = {};

                if($(this).is(':checked')){
                 // perform operation for checked
                    checkObj = { Name: name, Value: "on" };
                }
                else{
                 // perform operation for unchecked
                    checkObj = { Name: name, Value: "off" };
                }
                stepData2.push(checkObj);
            });
        }

        //if birthday field object is not empty, create string and add to step data
        if (_.isEmpty(birthdayObj) === false) {
            var birthdayString = birthdayObj.birthdayYear + '-' + birthdayObj.birthdayMonth + '-' + birthdayObj.birthdayDay;
            stepData2.push({ Name: "BirthDate", Value: birthdayString });

            // cache birthdate into config
            regFlow.config.savedBirthdayString = birthdayString;
        }

        // console.group("saved step data");
        //     console.log(stepData2);
        //     console.log(JSON.stringify(stepData2));
        // console.groupEnd();

        $.ajax({
            type: "POST",
            url: "/Registration/SubmitStep",
            //data: stepData,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(stepData2),
            dataType: 'json',
            beforeSend: function () {
                regFlow.prepareStepForRender();
            }
        }).done(function (msg) {
            // console.group("SubmitStep response object");
            //     console.log( msg );
            // console.groupEnd();

            if (msg.Status === 0) {
                // get step based on action
                regFlow.getStepJSON(action);
                if ($("h2#regFlowTitle").hasClass("v2")) {
                    adjustProgressBarUp();
                }
                // trigger formSaved on step save
                $(regFlow.config.container).trigger( "formReady", [{formEvent:"saved"}]);
            }
            if (msg.Status === 1) {
                // console.log(JSON.stringify(msg));

                var errorObj = {},
                    errorMessagesHtml;

                // put error messages in an object
                errorObj.messages = _.values(msg.ErrorMessages);
                errorMessagesHtml = Mustache.to_html("{{#messages}}<div>{{.}}</div>{{/messages}}", errorObj);

                // reload current step with error message, callback added for error message
                regFlow.getStepJSON('CurrentStep', function () {
                    $(spark).trigger("uiMessaging", [{
                        type: "message page-message error",
                        content: errorMessagesHtml,
                        selector: "[data-role=page].ui-page > header"
                    }]);
                });
            }
            if (msg.Status === 2 && regFlow.config.currentStep === regFlow.config.steps) {
                // redirect to reg confirmation page: /Confirmation/
                window.location = "/Confirmation/";
            }
        });
    },
    'getStepJSON': function (action, callback) {
        // Action is either NextStep, PreviousStep, or CurrentStep
        regFlow.prepareStepForRender();

        $.ajax({
            url: '/Registration/' + action,
            dataType: 'json',
            success: function(data){
                regFlow.config.currentStep = data.CurrentStepNumber;
                regFlow.config.steps = data.TotalStepCount;

                regFlow.renderStep(data);
                if (typeof callback !== "undefined") {
                    callback.call(data);
                }
            },
            cache: false
        });
    },
    'renderStep': function (dataObj) {
        // clear global error messages from incoming step
        if(regFlow.config.currentStep === regFlow.config.steps){
           // last step, don't clear error
        } else {
           // not last step. turn messaging off
           $(spark).trigger("uiMessaging", [{ selector: "[data-role=page].ui-page > header", off: true }]);
        }

        // ui updates
        regFlow.controlButtons();

        var $regFlowFieldset = $('#regFlow').find('#formContainer');

        _.each(dataObj.Step.PopulatedControls, function (el, item, list) {

            var template = $("#form-" + el.Control.ControlDisplayType + "-tpl").html();

            //convert Name to lower case
            el.Control.Name = el.Control.Name.toLowerCase();

            // build currentStepItems array for applying custom code when specific form item is present
            // for example, see AboutMe text area counter bound in currentStepEvents
            regFlow.config.currentStepItems.push(el.Control.Name);
            // console.group("currentStepItems");
            //     console.log(regFlow.config.currentStepItems);
            // console.groupEnd();
            
            // Loop through Controls
            _.each(el, function(control, item, list){
                // if EnableAutoAdvance is true, add class to CSSClass property
                // this will need to be updated if another property is used to add an additional css class to the CSSClass property so nothing gets overwritten
                if(control.EnableAutoAdvance === true){
                    control["CSSClass"] = "auto-advance";
                    regFlow.config.autoAdvance = true;
                }
            });

            // build currentStepTemplates array for applying custom code when form item type is present
            regFlow.config.currentStepTemplates.push(el.Control.ControlDisplayType);
            // console.group("currentStepTemplates");
            //     console.log(regFlow.config.currentStepTemplates);
            // console.groupEnd();

            // ****Validation error message data preparation
            // Types:
            //      1 MinLength
            //      2 MaxLength
            //      3 RegEx
            //      4 AlphaNumeric
            el.Control["ErrorMessages"] = {};
            if(el.Control.RequiredErrorMessage){
                el.Control.ErrorMessages["valueMissing"] = el.Control.RequiredErrorMessage;
            }
            if(_.isEmpty(el.Control.Validations) === false){
                _.each(el.Control.Validations, function (v, item, list) {
                    // console.log(v.ErrorMessage);
                    switch (v.Type) {
                        // MinLength
                        case 1:
                            v["AttributeName"] = "data-minlength";
                            el.Control.ErrorMessages["tooShort"] = v.ErrorMessage;
                            break;
                        // MaxLength
                        case 2:
                            //
                            v["AttributeName"] = "maxlength";
                            el.Control.ErrorMessages["tooLong"] = v.ErrorMessage;
                            break;
                        // Pattern
                        case 3:
                            v["AttributeName"] = "pattern";
                            el.Control.ErrorMessages["patternMismatch"] = v.ErrorMessage;
                            break;
                        // AlphaNumeric
                        case 4:
                            //
                            v["AttributeName"] = "data-alphanumeric";
                            // // make sure this is set to true so the attribute gets properly added to the element in the template
                            v.Value = "true";
                            el.Control.ErrorMessages["alphanumeric"] = v.ErrorMessage;
                            break;
                        // API attribute test
                        case 5:
                            // api call to test for previously registered email address and usernames
                            v["AttributeName"] = "data-api";
                            v.Value = "true";
                            el.Control.ErrorMessages["api"] = v.ErrorMessage;
                            break;
                    }
                });
            }
            el.Control.ErrorMessages = JSON.stringify(el.Control.ErrorMessages);

            // ****ControlDisplayType customizations
            // perform data manipulations, loading of custom scripts, etc here
            // Created:
            //     DropdownList=1
            //     CheckBox=2
            //     TextBox=4
            //     TextArea=5
            //     Password=6
            //     Region=8
            //     DateOfBirth=9
            //     File=10
            //     ListBox=12
            //     Info=14
            // Not created:
            //     Captcha=3
            //     RadioList=7
            //     ColorCode=11
            //     RegionAjax=13
            // console.log(el.Control.ControlDisplayType);
            switch (el.Control.ControlDisplayType) {
                // parse data for templates with select options
                case 1:
                case 7:
                case 12:
                    if (regFlow.config.removeZeroValues === true) {
                        // remove any empty options from the object, otherwise a blank option will appear in the select dropdown
                        _.each(el.Control.Options, function (op, item, list) {
                            if (op.Value <= 0 && op.Description === "") {
                                el.Control.Options.splice(item, 1);
                            }
                        });
                    }
                    
                    _.each(el.Control.Options, function (op, item, list) {

                        // replace backslashes in iOS <= 5 Safari bug where radio button controls were not honoring word-wrap: break-word
                        if(el.Control.ControlDisplayType === 7){
                            op.Description = op.Description.replace(/\//g, " / ");
                        }

                        var optionValue = op.Value;

                        // convert optionValue to string for comparison to SelectedValue
                        optionValue = '' + optionValue;

                        if (el.SelectedValue === optionValue) {
                            op['Selected'] = true;
                        }

                        // must add Required to first radio option only, for use in webshims validation
                        if(el.Control.ControlDisplayType === 7 && item === 0){
                            op['RequiredAttr'] = true;
                        }

                        // add Size attribute to each Control
                        if(el.Control.Options.length > regFlow.config.selectSizeMax){
                            el.Control['Size'] = regFlow.config.selectSizeMax;
                        } else {
                            el.Control['Size'] = el.Control.Options.length;
                        }
                    });
                    break;
                // checkboxes
                case 2:
                    if(el.SelectedValue === "on"){
                        el.Control["Checked"] = "checked";
                    }
                    if(el.SelectedValue === "off" || el.SelectedValue === ""){
                        el.Control["Checked"] = "";
                    }
                    break;
                // region picker data
                case 8:
                    // dataObj.Step.RegionData.CountryRegionID = 223;

                    // convert Id's to strings for consistency with data
                    dataObj.Step.RegionData.CountryRegionID = dataObj.Step.RegionData.CountryRegionID.toString();
                    dataObj.Step.RegionData.StateRegionID = dataObj.Step.RegionData.StateRegionID.toString();
                    dataObj.Step.RegionData.CityRegionID = dataObj.Step.RegionData.CityRegionID.toString();
                    dataObj.Step.RegionData.RegionID = dataObj.Step.RegionData.RegionID.toString();

                    // test data for USA
                    // dataObj.Step.RegionData.CountryRegionID = "223";
                    // dataObj.Step.RegionData.ZipCode = "90025";

                    // test data for Canada
                    // dataObj.Step.RegionData.CountryRegionID = "38";
                    // dataObj.Step.RegionData.ZipCode = "A0A 0A0";

                    // set Selected as true on selected attributes if one exists
                    _.find(dataObj.Step.RegionData.Countries, function (el) {
                        if (el.Id === Number(dataObj.Step.RegionData.CountryRegionID)) {
                            return el["Selected"] = true;
                        }
                    });
                    _.find(dataObj.Step.RegionData.Cities, function (el) {
                        if (el.Id === Number(dataObj.Step.RegionData.CityRegionID)) {
                            return el["Selected"] = true;
                        }
                    });
                    _.find(dataObj.Step.RegionData.States, function (el) {
                        if (el.Id === Number(dataObj.Step.RegionData.StateRegionID)) {
                            return el["Selected"] = true;
                        }
                    });

                    // move the region data to the same level that Options live for consistency
                    dataObj.Step.PopulatedControls[0].Control["RegionData"] = dataObj.Step.RegionData;

                    // remove the original RegionData for cleanup
                    delete dataObj.Step.RegionData;

                    // update the global dataObj with the new dataObj
                    regFlow.config.dataObj = dataObj;

                    break;
                case 9:
                case 17:
                    regFlow.config.savedBirthdayString = el.SelectedValue;
                    break;
                // checklist
                case 15:
                    var checklistTotal = 0;

                    _.each(el.Control.Options, function (op, item, list) {
                        // add the name to the Options objects for templating
                        op["Name"] = el.Control.Name;

                        // if SelectedValue comes back, parse and add to option as checked attribute
                        if (el.Control.SelectedValue !== "") {
                            if ((op.Value & el.SelectedValue) === op.Value) {
                                op["Selected"] = true;
                            }
                        }
                    });
                    break;
                default:
                    // do nothing
            }
            regFlow.config.fieldsetHtml += Mustache.to_html(template, el);
        });

        regFlow.preStepRender(dataObj);

        $.mobile.hidePageLoadingMsg();

        regFlow.config.currentStepblocked = false;

        $regFlowFieldset.html(regFlow.config.fieldsetHtml);

        regFlow.currentStepEvents();

        if (dataObj.OmnitureData && dataObj.OmnitureData.Variables) {
            spark.tracking.addPageName(dataObj.OmnitureData.PageName, true);
            _.each(dataObj.OmnitureData.Variables, function(ov, item, list) {
                if (item) {
                    if (item.indexOf("event") > -1) {
                        spark.tracking.addEvent(ov, true);
                    }
                    else if (item.indexOf("prop") > -1) {
                        var prop = item.substring(4);
                        spark.tracking.addProp(prop, ov, true);
                    }
                    else if (item.indexOf("eVar") > -1) {
                        var evar = item.substring(4);
                        spark.tracking.addEvar(evar, ov, true);
                    }
                }
            });
            spark.tracking.track();
        }
    },
    'controlButtons': function () {
        //enable all buttons
        $(regFlow.config.nextButton).add(regFlow.config.prevButton).button('enable');

        // disable previous if first step
        if (regFlow.config.currentStep === 1) {
            $(regFlow.config.prevButton).button('disable');

            $('#stepControls').find('.ui-block-a').hide().closest('#stepControls').find('.ui-block-b').css({
                'marginLeft': 'auto',
                'marginRight': 'auto',
                'float': 'none'
            });
        } else {
            $('#stepControls').find('.ui-block-a').show().closest('#stepControls').find('.ui-block-b').attr('style','');
        }

        // remove login button on steps > 1
        if (regFlow.config.currentStep > 1) {
            $('#loginBtn').hide();
        } else{
            $('#loginBtn').show();
        }

    },
    'prepareStepForRender': function () {
        if (regFlow.config.currentStepblocked !== true) {
            regFlow.config.currentStepblocked = true;

            // disable buttons to prevent clicking while thinking
            // if(regFlow.config.initialRender !== true){
            //     console.log('disabled!');
            //     $(regFlow.config.nextButton).button('disable');
            //     $(regFlow.config.prevButton).button('disable');
            // }

            $(regFlow.config.stepControls).hide();
            $.mobile.showPageLoadingMsg();

            document.getElementById('formContainer').innerHTML = '';

            //clear current step item array, template array, fieldsetHtml
            regFlow.config.currentStepItems = [];
            regFlow.config.currentStepTemplates = [];
            regFlow.config.fieldsetHtml = "";
        }
    },
    'preStepRender': function(dataObj){
        // inject title metadata into registration step header
        if (dataObj.Step.TitleText !== "") {
            regFlow.config.$titleElement.html(dataObj.Step.TitleText);
        } else {
            // update title with default
            regFlow.titleUpdate();
        }

        //add step header text into template html
        if (dataObj.Step.HeaderText !== "") {
            var headerTextHtml = "<div id='" + regFlow.config.headerTextID + "' class='header-text header-text-" + regFlow.config.currentStep + "'>"+ dataObj.Step.HeaderText +"</div>";
            regFlow.config.fieldsetHtml = headerTextHtml + regFlow.config.fieldsetHtml;
        }
    },
    'currentStepEvents': function () {
        // $("[data-role=page]").page("destroy").page();
        $("[data-role=page]").trigger("create");
        $(regFlow.config.stepControls).show();
        // regFlow.titleUpdate();

        // initialize auto advance module
        // if(dataObj.EnableAutoAdvance === true && typeof autoAdvance === "undefined"){
        if(regFlow.config.autoAdvance === true && typeof autoAdvance === "undefined"){
            // set remove zero values to true
            regFlow.config.removeZeroValues = true;

            Modernizr.load({
                load: regFlow.config.scriptPath + "/Modules/auto-advance.js",
                complete: function () {
                    autoAdvance.init({
                        container: regFlow.config.container,
                        autoFunction: function(){
                            regFlow.nextStep();
                        }
                    });
                    // trigger formReady on auto-advance init
                    $(regFlow.config.container).trigger( "formReady", [{formEvent: "init"}]);
                }
            });
        }

        // Parse birthdate string and set defaults in dropdowns
        if (_.include(regFlow.config.currentStepItems, "birthdate")) {

            // if no support for date-picker
            if (Modernizr && Modernizr.inputtypes.date) {
                $("#formItem_birthdate_dateselects").remove();
                $("#formItem_birthdate_dateinput").show();

                if (regFlow.config.savedBirthdayString !== "") {
                    $('#formItem_birthdate_dateinput').find('input[type=date]').val(regFlow.config.savedBirthdayString);
                }
            }
            else {
                $("#formItem_birthdate_dateinput").remove();
                $("#formItem_birthdate_dateselects").show();
                if (regFlow.config.savedBirthdayString !== "") {
                    var birthdayArray = regFlow.config.savedBirthdayString.split("-");

                    $("#birthdayYear option[value=" + birthdayArray[0] + "]").attr("selected", "selected").closest('select').selectmenu('refresh');
                    $("#birthdayMonth option[value=" + birthdayArray[1] + "]").attr("selected", "selected").closest('select').selectmenu('refresh');
                    $("#birthdayDay option[value=" + birthdayArray[2] + "]").attr("selected", "selected").closest('select').selectmenu('refresh');
                }

                //load moment.js, if not already loaded, validate birthdate on change
                Modernizr.load({
                    test : typeof moment !== 'undefined' ? true : false,
                    nope : regFlow.config.scriptPath + "/Libs/moment.1.6.2.min.js",
                    complete : function(){
                        $('#birthdayMonth,#birthdayDay,#birthdayYear').on('change', function() {
                            var $month = $('#birthdayMonth'),
                                $day = $('#birthdayDay'),
                                $year = $('#birthdayYear');

                            if($month.val() !== "" && $day.val() !== "" && $year.val() !== ""){
                                var birthdayString = $month.val() + '/' + $('#birthdayDay').val() + '/' + $('#birthdayYear').val(),
                                    birthday = moment(birthdayString, 'MM-DD-YYYY'),
                                    now = new Date(),
                                    age = moment(now).diff(birthday, 'years', true);

                                if(birthday.isValid() === false){
                                    // reset day to prevent submitting failed birthdate
                                    $('#birthdayDay').val('').selectmenu('refresh');

                                    //trigger error message
                                    $.webshims.validityAlert.showFor($('#birthdayDay'), regFlow.config.msgDateDoesNotExist);
                                }

                                if(age < 18){
                                    // reset day to prevent submitting failed birthdate, blur() forces style update to invalid
                                    $('#birthdayDay').val('').blur().selectmenu('refresh');
                                    $('#birthdayMonth').val('').blur().selectmenu('refresh');
                                    $('#birthdayYear').val('').blur().selectmenu('refresh');

                                    //trigger error message
                                    $.webshims.validityAlert.showFor($('#birthdayMonth'), regFlow.config.msgDateTooYoung);
                                }

                                if(age >= 100){
                                    // reset day to prevent submitting failed birthdate, blur() forces style update to invalid
                                    $('#birthdayDay').val('').blur().selectmenu('refresh');
                                    $('#birthdayMonth').val('').blur().selectmenu('refresh');
                                    $('#birthdayYear').val('').blur().selectmenu('refresh');

                                    //trigger error message
                                    $.webshims.validityAlert.showFor($('#birthdayMonth'), regFlow.config.msgDateTooOld);
                                }
                            }
                        });
                    }
                });
            }
        }

        // Initialize region picker
        // console.log(regFlow.config.currentStepItems);
        if (_.include(regFlow.config.currentStepItems, "regionid")) {
            if (typeof regionPicker == "undefined"){
                Modernizr.load({
                    load : regFlow.config.scriptPath + "/Modules/m-region-picker.js",
                    complete : function(){
                        regionPicker.init({
                            incomingData: regFlow.config.dataObj
                        });
                    }
                });
            }
            else {
                regionPicker.zipCodeRepopulator();
            }
        }

        // Group-required template specific customizations
        if (_.include(regFlow.config.currentStepTemplates, 15)){
            // re-trigger group-required validations
            $('.group-required').trigger('refreshCustomValidityRules');

            // Put group-required custom error message position after last checkbox instead of first
            $('.template-15').each(function(){
                var $group = $(this);

                $group.find('.group-required').bind('firstinvalid', function(e){
                    $.webshims.validityAlert.showFor($group.find('.group-required:last'));

                    return false;
                });
            });
        }

        // trigger formReady on step load
        $(regFlow.config.container).trigger( "formReady", [{formEvent:"formDomReady"}]);

    },
    'titleUpdate': function () {
        var titleTemplate = _.template(regFlow.config.titleTemplate);
        var html = titleTemplate({ currentStep : regFlow.config.currentStep, totalSteps: regFlow.config.steps });
        regFlow.config.$titleElement.html(html);
    }
};

regFlow.init();
