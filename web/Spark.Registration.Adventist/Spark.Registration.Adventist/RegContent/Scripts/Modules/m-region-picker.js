﻿/*
|--------------------------------------------------------------------------
| Mobile region picker zip code only
|--------------------------------------------------------------------------
|
| Depedencies: jQuery, jQuery jQuery Mobile, underscore
| Code pattern: Object litteral pattern: http://rmurphey.com/blog/2009/10/15/using-objects-to-organize-your-code/
|
*/

var regionPicker = {
    'regionData': {},
    'config': {
        'container': "#regFlow",
        'incomingData': {},
        'nextButton': $('#formContinue'),
        'prevButton': $('#formPrevious'),
        'msgNoZipFound': 'Postal Code <%=zipcode%> is harder to find than your Beshert! Please enter a valid Postal Code.',
        'msgFieldRequired': 'Can you just give us a little something? This field is required so you kind of have to!',
        'msgGeneralAjaxError': 'Talk about bad timing! There was a connection error. Please try again later.'
    },
    'init': function (config) {
        // This code is for handling any updates/additions to 'config' that get passed in during init().
        if (config && typeof (config) == 'object') {
            $.extend(regionPicker.config, config);
        }

        // cache dom items into $els
        regionPicker.config.container = $(regionPicker.config.container);

        // stuff RegionData into regionData object for berevity
        _.each(regionPicker.config.incomingData.Step.PopulatedControls, function (el, item, list) {
            if (el.Control.RegionData) {
                regionPicker.regionData = el.Control.RegionData;
            }
        });

        // prepopulate zipcode and countryid
        regionPicker.zipCodeRepopulator();

        // bind events
        regionPicker.eventBindings();
    },
    'postalTypeCleaner' : function(value){

        // Strip out spaces and special characters
        value = value.replace(/[^a-zA-Z0-9]/g,"");

        // If 9 consecutive numbers, probably a US zip code with additional four numbers 90024-0000
        // we want to keep only the first five
        if(value.length === 9 && /^\d{9}$/g.test(value)){
           value = value.slice(0, 5);
        }

        return value;
    },
    'postalTypeValidator' : function(value){
        // run value through cleaner first
        value = regionPicker.postalTypeCleaner(value);

        // If a US Zip code in format, verify and return "USA" and cleaned value
        if(/^\d{5}?$/g.test(value)){
            return {"Country": "223", "Value": value};
        }

        // If a Canadian Postal code in format, verify and return "Canada" and cleaned value
        if(/^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]{1}\d{1}[a-zA-Z]{1}(| |-)\d{1}[a-zA-Z]{1}\d{1}$/g.test(value)){
            return {"Country": "38", "Value": value};
        }

        // If no conditions are met, fail the value
        return {"Country": "Failed", "Value": value};
    },
    'eventBindings' : function(){

        // zip code
        regionPicker.config.container.on('keyup paste input', '#regionzipcode', function(event) {
            var $this = $(this),
                postValue = $this.val(),
                postObj = { "zipcode" : postValue },
                countryObj = regionPicker.postalTypeValidator(postValue);

            // keys to ignore
            // arrow keys: 37, 38, 39, 40
            // alt: 18
            if( event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40 ){
                return;
            }

            if(countryObj.Country === "38" || countryObj.Country === "223"){

                // update regionData object
                regionPicker.regionData.CountryRegionID = countryObj.Country;
                regionPicker.regionData.ZipCode = countryObj.Value;

                // repopulate input with cleaned value
                $this.val(countryObj.Value);

                regionPicker.getJSON('/RegContent/region/cities/' + countryObj.Value, function(){

                    // check to make sure Cities does not come back empty
                    if(_.isEmpty(this)){
                        $.webshims.validityAlert.showFor($this, _.template(regionPicker.config.msgNoZipFound, postObj));
                        $this.val('');
                    } else {
                        // Update object with selected ZipCode
                        // regionPicker.regionData.ZipCode = countryObj.Value;

                        // Update object with first returned CityRegionID
                        regionPicker.regionData.CityRegionID = this[0].Id;

                        // Update hidden input with CountryRegionID
                        $('#regioncountryid').attr('value', regionPicker.regionData.CountryRegionID);

                        // Update hidden input with CityRegionID
                        $('#regioncityid').attr('value', regionPicker.regionData.CityRegionID);

                        // update field validation
                        $('#regionzipcode').addClass('form-ui-valid').removeClass('form-ui-invalid');
                    }
                }, $(this).closest('div'));
                return;
            }

        });
    },
    'getJSON' : function(path, callback, spinnerElement){
        $.mobile.showPageLoadingMsg();

        $.ajax({
            url: path,
            dataType: "json",
            beforeSend: function(){
                regionPicker.config.nextButton.button('disable');
            },
            success: function( data ) {
                callback.call(data);
                $.mobile.hidePageLoadingMsg();
                regionPicker.config.nextButton.button('enable');
            },
            cache: false
        });
    },
    'zipCodeRepopulator' : function(){
        $('#regionzipcode').attr('value', regionPicker.regionData.ZipCode);
        $('#regioncountryid').attr('value', regionPicker.regionData.CountryRegionID);
    }
};