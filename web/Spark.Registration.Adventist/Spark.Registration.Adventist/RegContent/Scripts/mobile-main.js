﻿// Make clicking on logo send you to home page
$('[data-role=page]:not(".m_home, .m_landing, .registration")').find('header').find('.logo').addClass('ui-title-linked').live('tap click', function (e) {
    window.location = "http://m.adventistsinglesconnection.com/home/";
});

// ==Helper plugins and functions

// ==Custom code
(function ($) {
    __createNamespace__ = function (namespace) {
        var components = namespace.split(/[^a-z0-9]+/ig);
        var context = window;
        for (var i = 0, l = components.length; i < l; i += 1) {
            var component = components[i];
            if (!context[component]) {
                context[component] = {};
            }
            context = context[component];
        }
        return context;
    };
    __addToNamespace__ = function (ns, o) {
        var initFunc = null;
        var myns = __createNamespace__(ns);
        for (var i in o) {
            if (i + "" == "__init__") {
                initFunc = o[i];
            } else {
                myns[i] = o[i];
            }
        }
        if (initFunc) initFunc();
        initFunc = null;
    };

    // ==Custom Events
    // UI messaging
    // puts/removes an message after an element

    // Usage examples:
    // $(spark).trigger( "uiMessaging", [{type: "message notification", content: "We could not find it.", selector: "[role=content] > h1"}] );
    // $(spark).trigger( "uiMessaging", [{type: "message error", content: "We could not find it.", selector: "body"}] );
    // $(spark).trigger( "uiMessaging", [{selector: "[role=content] > h1", off: true}] );

    $(spark).on("uiMessaging", function (event, data) {

        var $el = $(data.selector);

        // if off, remove the message and get out of Dodge!
        if (data.off) {
            $el.next('.element-message').remove();
            return;
        }

        var content = $("<div/>").html(data.content).text(), //decode any html entities
            $customMessage = $('<div></div>').addClass('element-message ' + data.type).html(content);

        // check for existing message
        if ($el.next('.element-message').length === 0) {
            $el.after($customMessage);
        } else {
            $el.next('.element-message').removeClass().addClass('element-message ' + data.type).empty().html(content);
        }
    });

    // ==Spark tracking
    __addToNamespace__('spark.tracking', {
        addKeyAndValue: function (key, val, overwrite) {
            if (s) {
                if (s[key] && s[key].toLowerCase() != "none" && !overwrite) {
                    var str = s[key] + "";
                    if (str.indexOf(val) == -1) s[key] += "," + val;
                } else {
                    s[key] = val;
                }
            }
        },
        addEvent: function (evt, overwrite) {
            spark.tracking.addKeyAndValue('events', evt, overwrite);
        },
        addLinkTrackVar: function (ltv, overwrite) {
            spark.tracking.addKeyAndValue('linkTrackVars', ltv, overwrite);
        },
        addLinkTrackEvent: function (lte, overwrite) {
            spark.tracking.addKeyAndValue('linkTrackEvents', lte, overwrite);
        },
        addProp: function (num, val) {
            spark.tracking.addKeyAndValue('prop' + num, val, true);
        },
        addEvar: function (num, val) {
            spark.tracking.addKeyAndValue('eVar' + num, val, true);
        },
        addPageName: function (val, overwrite) {
            spark.tracking.addKeyAndValue('pageName', val, overwrite);
        },
        track: function () {
            if (s) s.t();
        },
        trackLink: function (params) {
            if (s) s.tl(params.firstParam, 'o', params.linkName);
        }
    });

    // ==Iovation Blackbox javascript
    __addToNamespace__('spark.iovation', {
        GetIOBB: function (pass) {
            if (typeof ioGetBlackbox == 'function') {
                var bb_data = ioGetBlackbox();
                var blackbox = bb_data.blackbox;
                if (bb_data.finished || pass > 10) {
                    if (typeof jQuery != 'undefined') {
                        jQuery.cookie('MOS_regiobb', blackbox, { expires: 1, path: '/' });
                    }
                    return false;
                }
                setTimeout("spark.iovation.GetIOBB(" + (pass + 1) + ")", 100);
            }
            return false;
        }
    });

} (jQuery));
