﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Persistence;
using Spark.Registration.Dictionaries.Constants;

namespace Spark.Registration.Adventist.Models
{
    public class RegistrationPersistence : IRegistrationValuePersistence
    {
        #region ITemporaryPersistence Members

        private HttpCookie _persistCookie;
        public const string COOKIE_NAME = "AdventistSinglesConnectionRegFATL";
        private const string ATTR_PREFIX = "ATTR";
        private const int COOKIE_EXPIRATION_DAYS = 30;

        public RegistrationPersistence()
        {
            EnsureCookieExists();
        }

        public string Name
        {
            get { return COOKIE_NAME; }
        }

        public string this[string index]
        {
            get
            {
                return HttpUtility.UrlDecode(_persistCookie[index.ToLower()]);
            }
            set
            {
                _persistCookie[index.ToLower()] = HttpUtility.UrlEncode(value);
            }
        }

        public DateTime Expires
        {
            get
            {
                return _persistCookie.Expires;
            }
            set { _persistCookie.Expires = value; }
        }

        public void Persist()
        {
            _persistCookie.Expires = DateTime.Now.AddDays(COOKIE_EXPIRATION_DAYS);
            HttpContext.Current.Response.Cookies.Add(_persistCookie);
        }

        public void Clear()
        {
            if (_persistCookie != null)
            {
                _persistCookie.Expires = DateTime.Now.AddYears(-30);
                HttpContext.Current.Response.Cookies.Add(_persistCookie);
            }
        }

        public bool HasValues
        {
            get
            {
                return _persistCookie.Values.Count > 0;
            }

        }

        public NameValueCollection Values
        {
            get { return _persistCookie.Values; }
        }

        private void EnsureCookieExists()
        {
            if (_persistCookie == null)
                _persistCookie = HttpContext.Current.Request.Cookies[COOKIE_NAME];

            if (_persistCookie == null)
            {
                _persistCookie = new HttpCookie(COOKIE_NAME, COOKIE_NAME);

                //cookie doesn't exist, meaning this is a new session, so generate a new ID
                _persistCookie[PersistenceConstants.SessionID] = Guid.NewGuid().ToString();

                HttpContext.Current.Response.Cookies.Add(_persistCookie);
            }
        }

        public void AddValue(string name, string value)
        {
            _persistCookie[name.ToLower()] = HttpUtility.UrlEncode(value);
        }

        public void AddAttributeValue(string name, string value)
        {
            _persistCookie[ATTR_PREFIX + name.ToLower()] = HttpUtility.UrlEncode(value);
        }

        public Dictionary<string, string> GetAttributeValues()
        {
            var values = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            foreach (string key in _persistCookie.Values.AllKeys)
            {
                if (key != null && key.IndexOf(ATTR_PREFIX) > -1)
                {
                    string modifiedKey = key.Substring(ATTR_PREFIX.Length);
                    values.Add(modifiedKey, HttpUtility.UrlDecode(_persistCookie[key]));
                }
            }
            return values;
        }

        public void RemoveValue(string name)
        {
            _persistCookie.Values.Remove(name.ToLower());
        }
        
        #endregion

        public override string ToString()
        {
            var cookieValues = new StringBuilder();
            foreach (string key in _persistCookie.Values.AllKeys)
            {
                if (!string.IsNullOrEmpty(key) )
                {
                    var value = key.ToLower() == "password" ? string.Empty : _persistCookie[key];
                    cookieValues.AppendLine(key + ": " + value);
                }
            }

            return cookieValues.ToString();
        }
    }
}
