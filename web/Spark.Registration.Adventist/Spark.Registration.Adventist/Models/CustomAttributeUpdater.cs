﻿using System.Collections.Generic;

namespace Spark.Registration.Adventist.Models
{
    public class CustomAttributeUpdater 
    {
        internal delegate void AttributeUpdaterDelegate(Dictionary<string, object> attributeUpdateDictionary, 
            Dictionary<string, List<int>> multivalueAttributeUpdateDictionary,
            object value);

        internal static readonly Dictionary<string, AttributeUpdaterDelegate> AttributeUpdaterMethodMap = new Dictionary
            <string, AttributeUpdaterDelegate>
		                                                                                               	{
		                                                                                               		{"newslettermask", SetNewsletterMask }
		                                                                                               	};


        private static void SetNewsletterMask(Dictionary<string, object> attributeUpdateDictionary, 
            Dictionary<string, List<int>> multivalueAttributeUpdateDictionary,
            object value)
        {
            var boolValue = value.ToString().ToLower() == "on";

            multivalueAttributeUpdateDictionary.Add("newslettermask",
                                                    boolValue ? new List<int> {1, 2, 4} : new List<int> {0});
        }
    }
}
