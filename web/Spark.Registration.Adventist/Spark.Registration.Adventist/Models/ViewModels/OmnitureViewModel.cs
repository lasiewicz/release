﻿using System.Collections.Generic;

namespace Spark.Registration.Adventist.Models.ViewModels
{
    public class OmnitureViewModel
    {
        public Dictionary<string, string> Variables;

        public string PageName { get; set; }
        public string Server { get; set; }
        public string Referrer { get; set; }
        public string PageUrl { get; set; }
    }
}