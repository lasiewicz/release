﻿using System.Collections.Specialized;

namespace Spark.Registration.Adventist.Models
{
    public class UpdateRegistrationRequest
    {
        public int StepNumber { get; set; }
        public NameValueCollection Values { get; set; }
    }
}