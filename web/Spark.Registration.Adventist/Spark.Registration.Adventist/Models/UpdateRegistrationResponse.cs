﻿using System.Collections.Generic;

namespace Spark.Registration.Adventist.Models
{
    public enum UpdateRegistrationStatus
    {
        Success = 0,
        Failure =1, 
        Registered=2
    }
    
    public class UpdateRegistrationResponse
    {
        public UpdateRegistrationStatus Status { get; set; }
        public Dictionary<string, string> ErrorMessages { get; set; }
        public string ConfirmationScenarioName { get; set; }

        public UpdateRegistrationResponse() { ErrorMessages = new Dictionary<string, string>(); }

        public UpdateRegistrationResponse(UpdateRegistrationStatus status)
        {
            Status = status;
            ErrorMessages = new Dictionary<string, string>();
        }

        public UpdateRegistrationResponse(UpdateRegistrationStatus status, Dictionary<string, string> errorMessages)
        {
            Status = status;
            ErrorMessages = errorMessages;
        }
    }
}