﻿namespace Spark.Registration.Adventist.Models
{
    public class DebugInfo
    {
        public string Host { get; set; }
        public string URL { get; set; }
        public int CommunityID { get; set; }
        public int PromotionID { get; set; }
        public string Luggage { get; set; }
        public int BannerID { get; set; }
        public int LandingPageID { get; set; }
    }
}