﻿using Spark.Common.RestConsumer.V2.Models.Content.Registration;

namespace Spark.Registration.Adventist.Models
{
    public class PopulatedRegControl
    {
        public RegControl Control { get; set; }
        public string SelectedValue { get; set; }

        public PopulatedRegControl() {}

        public PopulatedRegControl(RegControl control, string selectedValue)
        {
            Control = control;
            SelectedValue = selectedValue;
        }
    }
}