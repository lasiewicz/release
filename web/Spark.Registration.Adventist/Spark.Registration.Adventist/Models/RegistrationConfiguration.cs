﻿using System;
using System.Configuration;
using System.Web.Http;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.RestConsumer.V2.Models.Content.BrandConfig;
using Spark.Registration.Adventist.Framework;
using Spark.Registration.Adventist.Framework.Helpers;
using Spark.Registration.Adventist.Interfaces.Adapters;

namespace Spark.Registration.Adventist.Models
{
    public class RegistrationConfiguration
    {
        private static Brand _brand;
        private static object _lock = new object();

        public static string BedrockHost
        {
            get { return ConfigurationManager.AppSettings["BedrockHost"]; }
        }

        public static string MOSHost
        {
            get { return ConfigurationManager.AppSettings["MOSHost"]; }
        }

        public static string BedrockRedirectPath
        {
            get { return ConfigurationManager.AppSettings["BedrockRedirectPath"]; }
        }

        public static string MOSRedirectPath
        {
            get { return ConfigurationManager.AppSettings["MOSRedirectPath"]; }
        }

        public static string FailedRegistrationMessage
        {
            get { return ConfigurationManager.AppSettings["FailedRegistrationMessage"]; } 
        }

        public static string SiteURI
        {
            get { return ConfigurationManager.AppSettings["SiteURI"]; }
        }

        public static string OmnitureAccountName
        {
            get { return ConfigurationManager.AppSettings["OmnitureAccountName"]; }
        }
        
        public static string MOSOmnitureAccountName
        {
            get { return ConfigurationManager.AppSettings["MOSOmnitureAccountName"]; }
        }

        public static string MOSOmniturePagenamePrefix
        {
            get { return ConfigurationManager.AppSettings["MOSOmniturePagenamePrefix"]; }
        }

        public static int BrandId
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["BrandId"]); }
        }
        
        public static int ApplicationId
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["ApplicationId"]); }
        }
       
        public static string ClientSecret
        {
            get { return ConfigurationManager.AppSettings["ClientSecret"]; } 
        }

        public static string OAuthCookieDomain
        {
            get { return ConfigurationManager.AppSettings["OAuthCookieDomain"]; } 
        }

        public static Brand Brand
        {
            get
            {
                if (null == _brand)
                {
                    lock (_lock)
                    {
                        if (null == _brand)
                        {
                            var adapter = (IApiAdapter) UnityHelper.Resolver.GetService(typeof(IApiAdapter));
                            _brand = adapter.GetBrand();
                        }
                    }
                }
                return _brand;
            }
        }

        public static string GetRuntimeSettings(string settingName)
        {
            var brand = Brand;
            return RuntimeSettings.GetSetting(settingName, brand.Site.Community.Id, brand.Site.Id);
        }

        //stage
        /*<add key="UTRestAuthority" value="https://api-dev.securemingle.com/v3/9011/"/>
    <add key="UTClientSecret" value="zUG8YkA2BMUFfJGklA58hy8/0+2vDZE0to4/rutIqPI="/>
    <add key="UTClientId" value="1028"/>
         * 
         * //prod
         *   <add key="UTRestAuthority" value="https://api.securemingle.com/v3/9011" />
    <add key="UTClientSecret" value="HfkUTVnMBpVsPJGhAksx+vuRd4kNSKq7kJLkjLPXSfI=" />
    <add key="UTClientId" value="1014" />
         * 
         */

        public static string UTRestAuthority
        {
            get { return ConfigurationManager.AppSettings["UTRestAuthority"]; }
        }

        public static string UTClientSecret
        {
            get { return ConfigurationManager.AppSettings["UTClientSecret"]; }
        }

        public static string UTClientId
        {
            get { return ConfigurationManager.AppSettings["UTClientId"]; }
        }
    }
}