﻿using Matchnet;

namespace Spark.Registration.Adventist.Models
{
    public class CachedMOLCount : ICacheable
    {
        public static string CACHE_KEY = "~MOLCOUNT";
        public int Count { get; private set; }
        
        public CachedMOLCount(int count)
        {
            Count = count;
        }


        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return CacheItemPriorityLevel.High;
            }
            set { }
        }

        public int CacheTTLSeconds
        {
            get
            {
                return 60; //1 minute
            }
            set
            {

            }
        }

        public string GetCacheKey()
        {
            return CACHE_KEY;
        }

        #endregion
    }
}