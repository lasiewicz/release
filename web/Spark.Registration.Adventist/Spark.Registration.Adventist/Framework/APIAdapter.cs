﻿using System;
using System.Collections.Generic;
using System.Web;
using Spark.Common.RestConsumer.V2;
using Spark.Common.RestConsumer.V2.Models;
using Spark.Common.RestConsumer.V2.Models.Content.BrandConfig;
using Spark.Common.RestConsumer.V2.Models.Content.Pixels;
using Spark.Common.RestConsumer.V2.Models.Content.Promotion;
using Spark.Common.RestConsumer.V2.Models.Content.Region;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Content.RegistrationActivityRecording;
using Spark.Common.RestConsumer.V2.Models.Member;
using Spark.Common.RestConsumer.V2.Models.MembersOnline;
using Spark.Common.RestConsumer.V2.Models.MingleMigration;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Interfaces.Adapters;
using Spark.Registration.Adventist.Models;
using log4net;

namespace Spark.Registration.Adventist.Framework
{
    public class ApiAdapter : IApiAdapter
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(ApiAdapter));
        private readonly RestConsumer _restConsumer;
        
        public Dictionary<string, string> UrlParams { get; private set; }
        public Dictionary<string, string> QueryParams { get; private set; }

        public ApiAdapter(string applicationId, string clientSecret, ILog log, RestConsumer restConsumer)
        {
            _restConsumer = restConsumer;
            _log = log;

            UrlParams = new Dictionary<string, string> { { "applicationId", applicationId } };
            QueryParams = new Dictionary<string, string> { { "client_secret", clientSecret }, { "applicationId", applicationId } };
        }

        //public ApiAdapter()
        //{
        //    UrlParams = new Dictionary<string, string> { { "applicationId", RegistrationConfiguration.ApplicationId.ToString() } };
        //    QueryParams = new Dictionary<string, string> { { "client_secret", RegistrationConfiguration.ClientSecret }, { "applicationId", RegistrationConfiguration.ApplicationId.ToString() } };
        //}

        public List<Country> GetCountries()
        {
            ResponseDetail responseDetail;
            var urlParams = new Dictionary<string, string>
                                {
                                    {"languageId", "2"}
                                };

            return _restConsumer.Get<List<Country>>(urlParams, out responseDetail);
        }

        public List<Region> GetRegionsForZip(string zipCode)
        {
            ResponseDetail responseDetail;
            var urlParams = new Dictionary<string, string>
                                    {
                                        {"zipCode", zipCode}
                                    };

            return _restConsumer.Get<List<Region>>(urlParams, out responseDetail);
        }

        public List<State> GetStatesForCountry(int countryRegionId)
        {
            ResponseDetail responseDetail;
            var urlParams = new Dictionary<string, string>
                                    {
                                        {"languageId", "2"},
                                        {"countryRegionId", countryRegionId.ToString()}
                                    };

            return _restConsumer.Get<List<State>>(urlParams, out responseDetail);
        }

        public List<City> GetCitiesForCountryState(int countryRegionId, int stateRegionId)
        {
            ResponseDetail responseDetail;
            var urlParams = new Dictionary<string, string>
                                    {
                                        {"languageId", "2"},
                                        {"countryRegionId", countryRegionId.ToString()},
                                        {"stateRegionId", stateRegionId.ToString()}
                                    };

            return _restConsumer.Get<List<City>>(urlParams, out responseDetail);
        }

        public List<Scenario> GetScenarios()
        {
            ResponseDetail responseDetail;
            var result = _restConsumer.Get<List<Scenario>>(UrlParams, out responseDetail, QueryParams);

            return result;
        }

        public string GetRandomScenarioName(DeviceType deviceType)
        {
            ResponseDetail responseDetail;
            var scenarioParams = new Dictionary<string, string>(UrlParams)
                                       {
                                           { "DeviceType", deviceType.ToString("d") }
                                       };

            return _restConsumer.Get<String>(scenarioParams, out responseDetail, QueryParams);
        }

        public ExtendedRegisterResult RegisterExtended(ExtendedRegisterRequest register)
        {
            ResponseDetail responseDetail;
            ExtendedRegisterResult registerResult;

            try
            {
                registerResult = _restConsumer.Post<ExtendedRegisterResult, ExtendedRegisterRequest>(UrlParams, register, out responseDetail, QueryParams);
            }
            catch (Exception ex)
            {
                _log.Error("Error performing registration via REST", ex);
                registerResult = new ExtendedRegisterResult { RegisterStatus = "Failure" };
            }

            if (registerResult == null)
            {
                _log.Error("Error performing registration via REST, null result");
                registerResult = new ExtendedRegisterResult { RegisterStatus = "Failure" };
            }
            else if (registerResult.RegisterStatus != "Success")
            {
                registerResult.RegisterStatus = registerResult.RegisterStatus.Replace(@"<a href='/default.aspx'>login</a>",
                                                                             "<a href='http://www.adventistsinglesconnection.com/member_login.html'>login</a>");
            }

            return registerResult;
        }

        public void RecordRegistrationStart(string registrationSessionID, string formFactor, int applicationID, int scenarioID, int ipAddress,HttpRequestBase httpRequestBase = null)
        {
            ResponseDetail responseDetail;
            try
            {
                var registrationStart = new RegistrationStart
                {
                    RegistrationSessionID = registrationSessionID,
                    ApplicationID = applicationID,
                    FormFactor = formFactor,
                    ScenarioID = scenarioID,
                    IPAddress = ipAddress, 
                    TimeStamp = DateTime.Now
                };

                _restConsumer.Post<RegistrationRecordingResult, RegistrationStart>(UrlParams, registrationStart, out responseDetail, QueryParams, httpRequestBase);
            }
            catch (Exception ex)
            {
                _log.Error("Error recording registration start via REST", ex);
            }
        }

        public string RecordRegistrationStep(string registrationSessionID, int stepID, Dictionary<string, object> stepDetails, 
            Dictionary<string, string> allRegFields, string emailAddress, string recaptureID)
        {
            ResponseDetail responseDetail;
            var returnedRecaptureID = recaptureID;
            
            try
            {
                var convertedRegFields = new Dictionary<string, object>();

                foreach(var attribute in allRegFields)
                {
                    convertedRegFields.Add(attribute.Key, attribute.Value);
                }
                
                var registrationStep = new RegistrationStepWithRecaptureInfo
                {
                    RegistrationSessionID = registrationSessionID,
                    StepID = stepID,
                    StepDetails = stepDetails,
                    TimeStamp = DateTime.Now, 
                    BrandID = RegistrationConfiguration.BrandId, 
                    AllRegFields = convertedRegFields, 
                    EmailAddress=emailAddress, 
                    RecaptureID = recaptureID
                };

                var response = _restConsumer.Post<RegistrationRecordingResultWithRecaptureInfo, RegistrationStepWithRecaptureInfo>(UrlParams, registrationStep, out responseDetail, QueryParams);
                returnedRecaptureID = response.RecaptureID;
            }
            catch (Exception ex)
            {
                _log.Error("Error recording registration step via REST", ex);
            }

            return returnedRecaptureID;
        }

        public RegistrationRecaptureInfo GetRegistrationRecpatureInfo(string recaptureID)
        {
            ResponseDetail responseDetail;
            var result = new RegistrationRecaptureInfo();

            try
            {
                var recaptureParams = new Dictionary<string, string>(QueryParams)
                                       {
                                            { "RecaptureID", recaptureID }
                                       };


                result = _restConsumer.Get<RegistrationRecaptureInfo>(UrlParams, out responseDetail, recaptureParams);
            }
            catch (Exception ex)
            {
                _log.Error("Error getting registration recapture info via REST", ex);
            }


            return result;
        }
        
        public PromotionMappingResult MapMapURLToPromo(string referrer)
        {
            ResponseDetail responseDetail;
            var result = new PromotionMappingResult {PromotionID =0, LuggageID = string.Empty};

            try
            {
                if (string.IsNullOrEmpty(referrer) || !Uri.IsWellFormedUriString(referrer, UriKind.Absolute))
                {
                    //if the reffer is malformed, just use http://www.adventistsinglesconnection.com so we'll at least get the correct default back. 
                    referrer = "http://www.adventistsinglesconnection.com";
                }
                    
                var uri = new Uri(referrer);
                var referralHost = uri.Host;    
                var referralURLQueryString = !String.IsNullOrEmpty(uri.Query) ? uri.Query : string.Empty;

                //trim the referralQueryString to prevent hacks to the system
                if (referralURLQueryString.Length > 50)
                    referralURLQueryString = referralURLQueryString.Substring(0,50);

                var referralURL = uri.AbsoluteUri;


                var mappingParams = new Dictionary<string, string>(QueryParams)
                                       {
                                            { "ReferralHost", referralHost }, 
                                            { "ReferralURLQueryString", referralURLQueryString },
                                            { "ReferralURL", referralURL }
                                       };

                result = _restConsumer.Get<PromotionMappingResult>(UrlParams, out responseDetail, mappingParams) ?? result;

            }
            catch (Exception ex)
            {
                _log.Error("Error mapping URL to promo via REST", ex);
            }
            
            return result;
        }

        public int GetMemberForSession(string sessionId)
        {
            ResponseDetail responseDetail;
            int memberId = Matchnet.Constants.NULL_INT;
            var sessionParams = new Dictionary<string, string>(QueryParams)
                                       {
                                           { "SessionId", sessionId }
                                       };

            try
            {
                var response = _restConsumer.Get<SessionMemberResponse>(UrlParams, out responseDetail, sessionParams);
                memberId = response.MemberId;
            }
            catch (Exception ex)
            {
                _log.Error("Error getting member from session via REST", ex);
            }

            return memberId;
        }

        public string CreateBedrockSession(int memberId)
        {
            ResponseDetail responseDetail;
            string sessionID = string.Empty;
            Session session = null;

            var sessionParams = new Dictionary<string, string>(QueryParams)
                                       {
                                           { "memberId", memberId.ToString() }
                                       };

            try
            {
                session = _restConsumer.Get<Session>(UrlParams, out responseDetail, sessionParams);
            }
            catch (Exception ex)
            {
                _log.Error("Error creating bedrock session via REST", ex);
            }

            if (session != null)
            {
                sessionID = session.Key;
            }
            else
            {
                _log.Error("Error creating bedrock session via REST: null session");
            }
            
            return sessionID;
        }

        public void CreateBedrockSession(int memberId, string sessionId)
        {
            Session session = null;

            var sessionCreateRequest = new SessionCreateRequestWithSessionId
                                           {MemberID = memberId, SessionId = sessionId};

            try
            {
                ResponseDetail responseDetail;
                session = _restConsumer.Post<Session, SessionCreateRequestWithSessionId>(UrlParams, sessionCreateRequest, out responseDetail, QueryParams);
            }
            catch (Exception ex)
            {
                _log.Error("Error creating bedrock session via REST", ex);
            }

            if (session == null)
            {
                _log.Error("Error creating bedrock session via REST: null session");
            }
        }

        public int GetMembersOnlineCount()
        {
            ResponseDetail responseDetail;
            var count = Matchnet.Constants.NULL_INT;

            try
            {
                var countResult = _restConsumer.Get<MembersOnlineCount>(new Dictionary<string, string> { { "brandUri", "adventistsinglesconnection.com" } }, out responseDetail);
                count = countResult.Count;
            }
            catch (Exception ex)
            {
                _log.Error("Error getting MOL count.", ex);

            }
            return count;
        }

        public List<string> GetPixels(string memberId, string luggageId, string promotionId, string referralURL, string pageName)
        {
            ResponseDetail responseDetail;
            List<string> result = null;
            
            try
            {
                var pixelQueryParams = new Dictionary<string, string>(QueryParams)
                                       {
                                           {"languageId", "2"},
                                           {"memberid", memberId},
                                           {"luggageID", luggageId},
                                           {"promotionid", promotionId},
                                           {"referralURL", referralURL},
                                           {"pagename", pageName}
                                       };

                var pixelResult = _restConsumer.Get<PixelResult>(UrlParams, out responseDetail, pixelQueryParams);
                result = pixelResult.RenderedPixels;
            }
            catch (Exception ex)
            {
                _log.Error("Error getting pixels.", ex);
            }

            return result;
        }

        public int CreateSessionTrackingRequest(string sessionId, int promotionId, string luggageId, int bannerId,
                                         string clientIP, string referrerURL, string landingPageId,  string landingPageTestId)
        {
            ResponseDetail responseDetail;
            var sessionTrackingId = 0;

            try
            {
                var createSessionTrackingRequest = new CreateSessionTrackingRequest
                {
                    SessionID = sessionId, 
                    PromotionID = promotionId, 
                    LuggageID = luggageId, 
                    BannerID = bannerId, 
                    ClientIP = clientIP, 
                    ReferrerURL = referrerURL,
                    LandingPageID = landingPageId,
                    LandingPageTestID = landingPageTestId

                    
                };

                var sessionTrackingResult = _restConsumer.Post<CreateSessionTrackingResponse, CreateSessionTrackingRequest>(UrlParams, createSessionTrackingRequest, out responseDetail, QueryParams);
                sessionTrackingId = sessionTrackingResult.SessionTrackingID;
            }
            catch (Exception ex)
            {
                _log.Error("Error creating session tracking.", ex);
            }

            return sessionTrackingId;
        }

        public bool ValidateRegistrationAttribute(string attributeName, string attributeValue)
        {
            ResponseDetail responseDetail;
            var attributeValueExists = false;

            try
            {
                var attributeQueryParams = new Dictionary<string, string>(QueryParams)
                                       {
                                           {"attributename", HttpUtility.UrlEncode(attributeName)},
                                           {"attributevalue", HttpUtility.UrlEncode(attributeValue)}
                                       };
                _log.InfoFormat("Params - AttributeName:{0} Value:{1}", attributeName, attributeValue);
                var response = _restConsumer.Get<AttributeValidationResult>(UrlParams, out responseDetail, attributeQueryParams);
                attributeValueExists = response.AttributeValueExists;
            }
            catch (Exception ex)
            {
                _log.Error("Error validating attribute value via REST", ex);
            }

            return attributeValueExists;
        }

        public Brand GetBrand()
        {
            ResponseDetail responseDetail;
            Brand _brand=null;
            string brandId = RegistrationConfiguration.BrandId.ToString();
            var brandParams = new Dictionary<string, string>()
            {
                {"brandId", brandId}
            };

            try
            {
                _brand = _restConsumer.Get<Brand>(brandParams, out responseDetail);
            }
            catch (Exception e)
            {
                _log.Debug("Could not get Brand for Id:"+brandId, e);
            }
            return _brand;
        }

        // Map attributes from BH to Mingle
        public Dictionary<string, object> MapToMingleAttributes(MingleAttributeMappingRequest request)
        {
            try
            {
                ResponseDetail responseDetail;
                var result = _restConsumer.Post<MingleMappedAttributes, MingleAttributeMappingRequest>(UrlParams, request, out responseDetail, QueryParams);

                return result.AttributeData;
            }
            catch (Exception ex)
            {
                _log.Error("Error creating session tracking.", ex);
            }

            return null;
        }
    }
}