﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;


namespace Spark.Registration.Adventist.Framework.Helpers
{
    public static class Encryption
    {
        private const string PstrEncrKey = "spark!#z";

        public static string Encrypt(string pstrText)
        {
            var des = new DESCryptoServiceProvider();
            var ms = new MemoryStream();

            try
            {
                byte[] iv = { 18, 52, 86, 120, 144, 171, 205, 239 };
                byte[] byKey = Encoding.UTF8.GetBytes(PstrEncrKey);

                byte[] inputByteArray = Encoding.UTF8.GetBytes(pstrText);

                var cs = new CryptoStream(ms, des.CreateEncryptor(byKey, iv), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            finally
            {
                ms.Dispose();
                des.Dispose();
            }
        }

        public static string Decrypt(string pstrText)
        {
            var des = new DESCryptoServiceProvider();
            var ms = new MemoryStream();

            try
            {
                pstrText = pstrText.Replace(" ", "+");
                const string pstrDecrKey = "spark!#z";
                byte[] iv = { 18, 52, 86, 120, 144, 171, 205, 239 };
                var inputByteArray = new byte[pstrText.Length];

                byte[] byKey = Encoding.UTF8.GetBytes(pstrDecrKey.Substring(0, 8));
                inputByteArray = Convert.FromBase64String(pstrText);

                var cs = new CryptoStream(ms, des.CreateDecryptor(byKey, iv), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                Encoding encoding = Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            finally
            {
                ms.Dispose();
                des.Dispose();
            }
        }
    }
}