﻿using System;

namespace Spark.Registration.Adventist.Framework.Helpers
{
    public static class MemberHelper
    {
        internal static string GetGenderDescription(string genderMask)
        {
            string description = string.Empty;
            int genderMaskAsInt;

            bool parse = Int32.TryParse(genderMask, out genderMaskAsInt);

            if(parse && genderMaskAsInt != 0)
            {
                if (genderMaskAsInt == 1 || genderMaskAsInt == 3)
                {
                    description = "Male";
                }
                if (genderMaskAsInt == 2 || genderMaskAsInt == 4)
                {
                    description = "Female";
                }
            }

            return description;
        }

        internal static int GetAge(string birthDate)
        {
            var age = 0;
            DateTime birthdateAsDate;

            var parse = DateTime.TryParse(birthDate, out birthdateAsDate);

            if (parse && birthdateAsDate != DateTime.MinValue)
            {
                age = (int)((double)DateTime.Now.Subtract(birthdateAsDate).Days / 365.25);
            }

            return age;
        }
    }
}