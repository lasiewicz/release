﻿using System.Collections.Generic;

namespace Spark.Registration.Adventist.Framework.Analytics
{
    public class Omniture
    {
        public Omniture()
        {
            Variables = new Dictionary<string, string>();
        }
        
        public Dictionary<string, string> Variables { get; set; }

        public void SetVariable(string variable, string value)
        {
            var valActual = value;

            if (variable == "events")
            {
                if (Variables.ContainsKey("events"))
                {
                    valActual = Variables["events"] + ";" + valActual;
                    Variables.Remove("events");
                }
            }
            else
            {
                Variables.Remove(variable);
            }

            Variables.Add(variable, valActual);
        }
    }
}