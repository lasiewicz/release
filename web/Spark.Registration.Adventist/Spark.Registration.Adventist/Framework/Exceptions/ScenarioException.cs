﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spark.Registration.Adventist.Framework.Exceptions
{
    public class ScenarioException : Exception
    {
        public ScenarioException(string message) : base(message)
        {
            
        }

        public ScenarioException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}