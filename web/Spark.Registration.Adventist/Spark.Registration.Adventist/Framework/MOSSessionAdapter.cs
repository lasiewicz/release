﻿using Matchnet.Configuration.ServiceAdapters;
using Spark.Caching;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Adapters;

namespace Spark.Registration.Adventist.Framework
{
    public class MOSSessionAdapter : IMOSSessionAdapter
    {
        #region IMOSSessionAdapter Members

        public void CreateSession(Session session, string sessionId)
        {
            MembaseCaching.GetInstance(RuntimeSettings.GetMembaseConfigByBucket("mobilesession")).Insert(sessionId, session);
        }

        #endregion
    }
}