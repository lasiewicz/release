﻿using log4net;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.RestConsumer.V2.Models.Content.BrandConfig;
using Spark.Registration.Adventist.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spark.Registration.Adventist.Framework.Attributes
{
    public class RedirectToSslAttribute : ActionFilterAttribute
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(RedirectToSslAttribute));

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Brand brand = RegistrationConfiguration.Brand;
            bool requiresSSL = Convert.ToBoolean(RuntimeSettings.GetSetting("USE_REGISTRATION_SSL", brand.Site.Community.Id, brand.Site.Id));
            if (requiresSSL)
            {
                //ssl will be at lb so we will need to rely on port
                //lb will append 44 to begining of port to indicate ssl
                string port = filterContext.HttpContext.Request.ServerVariables["SERVER_PORT"];
                bool ssl = ((port.Length > 2 && port.StartsWith("44")) || port == "443");

                if (!ssl)
                {
                    string httpsURL = filterContext.HttpContext.Request.Url.ToString().Replace("http:", "https:");
                    UriBuilder uriBuilder = new UriBuilder(httpsURL);
                    uriBuilder.Port = -1;
                    filterContext.Result = new RedirectResult(uriBuilder.Uri.ToString());
                    Log.Info("SSL redirect url to. server ssl: " + ssl.ToString() + ", port: " + port + ", httpsURL: " + httpsURL + ", uriBuilder: " + uriBuilder.Uri.ToString());
                }
            }
        }
    }
}