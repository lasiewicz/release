﻿using System;
using System.Web.Mvc;
using Spark.Registration.Adventist.Framework.Exceptions;
using Spark.Registration.Adventist.Models;
using log4net;
using Spark.Registration.Dictionaries.Constants;

namespace Spark.Registration.Adventist.Framework.Attributes
{
    public class HandleErrorAttributeWithLogging: HandleErrorAttribute
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(HandleErrorAttributeWithLogging));
        public override void OnException(ExceptionContext filterContext)
        {
            var errorMessage = "Unhandled error: ";
            
            //adding the i.p of the user. 
            var context = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            string hostIP = " IP:" + context.Request.UserHostAddress;

            if (filterContext.HttpContext.Request.Cookies[RegistrationPersistence.COOKIE_NAME] != null && 
                !string.IsNullOrEmpty(filterContext.HttpContext.Request.Cookies[RegistrationPersistence.COOKIE_NAME][PersistenceConstants.SessionID]))
            {
                var sessionID = filterContext.HttpContext.Request.Cookies[RegistrationPersistence.COOKIE_NAME][PersistenceConstants.SessionID];
                errorMessage = "Unhandled error (" + sessionID + "): ";
            }
            
            Log.Error(errorMessage + filterContext.Exception + hostIP);

            Exception innerException = filterContext.Exception.InnerException;

            

            while (innerException != null)
            {
                Log.Error("Unhandled error inner exception " + innerException + hostIP);
                innerException = innerException.InnerException;
            }

            var controllerName = (string)filterContext.RouteData.Values["controller"];
            var actionName = (string)filterContext.RouteData.Values["action"];
            var model = new HandleErrorInfo(filterContext.Exception, controllerName, actionName);
            string viewName = "~/Views/Error/Error.cshtml";
            if(filterContext.Exception is ScenarioException)
            {
                viewName = (actionName == "Splash") ? "~/Views/Error/APIErrorSplash.cshtml"
                                  : "~/Views/Error/APIError.cshtml";
            }
                                  
            var result = new ViewResult
            {
                ViewName = viewName,
                ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
            };
            result.ViewBag.BedrockHost = RegistrationConfiguration.BedrockHost;
            
            filterContext.Result = result;
            filterContext.ExceptionHandled = true;
            base.OnException(filterContext);
        }
    }
}