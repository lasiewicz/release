﻿using System.Web.Mvc;
using Spark.Registration.Adventist.Managers;
using Spark.Registration.Adventist.Models;

namespace Spark.Registration.Adventist.Framework.Attributes
{
    public class RedirectToBedrockForLoggedInMembersAttribute : ActionFilterAttribute    
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var bedrockManager = new BedrockManager();
            bool shouldRedirect = bedrockManager.ShouldRedirectUserToBedrock();
            if(shouldRedirect)
            {
                filterContext.Result = new RedirectResult("http://" + RegistrationConfiguration.BedrockHost + RegistrationConfiguration.BedrockRedirectPath);
            }
        }
    }
}
