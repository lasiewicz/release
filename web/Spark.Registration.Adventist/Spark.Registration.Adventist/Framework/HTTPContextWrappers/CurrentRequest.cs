﻿using System;
using System.Web;
using Spark.Registration.Adventist.Interfaces;
using System.Collections.Specialized;
using Spark.Registration.Adventist.Interfaces.Web;
using Spark.Registration.Dictionaries.Constants;

namespace Spark.Registration.Adventist.Framework.HTTPContextWrappers
{
    public class CurrentRequest : ICurrentRequest
    {
        #region ICurrentContext Members

        private string _clientIP;
        private readonly ICurrentBrowserCapabilities _browserCapabilities;// = new CurrentBrowserCapabilities();

        public CurrentRequest(ICurrentBrowserCapabilities browserCapabilities)
        {
            this._browserCapabilities = browserCapabilities;
        }

        public NameValueCollection QueryString
        {
            get { return HttpContext.Current.Request.QueryString; }
        }

        public NameValueCollection Form
        {
            get { return HttpContext.Current.Request.Form; }
        }

        public HttpCookieCollection Cookies
        {
            get { return HttpContext.Current.Request.Cookies; }
        }

        public ICurrentBrowserCapabilities Browser  
         {
             get { return _browserCapabilities; }
        } 

        public Uri Url
        {
            get { return HttpContext.Current.Request.Url; }
        }

        public string RawUrl
        {
            get { return HttpContext.Current.Request.RawUrl; }
        }
        
        public string UrlReferrer
        {
            get
            {
                if(!string.IsNullOrEmpty(HttpContext.Current.Request[URLConstants.Referrer]))
                {
                    return HttpContext.Current.Request[URLConstants.Referrer];
                }

                return HttpContext.Current.Request.UrlReferrer == null ? string.Empty : HttpContext.Current.Request.UrlReferrer.ToString();
            }
        }

        public string this[string index]
        {
            get { return HttpContext.Current.Request[index]; }
        }

        public string ClientIP
        {
            get
            {
                if (_clientIP == null)
                {
                    _clientIP = HttpContext.Current.Request.Headers["client-ip"] ?? HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
                   
                    Int32 commaPos = _clientIP.IndexOf(",");
                    if (commaPos > -1)
                    {
                        _clientIP = _clientIP.Substring(0, commaPos);
                    }
                }

                return _clientIP;
            }
        }

        public bool IsAjaxRequest
        {
            get
            {
                return ((HttpContext.Current.Request["X-Requested-With"] == "XMLHttpRequest") ||
                        ((HttpContext.Current.Request.Headers != null) &&
                         ((HttpContext.Current.Request.Headers["X-Requested-With"] == "XMLHttpRequest"))));
            }
        }

        public string UserAgent { get { return HttpContext.Current.Request.UserAgent; } }

        public NameValueCollection Params
        {
            get { return HttpContext.Current.Request.Params; }
        }

        #endregion
    }
}
