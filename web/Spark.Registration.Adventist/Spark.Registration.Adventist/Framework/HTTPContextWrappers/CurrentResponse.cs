﻿using System.Web;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Web;

namespace Spark.Registration.Adventist.Framework.HTTPContextWrappers
{
    public class CurrentResponse: ICurrentResponse
    {
        public HttpCookieCollection Cookies
        {
            get { return HttpContext.Current.Response.Cookies; }
        }

        public void AppendCookie(HttpCookie cookie)
        {
            HttpContext.Current.Response.AppendCookie(cookie);
        }

        public void SetCookie(HttpCookie cookie)
        {
            HttpContext.Current.Response.SetCookie(cookie);
        }
    }
}