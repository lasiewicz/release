﻿using System.Collections;
using System.Web;
using Spark.Registration.Adventist.Interfaces;
using Spark.Registration.Adventist.Interfaces.Web;

namespace Spark.Registration.Adventist.Framework.HTTPContextWrappers
{
    public class CurrentContext: ICurrentContext
    {
        public IDictionary Items { get; private set; }

        public CurrentContext()
        {
            if(HttpContext.Current != null)
            {
                Items = HttpContext.Current.Items;
            }
        }

        public bool IsNull
        {
            get { return HttpContext.Current == null; }
        }
    }
}