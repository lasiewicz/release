﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Registration.JDate.Interfaces;

namespace Spark.Registration.JDate.Framework.HTTPContextWrappers
{
    public class CurrentServer: ICurrentServer
    {
        public string MapPath(string path)
        {
            return HttpContext.Current.Server.MapPath(path);
        }
    }
}