﻿using System.Collections.Generic;
using System.Web.Mvc;
using Spark.Registration.Adventist.Interfaces.Adapters;
using Spark.Registration.Adventist.Managers;

namespace Spark.Registration.Adventist.Controllers
{
    public class PixelController : BaseController
    {
        public PartialViewResult Index(string pageName)
        {
            List<string> pixels = new PixelManager().RetrievePixels(pageName);
            return PartialView("Pixels", pixels);
        }
    }
}
