﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Spark.Registration.Adventist.App_Start;
using Spark.Registration.Adventist.Framework.Attributes;
using log4net;
using Spark.Registration.Adventist.Framework.Modules;

namespace Spark.Registration.Adventist
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(MvcApplication));

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttributeWithLogging());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.jpg");
            routes.IgnoreRoute("{resource}.ico");
            //routes.IgnoreRoute("{resource}.woff");
            //routes.IgnoreRoute("{resource}.ttf");
            //routes.IgnoreRoute("{resource}.svg");

            routes.MapRoute(
                "get-omniture", "omniture/{PageName}",
                new { controller = "Omniture", action = "Index" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-pixel", "Pixel/{pageName}",
                new { controller = "Pixel", action = "Index" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-nextStep", "registration/nextstep",
                new { controller = "Registration", action = "GetNextStep" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-previousStep", "registration/previousstep",
                new { controller = "Registration", action = "GetPreviousStep" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-currentStep", "registration/currentstep",
                new { controller = "Registration", action = "GetCurrentStep" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "post-submitStep", "registration/submitstep",
                new { controller = "Registration", action = "SubmitStep" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            //Added as part of Kount integration
            routes.MapRoute(
              "do-dcfiring", "Registration/DoDcFiring",
              new { controller = "Registration", action = "DoDcFiring" });

            routes.MapRoute(
                "get-citiesforzipcode", "regcontent/region/cities/{zipCode}",
                new { controller = "Region", action = "GetCitiesForZipCode" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-cities", "regcontent/region/cities/{countryRegionID}/{stateRegionID}",
                new { controller = "Region", action = "GetCities" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-citieswithsubstring", "regcontent/region/cities/{countryRegionID}/{stateRegionID}/{substring}",
                new { controller = "Region", action = "GetCitiesContainingString" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-states", "regcontent/region/states/{countryRegionID}",
                new { controller = "Region", action = "GetStates" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "get-get404", "errors/notfound",
                new { controller = "admin", action = "get404" },
                new { httpMethod = new HttpMethodConstraint("Get") });

            //routes.MapRoute(
            //    "get-get4042", "errors/notfound",
            //    new { controller = "admin", action = "get404" },
           //     new { httpMethod = new HttpMethodConstraint("Get") });
            
            routes.MapRoute(
                "Confirmation",  // Route name
                "Confirmation",
                new { controller = "Registration", action = "Confirmation" });

            routes.MapRoute(
                "Registration",  // Route name
                "Registration",
                new { controller = "Registration", action = "Registration" });

            routes.MapRoute(
                "Registration-mos",  // Route name
                "Registration/step1",
                new { controller = "Registration", action = "Registration" });

            routes.MapRoute(
                "Error",  // Route name
                "Error",
                new { controller = "Registration", action = "Error" });

            routes.MapRoute(
                "get-healthcheck", "admin/healthcheck",
                new { controller = "admin", action = "gethealthcheck" },
                new { httpMethod = new HttpMethodConstraint("Get") });

            routes.MapRoute(
                "get-bustcache", "regcontent/admin/ClearScenarioCache",
                new { controller = "admin", action = "ClearScenarioCache" },
                new { httpMethod = new HttpMethodConstraint("Get") });

           // routes.MapRoute(
           //     "Error2",  // Route name
            //    "regcontent/Error",
            //    new { controller = "Registration", action = "Error" });

            routes.MapRoute(
                "Default2", // Route name
                "", // URL with parameters
                new { controller = "Registration", action = "Splash"} // Parameter defaults
            );

            routes.MapRoute(
               "ValidateRegAttributeValue", "registration/ValidateAttributeValue",
               new { controller = "Registration", action = "ValidateAttributeValue" },
               new { httpMethod = new HttpMethodConstraint("GET") });


            routes.MapRoute(
               "ValidateMingleAttributeValue", "registration/ValidateMingleAttributeValue",
               new { controller = "Registration", action = "ValidateMingleAttributeValue" },
               new { httpMethod = new HttpMethodConstraint("GET") });

            


        }

        protected void Application_BeginRequest(object sender,   EventArgs e)
        {
            //SessionModule.OpenSession(sender, e);
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            //SessionModule.CloseSession(sender, e);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError().GetBaseException();
            Log.Error(ex);

            var innerException = ex.InnerException;

            while(innerException != null)
            {
                Log.Error(innerException);
            }
        }

        protected void Application_Start()
        {
            var configFile = new System.IO.FileInfo(Server.MapPath("/") + "log4net.config");
            log4net.Config.XmlConfigurator.ConfigureAndWatch(configFile);
            UnityWebActivator.Start();

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            SessionModule sessionModule = new SessionModule();
            sessionModule.Init(this);
        }
    }
}