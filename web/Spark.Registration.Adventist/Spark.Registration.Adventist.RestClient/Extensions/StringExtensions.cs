﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.Registration.Adventist.RestClient.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Check if the source string contains a sub string.
        /// </summary>
        /// <param name="source">The source string</param>
        /// <param name="subString">The sub string to check</param>
        /// <returns>True if the source string contains a sub string. </returns>
        public static bool ContainsIgnoreCase(this string source, string subString)
        {
            if (string.IsNullOrEmpty(subString) || string.IsNullOrEmpty(source))
                return true;

            return source.IndexOf(subString, StringComparison.InvariantCultureIgnoreCase) >= 0;
        }
    }
}
