﻿using System;
using System.Net;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Contrib;
using RestSharp.Serializers;
using Spark.Common.RestConsumer.V2.Models.Member;
using Spark.Registration.Adventist.Interfaces.Adapters;
using Spark.Registration.Adventist.RestClient.Extensions;
using Spark.Registration.Adventist.RestClient.Util;
using Spark.Registration.Models.Web;
using System.Text;
using System.Collections.Generic;

namespace Spark.Registration.Adventist.RestClient.Web
{
    public class AdventistRegistrationAdapter : IMingleRegistrationAdapter
    {
        private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MissingMemberHandling = MissingMemberHandling.Ignore,
            NullValueHandling = NullValueHandling.Include,
            DefaultValueHandling = DefaultValueHandling.Include,
        };

        
        private const string EXTENDED_REGISTRATION_URL = "/member/registerextended";
        private const string VALIDATION_URL = "/member/validateregistrationattribute";
        private const string GET_COUNTRIES = "/content/region/countries";
        private const string GET_STATES = "/content/region/states/1/";
        private const string GET_CITIES = "/content/region/cities/1/";


        private const string OK = "ok";

        private readonly string _baseUrl;
        private readonly string _clientId;
        private readonly string _clientSecret;

        public AdventistRegistrationAdapter(string baseUrl, string clientId, string clientSecret)
        {
            _baseUrl = baseUrl;
            _clientId = clientId;
            _clientSecret = clientSecret;

            if (IsDevelopmentEnvironment())
            {
                ByPassSelfSignedCertificatesCheck();
            }
        }

        /// <summary>
        /// Call the API with a registration request
        /// </summary>
        /// <param name="registerRequest">The registration request data</param>
        /// <returns>The registration result</returns>
        public MingleRegistrationResponse RegisterExtended(MingleRegistrationRequest registerRequest)
        {
            // create the client
            var client = CreateClient();

            // set the action to registration
            var request = CreateRequest(EXTENDED_REGISTRATION_URL);

            // add the request body content
            request.AddBody(registerRequest);

            // call the API
            var response = client.Execute<MingleRegistrationResponse>(request);

            // response
            return response.Data;
        }

        /// <summary>
        /// Call the API with a registration request to validate all attributes are correct.
        /// </summary>
        /// <param name="registerRequest">The registration request data</param>
        /// <returns>The registration validation result</returns>
        public bool ValidateExtended(MingleValidationRequest registerRequest)
        {
            // create the client
            var client = CreateClient();

            // set the action to registration
            var request = CreateRequest(VALIDATION_URL);

            // add the request body content
            request.AddBody(registerRequest);

            // call the API
            var response = client.Execute<MingleValidationResponse>(request);

            // response


            if (response.Data.Status.ToLower().Equals(OK))
            {
                // not going to do any null checks because we expect the api to return the below value. If if doesnt the exception will be caught and logged by the caller. 
                return Convert.ToBoolean(response.Data.Data["attributeValueExists"]);
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Error in AdventistRegistrationAdapter ValidateExtended getting data from Mingle API: ");
                sb.Append(VALIDATION_URL).Append(" ");
                sb.Append(" attributename: ").Append(registerRequest.attributeName);
                sb.Append(" attributevalue: ").Append(registerRequest.attributeValue);
                sb.Append(" Code: ").Append(response.Data.Code);
                sb.Append(" Status: ").Append(response.Data.Status);

                throw new Exception(sb.ToString());
            }
            
        }


        public List<Region> GetCountries()
        {
            // create the client
            var client = CreateClient();

            // set the action to registration
            var request = CreateRequest(GET_COUNTRIES);

            
            // call the API
            var response = client.Execute<MingleRegionResponse>(request);

            // response
            if (response.Data.Status.ToLower().Equals(OK))
            {

                return response.Data.data;
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Error in AdventistRegistrationAdapter GetCountries getting data from Mingle API: ");
                sb.Append(GET_COUNTRIES).Append(" ");
                sb.Append(" Code: ").Append(response.Data.Code);
                sb.Append(" Status: ").Append(response.Data.Status);

                throw new Exception(sb.ToString());
            }
        }

       public List<Region> GetStatesForCountry(int regionId)
        {
            // create the client
            var client = CreateClient();

            // set the action to registration
            var request = CreateRequest(GET_STATES+regionId);

            // call the API
            var response = client.Execute<MingleRegionResponse>(request);

            // response
            if (response.Data.Status.ToLower().Equals(OK))
            {

                return response.Data.data;
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Error in AdventistRegistrationAdapter GetCountries getting data from Mingle API: ");
                sb.Append(GET_STATES + regionId).Append(" ");
                sb.Append(" Code: ").Append(response.Data.Code);
                sb.Append(" Status: ").Append(response.Data.Status);

                throw new Exception(sb.ToString());
            }
        }


       public List<Region>  GetCitiesForCountryState(int countryId, int stateId)
       {
           // create the client
           var client = CreateClient();

           // set the action to registration
           var request = CreateRequest(GET_CITIES + countryId + "/" + stateId);

           // call the API
           var response = client.Execute<MingleRegionResponse>(request);

           // response
           if (response.Data.Status.ToLower().Equals(OK))
           {

               return response.Data.data;
           }
           else
           {
               StringBuilder sb = new StringBuilder();
               sb.Append("Error in AdventistRegistrationAdapter GetCitiesForCountryState getting data from Mingle API: ");
               sb.Append(GET_COUNTRIES + countryId + "/" + stateId).Append(" ");
               sb.Append(" Code: ").Append(response.Data.Code);
               sb.Append(" Status: ").Append(response.Data.Status);

               throw new Exception(sb.ToString());
           }
       }


        /// <summary>
        /// Create a new REST request object.
        /// </summary>
        /// <param name="action">The API action we want to call</param>
        /// <param name="method">The API method to use</param>
        /// <returns>A new REST request object</returns>
        private RestRequest CreateRequest(string action, RestSharp.Method method = Method.POST)
        {
            // create the new rest request envelope
            var request = new RestSharp.RestRequest(  action, method);

            // add authentication parameters
            request.AddParameter("applicationId", _clientId, ParameterType.QueryString);
            request.AddParameter("client_secret", _clientSecret, ParameterType.QueryString);

            // set content type to json
            request.AddHeader("Accept", "application/json;");
            request.AddHeader("Content-type", "application/json; charset=utf-8");

            // set the serializer
            request.JsonSerializer = new SparkRestSharpSerializer(Settings);
            request.RequestFormat = DataFormat.Json;

            return request;
        }
        
        /// <summary>
        /// Create a new REST client.
        /// </summary>
        /// <returns>The new REST client.</returns>
        private RestSharp.RestClient CreateClient()
        {
            // create a new client with the base url
            return new RestSharp.RestClient(_baseUrl);
        }

        /// <summary>
        /// When in development environment we might get SSL errors
        /// from connecting to a server with a self-signed certificate.
        /// </summary>
        private void ByPassSelfSignedCertificatesCheck()
        {
            ServicePointManager.ServerCertificateValidationCallback +=
                (sender, certificate, chain, sslPolicyErrors) => true;
        }

        /// <summary>
        /// Check if we are calling a development or staging API.
        /// </summary>
        /// <returns>True if we are calling a development or staging API.</returns>
        private bool IsDevelopmentEnvironment()
        {
            return _baseUrl.ContainsIgnoreCase("dev") || _baseUrl.ContainsIgnoreCase("stg");
        }
    }
}
