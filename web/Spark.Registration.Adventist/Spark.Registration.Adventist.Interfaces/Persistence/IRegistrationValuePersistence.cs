﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Spark.Registration.Adventist.Interfaces.Persistence
{
    public interface IRegistrationValuePersistence
    {
        string this[string index] { get; }
        string Name { get; }
        DateTime Expires { get; set; }
        NameValueCollection Values { get; }
        bool HasValues { get; }
        void Persist();
        void Clear();
        void RemoveValue(string name);
        void AddValue(string name, string value);
        void AddAttributeValue(string name, string value);
        Dictionary<string, string> GetAttributeValues();
    }
}