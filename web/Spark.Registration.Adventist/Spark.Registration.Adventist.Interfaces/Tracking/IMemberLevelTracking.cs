﻿using Spark.Common.MemberLevelTracking;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Adventist.Interfaces.Web;

namespace Spark.Registration.Adventist.Interfaces.Tracking
{
    public interface IMemberLevelTracking
    {
        string GetOs(ICurrentRequest request);
        string GetFormFactor(ICurrentRequest request);
        bool IsMobileDevice(ICurrentRequest request);
        bool IsTablet(ICurrentRequest request);
        DeviceType GetDeviceType(ICurrentRequest request, Session userSession);
        Application GetApplication(ICurrentRequest request, Session userSession);
    }
}