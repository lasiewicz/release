﻿using System.Collections;

namespace Spark.Registration.Adventist.Interfaces.Web
{
    public interface ICurrentContext
    {
        IDictionary Items { get; }
        bool IsNull { get; }
    }
}