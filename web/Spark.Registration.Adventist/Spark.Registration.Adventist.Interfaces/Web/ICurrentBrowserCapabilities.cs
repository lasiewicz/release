﻿namespace Spark.Registration.Adventist.Interfaces.Web
{
    public interface ICurrentBrowserCapabilities
    {
        string Platform { get; }
        int MajorVersion { get; }
        bool IsMobileDevice { get; }
        string MobileDeviceManufacturer { get; }
        string MobileDeviceModel { get; }
        int ScreenPixelsHeight { get; }
        int ScreenPixelsWidth { get; }
        string this[string index] { get; }
    }
}