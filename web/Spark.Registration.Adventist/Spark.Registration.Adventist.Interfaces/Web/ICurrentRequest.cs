﻿using System;
using System.Collections.Specialized;
using System.Web;

namespace Spark.Registration.Adventist.Interfaces.Web
{
    public interface ICurrentRequest
    {
        NameValueCollection QueryString { get; }
        NameValueCollection Form { get; }
        NameValueCollection Params { get; }
        HttpCookieCollection Cookies { get; }
        ICurrentBrowserCapabilities Browser { get; }
        string this[string index] { get; }
        Uri Url { get; }
        string RawUrl { get; }
        string UrlReferrer { get; }
        string ClientIP { get; }
        bool IsAjaxRequest { get; }
        string UserAgent { get; }
    }
}
