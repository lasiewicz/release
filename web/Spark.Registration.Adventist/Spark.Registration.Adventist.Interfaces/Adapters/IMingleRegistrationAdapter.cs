﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spark.Common.RestConsumer.V2.Models.Member;
using Spark.Registration.Models.Web;

namespace Spark.Registration.Adventist.Interfaces.Adapters
{
    public interface IMingleRegistrationAdapter
    {
        MingleRegistrationResponse RegisterExtended(MingleRegistrationRequest register);
        bool ValidateExtended(MingleValidationRequest register);
        List<Region> GetCountries();
        List<Region> GetStatesForCountry(int regionId);
        List<Region> GetCitiesForCountryState(int countryId, int stateId);
    }
}
