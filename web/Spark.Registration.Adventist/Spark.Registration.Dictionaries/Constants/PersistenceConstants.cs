﻿namespace Spark.Registration.Dictionaries.Constants
{
    public class PersistenceConstants
    {
        public const string ScenarioName = "SCENARIO_NAME";
        public const string CurrentStep = "CURRENT_STEP";
        public const string LastCompletedStep = "LAST_COMPLETED_STEP";
        public const string RegionCountryID = "COUNTRY_REGIONID";
        public const string RegionStateID = "STATE_REGIONID";
        public const string RegionCityID = "CITY_REGIONID";
        public const string RegionZipCode = "REGION_ZIPCODE";
        public const string SessionID = "REGSESSIONID";
        public const string RegistrationStartRecorded = "REG_START_RECORDED";
        public const string RecaptureID = "RECAPTUREID";
        public const string SessionTrackingID = "SESSION_TRACKING_ID";
    }
}