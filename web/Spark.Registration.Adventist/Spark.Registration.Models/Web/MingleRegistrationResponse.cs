﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spark.Common.RestConsumer.V2.Models.Member;

namespace Spark.Registration.Models.Web
{
    public class MingleRegistrationResponse : MingleResponse
    {
        /// <summary>
        /// The registration result data
        /// </summary>
        public ExtendedRegisterResult Data { get; set; }
    }
}
