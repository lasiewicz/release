﻿using System.Collections.Generic;

namespace Spark.Registration.Models.Web
{
    public class MingleValidationResponse : MingleResponse
    {
        /// <summary>
        /// Any additional data the API sends
        /// </summary>
        public Dictionary<string, object> Data { get; set; }
    }
}
