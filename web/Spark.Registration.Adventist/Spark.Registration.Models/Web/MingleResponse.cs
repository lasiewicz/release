﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.Registration.Models.Web
{
    public class MingleResponse
    {
        /// <summary>
        /// The response status description
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// The response status code.
        /// </summary>
        public int Code { get; set; }
    }
}
