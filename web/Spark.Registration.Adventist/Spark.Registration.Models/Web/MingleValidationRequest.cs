﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.Registration.Models.Web
{
    public class MingleValidationRequest : MingleResponse
    {
        public string attributeName { get; set; }
        public string attributeValue { get; set; }
    }
}
