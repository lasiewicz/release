using System;
using System.Web.Security;
using System.Web.UI.WebControls;
using Spark.ResourceAdmin.ValueObjects;
using Spark.ResourceAdmin.ValueObjects.ServiceDefinitions;

namespace Spark.ResourceAdmin
{
	/// <summary>
	/// Summary description for Login.
	/// </summary>
	public class Login : System.Web.UI.Page
	{
		protected TextBox	UsernameTextBox;
		protected TextBox	PasswordTextBox;
		protected Button	LoginButton;
		protected Label		ErrorLabel;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		private void LoginButton_Click(object sender, EventArgs e)
		{
			AuthInfo authInfo = new AuthInfo(UsernameTextBox.Text, PasswordTextBox.Text);

			if (ValidCredentials(authInfo))
			{
				// save credentials in session
				// TODO: this is ugly, ugly, ugly, replace with something more secure!
				Session["VssAuthInfo"] = authInfo;

				FormsAuthentication.RedirectFromLoginPage(UsernameTextBox.Text, false);
			}
		}

		private bool ValidCredentials(AuthInfo authInfo)
		{
			IResourceAdminService resourceAdminService = ResourceAdminService.Instance.ResouceAdminProxy;
			OperationResult result = resourceAdminService.Login(authInfo);

			if (! result.Success)
			{
				ErrorLabel.Text = result.ErrorMessage;
			}

			return result.Success;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			this.LoginButton.Click += new EventHandler(LoginButton_Click);
		}
		#endregion
	}
}
