<%@ Page language="c#" Codebehind="Login.aspx.cs" AutoEventWireup="false" Inherits="Spark.ResourceAdmin.Login" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
	<head>
		<title>Login</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name=vs_defaultClientScript content="JavaScript">
		<meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" href="StyleSheet.css">
	</head>
	<body>
		
		<form id="Form1" method="post" runat="server">
		
			<h1>Resource Admin Login</h1>
			
			<table>
				<tr>
					<td class="headerText">Username:</td>
					<td>
						<asp:TextBox ID="UsernameTextBox" Runat="server" />
					</td>
				</tr>
				<tr>
					<td class="headerText">Password:</td>
					<td>
						<asp:TextBox ID="PasswordTextBox" TextMode="Password" Runat="server" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:Label ID="ErrorLabel" ForeColor="Red" Runat="server" />
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<asp:Button ID="LoginButton" Text="Login" Runat="server" />
					</td>
				</tr>
			</table>

		</form>
		
	</body>
</html>
