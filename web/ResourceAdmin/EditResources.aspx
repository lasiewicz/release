<%@ Page language="c#" 
	Codebehind="EditResources.aspx.cs" 
	AutoEventWireup="false" 
	ValidateRequest="false" 
	Inherits="Spark.ResourceAdmin.EditResources" 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Bedrock Resource Editor</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="StyleSheet.css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<h2>Bedrock Resource Editor</h2>
			<input onclick="window.close()" type="button" value="Close Window"> <input type="hidden" name="FileVersion" id="FileVersion" value="<%=FileVersion%>" >
			<asp:button id="LogoutButton" Text="Logout" Runat="server"></asp:button><br>
			<asp:label id="GeneralErrorLabel" Runat="server" ForeColor="Red"></asp:label><br>
			<asp:panel id="CloneSourcesPanel" Runat="server">
				<P>The resource file for this control does not yet exist. You can start editing 
					from scratch or you can clone values from a related file. Please make your 
					selection below and then click <STRONG>Continue</STRONG>.
				</P>
				<asp:DropDownList id="CloneSourcesList" Runat="server"></asp:DropDownList>
				<asp:Button id="SelectCloneSourceButton" Runat="server" Text="Continue >>"></asp:Button>
				<BR>
				<asp:RequiredFieldValidator id="CloneSourceListRequiredValidator" Runat="server" ControlToValidate="CloneSourcesList"
					Display="Dynamic" EnableClientScript="True" ErrorMessage="Please make a selection" InitialValue=""></asp:RequiredFieldValidator>
			</asp:panel><asp:panel id="ResourceEditPanel" Runat="server">
<TABLE>
					<TR>
						<TD class="headerText">Resource:</TD>
						<TD class="valueText">
							<asp:Label id="BasePathLabel" Runat="server"></asp:Label></TD>
					</TR>
					<TR class="alternateRow">
						<TD class="headerText">Site:</TD>
						<TD class="valueText">
							<asp:Label id="SiteLabel" Runat="server"></asp:Label></TD>
					</TR>
					<TR>
						<TD class="headerText">Culture:</TD>
						<TD class="valueText">
							<asp:Label id="CultureLabel" Runat="server"></asp:Label></TD>
					</TR>
					<TR class="alternateRow">
						<TD class="headerText">Resource File:</TD>
						<TD class="valueText">
							<asp:Label id="ResxFileNameLabel" Runat="server"></asp:Label></TD>
					</TR>
					<TR>
						<TD class="headerText">Clone Source:</TD>
						<TD class="valueText">
							<asp:Label id="CloneSourceResxFileNameLabel" Runat="server"></asp:Label></TD>
					</TR>
				</TABLE><BR>You 
can make changes to the resource values in the fields below. To save your 
changes, click <STRONG>Save Changes</STRONG>. 
<asp:HyperLink id="lnkGlobalResx" runat="server" NavigateUrl="http://" ToolTip="Click here if you can't find the key in this file. There is a chance this control is using a global resource instead of a local one."
					Target="_blank">Can't find key? Try global resx..</asp:HyperLink><BR>
<asp:Button id="SaveChangesButton2" Runat="server" Text="Save Changes"></asp:Button>
<asp:Label id="OperationStatusLabel" Runat="server" ForeColor="Red" EnableViewState="False"
					Font-Bold="True"></asp:Label>
<asp:LinkButton id="ReloadButton" Runat="server">Reload Now</asp:LinkButton>
<asp:Label id="Label1" Runat="server" ForeColor="Red" EnableViewState="False" Font-Bold="True"></asp:Label>
<asp:DataGrid id="ResourceDataGrid" Runat="server" AutoGenerateColumns="False" AlternatingItemStyle-CssClass="alternateRow"
					HeaderStyle-CssClass="headerText">
					<AlternatingItemStyle CssClass="alternateRow" VerticalAlign="Top"></AlternatingItemStyle>
					<ItemStyle VerticalAlign="Top"></ItemStyle>
					<HeaderStyle CssClass="headerText"></HeaderStyle>
					<Columns>
						<asp:TemplateColumn HeaderText="Key Name">
							<ItemTemplate>
								<a id="jumpanchor" runat="server" name='<%# DataBinder.Eval(Container, "DataItem.Key") %>'>
									<%# FormatAnchorKey(DataBinder.Eval(Container,"DataItem.Key").ToString()) %>
								</a>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="Value">
							<HeaderTemplate>
							</HeaderTemplate>
							<ItemTemplate>
								<asp:TextBox id=ResourceValueTextBox Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Value") %>' TextMode="Multiline" Rows="6" Columns="80">
								</asp:TextBox>
							</ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</asp:DataGrid><BR>
<asp:Button id="SaveChangesButton" Runat="server" Text="Save Changes"></asp:Button>
			</asp:panel></form>
	</body>
</HTML>
