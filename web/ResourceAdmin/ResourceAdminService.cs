using System;
using Spark.ResourceAdmin.ValueObjects.ServiceDefinitions;

namespace Spark.ResourceAdmin {
	/// <summary>
	/// The single instance of the remote service.
	/// </summary>
	internal class ResourceAdminService {

		private IResourceAdminService _ResourceAdminService;

		public static ResourceAdminService Instance = new ResourceAdminService();

		private ResourceAdminService() {
			_ResourceAdminService = (IResourceAdminService)Activator.GetObject(typeof(IResourceAdminService), "tcp://localhost:12345/ResourceAdminService.rem");
		}


		public IResourceAdminService ResouceAdminProxy{
			get {
				return _ResourceAdminService;
			}
		}
	}
}
