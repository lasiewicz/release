using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Resources;
using System.Web.Security;
using System.Web.UI.WebControls;
using Spark.ResourceAdmin.ValueObjects;
using Spark.ResourceAdmin.ValueObjects.ServiceDefinitions;

namespace Spark.ResourceAdmin 
{
	/// <summary>
	/// Summary description for EditResources.
	/// </summary>
	public class EditResources : System.Web.UI.Page 
	{
		private bool			_showValuesInResourceDataGrid = true;
		private string _Anchor = string.Empty;

		protected Label						GeneralErrorLabel;
		protected Label						BasePathLabel;
		protected Label						SiteLabel;
		protected Label						CultureLabel;
		protected Label						ResxFileNameLabel;
		protected Label						CloneSourceResxFileNameLabel;
		protected Panel						CloneSourcesPanel;
		protected DropDownList				CloneSourcesList;
		protected Label						OperationStatusLabel;
		protected Button					SelectCloneSourceButton;
		protected Panel						ResourceEditPanel;
		protected DataGrid					ResourceDataGrid;
		protected Button					SaveChangesButton;
		protected Button					SaveChangesButton2;
		protected RequiredFieldValidator	CloneSourceListRequiredValidator;
		protected System.Web.UI.WebControls.HyperLink lnkGlobalResx;
		protected Button					LogoutButton;
		protected LinkButton				ReloadButton;
		protected System.Web.UI.WebControls.Label Label1;
		

		private int _fileVersion;
		public int FileVersion
		{
			get { return _fileVersion; }
			set { _fileVersion = value; }
		}

		private string ResxFileName 
		{
			get { return (null == ViewState["ResxFileName"]) ? "" : ViewState["ResxFileName"].ToString(); }
			set { ViewState["ResxFileName"] = value; }
		}
		private string BaseResxFileName 
		{
			get { return (null == ViewState["BaseResxFileName"]) ? "" : ViewState["BaseResxFileName"].ToString(); }
			set { ViewState["BaseResxFileName"] = value; }
		}
		private string CloneSourceResxFileName 
		{
			get { return (null == ViewState["CloneSourceResxFileName"]) ? "" : ViewState["CloneSourceResxFileName"].ToString(); }
			set { ViewState["CloneSourceResxFileName"] = value; }
		}
		//		private string MasterResxFileName {
		//			get { return (null == ViewState["MasterResxFileName"]) ? "" : ViewState["MasterResxFileName"].ToString(); }
		//			set { ViewState["MasterResxFileName"] = value; }
		//		}

		public string FormatAnchorKey(string Key)
		{
			if (_Anchor == string.Empty || Key == string.Empty)
			{
				return Key;
			}
			if (_Anchor == Key)
			{
				return String.Format("<strong>{0}</strong>",Key);
			}
			else 
			{
				return Key;
			}
		}

		private void Page_Load(object sender, System.EventArgs e) 
		{
			
			
			_Anchor = Request.QueryString["resourcename"];

			if (! IsPostBack) 
			{
				// read in and validate querystring arguments
				ProcessArgumentsAndSetResxFileName();

				// when returning from a save/add operation, the status
				// will be passed in on the QueryString, show it.
				//				OperationStatusLabel.Text = Server.HtmlEncode(Request["OperationStatus"]);

				// if the specific resource file does not exist,
				// show the list of related resource files that could
				// be used as a clone source (if any exist!).
				// if the file does exist, display the edit fields

				if (File.Exists(ResxFileName)) 
				{
					// make sure you get the latest version from SourceSafe - lucena
					string vssFilePath = ConfigurationSettings.AppSettings["BedrockVssRootFolder"];
					if (null == vssFilePath || 0 == vssFilePath.Length) 
					{
						throw new ConfigurationException("Bedrock VSS web application root has not been configured for this application.");
					}
					vssFilePath += Path.Combine(vssFilePath, ResxFileName.Substring(ConfigurationSettings.AppSettings["BedrockWebAppRootFolder"].Length)).Replace('\\', '/');

					IResourceAdminService service = ResourceAdminService.Instance.ResouceAdminProxy;
					OperationResult result = service.Get((AuthInfo) Session["VssAuthInfo"], vssFilePath, ResxFileName);

					// set the file version number to the page's hidden field
					this.FileVersion = service.GetVersion((AuthInfo) Session["VssAuthInfo"], vssFilePath, ResxFileName);

					if (! result.Success) 
					{
						throw new Exception(result.ErrorMessage);
					}

				} 
				else
				{
					// if the resources directory is missing, there is nothing for us to do here
					if (! Directory.Exists(Path.GetDirectoryName(BaseResxFileName)))
					{
						GeneralErrorLabel.Text = "There are no resources available for this control.";
						CloneSourcesPanel.Visible = ResourceEditPanel.Visible = false;
						return;
					}

					string[] cloneSources = Directory.GetFiles(Path.GetDirectoryName(BaseResxFileName), Path.GetFileName(BaseResxFileName) + ".*");
					if (0 == cloneSources.Length) 
					{
						return;
					}

					CloneSourcesPanel.Visible = true;
					ResourceEditPanel.Visible = false;

					// list clone sources
					PopulateCloneSourceGrid(cloneSources);

					return;
				}

				CloneSourcesPanel.Visible = false;
				ResourceEditPanel.Visible = true;

				CloneSourceResxFileNameLabel.Text = "N/A";

				// set up resources for editing
				PopulateResourceGrid();
			}
		}

		/// <summary>
		///		Read in and validate arguments that determine the resx file to be edited:
		///			- control type
		///			- site url
		///			- culture
		/// </summary>
		/// <remarks>
		///		The method sets the _resxFileName field.
		/// </remarks>
		private void ProcessArgumentsAndSetResxFileName() 
		{
			// initialize resx file name to the root web app folder
			string resxFileName = System.Configuration.ConfigurationSettings.AppSettings["BedrockWebAppRootFolder"];
			if (null == resxFileName || 0 == resxFileName.Length) 
			{
				throw new ConfigurationException("Bedrock web application root has not been configured for this application.");
			}
			if (! Directory.Exists(resxFileName)) 
			{
				throw new ConfigurationException(string.Format("The configured Bedrock web application root folder does not exist. [{0}]", resxFileName));
			}

			// append base path
			string basePath = Request.QueryString["basepath"];
			if (null == basePath || 0 == basePath.Length) 
			{
				throw new ArgumentException("Missing the base path of the control for which resource editing was requested.", "basepath");
			}
			BasePathLabel.Text = basePath;
			resxFileName = Path.Combine(resxFileName, basePath);
			BaseResxFileName = resxFileName;
			//			MasterResxFileName = resxFileName + ".en-US.resx";

			// append site
			string site = Request.QueryString["site"];
			if (null == site || 0 == site.Length) 
			{
				throw new ArgumentException("Missing the site URL for which resource editing was requested.", "site");
			}
			SiteLabel.Text = site;
			resxFileName += "." + site;

			// append culture
			string culture = Request.QueryString["culture"];
			if (null == culture || 0 == culture.Length) 
			{
				throw new ArgumentException("Missing the culture for which resource editing was requested.", "culture");
			}
			CultureLabel.Text = culture;
			resxFileName += "." + culture;

			// add resx extension and set the member field
			ResxFileName = resxFileName + ".resx";
			ResxFileNameLabel.Text = ResxFileName;

			lnkGlobalResx.NavigateUrl = String.Format(@"/EditResources.aspx?basepath=_Resources\Global&site={0}&culture={1}&resourcename={2}#{2}",
				site,culture,_Anchor);
				

		}

		private void PopulateCloneSourceGrid(string[] cloneSourceFileNames) 
		{
			CloneSourcesList.Items.Clear();
			CloneSourcesList.Items.Add(new ListItem("<select clone source>", ""));
			CloneSourcesList.Items.Add(new ListItem("-- Start from scratch --", "scratch"));
			foreach(string cloneSourceFileName in cloneSourceFileNames) 
			{
				CloneSourcesList.Items.Add(new ListItem(Path.GetFileName(cloneSourceFileName), cloneSourceFileName));
			}
		}

		private void PopulateResourceGrid() 
		{
			string resxFileName = ResxFileName;
			Hashtable valuePairs = new Hashtable();

			// if culture + site specific resource file does not exist, try loading specified clone source
			if ("" != CloneSourceResxFileName) 
			{
				resxFileName = CloneSourceResxFileName;
			}

			//			// first load the resource names from the master file
			//			// make sure the file exists
			//			if (! File.Exists(MasterResxFileName)) {
			//				throw new FileNotFoundException(string.Format("Missing resource file. [{0}]", MasterResxFileName), MasterResxFileName);
			//			}
			//
			//			using (ResXResourceReader reader = new ResXResourceReader(MasterResxFileName)) {
			//				// iterate through the resources and load the contents into the Hashtable
			//				foreach (DictionaryEntry entry in reader) {
			//					valuePairs.Add(entry.Key.ToString(), "");
			//				}
			//
			//				// close the reader
			//				reader.Close();
			//			}

			// if not starting from scratch, load up resource values
			if (_showValuesInResourceDataGrid) 
			{
				// make sure the file exists
				if (! File.Exists(resxFileName)) 
				{
					throw new FileNotFoundException(string.Format("Missing resource file. [{0}]", ResxFileName), ResxFileName);
				}

				// read in resource value pairs
				using (ResXResourceReader reader = new ResXResourceReader(resxFileName)) 
				{
					// iterate through the resources and load the contents into the Hashtable
					foreach (DictionaryEntry entry in reader) 
					{
						valuePairs[entry.Key.ToString()] = entry.Value;
					}
					// close the reader
					reader.Close();
				}
			}

			// populate the grid
			ResourceDataGrid.DataSource = valuePairs;
			ResourceDataGrid.DataKeyField = "Key";
			ResourceDataGrid.DataBind();
		}

		private void SaveChangesButton_Click(object sender, EventArgs e) 
		{
			// collect resource names and values from the grid
			SortedList updatedValues = new SortedList();
			for (int x = 0; x < ResourceDataGrid.Items.Count; x++) 
			{
				updatedValues.Add(
					ResourceDataGrid.DataKeys[x].ToString(), 
					(ResourceDataGrid.Items[x].FindControl("ResourceValueTextBox") as TextBox).Text
					);
			}

			// compute the VSS file path
			string vssFilePath = ConfigurationSettings.AppSettings["BedrockVssRootFolder"];
			if (null == vssFilePath || 0 == vssFilePath.Length) 
			{
				throw new ConfigurationException("Bedrock VSS web application root has not been configured for this application.");
			}
			vssFilePath += Path.Combine(vssFilePath, ResxFileName.Substring(ConfigurationSettings.AppSettings["BedrockWebAppRootFolder"].Length)).Replace('\\', '/');

			// if the resource file exists: check out / save / checkin
			// if it does not yet exist: save / add
			if (File.Exists(ResxFileName)) 
			{
				// check out resx file for editing
				IResourceAdminService service = ResourceAdminService.Instance.ResouceAdminProxy;

				// compare file versions before checking out
				int vssVersion = service.GetVersion((AuthInfo) Session["VssAuthInfo"], vssFilePath, ResxFileName);
				if (vssVersion > Int32.Parse(Request.Form["FileVersion"]))
				{
					OperationStatusLabel.Text = "A newer version of this resource file is available. Please copy your changes to a temporary file, and ";
					ReloadButton.Visible = true;
				}
				else
				{
					// carry on ...there's no version conflict

					ReloadButton.Visible = false;

					OperationResult result = service.Checkout((AuthInfo) Session["VssAuthInfo"], vssFilePath, ResxFileName);
					if (! result.Success) 
					{
						throw new Exception(result.ErrorMessage);
					}

					// write out updated values
					WriteUpdatesToResourceFile(updatedValues, ResxFileName);

					// check in file
					result = service.Checkin((AuthInfo) Session["VssAuthInfo"], vssFilePath, ResxFileName);
					if (! result.Success) 
					{
						throw new Exception(result.ErrorMessage);
					}
				}
			}
			else 
			{
				// write out updated values
				WriteUpdatesToResourceFile(updatedValues, ResxFileName);

				// add file
				IResourceAdminService service = ResourceAdminService.Instance.ResouceAdminProxy;
				OperationResult result = service.Add((AuthInfo) Session["VssAuthInfo"], vssFilePath, ResxFileName);
				if (! result.Success) 
				{
					throw new Exception("Failed adding file to source safe:" + result.ErrorMessage);
				}
			}

			OperationStatusLabel.Text = "Your changes have been saved. " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
			// repaint page
			//Response.Redirect(Request.Url.PathAndQuery + "&OperationStatus=" + Server.UrlEncode("Your changes have been saved. " + DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss")));
		}


		private void WriteUpdatesToResourceFile(SortedList updatedValues, string resxFileName) 
		{
			string tmpFilePath = Path.GetTempFileName();
			using (ResXResourceWriter writer = new ResXResourceWriter(tmpFilePath)) 
			{
				foreach (DictionaryEntry entry in updatedValues) 
				{
					object key = entry.Key;
					object value = entry.Value;

					writer.AddResource(key.ToString(), value);
				}

				writer.Close();
				writer.Dispose();
			}

			// replace original resource file with new one
			if (File.Exists(resxFileName)) 
			{
				File.Delete(resxFileName);
			}
			File.Move(tmpFilePath, resxFileName);
		}

		private void SelectCloneSourceButton_Click(object sender, EventArgs e) 
		{
			// validate inputs
			if (! IsValid) 
			{
				return;
			}

			// get selected clone source
			string cloneSource = CloneSourcesList.SelectedValue;

			// if "scratch", set the flag that will indicate to datagrid loading code
			// to only load the resource names but not values.
			// otherwise, just set the clone source to the supplied value.
			if ("scratch" == cloneSource) 
			{
				_showValuesInResourceDataGrid = false;
			}
			else 
			{
				CloneSourceResxFileName = cloneSource;
				CloneSourceResxFileNameLabel.Text = cloneSource;
			}

			// hide clone selector and show the grid
			CloneSourcesPanel.Visible = false;
			ResourceEditPanel.Visible = true;
			PopulateResourceGrid();
		}

		private void LogoutButton_Click(object sender, EventArgs e) 
		{
			FormsAuthentication.SignOut();
			Response.Redirect(Request.Url.PathAndQuery);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) 
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() 
		{    
			this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
			this.SelectCloneSourceButton.Click += new System.EventHandler(this.SelectCloneSourceButton_Click);
			this.SaveChangesButton2.Click += new System.EventHandler(this.SaveChangesButton_Click);
			this.ReloadButton.Click += new System.EventHandler(this.ReloadButton_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void ReloadButton_Click(object sender, System.EventArgs e)
		{
			Response.Redirect(Request.Url.ToString());
		}
	}
}
