﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.Search.Interfaces;
using Matchnet.Web.Applications.HotList;
using Matchnet.Web.Framework.Exceptions;
using Matchnet.Web.Framework.Managers;
using Bedrock.Tests.Mocks;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Framework.Util;
using NUnit.Framework;

namespace Bedrock.Tests
{
    [TestFixture]
    public class FacebookValueTranslatorTests
    {
        [Test]
        public void TranslateFirstNameOnlyReturnsFirstName()
        {
            FacebookValueTranslator fbvt = new FacebookValueTranslator();
            string fullName = "Matt Mishkoff";
            string firstName = fbvt.TranslateFirstName(fullName);
            Assert.IsTrue(firstName == "Matt"); 
        }

        [Test]
        public void TranslateBirthdateParsesCorrectly()
        {
            FacebookValueTranslator fbvt = new FacebookValueTranslator();
            string birthdateString = "05/09/75";
            DateTime birthdate = fbvt.TranslateBirthdate(birthdateString);
            Assert.IsTrue(birthdate == DateTime.Parse("05/09/75"));
        }

        public void TranslateBirthdateReturnsDateMinForInvalidDate()
        {
            FacebookValueTranslator fbvt = new FacebookValueTranslator();
            string birthdateString = "05/09/744445";
            DateTime birthdate = fbvt.TranslateBirthdate(birthdateString);
            Assert.IsTrue(birthdate == DateTime.MinValue);
        }
    }

    
}
