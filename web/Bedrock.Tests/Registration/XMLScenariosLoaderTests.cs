﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Bedrock.Tests.Mocks;
using Matchnet.Web.Framework.Enumerations;
using Matchnet.Web.Framework.TemplateControls;
using NUnit.Framework;

namespace Bedrock.Tests.Registration
{
    [TestFixture]
    public class XMLScenariosLoaderTests: AbstractBaseUnitTest
    {
        [Test]
        public void TestLoaderFullRegistration()
        {
            MockCurrentServer currentServer = new MockCurrentServer();
            currentServer.SetMappedPath(ContentPath + "Registration\\Registration_103..xml");

            XMLRegistrationScenariosLoader registrationScenariosLoader = new XMLRegistrationScenariosLoader();
            registrationScenariosLoader.CurrentServer = currentServer;

            Scenarios scenarios = registrationScenariosLoader.LoadScenarios(GetJdateBrand(),
                                                                            RegistrationFlowType.FullRegistration,
                                                                            RegistrationScenarioType.Dynamic);

            Assert.IsTrue(scenarios.ScenarioList.Count == 2);
            Assert.IsTrue(currentServer.GetLastMappedPathRequest() == "/Applications/Registration/XML/Registration_103.xml");
        }

        [Test]
        public void TestLoaderSplashRegistration()
        {
            MockCurrentServer currentServer = new MockCurrentServer();
            currentServer.SetMappedPath(ContentPath + "Registration\\SplashRegistration_103.xml");

            XMLRegistrationScenariosLoader registrationScenariosLoader = new XMLRegistrationScenariosLoader();
            registrationScenariosLoader.CurrentServer = currentServer;

            Scenarios scenarios = registrationScenariosLoader.LoadScenarios(GetJdateBrand(),
                                                                            RegistrationFlowType.Splash,
                                                                            RegistrationScenarioType.Dynamic);

            Assert.IsTrue(scenarios.ScenarioList.Count == 2);
            Assert.IsTrue(currentServer.GetLastMappedPathRequest() == "/Applications/Registration/XML/SplashRegistration_103.xml");
        }

        [Test]
        public void TestLoaderRegistrationOverlay()
        {
            MockCurrentServer currentServer = new MockCurrentServer();
            currentServer.SetMappedPath(ContentPath + "Registration\\RegistrationOverlay_103.xml");

            XMLRegistrationScenariosLoader registrationScenariosLoader = new XMLRegistrationScenariosLoader();
            registrationScenariosLoader.CurrentServer = currentServer;

            Scenarios scenarios = registrationScenariosLoader.LoadScenarios(GetJdateBrand(),
                                                                            RegistrationFlowType.Overlay,
                                                                            RegistrationScenarioType.Dynamic);

            Assert.IsTrue(scenarios.ScenarioList.Count == 2);
            Assert.IsTrue(currentServer.GetLastMappedPathRequest() == "/Applications/Registration/XML/RegistrationOverlay_103.xml");
        }

        [Test]
        public void TestLoaderRegistrationStatic()
        {
            MockCurrentServer currentServer = new MockCurrentServer();
            currentServer.SetMappedPath(ContentPath + "Registration\\SplashRegistrationStatic_103.xml");

            XMLRegistrationScenariosLoader registrationScenariosLoader = new XMLRegistrationScenariosLoader();
            registrationScenariosLoader.CurrentServer = currentServer;

            Scenarios scenarios = registrationScenariosLoader.LoadScenarios(GetJdateBrand(),
                                                                            RegistrationFlowType.Splash,
                                                                            RegistrationScenarioType.Static);

            Assert.IsTrue(scenarios.ScenarioList.Count == 9);
            Assert.IsTrue(currentServer.GetLastMappedPathRequest() == "/Applications/Registration/XML/SplashRegistrationStatic_103.xml");
        }
    }
}
