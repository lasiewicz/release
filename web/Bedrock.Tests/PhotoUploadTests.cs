﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Exceptions;
using Matchnet.Web.Framework.Managers;
using Bedrock.Tests.Mocks;

using NUnit.Framework;

namespace Bedrock.Tests
{
    [TestFixture]
    public class PhotoUploadTests : AbstractBaseUnitTest
    {
        private const int MAX_FILE_SIZE = 5120000;
        private const int MAX_PHOTOS = 12;

        private MockSettingsService _settingsService;

        [TestFixtureSetUp]
        public void StartUp()
        {
            _settingsService = new MockSettingsService();
            _settingsService.AddSetting("MAX_PHOTO_COUNT", MAX_PHOTOS.ToString());
        }

        [Test]
        public void TestGetPhotos()
        {
            MockMember member = new MockMember();
            member.AddPhoto(GeneratePhoto(123, "1000.jpg", 1));
            member.AddPhoto(GeneratePhoto(456, "2000.jpg", 2));

            MemberPhotoUpdateManager manager = GetInitializedManager(member, GetJdateBrand(), true);
            
            List<Photo> photos = manager.GetPhotos();
            Assert.IsTrue(photos.Count == 2);
            Photo firstPhoto = (from Photo p in photos where p.MemberPhotoID == 123 select p).FirstOrDefault();
            Assert.IsTrue(firstPhoto != null);
            Assert.IsTrue(firstPhoto.ListOrder == 1);

            Photo secondPhoto = (from Photo p in photos where p.MemberPhotoID == 456 select p).FirstOrDefault();
            Assert.IsTrue(secondPhoto != null);
            Assert.IsTrue(secondPhoto.ListOrder == 2);

        }

        [Test]
        public void TestGetPhotosNotPreloaded()
        {
            MockMember member = new MockMember();
            member.AddPhoto(GeneratePhoto(123, "1000.jpg", 1));
            member.AddPhoto(GeneratePhoto(456, "2000.jpg", 2));

            //this test ensures that photos are loaded when calling GetPhotos even when the manager was initialized with 
            //pre-loading set to false
            MemberPhotoUpdateManager manager = GetInitializedManager(member, GetJdateBrand(), false);
            List<Photo> photos = manager.GetPhotos();
            Assert.IsTrue(photos.Count == 2);
            Photo firstPhoto = (from Photo p in photos where p.MemberPhotoID == 123 select p).FirstOrDefault();
            Assert.IsTrue(firstPhoto != null);
            Assert.IsTrue(firstPhoto.ListOrder == 1);

            Photo secondPhoto = (from Photo p in photos where p.MemberPhotoID == 456 select p).FirstOrDefault();
            Assert.IsTrue(secondPhoto != null);
            Assert.IsTrue(secondPhoto.ListOrder == 2);
        }

        [Test]
        public void TestGetPaddedPhotos()
        {
            MockMember member = new MockMember();
            member.AddPhoto(GeneratePhoto(123, "1000.jpg", 1));
            member.AddPhoto(GeneratePhoto(456, "2000.jpg", 2));

            MemberPhotoUpdateManager manager = GetInitializedManager(member, GetJdateBrand(), true);
            List<Photo> photos = manager.GetPaddedToMaxPhotos();
            Assert.IsTrue(photos.Count == MAX_PHOTOS);

            Photo firstPhoto = (from Photo p in photos where p.MemberPhotoID == 123 select p).FirstOrDefault();
            Assert.IsTrue(firstPhoto != null);
            Assert.IsTrue(firstPhoto.ListOrder == 1);

            Photo secondPhoto = (from Photo p in photos where p.MemberPhotoID == 456 select p).FirstOrDefault();
            Assert.IsTrue(secondPhoto != null);
            Assert.IsTrue(secondPhoto.ListOrder == 2);

            int numPaddedPhotos = (from Photo p in photos where p.MemberPhotoID == Constants.NULL_INT select p).Count();
            Assert.IsTrue(numPaddedPhotos == (MAX_PHOTOS-2));
        }

        [Test]
        public void AddAndSaveOneNewPhotoWorks()
        {
            MockMember member = new MockMember();
            Brand brand = GetJdateBrand();
            MemberPhotoUpdateManager manager = GetInitializedManager(member, GetJdateBrand(), false);
            manager.AddUpdate(Constants.NULL_INT, "My Photo", false, 1, GetNewFileBytes(),false);
            PhotoUpdateResult result = manager.SaveUpdates();

            Assert.IsTrue(result == PhotoUpdateResult.Success);

            MockPhotoSaveService mockPhotoSaveService = manager.PhotosSaveService as MockPhotoSaveService;
            Assert.IsTrue(mockPhotoSaveService.PhotoUpdates.Count()==1);
            Assert.IsTrue(mockPhotoSaveService.PhotoUpdates[0].Caption == "My Photo");
            Assert.IsTrue(mockPhotoSaveService.PhotoUpdates[0].ListOrder == 1);

            MockMemberSaveService mockMemberSaveService = manager.MemberSaveService as MockMemberSaveService;
            Assert.IsTrue(mockMemberSaveService.Member != null);
            Assert.IsTrue(mockMemberSaveService.Member as MockMember == member);
            Assert.IsTrue(mockMemberSaveService.Member.GetAttributeDate(brand, "LastUpdated") > DateTime.MinValue);
        }

        [Test]
        public void InvalidImageTypeCaught()
        {
            MockMember member = new MockMember();
            MemberPhotoUpdateManager manager = GetInitializedManager(member, GetJdateBrand(), false);
            (manager.PhotoRulesManager as MockPhotoRulesManager).SetAcceptedImageTypeResult(false);

            Exception expectedException = null;
            try
            {
                manager.AddUpdate(Constants.NULL_INT, "My Photo", false, 1, GetNewFileBytes(), false);
            }
            catch (ImageTypeNotSupportedException ex)
            {
                expectedException = ex;
            }

            Assert.IsTrue(expectedException != null);
            Assert.IsTrue(expectedException is ImageTypeNotSupportedException);
        }

        [Test]
        public void FailedPhotoSaveHandledCorrectly()
        {
            MockMember member = new MockMember();
            Brand brand = GetJdateBrand();
            MemberPhotoUpdateManager manager = GetInitializedManager(member, brand, false);
            (manager.PhotosSaveService as MockPhotoSaveService).SetResult(false);

            manager.AddUpdate(Constants.NULL_INT, "My Photo", false, 1, GetNewFileBytes(), false);
            PhotoUpdateResult result = manager.SaveUpdates();

            Assert.IsTrue(result == PhotoUpdateResult.Failure);
            MockMemberSaveService mockMemberSaveService = manager.MemberSaveService as MockMemberSaveService;
            Assert.IsTrue(mockMemberSaveService.Member == null);
        }

        [Test]
        public void TooManyHandledCorrectly()
        {
            MockMember member = new MockMember();
            Brand brand = GetJdateBrand();
            MemberPhotoUpdateManager manager = GetInitializedManager(member, brand, false);

            for (int i = 1; i <= MAX_PHOTOS + 1; i++)
            {
                manager.AddUpdate(Constants.NULL_INT, "My Photo " +i.ToString() , false, (byte)i, GetNewFileBytes(), false);
            }

            PhotoUpdateResult result = manager.SaveUpdates();
            Assert.IsTrue(result == PhotoUpdateResult.TooManyPhotos);
        }


        private Photo GeneratePhoto(int memberPhotoID, string webPath, byte listOrder)
        {
            return new Photo(memberPhotoID, 0, webPath, listOrder);
            //
        }

        private byte[] GetNewFileBytes()
        {
            ///this passes a jpeg signature test
            byte[] Bytes = { 0xFF, 0xD8, 0xFF, 0xE0, 0x2F, 0x2F, 0x4A, 0x46, 0x49, 0x46, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F };
            return Bytes;
        }

        private byte[] GetInvalidNewFileBytes()
        {
            ///this passes a jpeg signature test
            byte[] Bytes = { 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x4A, 0x46, 0x49, 0x46, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F };
            return Bytes;
        }

        private MemberPhotoUpdateManager GetInitializedManager(IMember member, Brand brand, bool loadExistingPhotos)
        {
            MemberPhotoUpdateManager manager = new MemberPhotoUpdateManager(member, brand, loadExistingPhotos);
            manager.SettingsManager.SettingsService = _settingsService;
            manager.PhotoRulesManager = new MockPhotoRulesManager();
            manager.MemberSaveService = new MockMemberSaveService();
            manager.PhotosSaveService = new MockPhotoSaveService();
            return manager;
        }
    }
}
