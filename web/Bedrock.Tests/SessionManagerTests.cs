﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bedrock.Tests.Mocks;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using NUnit.Framework;

namespace Bedrock.Tests
{
    [TestFixture]
    public class SessionManagerTests : AbstractBaseUnitTest
    {
        [Test]
        public void TestGetSessionNonAPIWorks()
        {
            MockSettingsService.AddSetting(SettingConstants.IS_DEVELOPMENT_MODE, "true");
            SessionManager sessionManager = new SessionManager();
            sessionManager.SessionService = new MockSessionService();
            sessionManager.CurrentRequest = new CurrentRequestMock();

            UserSession currentSession = sessionManager.GetCurrentSession(GetJdateBrand());
            Assert.IsTrue(currentSession != null);
        }

        [Test]
        public void TestGetSessionFromIDWorks()
        {
            string key = Guid.NewGuid().ToString();
            string encrpytedKey = Matchnet.Security.Crypto.Encrypt(WebConstants.SPARK_WS_CRYPT_KEY, key);

            SessionManager sessionManager = new SessionManager();
            sessionManager.SessionService = new MockSessionService();
            
            CurrentRequestMock currentRequestMock = new CurrentRequestMock();
            currentRequestMock.AddRequestParameter("SPWSCYPHER", encrpytedKey);
            sessionManager.CurrentRequest = currentRequestMock;

            UserSession currentSession = sessionManager.GetSessionFromID();
            Assert.IsTrue(currentSession != null);

        }

        [Test]
        public void TestGetSessionFromIDWorksIfMissingSPWCYPHER()
        {
            MockSettingsService.AddSetting(SettingConstants.IS_DEVELOPMENT_MODE, "true");
            SessionManager sessionManager = new SessionManager();
            sessionManager.SessionService = new MockSessionService();
            sessionManager.CurrentRequest = new CurrentRequestMock();

            UserSession currentSession = sessionManager.GetSessionFromID();
            Assert.IsTrue(currentSession != null);
        }
    }
}
