﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bedrock.Tests.Mocks;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Web.Framework.Managers;
using NUnit.Framework;

namespace Bedrock.Tests
{
    [TestFixture]
    public class LoggingManagerTests : AbstractBaseUnitTest
    {
        [Test]
        public void BasicLoggingWorks()
        {
            LoggingManager loggingManager = new LoggingManager();
            loggingManager.CurrentRequest = getMockContext();
            Brand brand = GetJdateBrand();
            MockMember member = new MockMember();
            member.MemberID = 12345;

            try
            {
                throw new Exception("here's an exception!");
            }
            catch (Exception ex)
            {
                loggingManager.LogException(ex, member, brand, "testing");

            }
            Assert.IsTrue(1==1);

        }

        private CurrentRequestMock getMockContext()
        {
            CurrentRequestMock currentRequestMock = new CurrentRequestMock();
            return currentRequestMock;
        }
    }
}
