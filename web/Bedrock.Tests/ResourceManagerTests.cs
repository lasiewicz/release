﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Bedrock.Tests.Mocks;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Managers;
using NUnit.Framework;

namespace Bedrock.Tests
{
    [TestFixture]
    public class ResourceManagerTests: AbstractBaseUnitTest
    {
        
        [TestFixtureSetUp]
        public void Setup()
        {
            MockSettingsService.AddSetting(SettingConstants.IS_DEVELOPMENT_MODE, "true");
            MockSettingsService.AddSetting(SettingConstants.SITE_NAME_ALIAS, "JDate");
            MockSettingsService.AddSetting(SettingConstants.SHOW_RESOURCE_HINTS, "false");
        }

        [Test]
        public void TestBasicFuntionality()
        {
            Brand jdateBrand = GetJdateBrand();
            ResourceManager resourceManager = new ResourceManager(jdateBrand);
            resourceManager.LocalizerService = new MockLocalizer();
            
            string result = resourceManager.GetResource(jdateBrand, "constant");
            Assert.IsTrue(result == string.Empty);

            result = resourceManager.GetResource("constant", this);
            Assert.IsTrue(result == string.Empty);

            result = resourceManager.GetResource("constant", this, new string[]{"abc", "def"});
            Assert.IsTrue(result == string.Empty);

            result = resourceManager.GetResource("constant", this, new StringDictionary());
            Assert.IsTrue(result == string.Empty);

            result = resourceManager.GetResource("constant", this, new StringDictionary(), new string[] { "abc", "def" });
            Assert.IsTrue(result == string.Empty);
        }

    }
}
