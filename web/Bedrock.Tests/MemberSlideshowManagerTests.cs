﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.Search.Interfaces;
using Matchnet.Web.Applications.HotList;
using Matchnet.Web.Framework.Exceptions;
using Matchnet.Web.Framework.Managers;
using Bedrock.Tests.Mocks;
using Matchnet.Web.Framework.Util;
using NUnit.Framework;
using SearchType = Spark.SAL.SearchType;

namespace Bedrock.Tests
{
    [TestFixture]
    public class MemberSlideshowManagerTests : AbstractBaseUnitTest
    {
        [TestFixtureSetUp]
        public void StartUp()
        {
            MockSettingsService.AddSetting("MEMBER_SLIDESHOW_USE_CONFIGURATION", "true");
            MockSettingsService.AddSetting("ENABLE_CONFIGURABLE_SLIDESHOW", "true");
            MockSettingsService.AddSetting("MEMBER_SLIDESHOW_PRIORITIZE_EXPIRING_MEMBERS", "true");
            MockSettingsService.AddSetting("MEMBER_SLIDESHOW_AGE_FACTOR", "7");
        }

        [Test]
        public void GetConfigurableSettingsReturnsPersistenceIfInitialized()
        {
            MemberSlideshowManager manager = GetInitializedMemberSlideshowManager(12345, GetJdateBrand());
            MockSlideshowPersistence persitence = GetInitializedMockPersistence(25, 99, 12345, SearchType.Region, Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT);
            manager.SetPersistence(persitence);
            MemberSlideshowSettings settings = manager.GetConfigurableSlideshowSettings();

            Assert.IsTrue(settings.MinAge==25);
            Assert.IsTrue(settings.MaxAge == 99);
            Assert.IsTrue(settings.RegionID == 12345);
            Assert.IsTrue(settings.SearchType == SearchType.Region);
            Assert.IsTrue(settings.AreaCode1 == Constants.NULL_INT);
            Assert.IsTrue(settings.AreaCode2 == Constants.NULL_INT);
            Assert.IsTrue(settings.AreaCode3 == Constants.NULL_INT);
            Assert.IsTrue(settings.AreaCode4 == Constants.NULL_INT);
            Assert.IsTrue(settings.AreaCode5 == Constants.NULL_INT);
            Assert.IsTrue(settings.AreaCode6 == Constants.NULL_INT);
        }

        [Test]
        public void GetConfigurableSettingsInitializesFromSearchPrefsCorrectly()
        {
            MemberSlideshowManager manager = GetInitializedMemberSlideshowManager(11111, GetJdateBrand());
            MockSlideshowPersistence persitence = new MockSlideshowPersistence();
            persitence.MinAge = Constants.NULL_INT;
            manager.SetPersistence(persitence);
            manager.SearchPreferencesService = GetInitializedSearchPrefService(25, 99, SearchType.Region, 12345);
            MockSettingsService.AddSetting("MEMBER_SLIDESHOW_AGE_FACTOR", "0");
            
            MemberSlideshowSettings settings = manager.GetConfigurableSlideshowSettings();
            Assert.IsTrue(settings.MinAge == 25);
            Assert.IsTrue(settings.MaxAge == 99);
            Assert.IsTrue(settings.SearchType == SearchType.Region);
            Assert.IsTrue(settings.RegionID == 12345);
            Assert.IsTrue(settings.AreaCode1 <= 0 );
            Assert.IsTrue(settings.AreaCode2 <= 0);
            Assert.IsTrue(settings.AreaCode3 <= 0);
            Assert.IsTrue(settings.AreaCode4 <= 0);
            Assert.IsTrue(settings.AreaCode5 <= 0);
            Assert.IsTrue(settings.AreaCode6 <= 0);
        }

        [Test]
        public void GetConfigurableSettingsInitializesFromSearchPrefsCorrectlyWithAreaCodes()
        {
            MemberSlideshowManager manager = GetInitializedMemberSlideshowManager(11111, GetJdateBrand());
            MockSlideshowPersistence persitence = new MockSlideshowPersistence();
            persitence.MinAge = Constants.NULL_INT;
            manager.SetPersistence(persitence);
            manager.SearchPreferencesService = GetInitializedSearchPrefService(25, 99, SearchType.AreaCode, 12345, 213, 323, 972, 310,214, 111);
            MockSettingsService.AddSetting("MEMBER_SLIDESHOW_AGE_FACTOR", "0");

            MemberSlideshowSettings settings = manager.GetConfigurableSlideshowSettings();
            Assert.IsTrue(settings.MinAge == 25);
            Assert.IsTrue(settings.MaxAge == 99);
            Assert.IsTrue(settings.SearchType == SearchType.AreaCode);
            Assert.IsTrue(settings.RegionID == 12345);
            Assert.IsTrue(settings.AreaCode1 == 213);
            Assert.IsTrue(settings.AreaCode2 == 323);
            Assert.IsTrue(settings.AreaCode3 == 972);
            Assert.IsTrue(settings.AreaCode4 == 310);
            Assert.IsTrue(settings.AreaCode5 == 214);
            Assert.IsTrue(settings.AreaCode6 == 111);
        }

        [Test]
        [ExpectedException("Matchnet.Search.ValueObjects.Exceptions.InvalidSearchPreferencesException")]
        public void InvalidPreferencesThrowException()
        {
            MemberSlideshowManager manager = GetInitializedMemberSlideshowManager(11111, GetJdateBrand());
            MockSlideshowPersistence persitence = new MockSlideshowPersistence();
            persitence.MinAge = Constants.NULL_INT;
            manager.SetPersistence(persitence);
            manager.SearchPreferencesService = GetInitializedSearchPrefServiceWithInvalidPrefs();

            MemberSlideshowSettings settings = manager.GetConfigurableSlideshowSettings();
        }

        [Test]
        public void AgeRangeRelaxedCorrectly()
        {
            MemberSlideshowManager manager = GetInitializedMemberSlideshowManager(11111, GetJdateBrand());
            MockSlideshowPersistence persitence = new MockSlideshowPersistence();
            persitence.MinAge = Constants.NULL_INT;
            manager.SetPersistence(persitence);
            manager.SearchPreferencesService = GetInitializedSearchPrefService(50, 50, SearchType.Region, 12345);
            MockSettingsService.AddSetting("MEMBER_SLIDESHOW_AGE_FACTOR", "10");

            MemberSlideshowSettings settings = manager.GetConfigurableSlideshowSettings();
            Assert.IsTrue(settings.MinAge == 40);
            Assert.IsTrue(settings.MaxAge == 60);
        }

        [Test]
        public void AgeRangeNotRelaxedOverLimits()
        {
            MemberSlideshowManager manager = GetInitializedMemberSlideshowManager(11111, GetJdateBrand());
            MockSlideshowPersistence persitence = new MockSlideshowPersistence();
            persitence.MinAge = Constants.NULL_INT;
            manager.SetPersistence(persitence);
            manager.SearchPreferencesService = GetInitializedSearchPrefService(18, 99, SearchType.Region, 12345);
            MockSettingsService.AddSetting("MEMBER_SLIDESHOW_AGE_FACTOR", "10");

            MemberSlideshowSettings settings = manager.GetConfigurableSlideshowSettings();
            Assert.IsTrue(settings.MinAge == 18);
            Assert.IsTrue(settings.MaxAge == 99);
        }

        [Test]
        public void SettingsPersistedCorrecty()
        {
            MemberSlideshowManager manager = GetInitializedMemberSlideshowManager(11111, GetJdateBrand());
            MockSlideshowPersistence persitence = new MockSlideshowPersistence();
            manager.SetPersistence(persitence);

            MemberSlideshowSettings settings = new MemberSlideshowSettings(25, 99, 12345, SearchType.AreaCode, 213,323,972,214,111,222);
            manager.SaveConfigurableSlideshowSettings(settings);

            Assert.IsTrue(persitence.Saved);
            Assert.IsTrue(persitence.MinAge==25);
            Assert.IsTrue(persitence.MaxAge == 99);
            Assert.IsTrue(persitence.RegionID == 12345);
            Assert.IsTrue(persitence.SearchType == SearchType.AreaCode);
            Assert.IsTrue(persitence.AreaCode1 == 213);
            Assert.IsTrue(persitence.AreaCode2 == 323);
            Assert.IsTrue(persitence.AreaCode3 == 972);
            Assert.IsTrue(persitence.AreaCode4 == 214);
            Assert.IsTrue(persitence.AreaCode5 == 111);
            Assert.IsTrue(persitence.AreaCode6 == 222);
        }

        [Test]
        public void RemoveFromCacheCalled()
        {
            MemberSlideshowManager manager = GetInitializedMemberSlideshowManager(11111, GetJdateBrand());
            manager.RemoveSlideshowFromCache(1111, 3);

            MockMemberSlideshowService mockMemberSlideshowService = (manager.MemberSlideshowService as MockMemberSlideshowService);
            Assert.IsTrue(mockMemberSlideshowService.RemoveFromCacheCalled);
        }

        [Test]
        public void ConfigurableSlideshowEnabledCorrect()
        {
            MemberSlideshowManager manager = GetInitializedMemberSlideshowManager(11111, GetJdateBrand());
            
            MockSettingsService.AddSetting("ENABLE_CONFIGURABLE_SLIDESHOW", "true");
            Assert.IsTrue(manager.IsConfigurableSlideshowEnabled());

            MockSettingsService.AddSetting("ENABLE_CONFIGURABLE_SLIDESHOW", "false");
            Assert.IsTrue(!manager.IsConfigurableSlideshowEnabled());
        }

        [Test]
        public void SlideshowUsesConfigurationCorrect()
        {
            MemberSlideshowManager manager = GetInitializedMemberSlideshowManager(11111, GetJdateBrand());

            MockSettingsService.AddSetting("MEMBER_SLIDESHOW_USE_CONFIGURATION", "true");
            Assert.IsTrue(manager.SlideshowUsesConfiguration());

            MockSettingsService.AddSetting("MEMBER_SLIDESHOW_USE_CONFIGURATION", "false");
            Assert.IsTrue(!manager.SlideshowUsesConfiguration());
        }

        [Test]
        public void GetConfigurableSlideshowWorksCorrectly()
        {
            MemberSlideshowManager manager = GetInitializedMemberSlideshowManager(11111, GetJdateBrand());
            MockSettingsService.AddSetting("MEMBER_SLIDESHOW_USE_CONFIGURATION", "true");
            MockSettingsService.AddSetting("MEMBER_SLIDESHOW_PRIORITIZE_EXPIRING_MEMBERS", "true");

            MockSlideshowPersistence persitence = GetInitializedMockPersistence(18, 99, 12345, SearchType.Region, Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT);
            manager.SetPersistence(persitence);
            manager.SearchPreferencesService = GetInitializedSearchPrefService(18, 99, SearchType.Region, 12345);
            (manager.RegionService as MockRegionService).SetRegionLanguage(GetRegionLanguageForSearch());

            MockMemberSlideshowService mockMemberSlideshowService =manager.MemberSlideshowService as MockMemberSlideshowService;
            mockMemberSlideshowService.InitializeSlideshow(11111, 3, new List<int>());

            Slideshow slideshow = manager.GetSlideshow();
            
            Assert.IsTrue(slideshow != null);
            Assert.IsTrue(mockMemberSlideshowService.SetBrand.BrandID == GetJdateBrand().BrandID);
            Assert.IsTrue(mockMemberSlideshowService.SetMemberID == 11111);
            Assert.IsTrue(mockMemberSlideshowService.SetSearchPrefs["GenderMask"] == "5");
            Assert.IsTrue(mockMemberSlideshowService.SetSearchPrefs["DomainID"] == GetJdateBrand().Site.Community.CommunityID.ToString());
            Assert.IsTrue(mockMemberSlideshowService.SetSearchPrefs["SearchTypeID"] == SearchType.Region.ToString("d"));
            Assert.IsTrue(mockMemberSlideshowService.SetSearchPrefs["Radius"] == "160");
            Assert.IsTrue(mockMemberSlideshowService.SetSearchPrefs["HasPhotoFlag"] == "1");
            Assert.IsTrue(mockMemberSlideshowService.SetSearchPrefs["MinAge"] == "18");
            Assert.IsTrue(mockMemberSlideshowService.SetSearchPrefs["MaxAge"] == "99");
            Assert.IsTrue(mockMemberSlideshowService.SetSearchPrefs["RegionID"] == "12345");
            Assert.IsTrue(mockMemberSlideshowService.SetSearchPrefs["CountryRegionID"] == "223");
        }

        [Test]
        public void GetFixedSlideshowWorksCorrectly()
        {
             MemberSlideshowManager manager = GetInitializedMemberSlideshowManager(11111, GetJdateBrand());
             MockSettingsService.AddSetting("MEMBER_SLIDESHOW_USE_CONFIGURATION", "false");

             MockMemberSlideshowService mockMemberSlideshowService = manager.MemberSlideshowService as MockMemberSlideshowService;
             mockMemberSlideshowService.InitializeSlideshow(11111, 3, new List<int>());

             Slideshow slideshow = manager.GetSlideshow();
             Assert.IsTrue(slideshow != null);
             Assert.IsTrue(mockMemberSlideshowService.SetBrand.BrandID == GetJdateBrand().BrandID);
             Assert.IsTrue(mockMemberSlideshowService.SetMemberID == 11111);
             Assert.IsTrue(mockMemberSlideshowService.SetSearchPrefs == null);
             Assert.IsTrue(mockMemberSlideshowService.SetPrioritizeExpiringMembers == false);
         }

        private MockSearchPreferencesService GetInitializedSearchPrefServiceWithInvalidPrefs()
        {
            MockSearchPreferencesService searchPrefsService = new MockSearchPreferencesService();
            searchPrefsService.AddPreference("MinAge", "18");
            searchPrefsService.AddPreference("MaxAge", "99");
            return searchPrefsService;
        }

        private RegionLanguage GetRegionLanguageForSearch()
        {
            RegionLanguage rl = new RegionLanguage();
            rl.UpdateCountryInfo(223, "US", "USA", 1);
            return rl;
        }

        private MockSearchPreferencesService GetInitializedSearchPrefService(int minAge, int maxAge, SearchType searchType, int regionID)
        {
            MockSearchPreferencesService searchPrefsService = new MockSearchPreferencesService();
            searchPrefsService.AddPreference("GenderMask", "5");
            searchPrefsService.AddPreference("MinAge", minAge.ToString());
            searchPrefsService.AddPreference("MaxAge", maxAge.ToString());
            searchPrefsService.AddPreference("SearchTypeID", searchType.ToString("d"));
            searchPrefsService.AddPreference("RegionID", regionID.ToString());
            searchPrefsService.AddPreference("SearchOrderBy", QuerySorting.Proximity.ToString("d")); 
            return searchPrefsService;
        }

        private MockSearchPreferencesService GetInitializedSearchPrefService(int minAge, int maxAge, SearchType searchType, int regionID,
            int areaCode1, int areaCode2, int areaCode3, int areaCode4, int areaCode5, int areaCode6)
        {
            MockSearchPreferencesService searchPrefsService = GetInitializedSearchPrefService(minAge, maxAge, searchType, regionID);
            searchPrefsService.AddPreference("AreaCode1", areaCode1.ToString());
            searchPrefsService.AddPreference("AreaCode2", areaCode2.ToString());
            searchPrefsService.AddPreference("AreaCode3", areaCode3.ToString());
            searchPrefsService.AddPreference("AreaCode4", areaCode4.ToString());
            searchPrefsService.AddPreference("AreaCode5", areaCode5.ToString());
            searchPrefsService.AddPreference("AreaCode6", areaCode6.ToString());
            return searchPrefsService;
        }

        

        private MockSlideshowPersistence GetInitializedMockPersistence(int minAge, int maxAge, int regionID, SearchType searchType,
            int areaCode1, int areaCode2, int areaCode3, int areaCode4, int areaCode5, int areaCode6)
        {
            MockSlideshowPersistence persistence = new MockSlideshowPersistence();
            persistence.MinAge = minAge;
            persistence.MaxAge = maxAge;
            persistence.RegionID = regionID;
            persistence.SearchType = searchType;
            persistence.AreaCode1 = areaCode1;
            persistence.AreaCode2 = areaCode2;
            persistence.AreaCode3 = areaCode3;
            persistence.AreaCode4 = areaCode4;
            persistence.AreaCode5 = areaCode5;
            persistence.AreaCode6 = areaCode6;
            return persistence;

        }

        private MemberSlideshowManager GetInitializedMemberSlideshowManager(int memberID, Brand brand)
        {
            MemberSlideshowManager manager = new MemberSlideshowManager(memberID, brand);
            manager.MemberSlideshowService = new MockMemberSlideshowService();
            manager.RegionService = new MockRegionService();
            manager.SearchPreferencesService = new MockSearchPreferencesService();
            manager.SettingsManager.SettingsService = MockSettingsService;
            manager.SetPersistence(new MockSlideshowPersistence());
            return manager;
        }
    }
}
