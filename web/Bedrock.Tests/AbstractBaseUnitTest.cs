﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Bedrock.Tests.Mocks;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Bedrock.Tests
{
    public abstract class AbstractBaseUnitTest
    {
        MockSettingsService _mockSettingsService = new MockSettingsService();
        
        protected Brand GetJdateBrand()
        {
            Community community = new Community(3, "JDate");
            Site site = new Site(103, community, "JDate.com", 1, 2, "en-US", 223, 7, 1, "/lib/css/jd_jc.css", 1255,
                                 DirectionType.ltr, -8, "www", "https://www.jdate.com", 3);
            return new Brand(1003, site, "Jdate.com", 9, "(877) 453-3861", 25, 40, 50);
        }

        protected MockSettingsService MockSettingsService
        {
            get
            {
                return _mockSettingsService;
            }
            set{}
        }

        protected string ContentPath
        {
            get
            {
                string currentDirectory = Directory.GetCurrentDirectory();
                return currentDirectory.Substring(0, currentDirectory.IndexOf("bin")) + "Content\\";
            }
        }

    }
}
