﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bedrock.Tests.Mocks;
using Matchnet;
using Matchnet.Web.Framework.Managers;
using NUnit.Framework;

namespace Bedrock.Tests
{
    [TestFixture]
    public class SettingsManagerTests: AbstractBaseUnitTest
    {
        
        
        [Test]
        public void GetBooleanWorksCorrectly()
        {
            MockSettingsService.AddSetting("ENABLE_X", "true");
            
            SettingsManager settingsManager = new SettingsManager();
            settingsManager.LoggingManager = new LoggingManagerMock();
            settingsManager.SettingsService = MockSettingsService;

            bool value = settingsManager.GetSettingBool("ENABLE_X", GetJdateBrand());
            Assert.IsTrue(value == true);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void GetBooleanCatchesNullBrand()
        {
            MockSettingsService.AddSetting("ENABLE_X", "true");

            SettingsManager settingsManager = new SettingsManager();
            settingsManager.LoggingManager = new LoggingManagerMock();
            settingsManager.SettingsService = MockSettingsService;

            bool value = settingsManager.GetSettingBool("ENABLE_X", null);
        }

        [Test]
        public void GetBooleanHandlesMissingSetting()
        {
            MockSettingsService.AddSetting("ENABLE_X", "true");
            LoggingManagerMock loggingMock = new LoggingManagerMock();

            SettingsManager settingsManager = new SettingsManager();
            settingsManager.LoggingManager = loggingMock;
            settingsManager.SettingsService = MockSettingsService;

            bool value = settingsManager.GetSettingBool("ENABLE_Y", GetJdateBrand());

            Assert.IsTrue(value == false);
            Assert.IsTrue(!String.IsNullOrEmpty(loggingMock.LoggedMessage));
        }

        [Test]
        public void GetIntWorksCorrectly()
        {
            MockSettingsService.AddSetting("INT_X", "10");
            
            SettingsManager settingsManager = new SettingsManager();
            settingsManager.LoggingManager = new LoggingManagerMock();
            settingsManager.SettingsService = MockSettingsService;

            int value = settingsManager.GetSettingInt("INT_X", GetJdateBrand());
            Assert.IsTrue(value == 10);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void GetIntCatchesNullBrand()
        {
            MockSettingsService.AddSetting("INT_X", "10");

            SettingsManager settingsManager = new SettingsManager();
            settingsManager.LoggingManager = new LoggingManagerMock();
            settingsManager.SettingsService = MockSettingsService;

            int value = settingsManager.GetSettingInt("INT_X", null);
        }

        [Test]
        public void GetIntHandlesMissingSetting()
        {
            MockSettingsService.AddSetting("INT_X", "10");
            LoggingManagerMock loggingMock = new LoggingManagerMock();

            SettingsManager settingsManager = new SettingsManager();
            settingsManager.LoggingManager = loggingMock;
            settingsManager.SettingsService = MockSettingsService;

            int value = settingsManager.GetSettingInt("INT_Y", GetJdateBrand());

            Assert.IsTrue(value == Constants.NULL_INT);
            Assert.IsTrue(!String.IsNullOrEmpty(loggingMock.LoggedMessage));
        }

        [Test]
        public void GetStringWorksCorrectly()
        {
            MockSettingsService.AddSetting("MESSAGE", "Hello World");

            SettingsManager settingsManager = new SettingsManager();
            settingsManager.LoggingManager = new LoggingManagerMock();
            settingsManager.SettingsService = MockSettingsService;

            string value = settingsManager.GetSettingString("MESSAGE", GetJdateBrand());
            Assert.IsTrue(value == "Hello World");
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void GetStringCatchesNullBrand()
        {
            MockSettingsService.AddSetting("MESSAGE", "Hello World");

            SettingsManager settingsManager = new SettingsManager();
            settingsManager.LoggingManager = new LoggingManagerMock();
            settingsManager.SettingsService = MockSettingsService;

            string value = settingsManager.GetSettingString("MESSAGE", null);
        }

        [Test]
        public void GetStringHandlesMissingSetting()
        {
            MockSettingsService.AddSetting("MESSAGE", "Hello World");
            LoggingManagerMock loggingMock = new LoggingManagerMock();

            SettingsManager settingsManager = new SettingsManager();
            settingsManager.LoggingManager = loggingMock;
            settingsManager.SettingsService = MockSettingsService;

            string value = settingsManager.GetSettingString("MESSAGE_2", GetJdateBrand());

            Assert.IsTrue(value == string.Empty);
            Assert.IsTrue(!String.IsNullOrEmpty(loggingMock.LoggedMessage));
        }
    }
}
