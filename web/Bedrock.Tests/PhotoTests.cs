﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using NUnit.Framework;
using Spark.CloudStorage;
using Bedrock.Tests.Mocks;

namespace Bedrock.Tests
{
    [TestFixture]
    public class PhotoTests
     {
         private string _imgDirectory = string.Empty;
         public const string CLOUD_URL = "s3.amazonaws.com";

         [TestFixtureSetUp]
        public void StartUp()
         {
             _imgDirectory = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL") + "/";
         }

        [Test]
        public void DisplayFromCloudDisabledReturnsLocalPaths()
        {
            MockMember memberBeingViewed = new MockMember();
            MockMember memberViewing = new MockMember();
            MockSettingsService settingsService = new MockSettingsService();

            settingsService.AddSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD", "false");

            memberBeingViewed.SetAttributeInt("gendermask", ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE);
            memberBeingViewed.SetAttributeBool("DisplayPhotosToGuests", false);
            Photo photo = GeneratePhoto("/Photo1000/2012/01/27/15/120129650.jpg",
                                        "/Photo1000/2012/01/27/15/120196975.jpg", string.Empty, string.Empty, true,
                                        false);
            memberBeingViewed.AddPhoto(photo);
            Brand brand = GetBrand(1003);

            MemberPhotoDisplayManager.Instance.SettingsManager.SettingsService = settingsService;
            string fullImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                           PrivatePhotoImageType.Full);

            string thumbImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Thumbnail,
                                           PrivatePhotoImageType.Full);

            Assert.IsTrue(fullImagePath == photo.FileWebPath);
            Assert.IsTrue(thumbImagePath == photo.ThumbFileWebPath);
        }

        [Test]
        public void DisplayFromCloudEnabledReturnsCloudPaths()
        {
            MockMember memberBeingViewed = new MockMember();
            MockMember memberViewing = new MockMember();
            MockSettingsService settingsService = new MockSettingsService();

            settingsService.AddSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD", "true");
            InitSettingsMockForCloudStorage(settingsService, string.Empty, false);

            memberBeingViewed.SetAttributeInt("gendermask", ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE);
            memberBeingViewed.SetAttributeBool("DisplayPhotosToGuests", false);
            Photo photo = GeneratePhoto(string.Empty,string.Empty, "/2012/01/27/15/120129650.jpg","/2012/01/27/15/120196975.jpg", true,false);
            memberBeingViewed.AddPhoto(photo);
            Brand brand = GetBrand(1003);

            MemberPhotoDisplayManager.Instance.SettingsManager.SettingsService = settingsService;
            string fullImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                           PrivatePhotoImageType.Full);

            string thumbImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Thumbnail,
                                           PrivatePhotoImageType.Full);

            Client cloudClient = new Client(brand.Site.Community.CommunityID, brand.Site.SiteID, settingsService);

            Assert.IsTrue(fullImagePath == cloudClient.GetFullCloudImagePath(photo.FileCloudPath, FileType.MemberPhoto, false));
            Assert.IsTrue(thumbImagePath == cloudClient.GetFullCloudImagePath(photo.ThumbFileCloudPath, FileType.MemberPhoto, false));
        }

        [Test]
        public void WhenPhotoIsPrivateCorrectImageIsReturned()
        {
            MockMember memberBeingViewed = new MockMember();
            MockMember memberViewing = new MockMember();
            MockSettingsService settingsService = new MockSettingsService();

            settingsService.AddSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD", "false");

            memberBeingViewed.SetAttributeInt("gendermask", ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE);
            memberBeingViewed.SetAttributeBool("DisplayPhotosToGuests", true);
            Photo photo = GeneratePhoto("/Photo1000/2012/01/27/15/120129650.jpg",
                                        "/Photo1000/2012/01/27/15/120196975.jpg", string.Empty, string.Empty, true,
                                        true);

            memberBeingViewed.AddPhoto(photo);

            //use Cupid brand because it's the only one that uses private photos
            Brand brand = GetBrand(1015);

            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(settingsService);
            string privateImagePath;

            //test each one of the private photo types
            privateImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.Full);
            Assert.IsTrue(privateImagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_FULL);

            privateImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.BackgroundThumb);
            Assert.IsTrue(privateImagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB);
            
            privateImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.Thumb);
            Assert.IsTrue(privateImagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_THUMB);

            privateImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.TinyThumb);
            Assert.IsTrue(privateImagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_TINY_THUMB);

            //now test the female versions by flipping gender mask
            memberBeingViewed.SetAttributeInt("gendermask", ConstantsTemp.GENDERID_FEMALE + ConstantsTemp.GENDERID_SEEKING_MALE);

            privateImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.Full);
            Assert.IsTrue(privateImagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_FULL_FEMALE);

            privateImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.BackgroundThumb);
            Assert.IsTrue(privateImagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB_FEMALE);

            privateImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.Thumb);
            Assert.IsTrue(privateImagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_THUMB_FEMALE);

            privateImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.TinyThumb);
            Assert.IsTrue(privateImagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_TINY_THUMB_FEMALE);
        }

        [Test]
        public void WhenPhotoIsNullCorrectImageIsReturned()
        {
            MockMember memberBeingViewed = new MockMember();
            MockMember memberViewing = new MockMember();
            MockSettingsService settingsService = new MockSettingsService();

            settingsService.AddSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD", "false");

            memberBeingViewed.SetAttributeInt("gendermask", ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE);
            memberBeingViewed.SetAttributeBool("DisplayPhotosToGuests", true);
            Photo photo = null;
            
            Brand brand = GetBrand(1003);

            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(settingsService);
            string noImagePath;

            //test each one of the No photo types
            noImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.Full, NoPhotoImageType.BackgroundThumb);
            Assert.IsTrue(noImagePath == _imgDirectory + IMAGE_NOPHOTO_BACKGROUND_THUMB);

            noImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.BackgroundThumb, NoPhotoImageType.BackgroundTinyThumb);
            Assert.IsTrue(noImagePath == _imgDirectory + IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB);

            noImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.Thumb, NoPhotoImageType.Matchmail);
            Assert.IsTrue(noImagePath == _imgDirectory + IMAGE_NOPHOTO_MM);

            noImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.TinyThumb, NoPhotoImageType.Thumb);
            Assert.IsTrue(noImagePath == _imgDirectory + IMAGE_NOPHOTO_THUMB);

            noImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.TinyThumb, NoPhotoImageType.ThumbV2);
            Assert.IsTrue(noImagePath == _imgDirectory + IMAGE_NOPHOTO_THUMB_V2);

            noImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.TinyThumb, NoPhotoImageType.TinyThumbV2);
            Assert.IsTrue(noImagePath == _imgDirectory + IMAGE_NOPHOTO_TINY_THUMB_V2);

            //now test the female versions by flipping gender mask
            memberBeingViewed.SetAttributeInt("gendermask", ConstantsTemp.GENDERID_FEMALE + ConstantsTemp.GENDERID_SEEKING_MALE);

            noImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.Full, NoPhotoImageType.BackgroundThumb);
            Assert.IsTrue(noImagePath == _imgDirectory + IMAGE_NOPHOTO_BACKGROUND_THUMB_FEMALE);

            noImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.BackgroundThumb, NoPhotoImageType.BackgroundTinyThumb);
            Assert.IsTrue(noImagePath == _imgDirectory + IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB_FEMALE);

            noImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.Thumb, NoPhotoImageType.Matchmail);
            Assert.IsTrue(noImagePath == _imgDirectory + IMAGE_NOPHOTO_MM_FEMALE);

            noImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.TinyThumb, NoPhotoImageType.Thumb);
            Assert.IsTrue(noImagePath == _imgDirectory + IMAGE_NOPHOTO_THUMB_FEMALE);

            noImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.TinyThumb, NoPhotoImageType.ThumbV2);
            Assert.IsTrue(noImagePath == _imgDirectory + IMAGE_NOPHOTO_THUMB_V2_FEMALE);

            noImagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.TinyThumb, NoPhotoImageType.TinyThumbV2);
            Assert.IsTrue(noImagePath == _imgDirectory + IMAGE_NOPHOTO_TINY_THUMB_V2_FEMALE);
        }

       [Test]
        public void GetPrivatePhotoFileReturnsCorrectPhoto()
        {
            MockMember member = new MockMember();
            MockSettingsService settingsService = new MockSettingsService();

            settingsService.AddSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD", "false");
            member.SetAttributeInt("gendermask", ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE);
            Brand brand = GetBrand(1003);

            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(settingsService);
            string imagePath;

            //test each one of the private photo types
            imagePath = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.BackgroundThumb, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB);
            imagePath = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.Full, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_FULL);
            imagePath = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.Thumb, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_THUMB);
            imagePath = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.TinyThumb, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_TINY_THUMB);
            
            //flip the member gender and test each one of the private photo types again
            member.SetAttributeInt("gendermask", ConstantsTemp.GENDERID_FEMALE + ConstantsTemp.GENDERID_SEEKING_MALE);
            imagePath = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.BackgroundThumb, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB_FEMALE);
            imagePath = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.Full, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_FULL_FEMALE);
            imagePath = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.Thumb, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_THUMB_FEMALE);
            imagePath = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.TinyThumb, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_PRIVATE_PHOTO_TINY_THUMB_FEMALE);
        }

       [Test]
        public void GetNoPhotoFileReturnsCorrectPhoto()
        {
            MockMember member = new MockMember();
            MockSettingsService settingsService = new MockSettingsService();

            settingsService.AddSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD", "false");
            member.SetAttributeInt("gendermask", ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE);
            Brand brand = GetBrand(1003);

            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(settingsService);
            string imagePath;

            //test each one of the no photo types
            imagePath = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_BACKGROUND_THUMB);
            imagePath = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundTinyThumb, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB);
            imagePath = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Matchmail, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_MM);
            imagePath = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Thumb, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_THUMB);
            imagePath = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.ThumbV2, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_THUMB_V2);
            imagePath = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_TINY_THUMB_V2);

            //flip the member gender and test each one of the no photo types again
            member.SetAttributeInt("gendermask", ConstantsTemp.GENDERID_FEMALE + ConstantsTemp.GENDERID_SEEKING_MALE);
            imagePath = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_BACKGROUND_THUMB_FEMALE);
            imagePath = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundTinyThumb, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB_FEMALE);
            imagePath = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Matchmail, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_MM_FEMALE);
            imagePath = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Thumb, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_THUMB_FEMALE);
            imagePath = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.ThumbV2, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_THUMB_V2_FEMALE);
            imagePath = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, member, brand);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_TINY_THUMB_V2_FEMALE);
        }

        [Test]
        public void VistorsGetNoImageFileWhenDisplayPhotosToGuestsIsFalse()
        {
            MockMember memberBeingViewed = new MockMember();
            MockMember memberViewing = null;
            MockSettingsService settingsService = new MockSettingsService();

            settingsService.AddSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD", "false");
            memberBeingViewed.SetAttributeInt("gendermask",ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE);
            
            Photo photo = GeneratePhoto("/Photo1000/2012/01/27/15/120129650.jpg",
                                        "/Photo1000/2012/01/27/15/120196975.jpg", string.Empty, string.Empty, true,
                                        true);
            memberBeingViewed.AddPhoto(photo);

            Brand brand = GetBrand(1003);

            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(settingsService);
            string imagePath;

            memberBeingViewed.SetAttributeBool("DisplayPhotosToGuests", false);
            imagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.Full, NoPhotoImageType.BackgroundThumb);
            Assert.IsTrue(imagePath == _imgDirectory + IMAGE_NOPHOTO_BACKGROUND_THUMB);
        }

        [Test]
        public void VistorsGetPhotoWhenDisplayPrivatePhotosSetToTrue()
        {
            MockMember memberBeingViewed = new MockMember();
            MockMember memberViewing = null;
            MockSettingsService settingsService = new MockSettingsService();

            settingsService.AddSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD", "true");
            InitSettingsMockForCloudStorage(settingsService, string.Empty, false);

            memberBeingViewed.SetAttributeInt("gendermask", ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE);

            Photo photo = GeneratePhoto(string.Empty, string.Empty, "/2012/01/27/15/120129650.jpg", "/2012/01/27/15/120196975.jpg", true, false);
            memberBeingViewed.AddPhoto(photo);

            Brand brand = GetBrand(1003);

            MemberPhotoDisplayManager.Instance.SettingsManager.SettingsService = settingsService;
            string imagePath;

            memberBeingViewed.SetAttributeBool("DisplayPhotosToGuests", false);

            Client cloudClient = new Client(brand.Site.Community.CommunityID, brand.Site.SiteID, settingsService);
            imagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.Full, NoPhotoImageType.BackgroundThumb, false, true);
            Assert.IsTrue(imagePath == cloudClient.GetFullCloudImagePath(photo.FileCloudPath, FileType.MemberPhoto, false));
        }

        [Test]
        public void UseFullURLParameterReturnsFullURL()
        {
            MockMember memberBeingViewed = new MockMember();
            MockMember memberViewing = null;
            MockSettingsService settingsService = new MockSettingsService();

            settingsService.AddSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD", "false");
            memberBeingViewed.SetAttributeBool("DisplayPhotosToGuests", true);
            memberBeingViewed.SetAttributeInt("gendermask", ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE);

            Photo photo = GeneratePhoto("/Photo1000/2012/01/27/15/120129650.jpg", "/Photo1000/2012/01/27/15/120196975.jpg", string.Empty, string.Empty, true,true);
            memberBeingViewed.AddPhoto(photo);
            Brand brand = GetBrand(1003);

            MemberPhotoDisplayManager.Instance.SettingsManager.SettingsService = settingsService;
            string imagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, PhotoType.Full,
                                        PrivatePhotoImageType.Full, NoPhotoImageType.BackgroundThumb, true);

            Assert.IsTrue(imagePath == "http://" + brand.Site.DefaultHost + "." + brand.Uri + photo.FileWebPath); 
        }

        [Test]
        public void GetDefaultPhotoReturnsAppropriatePhoto()
        {
            MockMember member = new MockMember();
            Photo photo = GeneratePhoto("/Photo1000/2012/01/27/15/120129650.jpg", "/Photo1000/2012/01/27/15/120196975.jpg", string.Empty, string.Empty, false, false);
            member.AddPhoto(photo);
            Brand brand = GetBrand(1003);

            MockSettingsService settingsService = new MockSettingsService();
            MemberPhotoDisplayManager.Instance.SettingsManager.SettingsService = settingsService;

            //since the only photo is not approved this should return null
            Photo test1 = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(member, member, brand);
            Assert.IsTrue(test1 == null);

            //this time photo is approved, private, approved for main, and is not main, so should be returned as the default
            member = new MockMember();
            photo = GeneratePhoto(2, "/Photo1000/2012/01/27/15/120129650.jpg", "/Photo1000/2012/01/27/15/120196975.jpg", string.Empty, string.Empty, true, true, true, false);
            member.AddPhoto(photo);
            Photo test2 = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(member, member, brand);
            Assert.IsTrue(test2 == photo);
        }

        [Test]
        public void GetApprovedPhotosTest()
        {
            // GetApprovedPhotos without the photoType parameter is a pass-through method for now, so only test
            // the version with photoType
            var mockMember = new MockMember();
            var photo = GeneratePhoto(1, "file_web_path", "thumb_file_web_path", "file_cloud_path", "thumb_file_cloud_path",
                                      true, false, false, false);
            mockMember.AddPhoto(photo);
            // this is the bad photo.  this shouldn't come back at the end of the test
            photo = GeneratePhoto(2, string.Empty, string.Empty, string.Empty, string.Empty, true, false, false, false);
            mockMember.AddPhoto(photo);

            var setting = new MockSettingsService();
            setting.AddSetting(SettingConstants.ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD, "true");

            Brand brand = GetBrand(1003);

            MemberPhotoDisplayManager.Instance.SettingsManager.SettingsService = setting;

            var photos = MemberPhotoDisplayManager.Instance.GetApprovedPhotos(mockMember, mockMember, brand, PhotoType.Full);

            Assert.IsTrue(photos != null && photos.Count == 1);
        }

        [Test]
        public void GetApprovedForMainPhotosTest()
        {
            // GetApprovedForMainPhotosTest without the photoType parameter is a pass-through method for now, so only test
            // the version with photoType
            var mockMember = new MockMember();
            var photo = GeneratePhoto(1, "file_web_path", "thumb_file_web_path", "file_cloud_path", "thumb_file_cloud_path",
                                      true, false, true, true);
            mockMember.AddPhoto(photo);
            // this is the bad photo, so this shouldn't come back at the end of the test
            photo = GeneratePhoto(2, string.Empty, string.Empty, string.Empty, string.Empty, true, false, true, false);
            mockMember.AddPhoto(photo);
            // valid photo but no approved for main, so this photo shouldn't come back
            photo = GeneratePhoto(3, "file_web_path", "thumb_file_web_path", "file_cloud_path", "thumb_file_cloud_path",
                                      true, false, false, false);
            mockMember.AddPhoto(photo);

            var setting = new MockSettingsService();
            setting.AddSetting(SettingConstants.ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD, "true");

            Brand brand = GetBrand(1003);

            MemberPhotoDisplayManager.Instance.SettingsManager.SettingsService = setting;

            var photos = MemberPhotoDisplayManager.Instance.GetApprovedForMainPhotos(mockMember, mockMember, brand, PhotoType.Full);

            Assert.IsTrue(photos != null && photos.Count == 1);
        }
        
        private Photo GeneratePhoto(string fileWebPath, string thumbFileWebPath, string fileCloudPath, string thumbFileCloudPath, bool isApproved, bool isPrivate)
        {
            return new Photo(Matchnet.Constants.NULL_INT, Matchnet.Constants.NULL_INT, fileWebPath,
                Matchnet.Constants.NULL_INT, thumbFileWebPath, 1, isApproved, isPrivate, 0, 0, string.Empty, false, fileCloudPath, thumbFileCloudPath);
        }

        private Photo GeneratePhoto(int memberPhotoId, string fileWebPath, string thumbFileWebPath, string fileCloudPath, string thumbFileCloudPath, bool isApproved, bool isPrivate, bool isApprovedForMain, bool isMain)
        {
            return new Photo(memberPhotoId, Matchnet.Constants.NULL_INT, fileWebPath,
                Matchnet.Constants.NULL_INT, thumbFileWebPath, 1, isApproved, isPrivate, 0, 0, string.Empty, false, fileCloudPath, thumbFileCloudPath, isApprovedForMain, isMain);
        }

        private Brand GetBrand(int brandID)
        {
            return BrandConfigSA.Instance.GetBrandByID(brandID);
        }

        private void InitSettingsMockForCloudStorage(MockSettingsService settingsMock, string bucketName, bool includeBucketName)
        {
            settingsMock.AddSetting("AWS_ACCESSKEY", "AKIAJLZ5E2GDEO72NACA");
            settingsMock.AddSetting("AWS_SECRETKEY", "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");
            settingsMock.AddSetting("CLOUD_URL", CLOUD_URL);
            settingsMock.AddSetting("AWS_MEMBER_PHOTO_BUCKET_NAME", bucketName);
            settingsMock.AddSetting("CLOUD_URL_INCLUDE_BUCKET_NAME", includeBucketName.ToString());
        }

        #region Image Constants
        //composite of background and text, large size for use in member profile
        private const string IMAGE_PRIVATE_PHOTO_FULL = "privatephoto200x250_ynm4.gif";
        private const string IMAGE_PRIVATE_PHOTO_FULL_FEMALE = "privatephoto200x250_ynm4.gif";

        //background only, does not contain text, for use as thumbnail in searches and profiles (text appears on top).
        private const string IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB = "privatephoto80x104_ynm4.gif";
        private const string IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB_FEMALE = "privatephoto80x104_ynm4.gif";

        //composite of background and text, medium size and needs to be a non-interlaced jpg/jpeg for use in Userplane flash movie (and emails)
        private const string IMAGE_PRIVATE_PHOTO_THUMB = "privatephoto80x104_ynm4.jpg";
        private const string IMAGE_PRIVATE_PHOTO_THUMB_FEMALE = "privatephoto80x104_ynm4.jpg";

        //composite of background and text, small size for use in IM notifier
        private const string IMAGE_PRIVATE_PHOTO_TINY_THUMB = "privatephoto43x56_ym4.gif";
        private const string IMAGE_PRIVATE_PHOTO_TINY_THUMB_FEMALE = "privatephoto43x56_ym4.gif";

        //background only, does not contain text, for use as thumbnail in searches and profiles (text appears on top).
        private const string IMAGE_NOPHOTO_BACKGROUND_THUMB = "no-photo-m-m.png"; //"noPhoto.gif"; //current 80x103
        private const string IMAGE_NOPHOTO_BACKGROUND_THUMB_FEMALE = "no-photo-m-f.png"; //"noPhoto.gif";

        //composite of background and text, medium size and needs to be a non-interlaced jpg/jpeg for use in Userplane flash movie (and emails)
        private const string IMAGE_NOPHOTO_THUMB = "no-photo-m-m.png";//"noPhoto.jpg"; //current 80x103
        private const string IMAGE_NOPHOTO_THUMB_FEMALE = "no-photo-m-f.png";//"noPhoto.jpg";

        //background only, does not contain text, small size for use in IM notifier
        private const string IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB = "no-photo-s-m.png";//"noPhoto_small.gif"; //current 43x56
        private const string IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB_FEMALE = "no-photo-s-f.png";//"noPhoto_small.gif";

        //for use as thumbnail (new version 2.0)
        private const string IMAGE_NOPHOTO_THUMB_V2 = "no-photo-s-m.png"; //"no-photo-sm.gif"; //current 43x56
        private const string IMAGE_NOPHOTO_THUMB_V2_FEMALE = "no-photo-s-f.png";//"no-photo-sm.gif";

        //for use as tiny thumbnail (new version 2.0)
        private const string IMAGE_NOPHOTO_TINY_THUMB_V2 = "no-photo-s-m.png";//"no-photo-sm-54x70.gif";
        private const string IMAGE_NOPHOTO_TINY_THUMB_V2_FEMALE = "no-photo-s-f.png";//"no-photo-sm-54x70.gif";

        //Used by MMImageDisplay.aspx (email related), must stay as jpg
        private const string IMAGE_NOPHOTO_MM = "no-photo-l-m.jpg"; //nophotoLargeMM.jpg 246x328
        private const string IMAGE_NOPHOTO_MM_FEMALE = "no-photo-l-f.jpg"; //nophotoLargeMM.jpg
        #endregion
    }
}
