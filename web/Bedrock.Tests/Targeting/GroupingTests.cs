﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using NUnit.Framework;
using Matchnet.Web.Framework.Targeting;

namespace Bedrock.Tests.Targeting
{
    [TestFixture]
    public class GroupingTests: AbstractBaseUnitTest
    {
        [Test]
        public void Test1()
        {
            Brand jDateBrand = GetJdateBrand();
            MockMember member = new MockMember();
            member.SetAttributeDate("birthdate", DateTime.Parse("05/09/75"));
            member.SetAttributeInt("maritalstatus", 2);
            member.SetAttributeText("EmailAddress", "lokojsadf@spark.net");
            member.SetAttributeInt("GenderMask", 5);

            GenderCriteria first = new GenderCriteria(member, jDateBrand, GenderCriteriaOperator.Male);
            SeekingGenderCriteria second = new SeekingGenderCriteria(member, jDateBrand, GenderCriteriaOperator.Male);
            IntAttributeCriteria third = new IntAttributeCriteria(member, jDateBrand, "maritalstatus", IntAttributeCriteriaOperator.Equals, 3);
            DateAttributeEqualityCriteria fourth = new DateAttributeEqualityCriteria(member, jDateBrand, "birthdate", DateTime.Parse("05/09/75"));
            TextAttributeCriteria fifth = new TextAttributeCriteria(member, jDateBrand, "EmailAddress", TextAttributeCriteriaOperator.Equals, "lokojsadf@spark.net");

            CriteriaEvaluationGroup group1 = new CriteriaEvaluationGroup();

            group1.AddEvaluationUnit(new CriteriaEvaluationElement(first), BooleanEvaluationOperator.None);
            group1.AddEvaluationUnit(new CriteriaEvaluationElement(second), BooleanEvaluationOperator.And);
            group1.AddEvaluationUnit(new CriteriaEvaluationElement(third), BooleanEvaluationOperator.And);

            CriteriaEvaluationGroup group2 = new CriteriaEvaluationGroup();
            group2.AddEvaluationUnit(new CriteriaEvaluationElement(fourth), BooleanEvaluationOperator.None);
            group2.AddEvaluationUnit(new CriteriaEvaluationElement(fifth), BooleanEvaluationOperator.And);

            CriteriaEvaluationGroup outerGroup = new CriteriaEvaluationGroup();
            outerGroup.AddEvaluationUnit(group1, BooleanEvaluationOperator.None);
            outerGroup.AddEvaluationUnit(group2, BooleanEvaluationOperator.Or);
            
            CriteriaEvaluationGroup outerGroup2 = new CriteriaEvaluationGroup();
            outerGroup2.AddEvaluationUnit(group1, BooleanEvaluationOperator.None);
            outerGroup2.AddEvaluationUnit(group2, BooleanEvaluationOperator.And);

            CriteriaEvaluationGroup outerGroup3 = new CriteriaEvaluationGroup();
            outerGroup3.AddEvaluationUnit(group1, BooleanEvaluationOperator.None);
            outerGroup3.AddEvaluationUnit(new CriteriaEvaluationElement(fifth), BooleanEvaluationOperator.Or);

            bool passes = outerGroup.PassesEvaluationCriteria();
            bool passes2 = outerGroup2.PassesEvaluationCriteria();
            bool passes3 = outerGroup3.PassesEvaluationCriteria();

            Assert.IsTrue(1==1);

        }
    }
}
