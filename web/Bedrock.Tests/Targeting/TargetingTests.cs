﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Targeting;
using NUnit.Framework;
using Bedrock.Tests.Mocks;

namespace Bedrock.Tests
{
    [TestFixture]
    public class TargetingTests: AbstractBaseUnitTest
    {
        /*[Test]
        public void DateAttributeEqualsWorks()
        {
            MockMember member = new MockMember();
            member.SetAttributeDate("birthdate", DateTime.Parse("05/09/75"));
            Brand brand = GetJdateBrand();

            DateAttributeCriteria dateCriteria = new DateAttributeCriteria(member, brand, "birthdate", CriteriaOperator.Equals, DateTime.Parse("05/09/75"));
            Assert.Is dateCriteria.PassesCriteria()
        }*/

        [Test]
        public void TestLoader()
        {
            Brand jDateBrand = GetJdateBrand();
            MockMember member = new MockMember();
            member.SetAttributeDate("birthdate", DateTime.Parse("05/09/75"));

            MockCurrentServer currentServer = new MockCurrentServer();
            //currentServer.SetMappedPath(@"C:\matchnet\podm2m\web\bedrock.matchnet.com\Framework\Targeting\XML\SecretAdmirerOverlay_103.xml");
            currentServer.SetMappedPath(ContentPath + "Targeting\\Test_103.xml");


            XMLTargetingCriteriaLoader loader = new XMLTargetingCriteriaLoader();
            loader.CurrentServer = currentServer;

            CriteriaEvaluationUnit criteria = loader.GetCriteria("Test", member, jDateBrand);
            Assert.IsTrue(1==1);
        }
    }
}
