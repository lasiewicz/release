﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bedrock.Tests.Mocks;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Targeting;
using NUnit.Framework;

namespace Bedrock.Tests.Targeting
{
    [TestFixture]
    public class XMLLoaderTests: AbstractBaseUnitTest
    {
        [Test]
        public void TestLoader()
        {
            Brand jDateBrand = GetJdateBrand();
            MockMember member = new MockMember();
            member.SetAttributeDate("birthdate", DateTime.Parse("05/09/75"));
            member.SetAttributeInt("maritalstatus", 2);
            member.SetAttributeText("EmailAddress", "lokojsadf@spark.net");

            MockCurrentServer currentServer = new MockCurrentServer();
            currentServer.SetMappedPath(ContentPath + @"Targeting\Test_103.xml");
            
            XMLTargetingCriteriaLoader loader = new XMLTargetingCriteriaLoader();
            loader.CurrentServer = currentServer;

            CriteriaEvaluationUnit criteria = loader.GetCriteria("Test", member, jDateBrand);
            Assert.IsTrue(1 == 1);
        }

        [Test]
        public void TestSecretAdmirer()
        {
            Brand jDateBrand = GetJdateBrand();
            MockMember member = new MockMember();
            member.SetAttributeInt("GenderMask", 5);
            member.MemberID = 111;

            MockSettingsService.AddSetting("SECRET_ADMIRER_MODAL_ENABLED", "true");
            MockSettingsService.AddSetting(SettingConstants.IS_DEVELOPMENT_MODE, "false");
            

            MockCurrentServer currentServer = new MockCurrentServer();
            currentServer.SetMappedPath(ContentPath + @"Targeting\SecretAdmirer_103.xml");

            XMLTargetingCriteriaLoader loader = new XMLTargetingCriteriaLoader();
            loader.CurrentServer = currentServer;
            loader.SettingsManager.SettingsService = MockSettingsService;
            loader.SessionManager.SessionService = new MockSessionService();
            loader.SessionManager.CurrentRequest = new CurrentRequestMock();
            
            CriteriaEvaluationUnit criteria = loader.GetCriteria("SecretAdmirer", member, jDateBrand);

            Assert.IsTrue(criteria.PassesEvaluationCriteria());

            MockSettingsService.AddSetting("SECRET_ADMIRER_MODAL_ENABLED", "false");
            Assert.IsTrue(!criteria.PassesEvaluationCriteria());

            MockSettingsService.AddSetting("SECRET_ADMIRER_MODAL_ENABLED", "true");
            member.MemberID = 112;
            Assert.IsTrue(!criteria.PassesEvaluationCriteria());

            member.MemberID = 111;
            member.SetAttributeInt("GenderMask", 6);
            Assert.IsTrue(!criteria.PassesEvaluationCriteria());

            member.SetAttributeInt("GenderMask", 5);
            UserSession userSession = new UserSession(new Guid());

            userSession.Add("SecretAdmirerModalShowCount", "1", SessionPropertyLifetime.Temporary);
            (loader.SessionManager.SessionService as MockSessionService).SetUserSession(userSession);
            Assert.IsTrue(!criteria.PassesEvaluationCriteria());

            userSession.Add("SecretAdmirerModalShowCount", "0", SessionPropertyLifetime.Temporary);
            Assert.IsTrue(criteria.PassesEvaluationCriteria());
        }
    }
}
