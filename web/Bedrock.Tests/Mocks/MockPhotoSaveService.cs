﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Photos;

namespace Bedrock.Tests
{
    public class MockPhotoSaveService : ISavePhotos
    {
        private bool _result = true;
        public PhotoUpdate[] PhotoUpdates { get; private set; }

        #region ISavePhotos Members

        public bool SavePhotos(int brandID, int siteID, int communityID, int memberID, PhotoUpdate[] photoUpdates)
        {
            PhotoUpdates = photoUpdates;
            return _result;
        }
        
        #endregion

        public void SetResult(bool result)
        {
            _result = result;
        }
    }
}
