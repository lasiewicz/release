﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;

namespace Bedrock.Tests
{
    public class MockMember: IMember, IMemberDTO
    {
        private Dictionary<string, DateTime> _dateDictionary;
        private Dictionary<string, Int32> _intDictionary;
        private Dictionary<string, bool> _boolDictionary;
        private Dictionary<string, string> _stringDictionary;
        private PhotoCommunity _photoCommunity;
        private string _userName = string.Empty;
        private bool _isPayingMember = false;
        
        public void SetIsPayingMember(bool paying)
        {
            _isPayingMember = paying;
        }

        public MockMember()
        {
            _dateDictionary = new Dictionary<string, DateTime>();
            _intDictionary = new Dictionary<string, int>();
            _boolDictionary = new Dictionary<string, bool>();
            _stringDictionary = new Dictionary<string, string>();
            _photoCommunity = new PhotoCommunity();
        }

        public bool IsPayingMember(Int32 siteID)
        {
            return _isPayingMember;
        }

        public int MemberID { get; set; }
        public MemberUpdate MemberUpdate { get; private set; }
        public void CommitChanges()
        {
            
        }

        public string GetUserName(Brand brand)
        {
            return _userName;
        }

        public void SetUserName(string userName)
        {
            _userName = userName;
        }

        public void SetAttributeInt(string attributeName, Int32 value)
        {
            if (_intDictionary.ContainsKey(attributeName.ToLower()))
            {
                _intDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _intDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void SetAttributeInt(Brand brand, string attributeName, Int32 value)
        {
            if (_intDictionary.ContainsKey(attributeName.ToLower()))
            {
                _intDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _intDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void SetAttributeDate(string attributeName, DateTime value)
        {
            if (_dateDictionary.ContainsKey(attributeName.ToLower()))
            {
                _dateDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _dateDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void SetAttributeDate(Brand brand, string attributeName, DateTime value)
        {
            if (_dateDictionary.ContainsKey(attributeName.ToLower()))
            {
                _dateDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _dateDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void SetAttributeBool(string attributeName, bool value)
        {
            if (_boolDictionary.ContainsKey(attributeName.ToLower()))
            {
                _boolDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _boolDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void SetAttributeText(string attributeName, string value)
        {

            if (_stringDictionary.ContainsKey(attributeName.ToLower()))
            {
                _stringDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _stringDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void SetAttributeText(Brand brand, string attributeName, string value, TextStatusType textStatusType)
        {

            if (_stringDictionary.ContainsKey(attributeName.ToLower()))
            {
                _stringDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _stringDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void AddPhoto(Photo photo)
        {
            _photoCommunity[(byte) _photoCommunity.Count] = photo;
        }

        public DateTime GetAttributeDate(Brand brand, string attributeName)
        {
            if (_dateDictionary.ContainsKey(attributeName.ToLower()))
            {
                return _dateDictionary[attributeName.ToLower()];
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        public DateTime GetAttributeDate(Brand brand, string attributeName, DateTime defaultValue)
        {
            if (_dateDictionary.ContainsKey(attributeName.ToLower()))
            {
                return _dateDictionary[attributeName.ToLower()];
            }
            else
            {
                return defaultValue;
            }
        }

        public Int32 GetAttributeInt(Brand brand, string attributeName)
        {
            if (_intDictionary.ContainsKey(attributeName.ToLower()))
            {
                return _intDictionary[attributeName.ToLower()];
            }
            else
            {
                return Matchnet.Constants.NULL_INT;
            }
        }

        public Int32 GetAttributeInt(Brand brand, string attributeName, Int32 defaultValue)
        {
            if (_intDictionary.ContainsKey(attributeName.ToLower()))
            {
                return _intDictionary[attributeName.ToLower()];
            }
            else
            {
                return defaultValue;
            }
        }

        public bool GetAttributeBool(Brand brand, string attributeName)
        {
            if (_boolDictionary.ContainsKey(attributeName.ToLower()))
            {
                return _boolDictionary[attributeName.ToLower()];
            }
            else
            {
                return false;
            }
        }

        public string GetAttributeText(Brand brand, string attributeName)
        {
            if (_stringDictionary.ContainsKey(attributeName.ToLower()))
            {
                return _stringDictionary[attributeName.ToLower()];
            }
            else
            {
                return string.Empty;
            }
        }

        public PhotoCommunity GetPhotos(Int32 communityID)
        {
            return _photoCommunity;
        }

        public List<Photo> GetApprovedForMainPhotos(int communityId, bool publicOnly)
        {
            var photos = GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var approvedPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if (publicOnly)
                {
                    if (photos[i].IsApproved && photos[i].IsApprovedForMain && !photos[i].IsPrivate)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
                else
                {
                    if (photos[i].IsApproved && photos[i].IsApprovedForMain)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
            }

            approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return approvedPhotos;
        }

        public List<Photo> GetApprovedPhotos(int communityId, bool publicOnly)
        {
            var photos = GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var approvedPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if (publicOnly)
                {
                    if (photos[i].IsApproved && !photos[i].IsPrivate)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
                else
                {
                    if (photos[i].IsApproved)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
            }

            approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return approvedPhotos;
        }

        public List<Photo> GetApprovedPhotosExcludingMain(int communityId, bool publicOnly)
        {
            var photos = GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var approvedPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if (!photos[i].IsApproved || photos[i].IsMain) continue;

                if (publicOnly)
                {
                    if (!photos[i].IsPrivate)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
                else
                {
                    approvedPhotos.Add(photos[i]);
                }
            }

            approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return approvedPhotos;
        }

        public Photo GetDefaultPhoto(int communityID, bool publicOnly)
        {
            Photo retVal = null;

            var photos = GetApprovedForMainPhotos(communityID, publicOnly);
            if (photos == null)
                return null;

            // photo priority in this order; photo marked as IsMain, 1st photo approved for main and public, 1st photo approved for main but private
            var query = from p in photos
                        where p.IsMain
                        orderby p.ListOrder
                        select p;

            retVal = query.FirstOrDefault();
            if (retVal != null)
                return retVal;

            // no photo was marked as main explicitly, look for another candidate
            foreach (Photo photo in photos)
            {
                if (photo.IsPrivate)
                {
                    retVal = photo;
                }
                else
                {
                    retVal = photo;
                    break;
                }
            }

            return retVal;
        }

        public Photo GetRandomApprovedForMainPhoto(int communityId, bool publicOnly)
        {
            var photos = GetApprovedForMainPhotos(communityId, publicOnly);
            if (photos == null || photos.Count == 0)
                return null;

            var random = new Random();
            return photos[random.Next(0, photos.Count - 1)];
        }

        #region IMember Members


        public Matchnet.Member.ValueObjects.Interfaces.IMemberDTO GetIMemberDTO()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IMemberDTO Members


        public string EmailAddress
        {
            get { throw new NotImplementedException(); }
        }

        public DateTime GetAttributeDate(int communityID, int siteID, int brandID, string attributeName, DateTime defaultValue)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeText(int communityID, int siteID, int brandID, int languageID, string attributeName, string defaultValue, out TextStatusType textStatus)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeText(int communityID, int siteID, int brandID, int languageID, string attributeName, string defaultValue)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeText(int communityID, int siteID, int brandID, int languageID, int attributeID, string defaultValue)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeText(int communityID, int siteID, int brandID, int languageID, string attributeName)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeTextApproved(Brand brand, int languageID, string attributeName, string defaultValue, string unapprovedResource)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeTextApproved(Brand brand, string[] attributeNames, int viewerMemberID, string unapprovedResourceValue)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeTextApproved(Brand brand, string attributeName, int viewerMemberID, string unapprovedResourceValue)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeTextApproved(Brand brand, string attributeName, string unapprovedResource)
        {
            throw new NotImplementedException();
        }

        public DateTime GetLastLogonDate(int communityID, out int brandID)
        {
            throw new NotImplementedException();
        }

        public DateTime GetLastLogonDate(int communityID)
        {
            throw new NotImplementedException();
        }

        public Spark.Common.AccessService.AccessPrivilege GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType privilegeType, int brandID, int siteID, int communityID)
        {
            throw new NotImplementedException();
        }

        public bool HasApprovedPhoto(int communityID)
        {
            throw new NotImplementedException();
        }

        public bool IsNewMember(int communityID, int days)
        {
            throw new NotImplementedException();
        }

        public bool IsNewMember(int communityID)
        {
            throw new NotImplementedException();
        }

        public bool IsUpdatedMember(int communityID)
        {
            throw new NotImplementedException();
        }
       
        public bool GetAttributeBool(int communityID, int siteID, int brandID, string attributeName)
        {
            throw new NotImplementedException();
        }

        public int GetAttributeInt(int communityID, int siteID, int brandID, string attributeName, int defaultValue)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeText(Brand brand, string attributeName, string defaultValue)
        {
            return GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID,
                                    brand.Site.LanguageID, attributeName, defaultValue);
        }

        public bool IsSiteMember(int siteID)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Member.ValueObjects.MemberDTO.MemberType MemberType
        {
            get { throw new NotImplementedException(); }
        }

        public string GetAttributeText(Brand brand, string attributeName, out TextStatusType textStatus)
        {
            throw new NotImplementedException();
        }

        public int[] GetCommunityIDList()
        {
            throw new NotImplementedException();
        }

        public int[] GetSiteIDList()
        {
            throw new NotImplementedException();
        }

        public bool IsCommunityMember(int communityID)
        {
            throw new NotImplementedException();
        }
        
        #endregion

        #region ICacheable Members

        public Matchnet.CacheItemMode CacheMode
        {
            get { throw new NotImplementedException(); }
        }

        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int CacheTTLSeconds
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string GetCacheKey()
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
