﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects.ServiceDefinitions;
using Matchnet.Search.ValueObjects;

namespace Bedrock.Tests.Mocks
{
    public class MockSearchPreferencesService : ISearchPreferencesSA
    {
        #region ISearchPreferencesSA Members

        private Hashtable _prefs = new Hashtable();

        public ISearchPreferences SavedSearchPrefs { get; set; }

        public void AddPreference(string key, string value)
        {
            _prefs.Add(key, value);
        }
        
        public SearchPreferenceCollection GetSearchPreferences(int pMemberID, int pCommunityID, bool ignoreSACache)
        {
            SearchPreferenceCollection prefs = new SearchPreferenceCollection();
            foreach(string key in _prefs.Keys)
            {
                prefs.Add(key, _prefs[key].ToString());
            }
            return prefs;

        }

        public SearchPreferenceCollection GetSearchPreferences(int pMemberID, int pCommunityID)
        {
            SearchPreferenceCollection prefs = new SearchPreferenceCollection();
            foreach (string key in _prefs.Keys)
            {
                prefs.Add(key, _prefs[key].ToString());
            }
            return prefs;
        }

        public void Save(int pMemberID, int pCommunityID, Matchnet.Search.Interfaces.ISearchPreferences pSearchPreferences)
        {
            SavedSearchPrefs = pSearchPreferences;
        }

        #endregion
    }
}
