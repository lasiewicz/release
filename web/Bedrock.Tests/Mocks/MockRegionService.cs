﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Content.ValueObjects.ServiceDefinitions;

namespace Bedrock.Tests.Mocks
{
    public class MockRegionService: IRegionSA
    {
        private RegionLanguage _regionLanguage = null;
        
        public void SetRegionLanguage(RegionLanguage regionLanguage)
        {
            _regionLanguage = regionLanguage;
        }
        
        #region IRegionSA Members

        public Matchnet.Content.ValueObjects.Region.RegionID FindRegionIdByCity(int parentRegionID, string description, int languageID)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionID FindRegionIdByPostalCode(int countryRegionID, string postalCode)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.LanguageCollection GetLanguages()
        {
            throw new NotImplementedException();
        }

        public bool IsValidAreaCode(int areaCode, int countryRegionID)
        {
            throw new NotImplementedException();
        }

        public string IsValidAreaCodes(int[] areaCodes, int countryRegionID)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.AreaCodeDictionary RetrieveAreaCodes()
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionAreaCodeDictionary RetrieveAreaCodesByRegion()
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionCollection RetrieveBirthCountries(int languageID)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, int translationID)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, string description)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionCollection RetrieveChildRegions(int parentRegionID, int languageID)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, int translationID, bool forceLoad)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, string description, int translationID)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionCollection RetrieveCitiesByPostalCode(string strPostalCode)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionCollection RetrieveCountries()
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionCollection RetrieveCountries(int languageID)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionLanguage RetrievePopulatedHierarchy(int regionID, int languageID, int maxDepth)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionLanguage RetrievePopulatedHierarchy(int regionID, int languageID)
        {
            return _regionLanguage;
        }

        public Matchnet.Content.ValueObjects.Region.Region RetrieveRegionByID(int regionID, int languageID)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.SchoolRegionCollection RetrieveSchoolListByStateRegion(int stateRegionID)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.RegionSchoolName RetrieveSchoolName(int schoolRegionID)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Content.ValueObjects.Region.SchoolRegionCollection RetrieveSchoolParents(int schoolRegionID)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
