﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.MemberSlideshow.ValueObjects.ServiceDefinitions;
using Matchnet.Search.ValueObjects;

namespace Bedrock.Tests.Mocks
{
    public class MockMemberSlideshowService : IMemberSlideshowSA
    {
        private Slideshow _slideshow ;
        private IMember _validMember = null;

        public int SetMemberID { get; set; }
        public Brand SetBrand { get; set; }
        public SearchPreferenceCollection SetSearchPrefs { get; set; }
        public bool SetPrioritizeExpiringMembers { get; set; }


        public void InitializeSlideshow(int memberID, int communityID, List<int> MemberIDs)
        {
            List<SlideshowEntry> entries = memberIDListToSlideshowEntryList(MemberIDs);
            _slideshow = new Slideshow(memberID, communityID, entries);
        }

        public void IntializeValidMember(IMember member)
        {
            _validMember = member;
        }

        public bool RemoveFromCacheCalled { get; set; }
        public int NewSlideshowPosition { get; set; }
        
        #region IMemberSlideshowSA Members

        public Slideshow GetSlideshow(int memberID, Brand brand, SearchPreferenceCollection preferences, bool prioritizeExpiringMembers)
        {
            SetMemberID = memberID;
            SetBrand = brand;
            SetSearchPrefs = preferences;
            SetPrioritizeExpiringMembers = prioritizeExpiringMembers;
            
            if(_slideshow == null) throw new Exception("slideshow not initialized");
            return _slideshow;
        }

        public Slideshow GetSlideshow(int memberID, Brand brand)
        {
            SetMemberID = memberID;
            SetBrand = brand;
            
            if (_slideshow == null) throw new Exception("slideshow not initialized");
            return _slideshow;
        }

        public IMember GetValidMemberFromSlideshow(Matchnet.MemberSlideshow.ValueObjects.Slideshow slideshow, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, bool updatePosition)
        {
            return _validMember;
        }

        public void RemoveFromCache(int memberID, int communityID)
        {
            RemoveFromCacheCalled = true;
        }

        public void UpdateSildeshowPosition(Slideshow slideshow, int newPosition)
        {
            NewSlideshowPosition = newPosition;
        }

        #endregion

        private List<SlideshowEntry> memberIDListToSlideshowEntryList(List<int> memberIDList)
        {
            List<SlideshowEntry> entries = new List<SlideshowEntry>();
            foreach (int i in memberIDList)
            {
                entries.Add(new SlideshowEntry(i));
            }

            return entries;
        }
    }
}
