﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Web.Interfaces;

namespace Bedrock.Tests.Mocks
{
    public class MockCurrentServer: ICurrentServer
    {
        private string _MappedPath = string.Empty;
        private string _RequestedMapPath = string.Empty;

        public void SetMappedPath(string mappedPath)
        {
            _MappedPath = mappedPath;
        }
        
        public string GetLastMappedPathRequest()
        {
            return _RequestedMapPath;
        }

        #region ICurrentServer Members
        
        public string MapPath(string path)
        {
            _RequestedMapPath = path;
            return _MappedPath;
        }

        #endregion
    }
}
