﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects;

namespace Bedrock.Tests
{
    public class MockMemberSaveService: ISaveMember 
    {
        public IMember Member { get; private set; }
        private MemberSaveResult _result;
        
        #region ISaveMember Members

        public MockMemberSaveService()
        {
            _result = new MemberSaveResult(MemberSaveStatusType.Success, string.Empty);
        }

        public MemberSaveResult SaveMember(IMember member, int communityID)
        {
            Member = member;
            return _result;
        }

        public string ResetPassword(int memberId)
        {
            throw new NotImplementedException();
        }

        public string GenerateImpersonateToken(int adminMemberId, int impersonateMemberId, int impersonateBrandId)
        {
            throw new NotImplementedException();
        }

        public void ExpireImpersonationToken(int adminMemberId)
        {
            throw new NotImplementedException();
        }

        public MemberSaveResult SaveMember(IMember member)
        {
            Member = member;
            return _result;
        }

        #endregion

        public void SetResult(MemberSaveStatusType status)
        {
            _result = new MemberSaveResult(status, string.Empty);
        }
    }
}
