﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Web.Interfaces;

namespace Bedrock.Tests.Mocks
{
    public class MockLocalizer: ILocalizer
    {
        #region ILocalizer Members

        public string GetFormattedStringResource(string resourceName, object caller, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, System.Collections.Specialized.StringDictionary tokenMap, string[] args, bool showResourceHintsBySetting)
        {
            return string.Empty;
        }

        public string GetStringResource(string key, object caller, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            return string.Empty;
        }

        public string GetStringResource(string key, object caller, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, System.Collections.Specialized.StringDictionary tokenMap)
        {
            return string.Empty;
        }

        public string GetStringResourceWithNoTokeMap(string key, object caller, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            return string.Empty;
        }

        public string GetFormattedStringResource(string resourceName, object caller, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, System.Collections.Specialized.StringDictionary tokenMap, string[] args)
        {
            return string.Empty;
        }

        public string ConvertTypeToResourcePath(Type type)
        {
            return string.Empty;
        }

        public string ExpandTokens(string source, System.Collections.Specialized.StringDictionary tokenMap)
        {
            return string.Empty;
        }

        public string ExpandImageTokens(string source, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            return string.Empty;
        }

        #endregion
    }
}
