﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Web.Interfaces;

namespace Bedrock.Tests.Mocks
{
    public class MockSlideshowPersistence : ISlideshowPersistence
    {
        #region ISlideshowPersistence Members

        public bool Saved { get; set; }

        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int RegionID { get; set; }
        public Spark.SAL.SearchType SearchType { get; set; }
        public int AreaCode1 { get; set; }
        public int AreaCode2 { get; set; }
        public int AreaCode3 { get; set; }
        public int AreaCode4 { get; set; }
        public int AreaCode5 { get; set; }
        public int AreaCode6 { get; set; }
       
        public void PersistValues()
        {
            Saved = true;
        }

        #endregion
    }
}
