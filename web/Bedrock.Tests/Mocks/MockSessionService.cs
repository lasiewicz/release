﻿#region

using System;
using Matchnet.Session.ValueObjects;
using Matchnet.Session.ValueObjects.ServiceDefinitions;

#endregion

namespace Bedrock.Tests.Mocks
{
    public class MockSessionService : ISessionSA
    {
        private UserSession _userSession;

        #region ISessionSA Members

        public string GetCookieName(bool isDevMode)
        {
            throw new NotImplementedException();
        }

        public int GetMemberID(string pSessionGUID)
        {
            throw new NotImplementedException();
        }

        public int GetNextSessionTrackingID(UserSession session)
        {
            throw new NotImplementedException();
        }

        public UserSession GetSession(string key)
        {
            return _userSession != null ? _userSession : new UserSession(new Guid(key));
        }

        public UserSession GetSession(bool isDevMode)
        {
            return _userSession != null ? _userSession : new UserSession(new Guid());
        }

        public int GetSessionCount(int communityID)
        {
            throw new NotImplementedException();
        }

        public UserSession RESTCreateSession(int brandID, int memberID, string emailAddress, int genderMask)
        {
            throw new NotImplementedException();
        }

        public UserSession RESTGetSession(string key)
        {
            throw new NotImplementedException();
        }

        public void RESTSaveSession(UserSession session)
        {
            throw new NotImplementedException();
        }

        public void SaveSession(UserSession session, string domainName, bool isDevMode)
        {
            throw new NotImplementedException();
        }

        public void SaveSession(UserSession session, string domainName, bool isDevMode, bool async)
        {
            throw new NotImplementedException();
        }

        public void SaveSessionDetails(UserSession session, int siteID, SessionType sessionType)
        {
            throw new NotImplementedException();
        }

        #endregion

        public UserSession RESTCreateSession(int brandID)
        {
            throw new NotImplementedException();
        }

        public void SetUserSession(UserSession userSession)
        {
            _userSession = userSession;
        }
    }
}