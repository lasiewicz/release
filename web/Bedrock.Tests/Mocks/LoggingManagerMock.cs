﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Web.Interfaces;

namespace Bedrock.Tests.Mocks
{
    public class LoggingManagerMock: ILoggingManager
    {
        #region ILoggingManager Members

        public string LoggedMessage { get; private set; }

        public void LogException(Exception exception, IMember member, Brand brand, string className)
        {
            LoggedMessage = exception.ToString();
        }

        public void LogException(Exception exception, IMember member, int communityId, int siteID, int brandID, string className)
        {
            LoggedMessage = exception.ToString();
        }

        public void LogInfoMessage(string className, string message)
        {
            LoggedMessage = className + ": " + message;
        }

        #endregion
    }
}
