﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Interfaces;
using Matchnet.Web.Framework.Managers;

namespace Bedrock.Tests
{
    public class MockPhotoRulesManager: IPhotoRules
    {
        private bool _validImageResult = true;
        private bool _fileSizeAllowedResult = true;
        private bool _isAcceptedImageTypeResult = true;
        private bool _isFileNameExtensionAllowed = true;

        #region IPhotoRules Members

        public bool IsValidImage(System.IO.Stream stream, int contentLength)
        {
            return _validImageResult;
        }

        public bool IsFileSizeAllowed(int fileSize)
        {
            return _fileSizeAllowedResult;
        }

        public int GetMaxPhotoCount(Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            return new SettingsManager().GetSettingInt(SettingConstants.MAX_PHOTO_COUNT, brand);
        }

        public bool IsAcceptedImageType(byte[] input)
        {
            return _isAcceptedImageTypeResult;
        }

        public bool FileNameExtensionAllowed(string filename)
        {
            return _isFileNameExtensionAllowed;
        }

        #endregion

        public void SetValidImageResult(bool result)
        {
            _validImageResult = result;
        }

        public void SetFileSizeAllowedResult(bool result)
        {
            _fileSizeAllowedResult = result;
        }

        public void SetAcceptedImageTypeResult(bool result)
        {
            _isAcceptedImageTypeResult = result;
        }

        public void SetFileNameExtensionAllowed(bool allowed)
        {
            _isFileNameExtensionAllowed = allowed;
        }

        #region IPhotoRules Members


        


        #endregion
    }
}
