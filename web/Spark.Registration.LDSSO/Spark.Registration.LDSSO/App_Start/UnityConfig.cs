using System;
using System.Globalization;
using log4net;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Spark.Common.RestConsumer.V2;
using Spark.Registration.Framework;
using Spark.Registration.Framework.Helpers;
using Spark.Registration.Framework.HTTPContextWrappers;
using Spark.Registration.Interfaces.Adapters;
using Spark.Registration.Interfaces.Persistence;
using Spark.Registration.Interfaces.Tracking;
using Spark.Registration.Interfaces.Web;
using Spark.Registration.Models;
using Spark.Registration.RestClient.Web;

namespace Spark.Registration.LDSSO.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // Register any logging utils
            RegisterLoggers(container);

            // Register adapters
            RegisterAdapters(container);

            // Register Web Objects - requests, context, etc
            RegisterWebTypes(container);

            // Register Objects from the Framework folder
            RegisterFrameworkTypes(container);
        }

        private static void RegisterLoggers(IUnityContainer container)
        {
            // TODO: add class type into injection factory
            //container.RegisterType<ILog, >()
        }

        private static void RegisterAdapters(IUnityContainer container)
        {
            // ApiAdapter
            container.RegisterType<IApiAdapter, ApiAdapter>(new InjectionConstructor(
                    RegistrationConfiguration.ApplicationId.ToString(CultureInfo.InvariantCulture),
                    RegistrationConfiguration.ClientSecret,
                    LogManager.GetLogger(typeof(ApiAdapter)),
                    RestConsumer.Instance
                ));

            // Mobile site session adapter
            container.RegisterType<IMOSSessionAdapter, MOSSessionAdapter>();

            // Mingle Registration API
            container.RegisterType<IMingleRegistrationAdapter, ChristianmingleRegistrationAdapter>(
                new InjectionConstructor(
                    RegistrationConfiguration.UTRestAuthority,
                    RegistrationConfiguration.UTClientId,
                    RegistrationConfiguration.UTClientSecret
                    ));
        }

        private static void RegisterWebTypes(IUnityContainer container)
        {
            container.RegisterType<ICurrentContext, CurrentContext>();
            container.RegisterType<ICurrentBrowserCapabilities, CurrentBrowserCapabilities>();
            container.RegisterType<ICurrentRequest, CurrentRequest>();
            container.RegisterType<ICurrentResponse, CurrentResponse>();
            container.RegisterType<IRegistrationValuePersistence, RegistrationPersistence>();
        }

        private static void RegisterFrameworkTypes(IUnityContainer container)
        {
            container.RegisterType<IMemberLevelTracking, MemberLevelTracking>();
        }
    }
}
