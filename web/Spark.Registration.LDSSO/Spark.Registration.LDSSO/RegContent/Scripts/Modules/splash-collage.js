var splashCollage = (function() {
	var hello = {},

//Private variables
	gridPath = "RegContent/Images/splash-collage-couples-grid.jpg",
	offsetxpoint=6, //Customize x offset of tooltip
	offsetypoint=6, //Customize y offset of tooltip
	ie=document.all,
	ns6=document.getElementById && !document.all,
	enabletip=false,
	tipobj = null,
	resetImageID,
	coordImageID,
	hoverImageCoord = new Array("-8px -491px", "-229px -491px", "-346px -491px", "-462px -491px", "-765px -491px", "-890px -491px", "-229px -567px", "-462px -578px", "-8px -643px", "-136px -643px", "-229px -685px", "-346px -657px", "-504px -772px", "-622px -772px", "-771px -772px", "-8px -873px", "-109px -873px", "-208px -873px", "-444px -873px", "-622px -873px"),
	resetImageCoord = new Array("-8px -8px","229px 8px","346px 8px","462px 8px","765px 8px","890px 8px","229px 84px","462px 95px","8px 160px","136px 160px","229px 202px","346px 174px","504px 289px","622px 289px","771px 289px","8px 390px","109px 390px","208px 390px","444px 390px","622px 390px"),
	timedFunc = '';

	document.onmousemove=positiontip;

//public properties
//hello.someProperty = 0;
	
//private functions/code
function ietruebody(){
	return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body;
}

function positiontip(e){
	if (enabletip){
		var curX=(ns6)?e.pageX : event.clientX+ietruebody().scrollLeft;
		var curY=(ns6)?e.pageY : event.clientY+ietruebody().scrollTop;

		var rightedge=ie&&!window.opera? ietruebody().clientWidth-event.clientX-offsetxpoint : window.innerWidth-e.clientX-offsetxpoint-20;
		var bottomedge=ie&&!window.opera? ietruebody().clientHeight-event.clientY-offsetypoint : window.innerHeight-e.clientY-offsetypoint-20;
		
		var leftedge=(offsetxpoint<0)? offsetxpoint*(-1) : -1000;
		
		if (rightedge<tipobj.offsetWidth){
			tipobj.style.left=ie? ietruebody().scrollLeft+event.clientX-tipobj.offsetWidth+"px" : window.pageXOffset+e.clientX-tipobj.offsetWidth+"px";
		}
		else if (curX<leftedge){
			tipobj.style.left="5px";
		}
		else{
			tipobj.style.left=curX+offsetxpoint+"px";
		}

		if (bottomedge<tipobj.offsetHeight){
			tipobj.style.top=ie? ietruebody().scrollTop+event.clientY-tipobj.offsetHeight-offsetypoint+"px" : window.pageYOffset+e.clientY-tipobj.offsetHeight-offsetypoint+"px";
		}
		else{
			tipobj.style.top=curY+offsetypoint+"px";
			tipobj.style.display="block";
		}
	}
}

function resetImage(){
	if (resetImageID !== null){
		var ulImage = document.getElementById("couples");
		ulImage.style.backgroundPosition = "0px 0px";
		
		element = document.getElementById(resetImageID);

		if (element !== null){
			element.style.backgroundImage = "url(" + gridPath + ")";
			element.style.backgroundRepeat = "no-repeat";
			element.style.backgroundPosition = resetImageCoord[coordImageID];
		}
	}
}

//public functions
hello.TimedImage = function(){
  resetImage();
  //slide(timedNum,'mypic10','mylbl10');
  var randomnumber=Math.floor(Math.random()*20);
  coordImageID = randomnumber === 0 ? 0 : randomnumber-1;
  //alert(coordImageID);
  //dont want zero, so modify if 0 is encountered
  randomnumber = randomnumber === 0 ? 1 : randomnumber;
  
  var coupleID = "couple" + randomnumber;
  resetImageID = coupleID;
  var element = document.getElementById(coupleID);
  element.style.backgroundImage = "url(" + gridPath + ")";
  element.style.backgroundRepeat = "no-repeat";
  //element.style.backgroundPosition = "-346px -483px";
  element.style.backgroundPosition = hoverImageCoord[coordImageID];
  //alert(coupleID);
};
hello.InitTimeInterval = function() {
  timeFunc = setInterval("splashCollage.TimedImage()", 2000);
};

hello.ddrivetip = function(coupleName, coupleMarriedText){
	if (ns6||ie){
		tipobj=document.all? document.all["callout"] : document.getElementById? document.getElementById("callout") : "";
		
		tipobj.style.display="block";
		tipobj.innerHTML="<strong>" + coupleName + "</strong><br /><span style='font-size:10px; color:#666666;'>" + coupleMarriedText + "</span>";
		enabletip=true;
		return false;
	}
};

hello.hideddrivetip = function(){
	if(tipobj === null){return;}
	if (ns6||ie){
		enabletip=false;
		tipobj.style.display="none";
		tipobj.style.left="-1000px";
		tipobj.style.backgroundColor='';
		tipobj.style.width='';
	}
};

// lets do some brute-force bindings
// should be more dry
document.getElementById('couple1').onmouseover = function(){hello.ddrivetip('Vicky &amp; Chad', 'Married May 2013'); };
document.getElementById('couple2').onmouseover = function(){hello.ddrivetip('Juliana &amp; Alex', 'Married September 2012'); };
document.getElementById('couple3').onmouseover = function(){hello.ddrivetip('Meredith &amp; Irwin', 'Married February 2013'); };
document.getElementById('couple4').onmouseover = function(){hello.ddrivetip('Meryl &amp; Eric ', 'Engaged February 2013'); };
document.getElementById('couple5').onmouseover = function(){hello.ddrivetip('Amy &amp; Donny', 'Married June 2012'); };
document.getElementById('couple6').onmouseover = function(){hello.ddrivetip('Natalie &amp; Shai', 'Married March 2012');};
document.getElementById('couple7').onmouseover = function(){hello.ddrivetip('Allie &amp; Chad', 'Married May 2013'); };
document.getElementById('couple8').onmouseover = function(){hello.ddrivetip('Ronni &amp; John', 'Married October 2009');};
document.getElementById('couple9').onmouseover = function(){hello.ddrivetip('Alli &amp; Aaron', 'Engaged December 2012'); };
document.getElementById('couple10').onmouseover = function(){ hello.ddrivetip('Loni &amp; Daniel', 'Married May 2012'); };
document.getElementById('couple11').onmouseover = function(){hello.ddrivetip('Lorraine &amp; Aviv','Married May 2013'); };
document.getElementById('couple12').onmouseover = function(){hello.ddrivetip('Tiffany &amp; Josh', 'Engaged October 2012'); };
document.getElementById('couple13').onmouseover = function(){hello.ddrivetip('Jennifer &amp; Ethan', 'Married April 2013'); };
document.getElementById('couple14').onmouseover = function(){hello.ddrivetip('Jaclyn &amp; Jared', 'Engaged March 2013'); };
document.getElementById('couple15').onmouseover = function(){hello.ddrivetip('Stephanie &amp; Jonathan','Engaged March 2012'); };
document.getElementById('couple16').onmouseover = function(){hello.ddrivetip('Jessica &amp; Steve', 'Married May 2013'); };
document.getElementById('couple17').onmouseover = function(){hello.ddrivetip('Missy &amp; Seth', 'Married January 2013'); };
document.getElementById('couple18').onmouseover = function(){hello.ddrivetip('Lindsay &amp; Jared', 'Married 2013'); };
document.getElementById('couple19').onmouseover = function(){hello.ddrivetip('Jill &amp; Allen', 'Married September 2012'); };
document.getElementById('couple20').onmouseover = function(){hello.ddrivetip('Beverly &amp; Jay', 'Married May 2012');};

document.getElementById('couple1').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple2').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple3').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple4').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple5').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple6').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple7').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple8').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple9').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple10').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple11').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple12').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple13').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple14').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple15').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple16').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple17').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple18').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple19').onmouseout = function(){hello.hideddrivetip();};
document.getElementById('couple20').onmouseout = function(){hello.hideddrivetip();};

	return hello;
}());