/*
|--------------------------------------------------------------------------
| Region picker
|--------------------------------------------------------------------------
|
| Depedencies: jQuery, jQuery UI (for Countries that use Autocomplete), LAB.js, mustache.js, underscore, spin.js
| Code pattern: Object litteral pattern: http://rmurphey.com/blog/2009/10/15/using-objects-to-organize-your-code/
|
| regionPicker renders out the regionPicker UI. The Country dropdown is rendered on page/step load and then the rest of the regionPicker is built depending
| on the default, user selections, or previously entered data. If the user changes Country, existing data is cleared and UI elements removed and new ones added.
| Autocomplete validation relies on a flag in config. This flag is used both internally, and by other modules, for checking if a city was actually selected from
| the returned results. The reg-scenario.js module uses it for verifying validation beyond basic form validation (required, minlength, etc).
|
| ***Template type data customizations: For custom data manipulates. For example, if only one City result, set the City as Selected and add a Oneresult
| attribute (both used in rendering the html)
|
*/

var regionPicker = {
    'regionData': {},
    'config': {
        'container': "#regionPicker",
        'incomingData': {},
        'acInput': '#regionCitiesByRegionId',
        'acRegionId': '#citiesRegionID',
        'isAutoComplete': false,
        'isAutoCompleteValid': false,
        'acMsgNoMatchesOnSubmit': 'Try again! We were unable to find any cities matching the information you entered.',
        'acMsgNoResultsOnSubmit': 'Try again! We were unable to find any cities matching the information you entered.',
        'acMsgNoResultsOnChange': 'Try again! We were unable to find any cities matching the information you entered.',
        'acMsgNoCitiesSelected': 'Try again! We were unable to find any cities matching the information you entered.',
        'msgNoZipFound': 'Zip Code <%=zipcode%> Please enter a valid Zip Code.',
        'msgNoPostalCodeFound': 'Don\'t go postal on us, but the Postal Code you entered does not exist.',
        'msgFieldRequired': 'Can you just give us a little something? This field is required so you kind of have to!',
        'msgGeneralAjaxError': 'Talk about bad timing! There was a connection error. Please try again later.'
    },
    'init': function (config) {
        // This code is for handling any updates/additions to 'config' that get passed in during init().
        if (config && typeof (config) == 'object') {
            $.extend(regionPicker.config, config);
        }

        // cache dom items
        regionPicker.config.container = $(regionPicker.config.container);

        // stuff RegionData into regionData object for berevity
        _.each(regionPicker.config.incomingData.Step.PopulatedControls, function (el, item, list) {
            if (el.Control.RegionData) {
                regionPicker.regionData = el.Control.RegionData;
            }
        });

        // initial render and data manipulations
        regionPicker.initialRender();

        // bind events
        regionPicker.eventBindings();
    },
    'eventBindings' : function(){
        // Country change.
        regionPicker.config.container.on('change', '#idregioncountryid', function(event) {
            $('#regionPickerSubmit').hide();
            $('#btnContinue').show();
            var $this = $(this),
                region = $this.val();

            regionPicker.regionData.CountryRegionID = region;

            // empty all bins
            regionPicker.resetAll();

            // clear any leftover region data if a different region data was entered
            regionPicker.regionData.ZipCode = "";
            regionPicker.regionData.Cities = "";
            regionPicker.regionData.CityRegionID = "";
            regionPicker.regionData.States = "";
            regionPicker.regionData.StateRegionID = "";
            // set isAutoCompleteValid to false
            regionPicker.config.isAutoCompleteValid = false;

            // based on regionid
            switch (region) {
                case "1": // USA
                    regionPicker.renderer("RegionZipCode");
                    break;
                case "42": //Canada
                    regionPicker.renderer("RegionPostalCode");
                    break;
                default:
                    regionPicker.getJSON('/RegContent/region/states/' + region, function(){
                        // update States with data
                        regionPicker.regionData.States = this;

                        if(this.length > 1){
                            regionPicker.renderer("RegionStates");
                        } else{
                            regionPicker.regionData.StateRegionID = "0";
                            regionPicker.renderer("RegionCitiesAutocomplete");
                        }
                    }, $this.closest('.form-input'));
            }
        });

        // state change
        regionPicker.config.container.on('change', '#regionStateId', function(event) {
            var $this = $(this),
                region = $this.val();

            regionPicker.regionData.StateRegionID = region;
            regionPicker.renderer("RegionCitiesAutocomplete");
        });

        // zip code and postal code inputs
        regionPicker.config.container.on('keyup keypress', '#regionZipId, #regionPostalId', function(event) {
            var $this = $(this),
                postType = $this.is('#regionZipId') ? "zipcode" : "postalcode",
                postValue = $this.val(),
                postObj = { "zipcode" : postValue };

            // deactivate enter
            if(event.type === "keypress" && event.which === 13){
                event.preventDefault();
            }

            // USA zipcode
            if(event.type === "keyup" && postType === "zipcode" && $this.val().length === 5) {
                regionPicker.getJSON('/RegContent/region/cities/' + postValue, function(){
                    $('#binRegionCities').empty();

                    // check to make sure Cities does not come back empty
                    if(_.isEmpty(this)){
                        $.webshims.validityAlert.showFor($this, _.template(regionPicker.config.msgNoZipFound, postObj));
                        $this.val('');
                    } else {
                        regionPicker.regionData.Cities = this;
                        regionPicker.renderer("RegionCities");
                    }
                }, $this.closest('.form-input'));
            }

            // Canada postalcode
            if(event.type === "keyup" && postType === "postalcode" && $this.val().length === 6) {                
                regionPicker.getJSON('/RegContent/region/cities/' + postValue, function(){
                    $('#binRegionCities').empty();

                    // check to make sure Cities does not come back empty
                    if(_.isEmpty(this)){
                        $.webshims.validityAlert.showFor($this, _.template(regionPicker.config.msgNoPostalCodeFound, postObj));
                        $this.val('');
                    } else {
                        regionPicker.regionData.Cities = this;
                        regionPicker.renderer("RegionCities");
                    }
                }, $this.closest('.form-input'));
            }
        });
    },
    'renderer' : function(type){
        // ****Template type data customizations
        var custom = {};
        custom['RegionCities'] = function() {
            if(regionPicker.regionData.Cities.length === 1){
                regionPicker.regionData.Cities[0]["Selected"] = true;
                regionPicker.regionData.Cities[0]["Oneresult"] = true;
            }
        };
        // make sure type has an associated function, then execute
        if (typeof custom[type] === 'function') {
            custom[type]();
        }

        var template = $("#form-" + type + "-tpl").html();
        var html = Mustache.to_html(template, regionPicker.regionData);
        regionPicker.config.container.find('#bin' + type).html(html);

        // reset isAutoComplete flag
        regionPicker.config.isAutoComplete = false;
        // initialize autocomplete, if nec.
        if(type === "RegionCitiesAutocomplete"){
            regionPicker.initializeAutocomplete();
        }
    },
    'initialRender' : function(){
        // render initial region picker based on previous region, if any
        // user must have selected a final region so there will be no partially filled out forms

        // initial data manipulations
        // add error messages
        regionPicker.regionData["msgFieldRequired"] = regionPicker.config.msgFieldRequired;

        function renderAutocompleteWithData(){
            var cityName = "";

            if(_.isEmpty(regionPicker.regionData.Cities) === false){
                // using plain js and not underscore because the Cities object could be very large
                for (var i = 0, len = regionPicker.regionData.Cities.length; i < len; ++i) {
                    //to compare, the CityRegionID must be converted to a number
                    if(regionPicker.regionData.Cities[i].Id === Number(regionPicker.regionData.CityRegionID)){
                        cityName = regionPicker.regionData.Cities[i].Description;
                        break;
                    }
                }
            }

            regionPicker.renderer("RegionCitiesAutocomplete");
            // populate autopicker for elements with data
            $('#regionCitiesByRegionId').val(cityName);
            $('#citiesRegionID').val(regionPicker.regionData.CityRegionID);
        }
        // if we have pre-selected data coming in, set isAutoCompleteValid to true
        if(regionPicker.config.CountryRegionID !== 0){
            regionPicker.config.isAutoCompleteValid = true;
        }
        
        // "Clean"
        if(regionPicker.regionData.CountryRegionID === "0"){
            // console.log("Clean");
            // Some cleanup
            $("#idregioncountryid").val(regionPicker.regionData.CityRegionID).removeAttr("selected");
            return; // nothing to do here
        }
        // "ZipCode"
        else if(regionPicker.regionData.CountryRegionID === "1"){
            // console.log("ZipCode");
            // templates: Country, ZipCode, City (if more than one option)
            regionPicker.renderer("RegionZipCode");
            $('#regionZipId').val(regionPicker.regionData.ZipCode);

            // render City if Cities is not empty
            // the only reason this is neccesary is that USA needs to load by default
            if(_.isEmpty(regionPicker.regionData.Cities) === false){
                regionPicker.renderer("RegionCities");
                $("#regionCitiesId").val(regionPicker.regionData.CityRegionID).attr("selected", "selected");
            }
            return;
        }
        // "PostalCode"
        else if (regionPicker.regionData.CountryRegionID === "42"){
            // console.log("PostalCode");
            // templates: Country, PostalCode
            regionPicker.renderer("RegionPostalCode");
            $('#regionPostalId').val(regionPicker.regionData.ZipCode);

            // render City if Cities is not empty
            // the only reason this is neccesary is that USA needs to load by default
            if(_.isEmpty(regionPicker.regionData.Cities) === false){
                regionPicker.renderer("RegionCities");
                $("#regionCitiesId").val(regionPicker.regionData.CityRegionID).attr("selected", "selected");
            }
            return;
        }
        // "Default"
        else{
            // console.log("Default");
            // templates: Country, States, CitiesAutocomplete

            if(_.isEmpty(regionPicker.regionData.States) === false){
                if(_.size(regionPicker.regionData.States) > 1){
                    regionPicker.renderer("RegionStates");
                    $("#regionStateId").val(regionPicker.regionData.StateRegionID).attr("selected", "selected");
                    renderAutocompleteWithData();
                } else{
                    renderAutocompleteWithData();
                }
            }
        }
    },
    'getJSON' : function(path, callback, spinnerElement){
        spinnerElement.spin('tiny-over');

        $.ajax({
            url: path,
            dataType: "json",
            beforeSend: function(){
                $('#btnContinue, [role=next]').addClass('disabled').attr('disabled', 'disabled');
            },
            success: function( data ) {
                callback.call(data);
                spinnerElement.spin(false);
                $('#btnContinue, [role=next]').removeClass('disabled').removeAttr('disabled');
            },
            cache: false
        });
    },
    'resetAll' : function(){
        // use this to empty all bins
        $('#binRegionZipCode, #binRegionPostalCode, #binRegionStates, #binRegionCities, #binRegionCitiesAutocomplete').empty();
    },
    'initializeAutocomplete' : function(){
        // cache dom elements
        var $acInput = $(regionPicker.config.acInput),
            $acRegionId = $(regionPicker.config.acRegionId);

        // set isAutoComplete flag to true
        regionPicker.config.isAutoComplete = true;
        // console.log('autocomplete initialized! isAutoCompleteValid set to false');

        // dependencies jQueryUI
        $LAB
        .script(function () {
            // check for jQueryUI, load core if necessary
            if (typeof jQuery.ui === "undefined") return "https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.min.js";
        }).wait(function () {

            // add highlighting to entered autocomplete data
            $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                var highlightedLabel = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + highlightedLabel + "</a>")
                .appendTo(ul);
            };

            $acInput.autocomplete({
                source: function( request, response ) {
                    // console.log(request);
                    $acInput.closest('.form-input').spin('tiny-over');
                    $.ajax({
                        url: "/RegContent/region/cities/" + regionPicker.regionData.CountryRegionID + "/" + regionPicker.regionData.StateRegionID + "/" + request.term,
                        dataType: "json",
                        success: function( data ) {
                            response(_.map(data, function(value){
                                return{
                                    label: value.Description,
                                    id: value.Id,
                                    value: value.Description
                                };
                            }));
                            regionPicker.regionData.Cities = data;
                        }
                    })
                    .done(function() {
                        $acInput.closest('.form-input').spin(false);

                        if(_.isEmpty(regionPicker.regionData.Cities)){
                            $(spark).trigger( "uiMessaging", [{
                                type: "autocomplete error",
                                content: regionPicker.config.acMsgNoResultsOnChange,
                                selector: $acInput
                            }] );

                            $('#regionCitiesByRegionId').addClass('autocomplete-invalid');

                            regionPicker.config.isAutoCompleteValid = false;
                            // console.log('source ajax empty. isAutoCompleteValid set to false');
                            return;
                        }
                    })
                    .fail(function(){
                        $(spark).trigger( "uiMessaging", [{
                            type: "autocomplete error",
                            content: regionPicker.config.msgGeneralAjaxError,
                            selector: $acInput
                        }] );
                        $('#regionCitiesByRegionId').addClass('autocomplete-invalid');
                        regionPicker.config.isAutoCompleteValid = false;
                        // console.log('source ajax fail. isAutoCompleteValid set to false');
                    });
                },
                minLength: 2,
                delay: 500,
                change: function(event, ui){
                    regionPicker.config.isAutoCompleteValid = false;
                    // console.log('autocomplete change initiated. isAutoCompleteValid set to false');

                    $(spark).trigger( "uiMessaging", [{ off: true, selector: $acInput }] );
                    $('#regionCitiesByRegionId').removeClass('autocomplete-invalid');

                    var value = $acInput.val();

                    _.find(regionPicker.regionData.Cities, function (el) {
                        if (el.Description === value) {
                            $acRegionId.val(el.Id);
                            regionPicker.config.isAutoCompleteValid = true;
                            // console.log('autocomplete change input val found in Cities. isAutoCompleteValid set to true');
                            return el.Description === value;
                        }
                    });

                     if(!regionPicker.config.isAutoCompleteValid){
                        $(spark).trigger( "uiMessaging", [{
                            type: "autocomplete error",
                            content: regionPicker.config.acMsgNoCitiesSelected,
                            selector: $acInput
                        }] );
                        $('#regionCitiesByRegionId').addClass('autocomplete-invalid');
                        // console.log('autocomplete change after comparison. isAutoCompleteValid still false');
                    }
                },
                select: function( event, ui ) {
                    $acInput.next('.error').remove();
                    $acInput.val( ui.item.label );
                    $acRegionId.val( ui.item.id );

                    regionPicker.config.isAutoCompleteValid = true;
                    // console.log('autocomplete item select. isAutoCompleteValid set to true');

                    return false;
                }
            });
        });
    }
};
