﻿using System;
using System.IO;
using System.Net;
using System.Web.Mvc;
using Spark.Registration.Managers;

namespace Spark.Registration.Controllers
{
    public class AdminController : Controller
    {
        const string MatchnetServerDown = "Matchnet Server Down";
        const string MatchnetServerEnabled = "Matchnet Server Enabled";

        private const string CacheCleared = "Scenario Cache Cleared";
        private const string CacheNotCleared = "Scenario Cache Not Cleared";

        public ActionResult GetHealthCheck()
        {
            var serverStatus = MatchnetServerDown;
            try
            {
                var filePath = Path.GetDirectoryName(Server.MapPath("/")) + @"\deploy.txt";
                var fileExists = System.IO.File.Exists(filePath);

                if (!fileExists)
                {
                    serverStatus = MatchnetServerEnabled;
                }
            }
            catch (Exception)
            {
                serverStatus = MatchnetServerDown;
            }

            try
            {
                Response.AddHeader("Health", serverStatus);
                Response.Write(serverStatus);
                Response.End();
            }
            catch (Exception)
            {
                //do nothing, this error happens when we try to respond but the client has already disconnected
            }

            return Content(serverStatus);
        }

        //This is similar to how 'InvokeHttp404' Action result is in other projects
        public ActionResult Get404()
        {
            Response.TrySkipIisCustomErrors = true;
            Response.StatusCode = (int)HttpStatusCode.NotFound;

            return new ViewResult { ViewName = "~/Views/Error/Error.cshtml" };
        }

        public ActionResult ClearScenarioCache()
        {
            var cacheManager = new CacheManager();
            var cacheCleared = cacheManager.ClearScenariosCache();

            if (cacheCleared)
            {
                return Content(CacheCleared);
            }
            else
            {
                return Content(CacheNotCleared);
            }
        }

    }
}
