﻿using System.Web.Mvc;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Registration.Framework;
using Spark.Registration.Framework.Helpers;
using Spark.Registration.Framework.HTTPContextWrappers;
using Spark.Registration.Interfaces.Web;
using Spark.Registration.Interfaces.Scenarios;
using Spark.Registration.Managers;
using Spark.Registration.Models;
//using Spark.Registration.Dictionaries.Constants; 


namespace Spark.Registration.LDSSO.Controllers
{
    public class OmnitureController : BaseController
    {
        private IScenarioLoader _scenarioLoader;

        public IScenarioLoader ScenarioLoader
        {
            get { return _scenarioLoader ?? (_scenarioLoader = new APIScenarioLoader()); }
            set { _scenarioLoader = value; }
        }

        public PartialViewResult Index(string pageName)
        {
            Scenario scenario = null;
            //Fix for Ominuture variable getting wrong scenario name on Confirmation page.
            //Since persistence is cleared after successful registration, it loads a random scenario.
            //Soln - If Usersession contains scenarioname, grab scenario from usersession.
            if (UserSession != null && UserSession[SessionConstants.ScenarioName] != null)
            {
                var scenarioName = UserSession[SessionConstants.ScenarioName].ToString();
                scenario = ScenarioLoader.GetNamedScenario(scenarioName);
            }
            if (scenario == null) //If for some reason usersession did not have valid scenario name, this is a fall back mechanism to get random scenario.
                scenario = ScenarioLoader.GetCurrentScenario();
            //var pageName = ControllerContext.ParentActionViewContext.RouteData.Values["action"].ToString();
            var currentRequest = UnityHelper.Resolver.GetService(typeof(ICurrentRequest)) as ICurrentRequest;
            var omnitureManager = new OmnitureManager(pageName, currentRequest, scenario);
            var viewModel = omnitureManager.GetOmnitureModel();

            return PartialView("Omniture", viewModel);
        }

    }
}
