﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.RestConsumer.V2.Models.Content.BrandConfig;
using Spark.Common.RestConsumer.V2.Models.Session;
using Spark.Registration.Framework;
using Spark.Registration.Framework.Analytics;
using Spark.Registration.Framework.Exceptions;
using Spark.Registration.Framework.Helpers;
using Spark.Registration.Interfaces.Adapters;
using Spark.Registration.Interfaces.Persistence;
using Spark.Registration.Managers;
using Spark.Registration.Models;
using Spark.Registration.Interfaces;
//using Spark.Registration.Dictionaries.Constants; 

//More than likely this base controller can be shared across all of the reg sites. It is proabably not LDSSO specific.
namespace Spark.Registration.LDSSO.Controllers
{

    //If we wanted to, we could create a few filters that would do important things such as redirect user to www, 
    public class BaseController : System.Web.Mvc.Controller
    {
        private readonly IApiAdapter _apiAdapter;
        private IRegistrationValuePersistence _persistence;
        private bool _generatePersistence = true;

        protected Session UserSession { get; set; }
        protected DebugInfo DebugInfo { get; private set; }

        protected bool GeneratePersistence
        {
            get { return _generatePersistence; }
            set { _generatePersistence = value; }
        }

        public IApiAdapter ApiAdapter
        {
            get { return _apiAdapter; }
        }

        public IRegistrationValuePersistence Persistence
        {
            get { return _persistence ?? (_persistence = new RegistrationPersistence()); }
            set { _persistence = value; }
        }

        private Omniture _omniture;
        protected Omniture Omniture
        {
            get
            {
                _omniture = HttpContext.Items["Omniture"] as Omniture;

                if (_omniture == null)
                {
                    _omniture = new Omniture();
                    HttpContext.Items["Omniture"] = _omniture;
                }

                return _omniture;
            }
        }

        protected new HttpContextBase HttpContext
        {
            get
            {
                var context = new HttpContextWrapper(System.Web.HttpContext.Current);
                return context;
            }
        }

        protected BaseController() :
            this(UnityHelper.Resolver.GetService(typeof(IApiAdapter)) as IApiAdapter)
        {

        }

        protected BaseController(IApiAdapter apiAdapter)
        {
            _apiAdapter = apiAdapter;
            UserSession = HttpContext.Items["UserSession"] as Session;
            if (HttpContext.Request != null && UserSession != null
                && (UserSession[SessionConstants.GenerateRegistrationPersistence] == null || (UserSession[SessionConstants.GenerateRegistrationPersistence] != null && !Conversion.CBool(UserSession[SessionConstants.GenerateRegistrationPersistence], false))))
            {
                SetAffiliateVariables();
                SetBedrockTransferCookie();
                ProcessLandingPageTransferCookie();
                ProccessCampLPTestIDCookie();
            }

            SetDebugInfo();

            if (UserSession != null && string.IsNullOrEmpty(UserSession.Key))
            {
                //set the sessionid from persistence if it isn't there already. Persistence can span 
                //multiple sessions because the persistence timeout is 30 days
                UserSession.Key = Persistence[PersistenceConstants.SessionID];
            }

        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.BedrockHost = RegistrationConfiguration.BedrockHost;
            ViewBag.MOSHost = RegistrationConfiguration.MOSHost;
            ViewBag.DebugInfo = DebugInfo;
            SetLogonAndSubscriptionLink();
            SetOmnitureAccount();
        }

        private void SetDebugInfo()
        {
            if (HttpContext.Request != null && UserSession != null)
            {
                DebugInfo = new DebugInfo
                {
                    Host = Environment.MachineName,
                    CommunityID = 3,
                    URL = HttpContext.Request.Url != null ? HttpContext.Request.Url.Host : string.Empty
                };

                if (UserSession[SessionConstants.PromotionId] != null)
                {
                    DebugInfo.PromotionID = Conversion.CInt(UserSession[SessionConstants.PromotionId]);
                }
                if (UserSession[SessionConstants.BannerId] != null)
                {
                    DebugInfo.BannerID = Conversion.CInt(UserSession[SessionConstants.BannerId]);
                }
                if (UserSession[SessionConstants.LandingPageID] != null)
                {
                    DebugInfo.LandingPageID = Conversion.CInt(UserSession[SessionConstants.LandingPageID]);
                }
                if (UserSession[SessionConstants.Luggage] != null)
                {
                    DebugInfo.Luggage = UserSession[SessionConstants.Luggage].ToString();
                }
            }
        }


        internal void SetAffiliateVariables()
        {
            var newPromotion = SetPromotionSession();

            if (newPromotion)
            {
                UserSession.RemoveValue(SessionConstants.Luggage);
            }

            bool luggageAdded = AddAffiliateVariableFromRequest(URLConstants.LuggageID, SessionConstants.Luggage, newPromotion);
            if (!luggageAdded)
            {
                AddAffiliateVariableFromRequest(URLConstants.Luggage, SessionConstants.Luggage, newPromotion);
            }
            AddAffiliateVariableFromRequest(URLConstants.BannerID, SessionConstants.BannerId, newPromotion);
            AddAffiliateVariableFromRequest(URLConstants.Refcd, SessionConstants.Refcd, alwaysSetInSession: true);
            AddAffiliateVariableFromRequest(URLConstants.TSACR, SessionConstants.TSACR, alwaysSetInSession: true);
            AddAffiliateVariableFromRequest(URLConstants.EID, SessionConstants.EID, alwaysSetInSession: true);
            AddAffiliateVariableFromRequest(URLConstants.LandingPageID, SessionConstants.LandingPageID, alwaysSetInSession: true);
            AddAffiliateVariableFromRequest(URLConstants.LGID2, SessionConstants.LGID2, alwaysSetInSession: true);
            AddAffiliateVariableFromRequest(URLConstants.Omnivar, SessionConstants.Omnivar, alwaysSetInSession: true);

            if (UserSession[SessionConstants.Luggage] == null)
            {
                //special case for Commission junction luggage
                var luggage = HttpContext.Request[URLConstants.AID] + " " +
                          HttpContext.Request[URLConstants.PID] + " " +
                          HttpContext.Request[URLConstants.SID];
                if (luggage.Length > 2)
                {
                    UserSession[SessionConstants.Luggage] = luggage;
                }
            }

            var referrer = string.Empty;
            if (!string.IsNullOrEmpty(HttpContext.Request[URLConstants.Referrer]))
            {
                referrer = HttpContext.Request[URLConstants.Referrer];
            }
            else if (HttpContext.Request.UrlReferrer != null)
            {
                referrer = HttpContext.Request.UrlReferrer.AbsoluteUri;
            }

            if (UserSession[SessionConstants.PromotionId] == null)
            {
                //no prm supplied, so map it from the URL
                var mappingResult = ApiAdapter.MapMapURLToPromo(referrer);
                UserSession[SessionConstants.PromotionId] = mappingResult.PromotionID;

                if (mappingResult.LuggageID != null && UserSession[SessionConstants.Luggage] == null && !mappingResult.LuggageID.ToLower().Contains(RegistrationConfiguration.SiteURI))
                {
                    UserSession[SessionConstants.Luggage] = mappingResult.LuggageID;
                }
                else if (UserSession[SessionConstants.Luggage] == null)
                {
                    UserSession[SessionConstants.Luggage] = string.Empty;
                }
            }

        }

        private void ProcessLandingPageTransferCookie()
        {
            HttpCookie landingPageTransferCookie = HttpContext.Request.Cookies["landingPageTransfer"];
            if (landingPageTransferCookie != null)
            {
                UserSession[SessionConstants.RequestFromLandingPage] = true;
                landingPageTransferCookie.Expires = DateTime.Now;
                HttpContext.Response.Cookies.Add(landingPageTransferCookie);
            }
        }

        private void ProccessCampLPTestIDCookie()
        {
            HttpCookie campLPTestID = HttpContext.Request.Cookies["camp_lptestid"];
            if (campLPTestID != null)
            {
                UserSession[SessionConstants.LandingPageTestID] = campLPTestID.Value;
                campLPTestID.Expires = DateTime.Now;
                //HttpContext.Response.Cookies.Add(campLPTestID);
            }
        }

        private void SetBedrockTransferCookie()
        {
            var cookieMaanger = new CookieManager();

            if (!cookieMaanger.RegistrationTransferCookieSet())
            {
                var cookieValues = new Dictionary<string, string>();

                if (UserSession[SessionConstants.Luggage] != null)
                {
                    cookieValues.Add(SessionConstants.Luggage, UserSession[SessionConstants.Luggage].ToString());
                }

                if (UserSession[SessionConstants.PromotionId] != null)
                {
                    cookieValues.Add(SessionConstants.PromotionId, UserSession[SessionConstants.PromotionId].ToString());
                }

                if (UserSession[SessionConstants.LGID2] != null)
                {
                    cookieValues.Add(SessionConstants.LGID2, UserSession[SessionConstants.LGID2].ToString());
                }

                if (UserSession[SessionConstants.BannerId] != null)
                {
                    cookieValues.Add(SessionConstants.BannerId, UserSession[SessionConstants.BannerId].ToString());
                }

                if (UserSession[SessionConstants.Refcd] != null)
                {
                    cookieValues.Add(SessionConstants.Refcd, UserSession[SessionConstants.Refcd].ToString());
                }

                if (UserSession[SessionConstants.LandingPageID] != null)
                {
                    cookieValues.Add(SessionConstants.LandingPageID, UserSession[SessionConstants.LandingPageID].ToString());
                }

                if (UserSession[SessionConstants.Omnivar] != null)
                {
                    cookieValues.Add(SessionConstants.Omnivar, UserSession[SessionConstants.Omnivar].ToString());
                }

                if (UserSession[SessionConstants.EID] != null)
                {
                    cookieValues.Add(SessionConstants.EID, UserSession[SessionConstants.EID].ToString());
                }

                cookieMaanger.GenerateRegistrationTransferCookie(cookieValues);
            }
        }

        private bool SetPromotionSession()
        {
            var existingPRM = UserSession[SessionConstants.PromotionId] != null
                ? Conversion.CInt(UserSession[SessionConstants.PromotionId], Matchnet.Constants.NULL_INT) : Matchnet.Constants.NULL_INT;
            int newPRM;

            if (!string.IsNullOrEmpty(HttpContext.Request[URLConstants.PromotionID]))
            {
                newPRM = Conversion.CInt(HttpContext.Request[URLConstants.PromotionID], Matchnet.Constants.NULL_INT);
                if (newPRM != Matchnet.Constants.NULL_INT && newPRM != existingPRM)
                {
                    UserSession[SessionConstants.PromotionId] = newPRM;
                    return true;
                }
            }

            if (!string.IsNullOrEmpty(HttpContext.Request[URLConstants.PRM]))
            {
                newPRM = Conversion.CInt(HttpContext.Request[URLConstants.PRM], Matchnet.Constants.NULL_INT);
                if (newPRM != Matchnet.Constants.NULL_INT && newPRM != existingPRM)
                {
                    UserSession[SessionConstants.PromotionId] = newPRM;
                    return true;
                }
            }

            HttpCookie landingPageTransferCookie = HttpContext.Request.Cookies["landingPageTransfer"];
            if (landingPageTransferCookie != null && !string.IsNullOrEmpty(landingPageTransferCookie[URLConstants.PRM]))
            {
                newPRM = Conversion.CInt(landingPageTransferCookie[URLConstants.PRM], Matchnet.Constants.NULL_INT);
                if (newPRM != Matchnet.Constants.NULL_INT && newPRM != existingPRM)
                {
                    UserSession[SessionConstants.PromotionId] = newPRM;
                    return true;
                }
            }

            //if we haven't set prm in session yet, this is is a new promotion session
            if (UserSession[SessionConstants.PromotionId] == null) return true;

            return false;
        }

        /// <summary>
        /// Helper for Set AffiliateVariables
        /// </summary>
        /// <param name="checkVar"></param>
        /// <param name="actualVar"></param>
        /// <param name="overwriteExistingValue"></param>
        /// <param name="alwaysSetInSession"></param>
        /// <returns></returns>
        private bool AddAffiliateVariableFromRequest(string checkVar, string actualVar, bool overwriteExistingValue = false, bool alwaysSetInSession = false)
        {
            bool added = false;

            if (overwriteExistingValue) UserSession.RemoveValue(actualVar);

            var val = HttpContext.Request[checkVar];

            if ((alwaysSetInSession && !String.IsNullOrWhiteSpace(val)) || (!String.IsNullOrWhiteSpace(val) && (UserSession[actualVar] != null && UserSession[actualVar].ToString() != val || overwriteExistingValue)))
            {
                UserSession[actualVar] = val;
                return true;
            }

            HttpCookie landingPageTransferCookie = HttpContext.Request.Cookies["landingPageTransfer"];
            if (landingPageTransferCookie != null && !string.IsNullOrEmpty(landingPageTransferCookie[checkVar]))
            {
                UserSession[actualVar] = landingPageTransferCookie[checkVar];
                Persistence.AddValue(actualVar, landingPageTransferCookie[checkVar]);
                added = true;
            }

            return added;
        }

        private void SetLogonAndSubscriptionLink()
        {
            //OI-1902 - This is to make sure we append any destination url that comes as part of promotion.
            ViewBag.LogonLink = "http://" + RegistrationConfiguration.BedrockHost + "/member_login.html";
            ViewBag.SubscriptionLink = "http://" + RegistrationConfiguration.BedrockHost + "/account/payment/purchase.html?&refer=renew";
            if (!string.IsNullOrEmpty(HttpContext.Request[URLConstants.DestinationURL]))
            {
                var destinationUrl = string.Empty;
                destinationUrl = HttpContext.Request[URLConstants.DestinationURL];
                if (!string.IsNullOrEmpty(destinationUrl))
                {
                    ViewBag.LogonLink = string.Format("{0}?DestinationURL={1}", ViewBag.LogonLink, destinationUrl);

                    //Set Subscription Link
                    //Append promoId to the link if there is a destination URL
                    if (destinationUrl.ToLower().Contains("subscribe"))
                    {
                        destinationUrl += (destinationUrl.Contains("?") ? "&prtid=41" : "?prtid=41");
                        if (destinationUrl.StartsWith("http"))
                            ViewBag.SubscriptionLink = destinationUrl;
                        else
                            ViewBag.SubscriptionLink = "http://" + RegistrationConfiguration.BedrockHost +
                                                       destinationUrl;
                    }
                }
            }
        }

        private void SetOmnitureAccount()
        {
            ViewBag.OmnitureAccountName = RegistrationConfiguration.GetRuntimeSettings("OMNITURE_ACCOUNT_NAME");
        }

    }
}
