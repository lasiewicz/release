﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.RestConsumer.V2.Models.Content.Registration;
using Spark.Registration.Framework;
using Spark.Registration.Framework.Attributes;
using Spark.Registration.Framework.HTTPContextWrappers;
using Spark.Registration.Framework.Helpers;
using Spark.Registration.Interfaces;
using Spark.Registration.Interfaces.Adapters;
using Spark.Registration.Interfaces.Scenarios;
using Spark.Registration.Interfaces.Web;
using Spark.Registration.Managers;
using Spark.Registration.Models;
using Spark.Registration.Serialization;
using log4net;
//using Spark.Registration.Dictionaries.Constants;
//using Spark.Registration.Dictionaries.Constants; 


namespace Spark.Registration.LDSSO.Controllers
{
    public class RegistrationController : BaseController
    {
        private IScenarioLoader _scenarioLoader;
        private readonly ICurrentRequest _currentRequest;

        public RegistrationController(IApiAdapter apiAdapter, ICurrentRequest currentRequest)
            : base(apiAdapter)
        {
            _currentRequest = currentRequest;
        }

        public IScenarioLoader ScenarioLoader
        {
            get
            {
                HttpRequest httpRequest = System.Web.HttpContext.Current.Request;
                httpRequest.Headers.Add(ExternalValueConstants.XSparkOriginIP, HttpContext.Request.ServerVariables["REMOTE_HOST"]);
                var httpRequestBase = new HttpRequestWrapper(System.Web.HttpContext.Current.Request);
                return _scenarioLoader ?? (_scenarioLoader = new APIScenarioLoader(null, false, httpRequestBase));
            }
            set { _scenarioLoader = value; }
        }

        public ICurrentRequest CurrentRequest
        {
            get { return _currentRequest; }
        }

        private static readonly ILog Log = LogManager.GetLogger(typeof(RegistrationController));

        public ActionResult GetNextStep()
        {
            //adding any custome request headers. 
            HttpContext.Request.Headers.Add(ExternalValueConstants.XSparkOriginIP, CurrentRequest.ClientIP);

            var flowManager = new RegistrationFlowManager();
            var stepModelView = flowManager.GetNextStep();

            return new NewtonsoftJsonResult { Result = stepModelView };
        }

        public ActionResult GetPreviousStep()
        {
            //adding any custome request headers. 
            HttpContext.Request.Headers.Add(ExternalValueConstants.XSparkOriginIP, CurrentRequest.ClientIP);

            var flowManager = new RegistrationFlowManager();
            var stepModelView = flowManager.GetPreviousStep();

            return new NewtonsoftJsonResult { Result = stepModelView };
        }

        public ActionResult GetCurrentStep()
        {
            HttpRequest httpRequest = System.Web.HttpContext.Current.Request;
            httpRequest.Headers.Add(ExternalValueConstants.XSparkOriginIP, HttpContext.Request.ServerVariables["REMOTE_HOST"]);
            var httpRequestBase = new HttpRequestWrapper(System.Web.HttpContext.Current.Request);

            var flowManager = new RegistrationFlowManager();
            var stepModelView = flowManager.GetCurrentStep(httpRequestBase);

            return new NewtonsoftJsonResult { Result = stepModelView };
        }

        [HttpPost]
        public ActionResult SubmitStep(List<NameValuePair> values)
        {
            UpdateRegistrationResponse response;
            try
            {
                var flowManager = new RegistrationFlowManager();
                response = flowManager.SubmitStep(values);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                response = new UpdateRegistrationResponse { Status = UpdateRegistrationStatus.Failure };
                response.ErrorMessages.Add("General", RegistrationConfiguration.FailedRegistrationMessage);
            }

            return new NewtonsoftJsonResult { Result = response };
        }
        // It doesn't appear that these filters are used anymore. Instead, the FWS determines whether or not to autologin the user. If they dont autologin the user, they redirect to reg site. 
        //We could simplify this filter by just creating one filter named [RedirectBasedOnRequestCookie]
        [RedirectToBedrockForLoggedInMembers]
        [RedirectToMOSForLoggedInMembers]
        [RedirectToSsl(Order = 10)]
        public ActionResult Splash()
        {
            //adding any custome request headers. 
            HttpContext.Request.Headers.Add(ExternalValueConstants.XSparkOriginIP, CurrentRequest.ClientIP);

            if (string.IsNullOrEmpty(Persistence[PersistenceConstants.SessionTrackingID]))
            {
                var sessionTrackingHelper = new SessionTrackingHelper();
                sessionTrackingHelper.CreateAndPersistSessionTracking(Persistence, ApiAdapter, UserSession, CurrentRequest.ClientIP);
            }

            var templateManager = new TemplateManager();
            var membersOnlineManager = new MembersOnlineManager();

            ViewBag.MembersOnlineCount = membersOnlineManager.GetMembersOnlineCount();
            return new ViewResult { ViewName = templateManager.GetSplashTemplateViewPath(), ViewData = ViewData };
        }

        public ActionResult Confirmation(string scenarioName)
        {
            if (UserSession.MemberId <= 0 || UserSession[SessionConstants.ScenarioName] == null)
            {
                //if there's no memberid, registration hasn't completed, so redirect
                //also, if the scenario name is empty then it wasn't set correctly or the session timed out, so we need to redirect
                return RedirectToAction("Splash");
            }

            var templateManager = new TemplateManager();
            string viewName = templateManager.GetConfirmationTemplateViewPath(UserSession[SessionConstants.ScenarioName].ToString());

            if (string.IsNullOrEmpty(viewName))
            {
                Log.Error("Could not find confirmation view");
                return RedirectToAction("Splash");
            }

            Omniture.SetVariable("events", "event8");
            Omniture.SetVariable("prop72", UserSession[SessionConstants.ScenarioName].ToString());
            Omniture.SetVariable("prop74", UserSession.Key);
            Omniture.SetVariable("evar48", UserSession[SessionConstants.ScenarioName].ToString());

            var mOm = new MembersOnlineManager();


            ViewBag.MembersOnlineCount = mOm.GetMembersOnlineCount();
            ViewBag.NewMemberId = UserSession.MemberId; //add member id to view bag for use with button on confimarion page. 


            return new ViewResult { ViewName = viewName, ViewData = ViewData };
        }

        //I cant see these action filters ever redirecting the user because when you start on this app, the first method is 'Splash', which executes the filters.
        //Does the user ever click on a link from any page OTHER THAN the index page of the reg site? If they dont, then these filters are redundant. 
        //However, I do see that the only thing that a user needs to call this action method is a link to <DomainName>.com/Registration. I woudl imagine that we want to have the flexibility
        // to redirect the user to some user page from within another page, say "about me" or "success stories". Some place/page a user lands but hasnt yet signed up. 
        [RedirectToBedrockForLoggedInMembers]
        [RedirectToMOSForLoggedInMembers]
        [RedirectToSsl(Order = 10)]
        public ActionResult Registration()
        {
            //adding any custome request headers. 
            HttpContext.Request.Headers.Add(ExternalValueConstants.XSparkOriginIP, CurrentRequest.ClientIP);


            if (string.IsNullOrEmpty(Persistence[PersistenceConstants.SessionTrackingID]))
            {
                var sessionTrackingHelper = new SessionTrackingHelper();
                sessionTrackingHelper.CreateAndPersistSessionTracking(Persistence, ApiAdapter, UserSession, CurrentRequest.ClientIP);
            }

            if (HttpContext.Request[URLConstants.StaticRegForm] != null)
            {
                var scenario = ScenarioLoader.GetCurrentScenario();
                var externalValuesProcessor = new ExternalValuesProcessor(CurrentRequest, scenario, Persistence);
                externalValuesProcessor.ProcessPostedValues();
            }

            if (HttpContext.Request[URLConstants.RecaptureID] != null)
            {
                var scenario = ScenarioLoader.GetCurrentScenario();
                var allScenarios = ScenarioLoader.GetScenarios();
                var recaptureHelper = new RecaptureHelper();
                recaptureHelper.LoadPersistenceFromRecapture(Persistence, ApiAdapter, HttpContext.Request[URLConstants.RecaptureID], scenario, allScenarios);
            }

            var templateManager = new TemplateManager();
            return new ViewResult { ViewName = templateManager.GetRegistrationTemplateViewPath(), ViewData = ViewData };
        }


        public ActionResult ValidateAttributeValue(string attributeName, string attributeValue)
        {
            bool attributeValueExists = false;
            try
            {
                var validationManager = new ValidationManager();
                attributeValueExists = validationManager.ValidateRegistrationAttributeValue(attributeName,
                                                                                            attributeValue);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            var result = new { AttributeValueExists = attributeValueExists };
            return new NewtonsoftJsonResult { Result = result };
        }

        public ActionResult ValidateMingleAttributeValue(string attributeName, string attributeValue)
        {
            bool attributeValueExists = false;
            try
            {
                var validationManager = new ValidationManager();
                attributeValueExists = validationManager.ValidateMingleRegistrationAttributeValue(attributeName,
                                                                                            attributeValue);


            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            var result = new { AttributeValueExists = attributeValueExists };
            return new NewtonsoftJsonResult { Result = result };

        }

        public ActionResult Error()
        {
            var templateManager = new TemplateManager();
            return new ViewResult { ViewName = templateManager.GetErrorTemplateViewPath(), ViewData = ViewData };
        }
    }
}
