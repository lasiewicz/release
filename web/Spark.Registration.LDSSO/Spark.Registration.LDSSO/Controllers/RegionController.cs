﻿using System.Web.Mvc;
using Spark.Registration.Managers;
using Spark.Registration.Serialization;

namespace Spark.Registration.LDSSO.Controllers
{
    public class RegionController : Controller
    {

        public ActionResult GetCitiesForZipCode(string zipCode)
        {
            var regionManager = new RegionManager();
            var cities = regionManager.GetCitiesForZipCode(zipCode);
            return new NewtonsoftJsonResult { Result = cities };
        }

        public ActionResult GetStates(int countryRegionID)
        {
            var regionManager = new RegionManager();
            var states = regionManager.GetStates(countryRegionID);
            return new NewtonsoftJsonResult { Result = states };
        }

        public ActionResult GetCities(int countryRegionID, int stateRegionID)
        {
            var regionManager = new RegionManager();
            var cities = regionManager.GetCities(countryRegionID, stateRegionID);
            return new NewtonsoftJsonResult { Result = cities };
        }

        public ActionResult GetCitiesContainingString(int countryRegionID, int stateRegionID, string substring)
        {
            var regionManager = new RegionManager();
            var cities = regionManager.GetCitiesContainingString(countryRegionID, stateRegionID, substring);
            return new NewtonsoftJsonResult { Result = cities };
        }

    }
}
