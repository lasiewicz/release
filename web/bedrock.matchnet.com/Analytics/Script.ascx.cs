#region System References
using System;
using System.Web;
using System.Web.UI;
#endregion

#region Matchnet Web App References
using Matchnet;
#endregion

#region Matchnet Service References
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Content.ServiceAdapters;
#endregion

namespace Matchnet.Web.Analytics
{
	/// <summary>
	///		Matchnet control that generates analytics javascript code.
	/// </summary>
	public class Script : System.Web.UI.UserControl
	{
		public const string ATTR_DOMAIN_ID = "d";		// g.Brand.Site.Community.CommunityID
		public const string ATTR_BASEPRIVATELABEL = "k"; // g.Brand.Site.SiteID
		public const string ATTR_PRIVATELABEL_ID = "l"; // g.Brand.BrandID
		public const string ATTR_MEMBER_ID = "m";		// g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID)
//		public const string ATTR_TARGET_DOMAIN_ID = "td";
//		public const string ATTR_TARGET_PRIVATELABEL_ID = "tl";
//		public const string ATTR_TARGET_MEMBER_ID = "tm";
		public const string ATTR_PAGE_ID = "p";			// g.AppPage.WebPage.PageID
		public const string ATTR_SUBSCRIBED_FLAG = "s";	// MemberPrivilegeAttr.IsCureentSubscribedMember(g)
		public const string ATTR_DISABLE_FLAG = "x";	// g.PrivateLabel.IsFeatureEnabled("Analytics");
		public const string ATTR_NAMES_FLAG = "n";		// g.PrivateLabel.IsFeatureEnabled("AnalyticsNames");
		public const string ATTR_GENDER_MASK = "g";
		public const string ATTR_BIRTH_DATE = "b";

		public const int DISABLED_MASK = 256;			// bit in PrivateLabel.StatusMask

		private const string YNM4_BETA_SUBDOMAIN = "beta";	// This is the subdomain for all YNM4 BETA sites.

		public static new string ID = "Analytics";

		/// <summary>
		/// Return an age-range code based on BirthDate from session.
		/// Ill-formed birthdates, 1/1/1900 and any under 18 return BAD_AGE
		/// </summary>
		/// <returns></returns>
		static DateTime INVALID_DATE_TIME = new DateTime(1900,1,1);
		private static readonly string[] AGE_RANGE = new string [] {
			"N/A",	// 0
			"N/A",	// 1
			"N/A",	// 2
			"18-25", // 3
			"18-25", // 4
			"26-40", // 5
			"26-40", // 6
			"26-40", // 7
			"41-65", // 8
			"41-65", // 9
			"41-65", // 10
			"41-65", // 11
			"41-65", // 12
			"66+",	// 13
			"66+",	// 14
			"66+",	// 15
			"66+",	// 16
			"66+",	// 17
			"66+",	// 18
			"66+"	// 19
		};

		/// <summary>
		/// Generate and age group string for Sage Analytics from member's birthdate
		/// </summary>
		/// <returns>String containing Sage age group</returns>
		private string GetBirthdateCode()
		{
			string strBirthDate = ((IAttributeAccessor)this).GetAttribute(ATTR_BIRTH_DATE);
			if (strBirthDate != null)
			{
				DateTime dtBirthDate;
				try 
				{
					dtBirthDate = System.Convert.ToDateTime(strBirthDate);
				} 
				catch (FormatException) 
				{
					return AGE_RANGE[0];
				}

				int age = Matchnet.Lib.Util.Util.Age(dtBirthDate);
				if (age < 18 || age > 100)
				{
					return AGE_RANGE[0];
				}
				return AGE_RANGE[(int)((age - 1) / 5)];
			}
			else 
			{
				return AGE_RANGE[0];
			}
		}

		/// <summary>
		/// Decrypt the mtid parameter on subscription URLs
		/// </summary>
		/// <param name="encryptedMemberTranID"></param>
		/// <returns></returns>
		private int GetDecryptedValue(string encryptedMemberTranID)
		{
			int memberTranID = 0;
			try
			{
				const string SUBSCRIPTION_ENCRYPT_KEY = "m0nk$ies"; //TODO: Remove this encryption key from the source code
				Matchnet.Lib.Encryption.SymmetricalEncryption  decryptMemberTranID  = 
					new Matchnet.Lib.Encryption.SymmetricalEncryption(Matchnet.Lib.Encryption.SymmetricalEncryption.Provider.DES);
				memberTranID = Matchnet.Conversion.CInt(decryptMemberTranID.Decrypt(encryptedMemberTranID,
					SUBSCRIPTION_ENCRYPT_KEY));
			}
			catch
			{
				//_g.ProcessException(ex);
				memberTranID = 0;
			}

			return memberTranID;
		}

		/// <summary>
		/// Concoct a subscription report style duration code
		/// </summary>
		/// <param name="durationTypeID"></param>
		/// <param name="duration"></param>
		/// <returns></returns>
		private string GetDurationCode(int durationTypeID, int duration)
		{
			const int DURATION_DAY = 3;
			const int DURATION_WEEK = 4;
			const int DURATION_MONTH = 5;
			const int DURATION_YEAR = 6;

			string durationName = duration.ToString();

			switch (durationTypeID)
			{
				case DURATION_DAY:
					durationName = durationName + "D";
					break;
				case DURATION_WEEK:
					durationName = durationName + "W";
					break;
				case DURATION_MONTH:
					durationName = durationName + "M";
					break;
				case DURATION_YEAR:
					durationName = durationName + "Y";
					break;
			}

			return durationName;
		}

		/// <summary>
		/// Spit out some javascript (and HTML comments) that track users for Sage Analytics
		/// </summary>
		/// <param name="htw"></param>
		protected override void Render(HtmlTextWriter htw)
		{
			if (((IAttributeAccessor)this).GetAttribute(ATTR_DISABLE_FLAG) == "1")
				return;
			try
			{
				int domainID = Int32.Parse(((IAttributeAccessor)this).GetAttribute(ATTR_DOMAIN_ID));
				int privateLabelID = Int32.Parse(((IAttributeAccessor)this).GetAttribute(ATTR_PRIVATELABEL_ID));
				int basePrivateLabel = Int32.Parse(((IAttributeAccessor)this).GetAttribute(ATTR_BASEPRIVATELABEL));
				if (basePrivateLabel == Matchnet.Constants.NULL_INT) 
				{
					basePrivateLabel = privateLabelID;
				}

				// FOR YNM4 BETA:  In order to differentiate between the regular site and the YNM4 BETA site, we must
				// examine the URL.
				if (HttpContext.Current.Request.ServerVariables["SERVER_NAME"].Substring(0, YNM4_BETA_SUBDOMAIN.Length).ToLower() == YNM4_BETA_SUBDOMAIN)
				{
					// Make the basePrivateLabel negative if we are on a YNM4 BETA site.
					basePrivateLabel = -basePrivateLabel;
				}

				// eventually these should go in the PrivateLabel table and a stored proc written to retrieve them
				// (or at least put them in a Dictionary)
				string strPrivateLabelDomain = "";
				string diCode = ""; // domain info
				switch (basePrivateLabel)
				{
					case 1:
					case 101: diCode = "d001"; strPrivateLabelDomain = "AmericanSingles.com"; break;
					case 2:
					case 102: diCode = "d005"; strPrivateLabelDomain = "Glimpse.com"; break;
					case 3:
					case 103: diCode = "d002"; strPrivateLabelDomain = "JDate.com"; break;
					case 4: diCode = "d003"; strPrivateLabelDomain = "JDate.co.il"; break;
					case 5: diCode = "d009"; strPrivateLabelDomain = "Matchnet.de"; break;
					case 6: diCode = "d010"; strPrivateLabelDomain = "Matchnet.co.uk"; break;
					case 7: diCode = "d011"; strPrivateLabelDomain = "Matchnet.com.au"; break;
					case 8:
					case 108: diCode = "d006"; strPrivateLabelDomain = "Matchnet.com"; break;
					case 9:
					case 109: diCode = "d007"; strPrivateLabelDomain = "Facelink.com"; break;
					case 12:
					case 112: diCode = "d008"; strPrivateLabelDomain = "CollegeLuv.com"; break;
					case 13: diCode = "d012"; strPrivateLabelDomain = "Date.ca"; break;
					case 15: diCode = "d004"; strPrivateLabelDomain = "Cupid.co.il"; break;
					case 1840: diCode = "d013"; strPrivateLabelDomain = "JDate.msn.co.il"; break;
					case 1934: diCode = "d014"; strPrivateLabelDomain = "DatingResults.com"; break;
					case 1954: diCode = "d015"; strPrivateLabelDomain = "JDate.be"; break;
					case 1955: diCode = "d016"; strPrivateLabelDomain = "JDate.com.ar"; break;
					case 1985: diCode = "d017"; strPrivateLabelDomain = "JDate.br"; break;
					// YNM4 BETA Sites.
					case -1: diCode = "d001"; strPrivateLabelDomain = "beta.AmericanSingles.com"; break;
					case -3: diCode = "d002"; strPrivateLabelDomain = "beta.JDate.com"; break;
					// NOTE: this default case requires that Sage not filter di codes based on domain name,
					// since it will lump unknown private label traffic in the american singles bucket.
					default: diCode = "d001"; strPrivateLabelDomain = "DOMAIN" + basePrivateLabel.ToString(); break;
				}

				// want to put more info here about the page -- search results keys, profile being viewed, etc.
				int pageID = Int32.Parse(((IAttributeAccessor)this).GetAttribute(ATTR_PAGE_ID));
				int memberID = Int32.Parse(((IAttributeAccessor)this).GetAttribute(ATTR_MEMBER_ID));
				//int memberFlag = (memberID != 0) ? 1 : 0;

				string aiParams;
				if (((IAttributeAccessor)this).GetAttribute(ATTR_NAMES_FLAG) == "1") 
				{
					aiParams =	"m:" + Convert.ToString(memberID) + 
								",s:" + Convert.ToString(((IAttributeAccessor)this).GetAttribute(ATTR_SUBSCRIBED_FLAG)) + 
								",p:" + Convert.ToString(pageID) +
								",n:" + strPrivateLabelDomain + 
								"/" + PageConfigSA.Instance.GetAnalyticsName(pageID) + 
								",g:" + ((IAttributeAccessor)this).GetAttribute(ATTR_GENDER_MASK) + 
								",b:" + GetBirthdateCode();
				}
				else
				{
					aiParams =	"m:" + Convert.ToString(memberID) + 
								",s:" + Convert.ToString(((IAttributeAccessor)this).GetAttribute(ATTR_SUBSCRIBED_FLAG)) + 
								",p:" + Convert.ToString(pageID);
				}


				string aiEncoded = HttpUtility.UrlEncode(aiParams);

			//	htw.Write("\n<!--BEGIN-SMB-1.2.3.744."); htw.Write(diCode); htw.Write("-->\n");
				//htw.Write("<script language=\"JavaScript\">var st_pg=\"\"; var st_ai=\""); htw.Write(aiParams);
				//htw.Write("\"; var st_v=1.0; var st_ci=\"744\"; var st_di=\""); htw.Write(diCode);
				//htw.Write("\"; var st_dd=\"st.sageanalyst.net\"; var st_tai=\"v:1.2.3\";</script>\n");
				//htw.Write("<script language=\"JavaScript1.1\">st_v=1.1;</script><script language=\"JavaScript1.2\">st_v=1.2;</script>\n");

				//RenderSageInclude(htw);

			//	htw.Write("<script language=\"JavaScript\">if (st_v==1.0) { var st_dn=(new Date()).getTime(); var st_rf=escape(document.referrer);\n");
				//htw.Write(" var st_uj=\"//\"+st_dd+\"/\"+st_dn+\"/JS?ci=\"+st_ci+\"&di=\"+st_di+\"&pg=\"+st_pg+\"&rf=\"+st_rf+\"&jv=\"+st_v+\"&tai=\"+st_tai+\"&ai=\"+st_ai;\n");
				//htw.Write(" var iXz=new Image(); iXz.src=st_uj;\n"); // add comment in javascript code here to disable image fetch
				//htw.Write("}</script><noscript><img src=\"//st.sageanalyst.net/NS?ci=744&di="); htw.Write(diCode);
				//htw.Write("&pg=&ai="); htw.Write(aiEncoded); htw.Write("\"></noscript>\n");
			//	htw.Write("<!--END-SMB-1.2.3.744."); htw.Write(diCode); htw.Write("-->");
				//htw.Write("<!--["); htw.Write(aiParams); htw.Write("]-->\n");
			}
			catch(Exception ex)
			{
				// TODO: security level prevents handling this exception!??
				//g.ProcessException(ex);
				htw.Write("\n<!--{0}-->\n", HttpUtility.HtmlEncode(ex.Message));
			}
		}


		/// <summary>
		/// Render the Sage javascript include as an include, or inline
		/// </summary>
		/// <param name="htw"></param>
		void RenderSageInclude(HtmlTextWriter htw)
		{
			htw.WriteLine("<!-- SERVER_PORT: " + HttpContext.Current.Request.ServerVariables["SERVER_PORT"] + " --->");
			if ( ((IAttributeAccessor)this).GetAttribute(ATTR_NAMES_FLAG) == "1"
				&& HttpContext.Current.Request.ServerVariables["SERVER_PORT"] != "443") 
			{
				//htw.WriteLine("<script language=\"JavaScript1.1\" src=\"http://matchnet.st.sageanalyst.net/javascript/tag-744.aspx\"></script>");
			}
			else
			{
				htw.WriteLine("<script language=\"JavaScript1.1\">var st_iu=st_rs=st_cd=st_rf=st_us=st_hn=st_qs=st_ce=\"\";");
				htw.WriteLine("var d=document;var l=location;var n=navigator;");
				htw.WriteLine("st_ce=n.cookieEnabled;st_qs=escape(l.search.substring(1));st_us=escape(l.pathname);st_hn=l.host;st_rf=escape(d.referrer);");
				htw.WriteLine("if(st_v==1.2){var s=screen;st_rs=escape(s.width+\"x\"+s.height);st_cd=s.colorDepth;}");
				htw.WriteLine("proto=\"http:\";if(location.protocol==\"https:\"){proto=\"https:\";}");
				htw.WriteLine("st_iu=proto+\"//\"+st_dd+\"/\" + (new Date()).getTime() + \"/JS?ci=\"+st_ci+\"&di=\"+st_di+\"&pg=\"+st_pg+\"&us=\"+st_us+\"&qs=\"");
				htw.WriteLine(" +st_qs+\"&rf=\"+st_rf+\"&rs=\"+st_rs+\"&cd=\"+st_cd+\"&hn=\"+st_hn+\"&ce=\"+st_ce+\"&jv=\"+st_v+\"&tai=\"+st_tai+\"&ai=\"+st_ai;");
				htw.WriteLine("var iXz=new Image();iXz.src=st_iu;");
				htw.WriteLine("</script>");
			}
		}


		private void Page_Load(object sender, System.EventArgs e)
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += (new System.EventHandler(this.Page_Load));
		}
		#endregion
	}

}
