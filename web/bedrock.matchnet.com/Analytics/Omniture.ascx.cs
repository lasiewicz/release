using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Analytics
{
    using System;
    using System.Text;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using Matchnet.Web.Framework;
    using Matchnet.Web.Applications;
    using Matchnet.Web.Framework.Ui;
    using Matchnet.Purchase.ServiceAdapters;
    using Matchnet.Purchase.ValueObjects;
    using Matchnet.Member.ServiceAdapters;
    using Matchnet.Member.ValueObjects;
    using Matchnet.MatchTest.ValueObjects.MatchMeter;
    using Matchnet.MatchTest.ServiceAdapters;

    using Matchnet.OmnitureHelper;
    using Microsoft.Security.Application;
    using Matchnet.Configuration.ServiceAdapters;
    using h1 = Matchnet.HTTPMessaging;
    using Matchnet.Web.Applications.ColorCode;
    using System.Web.Caching;
    using Matchnet.Web.Applications.QuestionAnswer;
    using Matchnet.Web.Framework.Managers;
    using Matchnet.Web.Framework.Util;

    /// <summary>
    /// 
    /// Extended version of the Ominiture POC project. All the variables are hard coded in code behinds and cannot be changed dynamically.
    /// Most variables are retrieved and tracked inside OnPreRender method of the pages. 
    /// These variables register themselves to context.AnalyticsOmniture which is an instance of this class.
    /// That instance is then rendered during the load of layouttemplates.
    /// 
    /// Won 12/2007
    /// 
    /// Omniture Catalyst web application - my.omniture.com 
    /// Modifications:
    ///					3/31/2008- Added Evar4 for PromoID-	RB
    ///					mike cho is so so so so awesome
    ///					But Jay RHO thinks Mike CHO   SUX
    /// </summary>
    public class Omniture : Framework.FrameworkControl
    {
        protected System.Web.UI.WebControls.PlaceHolder PlaceHolderOmniture;
        protected System.Web.UI.WebControls.PlaceHolder PlaceHolderMbox;

        const string topnav_tracking_format = "TopNav - {0}";
        const string subnav_tracking_format = "SubNav - {0}";
        public const string OMNITURE_ALIAS_SESSION_KEY = "OMNITURE_ALIAS";
        string _prevTrackedPage = "";
        #region Private Variables

        private string _javascriptFileName = Constants.NULL_STRING;

        private string s_AccountName = Constants.NULL_STRING;
        private string s_LinkInternalFilters = Constants.NULL_STRING;

        // Traffic Variables
        private string _pageName = Constants.NULL_STRING;
        private string _server = Constants.NULL_STRING;
        private string _tempPageNameSuffix = "";

        public string GetTNTVesrionSet()
        {
            string result = string.Empty;
            if (g.AppPage.ID == (int)WebConstants.PageIDs.Splash ||
                g.AppPage.ID == (int)WebConstants.PageIDs.Splash20)
            {
                result = "if (window.tntVersion) { s.eVar16 = (clearValue) ? \"\" : tntVersion; } else { s.eVar16 = (clearValue) ? \"\" : \"\"; } ";
            }
            return result;
        }

        public string GetTNTVersionSetMBox()
        {
            string result = string.Empty;
            if (g.AppPage.ID == (int)WebConstants.PageIDs.Splash ||
                g.AppPage.ID == (int)WebConstants.PageIDs.Splash20)
            {
                // for spark.com, we have multiple mboxes on the splash page, so disble this
                if (g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.AmericanSingles)
                    result = "<div class=\"mboxDefault\"><div class=\"hide\"></div></div>\n<script type=\"text/javascript\">mboxCreate(\"" + g.Brand.Site.Name.ToLower().Replace(".", string.Empty) + "_tnt_page_name\");</script>";
            }

            return result;
        }



        public string PageNameEncoded
        {
            get { return HttpContext.Current.Server.UrlEncode(_pageName); }
        }
        /// <summary>
        /// Viewed member premium service
        /// </summary>
        private string _prop1 = Constants.NULL_STRING;
        /// <summary>
        /// First page view time
        /// </summary>
        private string _prop9 = Constants.NULL_STRING;
        /// <summary>
        /// URL
        /// Member subscription status
        /// </summary>
        private string _prop10 = Constants.NULL_STRING;
        /// <summary>
        /// UI Redesign Top Nav actions
        /// </summary>
        private string _prop14 = Constants.NULL_STRING;
        /// Profile Completeness
        /// </summary>
        private string _prop17 = Constants.NULL_STRING;
        /// <summary>
        /// Gender
        /// </summary>
        private string _prop18 = Constants.NULL_STRING;
        /// <summary>
        /// Age
        /// </summary>
        private string _prop19 = Constants.NULL_STRING;
        /// <summary>
        /// Ethnicity
        /// </summary>
        private string _prop20 = Constants.NULL_STRING;
        /// <summary>
        /// Location
        /// </summary>
        private string _prop21 = Constants.NULL_STRING;
        /// <summary>
        /// Subscriber Status
        /// </summary>
        private string _prop22 = Constants.NULL_STRING;
        /// <summary>
        /// MemberID
        /// </summary>
        private string _prop23 = Constants.NULL_STRING;
        /// <summary>
        /// Profile Name
        /// </summary>
        private string _prop24 = Constants.NULL_STRING;
        /// <summary>
        /// Email verified "Not verified"
        private string _prop4 = Constants.NULL_STRING;
        /// <summary>
        /// <summary>

        /// <summary>
        /// Jmeter status
        /// </summary>
        private string _prop7 = Constants.NULL_STRING;
        /// <summary>
        /// Force page version
        /// </summary>
        private string _prop8 = Constants.NULL_STRING;
        /// <summary>
        /// JRewards status
        /// </summary>
        private string _prop27 = Constants.NULL_STRING;

        /// <summary>
        /// Color Code Test complete
        /// </summary>
        private string _prop6 = Constants.NULL_STRING;

        /// <summary>
        /// Slideshow YNM advance
        /// </summary>
        private string _prop30 = Constants.NULL_STRING;

        /// <summary>
        /// Slideshow id of member viewed
        /// </summary>
        private string _prop31 = Constants.NULL_STRING;

        /// <summary>
        /// Hot Lists Activity on home page
        /// </summary>
        private string _prop32 = Constants.NULL_STRING;

        /// <summary>
        /// Your Matches on home page
        /// </summary>
        private string _prop33 = Constants.NULL_STRING;

        /// <summary>
        /// Target memberid
        /// </summary>
        private string _prop35 = Constants.NULL_STRING;

        /// <summary>
        /// Total count of favorites online
        /// </summary>
        private string _prop36 = Constants.NULL_STRING;
        /// URL referrer
        /// </summary>
        private string _prop29 = Constants.NULL_STRING;

        private string _prop38 = Constants.NULL_STRING;

        /// <summary>
        /// Beta testing
        /// </summary>
        private string _prop40 = Constants.NULL_STRING;
        /// <summary>
        /// Sort order Changed
        /// </summary>
        private string _prop45 = Constants.NULL_STRING;
        /// <summary>
        /// Contact History View
        /// </summary>
        private string _prop46 = Constants.NULL_STRING;
        /// <summary>
        /// Force Page Result
        /// </summary>
        private string _prop55 = Constants.NULL_STRING;
        /// <summary>
        /// Subscription Expiration Alert
        /// </summary>
        private string _prop75 = Constants.NULL_STRING;
        /// <summary>
        /// Events
        /// </summary>
        private string _events = Constants.NULL_STRING;

        /// <summary>
        /// Viewed member premium service
        /// </summary>
        private string _evar1 = Constants.NULL_STRING;
        /// <summary>
        /// Page name
        /// </summary>
        private string _evar2 = Constants.NULL_STRING;
        /// <summary>
        /// Subscription Plans
        /// </summary>
        private string _evar3 = Constants.NULL_STRING;
        /// <summary>
        /// PromotionID for subscription plans (PromoEngine)	
        /// </summary>
        private string _evar4 = Constants.NULL_STRING;
        /// <summary>
        /// PurchaseReasonTypeID for subscriptions 
        /// </summary>
        private string _evar6 = Constants.NULL_STRING;
        /// <summary>
        /// More detailed member subscription types	
        /// </summary>
        private string _evar8 = Constants.NULL_STRING;
        /// <summary>
        /// Member profile tab	
        /// </summary>
        private string _evar9 = Constants.NULL_STRING;
        /// <summary>
        /// Member subscription status	
        /// </summary>
        private string _evar10 = Constants.NULL_STRING;
        /// <summary>
        /// Quick Search Box 
        /// </summary>
        private string _evar14 = Constants.NULL_STRING;
        /// <summary>
        /// Search Results Depth
        /// </summary>
        private string _evar25 = Constants.NULL_STRING;
        /// <summary>
        /// Message Type
        /// </summary>
        private string _evar26 = Constants.NULL_STRING;
        /// <summary>
        /// </summary>
        /// Tracking A/B layout scenarios
        private string _evar16 = Constants.NULL_STRING;
        /// <summary>
        /// Fraud Pledge
        /// </summary>
        private string _evar17 = Constants.NULL_STRING;
        /// <summary>
        /// Products
        /// Tracking actions
        private string _evar27 = Constants.NULL_STRING;
        /// Products
        /// </summary>
        private string _products = Constants.NULL_STRING;
        /// <summary>
        /// PurchaseID
        /// </summary>
        private string _purchaseID = Constants.NULL_STRING;
        /// <summary>
        /// ViewProfile Popup vs NonPopup
        /// </summary>
        private string _evar5 = Constants.NULL_STRING;
        /// <summary>
        /// Registration or login from Q/A Gate Page
        /// </summary>
        private string _evar20 = Constants.NULL_STRING;
        //Forced email variables
        /// <summary>
        /// Email verified "Registed"
        /// </summary>
        private string _evar22 = Constants.NULL_STRING;
        /// <summary>
        /// 	/// <summary>
        /// Email verified "Not Verified"
        /// QuestionID for when member answers a question
        /// </summary>
        private string _evar15 = Constants.NULL_STRING;

        /// Email action traqcking
        /// </summary>
        private string _evar28 = Constants.NULL_STRING;
        private string _evar31 = Constants.NULL_STRING;

        /// <summary>
        /// Logged in user admin Y/N
        /// </summary>
        private string _evar29 = Constants.NULL_STRING;

        /// <summary>
        /// Member Profile Page Viewee's Color Code - Color
        /// </summary>
        private string _evar33 = Constants.NULL_STRING;

        /// <summary>
        /// Recipient color code on message sent (MessageSent.aspx)
        /// </summary>
        private string _evar34 = Constants.NULL_STRING;

        /// <summary>
        ///  Search Type
        /// </summary>
        private string _evar41 = Constants.NULL_STRING;

        /// <summary>
        private string _evar43 = Constants.NULL_STRING;
        /// Last digit of member ID mod10 of member ID
        /// </summary>
        private string _evar44 = Constants.NULL_STRING;
        private string _evar46 = Constants.NULL_STRING;
        private string _evar47 = Constants.NULL_STRING;
        private string _evar48 = Constants.NULL_STRING;
        private string _evar40 = Constants.NULL_STRING;
        /// Tracking # photos
        private string _evar32 = Constants.NULL_STRING;
        /// <summary>
        /// JDate Mobile status
        /// </summary>
        private string _evar36 = Constants.NULL_STRING;
        /// <summary>
        /// Mobile Setup Errors
        /// </summary>
        private string _evar35 = Constants.NULL_STRING;
        /// <summary>
        /// Mobile Setup Source
        /// </summary>
        private string _evar42 = Constants.NULL_STRING;
        /// <summary>
        /// Days since registration
        /// </summary>
        private string _evar45 = Constants.NULL_STRING;
        /// <summary>
        /// Edit Preferences
        /// </summary>
        private string _evar50 = Constants.NULL_STRING;
        /// <summary>
        /// Facebook Connect Statuses
        /// </summary>
        private string _evar55 = Constants.NULL_STRING;
        /// <summary>
        /// Viewing profile with Facebook data: Y or N
        /// </summary>
        private string _evar56 = Constants.NULL_STRING;
        /// <summary>
        /// Facebook prompt message: "FB_Connect_Prompt" or "FB_Connect_Close"
        /// </summary>
        private string _evar57 = Constants.NULL_STRING;
        /// <summary>
        /// Mutual Match Proof of Concept
        /// </summary>
        private string _evar64 = Constants.NULL_STRING;
        /// <summary>
        /// Profile No Essay Request email feature
        /// </summary>
        private string _evar65 = Constants.NULL_STRING;
        /// <summary>
        /// Photo Gallery Sort By
        /// </summary>
        private string _evar67 = Constants.NULL_STRING;

        private string _prtid = string.Empty;

        bool _demographicsAlreadySet = false;

        private string _referrer = Constants.NULL_STRING;
        private string _prm = Constants.NULL_STRING;
        private string _eid = string.Empty;
        
        #endregion

        #region Public Variables

        #region Straight from Request parms

        public string LGID
        {
            get
            {
                string lgid = string.Empty;
                if (!string.IsNullOrEmpty(Request["lgid"]))
                {
                    lgid = JavaScriptEncode(Request["lgid"]);
                }
                else if (!string.IsNullOrEmpty(Request["LGID"]))
                {
                    lgid = JavaScriptEncode(Request["LGID"]);
                }
                else if (!string.IsNullOrEmpty(Request["Luggage"]))
                {
                    lgid = JavaScriptEncode(Request["Luggage"]);
                }

                return (lgid);
            }
        }

        public string EID
        {
            get
            {
                string eid = string.Empty;
                if (string.IsNullOrEmpty(_eid))
                {
                    if (!string.IsNullOrEmpty(g.Session[WebConstants.SESSION_PROPERTY_NAME_EID]))
                    {
                        eid = g.Session[WebConstants.SESSION_PROPERTY_NAME_EID];
                    }
                }

                return JavaScriptEncode(eid);
            }
        }

        public string PRM
        {
            get
            {
                if (String.IsNullOrEmpty(_prm))
                    return JavaScriptEncode(Request["prm"]);
                else
                    return _prm;
            }
            set
            {
                _prm = value;
            }
        }

        public string PRT_ID
        {
            set { _prtid = value; }
            
            get
            {

                if (string.IsNullOrEmpty(_prtid))
                {
                    return JavaScriptEncode(Request["prtid"]);
                }
                else
                {
                    return JavaScriptEncode(_prtid);
                }
            }
        }

        public string SR_ID
        {
            get
            {
                return JavaScriptEncode(Request["srid"]);
            }
        }

        public string LP_ID
        {
            get
            {
                string lpid = string.Empty;

                if(Request["lpid"] != null)
                {
                    lpid = JavaScriptEncode(Request["lpid"]);
                }
                
                if(g.Session[WebConstants.SESSION_PROPERTY_NAME_LANDINGPAGETESTID] != null)
                {
                    lpid = g.Session[WebConstants.SESSION_PROPERTY_NAME_LANDINGPAGETESTID];
                }

                return lpid;
            }
        }

        public string TB_Link
        {
            get
            {
                return JavaScriptEncode(Request["tb_link"]);
            }
        }
        #endregion

        /// <summary>
        /// This determines whether Omniture should be rendered or not at all
        /// </summary>
        public bool Render { get; set; }

        public string S_AccountName
        {
            get
            {
                return s_AccountName;
            }
            set
            {
                s_AccountName = value;
            }
        }
        public string S_LinkInternalFilters
        {
            get
            {
                return s_LinkInternalFilters;
            }
            set
            {
                s_LinkInternalFilters = value;
            }
        }
        public string PageName
        {
            get
            {
                return _pageName;
            }
            set
            {
                _pageName = value;
            }
        }

        public string TempPageNameSuffix
        {
            get { return _tempPageNameSuffix; }
            set { _tempPageNameSuffix = value; }
        }

        public string Server
        {
            get
            {
                return _server;
            }
            set
            {
                _server = value;
            }
        }

        public string Referrer
        {
            get
            {
                return JavaScriptEncode(_referrer);
            }
            set
            {
                _referrer = value;
            }
        }
        public string PageURL { get; set; }

        public string JavascriptFileName
        {
            get
            {
                return _javascriptFileName;
            }
            set
            {
                _javascriptFileName = value;
            }
        }

        /// <summary>
        /// Viewed member premium service
        /// </summary>
        public string Prop1
        {
            get
            {
                return _prop1;
            }
            set
            {
                _prop1 = value;
            }
        }
        /// <summary>
        /// Force Page version
        /// </summary>
        public string Prop8
        {
            get
            {
                return _prop8;
            }
            set
            {
                _prop8 = value;
            }
        }
        /// <summary>
        /// First Page view time
        /// </summary>
        public string Prop9
        {
            get
            {
                return _prop9;
            }
            set
            {
                _prop9 = value;
            }
        }
        /// <summary>
        /// URL
        /// Member subscription status
        /// </summary>
        public string Prop10
        {
            get
            {
                return JavaScriptEncode(_prop10);
            }
            set
            {
                _prop10 = value;
            }
        }

        /// Top Nav Actions
        /// </summary>
        public string Prop14
        {
            get
            {
                return JavaScriptEncode(_prop14);
            }
            set
            {
                _prop14 = value;
            }
        }
        /// <summary>
        /// Profile Completeness
        /// </summary>
        public string Prop17
        {
            get
            {
                return _prop17;
            }
            set
            {
                _prop17 = value;
            }
        }

        /// <summary>
        /// Gender
        /// </summary>
        public string Prop18
        {
            get
            {
                return _prop18;
            }
            set
            {
                _prop18 = value;
            }
        }
        /// <summary>
        /// Age
        /// </summary>
        public string Prop19
        {
            get
            {
                return this._prop19;
            }
            set
            {
                _prop19 = value;
            }
        }

        /// <summary>
        /// Ethnicity
        /// </summary>
        public string Prop20
        {
            get
            {
                return _prop20;
            }
            set
            {
                _prop20 = value;
            }
        }

        /// <summary>
        /// Location
        /// </summary>
        public string Prop21
        {
            get
            {
                return _prop21;
            }
            set
            {
                _prop21 = value;
            }
        }
        /// <summary>
        /// Subscriber Status
        /// </summary>
        public string Prop22
        {
            get
            {
                return _prop22;
            }
            set
            {
                _prop22 = value;
            }
        }
        /// <summary>
        /// Member ID
        /// </summary>
        public string Prop23
        {
            get
            {
                return _prop23;
            }
            set
            {
                _prop23 = value;
            }
        }
        /// <summary>
        /// Profile Name
        /// </summary>
        public string Prop24
        {
            get
            {
                return _prop24;
            }
            set
            {
                _prop24 = value;
            }
        }
        /// <summary>
        /// 
        ///	/// Forced email - "unverified"
        /// </summary>
        public string Prop4
        {
            get
            {
                return _prop4;
            }
            set
            {
                _prop4 = value;
            }
        }

        /// <summary>
        ///	Jmeter status
        /// </summary>
        public string Prop7
        {
            get
            {
                return _prop7;
            }
            set
            {
                _prop7 = value;
            }
        }
        /// <summary>
        /// JRewards status
        /// </summary>
        public string Prop27
        {
            get
            {
                return _prop27;
            }
            set
            {
                _prop27 = value;
            }
        }
        /// <summary>
        /// JRewards status
        /// </summary>
        public string Prop29
        {
            get
            {
                return _prop29;
            }
            set
            {
                _prop29 = value;
            }
        }
        /// <summary>
        /// Color Code Test complete
        /// </summary>
        public string Prop6
        {
            get { return _prop6; }
            set { _prop6 = value; }
        }

        /// <summary>
        /// Slideshow YNM advance
        /// </summary>
        public string Prop30
        {
            get { return _prop30; }
            set { _prop30 = value; }
        }

        /// <summary>
        /// Slideshow memberid being viewed
        /// </summary>
        public string Prop31
        {
            get { return _prop31; }
            set { _prop31 = value; }
        }

        /// <summary>
        /// Hot Lists Activity on home page
        /// </summary>
        public string Prop32
        {
            get { return _prop32; }
            set { _prop32 = value; }
        }

        /// <summary>
        /// Your Matches on home page
        /// </summary>
        public string Prop33
        {
            get { return _prop33; }
            set { _prop33 = value; }
        }

        /// <summary>
        /// Target MemberID
        /// </summary>
        public string Prop35
        {
            get { return _prop35; }
            set { _prop35 = value; }
        }


        /// <summary>
        /// Total count of favorites online
        /// </summary>
        public string Prop36
        {
            get { return _prop36; }
            set { _prop36 = value; }
        }

        public string Prop38
        {
            get { return _prop38; }
            set { _prop38 = value; }
        }

        public string Prop40
        {
            get { return _prop40; }
            set { _prop40 = value; }
        }

        public string Prop45
        {
            get { return _prop45; }
            set { _prop45 = value; }
        }

        public string Prop46
        {
            get { return _prop46; }
            set { _prop46 = value; }
        }
        public string Prop55
        {
            get { return _prop55; }
            set { _prop55 = value; }
        }
        public string Prop75
        {
            get { return _prop75; }
            set { _prop75 = value; }
        }

        /// </summary>
        /// Events
        /// </summary>
        public new string Events
        {
            get
            {
                return _events;
            }
            set
            {
                _events = value;
            }
        }

        /// <summary>
        /// Products
        /// </summary>
        public string Products
        {
            get
            {
                return _products;
            }
            set
            {
                _products = value;
            }
        }
        /// <summary>
        /// PurchaseID
        /// </summary>
        public string PurchaseID
        {
            get
            {
                return _purchaseID;
            }
            set
            {
                _purchaseID = value;
            }
        }

        /// <summary>
        /// Viewed member premium service
        /// </summary>
        public string Evar1
        {
            get
            {
                return _evar1;
            }
            set
            {
                _evar1 = value;
            }
        }
        public string Evar2
        {
            get
            {
                return _evar2;
            }
            set
            {
                _evar2 = value;
            }
        }
        public string Evar3
        {
            get
            {
                return _evar3;
            }
            set
            {
                _evar3 = value;
            }
        }
        /// More detailed member subscription types
        /// </summary>
        public string Evar8
        {
            get
            {
                return _evar8;
            }
            set
            {
                _evar8 = value;
            }
        }
        /// <summary>
        /// Tracks profile tabs
        /// </summary>
        public string Evar9
        {
            get
            {
                return _evar9;
            }
            set
            {
                _evar9 = value;
            }
        }
        /// <summary>
        /// Member subscription status
        /// </summary>
        public string Evar10
        {
            get
            {
                return _evar10;
            }
            set
            {
                _evar10 = value;
            }
        }
        public string Evar4
        {
            get
            {
                return _evar4;
            }
            set
            {
                _evar4 = value;
            }
        }
        public string Evar6
        {
            get
            {
                return _evar6;
            }
            set
            {
                _evar6 = value;
            }
        }
        /// <summary>
        /// Registration or login from Q/A Gate Page
        /// </summary>
        public string Evar20
        {
            get { return _evar20; }
            set { _evar20 = value; }
        }
        /// <summary>
        /// Search Results Depth
        /// </summary>
        public string Evar25
        {
            get
            {
                return _evar25;
            }
            set
            {
                _evar25 = value;
            }
        }

        /// <summary>
        /// Message Type
        /// </summary>
        public string Evar26
        {
            get
            {
                return _evar26;
            }
            set
            {
                _evar26 = value;
            }
        }

        /// <summary>
        /// A/B redesign scenario
        /// </summary>
        public string Evar16
        {
            get
            {
                return _evar16;
            }
            set
            {
                _evar16 = value;
            }
        }
        public string Evar17
        {
            get
            {
                return _evar17;
            }
            set
            {
                _evar17 = value;
            }
        }
        public string Evar14
        {
            get
            {
                return _evar14;
            }
            set
            {
                _evar14 = value;
            }
        }
        /// <summary>
        /// ViewProfile popup vs nonpopup
        /// </summary>
        public string Evar5
        {
            get
            {
                return _evar5;
            }
            set
            {
                _evar5 = value;
            }
        }

        /// Forced email "registered"
        /// </summary>
        public string Evar22
        {
            get
            {
                return _evar22;
            }
            set
            {
                _evar22 = value;
            }
        }
        /// Forced email "registered"
        /// </summary>
        public string Evar15
        {
            get
            {
                return _evar15;
            }
            set
            {
                _evar15 = value;
            }
        }

        public string Evar27
        {
            get
            {
                return _evar27;
            }
            set
            {
                _evar27 = value;
            }
        }

        /// Email action tracking
        /// </summary>
        public string Evar28
        {
            get
            {
                return _evar28;
            }
            set
            {
                _evar28 = value;
            }
        }
        public string Evar31
        {
            get
            {
                return _evar31;
            }
            set
            {
                _evar31 = value;
            }
        }
        /// <summary>
        /// Logged in user admin Y/N
        /// </summary>
        public string Evar29
        {
            get
            {
                return _evar29;
            }
            set
            {
                _evar29 = value;
            }
        }

        /// <summary>
        /// Member Profile Page Viewee's Color code - color
        /// </summary>
        public string Evar33
        {
            get { return _evar33; }
            set { _evar33 = value; }
        }

        /// <summary>
        /// Recipient Color on message sent
        /// </summary>
        public string Evar34
        {
            get { return _evar34; }
            set { _evar34 = value; }
        }

        public string Evar41
        {
            get
            {
                return _evar41;
            }
            set
            {
                _evar41 = value;
            }
        }

        public string Evar43
        {
            get
            {
                return _evar43;
            }
            set
            {
                _evar43 = value;
            }
        }
        /// <summary>
        /// Last digit of member ID mod10 of member ID
        /// </summary>
        public string Evar44
        {
            get
            {
                return _evar44;
            }
            set
            {
                _evar44 = value;
            }
        }

        /// <summary>
        /// Days since registration
        /// </summary>
        public string Evar45
        {
            get
            {
                return _evar45;
            }
            set
            {
                _evar45 = value;
            }
        }



        /// <summary>
        /// Indicates if the a subscription is one-click or traditional
        /// </summary>
        public string Evar46
        {
            get
            {
                return _evar46;
            }
            set
            {
                _evar46 = value;
            }
        }
        public string Evar47
        {
            get
            {
                return _evar47;
            }
            set
            {
                _evar47 = value;
            }
        }
        public string Evar48
        {
            get
            {
                return _evar48;
            }
            set
            {
                _evar48 = value;
            }
        }

        public string Evar40
        {
            get
            {
                return _evar40;
            }
            set
            {
                _evar40 = value;
            }
        }


        /// <summary>
        /// Approved photos
        /// </summary>
        public string Evar32
        {
            get
            {
                return _evar32;
            }
            set
            {
                _evar32 = value;
            }
        }
        /// <summary>
        /// Site name (www. for fws, m. for mobile) 
        /// </summary>
        public string Evar36
        {
            get { return _evar36; }
            set { _evar36 = value; }
        }
        /// <summary>
        /// Mobile Setup Errors
        /// </summary>
        public string Evar35
        {
            get { return _evar35; }
            set { _evar35 = value; }
        }
        /// <summary>
        /// Mobile Setup Source
        /// </summary>
        public string Evar42
        {
            get { return _evar42; }
            set { _evar42 = value; }
        }
        public string Evar50
        {
            get { return _evar50; }
            set { _evar50 = value; }
        }
        public string Evar55
        {
            get { return _evar55; }
            set { _evar55 = value; }
        }
        public string Evar56
        {
            get { return _evar56; }
            set { _evar56 = value; }
        }
        public string Evar57
        {
            get { return _evar57; }
            set { _evar57 = value; }
        }
        public string Evar64
        {
            get { return _evar64; }
            set { _evar64 = value; }
        }
        public string Evar65
        {
            get { return _evar65; }
            set { _evar65 = value; }
        }
        public string Evar67
        {
            get { return _evar67; }
            set { _evar67 = value; }
        }
        public bool DisableFireOmnitureCode { get; set; }

        #endregion

        public Omniture()
        {
            this.ID = "Omniture";
            Render = true;
        }

        #region Event Methods
        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {

                _javascriptFileName = "Omniture.js";

                // Single Omniture Javascript file is used for all sites. s_AccountName variable name declared at the very top to make use
                // of dynamically generated values.
                s_AccountName = RuntimeSettings.GetSetting("OMNITURE_ACCOUNT_NAME", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                if(string.IsNullOrEmpty(s_AccountName))
                {
                    s_AccountName = "spark" + _g.Brand.Site.Name.Replace(".", string.Empty);
                }

                if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID))))
                {
                    s_LinkInternalFilters = "javascript:," + _g.Brand.Site.Name + ",secure.spark.net";
                }
                else
                {
                    s_LinkInternalFilters = "javascript:," + _g.Brand.Site.Name;
                }


                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate && _g.Session.GetString("OmnitureFirstPageLoadTime") != "true")
                {
                    Prop9 = String.Format("{0:yyyy-MM-dd HH:mm}", DateTime.Now);
                    _g.Session.Add("OmnitureFirstPageLoadTime", "true", SessionPropertyLifetime.Temporary);
                }

                //site name
                Evar36 = "www." + _g.Brand.Uri;


                // Adding in constant prefix to avoid conflicts in resource key names.
                // There are two issues with getting unique site page names.			

                // try not to use the resource file.
                //string pageName = _g.GetResource("PAGE_" + g.AppPage.ResourceConstant, this);
                string pageName = g.AppPage.ControlName;

                if (g.AppPage.ResourceConstant == string.Empty)
                {
                    pageName = g.AppPage.ControlName;
                }

                // Following call uses resource names of web applications defined in the global resx of AS. This provides consistent and readable
                // application names in English for reporting. This call required changes to the Localizer methods to use Site object's ID and
                // culture info instead of using the current thread's culture info.
                string appName = Matchnet.Web.Framework.Globalization.Localizer.GetStringResource(g.AppPage.App.ResourceConstant, null,
                    Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(1001), _g.ExpansionTokens);

                #region Page & App name manual overrides per product requests.

                // There are pages that use the same site page name - Splash can be splash or home.
                if ((_g.GetResource("APP_" + g.AppPage.App.ResourceConstant, this) == "Home") & (_g.Session.GetInt(Matchnet.Web.Framework.WebConstants.SESSION_PROPERTY_NAME_MEMBERID, 0) > 0))
                {
                    pageName = "Default";
                }

                // Need to differentiate initial Registration and EditProfile. RegistrationMode() call does not work for Reg1.
                if ((_g.AppPage.ControlName.IndexOf("RegistrationStep") > -1) && (_g.Session.GetInt(Matchnet.Web.Framework.WebConstants.SESSION_PROPERTY_NAME_MEMBERID, 0) > 0))
                {
                    pageName = _g.AppPage.ControlName.Replace("RegistrationStep", "EditProfile");
                    appName = "Edit Your Profile";
                }

                // SubscriptionConfirmation.ascx was also updated to change the page name. Since the page's state is only known at pre-render, it was not implemented here.

                // Differentiate ViewProfile page name by logged in status while also changing out its name.
                if (_g.AppPage.ControlName.IndexOf("ViewProfile") > -1 || _g.AppPage.ControlName.IndexOf("ViewTabbedProfile") > -1)
                {
                    if ((Matchnet.Conversion.CInt(Request["MemberID"], Constants.NULL_INT) != Constants.NULL_INT) &&
                    (_g.Session.GetInt(Matchnet.Web.Framework.WebConstants.SESSION_PROPERTY_NAME_MEMBERID, Constants.NULL_INT) != Matchnet.Conversion.CInt(Request["MemberID"], Constants.NULL_INT)))
                    {
                        pageName = "View Full Profile";
                        appName = "SearchResults";
                    }
                    else
                    {
                        pageName = "View Your Profile";
                    }
                }

                // #MPR191
                if ((_g.AppPage.ControlName.IndexOf("View") > -1) && (_g.AppPage.App.ID == 26))
                {
                    int categoryID = Matchnet.Conversion.CInt(Request["CategoryID"]);
                    pageName = Enum.GetName(typeof(Matchnet.List.ValueObjects.HotListCategory), categoryID);
                }
                // _prevTrackedPage = g.Session.GetString(WebConstants.ACTION_CALL_PAGE);

                // g.Session.Remove(WebConstants.ACTION_CALL_PAGE);

                // Color Code Test and Result pages
                if (_g.AppPage.App.ID == (int)WebConstants.APP.ColorCode)
                {
                    appName = "ColorCode";
                    pageName = GetColorCodePageName(_g.AppPage.ID);
                }

                if (_g.AppPage.App.ID == (int)WebConstants.APP.QuestionAnswer)
                {
                    appName = "QuestionAnswer";
                }

                #endregion

                if (pageName == "GAMIframe")
                {
                    this.Visible = false;
                }

                _pageName = appName + " - " + pageName;

                _server = System.Environment.MachineName;

                if (!string.IsNullOrEmpty(Request["origref"]))
                {
                    Referrer = this.Page.Server.UrlDecode(Request["origref"]);
                }

                // For TNT Implementation - Only for JD Splash & Registration 
                if (RuntimeSettings.GetSetting("TEST_N_TARGET_MBOX_JS_INCLUDE_FLAG", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToLower() == "true")
                {
                    PlaceHolderMbox.Visible = true;
                }

                if (!string.IsNullOrEmpty(g.Session[OMNITURE_ALIAS_SESSION_KEY]))
                {
                    PageURL = this.Page.Server.UrlDecode(g.Session[OMNITURE_ALIAS_SESSION_KEY]);
                    g.Session.Remove(OMNITURE_ALIAS_SESSION_KEY);
                }

                string rawUrl = Request.RawUrl;
                _prop10 = rawUrl.Split(new char[] { '?' })[0];

                if (Request.UrlReferrer != null)
                {
                    string referrerUrl = Request.UrlReferrer.ToString();
                    _prop29 = referrerUrl;
                }
                // Handle previous page actions,  previous page is redirected to a different page, use session variables to render the values.

                // Handle every reg step save.
                // Session value is set to true in BaseRegistrationControl.cs navigateToNextRegistrationStep()
                if (_g.Session["OmnitureSavedRegStep"] == "true")
                {
                    _prop17 = GetProfileCompetionPercentage();

                    _g.Session.Remove("OmnitureSavedRegStep");
                }

                #region Logged In Variables

                bool alwaysSetDemographics = Conversion.CBool(RuntimeSettings.GetSetting("OMNITURE_ALWAYS_SEND_DEMOGRAPHICS", g.Brand.Site.Community.CommunityID,
                                                                    g.Brand.Site.SiteID, g.Brand.BrandID));

                // Session value is set to true in AutoLoginHelper and Logon.ascx.cs on successful login.
                bool logon = false;
                if (_g.Session["OmnitureLoggedIn"] == "true" || alwaysSetDemographics)
                {
                    setDemographics();

                    if (_g.Session["OmnitureLoggedIn"] == "true")
                    {
                        logon = true;
                        // Events - Logins
                        AddEvent("event3");
                        if (_g.Session["OmnitureAutoLoggedIn"] == "true")
                        {
                            AddEvent("event20");

                        }
                    }

                    int memberSubscriptionStatus = 0;
                    if (_g.Member != null)
                    {
                        memberSubscriptionStatus = FrameworkGlobals.GetMemberSubcriptionStatus(_g.Member, _g.Brand);
                        SubscriptionStatus enumSubscriptionStatus = (SubscriptionStatus)memberSubscriptionStatus;

                        this._evar10 = this.ConvertSubscriptionStatusToString(enumSubscriptionStatus);
                        this._evar8 = this.ConvertSubscriptionStatusToString(enumSubscriptionStatus, true);

                        //color code test status
                        QuizStatus colorCodeQuizStatus = ColorCodeHelper.GetMemberQuizStatus(g.Member, g.Brand);
                        switch (colorCodeQuizStatus)
                        {
                            case QuizStatus.none:
                                this._prop6 = "N";
                                break;
                            case QuizStatus.complete:
                                this._prop6 = "Y";
                                break;
                            case QuizStatus.partial:
                                this._prop6 = "Partial";
                                break;
                        }

                        //# of approved question answers
                        int numberOfApprovedAnswers = QuestionAnswerHelper.GetMemberApprovedAnswersCount(_g.Member.MemberID, _g.Brand.Site.SiteID);
                        AddEvent("event41");
                        AddProductEvent("event41=" + numberOfApprovedAnswers.ToString());

                        //login from Q/A Gate page
                        if (!String.IsNullOrEmpty(Request.QueryString["PRM"]))
                        {
                            if (Request.QueryString["PRM"] == "66799")
                            {
                                this._evar20 = "Q&A Gate Page:Login";
                            }

                            if (Request.QueryString["PRM"] == "77568" && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                            {
                                this.Render = false;
                            }
                        }

                        // Days since registration
                        DateTime regDate = Convert.ToDateTime(g.Member.GetAttributeDate(g.Brand, "BrandInsertDate"));
                        string days = "Editing new profile";
                        if (regDate != DateTime.MinValue)
                        {
                            days = DateTime.Now.Subtract(regDate).Days.ToString();
                        }

                        Evar45 = days;

                    }

                    //MPR-81 Demographic Data Not Being Set For "Auto Login"
                    if (_g.AppPage.ControlName != "Splash" || _g.AppPage.ControlName != "Splash20")
                    {
                        _g.Session.Remove("OmnitureLoggedIn");
                        _g.Session.Remove("OmnitureAutoLoggedIn");

                    }

                    if (_g.Member != null && Matchnet.Web.Applications.MemberProfile.EmailVerifyHelper.IsMemberVerified(_g.Member, _g.Brand))
                    {
                        _evar22 = WebConstants.OMNITURE_EVAR22;
                        _evar15 = WebConstants.OMNITURE_EVAR15_VERIFIED;
                        _prop22 = (_g.Member.IsPayingMember(_g.Brand.Site.SiteID)) ? "Subscriber" : "Registered";
                        _prop4 = WebConstants.OMNITURE_SPROP4_VERIFIED;


                    }
                    if (_g.Member != null)
                    {
                        // Check to see if the logged in user is an admin
                        if (MemberPrivilegeSA.Instance.IsAdminByPrivilegesOnly(_g.Member.MemberID))
                            _evar29 = WebConstants.OMNITURE_EVAR29_YES;
                        else
                            _evar29 = WebConstants.OMINTURE_EVAR29_NO;

                        // updating 27 with sub status tracking
                        if (memberSubscriptionStatus != 0)
                        {
                            SubscriptionStatus enumSubscriptionStatus2 = (SubscriptionStatus)memberSubscriptionStatus;

                            _prop27 = this.ConvertSubscriptionStatusToString(enumSubscriptionStatus2, true); // Enum.GetName(typeof(WebConstants.JRewardsMembershipStatus), g.DetermineJRewardsStatus());
                        }
                    }
                }

                #endregion
                #region forced email tracking

                if (_g.AppPage.ControlName == "VerifyEmail")
                {

                    WebConstants.ForcedBlockingMask blockingMask = _g.EmailVerify.GetBlockingMaskAttr(_g.Member);
                    if (!Matchnet.Web.Applications.MemberProfile.EmailVerifyHelper.IsMemberVerified(_g.Member, _g.Brand))
                    {
                        if ((blockingMask & WebConstants.ForcedBlockingMask.BlockedAfterReg) == WebConstants.ForcedBlockingMask.BlockedAfterReg)
                        {
                            _evar22 = WebConstants.OMNITURE_EVAR22;
                            _evar15 = WebConstants.OMNITURE_EVAR15_UNVERIFIED;
                            _prop22 = (_g.Member.IsPayingMember(_g.Brand.Site.SiteID)) ? "Subscriber" : "Registered";
                            _prop4 = WebConstants.OMNITURE_SPROP4_UNVERIFIED;
                            bool resendEmail = Conversion.CBool(_g.Session.GetString("OmnitureVerifyEmailResend"), false);
                            _g.Session.Remove("OmnitureVerifyEmailResend");
                            bool justRegistered = Conversion.CBool(_g.Session.GetString(Matchnet.Web.Applications.MemberProfile.EmailVerifyHelper.SESSION_EMAIL_VERIFY_JUST_REGISTERED), false);
                            _g.Session.Remove(Matchnet.Web.Applications.MemberProfile.EmailVerifyHelper.SESSION_EMAIL_VERIFY_JUST_REGISTERED);
                            if (!logon && !resendEmail && justRegistered)
                            {
                                AddEvent("event8");
                                bool photoUploaded = Conversion.CBool(g.Session.GetString("REG_PHOTO_UPLOADED"));
                                if (photoUploaded)
                                {
                                    AddEvent("event28");
                                }



                                setDemographics();
                            }
                        }
                        else if ((blockingMask & WebConstants.ForcedBlockingMask.BlockedAfterEmailChange) == WebConstants.ForcedBlockingMask.BlockedAfterEmailChange)
                        {
                            _evar15 = WebConstants.OMNITURE_EVAR15_UNVERIFIED;
                            _prop4 = WebConstants.OMNITURE_SPROP4_UNVERIFIED;
                        }
                    }
                }
                bool photoProfileUploaded = Conversion.CBool(g.Session.GetString("PROFILE_PHOTO_UPLOADED"));
                if (photoProfileUploaded)
                {
                    AddEvent("event29");
                }
                g.Session.Remove("PROFILE_PHOTO_UPLOADED");

                if ((_g.AppPage.ControlName == "Default" && _g.AppPage.App.ID == (int)WebConstants.APP.Home))
                {
                    bool emailVerifiedMsg = Conversion.CBool(_g.Session.GetString(Web.Applications.MemberProfile.EmailVerifyHelper.SESSION_EMAIL_VERIFY_NOTIFICATION_DISPLAY, "false"));
                    if (emailVerifiedMsg)
                    {
                        _evar15 = WebConstants.OMNITURE_EVAR15_VERIFIED;
                        _prop4 = WebConstants.OMNITURE_SPROP4_VERIFIED;
                    }
                }
                else if (_g.AppPage.ControlName == "VerifyEmailConfirmation")
                {

                    if (Matchnet.Web.Applications.MemberProfile.EmailVerifyHelper.IsMemberVerified(_g.Member, _g.Brand))
                    {
                        bool justConfirmed = Conversion.CBool(_g.Session.GetString(Matchnet.Web.Applications.MemberProfile.EmailVerifyHelper.SESSION_EMAIL_VERIFY_JUST_VERIFIED), false);
                        _g.Session.Remove(Matchnet.Web.Applications.MemberProfile.EmailVerifyHelper.SESSION_EMAIL_VERIFY_JUST_VERIFIED);
                        _evar15 = WebConstants.OMNITURE_EVAR15_VERIFIED;
                        _prop4 = WebConstants.OMNITURE_SPROP4_VERIFIED;

                        if (justConfirmed)
                            AddEvent("event16");
                    }
                }

                DateTime lastRequestedTime = DateTime.MinValue;
                if (DateTime.TryParse(FrameworkGlobals.GetCookie("OmnitureSessionCheck", DateTime.MinValue.ToString(), false), out lastRequestedTime))
                {
                    if (lastRequestedTime != DateTime.MinValue)
                    {
                        if (DateTime.Now.Subtract(lastRequestedTime).Minutes > 15)
                        {
                            if (!this._demographicsAlreadySet)
                            {
                                setDemographics();
                            }

                            setDetailedSubscriptionType();
                        }
                    }
                }

                FrameworkGlobals.AddCookie("OmnitureSessionCheck", DateTime.Now.ToString("u"), Constants.NULL_INT, false);

                #endregion

                if (!string.IsNullOrEmpty(Request["rc"]) && Request["rc"] == "JMETER_INVITATION_SUCCESSFULLY_SENT")
                {
                    AddEvent("event18");
                }

                // Sets an Omniture property with The member's Match Test Jmeter status
                try
                {
                    int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID,
                                                                    g.Brand.Site.SiteID, g.Brand.BrandID));

                    int mmenumval = (Int32)WebConstants.MatchMeterFlags.EnableApp;

                    if ((mmsett & mmenumval) == mmenumval)
                    {
                        MatchTestStatus mts = MatchMeterSA.Instance.GetMatchTestStatus(_g.Member.MemberID, MemberPrivilegeAttr.IsCureentSubscribedMember(_g), _g.Brand);
                        switch (mts)
                        {
                            case MatchTestStatus.Completed: _prop7 = "Y"; break;
                            case MatchTestStatus.MinimumCompleted: _prop7 = "Y"; break;
                            case MatchTestStatus.NotCompleted: _prop7 = "Partial"; break;
                            default: _prop7 = "N"; break;
                        }
                    }
                    else
                    {
                        _prop7 = "N";
                    }
                }
                catch
                {
                    _prop7 = "N";
                }
                try
                {
                    string approvedCaptionsCacheKey = "NUMBER_OF_APPROVED_CAPTIONS_KEY_" + g.Member.MemberID.ToString();
                    if (g.Cache[approvedCaptionsCacheKey] == null)
                    {
                        int total = 0;
                        PhotoCommunity pc = g.Member.GetPhotos(g.Brand.Site.Community.CommunityID);
                        for (byte i = 0; i < pc.Count; i++)
                        {
                            if (pc[i].IsCaptionApproved)
                            {
                                total++;
                            }
                        }

                        g.Cache.Add(approvedCaptionsCacheKey, total.ToString(),
                                    null, DateTime.Now.Add((new TimeSpan(0, 30, 0))), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                    }

                    this.Evar40 = g.Cache[approvedCaptionsCacheKey] as string;
                }
                catch (Exception)
                {
                    this.Evar40 = "0";
                }

                if (g.Member != null)
                {
                    //Homepage Version Test
                    if (Matchnet.Web.Applications.Home.HomeUtil.IsHome50Enabled(_g.Brand))
                    {
                        if (Matchnet.Web.Applications.Home.HomeUtil.IsDisplayingHome50(_g.Member, _g.AppPage, _g.Brand))
                        {
                            int homeTabIndex = Matchnet.Web.Applications.Home.HomeUtil.HOME_TABS_DEFAULT_INDEX;
                            if (Request.Cookies[Matchnet.Web.Applications.Home.HomeUtil.HOME_TABS_COOKIE_NAME] != null)
                            {
                                homeTabIndex =
                                    Convert.ToInt32(
                                        Request.Cookies[Matchnet.Web.Applications.Home.HomeUtil.HOME_TABS_COOKIE_NAME]
                                            .Value);
                            }

                            this._tempPageNameSuffix =
                                Matchnet.Web.Applications.Home.HomeUtil.HomeTabOmnitureName[homeTabIndex];
                        }

                        ControlVersion hpVersion = Matchnet.Web.Applications.Home.HomeUtil.GetHomepageVersion(_g.Member, _g.Brand);
                        switch (hpVersion)
                        {
                            case ControlVersion.Version50:
                                this._prop40 = "hp_voting";
                                break;
                            case ControlVersion.Version50b:
                                this._prop40 = "hp_novoting";
                                break;
                            default:
                                this._prop40 = "hp_control";
                                this._tempPageNameSuffix = "";
                                break;
                        }
                    }
                    else if (Matchnet.Web.Applications.Home.HomeUtil.IsHome40Enabled(_g.Brand))
                    {
                        ControlVersion hpVersion = Matchnet.Web.Applications.Home.HomeUtil.GetHomepageVersion(_g.Member, _g.Brand);
                        switch (hpVersion)
                        {
                            case ControlVersion.Version40a:
                                this._prop40 = "hp_nohero";
                                break;
                            case ControlVersion.Version40b:
                                this._prop40 = "hp_hero";
                                break;
                            default:
                                this._prop40 = "hp_control";
                                break;
                        }
                    }
                }

                if (g.Member != null)
                {
                    //Facebook connect status
                    FacebookManager.FacebookConnectStatus fbConnectStatus = FacebookManager.Instance.GetFacebookConnectStatus(g.Member, g.Brand);
                    switch (fbConnectStatus)
                    {
                        case FacebookManager.FacebookConnectStatus.NonConnected:
                            this._evar55 = "Not_Connected";
                            break;
                        case FacebookManager.FacebookConnectStatus.ConnectedNoFBData:
                            this._evar55 = "Connected_NoFBData";
                            break;
                        case FacebookManager.FacebookConnectStatus.ConnectedNoSavedData:
                            this._evar55 = "Connected_NoSavedData";
                            break;
                        case FacebookManager.FacebookConnectStatus.ConnectedShowData:
                            this._evar55 = "Connected_ShowData";
                            break;
                        case FacebookManager.FacebookConnectStatus.ConnectedHideData:
                            this._evar55 = "Connected_HideData";
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {

            if (!Render)
            {
                PlaceHolderOmniture.Visible = false;
                return;

            }
            else { setABRedesignProperties(); }
        }

        #endregion

        #region Private Helper Methods

        private string GetProfileCompetionPercentage()
        {
            bool hasPhoto = false;
            int currentStep;
            int completedSteps = 0;
            int stepValue = 1;

            if (g.Member == null)
            {
                return "";
            }
            if (_g.Member.GetPhotos(_g.Brand.Site.Community.CommunityID).Count > 0)
            {
                hasPhoto = true;
            }
            string regsteps = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("REGISTRATION_STEPS", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
            int stepCount = Matchnet.Conversion.CInt(regsteps) + 1; //add 1 for photo upload page
            int completedStepMask = g.Member.GetAttributeInt(_g.Brand, "NextRegistrationActionPageID");
            for (currentStep = 1; currentStep <= stepCount; currentStep++)
            {
                if (currentStep != stepCount)
                {
                    //regular steps
                    if ((completedStepMask & stepValue) == stepValue)
                    {
                        completedSteps++;
                    }
                }
                else
                {
                    //photoupload
                    if (hasPhoto)
                    {
                        completedSteps++;
                    }
                }

                stepValue = stepValue * 2;
            }

            return Convert.ToString(Convert.ToInt32((double)completedSteps / (double)stepCount * 100).ToString() + "%");
        }

        private string GetGender(int genderMask)
        {
            string gender;

            genderMask = genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE;

            gender = (genderMask == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) ? "Female" : "Male";

            return gender;
        }

        public string GetOnClickCustomLinkTracking(string prefix, string name)
        {
            string linkName = Constants.NULL_STRING;

            if (prefix != Constants.NULL_STRING)
            {
                linkName = _pageName + " : " + prefix + " - " + name;
            }
            else
            {
                linkName = _pageName + " : " + name;
            }

            return "var s=s_gi(s_account); s.linkTrackVars=\'None\'; s.linkTrackEvents=\'None\';" +
                "s.tl(this, \'o\', " + "\'" + linkName + "\');";
        }

        /// <summary>
        /// Site Redesign 20 Custom Link Tracking for Client side actions for Omniture
        /// </summary>
        /// <param name="action"></param>
        /// <param name="pagename"></param>
        /// <param name="subpagename"></param>
        /// <param name="includeReturnTrue">Indicates whether the JS script should include a return true; This should be true if you're applying this to the onclick to a link or button so that the user gets navigated to the Href after js code runs.</param>
        /// <param name="isSubPageNameJS">Indicates whether the subpagename is a js function to determine the subpagename dynamically on client side (e.g. As needed by profile tabs)</param>
        /// <returns></returns>
        public string GetOnClickCustomLinkTracking(WebConstants.Action action, string pagename, string subpagename, bool includeReturnTrue, bool isSubPageNameJS)
        {
            //Note: CustomLinkTracking() is defined in javascript20/spark.js.
            string onClickJS = "";
            string actionResxKey = "ACTION_" + action.ToString().ToUpper();
            string actionResx = g.GetResource(actionResxKey, this);
            if (isSubPageNameJS)
            {
                onClickJS = "var spn = " + subpagename + "; CustomLinkTracking(this, '" + actionResx.ToString() + "', '" + pagename + "', spn);";
            }
            else
            {
                onClickJS = "CustomLinkTracking(this, '" + actionResx.ToString() + "', '" + pagename + "', '" + subpagename + "');";
            }

            if (includeReturnTrue)
                onClickJS += "return true;";

            return onClickJS;

        }
        private void setDemographics()
        {
            // MPR-639: Capture the last digit of memberID
            if (_g.Member != null)
            {
                _evar44 = string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (_g.Member.MemberID % 10).ToString());

                // Gender of the member
                _prop18 = OmnitureHelper.GetGender(_g.Member.GetAttributeInt(_g.Brand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK));
                // Age
                _prop19 = OmnitureHelper.GetAge(_g.Member.GetAttributeDate(_g.Brand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString();
                // Ethnicity
                if (_g.Brand.Site.Community.CommunityID == (int)Matchnet.Web.Framework.WebConstants.COMMUNITY_ID.JDate)
                {
                    _prop20 = Matchnet.Web.Applications.MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.Member, _g, "JDateEthnicity", "JDateEthnicity");
                }
                else
                {
                    _prop20 = Matchnet.Web.Applications.MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.Member, _g, "Ethnicity", "Ethnicity");
                }
                // Region

                int languageID = _g.Brand.Site.LanguageID;
                if (_g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
                {
                    languageID = (int)Matchnet.Language.English;
                }

                _prop21 = OmnitureHelper.GetRegionString(_g.Member, _g.Brand, languageID, false, true, false);
                _prop21 = _prop21.Replace("\"", string.Empty); // There are regions with quote characters. 
                // Subscriber Status
                _prop22 = OmnitureHelper.GetSubscriberStatus(_g.Member, _g.Brand.Site.SiteID);
                // MemberID
                _prop23 = _g.Member.MemberID.ToString();

                // Profile Completeness
                _prop17 = OmnitureHelper.GetProfileCompetionPercentage(_g.Member, _g.Brand);
                _evar32 = getApprovedPhotoCountString();

                _prop36 = g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.Default, _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID).ToString();
            }

            this._demographicsAlreadySet = true;
        }

        private void setDetailedSubscriptionType()
        {
            if (_g.Member != null)
            {
                int memberSubscriptionStatus = FrameworkGlobals.GetMemberSubcriptionStatus(_g.Member, _g.Brand);
                SubscriptionStatus enumSubscriptionStatus = (SubscriptionStatus)memberSubscriptionStatus;
                this._evar8 = this.ConvertSubscriptionStatusToString(enumSubscriptionStatus, true);
            }
        }

        private string getApprovedPhotoCountString()
        {
            try
            {
                if (g.Member == null)
                    return "";

                int count = MemberPhotoDisplayManager.Instance.GetApprovedPhotosCount(g.Member, g.Member, g.Brand);

                return count.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }

        }
        #endregion

        #region UI redesign
        private void setABRedesignProperties()
        {
            //05192010 TL - MPR-1819 - No longer setting UI Redesign tracking for evar16
            if (g.Member != null &&
                g.AppPage.ControlName.ToLower() != "logoff" &&
                g.AppPage.ControlName.ToLower() != "logoff20" &&
                g.AppPage.ControlName.ToLower() != "logon" &&
                g.AppPage.ControlName.ToLower() != "logon20"
                )
            {
                if (g.AppPage.ControlName.ToLower().IndexOf("default") > -1)
                {
                    if (Request.QueryString["tnt"] != null)
                    {
                        if (Request.QueryString["tnt"] == "welcome_overlay")
                        {
                            this._evar16 = "Welcome Overlay";
                        }
                    }
                }

                //    if (!g.IsSite20Enabled)
                //        _evar16 = WebConstants.OMNITURE_EVAR16_A;
                //    else
                //    {
                //        _evar16 = WebConstants.OMNITURE_EVAR16_B;
                //    }
            }



            SetActionVars();

            string prevTopNav = Request["NavPoint"];
            if (!String.IsNullOrEmpty(prevTopNav))
            {
                if (prevTopNav == "top")
                {
                    string resx = "";
                    string resxKey = "";
                    WebConstants.APP appid = (WebConstants.APP)g.AppPage.App.ID;
                    if (appid == WebConstants.APP.Subscription)
                    {
                        if (g.AppPage.ID == (int)WebConstants.PageIDs.Subscribe)
                        {
                            resxKey = "TOPNAV_" + WebConstants.PageIDs.Subscribe.ToString().ToUpper();
                        }
                        else if (g.AppPage.ID == (int)WebConstants.PageIDs.History)
                        {
                            resxKey = "TOPNAV_" + WebConstants.PageIDs.History.ToString().ToUpper();
                        }
                    }
                    else
                    {
                        resxKey = "TOPNAV_" + appid.ToString().ToUpper();

                    }
                    resx = g.GetResource(resxKey, this);

                    _prop14 = string.Format(topnav_tracking_format, resx);
                }

                if (prevTopNav == "sub")
                {
                    string resx = "";
                    string resxKey = "";
                    WebConstants.PageIDs pageid = (WebConstants.PageIDs)g.AppPage.ID;

                    if (g.AppPage.App.ID == (int)WebConstants.APP.HotList)
                    {
                        resx = GetHostListSubMenu();
                    }
                    else if (g.AppPage.ID == (int)WebConstants.PageIDs.MailBox)
                    {
                        resx = GetMailBoxSubMenu();
                    }
                    else
                    {
                        resxKey = "SUBNAV_" + pageid.ToString().ToUpper();
                        resx = g.GetResource(resxKey, this);
                    }

                    _prop14 = string.Format(subnav_tracking_format, resx);

                }

                if (prevTopNav == "profile" || prevTopNav == "mailbox")
                {
                    WebConstants.PageIDs pageid = (WebConstants.PageIDs)g.AppPage.ID;

                    //nav from profile may not only navigate to profile pages
                    string navvalue = Request["NavValue"];
                    if (!String.IsNullOrEmpty(navvalue))
                        _prop14 = navvalue;

                }
            }


        }
        private string GetHostListSubMenu()
        {
            int category = Conversion.CInt(HttpContext.Current.Request["CategoryID"]);
            string subdisplay = "";
            switch (category)
            {
                case -0:
                case -1:
                case -3:
                case -4:
                case -2:
                case -25:
                case -5:
                    subdisplay = "HotListsMembersWhoHave";
                    break;
                case -9:
                case -6:
                case -7:
                case -24:
                case -8:
                case -13:
                    subdisplay = "HotListsMembersYouHave";
                    break;
                case -15:
                    subdisplay = "HotListsClick";
                    break;
                case -10:
                    subdisplay = "HotListsMatchMail";
                    break;
                default:
                    subdisplay = "HotListsCustomLists";
                    break;

            }
            return subdisplay;
        }

        private string GetMailBoxSubMenu()
        {
            string subdisplay = "";
            int folderid = Conversion.CInt(HttpContext.Current.Request["MemberFolderID"]);
            if (folderid <= 0)
                folderid = 1;
            switch (folderid)
            {
                case 1:
                case 2:
                case 3:
                    subdisplay = "Messages";
                    break;

                default:
                    subdisplay = "Custom Folder Settings";
                    break;

            }
            return subdisplay;


        }

        private string GetColorCodePageName(int pageID)
        {
            string colorCodePage = "ColorCodeTestLanding";

            try
            {
                switch (pageID)
                {
                    case (int)WebConstants.PageIDs.ColorCodeAnalysis:
                        colorCodePage = GetColorCodeAnalysisPageName();
                        break;
                    case (int)WebConstants.PageIDs.ColorCodeQuiz1:
                        colorCodePage = "ColorCodeTestPage1";
                        break;
                    case (int)WebConstants.PageIDs.ColorCodeQuiz2:
                        colorCodePage = "ColorCodeTestPage2";
                        break;
                    case (int)WebConstants.PageIDs.ColorCodeQuizConfirmation:
                        colorCodePage = "ColorCodeTestConfirmation";
                        break;
                    case (int)WebConstants.PageIDs.ColorCodeQuizIntro:
                        colorCodePage = "ColorCodeTestIntro";
                        break;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return colorCodePage;

        }

        private string GetColorCodeAnalysisPageName()
        {
            string colorCodePage = "ColorCodeResultIntro";
            if (!String.IsNullOrEmpty(HttpContext.Current.Request["report"]))
            {
                ReportType report = (ReportType)Enum.Parse(typeof(ReportType), Request.QueryString["report"], true);
                switch (report)
                {
                    case ReportType.hot:
                        colorCodePage = "ColorCodeResultHot";
                        break;
                    case ReportType.matchIntro:
                        colorCodePage = "ColorCodeResultMatchIntro";
                        break;
                    case ReportType.matchB:
                        colorCodePage = "ColorCodeResultMatchBlue";
                        break;
                    case ReportType.matchR:
                        colorCodePage = "ColorCodeResultMatchRed";
                        break;
                    case ReportType.matchW:
                        colorCodePage = "ColorCodeResultMatchWhite";
                        break;
                    case ReportType.matchY:
                        colorCodePage = "ColorCodeResultMatchYellow";
                        break;
                    case ReportType.need:
                        colorCodePage = "ColorCodeResultNeed";
                        break;
                    case ReportType.not:
                        colorCodePage = "ColorCodeResultNot";
                        break;
                    case ReportType.otherIntro:
                        colorCodePage = "ColorCodeResultOtherIntro";
                        break;
                    case ReportType.otherB:
                        colorCodePage = "ColorCodeResultOtherBlue";
                        break;
                    case ReportType.otherR:
                        colorCodePage = "ColorCodeResultOtherRed";
                        break;
                    case ReportType.otherW:
                        colorCodePage = "ColorCodeResultOtherWhite";
                        break;
                    case ReportType.otherY:
                        colorCodePage = "ColorCodeResultOtherYellow";
                        break;
                    case ReportType.want:
                        colorCodePage = "ColorCodeResultWant";
                        break;
                    default:
                        colorCodePage = "ColorCodeResultIntro";
                        break;

                }
            }
            return colorCodePage;
        }

        private void SetActionVars()
        {
            try
            {
                string trackingFormat = "{0}({1})";
                string action = Request[WebConstants.ACTION_CALL];
                string actionLink = Request[WebConstants.ACTION_LINK];

                if (String.IsNullOrEmpty(action))
                    return;
                WebConstants.PageIDs pageID = (WebConstants.PageIDs)Enum.Parse(typeof(WebConstants.PageIDs), action);
                if (((int)pageID == (int)WebConstants.PageIDs.ViewMessage && g.AppPage.ID == (int)WebConstants.PageIDs.MailBox)
                            || pageID == WebConstants.PageIDs.MiniSearch)
                {//continue - exception)
                }
                else if ((int)pageID != g.AppPage.ID)
                    return;

                if (!String.IsNullOrEmpty(actionLink))
                {
                    action = actionLink;
                }
                string actionPage = Request[WebConstants.ACTION_CALL_PAGE];
                string actionPageDetail = Request[WebConstants.ACTION_CALL_PAGE_DETAIL];

                string actionSource = "";
                if (String.IsNullOrEmpty(action))
                    return;

                if (String.IsNullOrEmpty(actionPage))
                    return;

                actionSource = actionPage;
                if (!String.IsNullOrEmpty(actionPageDetail))
                    actionSource = actionSource + " - " + actionPageDetail;

                string resKey = "ACTION_" + action.ToUpper();
                string actionResourse = g.GetResource(resKey, this);

                if (pageID == WebConstants.PageIDs.MiniSearch)
                    _prop14 = actionSource;
                else if (_g.AppPage.App.ID == (int)WebConstants.APP.Email)
                {
                    _evar28 = String.Format(trackingFormat, actionResourse, actionSource);
                    _evar27 = String.Format(trackingFormat, actionResourse, actionSource);
                }
                else
                    _evar27 = String.Format(trackingFormat, actionResourse, actionSource);


            }
            catch (Exception ex)
            { // will think how to handle error (mostly bad enum value) later.....probably never}
            }

        }

        #endregion

        #region Public Utility Methods
        public void AddEvent(string newEvent)
        {
            if (_events == Constants.NULL_STRING)
            {
                _events = newEvent;
            }
            else
            {
                string[] eventArray = _events.Split(new char[] { ',' });
                bool hasEvent = false;
                foreach (string s in eventArray)
                {
                    if (s == newEvent)
                    {
                        hasEvent = true;
                        break;
                    }
                }

                if (!hasEvent)
                    _events += "," + newEvent;
            }
        }

        /// <summary>
        /// This adds a product event (e.g. event21=100), do not use this if you're adding category, product name, quantity or price portion.
        /// For that, you'll need to clear the product and set the Product property directly.
        /// </summary>
        /// <param name="newProduct"></param>
        public void AddProductEvent(string newProduct)
        {
            if (_products == Constants.NULL_STRING)
            {
                _products = ";;;;" + newProduct; //format: category;product name;quanity;price;eventN|eventN
            }
            else
            {
                _products += "|" + newProduct;
            }
        }

        public string GetJDateMobileStatus()
        {
            string ret = string.Empty;

            // check to see if this member is even capable
            h1.Constants.MessmoCapableStatus messmoCapable = (h1.Constants.MessmoCapableStatus)g.Member.GetAttributeInt(g.Brand, "MessmoCapable", 0);
            if (messmoCapable != h1.Constants.MessmoCapableStatus.Capable)
            {
                ret = "Non Mobile";
            }
            else
            {
                h1.Constants.MessmoAlertSetting messmoAlertSetting = (h1.Constants.MessmoAlertSetting)g.Member.GetAttributeInt(g.Brand, "MessmoAlertSetting", 0);
                switch (messmoAlertSetting)
                {
                    case Matchnet.HTTPMessaging.Constants.MessmoAlertSetting.Always:
                        ret = "Mobile - Alert Always";
                        break;
                    case Matchnet.HTTPMessaging.Constants.MessmoAlertSetting.WhenLoggedOff:
                        ret = "Mobile - Alert Offline";
                        break;
                    case Matchnet.HTTPMessaging.Constants.MessmoAlertSetting.DoNotSend:
                        ret = "Mobile - Never";
                        break;
                }
            }

            return ret;
        }

        public string JavaScriptEncode(string jsArg)
        {
            return AntiXss.JavaScriptEncode(jsArg, false);
        }

        public string ConvertSubscriptionStatusToString(SubscriptionStatus subStatus)
        {
            return ConvertSubscriptionStatusToString(subStatus, false);
        }


        /// <summary>
        /// 1.WB - winback
        /// 2.FTS - first time subscriber
        /// 3.never Sub - Never subscribed
        /// 4.ex sub - subscribed at one point but no longer a subscriber
        /// 5.Renewing WB - renewed after buying a winback
        /// 6.Renewing FTS - renewed without lapse in subscriptions
        /// </summary>
        /// <param name="subStatus"></param>
        /// <param name="moreDetailed"></param>
        /// <returns></returns>
        public string ConvertSubscriptionStatusToString(SubscriptionStatus subStatus, bool moreDetailed)
        {
            if (moreDetailed)
            {
                switch (subStatus)
                {
                    case SubscriptionStatus.PremiumSubscriber:
                        return "Premium";
                    case SubscriptionStatus.SubscribedFTS:
                        return "FTS - first time subscriber";
                    case SubscriptionStatus.SubscribedWinBack:
                        return "WB - winback";
                    case SubscriptionStatus.RenewalSubscribed:
                        return "Renewing FTS - renewed without lapse in subscriptions";
                    case SubscriptionStatus.RenewalWinBack:
                        return "Renewing WB - renewed after buying a winback";
                    case SubscriptionStatus.SubscriberFreeTrial:
                        return "FreeTrial";
                    case SubscriptionStatus.NonSubscriberNeverSubscribed:
                        return "never Sub - Never subscribed";
                    case SubscriptionStatus.NonSubscriberExSubscriber:
                        return "ex sub - subscribed at one point but no longer a subscriber";
                    default:
                        return "";
                }
            }
            else
            {
                switch (subStatus)
                {
                    case SubscriptionStatus.NonSubscriberNeverSubscribed:
                    case SubscriptionStatus.NonSubscriberExSubscriber:
                        return "Member";
                    case SubscriptionStatus.PremiumSubscriber:
                        return "PremiumSubscriber";
                    default:
                        return "Subscriber";
                }
            }
        }
        #endregion

        #region Public Link Tracking Methods

        /// <summary>
        /// Gets Omniture link tracking params that you can add to your URL
        /// </summary>
        public static string GetActionURLParams(WebConstants.PageIDs targetPage, WebConstants.Action action, string currentOmniturePageName, string additionalPageNameDetail)
        {
            string actionParamFormat = "{0}={1}&{2}={3}&{4}={5}";
            string pagename = "";
            if (!string.IsNullOrEmpty(currentOmniturePageName))
            {
                pagename = HttpContext.Current.Server.UrlEncode(currentOmniturePageName);
            }
            if (!String.IsNullOrEmpty(additionalPageNameDetail))
            {
                pagename = pagename + HttpContext.Current.Server.UrlEncode(" - " + additionalPageNameDetail);
            }

            return String.Format(actionParamFormat, WebConstants.ACTION_CALL, targetPage, WebConstants.ACTION_LINK, action, WebConstants.ACTION_CALL_PAGE, pagename);
        }

        /// <summary>
        /// Adds Omniture link tracking param to your URL
        /// </summary>
        public string GetActionURL(WebConstants.PageIDs targetPage, WebConstants.Action action, string currentControlURL, string additionalPageNameDetail)
        {
            string trackingURL = currentControlURL;
            try
            {
                if (currentControlURL.IndexOf("?") >= 0)
                    trackingURL = trackingURL + "&";
                else
                    trackingURL = trackingURL + "?";

                if (trackingURL.StartsWith("javascript:"))
                {
                    int questionMarkIndex = trackingURL.IndexOf("?");
                    trackingURL = trackingURL.Insert(questionMarkIndex + 1, GetActionURLParams(targetPage, action, _pageName, additionalPageNameDetail) + "&");
                    if (trackingURL.EndsWith("&"))
                        trackingURL = trackingURL.Remove(trackingURL.Length - 1);
                }
                else
                {
                    trackingURL += GetActionURLParams(targetPage, action, _pageName, additionalPageNameDetail);
                }

                return trackingURL;
            }
            catch (Exception ex)
            {
                g.Notification.AddErrorString(ex.Message);
                return currentControlURL;

            }
        }

        /// <summary>
        /// Gets URL Parameters for Omniture Click to Page tracking.
        /// These params are used by pages like Compose or Flirt to render omniture data to specify which page sent them to the Compose or Flirt.
        /// </summary>
        /// <param name="ActionCallPage"></param>
        /// <param name="Action"></param>
        /// <param name="ActionCallPageDetail"></param>
        /// <param name="isForConnect">Specifies whether this is for connect page, it needs to combine the call page and detail into one</param>
        /// <returns></returns>
        public static string GetOmnitureClickToPageURLParams(string ActionCallPage, WebConstants.PageIDs Action, string ActionCallPageDetail, WebConstants.Action actionType, bool isForConnect)
        {

            string param = "";
            if (isForConnect)
            {
                //note: profile page currently uses this and requires that the ActionCallPage param is the last one returned in this function, because dynamic javascript will append to end of this url
                param = "&" + WebConstants.ACTION_CALL + "=" + actionType.ToString()
                    + "&" + WebConstants.ACTION_CALL_PAGE + "=" + HttpContext.Current.Server.UrlEncode(ActionCallPage + " - ");

            }
            else
            {
                //note: profile page currently uses this and requires that the ActionCallPageDetail param is the last one returned in this function, because dynamic javascript will append to end of this url
                param = "&" + WebConstants.ACTION_CALL_PAGE + "=" + HttpContext.Current.Server.UrlEncode(ActionCallPage)
                        + "&" + WebConstants.ACTION_CALL + "=" + Action.ToString() + "&" + WebConstants.ACTION_CALL_PAGE_DETAIL + "=" + HttpContext.Current.Server.UrlEncode(ActionCallPageDetail);
            }
            return param;

        }

        /// <summary>
        /// Gets UrL Parameters for Omniture Navigation Click to Page tracking
        /// </summary>
        /// <param name="navPoint"></param>
        /// <param name="navValue"></param>
        /// <returns></returns>
        public static string GetOmnitureNavClickToPageURLParams(string navPoint, string navValue)
        {
            //note: profile page currently uses this and requires that the navValue param is the last one returned in this function

            string param = "&" + WebConstants.URL_PARAMETER_NAME_OMNITURE_NAVPOINT + "=" + HttpContext.Current.Server.UrlEncode(navPoint)
                    + "&" + WebConstants.URL_PARAMETER_NAME_OMNITURE_NAVVALUE + "=" + HttpContext.Current.Server.UrlEncode(navValue);

            return param;

        }

        public string GetPagingURL(string currentControlURL, string navPoint, string navValue)
        {
            string actionURLFormat = "{0}={1}&{2}={3}";
            string trackingURL = currentControlURL;
            try
            {
                if (currentControlURL.IndexOf("?") >= 0)
                    trackingURL = trackingURL + "&";
                else
                    trackingURL = trackingURL + "?";

                if (trackingURL.StartsWith("javascript:"))
                {
                    int questionMarkIndex = trackingURL.IndexOf("?");
                    trackingURL = trackingURL.Insert(questionMarkIndex + 1, String.Format(actionURLFormat, WebConstants.URL_PARAMETER_NAME_OMNITURE_NAVPOINT, navPoint, WebConstants.URL_PARAMETER_NAME_OMNITURE_NAVVALUE, HttpContext.Current.Server.UrlEncode(navValue)) + "&");
                    if (trackingURL.EndsWith("&"))
                        trackingURL = trackingURL.Remove(trackingURL.Length - 1);
                }
                else
                {
                    trackingURL += String.Format(actionURLFormat, WebConstants.URL_PARAMETER_NAME_OMNITURE_NAVPOINT, navPoint, WebConstants.URL_PARAMETER_NAME_OMNITURE_NAVVALUE, HttpContext.Current.Server.UrlEncode(navValue));
                }
                return trackingURL;
            }
            catch (Exception ex)
            {
                return currentControlURL;

            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new EventHandler(this.Page_Init);
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}
