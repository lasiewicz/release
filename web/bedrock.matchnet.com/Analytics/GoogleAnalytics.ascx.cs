﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework;
using Spark.Common.RestConsumer.V2.Models.Content.Region;

namespace Matchnet.Web.Analytics
{
    public partial class GoogleAnalytics : Framework.FrameworkControl
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (_g.Session["GoogleAnalyticsLoggedIn"] == "true")
            {
                plcTrackLogin.Visible = true;
                _g.Session.Remove("GoogleAnalyticsLoggedIn");
            }
        }

        protected string GetGoogleAnalyticsAccount()
        {
            return SettingsManager.GetSettingString(SettingConstants.GOOGLE_ANALYTICS_ACCOUNT, g.Brand);
        }

        protected string GetGenderText()
        {
            return g.AnalyticsOmniture.Prop18;
        }

        protected string GetSeekingGenderText()
        {
            string seekingGender = string.Empty;

            if (g.Member != null && g.Member.MemberID > 0)
            {
                int genderMask = g.Member.GetAttributeInt(g.Brand, "gendermask") &
                                 Matchnet.Lib.ConstantsTemp.GENDERID_SEEKING_FEMALE;
                seekingGender = (genderMask == Matchnet.Lib.ConstantsTemp.GENDERID_SEEKING_FEMALE) ? "Female" : "Male";
            }

            return seekingGender;
        }

        protected string GetAge()
        {
            if (g.Member != null && g.Member.MemberID > 0)
            {
                return FrameworkGlobals.GetAge(g.Member, g.Brand).ToString();
            }

            return string.Empty;
        }

        protected string GetMaritalStatus()
        {
            if (g.Member != null && g.Member.MemberID > 0)
            {
                return ProfileDisplayHelper.GetOptionValue(g.Member, g, "MaritalStatus", "MaritalStatus");
            }

            return string.Empty;
        }

        protected string GetLocation()
        {
            if (g.Member != null && g.Member.MemberID > 0)
            {
                return ProfileDisplayHelper.GetRegionDisplay(g.Member, g);
            }
            return string.Empty;
        }

        protected string IsPayingMember()
        {
            bool isPayingMember = false;

            if (g.Member != null && g.Member.MemberID > 0)
            {
                isPayingMember = g.Member.IsPayingMember(g.Brand.Site.SiteID);
            }

            return isPayingMember.ToString().ToLower();
        }

        protected string GetJDateReligion()
        {
            if (g.Member != null && g.Member.MemberID > 0)
            {
                return ProfileDisplayHelper.GetOptionValue(g.Member, g, "JDateReligion", "JDateReligion");
            }

            return string.Empty;
        }

        protected string GetEducationLevel()
        {
            if (g.Member != null && g.Member.MemberID > 0)
            {
                return ProfileDisplayHelper.GetOptionValue(g.Member, g, "EducationLevel", "EducationLevel");
            }

            return string.Empty;
        }

        protected string GetJDateEthnicity()
        {
            if (g.Member != null && g.Member.MemberID > 0)
            {
                return ProfileDisplayHelper.GetOptionValue(g.Member, g, "JDateEthnicity", "JDateEthnicity");
            }

            return string.Empty;
        }

        protected string GetIncomeLevel()
        {
            if (g.Member != null && g.Member.MemberID > 0)
            {
                return ProfileDisplayHelper.GetOptionValue(g.Member, g, "IncomeLevel", "IncomeLevel");
            }

            return string.Empty;
        }

        protected string GetOccupation()
        {
            if (g.Member != null && g.Member.MemberID > 0)
            {
                return ProfileDisplayHelper.GetOptionValue(g.Member, g, "IndustryType", "IndustryType");
            }

            return string.Empty;

        }

        protected string GetCountry()
        {
            if (g.Member != null && g.Member.MemberID > 0)
            {
                int RegionID = g.Member.GetAttributeInt(g.Brand, "regionid");
                RegionLanguage region = null;

                if (RegionID > 0)
                {
                    region = RegionSA.Instance.RetrievePopulatedHierarchy(RegionID, g.Brand.Site.LanguageID);
                    string country = string.Empty;

                    if (region != null)
                    {
                        if (region.CountryRegionID > 0)
                        {
                            country =
                                RegionSA.Instance.RetrieveRegionByID(region.CountryRegionID, g.Brand.Site.LanguageID)
                                    .Description;
                        }
                    }
                    return country;
                }
            }
            return string.Empty;
        }
    }
}