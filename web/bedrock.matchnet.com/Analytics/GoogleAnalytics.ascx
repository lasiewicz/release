﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoogleAnalytics.ascx.cs" Inherits="Matchnet.Web.Analytics.GoogleAnalytics" %>
<script type="text/javascript" src="/Analytics/Javascript/spark.ga.js"></script>
<%= "<script type=\"text/javascript\">" %> 
    var ga_clientId;
    spark.ga.tid = '<%=GetGoogleAnalyticsAccount() %>';

    jQuery(window).load(function() {
        ga_clientId = spark.ga.getClientIDByTackerID_ga();
    });

    <asp:PlaceHolder runat="server" ID="plcTrackLogin" Visible="False">
        jQuery(window).load(function() {
            var data = new Object();
            data.memberId =  '<%=_g.Member.MemberID%>';
            data.gender = '<%=GetGenderText() %>';
            data.seekingGender = '<%=GetSeekingGenderText() %>';
            data.age = '<%=GetAge() %>';
            data.maritalStatus = '<%=GetMaritalStatus() %>';
            data.location = '<%=GetLocation() %>'; 
            data.isPayingMember = '<%=IsPayingMember() %>';
            data.jDateReligion ='<%=GetJDateReligion() %>';
            data.educationLevel ='<%=GetEducationLevel() %>';
            data.jDateEthnicity ='<%=GetJDateEthnicity() %>';
            data.incomeLevel ='<%=GetIncomeLevel() %>';
            data.occupation ='<%=GetOccupation() %>';
            data.country='<%=GetCountry() %>';
            spark.ga.trackPageSuccessLogin(data, ga_clientId);
        });
    </asp:PlaceHolder>
<%= "</script>"%>



