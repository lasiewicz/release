﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.fbchannel
{
    public partial class channel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //specify long expiration time
            Response.Cache.SetCacheability(HttpCacheability.Public);
            Response.Cache.SetExpires(DateTime.Now.AddYears(1));
            Response.AppendHeader("Pragma", "public");
        }
    }
}