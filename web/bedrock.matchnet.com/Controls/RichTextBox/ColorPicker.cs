using System;
using System.Text;

namespace Matchnet.Web.Controls.RichTextBox
{
	public class ColorPicker
	{
		private RichTextEdit _RichTextBox;
		private System.Drawing.Color _ToolbarColor;
		private System.Drawing.Color _ToolbarItemOverColor;
		private System.Drawing.Color _ToolbarItemOverBorderColor;
		private string _ClientID = Matchnet.Constants.NULL_STRING;
		private string _FormName = Matchnet.Constants.NULL_STRING;

		public ColorPicker(RichTextEdit parent)
		{
			_RichTextBox = parent;
			_ToolbarColor = parent.ToolbarColor;
			_ToolbarItemOverColor = parent.ToolbarItemOver;
			_ToolbarItemOverBorderColor = parent.ToolbarItemOverBorder;
			_ClientID = _RichTextBox.TidyObjectRef(_RichTextBox.ClientID);
			_FormName = parent.FormName;
		}

		public string CreateColorPickerCode(string objID)
		{
			System.Web.UI.Control control = new System.Web.UI.Control();
			string colorPickerObj = _ClientID + "_" + objID;
			StringBuilder sb = new StringBuilder();

			sb.Append("<script language=\"Javascript\">");
			sb.Append("var " + colorPickerObj + "_sInitColor = null;");
			sb.Append("var " + colorPickerObj + "_divSet = false; var " + colorPickerObj + "_curId;");

			sb.Append("function " + colorPickerObj + "_setColor(color) {");
			sb.Append("var preview = " + _ClientID + "_getObj('" + colorPickerObj + "_Preview');");
			sb.Append("var picker = " + _ClientID + "_getObj('" + colorPickerObj + "_DivLayer');");
			sb.Append("picker.style.display = 'none';");
			sb.Append("picker.style.zindex = 999999;");
			sb.Append("preview.style.background = '" + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + "';");
			sb.Append("preview.innerHTML = '&nbsp;';");

			if (objID == "FontColor")
			{
				sb.Append(_ClientID + "_RTB.document.execCommand(\"ForeColor\", false, color);");
			}
			else if (objID == "Highlight")
			{
				sb.Append(_ClientID + "_RTB.document.execCommand(\"BackColor\", false, color);");
			}
			
			sb.Append("}");

			// display the color selection dialog
			sb.Append("function " + colorPickerObj + "_callColorDlg() {");
			sb.Append("if (" + _ClientID + "_isHTMLMode) {alert(\"Function not available in HTML Mode\"); return;}");
			sb.Append("var picker = " + _ClientID + "_getObj('" + colorPickerObj + "_DivLayer');");
			sb.Append("picker.style.display = 'none';");
			sb.Append("if (" + colorPickerObj + "_sInitColor == null) { ");
			sb.Append("var " + colorPickerObj + "_sColor = " + _FormName + "." + _ClientID + "_dlgHelper.ChooseColorDlg(); }");
			sb.Append("else {");
			sb.Append("var " + colorPickerObj + "_sColor = " + _FormName + "." + _ClientID + "_dlgHelper.ChooseColorDlg(" + colorPickerObj + "_sInitColor); ");
			sb.Append(colorPickerObj + "_sColor = " + colorPickerObj + "_sColor.toString(16);");
			sb.Append("if (" + colorPickerObj + "_sColor.length < 6) {");
			sb.Append("var " + colorPickerObj + "_sTempString = \"000000\".substring(0,6-" + colorPickerObj + "_sColor.length);");
			sb.Append(colorPickerObj + "_sColor = " + colorPickerObj + "_sTempString.concat(" + colorPickerObj + "_sColor);");
			sb.Append("}}");
			sb.Append(colorPickerObj + "_setColor(" + colorPickerObj + "_sColor);");
			sb.Append("}");

			sb.Append("function " + colorPickerObj + "_setDiv() {");
			sb.Append("if (!document.createElement) {return;}");
			sb.Append("var " + _ClientID + "_elemDiv = document.createElement('div');");
			sb.Append("if (typeof(" + _ClientID + "_elemDiv.innerHTML) != 'string') { return; }");
			sb.Append(_ClientID + "_elemDiv.id = '" + colorPickerObj + "_DivLayer';");
			sb.Append(_ClientID + "_elemDiv.style.position = 'absolute';");
			sb.Append(_ClientID + "_elemDiv.unselectable = 'on';");
			sb.Append(_ClientID + "_elemDiv.style.filter = 'progid:DXImageTransform.Microsoft.Shadow(color=\"#888888\", Direction=120, Strength=3); alpha(Opacity=100);';");
			sb.Append(_ClientID + "_elemDiv.style.display = 'none';");
			sb.Append(_ClientID + "_elemDiv.style.border = '1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + " outset';");
			sb.Append(_ClientID + "_elemDiv.style.background = '" + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + "';");
			sb.Append(_ClientID + "_elemDiv.innerHTML = " + colorPickerObj + "_getColorTable();");
			sb.Append("document.body.appendChild(" + _ClientID + "_elemDiv);");
			sb.Append(colorPickerObj + "_divSet = true;");
			sb.Append("}");

			sb.Append("function " + colorPickerObj + "_pickColor(id) {");
			sb.Append("if (" + _ClientID + "_isHTMLMode) {alert(\"Function not available in HTML Mode\"); return;}");

			// hide any other menus that maybe showing
			if (objID == "FontColor")
			{
				sb.Append("var hideme = " + _ClientID + "_getObj('" + _ClientID + "_Highlight_DivLayer');"); // hide highlight
				sb.Append("if (hideme != null) {hideme.style.display = 'none';}");
				sb.Append("var hideme = " + _ClientID + "_getObj('" + _ClientID + "_TableMenu_tableMenu');"); // hide table menu
				sb.Append("if (hideme != null) {hideme.style.display = 'none';}");
			}
			else if (objID == "Highlight")
			{
				sb.Append("var hideme = " + _ClientID + "_getObj('" + _ClientID + "_FontColor_DivLayer');");
				sb.Append("if (hideme != null) {hideme.style.display = 'none';}");
				sb.Append("var hideme = " + _ClientID + "_getObj('" + _ClientID + "_TableMenu_tableMenu');"); // hide table menu
				sb.Append("if (hideme != null) {hideme.style.display = 'none';}");
			}

			sb.Append("if (!" + colorPickerObj + "_divSet) {" + colorPickerObj + "_setDiv();}");
			sb.Append("var picker = " + _ClientID + "_getObj('" + colorPickerObj + "_DivLayer');");
			sb.Append("if (id == " + colorPickerObj + "_curId && picker.style.display == 'block') {");
			sb.Append("picker.style.display = 'none';");
			sb.Append("return;");
			sb.Append("}");
			sb.Append(colorPickerObj + "_curId = id;");
			sb.Append("var thelink = " + _ClientID + "_getObj(id);");
			sb.Append("picker.style.top = " + _ClientID + "_getAbsoluteOffsetTop(thelink); + 20;");
			sb.Append("picker.style.left = " + _ClientID + "_getAbsoluteOffsetLeft(thelink);");
			sb.Append("picker.style.display = 'block';");
			sb.Append("}");

			sb.Append("function " + colorPickerObj + "_getColorTable() {");
			sb.Append("var colors = " + _ClientID + "_colorArray;");
			sb.Append("var tableCode = '';");
			sb.Append("tableCode += '<table border=\"0\" cellspacing=\"6\" cellpadding=\"0\">';");
			sb.Append("tableCode += '<tr><td style=\"border: 1px #000000 dotted; font-family:verdana; font-size: 8pt;\" ';");
			sb.Append("tableCode += 'colspan=\"' + " + _ClientID + "_perline + '\">';");
			sb.Append("tableCode += '<table width=\"100%\" cellspacing=\"0\" cellpadding=\"3\"><tr>';");
			sb.Append("tableCode += '<td id=\"" + colorPickerObj + "_Preview\" style=\"font-family:verdana; font-size: 8pt;\">';");
			sb.Append("tableCode += '&nbsp;&nbsp;&nbsp;';");
			sb.Append("tableCode += '</td></tr></table>';");
			sb.Append("tableCode += '</td></tr>';");
			sb.Append("for (i = 0; i < colors.length; i++) {");
			sb.Append("if (i % " + _ClientID + "_perline == 0) {tableCode += '<tr>';}");
			sb.Append("tableCode += '<td style=\"border: 1px #000000 solid; width: 5px;\" bgcolor=\"#ffffff\"><a style=\"color: ' ");
			sb.Append("+ colors[i] + '; background: ' + colors[i] + '; font-size: 10px; padding: 0px; font-face: tahoma; font-weight: none;\" title=\"' ");
			sb.Append("+ colors[i] + '\" href=\"javascript:" + colorPickerObj + "_setColor(\\\'' + colors[i] + '\\\');\"' ");
			sb.Append("+ 'onMouseOver=\"" + colorPickerObj + "_updatePreview(\\\'' + colors[i] + '\\\');\"><font size=\"1\">&nbsp;&nbsp;&nbsp;</font></a></td>';");
			sb.Append("if (i % " + _ClientID + "_perline == " + _ClientID + "_perline - 1) {tableCode += '</tr>';}");
			sb.Append("}");
			sb.Append("if (i % " + _ClientID + "_perline != 0) {tableCode += '</tr>';}");
			sb.Append("tableCode += '<tr><td style=\"font-family:verdana; cursor:hand; font-size: 8pt;\" colspan=\"' + " + _ClientID + "_perline + '\">';");
			sb.Append("tableCode += '<a style=\"text-decoration: none; width:100%; padding:3px; border: 1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + " solid\" onmouseover=\\'this.style.border=\"1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarItemOverBorderColor) + " solid\"; this.style.background=\\\"" + System.Drawing.ColorTranslator.ToHtml(_ToolbarItemOverColor) + "\\\";\\' onmouseout=\\'this.style.border=\"1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + " solid\";this.style.background=\\\"" + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + "\\\";\\'  href=\"javascript:" + colorPickerObj + "_callColorDlg();\"><font color=\"#000000\">More Colors...</a></font>';");			
			sb.Append("tableCode += '</td></tr>';");
			sb.Append("tableCode += '<tr><td style=\"font-family:verdana; cursor:hand; font-size: 8pt;\" colspan=\"' + " + _ClientID + "_perline + '\">';");
			sb.Append("tableCode += '<a style=\"text-decoration: none; width:100%; padding:3px; border: 1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + " solid\" onmouseover=\\'this.style.border=\"1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarItemOverBorderColor) + " solid\"; this.style.background=\\\"" + System.Drawing.ColorTranslator.ToHtml(_ToolbarItemOverColor) + "\\\";\\' onmouseout=\\'this.style.border=\"1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + " solid\";this.style.background=\\\"" + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + "\\\";\\' href=\"javascript:" + colorPickerObj + "_setColor(\\\'\\\');\"><font color=\"#000000\">None</font></a>';");
			sb.Append("tableCode += '</td></tr>';");
			sb.Append("tableCode += '</table>';");
			sb.Append("return tableCode;");
			sb.Append("}");

			sb.Append("function " + colorPickerObj + "_updatePreview(strHexVal) {");
			sb.Append("var preview = " + _ClientID + "_getObj('" + colorPickerObj + "_Preview');");
			sb.Append("preview.style.background = strHexVal;");
			sb.Append("preview.innerHTML = strHexVal;");
			sb.Append("}");

			sb.Append("</script>");

			return sb.ToString();
		}
	}
}
