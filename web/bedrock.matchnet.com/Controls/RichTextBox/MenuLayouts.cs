using System;
using System.Text;

namespace Matchnet.Web.Controls.RichTextBox
{
	public class MenuLayouts
	{
		private RichTextEdit _RichTextBox ;

		public MenuLayouts(RichTextEdit parent)
		{
			_RichTextBox = parent;
		}

		public string DefaultTemplate()
		{
			StringBuilder sb = new StringBuilder();
			ToolBar toolBar = new ToolBar(_RichTextBox);
			ToolBarItems toolBarItems = new ToolBarItems(_RichTextBox);

			sb.Append(toolBar.CreateToolBar());
			sb.Append(toolBar.InsertToolBarGrabber());
			sb.Append(toolBarItems.InsertCut());
			sb.Append(toolBarItems.InsertCopy());
			sb.Append(toolBarItems.InsertPaste());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertUndo());
			sb.Append(toolBarItems.InsertRedo());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertNumberedList());
			sb.Append(toolBarItems.InsertBulletedList());
			sb.Append(toolBarItems.InsertIndent());
			sb.Append(toolBarItems.InsertUnIndent());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertAlignLeft());
			sb.Append(toolBarItems.InsertAlignCenter());
			sb.Append(toolBarItems.InsertAlignRight());
			sb.Append(toolBarItems.InsertJustify());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertTable());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertImage());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertHyperLink());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertTextBox());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertToggleGuideLines());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertPrint());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertDateTime());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBar.InsertToolBarSeperator());
			sb.Append(toolBar.InsertToolBarGrabber());
			sb.Append(toolBarItems.InsertFontDropDownList());
			sb.Append(toolBarItems.InsertSpacer());
			sb.Append(toolBarItems.InsertFontSizeDropDownList());
			sb.Append(toolBarItems.InsertSpacer());
			sb.Append(toolBarItems.InsertFontStyleDropDownList());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertFontColor());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertHighlightColor());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertBold());
			sb.Append(toolBarItems.InsertItalics());
			sb.Append(toolBarItems.InsertUnderline());
			sb.Append(toolBarItems.InsertStrikeThrough());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertSuperScript());
			sb.Append(toolBarItems.InsertSubScript());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertHorizontalRule());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBar.DisposeToolBar());

			return sb.ToString();
		}
		
		public string BasicOptions()
		{
			StringBuilder sb = new StringBuilder();
			ToolBar toolBar = new ToolBar(_RichTextBox);
			ToolBarItems toolBarItems = new ToolBarItems(_RichTextBox);

			sb.Append(toolBar.CreateToolBar());
			sb.Append(toolBar.InsertToolBarGrabber());
			sb.Append(toolBarItems.InsertBold());
			sb.Append(toolBarItems.InsertItalics());
			sb.Append(toolBarItems.InsertUnderline());
			sb.Append(toolBarItems.InsertStrikeThrough());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertHorizontalRule());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertNumberedList());
			sb.Append(toolBarItems.InsertBulletedList());
			sb.Append(toolBarItems.InsertIndent());
			sb.Append(toolBarItems.InsertUnIndent());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertAlignLeft());
			sb.Append(toolBarItems.InsertAlignCenter());
			sb.Append(toolBarItems.InsertAlignRight());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertUndo());
			sb.Append(toolBarItems.InsertRedo());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBar.InsertToolBarSeperator());
			sb.Append(toolBar.InsertToolBarGrabber());
			sb.Append(toolBarItems.InsertFontDropDownList());
			sb.Append(toolBarItems.InsertSpacer());
			sb.Append(toolBarItems.InsertFontSizeDropDownList());
			sb.Append(toolBarItems.InsertSpacer());
			sb.Append(toolBarItems.InsertFontStyleDropDownList());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertCut());
			sb.Append(toolBarItems.InsertCopy());
			sb.Append(toolBarItems.InsertPaste());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBar.DisposeToolBar());

			return sb.ToString();
		}

		public string MinimalOptions()
		{
			StringBuilder sb = new StringBuilder();
			ToolBar toolBar = new ToolBar(_RichTextBox);
			ToolBarItems toolBarItems = new ToolBarItems(_RichTextBox);
			
			sb.Append(toolBar.CreateToolBar());
			sb.Append(toolBar.InsertToolBarGrabber());
			sb.Append(toolBarItems.InsertCut());
			sb.Append(toolBarItems.InsertCopy());
			sb.Append(toolBarItems.InsertPaste());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertUndo());
			sb.Append(toolBarItems.InsertRedo());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertBold());
			sb.Append(toolBarItems.InsertItalics());
			sb.Append(toolBarItems.InsertUnderline());
			sb.Append(toolBarItems.InsertStrikeThrough());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertSuperScript());
			sb.Append(toolBarItems.InsertSubScript());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBarItems.InsertHorizontalRule());
			sb.Append(toolBarItems.InsertItemSeperator());
			sb.Append(toolBar.DisposeToolBar());

			return sb.ToString();
		}

		public string FreeTextApproval()
		{
			StringBuilder sb = new StringBuilder();
			ToolBar toolBar = new ToolBar(_RichTextBox);
			ToolBarItems toolBarItems = new ToolBarItems(_RichTextBox);
			sb.Append(toolBar.CreateToolBar(false));
			return sb.ToString();
		}
	}
}
