using System;
using System.Text;

using Matchnet.Web.Framework;

namespace Matchnet.Web.Controls.RichTextBox
{
	public class ToolBar
	{
		private RichTextEdit _RichTextBox;
		private System.Drawing.Color _ToolbarColor;
		private string _ClientID = Matchnet.Constants.NULL_STRING;
		private Matchnet.Web.Framework.Image _image;

		public ToolBar(RichTextEdit parent)
		{
			_RichTextBox = parent;
			_ToolbarColor = parent.ToolbarColor;
			_ClientID = _RichTextBox.TidyObjectRef(_RichTextBox.ClientID);
			_image = new Matchnet.Web.Framework.Image();
		}

		public string CreateToolBar()
		{
			return CreateToolBar(true);
		}

		public string CreateToolBar(bool style)
		{
			if (!style)
				return "<div id=\"" + _ClientID + "_toolbar\" style=\"padding-top:0px; padding-bottom:0px; padding-left:0px; padding-right:0px;\">";
			else
				return "<div id=\"" + _ClientID + "_toolbar\" style=\"width:100%; background-color: " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + "; padding-top:1px; padding-bottom:2px; padding-left:2px; padding-right:0px;\">";
		}

		public string DisposeToolBar()
		{
			return "</div>";
		}

		public string InsertToolBarSeperator()
		{
			return "<br /><img src=\"" +  _image.GetUrl("mnu_toolsep.gif", Constants.APP_FREETEXTAPPROVAL, false) +"\" unselectable=\"on\" height=\"2\" class=\"" + _ClientID + "_ToolBarSeperator\">";
		}

		public string InsertToolBarGrabber()
		{
			return "<img src=\"" +  _image.GetUrl("mnu_grab.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" unselectable=\"on\" width=\"6\" align=\"absmiddle\" height=\"18\">&nbsp;";
		}
	}

	public class ToolBarItems
	{
		private RichTextEdit _RichTextBox;
		private System.Drawing.Color _ToolbarColor;
		private string _ClientID = Matchnet.Constants.NULL_STRING;
		private Matchnet.Web.Framework.Image _image;

		public ToolBarItems(RichTextEdit parent)
		{
			_RichTextBox = parent;
			_ToolbarColor = parent.ToolbarColor;
			_ClientID = _RichTextBox.TidyObjectRef(_RichTextBox.ClientID);
			_image = new Matchnet.Web.Framework.Image();
		}
		
		public string InsertRule()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" backgroundcolor: " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + "; unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('Cut')\" title=\"Cut\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_cut.gif", Constants.APP_FREETEXTAPPROVAL, false) + "></BUTTON>";
		}

		public string InsertCut()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" backgroundColor=\"" + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + "\" onClick=\"" + _ClientID + "_cmdExec('Cut')\" title=\"Cut\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_cut.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertCopy()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('Copy')\" title=\"Copy\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_copy.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertPaste() 
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('Paste')\" title=\"Paste\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_paste.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertImage() 
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_insertImage()\" title=\"Insert Picture\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_insertimg.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertHyperLink()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_createLink()\" title=\"Create Hyperlink\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_link.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertNumberedList() 
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('insertorderedlist')\" title=\"Numbered List\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_numlist.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertBulletedList()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('insertunorderedlist')\" title=\"Bullets\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_bullets.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertUnIndent()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('outdent')\" title=\"Decrease Indent\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_unindent.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertIndent()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('indent')\" title=\"Increse Indent\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_indent.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertUndo()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onclick=\"" + _ClientID + "_Undo();\" title=\"Undo\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_undo.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertRedo()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onclick=\"" + _ClientID + "_Redo()\" title=\"Redo\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_redo.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertPrint()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onclick=\"" + _ClientID + "_cmdExec('print')\" title=\"Print\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_print.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertDateTime() 
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onclick=\"" + _ClientID + "_InsertDateTime();\" title=\"Insert current date + time\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_datetime.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertTable()
		{
			TableMenu tablePicker = new TableMenu(_RichTextBox);
			string javaScript = tablePicker.CreateTableMenuCode();

			return javaScript + "<BUTTON unselectable=\"on\" CLASS=\"" + _ClientID + "_MenuButton\" onclick=\"" + _ClientID + "_TableMenu_displayTableMenu('" + _ClientID + "_tablebutton');\" id=\"" + _ClientID + "_tablebutton\" title=\"Insert Table\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_table.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertFontColor()
		{
			ColorPicker colorPicker = new ColorPicker(_RichTextBox);
			string javaScript = colorPicker.CreateColorPickerCode("FontColor");

			return javaScript + "<BUTTON unselectable=\"on\" CLASS=\"" + _ClientID + "_MenuButton\" ID=\"" + _ClientID + "_FontColor\" onClick=\"" + _ClientID + "_FontColor" + "_pickColor('" + _ClientID + "_FontColor')\" title=\"Font Color\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_fontcolor.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertHighlightColor() 
		{
			ColorPicker colorPicker = new ColorPicker(_RichTextBox);
			string javaScript = colorPicker.CreateColorPickerCode("Highlight");

			return javaScript + "<BUTTON unselectable=\"on\" CLASS=\"" + _ClientID + "_MenuButton\" ID=\"" + _ClientID + "_Highlight\" onClick=\"" + _ClientID + "_Highlight" + "_pickColor('" + _ClientID + "_Highlight')\" title=\"Highlight\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_highlight.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertBold()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onclick=\"" + _ClientID + "_cmdExec('bold')\" title=\"Bold\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_bold.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertStrikeThrough()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onclick=\"" + _ClientID + "_cmdExec('StrikeThrough')\" title=\"StrikeThrough\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_strike.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertItalics()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onclick=\"" + _ClientID + "_cmdExec('Italic')\" title=\"Italics\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_italics.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertUnderline()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onclick=\"" + _ClientID + "_cmdExec('Underline')\" title=\"Underline\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_underline.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}
			
		public string InsertAlignLeft()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('justifyleft')\" title=\"Align Left\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_left.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertAlignCenter()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('justifycenter')\" title=\"Align Center\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_center.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertAlignRight()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('justifyright')\" title=\"Align Right\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_right.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertJustify()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('justifyfull')\" title=\"Justify Text\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_justify.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertSuperScript() 
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('superscript')\" title=\"Superscript\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_superscript.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertSubScript()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('subscript')\" title=\"Subscript\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_subscript.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertToggleGuideLines()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_toggleGuidelines()\" title=\"Toggle Table GuideLines\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_toggleguidelines.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}
		
		public string InsertTextBox()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_InsertFloatFont()\" title=\"Insert TextBox\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_textbox.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertHorizontalRule()
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + _ClientID + "_cmdExec('InsertHorizontalRule')\" title=\"Insert Horizontal Rule\" style=\"cursor:hand;\"><img src=\"" + _image.GetUrl("mnu_hoz.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertCustomTool(string javaScriptCommand, string toolTip, string iconPath)
		{
			return "<BUTTON CLASS=\"" + _ClientID + "_MenuButton\" unselectable=\"on\" onClick=\"" + javaScriptCommand + "\" title=\"" + toolTip + "\" style=\"cursor:hand;\"><img src=\"" + iconPath + "\" width=\"16\" height=\"16\"></BUTTON>";
		}

		public string InsertItemSeperator()
		{
			return "&nbsp;<img src=\"" + _image.GetUrl("mnu_copy.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" unselectable=\"on\" width=\"2\" align=\"absmiddle\" class=\"" + _ClientID + "_MenuItemSeperator\">&nbsp;";
		}

		public string InsertFontDropDownList()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("<select style=\"font-family: sans-serif, arial, tahoma, verdana; cursor:hand; font-color: #333333; font-size: 11px;\" onchange=\"" + _ClientID + "_cmdExec('fontname',this[this.selectedIndex].value); this.selectedIndex=0; this.focus()\">");
			sb.Append("<option selected>Font</option>");
			sb.Append("<option value=\"Arial\">Arial</option>");
			sb.Append("<option value=\"Arial Black\">Arial Black</option>");
			sb.Append("<option value=\"Arial Narrow\">Arial Narrow</option>");
			sb.Append("<option value=\"Comic Sans MS\">Comic Sans MS</option>");
			sb.Append("<option value=\"Courier New\">Courier New</option>");
			sb.Append("<option value=\"System\">System</option>");
			sb.Append("<option value=\"Tahoma\">Tahoma</option>");
			sb.Append("<option value=\"Times New Roman\">Times New Roman</option>");
			sb.Append("<option value=\"Verdana\">Verdana</option>");
			sb.Append("<option value=\"Wingdings\">Wingdings</option>");

			sb.Append("</select>");
			return sb.ToString();
		}

		public string InsertFontSizeDropDownList()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("<select style=\"font-family: sans-serif, arial, tahoma, verdana; cursor:hand; font-color: #333333; font-size: 11px;\" onChange=\"" + _ClientID + "_cmdExec('fontsize',this[this.selectedIndex].value); this.selectedIndex=0; this.focus()\">");
			sb.Append("<option selected>Size</option>");
			sb.Append("<option value=\"1\">1</option>");
			sb.Append("<option value=\"2\">2</option>");
			sb.Append("<option value=\"3\">3</option>");
			sb.Append("<option value=\"4\">4</option>");
			sb.Append("<option value=\"5\">5</option>");
			sb.Append("<option value=\"6\">6</option>");
			sb.Append("<option value=\"7\">7</option>");
			sb.Append("</select>");
		
			return sb.ToString();
		}
							  
		public string InsertFontStyleDropDownList()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("<select style=\"font-family: sans-serif, arial, tahoma, verdana; cursor:hand; font-color: #333333; font-size: 11px;\" onChange=\"" + _ClientID + "_cmdExec('formatBlock',this[this.selectedIndex].value); this.selectedIndex=0; this.focus()\">");
			sb.Append("<option selected>Style</option>");
			sb.Append("<option value=\"Normal\">Normal</option>");
			sb.Append("<option value=\"Heading 1\">Heading 1</option>");
			sb.Append("<option value=\"Heading 2\">Heading 2</option>");
			sb.Append("<option value=\"Heading 3\">Heading 3</option>");
			sb.Append("<option value=\"Heading 4\">Heading 4</option>");
			sb.Append("<option value=\"Heading 5\">Heading 5</option>");
			sb.Append("<option value=\"Address\">Address</option>");
			sb.Append("<option value=\"Formatted\">Formatted</option>");
			sb.Append("<option value=\"Definition Term\">Definition Term</option>");
			sb.Append("</select>");
		
			return sb.ToString();
		}

		public string InsertSpacer()
		{
			return "&nbsp;";
		}

	}
}
