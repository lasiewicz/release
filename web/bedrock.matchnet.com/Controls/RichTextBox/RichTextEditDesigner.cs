using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.Design;
using System.Text;

namespace Matchnet.Web.Controls.RichTextBox
{
	public class RichTextEditDesigner : ControlDesigner
	{
		public override string GetDesignTimeHtml()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("<table border='0' cellpadding='5' cellspacing='0' style='border-width:1; border-collapse: collapse; border-style: outset; padding: 3; background-color:#E0E0E0'>");
			sb.Append("<tr><td><font face='verdana' size='2'>Matchnet RichTextBox</font></td></tr></table>");
			return sb.ToString();
		}
	}
}
