<%@ Control Language="c#" AutoEventWireup="false" enableviewstate="false" Codebehind="StandardNotification.ascx.cs" Inherits="Matchnet.Web.NotificationTemplates.StandardNotification" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<div id="divNotification" class="pageMessage" runat="server">
	<asp:Image ID="imgIcon" Runat="server" />
	<asp:Literal ID="litMessage" Runat="server" />
</div>