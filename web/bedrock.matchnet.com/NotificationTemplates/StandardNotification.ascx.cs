#region System References
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

namespace Matchnet.Web.NotificationTemplates
{
	/// <summary>
	///		The standard notification control used by the Standard template.
	/// </summary>
	public class StandardNotification : Lib.NotificationTemplateBase
	{
		#region Member variables
		protected System.Web.UI.WebControls.Image imgIcon;
		protected System.Web.UI.WebControls.Literal litMessage;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divNotification;
		#endregion 

		#region Private Methods
		private void Page_Load(object sender, System.EventArgs e)
		{
			imgIcon.ImageUrl = base.ImagePath;
			litMessage.Text = base.Message;
			divNotification.Attributes.Add("class", base.CssClass);
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
