﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework.Ui.Advertising;
using Matchnet.Web.Framework;

namespace Matchnet.Web.LayoutTemplates
{
    public partial class IFrame : Lib.LayoutTemplateBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // GAM
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("GOOGLE_AD_MANAGER", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
            {
                GAM gam = (GAM)Page.LoadControl("~/Framework/Ui/Advertising/GAM.ascx");
                PlaceHolderGAM.Controls.Add(gam);
                g.GAM = gam;
            }

            // Fetchback pixel
            if (PixelHelper.FetchbackPixelVisible(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.AppPage.ControlName, g.Member))
            {
                plcFetchbackPixel.Controls.Add(PixelHelper.GetFetchbackPixel(g.Member, g.Brand.Site.SiteID));
            }
        }
    }
}