﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;

using Matchnet.Web.Framework.PagePixels;
using Matchnet.Web.Applications.Search;
using Matchnet.Web.Applications.Search;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.LayoutTemplates
{
    public partial class WideReset : Lib.LayoutTemplateBase
    {
        //!***************** Note **************************************
        //This template has bad choosen name, it is used on new Splash20/registration pages
        //the main use is for square rounded corners area in the middle of the page.
        //!*****************************************************************
        protected const string JAVASCRIPT_FILE_PATH_MAIN = "/javascript20/spark.js";

        const string structCSSSetting = "CSS_STRUCT";
        const string styleCSSSetting = "CSS_STYLE";
        private const string IMG_LOGO_FILE_NAME = "logo-header-img.png";


        protected const string CSS_PATH = "/css/";
        protected const string HREF = "href";
        public bool IsBlank = false;
        private Boolean allowFramesFlag = false;
        private bool renderAdHeaderTopIFrameIDToJS = false;

        private void Page_Init(object sender, System.EventArgs e)
        {
            //for dynamic css classes
            string cssformat = "page-{0} sub-page-{1} brand-alias-{2} fixed-width";
            string[] appname = _g.AppPage.App.Path.Split('/');
            int upperInd = appname.Length - 1;
            string app = appname[upperInd];
            string subpage = _g.AppPage.ControlName.ToLower();

            g.Head20 = Head20;
            g.LayoutTemplateBase = this;

            bdyMain.Attributes["class"] = String.Format(cssformat, app, subpage, g.Session[WebConstants.SESSION_PROPERTY_NAME_BRANDALIASID]);

            //// Add ExitPopup if applicable.
            Matchnet.Web.Framework.Popup popup = GalleryExitPopup.CreatePopup(g);
            if (popup != null)
                _g.Page.Controls.Add(popup);

            // Add the PagePixel control.
            plcPixels.Controls.Add(new PagePixelControl());

            // Add the Sage Analytics control.
            Lib.Util util = new Lib.Util();
            plcSage.Controls.Add(util.RenderAnalytics(base.SaveSession));

            //// Omniture Analytics - Use Sage Place Holder for now.
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {

                Analytics.Omniture omniture = (Analytics.Omniture)Page.LoadControl("/Analytics/Omniture.ascx");
                plcSage.Controls.Add(omniture);
                _g.AnalyticsOmniture = omniture;

            }

            // Add Google analytics
            if (!string.IsNullOrEmpty(SettingsManager.GetSettingString(SettingConstants.GOOGLE_ANALYTICS_ACCOUNT, g.Brand)))
            {
                var googleAnalytics =
                    (Analytics.GoogleAnalytics)Page.LoadControl("/Analytics/GoogleAnalytics.ascx");
                plcSage.Controls.Add(googleAnalytics);
            }

            // Fetchback pixel
            if (PixelHelper.FetchbackPixelVisible(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.AppPage.ControlName, g.Member))
            {
                plcFetchbackPixel.Controls.Add(PixelHelper.GetFetchbackPixel(g.Member, g.Brand.Site.SiteID));
            }

            //google marketing pixel
            if (g.Member != null)
            {
                if (g.Session.GetInt(WebConstants.SESSION_GOOGLE_PIXEL_FIRED) != 1)
                {
                    // In JDIL, we are going to add the gender mask in the pixel code
                    string genderMask = g.Member.GetAttributeInt(g.Brand, "GenderMask").ToString();
                    string[] pixelArgs = new string[2] { genderMask, genderMask };

                    //fire paid/subscriber pixel only once per login
                    if (g.Member.IsPayingMember(g.Brand.Site.SiteID))
                    {
                        txtGoogleRemarketingCodeSubscriber.Visible = true;
                        txtGoogleRemarketingCodeSubscriber.Args = pixelArgs;
                    }
                    else
                    {
                        txtGoogleRemarketingCodeFree.Visible = true;
                        txtGoogleRemarketingCodeFree.Args = pixelArgs;
                    }
                    g.Session.Add(WebConstants.SESSION_GOOGLE_PIXEL_FIRED, "1", SessionPropertyLifetime.Temporary);
                }
            }

            //facebook marketing pixel
            if (g.Member != null)
            {
                if (g.Session.GetInt(WebConstants.SESSION_FACEBOOK_PIXEL_FIRED) != 1)
                {
                    //fire pixel only once per login
                    txtFacebookRemarketingCode.Visible = true;
                    g.Session.Add(WebConstants.SESSION_FACEBOOK_PIXEL_FIRED, "1", SessionPropertyLifetime.Temporary);
                }
            }

            if (LivePersonManager.Instance.IsLivePersonEnabled(g.Member, g.Brand))
            {
                plcLivePerson.Visible = true;
            }
            
            _g.LayoutVersion = WebConstants.LayoutVersions.versionWide;
            
            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                bdyMain.Attributes["dir"] = base.Direction;
            }

            txtGoogleTagManagerCode.Visible = SettingsManager.GetSettingBool("ENABLE_GOOGLE_TAG_MANAGER", g.Brand,
                   false);

            txtDynamicYieldBodyCode.Visible = SettingsManager.GetSettingBool("ENABLE_DYNAMIC_YIELD_PIXEL", g.Brand, false);
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            //imgLogo.SecureRequest = false;
            //imgLogoNo.SecureRequest = false;
            //headerCobrand.Style.Add("background-image", "url(" + imagePath + ")");
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!allowFramesFlag && !g.LoadingRightOnly)
                base.BustaFrames();

            if (IsBlank)
                PlaceHolderAppendOnloadEventScript.Visible = false;

            base.OnPreRender(e);
        }
        /// Appends ?v=[GetLastWriteTimeString] to the given path. (Useful for defeating browser caching when deploying new 
        /// versions (e.g. sparkCommonNonHe.css?v=11172005115104)
        /// </summary>
        /// <param name="path">The relative to site root or relative path to file</param>
        /// <returns></returns>
        protected string AppendVersion(string path)
        {
            return path + "?v=" + FrameworkGlobals.GetLastWriteTimeString(path);
        }        

        
        protected string GetPageLang()
        {
            string lang = "en";
            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                lang = "he";
            }
            return lang;
        }

        protected string GetPageDirection()
        {
            string direction = "ltr";
            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                direction = base.Direction;
            }
            return direction;
        }
    }

}