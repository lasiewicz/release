﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WideReset.ascx.cs" Inherits="Matchnet.Web.LayoutTemplates.WideReset" %>
<%@ Register TagPrefix="MNFramework" TagName="Right" Src="/Framework/Ui/Right.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Head20" Src="/Framework/Ui/Head20.ascx" %>
<%@ Register TagPrefix="mn1" TagName="TopAuxNav" Src="/Framework/Ui/HeaderNavigation20/TopAuxNav.ascx" %>
<%@ Register TagPrefix="mn" TagName="Header" Src="/Framework/Ui/Header20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="fp" TagName="ForcedPage" Src="/Applications/PremiumServices/Controls/ForcedPage.ascx" %>
<%@ Register tagPrefix="mn1"  tagName="LivePersonCustomVars" src="/Applications/LivePerson/CustomVariables.ascx"%>
<%@ Register TagPrefix="uc1" TagName="TealiumScript" Src="~/Applications/Tealium/TealiumScript.ascx" %>
<!doctype html>
<!--[if lte IE 7]> <html class="ie ie7 lte9 lte8 lte7 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"> <![endif]--> 
<!--[if IE 8]> <html class="ie ie8 lte9 lte8 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"> <![endif]--> 
<!--[if IE 9]> <html class="ie ie9 lte9 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"> <![endif]--> 
<!--[if (gte IE 10)|!(IE)]><!--><html class="no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"><!--<![endif]--> 
<mn1:Head20 id="Head20" runat="server" IncludeCSS="false" IncludeJSLibrary="false" />
<%-- attention everyone who will work on this file, we don't want NotificationControlPlaceholder to be declared in WideReset.ascx.designer.cs but unfortunately VS does it automatically. It screws up Notifications, so  please see if there is a line: protected global::System.Web.UI.WebControls.PlaceHolder NotificationControlPlaceholder; in WideReset.ascx.designer.cs and comment it. Thanks --%>
<body runat="server" id="bdyMain" >
    <uc1:TealiumScript runat="server" ID="TealiumScript" />
    <mn:Txt ID="txtGoogleTagManagerCode" runat="server" ResourceConstant="GOOGLE_TAG_MANAGER_CODE" Visible="False" />
    <mn:Txt ID="txtDynamicYieldBodyCode" runat="server" ResourceConstant="TXT_DYNAMIC_YIELD_BODY_CODE" Visible="False" />
<asp:PlaceHolder runat="server" Visible="false" ID="plcLivePerson">
    <mn:Txt ID="txtLivePersonBodyTag" runat="server" ResourceConstant="TXT_LIVEPERSON_BODY_TAG"  />
    <mn1:LivePersonCustomVars runat="server" ID="lpCustomVars"/>
</asp:PlaceHolder>
<asp:PlaceHolder ID="plcSage" Runat="server" />
<form id="Form1" method="post" runat="server" autocomplete="false">
    <asp:PlaceHolder ID="AppControlPlaceholder" Runat="server" />
</form>
<asp:PlaceHolder ID="plcPixels" Runat="server" />
<asp:PlaceHolder ID="plcFetchbackPixel" Runat="server" />
<asp:PlaceHolder ID="PlaceHolderAppendOnloadEventScript" runat="server" Visible="true">
    <script type="text/javascript">
        $j(document).ready(function(){init()});
    </script>
</asp:PlaceHolder>
<mn:Txt ID="txtGoogleRemarketingCode" runat="server" ResourceConstant="JAVASCRIPT_GOOGLE_REMARKETING_TAG"/>
<mn:Txt ID="txtGoogleRemarketingCodeFree" runat="server" ResourceConstant="JAVASCRIPT_GOOGLE_REMARKETING_TAG_FREE" Visible="False"/>
<mn:Txt ID="txtGoogleRemarketingCodeSubscriber" runat="server" ResourceConstant="JAVASCRIPT_GOOGLE_REMARKETING_TAG_SUBSCRIBER" Visible="False"/>
<mn:Txt ID="txtFacebookRemarketingCode" runat="server" ResourceConstant="JAVASCRIPT_FACEBOOK_REMARKETING_TAG" Visible="False"/>
</body>
</html>