<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PopupOnce.ascx.cs" Inherits="Matchnet.Web.LayoutTemplates.PopupOnce" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="MNFramework" TagName="Head" Src="/Framework/Ui/Head20.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<MNFramework:Head ID="Head" Runat="server" />
<body id="BodyTag" dir="<asp:Literal id=litDirection runat=server/>">
<asp:PlaceHolder ID="plcSage" Runat="server" />
<%-- these IE conditional comments have been added to eliminate the use of css hacks in the stylesheets --%>
<!--[if IE]><div id="ieAll"><![endif]-->
<!--[if IE 8]><div id="ie8only"><![endif]-->
<!--[if IE 7]><div id="ie7only"><![endif]-->
<!--[if (IE 6)|(IE 7)]><div id="ie6-7"><![endif]-->
<!--[if IE 6]><div id="ie6only"><![endif]-->
	<form id="Main" method="post" runat="server">
		<asp:PlaceHolder ID="NotificationControlPlaceholder" Runat="server" />
		<asp:PlaceHolder ID="AppControlPlaceholder" Runat="server" />
	</form>
	<%-- removed per josh viney<div class="ppsubnavbottom"></div>
	<center>
		<mn:Copyright ID="copyright" Runat="server" />
	</center> --%>
	
	<asp:PlaceHolder ID="plcPixels" Runat="server" />
	<asp:PlaceHolder ID="plcFetchbackPixel" Runat="server" />
	
<!--[if IE]></div><![endif]-->
<!--[if IE 8]></div><![endif]-->
<!--[if IE 7]></div><![endif]-->
<!--[if (IE 6)|(IE 7)]></div><![endif]-->
<!--[if IE 6]></div><![endif]-->

</body>
