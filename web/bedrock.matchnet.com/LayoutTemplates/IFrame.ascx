﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IFrame.ascx.cs" Inherits="Matchnet.Web.LayoutTemplates.IFrame" %>
<html>
    <head><asp:PlaceHolder ID="PlaceHolderGAM" runat="server"></asp:PlaceHolder>
    <script type="text/javascript">
        //This is code copied from spark.js to prevent javascript errors caused by the call to
        //dg_appendOnloadEvent_appendOnloadEvent in the layout template base
        
        var dg_appendOnloadEvent__onloadEvents = new Array();

        function dg_appendOnloadEvent_appendOnloadEvent(functionName) {//debugger;
            if (!window.onload) {
                window.onload = functionName;
            }
            else {
                if (window.onload != dg_appendOnloadEvent_bindOnloadEvents) {
                    dg_appendOnloadEvent__onloadEvents[dg_appendOnloadEvent__onloadEvents.length]
				= window.onload;
                    window.onload = dg_appendOnloadEvent_bindOnloadEvents;
                }

                dg_appendOnloadEvent__onloadEvents[dg_appendOnloadEvent__onloadEvents.length]
			= functionName;
            }
        }

        function delegateClickEvents() {
            if (getUrlParam(location.href, 'PersistLayoutTemplate') != null) {
                var links = document.getElementsByTagName('a');

                for (var i = 0; i < links.length; i++) {
                    if (links[i].href.indexOf('javascript') == -1) {
                        if (!links[i].onclick) {
                            links[i].onclick = new Function("appendPopupKeyToLink(this);");
                        }
                        else {
                            currentOnClick = links[i].onclick;
                            if (typeof currentOnClick == "function") {
                                currentOnClick = getBodyFromFunction(currentOnClick);
                            }

                            links[i].onclick = new Function("appendPopupKeyToLink(this); " + currentOnClick);
                        }
                    }
                }
            }
        }

        function getUrlParams(url) {
            var queryString = url.split('?');
            if (queryString[1] != null)
                return queryString[1].split('&');
        }

        function getUrlParam(url, paramName) {
            var params = getUrlParams(url);

            if (params == null)
                return null;

            for (i = 0; i < params.length; i++) {
                var param = params[i];

                if (param.indexOf(paramName + '=') == 0) {
                    return param.substr(paramName.length + 1);
                }
            }

            return null;
        }

        function getUrlParams(url) {
            var queryString = url.split('?');
            if (queryString[1] != null)
                return queryString[1].split('&');
        }
    
    
    </script>
    
    </head>
    <body style="margin:0;">
        <form id="Form1" method="post" runat="server">
            <asp:PlaceHolder ID="AppControlPlaceholder" Runat="server" />
            
        </form>
        <asp:PlaceHolder ID="plcFetchbackPixel" Runat="server" />
    </body>
</html>