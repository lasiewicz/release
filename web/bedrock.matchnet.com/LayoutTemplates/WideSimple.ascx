﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WideSimple.ascx.cs" Inherits="Matchnet.Web.LayoutTemplates.WideSimple" %>
<%@ Register TagPrefix="MNFramework" TagName="Right" Src="/Framework/Ui/Right.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Head20" Src="/Framework/Ui/Head20.ascx" %>
<%@ Register TagPrefix="mn1" TagName="TopAuxNav" Src="/Framework/Ui/HeaderNavigation20/TopAuxNav.ascx" %>
<%@ Register TagPrefix="mn" TagName="Header" Src="/Framework/Ui/Header20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="fp" TagName="ForcedPage" Src="/Applications/PremiumServices/Controls/ForcedPage.ascx" %>
<%@ Register TagPrefix="fb" TagName="API" Src="~/Framework/Facebook/FacebookAPI.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register tagPrefix="mn1"  tagName="LivePersonCustomVars" src="/Applications/LivePerson/CustomVariables.ascx"%>
<%@ Register TagPrefix="uc1" TagName="TealiumScript" Src="~/Applications/Tealium/TealiumScript.ascx" %>
<!doctype html>
<!--[if lte IE 7]> <html class="ie ie7 lte9 lte8 lte7 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"> <![endif]--> 
<!--[if IE 8]> <html class="ie ie8 lte9 lte8 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"> <![endif]--> 
<!--[if IE 9]> <html class="ie ie9 lte9 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"> <![endif]--> 
<!--[if (gte IE 10)|!(IE)]><!--><html class="no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"><!--<![endif]--> 
<mn1:Head20 id="Head20" runat="server" />
<%-- attention everyone who will work on this file, we don't want NotificationControlPlaceholder to be declared in WideSimple.ascx.designer.cs but unfortunately VS does it automatically. It screws up Notifications, so  please see if there is a line: protected global::System.Web.UI.WebControls.PlaceHolder NotificationControlPlaceholder; in WideSimple.ascx.designer.cs and comment it. Thanks --%>
<body runat="server" id="bdyMain">
    <uc1:TealiumScript runat="server" ID="TealiumScript" />
<asp:PlaceHolder runat="server" Visible="false" ID="plcLivePerson">
    <mn:Txt ID="txtLivePersonBodyTag" runat="server" ResourceConstant="TXT_LIVEPERSON_BODY_TAG"  />
    <mn1:LivePersonCustomVars runat="server" ID="lpCustomVars"/>
</asp:PlaceHolder>
<fb:API runat="server" ID="fbAPI" visible="false"></fb:API>
<mn:AdUnit id="adSplashOverlay"  visible="false" runat="server" ExpandImageTokens="true" runat="server" ></mn:AdUnit>
<asp:PlaceHolder ID="plcSage" Runat="server" />
<form id="Form1" method="post" runat="server" autocomplete="false">
    <asp:PlaceHolder ID="AppControlPlaceholder" Runat="server" />
</form>
	<asp:PlaceHolder ID="plcPixels" Runat="server" />
	<asp:PlaceHolder ID="plcFetchbackPixel" Runat="server" />
    <script type="text/javascript">
        $j(document).ready(function(){init()});
    </script>
</body>
</html>