﻿

var MemberData = {
    MemberID: '',
    Ordinal: '',
    UserName: '',
    BirthDate: '',
    DisplayAge: '',
    DisplayLocation: '',
    ColorCode: '',
    ColorCodeText: '',
    YNMVote:'',
    EducationLevel: ''
};




function GetMemberByID(memberid, ordinal,successfunction, errorfunction) {

 

    $j.ajax({
        type: "POST",
        url: "/WebServices/Member/MemberService.asmx/GetMemberByID",
        data: "{memberid:" + memberid  + "," + "ordinal:" + ordinal + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout:10000,
        success: function(msg) {
        successfunction(msg);
        },
        error: errorfunction
    });

    return false;
}

function GetYNMMember(memberid, ynmbucket, successfunction, errorfunction) {



    $j.ajax({
        type: "POST",
        url: "/WebServices/Member/MemberService.asmx/GetYNMMember",
        data: "{memberid:" + memberid + "," + "ynmvote:'" + ynmbucket + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 10000,
        success: function(msg) {
            successfunction(msg);
        },
        error: errorfunction
    });

    return false;
}