﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Matchnet.Web.WebServices.Member
{
   
    public class MemberData
    {
      
        public int MemberID { get; set; }

        public int Ordinal { get; set; }
     
        public string UserName { get; set; }

        public string UserNameEllipsed { get; set; }
       
        public DateTime BirthDate { get; set; }
       
        public int Age { get; set; }
       
        public string DisplayLocation { get; set; }

        public string ViewProfileURL { get; set; }

        /// <summary>
        /// Large photos of main approved photos.
        /// </summary>
        public List<string> LargePhotos { get; set; }

        /// <summary>
        /// Thumbnails of main approved photos.
        /// </summary>
        public List<string> ThumbPhotos { get; set; }

        public string ColorCode { get; set; }

        public string ColorCodeText { get; set; }

        public string YNMVote { get; set; }

        public string Seeking { get; set; }

        public string EducationLevel { get; set; }

        /// <summary>
        /// Count of all approved photos, not just main approved. This count wont' match with LargePhotos or ThumbPhotos count.
        /// </summary>
        public int ApprovedPhotosCount { get; set; }

        public static MemberData Parse(Matchnet.Member.ServiceAdapters.Member member)
        {
            MemberData memberData = null;
            if (null != member)
            {
                memberData = new MemberData();
                memberData.MemberID = member.MemberID;
                memberData.LargePhotos = new List<string>();
                memberData.ThumbPhotos = new List<string>();
            }
            return memberData;
        }
    }
}
