﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileDataGroup;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.WebServices.Member
{
    /// <summary>
    /// Summary description for Member
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class MemberService : System.Web.Services.WebService
    {
        private ContextGlobal _g;

        public MemberService()
        {
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                }
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public MemberData GetMemberByID(int memberid, int ordinal)
        {
            MemberData memberData = new MemberData();

            Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);

            memberData.MemberID = member.MemberID;
            DateTime birthDate = member.GetAttributeDate(_g.Brand, "Birthdate");
            memberData.DisplayLocation = ProfileDisplayHelper.GetRegionDisplay(member, _g);
            memberData.UserName = member.GetUserName(_g.Brand);
            memberData.Age = FrameworkGlobals.GetAge(birthDate);
            //memberData.LargePhotos = GetLargePhotoLinks(member);
            memberData.LargePhotos = new List<string>();
            memberData.ThumbPhotos = new List<string>();
            memberData.ApprovedPhotosCount = MemberPhotoDisplayManager.Instance.GetApprovedPhotosCount(member, member,
                                                                                                       _g.Brand);

            GetPhotoLinks(member, memberData.LargePhotos, memberData.ThumbPhotos);
            memberData.Ordinal = ordinal;
            if (ColorCodeHelper.IsColorCodeEnabled(_g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(member, _g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(member, _g.Brand))
            {

                Color color = ColorCodeHelper.GetPrimaryColor(member, _g.Brand);
                memberData.ColorCode = color.ToString().ToLower();

                memberData.ColorCodeText = ColorCodeHelper.GetFormattedColorText(color).ToLower();
            }

            memberData.EducationLevel = new BaseDataGroup().GetDataGroupAttributeDisplayValue(member, "EducationLevel", null, false);

            return memberData;


        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public MemberData GetYNMMember(int memberid, string ynmvote)
        {
            MemberData memberData = GetMemberByID(memberid, 1);
            memberData.YNMVote = ynmvote;
            return memberData;

        }


        private List<string> GetLargePhotoLinks(Matchnet.Member.ServiceAdapters.Member member)
        {
            List<string> photolist = new List<string>();

            try
            {
                ArrayList approvedPhotos = new ArrayList();
                // This must be appended to all photos that hit the file servers in order for the site-specific
                // "No Photo" images to appear.  Appending this to any other photos should not cause errors.
                string siteIDParam = "?" + WebConstants.URL_PARAMETER_NAME_SITEID + "=" + _g.Brand.Site.SiteID.ToString();

                /* Only approved photos should be shown to both viewing member and 
                * the member who owns the photos */
                var photos = MemberPhotoDisplayManager.Instance.GetApprovedPhotos(member, member,
                                                                                  _g.Brand);
                if (photos != null)
                {
                    for (int i = 0; i < photos.Count; i++)
                    {

                        var photo = photos[i];
                        //display large photo
                        //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                        string imageURL = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(member, member, _g.Brand, photo,
                                                                               PhotoType.Full,
                                                                               PrivatePhotoImageType.Full,
                                                                               NoPhotoImageType.Thumb, false);

                        photolist.Add(imageURL);
                    }
                }

                return photolist;
            }
            catch (Exception ex)
            { _g.ProcessException(ex); return photolist; }
        }

        private void GetPhotoLinks(Matchnet.Member.ServiceAdapters.Member member, List<string> largePhotos, List<string> thumbPhotos)
        {
            // List<string> photolist = new List<string>();

            try
            {
                // This must be appended to all photos that hit the file servers in order for the site-specific
                // "No Photo" images to appear.  Appending this to any other photos should not cause errors.
                string siteIDParam = "?" + WebConstants.URL_PARAMETER_NAME_SITEID + "=" + _g.Brand.Site.SiteID.ToString();

                var photos = MemberPhotoDisplayManager.Instance.GetApprovedForMainPhotos(member, member,
                                                                                          _g.Brand);

                if (photos == null)
                    return;

                foreach (var photo in photos)
                {
                    string imageURL = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(member, member, _g.Brand, photo,
                                                                                PhotoType.Full,
                                                                                PrivatePhotoImageType.Full,
                                                                                 false);
                    string thumbUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(member, member, _g.Brand, photo,
                                                                           PhotoType.Thumbnail,
                                                                           PrivatePhotoImageType.Thumb,
                                                                           NoPhotoImageType.Thumb, false);
                    largePhotos.Add(imageURL);
                    thumbPhotos.Add(thumbUrl);
                }

            }
            catch (Exception ex)
            { _g.ProcessException(ex); }
        }

    }
}
