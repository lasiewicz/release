using System;
using System.IO;
using System.Web;
using System.Web.UI;

using Matchnet.Web.Framework;
using Matchnet.Web.Analytics;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Managers;
using Spark.Authentication.OAuth.V2;
using Matchnet.Session.ServiceAdapters;

namespace Matchnet.Web.Lib
{
	/// <summary>
	/// Utility class for main project.
	/// </summary>
	public class Util : FrameworkControl
	{
		/// <summary>
		/// Singleton instance
		/// </summary>
		//public static readonly Util Instance = new Util();

		public Util()
		{
		}
		
		/// <summary>
		/// Inserts notifications into the specific layouttemplate using notificationtemplates.
		/// Copied from legacy Default.aspx.cs
		/// </summary>
		/// <param name="layoutTemplate">The layout template to add notifications to.</param>
		/// <param name="NotificationTemplatePath">The path of the notification template to use for insertin the notification controls.</param>
		public void InsertNotifications(LayoutTemplateBase layoutTemplate, string NotificationTemplatePath)
		{
            try
            {

                if (HttpContext.Current.Request.QueryString["rc"] != null)
                {
                    string rcRequest = HttpContext.Current.Request.QueryString["rc"];
                    string[] resourceConstants = rcRequest.Split(',');
                    string[] rcArguments = null;

                    string resourceConstant = resourceConstants[0];

                    if (null != resourceConstant && resourceConstant.Length > 0)
                    {
                        if (resourceConstants.Length > 1)
                        {
                            rcArguments = new string[resourceConstants.Length - 1];
                            Array.Copy(resourceConstants, 1, rcArguments, 0, resourceConstants.Length - 1);

                            g.Notification.AddMessage(resourceConstant, rcArguments);
                        }
                        else
                        {
                            g.Notification.AddMessage(resourceConstant);
                        }
                    }
                }

                if (g != null)
                {
                    foreach (Notification msg in g.Notification.NotificationList)
                    {
                        string message = String.Empty;
                        string imagePath = String.Empty;
                        string cssClass = String.Empty;

                        if (msg.NotifyType == NotificationType.Error)
                        {
                            // app id = 1
                            // page_error.gif
                            if (msg.Message.Length > 0)
                            {
                                message = msg.Message;
                            }
                            else
                            {
                                if (msg.Args != null)
                                {
                                    message = _g.GetResource(msg.ResourceConstant, msg.Args, true, null);
                                }
                                else
                                {
                                    message = _g.GetResource(msg.ResourceConstant, null, true, null);
                                }
                            }


                            imagePath = Framework.Image.GetURLFromFilename("page_error.gif");

                            if (_g.LayoutTemplate == LayoutTemplate.Standard)
                            {
                                cssClass = "ErrorNoLeftNav";
                            }
                            else
                            {
                                cssClass = "notification error";
                            }

                        }

                        if (msg.NotifyType == NotificationType.Normal)
                        {
                            if (msg.Message.Length > 0)
                            {
                                message = msg.Message;
                            }
                            else
                            {
                                if (msg.Args != null)
                                {
                                    message = _g.GetResource(msg.ResourceConstant, msg.Args, true, null);
                                }
                                else
                                {
                                    message = _g.GetResource(msg.ResourceConstant, null, false, null);
                                }
                            }

                            imagePath = Framework.Image.GetURLFromFilename("page_message.gif");
                            cssClass = "PageMessage notification";
                        }

                        NotificationTemplateBase notificationTemplate;

                        if (System.IO.File.Exists(NotificationTemplatePath))
                        {
                            notificationTemplate = LoadControl(NotificationTemplatePath) as NotificationTemplateBase;
                        }
                        else
                        {
                            //If we can't load the appropriate notification control for this page layout,
                            //just load the default notification control.
                            notificationTemplate = LoadControl(NotificationTemplateBase.DEFAULT_NOTIFICATION_PATH) as NotificationTemplateBase;
                        }

                        notificationTemplate.ImagePath = imagePath;
                        notificationTemplate.Message = message;
                        notificationTemplate.CssClass = cssClass;

                        layoutTemplate.NotificationControlPlaceholder.Controls.Add(notificationTemplate);
                    }
                }
            }
            catch (Exception)
            {
                // Handle pages like ICXML which has no template. 
            }
		}

		public UserControl RenderAnalytics(bool saveSession)
		{
			if (saveSession)
			{
				InitAnalyticsScript();
			}
			return AddAnalyticsScript();
		}

		private void InitAnalyticsScript()
		{
			// Always set page ID, it's used by stuff other than Analytics
			if (g.Analytics == null)
			{
				g.Analytics = new Matchnet.Web.Analytics.Script();
			}

			g.Analytics.SetAttribute(Script.ATTR_PAGE_ID, g.AppPage.ID.ToString());

			
			if (Convert.ToInt32(RuntimeSettings.GetSetting("ENABLE_ANALYTICS" , g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)) == 0) 
			{
				g.Analytics.SetAttribute(Script.ATTR_DISABLE_FLAG, "1");
			}
			else
			{
				if (Convert.ToInt32(RuntimeSettings.GetSetting("ENABLE_ANALYTICSNAMES" , g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)) == 1) 
				{
					g.Analytics.SetAttribute(Script.ATTR_NAMES_FLAG, "1");
				}
				g.Analytics.SetAttribute(Script.ATTR_DOMAIN_ID, g.Brand.Site.Community.CommunityID.ToString());
				g.Analytics.SetAttribute(Script.ATTR_BASEPRIVATELABEL, g.Brand.Site.SiteID.ToString());
				g.Analytics.SetAttribute(Script.ATTR_PRIVATELABEL_ID, g.Brand.BrandID.ToString());
				g.Analytics.SetAttribute(Script.ATTR_SUBSCRIBED_FLAG, (g.Member != null && MemberPrivilegeAttr.IsCureentSubscribedMember(g)) ? "1" : "0");
//				g.Analytics.SetAttribute(Script.ATTR_TARGET_DOMAIN_ID, g.TargetCommunityID.ToString());
//				g.Analytics.SetAttribute(Script.ATTR_TARGET_PRIVATELABEL_ID, g.TargetBrand.BrandID.ToString());
				g.Analytics.SetAttribute(Script.ATTR_DISABLE_FLAG, "0");
				//TODO:  this one is analytics???
//				g.Analytics.SetAttribute(Script.ATTR_TARGET_MEMBER_ID, g.TargetMemberID.ToString());

				if (g.Member != null) 
				{
					g.Analytics.SetAttribute(Script.ATTR_MEMBER_ID, g.Member.MemberID.ToString());
					g.Analytics.SetAttribute(Script.ATTR_GENDER_MASK,g.Member.GetAttributeInt(g.Brand,"GenderMask",Constants.NULL_INT).ToString());			
					g.Analytics.SetAttribute(Script.ATTR_BIRTH_DATE, g.Member.GetAttributeDate(g.Brand, WebConstants.ATTRIBUTE_NAME_BIRTHDATE,DateTime.MinValue).ToString());
				}
				else 
				{
					g.Analytics.SetAttribute(Script.ATTR_MEMBER_ID, "0");
					g.Analytics.SetAttribute(Script.ATTR_GENDER_MASK,"0");			
					g.Analytics.SetAttribute(Script.ATTR_BIRTH_DATE, DateTime.MinValue.ToString());
				}
			}
		}

		private UserControl AddAnalyticsScript()
		{
			Matchnet.Web.Analytics.Script script = g.Analytics as Matchnet.Web.Analytics.Script;
//			script.ID = "Analytics";
			return script;
		}

		public void PrivilegeRedirect(string pagePath) 
		{
			// check login
			bool requiresLogin = ((_g.AppPage.SecurityMask & SecurityMask.RequiresLogin) == SecurityMask.RequiresLogin);
			bool requiresPrivilege = ((_g.AppPage.SecurityMask & SecurityMask.RequiresPrivilege) == SecurityMask.RequiresPrivilege);
			
			if(Context.Request.QueryString.Count > 0) 
			{
				pagePath += "?" + Context.Request.QueryString;
			} 

            // Old AutoLogin and SUA is turned off (which is never now..)
			if (AutoLoginHelper.IsSettingEnabled(_g) && !LogonManager.IsSuaLogon(_g))
			{
				if ( _g.Member == null)
				{
					// RegStep1 was initially required as a hard login page, but the configuration for this page does not require login or privileges so it was removed as a hard loginpage.
					 if (!g.AutoLogin.AutoLogin()) 
					{
						if (requiresLogin || requiresPrivilege)
						{
							_g.LogonRedirect(pagePath);
						}
					}
				}
			}
			else if (LogonManager.IsSuaLogon(g))
			{
                // If it's a null session (no member), try Auto Login using the stored access token
			    if ((_g.Member == null) 
                    // do trigger autologin even for the logon page
                    // && (pagePath.ToLower().IndexOf("logon/logon.aspx", StringComparison.Ordinal) == -1)
                    // no need to trigger auto login with checkim is the caller results in addtional calls
			        && (pagePath.ToLower().IndexOf("checkim.aspx", StringComparison.Ordinal) == -1))
			    {
                    // TODO FAWAD(what was it that you needed to do? comment)
                    var lm = new LogonManager(g) {PreserverRedirectURLForAutoLogin = pagePath};
			        var logonManager = new SuaLogonManager(g.Brand.BrandID, lm);
			        if (!logonManager.AutoLogin())
			        {
                        // if SUA auto login fails, send request to the logon page
			            if ((requiresLogin || requiresPrivilege))
			            {
			                _g.LogonRedirect(pagePath);
			            }
			        }
			    }
			}
			else
			{
				if ((requiresLogin || requiresPrivilege) && _g.Member == null)
				{
					_g.LogonRedirect(pagePath);
				}
			}

			// check privileges
			if (requiresPrivilege)
			{
				if (g.IsAdmin)
				{
					if (!HasPrivilege())
					{
						_g.Transfer(
							FrameworkGlobals.LinkHref("/Applications/Article/Article.aspx?resourceid=" + 
							_g.GetResource("YOU_ARE_NOT_AUTHORIZED_TO_SEE_THIS_PAGE", null),true));
					}
				}
				else
				{
					_g.LogonRedirect(pagePath);
				}
			}
		}

		public void DetectRemoteRegistration()
		{
			//Note: This is necessary for legacy external registration pages that use ?a=7500 on the URL to redirect to registration
			//New external registration forms should point directly to /Applications/MemberProfile/RegistrationStep1.aspx

			//TT17036 - check for RemoteReg to persist the "RegistrationStep1_Remote.ascx" control render
			if (Conversion.CInt(HttpContext.Current.Request["a"]) == (int) ContextGlobal.Pages.ACTION_MEMBER_PROFILE_SAVE_STEP_1 || HttpContext.Current.Request["UserName"] != null || HttpContext.Current.Request["RemoteReg"] != null)
			{
				g.IsRemoteRegistration = true;
			}
		}

		private bool HasPrivilege()
		{
			//todo: GP awaiting implementation in service
			//todo: TEMPORARY
			//return Member.HasPrivilege(_g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), (int)_g.AppPage.SecurityMask, _g.Member != null && _g.Member.IsAdmin);
			
			return true;
		}

        public void BetaRedirect(string pagePath)
        {
            if (LogonManager.IsBetaRedesignEnabled(_g))
            {
                var canRedirect = (pagePath.ToLower().IndexOf("checkim.aspx", StringComparison.Ordinal) == -1)
                                  && (pagePath.ToLower().IndexOf("logon", StringComparison.Ordinal) == -1)
                                  && (pagePath.ToLower().IndexOf("applications/api", StringComparison.Ordinal) == -1);
                if ((canRedirect && _g.Member != null) && _g.Member.GetAttributeBool(_g.Brand, "REDRedesignBetaParticipatingFlag"))
                {
                    //Get setting and redirect (if setting is not empty value)
                    var redirectUrl = LogonManager.GetBetaRedesignUrl(_g, "home");
                    if (!string.IsNullOrEmpty(redirectUrl.Trim()) && redirectUrl.ToLower() != "dev.jdate.com")
                    {
                        LogonManager logonManager = new LogonManager(_g);
                        logonManager.CreateAndStoreBetaFlag(true);

                        //remove mnc5 (bedrock session cookie)
                        HttpCookie mnc5Cookie = HttpContext.Current.Request.Cookies.Get(SessionSA.Instance.GetCookieName(_g.IsDevMode));
                        if (mnc5Cookie != null)
                        {
                            if (_g.Session != null)
                            {
                                _g.Session.Clear();
                            }
                            mnc5Cookie.Expires = DateTime.Now.AddDays(-1);
                            HttpContext.Current.Response.Cookies.Add(mnc5Cookie);
                        }

                        //_g.Transfer(redirectUrl); //This method is not going to work as expected. So we need to just call ResponseRedirect directly istead of using helper. 

                        //add current URL as a query parameter, which will be mapped within Norbert to the right path
                        if (redirectUrl.IndexOf("?") < 0)
                        {
                            redirectUrl = redirectUrl + "?";
                        }
                        redirectUrl = redirectUrl + String.Format("redirecturl={0}", HttpUtility.UrlEncode(HttpContext.Current.Request.RawUrl));
                        g.LoggingManager.LogInfoMessage("Util", "BetaRedirect to " + redirectUrl);

                        HttpContext.Current.Response.Redirect(redirectUrl, true);
                    }
                }
            }
        }
	}
}
