﻿/**** variables reset ********/
//curent step
var step = 0;
var totalSteps = 0;
var currentStepElements = 0;
var elementsChange = 0;
var isNextBtnVisiable = false;
var isBackBtnVisiable = false;

var isBackBtnEnable = false;
var isNextBtnEnable = false;
var curentStepId = "step_0";
var varLookupCityLinks;
var omnitureFlag = false;
var birthdateValidatorCounter = 3;
var cookieName = "REG091202";

//is back button clicked
var isBackClick = false;

//is next button clicked
var isNextClick = false;

//is overlay box is open
var isOpen = false;

//move to next step
var isNextStep = false;

//gender of the user
var gender = 1;

//iOS detection
var iOS = false;
if (navigator.userAgent.match(/iP(ad|hone|od)/i)) {
    iOS = true;
}

//select values
var values = {};

var nextBtnIsDisabled = false;

var generalErrorFlag = false;

var generalErrorHeight = 40;

var lastStepFlag = false;

var ajaxTimeout = 300000;

var defaultContentHeight = 0;

var originalOmnitureSEvents = s.events;

var sendRegEventOnInputFocus = "notsent";

var sessionKeepAliveInterval;

var src = "";

function getUrlParams(url) {
    var queryString = url.split('?');
    if (queryString[1] != null)
        return queryString[1].split('&');
}

function getUrlParamsString(url) {
    var queryString = url.split('?');
    if (queryString[1] != null)
        return queryString[1];
    else {
        return "";
    }
}

function getUrlParam(url, paramName) {
    var params = getUrlParams(url);

    if (params == null)
        return null;

    for (i = 0; i < params.length; i++) {
        var param = params[i];

        if (param.indexOf(paramName + '=') == 0) {
            return param.substr(paramName.length + 1);
        }
    }

    return null;
}

function raiseOmnitureEvent7(element) {
    if (!omnitureFlag && readCookie("event7Fired") == null) {
        s.events = "event7";
        s.t();
        omnitureFlag = true;
        createCookie("event7Fired", "true", 100);
    }
}

//validate element return false if on error
function validateElement(element, isValidateForm) {
    //if in Ynet iFrame - show password field
    //showPasswordField();

    if (jQuery(element).is(":visible")) {
        var elementErrorFlag = false;
        var name = jQuery(element).attr('name');
        if (jQuery(element).hasClass("select-birthdate")) {
            jQuery(".select-birthdate").each(function () {
                if (validator[jQuery(this).attr('name')]) {
                    name = jQuery(this).attr('name');
                    return false;
                }
            });
        }
        if (validator[name]) {
            for (key in validator[name]) {
                var value = jQuery(element).val();
                if (jQuery(element).is(':checkbox')) {
                    value = jQuery(element).is(':checked') ? "1" : "";
                }
                switch (validator[name][key].errorKey) {
                    case "required":
                        if (!validateRequired(value)) {
                            displayError(element, validator[name][key].message);
                            elementErrorFlag = true;
                        }
                        break;
                    case "email":
                        if (!validateEmail(value)) {
                            displayError(element, validator[name][key].message);
                            elementErrorFlag = true;
                        }
                        break;
                    case "minlength":
                        if (!validateMinLength(value, validator[name][key].value)) {
                            displayError(element, validator[name][key].message);
                            elementErrorFlag = true;
                        }
                        break;
                    case "maxlength":
                        if (!validateMaxLength(value, validator[name][key].value)) {
                            displayError(element, validator[name][key].message);
                            elementErrorFlag = true;
                        }
                        break;
                    case "wrongBirthDate":
                        if (!validateBirthDate(value, validator[name][key].value, isValidateForm)) {
                            displayError(element, validator[name][key].message);
                            elementErrorFlag = true;
                        }
                        break;
                    case "wrongCity":
                        var country = jQuery(".Region").children().find(".field").filter(":first").children().val();
                        if (!validateCity(value, validator[name][key].value) && (siteId == 4 || siteId == 15) && country == 105) {
                            displayError(element, validator[name][key].message);
                            elementErrorFlag = true;
                        }
                        break;
                    case "password":
                        if (!validatePassword(value, validator[name][key].value)) {
                            displayError(element, validator[name][key].message)
                            elementErrorFlag = true;
                        }
                        break;
                }
                if (elementErrorFlag) {
                    break;
                }
            }
            if (!elementErrorFlag) {
                clearError(element)
            }
        }
    }
    return elementErrorFlag;
}

//validate wrong password
function validatePassword(value, length) {
    var reg = /^[a-zA-Z0-9א-ת]{4,15}$/;
    if (reg.test(value) == false) {
        return false;
    }
    return true;
}

//validate city

//check if user entered birthdate
function validateBirthDate(value, length, isValidateForm) {
    errorFlag = true;
    counter = 0;
    jQuery(".select-birthdate").each(function () {
        if (isNaN(jQuery(this).val()) == true) {
            errorFlag = false;
            counter++;
        }
    });
    if (!isValidateForm) {
        if (birthdateValidatorCounter == counter && !errorFlag) {
            return false;

        }
        return true;
    }
    return errorFlag;

}

//validate max length
function validateMaxLength(value, length) {
    if (value.length > length) {
        return false;
    }
    return true;
}

//validate min length
function validateMinLength(value, length) {
    if (value.length < length) {
        return false;
    }
    return true;
}

function validateRequired(value) {
    if (value == "" || value.length == 0 || value == " ") {
        return false;
    }
    return true;
}

function validateEmail(email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(email) == false) {
        return false;
    }
    return true;
}

function validateCity(value, length) {
    if (citiesArr[value]) {
        return true;
    }
    return false;

}

function documentReady() {
    //if in Ynet iFrames
    if (typeof ShowLabelInsideInput === 'undefined') {
        //do nothing
    } else {
        if (ShowLabelInsideInput == true) {
            jQuery('#logo_link').css('display', 'block');
            jQuery('#logo_link').click(function () { window.location.href = "http://www.jdate.co.il"; });

            var pwdInput = '#_ctl0__ctl2__ctl0__ctl0_idPassword_AttributeTextControl';
            var pwdText = jQuery.trim(jQuery(pwdInput).parent().prev().text());
            jQuery(pwdInput).after('<input type="text" value="' + pwdText + '" name="password_text" id="password_text" style="display:block;"/>');
            jQuery(pwdInput).css('display', 'none');
            jQuery('#password_text').focus(function () {
                showPasswordField();
                jQuery(pwdInput).focus();
            });

            //if input value is default - set value to empty string
            jQuery('.mandatory').focus(function () {
                var defaultVals = jQuery(this).val();
                if (defaultVals == jQuery.trim(jQuery(this).parent().prev().text())) {
                    jQuery(this).val("");
                }
            });
        }
    }

    readCookiesData();

    jQuery('.reg-overlay-osr .mandatory,.reg-overlay-osr .optional').keypress(function (e) {
        if (e.keyCode == 13) {
            hideCityList();
        }
    });

    jQuery(".overlayBox input,.overlayBox select").focusin(function () { raiseOmnitureEvent7(this); });


    //jQuery(".autocomplete").focusout(function() { hideCityList(); });

    //hide city list when focusin on countries list
    jQuery(".Region").children("div").filter(":first").children(".field").children("select").focusin(function () { hideCityList(); });
    jQuery(".autocomplete").autocompleteArray(
		    citiesArr,
		{
		    delay: 10,
		    minChars: 1,
		    matchSubset: 1,
		    onItemSelect: selectItem,
		    onFindValue: findValue,
		    autoFill: true,
		    maxItemsToShow: 6,
		    regionIdInput: "regionid",
		    selectWidth: "143px",
		    selectHeight: "130px"
		}
	);


    /***** refresh capcha ******/
    jQuery(".refresh-capcha, #refresh-capcha").click(function () {
        jQuery.ajax({
            type: "POST",
            url: "/Applications/API/Captcha.asmx/RefreshCaptcha",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            timeout: 10000,
            success: function (msg) {
                var tempSrc;
                if (src == "") {
                    src = jQuery(".capcha-image").attr("src");
                }
                tempSrc = src;
                var rand_no = Math.random();
                rand_no = rand_no * 999999;

                tempSrc += "&" + rand_no;
                jQuery(".capcha-image").attr("src", tempSrc);
            }
        });
    });

    jQuery('.reg-overlay-osr .Region input.mandatory.autocomplete').keyup(function () {
        if (jQuery.osr.isClicked) {
            checkData();
        }
    });

    //jQuery('#TNCInput input').change(function () {
    //    if (jQuery(this).is(':checked')) {
    //        disableEnableNextBtn(false);
    //        return false;
    //    }
    //    else {
    //        disableEnableNextBtn(true);
    //        return false;
    //    }
    //});
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookiesData() {
    var rem = readCookie("remember");
    if (rem == "yes") {
        //check if this elements exists
        if (document.getElementById("remember")) {
            document.getElementById("remember").checked = true;
        }
        if (document.getElementById("email_login")) {
            document.getElementById("email_login").value = readCookie("username2");
        }
    }
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function amAddToUrl() {
    var amEmail = document.getElementsByName("email_login")[0].value;
    var amPass = document.getElementsByName("password_login")[0].value;

    var newUrl = window.location.href.substring(0, window.location.href.lastIndexOf('/') + 1) + "applications/logon/logon.aspx?emailaddress=";
    newUrl = newUrl + amEmail + "&password=" + amPass;
    window.location = newUrl;
}


function displayLoader() {
}

jQuery(document).ready(function () {
    setTimeout("documentReady()", 400);
});

function hideCityList() {
    if (jQuery(".ac_results").is(":visible")) {
        jQuery(".regionid").val(citiesArr[jQuery(".autocomplete").val()]);

        jQuery(".ac_results").hide();
    }
}

function findValue(li) {
    if (li == null) return false;

    // if coming from an AJAX call, let's use the CityId as the value
    if (!!li.extra) var sValue = li.extra[0];

    // otherwise, let's just display the value in the text box
    else var sValue = li.selectValue;

}

function selectItem(li) {
    findValue(li);
}

function formatItem(row) {
    return row[0] + " (id: " + row[1] + ")";
}

function lookupLocal() {
    var oSuggest = jQuery("#CityLocal")[0].autocompleter;
    oSuggest.findValue();

    return false;
}


//callback from change country/region
function autocompleteCities() {
    documentReady();
}
//before callback from change country/region
function BeforeRegionCallback() {
    displayLoader();
}

var citiesArr = {};

function Replace(str, substring, newstring) {
    if (str.length <= 0)
        return "";

    temp = "" + str;

    while (temp.indexOf(substring) > -1) {
        pos = temp.indexOf(substring);
        temp = "" + (temp.substring(0, pos) + newstring + temp.substring((pos + substring.length), temp.length));
    }
    return temp;
}

function ReplaceSingle(str, substring, newstring) {
    if (str.length <= 0)
        return "";

    temp = "" + str;

    if (temp.indexOf(substring) > -1) {
        pos = temp.indexOf(substring);
        temp = "" + (temp.substring(0, pos) + newstring + temp.substring((pos + substring.length), temp.length));
    }
    return temp;
}

/**** handle overlay  ********/

//function to display the box
function showOverlayBox() {
    //if box is not set to open then don't do anything
    if (isOpen == false) return;
    // set the properties of the overlay box, the left and top positions
    topToBePositive = (jQuery(window).height() - jQuery('.overlayBox').height()) / 2 - 20
    if (topToBePositive < 0) { topToBePositive = 0 }
    jQuery('.overlayBox').css({
        display: 'block',
        left: (jQuery(window).width() - jQuery('.overlayBox').width()) / 2,
        top: topToBePositive,
        position: 'absolute'
    });
    // set the window background for the overlay. i.e the body becomes darker
    jQuery('.bgCover').css({
        display: 'block',
        width: jQuery(document).width(),
        height: jQuery(document).height()
    });
}
function doOverlayOpen() {
    //set status to open
    isOpen = true;
    showOverlayBox();

    // dont follow the link : so return false.
    return false;
}
function coverBody() {
    if ($j.browser.msie && parseInt($j.browser.version) <= "8") {
        jQuery('.bgCover').css({ opacity: 0.5, backgroundColor: '#888888' });
    }
    else {
        jQuery('.bgCover').css({ opacity: 0.5, backgroundColor: '#000' });
    }

}

function showOverLayBox() {
    jQuery(window).bind('resize', showOverlayBox);
    doOverlayOpen();
}

function doOverlayClose() {
    //set status to closed
    isOpen = false;
    jQuery('.overlayBox').css('display', 'none');
    // now animate the background to fade out to opacity 0
    // and then hide it after the animation is complete.
    jQuery('.bgCover').animate({ opacity: 0 }, null, null, function () { jQuery(this).hide(); });
}



/***  check if user complete current step ***/
function checkInputStatus() {
    jQuery('#' + curentStepId + ' .mandatory').each(function (index) {
        if (jQuery(this).val() != null && jQuery(this).val() != "") {
            isNextStep = true;
        }
        else {
            isNextStep = false;
            return false;
        }
    });
    if (isNextStep) {
        if (elementsChange != 0 && elementsChange != currentStepElements) {
            isNextStep = false;
        }
    }
}

//get gender from cookie
function getGender() {
    cookie = getCookie();
    if (cookie != undefined && cookie["GenderMask"] != undefined) {
        return cookie["GenderMask"] & 1;
    }
    return gender;
}

//move to next step
function nextStep() {
    sendData();
}

//check if move to next step
function checkIfNextStep() {
    if (isStepCompleted()) {
        sendData();
        /*                    
        if (step < totalSteps) {
        step++;
        } */
    }
}

function IsNumeric(input) {
    return (input - 0) == input && input.length > 0;
}

//check if step completed
function isStepCompleted() {
    var completed = true;
    curentStepId = "step_" + step;

    if (jQuery('#' + curentStepId + ' .mandatory').is("select")) {
        jQuery('#' + curentStepId + ' .mandatory').each(function (index) {
            if (IsNumeric(jQuery(this).val())) {
                completed = true;
            }
            else {
                completed = false;
                return false;
            }
        });
    }
    else {
        jQuery('#' + curentStepId + ' .mandatory').each(function (index) {
            if (jQuery(this).is(':checkbox')) {
                if (jQuery(this).is(':checked')) {
                    completed = true;
                }
                else {
                    completed = false;
                    return false;
                }
            }
            else if (jQuery(this).val() != null && jQuery(this).val() != "") {
                completed = true;
            }
            else {
                completed = false;
                return false;
            }

        });
    }
    if (elementsChange != 0 && elementsChange != currentStepElements) {
        return false;
    }
    return completed;
}



function stepLoad() {
    //set vars
    gender = getGender();
    curentStepId = "step_" + step;

    isBackClick = false;
    isNextClick = false;

    //hide general error message

    if (generalErrorFlag) {
        jQuery('#overlay-error').addClass("hide");
        jQuery('#overlay-content').height(defaultContentHeight);
        generalErrorFlag = false;
    }

    //show different button in last step
    if (step == totalSteps - 1) {
        jQuery("#next-button").hide();
        jQuery("#regconfirm-button").show();
    }
    else {
        jQuery("#next-button").show();
        jQuery("#regconfirm-button").hide();
    }

    //hide save details step
    if (!lastStepFlag) {
        jQuery("#overlay-content").show();
        jQuery("#loading-next-bar").show();
        jQuery("#overlat-save-details").hide();
        jQuery("#overlat-save-details").addClass("hide");
        jQuery("#progressbar").show();
        //jQuery("#back").show();
        jQuery("#save-details-loader").hide();

    }

    if (typeof nextButtonText != 'undefined') {
        jQuery("#nextBtn").text(nextButtonText);
    }

    if (step == 0) {
        isBackBtnEnable = false;
        isBackBtnVisiable = false;
        isNextBtnVisiable = true;
        if (typeof firstNextButtonText != 'undefined') {
            jQuery("#nextBtn").text(firstNextButtonText);
        }
    }
    else if (step == totalSteps) {
        isNextBtnVisiable = false;
    }
    else {
        isBackBtnEnable = true;
        isBackBtnVisiable = true;
        isNextBtnVisiable = true;
    }
    //count how much elements in step
    currentStepElements = 0;
    jQuery('#' + curentStepId + ' .mandatory').each(function (index) {
        currentStepElements++;
    });
    elementsChange = 0;


    //checkIfNextStep();

    isBackClick = false;

    //control progress bar            
    var progressBarVal = ((step) / totalSteps) * 100;
    if (progressBarVal > 100) progressBarVal = 100;
    jQuery("#progressbar").progressbar({ value: progressBarVal });
    displayStep();
    hideElements();
}

//reades cookie information and set values to the elements
function getCookie() {
    var cookieVal = jQuery.cookie(cookieName);
    if (cookieVal != null) {
        var nvPairs = cookieVal.split("&");


        var cookieValues = {};
        for (i = 0; i < nvPairs.length; i++) {
            var nvPair = nvPairs[i].split("=");

            var name = nvPair[0];
            var value = nvPair[1];
            cookieValues[name] = value;
        }
    }

    return cookieValues;
}

function buildRequest() {
    curentStepId = "step_" + step;
    var data = {};
    jQuery('#' + curentStepId + ' .mandatory,#' + curentStepId + ' .optional').each(function () {
        if (jQuery(this).is(':checkbox')) {
            if (jQuery(this).is(':checked')) {
                data[jQuery(this).attr('name')] = jQuery(this).val();
            }
        }
        else if (jQuery('#' + curentStepId + ".aboutme .steptip").html() != null && jQuery('#' + curentStepId + ".aboutme .steptip").html().indexOf(jQuery('#' + curentStepId + " textarea").val()) != -1) {
            data[jQuery(this).attr('name')] = "";
        }
        else if (jQuery(this).children().is(':checkbox')) {
            jQuery(this).children().each(function () {
                if (jQuery(this).is(':checkbox')) {
                    if (jQuery(this).is(':checked')) {
                        data[jQuery(this).attr('name')] = jQuery(this).val();
                    }
                }
            });
        }
        else if (jQuery(this).val() != null && jQuery(this).val() != "") {

            var val = jQuery(this).val();
            if (jQuery(this).not('multiple') && jQuery.isArray(val)) {
                val = sum(val);
            }
            data[jQuery(this).attr('name')] = val;
        }
        else {
            data[jQuery(this).attr('name')] = ""
        }
    });

    data["stepID"] = step;

    return data;
}

sum = function (o) {//function to sum an array
    var summedValue = 0;
    for (var i = 0; i < (o.length); i++) {
        summedValue = summedValue + parseInt(o[i]);
    }
    return summedValue;
};

//disable next button if flag==true
function disableEnableNextBtn(flag) {
    if (!flag) {
        jQuery('#next').removeAttr('disabled');
        jQuery('.overlay-next-button').removeClass("disabled");
        jQuery('.overlay-next-button').removeAttr('disabled');
        jQuery('#back').removeAttr('disabled');
        nextBtnIsDisabled = false;
    }
    else {
        jQuery('#next').attr('disabled', 'disabled');
        jQuery('.overlay-next-button').addClass("disabled");
        jQuery('.overlay-next-button').attr('disabled', 'disabled');
        jQuery('#back').attr('disabled', 'disabled');
        nextBtnIsDisabled = true;
    }
}

function showSaveDetailsStep() {
    jQuery("#overlay-content").hide();
    jQuery("#loading-next-bar").hide();
    jQuery("#overlat-save-details").show();
    jQuery("#overlat-save-details").removeClass("hide");
    jQuery("#progressbar").hide();
    //jQuery("#back").hide();
    jQuery("#save-details-loader").show();
}

function handelGeneralError() {
    generalErrorFlag = true;
    if (defaultContentHeight == 0) {
        defaultContentHeight = jQuery('#overlay-content').height();
    }
    else {
        jQuery('#overlay-content').height(defaultContentHeight);
    }
    jQuery('#overlay-content').height(jQuery('#overlay-content').height() - generalErrorHeight);
    jQuery('#overlay-error').removeClass("hide");
    jQuery('#overlay-error').show();
    if (lastStepFlag) {
        lastStepFlag = false;
        stepLoad();
    }
    jQuery('#overlay-error').remove();
}

//if cities list is visible hide it and choose current city
function hideCityList(chooseCityFlag) {
    if (jQuery(".ac_results").is(":visible")) {
        if (chooseCityFlag) {
            jQuery(".regionid").val(citiesArr[jQuery(".autocomplete").val()]);
        }
        else {
            jQuery(".ac_input").val("");
        }
        jQuery(".ac_results").hide();
    }
}

function sendData() {
    var photoFlag = false;
    jQuery('#' + curentStepId + ' .mandatory' + ',' + '#' + curentStepId + ' .optional').each(function () {
        if (jQuery(this).is(".photo")) {
            photoFlag = true;
        }
    });

    //hide general error
    if (!jQuery('#overlay-error').hasClass("hide")) {
        jQuery('#overlay-error').addClass("hide");
        jQuery('#overlay-error').hide();
        jQuery('#overlay-content').height(defaultContentHeight);
    }

    //if cities list is visible hide it and choose current city

    var data = buildRequest();

    var param = "";
    if (isBackClick) {
        param = "backstep=1";
    }
    else {
        param = "checkstep=1";
    }
    removeErrors();


    disableEnableNextBtn(true);
    jQuery.ajaxSettings.traditional = true;
    generalErrorFlag = false;


    //regular ajax call
    if (!photoFlag) {
        jQuery.ajax({
            type: "POST",
            url: window.location.pathname + "?" + getUrlParamsString(location.href) + "&" + param + "&regabtest=" + getUrlParam(location.href, 'regabtest'),
            data: data,
            timeout: ajaxTimeout,
            success: function (response) {
                try {
                    var obj = jQuery.parseJSON(response);
                    if (obj.status == "ok") {
                        if (step < totalSteps) {
                            step++;
                        }
                        if (obj.refreshSearchResults == "true") {
                            //refresh search results
                            RegistrationOverlay_RefreshSearchResults();
                        }
                        stepLoad();
                        disableEnableNextBtn(false);
                    }
                    else if (obj.status == "complete") {
                        //show save details step
                        if (step == totalSteps - 1) {
                            showSaveDetailsStep();
                            lastStepFlag = true;
                        }
                        else {
                            lastStepFlag = false;
                        }
                        if (obj.url.indexOf("?") !== -1) {
                            window.location = obj.url + '&FromRegOverlay=1';
                        }
                        else {
                            window.location = obj.url + '?FromRegOverlay=1';
                        }
                    }
                    else {
                        if (obj.status == "errors") {
                            if (lastStepFlag) {
                                lastStepFlag = false;
                                stepLoad();
                            }
                            handelErrors(obj);
                        }
                        else {
                            handelGeneralError();
                        }
                        disableEnableNextBtn(false);
                    }
                }
                catch (ex) {
                    if (response.indexOf("registrant") != -1) {
                        url = window.location.toString();
                        url = url.toLowerCase();
                        var registrationSuccessUrl = url.split("/applications")[0] + "/Applications/MemberProfile/RegistrationWelcome.aspx?registrationMode=1";
                        window.location = registrationSuccessUrl;
                    }
                    else {
                        handelGeneralError();
                        jQuery('.error#overlay-error').remove();
                        disableEnableNextBtn(false);
                    }
                }
            },
            error: function (response, status, e) {
                handelGeneralError();
                jQuery('.error#overlay-error').remove();
                disableEnableNextBtn(false);
            }
        });
    }
    //photo upload ajax call
    else {
        jQuery('#' + curentStepId + ' .mandatory' + ',' + '#' + curentStepId + ' .optional').each(function () {
            if (jQuery(this).is(".photo")) {
                jQuery.ajaxFileUpload
                        (
	                        {
	                            url: window.location.pathname + "?" + getUrlParamsString(location.href) + "&" + param + "&stepID=" + step,
	                            secureuri: false,
	                            fileElementId: jQuery(this).attr('id'),
	                            data: data,
	                            dataType: 'json',
	                            timeout: ajaxTimeout,
	                            success: function (obj, status) {
	                                if (obj.status == "ok") {
	                                    if (step < totalSteps) {
	                                        step++;
	                                    }
	                                    stepLoad();
	                                    disableEnableNextBtn(false);
	                                }
	                                else if (obj.status == "complete") {
	                                    window.location = obj.url;
	                                }
	                                else {
	                                    if (obj.status == "errors") {
	                                        handelErrors(obj);
	                                    }
	                                    else {
	                                        handelGeneralError();
	                                    }
	                                    disableEnableNextBtn(false);
	                                }
	                            },
	                            error: function (response, status, e) {
	                                handelGeneralError();
	                                disableEnableNextBtn(false);
	                            }
	                        }
                        )
            }
        });
    }
}

function checkData() {
    var data = buildRequest();

    var param = "";
    if (isBackClick) {
        param = "backstep=1";
    }
    else {
        param = "checkstep=1";
    }

    disableEnableNextBtn(true);
    jQuery.ajaxSettings.traditional = true;
    generalErrorFlag = false;
    jQuery.ajax({
        type: "POST",
        url: window.location.pathname + "?" + getUrlParamsString(location.href) + "&ValidateOnly=1&regabtest=" + getUrlParam(location.href, 'regabtest'),
        data: data,
        timeout: ajaxTimeout,
        success: function (response) {
            try {
                var obj = jQuery.parseJSON(response);
                if (obj.status == "errors") {
                    handelErrors(obj);
                    disableEnableNextBtn(false);
                }
                else if (obj.status == "ok") {
                    removeErrors();
                    disableEnableNextBtn(false);
                }
            }
            catch (ex) {
                if (response.indexOf("registrant") != -1) {
                    url = window.location.toString();
                    url = url.toLowerCase();
                    var registrationSuccessUrl = url.split("/applications")[0] + "/Applications/MemberProfile/RegistrationWelcome.aspx?registrationMode=1";
                    window.location = registrationSuccessUrl;
                }
                else {
                    handelGeneralError();
                    jQuery('.error#overlay-error').remove();
                    disableEnableNextBtn(false);
                }
            }
        },
        error: function (response, status, e) {
            handelGeneralError();
            jQuery('.error#overlay-error').remove();
            disableEnableNextBtn(false);
        }
    });
}

function handelErrors(obj) {
    removeErrors();
    for (var key in obj.errors) {
        if (obj.errors[key].msg.indexOf("#") != -1) {
            var text = obj.errors[key].msg.split("#");
            jQuery("#" + obj.errors[key].id).after("<div class='error'>" + text[gender] + "</div>");
            var regHeight = jQuery(".reg-overlay-osr").css('height');
            jQuery('.overlayBox.osr-cont').css({ height: parseInt(regHeight) + 175 });
            jQuery('.error#overlay-error').remove();
        }
        else {
            jQuery("#" + obj.errors[key].id).after("<div class='error'>" + obj.errors[key].msg + "</div>");
            var regHeight = jQuery(".reg-overlay-osr").css('height');
            jQuery('.overlayBox.osr-cont').css({ height: parseInt(regHeight) + 175 });
            jQuery('.error#overlay-error').remove();
        }
        jQuery('.error#overlay-error').remove();
        if (jQuery('.Region input.mandatory').val() > '') {
            if (validateCity(jQuery('.Region input.mandatory').val())) {
                jQuery('.Region').next('.error').remove();
            }
        }
    }
}

function removeErrors() {
    jQuery('#' + curentStepId + ' .error').remove();
}

jQuery(document).ajaxStart(function () {
    jQuery("#ajax-loader").show();
});

jQuery(document).ajaxStop(function () {
    if (lastStepFlag) {
        jQuery("#save-details-loader").hide();
    }
    else {
        jQuery("#ajax-loader").hide();
    }
});

jQuery(document).ready(function () {
    jQuery.osr = {};
    jQuery.osr.isClicked = false;
    coverBody();
    //jQuery('#min-max-container select').css("visibility", "hidden");

    //if iOS, apply spark_fixIOSSelectBug to fix select bug
    if (iOS) {
        spark_fixIOSSelectBug($j('select'));
    }

    if (!iOS) {
        jQuery('select.mandatory,input.mandatory').change(function () {
            elementsChange++;
            if (isStepCompleted()) {
                isNextClick = true;
            }
            checkIfNextStep();
        });

        jQuery('select.mandatory').keyup(function () {
            elementsChange++;

            if (isStepCompleted()) {
                isNextClick = true;
            }
            checkIfNextStep();
        });

        jQuery('.reg-overlay-osr select.mandatory, .reg-overlay-osr input.mandatory').change(function () {
            if (jQuery.osr.isClicked) {
                briefValidate(jQuery(this));
            }
        });

        jQuery('.reg-overlay-osr select.mandatory').keyup(function () {
            if (jQuery.osr.isClicked) {
                briefValidate(jQuery(this));
            }
        });
    }
    // Checks if reg start omniture event should be triggered once a control is clicked
    jQuery("#overlay-content input,#overlay-content select").focusin(function () {
        if (sendRegEventOnInputFocus == "notsent") {
            sendRegEventOnInputFocus = "send"
            omniture();
        }

    });

    /*** move next step if click enter ****/
    jQuery(document).keypress(function (event) {
        if (!jQuery('.aboutme').is(':visible') && event.keyCode == '13') {
            event.preventDefault();
            jQuery(".overlay-next-button").click();
        }
    });

    jQuery('input').keypress(function (event) {
        if (event.keyCode == '13') {
            event.preventDefault();
            jQuery(".overlay-next-button").click();
        }
    });
    /*************/

    jQuery("#back").click(function () {
        if (!nextBtnIsDisabled) {
            isBackClick = true;
            if (step > 0) {
                step--;
            }
            hideCityList(false);
            lastStepFlag = false;
            stepLoad();
        }
    });

    jQuery(".overlay-next-button").click(function () {
        if (jQuery('.reg-overlay-osr').length != 0) { jQuery.osr.isClicked = true; }
        if (!nextBtnIsDisabled) {
            jQuery("#next").click();
        }
    });

    jQuery("#next").click(function () {
        {
            isNextClick = true;
            sendData();
        }
    });

    /****** count total steps ******/
    jQuery("div.step").each(function (index) {
        totalSteps++;
    });

    skipStepsIfCompleted();

    //Fix for Blacksingles.com Registration Overlay error - users can not complete registration process
    //Keep session alive for non-logged in users to give enough time for registration process
    //Clear the interval once the user completes the registration process, since a new interval will be started for loggedin users(Spark.js initOnlineChecker() method).
    var loggedIn = document.getElementById("statusLoggedIn");
    if (loggedIn) {
        if (sessionKeepAliveInterval) {
            clearInterval(sessionKeepAliveInterval);
        }
    }
    else {
        sessionKeepAliveInterval = setInterval("sessionKeepAlive()", 300000);
    }
});

function skipStepsIfCompleted() {
    curentStepId = "step_" + step;
    if (jQuery('#' + curentStepId).attr("DontSkip") != "true" && isStepCompleted()) {
        /*
        if (step < totalSteps) {
        step++;                 
        }
        skipStepsIfCompleted();
        */
        var data = buildRequest();

        param = "checkstep=1";

        disableEnableNextBtn(true);
        jQuery.ajaxSettings.traditional = true;
        jQuery.ajax({
            type: "POST",
            url: window.location.pathname + "?regoverlay=true&" + param,
            data: data,
            success: function (response) {
                try {
                    var obj = jQuery.parseJSON(response);

                    if (obj.status == "ok") {
                        if (step < totalSteps) {
                            step++;
                            return skipStepsIfCompleted();
                        }
                        stepLoad();
                        disableEnableNextBtn(false);
                    }
                    else {
                        if (obj.status == "errors") {
                            handelErrors(obj);
                        }
                        else {
                            handelGeneralError();
                        }
                        //cover body
                        var t = setTimeout("showOverLayBox();", overlayTimeout);
                        stepLoad();
                        disableEnableNextBtn(false);
                    }
                }
                catch (ex) {
                    handelGeneralError();
                    disableEnableNextBtn(false);
                    //cover body
                    var t = setTimeout("showOverLayBox();", overlayTimeout);
                    stepLoad();
                }
            },
            error: function (response, status, e) {
                handelGeneralError();

                disableEnableNextBtn(false);
                //cover body
                var t = setTimeout("showOverLayBox();", overlayTimeout);
                stepLoad();
            }
        });

    }
    else {
        //check if there's cookie value for control
        if (jQuery('#' + curentStepId).attr("DontSkip")) {

            //if there's cookie val skip step
            if (checkStepCookies()) {
                if (step < totalSteps) {
                    step++;
                }
                skipStepsIfCompleted();
            }
        }
        //cover body
        var t = setTimeout("showOverLayBox();", overlayTimeout);
        disableEnableNextBtn(false);
        return stepLoad();
    }
}

//check if user completed step with cookies
function checkStepCookies() {
    var skip = true;
    jQuery('#' + curentStepId + ' .mandatory,#' + curentStepId + ' .optional').each(function () {
        if (jQuery(this).parent().attr("cookiekey") != undefined) {
            var cookie = getCookie();
            if (cookie != undefined) {
                if (cookie[jQuery(this).parent().attr("cookiekey")] == undefined) {
                    skip = false;
                    return false;
                }
            }
        }
    });
    return skip;
}

/*** handel next back button display/enable ******/
function setNextBackVisible() {
    if (step == 0) {
        isBackBtnVisiable = false;
    }
    if (step == totalSteps) {
        isNextBtnVisiable = false;
    }


    // handel next back button display
    if (!isNextBtnVisiable) {
        jQuery("#next").hide();
    }
    else {
        jQuery("#next").show();
    }

    if (!isBackBtnVisiable) {
        jQuery("#back").hide();
    }
    else {
        jQuery("#back").show();
    }
}

//count and show textarea chars
function showCharCount() {
    var length = jQuery('#' + curentStepId + " textarea").val().length;
    jQuery(".charcount font").html(length);
    if (length < 50 && jQuery(".charcount font").attr("color") != "red") {
        jQuery(".charcount font").attr("color", "red");
    }
    else if (length >= 50) {
        jQuery(".charcount font").attr("color", "green");
    }
}

/****** handel steps display ********/
function displayStep() {
    jQuery("div.step").each(function (index) {
        if (step != index) {
            jQuery(this).hide();
        }
        else {
            jQuery(this).show();
        }
    });
    jQuery("#overlaybox-header-content").html(jQuery("#" + curentStepId + " div.stepTitle").html());
    //last step title
    if (lastStepFlag) {
        jQuery("#overlaybox-header-content").html(jQuery("div#save-details_title").html());
    }

    //add aboutme tip to aboutme textarea
    if ((jQuery('#' + curentStepId + ".aboutme .steptip").html() != null && jQuery('#' + curentStepId + " textarea").val() == "") ||
                (jQuery('#' + curentStepId + ".aboutme .steptip").html() != null && jQuery('#' + curentStepId + ".aboutme .steptip").html().indexOf(jQuery('#' + curentStepId + " textarea").val()) != -1)) {
        jQuery('#' + curentStepId + " textarea").html(jQuery('#' + curentStepId + ".aboutme .steptip").html());
        jQuery('#' + curentStepId + " textarea").addClass("aboutme-tip");
        jQuery('#' + curentStepId + ".aboutme .steptip").hide();


        jQuery('#' + curentStepId + " textarea").click(function () {
            if (jQuery('#' + curentStepId + " textarea").hasClass("aboutme-tip") && jQuery('#' + curentStepId + " textarea").html() != "") {
                jQuery('#' + curentStepId + " textarea").html("");
                jQuery('#' + curentStepId + " textarea").removeClass("aboutme-tip");
            }
        });
    }
    else {
        jQuery('#' + curentStepId + " textarea").removeClass("aboutme-tip");
        jQuery('#' + curentStepId + ".aboutme .steptip").hide();
    }

    //show different text for male/female if there's # seperator in the text
    jQuery('#' + curentStepId + ' .mandatory option' + ',' + '#' + curentStepId + ' .optional option' + ',' + '#' + curentStepId + ' .h2' + ',' + '#' + curentStepId + ' .steptip').each(function (index) {

        parentId = jQuery(this).parent().attr('id');
        if ((parentId != undefined) && (jQuery(this).text().indexOf("#") != -1 || (values[parentId + "_" + index] != undefined && values[parentId + "_" + index].indexOf("#") != -1))) {
            var text = jQuery(this).text().split("#");

            if (text != undefined && values[parentId + "_" + index] != undefined) {
                text = values[parentId + "_" + index].split("#");
                jQuery(this).text(text[gender]);
            }
            else if (values[parentId + "_" + index] != undefined) {
                jQuery(this).text(values[parentId + "_" + index]);
                var text = values[parentId + "_" + index].split("#");
                jQuery(this).text(text[gender]);
            }
            else if (text != undefined) {
                values[parentId + "_" + index] = jQuery(this).text();
                jQuery(this).text(text[gender]);
            }
        }
    });
    jQuery('#' + curentStepId + ' .optional option').each(function (index) {
        if (jQuery(this).text().indexOf("#") != -1) {
            var text = jQuery(this).text().split("#");
            jQuery(this).text(text[gender]);
        }
    });
    removeErrors();
    setNextBackVisible();
    omniture();

}

//send pagename to omniture
function omniture() {

    // Removing the reg start event from the Omniture call
    if (s2.events.indexOf(omnitureRegStartEvent) != -1) {
        s2.events = s2.events.replace(omnitureRegStartEvent, '');
    }

    if (sendRegEventOnInputFocus == "send" && omnitureRegStartEvent != '' && getUrlParam(location.href, 'media') == "1") {
        sendRegEventOnInputFocus = "sent";
        // append reg start event
        s2.events = s2.apl(s2.events, omnitureRegStartEvent, ',', 1);
    }

    s2.pageName = eval("step_" + step + "_pageName");
    s2.t();
}



//hide elements with class hide
function hideElements() {

    jQuery('.hide').each(function () {
        jQuery(this).hide();
    });
}



/****** autocomplete *******/
function findValue(li) {
    if (li == null) return false;

    // if coming from an AJAX call, let's use the CityId as the value
    if (!!li.extra) var sValue = li.extra[0];

    // otherwise, let's just display the value in the text box
    else var sValue = li.selectValue;

}

function selectItem(li) {
    findValue(li);
    checkIfNextStep();
    jQuery(".ac_results").hide();
}

function formatItem(row) {
    return row[0] + " (id: " + row[1] + ")";
}

function lookupLocal() {
    var oSuggest = jQuery("#CityLocal")[0].autocompleter;
    oSuggest.findValue();

    return false;
}

//callback from change country
function autocompleteCities() {
    jQuery(".autocomplete").autocompleteArray(
		    citiesArr
        ,
		{
		    delay: 10,
		    minChars: 1,
		    matchSubset: 1,
		    onItemSelect: selectItem,
		    onFindValue: findValue,
		    autoFill: true,
		    maxItemsToShow: 6,
		    regionIdInput: "regionid",
		    selectWidth: "143px",
		    selectHeight: "130px"
		});
}

//refresh search results
var RegistrationOverlayRefreshInProgress = false;

function RegistrationOverlay_RefreshSearchResults() {
    var isVerbose = true;

    if (!RegistrationOverlayRefreshInProgress) {

        $j.ajax({
            type: "POST",
            url: "/Applications/API/SearchResults.asmx/GetRegOverlaySearchResults",
            data: "{}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                RegistrationOverlayRefreshInProgress = true;
            },
            complete: function () {
                RegistrationOverlayRefreshInProgress = false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (isVerbose) { alert(textStatus); }
            },
            success: function (result) {
                var GetSearchResult = (typeof result.d == 'undefined') ? result : result.d;
                var resultStatus = GetSearchResult.Status;
                if (resultStatus == 3) {
                    if (isVerbose) { alert(GetSearchResult.StatusMessage); }
                }
                else if (resultStatus == 2) {
                    //update content area
                    //alert("IsSearchResultNested: " + GetSearchResult.IsSearchResultNested + ", " + GetSearchResult.SearchResultHTML);
                    if (GetSearchResult.IsSearchResultNested)
                        $j('#divSearchResultsPageContainer').html($j(GetSearchResult.SearchResultHTML).find('#divSearchResultsPageContainer').html());
                    else
                        $j('#divSearchResultsPageContainer').html($j(GetSearchResult.SearchResultHTML).html());
                }
            }
        });
    }
}

function briefValidate(element) {
    isGoToValidate = true;
    if (jQuery(element).hasClass('select-birthdate')) {
        jQuery('select.select-birthdate').each(function () {
            if (isNaN(parseInt(jQuery(this).val()))) {
                isGoToValidate = false;
            }
        });
    }
    if (isGoToValidate) { checkData(); }
}
