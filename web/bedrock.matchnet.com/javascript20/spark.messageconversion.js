﻿var spark = spark || {};
spark.messageconversion = spark.messageconversion || {
	messageAttachmentURLPath : 'http://share.sparksites.com',
	
	/**Templates**/
	//photo
	sparkTagPhoto : '<sparktag type="photo" alt="{{alt}}" src="{{src}}" id="{{id}}" data-originalExt="{{originalExt}}"/>',
	htmlPhoto : '<a href="{{href}}" target="_blank"><img alt="{{alt}}" src="{{src}}" id="{{id}}"/></a>',
	htmlPhotoPreview : '<img alt="{{alt}}" src="{{src}}" id="{{id}}" data-originalExt="{{originalExt}}"/>&nbsp;',
	
	//emoticon
	sparkTagEmoticon : '<sparktag type="emoticon" class="{{styleClass}}"/>',
	htmlEmoticon : '<span class="{{styleClass}}" data-type="emoticon"></span>',
	htmlEmoticonPreview : '<span class="{{styleClass}}" data-type="emoticon"></span>',
		
	/**Conversion functions**/
	convertHtmlToCustomTag : function(message){
		//remove paragraphs, nbsp, and divs that we don't care for
		message = spark.messageconversion.removeHtmlParagraphs(message);
		message = spark.messageconversion.removeHtmlNoise(message);
		message = $j.trim(message);
		var rebuiltHtml = '';
		var html = $j('<div>').append(message).contents();
		$j.each(html, function (i, el) {
			switch (el.nodeName.toLowerCase()) {
				case "#text":
					//text
				    rebuiltHtml += spark.messageconversion.htmlEncode($j(el).text());
					break;
				case "img":
					//photo
					var dtoPhoto = {
						src: String($j(el).attr('src')).replace(spark.messageconversion.messageAttachmentURLPath, ''),
						id: $j(el).attr('id'),
						alt: $j(el).attr('alt'),
						originalExt: $j(el).attr('data-originalExt')
					};
					rebuiltHtml += Mustache.to_html(spark.messageconversion.sparkTagPhoto, dtoPhoto);
					break;
				case "span":
					var type = $j(el).attr('data-type');
					//emoticon
					if (type == "emoticon"){
						var dtoEmoticon = {
							styleClass: $j(el).attr('class')
						};
						rebuiltHtml += Mustache.to_html(spark.messageconversion.sparkTagEmoticon, dtoEmoticon);
					}
					break;
			}
		});
		
		return $j.trim(rebuiltHtml);
	},
	
	convertCustomTagToHtml : function(message){
		var rebuiltHtml = '';
		var html = $j('<div>').append(message).contents();
		$j.each(html, function (i, el) {
			switch (el.nodeName.toLowerCase()) {
				case "#text":
					//text
				    rebuiltHtml += spark.messageconversion.htmlEncode($j(el).text());
					break;
				case "sparktag":
					var type = $j(el).attr('type');
					if (type == "photo"){
						//photo
						var dtoPhoto = {
							src: spark.messageconversion.messageAttachmentURLPath + $j(el).attr('src'),
							id: $j(el).attr('id'),
							alt: $j(el).attr('alt'),
							href: spark.messageconversion.messageAttachmentURLPath + String($j(el).attr('src')).replace("preview", "original").replace('.jpg', '.' + $j(el).attr('data-originalExt'))
						};
						rebuiltHtml += Mustache.to_html(spark.messageconversion.htmlPhoto, dtoPhoto);
					}
					else if (type == "emoticon"){
						//emoticon
						var dtoEmoticon = {
							styleClass: $j(el).attr('class')
						};
						rebuiltHtml += Mustache.to_html(spark.messageconversion.htmlEmoticon, dtoEmoticon);
					}
					break;
			}
		});

	    //apply emoticon transforms for consolidated IM
		rebuiltHtml = translate_smiles(rebuiltHtml);
		
		return $j.trim(rebuiltHtml);
	},
	
	/**Encode/Decode which also strips white spaces **/
	htmlEncode: function(value) {
		//create a in-memory div, set it's inner text(which jQuery automatically encodes)
		//then grab the encoded contents back out.  The div never exists on the page.
		return $j('<div/>').text(value).html();
	},

    htmlDecode: function(value) {
		return $j('<div/>').html(value).text();
	},
	
	/**Encode/Decode without stripping white spaces **/
	htmlEscape: function (str) {
		return String(str)
		.replace(/&/g, '&amp;')
		.replace(/"/g, '&quot;')
		.replace(/'/g, '&#39;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;');
	},

    htmlUnescape: function (value) {
		return String(value)
		.replace(/&quot;/g, '"')
		.replace(/&#39;/g, "'")
		.replace(/&lt;/g, '<')
		.replace(/&gt;/g, '>')
		.replace(/&amp;/g, '&');
	},
	
	removeHtmlParagraphs: function(value) {
		return String(value)
			.replace(/<p>&nbsp;<\/p>/g, "")
			.replace(/<p>/g, "")
			.replace(/<\/p>/g, "")
			.replace(/<br\/>/g, "")
			.replace(/<br>/g, "")
			.replace(/<\/br>/g, "");
	},
	removeParagraphs: function(value){
		return String(value)
			.replace("\n", "")
			.replace("\r", "");
	},
	removeHtmlNoise: function(value){
		return String(value)
			.replace(/&nbsp;/g, ' ')
			.replace(/<div>/g, "")
			.replace(/<\/div>/g, "");
	},
	
};


/*
 * Emoticons
 * https://github.com/kof/emoticons
 * 
 */
(function ($, exports, window, name) {

    if (!exports) {
        exports = {};
        /*
        if ($) {
            $[name] = exports;
        } else {
            window[name] = exports;
        }
        */
        window[name] = exports;
    }

    var emoticons,
        codesMap = {},
        primaryCodesMap = {},
        regexp,
        metachars = /[[\]{}()*+?.\\|^$\-,&#\s]/g,
        entityMap;

    entityMap = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;'
    };

    function escape(string) {
        return String(string).replace(/[&<>"'\/]/g, function (s) {
            return entityMap[s];
        });
    }

    /**
     * Define emoticons set.
     *
     * @param {Object} data
     */
    exports.define = function (data) {
        var name, i, codes, code,
            patterns = [];

        for (name in data) {
            codes = data[name].codes;
            for (i in codes) {
                code = codes[i];
                codesMap[code] = name;

                // Create escaped variants, because mostly you want to parse escaped
                // user text.
                codesMap[escape(code)] = name;
                if (i == 0) {
                    primaryCodesMap[code] = name;
                }
            }
        }

        for (code in codesMap) {
            patterns.push('(' + code.replace(metachars, "\\$&") + ')');
        }

        regexp = new RegExp(patterns.join('|'), 'g');
        emoticons = data;
    };

    /**
     * Replace emoticons in text.
     *
     * @param {String} text
     * @param {Function} [fn] optional template builder function.
     */
    exports.replace = function (text, fn) {
        return text.replace(regexp, function (code) {
            var name = codesMap[code];
            return (fn || exports.tpl)(name, code, emoticons[name].title);
        });
    };

    /**
     * Get primary emoticons as html string in order to display them later as overview.
     *
     * @param {Function} [fn] optional template builder function.
     * @return {String}
     */
    exports.toString = function (fn) {
        var code,
            str = '',
            name;

        for (code in primaryCodesMap) {
            name = primaryCodesMap[code];
            str += (fn || exports.tpl)(name, code, emoticons[name].title);
        }

        return str;
    };

    /**
     * Build html string for emoticons.
     *
     * @param {String} name
     * @param {String} code
     * @param {String} title
     * @return {String}
     */
    exports.tpl = function (name, code, title) {
        return '<span class="spr ' + name + '" data-type="emoticon">' + code + '</span>';
    };

}(typeof jQuery != 'undefined' ? jQuery : null,
  typeof exports != 'undefined' ? exports : null,
  window,
  'emoticons'));

function translate_smiles(msg) {
    var definition = {
        "icons_smiley_a_0000": { title: "Angry", codes: [":@", ":-@", ":=@", "x(", "x-(", "x=(", "X(", "X-(", "X=("] },
        "icons_smiley_a_0001": { title: "Lips Sealed", codes: [":x", ":-x", ":X", ":-X", ":#", ":-#", ":=x", ":=X", ":=#"] },
        "icons_smiley_a_0002": { title: "Drunk", codes: ["(drunk)"] },
        "icons_smiley_a_0003": { title: "In love", codes: ["(inlove)"] },
        "icons_smiley_a_0004": { title: "Wondering", codes: [":^)"] },
        "icons_smiley_a_0005": { title: "Tongue Out", codes: [":P", ":=P", ":-P", ":p", ":=p", ":-p"] },
        "icons_smiley_a_0006": { title: "Kiss", codes: [":*", ":=*", ":-*"] },
        "icons_smiley_a_0008": { title: "Smirking", codes: ["(smirk)"] },
        "icons_smiley_a_0009": { title: "Smile", codes: [":|"] },
        "icons_smiley_a_0010": { title: "Smile", codes: [":}"] },
        "icons_smiley_a_0011": { title: "Smile", codes: [".)"] },
        "icons_smiley_a_0012": { title: "", codes: [")-"] },
        "icons_smiley_a_0013": { title: "Sad", codes: [":("] },
        "icons_smiley_a_0014": { title: "", codes: [":))"] },
        "icons_smiley_a_0016": { title: "Smile", codes: [":)"] },
        "icons_smiley_a_0017": { title: "", codes: ["(-)"] },
        "icons_smiley_a_0018": { title: "Smile", codes: ["8)"] },
        "icons_smiley_a_0019": { title: "", codes: [":O", ":=O", ":-O"] },
        "icons_smiley_a_0020": { title: "Smile", codes: [":=)"] },
        "icons_smiley_a_0021": { title: "Smile", codes: ["(yellow_deep_smile)"] },
        "icons_smiley_a_0022": { title: "Sad", codes: ["B=("] },
        "icons_smiley_a_0023": { title: "Sad", codes: [":=("] },
        "icons_smiley_a_0024": { title: "Wink", codes: [";)", ";-)", ";=o", ";-o"] },
        "icons_smiley_a_0025": { title: "Devil", codes: ["(devil)"] },
        "icons_smiley_a_0026": { title: "Wink Tongue", codes: [";p"] },
        "icons_smiley_a_0027": { title: "Sad", codes: [":-("] },
        "icons_smiley_a_0028": { title: "", codes: ["(yellow_content)"] },
        "icons_smiley_a_0029": { title: "", codes: ["(yellow_big_smile)"] },

        "icons_smiley_a_0015": { title: "", codes: [":-"] },

        "icons_smiley_b_0001": { title: "Big Smile", codes: [":D", ":=D", ":-D", ":d", ":=d", ":-d"] },
        "icons_smiley_b_0002": { title: "", codes: ["(blue_starryeyed)"] },
        "icons_smiley_b_0003": { title: "", codes: ["(blue_bad)"] },
        "icons_smiley_b_0004": { title: "", codes: ["(blue_notgood)"] },
        "icons_smiley_b_0005": { title: "", codes: ["(blue_vilian)"] },
        "icons_smiley_b_0006": { title: "", codes: ["(blue_ohno)"] },
        "icons_smiley_b_0008": { title: "", codes: ["(blue_upset)"] },
        "icons_smiley_b_0009": { title: "", codes: ["(blue_mean)"] },
        "icons_smiley_b_0010": { title: "", codes: ["(blue_malicious)"] },
        "icons_smiley_b_0011": { title: "", codes: ["(blue_sick)"] },
        "icons_smiley_b_0012": { title: "", codes: ["(blue_tongue_hearteyes)"] },
        "icons_smiley_b_0013": { title: "", codes: ["(blue_tongue)"] },
        "icons_smiley_b_0014": { title: "", codes: ["(blue_tongue_side)"] },
        "icons_smiley_b_0015": { title: "", codes: ["(blue_kiss)"] },
        "icons_smiley_b_0016": { title: "", codes: ["(blue_lovestruck)"] },
        "icons_smiley_b_0017": { title: "", codes: ["(blue_wink)"] },
        "icons_smiley_b_0018": { title: "", codes: ["(blue_lol)"] },
        "icons_smiley_b_0019": { title: "", codes: ["(blue_happy_crazy)"] },
        "icons_smiley_b_0020": { title: "", codes: ["(blue_bigteeth)"] },
        "icons_smiley_b_0021": { title: "", codes: ["(blue_smile_right)"] },
        "icons_smiley_b_0022": { title: "", codes: ["(blue_big_eyes)"] },

        "icons_smiley_b_0023": { title: "", codes: ["(blue_blackeyes)"] },
        "icons_smiley_b_0024": { title: "", codes: ["(blue_worried)"] },
        "icons_smiley_b_0025": { title: "", codes: ["(blue_drunk)"] },
        "icons_smiley_b_0026": { title: "", codes: ["(blue_26)"] },
        "icons_smiley_b_0027": { title: "", codes: ["(blue_squint)"] },
        "icons_smiley_b_0028": { title: "", codes: ["(blue_eyepop)"] },
        "icons_smiley_b_0029": { title: "", codes: ["(blue_29)"] },
        "icons_smiley_b_0030": { title: "", codes: ["(blue_really)"] },
        "icons_smiley_b_0031": { title: "", codes: ["(blue_worried31)"] },
        "icons_smiley_b_0032": { title: "", codes: ["(blue_sinister)"] },
        "icons_smiley_b_0033": { title: "", codes: ["(blue_whoa)"] },
        "icons_smiley_b_0034": { title: "", codes: ["(blue_wow)"] },
        "icons_smiley_b_0035": { title: "", codes: ["(blue_35)"] },
        "icons_smiley_b_0036": { title: "", codes: ["(blue_36)"] },
        "icons_smiley_b_0037": { title: "", codes: ["(blue_inlove)"] },
        "icons_smiley_b_0038": { title: "", codes: ["(blue_talk)"] },
        "icons_smiley_b_0039": { title: "", codes: ["(blue_tears)"] },
        "icons_smiley_b_0040": { title: "", codes: ["(blue_nerd)"] }

    };
    window.emoticons.define(definition);
    var new_msg = window.emoticons.replace(msg);

    //convert _ to - for la css
    new_msg = new_msg.replace(/_/g, '-');
    return new_msg;
}
