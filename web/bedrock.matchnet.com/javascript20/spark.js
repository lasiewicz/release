﻿/*
* New "Master" javascript file for spark
*/

////////////////////////////////////////////////////////////////////////////////////////
// These functions are required to persist a Layout Template across multiple requests

function getUrlParams(url) {
    var queryString = url.split('?');
    if (queryString[1] != null)
        return queryString[1].split('&');
}

function getUrlParam(url, paramName) {
    var params = getUrlParams(url);

    if (params == null)
        return null;

    for (i = 0; i < params.length; i++) {
        var param = params[i];

        if (param.indexOf(paramName + '=') == 0) {
            return param.substr(paramName.length + 1);
        }
    }

    return null;
}

function getBodyFromFunction(func) {
    func = func.toString();
    func = func.substring(func.indexOf("{") + 1, func.lastIndexOf("}"));
    return func;
}

function appendPopupKeyToLink(link) {
    var layoutTemplateID = getUrlParam(location.href, 'LayoutTemplateID');

    if (layoutTemplateID != null) {
        if (link.href != null && link.href != "") {
            if (link.href.indexOf('?') == -1) {
                delim = '?';
            }
            else {
                delim = '&';
            }

            if (getUrlParam(link.href, 'PersistLayoutTemplate') == null) {
                link.href += delim + 'PersistLayoutTemplate=1';
                delim = '&';
            }

            if (getUrlParam(link.href, 'LayoutTemplateID') == null) {
                link.href += delim + 'LayoutTemplateID=' + layoutTemplateID;
            }
        }
    }
}

function delegateClickEvents() {
    if (getUrlParam(location.href, 'PersistLayoutTemplate') != null) {
        var links = document.getElementsByTagName('a');

        for (var i = 0; i < links.length; i++) {
            if (links[i].href.indexOf('javascript') == -1) {
                if (!links[i].onclick) {
                    links[i].onclick = new Function("appendPopupKeyToLink(this);");
                }
                else {
                    currentOnClick = links[i].onclick;
                    if (typeof currentOnClick == "function") {
                        currentOnClick = getBodyFromFunction(currentOnClick);
                    }

                    links[i].onclick = new Function("appendPopupKeyToLink(this); " + currentOnClick);
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////

function spark_getCookie(name) {
    var start = document.cookie.indexOf(name + '=');
    var len = start + name.length + 1;

    if ((!start) && (name != document.cookie.substring(0, name.length))) {
        return null;
    }

    if (start == -1) {
        return null;
    }

    var end = document.cookie.indexOf(';', len);

    if (end == -1) {
        end = document.cookie.length;
    }

    return unescape(document.cookie.substring(len, end));
}

function spark_setCookie(name, value, expires, path, domain, secure) {
    document.cookie = name + '=' + escape(value)
        + ((expires) ? ';expires=' + expires.toGMTString() : '')
        + ((path) ? ';path=' + path : '')
        + ((domain) ? ';domain=' + domain : '')
        + ((secure) ? ';secure' : '');
}

function matchnetYNM4_checkWindowOpener(objectId) {
    var lnkNoPhoto = document.getElementById(objectId);

    if (lnkNoPhoto != null) {
        if (window.top.opener != null) {
            window.top.opener.location.href = lnkNoPhoto.href;
            lnkNoPhoto.onclick = matchnetYNM4_returnFalse;

            return false;
        }
        else {
            window.location.href = lnkNoPhoto.href;
        }
    }
}

function matchnetYNM4_returnFalse() {
    return false;
}

/* bust out of frames always */
function RemoveFrames() {
    if (self.location != top.location) top.location = self.location;
}

/**********************************************************************************/
function launchWindow(strURL, strName, intWidth, intHeight, strProperties) {
    // just create a window with the given properties and don't return the handle
    // to the window. (for inline javascript: calls in hrefs)
    var w = createWindow(strURL, strName, intWidth, intHeight, strProperties);
    // make sure created window has focus. In case a popup profile was in background
    // or minimized, bring it to front.
    w.focus();
}

function createWindow(strURL, strName, intWidth, intHeight, strProperties) {
    var mywin;
    var intVersion;
    var dummyDate = new Date();

    var strPoundURL;
    var intPoundPos;

    intVersion = navigator.appVersion.substring(0, 1);
    if (strURL != "") {
        intPoundPos = strURL.indexOf("#");
        if (intPoundPos != -1) {
            strPoundURL = strURL.substring(intPoundPos);
            strURL = strURL.substring(0, intPoundPos);
        }

        if (strURL.indexOf("?") != -1) {
            strURL = strURL + "&rnd="
        } else {
            strURL = strURL + "?rnd="
        }

        strURL = strURL + dummyDate.getTime();

        if (strPoundURL != null) {
            strURL = strURL + strPoundURL;
        }

        strURL = Replace(strURL, "@", "%40");
    }

    if (strProperties == "") {
        strProperties = "scrollbars=yes,resizable=yes,menubar=no,location=no,directories=no,toolbar=no";
    }

    strProperties = "status=yes,height=" + intHeight + ",width=" + intWidth + "," + strProperties;

    // replace all non-alphacharacters with X
    if (strName.replace)
        strName = strName.replace(/\W/g, "X");

    // lower case the string
    strName = strName.toLowerCase();

    //try to open a pop-up window
    mywin = window.open(strURL, strName, strProperties);


    // POP-BLOCKER OVERRIDE SECTION
    // try to load the link in the current window, unless this is a view profile request from IM.
    // If it is a view profile request from IM, try to show the profile in window.opener or else give up
    if (mywin == null)//i.e., pop-up blocker
    {
        mywin = self; // set to self so we don't return null reference

        // We are changing the template here to show nav and header 
        // since we can't open a pop-up window
        strURL = strURL.replace("LayoutTemplateID=2", "LayoutTemplateID=1");
        strURL = strURL.replace("LayoutTemplateID=4", "LayoutTemplateID=8");
        strURL = strURL.replace("LayoutTemplateID=11", "LayoutTemplateID=1"); //Open profile in same window if popup blockers are enabled.

        // If the current window is an IM window, we will allow the popup blocker to notify the user that
        // the window couldn't be opened, otherwise, we load the link in the current window.
        if (window.location.href.indexOf("/upInstantCommunicator.aspx") == -1) {
            window.document.location = strURL;
        }
        else {
            // if this is an IM window request for a profile, check if window.opener is still open
            var IsWindowOpenerClosed = false;
            try {	//Attempt to access a property on the window.opener object
                //Using 'window.opener.closed' is not consistent on all browser implementations.
                var temp = window.opener.location.href;
            }
            catch (e) {
                IsWindowOpenerClosed = true;
            }
            if (!IsWindowOpenerClosed) {

                //do not persist the layout template (bug: 15532) (or else all subsequent requests will have nav and header around them)
                strURL = strURL.replace("PersistLayoutTemplate=1", "")
                window.opener.location = strURL;
            }
            else {
                //do nothing. let the pop-up blocker work TODO: resource in other languages
                //alert("to view member's profile requires pop-up blocker to be disabled.");
            }

        }
    }

    // return handle to caller
    return mywin;
}

/**********************************************************************************/
function AddFriendsToHotList(strBaseForm, lngAction, lngLayoutTemplateID) {
    var mywin;
    var i;
    var lngCount;
    var strFriendMemberIDs;
    var lngCachedAction;
    var strCachedTarget;

    strFriendMemberIDs = '';
    lngCount = 0
    for (var i = 0; i < document.forms[strBaseForm].FriendMemberID.length; i++) {
        if (document.forms[strBaseForm].FriendMemberID[i].checked == true) {
            if (lngCount == 0)
                strFriendMemberIDs = strFriendMemberIDs + document.forms[strBaseForm].FriendMemberID[i].value;
            else
                strFriendMemberIDs = strFriendMemberIDs + ',' + document.forms[strBaseForm].FriendMemberID[i].value;
            lngCount++;
        }
    }

    if (lngCount == 0) {
        alert("Please choose at least one member");
        return;
    }


    mywin = window.open('/blank.htm', 'friends', 'status=yes,height=525,width=750,scrollbars=yes,resizable=yes,menubar=no,location=no,directories=no,toolbar=no');
    if (mywin.opener == null) mywin.opener = self;
    mywin.focus();

    strCachedTarget = document.forms[strBaseForm].target;
    lngCachedAction = document.forms[strBaseForm].elements["a"].value;

    strSavedAction = document.forms[strBaseForm].action;
    document.forms[strBaseForm].action = "default.asp?LayoutTemplateID=" + lngLayoutTemplateID;
    document.forms[strBaseForm].target = mywin.name;
    document.forms[strBaseForm].elements["a"].value = lngAction;
    document.forms[strBaseForm].submit();

    document.forms[strBaseForm].target = strCachedTarget;
    document.forms[strBaseForm].elements["a"].value = lngCachedAction;
    document.forms[strBaseForm].action = strSavedAction;
}

/**********************************************************************************
/ The name of the OpenerFormName is hardcoded for these reasons:
/ 1) For effeciency. We don't want to pass it around the form and querystring
/ 2) The SelectMemberForm is really comming from CMemberList. Adding support for this
/		would make this code get more ugly then
/ 3) Don't want to make a global constant; it would have to be passed into this function
**********************************************************************************/
function SelectMemberToEmailFromHotlist(strFormName, lngMaxRadioButtonIndex, strErrorMsg) {
    var strOpenerFormName = 'frmComposeEmailFull';
    var lngToMemberID;
    var blnFound = false;

    if (lngMaxRadioButtonIndex == 1) {
        if (document.forms[strFormName].ToMemberID.checked) {
            lngToMemberID = document.forms[strFormName].ToMemberID.value;
            blnFound = true;
        }
    } else {
        for (i = 0; i < lngMaxRadioButtonIndex; i++) {
            if (document.forms[strFormName].ToMemberID[i].checked) {
                lngToMemberID = document.forms[strFormName].ToMemberID[i].value;
                blnFound = true;
            }
        }
    }

    if (blnFound == false) {
        alert(strErrorMsg);
        return;
    }

    window.opener.document.forms[strOpenerFormName].ToMemberID.value = lngToMemberID;
    window.opener.document.forms[strOpenerFormName].submit();
    window.close();
}

/**********************************************************************************/
function SelectMemberToEmail(lngToMemberID) {
    var strOpenerFormName = 'frmComposeEmailFull';

    window.opener.document.forms[strOpenerFormName].ToMemberID.value = lngToMemberID;
    window.opener.document.forms[strOpenerFormName].submit();
    window.close();
}

/**********************************************************************************/
function Split(str, strDelimiter) {
    var ary = new Array();
    var lngCurrentLoc;
    var i = 0;
    var strValue;
    var lngLength;

    lngCurrentLoc = str.indexOf(strDelimiter);
    while (lngCurrentLoc >= 0) {
        strValue = str.substring(0, lngCurrentLoc);
        if (strValue.length > 0) {
            ary[i++] = strValue
        }
        str = str.substring(lngCurrentLoc + 1, str.length);
        lngCurrentLoc = str.indexOf(strDelimiter);
    }
    if (str.length > 0) {
        ary[i] = str;
    }
    return ary;
}

/**********************************************************************************/



/**********************************************************************************/
function Replace(str, substring, newstring) {
    if (str.length <= 0)
        return "";

    temp = "" + str;

    while (temp.indexOf(substring) > -1) {
        pos = temp.indexOf(substring);
        temp = "" + (temp.substring(0, pos) + newstring + temp.substring((pos + substring.length), temp.length));
    }
    return temp;
}

function ReplaceSingle(str, substring, newstring) {
    if (str.length <= 0)
        return "";

    temp = "" + str;

    if (temp.indexOf(substring) > -1) {
        pos = temp.indexOf(substring);
        temp = "" + (temp.substring(0, pos) + newstring + temp.substring((pos + substring.length), temp.length));
    }
    return temp;
}

function nl2br(str) {
    var br = '<br />';
    return str.replace(/\r\n|\r|\n/g, br);
}

function br2nl(str) {
    var nl = '\r\n';
    return str.replace(/\<br(\s*\/|)\>/gi, nl);
}

/**********************************************************************************/
function ConfirmDelete(strMessage, strLocation) {
    if (confirm(strMessage)) {
        document.location.href = strLocation;
    }
}

/**********************************************************************************/
function CheckAll(frm) {
    for (var i = 0; i < frm.elements.length; i++) {
        var e = frm.elements[i];
        if (e.name != 'allbox')
            e.checked = frm.allbox.checked;
    }
}

/**********************************************************************************/
function UnCheckAllExceptOne(form_object, lngItem) {
    if (lngItem == null) { lngItem = 0 };
    if (form_object[lngItem].checked == true) {
        var len = form_object.length;
        for (var i = 0; i < len; i++) {
            if (i != lngItem) {
                form_object[i].checked = false;
            }
        }
    }
}

/**********************************************************************************/
function UnCheckTop(form_object) {
    form_object[0].checked = false;
}


/**********************************************************************************/
/* Form Validation: Begin													      */
/**********************************************************************************/

function IsEmpty(obj, obj_type) {
    if (obj_type == "text" || obj_type == "password" || obj_type == "textarea" || obj_type == "file") {
        var objValue;

        objValue = obj.value.replace(/\s+$/, "");

        if (objValue.length == 0) {
            obj.focus();
            return true;
        } else {
            return false;
        }
    } else if (obj_type == "select") {
        for (i = 0; i < obj.length; i++) {
            if (obj.options[i].selected) {
                if (obj.options[i].value == "") {
                    obj.focus();
                    return true;
                } else {
                    return false;
                }
            }

        }
        return true;
    } else if (obj_type == "radio" || obj_type == "checkbox") {
        if (!obj[0] && obj) {
            if (obj.checked) {
                return false;
            } else {
                obj.focus();
                return true;
            }
        } else {
            for (i = 0; i < obj.length; i++) {
                if (obj[i].checked) {
                    return false;
                }
            }
            obj[0].focus();
            return true;
        }
    } else {
        return false;
    }
}

function InLengthRange(object_value, min_value, max_value) {

    if (object_value == null) {
        return true;
    }

    if (min_value == null) {
        min_value = 0;
    }

    if (max_value == null) {
        if (object_value.length >= min_value) {
            return true;
        } else {
            return false;
        }
    }

    if (object_value.length >= min_value && object_value.length <= max_value) {
        return true;
    } else {
        return false;
    }

}

function InValueRange(object_value, min_value, max_value) {

    if (object_value == null) {
        return true;
    }
    else {
        if (object_value == '')
            return true;
    }

    if (min_value == null) {
        min_value = 0;
    }



    if (max_value == null) {
        if (object_value >= min_value) {
            return true;
        } else {
            return false;
        }
    }

    if (object_value >= min_value && object_value <= max_value) {
        return true;
    } else {
        return false;
    }

}


function TestNumberRange(object_value, min_value, max_value) {
    // check minimum
    if (min_value != null) {
        if (object_value < min_value) {
            return false;
        }
    }

    // check maximum
    if (max_value != null) {
        if (object_value > max_value) {
            return false;
        }
    }
    //All tests passed, so...
    return true;
}

function IsValidDate(month, day, year) {
    var blnRet;

    if (month == "" || day == "" || year == "") {
        return true;
    }

    day = parseInt(day)
    month = parseInt(month)
    year = parseInt(year)


    if ((day >= 1 && day <= 31) &&
		(month >= 1 && month <= 12) &&
		(year > 0)) {
        blnRet = true;
        switch (month) {
            case 2:

                if (day > 28) {
                    // check for leap years
                    if (((year % 4 == 0) && (year % 100 != 0)) ||
						(year % 400 == 0)) {
                        if (day > 29) {
                            blnRet = false;
                        }
                    } else {
                        blnRet = false;
                    }
                }
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                if (day > 30) {
                    blnRet = false;
                }
                break;
            default:
                break;
        }
    } else {
        blnRet = false;
    }

    return blnRet;
}

function IsInteger(object_value) {
    if (object_value.length == 0) {
        return true;
    }

    var decimal_format = ".";
    var check_char;

    //The first character can be + -  blank or a digit.
    check_char = object_value.indexOf(decimal_format)

    //Was it a decimal?
    if (check_char < 1) {
        return IsNumeric(object_value);
    } else {
        return false;
    }
}


function IsNumeric(object_value) {
    //Returns true if value is a number or is NULL
    //otherwise returns false	

    if (object_value.length == 0) {
        return true;
    }

    //	Returns true if value is a number defined as
    //	having an optional leading + or -.
    // 	having at most 1 decimal point.
    //	otherwise containing only the characters 0-9.
    var start_format = " .+-0123456789";
    var number_format = " .0123456789";
    var check_char;
    var decimal = false;
    var trailing_blank = false;
    var digits = false;

    //The first character can be + - .  blank or a digit.
    check_char = start_format.indexOf(object_value.charAt(0))
    //Was it a decimal?
    if (check_char == 1) {
        decimal = true;
    } else if (check_char < 1) {
        return false;
    }

    //Remaining characters can be only . or a digit, but only one decimal.
    for (var i = 1; i < object_value.length; i++) {
        check_char = number_format.indexOf(object_value.charAt(i))
        if (check_char < 0) {
            return false;
        } else if (check_char == 1) {
            if (decimal) {		// Second decimal.
                return false;
            } else {
                decimal = true;
            }
        } else if (check_char == 0) {
            if (decimal || digits) {
                trailing_blank = true;
            }
            // ignore leading blanks
        } else if (trailing_blank) {
            return false;
        } else {
            digits = true;
        }
    }
    //All tests passed, so...
    return true
}

function IsNumberChar(ch) {
    return (ch >= '0' && ch <= '9');
}

function IsAlphaChar(ch) {
    return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || (ch.charCodeAt(0) >= 0x05d0 && ch.charCodeAt(0) <= 0x05ea);
}

function IsAlphaNumeric(object_value) {
    var i;
    var intLength, ch;
    intLength = object_value.length;

    for (i = 0; i < intLength; i++) {
        ch = object_value.charAt(i);
        if (!(IsAlphaChar(ch) || IsNumberChar(ch))) {
            return false;
        }
    }
    return true;
}

function IsAlpha(object_value) {
    var i;
    var intLength, ch;
    intLength = object_value.length;

    for (i = 0; i < intLength; i++) {
        ch = object_value.charAt(i);
        if (!IsAlphaChar(ch)) {
            return false;
        }
    }
    return true;
}


function IsEmail(object_value, min_value) {
    var i, ch, intAtCount, intDotPos, intLastDotPos;

    var strEmailChars = "_-@.~!#$%&*+\"'"

    var len = object_value.length;

    if (len == 0) {
        return true;
    }

    if (len < min_value) {
        return false;
    }

    intAtCount = 0;
    intDotPos = 0;
    for (i = 0; i < len; i++) {
        ch = object_value.charAt(i);

        if (!(IsAlphaChar(ch) ||
				IsNumberChar(ch) ||
				strEmailChars.indexOf(ch) != -1)
			) {
            return false;
        } else {
            if (ch == '@') {
                intAtCount++;
                if (intAtCount > 1) {
                    return false;
                }
            } else if (ch == '.') {
                // don't allow consecutive .'s
                if (i - 1 == intLastDotPos) {
                    return false;
                }
                intDotPos = i;
                intLastDotPos = i;
            }
        }
    }

    return (intDotPos > 0 && intAtCount > 0 && intDotPos < (len - 1));

}

function IsURL(object_value) {
    var i, ch, intDotCount;

    var strURLChars = "~!@#$%*_+-:/.?&="

    var len = object_value.length;

    if (object_value.length == 0) {
        return true;
    }

    if (object_value.length < 10) {
        return false;
    }

    if ((object_value.indexOf("http://") == -1) &&
			(object_value.indexOf("https://") == -1) &&
			(object_value.indexOf("ftp://") == -1)) {
        return false;
    }

    intDotCount = 0;
    for (i = 0; i < len; i++) {
        ch = object_value.charAt(i);

        if (!(IsAlphaChar(ch) ||
				IsNumberChar(ch) ||
				strURLChars.indexOf(ch) != -1)
			) {
            return false;
        } else if (ch == '.') {
            intDotCount++;
        }
    }

    return (intDotCount > 0);

}

function IsValidExtension(object_value, extensions) {
    // ---------------Examples----------------
    // extensions -->   "jpeg, gif, zip"
    // object_value --> "chaka.zip"
    // ---------------------------------------

    var i, ch, intDotCount;
    var test_extension = "";
    var obj_extension = "";
    var len = object_value.length;

    if ((object_value == "") || (extensions == ""))
        return true;

    // --------------------------------------------------
    // First get the extension of the object value
    // --------------------------------------------------
    for (i = len; i > 0; i--) {
        ch = object_value.charAt(i);
        if (ch == '.') {
            intDotCount = i;
            break;
        }
    }

    for (i = (intDotCount + 1); i < len; i++) {
        obj_extension = obj_extension + object_value.charAt(i);
    }

    if (obj_extension.length == 0)
        return false;

    // --------------------------------------------------
    // Then, we compare that string to every extension we passed in
    // --------------------------------------------------

    extensions = extensions.toLowerCase();
    obj_extension = obj_extension.toLowerCase();

    len = extensions.length;
    test_extension = "";

    for (i = 0; i <= len; i++) {

        ch = extensions.charAt(i);

        if ((ch == ',') || (ch == ' ') || (i == len)) {
            if (test_extension.length == 0)
                continue;
            if (test_extension == obj_extension)
                return true;
            test_extension = "";
        }
        else {
            test_extension = test_extension + ch;
        }
    }
    return false;
}

function LimitInput(field, countfield, maxlimit) {
    if (field.value.length > maxlimit) // if too long...trim it!
    {
        field.value = field.value.substring(0, maxlimit);
    }
    // otherwise, update 'characters left' counter
    else {
        if (countfield != null)
            countfield.value = maxlimit - field.value.length;
    }
}

function IsCreditCard(object_value) {
    // Encoding only works on cards with less than 19 digits
    if (object_value.length > 19)
        return (false);

    // Use regular express to check credit card format
    var reg = /^3[4,7]\d{13}|3[0,6,8]\d{12}|((4\d{3})|(5[1-5]\d{2})|(6011)|(35\d{2}))\d{12}$/;
    if (reg.test(object_value) == false) {
        return (false);
    }
    // User Mod 10 
    sum = 0; mul = 1; l = object_value.length;
    for (i = 0; i < l; i++) {
        digit = object_value.substring(l - i - 1, l - i);
        tproduct = parseInt(digit, 10) * mul;
        if (tproduct >= 10)
            sum += (tproduct % 10) + 1;
        else
            sum += tproduct;
        if (mul == 1)
            mul++;
        else
            mul--;
    }

    if ((sum % 10) == 0)
        return (true);
    else
        return (false);

} // END FUNCTION isCreditCard()

/**********************************************************************************/
/* Form Validation: End															  */
/**********************************************************************************/

/**********************************************************************************/
/*	START - FORM OPTIONS (check or uncheck)										  */
/**********************************************************************************/
function UnCheckAllExceptThis(form_object, lngItem) {
    if (lngItem == null) { lngItem = 0 };
    if (form_object[lngItem].checked == true) {
        var len = form_object.length;
        for (var i = 0; i < len; i++) {
            if (i != lngItem) {
                form_object[i].checked = false;
            }
        }
    }
}

/**********************************************************************************/
function UnCheckThis(form_object, lngItem) {
    form_object[lngItem].checked = false;
    var len = form_object.length;
    var uncheckedAll = true;
    for (var i = 0; i < len; i++) {
        if (form_object[i].checked == true) {
            uncheckedAll = false;
        }
    }
    if (uncheckedAll == true) {
        form_object[0].checked = true;
    }
}

/**********************************************************************************/
/*	END - FORM OPTIONS (check or uncheck)										  */
/**********************************************************************************/

function ResetOnUnloadEvent() {
    window.onunload = null;
}

function PopupInit() {
    if (typeof (popupInitValues) != "undefined") {
        if (popupInitValues.length == 5) {
            launchWindow(popupInitValues[0], popupInitValues[1], popupInitValues[2], popupInitValues[3], popupInitValues[4]);
        }
    }
}

function PopInInit() {
    if (typeof (popInInitValues) != "undefined" && popInInitValues.length == 1) {
        window.location = popInInitValues[0];
    }
}

// This function specifically for exit popups.  It will call PopupInit only if the window has been closed.
function ExitPopupInit() {
    var i = window.screenTop; // Close is always a big number.
    if (i > 9000) {
        PopupInit();
    }
    else {
        // A window refresh.  Do nothing.
    }
}

function stripWhiteSpace(txtField) {
    if (txtField.value) {
        if (txtField.value.length > 0) {
            txtField.value = Replace(txtField.value, " ", "");
        }
    }
}

/**********************************************************************************/
/*	Open link in parent window.  If parnet window is closed
/*  then resize popup to height and width 
/**********************************************************************************/
function OpenLinkInParentWindow(href, width, height) {
    var Parent = window.opener;

    //alert("hehehe");
    ///alert(href);

    var blnParentExists = false;

    var sType = typeof (Parent);

    if (sType.toLowerCase() != 'undefined') {
        blnParentExists = true;

    }

    if (blnParentExists) {
        Parent.location = href;
        window.close();
    }
    else {
        window.resizeTo(width, height);
        window.location = href;
    }
}

/********************************************************************************/
/*  FUNCTION to check CSS Navigation Elements
/********************************************************************************/
function init() {
    //cssjsmenu('navbar');
    //cssjsmenu('navBarTop'); not in use anywhere
    initOnlineChecker(false); //start checking IMs (on most pages that use standard template - false means don't ignore time limit)
}

/********************************************************************************/
/*  FUNCTION to show / hide divs like in FAQ section
/ added elementType for showing/hiding non-block elements
/********************************************************************************/
function showHideDiv(sDivId, hDivId, elementType) {
    if (elementType = "span") {
        var sDiv = document.getElementById(sDivId);
        var hDiv = document.getElementById(hDivId);

        sDiv.className = "showSpan";
        hDiv.className = "hideSpan";
    }

    else {
        var sDiv = document.getElementById(sDivId);
        var hDiv = document.getElementById(hDivId);

        sDiv.className = "show";
        hDiv.className = "hide";
    }
}

function changeClassStyle(changeThisDivId, changeToThisClass) {
    document.getElementById(changeThisDivId).className = changeToThisClass;
}

function changeDivBgImage(changeThisDivBgImage, changeThisDivBgImagePath) {
    bgImagePath = "url(" + changeThisDivBgImagePath + ")";
    document.getElementById(changeThisDivBgImage).style.backgroundImage = bgImagePath;
}

function swapState(object, strDiv, strSelect, strDeselect, strHideDiv, strShowDiv, strHide, strShow) {
    var hotlist = document.getElementById(strDiv);
    var hotlistSelectText = document.getElementById(strSelect);
    var hotlistDeselectText = document.getElementById(strDeselect);

    if (object.checked) {
        hotlist.className = strHideDiv;
        hotlistSelectText.className = strHide;
        hotlistDeselectText.className = strShow;
    }
    else {
        hotlist.className = strShowDiv;
        hotlistSelectText.className = strShow;
        hotlistDeselectText.className = strHide;
    }

}


/********************************************************************************/
/*  FUNCTION to handle mouseovers of mini profile.  Also handles voting.  Legacy code.  -wel
/********************************************************************************/

//fix parent client id if it doesn't exist
function setParentClientIdIfNotExist(parentClientID) {
    if (document.getElementById(parentClientID + "_YNMVoteStatus") == null) {
        parentClientID = parentClientID.substring(0, parentClientID.lastIndexOf("_"));
    }
    return parentClientID;
}


function YNMMouseOver(sender, parentClientID, type) {
    parentClientID=setParentClientIdIfNotExist(parentClientID);
    if ((document.forms[window.document.forms[0].name].elements[parentClientID + "_YNMVoteStatus"].value & type) != type) {
        YNMSwap(sender);
    }
}

function YNMMouseOut(sender, parentClientID, type) {
    parentClientID = setParentClientIdIfNotExist(parentClientID);
    if ((document.forms[window.document.forms[0].name].elements[parentClientID + "_YNMVoteStatus"].value & type) != type) {
        YNMSwap(sender);
    }

}

function YNMMouseOverWithSenderID(senderID, parentClientID, type) {
    var sender = document.getElementById(senderID);
    if (sender != null)
        YNMMouseOver(sender, parentClientID, type);
}

function YNMMouseOutWithSenderID(senderID, parentClientID, type) {
    var sender = document.getElementById(senderID);
    if (sender != null)
        YNMMouseOut(sender, parentClientID, type);

}

function YNMSwap(img) {
    var imageOffIndex = img.src.indexOf('off.gif');
    var imageOnIndex = img.src.indexOf('on.gif');

    img.src = (imageOffIndex == -1)
		? (img.src.substr(0, imageOnIndex) + 'off.gif')
		: (img.src.substr(0, imageOffIndex) + 'on.gif');
}

function SetYNMImage(img, highlight, type, parentClientID) {
    if (img != null) {
        var imageOffIndex = img.src.indexOf('off.gif');
        var imageOnIndex = img.src.indexOf('on.gif');
        parentClientID = setParentClientIdIfNotExist(parentClientID);
        if (highlight) {
            if ((document.forms[window.document.forms[0].name].elements[parentClientID + "_YNMVoteStatus"].value & type) == type) {
                return;
            }
            else {
                if (imageOnIndex != -1) {
                    img.src = img.src.substr(0, imageOnIndex) + 'on.gif'
                }
                else {
                    img.src = img.src.substr(0, imageOffIndex) + 'on.gif'
                }
            }
        }
        else {
            if (imageOffIndex != -1) {
                img.src = img.src.substr(0, imageOffIndex) + 'off.gif'
            }
            else {
                img.src = img.src.substr(0, imageOnIndex) + 'off.gif'
            }
        }
    }
}

function SetBothSaidYes(parentClientID, visibility) {
    var divBothSaidYes = document.getElementById(parentClientID + "_divBothSaidYes");
    var imgBothSaidYes = document.getElementById(parentClientID + "_imgBothSaidYes");
    var imgBothSaidYesTabProfile = document.getElementById(tabProfileObject.YYImageID);

    if (divBothSaidYes != null)
        divBothSaidYes.style.visibility = visibility;
    if (imgBothSaidYes != null)
        imgBothSaidYes.style.visibility = visibility;
    if (imgBothSaidYesTabProfile != null) {
        imgBothSaidYesTabProfile.style.display = (visibility == 'hidden') ? "none" : "inline";
    }
}

function ShowMutualYes(divMutualYesID, divVoteButtonsID) {
    // This function is used by the YNMVoteBarSmall object to hide the YNM buttons and show the MutualYes icon
    var divMutualYes = document.getElementById(divMutualYesID);
    var divVoteButtons = document.getElementById(divVoteButtonsID);

    if (divMutualYes != null)
        divMutualYes.style.display = "inline-block";
    if (divVoteButtons != null)
        divVoteButtons.style.display = "none";

}

function HideMutualYes(divMutualYesID, divVoteButtonsID) {
    var divMutualYes = document.getElementById(divMutualYesID);
    var divVoteButtons = document.getElementById(divVoteButtonsID);

    if (divMutualYes != null)
        divMutualYes.style.display = "none";
    if (divVoteButtons != null)
        divVoteButtons.style.display = "block";

}

//This function gets called only when voting from ViewProfile.
function ProfileYNMVote(type, encryptedParams) {
    if (ConfirmYNMVote(HeaderYNMVoteBarID, type)) {
        SaveYNMVote(type, encryptedParams);
        ProcessYNMVote(HeaderYNMVoteBarID, type, encryptedParams);
        ProcessYNMVote(FooterYNMVoteBarID, type, encryptedParams);
    }
}


function YNMVote(parentClientID, type, encryptedParams, source) {
    if (ConfirmYNMVote(parentClientID, type)) {
        SaveYNMVote(type, encryptedParams, source);
        ProcessYNMVote(parentClientID, type, encryptedParams);
    }
}

//If necessary, displays a prompt to confirm that the user wants to change his/her YNM vote.
function ConfirmYNMVote(parentClientID, type) {
    parentClientID = setParentClientIdIfNotExist(parentClientID);
    var currentStatus = document.forms[window.document.forms[0].name].elements[parentClientID + "_YNMVoteStatus"];
    var process = true;

    //only the three least significant bits are used to indicate whether you have voted for this member
    //the more significant bits indicate whether they have voted for you, so ignore them
    currentVote = currentStatus.value & 7;

    //Type =				1 for yes, 2 for no, 3 for maybe
    //but currentVote =		1 for yes, 2 for no, 4 for maybe
    //so typeStatusMask is a value we can compare to currentStatus
    var typeStatusMask = type;
    if (type == "3")
        typeStatusMask = 4;

    var hasVote = (currentVote > 0);

    // bypass confirmation on existing selection and no previous selection
    if (hasVote && currentVote != typeStatusMask) {
        //18106 for FireFox: can't getElementByID because form elements only had name property - not ID
        process = confirm(document.forms[0].elements['js_ynmConfirmText'].value);
    }

    return process;
}

//Updates the images and the hidden form input to reflect the new YNM vote.
function ProcessYNMVote(parentClientID, type) {
    parentClientID = setParentClientIdIfNotExist(parentClientID);
    var currentStatus = document.forms[window.document.forms[0].name].elements[parentClientID + "_YNMVoteStatus"];

    switch (type) {
        case "1":
            SetYNMImage(document.getElementById(parentClientID + "_ImageYes"), true, type, parentClientID);
            SetYNMImage(document.getElementById(parentClientID + "_ImageMaybe"), false, type, parentClientID);
            SetYNMImage(document.getElementById(parentClientID + "_ImageNo"), false, type, parentClientID);
            if ((currentStatus.value & 2) == 2) {
                currentStatus.value = currentStatus.value ^ 2;
            }
            if ((currentStatus.value & 4) == 4) {
                currentStatus.value = currentStatus.value ^ 4;
            }
            currentStatus.value = currentStatus.value | 1;

            //If the other user has clicked yes, display the "you both clicked yes" icon:
            if ((currentStatus.value & 8) == 8)
                SetBothSaidYes(parentClientID, "visible");

            break;

        case "2":
            SetYNMImage(document.getElementById(parentClientID + "_ImageYes"), false, type, parentClientID);
            SetYNMImage(document.getElementById(parentClientID + "_ImageMaybe"), false, type, parentClientID);
            SetYNMImage(document.getElementById(parentClientID + "_ImageNo"), true, type, parentClientID);
            if ((currentStatus.value & 1) == 1) {
                currentStatus.value = currentStatus.value ^ 1;
            }
            if ((currentStatus.value & 4) == 4) {
                currentStatus.value = currentStatus.value ^ 4;
            }
            currentStatus.value = currentStatus.value | 2;

            //Hide the "you both clicked yes" icon:
            SetBothSaidYes(parentClientID, "hidden");
            break;

        case "3":
            SetYNMImage(document.getElementById(parentClientID + "_ImageYes"), false, type, parentClientID);
            SetYNMImage(document.getElementById(parentClientID + "_ImageMaybe"), true, type, parentClientID);
            SetYNMImage(document.getElementById(parentClientID + "_ImageNo"), false, type, parentClientID);
            if ((currentStatus.value & 1) == 1) {
                currentStatus.value = currentStatus.value ^ 1;
            }
            if ((currentStatus.value & 2) == 2) {
                currentStatus.value = currentStatus.value ^ 2;
            }
            currentStatus.value = currentStatus.value | 4;

            //Hide the "you both clicked yes" icon:
            SetBothSaidYes(parentClientID, "hidden");
            break;
    }
}

//Saves the YNM vote to the DB
function SaveYNMVote(type, encryptedParams, source) {
    if (window.frames["FrameYNMVote"] != null) {
        window.frames["FrameYNMVote"].location.href = "/Applications/MemberProfile/AttractionBarSave.aspx?YNMLT=" + type + "&ynmp=" + encryptedParams + "&source=" + source;
    }
}

function GoToNextIfHaveVoted(destinationUrl, message) {
    var str;
    var index;
    var currentName;
    var currentValue;

    for (var i = 0; i < document.forms[window.document.forms[0].name].elements.length; i++) {
        currentName = document.forms[window.document.forms[0].name].elements[i].name;
        index = currentName.indexOf("YNMVoteStatus");
        if (index != -1) {
            currentValue = document.forms[window.document.forms[0].name].elements[i].value;
            break;
        }
    }

    if (index == -1) {
    }
    else {
        // ensure there is a vote value and that the vote value pertains to the member's votes
        if (currentValue > 0 && currentValue != 8 && currentValue != 16 && currentValue != 32) {
            // vote has been cast already
            window.location = destinationUrl;
            return;
        }
    }

    alert(message);

    return;
}

/********************************************************************************/
/*  FUNCTION to format link for deleting a hot list member.  taken from legacy site.
/********************************************************************************/
function UnListHotList(byMember, toMember, strMsg, strConfirmation) {
    if (confirm(strMsg)) {
        window.location = "/Applications/HotList/View.aspx?a=delete&MemberID=" + byMember + "&ToMemberID=" + toMember + "&FromMemberID=" + byMember + "&rc=" + strConfirmation;
    }
    else { }
}

// Private method to open Connect IM pop-up window
// Couldn't reuse launchImWindow because the windownames for initiate window and accept IM window 
// are different
function launchConnectIMWindow(strURL, strWindowname) {

    //set width and height amd properties of browser window
    var intWidth = 740;
    var intHeight = 520;
    var strProperties = "height=" + intHeight + ",width=" + intWidth + "," + "status=yes, Resizable=no,scrollbars=no,status=no";

    checkForOpenIMWindowThenLaunch(strURL, strWindowname, strProperties);
}

function isCorsSupported() {
    return !!(window.XMLHttpRequest && 'withCredentials' in new XMLHttpRequest());
}

function makePreflightCallsForIM () {
	// make dummy calls to the Spark API and ejabberd server in order to pre-fetch DNS to the browser
	// and hopefully get IM to launch faster, particularly for international users.  Ignore results of the calls
	var apiUrl = 'https://api.spark.net/admin/healthcheck';
	var ejabUrl = '//chat.spark-networks.com/http-bind';
	if (isCorsSupported()) {
		$j.ajax({ url: apiUrl });
		$j.ajax({ url: ejabUrl });
	}
	else if (typeof XDomainRequst !== null) { // IE
		var xdr = new XDomainRequest();
		xdr.open('GET', ejabUrl); // only do the ejab call (no API call), since IE would complain about making an https call from an http page
		xdr.send();
	}
}

function launchEjabIMWindow(strURL, strWindowname) {

	makePreflightCallsForIM();
	//set width and height amd properties of browser window
	var intWidth = 580;
    var intHeight = 720;
    var strProperties = "height=" + intHeight + ",width=" + intWidth + "," + "status=yes,resizable=yes,scrollbars=no,status=no";

    checkForOpenIMWindowThenLaunch(strURL, strWindowname, strProperties);
}

// Private method to open IM pop-up window
// This method should not be called from outside. The purpose of the strDestinationMemberId
// parameter is simply to create a unique browser window name.
// Use LaunchNewIMConversation or JoinIMConversation instead
function launchImWindow(strURL, strDestinationMemberId) {
    //set width and height amd properties of browser window
    var intWidth = 360;
    var intHeight = 470;
    var uniquewindowname = "IM" + strDestinationMemberId;
    var strProperties = "height=" + intHeight + ",width=" + intWidth + "," + "status=yes, Resizable=no,scrollbars=no,status=no";

    checkForOpenIMWindowThenLaunch(strURL, uniquewindowname, strProperties);

    //did not use launchWindow function because that would always refresh the page (and thereby
    //potentially cause the loss of any ongoing instant message text conversation
    // launchWindow(strURL , uniquewindowname , width , height , strproperties);
}

// Originally inside launchImWindow
// Separated so that the logic can be reused
function checkForOpenIMWindowThenLaunch(strURL, strUniqueWindowName, strProperties) {
    //console.log("uniqueWindowName:" + strUniqueWindowName);
    //check to see if IM window is already open
    //(this is a javascript trick to return a reference by target name without specifying URL - it opens about:blank)
    var imWindowHandle = window.open("", strUniqueWindowName, strProperties);
    //handle permission denied errors by closing window first, then re-opening (16725)
    var imWindowLocationIsDeniedBecauseOutsideDomain = false;
    try {
        if (imWindowHandle) {
            //try and get the location of the IM window. This will raise a permission denied security error
            //if the open window is on another domain (security built-in to browser)
            var imWindowLocationTemp = imWindowHandle.location.href;
        }
    }
    catch (e) {
        //alert("IM window already open, but on another domain. This causes a security error: " + e.message);
        imWindowLocationIsDeniedBecauseOutsideDomain = true;
        //closes window then re-opens it, thereby avoiding permission denied error (and erasing any ongoing conversations)
        imWindowHandle.close();
        imWindowHandle = window.open(strURL, strUniqueWindowName, strProperties);
    }
    //only continue if no security problems with IM window outside current domain
    if (!imWindowLocationIsDeniedBecauseOutsideDomain
		&& imWindowHandle
		&& imWindowHandle.location.href.indexOf(strURL) >= 0
		&& !imWindowHandle.closed) {

        //if it exists, and the URL is the IM URL, simply bring window into focus
        //without refreshing (so as not to lose any ongoing IM conversation text)
        imWindowHandle.focus();
    }
    else {
        if (imWindowHandle) {
            imWindowHandle.location.href = strURL;
            imWindowHandle.focus(); //bring into focus for good measure, although may be unncessary
        }
        else {
            //probably a pop-up blocker... do nothing :( TODO: resource in other languages
            //alert("use of Instant Messenger requires pop-up blocker to be disabled.");
        }
    }
}

// Launches new Instant Messenger window from Whos online 
// and search pages (replaces former LaunchDotNetIMByMemberID)
// strDestinationMemberId is the member ID of the user that the user wants to initiate a conversation with
function LaunchNewIMConversation(strDestinationMemberId) {

    //append ID of destination member ID
    //adding action parameter to IM path, since it's javascript20 folder we will get this only in new layout, action=19400(instantcommunicator pageid)
    var url = GetInstantMessengerPath() + '?DestinationMemberID=' + strDestinationMemberId + "&action=19400";
    launchImWindow(url, strDestinationMemberId);

}

// Launches Instant Messenger window from a user choosing
// "Yes" to join an IM initiated by other user (see ImYes)
// Requires the conversation ID of the conversation user wishes to join
// and the member id of the user that the user is accepting a conversation with
function JoinIMConversation(strDestinationMemberId, strConversationKey) {

    var url = GetInstantMessengerPath() + '?DestinationMemberID=' + strDestinationMemberId
		+ '&ConversationKey=' + strConversationKey;
    launchImWindow(url, strDestinationMemberId);
}

function LaunchNewConnectIMConversation(strUsername, strDestinationMemberId, strSpwscypher) {
    //    var url = GetConnectInstantMessengerPath() + 'chat.html?u=' + strUsername + "&SPWSCYPHER=" + encodeURIComponent(strSpwscypher);
    var url = GetConnectInstantMessengerPath() + 'chat.html?mid=' + strDestinationMemberId + "&SPWSCYPHER=" + encodeURIComponent(strSpwscypher);
    launchConnectIMWindow(url, 'ftchat_im_' + strUsername);
}

function JoinConnectIMConversation(strIMID, strOtherUsername, strSpwscypher) {
    var url = GetConnectInstantMessengerPath() + 'chat.html?imid=' + strIMID + '&u=' + strOtherUsername + "&SPWSCYPHER=" + encodeURIComponent(strSpwscypher);
    launchConnectIMWindow(url, 'im' + strIMID);
   }

function LaunchNewEjabIMConversation(targetMemberId, webChatUrl, evar27Value) {
    var url = webChatUrl + '/IM.html?memberId=' + targetMemberId + "&evar27Value=" + evar27Value;
    s.pageName = "im_chat_window";
    s.events = '';	
    s.t();
    launchEjabIMWindow(url, 'im_' + targetMemberId);
}

function JoinEjabIMConversation(targetMemberId, webChatUrl, conversationTokenId) {
    var url = webChatUrl + '/IM.html?memberId=' + targetMemberId;
    if (conversationTokenId) {
        url = url + '&conversationTokenId=' + conversationTokenId;
    }
    s.pageName = "im_notifier";
    s.events = "event68";
    s.t();
    launchEjabIMWindow(url, 'im_' + targetMemberId);
}

function LaunchConsolidatedIMChat(memberid, targetUsername)
{
    //this is defined in connect js from Utah for consolidated presence/im
    SparkPresence.open_im_window('username=' + memberid, "im_" + targetUsername);
}

function GetConnectInstantMessengerPath() {
    var hostname = '';

    if (location.hostname.toLowerCase().indexOf('www.') > -1) {
        hostname = location.hostname.toLowerCase().replace('www.', 'connect.');
    }
    else if (location.hostname.toLowerCase().indexOf('admin.') > -1) {
        hostname = location.hostname.toLowerCase().replace('admin.', 'connect.');
    }
    else {
        hostname = 'connect.' + location.hostname;
    }

    return 'http://' + hostname + '/imclient/';
}

// Gets the path to the aspx page that hosts the Instant Messenger application
function GetInstantMessengerPath() {

    // if the location.hostname is a production site and does NOT include the 'www' prefix, we need to add it.
    // Userplane DHTML (Hebrew version will not function correctly without the host prefix. 
    // (this is as of 8/23/05 bug 14968)
    var urlPrefix = '';

    if ('cupid.co.il' == location.hostname.toLowerCase()) {
        urlPrefix = 'http://www.cupid.co.il';
    }
    if ('jdate.co.il' == location.hostname.toLowerCase()) {
        urlPrefix = 'http://www.jdate.co.il';
    }
    // link to IM should not be to secure site - remove https
    if (location.href.indexOf('https') > -1) {
        urlPrefix = "http://" + location.hostname;
    }

    return urlPrefix + '/Applications/InstantMessenger/upInstantCommunicator.aspx'

}

// Loops through the validators on the page and focuses on the first failed control
function ClientValidateAndFocus() {
    var x;

    // Run the page validation then evaluate the validators
    Page_ClientValidate();

    for (x in Page_Validators) {
        if (!Page_Validators[x].isvalid) {
            badControl = document.getElementById(Page_Validators[x].controltovalidate);

            if (badControl != null)
                badControl.focus();
            break;
        }
    }
}

function CaseInsensitiveCompareValidatorEvaluateIsValid(val) {
    var valueToValidate = ValidatorTrim(ValidatorGetValue(val.controltovalidate));
    var valueToCompare = ValidatorTrim(ValidatorGetValue(val.controltocompare));

    if (valueToValidate != null && valueToCompare != null) {
        if (valueToValidate.toLowerCase() == valueToCompare.toLowerCase()) {
            return true;
        }
    }

    return false;
}

function MultiValidatorEvaluateIsValid(val) {
    try {
        var valueToValidate =
				ValidatorTrim(ValidatorGetValue(val.controltovalidate));

        var controlToValidate = document.forms[window.document.forms[0].name].elements[val.controltovalidate];

        if (val.isRequired == 'False' && valueToValidate.length == 0)
            return true;

        if (val.isRequired == 'True') {
            if (controlToValidate.type == 'checkbox') {
                if (!controlToValidate.checked) {
                    val.innerText = '';
                    val.innerHTML = val.requiredErrorMessage;

                    MultiValitorSetControlFocus(val.controltovalidate);

                    return false;
                }
                else {	// Valid checkbox, exit validation script.
                    return true;
                }
            }

            if (valueToValidate.length == 0) {
                val.innerText = val.requiredErrorMessage;

                if (val.showPopup == 'True') {
                    alert(val.innerText);
                }
                else {
                    if (val.appendToErrorMessage.length != 0) {
                        val.innerText = '';
                        val.innerHTML = val.requiredErrorMessage;
                        val.innerHTML += val.appendToErrorMessage;
                    }
                }

                MultiValitorSetControlFocus(val.controltovalidate);

                return false;
            }
        }

        if (val.maxLength != '0') {
            if (valueToValidate.length > parseInt(val.maxLength, 10)) {
                val.innerText = val.maximumLengthErrorMessage;

                if (val.showPopup == 'True') {
                    alert(val.innerText);
                }
                else {
                    if (val.appendToErrorMessage.length != 0) {
                        val.innerText = '';
                        val.innerHTML = val.maximumLengthErrorMessage;
                        val.innerHTML += val.appendToErrorMessage;
                    }
                }

                MultiValitorSetControlFocus(val.controltovalidate);

                return false;
            }
        }

        if (val.minLength != '0') {
            if (valueToValidate.length < parseInt(val.minLength, 10)) {
                val.innerText = val.minimumLengthErrorMessage;

                if (val.showPopup == 'True') {
                    alert(val.innerText);
                }
                else {
                    if (val.appendToErrorMessage.length != 0) {
                        val.innerText = '';
                        val.innerHTML = val.minimumLengthErrorMessage;
                        val.innerHTML += val.appendToErrorMessage;
                    }
                }

                MultiValitorSetControlFocus(val.controltovalidate);

                return false;
            }
        }

        if (val.minVal != '0') {
            if (valueToValidate < parseInt(val.minVal, 10)) {
                val.innerText = val.minValErrorMessage;

                if (val.showPopup == 'True') {
                    alert(val.innerText);
                }
                else {
                    if (val.appendToErrorMessage.length != 0) {
                        val.innerText = '';
                        val.innerHTML = val.minValErrorMessage;
                        val.innerHTML += val.appendToErrorMessage;
                    }
                }

                MultiValitorSetControlFocus(val.controltovalidate);

                return false;
            }
        }

        if (val.maxVal != '0') {
            if (valueToValidate > parseInt(val.maxVal, 10)) {
                val.innerText = val.maxValErrorMessage;

                if (val.showPopup == 'True') {
                    alert(val.innerText);
                }
                else {
                    if (val.appendToErrorMessage.length != 0) {
                        val.innerText = '';
                        val.innerHTML = val.maxValErrorMessage;
                        val.innerHTML += val.appendToErrorMessage;
                    }
                }

                MultiValitorSetControlFocus(val.controltovalidate);

                return false;
            }
        }

        if (val.requiredTypeRegEx.length > 0) {

            if (valueToValidate == "")
                return true;

            var rx = new RegExp(val.requiredTypeRegEx);
            var matches = rx.exec(valueToValidate);
            if (!(matches != null && valueToValidate == matches[0])) {
                val.innerText = val.requiredTypeErrorMessage;

                if (val.showPopup == 'True') {
                    alert(val.innerText);
                }
                else {
                    if (val.appendToErrorMessage.length != 0) {
                        val.innerText = '';
                        val.innerHTML = val.requiredTypeErrorMessage;
                        val.innerHTML += val.appendToErrorMessage;
                    }
                }

                MultiValitorSetControlFocus(val.controltovalidate);

                return false;
            }
        }
    }
    catch (Ex) {
    }

    return true;
}

var isValidationFocusSet = false;
function MultiValitorSetControlFocus(controlToValidate) {
    if (!isValidationFocusSet) {
        document.forms[window.document.forms[0].name].elements[controlToValidate].focus();
        isValidationFocusSet = true;
    }
}


var onListMenu = false

///The function that governs Add2List control (adding to Hotlist)
///Note: onblur in Safari will only work on INPUT, TEXTAREA and SELECT elements.
function ToggleDisplay(element, Visible) {
    var menu = document.getElementById('smenu' + element);

    if (Visible == false) {
        if (onListMenu == false) {
            menu.style.display = 'none';
        }
    }
    else {
        //menu.style.display = 'block';
        $j('#' + 'smenu' + element).toggle();
    }
}

function setHighlight(Over) {
    onListMenu = Over;
}

function ChangeImage(ImageName, NewImage, Directory) {
    if (document.images) {
        if (String(Directory) != "undefined")
            document[ImageName].src = Directory + "/" + NewImage;
        else
            document[ImageName].src = "images/" + NewImage;
    }

}

/// This method checks if the Enter key was pressed, and if it was, fires the click 
/// event for the given button object. This method is called from the onkeydown
/// event on textboxes where you want the enter key to submit the form or fire
/// validation. This method expects to receive a button object, NOT a string id or name.
/// (You can apparently pass an object to this method by calling the name of 
/// button object directly, but only if the object is inside a <form> tag
/// in FireFox (IE is more forgiving)). Also, IE recognizes the event.KeyCode
/// property while FF and others do not. Likewise, IE does NOT recognize the
/// event.which property, hence you must use browser detection techniques 
/// before checking if the Enter key was pressed.
function setAutoSubmitButton(btn, event) {
    // accounts for IE
    if (document.all) {
        if (event.keyCode == 13) {
            event.returnValue = false;
            event.cancel = true;
            btn.click();
        }
    }
    // accounts for FF and NN
    else if (window.sidebar) {
        if (event.which == 13) {
            event.returnValue = false;
            event.cancel = true;
            btn.click();
        }
    }
    // accounts for others 
    // 18285 - use a string to represent the id of a button object (not the object) itself when using getElementById
    else if (document.getElementById(btn.id)) {
        if (event.which == 13) {
            event.returnValue = false;
            event.cancel = true;
            btn.click();
        }
    }
    // accounts for others
    else if (document.layers) {
        if (event.which == 13) {
            event.returnValue = false;
            event.cancel = true;
            btn.click();
        }
    }
}


function readKey(e) {
    //window.status = e.keyCode;
    //alert(e.keyCode);
}
//document.onkeypress = readKey;


/********************************************************************************/
/*  FUNCTION to float div over content
/********************************************************************************/
function MM_findObj(n, d) { //v4.01
    var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById) x = d.getElementById(n); return x;
}

function MM_showHideLayers() { //v6.0
    var i, p, v, obj, args = MM_showHideLayers.arguments;
    for (i = 0; i < (args.length - 2); i += 3) if ((obj = MM_findObj(args[i])) != null) {
        v = args[i + 2];
        if (obj.style) { obj = obj.style; v = (v == 'show') ? 'visible' : (v == 'hide') ? 'hidden' : v; }
        obj.visibility = v;
    }
}


function QueryString(qs) {
    if (qs.length > 1) {
        this.qs = qs.substring(1, qs.length);
    }
    else {
        this.qs = null;
    }

    this.keyValuePairs = new Array();

    if (qs) {
        for (var i = 0; i < this.qs.split("&").length; i++) {
            this.keyValuePairs[i] = this.qs.split("&")[i];
        }
    }

    this.getKeyValuePairs = function() {
        return this.keyValuePairs;
    }

    this.getValue = function(keyvalue) {
        for (var i = 0; i < this.keyValuePairs.length; i++) {
            if (this.keyValuePairs[i].split("=")[0] == keyvalue) {
                return this.keyValuePairs[i].split("=")[1];
            }
        }
        return false;
    }

    this.getLength = function() {
        return this.keyValuePairs.length;
    }

    // removes a querystring parameter ("&key=value" turns to "" and "?key=value" turnes to "?")
    this.RemoveQueryKey = function(queryKey, location) {
        for (var i = 0; i < this.keyValuePairs.length; i++) {
            if (this.keyValuePairs[i].split("=")[0] == queryKey) {
                var retval;
                retval = location.toString().replace("&" + queryKey + "=" + this.getValue(queryKey), "");
                retval = retval.toString().replace("?" + queryKey + "=" + this.getValue(queryKey), "?");
                return retval;
            }
        }
        return false;
    }

    this.ReplaceQueryKey = function(queryKey, replaceValue, location) {
        for (var i = 0; i < this.keyValuePairs.length; i++) {
            if (this.keyValuePairs[i].split("=")[0] == queryKey) {
                var retval;
                retval = location.toString().replace("&" + queryKey + "=" + this.getValue(queryKey), "&" + queryKey + "=" + replaceValue);
                retval = retval.toString().replace("?" + queryKey + "=" + this.getValue(queryKey), "?" + queryKey + "=" + replaceValue);
                return retval;
            }
        }
        return false;
    }

}

//
// This is where the file formerly called dg_dhtmlNotifier.js has been appended to this one - lucena
//

var dg_dhtmlNotifier_CONST_ALERT_WIDTH = 165;
var dg_dhtmlNotifier_CONST_DIRECTION_LEFT = 'left';
var dg_dhtmlNotifier_CONST_DIRECTION_RIGHT = 'right';
var dg_dhtmlNotifier_CONST_DIRECTION_UP = 'up';
var dg_dhtmlNotifier_CONST_ALERT_HEIGHT = 136;
var dg_dhtmlNotifier_CONST_VISIBLE_LEFT = -10; //Original value was 5.						20070406 modified by RB  
var dg_dhtmlNotifier_CONST_INVISIBLE_LEFT = dg_dhtmlNotifier_CONST_VISIBLE_LEFT - dg_dhtmlNotifier_CONST_ALERT_WIDTH;
var dg_dhtmlNotifier_CONST_TOP_POSITION = 100;  //5; - this is temporary change. it'll be adjusted when new IM style work done.
var dg_dhtmlNotifier_CONST_MAJOR_STEP_PIXELS = 6;
var dg_dhtmlNotifier_CONST_MINOR_STEP_PIXELS = 1;
var dg_dhtmlNotifier_CONST_STEP_TIME = 20;
var dg_dhtmlNotifier__transInEffect = false;

//TL - Added left override for site redesign, seems js code in checkim uses parent window depending on the browser which causes the alert window to be way off the screen.
var dg_dhtmlNotifier_CONST_VISIBLE_LEFT_OVERRIDE = 700; //5; - this is temporary change. it'll be adjusted when new IM style work done.
var dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE = dg_dhtmlNotifier_CONST_VISIBLE_LEFT_OVERRIDE + (dg_dhtmlNotifier_CONST_ALERT_WIDTH * 2);
var dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE = dg_dhtmlNotifier_CONST_DIRECTION_RIGHT;

// Holder of active IMs, array of { alertId:text, visible:bool, html:string }
var dg_IMAlerts = new Array();

function dg_IMFromChild(fromChildList) {
    if (document.body.dir == 'rtl') {
        dg_dhtmlNotifier_CONST_ALERT_WIDTH = -165;
        dg_dhtmlNotifier_CONST_VISIBLE_LEFT_OVERRIDE = -250
        dg_dhtmlNotifier_CONST_INVISIBLE_LEFT = dg_dhtmlNotifier_CONST_VISIBLE_LEFT - dg_dhtmlNotifier_CONST_ALERT_WIDTH;
        dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE = dg_dhtmlNotifier_CONST_VISIBLE_LEFT_OVERRIDE + (dg_dhtmlNotifier_CONST_ALERT_WIDTH * 2);
    }
    dg_IMLoad(fromChildList);
    dg_dhtmlNotifier_setStartingPositions();
    dg_dhtmlNotifier_displayIMAlerts();
    dg_dhtmlNotifier_removeOldIMAlerts(fromChildList);

}

//This function removes any old IM alerts that are no longer valid due to initiator closing window, or timeout 16615
function dg_dhtmlNotifier_removeOldIMAlerts(newAlerts) {

    if (typeof (newAlerts) == 'undefined' || !newAlerts || !newAlerts.length) {
        //remove ALL current alerts
        for (var i = 0; i < dg_IMAlerts.length; i++) {
            //dg_dhtmlNotifier_deleteIMAlert(dg_IMAlerts[i].alertId);
            if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
                dg_dhtmlNotifier_moveNode(dg_IMAlerts[i].alertId, 0 - dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'left');
            }
            else {
                dg_dhtmlNotifier_moveNode(dg_IMAlerts[i].alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'right');
            }
        }
    }
    else {
        //loop through existing alerts on page if any
        for (var i = 0; i < dg_IMAlerts.length; i++) {
            //if not in the "new" alert list, then remove this alert
            if (dg_IMIsOld(dg_IMAlerts[i].alertId, newAlerts)) {
                //dg_dhtmlNotifier_deleteIMAlert(dg_IMAlerts[i].alertId);
                //dg_dhtmlNotifier_moveNode(dg_IMAlerts[i].alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT, 'left');

                if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
                    dg_dhtmlNotifier_moveNode(dg_IMAlerts[i].alertId, 0 - dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'left');
                }
                else {
                    dg_dhtmlNotifier_moveNode(dg_IMAlerts[i].alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'right');
                }
            }
        }
    }

}

//Returns true if dhtml alert is no longer in the "new" alert list that arrived from middle tier
function dg_IMIsOld(alertId, newAlerts) {
    //loop through all "new" alerts
    for (var i = 0; i < newAlerts.length; i++) {
        if (newAlerts[i].alertId == alertId) {
            //keep this alert around - because it's in the alert list from the middle tier
            return false;
        }
    }
    return true
}

function ConnectIMYes(strIMID, strSendingUsername, strDestinationMemberId, conversationInvitationKey, strSpwscypher) {
    var alertId = 'alertId_' + strDestinationMemberId;
    //make the notifier go away (don't know why or how this code works, but passing in 'left' is the key)
    //dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT, 'left');
    if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
        dg_dhtmlNotifier_moveNode(alertId, 0 - dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'left');
    }
    else {
        dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'right');
    }

    JoinConnectIMConversation(strIMID, strSendingUsername, strSpwscypher);

    removeInvitation(conversationInvitationKey, false); //make AJAX call to accept (isRejection false)
}

function ConnectIMNo(strIMID, strDestinationMemberId, conversationInvitationKey, strSpwscypher) {
    var alertId = 'alertId_' + strDestinationMemberId;

    //make the notifier go away (don't know why or how this code works, but passing in 'left' is the key)
    //dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT, 'left');
    if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
        dg_dhtmlNotifier_moveNode(alertId, 0 - dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'left');
    }
    else {
        dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'right');
    }

    removeInvitation(conversationInvitationKey, true); //make AJAX call to reject (isRejection true)
    //send a rejection message to the sender (call IFRAME with cmd.swf file)
    var url = GetConnectInstantMessengerPath() + 'im_request.html?imid=' + strIMID + '&reject=1&SPWSCYPHER=' + encodeURIComponent(strSpwscypher);
    var r = null; r = window.frm_rejectim; if (r != null) r.location.replace(url);
}

function ConnectIMClose(strIMID, strDestinationMemberId, conversationInvitationKey, strSpwscypher) {
    var alertId = 'alertId_' + strDestinationMemberId;

    //make the notifier go away (don't know why or how this code works, but passing in 'left' is the key)
    //dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT, 'left');
    if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
        dg_dhtmlNotifier_moveNode(alertId, 0 - dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'left');
    }
    else {
        dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'right');
    }

    removeInvitation(conversationInvitationKey, true); //make AJAX call to reject (isRejection true)
    //send a rejection message to the sender (call IFRAME with cmd.swf file)
    var url = GetConnectInstantMessengerPath() + 'im_request.html?imid=' + strIMID + '&later=1&SPWSCYPHER=' + encodeURIComponent(strSpwscypher);
    var r = null; r = window.frm_rejectim; if (r != null) r.location.replace(url);
}

function EjabIMYes(strDestinationMemberId, conversationInvitationKey, webchatUrl, conversationTokenId) {
    var alertId = 'alertId_' + strDestinationMemberId;
    //make the notifier go away (don't know why or how this code works, but passing in 'left' is the key)
    //dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT, 'left');
    if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
        dg_dhtmlNotifier_moveNode(alertId, 0 - dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'left');
    }
    else {
        dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'right');
    }

    //open up IM window and accept conversation invitation
    JoinEjabIMConversation(strDestinationMemberId, webchatUrl, conversationTokenId);

    removeInvitation(conversationInvitationKey, false); //make AJAX call to accept (isRejection false)
}


function EjabIMNo(strDestinationMemberId, conversationInvitationKey, conversationTokenId) {
    var alertId = 'alertId_' + strDestinationMemberId;

    //make the notifier go away (don't know why or how this code works, but passing in 'left' is the key)
    //dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT, 'left');
    if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
        dg_dhtmlNotifier_moveNode(alertId, 0 - dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'left');
    }
    else {
        dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'right');
    }

    s.pageName = 'im_notifier';
    s.events = 'event69';
    s.t();
    s.events = '';

    removeInvitation(conversationInvitationKey, true); //make AJAX call to reject (isRejection true)
	var recipientId = parseInt(strDestinationMemberId, 10);
	spark.chat.sendEjabIMRejectedMessage(recipientId, conversationTokenId);
}

function EjabIMClose(strDestinationMemberId, conversationInvitationKey, conversationTokenId) {
    var alertId = 'alertId_' + strDestinationMemberId;

    //make the notifier go away (don't know why or how this code works, but passing in 'left' is the key)
    //dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT, 'left');
    if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
        dg_dhtmlNotifier_moveNode(alertId, 0 - dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'left');
    }
    else {
        dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'right');
    }

    var recipientId = parseInt(strDestinationMemberId, 10);
    spark.chat.sendEjabIMRejectedMessage(recipientId, conversationTokenId);
    removeInvitation(conversationInvitationKey, true); //make AJAX call to reject (isRejection true)
    //send a rejection message to the sender (call IFRAME with cmd.swf file)
    var url = GetInstantMessengerPath() + '?DestinationMemberID=' + strDestinationMemberId
		+ '&ConversationKey=' + conversationInvitationKey
		+ '&RejectIM=false';
    var r = window.frm_rejectim;
    if (r != null) {
    	r.location.replace(url);
    }

	s.pageName = 'im_notifier';
	s.events = 'event69';
	s.t();
	s.events = '';
}

// Method for when user selects "Yes" to accepting an Instant Message conversation
// initiated by another user
// strDestinationMemberId is the id of the member to which the user is accepting
// a conversation
function IMYes(strDestinationMemberId, conversationInvitationKey) {
    var alertId = 'alertId_' + strDestinationMemberId;
    //make the notifier go away (don't know why or how this code works, but passing in 'left' is the key)
    //dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT, 'left');
    if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
        dg_dhtmlNotifier_moveNode(alertId, 0 - dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'left');
    }
    else {
        dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'right');
    }

    //open up IM window and accept conversation invitation
    JoinIMConversation(strDestinationMemberId, conversationInvitationKey);

    removeInvitation(conversationInvitationKey, false); //make AJAX call to accept (isRejection false)
}

// Method for when user selects "No" to accepting an Instant Message conversation
// invitation initiated by another user.
// strDestinationMemberId is the id of the member to which the user is rejecting
// a conversation 
function IMNo(strDestinationMemberId, conversationInvitationKey) {
    var alertId = 'alertId_' + strDestinationMemberId;

    //make the notifier go away (don't know why or how this code works, but passing in 'left' is the key)
    //dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT, 'left');
    if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
        dg_dhtmlNotifier_moveNode(alertId, 0 - dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'left');
    }
    else {
        dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'right');
    }

    removeInvitation(conversationInvitationKey, true); //make AJAX call to reject (isRejection true)
    //send a rejection message to the sender (call IFRAME with cmd.swf file)
    var url = GetInstantMessengerPath() + '?DestinationMemberID=' + strDestinationMemberId
		+ '&ConversationKey=' + conversationInvitationKey
		+ '&RejectIM=true';
    var r = null; r = window.frm_rejectim; if (r != null) r.location.replace(url);


}

// Method for when user selects "Close" to upgrading to reply to an IM invitation
// strDestinationMemberId is the id of the member to which the user is rejecting
// a conversation 
function IMClose(strDestinationMemberId, conversationInvitationKey) {
    var alertId = 'alertId_' + strDestinationMemberId;

    //make the notifier go away (don't know why or how this code works, but passing in 'left' is the key)
    //dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT, 'left');
    if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
        dg_dhtmlNotifier_moveNode(alertId, 0 - dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'left');
    }
    else {
        dg_dhtmlNotifier_moveNode(alertId, dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE, 'right');
    }

    removeInvitation(conversationInvitationKey, true); //make AJAX call to reject (isRejection true)
    //send a rejection message to the sender (call IFRAME with cmd.swf file)
    var url = GetInstantMessengerPath() + '?DestinationMemberID=' + strDestinationMemberId
		+ '&ConversationKey=' + conversationInvitationKey
		+ '&RejectIM=false';
    var r = null; r = window.frm_rejectim; if (r != null) r.location.replace(url);
}


function dg_IMLoad(newAlerts) {
    var domIMAlerts = document.getElementById('IMAlerts');

    var newHTML = "";

    if (typeof (newAlerts) == 'undefined' || !newAlerts || !newAlerts.length)
        return;

    for (var i = 0; i < newAlerts.length; i++) {
        var xna = newAlerts[i];
        if (dg_IMIsNew(xna.alertId)) {
            dg_IMAlerts.push(xna);
            if (xna.html != null) {
                newHTML += xna.html;
            }
        }
    }

    if (domIMAlerts != null && newHTML != null) {
        domIMAlerts.innerHTML += newHTML;
    }
}

function dg_IMIsNew(alertId) {
    if (alertId == null) return false;
    for (var i = 0; i < dg_IMAlerts.length; i++) {
        if (dg_IMAlerts[i].alertId == alertId)
            return false;
    }
    return true;
}


function dg_dhtmlNotifier_displayIMAlerts() {
    if (dg_IMAlerts == null
        || dg_dhtmlNotifier_CONST_VISIBLE_LEFT == null
        || dg_dhtmlNotifier_CONST_DIRECTION_RIGHT == null
        || dg_dhtmlNotifier__transInEffect == null) {
        return;
    }

    for (var i = 0; i < dg_IMAlerts.length; i++) {
        var xna = dg_IMAlerts[i];
        var element = document.getElementById(xna.alertId);

        if (element != null) {
            var currentLeft = parseInt(element.style.left.replace(/px/, ''), 10);
            dg_dhtmlNotifier_CONST_VISIBLE_LEFT = dg_dhtmlNotifier_CONST_VISIBLE_LEFT_OVERRIDE;

            if (currentLeft != dg_dhtmlNotifier_CONST_VISIBLE_LEFT) {
                if (!dg_dhtmlNotifier__transInEffect) {

                    if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
                        dg_dhtmlNotifier_moveNode(xna.alertId,
						dg_dhtmlNotifier_CONST_VISIBLE_LEFT,
                        dg_dhtmlNotifier_CONST_DIRECTION_RIGHT);
                    }
                    else {
                        dg_dhtmlNotifier_moveNode(xna.alertId,
						dg_dhtmlNotifier_CONST_VISIBLE_LEFT,
                        dg_dhtmlNotifier_CONST_DIRECTION_LEFT);
                    }
                }
                else {
                    setTimeout('dg_dhtmlNotifier_displayIMAlerts()', 1000);
                    break;
                }
            }
        }
    }
}

function dg_dhtmlNotifier_setStartingPositions() {
    if (dg_dhtmlNotifier_CONST_TOP_POSITION == null
        || dg_IMAlerts == null
        || dg_dhtmlNotifier_CONST_ALERT_WIDTH == null
        || dg_dhtmlNotifier_CONST_ALERT_HEIGHT == null) {
        return;
    }

    var startTop = dg_dhtmlNotifier_CONST_TOP_POSITION;

    for (var i = 0; i < dg_IMAlerts.length; i++) {
        var xna = dg_IMAlerts[i];
        var element = document.getElementById(xna.alertId);

        if (element != null) {
            if (!xna.visible) {
                element.style.position = 'absolute';
                element.style.top = startTop + 'px';

                if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
                    element.style.left = '-' + dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE + 'px';
                }
                else {
                    element.style.left = dg_dhtmlNotifier_CONST_INVISIBLE_LEFT_OVERRIDE + 'px';
                }
            }

            startTop += dg_dhtmlNotifier_CONST_ALERT_HEIGHT;
        }
    }
}

function dg_dhtmlNotifier_setNodeVisibility(nodeId, visible) {
    if (nodeId == null || visible == null || dg_IMAlerts == null)
        return;

    for (var i = 0; i < dg_IMAlerts.length; i++) {
        var xna = dg_IMAlerts[i];
        if (nodeId == xna.alertId) {
            xna.visible = visible;
            break;
        }
    }
}

function dg_dhtmlNotifier_moveNode(nodeId, to, direction) {
    if (nodeId == null || to == null || direction == null
        || dg_dhtmlNotifier__transInEffect == null
        || dg_dhtmlNotifier_CONST_DIRECTION_RIGHT == null
        || dg_dhtmlNotifier_CONST_DIRECTION_LEFT == null
        || dg_dhtmlNotifier_CONST_DIRECTION_UP == null
        || dg_dhtmlNotifier_CONST_STEP_TIME == null) {
        return;
    }

    var element = document.getElementById(nodeId);

    if (element != null) {
        var currentLeft = null;
        var currentTop = null;
        var axis = null;

        try {
            // This cause the alert box go too far right.
            //to = parseInt(to, 10);
            //if(direction==dg_dhtmlNotifier_CONST_DIRECTION_LEFT)
            //  { to = -10;}
            //else
            //{ to = parseInt(to, 10); }

            to = parseInt(to, 10);
        }
        catch (NumberFormatException) {
            return;
        }

        switch (direction) {
            case dg_dhtmlNotifier_CONST_DIRECTION_RIGHT:
            case dg_dhtmlNotifier_CONST_DIRECTION_LEFT:
                axis = parseInt(element.style.left.replace(/px/, ''), 10);
                break;
            case dg_dhtmlNotifier_CONST_DIRECTION_UP:
                axis = parseInt(element.style.top.replace(/px/, ''), 10);
                break;
        }

        if (axis != to) {
            dg_dhtmlNotifier__transInEffect = true;

            switch (direction) {
                case dg_dhtmlNotifier_CONST_DIRECTION_RIGHT:
                case dg_dhtmlNotifier_CONST_DIRECTION_LEFT:
                    axis = dg_dhtmlNotifier_determineStepPixels(axis, to,
                        direction) + 'px';
                    element.style.left = axis;
                    break;
                case dg_dhtmlNotifier_CONST_DIRECTION_UP:
                    axis = dg_dhtmlNotifier_determineStepPixels(axis, to,
                        direction) + 'px';
                    element.style.top = axis;
                    break;
            }

            var call = "dg_dhtmlNotifier_moveNode('" + nodeId + "', '" + to
                + "', '" + direction + "')";

            setTimeout(call, dg_dhtmlNotifier_CONST_STEP_TIME);
        }
        else {
            if (dg_dhtmlNotifier_CONST_ACTIVITY_CENTER_SIDE != dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
                if (direction == dg_dhtmlNotifier_CONST_DIRECTION_LEFT) {
                    dg_dhtmlNotifier_deleteIMAlert(nodeId);
                }
                else if (direction == dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
                    dg_dhtmlNotifier_setNodeVisibility(nodeId, true);
                }
            }
            else {
                if (direction == dg_dhtmlNotifier_CONST_DIRECTION_RIGHT) {
                    dg_dhtmlNotifier_deleteIMAlert(nodeId);
                }
                else if (direction == dg_dhtmlNotifier_CONST_DIRECTION_LEFT) {
                    dg_dhtmlNotifier_setNodeVisibility(nodeId, true);
                }
            }

            dg_dhtmlNotifier__transInEffect = false;
        }
    }
}

function dg_dhtmlNotifier_determineStepPixels(currentPosition, to, direction) {
    if (currentPosition == null || to == null || direction == null
        || dg_dhtmlNotifier_CONST_DIRECTION_RIGHT == null
        || dg_dhtmlNotifier_CONST_MAJOR_STEP_PIXELS == null
        || dg_dhtmlNotifier_CONST_MINOR_STEP_PIXELS == null
        || dg_dhtmlNotifier_CONST_DIRECTION_LEFT == null
        || dg_dhtmlNotifier_CONST_DIRECTION_UP == null) {
        return;
    }

    var absMinorStep = Math.abs(dg_dhtmlNotifier_CONST_MINOR_STEP_PIXELS);
    var absMajorStep = Math.abs(dg_dhtmlNotifier_CONST_MAJOR_STEP_PIXELS);
    var difference = -1;
    try {
        switch (direction) {
            case dg_dhtmlNotifier_CONST_DIRECTION_RIGHT:
                currentPosition
                    += (Math.abs(currentPosition - to) % absMajorStep != 0)
                    ? dg_dhtmlNotifier_CONST_MINOR_STEP_PIXELS
                    : dg_dhtmlNotifier_CONST_MAJOR_STEP_PIXELS;
                break;
            case dg_dhtmlNotifier_CONST_DIRECTION_LEFT:
                currentPosition
                    -= ((currentPosition + to)
                        % dg_dhtmlNotifier_CONST_MAJOR_STEP_PIXELS == 0)
                    ? dg_dhtmlNotifier_CONST_MAJOR_STEP_PIXELS
                    : dg_dhtmlNotifier_CONST_MINOR_STEP_PIXELS;
                break;
            case dg_dhtmlNotifier_CONST_DIRECTION_UP:
                currentPosition
                    -= (Math.abs(dg_dhtmlNotifier_CONST_MAJOR_STEP_PIXELS)
                        % (currentPosition - to) == 0)
                    ? Math.abs(dg_dhtmlNotifier_CONST_MINOR_STEP_PIXELS)
                    : Math.abs(dg_dhtmlNotifier_CONST_MAJOR_STEP_PIXELS);
                break;
        }

    }
    catch (NumberFormatException) {
        return;
    }

    difference = Math.abs(currentPosition - to);
    if (difference <= 5 && difference >= 0)
        currentPosition = to;

    return currentPosition;
}

function dg_dhtmlNotifier_deleteIMAlert(nodeId) {
    if (nodeId == null || dg_IMAlerts == null) {
        return;
    }

    if (dg_dhtmlNotifier__transInEffect) {
        var call = "dg_dhtmlNotifier_deleteIMAlert('" + nodeId + "')";

        setTimeout(call, 30);

        return;
    }

    var alertsToMoveUp = new Array();
    var IMAlerts = document.getElementById('IMAlerts');

    if (IMAlerts != null) {
        var nodeList = IMAlerts.getElementsByTagName('DIV');

        for (var i = 0; i < dg_IMAlerts.length; i++) {
            var xna = dg_IMAlerts[i];
            if (xna.alertId == nodeId) {
                for (var j = 0; j < nodeList.length; j++) {
                    if (nodeList[j].id == xna.alertId) {
                        for (var k = j + 1; k < nodeList.length; k++) {
                            if (nodeList[k].id.indexOf('alertId') != -1) {
                                alertsToMoveUp.push(nodeList[k].id);
                            }
                        }

                        break;
                    }
                }

                dg_IMAlerts[i] = dg_IMAlerts[dg_IMAlerts.length - 1];
                dg_IMAlerts.pop();

                var node = document.getElementById(nodeId);
                IMAlerts.removeChild(node);

                break;
            }
        }

        dg_dhtmlNotifier_moveAlertsUp(alertsToMoveUp);
    }
}

function dg_dhtmlNotifier_moveAlertsUp(alertsToMoveUp) {
    if (alertsToMoveUp == null || dg_dhtmlNotifier_CONST_ALERT_HEIGHT == null
        || dg_dhtmlNotifier_CONST_DIRECTION_UP == null) {
        return;
    }

    for (var i = 0; i < alertsToMoveUp.length; i++) {
        var element = document.getElementById(alertsToMoveUp[i]);

        if (element != null) {
            var currentTop = parseInt(element.style.top.replace(/px/, ''), 10);
            var newTop = currentTop - dg_dhtmlNotifier_CONST_ALERT_HEIGHT;

            dg_dhtmlNotifier_moveNode(alertsToMoveUp[i], newTop,
                dg_dhtmlNotifier_CONST_DIRECTION_UP);
        }
    }
}
/***************************************************************
IM, MOL, Session Support including consolidation
****************************************************************/

//global variables
var isAjaxEnabled = true; //whether or not to use Ajax to make async calls or not. Turn off for testing potential bugs 18114. False means use IFRAMES.
var isSparkMOLConsolidationEnabled = false; //determines whether consolidated MOL is enabled; this is set in the code based on a setting, just search for this variable
var isSparkWebchatConsolidationEnabled = false; //determines whether consolidated Webchat is enabled; this is set in the code based on a setting, just search for this variable
var isAjaxVerboseMode = false; //like debug mode. Turns alerts on and forces execution of remote javascript which would otherwise be fire and forget.
var isOnlineTimeLimitReached = false; //when true, checking invitations and updating MOL should stop (assuming ignoreOnlineTimeLimit is false)
var req1 = null; var req2 = null; var req3 = null; //1: check invitations 2: update mol 3: sessionkeepalive (4th is remove invitations, but uses new req variable every request)
var timerOnlineTimeout; var interval1; var interval2; var interval3; var countdownTimer1 = null; //integers used for timers
var IgnoreOnlineTimeLimit = false; //certain pages, such as chat, need to ignore the on-line time limit
//initialize the online checker functions. Called once onload on pages that have menu in init() and in chat. Else called manually by clicking hyperlinks.
function initOnlineChecker(ignoreOnlineTimeLimit) {
    IgnoreOnlineTimeLimit = ignoreOnlineTimeLimit;
    if (pageIncludesIMNotifier()) {
        clearTimeout(timerOnlineTimeout); clearInterval(interval1); clearInterval(interval2); clearInterval(interval3);
        isOnlineTimeLimitReached = false; //set to false because we're initializing
        //certain pages, like chat, need to ignore online IM checking limit. Otherwise set the time limit
        if (!ignoreOnlineTimeLimit) timerOnlineTimeout = setTimeout(OnlineTimeLimitReached, 3600000); //check for invitations/updateMOL for max of 60 minutes (3600000 milliseconds)
        setIMStatusOnline(); //show UI indication of being online
        checkForInvitations(); //check for invitations on load of page
        updateMembersOnline(); //add to members on-line on load of page
        interval1 = setInterval("if (!isOnlineTimeLimitReached) checkForInvitations();", 15000); //continue checking for invitations every 15 seconds until max limit reached. (change to 15000)
        interval2 = setInterval("if (!isOnlineTimeLimitReached) updateMembersOnline()", 100000); //continue adding to members online every 100 seconds (100000 milliseconds) until max limit reached.
    }
    var statusLoggedInElement = document.getElementById("statusLoggedIn");
    if (statusLoggedInElement != null) {
        if (statusLoggedInElement.style.display == '') {
            //set session keep alive regardless of whether we're checking IM (i.e., even for hidden members) but only for logged in users
            interval3 = setInterval("sessionKeepAlive()", 300000); //keep member session alive every 5 minutes (300000) milliseconds, forever, with no limit (note: session is different than Members Online)
        }
    }
}

//Remote "AJAX" Methods or alternate methods using IFRAME
function checkForInvitations() {
    var url = "/Applications/InstantMessenger/CheckIM.aspx?ajxmode=true&random=" + randomNumber();
    if (isAjaxEnabled) {
        //show('lblCheckingInvitations');showCountDownToNextCheckForInvitations(15);executeRemoteJavascript(url,req1);
        show('lblCheckingInvitations');

        try {
            if (req1 && req1.readyState != 0) {
                req1.abort();
            }
            req1 = getXHR();
            if (req1) {
                req1.open("GET", url, true);
                req1.onreadystatechange = function() {
                    try {
                        var done = 4, ok = 200;
                        if (req1.readyState == done) {
                            if (req1.status == ok && req1.responseText) {
                                var d = document.getElementById("divAj");  //divAj represents a hidden div in activity center

                                if (d != null) {
                                    //load into dom temporarily
                                    d.innerHTML = req1.responseText;

                                    //get js IM processing script
                                    var xIMAlerts = null;
                                    var e = document.getElementById("divAjScript");
                                    if (e != null) {
                                        //eval js to create IM alert array
                                        eval(e.innerHTML);
                                    }
                                    if (xIMAlerts) {
                                        //add html content to each Im alert object in array
                                        for (var i = 0; i < xIMAlerts.length; i++) {
                                            var domNode = document.getElementById(xIMAlerts[i].alertId);
                                            if (domNode) {
                                                xIMAlerts[i].html = "<div class='IMAlert' id='" + xIMAlerts[i].alertId + "'>" + domNode.innerHTML + "</div>";
                                            }
                                            else {
                                                xIMAlerts[i].html = "<div id='" + xIMAlerts[i].alertId + "'>Alert: " + xIMAlerts[i].alertId + "</div>";
                                            }
                                        }
                                    }

                                    //remove temporary html in dom
                                    d.innerHTML = "";

                                    //process IM Alerts
                                    if (xIMAlerts) {
                                        //pass the array of alerts to the IM processing function
                                        dg_IMFromChild(xIMAlerts);
                                        //alert("Error trying to call dg_IMFromChild\(xIMAlerts\)");
                                    }
                                    else {
                                        //there are no alerts, remove any that might still be on screen
                                        dg_dhtmlNotifier_removeOldIMAlerts();
                                        //alert("Error trying to call parent.dg_dhtmlNotifier_removeOldIMAlerts\(\)");

                                    }

                                }
                                else {
                                    if (isAjaxVerboseMode) alert('Error: divAj used to support Ajax is not found');
                                }

                            }
                            else {
                                if (isAjaxVerboseMode) alert('Error returned from server:' + req1.statusText);
                            }
                            req1 = null;
                        }
                    }
                    catch (e) {
                        if (isAjaxVerboseMode) alert(e.description);
                    }
                };
                req1.send(null);
            }
        }
        catch (e) {
        }

        hide('lblCheckingInvitations');
    }
    else {
        loadGetInvitations();
    }
}
function updateMembersOnline() {
    var url = "/Applications/API/UpdateMembersOnline.aspx?random=" + randomNumber();
    if (isAjaxEnabled) {
        executeFireAndForgetUrl(url + "&ajxmode=true", req2);
    }
    else {
        var fmol = null; fmol = window.frm_updateMOL; if (fmol != null) fmol.location.replace(url);
    }
}
function sessionKeepAlive() {
    var url = "/Applications/API/SessionKeepAlive.aspx?random=" + randomNumber();
    if (isAjaxEnabled) {
        executeRemoteJavascript(url + "&ajxmode=true", req3);
    }
    else {
        var fmol = null; fmol = window.frm_sessionKeepAlive; if (fmol != null) fmol.location.replace(url);
    }
}

function removeInvitation(conversationKey, isRejection) {
    var url = "/Applications/API/RemoveInvitation.aspx?&ConversationInvitationKey=" + conversationKey + "&IMReject=" + isRejection + "&random=" + randomNumber();
    if (isAjaxEnabled) {
        //passes null xhr object so it will create a new one for every click on yes or no. Uses fire and forget because we don't do anything with the response.
        executeFireAndForgetUrl(url + "&ajxmode=true", null);
    }
    else {
        var r = null; r = window.frm_removeim; if (r != null) r.location.replace(url);
    }
}


//CallBack Methods - you won't find calls to these here... They come from the output of ASPX pages
//called by result of checkForInvitations - loads GetInvitations page in IFRAME named frm_checkim
function loadGetInvitations() { var cim = null; cim = window.frm_checkim; if (cim != null) cim.location.replace("/Applications/InstantMessenger/CheckIM.aspx?random=" + randomNumber()); hide('lblCheckingInvitations') }
function noInvitationsFound() { hide('lblCheckingInvitations'); dg_dhtmlNotifier_removeOldIMAlerts(); }
//callback from checkForInvitations.aspx or sessionKeepAlive.aspx- if user logs out of another window, checking for IMs will fail. We want to show user they are logged out and if we're on a page where online time limit is ignored, force a reload of the current page (such as on chat page) because user would otherwise never know they couldn't receive IM's. Also, if user has logged out of second window, clear the sessionKeepAlive interval which would be in vain anyway.
function notLoggedIn() { isOnlineTimeLimitReached = true; show('statusLoggedOut'); hide('statusLoggedIn'); clearInterval(interval3); hide('divIMOffline'); hide('divIMOnline'); show('divMembersOnline'); if (IgnoreOnlineTimeLimit) parent.location.reload(parent.location.href) };

//called when online time limit reached
function OnlineTimeLimitReached() { if (isAjaxVerboseMode) alert('online time limit reached'); isOnlineTimeLimitReached = true; setIMStatusOffline(); }

//UI Methods
function show(elementId) { var elem = null; elem = document.getElementById(elementId); if (elem == null) return; elem.style.display = "" }; //shows a given element if it can find it
function hide(elementId) { var elem = null; elem = document.getElementById(elementId); if (elem == null) return; elem.style.display = "none" }; //hides a given element if it can find it
function toggle(elementId) { var elem = null; elem = document.getElementById(elementId); if (elem == null) return; elem.style.display = elem.style.display == "none" ? "" : "none"; } //This is the quintessential toggle display of given element
function randomNumber() { return Math.floor(Math.random() * 100000000000); } //for defeating caching

//show UI indication of being online. Not in use as of 16766 however.
function setIMStatusOnline() { show('divIMOnline'); hide('divIMOffline'); show('divMembersOnline'); }
//show UI indication of being disconnected (but still logged in)
function setIMStatusOffline() { show('divIMOffline'); hide('divIMOnline'); hide('divMembersOnline'); }

//Returns true/false. Currently, determine if check invitations process should happen by checking if IFRAME exists.
//TODO: in the future, will have to have another type of check when IFRAME goes away.
function pageIncludesIMNotifier() {
    cim = null;
    //cim = window.frm_checkim; // changed 'iframe' to 'div' in ActivitySupport.ascx
    cim = document.getElementById('frm_checkim');
    return (cim != null); 
}

//shows how many seconds left until next check for invitations. Not in use as of 16766 however.
function showCountDownToNextCheckForInvitations(i) {
    if (i == 15) { clearTimeout(countdownTimer1) };
    if (i == 0) return;
    var lblCheckingInvitationsCounter = null; lblCheckingInvitationsCounter = document.getElementById('lblCheckingInvitationsCounter'); if (lblCheckingInvitationsCounter == null) return;
    lblCheckingInvitationsCounter.innerHTML = i; //next check in i seconds
    i = i - 1; countdownTimer1 = setTimeout("showCountDownToNextCheckForInvitations(" + i + ")", 1000);
}

/****************************************************************
XMLHttpRequest Utility Functions
*****************************************************************/

//returns xmlhttp object if it can, if not an xmlhttprequest object, or null
//function getXHR(){var C=null;try{C=new ActiveXObject("Msxml2.XMLHTTP")}catch(e){try{C=new ActiveXObject("Microsoft.XMLHTTP")}catch(sc){C=null}}if(!C&&typeof XMLHttpRequest!="undefined"){C=new XMLHttpRequest()}return C}
function getXHR() {
    var client = null;
    if (window.XMLHttpRequest) {
        // code for all modern browsers (including IE 7)
        client = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        // code for IE5 and IE6
        var ua = navigator.userAgent.toLowerCase();
        if (ua.indexOf('msie 5') == -1)
            client = new ActiveXObject("Msxml2.XMLHTTP");
        else
            client = new ActiveXObject("Microsoft.XMLHTTP");
    }

    return client;

}

//calls remote page, executes the javascript returned (using eval). url is the page requested (with or without query string) and xhr is the XMLHttpRequest object to use.
function executeRemoteJavascript(url, xhr) {
    try {
        if (xhr && xhr.readyState != 0) {
            xhr.abort();
        }
        xhr = getXHR();
        if (xhr) {
            xhr.open("GET", url, true);
            xhr.onreadystatechange = function() {
                try {
                    var done = 4, ok = 200;
                    if (xhr.readyState == done) {
                        if (xhr.status == ok && xhr.responseText) {
                            if (xhr.responseText.charAt(0) == "<") {
                                if (isAjaxVerboseMode) alert('Error returned from server:' + xhr.responseText);
                            }
                            else {
                                eval(xhr.responseText)
                            }
                        }
                        else {
                            if (isAjaxVerboseMode) alert('Error returned from server:' + xhr.statusText);
                        }
                        xhr = null;
                    }
                }
                catch (e) {
                    if (isAjaxVerboseMode) alert(e.description);
                }
            };
            xhr.send(null);
        }
    }
    catch (e) {
    }
}

//calls remote page url with given xhr object and ignores response. If in AjaxVerboseMode (aka debug mode) calls executeRemoteJavascript instead.
function executeFireAndForgetUrl(url, xhr) {
    try {
        if (isAjaxVerboseMode) {
            executeRemoteJavascript(url, xhr)
        }
        else {
            if (xhr && xhr.readyState != 0) {
                xhr.abort();
            }
            xhr = getXHR();
            if (xhr) {
                xhr.open("GET", url, true);
                xhr.onreadystatechange = function() {
                    var done = 4;
                    if (xhr.readyState == done) {
                        xhr = null;
                    }
                };
                xhr.send(null);
            }
        }
    }
    catch (e) {
    }
}



// TT#19377
function activatePageObjects() {
    try {
        theObjects = document.getElementsByTagName("object");
        // Fix for CBS Ad Issue 100308
        splashElement = document.getElementById("splashBottomContainer");

        if (splashElement == null) {
            for (var i = 0; i < theObjects.length; i++) {
                //MM: hardcoded check for the Ooyala video player which stops working if this 
                //code is executed against it         
                if (theObjects[i].classid != "clsid:d27cdb6e-ae6d-11cf-96b8-444553540000") {
                    theObjects[i].outerHTML = theObjects[i].outerHTML;
                }
            }
        }

    }
    catch (err) {
    }
}


// FOR MEMBER SUSPENSION THROUGH FREE TEXT APPROVAL 
function validateSuspendReasons(divSuspendID, cblSuspendID, btnSuspendID, btnSuspendName) {
    var suspensionReasonSelected = false;
    var el = document.getElementById(divSuspendID);

    // Show the checkbox list of suspension reasons	            
    if (el.className == 'suspendReasonsHide') {
        el.className = 'suspendReasonsShow';
    }
    else {
        var group = document.getElementById(cblSuspendID);
        var col = group.getElementsByTagName('INPUT');
        var i = 0;

        for (i = 0; i < col.length; i++) {
            if (col.item(i).checked) {
                suspensionReasonSelected = true;
            }
        }
    }

    if (!suspensionReasonSelected) {
        alert('Please select at least one suspension reason.');
    }
    else {
        var btnSuspend = document.getElementById(sb);
        btnSuspend.value = 'Please wait...';
        btnSuspend.disabled = true;
        __doPostBack(btnSuspendName, '');
    }

    return suspensionReasonSelected;
}

function textboxMultilineMaxNumber(txt, maxLen) {
    try {
        if (txt.value.length > (maxLen - 1)) {
            alert('Maximum characters allowed is ' + maxLen.toString());
            return false;
        }
    }
    catch (e) {

    }
}

// Keep user from entering more than maxLength characters
function doKeypress(control) {

    maxLength = control.attributes["maxLength"].value;
    value = control.value;
    if (maxLength && value.length > maxLength - 1) {
        alert('too long');
        event.returnValue = false;
        maxLength = parseInt(maxLength);
    }
}
// Cancel default behavior
function doBeforePaste(control) {

    maxLength = control.attributes["maxLength"].value;
    if (maxLength) {
        event.returnValue = false;
    }
}
// Cancel default behavior and create a new paste routine
function doPaste(control) {

    maxLength = control.attributes["maxLength"].value;
    value = control.value;
    if (maxLength) {
        event.returnValue = false;
        maxLength = parseInt(maxLength);
        var oTR = control.document.selection.createRange();
        var iInsertLength = maxLength - value.length + oTR.text.length;
        var sData = window.clipboardData.getData("Text").substr(0, iInsertLength);
        oTR.text = sData;
    }
}

// For popup help divs
function ShowPopupDiv(id) {
    if (id != null && id != '' && document.getElementById(id) != null)
        document.getElementById(id).style.display = 'block';
}

function HidePopupDiv(id) {
    if (id != null && id != '' && document.getElementById(id) != null)
        document.getElementById(id).style.display = 'none';
}

/***************************************************
Profile Redesign - Tabbed profiles helper functions
****************************************************/
var ProfileTabEnum = new function() {
    //maps to the C# ProfileTabEnum
    this.Basic = 'Basic';
    this.Lifestyle = 'Lifestyle';
    this.Relationship = 'Relationship';
    this.Interests = 'Interests';
    this.Photos = 'Photo';
    this.ColorCode = 'ColorCode';
    this.Questions = 'Questions';
}

//object to hold relevant control IDs on client
//C# controls will dynamically set the IDs
var tabProfileObject = new function() {
    //current tab, possible values are in the ProfileTabEnum object
    this.currentTab = '';
    this.currentTabNumber = 1;

    //tab navs
    this.currentNavID = '';
    this.basicNavID = '';
    this.lifestyleNavID = '';
    this.interestsNavID = '';
    this.relationshipNavID = '';
    this.photosNavID = '';
    this.colorCodeNavID = '';
    this.questionsNavID = '';
    this.nextTabLinkID = '';
    this.previousTabLinkID = '';

    //tab content panels
    this.currentContentID = '';
    this.basicContentID = '';
    this.lifestyleContentID = '';
    this.interestsContentID = '';
    this.relationshipContentID = '';
    this.photosContentID = '';
    this.colorCodeContentID = '';
    this.questionsContentID = '';

    //adunit id
    this.adunitIFrameID = '';
    this.adunitPanelID = '';
    this.isGAM = false;

    //adunit src
    this.adunitSrcBasic = '';
    this.adunitSrcLifestyle = '';
    this.adunitSrcInterests = '';
    this.adunitSrcRelationship = '';
    this.adunitSrcPhotos = '';
    this.adunitSrcColorCode = '';
    this.adunitSrcQuestions = '';

    this.adunitSrcBasicRight = '';
    this.adunitSrcLifestyleRight = '';
    this.adunitSrcInterestsRight = '';
    this.adunitSrcRelationshipRight = '';
    this.adunitSrcPhotosRight = '';
    this.adunitSrcColorCodeRight = '';
    this.adunitSrcQuestionsRight = '';

    //large photo id
    this.largePhotoID = '';
    this.largeNoPhotoID = '';

    //large photo url
    this.basicPhotoUrl = '';
    this.lifestylePhotoUrl = '';
    this.interestsPhotoUrl = '';
    this.relationshipPhotoUrl = '';
    this.photosPhotoUrl = '';
    this.colorCodePhotoUrl = '';
    this.questionsPhotoUrl = '';

    //large photo caption
    this.largeCaptionID = '';
    this.basicPhotoCaption = '';
    this.lifestylePhotoCaption = '';
    this.interestsPhotoCaption = '';
    this.relationshipPhotoCaption = '';
    this.colorCodePhotoCaption = '';
    this.questionsPhotoCaption = '';

    //thumbnail ids
    this.currentThumbnailID = '';
    this.basicThumbnailID = '';
    this.lifestyleThumbnailID = '';
    this.interestsThumbnailID = '';
    this.relationshipThumbnailID = '';
    this.photosThumbnailID = '';
    this.colorCodeThumbnailID = '';
    this.questionsThumbnailID = '';

    //YMN
    this.YYImageID = '';

    //misc
    this.ViewingOwnProfile = false;
    this.OmnitureNavValue = '';
    this.HotlistActionCallPageDetailID = '';
    this.ProfileNavPageName = 'Profile'; //name of page used for client side tabbed navigation; omniture
    this.IsColorCodeEnabled = false;
    this.IsQuestionAnswerEnabled = false;

}

//Goes to next tab
function gotoNextTab() {
    gotoTab('next');
}

//Goes to previous tab
function gotoPreviousTab() {
    gotoTab('prev');
}

//Goes to appropriate tab
function gotoTab(nextPrev) {
    if (nextPrev != null) {
        if (tabProfileObject.currentTab == ProfileTabEnum.Basic) {
            if (nextPrev == 'next')
            { TabClick(ProfileTabEnum.Lifestyle, tabProfileObject.ProfileNavPageName + " - LifestyleBottomLink"); }
            else
            { TabClick(ProfileTabEnum.Photos, tabProfileObject.ProfileNavPageName + " - PhotosBottomLink"); }
        }
        else if (tabProfileObject.currentTab == ProfileTabEnum.Lifestyle) {
            if (nextPrev == 'next')
            { TabClick(ProfileTabEnum.Interests, tabProfileObject.ProfileNavPageName + " - InterestsBottomLink"); }
            else
            { TabClick(ProfileTabEnum.Basic, tabProfileObject.ProfileNavPageName + " - BasicBottomLink"); }
        }
        else if (tabProfileObject.currentTab == ProfileTabEnum.Relationship) {
            if (nextPrev == 'next') {
                if (tabProfileObject.IsColorCodeEnabled == true)
                { TabClick(ProfileTabEnum.ColorCode, tabProfileObject.ProfileNavPageName + " - ColorCodeBottomLink"); }
                else
                { TabClick(ProfileTabEnum.Photos, tabProfileObject.ProfileNavPageName + " - PhotosBottomLink"); }
            }
            else
            { TabClick(ProfileTabEnum.Interests, tabProfileObject.ProfileNavPageName + " - InterestsBottomLink"); }
        }
        else if (tabProfileObject.currentTab == ProfileTabEnum.Interests) {
            if (nextPrev == 'next')
            { TabClick(ProfileTabEnum.Relationship, tabProfileObject.ProfileNavPageName + " - RelationshipBottomLink"); }
            else
            { TabClick(ProfileTabEnum.Lifestyle, tabProfileObject.ProfileNavPageName + " - LifestyleBottomLink"); }
        }
        else if (tabProfileObject.currentTab == ProfileTabEnum.Photos) {
            if (nextPrev == 'next')
            { TabClick(ProfileTabEnum.Basic, tabProfileObject.ProfileNavPageName + " - BasicBottomLink"); }
            else if (tabProfileObject.IsQuestionAnswerEnabled == true)
            { TabClick(ProfileTabEnum.Questions, tabProfileObject.ProfileNavPageName + " - QuestionsBottomLink"); }
            else if (tabProfileObject.IsColorCodeEnabled == true)
            { TabClick(ProfileTabEnum.ColorCode, tabProfileObject.ProfileNavPageName + " - ColorCodeBottomLink"); }
            else
            { TabClick(ProfileTabEnum.Relationship, tabProfileObject.ProfileNavPageName + " - RelationshipBottomLink"); }
        }
        else if (tabProfileObject.currentTab == ProfileTabEnum.ColorCode) {
            if (nextPrev == 'next' && tabProfileObject.IsQuestionAnswerEnabled == true)
            { TabClick(ProfileTabEnum.Questions, tabProfileObject.ProfileNavPageName + " - QuestionsBottomLink"); }
            else if (nextPrev == 'next')
            { TabClick(ProfileTabEnum.Photos, tabProfileObject.ProfileNavPageName + " - PhotosBottomLink"); }
            else
            { TabClick(ProfileTabEnum.Relationship, tabProfileObject.ProfileNavPageName + " - RelationshipBottomLink"); }
        }
        else if (tabProfileObject.currentTab == ProfileTabEnum.Questions) {
            if (nextPrev == 'next')
            { TabClick(ProfileTabEnum.Photos, tabProfileObject.ProfileNavPageName + " - PhotosBottomLink"); }
            else if (tabProfileObject.IsColorCodeEnabled == true)
            { TabClick(ProfileTabEnum.ColorCode, tabProfileObject.ProfileNavPageName + " - ColorCodeBottomLink"); }
            else
            { TabClick(ProfileTabEnum.Relationship, tabProfileObject.ProfileNavPageName + " - RelationshipBottomLink"); }
        }
    }
}

function SwapTabImage(url) {
    if (url != '' && tabProfileObject.largePhotoID != '') {
        var imgObj = document.getElementById(tabProfileObject.largePhotoID);
        var swapped = false;
        jQuery(imgObj).attr({ src: url }).load(function() {
            profile_resizePhoto(imgObj);
            swapped = true;
        });
        if (!swapped)
        { profile_resizePhoto(imgObj); }
        HidePopupDiv(tabProfileObject.largeNoPhotoID);
        ShowPopupDiv(tabProfileObject.largePhotoID);
        jQuery('#memberPhotoLarge').show();
    }
    else {
        ShowPopupDiv(tabProfileObject.largeNoPhotoID);

        jQuery('#memberPhotoLarge').hide();



    }
}

//primary function to swap tabs
function TabClick(newTab, omnitureNavValue) {//debugger;
    try {

        var txtLifestyle = 'Lifestyle';
        var txtPhotos = 'Photos';
        var txtInterests = 'Interests';
        var txtBasics = 'Basics';
        var txtColorCode = 'Color Code';
        var txtRelationship = 'Relationship';
        var txtQuestions = 'Questions';

        if (document.body.dir == 'rtl') {
            txtLifestyle = 'סגנון חיים';
            txtPhotos = 'תמונות';
            txtInterests = 'תחומי עניין';
            txtBasics = 'כללי';
            txtRelationship = 'יחסים וזוגיות';
        }

        if (newTab != null && newTab != tabProfileObject.currentTab) {
            //set new tabID, contentID, and swap image
            var newNavID = '';
            var newContentID = '';
            var newThumbnailID = '';
            var newAdUnitSrc = ''; //top ad
            var newAdUnitSrcRight = ''; //right ad
            var nextTabLink = document.getElementById(tabProfileObject.nextTabLinkID);
            var prevTabLink = document.getElementById(tabProfileObject.previousTabLinkID);
            var caption = document.getElementById(tabProfileObject.largeCaptionID);

            var divProfileContent = document.getElementById("profile-content-div");
            divProfileContent.className = "profile-content";
            if (newTab == ProfileTabEnum.Basic) {
                newNavID = tabProfileObject.basicNavID;
                newContentID = tabProfileObject.basicContentID;
                newThumbnailID = tabProfileObject.basicThumbnailID;
                newAdUnitSrc = tabProfileObject.adunitSrcBasic;
                newAdUnitSrcRight = tabProfileObject.adunitSrcBasicRight;
                nextTabLink.innerHTML = txtLifestyle;
                prevTabLink.innerHTML = txtPhotos;
                caption.innerHTML = tabProfileObject.basicPhotoCaption;
                SwapTabImage(tabProfileObject.basicPhotoUrl);
            }
            else if (newTab == ProfileTabEnum.Lifestyle) {
                newNavID = tabProfileObject.lifestyleNavID;
                newContentID = tabProfileObject.lifestyleContentID;
                newThumbnailID = tabProfileObject.lifestyleThumbnailID;
                newAdUnitSrc = tabProfileObject.adunitSrcLifestyle;
                newAdUnitSrcRight = tabProfileObject.adunitSrcLifestyleRight;
                nextTabLink.innerHTML = txtInterests;
                prevTabLink.innerHTML = txtBasics;
                caption.innerHTML = tabProfileObject.lifestylePhotoCaption;
                SwapTabImage(tabProfileObject.lifestylePhotoUrl);
            }
            else if (newTab == ProfileTabEnum.Relationship) {
                newNavID = tabProfileObject.relationshipNavID;
                newContentID = tabProfileObject.relationshipContentID;
                newThumbnailID = tabProfileObject.relationshipThumbnailID;
                newAdUnitSrc = tabProfileObject.adunitSrcRelationship;
                newAdUnitSrcRight = tabProfileObject.adunitSrcRelationshipRight;
                if (tabProfileObject.IsColorCodeEnabled == true)
                { nextTabLink.innerHTML = txtColorCode; }
                else if (tabProfileObject.IsQuestionAnswerEnabled == true)
                { nextTabLink.innerHTML = txtQuestions; }
                else
                { nextTabLink.innerHTML = txtPhotos; }
                prevTabLink.innerHTML = txtInterests;
                caption.innerHTML = tabProfileObject.relationshipPhotoCaption;
                //			    if (tabProfileObject.relationshipPhotoCaption == "")

                //			        caption.style.display = "none";
                //			   

                SwapTabImage(tabProfileObject.relationshipPhotoUrl);
            }
            else if (newTab == ProfileTabEnum.Interests) {
                newNavID = tabProfileObject.interestsNavID;
                newContentID = tabProfileObject.interestsContentID;
                newThumbnailID = tabProfileObject.interestsThumbnailID;
                newAdUnitSrc = tabProfileObject.adunitSrcInterests;
                newAdUnitSrcRight = tabProfileObject.adunitSrcInterestsRight;
                nextTabLink.innerHTML = txtRelationship;
                prevTabLink.innerHTML = txtLifestyle;
                caption.innerHTML = tabProfileObject.interestsPhotoCaption;
                SwapTabImage(tabProfileObject.interestsPhotoUrl);
            }
            else if (newTab == ProfileTabEnum.Photos) {
                newNavID = tabProfileObject.photosNavID;
                newContentID = tabProfileObject.photosContentID;
                newThumbnailID = tabProfileObject.photosThumbnailID;
                newAdUnitSrc = tabProfileObject.adunitSrcPhotos;
                newAdUnitSrcRight = tabProfileObject.adunitSrcPhotosRight;
                nextTabLink.innerHTML = txtBasics;
                if (tabProfileObject.IsQuestionAnswerEnabled == true)
                { prevTabLink.innerHTML = txtQuestions; }
                else if (tabProfileObject.IsColorCodeEnabled == true)
                { prevTabLink.innerHTML = txtColorCode; }
                else
                { prevTabLink.innerHTML = txtRelationship; }
                // jQuery('#thumbs-picture', this).hide();

                divProfileContent.className = "profile-content photos";
                SwapTabImage(tabProfileObject.relationshipPhotoUrl);
            }
            else if (newTab == ProfileTabEnum.ColorCode) {
                newNavID = tabProfileObject.colorCodeNavID;
                newContentID = tabProfileObject.colorCodeContentID;
                newThumbnailID = tabProfileObject.colorCodeThumbnailID;
                newAdUnitSrc = tabProfileObject.adunitSrcColorCode;
                newAdUnitSrcRight = tabProfileObject.adunitSrcColorCodeRight;
                if (tabProfileObject.IsQuestionAnswerEnabled == true)
                { nextTabLink.innerHTML = txtQuestions; }
                else
                { nextTabLink.innerHTML = txtPhotos; }
                prevTabLink.innerHTML = txtRelationship;
                caption.innerHTML = tabProfileObject.colorCodePhotoCaption;
                SwapTabImage(tabProfileObject.colorCodePhotoUrl);
            }
            else if (newTab == ProfileTabEnum.Questions) {
                newNavID = tabProfileObject.questionsNavID;
                newContentID = tabProfileObject.questionsContentID;
                newThumbnailID = tabProfileObject.questionsThumbnailID;
                newAdUnitSrc = tabProfileObject.adunitSrcQuestions;
                newAdUnitSrcRight = tabProfileObject.adunitSrcQuestionsRight;
                nextTabLink.innerHTML = txtPhotos;
                if (tabProfileObject.IsColorCodeEnabled == true)
                { prevTabLink.innerHTML = txtColorCode; }
                else
                { prevTabLink.innerHTML = txtRelationship; }
                caption.innerHTML = tabProfileObject.questionsPhotoCaption;
                SwapTabImage(tabProfileObject.questionsPhotoUrl);
            }

            if (newNavID != '' && newContentID != '') {
                tabProfileObject.currentTab = newTab;

                //update current tab
                HidePopupDiv(tabProfileObject.currentContentID);
                changeClassStyle(tabProfileObject.currentNavID, "tab");
                changeClassStyle(tabProfileObject.currentThumbnailID, "profileThumb");

                //update new tab
                ShowPopupDiv(newContentID);
                tabProfileObject.currentContentID = newContentID;


                changeClassStyle(newNavID, "tab selected");
                if (newThumbnailID != null && newThumbnailID != "") {
                    tabProfileObject.currentThumbnailID = newThumbnailID;
                    changeClassStyle(newThumbnailID, "profileThumb selected");
                }
                tabProfileObject.currentNavID = newNavID;
                if (newTab == ProfileTabEnum.Photos) {
                    jQuery('.thumbs-picture').hide();
                    // jQuery('.header').hide();
                }
                else {
                    jQuery('.thumbs-picture').show();
                    // jQuery('.header').show();
                }
                //update new tab omniture var
                if (s != null) {
                    PopulateS(true); //clear existing values in omniture "s" object

                    //MPR-638, we do not want to add specific analytics on client side when member is viewing own profile
                    if (tabProfileObject.ViewingOwnProfile == false) {
                        s.events = (newTab == ProfileTabEnum.Basic) ? "event12" : "event4";
                        s.eVar9 = newTab;

                        if (omnitureNavValue != null && omnitureNavValue != '')
                        { s.prop14 = omnitureNavValue; }

                    }

                    // tab click should change the page name now MPR-299
                    s.pageName = "View Profile - " + newTab + " Tab " + getTabNumber(newTab);

                    s.t(); //send omniture updated values as page load
                    s.events = '';
                }

                //postback omniture for hotlist
                if (tabProfileObject.HotlistActionCallPageDetailID != '') {
                    var hidCallActionPageDetail = document.getElementById(tabProfileObject.HotlistActionCallPageDetailID);
                    if (hidCallActionPageDetail != null) {
                        hidCallActionPageDetail.value = getTabSubPage(false);
                    }
                }


                //refresh adunit
                if (newAdUnitSrc != null && newAdUnitSrc != '') {
                    try {
                        updateTop728by90SR(topAd728x90IFrameID, newAdUnitSrc, tabProfileObject.isGAM);
                    } catch (e) { }
                }

                if (newAdUnitSrcRight != null && newAdUnitSrcRight != '') {
                 try {
                    updateRight300by250SR(rightAd300x250IFrameID, newAdUnitSrcRight, tabProfileObject.isGAM)
                } catch (e) { }
                }

            }
        }
    }
    catch (e) {
        //exception occurred, browser may not support
    }

    return false;
}

function getTabSubPage(requiresUrlEncode) {
    var subpagename = '';

    if (requiresUrlEncode) {
        subpagename = tabProfileObject.currentTab + '+Tab+' + getTabNumber(tabProfileObject.currentTab);
    }
    else {
        subpagename = tabProfileObject.currentTab + ' Tab ' + getTabNumber(tabProfileObject.currentTab);
    }

    return subpagename;
}

function getTabNumber(tabName) {
    var tabNumber;

    if (tabName.toString().toLowerCase() == "basic")
        tabNumber = "1";
    else if (tabName.toString().toLowerCase() == "lifestyle")
        tabNumber = "2";
    else if (tabName.toString().toLowerCase() == "interests")
        tabNumber = "3";
    else if (tabName.toString().toLowerCase() == "relationship")
        tabNumber = "4";
    else if (tabName.toString().toLowerCase() == "colorcode")
        tabNumber = "5";
    else if (tabName.toString().toLowerCase() == "questions" && tabProfileObject.IsColorCodeEnabled)
        tabNumber = "6";
    else if (tabName.toString().toLowerCase() == "questions")
        tabNumber = "5";
    else if (tabName.toString().toLowerCase() == "photo" && tabProfileObject.IsQuestionAnswerEnabled && tabProfileObject.IsColorCodeEnabled)
        tabNumber = "7";
    else if (tabName.toString().toLowerCase() == "photo" && tabProfileObject.IsQuestionAnswerEnabled && !tabProfileObject.IsColorCodeEnabled)
        tabNumber = "6";
    else if (tabName.toString().toLowerCase() == "photo" && !tabProfileObject.IsQuestionAnswerEnabled && tabProfileObject.IsColorCodeEnabled)
        tabNumber = "6";
    else if (tabName.toString().toLowerCase() == "photo")
        tabNumber = "5";

    return tabNumber;
}

function profile_resizePhoto(image) {
    var newImage = new Image();
    var swapped = false;

    jQuery(newImage).attr({ src: image.src }).load(function() {
        profile_setPhotoSize(image, newImage);
        swapped = true;
    });

    if (!swapped) {
        newImage.src = image.src;
        profile_setPhotoSize(image, newImage);
    }
}

function profile_setPhotoSize(pImage, pNewImage) {
    var horNum = 246;
    var verNum = 328;

    var newHorNum = 0;
    var newVerNum = 0;

    if (pNewImage.width > horNum) {
        newVerNum = pNewImage.height * horNum / pNewImage.width;
        newHorNum = horNum;
    }
    else if (pNewImage.height > verNum) {
        newHorNum = pNewImage.width * verNum / pNewImage.height;
        newVerNum = verNum;
    }
    else {
        newHorNum = pNewImage.width;
        newVerNum = pNewImage.height;
    }

    //ensure max vertical is 328
    if (newVerNum > verNum) {
        newHorNum = newHorNum * verNum / newVerNum;
        newVerNum = verNum;
    }

    pImage.style.width = newHorNum + 'px';
    pImage.style.height = newVerNum + 'px';

    swapMemberPhoto();
}

function profile_resizePhoto30(image) {
    var newImage = new Image();
    var swapped = false;

    jQuery(newImage).attr({ src: image.src }).load(function() {
        profile_setPhotoSize30(image, newImage);
        swapped = true;
    });
    
    if (!swapped) {
        newImage.src = image.src;
        profile_setPhotoSize30(image, newImage);
    }

}

//used in new redesign for new larger profile image
function profile_setPhotoSize30(pImage, pNewImage) {
    var horNum = 280;
    var verNum = 350;

    var newHorNum = 0;
    var newVerNum = 0;
    
    if (pNewImage.width > horNum) {
        newVerNum = pNewImage.height * horNum / pNewImage.width;
        newHorNum = horNum;
    }
    else if (pNewImage.height > verNum) {
        newHorNum = pNewImage.width * verNum / pNewImage.height;
        newVerNum = verNum;
    }
    else {
        newHorNum = pNewImage.width;
        newVerNum = pNewImage.height;
    }

    //ensure max vertical is 350
    if (newVerNum > verNum) {
        newHorNum = newHorNum * verNum / newVerNum;
        newVerNum = verNum;
    }

    pImage.width = newHorNum;
    pImage.height = newVerNum;

    swapMemberPhoto();
}

//Swap images without user seeing resize
function swapMemberPhoto() {
    jQuery("#memberPhotoSwap img").removeAttr("onload");
    var imageSwap = jQuery("#memberPhotoSwap").html();
    jQuery("#memberPhotoLarge").html(imageSwap);
}

function showLargePhoto(url) {

}

//new tabs stuff
(function() {
    __createNamespace__ = function(namespace) {
        var components = namespace.split(/[^a-z0-9]+/ig);
        var context = window;
        for (var i = 0, l = components.length; i < l; i += 1) {
            var component = components[i];
            if (!context[component]) {
                context[component] = {};
            }
            context = context[component];
        }
        return context;
    };
    __addToNamespace__ = function(ns, o) {
        var initFunc = null;
        var myns = __createNamespace__(ns);
        for (var i in o) {
            if (i + "" == "__init__") {
                initFunc = o[i];
            } else {
                myns[i] = o[i];
            }
        }
        if (initFunc) initFunc();
        initFunc = null;
    };

    __addToNamespace__('spark.util', {
        jtemplates: {},
        extend: function(ChildClass, ParentClass) {
            function I() { };
            I.prototype = ParentClass.prototype;
            ChildClass.prototype = new I;
            ChildClass.prototype.constructor = ChildClass;
        },
        addItem: function(arr, item) {
            arr[arr.length] = item;
            return arr;
        },
        deleteItem: function(arr, item) {
            var tmpArr = [];
            for (var idx in arr) {
                if (arr[idx] === item) { continue; }
                spark.util.addItem(tmpArr, arr[idx]);
            }
            arr = null;
            return tmpArr;
        },
        ellipsis: function(str, maxLength) {
            if (str && str.length > maxLength) {
                str = str.substring(0, maxLength - 3) + "...";
            }
            return str;
        },
        alphaNumericSort: function(a, b) {
            // setup temp-scope variables for comparison evauluation
            var re = /(-?[0-9\.]+)/g,
		            x = a.toString().toLowerCase() || '',
		            y = b.toString().toLowerCase() || '',
		            nC = String.fromCharCode(0),
		            xN = x.replace(re, nC + '$1' + nC).split(nC),
		            yN = y.replace(re, nC + '$1' + nC).split(nC),
		            xD = (new Date(x)).getTime(),
		            yD = xD ? (new Date(y)).getTime() : null;
            // natural sorting of dates
            if (yD)
                if (xD < yD) return -1;
            else if (xD > yD) return 1;
            // natural sorting through split numeric strings and default strings
            for (var cLoc = 0, numS = Math.max(xN.length, yN.length); cLoc < numS; cLoc++) {
                oFxNcL = parseFloat(xN[cLoc]) || xN[cLoc];
                oFyNcL = parseFloat(yN[cLoc]) || yN[cLoc];
                if (oFxNcL < oFyNcL) return -1;
                else if (oFxNcL > oFyNcL) return 1;
            }
            return 0;
        },
        mapSize: function(map) {
            var i = 0;
            for (var y in map) { i++; }
            return i;
        },
        mapSort: function(map, sortField, reverse) {
            if (spark.util.mapSize(map) < 2) return map;
            var tmp = [];
            for (var key in map) {
                var sortVal = (sortField && sortField.length > 0) ? map[key][sortField] : map[key];
                tmp.push([sortVal, key]);
            }
            tmp.sort(function(a, b) {
                return spark.util.alphaNumericSort(a[0], b[0]);
            });
            if (reverse) tmp.reverse();
            var sortedMap = {};
            var len = tmp.length;
            for (var i = 0; i < len; i++) {
                var a = tmp[i];
                sortedMap[a[1]] = map[a[1]];
            }
            tmp = null;
            return sortedMap;
        },
        mapPrint: function(map, field) {
            for (var key in map) {
                var val = (field) ? map[key][field] : map[key];
                if (print) {
                    print(key + '->' + val);
                }
            }
        },
        uniescape: function(str) {
            if (!str) return null;
            return encodeURIComponent(spark.util.uni2ent(str));
        },
        ajaxLoader:'<div class="loading spinner-only" />',
        uni2ent: function(srcTxt) {
            var entTxt = '';
            var c, hi, lo;
            var wordStart = false;
            var wordEnd = false;
            var len = 0;
            for (var i = 0, code; code = srcTxt.charCodeAt(i); i++) {
                var rawChar = srcTxt.charAt(i);
                // needs to be an HTML entity
                if (code > 255) {
                    // normally we encounter the High surrogate first
                    if (0xD800 <= code && code <= 0xDBFF) {
                        hi = code;
                        lo = srcTxt.charCodeAt(i + 1);
                        // the next line will bend your mind a bit
                        code = ((hi - 0xD800) * 0x400) + (lo - 0xDC00) + 0x10000;
                        i++; // we already got low surrogate, so don't grab it again
                    }
                    // what happens if we get the low surrogate first?
                    else if (0xDC00 <= code && code <= 0xDFFF) {
                        hi = srcTxt.charCodeAt(i - 1);
                        lo = code;
                        code = ((hi - 0xD800) * 0x400) + (lo - 0xDC00) + 0x10000;
                    }
                    // wrap it up as Hex entity
                    c = "" + code.toString(16).toUpperCase() + ";";
                    if (wordStart) {
                        wordStart = false;
                        wordEnd = true;
                    }
                }
                else {
                    c = rawChar;
                    wordStart = true;
                    wordEnd = false;
                }
                entTxt += ((wordEnd) ? ";" : "") + c;
                len++;
            }
            return entTxt;
        },
        __init__: function() {
            //this function gets called after the namespace is created and
            // all of the objects have been added to the namespace
            //This is a good place to add to the prototype of an object
            //or to extend objects.
        }
    });

    __addToNamespace__('spark.log', {
        logError: function(e) {
            //write to firebug console
            if (window["console"] && window["console"]["log"]) {
                console.log(e);
            }
        }
    });

    __addToNamespace__('spark.observers', {
        Observer: function() { },
        Subject: function() { this.observers = []; },
        __init__: function() {
            spark.observers.Observer.prototype.Update = function(data) { };
            // notify per Observer object to execute the Update method
            spark.observers.Subject.prototype.notify = function(context) {
                for (var i = 0; i < this.observers.length; i++) {
                    this.observers[i].Update(context);
                }
            };
            //add a new Observer
            spark.observers.Subject.prototype.addObserver = function(observer) {
                if (observer instanceof spark.observers.Observer) {
                    spark.util.addItem(this.observers, observer);
                }
            };
            //delete the existing Observer
            spark.observers.Subject.prototype.removeObserver = function(observer) {
                if (observer instanceof spark.observers.Observer) {
                    spark.util.deleteItem(this.observers, observer);
                }
            };
        }
    });

    __addToNamespace__('spark.slideshows', {
        SlideShow: function() {
            this.arr = [];
            this.currentIndex = 0;
        },
        __init__: function() {
            spark.slideshows.SlideShow.prototype.addSlide = function(slide) {
                spark.util.addItem(this.arr, slide);
            };
            spark.slideshows.SlideShow.prototype.getSlide = function() {
                var slide = this.arr[this.currentIndex];
                return slide;
            };
            spark.slideshows.SlideShow.prototype.getSlideAt = function(idx) {
                if (idx >= 0 || idx < this.arr.length) {
                    this.currentIndex = idx;
                }
                return this.getSlide();
            };
            spark.slideshows.SlideShow.prototype.nextSlide = function() {
                this.increment();
                return this.getSlide();
            };
            spark.slideshows.SlideShow.prototype.previousSlide = function() {
                this.decrement();
                return this.getSlide();
            };
            spark.slideshows.SlideShow.prototype.increment = function() {
                this.currentIndex++;
                if (this.currentIndex > this.arr.length - 1) {
                    this.currentIndex = 0;
                }
            };
            spark.slideshows.SlideShow.prototype.decrement = function() {
                this.currentIndex--;
                if (this.currentIndex < 0) {
                    this.currentIndex = this.arr.length - 1;
                }
            };
            spark.slideshows.SlideShow.prototype.size = function() {
                return this.arr.length;
            };
        }
    });

    __addToNamespace__('spark.paging', {
        Page: function(contents, startIdx, endIdx) {
            this._contents = contents;
            this._startIndex = startIdx;
            this._endIndex = endIdx;
        },
        Pages: function(list, pageSize, linkMax, resourceMap, omniMap) {
            this.omnitureMap = (omniMap) ? omniMap : {};
            this.resourceMap = (resourceMap) ? resourceMap : {};
            this.linkDisplayMax = (linkMax) ? linkMax : 3;
            this.pages = new spark.slideshows.SlideShow();
            this.pageSize = pageSize;
            if (list && list.length && list.length > 0) {
                var len = list.length;
                var numOfLists = (len < pageSize) ? 1 : Math.ceil(len / pageSize);
                var lastIdx = 0;
                for (var i = 0; i < numOfLists; i++) {
                    if (lastIdx + pageSize > len) {
                        this.pages.addSlide(new spark.paging.Page(list.slice(lastIdx), lastIdx + 1, len));
                    } else {
                        this.pages.addSlide(new spark.paging.Page(list.slice(lastIdx, lastIdx + pageSize), lastIdx + 1, lastIdx + pageSize));
                    }
                    $j.each(this.pages.arr[i].getContents(), function() {
                        if (0 == i) {
                            $j(this).show();
                        } else {
                            $j(this).hide();
                        }
                    });
                    lastIdx = lastIdx + pageSize;
                }
            }
        },
        __init__: function() {
            spark.paging.Pages.prototype.getPages = function() {
                return this.pages;
            };
            spark.paging.Pages.prototype.getPagingText = function(params) {
                var pagingText = '';
                var base = (params.name) ? params.name : '';
                var elemIds = (params.elemIds) ? params.elemIds : [];
                var txtFirst = (this.resourceMap.first) ? this.resourceMap.first : '&laquo;';
                var txtPrev = (this.resourceMap.prev) ? this.resourceMap.prev : '&lsaquo;';
                var txtNext = (this.resourceMap.next) ? this.resourceMap.next : '&rsaquo;';
                var txtSpacer = (this.resourceMap.spacer) ? this.resourceMap.spacer : '&nbsp;/&nbsp;';
                var linkCss = (this.resourceMap.linkCss) ? this.resourceMap.linkCss : 'class="clickable"';
                var startIdx = (params.idx) ? ((params.idx == 0) ? params.idx : params.idx - 1) : 0;
                if (this.pages.size() > 0) {
                    if (params.idx && params.idx == (this.pages.size() - 1) && params.idx > 1) startIdx--;
                    if (this.pages.currentIndex > 1) { pagingText += '<span ' + linkCss + ' onClick="' + base + '.showPageAt({name:\'' + base + '\',elemIds:[\'' + elemIds.toString().replace(',', '\',\'') + '\'],idx:0});">' + txtFirst + '</span>' + txtSpacer; }
                    if (this.pages.currentIndex > 0) { pagingText += '<span ' + linkCss + ' onClick="' + base + '.showPreviousPage({name:\'' + base + '\',elemIds:[\'' + elemIds.toString().replace(',', '\',\'') + '\']});">' + txtPrev + '</span>' + txtSpacer; }
                    var linkDisplay = 0;
                    for (var i = startIdx; i < this.pages.size() && linkDisplay < this.linkDisplayMax; i++) {
                        var page = this.pages.arr[i];
                        if (i == this.pages.currentIndex) { pagingText += '<strong>'; }
                        pagingText += '<span ' + linkCss + ' onClick="' + base + '.showPageAt({name:\'' + base + '\',elemIds:[\'' + elemIds.toString().replace(',', '\',\'') + '\'],idx:' + i + '});">' + page.getPageText() + '</span>';
                        if (i == this.pages.currentIndex) { pagingText += '</strong>'; }
                        if (i + 1 < this.pages.size() && linkDisplay + 1 < this.linkDisplayMax) { pagingText += txtSpacer; }
                        linkDisplay++;
                    }
                    var atEnd = (this.pages.currentIndex > this.pages.size() - this.linkDisplayMax);
                    if (!atEnd) { pagingText += txtSpacer + '<span ' + linkCss + ' onClick="' + base + '.showNextPage({name:\'' + base + '\',elemIds:[\'' + elemIds.toString().replace(',', '\',\'') + '\']});">' + txtNext + '</span>'; }
                }
                if (params.elemIds) {
                    $j.each(params.elemIds, function() {
                        $j("#" + this).empty().append(pagingText);
                    });
                }
                return pagingText;
            };
            spark.paging.Pages.prototype.showNextPage = function(params) {
                $j(this.pages.getSlide().getContents()).each(function() {
                    $j(this).hide();
                });
                $j(this.pages.nextSlide().getContents()).each(function() {
                    $j(this).show();
                });
                params.idx = this.pages.currentIndex;
                this.getPagingText(params);
                this.track(params.idx);
                this.refreshAds();
            };
            spark.paging.Pages.prototype.showPreviousPage = function(params) {
                $j(this.pages.getSlide().getContents()).each(function() {
                    $j(this).hide();
                });
                $j(this.pages.previousSlide().getContents()).each(function() {
                    $j(this).show();
                });
                params.idx = this.pages.currentIndex;
                this.getPagingText(params);
                this.track(params.idx);
                this.refreshAds();
            };
            spark.paging.Pages.prototype.showPageAt = function(params) {
                if (params.idx == this.pages.currentIndex) return;
                $j(this.pages.getSlide().getContents()).each(function() {
                    $j(this).hide();
                });
                $j(this.pages.getSlideAt(params.idx).getContents()).each(function() {
                    $j(this).show();
                });
                this.getPagingText(params);
                this.track(params.idx);
                this.refreshAds();
            };
            spark.paging.Pages.prototype.refreshAds = function() {
                refreshTop728by90SR();
                refreshRight300by250SR();
            };
            spark.paging.Pages.prototype.track = function(idx) {
                if (this.omnitureMap.location) {
                    spark.tracking.addKeyAndValue('eVar25', (idx + 1) + '-' + this.omnitureMap.location, true);
                    spark.tracking.track();
                }
            };
            spark.paging.Page.prototype.getContents = function() {
                return this._contents;
            };
            spark.paging.Page.prototype.getPageText = function() {
                return this._startIndex + "-" + this._endIndex;
            };
        }
    });



	__addToNamespace__('spark.chat', {
		// let the other user know the instant message invitation was declined
		sendEjabIMRejectedMessage: function(recipientMemberId, conversationTokenId) {
			var siteLookup = {
				"blacksingles" : 24,
				"jdate" : 3,
				"spark" : 1,
				"bbwpersonalsplus" : 23,
				"cupid" : 10
			};

            var loginDataMemberId = (null == jQuery.cookie("sua_mid") || '' == jQuery.cookie("sua_mid")) ? jQuery.cookie("MOS_MEMBER") : jQuery.cookie("sua_mid");

            var loginData = {
                "memberId": loginDataMemberId,
                "accessToken": spark_getCookie("MOS_ACCESS")
            };

			var communityId;
			var domain = window.location.host.toLowerCase();
			for (var siteName in siteLookup) {
			    if (domain.indexOf(siteName) !== -1) {
			        communityId = siteLookup[siteName];
			        break;
			    }
			}
			if (!communityId) {
			    return; // unknown site
			}

			var chatServerName = "chat.spark-networks.com";
			var boshServer;

			if (domain.indexOf("stgv3") !== -1) {
			    chatServerName = "chat.stgv3.spark.net";
			}

			if (domain.indexOf("temp") !== -1) {
			    chatServerName = "chat-temp.stgv3.spark.net";
			}

			boshServer = "//" + chatServerName + "/http-bind";
		    var ownJid;
            if (!conversationTokenId) {
                ownJid = loginData.memberId + '-' + communityId + '@' + chatServerName;    
            } else {
                ownJid = recipientMemberId + '#' + loginData.memberId + '#' + conversationTokenId + '-' + communityId + '@' + chatServerName;
            }
			var recipientJid = recipientMemberId + '-' + communityId + '@' + chatServerName;
			spark.log.logError('ownJid:' + ownJid + ' recipientJid:' + recipientJid + ' boshServer:' + boshServer);				
            var conn = new Strophe.Connection(boshServer);
            conn.connect(ownJid, loginData.accessToken, function (status) {
                if (status === Strophe.Status.CONNECTED) {
                	if (conn) {
						conn.send($pres());
                		conn.send($pres({
                			to: recipientJid,
                			"type": "declined"
                		}));
                	}
                }
                else if (status === Strophe.Status.CONNFAIL) {
                    spark.log.logError('Connection to chat server at ' + boshServer + ' failed.');
                }
                else if (status === Strophe.Status.AUTHFAIL) {
                	spark.log.logError('authorization with chat server failed');
                }
            });
		}
	});

} ());

// Show Hide Function for Secondary Profile Controls
function timedHideControls() {
    setTimeout("showMoreControls('secondaryControls','more_close','more_open');", 700);
    /*setTimeout("showLessControls('more_open','more_close','secondaryControls');", 700);*/
}


function showMoreControls(sDivId, sDivId2, hDivId) {
    var sDiv = document.getElementById(sDivId);
    var sDiv2 = document.getElementById(sDivId2);
    var hDiv = document.getElementById(hDivId);

    if (sDiv != null) sDiv.style.display = 'block';
    if (sDiv2 != null) sDiv2.style.display = 'block';
    if (hDiv != null) hDiv.style.display = 'none';
}

function showLessControls(sDivId, hDivId, hDivId2) {
    var sDiv = document.getElementById(sDivId);
    var hDiv = document.getElementById(hDivId);
    var hDiv2 = document.getElementById(hDivId2);

    if (sDiv != null) sDiv.style.display = 'block';
    if (hDiv != null) hDiv.style.display = 'none';
    if (hDiv2 != null) hDiv2.style.display = 'none';
}
// End Function

// Detects scrolling and adjusts photo as needed
function scrollingDetector() {
    try {
        var browser = navigator.appName;

        var adHeight = 0;
        if (tabProfileObject.adunitPanelID != '' && document.getElementById(tabProfileObject.adunitPanelID) != null) {
            adHeight = document.getElementById(tabProfileObject.adunitPanelID).offsetHeight;
        }

        var mastheadHeight = document.getElementById('profileMasthead').offsetHeight;
        var photoBox = document.getElementById('profileControls');

        //alert(photoBox.id);
        if ((browser == "Microsoft Internet Explorer")) {
            if (window.XMLHttpRequest) //Detects for IE7
            {
                if (document.documentElement.scrollTop >= adHeight + mastheadHeight) {
                    photoBox.className = 'pinned';
                }
                else {
                    photoBox.className = '';
                }
            }
            else //IF IE6, doesn't adjust position
            {
            }

        }
        else {
            if (window.pageYOffset >= adHeight + mastheadHeight) {
                photoBox.className = 'pinned';
            }
            else {
                photoBox.className = '';
            }

        }
    }
    catch (e) {
        //exception occurred, browser may not support
    }
}
// End photo pinning function
//********************************//

// Forced page adunit
function fullWindowDiv() {
    var pageHeight = 800;
    var pageWidth = 100;

    if (window.innerHeight && window.scrollMaxY) {
        pageHeight = window.innerHeight + window.scrollMaxY;
    }
    else if (document.body.scrollHeight > document.body.offsetHeight) // all but Explorer Mac
    {
        pageHeight = document.body.scrollHeight;
    }
    else {
        pageHeight = document.body.offsetHeight + document.body.offsetTop;
    }

    var full_div = document.getElementById('blockUI');
    if (full_div != null) {
        full_div.style.height = pageHeight + "px";
        full_div.style.visibility = "visible";
    }

    var content_div = document.getElementById('blockUI_adunit');
    if (content_div != null)
        content_div.style.display = "block";
}

function TogglePopupDiv(id) {
    if (!document.getElementById(id)) {
        //return alert('The layer you are trying to show, ' + id + ', does not exist');
        return;
    }
    else {
        toggleId = document.getElementById(id);
        toggleId.style.visibility = ((toggleId.style.visibility == "visible") ? "hidden" : "visible");
    }
}

function TogglePopupDivDisplay(id) {

    if (!document.getElementById(id)) {
        //return alert('The layer you are trying to show, ' + id + ', does not exist');
        return;
    }
    else {
        toggleId = document.getElementById(id);
        toggleId.style.display = ((toggleId.style.display == "") ? "none" : "");
    }
}

// Promo Div
function setPromotionAction(action) {
    var exListDiv = document.getElementById("exListDiv");

    if (action == "show") {
        exListDiv.style.visibility = "visible";
    }
    else {
        exListDiv.style.display = "none";
    }
}
// End Promo Div

function OpenPopupDivDisplay(id) {
    if (!document.getElementById(id)) {
        //return alert('The layer you are trying to show, ' + id + ', does not exist');
        return;
    }
    else {
        toggleId = document.getElementById(id);

        //alert(toggleId.getAttribute("closed"));		

        if (toggleId.getAttribute("closed") != "true") {
            toggleId.style.display = "";
        }
    }
}

function ClosePopupDivDisplay(id) {
    if (!document.getElementById(id)) {
        return;
    }
    else {
        toggleId = document.getElementById(id);
        toggleId.style.display = "none";
        toggleId.setAttribute("closed", "true");
    }
}

function ExitPopupDivDisplayArea(id) {
    if (!document.getElementById(id)) {
        return;
    }
    else {
        toggleId = document.getElementById(id);
        toggleId.setAttribute("closed", "false");
    }
}

function containsDOM(container, containee) {
    var isParent = false;
    do {
        if ((isParent = container == containee))
            break;
        containee = containee.parentNode;
    }
    while (containee != null);
    return isParent;
}

function checkMouseEnter(element, evt) {
    if (element.contains && evt.fromElement) {
        return !element.contains(evt.fromElement);
    }
    else if (evt.relatedTarget) {
        return !containsDOM(element, evt.relatedTarget);
    }
}

function checkMouseLeave(element, evt) {
    if (element.contains && evt.toElement) {
        return !element.contains(evt.toElement);
    }
    else if (evt.relatedTarget) {
        return !containsDOM(element, evt.relatedTarget);
    }
}

$j(document).ready(function() {fullWindowDiv()});

/*******START ADMIN ADJUST TAB FEATURE*/////
var AdminAdjustTabEnum = new function() {
    //maps to the C# AdminAdjustTabEnum
    this.adjust = 'Adjust';
    this.buycredit = 'BuyCredit';
    this.buycheck = 'BuyCheck';
    this.renewal = 'Renewal';
    this.buyadminonlycredit = 'AdminOnlyCredit';
}

//object to hold relevant control IDs on client
//C# controls will dynamically set the IDs
var AdminAdjustObject = new function() {
    this.currentTab = '';
    this.currentTabID = '';
    this.currentTabContentID = '';
    this.tabTrackerID = '';

    //tab IDs
    this.adjustTabID = '';
    this.buycreditTabID = '';
    this.buycheckTabID = '';
    this.renewalTabID = '';
    this.buyadminonlycreditTabID = '';

    //tab content IDs
    this.adjustTabContentID = '';
    this.buycreditTabContentID = '';
    this.buycheckTabContentID = '';
    this.renewalTabContentID = '';
    this.buyadminonlycreditTabContentID = '';
}

//primary function to swap tabs
function AdminAdjustTabClick(newTab) {
    try {


        var newContentID = '';
        var newTabID = '';
        if (newTab == AdminAdjustTabEnum.adjust) {
            newContentID = AdminAdjustObject.adjustTabContentID;
            newTabID = AdminAdjustObject.adjustTabID;
        }
        else if (newTab == AdminAdjustTabEnum.buycheck) {
            newContentID = AdminAdjustObject.buycheckTabContentID;
            newTabID = AdminAdjustObject.buycheckTabID;
        }
        else if (newTab == AdminAdjustTabEnum.buycredit) {
            newContentID = AdminAdjustObject.buycreditTabContentID;
            newTabID = AdminAdjustObject.buycreditTabID;
        }
        else if (newTab == AdminAdjustTabEnum.renewal) {
            newContentID = AdminAdjustObject.renewalTabContentID;
            newTabID = AdminAdjustObject.renewalTabID;
        }
        else if (newTab == AdminAdjustTabEnum.buyadminonlycredit) {
            newContentID = AdminAdjustObject.buyadminonlycreditTabContentID;
            newTabID = AdminAdjustObject.buyadminonlycreditTabID;
        }

        if (newContentID != '' && newTabID != '') {
            //update current tab
            HidePopupDiv(AdminAdjustObject.currentTabContentID);
            changeClassStyle(AdminAdjustObject.currentTabID, "");

            //update new tab
            ShowPopupDiv(newContentID);
            changeClassStyle(newTabID, "selected");

            AdminAdjustObject.currentTab = newTab;
            AdminAdjustObject.currentTabID = newTabID;
            AdminAdjustObject.currentTabContentID = newContentID;

            varTabTracker = document.getElementById(AdminAdjustObject.tabTrackerID);
            if (varTabTracker != null)
                varTabTracker.value = newTab;
        }

    }
    catch (e) {
        //exception occurred, browser may not support
    }

    return false;
}

/******END ADMIN ADJUST TAB FEATURE*/////

function getElementsByClassName(clsName, htmltag) {
    var arr = new Array();
    var elems = document.getElementsByTagName(htmltag);
    for (var cls, i = 0; (elem = elems[i]); i++) {
        if (elem.className == clsName) {
            arr[arr.length] = elem;
        }
    }

    return arr;
}


//**SITE REDESIGN AD UNIT************************************************/

//topAd728x90IFrameID - this variable value is rendered from TopAuxNav control for GAM
//rightAd300x250IFrameID - this variable value is rendered from Right control for GAM
//rightAdProfilePopup - this variable value is rendered from Right control or WidePopup template to support ad in popup

function updateTop728by90SR(topIframeID, newAdUnitSrc, isGAM) {
    updateAdFrameSR(topIframeID, newAdUnitSrc, isGAM);
}

function updateBelowPhoto234by60SR(belowPhotoIframeID, newAdUnitSrc, isGAM) {
    updateAdFrameSR(belowPhotoIframeID, newAdUnitSrc, isGAM);
}

function updateBelowCompose234by60SR(belowComposeIframeID, newAdUnitSrc, isGAM) {
    updateAdFrameSR(belowComposeIframeID, newAdUnitSrc, isGAM);
}

function updateRight300by250SR(rightIframeID, newAdUnitSrc, isGAM) {//debugger;
    if (rightAdProfilePopup == "true" &&
        (newAdUnitSrc.indexOf("profile_basics") < 0 && newAdUnitSrc.indexOf("profile_lifestyle") < 0
            && newAdUnitSrc.indexOf("profile_myownwords") < 0 && newAdUnitSrc.indexOf("profile_moreaboutme") < 0
            && newAdUnitSrc.indexOf("profile_thedetails") < 0 && newAdUnitSrc.indexOf("profile_photos") < 0)
    ) {
        //hide right ad on all tabs when profile is in pop-up window except the basics and lifestyle tab
        HidePopupDiv(rightIframeID);
    }
    else {
        ShowPopupDiv(rightIframeID);
        updateAdFrameSR(rightIframeID, newAdUnitSrc, isGAM);
    }
}

function updateAdFrameSR(IframeID, newAdUnitSrc, isGAM) {
    try {
        var iframeObj = eval('window.' + IframeID); //document.getElementById(IframeID);
        if (iframeObj != null) {
            //add random number to querystring of iframe's src url to cause it to refresh
            var iframeRandom = '&ptrdm=' + Math.floor(Math.random() * 1000); //this will generate number from 0 to 999
            if (isGAM == false) {
                iframeObj.location.replace(newAdUnitSrc + iframeRandom);
            }
            else {
                // for GAM implementation where rendernig adslots with iframe is required.
                if (newAdUnitSrc != null && newAdUnitSrc != '')
                    iframeObj.location.replace("/framework/ui/advertising/gamiframe.aspx?GAMAdSlot=" + newAdUnitSrc + iframeRandom);
                else
                    iframeObj.location.replace(iframeObj.location);
            }
        }
    }
    catch (e) {
    }
}

function refreshTop728by90SR() {
    try {
        updateAdFrameSR(topAd728x90IFrameID, '', true);
    }
    catch (e) {
    }
}

function refreshRight300by250SR() {
    try {
        updateAdFrameSR(rightAd300x250IFrameID, '', true);
    }
    catch (e) {
    }
}

//***CUSTOM OMNITURE************************************************/

//Submits click tracking
//e.g. CustomLinktracking(this, 'Email', 'View Profile', 'Basic Tab 1');
function CustomLinkTracking(control, action, pagename, subpagename) {

    var linkName = '';
    if (subpagename != null && subpagename != '') {
        linkName = action + ' (' + pagename + ' - ' + subpagename + ')';
    }
    else {
        linkName = action + ' (' + pagename + ')';
    }

    //s and s_account are defined and rendered from omniture control
    //var s=s_gi(s_account);
    //if (s != null) {
    //    s.linkTrackVars = 'eVar27';
    //    s.linkTrackEvents = 'None';
    //    s.eVar27 = linkName;
    //    s.tl(control, 'o', linkName);
    //}
}
//***profile refresh*****************************************************/
var clientProfile;
function GetProfile(params, nav) {
    try {
        if (clientProfile && clientProfile.readyState != 0) {
            clientProfile.abort();
        }
        clientProfile = getXHR();
        if (clientProfile) {
            var url = "/Applications/MemberProfile/ProfileTabs20/StandardTabGroupAPI.aspx?" + params;
            clientProfile.open("GET", url, true);
            clientProfile.onreadystatechange = function() {
                try {
                    var done = 4, ok = 200;
                    if (clientProfile.readyState == done) {
                        if (clientProfile.status == ok && clientProfile.responseText) {

                            //setTimeout("UpdateMiniSearchContent();", 5000); //make ajax loader stay up for testing
                            UpdateProfileContent();
                        }
                        else {
                            if (isAjaxVerboseModeMS) alert('Error returned from server:' + clientProfile.statusText);
                            //hide ajax loader
                            //HidePopupDiv('divMiniSearchAjaxLoading');
                            //ShowPopupDiv('photos-min');
                            //document.getElementById('photos-min').style.visibility = 'visible';
                            //document.getElementById('photos-header').style.visibility='visible';
                            clientProfile = null;
                        }
                    }
                }
                catch (e) {
                    if (isAjaxVerboseModeMS) alert(e.description);
                }
            };
            clientProfile.send(null);
        }
    }
    catch (e) {
        if (isAjaxVerboseModeMS) alert(e.description);
    }
}

function UpdateProfileContent() {
    try {
        var d = document.getElementById("divProfileContent");
        if (d != null) {
            //load
            d.innerHTML = clientProfile.responseText;
        }
        else {
            if (isAjaxVerboseModeMS) alert('Error: divProfileContent used to support Ajax is not found');
        }
        //hide ajax loader
        //HidePopupDiv('divMiniSearchAjaxLoading');

        clientProfile = null;
    }
    catch (e) {
        if (isAjaxVerboseModeMS) alert(e.description);
    }
}

//***Mini-SEARCH*****************************************************/
var clientMiniS;
var isAjaxVerboseModeMS = false;
function GetMiniSearch(params, nav, isVis) {
    GetMiniSearch(params, nav, isVis, false);
}
function GetMiniSearch(params, nav, isVis, isMiniSearchBar) {
    try {
        var isBachelorPage = !($j("body.sub-page-bachelor").length == 0);
        if (isVis && !isBachelorPage) {
            //visitors; it should never get here, unless we wanted other custom logic
            window.document.location = "/Default.aspx";
        }
        else {
            //display ajax loading
            ShowPopupDiv('divMiniSearchAjaxLoading');
            //HidePopupDiv('photos-min');
            document.getElementById('photos-min').style.visibility = 'hidden';
            //document.getElementById('photos-header').style.visibility='hidden';

            //refresh ads
            refreshTop728by90SR();
            refreshRight300by250SR();

            //update omniture
            if (s != null && s != undefined) {
                originalPageName = s.pageName;
                PopulateS(true); //clear existing values in omniture "s" object

                s.events = "event24";
                s.pageName = originalPageName;
                if (nav == 'prev') {
                    s.prop14 = 'Mini Search - Previous Button';
                }
                else {
                    s.prop14 = "Mini Search - Next Button";
                }

                s.t(); //send omniture updated values as page load
            	s.events = '';
            }

            //get mini-search content
            if (clientMiniS && clientMiniS.readyState != 0) {
                clientMiniS.abort();
            }
            clientMiniS = getXHR();
            if (clientMiniS) {
                var url = "/Applications/MemberProfile/MiniSearchAPI.aspx?ajxmode=true&random=" + randomNumber();
                if (isMiniSearchBar) {
                    url = url + "&MiniSearchBar=true&" + params;
                }
                else {
                    url = url + "&" + params;
                }

                if (isBachelorPage) {
                    url += "&fromBachelorPage=true";
                }
                
                clientMiniS.open("GET", url, true);
                clientMiniS.onreadystatechange = function() {
                    try {
                        var done = 4, ok = 200;
                        if (clientMiniS.readyState == done) {
                            if (clientMiniS.status == ok && clientMiniS.responseText) {

                                //setTimeout("UpdateMiniSearchContent();", 5000); //make ajax loader stay up for testing
                                if (isMiniSearchBar) {
                                    UpdateMiniSearchContent("divMiniSearchBar");
                                    //RTL support for fisheye carousel.
                                    isRTL = spark.favorites.BrandId == 1004 ? true : undefined;

                                    MiniSearchFisheye(isRTL);
                                }
                                else {
                                    UpdateMiniSearchContent("divMiniSearch");
                                }
                            }
                            else {
                                if (isAjaxVerboseModeMS) alert('Error returned from server:' + clientMiniS.statusText);
                                //hide ajax loader
                                HidePopupDiv('divMiniSearchAjaxLoading');
                                //ShowPopupDiv('photos-min');
                                document.getElementById('photos-min').style.visibility = 'visible';
                                //document.getElementById('photos-header').style.visibility='visible';
                                clientMiniS = null;
                            }
                        }
                    }
                    catch (e) {
                        if (isAjaxVerboseModeMS) alert(e.description);
                    }
                };
                clientMiniS.send(null);
            }
        }
    }
    catch (e) {
        if (isAjaxVerboseModeMS) alert(e.description);
    }
}

function MiniSearchFisheye(isRTL) {
    var halignValue = isRTL !== undefined ? 'rtl' : 'left';

    $j('#dock').Fisheye({
        maxWidth: 28,
        items: 'li',
        container: '#photos-min',
        itemWidth: 49,
        proximity: 40,
        halign: halignValue,
        margin: 0
    });
}

function UpdateMiniSearchContent(divID) {
    try {
        var d = document.getElementById(divID);
        if (d != null) {
            //load
            d.innerHTML = clientMiniS.responseText;
            
        }
        else {
            if (isAjaxVerboseModeMS) alert('Error: divMiniSearch used to support Ajax is not found');
        }
        //hide ajax loader
        //HidePopupDiv('divMiniSearchAjaxLoading');

        clientMiniS = null;
    }
    catch (e) {
        if (isAjaxVerboseModeMS) alert(e.description);
    }
}

//Image Preloader
function preloading() {
    var myImagesPreloader = new Array();
    for (x = 0; x < preloading.arguments.length; x++) {
        myImagesPreloader[x] = new Image();
        myImagesPreloader[x].src = preloading.arguments[x];
    }
}


/*******START ACCOUNT HISTORY TAB FEATURE*/////
var AccountHistoryTabEnum = new function() {
    //maps to the C# AccountHistoryTabEnum
    this.allfinancialhistory = 'AllFinancialHistory';
    this.financialhistory = 'FinancialHistory';
    this.nonfinancialhistory = 'NonFinancialHistory';
}

//object to hold relevant control IDs on client
//C# controls will dynamically set the IDs
var AccountHistoryObject = new function() {
    this.currentTab = '';
    this.currentTabID = '';
    this.currentTabContentID = '';
    this.tabTrackerID = '';

    //tab IDs
    this.allFinancialHistoryTabID = '';
    this.financialHistoryTabID = '';
    this.nonFinancialHistoryTabID = '';

    //tab content IDs
    this.allFinancialHistoryTabContentID = '';
    this.financialHistoryTabContentID = '';
    this.nonFinancialHistoryTabContentID = '';
}

//primary function to swap tabs
function AccountHistoryTabClick(newTab) {
    try {


        var newContentID = '';
        var newTabID = '';
        if (newTab == AccountHistoryTabEnum.allfinancialhistory) {
            newContentID = AccountHistoryObject.allFinancialHistoryTabContentID;
            newTabID = AccountHistoryObject.allFinancialHistoryTabID;
        }
        else if (newTab == AccountHistoryTabEnum.financialhistory) {
        newContentID = AccountHistoryObject.financialHistoryTabContentID;
        newTabID = AccountHistoryObject.financialHistoryTabID;
        }
        else if (newTab == AccountHistoryTabEnum.nonfinancialhistory) {
            newContentID = AccountHistoryObject.nonFinancialHistoryTabContentID;
            newTabID = AccountHistoryObject.nonFinancialHistoryTabID;
        }

        if (newContentID != '' && newTabID != '') {
            //update current tab
            HidePopupDiv(AccountHistoryObject.currentTabContentID);
            changeClassStyle(AccountHistoryObject.currentTabID, "");

            //update new tab
            ShowPopupDiv(newContentID);
            changeClassStyle(newTabID, "selected");

            AccountHistoryObject.currentTab = newTab;
            AccountHistoryObject.currentTabID = newTabID;
            AccountHistoryObject.currentTabContentID = newContentID;

            varTabTracker = document.getElementById(AccountHistoryObject.tabTrackerID);
            if (varTabTracker != null)
                varTabTracker.value = newTab;
        }

    }
    catch (e) {
        //exception occurred, browser may not support
    }

    return false;
}

/******END ACCOUNT HISTORY TAB FEATURE*/////

__addToNamespace__('spark.favorites', {
    configs: [],
    getConfig: function(idKey, idValue) {
        var config = null;
        for (var idx = 0; idx < spark.favorites.configs.length; idx++) {
            if (spark.favorites.configs[idx][idKey] && spark.favorites.configs[idx][idKey] == idValue) {
                config = spark.favorites.configs[idx];
            }
        }
        return config;
    },
    addConfig: function(config) {
        if (null != config) spark.util.addItem(spark.favorites.configs, config);
        return spark.favorites.configs;
    },
    onlineIndicatorIds: [],
    totalFavs: function(config) {
        var count = spark.util.mapSize;
        return count(config.onlineFavs) + count(config.idleFavs) + count(config.offlineFavs);
    }
});

__addToNamespace__('spark.tracking', {
    addKeyAndValue: function (key, val, overwrite) {
        if (s) {
            if (s[key] && s[key].toLowerCase() != "none" && !overwrite) {
                var str = s[key] + "";
                if (str.indexOf(val) == -1) s[key] += "," + val;
            } else {
                s[key] = val;
            }
        }
    },
    addEvent: function (evt, overwrite) {
        spark.tracking.addKeyAndValue('events', evt, overwrite);
    },
    addLinkTrackVar: function (ltv, overwrite) {
        spark.tracking.addKeyAndValue('linkTrackVars', ltv, overwrite);
    },
    addLinkTrackEvent: function (lte, overwrite) {
        spark.tracking.addKeyAndValue('linkTrackEvents', lte, overwrite);
    },
    addProp: function (num, val) {
        spark.tracking.addKeyAndValue('prop' + num, val, true);
    },
    addPageName: function (val, overwrite) {
        spark.tracking.addKeyAndValue('pageName', val, overwrite);
    },
    getPageName: function () {
        if (s && s['pageName'] && s['pageName'].toLowerCase() != "none") {
            return s['pageName'];
        } else {
            return false;
        }
    },
    track: function () {
        if (s) s.t();
    },
    trackLink: function (params) {
        if (s) s.tl(params.firstParam, 'o', params.linkName);
    }
});

function BlockAPI_SaveProfileBlocks(blockDivID, targetMemberID, blockConfirmationID, iconElementID, iconCSS, iconBlockedCSS, autoPageRefresh) {
    // BlockContact : 1, BlockFromTheirSearch : 2, RemoveFromYourSearch: 4
    var BlockEnum = [1, 4, 2];
    // Collect the input value from the 3 checkboxes
    var blockMask = 0;
    var autoRefreshNeeded = false;
    jQuery("input[type='checkbox']", "#" + blockDivID).each(function(index) {
        if (jQuery(this).attr("checked")) {
            blockMask += BlockEnum[index];
            if (BlockEnum[index] == 4 && autoPageRefresh) {
                autoRefreshNeeded = true;
            }
        }
    });

    jQuery.ajax({ url: "/Applications/API/ProfileBlocking.aspx", data: { blockMaskValue: blockMask, tmemberID: targetMemberID }, dataType: "json",
        success: function(data) {
            // console.log(data);
            var s;
            var profileBlockSaveSuccess = data.SuccessMessage;
            if (data.ErrorMask != 0) {
                // error: fix the checkbox binding and build the error message(s)
                jQuery("input[type='checkbox']", "#" + blockDivID).each(function(index) {
                    // binding of the checkboxes
                    jQuery(this).attr("checked", (data.MaskValue & BlockEnum[index]) == BlockEnum[index]);
                    // error handling for each of the checkboxes
                    if ((data.ErrorMask & BlockEnum[index]) == BlockEnum[index]) {
                        s += '<span class="spr s-icon-page-message"></span> ' + data.ErrorMessages[index];
                    }
                });
            }
            else {
                if (!autoRefreshNeeded) {
                    // success: build the success message. set the appropriate class here for success message
                    s = '<span class="spr s-icon-page-message"></span> ' + profileBlockSaveSuccess;
                    var iconElement = jQuery("#" + iconElementID);
                    if (iconElement) {
                        if (blockMask > 0)
                            iconElement.removeClass().addClass(iconBlockedCSS);
                        else
                            iconElement.removeClass().addClass(iconCSS);
                    }
                }
                else {
                    //refresh page
                    var hidRefreshField = $j("#hidRefreshFilteredSearchResults");
                    if (hidRefreshField) {
                        hidRefreshField.val('true');
                    }
                    $j("form").submit();
                }
            }

            jQuery('#' + blockConfirmationID).stop(true, true).addClass('message').html(s).fadeIn().delay(3000).fadeOut();
            //$j('#blockDiv').hide();
        }
    });

    return false;
}


function LoadContactHistory(layoverID, appendID, targetMemberID, isLazyLoad, hasContactHistory) {
    var isVerbose = false;

    if (isLazyLoad) {
        //verify if content is empty
        var appendHtml = $j("#" + appendID).html();
        if (appendHtml != '') {
            //get content
            $j.ajax({
                type: "POST",
                url: "/Applications/API/Member.asmx/GetContactHistory",
                data: "{ targetMemberID: " + targetMemberID + "}",
                contentType: "application/json; charset=utf-8",
                error: function(jqXHR, textStatus, errorThrown) {
                    if (isVerbose) { alert(textStatus); }
                },
                success: function(result) {
                    var getContactHistoryResultObj = (typeof result.d == 'undefined') ? result : result.d;

                    //get template and bind
                    var templateName = 'contactHistory';
                    if ($j.template[templateName]) {
                        $j('#' + appendID).empty();
                        $j.tmpl(templateName, getContactHistoryResultObj).appendTo("#" + appendID);
                        //$j('#' + layoverID).toggle();
                    }
                    else {
                        $j.ajax({ url: '/WebServices/templates/contactHistory.tmpl.htm',
                            error: function(jqXHR, textStatus, errorThrown) {
                                if (isVerbose) { alert(textStatus); }
                            },
                            success: function(templateMarkup) {
                                $j('#' + appendID).empty();
                                $j.template(templateName, templateMarkup);
                                $j.tmpl(templateName, getContactHistoryResultObj).appendTo("#" + appendID);
                                //$j('#' + layoverID).toggle();
                            }
                        });
                    }
                }
            });
        }
        else {
            //$j('#' + layoverID).toggle();
        }
    }
    else {
        TogglePopupDivDisplay(layoverID);
        //update omniture
        if (s != null) {
            PopulateS(true); //clear existing values in omniture "s" object
            s.prop46 = (hasContactHistory) ? "populated" : "empty";

            s.t(); //send omniture updated values as page load
        }
    }
}

//Object to contain shared params used to process YNM for /ui/profileelements/secretadmirer
var SecretAdmirer_SharedYNMParams = new function() {
    this.externalYAlt = '';
    this.externalMAlt = '';
    this.externalNAlt = '';
    this.externalYYAlt = '';
}

//Object to contain control specific params used to process YNM for /ui/profileelements/secretadmirer
function SecretAdmirer_YNMParams() {
    this.type = '';
    this.parentClientID = '';
    this.encryptedParams = '';
    this.memberID = '';
    this.externalYNMIndicatorID = '';
    this.externalYYIndicatorID = '';
    this.YNMIconCSS = '';
    this.spanSecretAdmirerIconID = '';
    this.secretAdmirerPopupID = '';
    this.secretAdmirerPopupID_desc = '';
    this.secretAdmirerPopupID_descY = '';
    this.secretAdmirerPopupID_descN = '';
    this.secretAdmirerPopupID_descM = '';
    this.secretAdmirerPopupID_descYY = '';
    this.spanYesID = '';
    this.spanNoID = '';
    this.spanMaybeID = '';
    this.YNMVoteStatusID = '';
    this.OmnitureName = '';
}

//Function to process YNM for /ui/profileelements/secretadmirer
function SecretAdmirer_ProcessYNM(params) {
    try {
        if (ConfirmYNMVote(params.parentClientID, params.type)) {
            SaveYNMVote(params.type, params.encryptedParams);

            $j('#' + params.secretAdmirerPopupID).hide();
            $j('#' + params.secretAdmirerPopupID_desc).hide();
            $j('#' + params.secretAdmirerPopupID_descY).hide();
            $j('#' + params.secretAdmirerPopupID_descN).hide();
            $j('#' + params.secretAdmirerPopupID_descM).hide();
            $j('#' + params.secretAdmirerPopupID_descYY).hide();
            $j('#' + params.spanYesID).removeClass().addClass('spr s-icon-click-y-off');
            $j('#' + params.spanNoID).removeClass().addClass('spr s-icon-click-n-off');
            $j('#' + params.spanMaybeID).removeClass().addClass('spr s-icon-click-m-off');

            $j('#' + params.spanSecretAdmirerIconID).removeClass().addClass(params.YNMIconCSS);
            if (params.externalYYIndicatorID != '') {
                $j('#' + params.externalYYIndicatorID).hide();
            }

            var currentStatus = $j('#' + params.YNMVoteStatusID);
            var ynmType = '';

            //apply it when new YNM is ready. currently BBW, BS & SP are not ready ['1001', '90410', '90510']
            var isNewYNMReday = $j.inArray(spark.favorites.BrandId, ['1003', '1004', '1105', '1107']) > -1;

            switch (params.type) {
                case "1":
                    ynmType = "Y";
                    if ((currentStatus.val() & 2) == 2) {
                        currentStatus.val(currentStatus.val() ^ 2);
                    }
                    if ((currentStatus.val() & 4) == 4) {
                        currentStatus.val(currentStatus.val() ^ 4);
                    }
                    currentStatus.val(currentStatus.val() | 1);

                    //If the other user has clicked yes, display the "you both clicked yes" icon:
                    if ((currentStatus.val() & 8) == 8) {
                        $j('#' + params.secretAdmirerPopupID_descYY).show();
                        if (params.externalYNMIndicatorID != '') {
                            $j('#' + params.externalYNMIndicatorID).show().removeClass().addClass('spr s-icon-click-yy').attr("title", SecretAdmirer_SharedYNMParams.externalYYAlt);
                        }
                        if (params.externalYYIndicatorID != '') {
                            $j('#' + params.externalYYIndicatorID).show();
                        }
                    }
                    else {
                        $j('#' + params.secretAdmirerPopupID_descY).show();
                        if (params.externalYNMIndicatorID != '') {
                            $j('#' + params.externalYNMIndicatorID).show().removeClass().addClass('spr s-icon-click-y-on').attr("title", SecretAdmirer_SharedYNMParams.externalYAlt);
                        }
                    }
                    $j('#' + params.spanYesID).removeClass().addClass('spr s-icon-click-y-on');
                    
                    if (isNewYNMReday) {
                        SecretAdmirer_UiUpdateYNM($j('#' + params.spanYesID).closest('.admirer'));
                    }

                    break;

                case "2":
                    ynmType = "N";
                    if ((currentStatus.val() & 1) == 1) {
                        currentStatus.val(currentStatus.val() ^ 1);
                    }
                    if ((currentStatus.val() & 4) == 4) {
                        currentStatus.val(currentStatus.val() ^ 4);
                    }
                    currentStatus.val(currentStatus.val() | 2);

                    $j('#' + params.secretAdmirerPopupID_descN).show();
                    if (params.externalYNMIndicatorID != '') {
                        $j('#' + params.externalYNMIndicatorID).show().removeClass().addClass('spr s-icon-click-n-on').attr("title", SecretAdmirer_SharedYNMParams.externalNAlt);
                    }
                    $j('#' + params.spanNoID).removeClass().addClass('spr s-icon-click-n-on');

                    if (isNewYNMReday) {
                        SecretAdmirer_UiUpdateYNM($j('#' + params.spanNoID).closest('.admirer'));
                    }

                    break;

                case "3":
                    ynmType = "M";
                    if ((currentStatus.val() & 1) == 1) {
                        currentStatus.val(currentStatus.val() ^ 1);
                    }
                    if ((currentStatus.val() & 2) == 2) {
                        currentStatus.val(currentStatus.val() ^ 2);
                    }
                    currentStatus.val(currentStatus.val() | 4);

                    $j('#' + params.secretAdmirerPopupID_descM).show();
                    if (params.externalYNMIndicatorID != '') {
                        $j('#' + params.externalYNMIndicatorID).show().removeClass().addClass('spr s-icon-click-m-on').attr("title", SecretAdmirer_SharedYNMParams.externalMAlt);
                    }
                    $j('#' + params.spanMaybeID).removeClass().addClass('spr s-icon-click-m-on');

                    if (isNewYNMReday) {
                        SecretAdmirer_UiUpdateYNM($j('#' + params.spanMaybeID).closest('.admirer'));
                    }

                    break;
            }



            //update omniture
            if (s != null) {
                PopulateS(true); //clear existing values in omniture "s" object

                var originalPageName = s.pageName;
                s.pageName = s.pageName + " - Secret Admirer";
                //s.events = "event41";
                s.prop30 = ynmType + " - " + params.OmnitureName;
                s.prop31 = params.memberID;
                s.t(); //send omniture updated values as page load
                s.events = '';
	            s.pageName = originalPageName;
            }
        }
    }
    catch (e) {
        //alert(e);
    }
}

//Function to update YNM UI - jQuery object needed
function SecretAdmirer_UiUpdateYNM($elements) {

    $elements.each(function () {
        var $elements = $j(this),
            $elementsClass = $elements.attr("class"),
            $items = $elements.find("[class*='s-icon-click']"),
            itemLengthAll = $items.length,
            itemsInitClass = [],
            $selectedItem = null,
            selectedItemClass = null,
            selectedItemIndex = null,
            $nonSelectedItems = [],
            nonSelectedItemsIndex = [],
            isSelected = false;

        $items.each(function (i) {
            var $this = $j(this),
                className = $this.attr('class'),
                hoverClassName = className.replace(/(\w+?)$/, 'hover');

            itemsInitClass[i] = className;

            // one choice has been selected
            if (className.indexOf('-on') > -1) {
                isSelected = true;
                $selectedItem = $this;
                selectedItemClass = $this.attr('class');
                selectedItemIndex = i;
            } else {
                $nonSelectedItems.push($this);
                nonSelectedItemsIndex.push(i);
            }

        });

        if (isSelected) {
            var itemLengthNonSelected = $nonSelectedItems.length;

            // apply non selected class name
            for (i = 0; i < itemLengthNonSelected; i++) {
                var $nonSelectedItem = $nonSelectedItems[i],
                    nonSelectedItemClass = $nonSelectedItem.attr("class"),
                    nonSelectedNewClass = nonSelectedItemClass.replace(/(\w+?)$/, 'non');

                $nonSelectedItem.removeClass().addClass(nonSelectedNewClass).css({'opacity':1});
            }

        }
        
        $items.unbind('mouseenter').bind('mouseenter', function () {
            var $this = $j(this),
                $thisClass = $this.attr('class'),
                $theHoveredOne = $this,
                $nonHoveredOnes = $items.not(this);

            $this.removeClass().addClass($thisClass.replace(/(\w+?)$/, 'hover'));
            $nonHoveredOnes.each(function () {
                var oldClass = $j(this).attr('class'),
                    newClass = oldClass.replace(/(\w+?)$/, 'off');

                $j(this).removeClass().addClass(newClass).css({ 'opacity': 0.15 });
            });

        });

        $items.unbind('mouseleave').bind('mouseleave', function () {

            if (isSelected) {
                $items.each(function (i) {
                    if (i == selectedItemIndex) {
                        $j(this).removeClass().addClass(selectedItemClass).css({ 'opacity': 1 });
                        return true;
                    }
                    $j(this).removeClass().addClass(itemsInitClass[i].replace(/(\w+?)$/, 'non')).css({ 'opacity': 1 });
                });
            } else {
                $items.each(function (i) {
                    $j(this).removeClass().addClass(itemsInitClass[i]).css({ 'opacity': 1 });
                });
            }
            
        });

    });
    
}

// Toggling tabbed/hover menus
function TabbedMenuToggler($menu, isHover, $link, eventType) {
    if (eventType === undefined) {
        eventType = 'click';
    }

    var $links = $menu.find('.menu'); //get the links

    if (eventType == 'click') {
        $links.click(function () {
            var indx = $links.index(this); //get current index

            TabbedControl($links, indx);

            if (!isHover) {
                $menu.mouseleave(function () {
                    TabbedReset($j(this));
                });
            }

            $menu.click(function (e) {
                TabbedReset($j(this));
                e.stopPropagation();
            });

            $j('body').one('click', function () {
                TabbedControl($links);
            });
        });
    } else if (eventType == 'hover') {

        $links.on('mouseenter', function (e) {
            $link.parents('.comm.item.tabbed').addClass('active');
        });

        $j('body').one('click', function () {
            TabbedControl($links);
        });
    }

    return false;    
}

function TabbedReset($obj) {
    $obj.find('li.item').removeClass('active')
            .find('.items').hide();
}

function TabbedControl($obj, indx) {
    // toggling opened layer & .active class name

    $j.each($obj, function (i) {
        if (i == indx) {
            $j($obj[i]).closest('li.item').toggleClass('active');
            return true;
        }

        $j($obj[i]).closest('li.item').find('.items').hide()
        .end().removeClass('active'); // return to li.item
    });
}

function SpriteIconsInHoverMenuCotrol($sIcon) {
    var isListView = $sIcon.closest('.list-view-30').length,
        isToolBar = $sIcon.closest('.profile30-toolbar').length,
        isPerformed = $sIcon.attr('class').indexOf('performed') > -1 || $sIcon.attr('class').indexOf('active') > -1,
        initIconBgPos = $sIcon.css('background-position').split(' '),
        iconWidth = $sIcon.width();

    //console.log('$sIcon: ', $sIcon, 'isListView: ', isListView, 'isPerformed: ', isPerformed, '$sIcon: ', $sIcon, 'initIconBgPos: ', initIconBgPos);

    if (!isPerformed) {
        if (isListView && $sIcon.attr('class').indexOf('hover-comm') > -1) {
            return;
        } else if (!isListView && !isToolBar && $sIcon.attr('class').indexOf('hover-block') > -1) {
            iconWidth = iconWidth * 2;
        }

        $sIcon.css('background-position', (parseInt(initIconBgPos[0].replace('px', '')) - iconWidth) + 'px ' + initIconBgPos[1]);

    }
}

function SpriteIconsInHoverMenuReset($sIcon) {
    var isListView = $sIcon.closest('.list-view-30').length,
        isToolBar = $sIcon.closest('.profile30-toolbar').length;
    
    $sIcon.each(function (i) {
        var $this = $j(this),
            initIconBgPos = null,
            isPerformed = $this.attr('class').indexOf('performed') > -1 || $this.attr('class').indexOf('active') > -1,
            xPos = null,
            iconWidth = $this.width();

        if (isPerformed) {
            $this.css('background-position', '');
            return true;
        }

        if ($this.attr('style') !== undefined && $this.attr('style').indexOf('background-position') !== -1) {

            initIconBgPos = $this.css('background-position').split(' ');
            xPos = initIconBgPos[0].replace('px', '');

            if (isListView) {
                if ($this.attr('class').indexOf('favorites') > -1 || $this.attr('class').indexOf('hover-block') > -1) {
                    if (xPos == iconWidth * -1) {
                        return true;
                    }
                }
            } else if (isToolBar) {
                if (xPos == iconWidth * -1) {
                    return true;
                }
            } else {
                if ($this.attr('class').indexOf('hover-block') > -1) {
                    iconWidth = iconWidth * 2;
                }

                if (xPos == 0) {
                    return true;
                }
            } 

            $this.css('background-position', (parseInt(initIconBgPos[0].replace('px', '')) + iconWidth) + 'px ' + initIconBgPos[1]);
        }
    });
}

function MenuToggler($link, $layer, eventType) {
    if (eventType === undefined) {
        eventType = 'click';
    }

    // toggling layer(lists)
    if (eventType == 'click') { 

        $link.click(function () {
            if ($layer.css('display') == 'none') {
                $layer.show();
            } else {
                $layer.hide();
            }
            $j('body').one('click', function () {
                $layer.hide();
            });
            return false;
        });

    } else if (eventType == 'hover') {

        $link.on('mouseenter', function (e) {
            $layer.show();
        }).on('mouseleave', function (e) {
            if ($layer.css('display') == 'block') {
                $layer.hide();
            } 
        });

        
        $layer.on('mouseenter', function (e) {
            $layer.show();
        }).on('mouseleave', function (e) {
            $layer.hide();
        });

        $link.mouseout(function () {
            if ($layer.css('display') == 'none') {
                $layer.hide();
            }
        });

        $j('body').one('click', function () {
            $layer.hide();
            return false;
        });       
   }

   $layer.click(function (e) {
       e.stopPropagation();
   });
}

// simplified blockUi 
function BlockElem(obj) {
    var $blockDiv = $j('<div class="block-elem"></div>'),
                    zNum = parseInt(obj.css('z-index')) || 1,
                    cssObj = {
                        'width': obj.outerWidth(),
                        'height': obj.outerHeight(),
                        'z-index': zNum,
                        'position': 'absolute',
                        'top' : 0,
                        'left' : 0
                    };

    $j(obj).css('position','relative').prepend($blockDiv);
    $blockDiv.css(cssObj);
}

// Begin IOS helper functions
function spark_fixIOSSelectBug($object){
    $object.each(function(){
        // set variables
        var $this = $j(this); // cache this
        var si = $this.attr('selectedIndex'); // find which option is selected, if any
        var $options = $this.find('option'); // make object of options
        var $firstOption = $options.filter(":first"); // filter first option
        
        // disable any existing empty first options
        if($firstOption.html() == ""){
            $firstOption.attr('disabled', 'disabled');
        }
        // all selects that do not have a selected attribute
        if (si === -1){
            //add temporary style to see if/where it is working
            //$this.css('border','2px solid green');
            // prepend an empty, disabled option if none exist
            if($firstOption.html() != ""){
                $this.prepend('<option value="" disabled="disabled"></option>');
                // set size +1
                $this.attr('size', function(i, val){
                    return ++val;
                }); 
            }
        }
    });
}
//End IOS helper functions

//Begin Search Preferences helper functions
var preferenceSliderTextArray = ['Not Important','Desirable','Somewhat Important', 'Important', 'Very Important'];

function preferenceSliderControl (preferenceContainerObj, hiddenSliderValueID, initialValue) {

    var $this = preferenceContainerObj,
        minValue = 0,
        maxValue = 4,
        stepValue = 1;

    $j(".slider", $this).slider({
        range: "min",
        value: initialValue,
        min: minValue,
        max: maxValue,
        step: stepValue,
        slide: function (event, ui) {
            // find any element with class .value WITHIN scope of $this
            $j(".value", $this).text(preferenceSliderTextArray[ui.value]);
            $j("#" + hiddenSliderValueID, $this).val(ui.value);
        },
        create: function (event, ui) {
            // Add measure below slider
            var measureHtml = $j("<ol class='measure'>"),
                incrementNum = maxValue / stepValue,
                measureWidth = $j(this).width() / incrementNum - 2;

            for (var i = 0; i < incrementNum; i++) {
                if (i == incrementNum - 1) {
                    measureHtml.append("<li class='number last' style='width:" + measureWidth + "px'></li>");
                } else {
                    measureHtml.append("<li class='number' style='width:" + measureWidth + "px'></li>");
                }
            }
            $j(this).append(measureHtml);
        }
    });

    $j(".value", $this).text(preferenceSliderTextArray[initialValue]);
    $j("#" + hiddenSliderValueID, $this).val(initialValue);

}

function preferenceToggler($clicker, $container, openClassName, closeClassName, arrowOpenClassName, arrowCloseClassName) {
    // Toggle control
    $clicker.click(function (e) {
        e.preventDefault();
        $clicker.toggleClass(openClassName).toggleClass(closeClassName)
                .find('.spr:first').toggleClass(arrowOpenClassName).toggleClass(arrowCloseClassName);
        $container.toggle();
    })
}

function preferenceToggleAll($clicker, $container, clickerOpenClassName, clickerCloseClassName, arrowOpenClassName, arrowCloseClassName, openClassName, closeClassName) {
    var collapsed = true,
        $clickerArrow = $clicker.find('span.spr'),
        $clickerWords = $clicker.find('span:not(".spr")'),
        $toggleBar = $container.find('.search-pref-header'),
        $toggleContainer = $container.find('.search-pref-container'),
        $arrow = $toggleBar.find('.spr:first');

    $clicker.click(function () {

        if (collapsed) {
            $toggleBar.removeClass(closeClassName).addClass(openClassName);
            $arrow.removeClass(arrowCloseClassName).addClass(arrowOpenClassName);
            $toggleContainer.show();

            collapsed = false;
        } else {
            $toggleBar.removeClass(openClassName).addClass(closeClassName);
            $arrow.removeClass(arrowOpenClassName).addClass(arrowCloseClassName);
            $toggleContainer.hide();

            collapsed = true;
        }

        $clickerArrow.toggleClass(clickerOpenClassName).toggleClass(clickerCloseClassName);
        $clickerWords.toggleClass('hide');

        return false;
    });
}

//End Search Preferences helper functions

// Fix header collpasing issue caused by ADP
$j(function () {
    var $collapsedEl = $j('.header-container'),
        existStyle = $j('.header-container').attr('style');
    
    $collapsedEl.attr('style', existStyle + 'display: block !important;');

    if (ElHeight = $collapsedEl.height() == 0) {
        //Firefox fix
        if ($j.browser.mozilla) {
            var mozBindingProp = $j(".ad-on").css('-moz-binding');
            if (mozBindingProp !== undefined && mozBindingProp.indexOf('url') > -1) { //Check if '-moz-binding' attribute exists, and if it does we determine that there's an ad-blocker present.
                $collapsedEl.removeClass('ad-on');
                $collapsedEl.addClass('headerDupFrom').clone(true).removeClass('headerDupFrom').insertBefore('.header-container');
                $j('.headerDupFrom').remove();
            }
        }
    }
});
