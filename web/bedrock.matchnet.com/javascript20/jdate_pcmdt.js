﻿/*---------------------------------------------------------------------------------------
/ Web2Wap Redirection Script
/
/ Initial Version: 16-June-2010
/ Last updated: 1-Apr-2011
/
/ This file is the property of PassCall Advanced Technologies Ltd. and may not be modified
/ in any way whatsoever without the prior written consent of PassCall.
/ PassCall takes no responsibility for the unauthorized modification of this file, which
/ is likely to damage the proper functionality.
/ Copyright 2000-2010 (c) PassCall Advanced Technologies Ltd. All Rights Reserved.
/---------------------------------------------------------------------------------------*/

var passcall_pcmdt =
{
    init: function() {
        try {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + 30);
            if (location.href.indexOf('nopcmdt=1') > -1) document.cookie = '___pcmdt___=0; path=/; expires=' + exdate.toUTCString();
            if (document.cookie.indexOf('___pcmdt___=0') > -1) return;
            var redir = parseInt(Number(document.cookie.indexOf('___pcmdt___=1') > -1 || /(^((SO|Se|ha|[m-p]{2})(NIM|ndo|ier|[h-k]{2}a)|(MO|HT|Pa|EZ)(T-|C|lm|OS)|(SA|po|Ph|UP|Mo|Er|fr|Ic|Bl|So)(MSU|rta|ili|Br|bil|ics|omp|op|ack|nyE)(NG|lmmm|ps|owse|eExp|so|assc|ie|Berr|rics)|(S(EC|GH|IE)|[O-Q][a-d]L\d{2}[a-z]{2}\d+|LG)))|([l-p]{2}[x-z]([(h-m)]|a){4}\/((1\.22)|([45]\.0).*?(H|sym|win|pal|ProF|we)(TC_|bia|dows|msou|iLE.+m|b)(HD\d|n\s*os|\sce|rce|IDp|os)))|((q|O|U|SZ)(q|ww|p|O|i)(v|dv|e|ds|OO)(Q|W|R|OO|Z)(V|PP|YY|A).*?\s[k-p](in|OB)[h-j])|([h-j][o-q][g-i][n-p]{2}[d-f])|([ba][na][qd][n-s]{2}[h-j][c-e])/i.test(navigator.userAgent) && !/([h-j]pa[c-e]|GT-P10{3})/ig.test(navigator.userAgent)));
            document.cookie = '___pcmdt___=' + redir + "; path=/; expires=" + exdate.toUTCString();
            if (redir) location.href = location.href.replace(location.host, 'nb.jdate.go.passcall.mobi');
        }
        catch (e) { }
    } ()
}

