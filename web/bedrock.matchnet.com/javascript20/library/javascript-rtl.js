﻿var sendRegEventOnInputFocusDefaultReg = "notsent";

//MiniSearch
jQuery(document).ready(function () {

    if (jQuery('#MarketingClose').length) {
        jQuery('#profile-full-comm .action-hot-list .listMenuContainer').css({ display: "none" });
    }

    jQuery('#MarketingClose').click(function () {
        jQuery('#divMiniSearchMarketingCopy').hide();
        jQuery('#profile-full-comm .action-hot-list .listMenuContainer').css({ display: "block" });
        return false;
    });
});

// rounded corner tab style enhancements
jQuery(document).ready(function () {

    var indHover = '<div class="nav-profile-full-indicator"></div>';
    var indSelected = '<div class="nav-profile-full-selector-indicator"></div>';

    jQuery('.nav-rounded-tabs li.tab.selected').not('#video-tabs .nav-rounded-tabs li.tab.selected').append(indSelected);

    jQuery('.nav-rounded-tabs li.tab a').hover(function () {
        jQuery('.nav-rounded-tabs li.tab .nav-profile-full-indicator').remove();
        jQuery(this).not('#video-tabs ul.nav-rounded-tabs li.tab a').not('.nav-rounded-tabs li.tab.selected a').append(indHover);
        jQuery('.nav-rounded-tabs li.tab.selected.hover').append(indSelected);
    }, function () {
        jQuery('.nav-rounded-tabs li.tab .nav-profile-full-indicator').hide();
    });

    jQuery('.nav-rounded-tabs.click li.tab a').click(function (event) {
        jQuery('.nav-rounded-tabs li.tab div.nav-profile-full-selector-indicator').remove();
        jQuery(this).append(indSelected);
    });

    // Change the down arrow on the tabs. 
    jQuery('.thumbs li').click(function (event) {
        jQuery('.nav-rounded-tabs li.tab div.nav-profile-full-selector-indicator').remove();
        var relatedTab = jQuery(".tab[id*=" + this.className + "]");
        jQuery(relatedTab).append(indSelected);
    });
});

// UI enhancements not based on browser or element widths
//all
jQuery(document).ready(function () {
    //open links that go off-site in new window
    jQuery('a[rel*=external]').attr('target', '_blank');
    jQuery('a[rel*=popup]').click(function () {
        var href = jQuery(this).attr('href');
        window.open(href, 'popup', 'height=500,width=646,toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,location=yes,directories=no,toolbar=yes');
        return false;
    });

    jQuery('[rel*=hover]').live('mouseover mouseout', function (event) {
        if (event.type == 'mouseover') {
            var $hoverE = jQuery(this).next('div.rel-layer-div');
            jQuery($hoverE).show();
        } else {
            jQuery(this).next('div.rel-layer-div').hide().css({ left: '0' });
            return false;
        }
    });

    jQuery('a[rel*=click]').click(function () {
        var swid = (jQuery('#content-container').width()) - 22;
        var $hoverE = jQuery(this).next('div.rel-layer-div');
        var lmar = jQuery('#content-container').offset();

        jQuery($hoverE).show();

        var hwid = $hoverE.width();
        var iposRaw = jQuery(this).offset();
        var ipos = iposRaw.right - lmar.right

        if (hwid > swid) {
        }
        else if (ipos + hwid > swid) {
            var move = swid - (ipos + hwid);
            $hoverE.css({ right: move });
        }
        return false;
    });

    jQuery('.rel-layer-div a.click-close').click(function () {
        jQuery(this).parent().parent().hide();
        return false;
    });
    $j('span.spr').parent().addClass('spr-parent');

    /* Full profile page - keyboard arrows navigation */
    attachKeyboardArrowsEventsProfileNav();

    //Remove keydown event (from body) when navigating photos with keyboard arrows & also when clicking on message textarea.
    jQuery("body.page-memberprofile.view-profile-not-yours .photos-placeholder, body.page-memberprofile.view-profile-not-yours .profile30-comm .comment textarea").click(function () {
        console.log('hover photos');
        jQuery("body.page-memberprofile.view-profile-not-yours").off('keydown');
    });

    //Attach back keydown event (to body) when losing focus form the message textarea.
    jQuery("body.page-memberprofile.view-profile-not-yours .profile30-comm .comment textarea").blur(function () {
        attachKeyboardArrowsEventsProfileNav();
    });

    //Attach back keydown event (to body) when closing navigating photos with keyboard arrows.
    jQuery("body.page-memberprofile.view-profile-not-yours .spop .spop-nav a.spop-switch-compact").click(function () {
        attachKeyboardArrowsEventsProfileNav();
    });

    if (jQuery('#divMiniSearchBar.mini-search.arrows-nav .nav-profile a.next-profile').length > 0
        || jQuery('#divMiniSearchBar.mini-search.arrows-nav .nav-profile a.prev-profile').length > 0) {
        var isDevice = isDeviceMobile();
        var userOS = isDevice === null ? 'desktopComputer' : isDevice[0];
        var devices = new Array('iPad', 'Android', 'BlackBerry', 'Mobile', 'mobile', 'Touch');

        if (userOS == 'desktopComputer') {
            //if we're on a desktop computer make the arrows show only when hovering over the profile area (beneath the fisheye carousel).
            jQuery("#content-container .wrapper, #content-container #content-promo").on('mouseover', function () {
                showArrowsProfileNav();
            }).on('mouseout', function () {
                hideArrowsProfileNav();
            });

            jQuery(".nav-profile a.next-profile, .nav-profile a.prev-profile").on('mouseover', function () {
                showArrowsProfileNav();
            });
        } else {
            if (devices.indexOf(userOS) > -1) {
                //if we're on a tablet device make the arrows always show.
                showArrowsProfileNav();
            }
        }
    }
});

function showArrowsProfileNav() {
    jQuery('.nav-profile a.prev-profile').show();
    jQuery('.nav-profile a.next-profile').show();

    return false;
}

function hideArrowsProfileNav() {
    jQuery('.nav-profile a.prev-profile').hide();
    jQuery('.nav-profile a.next-profile').hide();

    return false;
}

function attachKeyboardArrowsEventsProfileNav() {
    jQuery("body.page-memberprofile.view-profile-not-yours").keydown(function (e) {
        var keyCode = e.keyCode || e.which, arrow = { left: 37, up: 38, right: 39, down: 40 };

        switch (keyCode) {
            case arrow.left:
                navigateToNextProfile();
                break;
            case arrow.up:
                break;
            case arrow.right:
                navigateToPrevProfile();
                break;
            case arrow.down:
                break;
            default: return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
    });

    return false;
}

function navigateToPrevProfile() {
    var prevProfileURL;
    if (jQuery('#divMiniSearchBar.mini-search.arrows-nav .nav-profile a.next-profile').length > 0
        || jQuery('#divMiniSearchBar.mini-search.arrows-nav .nav-profile a.prev-profile').length > 0) {
        prevProfileURL = jQuery('#divMiniSearchBar.mini-search.arrows-nav .nav-profile a.prev-profile:first-child').attr('href');
        window.location.href = prevProfileURL + '&omniKeyboard=1'; //'omniKeyboard' param is for omniture
        return false;
    }
}

function navigateToNextProfile() {
    var nextProfileURL;
    if (jQuery('#divMiniSearchBar.mini-search.arrows-nav .nav-profile a.next-profile').length > 0
        || jQuery('#divMiniSearchBar.mini-search.arrows-nav .nav-profile a.prev-profile').length > 0) {
        nextProfileURL = jQuery('#divMiniSearchBar.mini-search.arrows-nav .nav-profile a.next-profile').attr('href');
        window.location.href = nextProfileURL + '&omniKeyboard=1'; //'omniKeyboard' param is for omniture;
        return false;
    }
}

function updateURLParameter(url, param, paramVal) {
    var TheAnchor = null;
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";

    if (additionalURL) {
        var tmpAnchor = additionalURL.split("#");
        var TheParams = tmpAnchor[0];
        TheAnchor = tmpAnchor[1];
        if (TheAnchor)
            additionalURL = TheParams;

        tempArray = additionalURL.split("&");

        for (i = 0; i < tempArray.length; i++) {
            if (tempArray[i].split('=')[0] != param) {
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }
    else {
        var tmpAnchor = baseURL.split("#");
        var TheParams = tmpAnchor[0];
        TheAnchor = tmpAnchor[1];

        if (TheParams)
            baseURL = TheParams;
    }

    if (TheAnchor)
        paramVal += "#" + TheAnchor;

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

function isDeviceMobile() {
    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i) && navigator.userAgent.match(/mobile|Mobile/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/BB10; Touch/);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/webOS/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    return isMobile.any();
}

//generic jQuery onDocReady adjustments
jQuery(document).ready(function () {
    // add element around results member spotlight
    jQuery('.results.spotlight-header').wrap('<div class="spotlighted-header-wrap"></div>');
    jQuery('.results.list-view.spotlighted').wrap('<div class="spotlighted-profile-wrap"></div>');
});

// z-index fixes - these should be made into functions at some point
if ($j.browser.msie && parseInt($j.browser.version) <= "8") {
    jQuery(document).ready(function () {
        var zIndexNumber = 100;
        jQuery('#content-main .listMenuContainer').each(function () {
            jQuery(this).css('zIndex', zIndexNumber);
            zIndexNumber -= 2;
        });
        jQuery('#content-main .sub-item').each(function () {
            jQuery(this).css('zIndex', zIndexNumber);
            zIndexNumber -= 2;
        });
    });
}

// zebra stripe for articles
jQuery(document).ready(function () {
    jQuery('#article table.zebra-striping tr:even').addClass('odd');
});

// zebra stripe inbox
jQuery(document).ready(function () {
    jQuery('#mail-list .mail-table tr:even').addClass('odd');
});


// transparent layer over mini-profile
// compose message
jQuery(document).ready(function () {
    try {
        var blockProfiles = jQuery('body.page-email.sub-page-viewmessage .results.list-view')
            .add('body.page-sendtofriend.sub-page-sendtofriend .results.list-view')
            .add('body.page-memberservices.sub-page-reportmember .results.list-view')
            .add('body.page-email.sub-page-tease .results.list-view')
            .add('#profile-mini-blocked .results.list-view');
        jQuery(blockProfiles).block({
                message: null,
                overlayCSS: { backgroundColor: '#96b1cc', opacity: '0.3', cursor: 'auto' }
            }
        );
        jQuery('body.page-subscription .carrot-profile .member-pic, body.page-subscription .carrot-profile .member-info').block({
                message: null,
                overlayCSS: { backgroundColor: '#96b1cc', opacity: '0.0', cursor: 'auto' }
            }
        );
        jQuery('.ie7 .secret-admirer-module .region-label').remove();
        jQuery('.secret-admirer-module .prof-info em br').remove();
    } catch(e) {
        if (typeof (console) !== "undefined") {
            console.log(e.message);
        }
    }
});



//edit profile adjustments
jQuery(document).ready(function () {

    jQuery('body.page-memberprofile.sub-page-registrationstep1 #prefBorder tr')
        .add('body.page-memberprofile.sub-page-registrationstep2 #prefBorder tr')
        .add('body.page-memberprofile.sub-page-registrationstep3 #prefBorder tr')
        .add('body.page-memberprofile.sub-page-registrationstep4 #prefBorder tr')
        .css({ height: '2.6em' })
    ;
    jQuery('body.page-memberprofile.sub-page-registrationstep1 #prefBorder td')
        .add('body.page-memberprofile.sub-page-registrationstep2 #prefBorder td')
        .add('body.page-memberprofile.sub-page-registrationstep3 #prefBorder td')
        .add('body.page-memberprofile.sub-page-registrationstep4 #prefBorder td')
        .css({ paddingLeft: '.3em' })
    ;
    jQuery('body.page-memberprofile.sub-page-registrationstep3 table.twoColumnCheckList td:even')
        .add('body.page-memberprofile.sub-page-registrationstep4 table.twoColumnCheckList td:even')
        .add('body.page-memberprofile.sub-page-registrationstep3 table.threeColumnCheckList td:even')
        .css({ width: '18em' })
    ;
    jQuery('body.page-memberprofile.sub-page-registrationstep1 #prefBorder .edit-profile-form-table')
        .add('body.page-memberprofile.sub-page-registrationstep2 #prefBorder .edit-profile-form-table')
        .add('body.page-memberprofile.sub-page-registrationstep3 #prefBorder .edit-profile-form-table')
        .add('body.page-memberprofile.sub-page-registrationstep4 #prefBorder .edit-profile-form-table')
        .css({ marginRight: '1em' })
    ;
    jQuery('body.page-memberprofile.sub-page-registrationstep1 #prefBorder textarea')
        .add('body.page-memberprofile.sub-page-registrationstep2 #prefBorder textarea')
        .add('body.page-memberprofile.sub-page-registrationstep3 #prefBorder textarea')
        .add('body.page-memberprofile.sub-page-registrationstep4 #prefBorder textarea')
        .css({ width: '98%' })
    ;

    jQuery('body.page-memberprofile.sub-page-registrationstep1 #prefBorder table.full-width').css({ width: '98%' });
    jQuery('body.page-memberprofile.sub-page-registrationstep1 #prefBorder [width]')
        .add('body.page-memberprofile.sub-page-registrationstep2 #prefBorder [width]')
        .add('body.page-memberprofile.sub-page-registrationstep3 #prefBorder [width]')
        .add('body.page-memberprofile.sub-page-registrationstep4 #prefBorder [width]')
        .removeAttr('width')
    ;

    jQuery('body.page-memberprofile #content-main #prefBorder').show();
    jQuery('body.page-memberprofile #content-main .personal-info').show();
});

// Classic overrides

jQuery(document).ready(function () {
    // Mail make view profile table 100%
    jQuery('#_ctl0__ctl4_viewProfileInfo_ViewProfileTable').css({ width: "100%" });
    jQuery('#_ctl0__ctl4_viewProfileInfo__ctl1_tbInterests td, #_ctl0__ctl4_viewProfileInfo__ctl0_tbRelationship td').css({ paddingBottom: "1em" });
});

jQuery(document).ready(function () {
    // toggle FAQ answer block
    jQuery('#faq-wrapper #top-ten h3').click(function () {
        jQuery(this).next("div.answer-block").toggle();
    });
});


//Show/Hide message setting help blocks
jQuery(document).ready(function () {

    if (jQuery('#msgPrefContainer').length > 0) {
        jQuery(".whatsthis").click(function () {
            //This grabs the first class name of the (What's this?) link, 
            //which should be the same as the first part of the id. It will
            //also be the first part of the corresponding answer box id.
            var answerBox = "#" + jQuery(this).attr("class").split(' ').slice(0)[0] + "-answer";

            //Which (What's this?) link was clicked.
            var whatsThisLink = jQuery(this);

            //Hide the (What's this?) link and show corresponding answer box
            jQuery(whatsThisLink).removeClass('showSpan').addClass('hide');
            jQuery(answerBox).removeClass('hide').addClass('show');

            //Add listener to "[Hide]" link
            var hideLink = answerBox + " .messageSettingsHelpParagraphHide a";
            jQuery(hideLink).click(function () {
                jQuery(answerBox).removeClass('show').addClass('hide');
                jQuery(whatsThisLink).removeClass('hide').addClass('showSpan');
            });
        });
    }
});
/*
jQuery(document).ready(function() {
function showslider() {
var windowHeight = jQuery(document).height();
jQuery('#ie6only #slider-container').width(jQuery(document).width());
jQuery('#slider-container').height(windowHeight).show();
jQuery('#emislider').show().animate({ top: "100px" }, 2000);   // modify the "2000" value to dictate animation speed; higher is slower
}

function setOmnitureValues(propertyValue, pageName) {
PopulateS(true); //clear existing values in omniture "s" object
s.prop14 = propertyValue;
s.pageName = pageName;
s.tl(); //send omniture updated values as page load
}
if (jQuery("#emislider").length > 0) {
// the close button
jQuery("map area#slider_close").click(function() {
jQuery('#emislider').hide();
jQuery('#slider-container').hide();
return false;
});

// the continue button
jQuery("map area#slider_continue, map area#slider_partner, map area#slider_claim").click(function() {
var thehref = jQuery(this).attr("href");
jQuery('#emislider').hide();
jQuery('#slider-container').hide();
setOmnitureValues("JRewards Slider – Continue", "jrewards");
window.open(thehref);
return false;
});
showslider();
}
});

//Close jrewards slider			
function hideSlider() {
jQuery(document).ready(function() {
jQuery('#emislider').hide();
jQuery('#slider-container').hide();
});
}
*/
$j(document).ready(function () {
    $j.gbParams = {};
    $j.gbParams.isLoaded = true;
    // homepage filmstrip
    var filmStripItems = $j(".filmstrip li.item").size();
    if (filmStripItems <= 6) {
        $j('button.filmstrip-next').addClass('disabled');
    }
    $j(".filmstrip").jCarouselLite({
        btnNext: ".filmstrip-next",
        btnPrev: ".filmstrip-prev",
        circular: false,
        mouseWheel: true,
        scroll: 6,
        speed: 600,
        visible: 6
    });

    // homepage activity items
    var vertStripItems = $j(".vertstrip-news li.item").size();
    if (vertStripItems <= 5) {
        $j('button.vertstrip-next').addClass('disabled');
    }
    $j(".vertstrip-news").jCarouselLite({
        btnNext: ".vertstrip-next",
        btnPrev: ".vertstrip-prev",
        circular: false,
        mouseWheel: true,
        visible: 3,
        vertical: true
    });
    $j('button.filmstrip-prev.disabled, button.filmstrip-next.disabled, button.vertstrip-prev.disabled, button.vertstrip-next.disabled').live('click', function () { return false; });

    $j(".ajax-loading").hide();

});

$j(document).ready(function () {
    var $smiles = $j('#smiles');

    //$smiles.find('ul li:first-child a').addClass('open');
    $smiles.find(".category").live('click', function (event) {
        event.preventDefault();
        if ($j(this).siblings().is(':hidden')) {
            $j(this).addClass('open');
            $j(this).siblings().show();
            $j('.ui-widget-overlay').css({ 'height': $j(document).height() });
        } else {
            $j(this).removeClass('open');
            $j(this).siblings().hide();
            // uncomment to shrink the modal height when closing 
            // causes a slight jump in certain situations. perhaps it is better to just let the page get long
            //$j('.ui-widget-overlay').css({'height': 'auto'}).css({'height':$j(document).height()});
        }
    });
    $j('#content-main').delegate('.tooltip', 'mouseover mouseout', function (e) {
        var $target = $j(e.target);
        var tipTitle = $target.attr('title');

        if (tipTitle != '') {
            $target.data('tipTitleData', tipTitle);
        }

        $target.attr('title', '');

        if (e.type == 'mouseover') {
            var tipHtml = '<div class="tooltip-content"><p>' + $target.data('tipTitleData') + '</p></div>';
            var tip = $j(tipHtml);
            tip.appendTo('body').css({ 'position': 'fixed', 'left': (e.pageX - tip.outerWidth()) - 10, 'top': (e.pageY - $j(window).scrollTop()) + 10 }).show();
        }
        if (e.type == 'mouseout') {
            $j('.tooltip-content').remove();
        }
    });
});

// Send reg start event only after focus on reg field when coming from media
$j(document).ready(function () {
    if (getUrlParam(location.href, 'media') == "1") {
        jQuery("#content-main input,#content-main select").focusin(function () {
            if (sendRegEventOnInputFocusDefaultReg == "notsent") {
                sendRegEventOnInputFocusDefaultReg = "sent"
                s.events = s.apl(s.events, omnitureRegStartEvent, ',', 1);
                s.t();
            }

        });
    }
});

__addToNamespace__('spark.util', {
    ajaxCallWithBlock: function (blockElemId, blockParams, ajaxFunc, ajaxUrl) {
        var bool = ajaxFunc();  //ajaxFunc will return false to indicate that ajax was not called in this case, don't block
        if (bool + '' != 'false') {
            var blockObj = (typeof (blockElemId) == 'string') ? $j('#' + blockElemId) : $j(blockElemId);
            blockObj.block(blockParams).addClass('ui-ajax-loader-sm');
            blockObj.ajaxSuccess(function (e, xhr, settings) {
                if (settings.url == ajaxUrl) {
                    blockObj.unblock().removeClass('ui-ajax-loader-sm');
                }
            }).ajaxComplete(function (e, xhr, settings) {
                if (settings.url == ajaxUrl) {
                    blockObj.unblock().removeClass('ui-ajax-loader-sm');
                }
            });
        }
        return bool;
    },
    __init__: function () { }
});
function array2json(arr) {
    var parts = [];
    var is_list = (Object.prototype.toString.apply(arr) === '[object Array]');

    for (var key in arr) {
        var value = arr[key];
        if (typeof value == "object") { //Custom handling for arrays
            if (is_list) parts.push(array2json(value)); /* :RECURSION: */
            else parts[key] = array2json(value); /* :RECURSION: */
        } else {
            var str = "";
            if (!is_list) str = '"' + key + '":';

            //Custom handling for multiple data types
            if (typeof value == "number") str += value; //Numbers
            else if (value === false) str += 'false'; //The booleans
            else if (value === true) str += 'true';
            else str += '"' + value + '"'; //All other things
            // :TODO: Is there any more datatype we should be in the lookout for? (Functions?)

            parts.push(str);
        }
    }
    var json = parts.join(",");

    if (is_list) return '[' + json + ']'; //Return numerical JSON
    return '{' + json + '}'; //Return associative JSON
}


/******* header menu - no cache for subscription image ********/
jQuery(document).ready(function () {
    var src = jQuery(".subli-with-icon img").attr("src");
    var numRand = Math.floor(Math.random() * 9999999);
    src += "?" + numRand;
    jQuery(".subli-with-icon img").attr("src", src);
});

/******* registration welcome one page reg ********/

//track start/end editing attributes vars
var trackStartEditAttribute = false;
var trackEndEditingAttributes = false;

//is already bind to save details click event
var isSaveDetailsBind = false;

jQuery(document).ready(function () {
    coverBody();
    showOverlayBox();
    var attribeCount = 0;

    //count total attributes
    jQuery(".send-attribute ul").each(function () {
        attribeCount++;
    });
    var selectedAttr = 0;

    var cityInput = jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute .txt-edit-city');
    var cityInputValue = jQuery.trim(cityInput.val());
    var zipCodeInput = jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute .txt-edit-zipcode');

    jQuery(".send-attribute ul li").click(function () {
        //track start editing attributes
        if (!trackStartEditAttribute) {
            var s = s_gi(s_account); s.linkTrackVars = 'events';
            s.linkTrackEvents = 'event57';
            s.events = 'event57';
            s.tl(true, 'o', 'Overlay complete profile - Start: Type=Counter');
            trackStartEditAttribute = true;
        }
        jQuery(".success-message").css("visibility", "hidden");
        jQuery(this).parent().children().each(function () {
            jQuery(this).removeClass("selected");
        });
        jQuery(this).addClass("selected");
        selectedAttr = 0;
        jQuery(".send-attribute ul li.selected").each(function () {
            selectedAttr++;
            if (selectedAttr >= attribeCount & cityInput.val() != '') {
                showBtn();
                return false;
            }
        });
    });

    //scroll height to 160cm
    var heightOptionsToScroll = 24;
    var liHeight = jQuery(".Height ul li:first").attr("scrollHeight");
    heightTab = liHeight * heightOptionsToScroll;
    jQuery(".Height ul").animate({ scrollTop: heightTab }, 0);

    //scroll to selected value
    jQuery(".send-attribute").each(function () {
        var i = 0;
        jQuery(this).children("ul").children("li").each(function () {
            if (jQuery(this).hasClass("selected")) {
                if (i == 1 != i == 0) {
                    jQuery(this).parent().animate({ scrollTop: 0 }, 0);
                }
                else {
                    jQuery(this).parent().animate({ scrollTop: i * liHeight }, 0);
                }
            }
            else {
                i++;
            }
        });
    });

    //FORCE PAGE OVERLAY: display the save details button when there's a value in a textbox.
    cityInput.on("keyup", function () {
        showBtnTextboxFilled(jQuery(this));
    });
    zipCodeInput.on("keyup", function () {
        showBtnTextboxFilled(jQuery(this));
    });

    //FORCE PAGE OVERLAY: if there's no selected height value, set the scrolling UL area to about 160cm
    if (!jQuery(".fill-details-box #complete-profile div.Height.send-attribute ul li").hasClass('selected')) {
        jQuery(".fill-details-box #complete-profile div.Height.send-attribute ul").animate({ scrollTop: 570 }, 0);
    }

    //FORCE PAGE OVERLAY (steps reg): if there's no values for region and city - set default values.
    if (jQuery.trim(cityInput.attr('value')) == '') {
        jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute .ddl-edit-state').val('3484012').trigger('change'); //set selected region value to the center region of Israel.
        cityInput.val('תל אביב'); //set city input text filed to Tel Aviv.
    }

    //FORCE PAGE OVERLAY: Disable enter key event 
    jQuery('.fill-details-box #complete-profile').bind('keypress keydown keyup', function (e) {        
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    //FORCE PAGE OVERLAY: init/change label position when we change countries via it's select box.
    var countryEle = jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute .txt-edit-country');
    var zipCodeLabel = jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute .lbl-edit-zipcode');
    if (countryEle.val() == '223' || countryEle.val() == '38') { //On init: when the forced overlay is alreay filled.
        zipCodeLabel.css('right', '12px');
    } else {
        zipCodeLabel.css('right', '170px');
    }
    countryEle.on('change', function () {
        if (jQuery(this).val() == '223' || jQuery(this).val() == '38') { //if USA or Candada is selected, we will change styles.
            zipCodeLabel.css('right', '12px');
        } else {
            zipCodeLabel.css('right', '170px');
        }
    });
});


function showBtnTextboxFilled(thisEle) {
    var numCategories = 8;
    var selectedAttrCount = jQuery(".send-attribute ul li.selected").length;

    if ($j('#complete-profile.circles').length > 0) { // "Circles" version of the forced page
        var numCategories = 6;
        var selectedAttrCount = jQuery(".category .send-attribute ul li.selected").length;
    }

    if (selectedAttrCount == numCategories && jQuery.trim(thisEle.val()) != '') {
            showBtn();
            return false;
    } else {
        jQuery("#send-attr-btn").removeClass("enabled");
    }    
}

function showBtn() {
    jQuery("#send-attr-btn").addClass("enabled");

    if (!isSaveDetailsBind) {
        jQuery("#send-attr-btn.enabled").click(function () {

            var cityInput = jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute .txt-edit-city');
            var cityInputValue = jQuery.trim(cityInput.val());

            jQuery("#send-attr-btn").removeClass("enabled");
            var data = "{attributesList:'[";

            jQuery(".send-attribute ul li.selected").each(function () {
                key = jQuery(this).parent().parent().attr("key");
                value = jQuery(this).attr("value");
                data += '{"Key":"' + key + '","Value":"' + value + '"},';
            });

            $othersAC = jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute .txt-edit-city');
            $israelAC = jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute #israeliRegionsInput');

            if ($israelAC.is(':visible')) {
                data += '{"Key":"cityname","Value":"' + $israelAC.val() + '"},';
            } else {
                data += '{"Key":"cityname","Value":"' + $othersAC.val() + '"},';
            }
            data += '{"Key":"countryregionid","Value":"' + jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute .txt-edit-country').val() + '"},';
            data += '{"Key":"stateregionid","Value":"' + jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute .ddl-edit-state').val() + '"},';
            data += '{"Key":"zipcode","Value":"' + jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute .txt-edit-zipcode').val() + '"},';
            data += '{"Key":"cityforzipcode","Value":"' + jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute .ddl-city-zip').val() + '"},';
            
            data = data.substring(0, data.length - 1);
            data += "]'}";

            jQuery.ajax({
                type: "POST",
                url: "/Applications/API/UpdateMember.asmx/SaveMemberAttributes",
                contentType: "application/json; charset=utf-8",
                data: data,
                datatype: "json",
                timeout: 10000,
                success: function (msg) {
                    if (typeof (msg) == "object") {
                        parsedJsonMsg = jQuery.parseJSON(msg['d']);

                        if (parsedJsonMsg.result == 'fail') {
                            jQuery('.fill-details-box #complete-profile #top-row div.Country.send-attribute .txt-edit-city').focus().val('').attr('value','');

                            jQuery('div.success-message').html(parsedJsonMsg.message);
                            jQuery('div.success-message').css('visibility', 'visible');
                            return;
                        } else {
                            jQuery('div.success-message').html('הפרטים נשמרו בהצלחה');
                        }

                        jQuery("#leftbox-1").addClass("enabled");
                        jQuery("#leftbox-2").addClass("enabled");

                        //disable links
                        jQuery(".disabled-matches-btn").hide();
                        jQuery(".enabled-matches-btn").show();
                        jQuery(".success-message").css("visibility", "visible");
                        jQuery(".box-content").css({
                            opacity: 1
                        });
                        jQuery("#send-attr-btn").addClass("enabled");
                        jQuery("#send-attr-btn").addClass("enabled");
                        if (jQuery('.fill-details-box #profile-overlay-content').length == 0 && jQuery('#one-page-reg-welcome-container #page-content').length == 0) {
                            setTimeout(function () { closeOverlay() }, 2000);

                            var addedParams;
                            var params = getUrlParams(window.location.href);
                            var locationHref = decodeURIComponent(window.location.href);
                            paramsQS = params == null ? '?' : '&'; //query string - add or start params 

                            //remove doneForce=0 from query string.
                            if (locationHref.indexOf('doneForce=0') > -1) {
                                if (locationHref.indexOf('&doneForce=0') > -1) {
                                    locationHref = locationHref.replace('&doneForce=0', '');
                                } else {
                                    locationHref = locationHref.replace('doneForce=0', '');
                                }
                            }
                           
                            window.location.href = locationHref + paramsQS + "doneForce=1"; // `doneForce` param was added to avoid the firing of GTM events more than once and can be used for other uses as well.
                        }

                        //track end editing attributes
                        if (!trackEndEditingAttributes) {
                            var s = s_gi(s_account); s.linkTrackVars = 'events';
                            s.linkTrackEvents = 'event58';
                            s.events = 'event58';
                            s.tl(true, 'o', 'Overlay complete profile - Complete: Type=Counter');
                            trackEndEditingAttributes = true;
                        }
                    }
                }
            });
        });
        isSaveDetailsBind = true;
    }
}

function coverBody() {
    if ($j.browser.msie && parseInt($j.browser.version) <= "8") {
        jQuery('.bg-cover-fill-details').css({ opacity: 0.5, backgroundColor: '#888888' });
    }
    else {
        jQuery('.bg-cover-fill-details').css({ opacity: 0.5, backgroundColor: '#000' });
    }

}
function showOverlayBox() {
    //if box is not set to open then don't do anything
    // set the properties of the overlay box, the left and top positions
    jQuery('.fill-details-box').has('#profile-overlay-content').css({
        background: 'none',
        width: 1000
    });
    topToBePositive = (jQuery(window).height() - jQuery('.fill-details-box').height()) / 2 - 20
    if (topToBePositive < 0) { topToBePositive = 0 }
    jQuery('.fill-details-box').css({
        display: 'block',
        left: (jQuery(window).width() - jQuery('.fill-details-box').width()) / 2,
        top: topToBePositive,
        position: 'absolute'
    });
    // set the window background for the overlay. i.e the body becomes darker
    jQuery('.bg-cover-fill-details').css({
        display: 'block',
        width: jQuery(document).width(),
        height: jQuery(document).height()
    });
}
function closeOverlay() {
    jQuery('.fill-details-box').css('display', 'none');
    // now animate the background to fade out to opacity 0
    // and then hide it after the animation is complete.
    jQuery('.bg-cover-fill-details').animate({ opacity: 0 }, null, null, function () { jQuery(this).hide(); });
}
/******************************/

/********* Quick Message ************/
var mainSubject;
var mainBody;
var defaultLoaderHtml;
var trackInitiateQuickMessage = new Object;
trackInitiateQuickMessage.openQuickMessage = [];
trackInitiateQuickMessage.startQuickMessage = [];
var quickMessage = [];
var alreadyOpen = new Object();
var mainTimer = [];
jQuery(document).ready(function () {
    mainSubject = jQuery(".quick-message-subject").val();
    mainBody = jQuery(".quick-message-body").val();
    defaultLoaderHtml = jQuery(".send-message-loading").html();
    jQuery(".close").click(function () {
        jQuery(this).parent().slideUp();
    });

    //open/close quick message box
    jQuery(".open-quick-message").click(function () {
        var memberid = jQuery(this).attr("memberid");

        clearSubjectAndBody(memberid);

        quickMessage[memberid] = jQuery(".quick-message[memberid='" + memberid + "']");

        jQuery(quickMessage[memberid]).slideDown();
        jQuery(this).unbind('mouseout.myHover');

        //track initiate quick message
        if (trackInitiateQuickMessage.openQuickMessage[memberid] == undefined) {
            trackInitiateQuickMessage.openQuickMessage[memberid] = false;
        }
        if (trackInitiateQuickMessage.startQuickMessage[memberid] == undefined) {
            trackInitiateQuickMessage.startQuickMessage[memberid] = false;
        }
        if (trackInitiateQuickMessage.openQuickMessage[memberid] == false) {
            var s = s_gi(s_account); s.linkTrackVars = 'events';
            s.linkTrackEvents = 'event60';
            s.events = 'event60';
            s.tl(true, 'o', 'click to Initiate Quick Message -IL: Type=Counter');
            trackInitiateQuickMessage.openQuickMessage[memberid] = true;
            clearTracking(trackInitiateQuickMessage.startQuickMessage, memberid);
        }

    });
    jQuery(".open-quick-message").bind('mouseover.myHover', function () {
        var memberid = jQuery(this).attr("memberid");

        quickMessage[memberid] = jQuery(".quick-message[memberid='" + memberid + "']");
        if (!jQuery(quickMessage[memberid]).is(":visible")) {
            if (mainTimer[memberid]) {
                clearTimeout(mainTimer[memberid]);
                mainTimer[memberid] = null;
            }
            mainTimer[memberid] = setTimeout(function () {
                jQuery(quickMessage[memberid]).slideDown();
            }, 500);
            clearSubjectAndBody(memberid);
            jQuery(".open-quick-message[memberid='" + memberid + "']").bind('mouseout.myHover', function () {

                quickMessage[memberid] = jQuery(".quick-message[memberid='" + memberid + "']");
                if (mainTimer[memberid]) {
                    clearTimeout(mainTimer[memberid]);
                    mainTimer[memberid] = null;
                }
                mainTimer[memberid] = setTimeout(function () {
                    jQuery(quickMessage[memberid]).slideUp();
                }, 750);
            });
        }
    });


    //Mouse hover on quick message box
    jQuery(".quick-message").hover(function () {
        var memberid = jQuery(this).attr("memberid");

        if (mainTimer[memberid]) {
            clearTimeout(mainTimer[memberid]);
            mainTimer[memberid] = null;
        }

        jQuery(".open-quick-message[memberid='" + memberid + "']").unbind('mouseout.myHover');

    }, function () {
        var memberid = jQuery(this).attr("memberid");


        var subject = jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-subject").val();
        var message = jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-body").val();
        if (subject == mainSubject && message == mainBody) {
            if (mainTimer[memberid]) {
                clearTimeout(mainTimer[memberid]);
                mainTimer[memberid] = null;
            }
            mainTimer[memberid] = setTimeout(function () {
                jQuery(".quick-message[memberid='" + memberid + "']").slideUp();
            }, 750);
        }
    });



    //send message
    jQuery(".send-message").click(function () {
        if (!validateQuickMessageForm(this)) {
            jQuery(this).parent().hide();
            var memberid = jQuery(this).parent().attr("memberid");
            jQuery(".send-message-loading[memberid='" + memberid + "']").show();

            var subject = jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-subject").val();
            var message = jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-body").val();
            var data = '{"targetMemberID":"' + memberid + '","messageSubject":"' + subject + '","message":"' + message + '","profileType":"' + quickMessageProfileType + '"}';

            jQuery.ajax({
                type: "POST",
                url: "/Applications/API/IMailService.asmx/SendMessageLocale",
                contentType: "application/json; charset=utf-8",
                data: data,
                datatype: "json",
                timeout: 30000,
                success: function (msg) {
                    var s = s_gi(s_account); s.linkTrackVars = 'events';
                    s.linkTrackEvents = 'event11';
                    s.events = 'event11';
                    s.eVar26 = "imail";
                    s.tl();

                    clearTracking(trackInitiateQuickMessage.openQuickMessage, memberid);
                    clearTracking(trackInitiateQuickMessage.startQuickMessage, memberid);
                    if (typeof (msg) == "object") {
                        var resultStatus = (typeof msg.d == 'undefined') ? msg.Status : msg.d.Status;

                        if (resultStatus == 2) {
                            showSendMessage(msg.d.StatusMessage, memberid);
                        }
                        else if (resultStatus == 4) {
                            window.location = msg.d.SubPageURL;
                        }
                        else if (resultStatus == 5) {
                            //session expired
                            window.location.href = '/Applications/Logon/Logon.aspx?DestinationURL=' + encodeURIComponent(window.location.href.replace('http://' + window.location.host, ''));
                        }
                        else {
                            generalError();
                        }
                    }
                    else {
                        generalError();
                    }
                },
                error: function () {
                    generalError();
                }
            });

        }
    });


    //clear subject/body values
    jQuery(".quick-message-subject,.quick-message-body").bind("click focus", function () {
        jQuery(this).removeClass("error");
        var cls = jQuery(this).attr("class");
        if (jQuery(this).val() == mainSubject || jQuery(this).val() == mainBody) {
            jQuery(this).val("");
        }
        for (key in quickMessageValidator[cls]) {
            if (quickMessageValidator[cls][key].message == jQuery(this).val()) {
                jQuery(this).val("");
            }
        }


        jQuery(this).addClass("black");
        var memberid = jQuery(this).parent().parent().attr("memberid");
        //track start typing message
        if (trackInitiateQuickMessage.startQuickMessage[memberid] == undefined) {
            trackInitiateQuickMessage.startQuickMessage[memberid] = false;
        }
        if (trackInitiateQuickMessage.startQuickMessage[memberid] == false) {
            var s = s_gi(s_account); s.linkTrackVars = 'events';
            s.linkTrackEvents = 'event61';
            s.events = 'event61';
            s.tl(true, 'o', 'Quick Message –Start- IL : Type=Counter');
            trackInitiateQuickMessage.startQuickMessage[memberid] = true;
        }
    });
});

function showSendMessage(msg, memberid) {
    jQuery(".send-message-loading[memberid='" + memberid + "']").html(msg)
    timer = setTimeout(function () {
        jQuery(".send-message-loading[memberid='" + memberid + "']").fadeOut();
        timer2 = setTimeout(function () {
            jQuery(".send-message-loading[memberid='" + memberid + "']").html(defaultLoaderHtml);
        }, 5000);
    }, 5000);
}

function validateQuickMessageForm(elem) {
    var elementErrorFlag = false;
    jQuery(elem).parent().find("input,textarea").each(function () {
        if (elementErrorFlag) {
            return elementErrorFlag;
        }
        jQuery(this).removeClass("error");
        jQuery(this).removeClass("black");
        var cls = jQuery(this).attr("class");
        if (quickMessageValidator[cls]) {
            value = jQuery(this).val();
            for (key in quickMessageValidator[cls]) {
                switch (quickMessageValidator[cls][key].errorKey) {
                    case "required":

                        if (value == "" || value.length == 0 || value == " " || value == quickMessageValidator[cls][key].message) {
                            elementErrorFlag = true;
                            jQuery(this).val(quickMessageValidator[cls][key].message);
                            jQuery(this).addClass("error");
                        }
                        break;
                    case "noChange":
                        if (value == mainSubject || value == mainBody || value == quickMessageValidator[cls][key].message) {
                            elementErrorFlag = true;
                            jQuery(this).val(quickMessageValidator[cls][key].message);
                            jQuery(this).addClass("error");
                        }
                        break;
                }
                if (elementErrorFlag) {
                    break;
                }
            }
        }

    });

    return elementErrorFlag;
}

function clearSubjectAndBody(memberid) {
    var val = jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-subject").val();
    if (val != mainSubject) {
        jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-subject").val(mainSubject);
        jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-subject").removeClass("black");
    }
    val = jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-body").val();
    if (val != mainBody) {
        jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-body").val(mainBody);
        jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-body").removeClass("black");
    }
}

function generalError() {
    jQuery(".send-message-loading").addClass("error");
    jQuery(".send-message-loading").html(quickMessageGeneralError);
    timer = setTimeout(function () {
        jQuery(".send-message-loading").fadeOut();
        timer2 = setTimeout(function () {
            jQuery(".send-message-loading").html(defaultLoaderHtml);
        }, 5000);
    }, 5000);
}

//clear trackong flag
function clearTracking(obj, memberid) {
    obj[memberid] = false;
}

/******* Bubble popup ********/
jQuery(function () {
    jQuery('.bubble-area').each(function () {
        // options
        var distance = 10;
        var time = 300;
        var hideDelay = 500;

        var hideDelayTimer = null;
        var triggerOnClick = jQuery(this).hasClass('trigger-on-click');

        // tracker
        var beingShown = false;
        var shown = false;

        var trigger = jQuery('.trigger', this);
        var popup = jQuery('.bubble-layer', this).css('display', 'none');

        if (triggerOnClick === true) {
            popup.append('<span class="spr s-icon-close"><span></span></span>');
            popup.children('.s-icon-close', this).click(function () {
                trigger.trigger('click');
            })
            trigger.toggle(function () {
                jQuery(this).closest('.profile30-comm').addClass('poped');
                popup.css({
                    bottom: 25,
                    left: -100,
                    display: 'block'
                });
            }, function () {
                jQuery(this).closest('.profile30-comm').removeClass('poped');
                popup.css('display', 'none');
            });
        }
        if (triggerOnClick === false) {
            // set the mouseover and mouseout on both element
            jQuery([trigger.get(0), popup.get(0)]).hoverIntent(function () {
                // stops the hide event if we move from the trigger to the popup element
                if (hideDelayTimer) clearTimeout(hideDelayTimer);

                // don't trigger the animation again if we're being shown, or already visible
                if (beingShown || shown) {
                    return;
                } else {
                    beingShown = true;

                    // reset position of popup box
                    popup.css({
                        bottom: 50,
                        left: -100,
                        display: 'block' // brings the popup back in to view
                    })

                    // (we're using chaining on the popup) now animate it's opacity and position
        .animate({
            bottom: '-=' + distance + 'px',
            opacity: 1
        }, time, 'swing', function () {
            // once the animation is complete, set the tracker variables
            beingShown = false;
            shown = true;
        });
                }
            },
	function () {
	    // reset the timer if we get fired again - avoids double animations
	    if (hideDelayTimer) clearTimeout(hideDelayTimer);

	    // store the timer so that it can be cleared in the mouseover if required
	    hideDelayTimer = setTimeout(function () {
	        hideDelayTimer = null;
	        popup.animate({
	            bottom: '-=' + distance + 'px',
	            opacity: 0
	        }, time, 'swing', function () {
	            // once the animate is complete, set the tracker variables
	            shown = false;
	            // hide the popup entirely after the effect (opacity alone doesn't do the job)
	            popup.css('display', 'none');
	        });
	    }, hideDelay);
	});
        }

    });
});

function deviceIsiPhone() {
    if (navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPhone/i)) {
        return true;
    }
    return false;
}

/******* Prompt for Switching to Mobile ********/
if (screen.width <= 699 || deviceIsiPhone() || navigator.userAgent.match(/Android/i)) {

    function inString(pages, value) {
        var result = false;
        if (typeof value === 'string') {
            value = value.split();
        }

        for (var i = '0'; i < value.length; i++) {
            if (pages.indexOf(value[i]) != -1) {
                result = true;
            }
        }
        return result;
    }

    function addMobileHeaderLink(link, copy) {
        var linkToMobile = $j('<a id="linkToMobile">');
        var offset = 0;

        linkToMobile.attr('href', link).text(copy).prependTo('body');
        $j('body').css('background-position', '0 ' + linkToMobile.outerHeight() + 'px');
        $j('#site-container').css('margin-top', linkToMobile.outerHeight() - offset);
        /*window.onscroll = function () {
        document.getElementById('linkToMobile').style.top = (window.pageYOffset) + 'px';
        };*/
    }

    function addMobileFooterLinks(links) {
        var mobileLinksUl = '<ul class="nav-footer" id="footer-mobile-nav">\n';
        for (var i = 0, len = links.length; i < len; i++) {
            var linkData = links[i];
            var htmlLink = '<li><a href="' + linkData.url + '"';
            if (linkData.target) {
                htmlLink += ' target="' + linkData.target + '"';
            }
            htmlLink += '>' + linkData.text + '</a>';
            if (i + 1 < len) {
                htmlLink += ' | ';
            }
            mobileLinksUl += htmlLink;
        }
        mobileLinksUl += '</ul>\n';
        $j("#footer > div").prepend(mobileLinksUl);
    }

    function MakeMobileVisitToFWSTemporary() {
        // for existing cookies with expiration date of a year from session, clear expire date, so cookie becomes session only
        // this code can be removed after March 2013 when all cookies will have been cleared out
        // starting march 2012, no more year-long cookies were set, only session cookies
        var cookieName = 'sofs'; // stay on full site (from mobile device)
        var sofsValue = spark_getCookie(cookieName);
        if (sofsValue) {
            var site = $j('meta[name="author"]').attr('content') || "";
            site = site.toLowerCase();
            var siteDomain = site.replace("http://www.", "");
            document.cookie = cookieName + '=' + escape(sofsValue) + ';expires=;path=/;domain=.' + siteDomain;
        }
    }

    jQuery(document).ready(function () {
        var site = $j('meta[name="author"]').attr('content') || "";
        site = site.toLowerCase();
        var siteDomain = site.replace("http://www.", "");
        if (site == 'http://www.jdate.co.il') {

            var pages = document.getElementsByTagName('body')[0].className;
            var pageTest = inString(pages, ['sub-page-default', 'page-logon', 'sub-page-splash20', 'page-home']);
            var onSplashOrLoginPage = inString(pages, ['sub-page-splash20', 'page-logon']);
            if (pageTest) { //see if the page is sub-page page logon or sub-page splash
                MakeMobileVisitToFWSTemporary();
                if (onSplashOrLoginPage) { // make sure it's not the home page and show the nav
                    addMobileHeaderLink('http://m.' + siteDomain, 'עבור לאתר המותאם לטלפון נייד');
                }
            }
            var links = [
			{
			    'text': 'עבור לאתר המותאם לטלפון נייד',
			    'url': 'http://m.' + siteDomain
			}];

            //            if (site == 'http://www.jdate.com') {
            //                links.push({
            //                    'text': 'JDate Mobile',
            //                    'url': 'http://www.jdatemobile.com'
            //                });

            //                if (deviceIsiPhone()) {
            //                    links.push({
            //                        'text': 'Download JDate iPhone App',
            //                        'url': 'http://itunes.apple.com/us/app/jdate/id484715536?mt=8&uo=4',
            //                        'target': 'itunes_store'
            //                    });
            //                }
            //            }

            addMobileFooterLinks(links);

        } else {
            return;
        }



    });
}

function spark_launchTeaseDialog(obj, title, close) {
    var href = obj.href;

    if ($j('#flirtContent').length === 0) {
        $j('<div id="flirtContent" class="hide"></div>').appendTo('body');
    }

    $j('#flirtContent')
        .dialog({
            width: 590,
            modal: true,
            title: title,
            position: ['center', 'top'],
            dialogClass: 'ui-dialog-rev modal-flirts',
            minHeight: 567,
            open: function () {
                var $this = $j(this),
                    ajax_load = '<div class="loading spinner-only" />',
                    reversedClose = $j('<div class="ui-dialog-titlebar-close-rev link-style">' + close + ' <span class="spr s-icon-closethick-color"></span></div>').bind('click', function () {
                        $j('#flirtContent').dialog('close');
                    });

                $this.html(ajax_load).load(href, function () {
                    $j(this).find('.category:eq(0)').addClass('open');
                });

                $this.parent().find('.ui-dialog-titlebar-close').replaceWith(reversedClose);
                $j('.ui-widget-overlay').bind('click', function () {
                    $j('#flirtContent').dialog('close');
                });
            }
        })
        .parent('.ui-dialog').css('top', function () {
            var x = $j(this).position();
            return x.top + 12;
        });

    //return false;
}

jQuery(document).ready(function () {
    jQuery('.overlayBox .reg-overlay-osr').parents('.overlayBox').addClass('osr-cont');
    jQuery('.overlayBox.osr-cont #progressbar').remove();
    jQuery('.overlayBox.osr-cont .JDateReligion').next().css({ minHeight: '74px' });
    jQuery('.overlayBox.osr-cont #overlay-error').remove();
});

//replace image when hover on date online
jQuery(document).ready(function () {
    jQuery('.results30-profile.gallery-view-30 .hover-menu li.action-omnidate, .results30-profile.list-view-30 .profile-body li.action-omnidate').hover(function () {
        var newSrc = jQuery(this).find('.item.omnidate img').attr('src');
        newSrc = newSrc.replace('30-s', '30-h-s');
        jQuery(this).find('.item.omnidate a img').attr('src', newSrc);
    }, function () {
        var newSrc = jQuery(this).find('.item.omnidate img').attr('src');
        newSrc = newSrc.replace('30-h-s', '30-s');
        jQuery(this).find('.item.omnidate a img').attr('src', newSrc);
    });
});

jQuery(document).ready(function() {
    jQuery(window).has('#profile-overlay-content').resize(function() {
        showOverlayBox();
    });
});

// Subscription Expiration Alert
function displayAnnouncementLoader(show) {
    if (show) {
        jQuery('.content-announcement .announcement-loader').css({
            height: jQuery('.content-announcement .welcome-box').height()+8
        }).show()
    }
    else {
        jQuery('.content-announcement .announcement-loader').hide();
    }
}

function announcementDetails() {
    jQuery('.content-announcement > div').hide();
    jQuery('.content-announcement .thankyou-box').show();
}

function announcementError() {
    jQuery('.content-announcement > div').hide();
    jQuery('.content-announcement .error-box').show();
}

jQuery(document).ready(function () {
    jQuery('.content-announcement a.click-close').click(function (e) {
        e.preventDefault();
        jQuery(this).parents('.content-announcement').slideUp();
    });
});

jQuery(".content-announcement .btn-renewal a").live('click', function (e) {
    e.preventDefault();
    var memberid = spark.favorites.MemberId;
    //var memberid = -2012;
    var data = '{"memberID":"' + memberid + '"}';

    jQuery.ajax({
        type: "POST",
        url: "/Applications/API/AutoRenewal.asmx/EnableAutoRenewal?callback=?",
        contentType: "application/json; charset=utf-8",
        data: data,
        datatype: "json",
        timeout: 30000,
        beforeSend: function () {
            displayAnnouncementLoader(true);
        },
        success: function (msg) {
            displayAnnouncementLoader(false);
            if (typeof (msg) == "object") {
                if (msg.d.Status == 2) {
                    s.prop75 = 'autorenew_on';
                    s.t();
                    jQuery('.content-announcement .renewal-amount').text(msg.d.RenewalAmount);
                    jQuery('.content-announcement .renewal-date').text(msg.d.NextRenewalDate);
                    jQuery('.content-announcement .renewal-total').text(msg.d.TotalAmount);
                    if (typeof (msg.d.PremiumServicesList) == "object" && msg.d.PremiumServicesList != null) {
                        jQuery.each(msg.d.PremiumServicesList, function (i, item) {
                            jQuery('<span>' + msg.d.PremiumServicesList[i].ServiceName + ' יתחדש בסכום: ' + msg.d.PremiumServicesList[i].RenewalAmount + ' ₪ לחודש</span><br />').appendTo('.content-announcement .renewal-services');
                        });
                    }
                    announcementDetails();
                }
                else if (msg.d.Status == 3) {
                    announcementError();
                }
                else {
                    generalError();
                }
            }
            else {
                generalError();
            }
        },
        error: function () {
            displayAnnouncementLoader(false);
            generalError();
        }
    });
});

// show alert when message is empty on message reply
jQuery(document).ready(function () {
    if (jQuery('.reply-box.compose-email .compose-email-content').length > 0) {
        var textMarkText = jQuery('.compose-email-content textarea').val();
        jQuery('.compose-email-content input.btn.primary').live('click', function (e) {
            var textMessageArea = jQuery('.compose-email-content textarea');
            if (textMessageArea.val() == textMarkText || jQuery.trim(textMessageArea.val()) == '') {
                e.preventDefault();
                jQuery('#emailEmpty').css({
                    width: textMessageArea.outerWidth(),
                    height: textMessageArea.outerHeight() / 2 + 10,
                    paddingTop: textMessageArea.outerHeight() / 2 - 10,
                    display: 'block'
                }).delay('2000').fadeOut('slow')
            }
        });
    }
});

// ie7 footer links fix
jQuery(document).ready(function () {
    jQuery('.ie7 #footer-links-main  .four-column li:empty, .ie7 #footer-links-main  .four-column li:nth-child(8)').hide();
});

// fix for peek banner and for none top banner
jQuery(document).ready(function () {
    if (jQuery('.plcPeekBnrFlash > .adunit > div').length == 0) {
        jQuery('.plcPeekBnrFlash').remove();
    }
    if (jQuery('.page-home').length > 0) {
        if (jQuery('.adspace_header > div').length == 0) {
            if (jQuery('div[id*="728x90"]').length == 0) {
                //jQuery('ul#nav-auxiliary').css({ marginTop: '70px' });
                jQuery('#header-content').css({ minHeight: '76px', height: '76px' });
                jQuery('#header-logo').css({ marginTop: '25px' });
                jQuery('.header-fb-like-btn').css({ top: '14px' });
                jQuery('.header-googleplus-btn').css({ top: '39px' });
                jQuery('.twitter-follow-button').css({ top: '39px' });
                //jQuery('.header-container').css({ backgroundPosition: '1000px 29px' });
                jQuery('.adspace_header').hide();
            }
        } 
        if (jQuery('.adspace_header').css('display') == 'none') {
            jQuery('#header-content').css({ minHeight: '116px', height: '116px' });
        }
    }
});

// Top menu nav link JExperts Clalit כללית אסתטיקה - make it open in a new tab
jQuery(document).ready(function () {
    $topNavJExperts = jQuery('#nav .jexperts');
    if ($topNavJExperts.length > 0) {
        $topNavJExperts.find('.menu-item-sub li[class*=כללית-אסתטיקה] a').attr('target', '_blank');
    }
});

jQuery(window).load(function () {
    /*
    * Header: collpase when DFP banner is hidden.
    */
    if (jQuery('.adspace_header').css('display') == 'none' || jQuery('.adspace_header > div').css('display') == 'none') {
        jQuery('.adspace_header').hide();
        jQuery('#header-content #header-nav').css({ position: 'absolute', left: '0', top: '52px' });
        jQuery('.header-fb-like-btn').css({ top: '0' });
        jQuery('.header-container').css({ 'background-position': '1000px 5px' });
        jQuery('.header-googleplus-btn').css({ top: '44px' });
        jQuery('.twitter-follow-button').css({ top: '19px' });
        jQuery('#header-content').css({ 'min-height': '66px' });
        jQuery('#header-logo').css({ 'margin-top': '0' });        
    }
});

// JMeter V3
function displayBoxLoader(show, boxType) {
    if (show) {
        boxType.find('.jmeter-box-matches-profiles-run').css({
            opacity:0.1
        });
        boxType.find('.jmeter-box-matches-loader').show()
    }
    else {
        boxType.find('.jmeter-box-matches-loader').hide();
        boxType.find('.jmeter-box-matches-profiles-run').css({
            opacity:1
        });
    }
}

function moveBoxLeft(boxType) {
    var pr = boxType.find('.jmeter-box-matches-profiles-run');
    pr.animate({
        right: (parseFloat(pr.css('right')) - 228) + 'px'
    }, 600, function () {
        activateBoxButton('right', true, boxType);
        if (boxType.find('.jmeter-box-matches-profiles-run .mb-container.current.last').length == 0) { activateBoxButton('left', true, boxType); }
    });
}

function moveBoxStart(boxType) {
    boxType.find('.jmeter-box-arrow-button').addClass('disable');
    var pr = boxType.find('.jmeter-box-matches-profiles-run');
    pr.animate({
        right: 0
    }, 600, function () {
        activateBoxButton('left', true, boxType);
        activateBoxButton('right', false, boxType);
    });
}

function addBoxContainer(boxType, callFirst) {
    if (callFirst) {
        boxType.find('.jmeter-box-matches-profiles-run .mb-container').removeClass('last current');
        boxType.find('.jmeter-box-matches-profiles-run .mb-container:first-child').addClass('current');
    } else {
        boxType.find('.jmeter-box-matches-profiles-run .mb-container.last').removeClass('last');
    }
    boxType.find('.jmeter-box-matches-profiles-run').append('<div class="mb-container last"></div>');
}

function showNoResultsBox(show, boxType) {
    if (show) {
        displayBoxLoader(false, boxType);
        boxType.find('.jmeter-box-matches-profiles .jmeter-box-error').show();
    }
    else {
        boxType.find('.jmeter-box-matches-profiles .jmeter-box-error').hide();
    }
}

function activateBoxButton(direction, active, boxType) {
    if (active) {
        boxType.find('.jmeter-box-arrow-button#matches-' + direction).removeClass('disable');
    }
    else {
        boxType.find('.jmeter-box-arrow-button#matches-' + direction).addClass('disable');
    }
}

function moveBoxRight(boxType) {
    var pr = boxType.find('.jmeter-box-matches-profiles-run');
    boxType.find('.jmeter-box-matches-profiles-run .mb-container.current').removeClass('current').prev().addClass('current');
    pr.animate({
        right: (parseFloat(pr.css('right')) + 228) + 'px'
    }, 600, function () {
        activateBoxButton('left', true, boxType);
        if (parseInt(pr.css('right')) < 0) {
            activateBoxButton('right', true, boxType);
        }
    });
}

function getOldMatchesProfiles(boxType) {
    moveBoxRight(boxType);
}

function getNewMatchesProfiles(isFirst, countCalls, boxType) {
    if (boxType.is('.matches')) {
        var profilesCount = 8;
        var useAPI = 'GetJMeterMatches';
    } else
    {
        var profilesCount = 12;
        var useAPI = 'GetJMeterMOL';  
    }
    showNoResultsBox(false, boxType);
    var isNewProfilesAjax = false;
    var profilesStartFrom = boxType.find('.jmeter-box-matches-profiles-run .mb-profile').length+1;
    if (boxType.find('.jmeter-box-matches-profiles-run .mb-container').eq(-3).is('.current') || countCalls > 1 || isFirst || !boxType.find('.jmeter-box-matches-profiles-run.scroll-end')) {
        isNewProfilesAjax = true;
    }
    if (isFirst) {
        profilesCount = profilesCount*3;
        var isRunning = false;
        var profilesStartFrom = 1;
    }
    if (isNewProfilesAjax) {
        var data = '{"startRow":"' + profilesStartFrom + '", "pageSize":"' + profilesCount + '"}';
        jQuery.ajax({
            type: "POST",
            url: "/Applications/API/SearchResults.asmx/" + useAPI,
            contentType: "application/json; charset=utf-8",
            data: data,
            datatype: "json",
            timeout: 30000,
            beforeSend: function () {
                if (isFirst) {
                    displayBoxLoader(true, boxType);
                } else {
                    boxType.find('.jmeter-box-matches-profiles-run .mb-container.current').removeClass('current').next().addClass('current');
                    moveBoxLeft(boxType);
                }
            },
            success: function (msg) {
                if (typeof (msg) == "object") {
                    if (msg.d.Status == 2) {
                        if (typeof (msg.d.ProfilesList) == "object" && msg.d.ProfilesList != null && !jQuery.isEmptyObject(msg.d.ProfilesList) && jQuery.trim(msg.d.ProfilesList) != '') {
                            addBoxContainer(boxType, false);
                            var increaseContainerWidth = 228;
                            jQuery.each(msg.d.ProfilesList, function (i, item) {
                                if (isFirst && i < profilesCount && i > 0 && i % (profilesCount / 3) == 0) { addBoxContainer(boxType, true) }
                                boxType.find('.mb-container.last').append('<div class="mb-profile"><a href="' + msg.d.ProfilesList[i].Link + '"><img alt="UserName" src="' + msg.d.ProfilesList[i].ThumbnailURL + '" /></a></div>');
                            });
                            if (isFirst) {
                                activateBoxButton('left', true, boxType);
                                increaseContainerWidth *= 2;
                            }
                            boxType.find('.jmeter-box-matches-profiles-run').width(function (i, width) {
                                return width + increaseContainerWidth;
                            });
                            isRunning = true;
                            displayBoxLoader(false, boxType);
                        } else {
                            boxType.find('.jmeter-box-matches-profiles-run').addClass('scroll-end');
                        }
                    }
                    else if (msg.d.Status == 3) {
                        if (isRunning) {
                            moveBoxLeft(boxType);
                            activateBoxButton('left', false, boxType);
                            isRunning = false;
                        }
                        else {
                            if (countCalls >= 5) {
                                if (!isFirst) {
                                    moveBoxStart(boxType);
                                    showNoResultsBox(true, boxType);
                                } else {
                                    showNoResultsBox(true, boxType);
                                }
                            }
                            else {
                                countCalls++;
                                //setTimeout(function () { getNewMatchesProfiles(isFirst, countCalls, boxType) }, 2000);
                            }
                        }
                    }
                    else {
                        showNoResultsBox(true, boxType);
                    }
                }
                else {
                    showNoResultsBox(true, boxType);
                }
            },
            error: function () {
                showNoResultsBox(true, boxType);
            }
        });
    }
    else {
        boxType.find('.jmeter-box-matches-profiles-run .mb-container.current').removeClass('current').next().addClass('current');
        moveBoxLeft(boxType);
    }
}

function increasePercentage(perc) {
    jQuery('.bar').show()
	var clockBarPerc = jQuery('.bar').attr('src').replace('.png','');
	var pos = clockBarPerc.lastIndexOf('/');
	newClockBarPerc = parseInt(clockBarPerc.substring(pos+1));
	if(newClockBarPerc==perc) {
	    return false;
	} else {
		jQuery('.bar').attr('src', clockBarPerc.substring(0,pos+1)+''+parseInt(newClockBarPerc+1)+'.png');
	}
}

function preloadClock() {
    jQuery('.bar').attr('src', '/img/site/jdate-co-il/clock/1.png').hide(); ;
    if (jQuery('.lte8').length == 0) {
        jQuery('.pan').css({ rotate: -2.25 });
        for (i = 0; i <= 7; i++) {
            jQuery('<img>').attr('src', '/img/site/jdate-co-il/clock/' + i + '.png').appendTo('body').hide();
            if (i == 7) {
                jQuery('.jmeter-clock > .loader').delay(3000).fadeOut(1500, function () {
                    runClock();
                });
            }
        }
    } else {
        jQuery('.jmeter-clock > .loader').fadeOut(1500);
        runClock();
    }
}

function runClock() {
    var percToPan = jQuery('.jmeter-clock > input').val();
    var valueRatio = 100 / 7;
    percToPan = percToPan / valueRatio;
    if (jQuery('.jmeter-clock').is('.personal, .mini')) {
        percToPan = jQuery('.jmeter-clock > input').val();
    }
    var percToBar = parseInt(Math.round(percToPan));
    if (percToBar == 1) { rotateRadius = -1.64; }
    if (percToBar == 2) { rotateRadius = -1; }
    if (percToBar == 3) { rotateRadius = -0.4; }
    if (percToBar == 4) { rotateRadius = 0.4; }
    if (percToBar == 5) { rotateRadius = 1; }
    if (percToBar == 6) { rotateRadius = 1.64; }
    if (percToBar == 7) { rotateRadius = 2.25; }
    /*
    var maxRadius = 4.5;
    var rotateRadius = parseFloat((maxRadius * percToPan) / 100);
    if (rotateRadius > maxRadius / 2) {
    var fixPositive = ((((100 - percToPan) / 100) * 40) / 100)
    rotateRadius = (rotateRadius - (maxRadius / 2)) + fixPositive;
    } else {
    if (rotateRadius == maxRadius / 2) {
    rotateRadius = 0
    } else {
    rotateRadius = -((maxRadius / 2 - rotateRadius) + ((rotateRadius * 10) / 100))
    }
    }
    */
    var clockInt = setInterval(function () { increasePercentage(percToBar) }, 100);
    var panSpeed = 3000 / percToBar;
    if (jQuery('.lte8').length == 0) {
        jQuery('.pan').animate({ rotate: rotateRadius }, panSpeed, 'linear');
    } else {
        jQuery('.pan').delay(2000).css({ rotate: rotateRadius }, panSpeed, 'linear');
    }   
}

function slideShowMad() {
    var scoreIncrease;
    jQuery('.jmeter-mad').each(function () {
        scoreIncrease = 46.4285;
        if (jQuery(this).is('.small')) { scoreIncrease = 14.14285 }
        if (jQuery(this).is('.medium')) { scoreIncrease = 16.41428 }
        if (jQuery('#tab-JMeter').length > 0) { scoreIncrease = 9.85714 }
        var thisScore = jQuery(this).find('input').val();
        var madToPixels = Math.round(parseInt(thisScore) * scoreIncrease);
        jQuery(this).find('.mad-main').animate({
            width: madToPixels
        }, 3000);
    });
}

function placeMatchOnboxes() {
    jQuery('.details-boxes').each(function () {
        var user_score = jQuery(this).find('input.user-score').val();
        var userX = user_score.substring(0, user_score.indexOf('.')) * 2;
        var userY = 200 - (user_score.substring(user_score.indexOf('.') + 1, user_score.length) * 2);

        jQuery(this).find('img.dot-user').css({
            top: userY,
            left: userX
        });
    });
}

function jmeterDimensions() {
    if (jQuery('#jmeter-side').height() < jQuery('.jmeter-content').outerHeight()) {
        jQuery('#jmeter-side').css({ height: jQuery('.jmeter-content').outerHeight() });
    } else {
        //jQuery('.jmeter-content').css({ height: jQuery('#jmeter-side').height() });
    }
}

jQuery(document).ready(function () {
    // JMeter left boxes
    jQuery('.jmeter-left-box .jmeter-box-arrow-button').addClass('disable');
    jQuery('.jmeter-left-box').each(function () {
        var boxHeight = jQuery(this).find('.jmeter-box-matches-profiles').height();
        jQuery(this).find('.jmeter-box-matches-loader, .jmeter-box-error').css({
            height: boxHeight / 2 + 10,
            paddingTop: boxHeight / 2 - 20
        });
        getNewMatchesProfiles(true, 1, jQuery(this));
    });
    jQuery('.jmeter-box-arrow-button:not(.disable)').live('click', function () {
        jQuery(this).parent().find('.jmeter-box-arrow-button').addClass('disable');
        if (jQuery(this).attr('id') == 'matches-left') {
            getNewMatchesProfiles(false, 1, jQuery(this).parent());
        } else {
            getOldMatchesProfiles(jQuery(this).parent());
        }
    });

    // JMeter invatition box
    var inviteEmailInput = jQuery('#jmeter-side input.invite-email');
    var inviteEmailText = inviteEmailInput.val();
    inviteEmailInput.live('focus', function () {
        if (jQuery(this).val() == inviteEmailText) { jQuery(this).val('') }
    });
    inviteEmailInput.live('focusout', function () {
        if (jQuery(this).val() == '') { jQuery(this).val(inviteEmailText) }
    });

    //JMeter tab adjustments
    jQuery('.tab-nav > li').live('click', function () {
        if (parseInt(jQuery('.jmeter-tab-main').height()) > 10) {
            jQuery('#tab-JMeter .jmeter-tab-left').css({ height: jQuery('.jmeter-tab-main').height() });
            jQuery('#tab-JMeter .jmeter-clock .loader').delay(2000).fadeOut(1500);
        }
    });

    //JMeter clock
    if (jQuery('.jmeter-clock').length > 0) {
        preloadClock();
    }

    //JMeter MAD
    if (jQuery('.jmeter-mad').length > 0) {
        slideShowMad();
    }

    if (jQuery('.details-boxes').length > 0) {
        placeMatchOnboxes();
    }

    //1 on 1 long names fix
    jQuery('.jmeter-1on1-box-profile .title').each(function () {
        newValue = jQuery(this).text().replace(/\s+/g, " ").replace(/(\r\n|\n|\r)/gm, " ");
        if (newValue.length > 15) {
            jQuery(this).text(newValue.substring(0, 13) + '...');
        }
    });

    //JMeter open review details
    jQuery('.category-button').toggle(function () {
        jQuery(this).parents('.jmeter-review-category').find('.review-details').show();
        jQuery(this).find('span').toggle();
        jQuery('#jmeter-side').css({
            height: jQuery('#jmeter-main').height()
        });
    }, function () {
        jQuery(this).parents('.jmeter-review-category').find('.review-details').hide();
        jQuery(this).find('span').toggle();
        jQuery('#jmeter-side').css({
            height: jQuery('#jmeter-main').height()
        });
    });

    //JMeter matches preferences
    jQuery('.JMselect').each(function () {
        jQuery(this).text(jQuery(this).prev('select').find(':selected').text());
    });
    jQuery('#sort-online-now .floatInside select').change(function () {
        jQuery(this).next('.JMselect').text(jQuery(this).find(':selected').text());
    });

    //JMeter collapse banner place holder
    if (jQuery('#jmeter-side .jmater-side-content .adunit > div').length == 0) {
        jQuery('#jmeter-side .jmater-side-content .adunit').hide();
    }

    jmeterDimensions();
});

//jQuery(document).ready(function () {
//    jQuery('.sf-menu-vertical > .menu-item > ul li').hover(function () {
//        if (jQuery(this).find('.menu-item-sub').length > 0) {
//            jQuery(this).find('.menu-item-sub').css({
//                display: 'block !important'
//            });
//        }
//    }, function () {
//        if (jQuery(this).find('.menu-item-sub').length > 0) {
//            display: 'none !important'
//        }
//    });
//});

//set offset cookie if not already present
$j(document).ready(function () {
    var cookieName = 'tzo'; 
    var sofsValue = spark_getCookie(cookieName);
    if (!sofsValue) {
        var site = $j('meta[name="author"]').attr('content') || "";
        site = site.toLowerCase();
        var domain = site.replace("http://www.", "");
        var expires = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
        spark_setCookie('tzo', getTimeZoneOffset(), expires, "/", domain, false);
    }
});

function getTimeZoneOffset() {
    var rightNow = new Date();
    var temp = rightNow.toGMTString();
    var offsetRightNow = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
    var std_time_offset = (rightNow - offsetRightNow) / (1000 * 60 * 60);

    return Math.round(std_time_offset);
}

function CreateSubAlertCookie(expirehours) {
    var cookiename = 'subexpalert';
    var value = 'true';
    var date = new Date();
    date.setTime(date.getTime() + (expirehours * 60 * 60 * 1000));
    var expires = "; expires=" + date.toGMTString();
    document.cookie = cookiename + "=" + value + expires + "; path=/";
}

jQuery(".content-announcement .click-close").live('click', function (e) {
    e.preventDefault();
    var alertdata = jQuery(this).closest('[data-expires]').data();
    //Create a cookie that expires in few hours. Do not show alert box until this cookie expires.
    //If expirehours is equal to 0, this means the alert is shown per session. Otherwise, the alert is shown every x hours.
    if (alertdata.expires > 0) {
        CreateSubAlertCookie(alertdata.expires);
    }
});

/*
* Compose Message | gallery/list view user profiles
*/
jQuery(function () {

    // Click on send message opens up a compose overlay (dialog)
    jQuery(".results30-profile.comm-overlay.mini-profile-sub .action-items .comm.item.tabbed .profile30-comm.items li.email > a").on('click', function (e) { //'Send Message' Button
        e.preventDefault();
        showComposeOverlay(jQuery(this));
    });
    jQuery(".results30-profile.comm-overlay.mini-profile-sub .action-items .comm.item.tabbed .tabbed-wrapper > a").on('click', function (e) { //'Say Hi' Button
        e.preventDefault();
        showComposeOverlay(jQuery(this), true);
    });

    jQuery('.comm-overlay .item.tabbed a.menu').on('click', function (e) {
        if (jQuery(this).next('.items').is(":visible") || jQuery(this).next('.items.secret-admirer').length > 0) {
            jQuery(this).parents('li.item.tabbed').addClass("active");
        } else {
            jQuery(this).parents('li.item.tabbed').removeClass("active");
        }
    });
    jQuery('body').on('click', function () {
        jQuery('.comm-overlay .item.tabbed:not(.comm)').removeClass('active');
    });
    jQuery('.comm-overlay li.item.tabbed .items').mouseenter(function () {
        jQuery(this).show();
    }).mouseleave(function () {
        jQuery('.comm-overlay .item.tabbed:not(.comm)').removeClass('active');
        jQuery(this).hide();
    });

    jQuery('body.compose-overlay #divSendVIPOverlay input.textlink').on('click', function (e) {
        jQuery(".close-compose-overlay").hide();
        jQuery('body.compose-overlay #divSendVIPOverlay, body.compose-overlay .ui-widget-overlay').hide();
    });

    //draft confirmation message (indox)
    if (jQuery('body.compose-overlay #mail-list').length > 0) {
        jQuery('body.compose-overlay .PageMessage.notification img').attr('src', '/img/Site/JDate-co-il/icon-email-confirmation.gif');
        jQuery('<div class="close-compose-overlay" id="close-compose-overlay-msg-x">X</div><h1>כתבו לי</h1><div class="compose-email">').insertBefore(jQuery('body.compose-overlay .PageMessage.notification img'));
        jQuery("<p>על מנת לשלוח את ההודעה יש לגשת ל 'תיבת דואר' וללחוץ על לשונית 'טיוטות'. שם נמצאות כל הטיוטות שלך. לחיצה על נושא ההודעה תפתח אותה. שם ניתן לערוך ולשלוח את ההודעה.</p>").insertAfter(jQuery('body.compose-overlay .PageMessage.notification img'));
        jQuery('<a href="javascript: window.top.location.href= \'/Applications/Email/MailBox.aspx?MemberFolderID=2\'" class="inbox">לתיבת הטיוטות שלי ושליחת ההודעה >></a>').insertAfter(jQuery('body.compose-overlay .PageMessage.notification img'));
        jQuery('<div class="close-compose-overlay" id="close-compose-overlay-msg">סגור חלון</div>').insertAfter(jQuery('body.compose-overlay .PageMessage.notification img'));
    }

    if (jQuery('.compose-overlay').length > 0) {
        if (jQuery('body.compose-overlay .page-container').length > 0) { //for compose mail dialog.
            jQuery('<div class="close-compose-overlay" id="close-compose-overlay-msg-x">X</div>').insertBefore(jQuery('body.compose-overlay .page-container h1'));
        } else if (jQuery('body.compose-overlay #page-container').length > 0) { //for  the message dialog (confirmation).
            jQuery('<div class="close-compose-overlay" id="close-compose-overlay-msg-x">X</div>').insertAfter(jQuery('body.compose-overlay #page-container h1'));
            jQuery('<div class="close-compose-overlay" id="close-compose-overlay-msg">סגור חלון</div>').insertAfter(jQuery('body.compose-overlay #page-container .confirmation-message'));
        }

        if (jQuery('.close-compose-overlay').length > 0) {
            jQuery('.close-compose-overlay').on('click', function (e) {
                hideComposeOverlay();
            });
        }

        //Close overlay when clicking outside of it.
        if (jQuery('.notification.error').length > 0) {
            parent.jQuery('iframe#composeOverlayFrame').show();
        }
        if (jQuery('.compose-overlay').is(":visible")) {
            jQuery(document).on('click', function (e) {
                hideComposeOverlay();
            });
            jQuery('.compose-overlay .page-container').on('click', function (e) {
                e.stopPropagation();
            });
        }
        if (jQuery('.confirmation-message').length > 0) {
            parent.jQuery('iframe#composeOverlayFrame').show();
        }

        //if it's a 'user details' dialog/overlay than do a scroll on the dialog and set a minimal height.
        jQuery('#profileDetailsBtn').bind('click', function () {
            if (jQuery('.ui-dialog #profileDetails').length > 0) {
                jQuery('.ui-dialog #profileDetails').css({ 'height': '500px', 'overflow-y': 'scroll' });
            } else {
                jQuery('.ui-dialog #profileDetails').css({ 'height': 'auto', 'overflow-y': 'hidden' });
            }
        });

    }
});

function hideComposeOverlay() {
    jQuery(this).hide();
    parent.jQuery('iframe#composeOverlayFrame').hide();
    return false;
}

function showComposeOverlay(thisElement, isOtherElement) {
    var thisIframeSrc = isOtherElement === undefined ? thisElement.attr('href') : thisElement.next('.profile30-comm').find('li.email > a').attr('href'); 

    var currentIframeSrc = jQuery('iframe#composeOverlayFrame').attr('src');
    var thisSrc = 'http://' + document.domain + thisIframeSrc + '&composeoverlay=1&LayoutTemplateID=2&LayoutOverride=none';
    if (thisIframeSrc != currentIframeSrc) {
        jQuery('iframe#composeOverlayFrame').attr('src', thisSrc);
    }

    jQuery('iframe#composeOverlayFrame').css({ 'height': jQuery(document).height() });
    window.setTimeout(
            function () {
                jQuery('iframe#composeOverlayFrame').show();
                jQuery("#close-compose-overlay").show();
            }, 1250);

    return false;
}

/* Multiple Flirts */
$j(spark).bind("simpleTooltip", function (event, obj) {

    $j(obj).delegate('.modal-push-flirts.ui-dialog .tooltip', 'mouseover mouseout', function (e) {
        var $target = $j(e.target);
        var tipTitle = $target.attr('title');

        if (tipTitle != '') {
            $target.data('tipTitleData', tipTitle);
        }

        $target.attr('title', '');

        if (e.type == 'mouseover') {
            var tipHtml = '<div class="tooltip-content"><p>' + $target.data('tipTitleData') + '</p></div>';
            $j(tipHtml).appendTo('body').css({ right: (e.pageX - 40) + 'px', top: (e.pageY + 15) + 'px' }).show();
        }
        if (e.type == 'mouseout') {
            $j('.tooltip-content').remove();
        }
    });
});

// Trigger custom events onDomReady
$j(function () {
    $j(spark).trigger("simpleTooltip", $j('body'));
});

/*
* Live Person
*/
$j(function () {
    jQuery(".lpchat-button, .lpchat-container").live('click', function (event) {
        PopulateS(true); //clear existing values in omniture "s" object

        s.eVar27 = "LivePersonChatSession - (" + s.pageName + ")";
        s.pageName = "LivePersonChatSession";
        s.events = "event2";
        s.eVar2 = "LivePersonChatSession";
        s.eVar10 = gLivepersonUserStatus;
        s.eVar15 = gLivepersonV15;
        s.eVar36 = "www.jdate.co.il";
        s.prop10 = window.location.pathname;
        s.prop17 = gLivepersonC17;
        s.prop18 = gLivepersonC18;
        s.prop19 = gLivepersonC19;
        s.prop23 = spark.favorites.MemberId;

        s.t(); //send omniture updated values as page load
        s.events = '';

        return false;
    });

    //Top Navigation - Live Person button
    if (jQuery("ul#nav-auxiliary .live-person-topnav-btn").length > 0) { //if the button exists, display it and it's link in the top aux menu.
        jQuery("ul#nav-auxiliary li.live-person-topnav-wrap").show().css({ 'width': 'auto', 'height': 'auto' });
        jQuery("ul#nav-auxiliary .live-person-topnav-btn").show().css({ 'width':'75px', 'height':'15px', 'margin':'-14px 10px 0 0px' });
    }
});

/*
* New YNM
*/
$j(function () {
    //apply it when new YNM is ready. currently BBW, BS & SP are not ready ['1001', '90410', '90510']
    var isNewYNMReday = $j.inArray(spark.favorites.BrandId, ['1003', '1004', '1105', '1107']) > -1;

    if (isNewYNMReday) {
        SecretAdmirer_UiUpdateYNM($j(".click20-horizontal, [id$='secretAdmirerPopup']"));
    }
});

//Peek Banner Hover
$j(function () {
    $peekBanner = jQuery('.plcPeekBnrFlash');
    $peekBannerInnerDiv = jQuery('.plcPeekBnrFlash > .adunit > div');
    $topNav = jQuery('ul#nav-auxiliary');

    jQuery(window).load(function () {
        $peekBanner.hover(function () {
            jQuery(this).css({ 'clip': 'rect(0px 655px 655px 0px)' });
        }, function () {
            jQuery(this).css({ 'clip': 'rect(0px 100px 100px 0px)' });
        });

        if ($peekBannerInnerDiv.is(":visible")) {
            $peekBanner.show();
            if (jQuery(window).width() <= 1024) {
                $topNav.css({ 'margin': '0 0 0 8.25em' });
            } else {
                $topNav.css({ 'margin': '0 0 0 .25em' });
            }
        } else {
            $peekBanner.hide();
        }
    });
});

