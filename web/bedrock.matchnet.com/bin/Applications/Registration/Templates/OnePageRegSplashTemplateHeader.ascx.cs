﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.Registration.Controls;
using Matchnet.Session.ServiceAdapters;

namespace Matchnet.Web.Applications.Registration.Templates
{
    public partial class OnePageRegSplashTemplateHeader : RegistrationTemplateBase, IRegistrationTemplate
    {
        private const string IMG_LOGO_FILE_NAME = "logo-header-img.png";

        public override PlaceHolder ContentPlaceholder { get { return phContent; } }
        public override ValidationMessage ValidationMessageText
        { get { return txtValidation; } }


        protected override void OnInit(EventArgs e)
        {
            #region Moved from WideSimple.ascx.cs

            imgLogo.NavigateUrl = FrameworkGlobals.GetHomepageAbsURL(g, Request.ServerVariables.Get("HTTP_HOST"));

            #endregion

            Control amadesaControl = g.LayoutTemplateBase.FindControl("Amadesa");

            if (amadesaControl != null)
            {
                amadesaControl.Visible = false;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {


            #region Moved from WideSimple.ascx.cs
            string imagePath = Matchnet.Web.Framework.Image.GetURLFromFilename(IMG_LOGO_FILE_NAME);
            if (g.BrandAlias != null)
            {
                imagePath = Matchnet.Web.Framework.Image.GetBrandAliasImagePath(IMG_LOGO_FILE_NAME, g.BrandAlias);
            }

            string host = HttpContext.Current.Request.Url.Host;
            string url = "http://" + host + "/default.aspx";
            imgLogo.NavigateUrl = url;
            imgLogo.ImageUrl = imagePath;
            #endregion

            //add mol
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                plcMol.Visible = true;
                g.ExpansionTokens.Add("MOLCOUNT", Convert.ToString(SessionSA.Instance.GetSessionCount(g.Brand.Site.Community.CommunityID)).ToString());
            }

            if (!string.IsNullOrEmpty(TipResource))
            {
                txtOnePageRegTip.ResourceConstant = TipResource;
            }
        }

        protected string GetOnePageRegContentDivCSS()
        {
            return "class=\"" + (string.IsNullOrEmpty(OnePageRegContentDivCSSClass) ? "default" : OnePageRegContentDivCSSClass) + "\"";
        }

        protected string GetOnePageRegContentDivTipCSS()
        {
            return "class=\"" + (string.IsNullOrEmpty(OnePageRegContentDivCSSClass) ? "" : OnePageRegContentDivCSSClass + "-tip") + "\"";
        }
    }
}
