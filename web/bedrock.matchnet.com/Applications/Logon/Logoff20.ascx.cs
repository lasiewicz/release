﻿#region

using System;
using System.Configuration;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Spark.Authentication.OAuth;
using Spark.Authentication.OAuth.V2;
using Spark.FacebookLike.ServiceAdapters;

#endregion

namespace Matchnet.Web.Applications.Logon
{
    public partial class Logoff20 : FrameworkControl
    {
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                g.HeaderControl.ShowBlankHeader(Matchnet.Web.Framework.Ui.HeaderImageType.Login);
                g.SetTopAuxMenuVisibility(false, true);
                if (LogonManager.IsSuaLogon(g))
                {
                    txtLoggedOut.Visible = false;
                    PlaceHolderReturnToLogon.Visible = true;
                    LabelLogonLink.Text = FrameworkGlobals.Link(_g.Brand.Site.Name,
                                                                "/applications/logon/logon.aspx",
                                                                "");
                }
                else
                {
                    txtLoggedOut.Visible = true;
                    PlaceHolderReturnToLogon.Visible = false;
                    PlaceHolderLogonControl.Controls.Add(
                        Page.LoadControl("/Applications/Logon/Controls/LogonControl20.ascx"));
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //If no ID is specified, viewstate values are not loaded into the controls properly during postback
                this.ID = "LogoffPage";

                // this logic has to be in Page_Load, otherwise Footer has not processed in Init
                g.FooterControl20.HideFooterLinks();

                if (g.Member != null)
                {
                    MembersOnlineSA.Instance.Remove(g.Brand.Site.Community.CommunityID,
                                                    g.Member.MemberID);

                    // Members are now deleted from cache if they log off to allow for synchronization
                    // with outside partners such as Mingle that use the SparkWS.
                    MemberSA.Instance.ExpireCachedMember(g.Member.MemberID);
                    MemberSA.Instance.ExpireCachedMemberAccess(g.Member.MemberID);
                    FacebookLikeServiceSA.Instance.RemoveAllFacebookLikesFromCache(g.Brand.Site.SiteID,
                                                                                   g.Member.MemberID);

                    g.Session.Add("OverrideTypeID", "", SessionPropertyLifetime.TemporaryDurable);
                    g.Session.Clear();
                    g.Member = null;

                    //	Set the custom Breadcrumbs for logout page			
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_THANK_YOU", this), null);
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_THANK_YOU", this), null);

                    // AutoLogin
                    if (AutoLoginHelper.IsSettingEnabled(g))
                    {
                        g.AutoLogin.ExpireAutoLogin();
                    }

                    //expire the connect cookie
                    FrameworkGlobals.ExpireConnectCookie();

                    if (Convert.ToBoolean(RuntimeSettings.GetSetting("SET_IM_OAUTH_COOKIES",
                                                                     g.Brand.Site.Community.CommunityID,
                                                                     g.Brand.Site.SiteID, g.Brand.BrandID)))
                    {
                        var oauthCookieDomain = SettingsManager.GetTokenEnvironmentForCookie();
                        oauthCookieDomain += "." + g.Brand.Site.Name.ToLower();
                        LoginManager.Instance.RemoveTokenCookies(oauthCookieDomain);
                    }

                    if (txtJoinToday != null)
                    {
                        txtJoinToday.Href = FrameworkGlobals.GetRegistrationPageURL(g);
                    }

                    SuaLogonManager suaLogonManager = new SuaLogonManager(g.Brand.BrandID, new LogonManager(g));
                    if (suaLogonManager.IsSuaLogon())
                    {
                        suaLogonManager.RemoveTokenCookies();
                    }
                }
                else // member wasn't logged in so go to normal version of login page
                {
                    if (!Page.IsPostBack)
                    {
                        g.Transfer("/Applications/Logon/Logon.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
            this.Init += new EventHandler(this.Page_Init);
        }

        #endregion
    }
}