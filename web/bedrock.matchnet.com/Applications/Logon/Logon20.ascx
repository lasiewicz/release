﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Logon20.ascx.cs" Inherits="Matchnet.Web.Applications.Logon.Logon20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="LogonControl" Src="Controls/LogonControl20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="~/Framework/Ui/Advertising/AdUnit.ascx" %>
<div id="page-container">
    <mn:Title runat="server" ID="ttlLogin" ResourceConstant="TXT_LOGIN"/>
    <mn:Txt runat="server" ID="txtReasonToSignUp" ResourceConstant="TXT_REASON_TO_SIGNUP" />
    <uc1:LogonControl ID="LogonControl" runat="server"></uc1:LogonControl>
    <div class="rbox-wrapper-nonmember">
        <div class="rbox-style-clear login-box non-member">
            <mn:Txt runat="server" ID="txtNotMemberYet" ResourceConstant="TXT_NOT_A_MEMBER_YET" />
            <mn:Txt runat="server" ID="txtJoinToday" ResourceConstant="TXT_JOIN_TODAY" CssClass="join-now" />
            <mn:Txt runat="server" ID="txtLoginUpsell" ResourceConstant="TXT_UPGRADE_PREMIUM_TEXT" />
            <%--<mn:AdUnit id="adLogon" Size="Square" runat="server" GAMAdSlot="logon_middle_300x250" />--%>
        </div>
    </div>
</div>
