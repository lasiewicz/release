﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Logoff20.ascx.cs" Inherits="Matchnet.Web.Applications.Logon.Logoff20" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Reference Control="Controls/LogonControl20.ascx" %>
<div id="page-container">
    <mn:Title runat="server" ID="ttlLogout" ResourceConstant="TXT_LOGOUT" />
    <p class="editorial">
        <mn:Txt runat="server" ID="txtLoggedOut" ResourceConstant="TXT_LOGGED_OUT" />
        <asp:PlaceHolder runat="server" ID="PlaceHolderReturnToLogon" Visible="False">
            <mn:Txt runat="server" ID="txtReturnToLogon" ResourceConstant="TXT_RETURN_TO_LOGON" />
            <asp:Label runat="server" ID="LabelLogonLink"></asp:Label>
        </asp:PlaceHolder>
    </p>
    <%--Login modules--%>
    <!--<p class="editorial"><mn:txt runat="server" id="lnkLogon" ResourceConstant="TXT_RETURN_TO_LOGON" Href="Logon.aspx" /></p>-->
    <asp:PlaceHolder runat="server" ID="PlaceHolderLogonControl"></asp:PlaceHolder>
    <div class="rbox-wrapper-nonmember">
        <div class="rbox-style-clear login-box non-member">
            <mn:Txt runat="server" ID="txtNotMemberYet" ResourceConstant="TXT_NOT_A_MEMBER_YET" />
            <mn:Txt runat="server" ID="txtJoinToday" ResourceConstant="TXT_JOIN_TODAY" CssClass="join-now" />
            <mn:Txt runat="server" ID="txtLoginUpsell" ResourceConstant="TXT_UPGRADE_PREMIUM_TEXT" />
        </div>
    </div>
</div>
<mn:AdUnit ID="adLargeRectangle" Size="LargeRectangle" runat="server" ExpandImageTokens="true"
    GAMAdSlot="logout_middle_400x600" />
