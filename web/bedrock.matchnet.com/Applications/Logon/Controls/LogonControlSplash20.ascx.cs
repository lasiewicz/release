﻿using System;
using System.Configuration;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using Matchnet.Caching.CacheBuster;
using Matchnet.Caching.CacheBuster.HTTPContextWrappers;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content;
using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Enumerations;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Lib.Util;
using Matchnet.Web.Framework.Managers;
using Spark.Authentication.OAuth;
using Spark.Authentication.OAuth.V2;

namespace Matchnet.Web.Applications.Logon.Controls
{
    public partial class LogonControlSplash20 : FrameworkControl
    {
        private static string _tokenCookieEnvironment;

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                string nonFrameworkPassword = (HttpContext.Current.Request.Form["password"] != null) ? HttpContext.Current.Request.Form["password"] : string.Empty;
                string nonFrameworkEmailAddress = (HttpContext.Current.Request.Form["emailaddress"] != null) ? HttpContext.Current.Request.Form["emailaddress"] : string.Empty;

                if (nonFrameworkPassword != string.Empty && nonFrameworkEmailAddress != string.Empty)
                {
                    this.Password.Text = nonFrameworkPassword;
                    this.EmailAddress.Text = nonFrameworkEmailAddress;
                    this.Login_Click(this.Login, null);
                }
            }

            try
            {

                SetDefaultButton(Password, Login);
                SetDefaultButton(EmailAddress, Login);

                if (!IsPostBack)
                {
                    LoadControlState();
                }

                //if (!AutoLoginHelper.IsSettingEnabled(_g))
                //{
                //    PlaceHolderAutoLogin.Visible = false;
                //}

                if (!IsPostBack)
                {
                    CheckBoxAutoLogin.Checked = false;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void LoadControlState()
        {
            // Pre-populate user email address based on cookie value (if present)
            string strEmail = g.Session["EmailAddress"];

            if (strEmail != String.Empty)
            {
                EmailAddress.Text = strEmail;
                this.fefControl.ElementClientId = this.Password.ClientID;
            }
            else
            {
                this.fefControl.ElementClientId = this.EmailAddress.ClientID;
            }
        }

        private void SetDefaultButton(TextBox txt, Matchnet.Web.Framework.Ui.FormElements.FrameworkButton button)
        {
            txt.Attributes.Add("onkeydown", "setAutoSubmitButton(" + button.ClientID + ",event)");
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Login.Click += new System.EventHandler(this.Login_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        protected void Login_Click(object sender, System.EventArgs e)
        {
            try
            {
                //todo : refactor ipaddress code
                CurrentRequest currentRequest = new CurrentRequest();
                CurrentServer currentServer = new CurrentServer();
                var ipConverter = new IPConverter();
                string ipAddress = currentRequest.ClientIP;
                g.LoggingManager.LogInfoMessage("AutoLoginHelper", "Email : " + EmailAddress.Text + " IPAddress : " + ipAddress);
                // Removed Marketing related tracking vals #21001
                AuthenticationResult authResult = MemberSA.Instance.Authenticate(g.Brand,
                    EmailAddress.Text,
                    Password.Text);

                // If this is SparkWS call, we are finding that sometimes the user logs in, is sent back to the
                // calling site (i.e. Connect), the calling Site makes a call to SparkWS.GetMember(), and SparkWS
                // is not having the most up-to-date Session.  Thus, we need to make this call synchronous.  NOTE:
                // this may still not solve this problem because there is a possibility that delay is not caused by
                // the asynchronous SaveSession call but instead by the asynchronous cache synchronization and replication.
                if (HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_SPARKWS_CONTEXT] == "true")
                    g.SaveSessionAsync = false;

                switch (authResult.Status)
                {
                    case AuthenticationStatus.Authenticated:
                        string encryptedPassword;
                        if (CheckBoxAutoLogin.Checked)
                        {
                            encryptedPassword = g.AutoLogin.CreateAutoLogin(EmailAddress.Text, Password.Text);
                        }
                        else
                        {
                            encryptedPassword = MemberSA.Instance.GetPasswordForAuth(EmailAddress.Text, g.Brand.Site.Community.CommunityID, Password.Text); 
                            if (Matchnet.Web.Framework.AutoLoginHelper.IsSettingEnabled(g))
                            {
                                g.AutoLogin.Status = AutoLoginHelper.AutoLoginStatus.NormalLoggedIn;
                            }
                        }
                        g.StartLogonSession(authResult.MemberID, g.Brand.Site.Community.CommunityID, g.Brand.BrandID,
                            this.EmailAddress.Text, authResult.UserName, false, Constants.NULL_INT);
                        var IMOAuthEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("SET_IM_OAUTH_COOKIES",
                                                                        g.Brand.Site.Community.CommunityID,
                                                                        g.Brand.Site.SiteID, g.Brand.BrandID));
                        if (IMOAuthEnabled)
                        {
                            if (_tokenCookieEnvironment == null)
                            {
                                _tokenCookieEnvironment = SettingsManager.GetTokenEnvironmentForCookie();
                            }
                            LoginManager.Instance.FetchTokensAndSaveToCookies(EmailAddress.Text, encryptedPassword, true,
                                                                     CheckBoxAutoLogin.Checked, _tokenCookieEnvironment, g.Brand.Site.Name.ToLower(),
                                                                     g.Brand.BrandID);
                        }
                        ProcessSuccesfulLogon();

                        break;
                    case AuthenticationStatus.InvalidEmailAddress:
                        ProcessFailedLogon("ERR_YOU_HAVE_PROVIDED_AN_INVALID_EMAIL_ADDRESS");
                        break;
                    case AuthenticationStatus.InvalidPassword:
                        ProcessFailedLogon("ERR_YOU_HAVE_PROVIDED_AN_INVALID_PASSWORD");
                        break;
                    case AuthenticationStatus.AdminSuspended:
                        ProcessFailedLogon("ADMIN_SUSPENDED_ACTION");
                        break;
                }
            }
			catch (ThreadAbortException)
			{
				// this is expected behavior due to a redirect after login
			}
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void ProcessFailedLogon(string ResourceError)
        {
            if (g.AppPage.ControlName.ToLower() == "splash20")
            {
                g.Transfer("/Applications/Logon/Logon.aspx?rc=" + ResourceError);
            }
            else
            {
                if (Request["rc"] != null && Request["rc"] != String.Empty)
                {
                    g.Transfer("/Applications/Logon/Logon.aspx?rc=" + ResourceError);
                }
                else
                {
                    g.Notification.AddMessageString(g.GetResource(ResourceError, null));
                }
            }
        }

        private void ProcessSuccesfulLogon()
        {
            // Omniture Analytics - Event Tracking
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                // Tracking Omniture Traffic Variables on successful login.
                g.Session.Add("OmnitureLoggedIn", "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
            }

            //for forced page on logon
            g.Session.Add("LoggedIn", "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

            MigrateMingleCookies();
            bool hasJdateReligion = true;

            if (_g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
            {
                if (_g.Member.GetAttributeInt(_g.Brand, "JdateReligion", Constants.NULL_INT) == Constants.NULL_INT)
                {
                    hasJdateReligion = false;
                }
            }

            if (!hasJdateReligion)
            {
                string _alreadyOffered = _g.Session.GetString("ATTR_EDIT_JDATERELIGION");
                if (String.IsNullOrEmpty(_alreadyOffered))
                    _g.Transfer("/Applications/MemberProfile/AttributeEdit.aspx?Attribute=JDatereligion&DestinationURL=" + Server.UrlEncode(_g.GetDestinationURL()));
            }
            //redirect to destination URL if set
            if (g.GetDestinationURL().Length > 0)
            {
                // if the logon contains a destination to the IM page and the current member is valid, proceed to transfer 
                // the user to viewProfile, otherwise send them to the IM control for any redirection considerations.
                if (g.DestinationPage == ContextGlobal.Pages.PAGE_INSTANT_MESSENGER_MAIN && g.ValidateMemberContact(g.RecipientMemberID))
                {
                    g.Transfer("/Applications/MemberProfile/ViewProfile.aspx?memberID=" + g.RecipientMemberID);
                }
                else
                {
                    g.DestinationURLRedirect();
                }
            }

            // if we are here, either no destination URL exists or the transfer to it was not allowed
            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), MemberLoadFlags.None);
            int globalStatusMask = member.GetAttributeInt(g.Brand, "GlobalStatusMask", 0);

            if (Matchnet.Lib.Util.Util.CBool(globalStatusMask & WebConstants.EMAIL_VERIFIED, true))
            {
                string nextStep = GetFirstEmptyStep();
                if (nextStep != null)
                {
                    g.Transfer(nextStep);
                }
                else
                {
                    // Taking this out for now because it is causing a problem only on production.
                    //// MPR-412: For AS and Date.CA, if the user is a non-sub take them to sub page
                    //if ((g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles ||
                    //    g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.DateCA) &&
                    //    !g.Member.IsPayingMember(g.Brand.Site.SiteID))
                    //{
                    //    g.Transfer(FrameworkGlobals.LinkHrefSSL("/Applications/Subscription/Subscribe.aspx?prtid=209"));
                    //}
                    //else
                    g.Transfer("/Applications/Home/Default.aspx");
                }
            }
            else
            {
                g.Transfer("/Applications/MemberServices/VerifyEmailNag.aspx");
            }
        }

        private string GetFirstEmptyStep()
        {
            int stepCount = Matchnet.Conversion.CInt(RuntimeSettings.GetSetting("REGISTRATION_STEPS", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));
            int completedStepMask = Matchnet.Conversion.CInt(g.Member.GetAttributeInt(g.Brand, "NextRegistrationActionPageID"));
            int stepValue = 1;

            for (int currentStep = 1; currentStep <= stepCount; currentStep++)
            {
                if (currentStep <= stepCount)
                {
                    //regular steps
                    if ((completedStepMask & stepValue) != stepValue)
                    {														// Returning user with incomplete reg steps
                        return "/Applications/MemberProfile/RegistrationStep" + currentStep + ".aspx?registrationMode="
                            + (int)RegistrationMode.LoginWithIncompleteSteps;
                    }
                }

                stepValue = stepValue * 2;
            }

            return null;
        }
        //this is migration routine to migrate mingle cookies, has a lot of hardcoding
        //should be hopefully called once per mingle user
        //and then can be deleted with time.....which probably means never
        private void MigrateMingleCookies()
        {
            try
            {
                if (_g.Member == null)
                    return;

                int communityid = _g.Brand.Site.Community.CommunityID;
                if (!FrameworkGlobals.IsMingleSite(communityid))
                {
                    return;
                }
                int cookiesmigrated = _g.Member.GetAttributeInt(_g.Brand, "MingleCookieMigrationFlag", Constants.NULL_INT);

                if (cookiesmigrated == 1)
                    return;

                int prm = Conversion.CInt(FrameworkGlobals.GetCookie("camp_src", "", false), Constants.NULL_INT);
                string luggage = FrameworkGlobals.GetCookie("camp_opt", "", false);
                int banner = Conversion.CInt(FrameworkGlobals.GetCookie("camp_adid", "", false), Constants.NULL_INT);
                if (prm > 0)
                {
                    _g.Member.SetAttributeInt(_g.Brand, WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, prm);
                }
                if (!String.IsNullOrEmpty(luggage))
                {
                    _g.Member.SetAttributeText(_g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE, luggage, TextStatusType.Auto);
                }
                if (banner > 0)
                {
                    _g.Member.SetAttributeInt(_g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDBANNERID, banner);
                }

                _g.Member.SetAttributeInt(_g.Brand, "MingleCookieMigrationFlag", 1);

                Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(_g.Member);
                //
                FrameworkGlobals.RemoveCookie("camp_src");
                FrameworkGlobals.RemoveCookie("camp_opt");
                FrameworkGlobals.RemoveCookie("camp_adid");

                FrameworkGlobals.RemoveCookie("singlesession");
                FrameworkGlobals.RemoveCookie("msession");
                FrameworkGlobals.RemoveCookie("encrPassword");
                FrameworkGlobals.RemoveCookie("loginusername");
                FrameworkGlobals.RemoveCookie("mbox");
                FrameworkGlobals.RemoveCookie("s_cc");
                FrameworkGlobals.RemoveCookie("s_sq");
                FrameworkGlobals.RemoveCookie("s_vi");

            }
            catch (Exception ex)
            { _g.ProcessException(ex); }


        }
    }
}