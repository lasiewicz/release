﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="LogonControl20.ascx.cs" Inherits="Matchnet.Web.Applications.Logon.Controls.LogonControl20" %>
<%@ Register TagPrefix="dg" TagName="FirstElementFocus" Src="../../../Framework/JavaScript/FirstElementFocus.ascx" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<dg:FirstElementFocus id="fefControl" runat="server" />

<div class="rbox-wrapper-member">
<fieldset id="loginBox" class="rbox-style-high login-box member">
<mn:txt runat="server" id="txtMemberLogin" ResourceConstant="TXT_MEMBER_LOGIN" />

<dl class="dl-form">
	<dt><mn:txt runat="server" id="txtEmailAddress" ResourceConstant="TXT_EMAIL_ADDRESS" /></dt>
	    <dd><asp:TextBox id="EmailAddress" Columns="30" runat="server" dir="ltr" />
            <br/><asp:RequiredFieldValidator ID="revEmailAddressRequiredValidator" Runat="server" ControlToValidate="EmailAddress"
			    Display="Dynamic"></asp:RequiredFieldValidator>
		    <asp:RegularExpressionValidator id="revEmailAddressValidator" runat="server" ControlToValidate="EmailAddress"
			    Display="Dynamic" ValidationExpression="\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\s*"></asp:RegularExpressionValidator>
        </dd>
	<dt><mn:txt runat="server" id="txtPassword" ResourceConstant="TXT_PASSWORD" /></dt>
	    <dd><asp:TextBox id="Password" Columns="30" TextMode="Password" runat="server" dir="ltr" MaxLength="16" style="margin-bottom: 8px;" />
        <br/><asp:RequiredFieldValidator ID="revPasswordRequiredValidator" Runat="server" ControlToValidate="Password"
			    Display="Dynamic" CssClass="error"></asp:RequiredFieldValidator>
            <asp:PlaceHolder runat="server" ID="phPasswordError" Visible="false">
                <span class="error"><asp:Literal runat="server" ID="litPasswordError"></asp:Literal></span>
            </asp:PlaceHolder>
        </dd>
	<dt>&nbsp;</dt>
	    <dd class="remember-me">
	        <asp:PlaceHolder ID="PlaceHolderAutoLogin" Runat="server">
		        <span class="checkBoxReset"><asp:CheckBox ID="CheckBoxAutoLogin" Runat="server" /></span>
                <mn:txt runat="server" id="txtRememberMe" ResourceConstant="TXT_REMEMBER_ME" />
                <div class="rel-layer-container"><a href="#" rel="click"><mn:image runat="server" filename="icon-help.gif" align="absmiddle" ID="Image2" /></a>
 
	                <div class="rel-layer-div">
			            <p class="float-outside"><a href="#" class="click-close"><mn:txt runat="server" id="Txt2" ResourceConstant="TXT_HELP_LAYER_CLOSE_LINK" /></a></p>
		                <h5><mn:txt runat="server" id="txtRememberMeHelpLayerHeadline" ResourceConstant="TXT_REMEMBER_ME_HELP_LAYER_HEADLINE" /></h5>
		                <mn:txt runat="server" id="txtRememberMeHelpLayerBody" ResourceConstant="TXT_REMEMBER_ME_HELP_LAYER_BODY" />	                </div>
	            
	            </div>

	        <small><mn:txt runat="server" id="Txt1" ResourceConstant="TXT_DO_NOT_CHECK_IF_ON_SHARED_COMPUTER" /></small>
	        </asp:PlaceHolder>
	    </dd>
	<dt>&nbsp;</dt>
	    <dd class="cta">
	        <mn2:FrameworkButton id="Login" CssClass="btn primary wide" runat="server" ResourceConstant="BTN_LOGIN" />
	        <small><a href="/applications/memberservices/ForgotPassword.aspx"><mn:txt runat="server" id="txtForgotPassword" ResourceConstant="TXT_FORGOT_YOUR_PASSWORD" /></a></small>
	    </dd>
</dl>
	
</fieldset>
</div>