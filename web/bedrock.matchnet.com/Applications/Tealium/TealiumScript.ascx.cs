﻿using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Content.ServiceAdapters;

namespace Matchnet.Web.Applications.Tealium
{
    public partial class TealiumScript : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            // Query DB to determine if we should include tealium script into the site using FWS. 
            string addTeliumScript = SettingsManager.GetSettingString(SettingConstants.ADD_TEALIUM_SCRIPT, g.Brand);

            // If for some reason the setting isnt in the DB or SettingConstant simple isnt found, do add a script. Otherwise see if we want to add it. 
            if (!string.IsNullOrEmpty(addTeliumScript))
            {
                if(addTeliumScript == "true")
                {
                    // Add a comment to the DOM.
                    ltrScriptAdded.Text = "<!--The tealium script has successfully been enabled for this site.-->";
                    //ltrScriptAdded.Text = "Script Added";

                    // Determine the value we want to set for variable 'a'. ( We end up setting src = 'a' in JS). This will either be global default or site specific override. 
                    string tealiumSrc = SettingsManager.GetSettingString(SettingConstants.TEALIUM_SRC, g.Brand) ?? "no setting found in DB for TEALIUM_SRC. Check siteSetting and Setting";

                    // Set each of the elements text.
                    ltrTealiumScriptElement1.Text = "<script type=\"text/javascript\"> var utag_data = {} </script>";
                    ltrTealiumScriptElement2.Text = "<script type=\"text/javascript\">(function (a, b, c, d) {a = '" + tealiumSrc + "';b = document;c = 'script';d = b.createElement(c);d.src = a;d.type = 'text/java' + c;d.async = true;a = b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d, a);})();</script>";
                }
                else
                {
                    ltrScriptAdded.Text = "<!--The tealium script has NOT been enabled for this site.-->";
                    //ltrScriptAdded.Text = "Script NOT Added";
                }
            }

            // Below were used to test that the design of the tealium did exactly what the color code did. Here we can toggle DB record attributes and visible see them in UI. 
            //g.Brand = BrandConfigSA.Instance.GetBrandByID(1003);
            //ltrBrand.Text = g.Brand.BrandID.ToString();
            //ltrEnableColorCodePurchase.Text = SettingsManager.GetSettingString(SettingConstants.ENABLE_COLORCODE_PURCHASE, g.Brand);
            //ltrAddTealiumScript.Text = SettingsManager.GetSettingString(SettingConstants.ADD_TEALIUM_SCRIPT, g.Brand); 
       }
    }
}