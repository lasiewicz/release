﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TealiumScript.ascx.cs" Inherits="Matchnet.Web.Applications.Tealium.TealiumScript" %>
<%--To use this control, add this to top of the layout page, or whatever other pages you would like this script to be in DOM.--%> 
<%--<%@ Register TagPrefix="uc1" TagName="TealiumScript" Src="~/Applications/Tealium/TealiumScript.ascx" %>--%>
<%--Then add this into the layout--%>
<%--<uc1:TealiumScript runat="server" ID="TealiumScript" />--%>
<!--BEGIN Tealium script-->
<asp:Literal ID="ltrTealiumScriptElement1" runat="server"></asp:Literal>
<!-- Loading script asynchronously -->
<asp:Literal ID="ltrScriptAdded" runat="server"></asp:Literal>
<asp:Literal ID="ltrTealiumScriptElement2" runat="server"></asp:Literal>
<!--END Tealium script.-->





