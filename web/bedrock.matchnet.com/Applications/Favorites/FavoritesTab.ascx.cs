﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.IO;
using System.Net;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.API;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.Favorites
{
    public partial class FavoritesTab : FavoritesControl
    {
        protected void Page_Load(object sender, EventArgs e){ }

        public override void LoadFavoritesOnline()
        {
            base.LoadFavoritesOnline();
            if (Favorites.Count > 0)
            {
                //add View more li
            }
            repeaterFavorites.DataSource = Favorites;
            repeaterFavorites.DataBind();            
        }

        protected void RepeaterFavorites_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                FavoriteMember dataItem = e.Item.DataItem as FavoriteMember;
                HtmlGenericControl liTab = (HtmlGenericControl)e.Item.FindControl("liFav");
                HyperLink hLink = new HyperLink();
                hLink.CssClass = "favslist";
                hLink.NavigateUrl = BreadCrumbHelper.MakeViewProfileLink(
                    BreadCrumbHelper.EntryPoint.NoArgs,
                    dataItem.MemberId,
                    e.Item.ItemIndex+1,
                    null,
                    0);
                hLink.NavigateUrl = _g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, hLink.NavigateUrl, string.Empty);
                HtmlImage img=new HtmlImage();
                img.Alt = dataItem.Name;
                img.Src = dataItem.Thumbnail;
                hLink.Controls.Add(img);
                HtmlGenericControl span = new HtmlGenericControl("span");
                span.InnerText = dataItem.Name;
                hLink.Controls.Add(span);
                if (dataItem.Status.Equals(FriendStatus.Offline.ToString()))
                {
                    hLink.CssClass += " idle";
                }
                liTab.Controls.Add(hLink);
            }
        }


    }
}