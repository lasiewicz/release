﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FavoritesClientNav.ascx.cs" Inherits="Matchnet.Web.Applications.Favorites.FavoritesClientNav" %>
<% if (IsFavoritesEnabled && IsLoggedIn){ %>
    <script type="text/javascript" src="/Applications/Favorites/favorites.js"></script>
    <script type="text/javascript">
        //<![CDATA[
        (function (){
            __addToNamespace__('spark.favorites', {
                MinPRTIdValue:<%=int.MinValue%>,
                BrandId:'<%=BrandId%>',
                MemberId:'<%=MemberId%>',
                BrowseMoreText:'<%=GetText("TXT_BROWSE_MORE",true,true)%>',
                getTimeInterval:function() {
                    return <%=TimeInterval%>;
                },
                __init__:function() {
                    spark.favorites.addConfig({
                        onlineFavs: {},
                        idleFavs: {},
                        offlineFavs: {},                        
                        StripClass:'vertstrip-favs-nav',
                        ListId:'online-favs-nav',                        
                        ListClass:'item',
                        ListOfflineClass:'offline',
                        ListOnlineClass:'online',                        
                        ListOnlineTitle:'<%=GetText("TXT_LI_ONLINE_TITLE",true,true)%>',
                        ListOfflineTitle:'<%=GetText("TXT_LI_OFFLINE_TITLE",true,true)%>',
                        HrefOnlineTitle:'<%=GetText("TXT_HREF_ONLINE_TITLE",true,true)%>',
                        HrefClass:'favslist onlineFavsNav', 
                        HrefOfflineClass:'favslist offlineFavsNav',                            
                        ClickId:'<%=ClickableElementId%>',
                        PrtId:787,
                        ViewMoreText:'<a id="viewMore<%=ClickableElementId%>" href="javascript:void(0);"><%=GetText("TXT_VIEW_MORE_FAVORITES",true,true)%></a>',
                        Carousel:function() {
		                    jQuery(".vertstrip-favs-nav").jCarouselLite({
			                    btnNext: ".vertstrip-next-favs-nav",
			                    btnPrev: ".vertstrip-prev-favs-nav",
			                    circular: false,
			                    mouseWheel: true,
			                    scroll: 2,
			                    visible: 6,
                                colNum:1,
			                    vertical: true,
                                pageSize:6
		                    });
                        }
                    });
                }
            });
        })();
        
        jQuery(document).ready(function() {
            if("null"!="<%=OnlineIndicatorElementId%>" && ""!="<%=OnlineIndicatorElementId%>" && jQuery("#<%=OnlineIndicatorElementId%>").length>0){
                spark.util.addItem(spark.favorites.onlineIndicatorIds,"<%=OnlineIndicatorElementId%>");
                //only create update interval once
                if(spark.favorites.onlineIndicatorIds.length==1) {
                    spark.favorites.loadOnlineIndicator();
                }
            }
            jQuery("#<%=ClickableElementId%>").click(function(evt) {
                if (jQuery('#vertstripFavsNav').is(':hidden')){
                    var config=spark.favorites.getConfig('ClickId', '<%=ClickableElementId%>');
                    var pageSize=(config)?spark.favorites.totalFavs(config):0;
                    spark.favorites.loadFavoritesWithConfig(evt,((pageSize>50)?pageSize:50), true);
                    jQuery('#vertstripFavsNav').show();
                    //omniture tracking


                    var originalPageName = spark.tracking.getPageName();
                    if (originalPageName){
                        spark.tracking.addPageName(originalPageName + " - Favorites", true);
                    }else{
                        PopulateS(true); //reset existing values in omniture "s" object
                        originalPageName = spark.tracking.getPageName();
                        spark.tracking.addPageName(originalPageName + " - Favorites", true);
                    }

                    spark.tracking.addEvent("event44", true);
                    spark.tracking.addLinkTrackEvent("event44", true);
                    spark.tracking.addLinkTrackVar("events", true);
                    spark.tracking.trackLink({ firstParam: true, linkName: "Click to View Favorite-Top Nav module" });
                    spark.tracking.addPageName(originalPageName, true);
                } else {
                    jQuery('#vertstripFavsNav').hide();
                    jQuery('#vertstripFavsNav').hide().parent().find('ul #favNav').removeClass('selected').find("[class*='s-icon']").removeClass("favorites-active");                    
                }
            });

            jQuery("body").click(function(evt) {
                if (!jQuery('#vertstripFavsNav').is(':hidden')) {
                    var targ=jQuery(evt.target);
                    if(targ.parents("#vertstripFavsNav").length == 0 && targ.parents("#<%=ClickableElementId%>").length == 0){
                        jQuery('#vertstripFavsNav').hide().parent().find('ul #favNav').removeClass('selected').find("[class*='s-icon']").removeClass("favorites-active");
                    }
                }            
            });

            jQuery("#viewMore<%=ClickableElementId%>").live("click",function(evt){
                var offset=10;
                var config=spark.favorites.getConfig('ClickId','<%=ClickableElementId%>');
                spark.favorites.__loadFavoritesWithConfig__({currentTarget:{id:'<%=ClickableElementId%>'}},spark.favorites.totalFavs(config)+offset);
                jQuery('#vertstripFavsNav').show();
                evt.stopPropagation();
            });            
                                                
            //omniture tracking
            jQuery("a.onlineFavsNav").live("click",function(evt) {
                    spark.tracking.addEvent("event45", true);
                    spark.tracking.addLinkTrackEvent("event45", true);
                    spark.tracking.addLinkTrackVar("events", true);
                    spark.tracking.addLinkTrackVar("prop34");
                    spark.tracking.addLinkTrackVar("prop35");
                    spark.tracking.addProp(34, "top nav module");
                    spark.tracking.addProp(35, "Favorite");
                    spark.tracking.trackLink({ firstParam: true, linkName: "Click to Initiate IM" });
            });
                
            jQuery("#favNav a").click(function(){
                var $this = jQuery(this),
                    siteDirection="<%=Direction%>",                
                    pos = $this.parent().position(),
                    pWidth = $this.parent().outerWidth(),
                    offset = jQuery("#vertstripFavsNav").outerWidth();
                //console.log(offset);
                $this.parent().addClass("selected");
                $this.find("[class*='s-icon']").addClass("favorites-active");

                if(siteDirection=="rtl")
                {
                    jQuery("#vertstripFavsNav").css({"left":(pos.left) + "px"});         
                }
                else{
                    jQuery("#vertstripFavsNav").css({"left":(pos.left - offset + pWidth) + "px"});         
                }
            });
                     
        });
                //]]>
    </script>

<div id="vertstripFavsNav" class="favs-online-wrapper favs-online outer-wrapper" style="display:none;"> 
	<div class="vertstrip-favs-nav feed-wrapper">
        <div class="vertstrip-favs-nav-feed feed">
	        <ul id="online-favs-nav"></ul>
	    </div>
        <div class="vertstrip-favs-nav-controls control">
	        <button class="vertstrip-prev-favs-nav disabled">&#9650;</button>
	        <button class="vertstrip-next-favs-nav">&#9660;</button>
        </div>
    </div>
	<div class="edit-favs clear-both edit">
		<a href="/Applications/HotList/View.aspx?CategoryID=0" title="<%=GetText("TXT_EDIT_FAVORITIES",true,false)%>"><%=GetText("TXT_EDIT_FAVORITIES",true,false)%></a> | 
		<a id="hide-nav-lnk" href="/Applications/MemberServices/DisplaySettings.aspx?NavPoint=sub" title="<%=GetText("TXT_HIDE_MASK",true,false).Replace("%s",GetStatus())%>"><%=GetText("TXT_HIDE_MASK",true,false).Replace("%s",GetStatus())%></a>
    </div>        
</div>
<% } %>