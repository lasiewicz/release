﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FavoritesTab.ascx.cs" Inherits="Matchnet.Web.Applications.Favorites.FavoritesTab" %>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery(".filmstrip").jCarouselLite({
			btnNext: ".filmstrip-next",
			btnPrev: ".filmstrip-prev",
			circular: false,
			mouseWheel: true,
			scroll:6,
			speed:600,
			visible:6
		});
		jQuery(".vertstrip-favs").jCarouselLite({
			btnNext: ".vertstrip-next-favs",
			btnPrev: ".vertstrip-prev-favs",
			circular: false,
			mouseWheel: true,
			scroll: 2,
			visible: 3,
			vertical: true
		});
});
</script>


    <div class="vertstrip-favs feed-wrapper">
	    <div class="vertstrip-favs-feed feed">
	    <ul>
        <asp:Repeater ID="repeaterFavorites" runat="server" OnItemDataBound="RepeaterFavorites_ItemDataBound">
        <ItemTemplate>
        <li class="item style-basic" runat="server" id="liFav"></li>
        </ItemTemplate>
        </asp:Repeater>
        <%if (null != Favorites.Count && Favorites.Count > 0)
          {%>
          <li class="item view-more"><a href="/Applications/Search/SearchResults.aspx">View More >></a></li>
        <% }
          else
          { %>
          <li class="item view-more"><a href="/Applications/Search/SearchResults.aspx">Search For Favorites Now! &gt;&gt;</a></li>
        <% } %>
        </ul>
	    </div>
	    <div class="vertstrip-favs-controls control">
		    <button class="vertstrip-prev-favs"  runat="server">&#9650;</button>
		    <button class="vertstrip-next-favs"  runat="server">&#9660;</button>
	    </div>
    </div>
