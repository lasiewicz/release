﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.IO;
using System.Net;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.API;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.Favorites
{
    public abstract partial class FavoritesControl : FrameworkControl
    {
        public static string FAVORITE_SERVICE_URL = "http://{0}/Applications/API/Favorites.asmx/GetFavorites";
        public List<FavoriteMember> Favorites {get; set;}
        public bool IsFavoritesEnabled
        {
            get { return FavoritesControl.IsFavoritesOnlineEnabled(g); }
        }

        public bool IsLoggedIn
        {
            get { return null != g.Member && g.Member.MemberID > 0; }
        }

        public bool IsVisible
        {
            get
            {
                bool isVisible=true;
                if(IsLoggedIn) { 
                    if((g.Member.GetAttributeInt(g.Brand, "HideMask") & (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline) == (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline)
                    {
                        isVisible = false;
                    }
                }
                return isVisible;
            }
        }

        public string ClickableElementId { get; set; }

        public string OnlineIndicatorElementId { get; set; }
        
        public string OmnitureVar { get; set; }

        public int BrandId
        {
            get { return g.Brand.BrandID; }
        }

        public int MemberId
        {
            get { return g.Member.MemberID; }
        }

        public int TimeInterval
        {
            get {
                string t=FrameworkGlobals.GetSetting("FAVORITES_ONLINE_TIME_INTERVAL",_g,"120000");
                int interval=Int32.Parse(t);
                return interval;
            }
        }

        public static bool IsFavoritesOnlineEnabled(ContextGlobal _g)
        {
            return FrameworkGlobals.GetBooleanFromConfig(_g, "ENABLE_FAVORITES_ONLINE");
        }

        public string GetStatus()
        {
            return (IsVisible) ? "online" : "offline";
        }

        public string GetText(string key)
        {
            string resText = g.GetResource(key, this);
            return resText;
        }

        public virtual void LoadFavoritesOnline()
        {
            // create a client object
            using (System.Net.WebClient client = new System.Net.WebClient())
            {
                string xml = string.Format("brandID={0}&memberID={1}", _g.Brand.BrandID, _g.Member.MemberID);
                //string xml = string.Format("brandID={0}&memberID={1}", _g.Brand.BrandID, "100004510");

                // performs an HTTP POST
                string url = string.Format(FAVORITE_SERVICE_URL, Request.ServerVariables.Get("HTTP_HOST"));
                String response = string.Empty;
                try
                {
                    Stream stream = client.OpenRead(url + "?" + xml);
                    StreamReader reader = new StreamReader(stream);
                    response = reader.ReadToEnd();
                }
                catch (WebException ex)
                {
                    if (ex.Response is HttpWebResponse)
                    {
                        switch (((HttpWebResponse)ex.Response).StatusCode)
                        {
                            case HttpStatusCode.NotFound:
                                response = null;
                                break;

                            default:
                                throw ex;
                        }
                    }
                }

                string str = "<string xmlns=\"http://spark.net/API\">";
                int startIdx = response.IndexOf(str) + str.Length;
                int jsonLength = response.IndexOf("</string>") - startIdx;
                string jsonString = response.Substring(startIdx, jsonLength);
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                Favorites = jsSerializer.Deserialize<List<FavoriteMember>>(jsonString);
            }
        }

    }
}
