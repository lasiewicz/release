﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Managers;
using Spark.FacebookLike.ValueObjects;

namespace Matchnet.Web.Applications.API
{
    [WebService(Namespace = "http://spark.net/API")]
    [System.Web.Script.Services.ScriptService]
    public class Facebook : System.Web.Services.WebService
    {
        private ContextGlobal _g;

        public Facebook()
		{
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }
		}

        /// <summary>
        /// Saves facebook connect info
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public GetFBLikesResult GetFacebookLikesByCategoryGroup(int memberID, int chunkStartIndex, int categoryGroupID)
        {
            GetFBLikesResult result = new GetFBLikesResult();

            try
            {
                if (_g != null)
                {
                    System.Diagnostics.Trace.WriteLine("GetFacebookLikesByCategoryGroup(" + memberID.ToString() + ", " + chunkStartIndex + ", " + categoryGroupID.ToString() + ")");
                    result.FacebookLikes = FacebookManager.Instance.GetFacebookLikesByCategoryGroup(memberID, _g.Brand, chunkStartIndex, categoryGroupID, true);

                    result.Status = (int)JSONStatus.Success;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Saves facebook connect info
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public UpdateResult UpdateFacebookConnect(string fbUserID, string accessToken, int expiresInSeconds)
        {
            UpdateResult result = new UpdateResult();

            try
            {
                if (_g != null && _g.Member != null && _g.Member.MemberID > 0)
                {
                    System.Diagnostics.Trace.WriteLine("UpdateFacebookConnect(" + fbUserID.ToString() + ", " + accessToken + ", " + expiresInSeconds.ToString() + ")");
                    Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(_g.Member.MemberID, MemberLoadFlags.None);
                    FacebookManager.Instance.UpdateFacebookConnect(member, _g.Brand, fbUserID, accessToken, expiresInSeconds);
                }

                result.Status = (int)JSONStatus.Success;
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Saves facebook likes selected by user
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public SaveFBLikesResult SaveFacebookLikes(int memberID, string fbLikes)
        {
            SaveFBLikesResult result = new SaveFBLikesResult();
            result.Status = (int)JSONStatus.Success;

            try
            {
                if (_g != null && _g.Member != null && memberID == _g.Member.MemberID && !string.IsNullOrEmpty(fbLikes))
                {
                    //create fb likes VO and save
                    System.Diagnostics.Trace.WriteLine("SaveFacebookLikes(" + memberID.ToString() + ", " + fbLikes + ")");

                    string[] fbLikesList = fbLikes.Split(new string[] { "sparkobj&&" }, StringSplitOptions.RemoveEmptyEntries);
                    if (fbLikesList.Length > 0)
                    {
                        List<FacebookLikeParams> facebookLikeParamsList = new List<FacebookLikeParams>();
                        foreach (string fbLike in fbLikesList)
                        {
                            FacebookLikeParams facebookLikeParams = new FacebookLikeParams();
                            facebookLikeParams.MemberId = memberID;
                            facebookLikeParams.SiteId = _g.Brand.Site.SiteID;

                            string[] fbLikeParams = fbLike.Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string fbLikeParam in fbLikeParams)
                            {
                                string[] param = fbLikeParam.Split(new string[] { "==" }, StringSplitOptions.RemoveEmptyEntries);
                                if (param.Length == 2)
                                {
                                    switch (param[0].ToLower())
                                    {
                                        case "id":
                                            facebookLikeParams.FacebookId = Convert.ToInt64(param[1]); break;
                                        case "name":
                                            facebookLikeParams.FbName = param[1];
                                            break;
                                        case "categoryid":
                                            facebookLikeParams.FbLikeCategoryId = Convert.ToInt32(param[1]);
                                            break;
                                        case "categorygroupid":
                                            break;
                                        case "picture":
                                            facebookLikeParams.FbImageUrl = param[1];
                                            break;
                                        case "link":
                                            facebookLikeParams.FbLinkHref = param[1];
                                            break;
                                        case "type":
                                            facebookLikeParams.FbType = param[1];
                                            break;
                                        case "approved":
                                            if (param[1] == "true")
                                            {
                                                facebookLikeParams.FbLikeStatus = Spark.FacebookLike.ValueObjects.ServiceDefinitions.FacebookLikeStatus.Approved;
                                            }
                                            break;
                                    }
                                }
                            }

                            if (facebookLikeParams.FacebookId > 0)
                            {
                                facebookLikeParamsList.Add(facebookLikeParams);
                            }
                        }

                        //result.Status = (int)JSONStatus.Success;
                        FacebookLikeSaveResult fbResult = FacebookManager.Instance.SaveFacebookLikes(facebookLikeParamsList, _g.Member, _g.Brand);

                        if (fbResult.SaveStatus == FacebookLikeSaveStatusType.Success)
                        {
                            result.Status = (int)JSONStatus.Success;

                        }
                        else if (fbResult.SaveStatus == FacebookLikeSaveStatusType.PartialSuccess)
                        {
                            result.Status = (int)JSONStatus.Success;
                            if (fbResult.FailedFacebookLikeIds != null && fbResult.FailedFacebookLikeIds.Count > 0)
                            {
                                foreach (long l in fbResult.FailedFacebookLikeIds)
                                {
                                    result.FailedFacebookLikeIDs.Add(l);
                                }
                            }
                        }
                        else
                        {
                            result.Status = (int)JSONStatus.Failed;
                            //TODO - Determine why it failed and get failed message
                            result.StatusMessage = "Failed to save facebook likes, error occurred in service.  Please try again.";
                        }
                    }
                }
                else
                {
                    result.Status = (int)JSONStatus.Failed;
                    if (string.IsNullOrEmpty(fbLikes))
                    {
                        result.StatusMessage = "Failed to save facebook likes. No saved likes selected.";
                    }
                    else
                    {
                        result.StatusMessage = "Failed to save facebook likes. Member is not recognized, you may need to refresh the page or login again.";
                    }
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Clears all facebook likes for user
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public UpdateResult ClearFacebookLikes(int memberID)
        {
            UpdateResult result = new UpdateResult();
            result.Status = (int)JSONStatus.Success;
            try
            {
                if (_g != null && _g.Member != null && memberID == _g.Member.MemberID)
                {
                    //create fb likes VO and save
                    System.Diagnostics.Trace.WriteLine("ClearFacebookLikes(" + memberID.ToString() + ")");
                    FacebookLikeSaveResult fbResult = FacebookManager.Instance.ClearFacebookLikes(_g.Member, _g.Brand);

                    if (fbResult.SaveStatus == FacebookLikeSaveStatusType.Success || fbResult.SaveStatus == FacebookLikeSaveStatusType.PartialSuccess)
                    {
                        result.Status = (int)JSONStatus.Success;
                    }
                    else
                    {
                        result.Status = (int)JSONStatus.Failed;
                        //TODO - Determine why it failed and get failed message
                        result.StatusMessage = "Failed to remove facebook likes, error occurred in service.  Please try again.";
                    }
                }
                else
                {
                    result.Status = (int)JSONStatus.Failed;
                    result.StatusMessage = "Failed to remove facebook likes. Member is not recognized, you may need to refresh the page or login again.";
                }
                
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }
        
    }
}
