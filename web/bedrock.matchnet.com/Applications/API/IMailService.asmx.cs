﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Matchnet.Content.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.ActivityRecording.ValueObjects;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Web.Applications.Email;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Ui.Feedback;
using System.Text.RegularExpressions;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for IMailService
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class IMailService : System.Web.Services.WebService
    {

        private ContextGlobal _g;

        private System.Web.UI.Control ComposeAPIResource
        {
            get
            {
                System.Web.UI.Page page = new System.Web.UI.Page();
                return page.LoadControl("/Applications/Email/ComposeAPIResource.ascx");
            }
        }

        public IMailService()
        {
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }
        }

        /// <summary>
        /// This is used to send a QnA email comment
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public Data SendComment(int brandId, string elementId, int questionId, int answerId, int toMemberId, int fromMemberId, string fromUserName, string mailSubject, string mailMessage)
        {
            Data data = new Data();
            data.ElementId = elementId;
            try
            {
                if (_g != null && _g.Member != null)
                {
                    _g.Brand = BrandConfigSA.Instance.GetBrandByID(brandId);
                    if (Matchnet.Web.Applications.QuestionAnswer.QuestionAnswerHelper.IsQuestionAnswerCommentEnabled(_g.Brand))
                    {
                        Matchnet.Member.ServiceAdapters.Member fromMember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(fromMemberId, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                        if (null != fromMember && !string.IsNullOrEmpty(mailMessage) && fromMember.IsPayingMember(_g.Brand.Site.SiteID))
                        {
                            if (fromMemberId != toMemberId && toMemberId > 0)
                            {
                                StringBuilder message = new StringBuilder(DecodeHexEntities(mailMessage));
                                message.AppendLine("");
                                string sFormat = "{{QuestionId:{0},AnswerId:{1},FromMemberId:{2},FromMemberName:'{3}'}}";
                                message.AppendLine(string.Format(sFormat, questionId, answerId, fromMemberId, DecodeHexEntities(fromUserName)));
                                Matchnet.Email.ValueObjects.MailType mailType = Matchnet.Email.ValueObjects.MailType.QandAComment;
                                string subject = DecodeHexEntities(mailSubject);
                                ComposeResult composeResult = Matchnet.Web.Applications.Email.ComposeHelper.CompileSendMessage(false, false, fromMemberId, toMemberId, subject, message.ToString(), mailType, true, Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT, false, ComposeAPIResource, _g);
                                data.Status = composeResult.ComposeSuccess;
                                data.StatusID = data.Status ? (int)JSONStatus.Success : (int)JSONStatus.Failed;
                            }
                            else
                            {
                                System.Diagnostics.EventLog.WriteEntry("WWW", string.Format("Did not attempt send comment for {{fromMemberId:{0},answerId:{1},toMemberId:(2)}}", fromMemberId, answerId, toMemberId));
                            }
                        }
                        else
                        {
                            data.Status = false;
                            data.StatusID = (int)JSONStatus.Failed;
                            data.Redirect = true;
                        }
                    }
                    else
                    {
                        data.StatusID = (int)JSONStatus.Failed;
                        data.Status = false;
                    }
                }
                else
                {
                    data.StatusID = (int)JSONStatus.SessionExpired;
                    data.Status = false;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                data.Status = false;
                data.Redirect = false;
                data.StatusID = (int)JSONStatus.Failed;
            }

            if (data.Status)
            {
                try
                {
                    //activity recording
                    ActivityRecordingSA.Instance.RecordActivity(
                        fromMemberId,
                        toMemberId,
                        _g.Brand.Site.SiteID,
                        ActivityRecording.ValueObjects.Types.ActionType.CommentAnswer,
                        ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
                        string.Format("<QuestionID>{0}</QuestionID>", questionId));
                }
                catch (Exception logEx)
                {
                    string message = "ActivityRecording threw an exception: " + logEx.Message;
                    System.Diagnostics.EventLog.WriteEntry(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, message, System.Diagnostics.EventLogEntryType.Error);
                }
            }

            return data;
        }

        /// <summary>
        /// This is used to send a QnA email comment
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public Data FreeTextSendComment(int brandId, string elementId, int toMemberId, int fromMemberId, string fromUserName, string mailSubject, string sectionAttribute, string mailMessage)
        {
            Data data = new Data();
            data.ElementId = elementId;
            try
            {
                if (_g != null && _g.Member != null)
                {
                    _g.Brand = BrandConfigSA.Instance.GetBrandByID(brandId);
                    if (Matchnet.Web.Applications.QuestionAnswer.QuestionAnswerHelper.IsQuestionAnswerCommentEnabled(_g.Brand))
                    {
                        Matchnet.Member.ServiceAdapters.Member fromMember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(fromMemberId, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                        if (null != fromMember && !string.IsNullOrEmpty(mailMessage) && fromMember.IsPayingMember(_g.Brand.Site.SiteID))
                        {
                            if (fromMemberId != toMemberId && toMemberId > 0)
                            {
                                StringBuilder message = new StringBuilder(DecodeHexEntities(mailMessage));
                                message.AppendLine("");
                                string sFormat = "{{FromMemberId:{0},FromMemberName:'{1}',SectionAttribute:'{2}'}}";
                                message.AppendLine(string.Format(sFormat, fromMemberId, DecodeHexEntities(fromUserName), DecodeHexEntities(sectionAttribute)));
                                Matchnet.Email.ValueObjects.MailType mailType = Matchnet.Email.ValueObjects.MailType.FreeTextComment;
                                string subject = DecodeHexEntities(mailSubject);
                                //subject = string.Format(subject, fromUserName, DecodeHexEntities(sectionAttribute));

                                ComposeResult composeResult = Matchnet.Web.Applications.Email.ComposeHelper.CompileSendMessage(false, false, fromMemberId, toMemberId, subject, message.ToString(), mailType, true, Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT, false, ComposeAPIResource, _g);
                                data.Status = composeResult.ComposeSuccess;
                                data.StatusID = data.Status ? (int)JSONStatus.Success : (int)JSONStatus.Failed;
                            }
                            else
                            {
                                System.Diagnostics.EventLog.WriteEntry("WWW", string.Format("Did not attempt send free text comment for {{fromMemberId:{0},toMemberId:(1)}}", fromMemberId, toMemberId));
                            }
                        }
                        else
                        {
                            data.Status = false;
                            data.Redirect = true;
                            data.StatusID = (int)JSONStatus.Failed;
                        }
                    }
                    else
                    {
                        data.Status = false;
                        data.StatusID = (int)JSONStatus.Failed;
                    }
                }
                else
                {
                    data.StatusID = (int)JSONStatus.SessionExpired;
                    data.Status = false;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                data.Status = false;
                data.Redirect = false;
                data.StatusID = (int)JSONStatus.Failed;
            }

            if (data.Status)
            {
                try
                {
                    //activity recording
                    ActivityRecordingSA.Instance.RecordActivity(
                        fromMemberId,
                        toMemberId,
                        _g.Brand.Site.SiteID,
                        ActivityRecording.ValueObjects.Types.ActionType.CommentAnswer,
                        ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
                        "Sending FreeTextComment");
                }
                catch (Exception logEx)
                {
                    string message = "ActivityRecording threw an exception: " + logEx.Message;
                    System.Diagnostics.EventLog.WriteEntry(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, message, System.Diagnostics.EventLogEntryType.Error);
                }
            }

            return data;
        }

        /// <summary>
        /// This is used to send a photo email comment
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public Data PhotoSendComment(int brandId, string elementId, int toMemberId, int fromMemberId, string fromUserName, string mailSubject, int photoID, string mailMessage)
        {
            Data data = new Data();
            data.ElementId = elementId;
            try
            {
                if (_g != null && _g.Member != null)
                {
                    _g.Brand = BrandConfigSA.Instance.GetBrandByID(brandId);
                    if (Matchnet.Web.Applications.QuestionAnswer.QuestionAnswerHelper.IsQuestionAnswerCommentEnabled(_g.Brand))
                    {
                        Matchnet.Member.ServiceAdapters.Member fromMember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(fromMemberId, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                        if (null != fromMember && !string.IsNullOrEmpty(mailMessage) && fromMember.IsPayingMember(_g.Brand.Site.SiteID))
                        {
                            if (fromMemberId != toMemberId && toMemberId > 0)
                            {
                                StringBuilder message = new StringBuilder(DecodeHexEntities(mailMessage));
                                message.AppendLine("");
                                string sFormat = "{{PhotoID:'{0}'}}";
                                message.AppendLine(string.Format(sFormat, photoID));
                                Matchnet.Email.ValueObjects.MailType mailType = Matchnet.Email.ValueObjects.MailType.PhotoComment;
                                string subject = DecodeHexEntities(mailSubject);
                                //subject = string.Format(subject, fromUserName, photoID);

                                ComposeResult composeResult = Matchnet.Web.Applications.Email.ComposeHelper.CompileSendMessage(false, false, fromMemberId, toMemberId, subject, message.ToString(), mailType, true, Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT, false, ComposeAPIResource, _g);
                                data.Status = composeResult.ComposeSuccess;
                                data.StatusID = data.Status ? (int)JSONStatus.Success : (int)JSONStatus.Failed;
                            }
                            else
                            {
                                System.Diagnostics.EventLog.WriteEntry("WWW", string.Format("Did not attempt send photo comment for {{fromMemberId:{0},toMemberId:(1)}}", fromMemberId, toMemberId));
                            }
                        }
                        else
                        {
                            data.Status = false;
                            data.Redirect = true;
                            data.StatusID = (int)JSONStatus.Failed;
                        }
                    }
                    else
                    {
                        data.Status = false;
                        data.StatusID = (int)JSONStatus.Failed;
                    }
                }
                else
                {
                    data.StatusID = (int)JSONStatus.SessionExpired;
                    data.Status = false;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                data.Status = false;
                data.Redirect = false;
                data.StatusID = (int)JSONStatus.Failed;
            }

            if (data.Status)
            {
                try
                {
                    //activity recording
                    ActivityRecordingSA.Instance.RecordActivity(
                        fromMemberId,
                        toMemberId,
                        _g.Brand.Site.SiteID,
                        ActivityRecording.ValueObjects.Types.ActionType.PhotoComment,
                        ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
                        "Sending PhotoComment");
                }
                catch (Exception logEx)
                {
                    string message = "ActivityRecording threw an exception: " + logEx.Message;
                    System.Diagnostics.EventLog.WriteEntry(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, message, System.Diagnostics.EventLogEntryType.Error);
                }
            }

            return data;
        }

        /// <summary> 
        /// This is used to send an internal email message from member to member
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public SendMessageResult SendMessage(int targetMemberID, string messageSubject, string message, bool sendAsVIP, bool sendAsVIPFromLayoverButton, bool saveAsDraft)
        {
            bool result = false;

            SendMessageResult sendMessageResult = new SendMessageResult();
            try
            {
                //get control strictly for resources
                System.Web.UI.Control ComposeResourceControl = ComposeAPIResource;

                if (_g != null && _g.Member != null)
                {
                    if (targetMemberID > 0 && messageSubject != "" && message.Trim() != "")
                    {
                        bool canNonsubSendFreeEmail = ComposeHandler.canNonSubSendFreeEmail(_g);
                        if (!canNonsubSendFreeEmail && ComposeHandler.canNonsubSaveAsDraft(_g))
                        {
                            saveAsDraft = true;
                        }

                        sendMessageResult.MemberID = _g.Member.MemberID;
                        if (ComposeHelper.PassesIMailFraudCheck(_g) && ComposeHelper.PassesSendNonReplyIMailCheck(_g) || ComposeHandler.canNonsubSaveAsDraft(_g))
                        {

                            messageSubject = FrameworkGlobals.DecodeHexEntities(messageSubject);
                            message = FrameworkGlobals.DecodeHexEntities(message.Trim());

                            //send message
                            ComposeResult composeResult = Matchnet.Web.Applications.Email.ComposeHelper.CompileSendMessage(saveAsDraft, false, _g.Member.MemberID, targetMemberID, messageSubject, message.ToString(), MailType.Email, true, Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT, sendAsVIP, ComposeResourceControl, _g);
                            if (composeResult != null && composeResult.ComposeSuccess)
                            {
                                result = true;
                                sendMessageResult.Status = (int)JSONStatus.Success;
                                sendMessageResult.MemberMailID = composeResult.membermailid;
                                if (sendAsVIP && saveAsDraft)
                                {
                                    //get upsale url
                                    sendMessageResult.UpsaleURL = VIPMailUtils.GetUpsaleURL(_g, sendAsVIPFromLayoverButton, _g.Brand, targetMemberID, false, composeResult.membermailid);
                                }
                                else if (!canNonsubSendFreeEmail && ComposeHandler.canNonsubSaveAsDraft(_g))
                                {
                                    sendMessageResult.UpsaleURL = VIPMailUtils.GetRegularSubscriptionURL(_g, sendAsVIPFromLayoverButton, _g.Brand, targetMemberID, false, composeResult.membermailid);
                                }
                            }
                            else
                            {
                                if (composeResult != null && composeResult.messageSendResult != null)
                                {
                                    sendMessageResult.MessageSendErrorID = (int)composeResult.messageSendResult.Error;
                                    if (composeResult.messageSendResult.Error == MessageSendError.ComposeLimitReached)
                                    {
                                        if (canNonsubSendFreeEmail)
                                        {
                                            sendMessageResult.Status = (int)JSONStatus.SubNeeded;
                                            sendMessageResult.UpsaleURL = VIPMailUtils.GetRegularSubscriptionURL(_g, sendAsVIPFromLayoverButton, _g.Brand, targetMemberID, false, composeResult.membermailid);
                                        }
                                        else
                                        {
                                            throw new Exception(_g.GetResource("TXT_COMPOSE_LIMIT_REACHED", ComposeResourceControl));
                                        }
                                    }
                                }
                                throw new Exception(_g.GetResource("TXT_ERROR_SENDING_MESSAGE", ComposeResourceControl));
                            }

                            if (!result)
                            {
                                // deduct from session All Access Overlay Counter
                                if (VIPMailUtils.isDisplayOverlayMaxReached(_g))
                                {
                                    _g.UpdateSessionInt("VIP_COMPOSE_OVERLAY_PER_SESSION", -1, 0);
                                    //GetVIPAllAccessOverlay(IsPostBack);
                                }
                            }
                            else
                            {
                                if (VIPMailUtils.isDisplayOverlayMaxReached(_g))
                                {
                                    _g.UpdateSessionInt("VIP_COMPOSE_OVERLAY_PER_SESSION", 1, 0);
                                }
                            }
                        }
                        else
                        {
                            throw new Exception(_g.GetResource("TXT_ERROR_SENDING_MESSAGE", ComposeResourceControl));
                        }
                    }
                    else
                    {
                        throw new Exception(_g.GetResource("TXT_MISSING_DATA_FOR_SENDING_EMAIL", ComposeResourceControl));
                    }
                }
                else
                {
                    //session expired
                    sendMessageResult.Status = (int)JSONStatus.SessionExpired;
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                sendMessageResult.Status = (int)JSONStatus.Failed;
                sendMessageResult.StatusMessage = ex.Message;
            }

            return sendMessageResult;// new JavaScriptSerializer().Serialize(updateResult);
        }



        /// <summary>
        /// This is used to send an internal email draft message from member to member
        /// </summary>
        /// <param name="targetMemberID"></param>
        /// <param name="messageID"></param>
        /// <param name="messageSubject"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public SendMessageResult SaveMessageDraft(int targetMemberID, int messageID, string messageSubject, string message)
        {

            if(messageSubject != "")
                messageSubject =  HttpUtility.HtmlEncode(messageSubject);

            if(message != "")
                message = HttpUtility.HtmlEncode(message);
         
            SendMessageResult sendMessageResult = new SendMessageResult();
            try
            {
                //get control strictly for resources
                System.Web.UI.Control ComposeResourceControl = ComposeAPIResource;

                if (_g != null && _g.Member != null)
                {
                    if (targetMemberID > 0 && messageID > 0  && messageSubject != "" && message.Trim() != "")
                    {
                        sendMessageResult.MemberID = _g.Member.MemberID;
                        if (ComposeHelper.PassesIMailFraudCheck(_g) && ComposeHelper.PassesSendNonReplyIMailCheck(_g) || ComposeHandler.canNonsubSaveAsDraft(_g))
                        {

                            messageSubject = FrameworkGlobals.DecodeHexEntities(messageSubject);
                            message = FrameworkGlobals.DecodeHexEntities(message.Trim());

                            //send message
                            ComposeResult composeResult = ComposeHelper.CompileSendMessage(true, 
                                                                    false, 
                                                                    _g.Member.MemberID, 
                                                                    targetMemberID, 
                                                                    messageSubject, 
                                                                    message.ToString(), 
                                                                    MailType.Email, 
                                                                    true, 
                                                                    messageID, 
                                                                    Constants.NULL_INT, 
                                                                    Constants.NULL_INT, 
                                                                    false, 
                                                                    ComposeResourceControl, 
                                                                    _g);
                            
                            if (composeResult != null && composeResult.ComposeSuccess)
                            {
                                sendMessageResult.Status = (int)JSONStatus.Success;
                                sendMessageResult.MemberMailID = composeResult.membermailid;
                                                            }
                            else
                            {
                                
                                throw new Exception(_g.GetResource("TXT_ERROR_SENDING_MESSAGE", ComposeResourceControl));
                            }

                             
                        }
                        else
                        {
                            throw new Exception(_g.GetResource("TXT_ERROR_SENDING_MESSAGE", ComposeResourceControl));
                        }
                    }
                    else
                    {
                        throw new Exception(_g.GetResource("TXT_MISSING_DATA_FOR_SENDING_EMAIL", ComposeResourceControl));
                    }
                }
                else
                {
                    //session expired
                    sendMessageResult.Status = (int)JSONStatus.SessionExpired;
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                sendMessageResult.Status = (int)JSONStatus.Failed;
                sendMessageResult.StatusMessage = ex.Message;
            }

            return sendMessageResult;
        }





        /// <summary>
        /// This is used to send a Feedback email for things like Beta feature
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public UpdateResult SendFeedback(int feedbackTypeID, string senderName, string senderEmail, string message)
        {
            UpdateResult updateResult = new UpdateResult();
            try
            {
                if (_g != null && _g.Member != null)
                {
                    if (senderName.Trim() != "" && senderEmail.Trim() != null && message.Trim() != "")
                    {
                        message = FrameworkGlobals.DecodeHexEntities(message.Trim());
                        senderName = FrameworkGlobals.DecodeHexEntities(senderName.Trim());
                        senderEmail = FrameworkGlobals.DecodeHexEntities(senderEmail.Trim());
                        message = System.Text.RegularExpressions.Regex.Replace(message, "<[^>]*>", "");

                        if (!isValidEmail(senderEmail))
                            throw new Exception("Invalid email address");

                        FeedbackHelper.SendBetaFeedbackEmail((FeedbackHelper.FeedbackType)Enum.Parse(typeof(FeedbackHelper.FeedbackType), feedbackTypeID.ToString()), _g.Brand.BrandID, senderName, senderEmail, _g.Member.MemberID, message);
                        updateResult.Status = (int)JSONStatus.Success;
                    }
                    else
                    {
                        throw new Exception("Missing data for sending feeback");
                    }
                }
                else
                {
                    //session expired
                    updateResult.Status = (int)JSONStatus.SessionExpired;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                updateResult.Status = (int)JSONStatus.Failed;
                updateResult.StatusMessage = ex.Message;
            }

            return updateResult;// new JavaScriptSerializer().Serialize(updateResult);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public UpdateResult SendMessageLocale(int targetMemberID, string messageSubject, string message, string profileType)
        {
            UpdateResult updateResult = new UpdateResult();
            try
            {

                if (_g != null && targetMemberID > 0)
                {
                    if (_g.Member != null)
                    {

                        if (messageSubject == "")
                        {
                            throw new Exception(_g.GetResource("TXT_MESSAGE_SUBJECT_BLANK"));
                        }

                        if (message.Trim() == "")
                        {
                            throw new Exception(_g.GetResource("TXT_MESSAGE_BODY_BLANK"));
                        }

                        if (ComposeHelper.PassesIMailFraudCheck(_g))
                        {
                            messageSubject = ComposeHandler.CleanQuickMessageSubject(messageSubject);
                            message = ComposeHandler.CleanQuickMessageBody(message);
                            if (ComposeHelper.PassesSendNonReplyIMailCheck(_g))
                            {
                                var toMember = MemberSA.Instance.GetMember(targetMemberID, MemberLoadFlags.None);

                                ComposeResult composeResult =
                               Matchnet.Web.Applications.Email.ComposeHelper.CompileSendMessage(false, false,
                                                                                                _g.Member.
                                                                                                    MemberID,
                                                                                                targetMemberID,
                                                                                                messageSubject,
                                                                                                message,
                                                                                                MailType.Email,
                                                                                                true,
                                                                                                Constants.
                                                                                                    NULL_INT,
                                                                                                Constants.
                                                                                                    NULL_INT,
                                                                                                Constants.
                                                                                                    NULL_INT,
                                                                                                false, ComposeAPIResource, _g);

                                if (composeResult.messageSendResult.Status == MessageSendStatus.Success)
                                {
                                    updateResult.Status = (int)JSONStatus.Success;
                                    updateResult.StatusMessage =
                                        _g.GetResource("TXT_MESSAGE_SUCCESSFULLY_SENT_TO_USERNAME").Replace(
                                            "((USERNAME))",
                                            toMember.GetUserName(_g.Brand) ?? targetMemberID.ToString());
                                }
                                else
                                {
                                    if (composeResult.messageSendResult.Error == MessageSendError.ComposeLimitReached)
                                    {
                                        throw new Exception(_g.GetResource("TXT_QUICK_MESSAGE_COMPOSE_LIMIT_REACHED"));
                                    }
                                    else
                                    {
                                        throw new Exception(_g.GetResource("ERROR_SENDING_MESSAGE"));
                                    }

                                }
                            }
                            else
                            {
                                updateResult = SetQuickMessageCookie(updateResult, profileType, messageSubject, message,
                                                             targetMemberID, _g.Member.MemberID);
                            }
                        }
                        else
                        {
                            throw new Exception(_g.GetResource("ERROR_SENDING_MESSAGE"));
                        }
                    }
                    else
                    {
                        // Member is null
                        //session expired
                        updateResult.Status = (int)JSONStatus.SessionExpired;
                    }
                }
                else
                {
                    throw new Exception("Missing data for sending email");
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                updateResult.Status = (int)JSONStatus.Failed;
                updateResult.StatusMessage = ex.Message;
            }

            return updateResult;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public IMHistoryMemberListResult GetIMHistoryMemberListBatch(int startRow, int pageSize)
        {
            IMHistoryMemberListResult response = new IMHistoryMemberListResult();

            try
            {
                if (_g != null && _g.Member != null)
                {
                    System.Web.UI.Page pResource = new System.Web.UI.Page();
                    FrameworkControl mailboxResourceControl = pResource.LoadControl("/Applications/Email/MailboxResource.ascx") as FrameworkControl;

                    //temp dummy data
                    response = IMManager.Instance.GetIMHistoryMembersDummyDataForJSON(_g.Brand, _g.Member.MemberID, startRow, pageSize, mailboxResourceControl);

                    response.Status = (int)JSONStatus.Success;
                    response.StatusMessage = "Success";
                }
                else
                {
                    response.Status = (int)JSONStatus.SessionExpired;
                    response.StatusMessage = "Not logged in";
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                response.Status = (int)JSONStatus.Failed;
                response.StatusMessage = "Error: " + ex.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public IMHistoryMessageListResult GetIMHistoryMessageListBatch(int startRow, int pageSize, int targetMemberID)
        {
            IMHistoryMessageListResult response = new IMHistoryMessageListResult();

            try
            {
                if (_g != null && _g.Member != null)
                {
                    System.Web.UI.Page pResource = new System.Web.UI.Page();
                    FrameworkControl mailboxResourceControl = pResource.LoadControl("/Applications/Email/MailboxResource.ascx") as FrameworkControl;

                    //temp dummy data
                    response = IMManager.Instance.GetIMHistoryMessagesDummyDataForJSON(_g.Brand, _g.Member.MemberID, targetMemberID, startRow, pageSize, mailboxResourceControl);

                    response.Status = (int)JSONStatus.Success;
                    response.StatusMessage = "Success";
                }
                else
                {
                    response.Status = (int)JSONStatus.SessionExpired;
                    response.StatusMessage = "Not logged in";
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                response.Status = (int)JSONStatus.Failed;
                response.StatusMessage = "Error: " + ex.Message;
            }
            return response;
        }

        private static UpdateResult SetQuickMessageCookie(UpdateResult updateResult, string profileType, string messageSubject, string message, int targetMemberID, int senderMemberID)
        {
            updateResult.Status = (int)JSONStatus.SubNeeded;
            int prtid = 1108;
            if (profileType == "gallery")
            {
                prtid = 1109;
            }
            updateResult.SubPageURL = FrameworkGlobals.GetSubscriptionLink(false, prtid);
            string cookieValue =
                string.Format("MessageSubject={0}&MessageBody={1}&TargetMemberID={2}&SenderMemberID={3}",
                              HttpUtility.UrlEncode(messageSubject),
                              HttpUtility.UrlEncode(message),
                              targetMemberID.ToString(),
                              senderMemberID.ToString());

            HttpCookie cookie = new HttpCookie("QuickMessage", cookieValue);
            cookie.Expires = DateTime.Now.AddDays(1);
            HttpContext.Current.Response.Cookies.Add(cookie);

            return updateResult;
        }

        public string DecodeHexEntities(string hexString)
        {
            return FrameworkGlobals.DecodeHexEntities(hexString);
        }

        private bool isValidEmail(string anEmailAddress)
        {
            bool isValidEmail = true;
            Regex emailPattern = new Regex("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$");

            if (!emailPattern.IsMatch(anEmailAddress))
            {
                isValidEmail = false;
            }

            return isValidEmail;
        }

    }

    [Serializable]
    public class Data
    {
        public bool Status { get; set; }
        public bool Redirect { get; set; }
        public string ElementId { get; set; }
        public int StatusID { get; set; }
    }


}
