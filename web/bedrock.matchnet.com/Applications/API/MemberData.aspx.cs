using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.IO;
using System.Xml;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Summary description for MemberData.
	/// </summary>
	public class MemberData : System.Web.UI.Page
	{
		Matchnet.Member.ServiceAdapters.Member member;
		Content.ValueObjects.BrandConfig.Brand brand;

		private void Page_Load(object sender, System.EventArgs e)
		{
			Int32 memberID = Conversion.CInt(Request["MemberID"]);
			Int32 brandID = Conversion.CInt(Request["BrandID"]);

			if(brandID == Constants.NULL_INT)
			{
				brandID = 1001;
			}

			brand = BrandConfigSA.Instance.GetBrandByID(brandID);
			member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

			System.IO.Stream s = HttpContext.Current.Response.OutputStream;
			XmlTextWriter writer = new XmlTextWriter(s, System.Text.Encoding.ASCII);
			
			writer.Formatting = System.Xml.Formatting.Indented;
			writer.WriteStartDocument();
			writer.WriteStartElement("Member");
			writer.WriteElementString("MemberID", member.MemberID.ToString());
			writer.WriteElementString("Username", member.GetUserName(brand).ToString());
			writer.WriteElementString("EmailAddress", member.EmailAddress.ToString());

			DateTime birthDate = member.GetAttributeDate(brand, "BirthDate", DateTime.MinValue);
			writer.WriteElementString("Age", FrameworkGlobals.GetAge(birthDate).ToString());
			
			writer.WriteElementString("Region", 
					Framework.FrameworkGlobals.GetRegionString(
						member.GetAttributeInt(brand, "regionid"), brand.Site.LanguageID, false,true,false));

		    var approvedPhotosList = MemberPhotoDisplayManager.Instance.GetApprovedPhotos(member, member,
		                                                                               brand);
            var approvedPhotos = new ArrayList(approvedPhotosList);

			writer.WriteElementString("Photo1", getPhotoPath(1, approvedPhotos));
			writer.WriteElementString("Photo2", getPhotoPath(2, approvedPhotos));
			writer.WriteElementString("Photo3", getPhotoPath(3, approvedPhotos));
			writer.WriteElementString("Photo4", getPhotoPath(4, approvedPhotos));

			writer.WriteEndElement();
			writer.WriteEndDocument();
			writer.Close();
		}

		public string getPhotoPath(int photoOrdinal, ArrayList approvedPhotos)
		{
			if (approvedPhotos != null && approvedPhotos.Count >= photoOrdinal)
			{
				Matchnet.Member.ValueObjects.Photos.Photo photo;
				try
				{
					photo = (Matchnet.Member.ValueObjects.Photos.Photo)approvedPhotos[photoOrdinal];
				}
				catch
				{
					return String.Empty;
				}

				if(photo != null)
				{
                    //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                    return MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(member, member, brand, photo, PhotoType.Thumbnail,
				                                                PrivatePhotoImageType.Thumb);
				}
			}

			return String.Empty;
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
