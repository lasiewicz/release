<%@ Page language="c#" Codebehind="CheckAlerts.aspx.cs" AutoEventWireup="false" Inherits="Matchnet.Web.Applications.API.CheckAlerts" %>

<script type="text/javascript">
	function _reload()
	{
		location.replace('<%= Location %>');
	}

	//auto-refresh this page every 5 seconds
	setTimeout('_reload()', 5000);
</script>

<asp:Repeater ID="rptAlerts" Runat="server">
	<HeaderTemplate>
		<script type="text/javascript">
	</HeaderTemplate>
	<ItemTemplate>
		<asp:Literal id="litAlert" Runat="server" />
	</ItemTemplate>
	<FooterTemplate>
		</script>
	</FooterTemplate>
</asp:Repeater>
