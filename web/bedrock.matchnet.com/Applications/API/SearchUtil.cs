using System;
using System.Collections;
using System.Xml;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Summary description for SearchUtil.
	/// 
	/// This utility class has been developed to assist in the front-end
	/// prototype for working with search results from the FAST service
	/// in XML format	K.Landrus 1/4/05
	/// </summary>
	public class SearchUtil
	{
		private SearchUtil()
		{
		}

		public static SearchPreferenceCollection ConvertRequestToSearchPreferences(
			System.Collections.Specialized.NameValueCollection pRequestParams
			, ContextGlobal g)
		{
			SearchPreferenceCollection searchPreferences = new SearchPreferenceCollection();
			if (pRequestParams != null)
			{	
				searchPreferences.Add("CommunityID", g.Brand.Site.Community.CommunityID.ToString());

				// GenderMask
				int maskValue = 0;
				string gender = string.Empty;
				string seeking = string.Empty;

				if (pRequestParams["Gender"] != null)
				{
					gender = pRequestParams["Gender"].ToLower();
				}
				if (pRequestParams["Seeking"] != null)
				{
					seeking = pRequestParams["Seeking"].ToLower();
				}

				if ("male".Equals(gender) && "female".Equals(seeking))
				{	// male seeking female
					maskValue = 6;
				}
				else
				{	// female seeking male
					maskValue = 9;
				}
				searchPreferences.Add("GenderMask", maskValue.ToString());

				if (pRequestParams["RegionID"] != null)
				{
					searchPreferences.Add("RegionID", pRequestParams["RegionID"]);
				}
				else	// Default a valid region so the search will return some results, using BH, CA
				{
					searchPreferences.Add("RegionID", "3443817");
				}

				if (pRequestParams["Distance"] != null)
				{
					searchPreferences.Add("Distance", pRequestParams["Distance"]);
				}
				else
				{
					searchPreferences.Add("Distance", g.Brand.DefaultSearchRadius.ToString());
				}

				if (pRequestParams["AgeMin"] != null)
				{
					searchPreferences.Add("AgeMin", pRequestParams["AgeMin"]);
				}
				else
				{
					searchPreferences.Add("AgeMin", g.Brand.DefaultAgeMin.ToString());
				}

				if (pRequestParams["AgeMax"] != null)
				{
					searchPreferences.Add("AgeMax", pRequestParams["AgeMax"]);
				}
				else
				{
					searchPreferences.Add("AgeMax", g.Brand.DefaultAgeMax.ToString());
				}

				if (pRequestParams["HasPhotoFlag"] != null)
				{
					searchPreferences.Add("HasPhotoFlag", pRequestParams["HasPhotoFlag"]);
				}
				else
				{
					searchPreferences.Add("HasPhotoFlag", "0");	// false
				}

				if (pRequestParams["SearchOrderBy"] != null)
				{
					searchPreferences.Add("SearchOrderBy", pRequestParams["SearchOrderBy"]);
				}
				else
				{
					searchPreferences.Add("SearchOrderBy"
						, ((int)Matchnet.Search.Interfaces.QuerySorting.Popularity).ToString());	
				}

				if (pRequestParams["SearchTypeID"] != null)
				{
					searchPreferences.Add("SearchTypeID", pRequestParams["SearchTypeID"]);
				}
				else
				{
					searchPreferences.Add("SearchTypeID"
						, ((int)Matchnet.Search.ValueObjects.SearchTypeID.Region).ToString());
				}
			}
			return searchPreferences;
		}

		/// <summary>
		/// This method transforms the search results for a Mode 1 Search (MemberIDs only)
		/// </summary>
		/// <param name="queryResults"></param>
		/// <returns></returns>
		public static XmlDocument TransformResultsToXml(MatchnetQueryResults queryResults)
		{
			XmlDocument xmlDoc = new XmlDocument();

			xmlDoc.LoadXml("<?xml version='1.0' encoding='iso-8859-1'?><SearchResults></SearchResults>");

			//TODO: Expose the individual preference values used to obtain these results...
			XmlElement questionElement = xmlDoc.CreateElement("question");
			xmlDoc.DocumentElement.AppendChild(questionElement);

			XmlElement answerElement = xmlDoc.CreateElement("answer");
			xmlDoc.DocumentElement.AppendChild(answerElement);

			XmlElement element;
			foreach (MatchnetResultItem resultItem in queryResults.Items.List)
			{
				// Create the Member node
				element = xmlDoc.CreateElement("Member"); 
				answerElement.AppendChild(element);
				element.SetAttribute("id", resultItem.MemberID.ToString());
			}
			return xmlDoc;
		}
	}
}
