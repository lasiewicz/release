﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Matchnet.Web.Framework;
using System.Web.Script.Services;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.API
{
    [WebService(Namespace = "http://spark.net/API")]
    [System.Web.Script.Services.ScriptService]
    public class Hotlist : System.Web.Services.WebService
    {
        private ContextGlobal _g;

        public Hotlist()
		{
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }
		}

        /// <summary>
        /// This updates the last viewed date for hotlist
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public UpdateResult YNMVote(int voteType, int fromMemberID, int toMemberID)
        {
            UpdateResult result = new UpdateResult();

            try
            {
                if (_g != null && _g.Member != null && _g.Member.MemberID == fromMemberID && toMemberID > 0 && voteType >= 0) 
                {
                    ListManager listManager = new ListManager();
                    listManager.YNMVote(voteType, fromMemberID, toMemberID, _g.Brand, _g);
                }

                result.Status = (int)JSONStatus.Success;
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// This updates the last viewed date for hotlist
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public UpdateResult MarkHotListLastViewed(string clearDate, string hotlistCategoryCommaDelimited)
        {
            UpdateResult result = new UpdateResult();

            try
            {
                if (_g != null && _g.Member != null && !string.IsNullOrEmpty(hotlistCategoryCommaDelimited))
                {
                    DateTime viewedDate = Convert.ToDateTime(clearDate);
                    string[] hotlistArray = hotlistCategoryCommaDelimited.Split(',');
                    if (hotlistArray.Length > 0)
                    {
                        List<HotListCategory> hotlistCategoryList = new List<HotListCategory>();
                        for (int i = 0; i < hotlistArray.Length; i++)
                        {
                            hotlistCategoryList.Add((HotListCategory)Enum.Parse(typeof(HotListCategory), hotlistArray[i].ToString()));
                        }
                        ListManager listManager = new ListManager();
                        listManager.MarkHotListLastViewed(_g.Member, _g.Brand, hotlistCategoryList, viewedDate);
                    }
                }

                result.Status = (int)JSONStatus.Success;
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }
    }
}
