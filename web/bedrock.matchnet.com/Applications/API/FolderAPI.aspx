<%@ Page language="c#" Codebehind="FolderAPI.aspx.cs" AutoEventWireup="false" Inherits="Matchnet.Web.Applications.API.FolderAPI" %>

<asp:Repeater ID="xmlRepeater" Runat="server">
	<HeaderTemplate>
		<folders>
	</HeaderTemplate>
	<ItemTemplate>
		<folder>
			<id><asp:Literal ID="litID" Runat="server" /></id>
			<description><asp:Literal ID="litDescription" Runat="server" /></description>
		</folder>
	</ItemTemplate>
	<FooterTemplate>
		</folders>
	</FooterTemplate>
</asp:Repeater>
