using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Summary description for CheckAlerts.
	/// </summary>
	public class CheckAlerts : System.Web.UI.Page
	{
		private Int32 _memberID;
		private Brand _brand;

		protected Repeater rptAlerts;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_memberID = Conversion.CInt(Request["MemberID"]);
			_brand = BrandConfigSA.Instance.GetBrandByID(Conversion.CInt(Request["BrandID"]));

			ConversationInvites invites = InstantMessengerSA.Instance.GetInvites(_memberID, _brand.Site.Community.CommunityID);

			ArrayList jsCalls = new ArrayList();

			if (invites != null)
			{
//				for (Int32 i = 0; i < invites.Count; i++)
//				{
//					jsCalls.Add(getJSFromInvite(invites[i]));
//				}
			}

			rptAlerts.DataSource = jsCalls;
			rptAlerts.DataBind();
		}

		private void rptAlerts_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Literal litAlert = (Literal) e.Item.FindControl("litAlert");
				litAlert.Text = e.Item.DataItem.ToString();
			}
		}

		private string getJSFromInvite(ConversationInvite invite)
		{
			Matchnet.Member.ServiceAdapters.Member senderMember = MemberSA.Instance.GetMember(invite.Message.Sender.MemberID, MemberLoadFlags.None);

			Int32 regionID = senderMember.GetAttributeInt(_brand, WebConstants.ATTRIBUTE_NAME_REGIONID);

			return "parent.IM(\"" + senderMember.GetUserName(_brand) + "\", \"" + FrameworkGlobals.GetRegionString(regionID, _brand.Site.LanguageID) + "\");";
		}

		public string Location
		{
			get
			{
				return "CheckAlerts.aspx?MemberID=" + _memberID.ToString() + "&BrandID=" + _brand.BrandID.ToString();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			rptAlerts.ItemDataBound += new RepeaterItemEventHandler(rptAlerts_ItemDataBound);
		}
		#endregion

	}
}
