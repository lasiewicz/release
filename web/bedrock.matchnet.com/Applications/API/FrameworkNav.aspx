﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrameworkNav.aspx.cs" Inherits="Matchnet.Web.Applications.API.FrameworkNav" %>
<%@ Register TagPrefix="mn1" TagName="Head20" Src="/Framework/Ui/Head20.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Header" Src="/Framework/Ui/HeaderNavigation20/HeaderMenu.ascx" %>
<%@ Register TagPrefix="mn1" TagName="TopAuxNav" Src="/Framework/Ui/HeaderNavigation20/TopAuxNav.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<asp:PlaceHolder ID="plcHead20" Runat="server" Visible="false">
    <title>
        <asp:Literal runat="server" ID="litTitle" /></title>
    <meta http-equiv="X-UA-Compatible" content="edge" />
   
    <asp:Literal runat="server" ID="litMeta" />
    <asp:Literal runat="server" ID="litGoogleWebmaster" />
    <link rel="shortcut icon" href="<%=GetShortcutIconURL()%>" />
    <asp:PlaceHolder ID="phCSSIncludes" runat="server">
    
        <link rel="stylesheet" type="text/css" href="" runat="server" id="structStyleSheet" />
        <link rel="stylesheet" type="text/css" href="" runat="server" id="styleStyleSheet" />
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="PlaceHolderSplashCSS" runat="server" visible="false">
        <link rel="stylesheet" type="text/css" href="css/splash.<%=_g.Brand.Site.SiteID%>.css" media="all" />
    </asp:PlaceHolder>
    
    <asp:PlaceHolder id="PlaceHolderBlank" runat="Server" visible="true">
        <script type="text/javascript" src="<asp:Literal runat='server' id='litModernizr'/>"></script>
        
        <%--third party js--%><script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script><%--must be in noConflict mode--%>
        <%--jQuery CDN fallback--%><script type="text/javascript">
            //<![CDATA[
                window.jQuery || document.write('<script type="text/javascript" src="<asp:Literal runat='server' id='litjQueryFallback'/>">\x3C/script>')
            //]]>
            </script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
        <%--jQueryUI CND fallback--%><script type="text/javascript">
            //<![CDATA[
                window.jQuery.ui || document.write('<script type="text/javascript" src="<asp:Literal runat='server' id='litjQueryUIFallback'/>">\x3C/script>')
            //]]>
            </script>
        
        <script type="text/javascript" src="<asp:Literal runat='server' id='litJQueryPluggin'/>"></script>
		<script type="text/javascript" src="<asp:Literal runat='server' id='litStrophe'/>"></script>
		<script type="text/javascript" src="<asp:Literal runat='server' id='litStropheXDomainRequest'/>"></script>

        <script type="text/javascript">var $j = jQuery.noConflict();</script>

        <script type="text/javascript" src="<asp:Literal runat='server' id='litJavascriptMain'/>"></script>

        <asp:PlaceHolder ID="phJavascriptLibrary" runat="server">
            <script type="text/javascript" src="<asp:Literal runat='server' id='litJavascript' />"></script>
        </asp:PlaceHolder>
        <asp:PlaceHolder Runat="server" ID="phAslMainScript"></asp:PlaceHolder>
        <asp:Literal runat="server" ID="litAdditionalContent" />
        <asp:PlaceHolder ID="PlaceHolderGAM" runat="server"></asp:PlaceHolder>
        <mn:Txt ID="TxtPreloadImages" runat="server"></mn:Txt>

        <%--<uc1:ExternalFeedback ID="ExternalFeedback1" runat="server" OutputTag="Head" />--%>
        <%--<uc2:OmnidateHeadCode ID="OmnidateHeadCode" runat="server" />--%>
	 </asp:PlaceHolder>
</asp:PlaceHolder>

<asp:PlaceHolder ID="plcTopAuxNavMenu" Runat="server"  Visible="false">
<div class="<asp:Literal runat=server  id=litClassTopAux/>">
    <mn1:TopAuxNav id="topAuxNavMenu" runat="server"  ApiMenu="true"/>
</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="plcNavMenu" Runat="server" Visible="false">
<div class="<asp:Literal runat=server  id=litClassHeaderMenu/>">
    <mn1:Header id="HeaderMenu" runat="server" ApiMenu="true"/>
</div>    
</asp:PlaceHolder>