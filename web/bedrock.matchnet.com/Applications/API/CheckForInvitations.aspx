<%@ Page language="c#" Codebehind="CheckForInvitations.aspx.cs" AutoEventWireup="false" Inherits="Matchnet.Web.Applications.API.CheckForInvitations" %>
<asp:literal runat="server" id="litLoadGetInvitations" visible="false">loadGetInvitations()</asp:literal>
<asp:literal runat="server" id="litNoInvitationsFound" visible="false">noInvitationsFound()</asp:literal>
<asp:literal runat="server" id="litNotLoggedIn" visible="false">notLoggedIn()</asp:literal>
