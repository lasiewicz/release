﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Managers;
using Spark.Common.AccessService;
using Spark.FacebookLike.ValueObjects;

namespace Matchnet.Web.Applications.API
{
    [WebService(Namespace = "http://spark.net/API")]
    [System.Web.Script.Services.ScriptService]
    public class AllAccess : System.Web.Services.WebService
    {
        private ContextGlobal _g;

        public AllAccess()
		{
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }
		}

        /// <summary>
        /// Purchase to AllAcceess
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public OneClickPurchaseResult PurchaseAllAccess(int memberID)
        {
            var result = new OneClickPurchaseResult();

            try
            {
                if (_g != null)
                {
                    result = CreateMockData(_g.Member.GetUserName(_g.Brand));
                    return result;

                    // Purchase all access
                    result.Status = (int)JSONStatus.Success;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                //result.StatusMessage = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Add AllAcceess
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public OneClickPurchaseResult AddAllAccess()
        {
           
            var result = new OneClickPurchaseResult();
            
            try
            {
                if (_g != null)
                {
                    result =  CreateMockData(_g.Member.GetUserName(_g.Brand));
                    return result;
                    // add all access emails 
                    result.Status = (int)JSONStatus.Success;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                //result.StatusMessage = ex.Message;
            }

            return result;
        }


        private OneClickPurchaseResult CreateMockData(string username)
        {
            return new OneClickPurchaseResult()
            {
                Username = "מידע_לבדיקה",
                OrderID = "9999999999",
                CreditCardType = "ויזה",
                LastFourDigitsCreditCardNumber = "1234",
                Status = (int)JSONStatus.Success,
                StatusMessage = "Success!"
            };
        }
    }
}
