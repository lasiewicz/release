﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Matchnet;
using Matchnet.Web.Framework;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;
namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for SessionUpdate
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class SessionUpdate : System.Web.Services.WebService
    {
  private ContextGlobal _g;

    public SessionUpdate()
		{
			if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                }
            }
        }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public bool UpdateSessionIntWithIncrement(string key, int value,int lifetime)
    {
        try
        {
            SessionPropertyLifetime sessionlifetime = SessionPropertyLifetime.Temporary;
            try
            {
                 sessionlifetime = (SessionPropertyLifetime)lifetime;
            }
            catch (Exception e)
            {
            }
            string val = _g.Session.GetString(key);
            int ival = Conversion.CInt(val, 0);
            ival += value;
            _g.Session.Add(key, ival.ToString(), sessionlifetime);
            return true;

        }
        catch (Exception ex)
        {
            return false;
        }


    }
    }
}
