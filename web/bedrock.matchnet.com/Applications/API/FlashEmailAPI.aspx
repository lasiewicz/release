<%@ Page language="c#" Codebehind="FlashEmailAPI.aspx.cs" AutoEventWireup="false" Inherits="Matchnet.Web.Applications.API.FlashEmailAPI" %>

<asp:Repeater ID="xmlRepeater" Runat="server">
	<HeaderTemplate>
		<data>
	</HeaderTemplate>
	<ItemTemplate>
		<title name="<asp:Literal ID='litFrom' Runat='server' />">
			<time><asp:Literal ID="litInsertDate" Runat="server" /></time>
			<subject><asp:Literal ID="litSubject" Runat="server" /></subject>
			<url><asp:Literal ID="litUrl" Runat="server" /></url>
			<thumburl><asp:Literal ID="litThumbUrl" Runat="server" /></thumburl>
			<region><asp:Literal ID="litRegion" Runat="server" /></region>
			<gender><asp:Literal ID="litGender" Runat="server" /></gender>
		</title>
	</ItemTemplate>
	<FooterTemplate>
		</data>
	</FooterTemplate>
</asp:Repeater>
