﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework;
using System.Web.Script.Serialization;
using System.Collections.Specialized;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for UpdateMember
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class UpdateMember : System.Web.Services.WebService
    {

        private ContextGlobal _g;

        public UpdateMember()
        {
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }
        }

        private class ArrayItem
        {
            public string Key { get; set; }
            public string Value { get; set; }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SaveMemberAttributes(string attributesList)
        {
            string result = "{\"result\" : \"fail\", \"message\" : \"\" }";
            string validationError = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(attributesList))
                {
                    if (_g.Member != null)
                    {

                        {
                            JavaScriptSerializer jss = new JavaScriptSerializer();
                            List<ArrayItem> arrayItems = jss.Deserialize<List<ArrayItem>>(attributesList);
                            var nvParams = new Dictionary<string, string>();

                            foreach (ArrayItem item in arrayItems)
                            {
                                try
                                {
                                    if (item.Key.StartsWith("ATTR_"))
                                    {
                                        _g.Member.SetAttributeInt(_g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID,
                                                                  _g.Brand.BrandID, item.Key.Replace("ATTR_", string.Empty), int.Parse(item.Value));
                                    }
                                    else
                                    {
                                        nvParams.Add(item.Key, item.Value);
                                    }

                                }
                                catch (Exception ex)
                                {
                                    _g.ProcessException(ex);
                                }
                            }

                            System.Web.UI.Page pResource = new System.Web.UI.Page();
                            FrameworkControl InternalEditResourceControl = pResource.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditResourceControl.ascx") as FrameworkControl;
                            if (nvParams.ContainsKey("countryregionid") && !string.IsNullOrEmpty(nvParams["countryregionid"]) && nvParams["countryregionid"] != "null")
                            {
                                RegionID regionID = null;
                                Region region = null;

                                try
                                {
                                    int currentRegionID = _g.Member.GetAttributeInt(_g.Brand, "regionid");

                                    int countryRegionID = Convert.ToInt32(nvParams["countryregionid"].Trim());
                                    if (countryRegionID > 0)
                                    {
                                        if (countryRegionID == ConstantsTemp.REGION_US || countryRegionID == ConstantsTemp.REGIONID_CANADA)
                                        {
                                            //regionID by zip code
                                            string zip = nvParams["zipcode"];
                                            if (!string.IsNullOrEmpty(zip))
                                            {
                                                regionID = RegionSA.Instance.FindRegionIdByPostalCode(countryRegionID, zip);
                                                if (regionID == null || regionID.ID <= 0)
                                                {
                                                    validationError = _g.GetResource("TXT_VALIDATION_ZIPCODE", InternalEditResourceControl);
                                                }
                                                else
                                                {
                                                    string cityForZip = nvParams["cityforzipcode"];
                                                    if (!string.IsNullOrEmpty(cityForZip))
                                                    {
                                                        int languageId = _g.Brand.Site.LanguageID;

                                                        try
                                                        {
                                                            int cityForZipInt = Convert.ToInt32(cityForZip);
                                                            region = RegionSA.Instance.RetrieveRegionByID(cityForZipInt, languageId);
                                                            if (region == null || region.RegionID <= 0)
                                                            {
                                                                validationError = _g.GetResource("TXT_VALIDATION_LOCATION", InternalEditResourceControl);
                                                            }

                                                        }
                                                        catch
                                                        {
                                                            validationError = _g.GetResource("TXT_VALIDATION_LOCATION", InternalEditResourceControl);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        validationError = _g.GetResource("TXT_VALIDATION_LOCATION", InternalEditResourceControl);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                validationError = _g.GetResource("TXT_VALIDATION_ZIPCODE", InternalEditResourceControl);
                                            }
                                        }
                                        else
                                        {
                                            //regionID by city
                                            bool validated = true;
                                            int languageId = _g.Brand.Site.LanguageID;

                                            //verify if country requires state/region
                                            bool isStatesRequired = RegionSA.Instance.RetrieveRegionByID(countryRegionID, _g.Brand.Site.LanguageID).ChildrenDepth == 2;

                                            int stateRegionID = 0;
                                            if (isStatesRequired)
                                            {
                                                try
                                                {
                                                    stateRegionID = Convert.ToInt32(nvParams["stateregionid"]);
                                                }
                                                catch
                                                {
                                                    validated = false;
                                                    validationError = _g.GetResource("TXT_VALIDATION_LOCATION", InternalEditResourceControl);
                                                }
                                            }

                                            if (validated)
                                            {
                                                string cityName = nvParams["cityname"];

                                                //get region by city based on country/state or just country
                                                if (stateRegionID > 0 && languageId > 0 && !string.IsNullOrEmpty(cityName))
                                                {
                                                    regionID = RegionSA.Instance.FindRegionIdByCity(stateRegionID, cityName, languageId);
                                                }
                                                else if (countryRegionID > 0 && languageId > 0 && !string.IsNullOrEmpty(cityName))
                                                {
                                                    regionID = RegionSA.Instance.FindRegionIdByCity(countryRegionID, cityName, languageId);
                                                }

                                                if (regionID == null || regionID.ID <= 0)
                                                {
                                                    validationError = _g.GetResource("TXT_VALIDATION_CITY", InternalEditResourceControl);
                                                }
                                            }
                                        }

                                        int newRegionID = 0;
                                        if (region != null && region.RegionID > 0)
                                        {
                                            newRegionID = region.RegionID;
                                        }
                                        else if (regionID != null && regionID.ID > 0)
                                        {
                                            newRegionID = regionID.ID;
                                        }

                                        if (newRegionID > 0)
                                        {
                                            _g.Member.SetAttributeInt(_g.Brand, "regionid", newRegionID);
                                            // put new region in session for use with MOL
                                            _g.Session.Add(WebConstants.ATTRIBUTE_NAME_MOLREGIONID, newRegionID,
                                                          Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                                        }

                                    }
                                }
                                catch
                                {
                                    validationError = _g.GetResource("TXT_VALIDATION_LOCATION", InternalEditResourceControl);
                                }
                            }
                        }
                    }
                    else
                    {
                        // _g.Member == null
                        _g.ProcessException(new Exception("UpdateMember.SaveMemberAttributes - _g.Member is null"));
                    }
                }
                else
                {
                    //string.IsNullOrEmpty(attributesList)
                    _g.ProcessException(new Exception("UpdateMember.SaveMemberAttributes - attributesList is empty"));
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
            finally
            {
                // Save the member in any case
                if (_g != null && _g.Member != null)
                {
                    try
                    {
                        MemberSA.Instance.SaveMember(_g.Member);
                        if (string.IsNullOrEmpty(validationError))
                        {
                            result = "{\"result\" : \"success\", \"message\" : \"\" }";
                        }
                        else
                        {
                            result = "{\"result\" : \"fail\", \"message\" : \"" + validationError + "\" }";
                        }

                    }
                    catch (Exception ex2)
                    {
                        _g.ProcessException(ex2);
                        result = "{\"result\" : \"fail\", \"message\" : \"" + _g.GetResource(_g.Brand, "MATCHMETER_ERROR_BREADCRUMB") + "\" }";
                    }
                }
            }

            return result;
        }
    }
}
