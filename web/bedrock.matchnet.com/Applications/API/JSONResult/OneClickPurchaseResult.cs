﻿namespace Matchnet.Web.Applications.API.JSONResult
{
    public class OneClickPurchaseResult : ResultBase
    {
        public string Username { get; set; }
        public string OrderID { get; set; }
        public string CreditCardType { get; set; }
        public string LastFourDigitsCreditCardNumber { get; set; }
        public string TransactionDate { get; set; }
    }
}
