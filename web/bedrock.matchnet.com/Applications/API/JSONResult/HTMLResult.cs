﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.API.JSONResult
{
    /// <summary>
    /// Result class to return HTML for a User Control; e.g. lazy load or partial refresh
    /// </summary>
    [Serializable]
    public class HTMLResult : ResultBase
    {
        public string HTML { get; set; }
    }
}
