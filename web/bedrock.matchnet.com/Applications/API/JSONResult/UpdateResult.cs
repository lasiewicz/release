﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.API.JSONResult
{
    /// <summary>
    /// Result class to encapsulate result data due to an API to update data
    /// </summary>
    [Serializable]
    public class UpdateResult : ResultBase
    {
        //list of properties for all possible values returned that may be needed to update other UI as part of this Ajax update
        public int MemberID { get; set; }
        public string SubPageURL { get; set; }
    }
}
