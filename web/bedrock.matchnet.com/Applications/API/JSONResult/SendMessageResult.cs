﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Web.Applications.API.JSONResult
{
    /// <summary>
    /// Result class to encapsulate result data due to an API to send a message
    /// </summary>
    [Serializable]
    public class SendMessageResult : ResultBase
    {
        //list of properties for all possible values returned that may be needed to update other UI as part of this Ajax update
        public int MemberID { get; set; }
        public int MemberMailID { get; set; }
        public string UpsaleURL { get; set; }
        public int MessageSendErrorID { get; set; }

        public SendMessageResult()
        {
            MemberID = 0;
            MemberMailID = 0;
            UpsaleURL = "";
            MessageSendErrorID = (int)MessageSendError.Unspecified;
        }
    }
}
