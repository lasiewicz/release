﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.PhotoSearch.ValueObjects;

namespace Matchnet.Web.Applications.API.JSONResult
{
    [Serializable]
    public class PhotoGalleryResult : ResultBase
    {
        public int MemberID { get; set; }
        public bool FiltersValid { get; set; }
        public int BatchResultCount { get; set; }
        public int TotalResultCount { get; set; }
        public List<PhotoGalleryMember> PhotoGalleryMemberList { get; set; }

    }
}