﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.API.JSONResult
{
    [Serializable]
    public class IMHistoryMessageListResult : ResultBase
    {
        public int MemberID { get; set; }
        public int TargetMemberID { get; set; }
        public int BatchResultCount { get; set; }
        public int TotalResultCount { get; set; }
        public string AgeLocationDisplayText { get; set; }
        public string ThumbWebPath { get; set; }
        public string UserName { get; set; }
        public string MaritalSeekingDisplayText { get; set; }
        public string IMURL { get; set; }
        public string ProfileURL { get; set; }
        public string EmailURL { get; set; }
        public bool IsOnline { get; set; }
        public List<IMHistoryMessage> IMHistoryMessageList { get; set; }
    }

    [Serializable]
    public class IMHistoryMessage
    {
        public int MemberID { get; set; }
        public int MessageID { get; set; }
        public string MessageText { get; set; }
        public string MessageDateString { get; set; }
    }
}