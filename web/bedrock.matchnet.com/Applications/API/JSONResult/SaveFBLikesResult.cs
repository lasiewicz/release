﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.API.JSONResult
{
    /// <summary>
    /// Result class to contain additional info when saving facebook likes
    /// </summary>
    [Serializable]
    public class SaveFBLikesResult : ResultBase
    {
        public List<long> FailedFacebookLikeIDs { get; set; }

        public SaveFBLikesResult()
        {
            FailedFacebookLikeIDs = new List<long>();
        }
    }
}