using System;
using System.Web;
using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Base class for use by .aspx pages that need a light-weight version of the framework (e.g., for APIs).
	/// </summary>
	public class SessionDependentAPIBase : System.Web.UI.Page
	{
		#region Private Variables
		protected UserSession _userSession;
		protected bool _isDevMode = false;
		private Int32 _memberID;
		private Brand _brand;
		private BrandAlias _brandAlias;
		#endregion

		#region Constructors
		public SessionDependentAPIBase()
		{
			_isDevMode = Boolean.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE"));
		}
		#endregion

		#region Overrides
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			_userSession = SessionSA.Instance.GetSession(_isDevMode);
			_memberID = Conversion.CInt(_userSession[WebConstants.SESSION_PROPERTY_NAME_MEMBERID]);
			_brand = BrandConfigSA.Instance.GetBrandByID(Conversion.CInt(_userSession[WebConstants.SESSION_PROPERTY_NAME_BRANDID]));
			_brandAlias = BrandConfigSA.Instance.GetBrands().GetBrandAlias(Conversion.CInt(_userSession[WebConstants.SESSION_PROPERTY_NAME_BRANDALIASID]));
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
		}


		protected override void OnUnload(EventArgs e)
		{

			base.OnUnload(e);
		}

		protected void SaveSession() 
		{
			//Commenting this out in attempt to fix 17608. Once we got an error about
			//a null reference here (most likely brand), but that wouldn't explain the 10 minute or more delay on loading the page
			//Also, we may not need the SaveSession because I wouldn't expect the UserSession to ever 
			//get "dirty" with an API call such as this, however that may be shortsighted...
			try 
			{
				string domainName = String.Empty;

				if (_brand != null) 
				{
					if ((_brand.StatusMask & (int) Matchnet.Content.ValueObjects.BrandConfig.StatusType.CoBrand) == 
						(int) Matchnet.Content.ValueObjects.BrandConfig.StatusType.CoBrand)
					{
						domainName = _brand.Site.Name;
					}
					else if (_brandAlias == null)
					{
						domainName = _brand.Uri;
					}
					else
					{
						if (_brandAlias != null) 
						{
							domainName = _brandAlias.Uri;
						}
					}

					SessionSA.Instance.SaveSession(_userSession, domainName, _isDevMode);	
				}
			}
			catch (Exception ex)
			{
				//write to log
				Matchnet.Web.Framework.Diagnostics.ContextExceptions.WriteExceptionToEventLogNoG(ex);
			}

		}
		#endregion

		#region Properties
		public UserSession UserSession
		{
			get
			{
				return _userSession;
			}
		}

		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		public Brand Brand
		{
			get
			{
				return _brand;
			}
		}
		#endregion
	}
}
