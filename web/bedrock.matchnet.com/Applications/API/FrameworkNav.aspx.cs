﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
namespace Matchnet.Web.Applications.API
{
    public partial class FrameworkNav : SessionDependentAPIBase
    {
        public ContextGlobal _g;

        //Constants for javascript and css file locations
        protected const string JAVASCRIPT_FILE_PATH_MAIN = "/javascript20/spark.js";
        protected const string JAVASCRIPT_FILE = "/javascript20/library/javascript.js";
        protected const string JQUERYPLUGGIN_FILE = "/javascript20/library/jquery-plugins.js";
        protected const string MODERNIZR2_CUSTOM_FILE = "/javascript20/library/modernizr2-custom.min.js";
        protected const string JQUERYFALLBACK_FILE = "/javascript20/library/jquery-1.7.1.min.js";
        protected const string JQUERYUIFALLBACK_FILE = "/javascript20/library/jquery-ui-1.8.16.min.js";
        protected const string STROPHE_FILE = "/javascript20/library/strophe/strophe.min.js";
        protected const string STROPHE_XDOMAIN_REQUEST_FILE = "/javascript20/library/strophe/strophe.xdomainrequest.js";
        //later change from Erik file name
        const string structCSSSetting = "CSS_STRUCT";
        const string styleCSSSetting = "CSS_STYLE";

        protected const string CSS_PATH = "/css/";
        protected const string HREF = "href";
        public FrameworkNav()
        {
            try
            {
                _g = new ContextGlobal(Context);
                _g.IsAPI = true;
            }
            catch (Exception ex)
            { }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string apiimagehost = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("API_CALL_IMAGE_HOST", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID);
                string absolitepath = "http://" + apiimagehost + "." + _g.Brand.Uri;
                _g.ExpansionTokens.Add("API_HOST", absolitepath);
                TxtPreloadImages.Text = _g.GetResource("TXT_PRELOAD_IMAGES_HEAD", this);
                if (Request.QueryString["head"] == "true")
                { plcHead20.Visible = true; }

                if (Request.QueryString["topaux"] == "true")
                { plcTopAuxNavMenu.Visible = true; }

                if (Request.QueryString["headermenu"] == "true")
                { plcNavMenu.Visible = true; }

                topAuxNavMenu.SetTopNavVisibility(true, false);

                _g.TopAuxilaryMenu = topAuxNavMenu;
                litClassTopAux.Text = Framework.Menus.MenuUtils.IsSocialNavEnabled(_g) ? "socialnav" : "";
                litClassHeaderMenu.Text = Framework.Menus.MenuUtils.IsSocialNavEnabled(_g) ? "socialnav" : "";
                TxtPreloadImages.Text = _g.GetResource("TXT_PRELOAD_IMAGES_HEAD", this);

                litJavascriptMain.Text = absolitepath + AppendVersion(JAVASCRIPT_FILE_PATH_MAIN);
                litJavascript.Text = absolitepath + AppendVersion(JAVASCRIPT_FILE);
                litJQueryPluggin.Text = absolitepath + AppendVersion(JQUERYPLUGGIN_FILE);
                litModernizr.Text = absolitepath + AppendVersion(MODERNIZR2_CUSTOM_FILE);
                litjQueryFallback.Text = absolitepath + AppendVersion(JQUERYFALLBACK_FILE);
                litjQueryUIFallback.Text = absolitepath + AppendVersion(JQUERYUIFALLBACK_FILE);
                litStrophe.Text = absolitepath + AppendVersion(STROPHE_FILE);
                litStropheXDomainRequest.Text = absolitepath + AppendVersion(STROPHE_XDOMAIN_REQUEST_FILE);
                //set source of javascript that controls navigation menu* (* you really shouldn't need to include this on the splash but since the body tag of every page calls a javascript Init() that touches the menu, we need this here - lucena ???)
                // litJavascriptMenu.Text = hebrew ? AppendVersion(JAVASCRIPT_FILE_PATH_MENU_HEBREW) : AppendVersion(JAVASCRIPT_FILE_PATH_MENU_NONHEBREW);

                //temporary hack don't forget to remove and configure thru settings 06/02/2010
                if (_g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.AmericanSingles)
                    structStyleSheet.Attributes[HREF] = absolitepath + AppendVersion(GetConfigurableStyleSheetName(structCSSSetting));

                styleStyleSheet.Attributes[HREF] = absolitepath + AppendVersion(GetConfigurableStyleSheetName(styleCSSSetting));

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }

        }

        public string GetShortcutIconURL()
        {
            return this.Image(1, "Shortcut.ico");
        }
        public string Image(int AppID, string fileName)
        {
            return Image((WebConstants.APP)AppID, fileName);
        }

        public string Image(WebConstants.APP app, string fileName)
        {
            Matchnet.Web.Framework.Image I = new Matchnet.Web.Framework.Image();
            I.FileName = fileName;
            I.App = app;
            return I.ImageUrl;
        }

        protected string AppendVersion(string path)
        {
            return path + "?v=" + FrameworkGlobals.GetLastWriteTimeString(path);
        }

        /// <summary>
        /// Returns site specific css file from db settings (as of this writing), appends path in front and .css behind it
        /// GetSetting will check site-specific then community-specific, and then grab the globaldefault if it doesn't find anything.
        /// </summary>
        /// <param name="cssSettingName">The name of the setting to look up in runtime settings</param>
        /// <returns></returns>
        protected string GetConfigurableStyleSheetName(string cssSettingName)
        {
            string configuredCss = RuntimeSettings.GetSetting(cssSettingName, _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID);

            if (!String.IsNullOrEmpty(configuredCss))
                return CSS_PATH + configuredCss + ".css";
            else
                return "";
        }
    }
}
