﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// API Page to contain various session related updates
    /// </summary>
    public partial class SessionUtilAPI : SessionDependentAPIBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (_userSession != null)
            {
                try
                {
                    //Hide announcement message for user's session
                    if (!String.IsNullOrEmpty(Request["HideAnnouncement"]) && Request["HideAnnouncement"] == "1")
                    {
                        _userSession.Add(WebConstants.SESSION_PROPERTY_NAME_HIDE_ANNOUNCEMENT_MESSAGE, "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                        SaveSession();
                    }
                }
                catch (Exception ex)
                {
                    Response.Write("alert('" + ex.Message + "')");
                }
            }
        }
    }
}
