using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.IO;
using System.Xml;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Summary description for MemberThumb.
	/// </summary>
	public class e1Admin : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				Response.Redirect("http://172.16.1.181:6969/" + Request.QueryString);
			}
			catch (Exception ex)
			{
				System.Diagnostics.EventLog.WriteEntry("WWW", ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
