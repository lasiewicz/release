﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for QuestionAnswerService
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    public class QuestionAnswerService : System.Web.Services.WebService
    {

        private ContextGlobal _g;
        private int _PageSize = Constants.NULL_INT;
        private int DEFAULT_PAGE_SIZE = 9;

        public int PageSize
        {
            get 
            {
                if (_PageSize == Constants.NULL_INT)
                {
                    SettingsManager settingsManager = new SettingsManager();
                    _PageSize = settingsManager.GetSettingInt(SettingConstants.QANDA_ANSWER_PAGE_SIZE, _g.Brand);

                    if (_PageSize == Constants.NULL_INT)
                    {
                        _PageSize = DEFAULT_PAGE_SIZE;
                    }
                }
                return _PageSize;
            }

        }

        public QuestionAnswerService()
        {
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public QuestionAnswer.QuestionViewObject GetQuestion(Int32 questionID, Int32 memberID, Int32 pageSize, Int32 startIndex, string sortBy, bool reverse, string entryPoint, string trackingParam, bool isSite20Enabled, bool includeAnsweredQuestions)
        {
            Hashtable additionalParams=new Hashtable();
            additionalParams["sortBy"] = sortBy;
            additionalParams["reverse"] = reverse;
            //additionalParams["pageUrl"] = "http://arod.spark.com/Applications/Home/default.aspx";
            if (!string.IsNullOrEmpty(entryPoint))
            {
                additionalParams["entryPoint"] = Enum.Parse(typeof(BreadCrumbHelper.EntryPoint), entryPoint.ToString());
            }
            if (!string.IsNullOrEmpty(trackingParam))
            {
                additionalParams["trackingParam"] = trackingParam;
            }
            additionalParams["pageSize"] = PageSize;
            _g.IsSite20Enabled = isSite20Enabled;
            QuestionAnswer.QuestionViewObject questionViewObject=QuestionAnswer.QuestionAnswerHelper.GetQuestionViewObject(questionID, memberID, _g, additionalParams, includeAnsweredQuestions);
            return questionViewObject;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool SubmitQuestionText(int brandID, string questionText, int memberID, bool isAnonymous)
        {
            if (!QuestionAnswer.QuestionAnswerHelper.IsQuestionAnswerMemberQuestionsEnabled(_g.Brand)) return false;
            bool success = true;
            try
            {
                if(_g.Member != null  && _g.Member.MemberID > 0 && _g.Member.MemberID == memberID) {
                    string userName =_g.Member.GetUserName(_g.Brand);
                    string decodedQuestionText = FrameworkGlobals.DecodeHexEntities(questionText);
                    string firstName = _g.Member.GetAttributeText(_g.Brand,"sitefirstname");
                    string lastName = _g.Member.GetAttributeText(_g.Brand, "sitelastname");

                    ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMemberGeneratedQuestion(brandID, userName, firstName, lastName, memberID, decodedQuestionText, isAnonymous);
                }
            }
            catch (Exception e)
            {
                _g.ProcessException(e);
                success = false;
            }
            return success;

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public QuestionAnswer.QuestionViewObject AnswerBlockedQuestion(Int32 questionID, Int32 memberID, Int32 answerMemberID, string answerText, string entryPoint, string trackingParam, bool isSite20Enabled)
        {
            QuestionAnswer.QuestionViewObject questionViewObject=new Matchnet.Web.Applications.QuestionAnswer.QuestionViewObject();
            try
            {
                if (_g.Member != null && _g.Member.MemberID > 0 && _g.Member.MemberID == answerMemberID)
                {
            Hashtable additionalParams = new Hashtable();
            if (!string.IsNullOrEmpty(entryPoint))
            {
                additionalParams["entryPoint"] = Enum.Parse(typeof(BreadCrumbHelper.EntryPoint), entryPoint.ToString());
            }
            if (!string.IsNullOrEmpty(trackingParam))
            {
                additionalParams["trackingParam"] = trackingParam;
            }            
            _g.IsSite20Enabled = isSite20Enabled;
                    questionViewObject = QuestionAnswer.QuestionAnswerHelper.AnswerBlockedQuestion(questionID, memberID, answerMemberID, answerText, _g, additionalParams);
                }
            } catch (Exception e) {
                _g.ProcessException(e);
            }
            return questionViewObject;
        }
    }
}
