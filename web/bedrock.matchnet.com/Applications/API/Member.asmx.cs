﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;

using Matchnet.Member.ServiceAdapters;
using System.Web.Script.Services;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.ProfileElements;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit;
using System.Collections.Generic;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls;
using System.IO;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Applications.Events;
using Matchnet.List.ValueObjects;


namespace Matchnet.Web.Applications.API
{
	[WebService(Namespace="http://spark.net/API")]
    [System.Web.Script.Services.ScriptService]
	public class Member : System.Web.Services.WebService
	{
        private ContextGlobal _g;

		public Member()
		{
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }

			InitializeComponent();
		}

		#region Component Designer generated code
		
		private IContainer components = null;
				
		private void InitializeComponent()
		{
		}

		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}
		
		#endregion

		[WebMethod]
		public bool[] IsValidMember(Int32 communityID,
			Int32 siteID,
			Int32 memberID)
		{
			bool[] returnValue = new bool[2]{false, false};

			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID,
				MemberLoadFlags.None);

			if (member != null)
			{
				returnValue[0] = member.IsCommunityMember(communityID);
				returnValue[1] = member.IsPayingMember(siteID);
			}
			
			return returnValue;
		}

        /// <summary>
        /// This is used to retrieve contact history for a member
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public GetContactHistoryResult GetContactHistory(int targetMemberID)
        {
            GetContactHistoryResult result = new GetContactHistoryResult();
            try
            {
                if (_g != null && _g.Member != null && targetMemberID > 0)
                {
                    result.MemberID = _g.Member.MemberID;
                    result.TargetMemberID = targetMemberID;

                    //get control strictly for resources
                    System.Web.UI.Page page = new System.Web.UI.Page();
                    System.Web.UI.Control ResourceControl = page.LoadControl("/Framework/Ui/ProfileElements/ProfileElementsAPIResource.ascx");

                    //get contact history items
                    result.HistoryItems = ProfileElementHelper.GetContactHistoryItemList(_g, targetMemberID, ResourceControl);
                    result.Status = (int)JSONStatus.Success;
                }
                else
                {
                    throw new Exception("Missing data error");
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// This updates a section of profile data for a given profile data group
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public EditProfileResult UpdateProfileDataSection(int memberID, int dataGroupID, string dataSectionName, string nvProfileDataParams)
        {
            EditProfileResult result = new EditProfileResult();
            try
            {
                if (_g != null && _g.Member != null && memberID > 0 && _g.Member.MemberID == memberID)
                {
                    result.MemberID = _g.Member.MemberID;
                    //nvProfileDataParams = FrameworkGlobals.DecodeHexEntities(nvProfileDataParams);

                    //update profile data
                    ProfileDataGroupEnum dataGroup = (ProfileDataGroupEnum)Enum.Parse(typeof(ProfileDataGroupEnum), dataGroupID.ToString());
                    ProfileDataSectionEnum dataSection = (ProfileDataSectionEnum)Enum.Parse(typeof(ProfileDataSectionEnum), dataSectionName);
                    Dictionary<string, string> dicParams = new Dictionary<string,string>();
                    foreach (string nv in nvProfileDataParams.Split(new char[] { '&' }))
                    {
                        string[] param = nv.Split(new char[] { '=' });
                        if (param.Length > 1)
                        {
                            double d;
                            bool isNum = double.TryParse(param[1], out d);
                            bool isAlphaNum = ViewProfileTabUtility.IsAlphanumeric(param[1]);
                            string paramValue = (isNum || isAlphaNum) ? param[1] : FrameworkGlobals.DecodeHexEntities(param[1]);
                            dicParams.Add(param[0].ToLower(), paramValue);
                        }
                    }

                    result.Status = (int)JSONStatus.Success;
                    EditProfileUtility.UpdateProfileDataSection(memberID, _g, dataGroup, dataSection, dicParams, result);
                }
                else
                {
                    throw new Exception("Invalid Member");
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// This updates a date attribute
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public UpdateResult UpdateAttributeDate(int memberID, string attributeName)
        {
            UpdateResult result = new UpdateResult();

            try
            {
                if (_g != null && _g.Member != null && memberID == _g.Member.MemberID && !string.IsNullOrEmpty(attributeName))
                {
                    System.Diagnostics.Trace.WriteLine("UpdateAttributeDate(" + memberID.ToString() + ", " + attributeName + ")");
                    //we'll restrict only to known attributes as needed
                    switch (attributeName.ToLower())
                    {
                        case "facebookfeaturepromptdate":
                        case "mutualmatchpocanswereddate":
                            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                            if (member != null)
                            {
                                member.SetAttributeDate(_g.Brand, attributeName, DateTime.Now);
                                MemberSA.Instance.SaveMember(member);
                            }
                            break;
                    }
                }

                result.Status = (int)JSONStatus.Success;
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// This updates an int attribute
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public UpdateResult UpdateAttributeInt(int memberID, string attributeName, int attributeValue)
        {
            UpdateResult result = new UpdateResult();

            try
            {
                if (_g != null && _g.Member != null && memberID == _g.Member.MemberID && !string.IsNullOrEmpty(attributeName))
                {
                    System.Diagnostics.Trace.WriteLine("UpdateAttributeInt(" + memberID.ToString() + ", " + attributeName + ", " + attributeValue.ToString() + ")");
                    //we'll restrict only to known attributes as needed
                    switch (attributeName.ToLower())
                    {
                        case "facebookconnectstatusid":
                            if (attributeValue >= 0)
                            {
                                _g.Member.SetAttributeInt(_g.Brand, "FacebookConnectStatusID", attributeValue);
                                MemberSA.Instance.SaveMember(_g.Member);
                            }
                            break;
                        case "pageannouncementviewedmask":
                            if (attributeValue > 0)
                            {
                                Matchnet.Web.Framework.Ui.PageAnnouncements.PageAnnouncementHelper.AddPageAnnouncementViewedMask(_g.Member, _g.Brand.Site.SiteID, (Framework.Ui.PageAnnouncements.PageAnnouncementHelper.PageAnnouncementMask)Enum.Parse(typeof(Framework.Ui.PageAnnouncements.PageAnnouncementHelper.PageAnnouncementMask), attributeValue.ToString()));
                            }
                            break;
                    }
                }

                result.Status = (int)JSONStatus.Success;
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Processes passcode request for Black Board Eats
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BBEResult ProcessBBEPasscodeRequest(string memberID)
        {
            BBEResult result = new BBEResult();

            try
            {
                if (_g != null && _g.Member != null && _g.Member.MemberID > 0 && _g.Member.MemberID.ToString() == memberID)
                {
                    System.Diagnostics.Trace.WriteLine("ProcessBBEPasscodeRequest(" + memberID.ToString() + ")");
                    BBE bbe = BBEManager.Instance.ProcessBBEPasscodeRequest(_g.Member, _g.Brand, _g.Member.EmailAddress);
                    if (bbe != null)
                    {
                        if (!string.IsNullOrEmpty(bbe.Passcode))
                        {
                            result.BBEExpired = false;
                            result.BBEPasscode = bbe.Passcode;
                        }
                        else
                        {
                            result.BBEExpired = true;
                            result.BBEPasscode = "";
                        }
                    }

                    result.Status = (int)JSONStatus.Success;
                }
                else
                {
                    result.StatusMessage = "Invalid member";
                    result.Status = (int)JSONStatus.Failed;
                }
                
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        /// Processes essay email request
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public UpdateResult ProcessNoEssayEmailRequest(string profileMemberID, string viewerMemberID)
        {
            UpdateResult result = new UpdateResult();

            try
            {
                if (_g != null && _g.Member != null && _g.Member.MemberID > 0 && _g.Member.MemberID.ToString() == viewerMemberID)
                {
                    System.Diagnostics.Trace.WriteLine("ProcessNoEssayEmailRequest(" + profileMemberID.ToString() + ", " + viewerMemberID.ToString() + ")");
                    //send external mail
                    ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendNoEssayRequestEmail(Convert.ToInt32(profileMemberID), Convert.ToInt32(viewerMemberID), _g.Brand.BrandID);

                    //update member attribute
                    string memberList = _g.Member.GetAttributeText(_g.Brand, "EssayRequestEmailMemberList", "");
                    if (string.IsNullOrEmpty(memberList))
                    {
                        memberList = profileMemberID.ToString();
                    }
                    else
                    {
                        memberList = memberList + "," + profileMemberID.ToString();
                    }

                    _g.Member.SetAttributeText(_g.Brand, "EssayRequestEmailMemberList", memberList, Matchnet.Member.ValueObjects.TextStatusType.Auto);
                    MemberSA.Instance.SaveMember(_g.Member);
                    result.Status = (int)JSONStatus.Success;
                }
                else
                {
                    result.StatusMessage = "Invalid member";
                    result.Status = (int)JSONStatus.Failed;
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        
	}
}
