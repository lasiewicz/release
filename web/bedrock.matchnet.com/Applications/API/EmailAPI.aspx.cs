using System;
using System.Collections;
using System.Web.UI.WebControls;
using System.Text;
using System.Web;

using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using System.Web.Services;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Summary description for EmailAPI.
	/// </summary>
	public class EmailAPI : System.Web.UI.Page
	{
		protected Repeater xmlRepeater;

		private Int32 _communityID;

		private void Page_Load(object sender, System.EventArgs e)
		{
			Int32 memberID = Conversion.CInt(Request["MemberID"]);
			_communityID = Conversion.CInt(Request["CommunityID"]);
			Int32 memberFolderID = Conversion.CInt(Request["MemberFolderID"]);
			Int32 startRow = Conversion.CInt(Request["StartRow"]);
			Int32 pageSize = Conversion.CInt(Request["PageSize"]);
			string orderBy = Request["OrderBy"];

			ICollection messages = EmailMessageSA.Instance.RetrieveMessages(memberID, _communityID, memberFolderID, startRow, pageSize, orderBy,
                APIUtils.GetAnyBrandForCommunity(_communityID));

			xmlRepeater.DataSource = messages;
			xmlRepeater.DataBind();
		}

		private void xmlRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Literal litFrom = (Literal) e.Item.FindControl("litFrom");
				Literal litSubject = (Literal) e.Item.FindControl("litSubject");

				EmailMessage message = (EmailMessage) e.Item.DataItem;

				litFrom.Text = message.FromUserName;
				litSubject.Text = message.Subject;
			}
		}

        [WebMethod]
        public static string GetEmailMessageByMessageID(int memberID, int groupID, int messageID)
        {
            string log = string.Format("emailbymessage memberid:{0}, groupid:{1}, messageid:{2}", memberID.ToString(), groupID.ToString(), messageID.ToString());
            System.Diagnostics.EventLog.WriteEntry("WWW", log);
            Message emailMessage = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveMessageByMessageID(
                memberID,
                messageID);

            System.Diagnostics.EventLog.WriteEntry("WWW", "emailbymessage message: " + emailMessage.MessageBody);
            return "<strong>" + emailMessage.MessageHeader + "</strong><p>" + FormatMessage(emailMessage.MessageBody);
            
        }

        private static string FormatMessage(string messageContent)
        {
            StringBuilder messageBuilder = new StringBuilder(HttpUtility.HtmlDecode(messageContent));

            messageBuilder.Replace("\n", "<br />", 0, messageBuilder.Length);
            return messageBuilder.ToString();
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			xmlRepeater.ItemDataBound += new RepeaterItemEventHandler(xmlRepeater_ItemDataBound);
		}
		#endregion
	}
}
