﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text.RegularExpressions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Applications.UserNotifications.Controls;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using System.Web.UI;
using Matchnet.Web.Framework.Util;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using System.Reflection;

namespace Matchnet.Web.Applications.UserNotifications
{
    public enum OmnitureTags
    {
        [StringValue("Profile Activity")]
        ProfileActivity = 1,
        [StringValue("Hot List Updates")]
        HotListUpdates = 2,
        [StringValue("Match Activity")]
        MatchActivity = 3,
        [StringValue("Nudges")]
        Nudges = 4,
        [StringValue("Subscription Renewal")]
        Subscription = 5
    }

    public class UserNotificationFactory
    {
        public static readonly UserNotificationFactory Instance = new UserNotificationFactory();
        private LastLoginDate _TimeStamp = null;
        private UserNotificationsControl _UserNotificationsControl = null;
        public static readonly string ResourceDrivenNotificationBase = "RESOURCE_DRIVEN_USER_NOTIFICATION_";
        private static SettingsManager _settingsManager = new SettingsManager();

        public static bool IsUserNotificationsEnabled(ContextGlobal _g)
        {
            return IsUserNotificationsEnabled(_g.Brand);
        }

        public static bool IsUserNotificationsEnabled(Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            return _settingsManager.GetSettingBool(SettingConstants.ENABLE_USER_NOTIFICATIONS, brand);
        }

        public List<UserNotification> GetSystemNotifications(int memberid, ContextGlobal g)
        {
            List<UserNotification> notifyList = new List<UserNotification>();
            String systemNotificationsList = _settingsManager.GetSettingString(SettingConstants.USER_NOTIFICATIONS_SYSTEM_LIST, g.Brand);

            if (!string.IsNullOrEmpty(systemNotificationsList))
            {
                string[] systemNotifications = systemNotificationsList.Split(',');
                foreach (string notificationFullPath in systemNotifications)
                {
                    UserNotification un = Assembly.GetExecutingAssembly().CreateInstance("Matchnet.Web.Applications.UserNotifications." + notificationFullPath) as UserNotification;
                    notifyList.Add(GetUserNotification(un, memberid, memberid, g));
                }
            }

            int MAX_RESX_DRIVEN_LIMIT = _settingsManager.GetSettingInt(SettingConstants.RESOURCE_DRIVEN_USER_NOTIFICATIONS_MAX_LIMIT, g.Brand);
            
            bool hasResourceDrivenNotifications = true;
            for (int i = 1; hasResourceDrivenNotifications && i < MAX_RESX_DRIVEN_LIMIT; i++)
            {
                string value = g.GetResource(ResourceDrivenNotificationBase + i, Control);
                if (string.IsNullOrEmpty(value) || value.IndexOf(ResourceDrivenNotificationBase + i) > -1)
                {
                    hasResourceDrivenNotifications = false;
                }
                else
                {
                    ResourceDrivenNudgeUserNotification rdn = new ResourceDrivenNudgeUserNotification();
                    rdn.resourceKey = ResourceDrivenNotificationBase + i;
                    notifyList.Add(GetUserNotification(rdn, memberid, memberid, g));
                }
            }

            return notifyList;
        }

        public List<UserNotification> GetSystemNotifications(Member.ServiceAdapters.Member member, ContextGlobal g)
        {
            List<UserNotification> notifyList = new List<UserNotification>();
            String systemNotificationsList = _settingsManager.GetSettingString(SettingConstants.USER_NOTIFICATIONS_SYSTEM_LIST, g.Brand);

            if (!string.IsNullOrEmpty(systemNotificationsList))
            {
                string[] systemNotifications = systemNotificationsList.Split(',');
                foreach (string notificationFullPath in systemNotifications)
                {
                    UserNotification un = Assembly.GetExecutingAssembly().CreateInstance("Matchnet.Web.Applications.UserNotifications." + notificationFullPath) as UserNotification;
                    notifyList.Add(GetUserNotification(un, member, member, g));
                }
            }

            int MAX_RESX_DRIVEN_LIMIT = _settingsManager.GetSettingInt(SettingConstants.RESOURCE_DRIVEN_USER_NOTIFICATIONS_MAX_LIMIT, g.Brand);
            
            bool hasResourceDrivenNotifications = true;
            for (int i = 1; hasResourceDrivenNotifications && i < MAX_RESX_DRIVEN_LIMIT; i++)
            {
                string value = g.GetResource(ResourceDrivenNotificationBase + i, Control);
                if (string.IsNullOrEmpty(value) || value.IndexOf(ResourceDrivenNotificationBase + i) > -1)
                {
                    hasResourceDrivenNotifications = false;
                }
                else
                {
                    ResourceDrivenNudgeUserNotification rdn = new ResourceDrivenNudgeUserNotification();
                    rdn.resourceKey = ResourceDrivenNotificationBase + i;
                    notifyList.Add(GetUserNotification(rdn, member, member, g));
                }
            }

            return notifyList;
        }


        private LastLoginDate TimeStamp
        {
            get
            {
                if (null == _TimeStamp)
                {
                    System.Web.UI.Page page = new System.Web.UI.Page();
                    _TimeStamp = page.LoadControl("/Framework/Ui/BasicElements/LastLoginDate.ascx") as Matchnet.Web.Framework.Ui.BasicElements.LastLoginDate;
                }
                return _TimeStamp;
            }
        }

        private UserNotificationsControl Control
        {
            get
            {
                if (null == _UserNotificationsControl)
                {
                    System.Web.UI.Page page = new System.Web.UI.Page();
                    _UserNotificationsControl = page.LoadControl("/Applications/UserNotifications/Controls/UserNotificationsControl.ascx") as UserNotificationsControl;
                }
                return _UserNotificationsControl;
            }
        }

        private UserNotificationFactory() { }

        public UserNotification GetUserNotification(UserNotification notification, int OwnerId, int CreatorId, ContextGlobal g, Hashtable additionalParams)
        {
            UserNotification userNotification = GetUserNotification(notification, OwnerId, CreatorId, g);
            userNotification.AdditionalParams = additionalParams;
            return userNotification;
        }

        public UserNotification GetUserNotification(UserNotification notification, int OwnerId, int CreatorId, ContextGlobal g)
        {
            Matchnet.Member.ServiceAdapters.Member notificationMember = MemberSA.Instance.GetMember(OwnerId, MemberLoadFlags.None);
            Matchnet.Member.ServiceAdapters.Member notificationCreator = MemberSA.Instance.GetMember(CreatorId, MemberLoadFlags.None);
            UserNotification userNotification = notification;
            userNotification._g = g;
            userNotification.ctrl = Control;
            userNotification.TimeStamp = TimeStamp;
            userNotification.NotificationOwner = notificationMember;
            userNotification.NotificationCreator = notificationCreator;
            return userNotification;
        }

        public UserNotification GetUserNotification(UserNotification notification, Matchnet.Member.ServiceAdapters.Member notificationMember, Matchnet.Member.ServiceAdapters.Member notificationCreator, ContextGlobal g)
        {
            UserNotification userNotification = notification;
            userNotification._g = g;
            userNotification.ctrl = Control;
            userNotification.TimeStamp = TimeStamp;
            userNotification.NotificationOwner = notificationMember;
            userNotification.NotificationCreator = notificationCreator;
            return userNotification;
        }

        public UserNotificationViewObject ConvertToFavoritesNotification(UserNotificationViewObject unvo, ContextGlobal g)
        {
            UserNotificationViewObject convertedNotification = unvo;

            unvo.DataPoints.Remove(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE);
            unvo.DataPoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            try
            {
                UserNotification userNotification = null;
                switch (unvo.ResourceKey)
                {
                    case "USER_NOTIFICATION_ADDED_YOU_MESSAGE_TXT":
                        userNotification = new FavoriteAddedYouAsFavoriteUserNotification();
                        break;
                    case "USER_NOTIFICATION_ECARDED_YOU_MESSAGE_TXT":
                        userNotification = new FavoriteECardedYouUserNotification();
                        break;
                    case "USER_NOTIFICATION_EMAILED_YOU_MESSAGE_TXT":
                        userNotification = new FavoriteEmailedYouUserNotification();
                        
                        if (unvo.DataPoints != null && unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID) && unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.MAIL_MESSAGE_ID))
                        {
                            var emailAdditionalParams = new Hashtable();
                            emailAdditionalParams.Add("messagefolderid", unvo.DataPoints[UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID]);
                            emailAdditionalParams.Add("messageid", unvo.DataPoints[UserNotificationDataPointConstants.MAIL_MESSAGE_ID]);
                            userNotification.AdditionalParams = emailAdditionalParams;
                        }
                        break;
                    case "USER_NOTIFICATION_FLIRTED_YOU_MESSAGE_TXT":
                        userNotification = new FavoriteFlirtedYouUserNotification();

                        if (unvo.DataPoints != null && unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID) && unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.MAIL_MESSAGE_ID))
                        {
                            var flirtAdditionalParams = new Hashtable();
                            flirtAdditionalParams.Add("messagefolderid", unvo.DataPoints[UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID]);
                            flirtAdditionalParams.Add("messageid", unvo.DataPoints[UserNotificationDataPointConstants.MAIL_MESSAGE_ID]);
                            userNotification.AdditionalParams = flirtAdditionalParams;
                        }
                        break;
                    case "USER_NOTIFICATION_IMED_YOU_MESSAGE_TXT":
                        userNotification = new FavoriteIMedYouUserNotification();
                        break;
                    case "USER_NOTIFICATION_MUTUAL_YES_MESSAGE_TXT":
                        userNotification = new FavoriteMutualYesUserNotification();
                        break;
                    case "USER_NOTIFICATION_VIEWED_YOU_MESSAGE_TXT":
                        userNotification = new FavoriteViewedYourProfileUserNotification();
                        break;
                    case "USER_NOTIFICATION_LIKED_ANSWER_TXT":
                        Regex regEx = new Regex("questid=[0-9]+");
                        Match m = regEx.Match(unvo.Text);
                        string result = m.Value.Split('=')[1];
                        Hashtable h = new Hashtable();
                        h["questionId"] = result;
                        userNotification = new FavoriteLikedAnswerUserNotification();
                        userNotification.AdditionalParams = h;
                        break;



                    case "USER_NOTIFICATION_LIKED_FREETEXT_ABOUTME_TXT":
                        userNotification = new FavoriteLikedFreeTextAboutMeUserNotification();
                        unvo.DataPoints.Remove(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED);
                        unvo.DataPoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, LikedFreeTextEssayType.AboutMe.ToString("d"));
                        break;
                    case "USER_NOTIFICATION_LIKED_FREETEXT_PERFECTMATCHESSAY_TXT":
                        userNotification = new FavoriteLikedFreeTextPerfectMatchEssayUserNotification();
                        unvo.DataPoints.Remove(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED);
                        unvo.DataPoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, LikedFreeTextEssayType.PerfectMatch.ToString("d"));
                        break;
                    case "USER_NOTIFICATION_LIKED_FREETEXT_PERFECTFIRSTDATEESSAY_TXT":
                        userNotification = new FavoriteLikedFreeTextPerfectFirstDateEssayUserNotification();
                        unvo.DataPoints.Remove(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED);
                        unvo.DataPoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, LikedFreeTextEssayType.PerfectFirstDate.ToString("d"));
                        break;
                    case "USER_NOTIFICATION_LIKED_FREETEXT_IDEALRELATIONSHIPESSAY_TXT":
                        userNotification = new FavoriteLikedFreeTextIdealRelationshipEssayUserNotification();
                        unvo.DataPoints.Remove(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED);
                        unvo.DataPoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, LikedFreeTextEssayType.IdealRelationship.ToString("d"));
                        break;
                    case "USER_NOTIFICATION_LIKED_FREETEXT_LEARNFROMTHEPASTESSAY_TXT":
                        userNotification = new FavoriteLikedFreeTextLearnFromThePastEssayUserNotification();
                        unvo.DataPoints.Remove(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED);
                        unvo.DataPoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, LikedFreeTextEssayType.LearnFromThePast.ToString("d"));
                        break;
                    case "USER_NOTIFICATION_LIKED_FREETEXT_PERSONALITYTRAIT_TXT":
                        userNotification = new FavoriteLikedFreeTextPersonalityTraitUserNotification();
                        unvo.DataPoints.Remove(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED);
                        unvo.DataPoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, LikedFreeTextEssayType.PersonalityTrait.ToString("d"));
                        break;
                    case "USER_NOTIFICATION_LIKED_FREETEXT_LEISUREACTIVITY_TXT":
                        userNotification = new FavoriteLikedFreeTextLeisureActivityUserNotification();
                        unvo.DataPoints.Remove(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED);
                        unvo.DataPoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, LikedFreeTextEssayType.LeisureActivity.ToString("d"));
                        break;
                    case "USER_NOTIFICATION_LIKED_FREETEXT_ENTERTAINMENTLOCATION_TXT":
                        userNotification = new FavoriteLikedFreeTextEntertainmentLocationUserNotification();
                        unvo.DataPoints.Remove(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED);
                        unvo.DataPoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, LikedFreeTextEssayType.EntertainmentLocation.ToString("d"));
                        break;
                    case "USER_NOTIFICATION_LIKED_FREETEXT_PHYSICALACTIVITY_TXT":
                        userNotification = new FavoriteLikedFreeTextPhysicalActivitytUserNotification();
                        unvo.DataPoints.Remove(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED);
                        unvo.DataPoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, LikedFreeTextEssayType.PhysicalActivity.ToString("d"));
                        break;
                    case "USER_NOTIFICATION_LIKED_FREETEXT_CUISINE_TXT":
                        userNotification = new FavoriteLikedFreeTextCuisineUserNotification();
                        unvo.DataPoints.Remove(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED);
                        unvo.DataPoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, LikedFreeTextEssayType.Cuisine.ToString("d"));
                        break;
                    case "USER_NOTIFICATION_LIKED_FREETEXT_MUSIC_TXT":
                        userNotification = new FavoriteLikedFreeTextMusicUserNotification();
                        unvo.DataPoints.Remove(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED);
                        unvo.DataPoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, LikedFreeTextEssayType.Music.ToString("d"));
                        break;
                    case "USER_NOTIFICATION_LIKED_FREETEXT_READING_TXT":
                        userNotification = new FavoriteLikedFreeTextReadingUserNotification();
                        unvo.DataPoints.Remove(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED);
                        unvo.DataPoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, LikedFreeTextEssayType.Reading.ToString("d"));
                        break;

                    case "USER_NOTIFICATION_LIKED_PHOTO_TXT":
                        userNotification = new FavoriteLikedPhotoUserNotification();
                        break;
                }
                if (null != userNotification)
                {
                    Matchnet.Member.ServiceAdapters.Member notificationCreator = MemberSA.Instance.GetMember(unvo.CreatorId, MemberLoadFlags.None);
                    userNotification._g = g;
                    userNotification.ctrl = Control;
                    userNotification.TimeStamp = TimeStamp;
                    userNotification.Created = unvo.Created;
                    userNotification.NotificationOwner = g.Member;
                    userNotification.NotificationCreator = notificationCreator;
                    convertedNotification = userNotification.CreateViewObject();
                    convertedNotification.Viewed = unvo.Viewed;
                    //UserNotificationParams unParams= UserNotificationParams.GetParamsObject(unvo.OwnerId, unvo.SiteId, unvo.CommunityId);
                    //Matchnet.UserNotifications.ServiceAdapters.UserNotificationsServiceSA.Instance.AddUserNotification(unParams, convertedNotification);
                    //Matchnet.UserNotifications.ServiceAdapters.UserNotificationsServiceSA.Instance.RemoveUserNotifications(unParams, unvo);
                }
            }
            catch (Exception e)
            {
                g.ProcessException(e);
            }
            return convertedNotification;
        }
    }

    public abstract class UserNotification
    {
        public Member.ServiceAdapters.Member NotificationCreator = null;
        public Member.ServiceAdapters.Member NotificationOwner = null;
        public DateTime Created = DateTime.Now;
        public bool Viewed = false;
        public DateTime Expired = DateTime.MaxValue;
        public Matchnet.Web.Framework.Ui.BasicElements.LastLoginDate TimeStamp = null;
        public ContextGlobal _g = null;
        public UserControl ctrl = null;
        private Hashtable _additionalParams = null;

        public Hashtable AdditionalParams
        {
            get { return _additionalParams; }
            set { _additionalParams = value; }
        }

        public virtual UserNotification Convert()
        {
            return this;
        }

        public virtual bool IsActivated()
        {
            return this.IsActivated(null);
        }

        public virtual bool IsActivated(Hashtable context)
        {
            return !string.IsNullOrEmpty(GetResourceKey());
        }

        public virtual string GetResourceKey()
        {
            return string.Empty;
        }

        public virtual string GetOmnitureTag()
        {
            return string.Empty;
        }

        public virtual string GenerateText()
        {
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            //string link = "/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&MemberID=" + NotificationCreator.MemberID;
            string link = Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NotificationCreate, NotificationCreator.MemberID, 0, null, 0, false, (int)Matchnet.Web.Framework.Ui.BreadCrumbHelper.PremiumEntryPoint.NoPremium, "", NotificationOwner.MemberID);
            string text = string.Format(baseText, link, GetMemberName());
            return text;
        }

        public virtual string GetThumbnailFile()
        {
            string thumbnailStr;

            if (this.NotificationCreator.HasApprovedPhoto(this._g.Brand.Site.Community.CommunityID))
            {
                Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(_g.Member, NotificationCreator, _g.Brand);
                thumbnailStr = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(_g.Member, NotificationCreator, _g.Brand, photo,
                                                           PhotoType.Thumbnail,
                                                           PrivatePhotoImageType.TinyThumb,
                                                           NoPhotoImageType.BackgroundTinyThumb);
            }
            else
            {
                bool isMale = FrameworkGlobals.IsMaleGender(NotificationCreator.MemberID, _g);
                thumbnailStr = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundTinyThumb, isMale);
            }

            return thumbnailStr;
        }


        public virtual string GetThumbnail()
        {
            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            string thumbHtml = string.Empty;
            int siteid = this._g.Brand.Site.SiteID;
            //for non-subs on jdate and jdate.uk, suppress member thumbnails and links
            if (siteid == (int)WebConstants.SITE_ID.JDate || siteid == (int)WebConstants.SITE_ID.JDateUK)
            {
                if (!this.NotificationOwner.IsPayingMember(siteid))
                {
                    string baseText1 = _g.GetResource("SPRITE_HTML", ctrl);
                    thumbHtml = string.Format(baseText1, "s-icon-notification-match");
                    return thumbHtml;
                }
            }

            //string profileLink = "/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&MemberID=" + NotificationCreator.MemberID;
            string profileLink = Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NotificationCreate, NotificationCreator.MemberID, 0, null, 0, false, (int)Matchnet.Web.Framework.Ui.BreadCrumbHelper.PremiumEntryPoint.NoPremium, "", NotificationOwner.MemberID);

            string thumbnailStr = GetThumbnailFile();

            string baseText2 = _g.GetResource("IMAGE_HTML", ctrl);
            thumbHtml = string.Format(baseText2, profileLink, thumbnailStr, GetMemberName());
            return thumbHtml;
        }

        public virtual string GetMemberName()
        {
            return NotificationCreator.GetUserName(this._g.Brand);
        }

        public string GenerateTimeStamp()
        {
            TimeStamp.LastLoginDateValue = Created;
            return TimeStamp.BuildLastLoginDate();
        }

        public virtual Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.NotTyped;
        }

        public virtual bool IsSystemNotification()
        {
            return false;
        }

        protected void InitializeStandardDataPoints(Dictionary<string, string> datapoints) 
        {
            if (NotificationCreator != null)
            {
                int genderMask = NotificationCreator.GetAttributeInt(_g.Brand, "GenderMask");
                int regionId = NotificationCreator.GetAttributeInt(_g.Brand, "RegionId");
                string maritalStatus = Option.GetContent("MaritalStatus", NotificationCreator.GetAttributeInt(_g.Brand, "MaritalStatus"), _g);

                datapoints.Add(UserNotificationDataPointConstants.CREATOR_USER_NAME,NotificationCreator.GetUserName(_g.Brand));
                datapoints.Add(UserNotificationDataPointConstants.CREATOR_AGE,FrameworkGlobals.GetAge(NotificationCreator, _g.Brand).ToString());
                datapoints.Add(UserNotificationDataPointConstants.CREATOR_GENDERMASK,FrameworkGlobals.GetGenderMaskString(genderMask));
                datapoints.Add(UserNotificationDataPointConstants.CREATOR_LOCATION,FrameworkGlobals.GetRegionString(regionId, _g.Brand.Site.LanguageID, false, true, false));
                datapoints.Add(UserNotificationDataPointConstants.CREATOR_MARITAL_STATUS, maritalStatus);
            }
        }

        public virtual void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            
        }

        public virtual UserNotificationViewObject CreateViewObject()
        {
            UserNotificationViewObject viewObj = new UserNotificationViewObject(
                                                this.GetMemberName(),
                                                this.NotificationOwner.MemberID,
                                                this.NotificationCreator.MemberID,
                                                this.GetThumbnail(),
                                                this.GenerateText(),
                                                this.GenerateTimeStamp(),
                                                this.Viewed,
                                                this.Created,
                                                this._g.Brand.Site.SiteID,
                                                this._g.Brand.Site.Community.CommunityID,
                                                this.GetResourceKey(),
                                                this.GetOmnitureTag(),
                                                this.IsSystemNotification(),
                                                this.GetThumbnailFile(), 
                                                this.GetNotificationType()
                                                );

            InitializeStandardDataPoints(viewObj.DataPoints);
            AddNotificationSpecificDataPoints(viewObj.DataPoints);

            return viewObj;
        }

        
    }

    public class ViewedYourProfileUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            string strFormat = "USER_NOTIFICATION_VIEWED_YOU_MESSAGE_TXT";
            //for non-subs, check if viewed you is a subscriber only feature
            ListManager listManager = new ListManager();
            if (!listManager.IsViewedYourProfileListAvailableToMember(this.NotificationOwner, this._g.Brand))
            {
                strFormat = "NON_SUB_USER_NOTIFICATION_VIEWED_YOU_MESSAGE_TXT";
            }
            return strFormat;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.ProfileActivity) + " - Viewed your profile";
        }

        public override UserNotification Convert()
        {
            return new FavoriteViewedYourProfileUserNotification();
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.ViewedYourProfile;
        }

        public override bool IsActivated()
        {
            bool b = base.IsActivated();
            if (b)
            {
                b = !((NotificationCreator.GetAttributeInt(_g.TargetBrand, "HideMask", 0) & (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists);
            }
            return b;
        }

    }

    public class FlirtedYouUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            int siteid = this._g.Brand.Site.SiteID;
            string strFormat = "USER_NOTIFICATION_FLIRTED_YOU_MESSAGE_TXT";
            //for non-subs on jdate and jdate.uk, suppress member thumbnails and links
            if (siteid == (int)WebConstants.SITE_ID.JDate || siteid == (int)WebConstants.SITE_ID.JDateUK)
            {
                if (!this.NotificationOwner.IsPayingMember(siteid))
                {
                    strFormat = "NON_SUB_USER_NOTIFICATION_FLIRTED_YOU_MESSAGE_TXT";
                }
            }
            return strFormat;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.ProfileActivity) + " - Flirted with you";
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.FlirtedYou;
        }

        public override UserNotification Convert()
        {
            return new FavoriteFlirtedYouUserNotification();
        }

        public override string GenerateText()
        {
            string mailLink = "/Applications/Email/MailBox.aspx";

            if (AdditionalParams != null && AdditionalParams.ContainsKey("messageid") && AdditionalParams.ContainsKey("messagefolderid"))
            {
                mailLink = string.Format("/Applications/Email/ViewMessage.aspx?MemberMailID={0}&MemberFolderID={1}",
                                AdditionalParams["messageid"],
                                AdditionalParams["messagefolderid"]);
            }

            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            string link = Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NotificationCreate, NotificationCreator.MemberID, 0, null, 0, false, (int)Matchnet.Web.Framework.Ui.BreadCrumbHelper.PremiumEntryPoint.NoPremium, "", NotificationOwner.MemberID);

            string text = string.Empty;

            if (!baseText.Contains("{1}")) //resource requires 1 or less param
            {
                text = string.Format(baseText, mailLink);
            }
            else
            {
                text = string.Format(baseText, link, GetMemberName(), mailLink);
            }


            return text;
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (AdditionalParams != null && AdditionalParams.ContainsKey("messageid"))
            {
                datapoints.Remove(UserNotificationDataPointConstants.MAIL_MESSAGE_ID);
                datapoints.Add(UserNotificationDataPointConstants.MAIL_MESSAGE_ID, AdditionalParams["messageid"].ToString());
            }
            if (AdditionalParams != null && AdditionalParams.ContainsKey("messagefolderid"))
            {
                datapoints.Remove(UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID);
                datapoints.Add(UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID, AdditionalParams["messagefolderid"].ToString());
            }
        }
    }

    public class EmailedYouUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            int siteid = this._g.Brand.Site.SiteID;
            string strFormat = "USER_NOTIFICATION_EMAILED_YOU_MESSAGE_TXT";
            //for non-subs on jdate and jdate.uk, suppress member thumbnails and links
            if (siteid == (int)WebConstants.SITE_ID.JDate || siteid == (int)WebConstants.SITE_ID.JDateUK)
            {
                if (!this.NotificationOwner.IsPayingMember(siteid))
                {
                    strFormat = "NON_SUB_USER_NOTIFICATION_EMAILED_YOU_MESSAGE_TXT";
                }
            }
            return strFormat;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.ProfileActivity) + " - Emailed you";
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.EmailedYou;
        }

        public override UserNotification Convert()
        {
            return new FavoriteEmailedYouUserNotification();
        }

        public override string GenerateText()
        {
            string mailLink = "/Applications/Email/MailBox.aspx";

            if (AdditionalParams != null && (AdditionalParams.ContainsKey("messageid") && AdditionalParams.ContainsKey("messagefolderid")))
            {
                mailLink = string.Format("/Applications/Email/ViewMessage.aspx?MemberMailID={0}&MemberFolderID={1}",
                                AdditionalParams["messageid"],
                                AdditionalParams["messagefolderid"]);
            }
            
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            string link = Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NotificationCreate, NotificationCreator.MemberID, 0, null, 0, false, (int)Matchnet.Web.Framework.Ui.BreadCrumbHelper.PremiumEntryPoint.NoPremium, "", NotificationOwner.MemberID);

            string text = string.Empty;

            if (!baseText.Contains("{1}")) //resource requires 1 or less param
            {
                text = string.Format(baseText, mailLink);
            }
            else
            {
                 text = string.Format(baseText, link, GetMemberName(), mailLink);
            }
            
            return text;
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (AdditionalParams != null && datapoints != null && AdditionalParams.ContainsKey("messageid"))
            {
                datapoints.Remove(UserNotificationDataPointConstants.MAIL_MESSAGE_ID);
                datapoints.Add(UserNotificationDataPointConstants.MAIL_MESSAGE_ID, AdditionalParams["messageid"].ToString());
            }
            if (AdditionalParams != null && datapoints != null && AdditionalParams.ContainsKey("messagefolderid"))
            {
                datapoints.Remove(UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID);
                datapoints.Add(UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID, AdditionalParams["messagefolderid"].ToString());
            }
        }
    }

    public class AddedYouAsFavoriteUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_ADDED_YOU_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.ProfileActivity) + " - Added you to Favorites";
        }

        public override string GenerateText()
        {
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            //string link = "/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&MemberID=" + NotificationCreator.MemberID;
            string link = Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NotificationCreate, NotificationCreator.MemberID, 0, null, 0, false, (int)Matchnet.Web.Framework.Ui.BreadCrumbHelper.PremiumEntryPoint.NoPremium, "", NotificationOwner.MemberID);
            string link2 = "/Applications/Email/Compose.aspx?PrtId=834&MemberId=" + NotificationCreator.MemberID;
            string text = string.Format(baseText, link, GetMemberName(), link2);
            return text;
        }

        public override UserNotification Convert()
        {
            return new FavoriteAddedYouAsFavoriteUserNotification();
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.AddedYouAsFavorite;
        }

        public override bool IsActivated()
        {
            bool b = base.IsActivated();
            if (b)
            {
                b = !((NotificationCreator.GetAttributeInt(_g.TargetBrand, "HideMask", 0) & (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists);
            }
            return b;
        }
    }

    public class ECardedYouUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            int siteid = this._g.Brand.Site.SiteID;
            string strFormat = "USER_NOTIFICATION_ECARDED_YOU_MESSAGE_TXT";
            //for non-subs on jdate and jdate.uk, suppress member thumbnails and links
            if (siteid == (int)WebConstants.SITE_ID.JDate || siteid == (int)WebConstants.SITE_ID.JDateUK)
            {
                if (!this.NotificationOwner.IsPayingMember(siteid))
                {
                    strFormat = "NON_SUB_USER_NOTIFICATION_ECARDED_YOU_MESSAGE_TXT";
                }
            }
            return strFormat;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.ProfileActivity) + " - Sent you an eCard";
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.ECardedYou;
        }

        public override UserNotification Convert()
        {
            return new FavoriteECardedYouUserNotification();
        }
    }

    public class IMedYouUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            int siteid = this._g.Brand.Site.SiteID;
            string strFormat = "USER_NOTIFICATION_IMED_YOU_MESSAGE_TXT";
            //for non-subs on jdate and jdate.uk, suppress member thumbnails and links
            if (siteid == (int)WebConstants.SITE_ID.JDate || siteid == (int)WebConstants.SITE_ID.JDateUK)
            {
                if (!this.NotificationOwner.IsPayingMember(siteid))
                {
                    strFormat = "NON_SUB_USER_NOTIFICATION_IMED_YOU_MESSAGE_TXT";
                }
            }
            return strFormat;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.ProfileActivity) + " - IM\'d you";
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.IMedYou;
        }

        public override UserNotification Convert()
        {
            return new FavoriteIMedYouUserNotification();
        }
    }

    public class MutualYesUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_MUTUAL_YES_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.ProfileActivity) + " - you both said Yes";
        }

        public override UserNotification Convert()
        {
            return new FavoriteMutualYesUserNotification();
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.MutualYes;
        }

        public override bool IsActivated()
        {
            bool b = base.IsActivated();
            if (b)
            {
                Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(this.NotificationCreator.MemberID);
                ClickMask clickMask = list.GetClickMask(_g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID, this.NotificationOwner.MemberID);
                b = ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes
                        && (clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes);
            }
            return b;
        }
    }

    public class FavoriteViewedYourProfileUserNotification : ViewedYourProfileUserNotification
    {
        public override string GetResourceKey()
        {
            string strFormat = "USER_NOTIFICATION_FAVORITE_VIEWED_YOU_MESSAGE_TXT";
            //for non-subs, check if viewed you is a subscriber only feature
            ListManager listManager = new ListManager();
            if (!listManager.IsViewedYourProfileListAvailableToMember(this.NotificationOwner, this._g.Brand))
            {
                strFormat = "NON_SUB_USER_NOTIFICATION_VIEWED_YOU_MESSAGE_TXT";
            }
            return strFormat;

        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.HotListUpdates) + " - Viewed your profile";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }
        }
    }

    public class FavoriteFlirtedYouUserNotification : FlirtedYouUserNotification
    {
        public override string GetResourceKey()
        {
            int siteid = this._g.Brand.Site.SiteID;
            string strFormat = "USER_NOTIFICATION_FAVORITE_FLIRTED_YOU_MESSAGE_TXT";
            //for non-subs on jdate and jdate.uk, suppress member thumbnails and links
            if (siteid == (int)WebConstants.SITE_ID.JDate || siteid == (int)WebConstants.SITE_ID.JDateUK)
            {
                if (!this.NotificationOwner.IsPayingMember(siteid))
                {
                    strFormat = "NON_SUB_USER_NOTIFICATION_FLIRTED_YOU_MESSAGE_TXT";
                }
            }
            return strFormat;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.HotListUpdates) + " - Flirted with you";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                base.AddNotificationSpecificDataPoints(datapoints);
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }
        }
    }

    public class FavoriteEmailedYouUserNotification : EmailedYouUserNotification
    {
        public override string GetResourceKey()
        {
            int siteid = this._g.Brand.Site.SiteID;
            string strFormat = "USER_NOTIFICATION_FAVORITE_EMAILED_YOU_MESSAGE_TXT";
            //for non-subs on jdate and jdate.uk, suppress member thumbnails and links
            if (siteid == (int)WebConstants.SITE_ID.JDate || siteid == (int)WebConstants.SITE_ID.JDateUK)
            {
                if (!this.NotificationOwner.IsPayingMember(siteid))
                {
                    strFormat = "NON_SUB_USER_NOTIFICATION_EMAILED_YOU_MESSAGE_TXT";
                }
            }
            return strFormat;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.HotListUpdates) + " - Emailed you";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                base.AddNotificationSpecificDataPoints(datapoints);
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }
        }
    }

    public class FavoriteAddedYouAsFavoriteUserNotification : AddedYouAsFavoriteUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_ADDED_YOU_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.HotListUpdates) + " - Added you to Favorites";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }
        }
    }

    public class FavoriteECardedYouUserNotification : ECardedYouUserNotification
    {
        public override string GetResourceKey()
        {
            int siteid = this._g.Brand.Site.SiteID;
            string strFormat = "USER_NOTIFICATION_FAVORITE_ECARDED_YOU_MESSAGE_TXT";
            //for non-subs on jdate and jdate.uk, suppress member thumbnails and links
            if (siteid == (int)WebConstants.SITE_ID.JDate || siteid == (int)WebConstants.SITE_ID.JDateUK)
            {
                if (!this.NotificationOwner.IsPayingMember(siteid))
                {
                    strFormat = "NON_SUB_USER_NOTIFICATION_ECARDED_YOU_MESSAGE_TXT";
                }
            }
            return strFormat;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.HotListUpdates) + " - Sent you an eCard";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }
        }
    }

    public class FavoriteIMedYouUserNotification : IMedYouUserNotification
    {
        public override string GetResourceKey()
        {
            int siteid = this._g.Brand.Site.SiteID;
            string strFormat = "USER_NOTIFICATION_FAVORITE_IMED_YOU_MESSAGE_TXT";
            //for non-subs on jdate and jdate.uk, suppress member thumbnails and links
            if (siteid == (int)WebConstants.SITE_ID.JDate || siteid == (int)WebConstants.SITE_ID.JDateUK)
            {
                if (!this.NotificationOwner.IsPayingMember(siteid))
                {
                    strFormat = "NON_SUB_USER_NOTIFICATION_IMED_YOU_MESSAGE_TXT";
                }
            }
            return strFormat;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.HotListUpdates) + " - IM\'d you";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }
        }
    }

    public class FavoriteMutualYesUserNotification : MutualYesUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_MUTUAL_YES_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.HotListUpdates) + " - you both said Yes";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }
        }
    }

    //User watching you gets notifications
    public class FavoriteUpdatedProfileUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_UPDATED_PROFILE_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.HotListUpdates) + " - Favorite has updated profile";
        }

        public override string GenerateText()
        {
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            int genderMask = NotificationCreator.GetAttributeInt(_g.Brand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK);
            string pronoun = ((genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) ? "TXT_HER" : "TXT_HIS";
            //string link = "/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&MemberID=" + NotificationCreator.MemberID;
            string link = Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NotificationCreate, NotificationCreator.MemberID, 0, null, 0, false, (int)Matchnet.Web.Framework.Ui.BreadCrumbHelper.PremiumEntryPoint.NoPremium, "", NotificationOwner.MemberID);
            string text = string.Format(baseText, link, GetMemberName(), _g.GetResource(pronoun, ctrl));
            return text;
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.FavoriteUpdatedProfile;
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }
        }
    }

    //User watching you gets notifications
    public class FavoriteUploadedPhotoUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_UPLOADED_PHOTO_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.HotListUpdates) + " - Favorite has uploaded new photos";
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.FavoriteUploadedPhoto;
        }

        public override string GenerateText()
        {
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            //string link = "/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&MemberID=" + NotificationCreator.MemberID;
            string link = Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NotificationCreate, NotificationCreator.MemberID, 0, null, 0, false, (int)Matchnet.Web.Framework.Ui.BreadCrumbHelper.PremiumEntryPoint.NoPremium, "", NotificationOwner.MemberID);
            string link2 = Matchnet.Web.Framework.Ui.BreadCrumbHelper.AppendParamToProfileLink(link, WebConstants.URL_PARAMETER_NAME_PROFILETAB + "=Photo");
            string text = string.Format(baseText, link, GetMemberName(), link2);
            return text;
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }
        }
    }

    //User watching you gets notifications
    public class FavoriteAnsweredQandAUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_ANSWERED_QANDA_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.HotListUpdates) + " - Answered new Q & A";
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.FavoriteAnsweredQandA;
        }

        public override string GenerateText()
        {
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            //string link = "/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&MemberID=" + NotificationCreator.MemberID;
            string link = Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NotificationCreate, NotificationCreator.MemberID, 0, null, 0, false, (int)Matchnet.Web.Framework.Ui.BreadCrumbHelper.PremiumEntryPoint.NoPremium, "", NotificationOwner.MemberID);
            string link2 = Matchnet.Web.Framework.Ui.BreadCrumbHelper.AppendParamToProfileLink(link, WebConstants.URL_PARAMETER_NAME_PROFILETAB + "=Questions");
            string text = string.Format(baseText, link, GetMemberName(), link2);
            return text;
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }
        }
    }

    public class NewMatchesAddedUserNotification : UserNotification
    {
        protected ArrayList newMatches = null;

        protected virtual void GetYourMatches(SearchPreferenceCollection prefs)
        {
            int startRow = 0;
            //Ensure that domainID is set on search preferences for the correct Community.
            prefs.Add("DomainID", _g.Brand.Site.Community.CommunityID.ToString());
            prefs.Add("ColorCode", "");
            if (string.IsNullOrEmpty(prefs["SearchOrderBy"]))
                prefs.Add("SearchOrderBy", "1");

            newMatches = new ArrayList();
            MatchnetQueryResults searchResults = null;
            try
            {
                // Potential for exception due to invalid search preferences on this member's stored set of preferences.
                searchResults = MemberSearchSA.Instance.Search(
                    prefs,
                    _g.Brand.Site.Community.CommunityID,
                    _g.Brand.Site.SiteID,
                    startRow,
                    100);
            }
            catch (Exception)
            {
                //invalid search preferences, initialize an empty results list so the home page will render correctly.
                searchResults = new MatchnetQueryResults(0);
            }

            ArrayList members = new ArrayList();
            if (searchResults != null && searchResults.ToArrayList().Count > 0)
            {
                members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);
                DateTime PastWeek = DateTime.Now.AddDays(-7);
                foreach (Member.ServiceAdapters.Member member in members)
                {
                    //add member
                    DateTime joinDate = member.GetInsertDate(_g.Brand.Site.Community.CommunityID);
                    string name = member.GetUserName(_g.Brand);

                    if (joinDate > PastWeek)
                    {
                        newMatches.Add(member);
                    }

                }
            }
        }

        //TODO: Only create this once
        public override bool IsActivated(Hashtable context)
        {
            bool b = base.IsActivated(context);
            if (b)
            {
                List<UserNotificationViewObject> notifications = (null != context) ? context["notifications"] as List<UserNotificationViewObject> : null;
                bool hasNotification = false;
                if (null != notifications)
                {
                    UserNotificationViewObject match = notifications.FindLast(delegate(UserNotificationViewObject un)
                    {
                        return un.Equals(this.CreateViewObject());
                    });

                    hasNotification = (null != match && match.Created > DateTime.Now.AddDays(-7));
                }

                if (!hasNotification)
                {
                    GetYourMatches(_g.SearchPreferences);
                    _g.ResetSearchPreferences();
                    b = newMatches.Count > 0;
                }
                else
                {
                    b = false;
                }

            }
            return b;
        }

        public override string GetResourceKey()
        {
            string suffixKey = (null != this.newMatches && this.newMatches.Count > 1) ? "_PLURAL" : "_SINGLE";
            return "USER_NOTIFICATION_NEW_MATCHES_ADDED_MESSAGE_TXT" + suffixKey;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.MatchActivity) + " - New matches added";
        }

        public override string GenerateText()
        {
            if (null == newMatches) GetYourMatches(_g.SearchPreferences);
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            int numOfMatches = newMatches.Count;
            string text = string.Format(baseText, numOfMatches);
            return text;
        }

        public override string GetThumbnail()
        {
            string baseText = _g.GetResource("SPRITE_HTML", ctrl);
            string thumbHtml = string.Format(baseText, "s-icon-notification-match");
            return thumbHtml;
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.NewMatchesAdded;
        }

        public override bool IsSystemNotification()
        {
            return true;
        }

        public override string GetThumbnailFile()
        {
            return string.Empty;
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if(newMatches != null && datapoints != null)
            {
                datapoints.Remove(UserNotificationDataPointConstants.NUMBER_NEW_MATCHES);
                datapoints.Add(UserNotificationDataPointConstants.NUMBER_NEW_MATCHES, newMatches.Count.ToString());
            }
        }
    }

    public class NewMembersInLocationUserNotification : NewMatchesAddedUserNotification
    {
        private ResultListHandler handler = new ResultListHandler(null, null);

        //TODO: Only create this once
        public override bool IsActivated(Hashtable context)
        {
            bool b = !string.IsNullOrEmpty(GetResourceKey());
            if (b)
            {
                List<UserNotificationViewObject> notifications = (null != context) ? context["notifications"] as List<UserNotificationViewObject> : null;
                bool hasNotification = false;
                if (null != notifications)
                {
                    UserNotificationViewObject match = notifications.FindLast(delegate(UserNotificationViewObject un)
                    {
                        return un.Equals(this.CreateViewObject());
                    });

                    hasNotification = (null != match && match.Created > DateTime.Now.AddDays(-7));
                }

                if (!hasNotification)
                {
                    _g.QuickSearchPreferences.Add("LanguageMask", HttpContext.Current.Request["LanguageMask"]);
                    GetYourMatches(_g.QuickSearchPreferences);
                    _g.ResetSearchPreferences();
                    b = newMatches.Count > 0;
                }
                else
                {
                    b = false;
                }

            }
            return b;
        }

        public override string GetResourceKey()
        {
            string suffixKey = (null != this.newMatches && this.newMatches.Count > 1) ? "_PLURAL" : "_SINGLE";
            return "USER_NOTIFICATION_NEW_MEMBERS_IN_LOCATION_MESSAGE_TXT" + suffixKey;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.MatchActivity) + " - New members in your area";
        }

        public override string GenerateText()
        {
            if (null == newMatches) GetYourMatches(_g.QuickSearchPreferences);
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            int numOfMatches = newMatches.Count;
            string link = "/Applications/QuickSearch/QuickSearchResults.aspx?SearchOrderBy=1";
            string text = string.Format(baseText, link, numOfMatches);
            return text;
        }

        public override string GetThumbnail()
        {
            string baseText = _g.GetResource("SPRITE_HTML", ctrl);
            string thumbHtml = string.Format(baseText, "s-icon-notification-match");
            return thumbHtml;
        }

        public override bool IsSystemNotification()
        {
            return true;
        }

        public override string GetThumbnailFile()
        {
            return string.Empty;
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.NewMembersInLocation;
        }
    }

    public class UpdateProfileNudgeUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_NUDGE_UPDATE_PROFILE_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.Nudges) + " - Update your profile";
        }

        public override bool IsActivated(Hashtable context)
        {
            bool b = base.IsActivated(context);
            if (b)
            {
                List<UserNotificationViewObject> notifications = (null != context) ? context["notifications"] as List<UserNotificationViewObject> : null;
                bool hasNotification = false;
                if (null != notifications)
                {
                    UserNotificationViewObject match = notifications.FindLast(delegate(UserNotificationViewObject un)
                    {
                        return un.Equals(this.CreateViewObject());
                    });
                    hasNotification = (null != match && match.Created > DateTime.Now.AddDays(-14));
                }
                b = !hasNotification;
            }
            return b;
        }

        public override string GetThumbnail()
        {
            string baseText = _g.GetResource("SPRITE_HTML", ctrl);
            string thumbHtml = string.Format(baseText, "s-icon-notification-nudges");
            return thumbHtml;
        }
       
        public override UserNotificationViewObject CreateViewObject()
        {
            UserNotificationViewObject viewObj = new UserNotificationViewObject(
                                                this.GetMemberName(),
                                                this.NotificationOwner.MemberID,
                                                this.NotificationCreator.MemberID,
                                                this.GetThumbnail(),
                                                this.GenerateText(),
                                                string.Empty,
                                                this.Viewed,
                                                this.Created,
                                                this._g.Brand.Site.SiteID,
                                                this._g.Brand.Site.Community.CommunityID,
                                                this.GetResourceKey(),
                                                this.GetOmnitureTag(), 
                                                this.IsSystemNotification(), 
                                                this.GetThumbnailFile(), 
                                                this.GetNotificationType());
            return viewObj;
        }

        public override bool IsSystemNotification()
        {
            return true;
        }

        public override string GetThumbnailFile()
        {
            return string.Empty;
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.UpdateProfileNudge;
        }
    }

    public class UploadPhotosNudgeUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_NUDGE_UPLOAD_PHOTOS_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.Nudges) + " - Add photos";
        }

        public override bool IsActivated(Hashtable context)
        {
            bool b = base.IsActivated(context);
            if (b)
            {
                List<UserNotificationViewObject> notifications = (null != context) ? context["notifications"] as List<UserNotificationViewObject> : null;
                bool hasNotification = false;
                if (null != notifications)
                {
                    UserNotificationViewObject match = notifications.FindLast(delegate(UserNotificationViewObject un)
                    {
                        return un.Equals(this.CreateViewObject());
                    });

                    hasNotification = (null != match && match.Created > DateTime.Now.AddDays(-14));
                }
                b = !hasNotification;
            }
            return b;
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.UploadPhotoNudge;
        }

        public override string GetThumbnail()
        {
            string baseText = _g.GetResource("SPRITE_HTML", ctrl);
            string thumbHtml = string.Format(baseText, "s-icon-notification-nudges");
            return thumbHtml;
        }

        public override UserNotificationViewObject CreateViewObject()
        {
            UserNotificationViewObject viewObj = new UserNotificationViewObject(
                                                this.GetMemberName(),
                                                this.NotificationOwner.MemberID,
                                                this.NotificationCreator.MemberID,
                                                this.GetThumbnail(),
                                                this.GenerateText(),
                                                string.Empty,
                                                this.Viewed,
                                                this.Created,
                                                this._g.Brand.Site.SiteID,
                                                this._g.Brand.Site.Community.CommunityID,
                                                this.GetResourceKey(),
                                                this.GetOmnitureTag(),
                                                this.IsSystemNotification(), 
                                                this.GetThumbnailFile(), 
                                                this.GetNotificationType());
            return viewObj;
        }

        public override bool IsSystemNotification()
        {
            return true;
        }

        public override string GetThumbnailFile()
        {
            return string.Empty;
        }
    }

    public class AnswerTodaySparksNudgeUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_NUDGE_ANSWER_TODAY_SPARKS_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.Nudges) + " - Answer Today\'s Sparks";
        }

        public override bool IsActivated(Hashtable context)
        {
            bool b = base.IsActivated(context);
            if (b)
            {
                List<UserNotificationViewObject> notifications = (null != context) ? context["notifications"] as List<UserNotificationViewObject> : null;
                bool hasNotification = false;
                if (null != notifications)
                {
                    UserNotificationViewObject match = notifications.FindLast(delegate(UserNotificationViewObject un)
                    {
                        return un.Equals(this.CreateViewObject());
                    });

                    hasNotification = (null != match && match.Created > DateTime.Now.AddDays(-14));
                }
                b = !hasNotification;
            }
            return b;
        }

        public override string GetThumbnail()
        {
            string baseText = _g.GetResource("SPRITE_HTML", ctrl);
            string thumbHtml = string.Format(baseText, "s-icon-notification-nudges");
            return thumbHtml;
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.AnswerTodaySparksNudge;
        }

        public override UserNotificationViewObject CreateViewObject()
        {
            UserNotificationViewObject viewObj = new UserNotificationViewObject(
                                                this.GetMemberName(),
                                                this.NotificationOwner.MemberID,
                                                this.NotificationCreator.MemberID,
                                                this.GetThumbnail(),
                                                this.GenerateText(),
                                                string.Empty,
                                                this.Viewed,
                                                this.Created,
                                                this._g.Brand.Site.SiteID,
                                                this._g.Brand.Site.Community.CommunityID,
                                                this.GetResourceKey(),
                                                this.GetOmnitureTag(),
                                                this.IsSystemNotification(),
                                                this.GetThumbnailFile(),
                                                this.GetNotificationType());
            return viewObj;
        }

        public override bool IsSystemNotification()
        {
            return true;
        }

        public override string GetThumbnailFile()
        {
            return string.Empty;
        }
    }

    public class SubscriptionRenewalUserNotification : UserNotification
    {
        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.SubscriptionRenewal;
        }
        
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_SUBSCRIPTION_RENEWAL_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.Subscription) + " - Subscription renewal";
        }

        public override bool IsActivated(Hashtable context)
        {
            bool b = base.IsActivated(context);
            //TODO: Only create once per session?
            if (b)
            {
                List<UserNotificationViewObject> notifications = (null != context) ? context["notifications"] as List<UserNotificationViewObject> : null;
                bool hasNotification = false;
                if (null != notifications)
                {
                    UserNotificationViewObject match = notifications.FindLast(delegate(UserNotificationViewObject un)
                    {
                        return un.Equals(this.CreateViewObject());
                    });

                    hasNotification = (null != match);
                }

                //MemberSub memberSub = PurchaseSA.Instance.GetSubscription(this.NotificationOwner.MemberID, _g.Brand.Site.SiteID);
                Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(this.NotificationOwner.MemberID, _g.Brand);
                if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0 && renewalSub.IsRenewalEnabled) return false; //auto renewal is on so do not activate this notification       
                if (!this.NotificationOwner.IsPayingMember(_g.Brand.Site.SiteID)) return false; //only show this notification to subscribers

                if (!hasNotification)
                {
                    DateTime expiration = this.NotificationOwner.GetAttributeDate(_g.Brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE, DateTime.MinValue);
                    //if now is before expiration and now is within 7 days of expiration
                    b = (!DateTime.MinValue.Equals(expiration)) && (DateTime.Now < expiration && expiration.AddDays(-7) < DateTime.Now);
                }
                else
                {
                    b = false;
                }
            }
            return b;
        }

        public override string GenerateText()
        {
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            DateTime expiration = this.NotificationOwner.GetAttributeDate(_g.Brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE, DateTime.MinValue);
            double days = Math.Ceiling((expiration - DateTime.Now).TotalDays);
            string daysTxt = (days > 1) ? _g.GetResource("PLURAL_DAYS", ctrl) : _g.GetResource("SINGLE_DAY", ctrl);
            string text = string.Format(baseText, days, daysTxt);
            return text;
        }

        public override string GetThumbnail()
        {
            string baseText = _g.GetResource("SPRITE_HTML", ctrl);
            string thumbHtml = string.Format(baseText, "s-icon-notification-sub");
            return thumbHtml;
        }

        public override UserNotificationViewObject CreateViewObject()
        {
            UserNotificationViewObject viewObj = new UserNotificationViewObject(
                                               this.GetMemberName(),
                                               this.NotificationOwner.MemberID,
                                               this.NotificationCreator.MemberID,
                                               this.GetThumbnail(),
                                               this.GenerateText(),
                                               string.Empty,
                                               this.Viewed,
                                               this.Created,
                                               this._g.Brand.Site.SiteID,
                                               this._g.Brand.Site.Community.CommunityID,
                                               this.GetResourceKey(),
                                               this.GetOmnitureTag(),
                                               this.IsSystemNotification(),
                                               this.GetThumbnailFile(),
                                               this.GetNotificationType());
            return viewObj;
        }

        public override bool IsSystemNotification()
        {
            return true;
        }

        public override string GetThumbnailFile()
        {
            return string.Empty;
        }
    }

    public class SubscriptionExpiredUserNotification : SubscriptionRenewalUserNotification
    {
        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.SubscriptionExpired;
        }
        
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_SUBSCRIPTION_EXPIRED_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.Subscription) + " - Subscription expired";
        }

        public override bool IsActivated(Hashtable context)
        {
            bool b = !string.IsNullOrEmpty(GetResourceKey());
            if (b)
            {
                //TODO: Only create once per session?
                List<UserNotificationViewObject> notifications = (null != context) ? context["notifications"] as List<UserNotificationViewObject> : null;
                bool hasNotification = false;
                if (null != notifications)
                {
                    UserNotificationViewObject match = notifications.FindLast(delegate(UserNotificationViewObject un)
                    {
                        return un.Equals(this.CreateViewObject());
                    });

                    hasNotification = (null != match);
                }

                //MemberSub memberSub = PurchaseSA.Instance.GetSubscription(this.NotificationOwner.MemberID, _g.Brand.Site.SiteID);
                Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(this.NotificationOwner.MemberID, _g.Brand);
                if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0 && renewalSub.IsRenewalEnabled) return false; //auto renewal is on so do not activate this notification       
                if (!this.NotificationOwner.IsPayingMember(_g.Brand.Site.SiteID)) return false; //only show this notification to subscribers

                if (!hasNotification)
                {
                    DateTime expiration = this.NotificationOwner.GetAttributeDate(_g.Brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE, DateTime.MinValue);
                    //if now is after expiration date
                    b = (!DateTime.MinValue.Equals(expiration)) && DateTime.Now > expiration;
                }
                else
                {
                    b = false;
                }
            }
            return b;
        }

        public override string GenerateText()
        {
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            DateTime expiration = this.NotificationOwner.GetAttributeDate(_g.Brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE, DateTime.MinValue);
            string dateString = string.Format("{0:MMM d, yyyy}", expiration);
            string text = string.Format(baseText, dateString);
            return text;
        }

        public override bool IsSystemNotification()
        {
            return true;
        }

        public override string GetThumbnailFile()
        {
            return string.Empty;
        }
    }

    public class ResourceDrivenNudgeUserNotification : UserNotification
    {
        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.ResourceDrivenNudge;
        }
        
        public string resourceKey;

        public override string GetResourceKey()
        {
            return resourceKey;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.Nudges);
        }

        public override bool IsActivated(Hashtable context)
        {
            bool b = base.IsActivated(context);
            if (b)
            {
                List<UserNotificationViewObject> notifications = (null != context) ? context["notifications"] as List<UserNotificationViewObject> : null;
                bool hasNotification = false;
                if (null != notifications)
                {
                    UserNotificationViewObject match = notifications.FindLast(delegate(UserNotificationViewObject unvo)
                    {
                        UserNotificationViewObject vo = this.CreateViewObject();
                        bool b2 = unvo.Equals(vo);
                        if (b2)
                        {
                            //if resource keys are the same, make sure text is the same.  if not, replace.
                            b2 = unvo.Text.Equals(vo.Text);
                        }
                        return b2;
                    });
                    hasNotification = (null != match);
                }
                b = !hasNotification;
            }
            return b;
        }

        public override bool IsSystemNotification()
        {
            return true;
        }

        public override string GetThumbnailFile()
        {
            return string.Empty;
        }
    }

    public class AllAccessEmailedYouUserNotification : UserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_EMAILED_VIP_MESSAGE_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.ProfileActivity) + " - AllAccess Emailed you";
        }

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.AllAccessEmailedYou;
        }

        public override string GenerateText()
        {
            string mailLink = "/Applications/Email/MailBox.aspx";

            if (AdditionalParams != null && AdditionalParams.ContainsKey("messageid") && AdditionalParams.ContainsKey("messagefolderid"))
            {
                mailLink = string.Format("/Applications/Email/ViewMessage.aspx?MemberMailID={0}&MemberFolderID={1}",
                                AdditionalParams["messageid"],
                                AdditionalParams["messagefolderid"]);
            }

            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            //string link = "/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&MemberID=" + NotificationCreator.MemberID;
            string link = Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NotificationCreate, NotificationCreator.MemberID, 0, null, 0, false, (int)Matchnet.Web.Framework.Ui.BreadCrumbHelper.PremiumEntryPoint.NoPremium, "", NotificationOwner.MemberID);
            string text = string.Format(baseText, link, GetMemberName(), mailLink);
            return text;
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (AdditionalParams != null && datapoints != null && AdditionalParams.ContainsKey("messageid"))
            {
                datapoints.Remove(UserNotificationDataPointConstants.MAIL_MESSAGE_ID);
                datapoints.Add(UserNotificationDataPointConstants.MAIL_MESSAGE_ID, AdditionalParams["messageid"].ToString());
            }
            if (AdditionalParams != null && datapoints != null && AdditionalParams.ContainsKey("messagefolderid"))
            {
                datapoints.Remove(UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID);
                datapoints.Add(UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID, AdditionalParams["messagefolderid"].ToString());
            }
        }
    }

    public class LikedAnswerUserNotification : UserNotification
    {
        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.LikedAnswer;
        }
        
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_LIKED_ANSWER_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.ProfileActivity) + " - Liked your answer";
        }

        public override UserNotification Convert()
        {
            return new FavoriteLikedAnswerUserNotification();
        }

        public override string GenerateText()
        {
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            string link = "/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&MemberID=" + NotificationCreator.MemberID;
            string link2 = "/Applications/QuestionAnswer/Question.aspx?questid=" + AdditionalParams["questionId"];
            string text = string.Format(baseText, link, GetMemberName(), link2);
            return text;
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (AdditionalParams != null && datapoints != null && AdditionalParams.ContainsKey("questionId"))
            {
                datapoints.Remove(UserNotificationDataPointConstants.QNA_QUESTION_ID);
                datapoints.Add(UserNotificationDataPointConstants.QNA_QUESTION_ID, AdditionalParams["questionId"].ToString());
            }
            if (AdditionalParams != null && datapoints != null && AdditionalParams.ContainsKey("answerId"))
            {
                datapoints.Remove(UserNotificationDataPointConstants.QNA_ANSWER_ID);
                datapoints.Add(UserNotificationDataPointConstants.QNA_ANSWER_ID, AdditionalParams["answerId"].ToString());
            }
            if (AdditionalParams != null && datapoints != null && AdditionalParams.ContainsKey("answerMemberId"))
            {
                datapoints.Remove(UserNotificationDataPointConstants.QNA_ANSWER_MEMBER_ID);
                datapoints.Add(UserNotificationDataPointConstants.QNA_ANSWER_MEMBER_ID, AdditionalParams["answerMemberId"].ToString());
            }
        }
    }

    public class FavoriteLikedAnswerUserNotification : LikedAnswerUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_ANSWER_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                base.AddNotificationSpecificDataPoints(datapoints);
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
            }
        }
    }

    public class LikedFreeTextUserNotification : UserNotification
    {
        private string sectionAttribute = "";
        private string resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_ABOUTME_TXT";
        private LikedFreeTextEssayType essayType;

        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.LikedFreeText;
        }

        public override string GetResourceKey()
        {
            return resourceKey;
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.ProfileActivity) + " - Liked your essay";
        }

        public override UserNotification Convert()
        {
            return new FavoriteLikedFreeTextAboutMeUserNotification();
        }

        public LikedFreeTextUserNotification SectionAttribute(string strsectionAttribute)
        {
            sectionAttribute = strsectionAttribute.ToUpper();
            switch (sectionAttribute)
            {
                case "ABOUTME":
                    resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_ABOUTME_TXT";
                    essayType = LikedFreeTextEssayType.AboutMe;
                    break;
                case "PERFECTMATCHESSAY":
                    resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_PERFECTMATCHESSAY_TXT";
                    essayType = LikedFreeTextEssayType.PerfectMatch;
                    break;
                case "PERFECTFIRSTDATEESSAY":
                    resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_PERFECTFIRSTDATEESSAY_TXT";
                    essayType = LikedFreeTextEssayType.PerfectFirstDate;
                    break;
                case "IDEALRELATIONSHIPESSAY":
                    resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_IDEALRELATIONSHIPESSAY_TXT";
                    essayType = LikedFreeTextEssayType.IdealRelationship;
                    break;
                case "LEARNFROMTHEPASTESSAY":
                    resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_LEARNFROMTHEPASTESSAY_TXT";
                    essayType = LikedFreeTextEssayType.LearnFromThePast;
                    break;
                case "PERSONALITYTRAIT":
                    resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_PERSONALITYTRAIT_TXT";
                    essayType = LikedFreeTextEssayType.PersonalityTrait;
                    break;
                case "LEISUREACTIVITY":
                    resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_LEISUREACTIVITY_TXT";
                    essayType = LikedFreeTextEssayType.LeisureActivity;
                    break;
                case "ENTERTAINMENTLOCATION":
                    resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_ENTERTAINMENTLOCATION_TXT";
                    essayType = LikedFreeTextEssayType.EntertainmentLocation;
                    break;
                case "PHYSICALACTIVITY":
                    resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_PHYSICALACTIVITY_TXT";
                    essayType = LikedFreeTextEssayType.PhysicalActivity;
                    break;
                case "CUISINE":
                    resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_CUISINE_TXT";
                    essayType = LikedFreeTextEssayType.Cuisine;
                    break;
                case "MUSIC":
                    resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_MUSIC_TXT";
                    essayType = LikedFreeTextEssayType.Music;
                    break;
                case "READING":
                    resourceKey = "USER_NOTIFICATION_LIKED_FREETEXT_READING_TXT";
                    essayType = LikedFreeTextEssayType.Reading;
                    break;
            }

            return this;
        }

        public override string GenerateText()
        {
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            string link = "/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&MemberID=" + NotificationCreator.MemberID;
            string freeSectionName = _g.GetResource(GetResourceKey(), ctrl); ;
            string text = string.Format(baseText, link, GetMemberName());
            return text;
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED, essayType.ToString("d"));
            }
        }
    }

    public class FavoriteLikedFreeTextAboutMeUserNotification : LikedFreeTextUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_FREETEXT_ABOUTME_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED,
                               LikedFreeTextEssayType.AboutMe.ToString("d"));
            }

        }
    }

    public class FavoriteLikedFreeTextPerfectMatchEssayUserNotification : LikedFreeTextUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_FREETEXT_PERFECTMATCHESSAY_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED,
                               LikedFreeTextEssayType.PerfectMatch.ToString("d"));
            }
        }
    }

    public class FavoriteLikedFreeTextPerfectFirstDateEssayUserNotification : LikedFreeTextUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_FREETEXT_PERFECTFIRSTDATEESSAY_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED,
                               LikedFreeTextEssayType.PerfectFirstDate.ToString("d"));
            }
        }
    }

    public class FavoriteLikedFreeTextIdealRelationshipEssayUserNotification : LikedFreeTextUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_FREETEXT_IDEALRELATIONSHIPESSAY_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED,
                               LikedFreeTextEssayType.IdealRelationship.ToString("d"));
            }
        }
    }

    public class FavoriteLikedFreeTextLearnFromThePastEssayUserNotification : LikedFreeTextUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_FREETEXT_LEARNFROMTHEPASTESSAY_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED,
                               LikedFreeTextEssayType.LearnFromThePast.ToString("d"));
            }
        }
    }

    public class FavoriteLikedFreeTextPersonalityTraitUserNotification : LikedFreeTextUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_FREETEXT_PERSONALITYTRAIT_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED,
                               LikedFreeTextEssayType.PersonalityTrait.ToString("d"));
            }
        }
    }

    public class FavoriteLikedFreeTextLeisureActivityUserNotification : LikedFreeTextUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_FREETEXT_LEISUREACTIVITY_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED,
                               LikedFreeTextEssayType.LeisureActivity.ToString("d"));
            }
        }
    }

    public class FavoriteLikedFreeTextEntertainmentLocationUserNotification : LikedFreeTextUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_FREETEXT_ENTERTAINMENTLOCATION_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED,
                               LikedFreeTextEssayType.EntertainmentLocation.ToString("d"));
            }
        }
    }

    public class FavoriteLikedFreeTextPhysicalActivitytUserNotification : LikedFreeTextUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_FREETEXT_PHYSICALACTIVITY_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED,
                               LikedFreeTextEssayType.PhysicalActivity.ToString("d"));
            }
        }
    }

    public class FavoriteLikedFreeTextCuisineUserNotification : LikedFreeTextUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_FREETEXT_CUISINE_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED,
                               LikedFreeTextEssayType.Cuisine.ToString("d"));
            }
        }
    }

    public class FavoriteLikedFreeTextMusicUserNotification : LikedFreeTextUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_FREETEXT_MUSIC_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED,
                               LikedFreeTextEssayType.Music.ToString("d"));
            }
        }
    }

    public class FavoriteLikedFreeTextReadingUserNotification : LikedFreeTextUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_FREETEXT_READING_TXT";
        }

        public override void AddNotificationSpecificDataPoints(Dictionary<string, string> datapoints)
        {
            if (datapoints != null)
            {
                datapoints.Add(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE, "true");
                datapoints.Add(UserNotificationDataPointConstants.SPECIFIC_ESSAY_LIKED,
                               LikedFreeTextEssayType.Reading.ToString("d"));
            }
        }
    }

    public class NewMemberPromoUserNotification : UserNotification
    {
        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.NewMemberPromo;
        }
        
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_NEW_MEMBER_PROMO_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.Subscription) + " - New Member Promo";
        }

        public override string GenerateText()
        {
            return _g.GetResource(GetResourceKey(), ctrl);
        }

        public override bool IsActivated(Hashtable context)
        {
            bool b = base.IsActivated(context);
            if (b)
            {
                List<UserNotificationViewObject> notifications = (null != context) ? context["notifications"] as List<UserNotificationViewObject> : null;
                bool hasNotification = false;
                if (null != notifications)
                {
                    UserNotificationViewObject match = notifications.FindLast(delegate(UserNotificationViewObject un)
                    {
                        return un.Equals(this.CreateViewObject());
                    });

                    hasNotification = (null != match);
                }

                b = !hasNotification;

                if (b)
                {
                    DateTime regDate = System.Convert.ToDateTime(_g.Member.GetAttributeDate(_g.Brand, "BrandInsertDate"));
                    if (DateTime.Now.Subtract(regDate).TotalDays > 3)
                    {
                        b = false;
                    }
                }

            }
            return b;
        }

        public override string GetThumbnail()
        {
            string baseText = _g.GetResource("SPRITE_HTML", ctrl);
            string thumbHtml = string.Format(baseText, "s-icon-notification-nudges");
            return thumbHtml;
        }
    }


    public class LikedPhotoUserNotification : UserNotification
    {
        public override Matchnet.UserNotifications.ValueObjects.NotificationType GetNotificationType()
        {
            return Matchnet.UserNotifications.ValueObjects.NotificationType.LikedPhoto;
        }
        
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_LIKED_PHOTO_TXT";
        }

        public override string GetOmnitureTag()
        {
            return StringEnum.GetStringValue(OmnitureTags.ProfileActivity) + " - Liked your photo";
        }

        public override UserNotification Convert()
        {
            return new FavoriteLikedPhotoUserNotification();
        }

        public override string GenerateText()
        {
            string baseText = _g.GetResource(GetResourceKey(), ctrl);
            string link = "/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&MemberID=" + NotificationCreator.MemberID;
            string text = string.Format(baseText, link, GetMemberName());
            return text;
        }
    }

    public class FavoriteLikedPhotoUserNotification : LikedPhotoUserNotification
    {
        public override string GetResourceKey()
        {
            return "USER_NOTIFICATION_FAVORITE_LIKED_PHOTO_TXT";
        }
    }
}
