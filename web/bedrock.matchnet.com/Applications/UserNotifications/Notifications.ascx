﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Notifications.ascx.cs" Inherits="Matchnet.Web.Applications.UserNotifications.Notifications" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="page-container">
<div id="notifications" class="clearfix">
    <h2 class="component-header"><mn:Txt ResourceConstant="TXT_HEADER" runat="server"/></h2>
    <div class="feed clearfix">
        <ul>
            <asp:Repeater ID="repeaterNotifications" runat="server" OnItemDataBound="rptNotifications_OnItemDataBound">
                <ItemTemplate>
                    <mn2:FrameworkLiteral ID="dateHeader" runat="server" Visible="false"></mn2:FrameworkLiteral>
                    <li class="item onlineNewsPage" ot="<asp:Literal ID='omnitureTag' runat='server'/>">	                                                                       
                        <mn2:FrameworkLiteral ID="imgThumbnail" runat="server"></mn2:FrameworkLiteral>
                        <span class="message">
                            <mn2:FrameworkLiteral ID="literalUserNotification" runat="server"></mn2:FrameworkLiteral>
                        </span> 
                        <span class="date"><mn2:FrameworkLiteral ID="lastTimeUserNotification" runat="server" /></span>	                                
                    </li>                    
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>    
</div>
</div>
<script type="text/javascript">
    //omniture tracking
    jQuery(".onlineNewsPage a").live("click", function(evt) {
        spark.tracking.addEvent("event47", true);
        spark.tracking.addLinkTrackEvent("event47", true);
        spark.tracking.addLinkTrackVar("events", true);
        spark.tracking.addLinkTrackVar("prop37");
        var type = jQuery(evt.target).closest("[ot]").attr("ot");
        spark.tracking.addProp(37, "Newsfeed Page - " + type);
        spark.tracking.trackLink({ firstParam: true, linkName: "Click on Newsfeed Item" });
    });

    jQuery(".onlineNewsPage a[href*='ViewProfile.aspx']").live("click", function(evt) {
        var href = jQuery(evt.target).attr("href");
        if (href.indexOf("?") > -1) {
            href = ReplaceSingle(href, "?", "?ClickFrom=" + escape("Newsfeed - Newsfeed Page") + "&");
        } else {
            href = ReplaceSingle(href, "ViewProfile.aspx", "ViewProfile.aspx?ClickFrom=" + escape("Newsfeed - Newsfeed Page"));
        }
        jQuery(evt.target).attr("href", href);
    });
</script>
