﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsfeedDouble.ascx.cs" Inherits="Matchnet.Web.Applications.UserNotifications.Controls.NewsfeedDouble" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<%--Newsfeed--%>
<asp:PlaceHolder runat="server" ID="phnNewsfeedData" Visible="False">
    <ul class="newsfeed">
    <asp:Repeater ID="repeaterNotifications" runat="server" OnItemDataBound="rptNotifications_OnItemDataBound">
        <ItemTemplate>
            <asp:Placeholder ID="plcItemContainer" runat="server" Visible="true">
                <li class="item clearfix" ot="<asp:Literal ID='omnitureTag' runat='server'/>">	                                                                           
                    <div class="image">
                        <mn2:FrameworkLiteral ID="imgThumbnail" runat="server"></mn2:FrameworkLiteral>
                        <asp:Placeholder runat="server" ID="plcSystemIcon" Visible="false">
                            <span class="spr-b s-icon-b-notification-nudges"></span>
                        </asp:Placeholder>
                    </div>
                    <ul class="message">
                        <li><mn2:FrameworkLiteral ID="literalUserNotification" runat="server"></mn2:FrameworkLiteral></li>
                        <li class="info"><mn2:FrameworkLiteral ID="literalUserNotificationSubheading" runat="server"></mn2:FrameworkLiteral></li>
                        <li class="time"><mn2:FrameworkLiteral ID="lastTimeUserNotification" runat="server" /></li>
                    </ul>
                    <div class="action">                    
                        <asp:Hyperlink runat="server" ID="lnkButton" CssClass="btn link-quaternary cta"></asp:Hyperlink>
                    </div>                              
                </li>       
            </asp:Placeholder>             
        </ItemTemplate>
    </asp:Repeater>
    </ul>
    
    <ul class="newsfeed">
    <asp:Repeater ID="repeaterNotifications2" runat="server" OnItemDataBound="rptNotifications_OnItemDataBound">
        <ItemTemplate>
            <asp:Placeholder ID="plcItemContainer" runat="server" Visible="true">
                <li class="item clearfix" ot="<asp:Literal ID='omnitureTag' runat='server'/>">	                                                                           
                    <div class="image">
                        <mn2:FrameworkLiteral ID="imgThumbnail" runat="server"></mn2:FrameworkLiteral>
                        <asp:Placeholder runat="server" ID="plcSystemIcon" Visible="false">
                            <span class="spr-b s-icon-b-notification-nudges"></span>
                        </asp:Placeholder>
                    </div>
                    <ul class="message">
                        <li><mn2:FrameworkLiteral ID="literalUserNotification" runat="server"></mn2:FrameworkLiteral></li>
                        <li class="info"><mn2:FrameworkLiteral ID="literalUserNotificationSubheading" runat="server"></mn2:FrameworkLiteral></li>
                        <li class="time"><mn2:FrameworkLiteral ID="lastTimeUserNotification" runat="server" /></li>
                    </ul>
                    <div class="action">                    
                        <asp:Hyperlink runat="server" ID="lnkButton" CssClass="btn link-quaternary cta"></asp:Hyperlink>
                    </div>                              
                </li>       
            </asp:Placeholder>             
        </ItemTemplate>
    </asp:Repeater>
    </ul>
    
    <asp:PlaceHolder runat="server" ID="phViewMoreLink" Visible="False">
        <a href="/Applications/UserNotifications/Notifications.aspx" id="lnkMoreNewsfeed" class="more-link" runat="server"><mn:txt runat="server" id="txtMoreNewsfeed" resourceConstant="TXT_VIEW_MORE" /></a>
    </asp:PlaceHolder>
</asp:PlaceHolder>

<%--Newsfeed no data--%>
<asp:PlaceHolder runat="server" ID="phNewsfeedNoData" Visible="False">
	<blockquote><mn:txt ID="Txt3" ResourceConstant="TXT_NO_NEWSFEED_INTRO_COPY" runat="server" /></blockquote>
	<ul class="no-newsfeed">
		<li class="clearfix">
			<a id="lnkNewsfeedMemberPhotoEdit" runat="server" href="/Applications/MemberProfile/MemberPhotoEdit.aspx" title="Upload a Photo" class="image">
                <img src="/img/Community/JDate/ui-newsfeed-mood-img-01.jpg" alt="Upload a Photo" /></a>
			<h3><a id="lnkNewsfeedMemberPhotoEdit2" runat="server" href="/Applications/MemberProfile/MemberPhotoEdit.aspx" title="Upload a Photo">
                <mn:txt ID="Txt4" ResourceConstant="TXT_NO_NEWSFEED_HEADER_ONE" runat="server" /></a></h3>
			<p><mn:txt ID="Txt5" ResourceConstant="TXT_NO_NEWSFEED_BODY_ONE" runat="server" /></p>
		</li>
        <li class="clearfix">
			<a id="lnkNewsfeedMatchPreferences" runat="server" href="/Applications/Search/SearchResults.aspx" title="Set Your Match Preferences" class="image">
                <img src="/img/Community/JDate/ui-newsfeed-mood-img-03.jpg" alt="Set Your Match Preferences" /></a>
			<h3><a id="lnkNewsfeedMatchPreferences2" runat="server" href="/Applications/Search/SearchResults.aspx" title="Set Your Match Preferences">
                <mn:txt ID="Txt8" ResourceConstant="TXT_NO_NEWSFEED_HEADER_THREE" runat="server" /></a></h3>
			<p><mn:txt ID="Txt9" ResourceConstant="TXT_NO_NEWSFEED_BODY_THREE" runat="server" /></p>
		</li>
		<li class="clearfix">
			<a id="lnkNewsfeedEditProfile" runat="server" href="/Applications/MemberProfile/ViewProfile.aspx" title="Write a Few Words" class="image">
            <img src="/img/Community/JDate/ui-newsfeed-mood-img-02.jpg" alt="Write a Few Words" /></a>
			<h3><a id="lnkNewsfeedEditProfile2" runat="server" href="/Applications/MemberProfile/ViewProfile.aspx" title="Write a Few Words">
                <mn:txt ID="Txt6" ResourceConstant="TXT_NO_NEWSFEED_HEADER_TWO" runat="server" /></a></h3>
			<p><mn:txt ID="Txt7" ResourceConstant="TXT_NO_NEWSFEED_BODY_TWO" runat="server" /></p>
		</li>
		<li class="clearfix">
			<a id="lnkNewsfeedSearch" runat="server" href="/Applications/Search/SearchResults.aspx" title="Don't Be Shy" class="image">
                <img src="/img/Community/JDate/ui-newsfeed-mood-img-04.jpg" alt="Don't Be Shy" /></a>
			<h3><a id="lnkNewsfeedSearch2" runat="server" href="/Applications/Search/SearchResults.aspx" title="Don't Be Shy">
                <mn:txt ID="Txt10" ResourceConstant="TXT_NO_NEWSFEED_HEADER_FOUR" runat="server" /></a></h3>
			<p><mn:txt ID="Txt11" ResourceConstant="TXT_NO_NEWSFEED_BODY_FOUR" runat="server" /></p>
		</li>
	</ul>
</asp:PlaceHolder>