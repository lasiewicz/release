﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Newsfeed.ascx.cs" Inherits="Matchnet.Web.Applications.UserNotifications.Controls.Newsfeed" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<ul class="newsfeed">
<asp:Repeater ID="repeaterNotifications" runat="server" OnItemDataBound="rptNotifications_OnItemDataBound">
    <ItemTemplate>
        <asp:Placeholder ID="plcItemContainer" runat="server" Visible="true">
            <li class="item clearfix" ot="<asp:Literal ID='omnitureTag' runat='server'/>">	                                                                           
                <div class="image">
                    <mn2:FrameworkLiteral ID="imgThumbnail" runat="server"></mn2:FrameworkLiteral>
                    <asp:Placeholder runat="server" ID="plcSystemIcon" Visible="false">
                        <span class="spr s-icon-notification-nudges"></span>
                    </asp:Placeholder>
                </div>
                <ul class="message">
                    <li><mn2:FrameworkLiteral ID="literalUserNotification" runat="server"></mn2:FrameworkLiteral></li>
                    <li class="info"><mn2:FrameworkLiteral ID="literalUserNotificationSubheading" runat="server"></mn2:FrameworkLiteral></li>
                    <li class="time"><mn2:FrameworkLiteral ID="lastTimeUserNotification" runat="server" /></li>
                </ul>
                <div class="action">
                    <p class="notice clearfix">
                        <asp:Placeholder ID="plcSpriteNewMember" runat="server" Visible="false">
                            <span class="spr s-icon-notification-flag"></span><em><mn2:FrameworkLiteral ID="litNewMember" runat="server" ResourceConstant="TXT_NEW_MEMBER" /></em>
                        </asp:Placeholder>
                        <asp:Placeholder ID="plcSpriteUpdatedProfile" runat="server" Visible="false">
                            <span class="spr s-icon-notification-profile"></span><em><mn2:FrameworkLiteral ID="litUpdatedProfile" runat="server" ResourceConstant="TXT_UPDATED_PROFILE" /></em>
                        </asp:Placeholder>
                        <asp:Placeholder ID="plcSpriteAllAccess" runat="server" Visible="false">
                            <span class="spr s-icon-notification-access"></span><em><mn2:FrameworkLiteral ID="litAllAccess" runat="server" ResourceConstant="TXT_ALL_ACCESS" /></em>
                        </asp:Placeholder>     
                        <asp:Placeholder ID="plcSpriteNewPhoto" runat="server" Visible="false">
                            <span class="spr s-icon-notification-photo"></span><em><mn2:FrameworkLiteral ID="litNewPhoto" runat="server" ResourceConstant="TXT_NEW_PHOTO" /></em>
                        </asp:Placeholder>
                    </p>
                    
                    <asp:Hyperlink runat="server" ID="lnkButton" CssClass="btn link-primary cta"></asp:Hyperlink>
                    
                </div>                              
            </li>       
        </asp:Placeholder>             
    </ItemTemplate>
</asp:Repeater>
</ul>
