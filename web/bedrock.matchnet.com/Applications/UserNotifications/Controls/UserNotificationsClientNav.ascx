﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserNotificationsClientNav.ascx.cs" Inherits="Matchnet.Web.Applications.UserNotifications.Controls.UserNotificationsClientNav" %>
<% if (IsNotificationsEnabled && IsLoggedIn) { %>
    <script type="text/javascript" src="/Applications/UserNotifications/Controls/usernotifications.js"></script>
    <script type="text/javascript">
    /* <![CDATA[ */
        (function (){
            __addToNamespace__('spark.notifications', {
                MinPRTIdValue:<%=int.MinValue%>,
                BrandId:'<%=BrandId%>',
                MemberId:'<%=MemberId%>',
                BrowseMoreText:'<%=GetText("TXT_NO_NOTIFICATIONS",true,true)%>',
                getTimeInterval:function() {
                    return <%=TimeInterval%>;
                },
                __init__:function() {
                    spark.notifications.addConfig({
                        newNotifications:[],
                        oldNotifications:[],
                        StripClass:'vertstrip-notifications-nav',
                        ListId:'online-notifications-nav',                        
                        ListClass:'item onlineNewsNav',
                        ThumbClass:'thumb-news',
                        MessageClass:'message',
                        TimestampClass:'date',
                        ClickId:'<%=ClickableElementId%>',
                        PrtId:787,
                        ViewMoreText:'<a id="viewMore<%=ClickableElementId%>" href="javascript:void(0);"><%=GetText("TXT_VIEW_MORE_NOTIFICATIONS",true, true)%></a>',
                        Carousel:function() {
		                    jQuery(".vertstrip-notifications-nav").jCarouselLite({
			                    btnNext: ".vertstrip-next-notifications-nav",
			                    btnPrev: ".vertstrip-prev-notifications-nav",
			                    circular: false,
			                    mouseWheel: true,
			                    scroll: 1,
			                    visible: 5,
                                colNum:1,
			                    vertical: true,
                                pageSize:5
		                    });
                        }
                    });
                }
            });
        })();
        
        jQuery(document).ready(function() {
            if("null"!="<%=OnlineIndicatorElementId%>" && ""!="<%=OnlineIndicatorElementId%>" && jQuery("#<%=OnlineIndicatorElementId%>").length>0){
                spark.util.addItem(spark.notifications.onlineIndicatorIds,"<%=OnlineIndicatorElementId%>");
                //only create update interval once
                if(spark.notifications.onlineIndicatorIds.length==1) {
                    spark.notifications.loadOnlineIndicator();
                }
            }
            jQuery("#<%=ClickableElementId%>").click(function(evt) {
                if (jQuery('#vertstripNotificationsNav').is(':hidden')){
                    var config=spark.notifications.getConfig('ClickId', '<%=ClickableElementId%>');
                    var pageSize=(config)?spark.notifications.totalNotifications(config):0;                    
                    spark.notifications.loadNotificationsWithConfig(evt,((pageSize>50)?pageSize:50), true, true);
                    if("null"!="<%=OnlineIndicatorElementId%>" && ""!="<%=OnlineIndicatorElementId%>" && jQuery("#<%=OnlineIndicatorElementId%>").length>0){
                        jQuery("#<%=OnlineIndicatorElementId%>").children("a").find("[class*='s-icon']").addClass("news-active");
                    }
                    jQuery('#vertstripNotificationsNav').show();
                    //omniture tracking

                    var originalPageName = spark.tracking.getPageName();
                    if (originalPageName){ // If there already is a populated pageName attribute on the omniture object
                        spark.tracking.addPageName(originalPageName + " - Newsfeed - Title Bar", true);
                    }else{
                        PopulateS(true); //Create the  values in omniture "s" object
                        originalPageName = spark.tracking.getPageName();
                        spark.tracking.addPageName(originalPageName + " - Newsfeed - Title Bar", true);
                    }

                    spark.tracking.addEvent("event46", true);
                    spark.tracking.addLinkTrackEvent("event46", true);
                    spark.tracking.addLinkTrackVar("events", true);
                    spark.tracking.trackLink({ firstParam: true, linkName: "Click to View Notification" });
                    spark.tracking.addPageName(originalPageName, true);
                } else {
                    jQuery('#vertstripNotificationsNav').hide();
                    jQuery('#vertstripNotificationsNav').hide().parent().find('ul #newsNav').removeClass('selected').find("[class*='s-icon']").removeClass("news-active");
                }
            });
            jQuery("body").click(function(evt) {
                if (!jQuery('#vertstripNotificationsNav').is(':hidden')) {
                    var targ=jQuery(evt.target);
                    if(targ.parents("#vertstripNotificationsNav").length == 0 && targ.parents("#<%=ClickableElementId%>").length == 0){
                        jQuery('#vertstripNotificationsNav').hide().parent().find('ul #newsNav').removeClass('selected').find("[class*='s-icon']").removeClass("news-active");
                    }
                }            
            });
            jQuery("#viewMore<%=ClickableElementId%>").live("click",function(evt){
                var offset=10;
                var config=spark.notifications.getConfig('ClickId','<%=ClickableElementId%>');
                spark.notifications.loadNotificationsWithConfig({currentTarget:{id:'<%=ClickableElementId%>'}},spark.notifications.totalNotifications(config)+offset, true, false);
                jQuery('#vertstripNotificationsNav').show();
                evt.stopPropagation();
            });            
                                                
            //omniture tracking
            jQuery(".onlineNewsNav a").live("click",function(evt) {
                    

                    //As an omniture "link" which does not redirect the page, we set a temporary pageName attribute
                    var originalPageName = spark.tracking.getPageName();
                    if (originalPageName){
                        spark.tracking.addPageName(originalPageName + " - Favorites", true);
                    }else{
                        PopulateS(true); //reset existing values in omniture "s" object
                        originalPageName = spark.tracking.getPageName();
                        spark.tracking.addPageName(originalPageName + " - Favorites", true);
                    }

                    spark.tracking.addEvent("event47", true);
                    spark.tracking.addLinkTrackEvent("event47", true);
                    spark.tracking.addLinkTrackVar("events", true);
                    spark.tracking.addLinkTrackVar("prop37");
                    var type = jQuery(evt.target).closest("[ot]").attr("ot");
                    spark.tracking.addProp(37, "Nav bar - " + type);
                    spark.tracking.trackLink({ firstParam: true, linkName: "Click on Newsfeed Item" });
                    spark.tracking.addPageName(originalPageName, true); // return the pageName value to its default
            });

            jQuery("#newsNav a").click(function(){
                var $this = jQuery(this),
                    direction = "<%=Direction %>",
                    pos = $this.parent().position(),
                    pWidth = $this.parent().outerWidth(),
                    offset = jQuery("#vertstripNotificationsNav").outerWidth();

                $this.parent().addClass("selected");

                if(direction=="rtl")
                {
                    jQuery("#vertstripNotificationsNav").css({"left":(pos.left) + "px"});
                }
                else
                {
                    jQuery("#vertstripNotificationsNav").css({"left":(pos.left - offset + pWidth) + "px"});
                }
            }); 
            
            jQuery(".onlineNewsNav a[href*='ViewProfile.aspx']").live("click", function(evt) {
                var href=jQuery(evt.target).attr("href");
                if(href.indexOf("?") > -1) {
                    href = ReplaceSingle(href, "?", "?ClickFrom=" + escape("Newsfeed – Nav Bar") + "&"); 
                } else {
                    href = ReplaceSingle(href, "ViewProfile.aspx", "ViewProfile.aspx?ClickFrom=" + escape("Newsfeed – Nav Bar")); 
                }
                jQuery(evt.target).attr("href",href);
            });
            
        });
    /* ]]> */
    </script>

<div id="vertstripNotificationsNav" class="notifications-online-wrapper notifications-online outer-wrapper" style="display:none;"> 
	<div class="vertstrip-notifications-nav feed-wrapper">
        <div class="vertstrip-notifications-nav-feed feed">
	        <ul id="online-notifications-nav"></ul>
	    </div>
        <div class="vertstrip-notifications-nav-controls control">
	        <button class="vertstrip-prev-notifications-nav disabled">&#9650;</button>
	        <button class="vertstrip-next-notifications-nav">&#9660;</button>
        </div>
    </div>
	<div class="edit-notifications clear-both edit">
		<a id="seeNotifications" href="/Applications/UserNotifications/Notifications.aspx" title="<%=GetText("TXT_SEE_ALL_NOTIFICATIONS", true, false)%>"><%=GetText("TXT_SEE_ALL_NOTIFICATIONS", true, false)%></a> | 
		<a href="/Applications/Search/SearchResults.aspx?SearchOrderBy=1" title="<%=GetText("TXT_YOUR_MATCHES", true, false)%>"><%=GetText("TXT_YOUR_MATCHES", true, false)%></a> |
		<a href="/Applications/HotList/View.aspx?CategoryID=-1" title="<%=GetText("TXT_WHOS_CHECKING_YOU", true, false)%>"><%=GetText("TXT_WHOS_CHECKING_YOU", true, false)%></a>
    </div>        
</div>
<% } %>
