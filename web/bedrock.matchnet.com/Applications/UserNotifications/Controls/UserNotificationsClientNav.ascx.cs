﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;


namespace Matchnet.Web.Applications.UserNotifications.Controls
{
    public partial class UserNotificationsClientNav : UserNotificationsControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsNotificationsEnabled && g.Member != null && g.Member.MemberID > 0)
            {
                try
                {
                    bool notificationsInit = Conversion.CBool(g.Session.GetString("USER_NOTIFICATIONS_INITIALIZED", bool.FalseString));
                    if (!notificationsInit)
                    {
                        g.Session.Add("USER_NOTIFICATIONS_INITIALIZED", bool.TrueString, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                        System.Collections.Generic.List<UserNotification> notifyList = UserNotificationFactory.Instance.GetSystemNotifications(g.Member, g);
                        UserNotificationParams unParams = UserNotificationParams.GetParamsObject(g.Member.MemberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                        Hashtable unContext = new Hashtable();
                        unContext.Add("notifications", GetUserNotficationViewObjects(g.Member.MemberID));
                        foreach (UserNotification notification in notifyList)
                        {
                            if (notification.IsActivated(unContext))
                            {
                                UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }
            }
        }
    }
}
