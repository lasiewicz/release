﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Ui.FormElements;
using NotificationType = Matchnet.UserNotifications.ValueObjects.NotificationType;
using Matchnet.Web.Analytics;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.UserNotifications.Controls
{
    public partial class NewsfeedDouble : FrameworkControl
    {
        private DateTime date = DateTime.Now.Date;
        private UserNotificationManager userNotificationManager = new UserNotificationManager();
        private LastLoginDate lDate;
        private string _OmnitureSourceName = "Newsfeed";

        public int NumberOfLoadedNotifications { get; set; }
        public int NumberOfLoadedNonSystemNotifications { get; set; }
        public int NewNotificationsCount { get; set; }
        public string OmniturePageName { get; set; }

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rptNotifications_OnItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var un = e.Item.DataItem as UserNotificationViewObject;
                var notificationText = userNotificationManager.GetNotificationText(this, un, g.Member, g.Brand);
                var plcItemContainer = e.Item.FindControl("plcItemContainer") as PlaceHolder;

                if (notificationText != string.Empty)
                {
                    plcItemContainer.Visible = true;

                    var thumbnail = e.Item.FindControl("imgThumbnail") as FrameworkLiteral;
                    var literalActivityAction = e.Item.FindControl("literalUserNotification") as FrameworkLiteral;
                    var lastTimeUserNotification = e.Item.FindControl("lastTimeUserNotification") as FrameworkLiteral;
                    var omnitureTag = e.Item.FindControl("omnitureTag") as Literal;
                    var literalUserNotificationSubheading = e.Item.FindControl("literalUserNotificationSubheading") as FrameworkLiteral;
                    var lnkButton = e.Item.FindControl("lnkButton") as HyperLink;
                    var plcSystemIcon = e.Item.FindControl("plcSystemIcon") as PlaceHolder;
                    Member.ServiceAdapters.Member creator = un.CreatorId > 0 ? MemberSA.Instance.GetMember(un.CreatorId, MemberLoadFlags.None) : null;
                    
                    lnkButton.NavigateUrl = userNotificationManager.GetNotificationLink(un, g.Member, g.Brand, WebConstants.Action.ViewPageButton);
                    lnkButton.Text = userNotificationManager.GetNotificationButtonText(this, un, g.Member, g.Brand);

                    thumbnail.Visible = !un.SystemNotification;
                    plcSystemIcon.Visible = un.SystemNotification;

                    if (thumbnail.Visible && !string.IsNullOrEmpty(un.ThumbnailFile))
                    {
                        thumbnail.Text = userNotificationManager.GetThumbnailLink(this, un, g.Member, creator, g.Brand);
                    }

                    omnitureTag.Text = un.OmnitureTag;
                    literalActivityAction.Text = notificationText;
                    //literalUserNotificationSubheading.Text = userNotificationManager.GetNotificationSubheading(this, un, g.Member, g.Brand);

                    lDate.LastLoginDateValue = un.Created;
                    lastTimeUserNotification.Text = lDate.BuildLastLoginDate();
                }
                else
                {
                    plcItemContainer.Visible = false;
                }
            }
        }

        #endregion

        #region Public Methods
        public void LoadNewsfeed(int startNum, int pageSize)
        {
            //initialize user notification manager with omniture for tracking
            userNotificationManager.OmniturePageName = string.IsNullOrEmpty(OmniturePageName) ? "Home - Default" : OmniturePageName;
            userNotificationManager.OmnitureSourceName = _OmnitureSourceName;

            lDate = LoadControl("/Framework/Ui/BasicElements/LastLoginDate.ascx") as LastLoginDate;

            if (g.Member != null)
            {
                var notifications = userNotificationManager.GetUserNotficationViewObjects(g.Member.MemberID, g.Brand,
                                                                                          g.List, startNum, 1000, false, g, false, false);
                if (notifications != null && notifications.Count > 0)
                {
                    phnNewsfeedData.Visible = true;
                    int maxItemsDisplayed = (notifications.Count > pageSize) ? pageSize : notifications.Count;
                    NumberOfLoadedNotifications = maxItemsDisplayed;
                    NewNotificationsCount = (from n in notifications where !n.Viewed select n).Count();

                    List<UserNotificationViewObject> list1 = new List<UserNotificationViewObject>();
                    List<UserNotificationViewObject> list2 = new List<UserNotificationViewObject>();
                    
                    for (int i = 0; i < maxItemsDisplayed; i++ )
                    {
                        if ((i % 2) == 0)
                        {
                            list1.Add(notifications[i]);
                        }
                        else
                        {
                            list2.Add(notifications[i]);
                        }

                        if (!notifications[i].SystemNotification)
                        {
                            NumberOfLoadedNonSystemNotifications++;
                        }
                    }
                    repeaterNotifications.DataSource = list1;
                    repeaterNotifications.DataBind();

                    repeaterNotifications2.DataSource = list2;
                    repeaterNotifications2.DataBind();

                    if (notifications.Count > pageSize)
                    {
                        phViewMoreLink.Visible = true;
                        lnkMoreNewsfeed.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Notifications, WebConstants.Action.ViewPageLink, lnkMoreNewsfeed.HRef, _OmnitureSourceName);

                    }
                }
                else
                {
                    phNewsfeedNoData.Visible = true;
                    NumberOfLoadedNonSystemNotifications = 0;
                    NumberOfLoadedNotifications = 0;
                    NewNotificationsCount = 0;

                    lnkNewsfeedEditProfile.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewPageImageLink, lnkNewsfeedEditProfile.HRef, _OmnitureSourceName);
                    lnkNewsfeedEditProfile2.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewPageTitle, lnkNewsfeedEditProfile2.HRef, _OmnitureSourceName);

                    lnkNewsfeedMemberPhotoEdit.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MemberPhotoEdit, WebConstants.Action.ViewPageImageLink, lnkNewsfeedMemberPhotoEdit.HRef, _OmnitureSourceName);
                    lnkNewsfeedMemberPhotoEdit2.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MemberPhotoEdit, WebConstants.Action.ViewPageTitle, lnkNewsfeedMemberPhotoEdit2.HRef, _OmnitureSourceName);

                    lnkNewsfeedMatchPreferences.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewPageImageLink, lnkNewsfeedMatchPreferences.HRef, _OmnitureSourceName);
                    lnkNewsfeedMatchPreferences2.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewPageTitle, lnkNewsfeedMatchPreferences2.HRef, _OmnitureSourceName);

                    lnkNewsfeedSearch.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewPageImageLink, lnkNewsfeedSearch.HRef, _OmnitureSourceName);
                    lnkNewsfeedSearch2.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewPageTitle, lnkNewsfeedSearch2.HRef, _OmnitureSourceName);
                }
            }

        }
        #endregion

    }
}