﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.PremiumServices.ServiceAdapter;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
#endregion

namespace Matchnet.Web.Applications.PremiumServices.Controls
{
    [Flags]
    public enum ForcedPageDisplayCondition
    {
        unconditional=1,
        spotlightNoPhotos=2, 
        timedPromotion=4
    }
    
    public partial class ForcedPage : FrameworkControl
    {
        public const int ANY_APP = -1;
        private ForcedPageDisplayCondition forcedPageCondition;

        private string spotlightResourceConstant = string.Empty;
        private string timedResourceConstant = string.Empty;

        private int appID = 0;
        private int displayCount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            bool visible = false;
            string resx = string.Empty;

            try
            {
                if (g.AppPage.App.ID != appID && appID != ANY_APP)
                    return;

                if ((forcedPageCondition & ForcedPageDisplayCondition.unconditional) == ForcedPageDisplayCondition.unconditional)
                {
                    visible = true;
                }
                else if ((forcedPageCondition & ForcedPageDisplayCondition.timedPromotion) == ForcedPageDisplayCondition.timedPromotion)
                {
                    visible = SetTimedPromotionForcedPage();
                    if (visible)
                    {
                        resx = g.GetResource(timedResourceConstant, null,true,this);
                    }
                }

                if (!visible && (forcedPageCondition & ForcedPageDisplayCondition.spotlightNoPhotos) == ForcedPageDisplayCondition.spotlightNoPhotos)
                {
                    visible = SetSpotlightForcedPage(spotlightResourceConstant);
                    if (visible)
                    {
                        resx = g.GetResource(spotlightResourceConstant, null, true, this);
                    }
                }

                if(visible && !String.IsNullOrEmpty(resx))
                {
                    pnlForcedPage.Visible = true;
                    litForcedPage.Text = resx;
                }
               
            }
            catch (Exception ex)
            { 
                g.ProcessException(ex);
            }
        }

        private bool SetTimedPromotionForcedPage()
        {
            bool result=false;
            try
            {
                if (_g.Member == null)
                {
                    //non-member or not before the login screen
                    return result;
                }
                
                int promoVersion = Conversion.CInt(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FORCEDPAGE_TIMEDPROMOTION_VERSION", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));
                DateTime startDate = Conversion.CDateTime(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FORCEDPAGE_TIMEDPROMOTION_BEGINDATE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));
                DateTime endDate = Conversion.CDateTime(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FORCEDPAGE_TIMEDPROMOTION_ENDDATE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));

                if (promoVersion > 0 && DateTime.Now >= startDate && DateTime.Now <= endDate)
                {
                    int memberPromoVersion = _g.Member.GetAttributeInt(_g.Brand, "ForcedPageTimedPromotionVersion");
                    if (memberPromoVersion == Constants.NULL_INT)
                    {
                        memberPromoVersion = 0;
                    }
                    if (memberPromoVersion < promoVersion)
                    {
                        result = true;
                        _g.Member.SetAttributeInt(_g.Brand, "ForcedPageTimedPromotionVersion", promoVersion);
                        MemberSA.Instance.SaveMember(_g.Member);
                    }
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
            return result;
        }

        private bool SetSpotlightForcedPage(string resourceConstant)
        { 
            bool result=false;
            try
            {
                if (g.Session["LoggedIn"] != "true")
                    return result;
                if (g.PremiumServices != null && g.PremiumServices.Count > 0)
                {
                    Matchnet.PremiumServices.ValueObjects.InstanceMember member = Matchnet.PremiumServices.ServiceAdapter.ServiceMemberSA.Instance.GetMemberService(g.Brand.BrandID, g.Member, (int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember);

                    if (member == null)
                        return result;

                    var photosCount = MemberPhotoDisplayManager.Instance.GetAllPhotosCount(g.Member,
                                                                                           g.Brand.Site.Community.
                                                                                               CommunityID);

                    if (photosCount == 0)
                    {
                        string sessionVar = forcedPageCondition.ToString().ToUpper() + resourceConstant.ToUpper();
                        int count = Conversion.CInt(g.Session.Get(sessionVar), 0);
                        if (count < displayCount)
                        {
                            count += 1;
                            g.Session.Add(sessionVar, count.ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                            result = true;
                        }
                    }
                }

                g.Session.Remove("LoggedIn");
                return result;
            }
            catch (Exception ex)
            {
                g.Session.Remove("LoggedIn");
                g.ProcessException(ex);
                return result;
            }
        }


        public ForcedPageDisplayCondition ForcedPageCondition
        {
            get { return forcedPageCondition; }
            set { forcedPageCondition = value; }
        }

        public String SpotlightResourceConstant
        {
            get { return spotlightResourceConstant; }
            set { spotlightResourceConstant = value; }
        }

        public String TimedResourceConstant
        {
            get { return timedResourceConstant; }
            set { timedResourceConstant = value; }
        }

        public Int32 AppID
        {
            get { return appID; }
            set { appID = value; }
        }

        public Int32 DisplayCount
        {
            get { return displayCount; }
            set { displayCount = value; }
        }
    }
}