﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Configuration.ValueObjects;
using Spark.SAL;
#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
using Matchnet.Member.ServiceAdapters;
#endregion

using Matchnet.PremiumServices.ValueObjects;
using Matchnet.Web.Framework.Ui.ProfileElements;

namespace Matchnet.Web.Applications.PremiumServices.Controls
{
    public partial class SpotlightMemberControl20 : PremiumServiceSettingBase, IPremiumServiceSettings
    {
        SpotlightMember servicemember;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (MemberPremiumServices.HasService((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember))
            {
                MemberMiniProfile.Member = Member;
                MemberMiniProfile.IsHighlighted = false;
                MemberMiniProfile.IsSpotlight = true;

                if (BetaHelper.IsSearchRedesign30Enabled(g))
                {
                    phSpotlightProfileNew.Visible = true;
                    SpotlightProfile pr = (SpotlightProfile)LoadControl("/Framework/Ui/ProfileElements/SpotlightProfile.ascx");
                    pr.Member = Member;
                    pr.EntryPoint = Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.Unknown;
                    pr.IsPromotionalMember = false;
                    pr.OnlineImageName = "icon-status-online.gif";
                    pr.OfflineImageName = "icon-status-offline.gif";
                    phSpotlightProfileNew.Controls.Add(pr);
                }
                else
                {
                    phSpotlightProfileOld.Visible = true;
                }

            }
            prSearchRegion.RegionSaved += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.RegionSavedEventHandler(prSearchRegion_RegionSaved);
            prSearchRegion.Closed += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.ClosedEventHandler(prSearchRegion_Closed);
            prSearchRegion.RegionReset += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.RegionResetEventHandler(prSearchRegion_RegionReset);

            AgeMin.Value = servicemember.AgeMin.ToString();
            AgeMax.Value = servicemember.AgeMax.ToString();
            rdbEnable.Checked = servicemember.EnableFlag;
            rdbDisable.Checked = !servicemember.EnableFlag;
            bindGender();
            bindDistance();

            bindLocation();
            DoValidation();
            //MemberMiniProfile.Member = g.Member;
            //MemberMiniProfile.IsHighlighted = false;
            //MemberMiniProfile.IsSpotlight = true;
        }

        public void Save()
        {
            try
            {
                servicemember.AgeMin = Conversion.CInt(AgeMin.Value);
                servicemember.AgeMax = Conversion.CInt(AgeMax.Value);
                servicemember.RegionID = prSearchRegion.RegionID;
                servicemember.Distance = Conversion.CInt(ddlDistance.Text);
                servicemember.GenderMask = getGenderMask();
                servicemember.EnableFlag = rdbEnable.Checked;

                Matchnet.PremiumServices.ServiceAdapter.ServiceMemberSA.Instance.SaveMemberService(Brand, Member.MemberID, servicemember.ServiceMember);


            }
            catch (Exception ex)
            { throw; }
        }

        public void SetMember(Content.ValueObjects.BrandConfig.Brand brand, Member.ServiceAdapters.Member member, InstanceMember premiummember)
        {
            servicemember = new SpotlightMember(premiummember);
            base.Member = member;
            base.Brand = brand;

        }

        private void DoValidation()
        {
            // Add min and max age validation
            string minAgeError = g.GetResource("MIN_AGE_NOTICE", this);
            string maxAgeError = g.GetResource("MAX_AGE_NOTICE", this);
            string greaterThanError = g.GetResource("GREATER_THAN_NOTICE", this);

            AgeMinRange.ErrorMessage = minAgeError;
            AgeMinRange.MinimumValue = ConstantsTemp.MIN_AGE_RANGE.ToString();
            AgeMinRange.MaximumValue = ConstantsTemp.MAX_AGE_RANGE.ToString();
            AgeMaxRange.ErrorMessage = maxAgeError;
            AgeMaxRange.MinimumValue = ConstantsTemp.MIN_AGE_RANGE.ToString();
            AgeMaxRange.MaximumValue = ConstantsTemp.MAX_AGE_RANGE.ToString();
            AgeMinReq.ErrorMessage = minAgeError;
            AgeMaxReq.ErrorMessage = maxAgeError;
            //valCompare.ErrorMessage = greaterThanError;

            AgeMinRange.Type = System.Web.UI.WebControls.ValidationDataType.Integer;
            AgeMaxRange.Type = System.Web.UI.WebControls.ValidationDataType.Integer;

        }

        private void bindGender()
        {
            ListItem itemMale = new ListItem(g.GetResource("MALE", this), ((Int32)Gender.Male).ToString());
            ListItem itemFemale = new ListItem(g.GetResource("FEMALE", this), ((Int32)Gender.Female).ToString());

            ddlGender.Items.Add(itemMale);
            ddlGender.Items.Add(itemFemale);

            itemMale = new ListItem(g.GetResource("MALE", this), ((Int32)Gender.Male).ToString());
            itemFemale = new ListItem(g.GetResource("FEMALE", this), ((Int32)Gender.Female).ToString());

            ddlSeekingGender.Items.Add(itemMale);
            ddlSeekingGender.Items.Add(itemFemale);

            string gender = ((int)Spark.SAL.GenderUtils.GetSeekingGender(servicemember.GenderMask)).ToString();
            string sekkinggender = ((int)Spark.SAL.GenderUtils.GetGender(servicemember.GenderMask)).ToString();
            FrameworkGlobals.SelectItem(ddlGender, gender);
            // Use Spark.SAL.GenderUtils to get the gender that the member is seeking
            FrameworkGlobals.SelectItem(ddlSeekingGender, sekkinggender);


        }

        private void bindDistance()
        {
            DataTable distanceOptions = Option.GetOptions(Matchnet.Web.Applications.Search.SearchUtil.GetDistanceAttribute(g), g);

            ddlDistance.DataSource = distanceOptions;
            ddlDistance.DataTextField = "Content";
            ddlDistance.DataValueField = "Value";
            ddlDistance.DataBind();

            FrameworkGlobals.SelectItem(ddlDistance, servicemember.Distance.ToString(), SettingsManager.GetSettingInt(SettingConstants.DEFAULT_DISTANCE_LIST_ORDER, g.Brand) - 1);
            //ddlDistance.SelectedValue = _memberSearch.Distance.ToString();
        }

        private void bindLocation()
        {

            prSearchRegion.SearchType = SearchType.Region;
            prSearchRegion.ShowAreaCodesTab = false;
            prSearchRegion.ShowZipCodesTab = false;
            prSearchRegion.AllowAnyRegion = true;


            prSearchRegion.RegionID = servicemember.RegionID;
            Content.ValueObjects.Region.RegionLanguage region = Content.ServiceAdapters.RegionSA.Instance.RetrievePopulatedHierarchy(servicemember.RegionID, g.Brand.Site.LanguageID);

            Int32 countryRegionID = 0;
            lnkLocation.Text = getRegionString(); ;
            //TODO: abstract area code stuff into the SAL.MemberSearch object


            lnkLocation.UpdateAfterCallBack = true;
            lnkLocation.Attributes.Add("onclick", "popUpDiv(\"searchPopupDiv\",\"191px\",\"0px\");return false");
            lnkLocation.Attributes.Add("href", "#");
            if (region.CityRegionID > 0)
            {
                plcDistance.Visible = true;
                plcDistance.UpdateAfterCallBack = true;
            }
            else
            {
                plcDistance.Visible = false;
            }




            prSearchRegion.RegionName = lnkLocation.Text;
            lnkLocation.Text += g.GetResource("EDIT", this);
        }

        private string getRegionString()
        {
            string txt = "";
            int id = 0;
            if (servicemember.RegionID < 0)
            {
                if (servicemember.Depth2RegionID < 0)
                {
                    id = servicemember.Depth1RegionID;
                }
                else
                {
                    id = servicemember.Depth2RegionID;
                }

            }
            else
            {

                id = servicemember.RegionID;
            }
            txt = FrameworkGlobals.GetRegionString(id, Brand.Site.LanguageID, false, false);
            return txt;
        }

        private int getGenderMask()
        {
            Gender gender = (Gender)Enum.Parse(typeof(Gender), ddlGender.SelectedValue);
            Gender seekingGender = (Gender)Enum.Parse(typeof(Gender), ddlSeekingGender.SelectedValue);
            int genderMask = Spark.SAL.GenderUtils.GetGenderMask(seekingGender, gender);
            return (genderMask);
        }


        private void prSearchRegion_RegionSaved(EventArgs e)
        {

            servicemember.RegionID = prSearchRegion.RegionID;


            bindLocation();
        }

        private void prSearchRegion_Closed(EventArgs e)
        {
            prSearchRegion.RegionID = servicemember.RegionID;

        }

        private void prSearchRegion_RegionReset(EventArgs e)
        {
            int regionid = Member.GetAttributeInt(g.Brand, "RegionID");

            Content.ValueObjects.Region.RegionLanguage region = Content.ServiceAdapters.RegionSA.Instance.RetrievePopulatedHierarchy(regionid, g.Brand.Site.LanguageID);
            if (region != null)
            {
                prSearchRegion.RegionID = region.CityRegionID;

            }
        }
    }
}