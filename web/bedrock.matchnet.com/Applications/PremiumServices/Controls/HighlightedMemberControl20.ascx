﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HighlightedMemberControl20.ascx.cs" Inherits="Matchnet.Web.Applications.PremiumServices.Controls.HighlightedMemberControl20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<h2><mn:Txt runat="server" id="Txt2" ExpandImageTokens="True" ResourceConstant="TXT_HIGHLIGHTED_PROFILES" /></h2>

<p class="editorial">
    <mn:Image FileName="ui-settings-highlight.png" id="Image6" CssClass="float-outside margin-light" runat="server" titleResourceConstant="TITLE_HIGHLIGHTED_PROFILE" ResourceConstant="ALT_HIGHLIGHTED_PROFILE" />
    <mn:txt runat="server" id="txtRememberMeHelpLayerBody" ResourceConstant="TXT_HIGHLIGHTED_PROFILE_HELP_LAYER_BODY" />
</p>

<div id="highlight-form-container">
    <asp:RadioButtonList ID="rbgHighlightProfile" RepeatLayout="Flow" Runat="server"></asp:RadioButtonList>
</div>