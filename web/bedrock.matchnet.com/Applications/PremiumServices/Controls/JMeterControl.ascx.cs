﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.PremiumServices.ValueObjects;

namespace Matchnet.Web.Applications.PremiumServices.Controls
{
    public partial class JMeterControl : PremiumServiceSettingBase, IPremiumServiceSettings
    {
        protected System.Web.UI.WebControls.RadioButtonList rbgJMeter;

        JMeterMember servicemember = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bindEnableDisableControl(rbgJMeter);
                if (servicemember.EnableFlag)
                    rbgJMeter.SelectedIndex = 0;
                else
                    rbgJMeter.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public void Save()
        {
            servicemember.EnableFlag = rbgJMeter.Items[0].Selected;
            Matchnet.PremiumServices.ServiceAdapter.ServiceMemberSA.Instance.SaveMemberService(g.Brand, Member.MemberID, servicemember.ServiceMember);

        }

        public void SetMember(Brand brand, Member.ServiceAdapters.Member member, InstanceMember premiummember)
        {
            servicemember = new JMeterMember(premiummember);
            base.Member = member;
            base.Brand = brand;
        }

        protected void bindEnableDisableControl(RadioButtonList pButtonList)
        {
            if (pButtonList != null)
            {
                ListItem liEnable = new ListItem();
                liEnable.Value = "1";
                liEnable.Text = g.GetResource("TXT_ENABLE", this);
                pButtonList.Items.Add(liEnable);

                ListItem liDisable = new ListItem();
                liDisable.Value = "0";
                liDisable.Text = g.GetResource("TXT_DISABLE", this);
                pButtonList.Items.Add(liDisable);
            }
        }
    }
}