﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.PremiumServiceSearch.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.PremiumServices
{

    public class MemberImpression:IComparable
    {
        int memberID;
        DateTime impressionTime;
        
        public MemberImpression(int memberid, DateTime impressiontime)
        {
            memberID = memberid;
            impressionTime = impressiontime;

        }

        public MemberImpression(int memberid)
        {
            memberID = memberid;
            impressionTime = DateTime.Now;

        }

        public int MemberID
        {
            get { return memberID; }
            set {  memberID=value; }

        }

        public DateTime ImpressionTime
        {
            get { return impressionTime; }
            set { impressionTime = value; }

        }

        public int CompareTo(object obj)
        {
            if (obj is MemberImpression)
            {
                MemberImpression m2 = (MemberImpression)obj;
                return impressionTime.CompareTo(m2.ImpressionTime) * -1;
            }
            else
                throw new ArgumentException("Object is not a MemberImpression.");
        }

       

    }



    public class VelocityHandler
    {
        string pairDelimiter = ";";
        string pairSplitDelimiter = "|";
        const int IMPRESSION_CAP = 50;
        List<MemberImpression> _impressions;
        List<MemberImpression> _promotionImpressions;
        bool promotionsEnabled;
        int _velocityCap = 0;
        int _velocityQuota = 0;
        int _velocityPeriod = 0;
        int _promotionRatio = 0;
        string _impressionsCookieName;
        string _promotionImpressionsCookieName;
        string _lastSpotlightMember;
        bool _ignoreQuota = false;
        bool _debugTrace = false;
        DateTime _requestTime = DateTime.MinValue;
        MemberImpression _lastImpression = new MemberImpression(0, DateTime.MinValue);
        ContextGlobal g;

        public VelocityHandler(ContextGlobal context, string impressionsCookie, string promotionsCookie, int velocitycap, int velocityperiod)
        {
            MemberImpression promoLastImpression = new MemberImpression(0, DateTime.MinValue);
            MemberImpression memberLastImpression = new MemberImpression(0, DateTime.MinValue);
            _requestTime =DateTime.Now;
            g = context;
            _debugTrace = Conversion.CBool(System.Configuration.ConfigurationSettings.AppSettings.Get("DebugTrace"));
            string impressionstring = "";
            _velocityCap = velocitycap;
            _velocityPeriod = velocityperiod;
            _impressionsCookieName = impressionsCookie;
            _promotionImpressionsCookieName = promotionsCookie;
           
            DebugTrace("VelocityHandler.New", "Initializing",true);
            if (g.Session.Get(impressionsCookie) != null)
                impressionstring = g.Session.Get(impressionsCookie).ToString();

            if (!String.IsNullOrEmpty(impressionstring))
            {
                DebugTrace(":VelocityHandler", "Member Cookie:" + impressionstring,false);
                _impressions = stringToList(impressionstring);
                _impressions = getQuotaList(_impressions, _velocityPeriod);
                if (_impressions != null && _impressions.Count > 0)
                {
                   memberLastImpression = (MemberImpression)_impressions[0];

                }
            }
            else {
                DebugTrace(":VelocityHandler", "Member Cookie:empty",false);
                _impressions = new List<MemberImpression>(); 
            
                }


            DebugTrace(":VelocityHandler", "reading promo impressions",false);

            impressionstring = "";
            if (g.Session.Get(promotionsCookie) != null)
                impressionstring = g.Session.Get(promotionsCookie).ToString();

            if (!String.IsNullOrEmpty(impressionstring))
            {
                DebugTrace(":VelocityHandler", "Promo Cookie:" + impressionstring, false);
                _promotionImpressions = stringToList(impressionstring);
        
                _promotionImpressions = getQuotaList(_promotionImpressions, _velocityPeriod);
                if (_promotionImpressions != null && _promotionImpressions.Count > 0)
                {
                    promoLastImpression = (MemberImpression)_promotionImpressions[0];
                }
            }
            else {
                DebugTrace(":VelocityHandler", "Promo Cookie:empty" , false);
                _promotionImpressions = new List<MemberImpression>(); 
            }

           // _lastImpression = (promoLastImpression.ImpressionTime > memberLastImpression.ImpressionTime ? promoLastImpression : memberLastImpression);
            DebugTrace(":VelocityHandler", "finish init", true);

        }

        //public int getMemberFromLastRequest(out bool promo)
        //{
        //    int mid = 0;
        //    promo = false;
        //    try
        //    {
        //        if (_promoLastImpression.ImpressionTime > _memberLastImpression.ImpressionTime)
        //        {
        //            if (_promoLastImpression.ImpressionTime.AddSeconds(2) > DateTime.Now)
        //            {
        //                mid = _promoLastImpression.MemberID;
        //                promo = true;
        //            }
        //        }
        //        else
        //        {
        //            if (_memberLastImpression.ImpressionTime.AddSeconds(2) > DateTime.Now)
        //                mid = _memberLastImpression.MemberID;


        //        }
        //        return mid;

        //    }
        //    catch (Exception ex)
        //    {
        //        return 0;
        //    }

        //}
        private List<MemberImpression> stringToList(String list)
        {
            List<MemberImpression> impressions = new List<MemberImpression>();
            try
            {
                
                List<string> arraylist = list.Split(pairDelimiter.ToCharArray()).ToList<string>();
                DateTime prevTime = DateTime.MinValue;
                for (int i = 0; i < arraylist.Count; i++)
                {
                    List<string> pair = arraylist[i].Split(pairSplitDelimiter.ToCharArray(), 2).ToList<string>();
                    DateTime imprTime = Conversion.CDateTime(pair[1]);
                    if (imprTime.AddHours(_velocityPeriod) > DateTime.Now )
                    {
                        MemberImpression impr = new MemberImpression(Conversion.CInt(pair[0]),imprTime);
                       
                        //if (prevTime == DateTime.MinValue || prevTime.AddSeconds(-1) > imprTime)
                        //{
                            impressions.Add(impr);
                            prevTime = impr.ImpressionTime;
                        //}
                    }
                }
                impressions.Sort();
                return impressions;
            }
            catch (Exception ex)
            { return impressions; }

        }

        private string listToString(List<MemberImpression> list)
        {
            string listString = "";
            try
            {
                if (list == null || list.Count <= 0)
                { return listString; }

                int count = list.Count > IMPRESSION_CAP ? IMPRESSION_CAP : list.Count;
                for (int i = 0; i < count; i++)
                {
                    string memberid = list[i].MemberID.ToString();
                    string time = list[i].ImpressionTime.ToString();
                    string pair = memberid + pairSplitDelimiter[0] + time;
                    listString += pair + pairDelimiter[0];
                }

                return listString;
            }
            catch (Exception ex)
            { return listString; }

        }


        private List<MemberImpression> getQuotaList(List<MemberImpression> list, double quota)
        {
            try
            {

                IEnumerable<MemberImpression> query = from l in list
                                                      where l.ImpressionTime.AddHours(quota) >= DateTime.Now
                                                      select l;

                return query.ToList<MemberImpression>();

            }
            catch (Exception ex)
            { return null; }

        }


        private List<int> getNotImpressedMembers(List<MemberImpression> list, List<int> results)
        {
            try
            {
                List<int> outList = null;
                DebugTrace("getNotImpressedMembers", "begin" , false);

                //IEnumerable<int> query = from l in list join r in results
                //                                       on l.MemberID equals r into g
                //                                       from r in g.DefaultIfEmpty() 
                //                                       where l.MemberID != r
                //                                       select r;

                if (list == null || list.Count==0)
                    return results;
                IEnumerable<int> query = from r in results
                                         where (
                                         from l in list
                                         select l.MemberID).Contains<int>(r) == false
                                         select r;
                return query.ToList<int>();
               
            }
            catch (Exception ex)
            { return null; }

        }


        private List<int> getImpressedMembers(List<MemberImpression> list, List<int> results, int quota, bool removeLastImpressedMember,bool ignoreQuota)
        {
            List<int> members = new List<int>();
            int lastMemberID=0;
            try
            {
                DebugTrace("getImpressedMembers", "ignore quota=" + ignoreQuota, false);
                if(removeLastImpressedMember)
                {
                    lastMemberID=getLastListMemberID(list);
                }

                var query = from l in list
                            join r in results
                            on l.MemberID equals r
                            group l by l.MemberID 
                            into g
                            select new { MemberID = g.Key, impressionsCount = g.Count() };
                foreach (var x in query)
                {

                    if (x.MemberID != lastMemberID)
                    {
                        if (ignoreQuota)
                            members.Add(x.MemberID);
                        else
                        {
                            if (x.impressionsCount < quota)
                                members.Add(x.MemberID);
                        }
                    }
                    
                }
                if(members != null)
                    DebugTrace("getImpressedMembers", "impressed members count:" + members.Count, false);
                return members;

            }
            catch (Exception ex)
            { return null; }

        }



        private int getImpressionsCount(List<MemberImpression> list, int memberid)
        {
            try
            {
                if (list == null)
                    return 0;
                IEnumerable<MemberImpression> query = from l in list
                                                      where l.MemberID == memberid
                                                      select l;

                return query.ToList<MemberImpression>().Count;

            }
            catch (Exception ex)
            { return 0; }

        }

        public bool PromotionsEnabled
        {
            get { return promotionsEnabled; }
            set { promotionsEnabled = value; }

        }

        public int VelocityCap
        {
            get { return _velocityCap; }
            set { _velocityCap = value; }

        }

        public int VelocityQuota
        {
            get { return _velocityQuota; }
            set { _velocityQuota = value; }

        }

        public int VelocityPeriod
        {
            get { return _velocityPeriod; }
            set { _velocityPeriod = value; }

        }

        public int PromotionRatio
        {
            get { return _promotionRatio; }
            set { _promotionRatio = value; }

        }

        public bool IgnoreQuota
        {
            set { _ignoreQuota=value; }
        }
        public List<int> FilterOutDupplicates(List<int>searchResults, List<int> spotResults)
        {
            List<int> outList = null;
            List<int> list = null;
            try
            {
                if (searchResults == null) return spotResults;
                DebugTrace("FilterOutDupplicates", "searchResults results are not empty:" + searchResults.Count, true);
                IEnumerable<int> query = from r in spotResults
                                         where (
                                         from l in searchResults
                                         select l).Contains<int>(r) == false
                                         select r;

                if (query != null)
                {
                    outList = query.ToList<int>();
                    DebugTrace("FilterOutDupplicates", "after removing dupplicates:" + query.ToList<int>().Count, true);

                }
                else
                {
                    DebugTrace("FilterOutDupplicates", "after removing dupplicates:0", true);
                    
                }

                return outList;
            }
            catch (Exception ex)
            {
                return null;
            }


        }


        //public int GetNextMemberID(List<int> results, List<MemberImpression> impressions)
        //{   
        //    try
        //    {
        //        int id = 0;
        //        DebugTrace("VelocityHandler.GetNextMemberID", "begin", true);
             
        //        id = getNextMemberID(promotionResults, impressions);
                 
        //        DebugTrace("VelocityHandler.GetNextMemberID", "Got member id:" + id.ToString(), false);
        //        return id;


        //    }
        //    catch (Exception ex)
        //    { return 0; }



        //}

        public int GetNextMemberID(List<int> results, List<int> promotionResults, out  bool promoMember)
        {
            promoMember = false;
            try
            {

                DebugTrace("GetNextMemberID", "begin",true);
               
                int id = 0;
                

                bool usePromo = false;
             
                if (promotionResults != null && promotionResults.Count > 0)
                {
                    usePromo = UsePromotionMember();

                    DebugTrace("GetNextMemberID", "calculating ratio, usePromo=" + usePromo,false);
                }

                string cookie = "";
               
                if (usePromo)
                {
                    DebugTrace("GetNextMemberID", "Getting promo id" ,false);
                    id = scanList(promotionResults, _promotionImpressions,false);
                    if(id > 0)
                    { 
                        cookie = _promotionImpressionsCookieName;
                        saveImpressions(_promotionImpressions,  id, cookie);
                        promoMember = usePromo;
                        g.Session.Add("LastSpotlightMember", id.ToString(), Session.ValueObjects.SessionPropertyLifetime.TemporaryDurable);
                        DebugTrace("GetNextMemberID", "Got promo id" + id.ToString(), false);
                        return id;
                       
                    }
                }

                //didn't get promoid
                usePromo = false;
                cookie = _impressionsCookieName;

                DebugTrace("GetNextMemberID", "PromoID not valid Getting spotlight member id", false);
                id = scanList(results, _impressions,_ignoreQuota);
                if (id > 0)
                {
                    DebugTrace("GetNextMemberID", "returning spotlight id: " + id.ToString(), false);
                    saveImpressions(_impressions, id, cookie);
                }
                   
             
                promoMember = usePromo;
                DebugTrace("VelocityHandler.GetNextMemberID", "Got member id:" + id.ToString(), false);
                g.Session.Add("LastSpotlightMember", id.ToString(), Session.ValueObjects.SessionPropertyLifetime.TemporaryDurable);
                return id;


            }
            catch (Exception ex)
            {
                DebugTrace("GetNextMemberID", "Exception:" + ex.ToString(), false);
                return 0;
            
            }



        }

        public Matchnet.Member.ServiceAdapters.Member GetLastDisplayedSpotlightMember()
        {
            Matchnet.Member.ServiceAdapters.Member lastSpotMember = null;
            try
            {
                int memberid;
                memberid = Conversion.CInt(g.Session.Get("LastSpotlightMember"));
                if (memberid > 0)
                {
                    SetOmniture();
                    LogImpression(g, memberid);
                    lastSpotMember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                }
                return lastSpotMember;
            }
            catch (Exception ex)
            {
                DebugTrace("GetLastDisplayedSpotlightMember", "Exception:" + ex.ToString(), false);
                return lastSpotMember;
            }
        }

        public bool UsePromotionMember()
        {
            bool usePromo = false;
            int impressionsCount = 0;
            int impressionsPromoCount = 0;
            // int sum = resCount + promoCount;
            
                if (_impressions != null)
                    impressionsCount = _impressions.Count;

                if (_promotionImpressions != null)
                    impressionsPromoCount = _promotionImpressions.Count;

                int sumImpressions = impressionsCount + impressionsPromoCount;
                if (sumImpressions > 0)
                {
                    int ratioMember = (impressionsCount * 100 / sumImpressions);
                    if (ratioMember > _promotionRatio)
                        usePromo = true;
                }

                DebugTrace("VelocityHandler.UsePromotionMember", "calculating ratio, usePromo=" + usePromo, false);
                return usePromo;
           
        }

        public void SetOmniture()
        {
            try
            {
                	// Omniture Analytics - Event Tracking
                //if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
                //{
                //    g.AnalyticsOmniture.Events = "event13";
                //    g.AnalyticsOmniture.Products = ";;;;event13=1";
                //}
            }
            catch (Exception ex)
            { DebugTrace("SetOmniture", ex.ToString(),false); }

        }

        public static void LogImpression(ContextGlobal g, int viewedmemberid)
        {   bool log=false;
            try
            {

            log=Conversion.CBool( Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SPOTLIGHT_LOG_FLAG"),false);
            if(!log || g.Member == null)
               return;

            int viewingMemberID=g.Member.MemberID;
            int siteid=g.Brand.Site.SiteID;
            int pageid=g.AppPage.ID;
            PremiumServiceSearch.ValueObjects.QueueMessage.ImpressionType impressiontype=PremiumServiceSearch.ValueObjects.QueueMessage.ImpressionType.spotlight;


            PremiumServiceSearch.ServiceAdapters.SpotlightMemberSA.Instance.LogImpression(siteid, pageid, impressiontype, viewedmemberid, viewingMemberID, DateTime.Now);

            }
            catch (Exception ex)
            {  }
        }


        private int scanList(List<int> results, List<MemberImpression> impressions, bool ignoreQuota)
        {
            int id=0;
            bool valid = false;
            int count = 0;
            while (!valid && results.Count > 0)
            {
                id = getNextMemberID(results, impressions,ignoreQuota);
                count += 1;
                DebugTrace("scanList", "getNextMemberID, of " + results.Count + ", Attempt: " + count, false);
                if (id <= 0)
                    break;
                valid = validateSpotlightMember(id);
                if (!valid)
                {
                    DebugTrace("scanList", "getNextMemberID, of " + results.Count + ", Attempt: " + count + " id not valid: " + id, false);
                    results.Remove(id);
                    id = 0;
                }
                
            }
            return id;
        }
        private int GetRandomMemberID(Matchnet.PremiumServiceSearch.ValueObjects.ResultList results)
        {

            try
            {
                int resCount = 0;

                if (results == null)
                    return 0;

                resCount = results.Results.Count;

                int seed = DateTime.Now.Minute * 100 + DateTime.Now.Second * 10;
                Random r = new Random(seed);

                int idx = r.Next(resCount - 1);
                int id = Conversion.CInt(results.Results[idx]);

                return id;
            }
            catch (Exception ex)
            { return 0; }

        }


        private int GetRandomMemberID(List<int> results)
        {

            try
            {
                int resCount = 0;

                if (results == null)
                    return 0;

                resCount = results.Count;

                int seed = DateTime.Now.Minute * 100 + DateTime.Now.Second * 10;
                Random r = new Random(seed);

                int idx = r.Next(resCount - 1);
                int id = Conversion.CInt(results[idx]);
               
                return id;
            }
            catch (Exception ex)
            { return 0; }

        }


        private int getNextMemberID(List<int> resultList, List<MemberImpression> impressionList, bool ignoreQuota)
        {
            int id = 0;
            try
            {
                List<int> members = null;
                DebugTrace("VelocityHandler.getNextMemberID", "getNotImpressedMembers", false);
                members = getNotImpressedMembers(impressionList, resultList);

                if (members != null && members.Count > 0)
                {
                    id = GetRandomMemberID(members);
                    }
                else
                {  
                    members = getImpressedMembers(impressionList, resultList, _velocityQuota,true,ignoreQuota);
                    if (members == null || members.Count <= 0)
                    {
                        DebugTrace("VelocityHandler.getNextMemberID", "getImpressedMembers removelastviewed=true", false);
                        members = getImpressedMembers(impressionList, resultList, _velocityQuota, false, ignoreQuota);
                    }

                    if (members != null && members.Count > 0)
                    {

                        id = GetRandomMemberID(members);
                        DebugTrace("VelocityHandler.getNextMemberID", "getImpressedMembers removelastviewed=true id:" + id.ToString(), false);
                    }
                   
                   
                     DebugTrace("VelocityHandler.getNextMemberID", "getImpressedMembers removelast=false id:" + id.ToString(), false);
                   
                }

                DebugTrace("VelocityHandler.getNextMemberID", "return id:" + id.ToString(), false);
                return id;
            }
            catch (Exception ex)
            { return id; }
        }

        private void saveImpressions(List<MemberImpression> impressionList, int id, string cookie)
        {
            
            if (impressionList != null)
            {
              
                    impressionList.Add(new MemberImpression(id));
                    impressionList.Sort();
              
               
                    string impr = listToString(impressionList);
                    DebugTrace("saveImpressions", "Save id:" + id + " Impressionlist:" + impr, false);
                    g.Session.Add(cookie, impr, 60, false);
                
               
            }
        }

        private int getLastListMemberID(List<MemberImpression> list)
        {
            int id = 0;
            if (list != null && list.Count > 0)
            {
               id = list[0].MemberID;

            }
            DebugTrace("getLastListMemberID", "id:" + id , false);
            return id;

        }

        private bool validateSpotlightMember(int memberid)
        {
            bool valid = false;
            IMemberDTO member=null;
            try
            {
                List.ServiceAdapters.List list = List.ServiceAdapters.ListSA.Instance.GetList(g.Member.MemberID);                
                member = MemberDTOManager.Instance.GetIMemberDTO(memberid, g, MemberType.Search, Member.ServiceAdapters.MemberLoadFlags.None);
                if (member == null)
                {
                    DebugTrace("validateSpotlightMember", "Member Obj is null, id:" + memberid, false);
                    return valid;
                }
                if (!member.IsCommunityMember(g.Brand.Site.Community.CommunityID))
                {
                    DebugTrace("validateSpotlightMember", "Member is not community member, id:" + memberid, false);
                    return valid;
                }
                if (list.IsHotListed(List.ValueObjects.HotListCategory.IgnoreList, g.Brand.Site.Community.CommunityID, memberid))
                {
                    DebugTrace("validateSpotlightMember", "Member is on blocked list, id:" + memberid, false);
                    return valid;
                }
                int status = member.GetAttributeInt(g.Brand, "GlobalStatusMask");
                int selfSuspendedMember = member.GetAttributeInt(g.Brand, "SelfSuspendedFlag", 0);

                if (selfSuspendedMember > 0 || ((status & (int)GlobalStatusMask.AdminSuspended) == (int)GlobalStatusMask.AdminSuspended))
                {
                    DebugTrace("validateSpotlightMember", "Member is self or admin suspended, id:" + memberid, false);
                    return valid;

                }

                return true;
            }
            catch (Exception ex)
            { return valid; }
        }

        private void DebugTrace(string source, string message, bool logAddInfo)
        {
            try
            {

               
                if (_debugTrace)
                {
                    if (logAddInfo)
                    {
                        if (g.Member != null)
                        {
                            int memberid = g.Member.MemberID;
                            int siteid = g.Brand.Site.SiteID;
                            source = String.Format("Siteid: {0}, MemberID: {1}, Source: {2}, Session: {3}", siteid, memberid, source,g.Session.Key.ToString());
                        }
                        string impr = listToString(_impressions);
                        string promo = listToString(_promotionImpressions);

                        string objtext =  String.Format("VelocityCap:{0}, VelocityPeriod: {1}, VelocityQuota: {2}, PromoRatio: {3}, ListCap: {4}, Spotlight Cookie:{5}\r\nPromo Cookie: {6}", _velocityCap, _velocityPeriod, _velocityQuota, _velocityCap, _promotionRatio, impr, promo);

                        System.Diagnostics.Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + String.Format("VelocityHandler Source: {0} \r\n Trace: {1}\r\n Data: {2}", source, message, objtext));
                    }
                    else
                    {

                        System.Diagnostics.Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + String.Format("VelocityHandler Source: {0}, Trace: {1}", source, message));
                    }
                }

            }
            catch (Exception ex)
            { }

        }
        public  static void DebugTrace(string source, string message, ContextGlobal g)
        {
            bool debugTrace = false;
            try
            {
               debugTrace = Conversion.CBool(System.Configuration.ConfigurationSettings.AppSettings.Get("DebugTrace"));
              // string req=HttpContext.Current.Request.ToString();
                if (!debugTrace)
                {
                    return;}
                    
                        if (g != null && g.Member != null)
                        {
                            int memberid = g.Member.MemberID;
                            int siteid = g.Brand.Site.SiteID;
                            source = String.Format(" Source: {2}, Siteid: {0}, MemberID: {1},", siteid, memberid, source);
                        }
                      
                     

                        System.Diagnostics.Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + String.Format("Source: {0}  Trace: {1}", source, message));
                  
              
            }
            catch (Exception ex)
            { }

        }

    }

    
}
