﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.Member.ServiceAdapters;
namespace Matchnet.Web.Applications.PremiumServices
{
    public interface IPremiumServiceSettings
    {
        void Save();
        void SetMember(Content.ValueObjects.BrandConfig.Brand brand, Member.ServiceAdapters.Member member, InstanceMember premiummember);
       
        
    }
}
