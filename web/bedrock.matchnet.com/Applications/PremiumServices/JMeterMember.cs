using System;
using Matchnet.PremiumServices.ValueObjects;

namespace Matchnet.Web.Applications.PremiumServices
{
    public class JMeterMember
    {
        private InstanceMember member;

        public JMeterMember(InstanceMember m)
        {
            if (m != null)
                member = m;
            else
                member = new Matchnet.PremiumServices.ValueObjects.InstanceMember();
        }

        public InstanceMember ServiceMember
        {
            get { return member; }
            set { member = value; }
        }

        public bool EnableFlag
        {
            get { return member.Attributes.Get<bool>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.EnableFlag); }
            set { member.Attributes.Add<bool>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.EnableFlag, value); }
        }


        public DateTime ExpirationDate
        {
            get { return member.Attributes.Get<DateTime>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.ExpirationDate); }
            set { member.Attributes.Add<DateTime>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.ExpirationDate, value); }
        }
    }
}