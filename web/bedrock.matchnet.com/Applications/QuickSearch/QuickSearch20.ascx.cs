﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml;
using System.Drawing;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Search;

using Spark.SAL;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Applications.QuickSearch
{
    public enum QuickSearchMode:int
    {filter=0,
     results=1}

    public partial class QuickSearch20 :  FrameworkControl,IQuickSearch
    {

        #region IQuickSearchImplementation
        public Matchnet.Web.Framework.Title TtlPreferences { get { return ttlPreferences; } }
        public DropDownList DdlGender { get { return ddlGender; } }
        public DropDownList DdlSeekingGender { get { return ddlSeekingGender; } }
        public TextBox TbAgeMin { get { return tbAgeMin; } }
        public TextBox TbAgeMax { get { return tbAgeMax; } }
        public Anthem.PlaceHolder PlcDistance { get { return plcDistance; } }
        public Anthem.PlaceHolder PlcAreaCodes { get { return plcAreaCodes; } }
        public Anthem.PlaceHolder PlcCollege { get { return plcCollege; } }
        public DropDownList DdlDistance { get { return ddlDistance; } }
        public Anthem.HyperLink LnkLocation { get { return lnkLocation; } }
        public CheckBox CbHasPhoto { get { return cbHasPhoto; } }
        public CheckBox CbJewishOnly { get { return cbJewishOnly; } }
        public CheckBox CbxRamahDate { get { return cbxRamahDate; } }
        public Button BtnSave { get { return btnSave; } }
      //  public Anthem.PlaceHolder PlcPickRegion { get { return plcPickRegion; } }
        public PickRegionNew PrSearchRegion { get { return prSearchRegion; } }
        public Matchnet.Web.Framework.MultiValidator MvAgeMinValidator { get { return AgeMinValidator; } }
        public Matchnet.Web.Framework.MultiValidator MvAgeMaxValidator { get { return AgeMaxValidator; } }
        public Matchnet.Web.Framework.MultiValidator MvLocationValidator { get { return LocationValidator; } }
        public MemberSearch MemberSearch { get { return _memberSearch; } set { _memberSearch = value; } }

        public FrameworkControl ResourceControl { get { return this; } }


        public PlaceHolder PlcTitle { get { return plcTitle; } }
        public PlaceHolder PlcDivRightNewEnd { get { return divRightNewEnd; } }
        public PlaceHolder PlcDivRightNewStart { get { return divRightNewStart; } }
        public PlaceHolder QuickSearchAd { get { return null; } }
        #endregion
        protected MemberSearch _memberSearch;
        QuickSearchHandler _handler;
        

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                SetQuickSearch(true);
                //Bind data to controls
                _handler.BindGender();
                _handler.BindAge();
                _handler.BindDistance();
                _handler.BindLocation(IsPostBack);
                _handler.BindHasPhoto();

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
                {
                    cbJewishOnly.Visible = true;
                    cbxRamahDate.Visible = true;
                    _handler.BindJewishOnly();
                    _handler.BindRamahOnly();
                }
               
                //bindXMLPreferences();

                btnSave.Text = g.GetResource("BTN_SHOW_PREFERENCES", this);

                // If this is a photo required site, let's hide the "Has Photo" checkbox because it doesn't make sense.
                if (Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID)))
                {
                    cbHasPhoto.Visible = false;
                }

                g.SetPageTitle(g.GetResource("TXT_PAGE_TITLE", this));

                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("QUICK_SEARCH", this),
                                                            g.AppPage.App.DefaultPagePath);

                }
                if (g.BreadCrumbTrailFooter != null)
                {
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_PHOTO_GALLERY", this),
                                                           g.AppPage.App.DefaultPagePath);
                }			
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (prSearchRegion.RegionName.Length > 0)
            {
                LocationValidator.IsRequired = false;
            }
            else
            {
                LocationValidator.IsRequired = true;
            }
            //LocationValidator.Enabled = false;

            Page.Validate();

            if (Page.IsValid)
            {
                _memberSearch.Gender = (Gender)Enum.Parse(typeof(Gender), ddlGender.SelectedValue);
                _memberSearch.SeekingGender = (Gender)Enum.Parse(typeof(Gender), ddlSeekingGender.SelectedValue);

                Int32 minAge = Conversion.CInt(tbAgeMin.Text);
                Int32 maxAge = Conversion.CInt(tbAgeMax.Text);

                if (minAge > maxAge)
                {
                    tbAgeMin.Text = tbAgeMax.Text;
                    tbAgeMax.Text = minAge.ToString();

                    minAge = maxAge;
                    maxAge = Conversion.CInt(tbAgeMax.Text);
                }

                _memberSearch.Age.MinValue = minAge;
                _memberSearch.Age.MaxValue = maxAge;

                _memberSearch.Distance = Conversion.CInt(ddlDistance.SelectedValue);

                if (Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID)))
                {
                    // If this is a photo required site, cbHasPhoto won't be visible to the user so just set the search option to true here
                    _memberSearch.HasPhoto = true;
                }
                else
                {
                    _memberSearch.HasPhoto = cbHasPhoto.Checked;
                }

                if (cbJewishOnly.Visible)
                {
                    Preference preference = Preference.GetInstance("jdatereligion");
                    if (preference != null)
                    {
                        MemberSearchPreference memberSearchPreference = _memberSearch[preference];
                        int val=Constants.NULL_INT;
                       

                        _memberSearch[preference] = memberSearchPreference;

                        if (cbJewishOnly.Checked)
                        {
                            val = FrameworkGlobals.GetJewishReligionOnly(g);
                        }
                        else
                        {
                            //this is so when binding we can tell the difference between the user having not 
                            //set a value, which means we want to check the box by defaul, and the user explicitly
                            //unchecking the box. We use the specified const value because in the FrameworkGlobals.GetJewishReligionOnly() 
                            //it is explicitly excluded from the calculation.
                            val = Matchnet.Web.Framework.Search.Constants.QS_JewishOnly_Unchecked;
                        }
                    (memberSearchPreference as MemberSearchPreferenceMask).Value = val;
                    }
                }

                if(cbxRamahDate.Visible)  // lets take care of Ramah pref
                {

                    if (cbxRamahDate.Checked)
                        (_memberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt).Value = 1;
                    else
                        (_memberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt).Value = Constants.NULL_INT;  //remove the preference
                }


                int memberid = Constants.NULL_INT;
                if (g.Member != null) memberid = g.Member.MemberID;
                
                _handler.PersistQSValues(_memberSearch);
                _memberSearch.Save();

                g.Transfer("/Applications/QuickSearch/QuickSearchResults.aspx");
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            btnSave.Click += new EventHandler(btnSave_Click);
            //rptAdditionalSections.ItemDataBound += new RepeaterItemEventHandler(rptAdditionalSections_ItemDataBound);
            prSearchRegion.RegionSaved += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.RegionSavedEventHandler(prSearchRegion_RegionSaved);
            prSearchRegion.Closed += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.ClosedEventHandler(prSearchRegion_Closed);
            prSearchRegion.RegionReset += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.RegionResetEventHandler(prSearchRegion_RegionReset);
        }
        #endregion

        private void prSearchRegion_RegionSaved(EventArgs e)
        {
            //TODO: abstract area code stuff into the SAL.MemberSearch object
            IEnumerator areaCodeEnumerator = prSearchRegion.GetAreaCodes();

            bool done = false;

            for (Int32 i = 1; i <= 6; i++)
            {
                MemberSearchPreferenceInt areaCodePreference = _memberSearch[Preference.GetInstance("AreaCode" + i)] as MemberSearchPreferenceInt;

                if (!done)
                {
                    done = !areaCodeEnumerator.MoveNext();
                }

                areaCodePreference.Value = done ? Constants.NULL_INT : Conversion.CInt(areaCodeEnumerator.Current);
            }

            _memberSearch.RegionID = prSearchRegion.RegionID;
            _memberSearch.SchoolID = prSearchRegion.SchoolID;
            _memberSearch.SearchType = prSearchRegion.SearchType;
            _memberSearch.Save();

            int memberid = Constants.NULL_INT;
            if (g.Member != null) memberid = g.Member.MemberID;

            _handler.PersistQSValues(_memberSearch);
            _handler.BindLocation(IsPostBack);
        }

        private void prSearchRegion_Closed(EventArgs e)
        {
            prSearchRegion.RegionID = _memberSearch.RegionID;
            prSearchRegion.SearchType = _memberSearch.SearchType;
        }

        private void prSearchRegion_RegionReset(EventArgs e)
        {
            prSearchRegion.RegionID = g.Member.GetAttributeInt(g.Brand, "RegionID");
        }

        public void SetQuickSearch(bool save)
        {
            _handler = new QuickSearchHandler(this, g);

            int memberid = Constants.NULL_INT;
            if (g.Member != null) memberid = g.Member.MemberID;

            _memberSearch = _handler.getMemberSearch(memberid);
            if (save)
            {
                _handler.PersistQSValues(_memberSearch);
                _memberSearch.Save();
            }
        }

    }
}
