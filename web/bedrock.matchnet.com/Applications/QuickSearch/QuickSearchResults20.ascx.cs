﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Search.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;

using System.Text;
using System.Xml;
using Matchnet.Web.Framework.Ui.FormElements;
using Spark.SAL;
using System.Drawing;
using Matchnet.Web.Applications.Search;


namespace Matchnet.Web.Applications.QuickSearch
{
    public partial class QuickSearchResults20 : FrameworkControl,IQuickSearchResults
    {
        ArrayList _sortSpans = new ArrayList();
        ArrayList _sortTitles = new ArrayList();
        ArrayList _sortLinks = new ArrayList();
        protected Int32 _searchOrderBy;
        protected string _changeVoteMessage;
        #region IQuickSearchResults implemjentation

        public int SearchOrderBy { get { return _searchOrderBy; } set { _searchOrderBy = value; } }
        public string ChangeVoteMessage { get { return _changeVoteMessage; } set { _changeVoteMessage = value; } }

        public ArrayList SortSpans { get { return _sortSpans; } set { _sortSpans = value; } }
        public ArrayList SortTitles { get { return _sortTitles; } set { _sortTitles = value; } }
        public ArrayList SortLinks { get { return _sortLinks; } set { _sortLinks = value; } }

        public Matchnet.Web.Framework.Txt TxtWantMore { get { return null; } }
        public Repeater RptMemberRepeater { get { return null; } }
        public HtmlTableRow TrNoResults { get { return null; } }
        public HtmlTableRow TrBarLower { get { return null; } }
        public PlaceHolder PhResults { get { return Results; } }
        public Panel PnlNoResults { get { return null; } }
        public DropDownList DdlSortMem { get { return null; } }
        public HyperLink LnkViewType { get { return null; } }
        public Matchnet.Web.Framework.Image Icon_ViewType { get { return null; } }
        public Matchnet.Web.Framework.Title TxtMatches { get { return txtMatches; } }
        public Matchnet.Web.Framework.Txt TxtView { get { return txtView; } }
        public Matchnet.Web.Framework.Txt TxtViewAs { get { return null; } }
        public HtmlInputHidden HidGalleryView { get { return null; } }
        public HtmlInputHidden HidResultViewChanged { get { return null; } }
        public Repeater RptSearchOption { get { return rptSearchOption; } }
        public Label LblListNavigationTop { get { return lblListNavigationTop; } }
        public Label LblListNavigationBottom { get { return LblListNavigationBottom; } }
        public System.Web.UI.HtmlControls.HtmlInputHidden HidSearchOrderBy { get { return hidSearchOrderBy; } }
        public System.Web.UI.WebControls.PlaceHolder PlcPromotionProfile { get { return null; } }


        public IResultList SearchResultList { get { return searchResultList; } }
        public IQuickSearch QuickSearchEdit { get { return ucQuickSearchEdit; } }

        public FrameworkControl ResourceControl { get { return this; } }
        #endregion

        QuickSearchResultsHandler _handler;
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
               

                _handler = new QuickSearchResultsHandler(this, g);
                _handler.SetUpQuickSearchEdit();
                ucQuickSearchEdit.SetQuickSearch(false);

                ChangeVoteMessage = g.GetResource("ARE_YOU_SURE_YOU_WANT_TO_CHANGE", this);

                // check to see if the current user access the site has any search preferences specified,
                // if not then the user is redirected to SearchPrefs for search specifications, otherwise
                // the results of the search are displayed
                SearchPreferenceCollection searchPreferences = g.QuickSearchPreferences;

                //There is no spec for the external search interface so detect an external search
                //by checking for some of the most common parameters.
                if (Request["SearchTypeID"] != null || Request["ZipCode"] != null || Request["GenderID"] != null)
                {
                    // User is hitting search results directly, we're going to look for preferences identified in
                    // the query string.
                    _handler.SetSearchPreferencesFromQueryString();

                    // need to get results in this case
                    //g.Transfer("/Applications/QuickSearch/QuickSearchResults.aspx");
                }

                // If there are no search preferences in the SearchPreference object or the regionID is invalid,
                // redirect to SearchPreferences so that the user can fill in search preferences.
                // We look for searchPreferences.Count <= 1 because sometimes there will be 1 searchPref (searchTypeID)
                // and that does not count as valid search preferences (see TT 13887).  Checking against a count of 0 was
                // causing problems.
                if (searchPreferences.Count <= 1 || (!_handler.ValidRegionID(searchPreferences["RegionID"]) && searchPreferences["SearchTypeID"] != ((Int32)SearchTypeID.AreaCode).ToString()))
                {
                    g.Transfer("/Applications/QuickSearch/QuickSearch.aspx");
                }


                //Check to see if a search order has been specified
                if (Request[WebConstants.URL_PARAMETER_NAME_SEARCHORDERBY] == null)
                {
                    SearchOrderBy = Conversion.CInt(searchPreferences.Value("SearchOrderBy"));

                    if (SearchOrderBy <= 0)
                    {
                        SearchOrderBy = (Int32)Matchnet.Search.Interfaces.QuerySorting.JoinDate;
                    }
                }
                else
                {
                    SearchOrderBy = Convert.ToInt32(Request[WebConstants.URL_PARAMETER_NAME_SEARCHORDERBY]);
                  
                }

                bool colorCodeEnabled = SettingsManager.GetSettingBool(SettingConstants.ENABLE_COLORCODE_SEARCH, g.Brand);
                if (SearchOrderBy == (Int32)Matchnet.Search.Interfaces.QuerySorting.ColorCode)
                {
                    if (colorCodeEnabled)
                    {
                        colorCodeSelect.Visible = true;
                        colorCodeSelect.ShowLink = true;
                        colorCodeSelect.NavigateURL = "/Applications/QuickSearch/QuickSearchResults.aspx?SearchOrderBy=5";
                    }
                    else
                    {
                        colorCodeSelect.Visible = false;
                        colorCodeSelect.ShowLink = false;
                        SearchOrderBy = (Int32)Matchnet.Search.Interfaces.QuerySorting.JoinDate;
                    }
                }
                else if (SearchOrderBy == (int)Matchnet.Search.Interfaces.QuerySorting.Popularity
                    && !SearchUtil.IsSearchSortByPopularityEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                {
                    SearchOrderBy = (Int32)Matchnet.Search.Interfaces.QuerySorting.JoinDate;
                }

                if (SearchOrderBy == (int)Matchnet.Search.Interfaces.QuerySorting.KeywordRelevance)
                {
                    SearchOrderBy = (Int32)Matchnet.Search.Interfaces.QuerySorting.JoinDate;
                }

                bool saveQuickSearchPreferences = false;
                if (!g.QuickSearchPreferences.ContainsKey("SearchOrderBy") || g.QuickSearchPreferences["SearchOrderBy"] != SearchOrderBy.ToString())
                {
                    //this indicates search order by has changed, we'll need to save quicksearchpreferences
                    saveQuickSearchPreferences = true;
                }

                g.QuickSearchPreferences["SearchOrderBy"] = SearchOrderBy.ToString();

                if (saveQuickSearchPreferences)
                {
                    QuickSearchHandler quickSearchHandler = new QuickSearchHandler(null, g);

                    int memberid = Constants.NULL_INT;
                    if (g.Member != null) memberid = g.Member.MemberID;

                    Spark.SAL.MemberSearch memberSearch = quickSearchHandler.getMemberSearch(memberid);
                    Spark.SAL.Preference preference = Spark.SAL.Preference.GetInstance("searchorderby");
                    if (preference != null)
                    {
                        (memberSearch[preference] as MemberSearchPreferenceInt).Value = SearchOrderBy;
                    }
                    quickSearchHandler.PersistQSValues(memberSearch);
                    memberSearch.Save();
                }

                _handler.BindSortDropDown();

                // Wire up the List Navigation.
                g.ListNavigationTop = lblListNavigationTop;
                g.ListNavigationBottom = lblListNavigationBottom;

                //this is used for QA/TEST purpose, allows to ignore search cache
                if (!Page.IsPostBack && !String.IsNullOrEmpty(Request["ignoreCachedSearchResults"]) && Request["ignoreCachedSearchResults"] == "true")
                {
                    SearchResultList.IgnoreAllSearchResultsCache = true;
                }
              
            }
            catch (Exception ex) { g.ProcessException(ex); }
        }





        private void RenderPage()
        {
            try
            {


                idResultsViewType.GetSearchResultViewMode();
                SearchResultList.GalleryView = idResultsViewType.GalleryViewFlag;
             

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            RenderPage();
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            // PreRender defaults for any html/controls on the page

            // TT #15790 - specifically, set hidden field value back to false by default in all cases
      

            // Omniture page name override. This cannot be done in Omniture.ascx.cs because the gallery/list view mode is set here which gets executed after Omniture code
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                _g.AnalyticsOmniture.PageName = "Quick Search Results - " + _handler.GetSearchOrderBy() + " " + idResultsViewType.GetSearchResultViewMode();

                if (g.Session.GetString("QuickSearchBoxSearchClick", Constants.NULL_STRING) != Constants.NULL_STRING)
                {
                    _g.AnalyticsOmniture.Evar14 = "Quick Search Box";
                    g.Session.Remove("QuickSearchBoxSearchClick");
                }
            }
        }


        private void rptSearchOption_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            DataRowView row = (DataRowView)e.Item.DataItem;

            HyperLink lnkSort = (HyperLink)e.Item.FindControl("lnkSort");
            lnkSort.Text = row["Content"].ToString();
            lnkSort.NavigateUrl = "/Applications/QuickSearch/QuickSearchResults.aspx?" + WebConstants.URL_PARAMETER_NAME_SEARCHORDERBY + "=" + row["Value"].ToString();
           
            Literal litSortSpan = (Literal)e.Item.FindControl("litSortSpan");
            Literal litSortTitle = (Literal)e.Item.FindControl("litSortTitle");

            litSortTitle.Text = row["Content"].ToString();

            _sortSpans.Add(litSortSpan);
            _sortLinks.Add(lnkSort);
            _sortTitles.Add(litSortTitle);


            if (row["Value"].ToString() == SearchOrderBy.ToString())
            {
                //hidSearchOrderBy.Value = SearchOrderBy.ToString();
                litSortSpan.Text = "selected";
                lnkSort.Visible = false;
                litSortTitle.Visible = true;
            }
            else
            {
                litSortSpan.Text = "";
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rptSearchOption.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptSearchOption_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);
            this.PreRender += new System.EventHandler(this.Page_PreRender);

        }
        #endregion
    }
}