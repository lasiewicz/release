﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Search.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;

using System.Text;
using System.Xml;
using Matchnet.Web.Framework.Ui.FormElements;
using Spark.SAL;
using System.Drawing;

namespace Matchnet.Web.Applications.QuickSearch
{
    public interface IQuickSearchResults
    {

         Int32 SearchOrderBy{get;set;}
         string ChangeVoteMessage { get; set; }
         ArrayList SortSpans { get; set; }
         ArrayList SortTitles { get; set; }
         ArrayList SortLinks { get; set; }

         Matchnet.Web.Framework.Txt TxtWantMore { get; }
         Repeater RptMemberRepeater{get;}
         HtmlTableRow TrNoResults{get;}
         HtmlTableRow TrBarLower{get;}
         PlaceHolder PhResults{get;}
         Panel PnlNoResults{get;}
        
         DropDownList DdlSortMem{get;}
         HyperLink LnkViewType{get;}
         Matchnet.Web.Framework.Image Icon_ViewType{get;}
         IResultList SearchResultList{get;}
         Matchnet.Web.Framework.Title TxtMatches{get;}
         Matchnet.Web.Framework.Txt TxtView{get;}
         Matchnet.Web.Framework.Txt TxtViewAs{get;}
         HtmlInputHidden HidGalleryView{get;}
         HtmlInputHidden HidResultViewChanged{get;}
         Repeater RptSearchOption{get;}

         Label LblListNavigationTop{get;}
         Label LblListNavigationBottom{get;}

         
         System.Web.UI.HtmlControls.HtmlInputHidden HidSearchOrderBy{get;}
         System.Web.UI.WebControls.PlaceHolder PlcPromotionProfile{get;}
        
         IQuickSearch QuickSearchEdit { get; }

         FrameworkControl ResourceControl { get; }
    }
}
