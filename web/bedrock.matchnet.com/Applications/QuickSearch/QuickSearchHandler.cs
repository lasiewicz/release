﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using System.Xml;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Applications.Search;

using Spark.SAL;
using SearchVO = Matchnet.Search.ValueObjects;

namespace Matchnet.Web.Applications.QuickSearch
{
    public class QuickSearchHandler
    {
        IQuickSearch _control = null;
        ContextGlobal g;
        int _qsearchCriteriaExpirationDays = 365;
        public QuickSearchHandler(IQuickSearch control, ContextGlobal context)
        {
            _control = control;
            g = context;
            int _qsearchCriteriaExpirationDays = 365;
        }

        public void BindGender()
        {
            ListItem itemMale = new ListItem(g.GetResource("MALE"), ((Int32)Gender.Male).ToString());
            ListItem itemFemale = new ListItem(g.GetResource("FEMALE"), ((Int32)Gender.Female).ToString());

            _control.DdlGender.Items.Add(itemMale);
            _control.DdlGender.Items.Add(itemFemale);

            itemMale = new ListItem(g.GetResource("MALE"), ((Int32)Gender.Male).ToString());
            itemFemale = new ListItem(g.GetResource("FEMALE"), ((Int32)Gender.Female).ToString());

            _control.DdlSeekingGender.Items.Add(itemMale);
            _control.DdlSeekingGender.Items.Add(itemFemale);

            if (_control.MemberSearch.SearchName == "" && g.Member != null)
            {
                // When a member is logged in initially and does not have any search preferences
                // saved in session
                // Use Spark.SAL.GetSeekingGender to get the gender of the member
                FrameworkGlobals.SelectItem(_control.DdlGender, ((Int32)Spark.SAL.GenderUtils.GetSeekingGender(g.Member.GetAttributeInt(g.Brand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK))).ToString());
                // Use Spark.SAL.GenderUtils to get the gender that the member is seeking
                FrameworkGlobals.SelectItem(_control.DdlSeekingGender, ((Int32)Spark.SAL.GenderUtils.GetGender(g.Member.GetAttributeInt(g.Brand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK))).ToString());
            }
            else
            {
                FrameworkGlobals.SelectItem(_control.DdlGender, ((Int32)_control.MemberSearch.Gender).ToString());
                FrameworkGlobals.SelectItem(_control.DdlSeekingGender, ((Int32)_control.MemberSearch.SeekingGender).ToString());
            }


        }

        public void BindAge()
        {
            _control.TbAgeMin.Text = _control.MemberSearch.Age.MinValue.ToString();
            _control.TbAgeMax.Text = _control.MemberSearch.Age.MaxValue.ToString();
        }

        public void BindDistance()
        {
            SettingsManager settingsManager = new SettingsManager();
            DataTable distanceOptions = Option.GetOptions(SearchUtil.GetDistanceAttribute(g), g);

            _control.DdlDistance.DataSource = distanceOptions;
            _control.DdlDistance.DataTextField = "Content";
            _control.DdlDistance.DataValueField = "Value";
            _control.DdlDistance.DataBind();

            FrameworkGlobals.SelectItem(_control.DdlDistance, _control.MemberSearch.Distance.ToString(), settingsManager.GetSettingInt(SettingConstants.DEFAULT_DISTANCE_LIST_ORDER, g.Brand) - 1);
            //ddlDistance.SelectedValue = _memberSearch.Distance.ToString();
        }

        public void BindLocation(bool postBack)
        {
            #region Bind PickRegion Control
            if (!postBack)
            {
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                {
                    // Initially select the Region tab when opening the Search
                    // Region popup in JDate.fr  
                    _control.PrSearchRegion.SearchType = SearchType.Region;
                }
                else
                {
                    _control.PrSearchRegion.SearchType = _control.MemberSearch.SearchType;
                }
            }

            _control.PrSearchRegion.RegionID = _control.MemberSearch.RegionID;
            _control.PrSearchRegion.SchoolID = _control.MemberSearch.SchoolID;

            Int32 countryRegionID = 0;

            //TODO: abstract area code stuff into the SAL.MemberSearch object
            StringBuilder sbAreaCodes = new StringBuilder();
            for (Int32 i = 1; i <= 6; i++)
            {
                MemberSearchPreferenceInt areaCode = _control.MemberSearch[Preference.GetInstance("AreaCode" + i)] as MemberSearchPreferenceInt;
                if (areaCode != null && Conversion.CInt(areaCode.Value) > 0)
                {
                    countryRegionID = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveAreaCodes()[areaCode.Value];
                    _control.PrSearchRegion.AddAreaCode(areaCode.Value.ToString(), countryRegionID);
                    sbAreaCodes.Append(areaCode.Value.ToString() + ", ");
                }
            }
            if (sbAreaCodes.Length > 0)
            {
                sbAreaCodes.Remove(sbAreaCodes.Length - 2, 2);
            }

            _control.PrSearchRegion.AreaCodeCountryRegionID = countryRegionID;
            #endregion

            _control.LnkLocation.UpdateAfterCallBack = true;
            _control.LnkLocation.Attributes.Add("onclick", "popUpDiv(\"searchPopupDiv\",\"3px\",\"-3px\");return false");
            _control.LnkLocation.Attributes.Add("href", "#");

            _control.PlcDistance.Visible = true;
            _control.PlcDistance.UpdateAfterCallBack = true;
            _control.PlcAreaCodes.Visible = false;
            _control.PlcAreaCodes.UpdateAfterCallBack = true;
            _control.PlcCollege.Visible = false;
            _control.PlcCollege.UpdateAfterCallBack = true;

            switch (_control.MemberSearch.SearchType)
            {
                case SearchType.PostalCode:
                case SearchType.Region:
                    _control.LnkLocation.Text = FrameworkGlobals.GetRegionString(_control.MemberSearch.RegionID, g.Brand.Site.LanguageID);
                    break;
                case SearchType.AreaCode:
                    _control.PlcDistance.Visible = false;
                    _control.PlcAreaCodes.Visible = true;
                    _control.LnkLocation.Text = sbAreaCodes.ToString();
                    break;
                case SearchType.College:
                    _control.PlcDistance.Visible = false;
                    _control.PlcCollege.Visible = true;
                    MemberSearchPreferenceInt schoolIDPreference = _control.MemberSearch[Preference.GetInstance("SchoolID")] as MemberSearchPreferenceInt;
                    _control.LnkLocation.Text = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveSchoolName(schoolIDPreference.Value).SchoolName;
                    break;
            }

            _control.PrSearchRegion.RegionName = _control.LnkLocation.Text;
            _control.LnkLocation.Text += g.GetResource("EDIT", _control.ResourceControl);
        }

        public void BindHasPhoto()
        {
            _control.CbHasPhoto.Text = g.GetResource("TXT_SHOW_PHOTO_PROFILES_ONLY", _control.ResourceControl);

           if (_control.MemberSearch.SearchName == "")
            {
                // When a member is logged in initially and does not have any search preferences
                // saved in session
                // Set the default to requires photos only
                _control.CbHasPhoto.Checked = true;
            }
            else
            {
                _control.CbHasPhoto.Checked = _control.MemberSearch.HasPhoto;
            }

          
        }

        public void BindJewishOnly()
        {
            _control.CbJewishOnly.Text = g.GetResource("TXT_SHOW_JEWISH_ONLY", _control.ResourceControl);
            

            if (_control.MemberSearch.SearchName == "")
            {
                // When a member is logged in initially and does not have any search preferences
                // saved in session
                // Set the default to requires photos only
                _control.CbJewishOnly.Checked = true;
            }
            else
            {
                MemberSearchPreference memberpref=_control.MemberSearch[Preference.GetInstance("jdatereligion")] ;

                int jewishonly=(memberpref as MemberSearchPreferenceMask).Value;
                _control.CbJewishOnly.Checked = (jewishonly == FrameworkGlobals.GetJewishReligionOnly(g) || jewishonly == Constants.NULL_INT);
            }


        }

        public void BindRamahOnly()
        {
            _control.CbxRamahDate.Text = g.GetResource("TXT_SHOW_RAMAH_ONLY", _control.ResourceControl);

            MemberSearchPreferenceInt ramaPref = _control.MemberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt;
            _control.CbxRamahDate.Checked = (ramaPref.Value == 1);

        }



        public MemberSearch getMemberSearch(int memberID)
        {
            MemberSearch search = null;
         try
         {
             bool foundInSession = false;

             if (SearchFoundInSession())
             {
                 search = MemberSearchCollection.Load(Constants.NULL_INT, g.Brand).PrimarySearch;
                 if (search != null)
                 {
                     UpdateQSFromPersistence(search);
                 }

             }
             else
             {
                 if (g.Member != null)
                 {
                     search = MemberSearchCollection.Load(g.Member.MemberID, g.Brand).PrimarySearch;
                     
                     if (search != null)
                     {
                        UpdateQSFromPersistence(search);
                     }
                     search.MemberID = Constants.NULL_INT;
                 }
                 else
                 {
                     search = MemberSearchCollection.Load(Constants.NULL_INT, g.Brand).PrimarySearch;
                 }
             }

             if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
             {
                 MemberSearchPreference jdatereligionPreference = search[Preference.GetInstance("jdatereligion")];
                 if (jdatereligionPreference != null)
                 {
                     MemberSearchPreferenceMask jdatereligionPreferenceMask = (jdatereligionPreference as MemberSearchPreferenceMask);
                     if (jdatereligionPreferenceMask.Value == Constants.NULL_INT)
                     {
                         jdatereligionPreferenceMask.Value = FrameworkGlobals.GetJewishReligionOnly(g);
                     }
                }


                 MemberSearchPreference ramahPreference =  search[Preference.GetInstance("RamahAlum")];
                 if (ramahPreference != null)
                 {
                     MemberSearchPreferenceInt ramahPreferenceInt = (ramahPreference as MemberSearchPreferenceInt);
                     if (ramahPreferenceInt.Value != Constants.NULL_INT)
                     {
                         ramahPreferenceInt.Value = 1;
                     }
                 }


             }

             return search;
         }
         catch (Exception ex)
         {
             return search;
         }

        }

        private bool SearchFoundInSession()
        {
            int memberID = Constants.NULL_INT;
            if (g.Member != null) memberID = g.Member.MemberID;
            bool found = true;

            QuickSearchPersistence persistence = new QuickSearchPersistence(memberID, getCriteriaExpirationDays());

            if (persistence.GenderMask == Constants.NULL_INT || persistence.RegionID == Constants.NULL_INT ||
                persistence.MinAge == Constants.NULL_INT || persistence.MaxAge == Constants.NULL_INT || persistence.Distance == Constants.NULL_INT)
                found = false;

            return found;
        }

        public void PersistQSValues(MemberSearch memberSearch)
        {

            int memberID = Constants.NULL_INT;
            if (g.Member != null) memberID = g.Member.MemberID;

            QuickSearchPersistence persistence = new QuickSearchPersistence(memberID, getCriteriaExpirationDays());

            foreach (Preference Preference in memberSearch.SearchPreferences)
            {
                string prefName = Preference.Attribute.Name;
                switch (prefName.ToLower())
                {
                    case "birthdate":
                        MemberSearchPreferenceRange prefRange = (MemberSearchPreferenceRange)memberSearch[Preference];
                        persistence.MinAge = prefRange.MinValue;
                        persistence.MaxAge = prefRange.MaxValue;
                        break;
                    case "gendermask":
                        persistence.GenderMask = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "distance":
                        persistence.Distance = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "regionid":
                        persistence.RegionID = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "hasphotoflag":
                        persistence.HasPhotoFlag = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "schoolid":
                        persistence.SchoolID = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "searchtypeid":
                        persistence.SearchTypeID = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "searchorderby":
                        persistence.SearchOrderBy = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "jdatereligion":
                        persistence.JdateReligion = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode1":
                        persistence.AreaCode1 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode2":
                        persistence.AreaCode2 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode3":
                        persistence.AreaCode3 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode4":
                        persistence.AreaCode4 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode5":
                        persistence.AreaCode5 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode6":
                        persistence.AreaCode6 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "ramahalum":
                        persistence.RamahAlum = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                }
            }

            persistence.PersistValues();

        }

        private void UpdateQSFromPersistence(MemberSearch search)
        {
            int memberID = Constants.NULL_INT;
            if (g.Member != null) memberID = g.Member.MemberID;
            QuickSearchPersistence persistence = new QuickSearchPersistence(memberID, getCriteriaExpirationDays());

            if (persistence.GenderMask == Constants.NULL_INT || persistence.RegionID == Constants.NULL_INT ||
                persistence.MinAge == Constants.NULL_INT || persistence.MaxAge == Constants.NULL_INT || persistence.Distance == Constants.NULL_INT)
                return;

            search.Gender = Spark.SAL.GenderUtils.GetGender(persistence.GenderMask);
            search.SeekingGender = Spark.SAL.GenderUtils.GetSeekingGender(persistence.GenderMask);
            search.Age.MinValue = persistence.MinAge;
            search.Age.MaxValue = persistence.MaxAge;
            search.Distance = persistence.Distance;
            search.RegionID = persistence.RegionID;

            if (persistence.HasPhotoFlag != Constants.NULL_INT) 
                search.HasPhoto = Convert.ToBoolean(persistence.HasPhotoFlag);
            if (persistence.SchoolID != Constants.NULL_INT)
                search.SchoolID = persistence.SchoolID;
            if (persistence.SearchTypeID != Constants.NULL_INT)
                search.SearchType = (SearchType)persistence.SearchTypeID;
            if (persistence.AreaCode1 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference1 = search[Preference.GetInstance("AreaCode1")] as MemberSearchPreferenceInt;
                areaCodePreference1.Value = persistence.AreaCode1;
            }
            if (persistence.AreaCode2 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference2 = search[Preference.GetInstance("AreaCode2")] as MemberSearchPreferenceInt;
                areaCodePreference2.Value = persistence.AreaCode2;
            }
            if (persistence.AreaCode1 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference3 = search[Preference.GetInstance("AreaCode3")] as MemberSearchPreferenceInt;
                areaCodePreference3.Value = persistence.AreaCode3;
            }
            if (persistence.AreaCode4 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference4 = search[Preference.GetInstance("AreaCode4")] as MemberSearchPreferenceInt;
                areaCodePreference4.Value = persistence.AreaCode4;
            }
            if (persistence.AreaCode5 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference5 = search[Preference.GetInstance("AreaCode5")] as MemberSearchPreferenceInt;
                areaCodePreference5.Value = persistence.AreaCode5;
            }
            if (persistence.AreaCode6 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference6 = search[Preference.GetInstance("AreaCode6")] as MemberSearchPreferenceInt;
                areaCodePreference6.Value = persistence.AreaCode6;
            }
            if (persistence.JdateReligion != Constants.NULL_INT)
            {
                MemberSearchPreference jdatereligionPreference = search[Preference.GetInstance("jdatereligion")];
                (jdatereligionPreference as MemberSearchPreferenceMask).Value = persistence.JdateReligion;
            }
            if (persistence.SearchOrderBy != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt searchOrderByPreference = search[Preference.GetInstance("SearchOrderBy")] as MemberSearchPreferenceInt;
                searchOrderByPreference.Value = persistence.SearchOrderBy;
            }

            if(persistence.RamahAlum != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt ramahDatePreference = search[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt;
                ramahDatePreference.Value = persistence.RamahAlum;
            }

        }


        public static SearchVO.SearchPreferenceCollection GetQuickSearchPreferenceCollection(MemberSearch memberSearch)
        {
            SearchVO.SearchPreferenceCollection searchPreferenceCollection = new SearchVO.SearchPreferenceCollection();

            foreach (Preference Preference in memberSearch.SearchPreferences)
            {
                string prefName = Preference.Attribute.Name;

                switch (prefName.ToLower())
                {
                    case "birthdate":
                        MemberSearchPreferenceRange agePref = memberSearch[Spark.SAL.Preference.GetInstance("Birthdate")] as MemberSearchPreferenceRange;
                        searchPreferenceCollection.Add("MinAge", agePref.MinValue.ToString());
                        searchPreferenceCollection.Add("MaxAge", agePref.MaxValue.ToString());
                        break;
                    case "gendermask":
                    case "jdatereligion":
                    case "distance":
                    case "regionid":
                    case "hasphotoflag":
                    case "schoolid":
                    case "searchtypeid":
                    case "areacode1":
                    case "areacode2":
                    case "areacode3":
                    case "areacode4":
                    case "areacode5":
                    case "areacode6":
                    case "ramahalum":
                    case "searchorderby":
                        MemberSearchPreferenceInt prefInt = (MemberSearchPreferenceInt)memberSearch[Preference];
                        if (prefInt.Value != Constants.NULL_INT && prefInt.Value != 0)
                        {
                            searchPreferenceCollection.Add(prefName, prefInt.Value.ToString());
                        }
                        break;
                }
            }

            return searchPreferenceCollection;
        }
        private int getCriteriaExpirationDays()
        {
            int days = _qsearchCriteriaExpirationDays;
            try
            {
                days = Conversion.CInt(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("QSEARCH_CRITERIA_EXPIRATION", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID), _qsearchCriteriaExpirationDays);
                return days;
            }
            catch (Exception ex)
            {
                return days;
            }
        }

    }
}
