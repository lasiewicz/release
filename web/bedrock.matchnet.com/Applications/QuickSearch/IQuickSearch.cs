﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using System.Xml;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;

using Spark.SAL;

namespace Matchnet.Web.Applications.QuickSearch
{
    public interface IQuickSearch
    {
         Matchnet.Web.Framework.Title TtlPreferences{get;}
         DropDownList DdlGender{get;}
         DropDownList DdlSeekingGender{get;}
         TextBox TbAgeMin{get;}
         TextBox TbAgeMax{get;}
         Anthem.PlaceHolder PlcDistance{get;}
         Anthem.PlaceHolder PlcAreaCodes{get;}
         Anthem.PlaceHolder PlcCollege{get;}
         DropDownList DdlDistance{get;}
         Anthem.HyperLink LnkLocation{get;}
         CheckBox CbHasPhoto{get;}
         CheckBox CbJewishOnly { get; }
         CheckBox CbxRamahDate { get; }
         Button BtnSave{get;}
         //Anthem.PlaceHolder PlcPickRegion{get;}
         PickRegionNew PrSearchRegion{get;}
         Matchnet.Web.Framework.MultiValidator MvAgeMinValidator{get;}
         Matchnet.Web.Framework.MultiValidator MvAgeMaxValidator{get;}
         Matchnet.Web.Framework.MultiValidator MvLocationValidator{get;}
         MemberSearch MemberSearch { get; set; }

        PlaceHolder PlcTitle{get;}
        PlaceHolder PlcDivRightNewEnd{get;}
        PlaceHolder PlcDivRightNewStart{get;}
        PlaceHolder QuickSearchAd { get; }
         FrameworkControl ResourceControl{get;}
    }
}
