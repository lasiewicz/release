﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.LookupProfile.Controls
{
    public partial class LookupByMemberID20 : FrameworkControl
    {

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    int memberID = this.g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);

                    if (memberID != 0)
                    {
                        this.LookupMemberID.Value = memberID.ToString();
                    }

                    //	Set up the handler so that Enter presses work properly
                    LookupMemberID.Attributes.Add("onkeydown", "setAutoSubmitButton(" + btnSearchByMemberNumber.ClientID + ",event)");

                    //Disable the MemberID validator on load and when the MemberID text box loses focus
                    LookupMemberIDValidator.Enabled = false;
                    LookupMemberID.Attributes.Add("onblur", LookupMemberIDValidator.ClientID + ".enabled=false; ");
                    //Only enable it if the Go button next to the MemberID box is clicked:
                    btnSearchByMemberNumber.Attributes.Add("onclick", LookupMemberIDValidator.ClientID + ".enabled=true; ");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void btnSearchByMemberNumber_Click(object sender, EventArgs e)
        {
            try
            {
                int iMemberID = Constants.NULL_INT;
                double memberID;
                Matchnet.Member.ServiceAdapters.Member member = null;

                if (!double.TryParse(LookupMemberID.Value.Trim(), System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out memberID))
                {
                    g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                    return;
                }
                else
                {
                    iMemberID = Convert.ToInt32(memberID);
                }

                // Issue 14077.  Check for user name and email address to verify an existing member, MemberSA.Instance.GetMembersByMemberID 
                // does not return accurate results for verifying existing members.  Continue to utilize MemberSA.Instance.GetMember which 
                // returns a member with a null Username and null EmailAddress but all other default attributes for no members found. -Shu
                if (iMemberID != Constants.NULL_INT && iMemberID != 0 && iMemberID != int.MinValue)
                {
                    member = MemberSA.Instance.GetMember(iMemberID, MemberLoadFlags.IngoreSACache);

                    if (member != null && member.MemberID == iMemberID && member.GetUserName(_g.Brand) != null && member.EmailAddress != null)
                    {
                        bool isMemberOfCurrentCommunity = false;
                        int[] communityIDList = member.GetCommunityIDList();
                        foreach (int communityID in communityIDList)
                        {
                            if (communityID == g.Brand.Site.Community.CommunityID)
                            {
                                isMemberOfCurrentCommunity = true;
                                break;
                            }
                        }

                        // profile blocking feature check
                        bool isProfileBlockingEnabled = Convert.ToBoolean(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROFILE_BLOCK_ENABLED", g.Brand.Site.Community.CommunityID));
                        bool isProfileBlocked = false;
                        if (isProfileBlockingEnabled)
                        {
                            isProfileBlocked = g.List.IsHotListed(Matchnet.List.ValueObjects.HotListCategory.ExcludeListInternal, g.Brand.Site.Community.CommunityID, member.MemberID);
                        }

                        if (isMemberOfCurrentCommunity && !isProfileBlocked)
                        {
                            g.Transfer("/Applications/MemberProfile/ViewProfile.aspx?MemberID="
                                + iMemberID
                                + "&EntryPoint=" + (int)BreadCrumbHelper.EntryPoint.LookUpMember);
                        }
                        else
                        {	// member is not part of current community
                            g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                        }
                    }
                    else
                    {
                        g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                    }
                }
                else
                {
                    g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.btnSearchByMemberNumber.Click += new EventHandler(this.btnSearchByMemberNumber_Click);
        }
        #endregion

    }
}