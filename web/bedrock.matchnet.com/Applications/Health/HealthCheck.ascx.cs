using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Configuration.ServiceAdapters;


namespace Matchnet.Web.Applications.Health
{
    public class HealthCheck : FrameworkControl
    {
        private ICaching _Cache;
		string MATCHNET_SERVER_DOWN = "Matchnet Server Down";
		string MATCHNET_SERVER_ENABLED = "Matchnet Server Enabled";

		private void Page_Load(object sender, System.EventArgs e)
		{
			string ServerStatus = MATCHNET_SERVER_DOWN;

			try
			{
				g.SaveSession = false;

                _Cache = CachingTemp.Cache.GetInstance();

			    JITRecord jitRecord = _Cache.Get(JITRecord.GetCacheKey(System.Environment.MachineName)) as JITRecord;

				if (jitRecord == null)
				{
					try
					{
						WebRequest webRequest = WebRequest.Create("http://" + System.Environment.MachineName + ":" + Request.ServerVariables["SERVER_PORT"]);
						WebResponse webResponse = webRequest.GetResponse(); 
					}
					catch (Exception ex)
					{
						g.ProcessException(ex);
					}
                    ICacheable record = new JITRecord();
					_Cache.Insert(record);
				}

				if (Request["Headers"] != null)
				{
					foreach (string key in Request.Headers.AllKeys)
					{
						Response.Write(key + ": " + Request.Headers[key] + "<br />");
					}
				}

				if(isEnabled())
				{
					ServerStatus = MATCHNET_SERVER_ENABLED;
				}
			}
			catch (Exception ex)
			{
				ServerStatus = MATCHNET_SERVER_DOWN;

				// write this straight to the event log without attempting to use g
				Framework.Diagnostics.ContextExceptions.WriteExceptionToEventLogNoG(ex);
			}
			finally
			{
				Response.AddHeader("Health", ServerStatus);
				Response.Write(ServerStatus);
				Response.End();
			}
		}


		private bool isEnabled()
		{
			string filePath = Path.GetDirectoryName(Server.MapPath("/")) + @"\deploy.txt";

			bool fileExists = System.IO.File.Exists(filePath);
			bool isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.IsServerEnabled();

			if (!fileExists && isEnabled)
			{
				return true;
			}

			return false;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}


    [Serializable]
	public class JITRecord : Matchnet.ICacheable
	{
		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				return 3600;
			}
			set
			{
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Sliding;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return Matchnet.CacheItemPriorityLevel.High;
			}
			set
			{
			}
		}


		public string GetCacheKey()
		{
			return JITRecord.GetCacheKey(System.Environment.MachineName);
		}


		public static string GetCacheKey(string machineName)
		{
			return "JITRecord:" + machineName.ToLower();
		}
		#endregion
	}
}
