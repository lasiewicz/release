using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework;

using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;

namespace Matchnet.Web.Applications.Chat.XMLResponses
{

	/// <summary>
	///		Summary description for BaseXMLResponse.
	/// </summary>
	public class BaseXMLResponse : FrameworkControl
	{
		private const string XML_ELEMENT		= "<{0}>{1}</{0}>";
		private const string XML_CDATA_ELEMENT	= "<{0}><![CDATA[{1}]]></{0}>";
		private int _MemberID = Constants.NULL_INT;
		protected Matchnet.Member.ServiceAdapters.Member _Member;
		private int _targetMemberID = Constants.NULL_INT;
		private bool _IsAdmin = false;
		private bool hasAdminRun = false;
		private const string PARAM_SESSIONGUID = "sessionGUID";
		private const string PARAM_USERID = "userID";
		private string _SessionGUID = string.Empty;


		protected void startResponse()
		{
			Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>");
			Response.Write("<communicationsuite>");
		}

		protected void endResponse()
		{
			Response.Write("</communicationsuite>");
		}

		protected void writeElement(string pElementName, string pElementValue)
		{
			writeElement(pElementName, pElementValue, false);
		}

		protected void writeElement(string pElementName, string pElementValue, bool pUseCDATA)
		{
			if (pUseCDATA)
			{
				Response.Write(string.Format(XML_CDATA_ELEMENT, pElementName, pElementValue));
			}
			else
			{
				Response.Write(string.Format(XML_ELEMENT, pElementName, pElementValue));
			}
		}

		/// <summary>
		/// Write out error, Process it (so it shows up in sparkmon) and End Response immediately so 
		/// further errors don't get thrown. By writing the error out with the Response,
		/// the XML that Userplane will receive should be unknown and or invalid, and thus
		/// should show up in appInspector as well as being visual cue in browser when testing.
		/// </summary>
		/// <param name="ex"></param>
		protected void WriteAndProcessException(Exception ex)
		{	
			g.ProcessException(ex);
			//this.writeElement("error",ex.ToString(),true); 
			Response.Write(ex.ToString());//write to the output so that userplane will get invalid XML response
			//Response.End(); can't use Response.End() cuz you get a thread abort error http://support.microsoft.com/default.aspx?scid=kb;en-us;312629
			//HttpContext.Current.ApplicationInstance.CompleteRequest();//this doesn't end it right away though :( too bad! seems to continue on!
			//HttpContext.Current.Response.Flush();
			//Throw the exception, because we DON'T want formatted XML as a response
			//throw ex; // this will cause global.asax to catch it and it will redirect to error page when not local dev (which is inconvenient for testing)
			//This didn't work either... ThreadAbortStill thrown
			//			try 
			//			{
			//				Response.End();
			//			}
			//			catch (System.Threading.ThreadAbortException)
			//			{
			//
			//			}
			
			//TODO: must buffer all output (not use Response.Write) and then clear all output
			//and display readable error in HTML view of browser (right now, sometimes it's valid XML, 
			//and sometimes it's not... producing un-uniform results.
		}

		protected string DomainID
		{
			get
			{
				return Request["domainID"];
			}
		}

		/// <summary>
		/// Tries to return a Member based on given user id.
		/// </summary>
		protected Matchnet.Member.ServiceAdapters.Member Member
		{
			get
			{
				if (_Member == null)
				{
					_Member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);
					//throw exception if member not found. Should end process here.
					// Copied this comment from LookupByMemberID.ascx.cs: this is a HACK until further notice!
					// *******************************************************************
					// Issue 14077.  Check for user name and email address to verify an existing member, MemberSA.Instance.GetMembersByMemberID 
					// does not return accurate results for verifying existing members.  Continue to utilize MemberSA.Instance.GetMember which 
					// returns a member with a null Username and null EmailAddress but all other default attributes for no members found. -Shu
					// *******************************************************************
					if (_Member != null && _Member.MemberID == MemberID && _Member.GetUserName(_g.Brand) == null && _Member.EmailAddress == null) 
					{
						ApplicationException ex = new ApplicationException("Cannot find member with ID: " + MemberID + " when trying to process: " + Request.RawUrl);
						this.WriteAndProcessException(ex);//write to the output so that userplane will get unknown - but valid - XML response;
					}
				}
				return _Member;
			}
		}

		/// <summary>
		/// Tries to return a member ID based on query string param userID. This param is required
		/// for several XML calls. In getUser, userID can be blank if Authentication is required, so
		/// in that case must call AuthenticateChatUser first.
		/// </summary>
		protected int MemberID
		{
			get
			{
				if (_MemberID == Constants.NULL_INT)
				{
					if(Request.QueryString[PARAM_USERID] != null)
					{
						try
						{
							_MemberID = Convert.ToInt32(Request.QueryString[PARAM_USERID]); 
						}
						catch (System.FormatException ex1)
						{
							ApplicationException ex = new ApplicationException("Parameter: " + PARAM_USERID + "=" + Request.QueryString[PARAM_USERID] + " not an integer. " , ex1);
							WriteAndProcessException(ex);//write to the output so that userplane will get invalid XML response
						}
					}
					else
					{
						ApplicationException ex = new ApplicationException("Required parameter: " + PARAM_USERID + " not found in querystring.");
						WriteAndProcessException(ex); //write to the output so that userplane will get invalid XML response
					}
				}
				return _MemberID;
			}
		}

		/// <summary>
		/// Returns session guid from querystring. SessionGUID is required for getUser if userID is blank!
		/// </summary>
		protected string SessionGUID
		{
			get 
			{
				if(Request.QueryString[PARAM_SESSIONGUID] != null && Request.QueryString[PARAM_SESSIONGUID] != string.Empty)
				{
					_SessionGUID = Request.QueryString[PARAM_SESSIONGUID]; 
				}
				else if (Request.QueryString[PARAM_USERID] == string.Empty)
				{
					ApplicationException ex = new ApplicationException(string.Format("Querystring parameter {0} is required if parameter {1} is blank",PARAM_SESSIONGUID, PARAM_USERID));
					WriteAndProcessException(ex);
				}
				return this._SessionGUID;

			}
		}

		/// <summary>
		/// Returns whether userid is blank in querystring. This signifies that we 
		/// must authenticate user by sesssion GUID
		/// </summary>
		private bool AuthenticationRequired
		{
			get 
			{
                //if(Request.QueryString[PARAM_USERID] != null && Request.QueryString[PARAM_USERID] == string.Empty)
                if (string.IsNullOrEmpty(Request.QueryString[PARAM_USERID]))
                {
					return true; 
				}
				else 
				{
					return false;
				}
			}
		}

		/// <summary>
		/// MUST call this before accessing Member in getUser.
		/// If userID supplied is blank, tries to get a valid member id based on supplied SessionGUID.
		/// Returns false if session is invalid. 
		/// Returns true if a non-blank userID is supplied (no validation is done here)
		/// or if sessionGUID supplied is valid.
		/// (Either a userID or sessionguid must be supplied for getUser, but are not required parameters for all XML calls.)
		/// </summary>
		protected bool AuthenticateChatUser()
		{
			bool authenticated = true;
			if (AuthenticationRequired) 
			{ 
				//returns Constants.NULL_INT if session is invalid
				try 
				{
                    System.Diagnostics.Trace.Write("AuthenticateChatUser: SessionGUID: "  + SessionGUID);

					_MemberID = Matchnet.Session.ServiceAdapters.SessionSA.Instance.GetMemberID(SessionGUID);

                    System.Diagnostics.Trace.Write("AuthenticateChatUser: Got memberid: " + MemberID.ToString());
				}
				catch(Exception ex)
				{
					//HACK: catch badly formed SessionGUID errors (such as incorrect number of characters and such)
					//Service should really return NULL_INT regardless of badly formed SessionGUID in my opinion.
					authenticated = false;
                    System.Diagnostics.Trace.Write("AuthenticateChatUser: Failed to get memberid, ex: " + ex.ToString());
                    g.ProcessException(ex);
				}
				if (_MemberID == Constants.NULL_INT || _MemberID==0) 
				{
					authenticated = false;
                    System.Diagnostics.Trace.Write("AuthenticateChatUser: Failed to get memberid: " + MemberID.ToString());
				}
				
			}
			return authenticated;
		}

		/// <summary>
		/// Adds a target user to current user's hotlist or block list.
		/// Handles setfriendstatus and setblockedstatus Userplane commands. </summary>
		/// <param name="hotListCategory">The type of hot list category to modify</param>
		protected void AddListMember(List.ValueObjects.HotListCategory hotListCategory)
		{
			List.ValueObjects.ListSaveResult result = new ListSaveResult();
			result = ListSA.Instance.AddListMember(hotListCategory,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID,
				this.Member.MemberID,
				_targetMemberID,
				string.Empty,
				Constants.NULL_INT,
				false,
				false);

            if (UserNotificationFactory.IsUserNotificationsEnabled(g))
            {
                UserNotificationParams unParams = UserNotificationParams.GetParamsObject(_targetMemberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new AddedYouAsFavoriteUserNotification(), _targetMemberID, this.Member.MemberID, g);
                if (notification.IsActivated())
                {
                    UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                }
            }
		}

		/// <summary>
		/// Removes a target user to current user's hotlist or block list.
		/// Handles setfriendstatus and setblockedstatus Userplane commands. </summary>
		/// <param name="hotListCategory">The type of hot list category to modify</param>
		protected void RemoveListMember(List.ValueObjects.HotListCategory hotListCategory)
		{
			ListSA.Instance.RemoveListMember(hotListCategory, 
				g.Brand.Site.Community.CommunityID, 
				this.Member.MemberID,
				_targetMemberID);
		}

		/// <summary>
		/// This parameter is required by both settingFriendStatus and setBlockedStatus to determine
		/// which user to add, remove or block as a friend.
		/// </summary>
		/// <param name="paramTargetUserID">The user id to perform the add/remove/block action on</param>
		protected void GetTargetUserID(string paramTargetUserID) 
		{
			// get the target user id
			if (Request[paramTargetUserID] != null) 
			{
				try 
				{
					_targetMemberID = Convert.ToInt32(Request[paramTargetUserID]);
				}
				catch (Exception ex) 
				{
					Response.Write("Querystring parameter " + paramTargetUserID + " must be an integer.");
					WriteAndProcessException(ex);
				}
			}
			else 
			{
				ApplicationException ex = new ApplicationException("Querystring parameter " + paramTargetUserID + " is required.");
				WriteAndProcessException(ex);
			}
		}

		/// <summary>
		/// Generic function that returns true/false for required query string parameters.
		/// Used by settingFriendStatus and setBlockedStatus for the friend and blocked parameters
		/// </summary>
		/// <param name="paramName">The string name of the parameter that has the true/false values</param>
		/// <returns></returns>
		protected bool GetParameter(string paramName){

			bool retVal = false;
			if (Request[paramName] != null) 
			{
				try
				{
					retVal = Convert.ToBoolean(Request[paramName]);
				}
				catch(Exception ex)
				{
					Response.Write("Querystring parameter " + paramName + " must be a boolean value.");
					WriteAndProcessException(ex);
				}
			}
			else
			{
				ApplicationException ex = new ApplicationException("Querystring parameter " + paramName + " is required");
				WriteAndProcessException(ex);
			}
			return retVal;

		}

		protected bool IsAdmin
		{
			get
			{
				if(hasAdminRun)
				{
					return _IsAdmin;
				}
				else
				{
					_IsAdmin = MemberPrivilegeSA.Instance.IsAdmin(this.Member.MemberID, HttpContext.Current);
					hasAdminRun = true;
					return _IsAdmin;
				}
			}
		}


	}
}
