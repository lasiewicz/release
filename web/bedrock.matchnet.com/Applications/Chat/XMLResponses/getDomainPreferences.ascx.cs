using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Chat.XMLResponses
{
    /// <summary>
    ///		Summary description for getDomainPreferences.
    /// </summary>
    public class getDomainPreferences : BaseXMLResponse
    {

        private void Page_Load(object sender, System.EventArgs e)
        {
            startResponse();

            writeElement("time", DateTime.Now.ToString());

            Response.Write("<domain>");

            writeElement("avenabled", "true");
            writeElement("gameButton", "true");
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JewishMingle)
            {
                writeElement("conferenceCallEnabled", "false");
            }
            else
            {
                writeElement("conferenceCallEnabled", "true");
            }

            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL ||
                g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
            {
                writeElement("allowCalls", "setBannedStatus,setBlockedStatus,setFriendStatus,getConfPhoneNumber");
            }


            Response.Write("<chat>");

            WriteLabels();

            writeElement("maxroomusers", "40");
            // 3 is the max dockable items
            writeElement("maxdockitems", "3");
            writeElement("characterlimit", g.GetResource("characterlimit", this));
            writeElement("userroomcreate", "true");
            writeElement("roomemptytimeout", "120");

            WriteGui();

            WriteRoomList();

            Response.Write("</chat>");
            Response.Write("</domain>");

            endResponse();
        }

        private void WriteLabels()
        {
            Response.Write("<labels>");

            Response.Write("<userdata initiallines=\"3\">");

            writeElement("line", g.GetResource("Username", this));
            writeElement("line", g.GetResource("Age", this));
            writeElement("line", g.GetResource("Gender", this));
            writeElement("line", g.GetResource("Location", this));

            Response.Write("</userdata>");

            Response.Write("<lobby>");

            writeElement("name", g.GetResource("LobbyName", this));
            writeElement("description", g.GetResource("LobbyDescription", this));

            Response.Write("</lobby>");

            Response.Write("</labels>");
        }

        /// <summary>
        /// Write out GUI related options
        /// </summary>
        private void WriteGui()
        {
            Response.Write("<gui>");
            writeElement("viewprofile", "true");
            writeElement("instantcommunicator", "true");
            writeElement("addfriend", "true");
            writeElement("block", "true");

            Response.Write("<images>");
            writeElement("watermark", Framework.Image.GetURLFromFilename("IM_watermark.jpg"));
            Response.Write("</images>");

            writeElement("initialinputlines", "1");

            Response.Write("</gui>");
        }

        /// <summary>
        /// Write the list of rooms to XML.
        /// Rooms are site specific and need to be resource driven for Name and Description
        /// </summary>
        private void WriteRoomList()
        {
            Response.Write("<roomlist>");

            // Room1, typically 20s
            Response.Write("<room>");
            writeElement("name", g.GetResource("Room1Name", this));
            writeElement("description", g.GetResource("Room1Description", this));
            Response.Write("</room>");

            // Room2, typically 30s
            Response.Write("<room>");
            writeElement("name", g.GetResource("Room2Name", this));
            writeElement("description", g.GetResource("Room2Description", this));
            Response.Write("</room>");

            // Room3, typically 40s
            Response.Write("<room>");
            writeElement("name", g.GetResource("Room3Name", this));
            writeElement("description", g.GetResource("Room3Description", this));
            Response.Write("</room>");

            // Room4, typically 50s
            Response.Write("<room>");
            writeElement("name", g.GetResource("Room4Name", this));
            writeElement("description", g.GetResource("Room4Description", this));
            Response.Write("</room>");

            if (g.GetResource("Room5Name", this).IndexOf("getDomainPreferences.ascx:Room5Name") == -1 && !string.IsNullOrEmpty(g.GetResource("Room5Name", this)))
            {
                // Room5, (for JDATE: East Coast)
                Response.Write("<room>");
                writeElement("name", g.GetResource("Room5Name", this));
                writeElement("description", g.GetResource("Room5Description", this));
                Response.Write("</room>");
            }

            if (g.GetResource("Room6Name", this).IndexOf("getDomainPreferences.ascx:Room6Name") == -1 && !string.IsNullOrEmpty(g.GetResource("Room6Name", this)))
            {
                // Room6, (for JDATE: West Coast)
                Response.Write("<room>");
                writeElement("name", g.GetResource("Room6Name", this));
                writeElement("description", g.GetResource("Room6Description", this));
                Response.Write("</room>");
            }

            Response.Write("</roomlist>");
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
