using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.InstantMessenger;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Chat.XMLResponses
{
	/// <summary>
	///		Authenticates user and returns information about user using chat feature. 	
	///		UserID parameter will be empty upon Authorization request. 
	///		Returns the userID, or "INVALID" if the sessionGUID is not valid.  
	///		From UP documentation: "This function is used in 2 ways.  When the user first connects, it is used to authenticate
	///		the user and give them access to the site.  Later, it is used to update the user�s
	///		information in the event of a change. Whenever you return any value other than �INVALID�, 
	///		you also need to return other data for the user."
	///		
	///			Parameters
	///			domainID:		The identifier for the domain the user is on (not used by us - we use URL to determine brand)
	///			sessionGUID:	For authentication requests, the session id - required if userID is blank (we pass this to UP in flashvars - they pass it back)
	///			key:	For authentication requests, optional additional identifier information (we are NOT using this)
	///			userID:	For non-authentication requests, the userID of the requested user (required, if sessionGUID is blank)
	/// </summary>
	public class getUser : BaseXMLResponse
	{
		/// <summary>
		/// Main page load method for writing out XML
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				startResponse();

				writeElement("time", DateTime.Now.ToString());

				WriteUser();

				endResponse();
			}
			catch(Exception ex)
			{
				base.WriteAndProcessException(ex);
			}
		}

		/// <summary>
		/// Write User specifc xml data to the response
		/// </summary>
		private void WriteUser()
		{
			Response.Write("<user>");

			//check if user is authenticated by passing back sessionGUID that was given to UP in flashvars
			if(AuthenticateChatUser())
			{
				int ChatBanned = this.Member.GetAttributeInt(g.Brand, "ChatBanned");
				if(ChatBanned > 0)
				{
					writeElement("userid", "INVALID");
				}
				else
				{
					writeElement("userid", this.Member.MemberID.ToString());
				}

				writeElement("admin", MemberPrivilegeSA.Instance.IsUserplaneRequestAdmin(this.Member.MemberID, g.ClientIP).ToString().ToLower());

				writeElement("displayname", this.Member.GetUserName(_g.Brand));

				WriteAVSettings();
				WriteList("buddylist", List.ValueObjects.HotListCategory.Default);
				WriteList("blocklist", List.ValueObjects.HotListCategory.IgnoreList);
				WriteImages();
				WriteChat();
			}
			else
			{
				//sessionGUID supplied is invalid
				writeElement("userid", "INVALID");
				writeElement("admin", "false");
				writeElement("displayname", "INVALID");
			}

			Response.Write("</user>");
		}

		/// <summary>
		/// Write AV settings to the response
		/// Currently these are the default settings and should not be tweaked 
		/// </summary>
		private void WriteAVSettings()
		{
			Response.Write("<avsettings>");
			
			writeElement("audiosend", "true");
			writeElement("videosend", "true");
			writeElement("audioreceive", "true");
			writeElement("videoreceive", "true");
			writeElement("audiokbps", "16");
			writeElement("videokbps", "200");
			writeElement("videofps", "15");

			Response.Write("</avsettings>");
		}

		/// <summary>
		/// Write list information to the response
		/// </summary>
		/// <param name="UPListName">The name of the list correlating to UP</param>
		/// <param name="CategoryType">The category type of the list</param>
		private void WriteList(string UPListName, List.ValueObjects.HotListCategory CategoryType)
		{
			Response.Write("<" + UPListName + ">");
			int TotalRows = 0;

			ArrayList targetMemberIDs = new ArrayList();
			
			List.ServiceAdapters.List list = List.ServiceAdapters.ListSA.Instance.GetList(this.Member.MemberID);

			targetMemberIDs = list.GetListMembers(CategoryType,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID,
				1,
				15,
				out TotalRows);

			foreach(int x in targetMemberIDs)
			{
				writeElement("userid", x.ToString());
			}
			Response.Write("</" + UPListName + ">");
		}

		/// <summary>
		/// Write out user-specific image data to the response
		/// These must be dynamic based on the user information
		/// </summary>
		private void WriteImages()
		{
			Response.Write("<images>");

			// we don't currently support icon's (these are displayed as small icons next to user names in the chat window
			//writeElement("icon", "icon url");

			writeElement("thumbnail", GetPhotoPath());
			writeElement("fullsize", GetPhotoPath());
			
			Response.Write("</images>");
		}

		/// <summary>
		/// Write user-specific chat data to the response
		/// </summary>
		private void WriteChat()
		{
			// initialize birth date
			DateTime birthDate = this.Member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue);		

			Response.Write("<chat>");
			
			Response.Write("<userdatavalues>");
			// username
			writeElement("line", this.Member.GetUserName(_g.Brand));
			// birth date
			writeElement("line", FrameworkGlobals.GetAgeCaption(birthDate,g,this, String.Empty));
			// gender text
			writeElement("line", FrameworkGlobals.GetGenderMaskString(this.Member.GetAttributeInt(g.Brand, "GenderMask")));
			// location text
			writeElement("line", FrameworkGlobals.GetRegionString(Member.GetAttributeInt(g.Brand, "regionid"), g.Brand.Site.LanguageID, false,true,false));
			Response.Write("</userdatavalues>");

			Response.Write("<gui>");
			writeElement("viewprofile", "true");
			writeElement("instantcommunicator", "true");
			Response.Write("</gui>");

			writeElement("notextentry", "false");	

			// if the member is an admin and he has HideMask of HideSearch (hide profile search to members), make this user invisible
			if(this.IsAdmin &&
				((this.Member.GetAttributeInt(g.Brand, "HideMask", 0) & 
					(Int32)WebConstants.AttributeOptionHideMask.HideSearch) == (Int32)WebConstants.AttributeOptionHideMask.HideSearch))
			{
				writeElement("invisible", "true");
			}
			else
			{
				writeElement("invisible", "false");
			}
			writeElement("userroomcreate", "true");

			// if this element is left blank, the user is defaulted to the lobby, which is what we want
			//writeElement("initialroom", "Lobby");

			Response.Write("</chat>");
		}

		private string GetPhotoPath()
		{
			string url = string.Empty;

			try 
			{
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(this.Member, this.Member, g.Brand);
                url = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(this.Member, this.Member, g.Brand, photo, PhotoType.Full, PrivatePhotoImageType.Thumb, NoPhotoImageType.Thumb);

                string subDomainPrefix = base.g.Brand.Site.DefaultHost;
                if (HttpContext.Current.Request.Url.Host.ToLower().IndexOf("stage.") > -1)
                {
                    subDomainPrefix = "stage";
                }

                if (!url.StartsWith("http://"))
                {
                    url = "http://" + subDomainPrefix + "." + base.g.Brand.Uri + url;
                }
			}
			catch (Exception ex) 
			{
				g.ProcessException(new ApplicationException("Error getting photo for member " + base.MemberID, ex));
			}

			return url;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
