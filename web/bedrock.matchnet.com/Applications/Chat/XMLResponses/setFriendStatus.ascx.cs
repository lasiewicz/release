using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.List.ValueObjects;

namespace Matchnet.Web.Applications.Chat.XMLResponses
{
	/// <summary>
	///	Sets whether userID wants friendUserID added or removed from their friend list
	///	Parameters
	///		domainID:		The identifier for the domain the user is on (not in used by us. We use URL to identify brand)
	///		userID:			The user�s Identifier (handled by base.MemberID);
	///		friendUserID:		The user to add (or remove)
	///		friend:	A true or false value that specifies whether the friend user is to be added or removed from their friend list

	/// </summary>
	public class setFriendStatus :  BaseXMLResponse
	{

		private const string PARAM_TARGETUSERID = "friendUserID";
		private const string PARAM_FRIEND = "friend";
		private const List.ValueObjects.HotListCategory _hotListCategory = List.ValueObjects.HotListCategory.Default; //modify the default hotlist


		private void Page_Load(object sender, System.EventArgs e)
		{
			//get required parameters
			base.GetTargetUserID(PARAM_TARGETUSERID);
			bool AddFriend = base.GetParameter(PARAM_FRIEND);

			// decide whether to add friend or
			switch (AddFriend) 
			{
				case true:
					base.AddListMember(_hotListCategory);
					break;
				case false:
					base.RemoveListMember(_hotListCategory);
					break;
			}

			// send nothing in response except root element
			startResponse();
			endResponse();
		}

		



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
