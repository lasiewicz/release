﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;


namespace Matchnet.Web.Applications.Chat
{
    public partial class Default20 : FrameworkControl
    {
        private const string PARAM_REDIRECTSUBSCRIPTION = "redirectSubscription";

        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
                if (g.Member != null)
                {
                    if (MemberPrivilegeAttr.IsPermitMember(g) || g.IsAdmin || !g.Brand.IsPaySite)
                    {
                        int ChatBanned = g.Member.GetAttributeInt(g.Brand, "ChatBanned");
                        // 0 or NULL_INT mean not banned
                        if (ChatBanned >= 1)
                        {
                            phBanned.Visible = true;
                            phChat.Visible = false;
                        }
                    }
                    else
                    {
                        if (Request.QueryString[PARAM_REDIRECTSUBSCRIPTION] == "true")
                        {
                            Redirect.Subscription(g.Brand
                                , (int)Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.AttemptToChat
                                , Constants.NULL_INT
                                , false
                                , Server.UrlEncode("/Applications/Chat/Default.aspx"));
                        }
                        else
                        {
                            imgChatButton.NavigateUrl = "default.aspx?" + PARAM_REDIRECTSUBSCRIPTION + "=true";
                        }

                    }
                }
                else
                {
                    Exception ex = new Exception("Null member arrived at Chat\\Default.ascx.");
                    g.ProcessException(ex);
                    g.Transfer("/Default.aspx");
                }

                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("NAV_CHAT", this),
                                                            g.AppPage.App.DefaultPagePath);

                }
                if (g.BreadCrumbTrailFooter != null)
                {
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("NAV_CHAT", this),
                                                           g.AppPage.App.DefaultPagePath);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.Page_Init);
        }
        #endregion
    }
}