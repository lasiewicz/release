using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Chat
{
    /// <summary>
    ///		Summary description for upChat.
    /// </summary>
    public class upChat : FrameworkControl
    {
        protected System.Web.UI.WebControls.Literal jsOwnProfileLink;
        protected System.Web.UI.WebControls.Literal jsDestinationProfileLink;
        protected Matchnet.Web.Framework.Image logo;
        protected System.Web.UI.WebControls.Literal jsOwnMemberID;
        protected System.Web.UI.WebControls.PlaceHolder phFrameset;
        protected System.Web.UI.WebControls.PlaceHolder phTopFrame;
        protected System.Web.UI.WebControls.PlaceHolder phMainFrame;
        protected System.Web.UI.WebControls.PlaceHolder phHebrewSupport;
        protected System.Web.UI.WebControls.Literal litFocusOnLoad;
        protected const string JAVASCRIPT_FILE_PATH_CHAT = "javascript/userplaneChat.js";

        public bool IsHebrewOnWin32IE
        {
            get
            {
                if (g.Brand.Site.LanguageID != (int)Matchnet.Language.Hebrew)
                {
                    return false;
                }
                else
                {
                    return ((Request.Browser.Browser == "IE") && (Request.Browser.Win32));
                }
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            // if Connect Chat is enabled, we don't want to allow access to this page
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONNECT_CHAT_ENABLE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)))
            {
                Response.Redirect("/Default.aspx");
            }

            //check if user has access to this chat feature
            if (!ValidateChatAccess())
            {
                //non-paying users were finding their way to chat through the URL. Now we
                //validate their access rights and redirect to sub.
                RedirectToSubscriptionFromChat();

            }

            // set up frameset - unfortunately since flash wants to be at 100%
            // must use framesets in order to brand chat with header logo at top
            string frame = string.Empty;
            if (Request.QueryString["frame"] != null)
            {
                frame = Request.QueryString["frame"];
                switch (frame)
                {
                    case "top":
                        phTopFrame.Visible = true;
                        phFrameset.Visible = false;
                        break;
                    case "main":
                        phMainFrame.Visible = true;
                        phFrameset.Visible = false;
                        break;
                    default:
                        //show only frameset (visible by default)
                        break;
                }
            }

            //add javascript values for links from flash chat movie
            jsOwnProfileLink.Text = "var ownProfileLink = \"" + ViewOwnProfileLink + "\";";//url of own profile
            jsDestinationProfileLink.Text = "var destinationProfileLink = \"" + ViewDestinationProfileLink + "\";";//launchwindow script to pop-open other's profile
            jsOwnMemberID.Text = "var ownMemberID = \"" + g.Member.MemberID + "\";"; //current member id

            if (IsHebrewOnWin32IE)
            {
                // The wmode parameter is only applicable to the DHTML version of the tool,
                // it's presence causes issues with the standard flash version.
                // litHebrewCompatParam.Visible = true;
                //phHebrewSupport.Visible = true;
                //Calling the focusIt() method on body load event causes focus to go to the typing area when the IM window opens (body onload) - see 15466.
                //However, if this is a Hebrew DHTML IM client, then focus on typing area is set on the createLayer call, 
                //not on body=onload, because it's a HTML text input -  see 16855.
                //So, don't call flash object into focus if this is Hebrew client... Also, in Cupid 
                //there is another side effect of setting focus on flash object - it seems to make flash jump upwards and
                // cover the ad banner IFRAME on the top of the window 16858. So, if we ever add an IFRAME ad banner to the
                //English sites, we will have to revisit this problem.
                //litFocusOnLoad.Visible = false;
            }

            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
            {
                logo.FileName = "im_header_left_red.gif";
            }
        }


        /// <summary>
        /// Returns whether or not the current member has access to the chat feature.
        /// Retursn true if logged in as an admin. Returns false if not a paying member on a 
        /// site that requires payment. Also returns false if a member is banned.
        /// </summary>
        /// <returns></returns>
        public bool ValidateChatAccess()
        {

            bool isValid = true;

            try
            {
                if (g.Member != null)
                {
                    if (g.IsAdmin)
                    {
                        //if is admin allow chat
                        isValid = true;

                    }
                    else if (!MemberPrivilegeAttr.IsPermitMember(g) && g.Brand.IsPaySite)
                    {
                        //if non-paying member on site that requires payment, do not allow chat
                        isValid = false;

                    }
                    else
                    {
                        //if banned from using chat, do not allow chat
                        int ChatBanned = g.Member.GetAttributeInt(g.Brand, "ChatBanned");
                        // 0 or NULL_INT mean not banned
                        if (ChatBanned >= 1)
                        {
                            isValid = false;
                        }

                    }
                }
                else
                {
                    //must be a logged in member to use chat, but this
                    //is handled earlier in the framework, and the user 
                    //should be redirected to login page
                    isValid = false;
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);

            }
            return isValid;
        }

        private void RedirectToSubscriptionFromChat()
        {

            Matchnet.Web.Framework.Util.Redirect.Subscription(g.Brand
                , (int)Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.AttemptToChat
                , Constants.NULL_INT
                , false
                , Server.UrlEncode("/Applications/Chat/Default.aspx"));

        }

        public string FlashComServer
        {
            get
            {
                return Configuration.ServiceAdapters.RuntimeSettings.GetSetting("INSTANTMESSENGERSVC_UP_FLASH_COM_SERVER");
            }
        }

        public string ApplicationName
        {
            get
            {
                return "CommunicationSuite";
            }
        }

        public string FlashVars
        {
            get
            {
                return "strServer=" + FlashComServer
                    + "&strSwfServer=" + SwfServer
                    + "&strDomainID=" + getDomainIDForUserplane()
                    + "&strSessionGUID=" + g.Session.Key.ToString()
                    + "&memberID=" + g.Member.MemberID
                    + "&strKey="
                    + "&strLocale=" + GetLocale()
                    + "&strInitialRoom=";
            }
        }

        public string SwfServer
        {
            get
            {
                // There is a one-to-one relationship restriction between the referrer (swfserver in this case)
                // with each DomainID.  Since JDate.com & JDate.co.uk are going to share the same DomainID, we will
                // have to make sure the referrer for JDate.co.uk is swf.jdate.com, and not swf.jdate.co.uk.
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                    return "swf." + g.Brand.Site.Name.Trim();

                if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
                    return "swf.jdate.com";
                else
                    return "swf." + g.Brand.Site.Name.Trim();
            }
        }

        /// <summary>
        /// Passes -1 as member id to helper therefore returns javascript to launchWindow. Removes any "javascript:" prefix
        /// </summary>
        public string ViewDestinationProfileLink
        {
            get
            {
                return Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NoArgs, -1).Replace("javascript:", string.Empty);
            }
        }
        /// <summary>
        /// Passes currently logged in member id to helper therefore returns url of profile as a string, not javascript. Removes any PersistLayoutTemplate=1.
        /// </summary>
        public string ViewOwnProfileLink
        {
            get
            {
                return Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NoArgs, g.Member.MemberID).Replace("&PersistLayoutTemplate=1", string.Empty);
            }
        }

        /// <summary>
        /// Appends ?v=[GetLastWriteTimeString] to the javascript path. (Useful for defeating browser caching when deploying new 
        /// versions (e.g. sparkCommonNonHe.css?v=11172005115104)
        /// </summary>
        /// <param name="path">The relative to site root or relative path to file</param>
        /// <returns></returns>
        protected string ChatJavascript
        {
            get
            {
                return JAVASCRIPT_FILE_PATH_CHAT + "?v=" + FrameworkGlobals.GetLastWriteTimeString(JAVASCRIPT_FILE_PATH_CHAT);
            }
        }
        private string getDomainIDForUserplane()
        {
            // As of 9/7/05, g.Brand.Site.DefaultHost returns either www or dev
            // In order to test on stage, we need to leave stage in the URL - related bug: 14779)
            string subDomainPrefix = g.Brand.Site.DefaultHost;

            string requestUrlBeg = string.Empty;
            if (Request.Url.Host.ToLower().Length >= 8)
                requestUrlBeg = Request.Url.Host.ToLower().Substring(0, 8);


            if (requestUrlBeg != string.Empty && requestUrlBeg != null)
            {
                if (requestUrlBeg.Substring(0, 3).IndexOf("stg") > -1 || requestUrlBeg.Substring(0, 3).IndexOf("dev") > -1)
                {

                    if (requestUrlBeg.Substring(0, 4).IndexOf("stgv") > -1)
                    {
                        //and now we have stgv101.....
                        subDomainPrefix = requestUrlBeg.Substring(0, 7);
                    }
                    else
                    {
                        // To support the new stage env that starts with stg, we need to be able to accept this format
                        // stg## --> Examples: stg01, stg02, etc.
                        subDomainPrefix = requestUrlBeg.Substring(0, 5);
                    }
                }
                else if (requestUrlBeg.IndexOf("preprod") > -1)
                {
                    subDomainPrefix = "preprod";
                }
#if DEBUG
                else if (requestUrlBeg.IndexOf("skener") > -1 || requestUrlBeg.IndexOf("okle") > -1) // TODO: REMOVE THIS LATER (ONLY FOR DEV)
                {
                    subDomainPrefix = "dev01";
                }
#endif
            }

            // JDIL and JDFR should each have a unique domain different than the JD community
            switch (g.Brand.Site.SiteID)
            {
                case (int)WebConstants.SITE_ID.JDateCoIL:
                case (int)WebConstants.SITE_ID.JDateFR:
                    return subDomainPrefix + "." + g.Brand.Site.Name;
                default: break;
            }

            // Jdate.com and Jdate.co.il belong to the same IM domain.
            switch (g.Brand.Site.Community.CommunityID)
            {
                case (int)WebConstants.COMMUNITY_ID.JDate:
                    return subDomainPrefix + ".jdate.com";
                case (int)WebConstants.COMMUNITY_ID.AmericanSingles:
                    return subDomainPrefix + ".spark.com";//american singles community sites belong to the same IM domain (date.ca, date.co.uk, americansingles.com) 16762
                case (int)WebConstants.COMMUNITY_ID.Cupid:
                    return subDomainPrefix + ".cupid.co.il";
                default:
                    return subDomainPrefix + "." + g.Brand.Site.Name;//cupid.co.il, glimpse.com, and collegeluv.com are each their own domains too.
            }



        }
        // The bedrock sites correspond to 'domains' in the userplane environment
        // an exception to this case is development, where individual developer workstations
        // will be routed through the dev.site.com XML server.
        //    private string getDomainIDForUserplane()
        //    {	
        //        // As of 9/7/05, g.Brand.Site.DefaultHost returns either www or dev
        //        // In order to test on stage, we need to leave stage in the URL - related bug: 14779)
        //        string subDomainPrefix = g.Brand.Site.DefaultHost;

        //        string requestUrlBeg = string.Empty;
        //        if(Request.Url.Host.ToLower().Length >= 8)
        //            requestUrlBeg = Request.Url.Host.ToLower().Substring(0, 8);

        //        if(requestUrlBeg != string.Empty && requestUrlBeg != null)
        //        {			
        //            if (requestUrlBeg.Substring(0,3).IndexOf("stg") > -1 || requestUrlBeg.Substring(0,3).IndexOf("dev") > -1) 
        //            {
        //                // To support the new stage env that starts with stg, we need to be able to accept this format
        //                // stg## --> Examples: stg01, stg02, etc.
        //                subDomainPrefix = requestUrlBeg.Substring(0, 5);
        //            }
        //            else if (requestUrlBeg.IndexOf("preprod") > -1)
        //            {
        //                // Support for preprod environment
        //                subDomainPrefix = "preprod";
        //            }
        //#if DEBUG
        //            else if (requestUrlBeg.IndexOf("bhd-mcho") > -1)
        //            {
        //                subDomainPrefix = "bhd-mcho";
        //            }
        //#endif
        //        }

        //        // currently there is no site specific loopholes in this logic, so defaulthost + sitename is fine.
        //        // Above comment was true until now (2.21.2008), JDate and JDate.co.uk need to be in the same domain.  Since JDFR &
        //        // JDCOIL have the chat rooms disabled, we don't have to worry these 2 cases.
        //        if(g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
        //            return subDomainPrefix + ".jdate.com";
        //        else
        //            return subDomainPrefix + "." + g.Brand.Site.Name.ToLower();
        //    }

        /// <summary>
        /// The locale parameter specifies to userplane which locale XML file to load on their end.
        /// The locale XML file contains the text elements that appear in Chat app. 
        /// As of 9/21/05 there are only two locale files: english-spark.xml and english-glimpse.xml
        /// (they differ in their strButtonAddFriend, strButtonRemoveFriend, strTtAddFriendButton (tooltip)
        /// text only.
        /// </summary>
        /// <returns></returns>
        private string GetLocale()
        {
            string locale = "english-spark";

            if ((int)WebConstants.COMMUNITY_ID.Glimpse == g.Brand.Site.Community.CommunityID)
            {
                locale = "english-glimpse";
            }
            else if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                locale = "hebrew";
            }
            else if (g.Brand.Site.LanguageID == (int)Matchnet.Language.French)
            {
                locale = "french-spark";
            }


            return locale.ToLower();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
