<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="upChat.ascx.cs" Inherits="Matchnet.Web.Applications.Chat.upChat"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/Ui/Advertising/AdUnit.ascx" %>
<asp:PlaceHolder ID="phFrameset" runat="server">
    <frameset rows="48,*" frameborder="NO" border="0" framespacing="0"> 
	<frame name="topFrame" scrolling="NO" noresize src="upChat.aspx?frame=top">
	<frame name="mainFrame" src="upChat.aspx?frame=main" scrolling="no">
	<noframes><body bgcolor="#FFFFFF" text="#000000">
		Page requires frames turned on in Browser preferences.
		</body></noframes>
	</frameset>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phTopFrame" runat="server" Visible="False">
    <body class="imBody">
        <div class="chatHeader" style="height: 48px">
            <mn:Image ID="logo" FileName="im_header_left.gif" runat="server" />
        </div>
    </body>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phMainFrame" runat="server" Visible="False">
    <body class="imBody" onload="initOnlineChecker(true)">
        <asp:PlaceHolder ID="phHebrewSupport" runat="server" Visible="False">
            <!-- for Hebrew DHTML overlay support. Used by CreateLayer method -->

            <script type="text/javascript" src="javascript/userplaneChatHebrewDHTMLsupport.js"></script>

            <div id="dynlayer" style="border-right: 0px solid; border-top: 0px solid; left: 0px;
                border-left: 0px solid; border-bottom: 0px solid; position: absolute; top: 0px">
            </div>

            <script type="text/vbscript" language="VBScript">
		<!--
		'Map VB script events to the JavaScript method - Netscape will ignore this... 
		'Since FSCommand fires a VB event under ActiveX, we respond here 
		Sub ch_FSCommand(ByVal command, ByVal args)
			call ch_DoFSCommand(command, args)
		end sub
		-->
            </script>

        </asp:PlaceHolder>
        <!-- Begin checkIM -->
        <!-- Need background check of IM and DTHML notifier because a) main window 
	might be closed and b) main window is usually not visible 16481 -->
        <div id="IMAlerts" style="z-index: 1; position: absolute; top: 100px">
        </div>
        <iframe tabindex="-1" name="frm_checkim" src="/Applications/InstantMessenger/CheckIM.aspx"
            frameborder="0" scrolling="NO" width="1" height="1"></iframe>
        <!--reject IM - for compatibility with https - make sure to use relative path to something secure-->
        <iframe name="frm_rejectim" src="/img/trans.gif" frameborder="0" scrolling="NO" width="1"
            height="1" tabindex="-1"></iframe>
        <!--remove IM invitation - for compatibility with https - make sure to use relative path to something secure-->
        <iframe name="frm_removeim" src="/img/trans.gif" frameborder="0" scrolling="NO" width="1"
            height="1" tabindex="-1"></iframe>
        <!--update mol status - for compatibility with https - make sure to use relative path to something secure-->
        <iframe name="frm_updateMOL" src="/img/trans.gif" frameborder="0" scrolling="NO"
            width="1" height="1" tabindex="-1"></iframe>
        <!--sessionKeepAlive - for compatibility with https - make sure to use relative path to something secure-->
        <iframe name="frm_sessionKeepAlive" src="/img/trans.gif" frameborder="0" scrolling="NO"
            width="1" height="1" tabindex="-1"></iframe>
        <!-- End checkIM -->

        <script type="text/javascript">
		<asp:Literal Runat="server" ID="jsOwnProfileLink" />
		<asp:Literal Runat="server" ID="jsDestinationProfileLink" />
		<asp:Literal Runat="server" ID="jsOwnMemberID" />
        </script>

        <script type="text/javascript" src='<%=ChatJavascript%>'></script>

        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
            width="100%" height="100%" name="ch" id="ch" align="" viewastext>
            <param name="movie" value="http://<%= SwfServer %>/<%= ApplicationName %>/ch.swf">
            <param name="quality" value="best">
            <param name="scale" value="noscale">
            <param name="bgcolor" value="#FFFFFF">
            <param name="menu" value="0">
            <param name="salign" value="LT">
            <param name="FlashVars" value="<%= FlashVars %>">
            <param name="AllowScriptAccess" value="always">
            <param name="wmode" value="transparent" />
            <!--to allow DHTML IM alert to appear on top 16481 -->
            <embed src="http://<%= SwfServer %>/<%= ApplicationName %>/ch.swf" quality="best"
                scale="noscale" bgcolor="#FFFFFF" menu="0" width="100%" height="500px" name="ch"
                align="top" salign="LT" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"
                flashvars="<%= FlashVars %>" allowscriptaccess="always" wmode="transparent">
		</embed>
        </object>
        <br />
        <!-- COPYRIGHT Userplane 2004 (http://www.userplane.com) -->
        <!-- CS version 1.8.1 -->
    </body>
</asp:PlaceHolder>
</html>