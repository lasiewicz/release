using Matchnet.Web.Framework;
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Applications.MemberProfile;

namespace Matchnet.Web.Applications.SendToFriend
{

	/// <summary>
	///		Summary description for MessageSent.
	/// </summary>
	public class MessageSent : FrameworkControl
	{
		protected Matchnet.Web.Framework.Txt txtTease;
		protected Matchnet.Web.Framework.Image GrnMessage;
		protected Matchnet.Web.Framework.Txt txtYourMessageHasBeenSent;
		protected Matchnet.Web.Framework.Txt txtMemberMessage;
		protected System.Web.UI.WebControls.Label lblSubscribeNow;
		protected Matchnet.Web.Framework.Title ttlMessage;
		protected System.Web.UI.WebControls.HyperLink backToProfileLink;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Request["Error"] != null && !Request["Error"].Equals(String.Empty)) 
			{
				txtYourMessageHasBeenSent.ResourceConstant = "TXT_YOUR_MESSAGE_FAILED_TO_SEND";
				txtYourMessageHasBeenSent.TitleResourceConstant = "TTL_YOUR_MESSAGE_FAILED_TO_SEND";
			}
			lblSubscribeNow.Text = string.Format(g.GetResource("TXT_SUBSCRIBE_MESSAGE", this), "<a href='/Applications/Subscription/Subscribe.aspx'>" + g.GetResource("TXT_SUBSCRIBE_NOW", this) + "</a>");

			string memberId = Request.Params.Get("MemberId");

			if(memberId != null && memberId.Trim().Length > 0) 
			{
                backToProfileLink.NavigateUrl = "/Applications/MemberProfile/ViewProfile.aspx?MemberId=" + memberId + ProfileDisplayHelper.GetURLParamsNeededByNextProfile();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
