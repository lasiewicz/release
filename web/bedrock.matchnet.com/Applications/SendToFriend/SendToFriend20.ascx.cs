﻿using System;
using System.Text.RegularExpressions;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.MemberProfile;

namespace Matchnet.Web.Applications.SendToFriend
{
    public partial class SendToFriend20 : FrameworkControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //	Set the Member for the MiniProfile component
                string sMemberID = Request["MemberID"];
                if (sMemberID != null && string.Empty != sMemberID)
                {
                    int memberID = int.Parse(sMemberID);
                    string controlPath = "../../Framework/Ui/BasicElements/MiniProfile20.ascx";

                    Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID,
                            MemberLoadFlags.None);

                    MiniProfile20 control = (MiniProfile20)LoadControl(controlPath);

                    control.Member = member;
                    control.Transparency = false;

                    this.miniProfile.Controls.Add(control);

                    if (g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) > 0)
                    {
                        if (!Page.IsPostBack)
                        {
                            member = MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                                MemberLoadFlags.None);

                            this.txtYourName.Text = member.GetUserName(_g.Brand);
                            this.txtYourEmail.Text = member.EmailAddress;
                        }
                    }

                    if (!Page.IsPostBack)
                    {
                        this.txtSubject.Text = g.GetTargetResource("AN_INTERESTING_PAGE", this).Replace("%s", g.Brand.Uri);
                        this.txtMessage.Text = g.GetTargetResource("HEY_CHECK_OUT_THIS_LINK__518575", this).Replace("%s", g.Brand.Uri);
                        this.instructionsLiteral.Text = g.GetTargetResource("SEND_INSTRUCTIONS", this).Replace("%s", g.Brand.Uri);
                    }

                    this.fefControl.ElementClientId = this.txtFriendName.ClientID;
                }
            }
            catch (Exception ex) { g.ProcessException(ex); }


            string reminder = g.GetResource(TxtReminder.ResourceConstant, this);
            if (String.IsNullOrEmpty(reminder) || reminder.ToLower().IndexOf("_resources") >= 0)
            {
                TxtReminder.Visible = false;
            }
        }

        private bool validateRequest()
        {
            bool isValidRequest = true;

            if (Request[this.txtFriendName.UniqueID] == "")
            {
                g.Notification.AddError("FRIENDS_NAME_IS_REQUIRED");
                isValidRequest = false;
            }
            else if (Request[this.txtFriendEmail.UniqueID] == "")
            {
                g.Notification.AddError("FRIENDS_EMAIL_IS_REQUIRED");
                isValidRequest = false;
            }
            else if (!this.isValidEmail(Request[this.txtFriendEmail.UniqueID]))
            {
                g.Notification.AddError("FRIENDS_EMAIL_IS_NOT_A_VALID_EMAIL_ADDRESS");
                isValidRequest = false;
            }
            else if (Request[this.txtYourName.UniqueID] == "")
            {
                g.Notification.AddError("YOUR_NAME_IS_REQUIRED");
                isValidRequest = false;
            }
            else if (Request[this.txtYourEmail.UniqueID] == "")
            {
                g.Notification.AddError("YOUR_EMAIL_ADDRESS_IS_REQUIRED");
                isValidRequest = false;
            }
            else if (!this.isValidEmail(Request[this.txtYourEmail.UniqueID]))
            {
                g.Notification.AddError("YOUR_EMAIL_ADDRESS_IS_NOT_A_VALID_EMAIL_ADDRESS");
                isValidRequest = false;
            }
            else if (Request[this.txtSubject.UniqueID] == "")
            {
                g.Notification.AddError("SUBJECT_IS_REQUIRED");
                isValidRequest = false;
            }
            else if (Request[this.txtMessage.UniqueID] == "" || txtMessage.Text.Length > 200)
            {
                g.Notification.AddError("MESSAGE_IS_REQUIRED");
                isValidRequest = false;
            }

            return isValidRequest;
        }

        private bool isValidEmail(string anEmailAddress)
        {
            bool isValidEmail = true;
            Regex emailPattern = new Regex("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$");

            if (!emailPattern.IsMatch(anEmailAddress))
            {
                isValidEmail = false;
            }

            return isValidEmail;
        }
        private void btnSendToFriend_Click(object sender, System.EventArgs e)
        {
            string redirectURL = "/Applications/SendToFriend/MessageSent.aspx?MemberID=" + Request["MemberID"];

            //adding on additional URL params needed for tabbed profile pages to generate Next Profile link
            redirectURL += ProfileDisplayHelper.GetURLParamsNeededByNextProfile();

            try
            {

                bool isValidRequest = this.validateRequest();

                if (!isValidRequest)
                {
                    return;
                }

                string friendName = txtFriendName.Text;
                string friendEmail = txtFriendEmail.Text;
                string yourEmail = txtYourEmail.Text;
                string subject = txtSubject.Text;
                string theMessage = txtMessage.Text;
                int memberID = Convert.ToInt32(Request["MemberID"]);

                ExternalMailSA.Instance.SendMemberToFriend(friendName, friendEmail, yourEmail, memberID, subject, theMessage, g.Brand.BrandID, g.Member.MemberID);
            }
            catch (Exception ex)
            {
                redirectURL += "&Error=true";
                g.ProcessException(ex);
            }

            g.Transfer(redirectURL);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSendToFriend.Click += new System.EventHandler(this.btnSendToFriend_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

    }
}