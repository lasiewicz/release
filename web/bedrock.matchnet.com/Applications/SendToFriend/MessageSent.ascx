<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MessageSent.ascx.cs" Inherits="Matchnet.Web.Applications.SendToFriend.MessageSent" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div id="rightNew">
	<mn:Title runat="server" id="ttlMessage" ResourceConstant="TXT_MESSAGE" ImageName="hdr_send_to_friend_confirm.gif" CommunitiesForImage="CI;" />
	<p><mn:Image id="GrnMessage" ResourceConstant="ALT_MESSAGE_SENT" titleResourceConstant="TTL_YOUR_MESSAGE_HAS_BEEN_SENT"
			FileName="icon_grn_sendtofriend.gif" runat="server" vspace="0" align="left"
			border="0" hspace="3" cssClass="sendToFriendImage" />
			<h2>&nbsp;<mn:txt id="txtYourMessageHasBeenSent" runat="server" ResourceConstant="TXT_YOUR_MESSAGE_HAS_BEEN_SENT" /></h2><br />
			<div style="margin-left:40px;margin-right:20px;"><mn:txt id="txtMemberMessage" Runat="server" ResourceConstant="TXT_MEMBER_MESSAGE" /><br />
			</div><br />
		<!--<asp:Label id="lblSubscribeNow" Runat="Server" /><br />
		<br />-->
		<asp:HyperLink Runat="server" ID="backToProfileLink">
			<mn:txt id="txtBackToProfile" runat="server" titleResourceConstant="TTL_BACK_TO_PROFILE"
				ResourceConstant="TXT_BACK_TO_PROFILE" />
		</asp:HyperLink></p>
		<p style="margin: 0px;">&nbsp;</p><!-- for ie mirrored copy/float bug -->
</div> 