﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.SendToFriend
{
    public class SendToFriendHelper
    {
        static string ELIPSES="...";
        #region Static Methods
        public static string Truncate(string s, int maxlength) {
            if (String.IsNullOrEmpty(s) || maxlength < 1 || s.Length <= maxlength) return s;
            int newMaxLength = (maxlength <= ELIPSES.Length) ? maxlength : maxlength - ELIPSES.Length;
            string truncated = s.Substring(0, newMaxLength)+ELIPSES;
            return truncated;
        }
        
        public static string TruncateAndPreserveWords(string s, int maxlength) {
            if (String.IsNullOrEmpty(s) || maxlength < 1 || s.Length <= maxlength) return s;
            string truncated = Truncate(s, maxlength);
            int indexofLastSpace = truncated.TrimEnd(new char [] {' '}).LastIndexOf(' ');
            truncated = (indexofLastSpace > 0)?truncated.Substring(0, indexofLastSpace)+ELIPSES:truncated;
            return truncated;
        }
        #endregion

        public static void Main() {
            string s1="This is not a very long string.";
            string s2="This is a very long string.".PadRight(1000,'a');

            string b1 = Truncate(s1, 0);
            string r1=Truncate(s1,13);
            string r2 = Truncate(s1, 20);
            string r3 = Truncate(s1, 100);
            string r4 = Truncate(s2, 13);
            string r5 = Truncate(s2, 100);

            string b1b = TruncateAndPreserveWords(s1, 0);
            string r1b = TruncateAndPreserveWords(s1, 13);
            string r2b = TruncateAndPreserveWords(s1, 20);
            string r3b = TruncateAndPreserveWords(s1, 100);
            string r4b = TruncateAndPreserveWords(s2, 13);
            string r5b = TruncateAndPreserveWords(s2, 100);

        }
    }

}
