﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using MPVO = Matchnet.Purchase.ValueObjects;

using Spark.Common.UPS;
using Spark.Common.Adapter;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem
{
    /// <summary>
    /// For handling Color Code Purchase
    /// 
    /// </summary>
    public class ColorCodeCompPurchasePage : FrameworkControl, IPaymentUIJSON
    {
        private int version = Constants.NULL_INT;
        private int templateID = Constants.NULL_INT;
        private PaymentType paymentType;
        private int upsLegacyDataID = Constants.NULL_INT;
        private int UPSGlobalLogID = Constants.NULL_INT;
        private bool saveLegacyData = true;
        public Dictionary<String, String> OrderAttributes { get; set; }
        public bool IsSubscriptionConfirmationEmailEnabled { get; set; }
        private PremiumType _premiumType = PremiumType.None;

        #region IPaymentUIJSON Members
        PremiumType IPaymentUIJSON.PremiumType
        {
            get
            {
                return _premiumType;
            }
            set
            {
                _premiumType = value;
            }
        }

        bool IPaymentUIJSON.SaveLegacyData
        {
            get
            {
                return saveLegacyData;
            }
            set
            {
                saveLegacyData = true;
            }
        }


        int IPaymentUIJSON.Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        int IPaymentUIJSON.TemplateID
        {
            get
            {
                return templateID;
            }
            set
            {
                templateID = value;
            }
        }

        PaymentType IPaymentUIJSON.PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        int IPaymentUIJSON.UPSLegacyDataID
        {
            get
            {
                return upsLegacyDataID;
            }
            set
            {
                upsLegacyDataID = value;
            }
        }

        int IPaymentUIJSON.UPSGlobalLogID
        {
            get
            {
                return UPSGlobalLogID;
            }
            set
            {
                UPSGlobalLogID = value;
            }
        }		
		
        object IPaymentUIJSON.GetJSONObject()
        { 
            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            if (paymentType == null)
                throw new Exception("PaymentType cannot be null.");

            if (upsLegacyDataID == Constants.NULL_INT)
                throw new Exception("UPS Legacy Data ID cannot be null.");

            Dictionary<string, string> colDataAttributes = new Dictionary<string, string>();			

            try
            {
                if (g.TargetMember == null)
                    throw new Exception("Member cannot null.");

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ENTER PaymentUIConnector jump page", null, 1, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);			

                int memberID = g.TargetMember.MemberID;

                PlanCollection originalCollection = PlanSA.Instance.GetPlans(g.TargetBrand.BrandID, PlanType.ALaCarte, PaymentType.CreditCard);

                if (originalCollection == null)
                    throw new Exception("Plancollection cannot be null.");

                List<MPVO.Plan> planCollection = new List<Purchase.ValueObjects.Plan>();

                var colorCodePlans = (from MPVO.Plan p in originalCollection
                                     where p.PremiumTypeMask == PremiumType.ColorCode
                                     orderby p.UpdateDate descending
                                     select p).Take(1).ToList();

                if (colorCodePlans.Count != 1)
                {
                    g.Notification.AddErrorString("No Color Code ALC plan found for " + g.Brand.Site.Name);
                    throw new Exception("No Color Code ALC plan found for " + g.Brand.Site.Name);
                }

                Matchnet.Purchase.ValueObjects.Plan plan = colorCodePlans[0];

                PaymentJson payment = new PaymentJson();

                payment.UPSLegacyDataID = upsLegacyDataID;
                payment.UPSGlobalLogID = UPSGlobalLogID;				
                payment.GetCustomerPaymentProfile = 1;
                payment.Version = version;
                payment.CallingSystemID = g.TargetBrand.Site.SiteID;
                payment.TemplateID = templateID;
                payment.TimeStamp = DateTime.Now;
                payment.Data.Navigation.CancelURL = "http://" + g.Page.Request.Url.Host + "/Applications/MemberServices/MemberServices.aspx";
                payment.Data.Navigation.ReturnURL = "http://" + g.Page.Request.Url.Host;
                payment.Data.Navigation.ConfirmationURL = "https://" + g.Page.Request.Url.Host + "/Applications/Subscription/SubscriptionConfirmation.aspx";
                payment.Data.Navigation.DestinationURL = (Context.Request["DestinationURL"] != null) ? Convert.ToString(Context.Request["DestinationURL"]) : string.Empty;

                // Member Info
                payment.Data.MemberInfo.CustomerID = g.TargetMemberID;
                payment.Data.MemberInfo.RegionID = g.TargetMember.GetAttributeInt(g.TargetBrand, "RegionID");
                if (payment.Data.MemberInfo.RegionID == Constants.NULL_INT
                    || payment.Data.MemberInfo.RegionID < 0)
                {
                    // There must be a valid RegionID to price the cart so get it from the database if the cache does not have a valid RegionID  
                    Member.ServiceAdapters.Member refreshedMember = MemberSA.Instance.GetMember(35, MemberLoadFlags.IngoreSACache);
                    payment.Data.MemberInfo.RegionID = refreshedMember.GetAttributeInt(g.TargetBrand, "RegionID");
                }
                payment.Data.MemberInfo.Language = ((Language)Enum.Parse(typeof(Language), g.TargetBrand.Site.LanguageID.ToString())).ToString();
                
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID, (int)Language.English);
                if (regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                    payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation.Trim().ToUpper();

                //// Omniture Variables
                PaymentJsonArray omnitureVariables = new PaymentJsonArray();

                omnitureVariables.Add("pageName", "ColorCode - Comp Purchase");
                omnitureVariables.Add("prop17", OmnitureHelper.OmnitureHelper.GetProfileCompetionPercentage(_g.TargetMember, _g.TargetBrand));
                omnitureVariables.Add("prop18", OmnitureHelper.OmnitureHelper.GetGender(_g.TargetMember.GetAttributeInt(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                omnitureVariables.Add("prop19", OmnitureHelper.OmnitureHelper.GetAge(_g.TargetMember.GetAttributeDate(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());
                if (_g.TargetBrand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
                {
                    omnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "JDateEthnicity", "JDateEthnicity"));
                }
                else
                {
                    omnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "Ethnicity", "Ethnicity"));
                }
                omnitureVariables.Add("prop21", OmnitureHelper.OmnitureHelper.GetRegionString(_g.TargetMember, _g.TargetBrand, _g.TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                omnitureVariables.Add("prop23", _g.TargetMember.MemberID.ToString());
                omnitureVariables.Add("events", "purchase");
                omnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (_g.TargetMember.MemberID % 10).ToString()));
                omnitureVariables.Add("prop29", g.AnalyticsOmniture.Prop29);

                omnitureVariables.Add("evar11", g.Session[WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID]);
                omnitureVariables.Add("evar12", g.Session[WebConstants.SESSION_PROPERTY_NAME_EID]);
                omnitureVariables.Add("evar13", g.Session[WebConstants.SESSION_PROPERTY_NAME_LUGGAGE]);

                payment.Data.OmnitureVariables.Add(omnitureVariables);

                // Legacy Data Info
                if (g.Member != null && g.Member.MemberID != g.TargetMemberID)
                {
                    payment.Data.LegacyDataInfo.AdminMemberID = g.Member.MemberID;
                }
                
                payment.Data.LegacyDataInfo.Member = g.TargetMemberID;
                payment.Data.LegacyDataInfo.BrandID = g.TargetBrand.BrandID;
                payment.Data.LegacyDataInfo.SiteID = g.TargetBrand.Site.SiteID;

                PaymentJsonPackage package;
                package = new PaymentJsonPackage();

                package.ID = plan.PlanID;
                package.Description = "";
                package.SytemID = g.TargetBrand.Site.SiteID;
                package.StartDate = DateTime.MinValue;
                package.ExpiredDate = DateTime.MinValue;
                package.CurrencyType = PaymentUIConnector.GetCurrencyAbbreviation(plan.CurrencyType);

                package.Items.Add("InitialCostPerDuration", plan.InitialCost);
                package.Items.Add("PlanID", plan.PlanID);
                package.Items.Add("CurrencyType", PaymentUIConnector.GetCurrencyAbbreviation(plan.CurrencyType));
                package.Items.Add("PaymentTypeMask", plan.PaymentTypeMask);
                package.Items.Add("PurchaseMode", PurchaseMode.New.ToString());
                package.Items.Add("PremiumType", plan.PremiumTypeMask.ToString());

                payment.Data.Packages.Add(package);

                colDataAttributes.Add("UPSLegacyDataID", Convert.ToString(upsLegacyDataID));
                colDataAttributes.Add("CustomerID", Convert.ToString(memberID));
                colDataAttributes.Add("CallingSystemID", Convert.ToString(g.TargetBrand.Site.SiteID));
                colDataAttributes.Add("TransactionType", "24");
                //colDataAttributes.Add("PRTID", OrderAttributes["PRTID"]);
                //colDataAttributes.Add("SRID", OrderAttributes["SRID"]);
                //colDataAttributes.Add("PricingGroupID", OrderAttributes["PricingGroupID"]);
                colDataAttributes.Add("RegionID", Convert.ToString(payment.Data.MemberInfo.RegionID));
                colDataAttributes.Add("IPAddress", payment.Data.MemberInfo.IPAddress);
                colDataAttributes.Add("TemplateID", Convert.ToString(payment.TemplateID));
                colDataAttributes.Add("CancelURL", payment.Data.Navigation.CancelURL);
                colDataAttributes.Add("ConfirmationURL", payment.Data.Navigation.ConfirmationURL);
                colDataAttributes.Add("DestinationURL", payment.Data.Navigation.DestinationURL);
                colDataAttributes.Add("PlanIDList", Convert.ToString(plan.PlanID));						

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "NO ERROR", null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.OK, DateTime.Now);

                return payment;
            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ERROR DETECTED: " + ex.Message, null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR, DateTime.Now);		

                throw new Exception("Error in building JSON for PaymentUI. MemberID:" +
                    g.TargetMember.MemberID, ex);
            }
            finally
            {
                //TraceLogConnectorServiceWebAdapter.GetProxyInstance().InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "EXIT PaymentUIConnector jump page", null, 0, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }
        }

        #endregion
    }
}
