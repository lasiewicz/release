﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Matchnet.Web.Framework;
using Matchnet.Purchase.ValueObjects;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters.TokenReplacement;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;

using Spark.Common.UPS;
using Matchnet.Member.ServiceAdapters;
using System.Globalization;

namespace Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem
{
    public class SubscriptionPage : FrameworkControl, IPaymentUIJSON
    {
        private int version = Constants.NULL_INT;
        private int templateID = Constants.NULL_INT;
        private PaymentType paymentType;
        private int upsLegacyDataID = Constants.NULL_INT;
        private int UPSGlobalLogID = Constants.NULL_INT;
        private bool saveLegacyData = true;

        public HttpContext Context { get; set; }
        public Dictionary<String,String> OrderAttributes { get; set; }
        public int PromoID { get; set; }
        public int SelectedPlanID { get; set; }
        public bool IsSubscriptionConfirmationEmailEnabled { get; set; }
        private PremiumType _premiumType = PremiumType.None;

        #region IPaymentUIJSON Members
        PremiumType IPaymentUIJSON.PremiumType
        {
            get
            {
                return _premiumType;
            }
            set
            {
                _premiumType = value;
            }
        }

        bool IPaymentUIJSON.SaveLegacyData
        {
            get
            {
                return saveLegacyData;
            }
            set
            {
                saveLegacyData = true;
            }
        }

        int IPaymentUIJSON.Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        int IPaymentUIJSON.TemplateID
        {
            get
            {
                return templateID;
            }
            set
            {
                templateID = value;
            }
        }

        PaymentType IPaymentUIJSON.PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        int IPaymentUIJSON.UPSLegacyDataID
        {
            get
            {
                return upsLegacyDataID;
            }
            set
            {
                upsLegacyDataID = value;
            }
        }

        int IPaymentUIJSON.UPSGlobalLogID
        {
            get
            {
                return UPSGlobalLogID;
            }
            set
            {
                UPSGlobalLogID = value;
            }
        }		
		
        object IPaymentUIJSON.GetJSONObject()
        {
            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT) 
                throw new Exception("TemplateID cannot be null.");

            if (paymentType == null)
                throw new Exception("PaymentType cannot be null.");

            if (upsLegacyDataID == Constants.NULL_INT)
                throw new Exception("UPS Legacy Data ID cannot be null.");

            try
            {
                if (g.Member == null)
                    throw new Exception("Member cannot null.");

                int memberID = g.Member.MemberID;

                Promo promo;

                if (PromoID != 0)
                    promo = PromoEngineSA.Instance.GetPromoByID(PromoID, g.Brand.BrandID, memberID);
                else if (g.PromoPlan.Promo != null)
                {
                    if (g.PromoPlan.FailedValidation)
                    {
                        g.Notification.Clear();
                        g.Notification.AddError("PROMOPLAN_EXPIRED");
                        throw new Exception("Promo Plan Expired.");
                    }
                    else
                    {
                        promo = g.PromoPlan.Promo;
                    }
                }
                else if (Context.Request["PromoID"] == null)
                    promo = PromoEngineSA.Instance.GetMemberPromo(memberID, g.Brand, paymentType);
                else
                    promo = PromoEngineSA.Instance.GetPromoByID(Convert.ToInt32(Context.Request["PromoID"]), g.Brand.BrandID, memberID);

                if (promo == null)
                    throw new Exception("Promo cannot be null.");

                if (promo.PromoPlans == null)
                    throw new Exception("Promo Plan(s) cannot be null.");

                PaymentJson payment = new PaymentJson();

                payment.UPSLegacyDataID = upsLegacyDataID;
                payment.GetCustomerPaymentProfile = (paymentType == PaymentType.CreditCard) ? 1 : 0;
                payment.Version = version;
                payment.CallingSystemID = g.Brand.Site.SiteID;
                payment.TemplateID = templateID;
                payment.TimeStamp = DateTime.Now;
                if (g.Page.Request.UrlReferrer != null)
                    payment.Data.Navigation.CancelURL = g.Page.Request.UrlReferrer.AbsoluteUri;
                else
                    payment.Data.Navigation.CancelURL = "http://" + g.Page.Request.Url.Host;
                payment.Data.Navigation.ReturnURL = "http://" + g.Page.Request.Url.Host;
                payment.Data.Navigation.ConfirmationURL = "https://" + g.Page.Request.Url.Host + "/Applications/Subscription/SubscriptionConfirmation.aspx";
                payment.Data.Navigation.DestinationURL = (Context.Request["DestinationURL"] != null) ? Convert.ToString(Context.Request["DestinationURL"]) : string.Empty;

                // Order Attributes
                OrderAttributes = new Dictionary<string, string>();

                OrderAttributes.Add("PRTID", (Context.Request["prtid"] != null) ? Convert.ToString(Context.Request["prtid"]) : string.Empty);
                OrderAttributes.Add("SRID", (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]) : string.Empty);
                OrderAttributes.Add("PricingGroupID", promo.PromoID.ToString());

                foreach(string key in OrderAttributes.Keys)
                {
                    if (OrderAttributes[key] != string.Empty)
                        payment.Data.OrderAttributes += key + "=" + OrderAttributes[key] + ",";
                }

                if (payment.Data.OrderAttributes.Length > 0)
                    payment.Data.OrderAttributes = payment.Data.OrderAttributes.TrimEnd(',');
                
                // Member Info
                payment.Data.MemberInfo.CustomerID = g.TargetMemberID;
                payment.Data.MemberInfo.RegionID = g.Member.GetAttributeInt(g.Brand, "RegionID");
                if (payment.Data.MemberInfo.RegionID == Constants.NULL_INT
                    || payment.Data.MemberInfo.RegionID < 0)
                {
                    // There must be a valid RegionID to price the cart so get it from the database if the cache does not have a valid RegionID  
                    Member.ServiceAdapters.Member refreshedMember = MemberSA.Instance.GetMember(35, MemberLoadFlags.IngoreSACache);
                    payment.Data.MemberInfo.RegionID = refreshedMember.GetAttributeInt(g.TargetBrand, "RegionID");
                }
                payment.Data.MemberInfo.Language = ((Language)Enum.Parse(typeof(Language), g.Brand.Site.LanguageID.ToString())).ToString();
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID, (int)Language.English);
                if (regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                    payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation.Trim().ToUpper();

                // Promo Info
                PaymentJsonArray promoInfo = new PaymentJsonArray();
                promoInfo.Add("PromoID", promo.PromoID);
                promoInfo.Add("PageTitle", promo.PageTitle);
                var promoEndDateLocalTime = promo.EndDate.ToUniversalTime().AddHours(g.Brand.Site.GMTOffset);

                var promoEndDateLocalTimeFormatted = promoEndDateLocalTime.ToString("d",
                    g.Brand.Site.CultureInfo.DateTimeFormat);
                System.Diagnostics.EventLog.WriteEntry("bedrock", "promoEndDateLocalTimeFormatted:" + promoEndDateLocalTimeFormatted);
                promoInfo.Add("EndDate", promoEndDateLocalTimeFormatted);
                payment.Data.PromoInfo.Add(promoInfo);

                // Omniture Variables
                PaymentJsonArray omnitureVariables = new PaymentJsonArray();

                omnitureVariables.Add("pageName", "Subscribe - SubscribeNew");
                omnitureVariables.Add("prop17", Matchnet.OmnitureHelper.OmnitureHelper.GetProfileCompetionPercentage(_g.Member, _g.Brand));
                omnitureVariables.Add("prop18", Matchnet.OmnitureHelper.OmnitureHelper.GetGender(_g.Member.GetAttributeInt(_g.Brand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                omnitureVariables.Add("prop19", Matchnet.OmnitureHelper.OmnitureHelper.GetAge(_g.Member.GetAttributeDate(_g.Brand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());
                if (_g.Brand.Site.Community.CommunityID == (int)Matchnet.Web.Framework.WebConstants.COMMUNITY_ID.JDate)
                {
                    omnitureVariables.Add("prop20", Matchnet.Web.Applications.MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.Member, _g, "JDateEthnicity", "JDateEthnicity"));
                }
                else
                {
                    omnitureVariables.Add("prop20", Matchnet.Web.Applications.MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.Member, _g, "Ethnicity", "Ethnicity"));
                }
                omnitureVariables.Add("prop21", Matchnet.OmnitureHelper.OmnitureHelper.GetRegionString(_g.Member, _g.Brand, _g.Brand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                omnitureVariables.Add("prop23", _g.Member.MemberID.ToString());
                omnitureVariables.Add("events", "event9");
                omnitureVariables.Add("eVar3", GetPlanIDListForOmniture(promo.PromoPlans));
                omnitureVariables.Add("eVar4", promo.PromoID);
                omnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (_g.Member.MemberID % 10).ToString()));

                omnitureVariables.Add("evar11", g.Session[WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID]);
                omnitureVariables.Add("evar12", g.Session[WebConstants.SESSION_PROPERTY_NAME_EID]);
                omnitureVariables.Add("evar13", g.Session[WebConstants.SESSION_PROPERTY_NAME_LUGGAGE]);
                
                payment.Data.OmnitureVariables.Add(omnitureVariables);

                // Legacy Data Info
                payment.Data.LegacyDataInfo.AdminMemberID = g.Member.MemberID;
                payment.Data.LegacyDataInfo.Member = g.TargetMemberID;
                payment.Data.LegacyDataInfo.BrandID = g.Brand.BrandID;
                payment.Data.LegacyDataInfo.SiteID = g.Brand.Site.SiteID;

                // Column Header Template Collection
                foreach (HeaderResourceTemplate template in promo.ColHeaderResourceTemplateCollection)
                {
                    PaymentJsonArray headerTemplate = new PaymentJsonArray();

                    string content = ResourceTemplateSA.Instance.GetResource(template.ResourceTemplateID, new ColumnHeaderTokenReplacer());
                    headerTemplate.Add("ResourceTemplateContent", content);
                    headerTemplate.Add("HeaderNumber", template.HeaderNumber);

                    payment.Data.ColHeaderTemplates.Add(headerTemplate);
                }

                // Row Header Template Collection
                foreach (RowHeaderResourceTemplate template in promo.RowHeaderResourceTemplateCollection)
                {
                    PaymentJsonArray headerTemplate = new PaymentJsonArray();

                    string content = ResourceTemplateSA.Instance.GetResource(template.ResourceTemplateID, new RowHeaderTokenReplacer(template.Duration));
                    headerTemplate.Add("ResourceTemplateContent", content);
                    headerTemplate.Add("HeaderNumber", template.HeaderNumber);

                    payment.Data.RowHeaderTemplates.Add(headerTemplate);
                }


                // Promo Plans
                foreach (PromoPlan promoPlan in promo.PromoPlans)
                {
                    if (SelectedPlanID > 0)
                    {
                        if (SelectedPlanID != promoPlan.PlanID)
                            continue;
                    }

                    PaymentJsonPackage package = new PaymentJsonPackage();

                    package.ID = promoPlan.PlanID;
                    package.Description = "";
                    package.SytemID = g.Brand.Site.SiteID;
                    package.StartDate = promoPlan.StartDate;
                    package.ExpiredDate = promoPlan.EndDate;
                    package.CurrencyType = PaymentUIConnector.GetCurrencyAbbreviation(promoPlan.CurrencyType);

                    string content = ResourceTemplateSA.Instance.GetResource(promoPlan.ResourceTemplateID, new PlanTokenReplacer(promoPlan, 2));
                    package.Items.Add("ResourceTemplateContent", content);
                    package.Items.Add("EnabledForMember", promoPlan.EnabledForMember.ToString());
                    package.Items.Add("InitialCostPerDuration", promoPlan.InitialCostPerDuration);
                    package.Items.Add("ListOrder", promoPlan.ListOrder);
                    package.Items.Add("ColumnGroup", promoPlan.ColumnGroup);
                    package.Items.Add("PlanID", promoPlan.PlanID);
                    package.Items.Add("CurrencyType", PaymentUIConnector.GetCurrencyAbbreviation(promoPlan.CurrencyType));
                    package.Items.Add("BestValueFlag", promoPlan.BestValueFlag);
                    package.Items.Add("DefaultPackage", (promoPlan.PlanID == promo.DefaultPlanID) ? true : false);
                    package.Items.Add("PaymentTypeMask", promoPlan.PaymentTypeMask);
                    package.Items.Add("PlanResourcePaymentTypeID", promoPlan.PlanResourcePaymentTypeID);
                    package.Items.Add("CreditAmount", promoPlan.CreditAmount);
                    package.Items.Add("PurchaseMode", promoPlan.PurchaseMode.ToString());
                    package.Items.Add("PlanServices", promoPlan.PlanServices);

                    payment.Data.Packages.Add(package);
                };

                return payment;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in building JSON for PaymentUI. MemberID:" +
                    g.Member.MemberID.ToString(), ex);
            }
        }

        private string GetPlanIDListForOmniture(Matchnet.PromoEngine.ValueObjects.PromoPlanCollection plans)
        {
            System.Text.StringBuilder planIDList = new System.Text.StringBuilder();
            
            foreach (Purchase.ValueObjects.Plan displayedPlan in plans)
            {
                if (displayedPlan != null)
                {
                    if (displayedPlan.PlanID > 0)
                    {
                        planIDList.Append(displayedPlan.PlanID.ToString() + ",");
                    }
                }
            }

            return planIDList.ToString().TrimEnd(',');
        }

        #endregion
    }
}

