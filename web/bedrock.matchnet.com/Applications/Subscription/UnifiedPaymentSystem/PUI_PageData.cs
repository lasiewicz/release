﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Xml;
using System.Xml.Linq;

namespace Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem
{
    /// <summary>
    /// Created this wrapper & xml so we can implement A/B whatever testing later.
    /// </summary>
    public static class PUI_PageData
    {
        private static XElement element;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        private static void LoadData(string url)
        {
            try
            {
                if (element != null)
                    return;

                element = XElement.Load(url);
            }
            catch (Exception ex)
            {
                throw new Exception("Error read XML test file for PUI data.", ex);
            }
        }

        /// <summary>
        /// Populates version & template ID
        /// </summary>
        /// <param name="iPaymentUIJSON"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static IPaymentUIJSON Hydrate(IPaymentUIJSON iPaymentUIJSON, string url)
        {
            try
            {
                LoadData(url);

                var result = from e in element.Elements("page")
                             where e.Element("name").Value.ToString() == ((object)iPaymentUIJSON).GetType().Name.ToString()
                             select e;

                if (result == null)
                    throw new Exception("No XML data found for " + ((object)iPaymentUIJSON).GetType().Name.ToString());

                iPaymentUIJSON.Version = Convert.ToInt32(result.FirstOrDefault().Element("version").Value);
                iPaymentUIJSON.TemplateID = Convert.ToInt32(result.FirstOrDefault().Element("templateID").Value);

                return iPaymentUIJSON;
            }
            catch(Exception ex)
            {
                throw new Exception("Error hydrating verion and template information for this page.", ex);
            }
        }
    }
}
