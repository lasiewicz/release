﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using MPVO = Matchnet.Purchase.ValueObjects;

using Spark.Common.UPS;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.Adapter;
using Spark.Common.OrderHistoryService;
using Spark.Common.PaymentProfileService;
using Matchnet.Member.ServiceAdapters;
using Spark.Common.RestConsumer.V2.Models.Subscription;
using Matchnet.Web.Framework.Managers;
using Spark.Common.RestConsumer.V2.Models.OAuth2;
using Matchnet.Configuration.ValueObjects;

namespace Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem
{
    public class FixedPricingUpsalePage : FrameworkControl, IPaymentUIJSON
    {
        private int version = Constants.NULL_INT;
        private int templateID = Constants.NULL_INT;
        private Matchnet.Purchase.ValueObjects.PaymentType paymentType;
        private int upsLegacyDataID = Constants.NULL_INT;
        private int UPSGlobalLogID = Constants.NULL_INT;		
        private bool saveLegacyData = true;
        public Dictionary<String, String> OrderAttributes { get; set; }
        public bool IsSubscriptionConfirmationEmailEnabled { get; set; }
        private PremiumType _premiumType = PremiumType.None;

        #region IPaymentUIJSON Members
        PremiumType IPaymentUIJSON.PremiumType
        {
            get
            {
                return _premiumType;
            }
            set
            {
                _premiumType = value;
            }
        }

        bool IPaymentUIJSON.SaveLegacyData
        {
            get
            {
                return saveLegacyData;
            }
            set
            {
                saveLegacyData = true;
            }
        }

        int IPaymentUIJSON.Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        int IPaymentUIJSON.TemplateID
        {
            get
            {
                return templateID;
            }
            set
            {
                templateID = value;
            }
        }

        Matchnet.Purchase.ValueObjects.PaymentType IPaymentUIJSON.PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        int IPaymentUIJSON.UPSLegacyDataID
        {
            get
            {
                return upsLegacyDataID;
            }
            set
            {
                upsLegacyDataID = value;
            }
        }

        int IPaymentUIJSON.UPSGlobalLogID
        {
            get
            {
                return UPSGlobalLogID;
            }
            set
            {
                UPSGlobalLogID = value;
            }
        }		
		
        object IPaymentUIJSON.GetJSONObject() 
        {
            SettingsManager settingsManager = new SettingsManager();

            if (settingsManager.GetSettingBool("USE_API_FOR_UPS_SUBSCRIPTION_PAYMENT_JSON", g.TargetCommunityID, g.Brand.Site.SiteID, g.TargetBrandID))
                return GetAPIPaymentJson();
            else
                return GetPaymentJson();
        }


        private PaymentUiPostData GetAPIPaymentJson()
        {
            saveLegacyData = false;

            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            //set up the access token for the API. 
            PaymentUiPostData paymentData = null;
            LogonManager logonManager = new LogonManager(_g);
            OAuthTokens oAuthTokens = logonManager.GetTokensFromCookies();
            string apiToken = string.Empty;
            if (oAuthTokens != null)
            {
                apiToken = oAuthTokens.AccessToken;

                string destinationURL = string.Empty;
                destinationURL = (Context.Request["DestinationURL"] != null) ? Context.Server.UrlEncode(Convert.ToString(Context.Request["DestinationURL"])) : string.Empty;
                if (destinationURL.IndexOf(g.Page.Request.Url.Host) < 0)
                {
                    destinationURL = "http://" + g.Page.Request.Url.Host + destinationURL;
                }


                int orderID = Constants.NULL_INT;
                if (Context.Request["oid"] != null)
                {
                    orderID = Convert.ToInt32(Context.Request["oid"]);
                }

                var request = new PaymentUiPostDataRequest
                {
                    PaymentUiPostDataType = "2",
                    CancelUrl = "http://" + g.Page.Request.Url.Host + "/Applications/MemberServices/MemberServices.aspx",
                    ConfirmationUrl = "https://" + g.Page.Request.Url.Host + "/Applications/Subscription/SubscriptionConfirmation.aspx",
                    DestinationUrl = destinationURL,
                    ReturnUrl =  "http://" + g.Page.Request.Url.Host,
                    ClientIp = g.ClientIP,
                    PrtTitle = "",
                    SrId = (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]).Replace(",", ";") : string.Empty,
                    PrtId = (Context.Request["prtid"] != null) ? Convert.ToString(Context.Request["prtid"]).Replace(",", ";") : string.Empty,
                    OmnitureVariables = new Dictionary<string, string>(),
                    MemberLevelTrackingLastApplication = "1",
                    MemberLevelTrackingIsMobileDevice = "false",
                    MemberLevelTrackingIsTablet = "false",
                    PromoType = "0",
                    OrderID = orderID.ToString(),
                    PremiumType = ((int)PremiumType.AllAccessEmail).ToString()
                };


                #region Omniture Variables
                System.Diagnostics.Trace.WriteLine("GetJSONObject() - Start Omniture Variables");

                request.OmnitureVariables.Add("pageName", "Ala Carte - Select Product");
                request.OmnitureVariables.Add("prop17", OmnitureHelper.OmnitureHelper.GetProfileCompetionPercentage(_g.TargetMember, _g.TargetBrand));
                request.OmnitureVariables.Add("prop18", OmnitureHelper.OmnitureHelper.GetGender(_g.TargetMember.GetAttributeInt(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                request.OmnitureVariables.Add("prop19", OmnitureHelper.OmnitureHelper.GetAge(_g.TargetMember.GetAttributeDate(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());
                if (_g.TargetBrand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
                {
                    request.OmnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "JDateEthnicity", "JDateEthnicity"));
                }
                else
                {
                    request.OmnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "Ethnicity", "Ethnicity"));
                }
                request.OmnitureVariables.Add("prop21", OmnitureHelper.OmnitureHelper.GetRegionString(_g.TargetMember, _g.TargetBrand, _g.TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                request.OmnitureVariables.Add("prop23", _g.TargetMember.MemberID.ToString());
                request.OmnitureVariables.Add("events", "purchase");
                request.OmnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (_g.TargetMember.MemberID % 10).ToString()));
                request.OmnitureVariables.Add("prop29", g.AnalyticsOmniture.Prop29);

                request.OmnitureVariables.Add("eVar6",
                                      (Context.Request["prtid"] != null)
                                          ? Convert.ToString(Context.Request["prtid"])
                                          : string.Empty);

                int memberSubscriptionStatus = FrameworkGlobals.GetMemberSubcriptionStatus(_g.TargetMember, _g.TargetBrand);
                SubscriptionStatus enumSubscriptionStatus = (SubscriptionStatus)memberSubscriptionStatus;
                request.OmnitureVariables.Add("eVar8", enumSubscriptionStatus == SubscriptionStatus.NonSubscriberNeverSubscribed ? "FTS" : "WB");


                request.OmnitureVariables.Add("evar11", g.Session[WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID]);
                request.OmnitureVariables.Add("evar12", g.Session[WebConstants.SESSION_PROPERTY_NAME_EID]);
                request.OmnitureVariables.Add("evar13", g.Session[WebConstants.SESSION_PROPERTY_NAME_LUGGAGE]);

                // Send Google Analytics client id for tracking continuity 
                if (!string.IsNullOrEmpty(SettingsManager.GetSettingString(SettingConstants.GOOGLE_ANALYTICS_ACCOUNT,
                        g.Brand)))
                {
                    if (HttpContext.Current.Request.Cookies["_ga"] != null && !string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["_ga"].Value))
                    {
                        request.OmnitureVariables.Add("GoogleAnalyticsClientID", HttpContext.Current.Request.Cookies["_ga"].Value.Remove(0, 6));
                    }
                }

                #endregion

                APIPaymentUIJSON apiPaymentUIPSON = new APIPaymentUIJSON();
                paymentData = apiPaymentUIPSON.PostPayment(request, apiToken, g.Brand.BrandID.ToString());

                if (paymentData == null)
                    throw new Exception("GetAPIPaymentJson Unable to get payment data from Subcription. FixedPricingUpsale page");

            }
            else
                throw new Exception("FixedPricingUpsalePage...GetAPIPaymentJson...oAuthTokens was null");

            return paymentData;      
        }



        #region old payment method
        private PaymentJson GetPaymentJson()
        {
            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            if (upsLegacyDataID == Constants.NULL_INT)
                throw new Exception("UPS Legacy Data ID cannot be null.");

            Dictionary<string, string> colDataAttributes = new Dictionary<string, string>();

            try
            {
                if (g.TargetMember == null)
                    throw new Exception("Member cannot null.");

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ENTER PaymentUIConnector jump page", null, 1, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);

                int memberID = g.TargetMember.MemberID;

                PaymentJson payment = new PaymentJson();

                // Get all the privileges that have not expired for this member for this site  
                Spark.Common.AccessService.AccessPrivilege[] accessPrivileges = Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstance().GetCustomerPrivilegeListAll(memberID);
                List<Spark.Common.AccessService.AccessPrivilege> activePrivileges = new List<Spark.Common.AccessService.AccessPrivilege>();
                foreach (Spark.Common.AccessService.AccessPrivilege checkAccessPrivilege in accessPrivileges)
                {
                    if (checkAccessPrivilege.CallingSystemID == g.TargetBrand.Site.SiteID)
                    {
                        if (checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.BasicSubscription
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.HighlightedProfile
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.SpotlightMember
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.JMeter
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess)
                        {
                            if (checkAccessPrivilege.EndDateUTC > DateTime.Now)
                            {
                                activePrivileges.Add(checkAccessPrivilege);
                            }
                        }
                    }
                }

                List<int> upsalePackages = GetFixedPricingUpsalePackage(g.TargetBrand.Site.SiteID);

                List<MPVO.Plan> allowedUpsalePlanCollection = new List<MPVO.Plan>();

                foreach (int planID in upsalePackages)
                {
                    MPVO.Plan upsalePlan = PlanSA.Instance.GetPlan(planID, g.TargetBrand.BrandID);

                    if (upsalePlan != null)
                    {
                        // The fixed pricing upsale package just contains all access emails that do not renew
                        // But in order to purchase all access emails, the member must have all access that has not yet expired 
                        // Also check the maximum number of all access emails that the member can purchase 
                        if ((upsalePlan.PremiumTypeMask & PremiumType.AllAccessEmail) == PremiumType.AllAccessEmail)
                        {
                            bool isAllowedToPurchaseAllAccessEmails = false;

                            Spark.Common.AccessService.AccessPrivilege checkPrivilege = activePrivileges.Find(delegate(Spark.Common.AccessService.AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess; });
                            if (checkPrivilege == null)
                            {
                                // Member can purchase this privilege since he does not have this privilege yet
                                isAllowedToPurchaseAllAccessEmails = false;
                            }
                            else
                            {
                                if (checkPrivilege.EndDatePST == DateTime.MinValue
                                    || checkPrivilege.EndDatePST < DateTime.Now)
                                {
                                    // All access expired   
                                    isAllowedToPurchaseAllAccessEmails = false;
                                }
                                else
                                {
                                    isAllowedToPurchaseAllAccessEmails = true;
                                }
                            }

                            if (isAllowedToPurchaseAllAccessEmails)
                            {
                                allowedUpsalePlanCollection.Add(upsalePlan);
                            }
                        }
                    }
                }

                payment.UPSLegacyDataID = upsLegacyDataID;
                payment.UPSGlobalLogID = UPSGlobalLogID;
                payment.GetCustomerPaymentProfile = 1;
                payment.Version = version;
                payment.CallingSystemID = g.TargetBrand.Site.SiteID;
                payment.TemplateID = templateID;
                payment.TimeStamp = DateTime.Now;
                payment.Data.Navigation.CancelURL = "http://" + g.Page.Request.Url.Host + "/Applications/MemberServices/MemberServices.aspx";
                payment.Data.Navigation.ReturnURL = "http://" + g.Page.Request.Url.Host;
                payment.Data.Navigation.ConfirmationURL = "https://" + g.Page.Request.Url.Host + "/Applications/Subscription/SubscriptionConfirmation.aspx";
                payment.Data.Navigation.DestinationURL = (Context.Request["DestinationURL"] != null) ? Context.Server.UrlEncode(Convert.ToString(Context.Request["DestinationURL"])) : string.Empty;
                if (payment.Data.Navigation.DestinationURL.IndexOf(g.Page.Request.Url.Host) < 0)
                {
                    payment.Data.Navigation.DestinationURL = "http://" + g.Page.Request.Url.Host + payment.Data.Navigation.DestinationURL;
                }

                // Order Attributes
                OrderAttributes = new Dictionary<string, string>();

                OrderAttributes.Add("PRTID", (Context.Request["prtid"] != null) ? Convert.ToString(Context.Request["prtid"]).Replace(",", ";") : string.Empty);
                OrderAttributes.Add("SRID", (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]).Replace(",", ";") : string.Empty);
                //OrderAttributes.Add("PricingGroupID", promo.PromoID.ToString());

                foreach (string key in OrderAttributes.Keys)
                {
                    if (OrderAttributes[key] != string.Empty)
                        payment.Data.OrderAttributes += key + "=" + OrderAttributes[key] + ",";
                }

                if (payment.Data.OrderAttributes.Length > 0)
                    payment.Data.OrderAttributes = payment.Data.OrderAttributes.TrimEnd(',');

                // Member Info
                payment.Data.MemberInfo.CustomerID = g.TargetMemberID;
                payment.Data.MemberInfo.RegionID = g.TargetMember.GetAttributeInt(g.TargetBrand, "RegionID");
                if (payment.Data.MemberInfo.RegionID == Constants.NULL_INT
                    || payment.Data.MemberInfo.RegionID < 0)
                {
                    // There must be a valid RegionID to price the cart so get it from the database if the cache does not have a valid RegionID  
                    Member.ServiceAdapters.Member refreshedMember = MemberSA.Instance.GetMember(35, MemberLoadFlags.IngoreSACache);
                    payment.Data.MemberInfo.RegionID = refreshedMember.GetAttributeInt(g.TargetBrand, "RegionID");
                }

                payment.Data.MemberInfo.Language = ((Language)Enum.Parse(typeof(Language), g.TargetBrand.Site.LanguageID.ToString())).ToString();

                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID, (int)Language.English);
                if (regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                    payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation.Trim().ToUpper();

                //// Omniture Variables
                PaymentJsonArray omnitureVariables = new PaymentJsonArray();

                omnitureVariables.Add("pageName", "Ala Carte - Select Product");
                omnitureVariables.Add("prop17", OmnitureHelper.OmnitureHelper.GetProfileCompetionPercentage(_g.TargetMember, _g.TargetBrand));
                omnitureVariables.Add("prop18", OmnitureHelper.OmnitureHelper.GetGender(_g.TargetMember.GetAttributeInt(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                omnitureVariables.Add("prop19", OmnitureHelper.OmnitureHelper.GetAge(_g.TargetMember.GetAttributeDate(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());
                if (_g.TargetBrand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
                {
                    omnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "JDateEthnicity", "JDateEthnicity"));
                }
                else
                {
                    omnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "Ethnicity", "Ethnicity"));
                }
                omnitureVariables.Add("prop21", OmnitureHelper.OmnitureHelper.GetRegionString(_g.TargetMember, _g.TargetBrand, _g.TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                omnitureVariables.Add("prop23", _g.TargetMember.MemberID.ToString());
                omnitureVariables.Add("events", "purchase");
                omnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (_g.TargetMember.MemberID % 10).ToString()));
                omnitureVariables.Add("prop29", g.AnalyticsOmniture.Prop29);

                omnitureVariables.Add("eVar6",
                                      (Context.Request["prtid"] != null)
                                          ? Convert.ToString(Context.Request["prtid"])
                                          : string.Empty);

                int memberSubscriptionStatus = FrameworkGlobals.GetMemberSubcriptionStatus(_g.TargetMember, _g.TargetBrand);
                SubscriptionStatus enumSubscriptionStatus = (SubscriptionStatus)memberSubscriptionStatus;
                omnitureVariables.Add("eVar8", enumSubscriptionStatus == SubscriptionStatus.NonSubscriberNeverSubscribed ? "FTS" : "WB");


                omnitureVariables.Add("evar11", g.Session[WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID]);
                omnitureVariables.Add("evar12", g.Session[WebConstants.SESSION_PROPERTY_NAME_EID]);
                omnitureVariables.Add("evar13", g.Session[WebConstants.SESSION_PROPERTY_NAME_LUGGAGE]);

                payment.Data.OmnitureVariables.Add(omnitureVariables);

                // Legacy Data Info
                if (g.Member.MemberID != g.TargetMemberID)
                {
                    payment.Data.LegacyDataInfo.AdminMemberID = g.Member.MemberID;
                }
                payment.Data.LegacyDataInfo.Member = g.TargetMemberID;
                payment.Data.LegacyDataInfo.BrandID = g.TargetBrand.BrandID;
                payment.Data.LegacyDataInfo.SiteID = g.TargetBrand.Site.SiteID;

                System.Text.StringBuilder planIDList = new System.Text.StringBuilder();

                // A La Carte Plans
                // Should only be two. One of each premium service.
                foreach (MPVO.Plan plan in allowedUpsalePlanCollection)
                {
                    if (plan.EndDate != DateTime.MinValue)
                        continue;

                    PaymentJsonPackage package;
                    package = new PaymentJsonPackage();

                    package.ID = plan.PlanID;
                    package.Description = "";
                    package.SytemID = g.TargetBrand.Site.SiteID;
                    package.StartDate = DateTime.MinValue;
                    package.ExpiredDate = DateTime.MinValue;
                    package.CurrencyType = PaymentUIConnector.GetCurrencyAbbreviation(plan.CurrencyType);

                    package.Items.Add("InitialCostPerDuration", plan.InitialCost);
                    package.Items.Add("ProratedAmount", plan.InitialCost);
                    package.Items.Add("DurationType", "None");
                    package.Items.Add("Duration", 0);
                    package.Items.Add("PlanID", plan.PlanID);
                    package.Items.Add("CurrencyType", PaymentUIConnector.GetCurrencyAbbreviation(plan.CurrencyType));
                    package.Items.Add("PaymentTypeMask", plan.PaymentTypeMask);
                    package.Items.Add("CreditAmount", (plan.CreditAmount < 0) ? 0 : plan.CreditAmount);
                    package.Items.Add("PurchaseMode", PurchaseMode.New.ToString());
                    package.Items.Add("PremiumType", plan.PremiumTypeMask.ToString());
                    package.Items.Add("RenewCost", plan.RenewCost);

                    Spark.Common.CatalogService.Package emailPackage = Spark.Common.Adapter.CatalogServiceWebAdapter.GetProxyInstance().GetPackageDetails(plan.PlanID);
                    Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();

                    foreach (Spark.Common.CatalogService.Item emailPackageItem in emailPackage.Items)
                    {
                        foreach (Spark.Common.CatalogService.PrivilegeType emailPackageItemPrivilege in emailPackageItem.PrivilegeType)
                        {
                            if (emailPackageItemPrivilege == Spark.Common.CatalogService.PrivilegeType.AllAccessEmails)
                            {
                                package.Items.Add("DisburseCount", emailPackageItem.DisburseCount);
                                package.Items.Add("DisburseDuration", emailPackageItem.DisburseDuration);
                                package.Items.Add("DisburseDurationType", Enum.GetName(typeof(Spark.Common.CatalogService.DurationType), emailPackageItem.DisburseDurationType));
                            }
                        }
                    }

                    payment.Data.Packages.Add(package);

                    if (package.ID > 0)
                    {
                        planIDList.Append(package.ID.ToString() + ",");
                    }
                };

                colDataAttributes.Add("UPSLegacyDataID", Convert.ToString(upsLegacyDataID));
                colDataAttributes.Add("CustomerID", Convert.ToString(memberID));
                colDataAttributes.Add("CallingSystemID", Convert.ToString(g.TargetBrand.Site.SiteID));
                colDataAttributes.Add("TransactionType", "24");
                colDataAttributes.Add("PRTID", OrderAttributes["PRTID"]);
                colDataAttributes.Add("SRID", OrderAttributes["SRID"]);
                //colDataAttributes.Add("PricingGroupID", OrderAttributes["PricingGroupID"]);
                colDataAttributes.Add("RegionID", Convert.ToString(payment.Data.MemberInfo.RegionID));
                colDataAttributes.Add("IPAddress", payment.Data.MemberInfo.IPAddress);
                colDataAttributes.Add("TemplateID", Convert.ToString(payment.TemplateID));
                colDataAttributes.Add("CancelURL", payment.Data.Navigation.CancelURL);
                colDataAttributes.Add("ConfirmationURL", payment.Data.Navigation.ConfirmationURL);
                colDataAttributes.Add("DestinationURL", payment.Data.Navigation.DestinationURL);
                colDataAttributes.Add("PlanIDList", planIDList.ToString().TrimEnd(','));
                colDataAttributes.Add("PaymentUIConnectorType", "FixedPricingUpsalePage");

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "NO ERROR", null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.OK, DateTime.Now);

                return payment;
            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ERROR DETECTED: " + ex.Message, null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR, DateTime.Now);

                throw new Exception("Error in building JSON for PaymentUI. MemberID:" +
                    g.TargetMember.MemberID, ex);
            }
            finally
            {
                //TraceLogConnectorServiceWebAdapter.GetProxyInstance().InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "EXIT PaymentUIConnector jump page", null, 0, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }
        }
        #endregion


        #endregion

        /// <summary>
        /// Copied from Purchase
        /// </summary>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private int GetDuration(DateTime endDate, DateTime startDate)
        {
            // Get remaining days left on this plan
            // Whenever there is any remaining time in a day, give that full day of remaining credit to the 
            // member.  
            // Round 2.1 to 3 days
            // Round 2.9 to 3 days
            TimeSpan span = endDate.Subtract(startDate);
            Int32 remainingDays = Convert.ToInt32(Math.Ceiling(span.TotalDays));

            System.Diagnostics.Trace.WriteLine("RemainingDays:" + remainingDays);

            return remainingDays;
        }

        private bool IsAllowedToPurchaseUpsalePlan(List<Spark.Common.AccessService.AccessPrivilege> activePrivileges, Spark.Common.AccessService.PrivilegeType checkPrivilegeType, DateTime renewalDate)
        {
            bool isAllowedToPurchaseUpsalePlan = false;

            Spark.Common.AccessService.AccessPrivilege checkPrivilege = activePrivileges.Find(delegate(Spark.Common.AccessService.AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == checkPrivilegeType; });
            if (checkPrivilege == null)
            {
                // Member can purchase this privilege since he does not have this privilege yet
                isAllowedToPurchaseUpsalePlan = true;
            }
            else
            {
                if (checkPrivilege.EndDatePST == DateTime.MinValue
                    || checkPrivilege.EndDatePST < DateTime.Now)
                {
                    // Member can purchase this privilege since he had this privilege but it expired  
                    isAllowedToPurchaseUpsalePlan = true;
                }
                else
                {
                    // The start date of the premium service should be either earlier than the renewal date 
                    // or the same as the renewal date. If the start date of the premium service is past the
                    // renewal date, then set the start date of the premium service to the renewal date. 
                    if (checkPrivilege.EndDatePST > renewalDate)
                    {
                        isAllowedToPurchaseUpsalePlan = false;
                    }
                    else
                    {
                        System.TimeSpan dateDifference = checkPrivilege.EndDatePST.Subtract(renewalDate);
                        if (Math.Abs(dateDifference.TotalMinutes) > 20)
                        {
                            // There is still additional time that member can purchase for this privilege
                            isAllowedToPurchaseUpsalePlan = true;
                        }
                        else
                        {
                            // The end date of the privilege is the same as the end date of the basic subscription  
                            // so do not allow the member to purchase this privilege  
                            isAllowedToPurchaseUpsalePlan = false;
                        }
                    }
                }
            }

            return isAllowedToPurchaseUpsalePlan;
        }

        private DateTime GetStartDateForUpsalePlan(MPVO.Plan plan, List<Spark.Common.AccessService.AccessPrivilege> activePrivileges)
        {
            Spark.Common.AccessService.PrivilegeType checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.None;
            switch (plan.PremiumTypeMask)
            {
                case PremiumType.HighlightedProfile:
                    checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.HighlightedProfile;
                    break;
                case PremiumType.SpotlightMember:
                    checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.SpotlightMember;
                    break;
                case PremiumType.JMeter:
                    checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.JMeter;
                    break;
                case PremiumType.AllAccess:
                    checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.AllAccess;
                    break;
                case PremiumType.AllAccessEmail:
                    checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.AllAccessEmails;
                    break;
                case PremiumType.ReadReceipt:
                    checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.ReadReceipt;
                    break;
                default:
                    checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.None;
                    break;
            }

            Spark.Common.AccessService.AccessPrivilege checkPrivilege = activePrivileges.Find(delegate(Spark.Common.AccessService.AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == checkPrivilegeType; });

            if (checkPrivilege == null)
            {
                return DateTime.Now;
            }
            else
            {
                if (checkPrivilege.EndDatePST <= DateTime.Now)
                {
                    return DateTime.Now;
                }
                else
                {
                    return checkPrivilege.EndDatePST;
                }
            }
        }

        /// <summary>
        /// Copied from Purchase
        /// </summary>
        /// <param name="initialCost"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private decimal GetMemberProratedAmount(decimal initialCost, DateTime endDate, DateTime startDate)
        {
            if (endDate < DateTime.Now)
            {
                g.Notification.AddErrorString("Subscription end date must be a future date. Enddate:" +
                                              endDate.ToString());

                throw new Exception("Subscription end date must be a future date. Enddate:" +
                                    endDate.ToString());
            }

            // Calculate cost per day
            decimal pricePerDay = (initialCost) / 30;

            // The start date of the premium service should be either earlier than the renewal date 
            // or the same as the renewal date. If the start date of the premium service is past the
            // renewal date, then set the start date of the premium service to the renewal date. 
            if (startDate > endDate)
            {
                startDate = endDate;
            }

            decimal amount = (pricePerDay) * (Convert.ToDecimal(GetDuration(endDate, startDate)));

            if (amount < 0)
                throw new Exception("Prorated Amount cannot be less then 0.");

            System.Diagnostics.Trace.WriteLine("InitialCost:" + initialCost +
                "EndDate:" + endDate + "PricePerDay:" + pricePerDay + "Amount:" + amount);

            return amount;
        }

        private List<int> GetFixedPricingUpsalePackage(int siteID)
        {
            List<int> upsalePackages = new List<int>();

            if (siteID == (int)WebConstants.SITE_ID.JDate)
            {
                upsalePackages.Add(4500);
                upsalePackages.Add(4501);
            }
            else if (siteID == (int)WebConstants.SITE_ID.JDateUK)
            {
                upsalePackages.Add(4045);
                upsalePackages.Add(4046);
            }
            else if (siteID == (int)WebConstants.SITE_ID.JDateFR)
            {
                upsalePackages.Add(4050);
                upsalePackages.Add(4051);
            }
            else if (siteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                upsalePackages.Add(4503);
            }

            return upsalePackages;
        }
    }
}
