﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Web.Framework.Util;
using MPVO = Matchnet.Purchase.ValueObjects;

using Spark.Common.UPS;
using Spark.Common.Adapter;
using Matchnet.Web.Framework.Managers;
using Brand = Matchnet.Content.ValueObjects.BrandConfig.Brand;
using Spark.Common.RestConsumer.V2.Models.Subscription;
using Spark.Common.RestConsumer.V2.Models.OAuth2;

namespace Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem
{
    public class PaymentProfilePage : FrameworkControl, IPaymentUIJSON
    {
        private int version = Constants.NULL_INT;
        private int templateID = Constants.NULL_INT;
        private PaymentType paymentType;
        private int upsLegacyDataID = Constants.NULL_INT;
        private int UPSGlobalLogID = Constants.NULL_INT;
        private bool saveLegacyData = true;
        public bool IsSubscriptionConfirmationEmailEnabled { get; set; }
        private PremiumType _premiumType = PremiumType.None;

        #region IPaymentUIJSON Members
        PremiumType IPaymentUIJSON.PremiumType
        {
            get
            {
                return _premiumType;
            }
            set
            {
                _premiumType = value;
            }
        }

        bool IPaymentUIJSON.SaveLegacyData
        {
            get
            {
                return saveLegacyData;
            }
            set
            {
                saveLegacyData = true;
            }
        }

        int IPaymentUIJSON.Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        int IPaymentUIJSON.TemplateID
        {
            get
            {
                return templateID;
            }
            set
            {
                templateID = value;
            }
        }

        PaymentType IPaymentUIJSON.PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        int IPaymentUIJSON.UPSLegacyDataID
        {
            get
            {
                return upsLegacyDataID;
            }
            set
            {
                upsLegacyDataID = value;
            }
        }

        int IPaymentUIJSON.UPSGlobalLogID
        {
            get
            {
                return UPSGlobalLogID;
            }
            set
            {
                UPSGlobalLogID = value;
            }
        }

        private bool UseAPIForPaymentJSON()
        {
            SettingsManager settingsManager = new SettingsManager();
            return settingsManager.GetSettingBool("USE_API_FOR_UPS_SUBSCRIPTION_PAYMENT_JSON", g.TargetCommunityID, g.Brand.Site.SiteID, g.TargetBrandID);
        }


        object IPaymentUIJSON.GetJSONObject() 
        {
            if (UseAPIForPaymentJSON())
                return GetAPIPaymentJson();
            else
                return GetPaymentJson();
        }

        private PaymentUiPostData GetAPIPaymentJson()
        {
            try
            {
                saveLegacyData = false;

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "ENTER PaymentUIConnector jump page", null, 1, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                int memberID = g.Member.MemberID;
            
                //version and template are set in payment ui connector and paymenttype by subscribenew page. 
                if (version == Constants.NULL_INT)
                    throw new Exception("Version cannot be null.");

                if (paymentType == null)
                    throw new Exception("PaymentType cannot be null.");

                //set up the access token for the API. 
                PaymentUiPostData paymentData = null;
                LogonManager logonManager = new LogonManager(_g);
                OAuthTokens oAuthTokens = logonManager.GetTokensFromCookies();
                string apiToken = string.Empty;
                if (oAuthTokens != null)
                {
                    apiToken =  oAuthTokens.AccessToken;

                    string cancelUrl = string.Empty;

                    if (g.Page.Request.UrlReferrer != null)
                        cancelUrl = g.Page.Request.UrlReferrer.AbsoluteUri;
                    else
                        cancelUrl = "http://" + g.Page.Request.Url.Host;

                    var request = new PaymentUiPostDataRequest
                    {
                        PaymentUiPostDataType = "4",
                        CancelUrl = cancelUrl,
                        ReturnUrl = "http://" + g.Page.Request.Url.Host,
                        ClientIp = g.ClientIP,
                        OmnitureVariables = new Dictionary<string, string>(),
                        MemberLevelTrackingLastApplication = "1",
                        MemberLevelTrackingIsMobileDevice = "false",
                        MemberLevelTrackingIsTablet = "false",
                        EID = g.Session[WebConstants.SESSION_PROPERTY_NAME_EID]
                    };
                
                    APIPaymentUIJSON apiPaymentUIPSON = new APIPaymentUIJSON();
                    paymentData = apiPaymentUIPSON.PostPayment(request, apiToken,g.Brand.BrandID.ToString());

                    if (paymentData == null)
                        throw new Exception("Unable to get payment data from Subcription. PaymentProfile page");

                    TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "NO ERROR", null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.OK, DateTime.Now);

                }
                else
                    throw new Exception("PaymentProfilePage...GetAPIPaymentJson...oAuthTokens was null");

                return paymentData;  
            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "ERROR DETECTED: " + ex.Message, null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR, DateTime.Now);

                throw new Exception("Error in getting JSON from API. MemberID:" +
                    g.TargetMember.MemberID.ToString(), ex);
            }
            finally
            {
                System.Diagnostics.Trace.WriteLine("PaymentProfilePage.cs GetAPIPaymentJson() - Finally clause");
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "EXIT PaymentUIConnector PaymentProfile jump page", null, 0, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }		
        }

        private PaymentJson GetPaymentJson()
        {
            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            Dictionary<string, string> colDataAttributes = new Dictionary<string, string>();

            try
            {
                if (g.Member == null)
                    throw new Exception("Member cannot null.");

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ENTER PaymentUIConnector jump page", null, 1, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                int memberID = g.Member.MemberID;

                PaymentJson payment = new PaymentJson();

                payment.Version = version;
                payment.CallingSystemID = g.Brand.Site.SiteID;
                payment.TemplateID = templateID;
                payment.TimeStamp = DateTime.Now;
                if (g.Page.Request.UrlReferrer != null)
                    payment.Data.Navigation.CancelURL = g.Page.Request.UrlReferrer.AbsoluteUri;
                else
                    payment.Data.Navigation.CancelURL = "http://" + g.Page.Request.Url.Host;
                payment.Data.Navigation.ReturnURL = "http://" + g.Page.Request.Url.Host;
                payment.GetCustomerPaymentProfile = 1;

                // Member Info11
                payment.Data.MemberInfo.CustomerID = g.TargetMemberID;
                payment.Data.MemberInfo.RegionID = g.Member.GetAttributeInt(g.Brand, "RegionID");
                payment.Data.MemberInfo.IPAddress = g.ClientIP;
                payment.Data.MemberInfo.Language = ((Language)Enum.Parse(typeof(Language), g.Brand.Site.LanguageID.ToString())).ToString();
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID, (int)Language.English);
                if (regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                    payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation;

                //LC: Adding extra fields needed for Kount RIS
                payment.Data.MemberInfo.EmailAddress = g.Member.GetAttributeText(g.Brand, "EmailAddress");
                payment.Data.MemberInfo.GenderMask = SubscriberManager.Instance.GetGender(g.Member.GetAttributeInt(g.Brand, "gendermask"));
                payment.Data.MemberInfo.BirthDate = g.Member.GetAttributeDate(g.Brand, "birthdate", DateTime.MinValue);
                payment.Data.MemberInfo.BrandInsertDate = g.Member.GetAttributeDate(g.Brand, "BrandInsertDate", DateTime.MinValue);

                #region UDFS
                //JS-1228 LC: Adding UDF fields needed for Kount
                payment.Data.MemberInfo.UserName = g.Member.GetUserName(g.Brand);
                //payment.Data.MemberInfo.Age = SubscriberManager.Instance.CalculateAge(payment.Data.MemberInfo.BirthDate.ToShortDateString());
                payment.Data.MemberInfo.MaritalStatus = Option.GetDescription("MaritalStatus", g.Member.GetAttributeInt(g.Brand, "MaritalStatus"), g);
                payment.Data.MemberInfo.Occupation = g.Member.GetAttributeText(g.Brand, "OccupationDescription");
                payment.Data.MemberInfo.Education = Option.GetDescription("EducationLevel", g.Member.GetAttributeInt(g.Brand, "EducationLevel"), g);
                payment.Data.MemberInfo.PromotionID = g.Member.GetAttributeInt(g.Brand, "PromotionID").ToString();
                payment.Data.MemberInfo.Eyes = Option.GetDescription("EyeColor", g.Member.GetAttributeInt(g.Brand, "EyeColor"), g);
                payment.Data.MemberInfo.Hair = Option.GetDescription("HairColor", g.Member.GetAttributeInt(g.Brand, "HairColor"), g);
                payment.Data.MemberInfo.Height = g.Member.GetAttributeInt(g.Brand, "Height").ToString();

                if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
                {
                    payment.Data.MemberInfo.Ethnicity = Option.GetDescription("JDateEthnicity", g.Member.GetAttributeInt(g.Brand, "JDateEthnicity"), g);
                    payment.Data.MemberInfo.Religion = Option.GetDescription("JDateReligion", g.Member.GetAttributeInt(g.Brand, "JDateReligion"), g);
                }
                else
                {
                    payment.Data.MemberInfo.Ethnicity = Option.GetDescription("Ethnicity", g.Member.GetAttributeInt(g.Brand, "Ethnicity"), g);
                    payment.Data.MemberInfo.Religion = Option.GetDescription("Religion", g.Member.GetAttributeInt(g.Brand, "Religion"), g);
                }

                string aboutMe = g.Member.GetAttributeText(g.Brand, "AboutMe");
                payment.Data.MemberInfo.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);

                #endregion UDF
                Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(g.TargetMemberID, g.TargetBrand);
                if (renewalSub == null || renewalSub.RenewalSubscriptionID <= 0)
                {
                    Spark.Common.PaymentProfileService.PaymentProfileInfo defaultPaymentProfileInfo = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(g.TargetMemberID, g.TargetBrand.Site.SiteID);
                    if (defaultPaymentProfileInfo != null)
                    {
                        // Default payment profile may exists from a free trial subscription 
                        // If the member took a free trial subscription, than show the details of the billing information used to take the free trial subscription. 
                        payment.Data.MemberInfo.MonthlyRenewalRate = 0.0m;
                        payment.Data.MemberInfo.NextRenewalDate = String.Empty;
                    }
                    else
                    {
                        throw new Exception("MemberSub cannot be null. Member must have an active or a past subsription.");
                    }
                }
                else
                {
                    Purchase.ValueObjects.Plan plan = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, g.TargetBrand.BrandID);
                    payment.Data.MemberInfo.MonthlyRenewalRate = plan.RenewCost;
                    payment.Data.MemberInfo.NextRenewalDate = renewalSub.RenewalDatePST.ToShortDateString();
                }

                colDataAttributes.Add("CustomerID", Convert.ToString(memberID));
                colDataAttributes.Add("CallingSystemID", Convert.ToString(g.Brand.Site.SiteID));
                colDataAttributes.Add("TransactionType", "10");
                colDataAttributes.Add("TemplateID", Convert.ToString(payment.TemplateID));
                colDataAttributes.Add("PaymentUIConnectorType", "ChangeCardPage");

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "NO ERROR", null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.OK, DateTime.Now);

                return payment;
            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ERROR DETECTED: " + ex.Message, null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR, DateTime.Now);

                throw new Exception("Error in building JSON for PaymentUI. MemberID:" +
                    g.Member.MemberID.ToString(), ex);
            }
            finally
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "EXIT PaymentUIConnector jump page", null, 0, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }
        }

        #endregion
    }
}
