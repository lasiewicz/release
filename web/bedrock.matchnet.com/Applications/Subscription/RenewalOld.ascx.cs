#region System References
using System;
using System.Web;
#endregion
	
#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Globalization;
#endregion

#region Matchnet Service References
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Member.ServiceAdapters;
#endregion

namespace Matchnet.Web.Applications.Subscription
{
	/// <summary>
	///		Summary description for SubscriptionMain.
	/// </summary>
	public class RenewalOld : SubscribeCommon
	{
		#region FormElements
		protected Matchnet.Web.Framework.Ui.FormElements.FrameworkButton ProcessPurchase;
		protected Matchnet.Web.Applications.Subscription.Controls.SubscriptionConfirmation SubscriptionConfirmation1;
		protected System.Web.UI.HtmlControls.HtmlTableCell tdTelecheck;
		#endregion
	
		#region Static Content 
		protected Matchnet.Web.Framework.Image Img1;
		protected Matchnet.Web.Framework.Txt Txt1;
		protected Matchnet.Web.Framework.Txt Txt2;
		protected Matchnet.Web.Framework.Txt Txt7;
		protected Matchnet.Web.Framework.Txt Txt8;
		protected Matchnet.Web.Framework.Txt Txt4;
		protected Matchnet.Web.Framework.Image Img3;
		protected Matchnet.Web.Framework.Txt Txt5;
		protected Matchnet.Web.Framework.Txt Txt6;
		protected Matchnet.Web.Framework.Txt Txt9;
		protected Matchnet.Web.Framework.Txt Txt10;
		protected Matchnet.Web.Framework.Txt Txt11;
		protected Matchnet.Web.Framework.Txt Txt12;
		protected Matchnet.Web.Framework.Txt Txt13;
		protected Matchnet.Web.Framework.Txt Txt14;
		protected Matchnet.Web.Framework.Txt Txt15;
		protected Matchnet.Web.Framework.Txt Txt16;
		protected Matchnet.Web.Framework.Txt Txt17;
		protected Matchnet.Web.Framework.Image Img4;
		protected Matchnet.Web.Framework.Txt Txt21;
		protected Matchnet.Web.Framework.Txt Txt20;
		protected Matchnet.Web.Framework.Image Img5;
		protected Matchnet.Web.Framework.Txt Txt22;
		protected Matchnet.Web.Framework.Txt Txt23;
		protected Matchnet.Web.Framework.Txt Txt25;
		protected Matchnet.Web.Framework.Txt Txt26;
		protected Matchnet.Web.Framework.Txt Txt27;
		protected Matchnet.Web.Framework.Txt Txt18;
		protected Matchnet.Web.Framework.Txt Txt19;
		protected Matchnet.Web.Framework.Txt Txt3;
		protected Matchnet.Web.Framework.SingleFormSubmitValidator Singleformsubmitvalidator2;
		protected Matchnet.Web.Framework.Txt Txt34;
		protected System.Web.UI.WebControls.PlaceHolder PlanListPanel;
		protected System.Web.UI.WebControls.PlaceHolder SinglePlanListPanel;
		protected Matchnet.Web.Framework.Txt txtAccount;
		protected System.Web.UI.HtmlControls.HtmlInputHidden Hidden1;
		protected Matchnet.Web.Framework.Txt Txt24;
		protected System.Web.UI.HtmlControls.HtmlTable PaymentVerification;
		protected System.Web.UI.WebControls.PlaceHolder PaymentByCheck;
		protected System.Web.UI.WebControls.PlaceHolder PayByPhone;
		protected new System.Web.UI.HtmlControls.HtmlTable CheckTable;
		#endregion

		#region Control Populators
		public string GetNewPaymentMethodLink()
		{
			try
			{
				return LinkFactory.Instance.GetLink("/Applications/Subscription/Subscribe.aspx?" + "OverrideTransactionHistory=1" + g.AppendDestinationURL(), g.Brand, HttpContext.Current.Request.Url.ToString());
			}
			catch(Exception ex)
			{
				_g.ProcessException(ex);
				return "";
			}
		}
		public string GetPayByCheckURL()
		{
			try
			{
				if (g.IsAdmin)
				{
					return FrameworkGlobals.LinkHrefSSL(
						"/Applications/Subscription/SubscribeByCheck.aspx?OverrideTransactionHistory=1" 
						+ "&" + WebConstants.IMPERSONATE_MEMBERID + "=" + g.TargetMemberID
						+ "&" + WebConstants.IMPERSONATE_BRANDID + "=" + g.TargetBrandID
						+ g.AppendDestinationURL());
				}
				else
				{
					return FrameworkGlobals.LinkHrefSSL(
						"/Applications/Subscription/SubscribeByCheck.aspx?OverrideTransactionHistory=1"
						+ g.AppendDestinationURL());
				}
			}
			catch(Exception ex)
			{
				_g.ProcessException(ex);
				return "";
			}
		}

		public string GetAnotherMethodURL()
		{
			try
			{
				if (g.IsAdmin)
				{
					return FrameworkGlobals.LinkHrefSSL(
						"/Applications/Subscription/Subscribe.aspx?OverrideTransactionHistory=1"
						+ "&" + WebConstants.IMPERSONATE_MEMBERID + "=" + g.TargetMemberID
						+ "&" + WebConstants.IMPERSONATE_BRANDID + "=" + g.TargetBrandID);
				}
				else
				{
					return FrameworkGlobals.LinkHrefSSL(
						"/Applications/Subscription/Subscribe.aspx?OverrideTransactionHistory=1");
				}
			}
			catch(Exception ex)
			{
				_g.ProcessException(ex);
				return "";
			}
		}

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ProcessPurchase.Click += new System.EventHandler(this.ProcessPurchase_Click);
			this.Load += new System.EventHandler(this.Page_Load);
			this.Init += new System.EventHandler(this.Page_Init);

		}
		#endregion

		protected override void OnPreRender(EventArgs e)
		{		
			base.OnPreRender (e);
			g.LeftNavControl.Visible = true;

			// Omniture Analytics - Event Tracking
			if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
			{
				// Without this check, this renders even on confirmation, pending, etc :| ...
				if ((SuccessPanel.Visible == false) && (PollingPanel.Visible == false))
				{
					// Subscription Start
					_g.AnalyticsOmniture.Events = "event9";

					// Subscription Plans
					string plans = Constants.NULL_STRING;

					foreach (Matchnet.Purchase.ValueObjects.Plan plan in this.PlanListControl1.Plans)
					{
						plans += plan.PlanID + ",";
					}

					_g.AnalyticsOmniture.Evar3 = plans.TrimEnd(new char[]{','});
				}
			}

		}

		private void Page_Init(object sender, System.EventArgs e)
		{
			if(g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Cupid)
			{
				tdTelecheck.Visible = false;
				CheckTable.Visible = false;
			}

			if(g.Brand.Uri.ToUpper() == "JDATE.CO.IL")
			{
				PaymentByCheck.Visible = false;
				PayByPhone.Visible = true;
			}

			else
			{
				PaymentByCheck.Visible = true;
				PayByPhone.Visible = false;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
                #region Redirect to Subscribe page
                //09162008 TL ET-92
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(WebConstants.SETTING_ENABLE_RENEWAL_TO_SUBSCRIBE_REDIRECT, _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID, _g.Brand.BrandID)))
                {
                    int prtidTransfer = 82; //default is the the user tried to extend/change subscription
                    if (Request.QueryString["prtid"] != null && !int.TryParse(Request.QueryString["prtid"], out prtidTransfer))
                    {
                        prtidTransfer = 82;
                    }

                    Matchnet.Web.Framework.Util.Redirect.Subscription(g.Brand, prtidTransfer, Constants.NULL_INT, true);
                }
                #endregion

				/*
				* this is a total hack to determine if someone is coming to subscription
				* by responding to an email
				* */
				string queryString = Request.Params.Get("QUERY_STRING");
				if( 0 > queryString.IndexOf("ReplyMailID"))
				{
					this.PlanListControl1.removePlan(408);
				}

				DataBind();


				// Put user code to initialize the page here
				if ( Request["CheckStatus"] != null && Request["CheckStatus"] == "1" )
				{
					int memberTranID = GetDecryptedValue(Request["mtid"]);
					int memberPaymentID = GetDecryptedValue(Request["mpid"]);

					try
					{
						if ( memberTranID == 0 )
						{
							if ( (Request["MD"] != null && Request["MD"].Length > 0) && (Request["PaRes"] != null && Request["PaRes"].Length > 0) )
							{
								memberTranID = Convert.ToInt32( Request["MD"] );
							}

							if ( memberTranID == 0 )
							{
								RedirectFailure("renewal", "MNCHARGE___GENERAL_DATABASE_FAILURE_13076", memberPaymentID, 0, false);
							}
						}

						int attempt = 0;
						if ( Request["Attempt"] != null )
							attempt = Matchnet.Conversion.CInt(Request["Attempt"]);

						// Now let's check the status and choose the outcome...
						SubscriptionResult sr;
						sr = PurchaseSA.Instance.GetMemberTranStatus(_g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), memberTranID);

						LastSelectedPlanID = sr.PlanID;

						switch (sr.MemberTranStatus)
						{
							case MemberTranStatus.Success:
								SubscriptionConfirmation1.Enabled = true;
								SubscriptionConfirmation1.PlanID = sr.PlanID;
								ShowSuccess();
								break;

							case MemberTranStatus.Failure:
								RedirectFailure("renewal", sr.ResourceConstant, memberPaymentID, memberTranID, false);
								break;

							case MemberTranStatus.Pending:
							case MemberTranStatus.VerifiedByVisaPending:
							case MemberTranStatus.AuthenticationPending:
								ShowInProgress(memberTranID, "Subscribe", attempt, memberPaymentID, false);
								break;
						}

						
					}
					catch(System.Threading.ThreadAbortException)
					{
					}
					catch
					{
						RedirectFailure("renewal", "MNCHARGE___GENERAL_DATABASE_FAILURE_13076", memberPaymentID, memberTranID, false);
					}
				}
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void ProcessPurchase_Click(object sender, System.EventArgs e)
		{
			try
			{
				int selectedPlan = Conversion.CInt(Request["SelectedPlan"]);
				int adminMemberID = Constants.NULL_INT;
				int memberPaymentID = Constants.NULL_INT;
				int conversionMemberID = Constants.NULL_INT;
				int sourceID = Conversion.CInt( Request["srid"] ) != Constants.NULL_INT ? Conversion.CInt( Request["srid"] ) : Constants.NULL_INT;
				int purchaseReasonTypeID = Conversion.CInt( Request["prtid"] ) != Constants.NULL_INT ? Conversion.CInt( Request["prtid"] ) : Constants.NULL_INT;

				if ( g.IsAdmin && g.Member != null)
				{
					adminMemberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
				}

				if (g.Session["ClickMemberID"] != null)
				{
					conversionMemberID = Conversion.CInt(g.Session["ClickMemberID"]);
				}

				if ( Request["r1"] != null )
				{
					memberPaymentID = GetDecryptedValue(Request["r1"]);
				}

				SubscriptionResult sr = PurchaseSA.Instance.BeginRenewalPurchase(
					g.TargetMemberID,
					selectedPlan,
					Constants.NULL_INT,
					g.TargetBrand.Site.SiteID,
					g.TargetBrand.Site.Community.CommunityID,
					g.TargetBrand.BrandID,
					adminMemberID,
					memberPaymentID,
					conversionMemberID,
					sourceID,
					purchaseReasonTypeID,
					g.ClientIP,
                    PlanListControl1.PromoID);

				if ( sr.ReturnValue == Constants.RETURN_OK )
				{
					ShowInProgress(sr.MemberTranID, "Subscribe", 0, memberPaymentID, false);
				}
				else
				{
					g.Notification.AddError( sr.ResourceConstant );
				}
			}
			catch(Exception)
			{
				// MNCHARGE___GENERAL_DATABASE_FAILURE_13076
				g.Notification.AddError("MNCHARGE___GENERAL_DATABASE_FAILURE_13076");
			}
		}
	}
}
