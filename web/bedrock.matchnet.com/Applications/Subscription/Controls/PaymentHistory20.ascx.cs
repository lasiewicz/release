﻿using System;
using System.Data;
using System.Drawing;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements.Links;
using Matchnet.Content;
using Matchnet.Lib;
using Matchnet.Lib.Util;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters.TokenReplacement;
using Matchnet.PromoEngine.ValueObjects;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.Adapter;
using Spark.Common.OrderHistoryService;
using Spark.Common.DiscountService;
using Matchnet.Web.Framework.UPS;
using PrivilegeType = Spark.Common.AccessService.PrivilegeType;
using Matchnet.Web.Framework.Managers;
using RestSharp;
using Newtonsoft.Json;

namespace Matchnet.Web.Applications.Subscription.Controls
{
    public partial class PaymentHistory20 : FrameworkControl
    {
        #region Private Members

        private int _decimalPrecision = 2;
        private AccountHistoryTabEnum _activeTab = AccountHistoryTabEnum.FinancialHistory;
        private string _selectedTabClientID = String.Empty;
        private string _selectedTabContentClientID = String.Empty;

        //a la carte
        Plan _planHighlight = null;
        Plan _planSpotlight = null;
        Plan _planJMeter = null;
        Plan _planAllAccess = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseHighlight = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseSpotlight = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseJMeter = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseAllAccess = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseReadReceipt = null;

        #endregion

        #region Enum
        /// <summary>
        /// Enums for representing the various admin adjust tabs
        /// </summary>
        public enum AccountHistoryTabEnum
        {
            None,
            AllFinancialHistory,
            FinancialHistory,
            NonFinancialHistory
        }
        #endregion

        public void Populate()
		{
			Populate(_g.TargetMemberID);
		}

		public void Populate(int memberID)
		{
			try
			{
                if (g.TargetBrand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
                    _decimalPrecision = 0;

                //CurrentMemberID.Text = g.TargetMember.EmailAddress;

                // For the current subscription plan information
                Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(g.TargetMemberID, g.TargetBrand);

                List<AccountHistory> colAccountHistory = new List<AccountHistory>();

                #region OLD - MemberTran
                // For the non financial account history
                //MemberTranCollection mtc = PurchaseSA.Instance.GetMemberTransactions(g.TargetMemberID, g.TargetBrand.Site.SiteID);
                //foreach (MemberTran memberTran in mtc)
                //{
                //    if ((memberTran.TranType == TranType.AdministrativeAdjustment
                //            || memberTran.TranType == TranType.ReopenAutoRenewal
                //            || memberTran.TranType == TranType.Termination
                //            || memberTran.TranType == TranType.TrialPayAdjustment)
                //        && (memberTran.Amount == 0))
                //    {
                //        AccountHistory accountHistory = new AccountHistory(memberTran, g.TargetBrand.BrandID, AccountHistoryType.NonFinancial);
                //        colAccountHistory.Add(accountHistory);
                //    }
                //}
                #endregion

                // For Non-Financials: Terminations/Reopen-AutoRenewals
                Spark.Common.RenewalService.RenewalTransaction[] renewalTrans = Spark.Common.Adapter.RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetRenewalTransactionHistory(g.TargetMemberID, g.TargetBrand.Site.SiteID, 1, 100);
                if (renewalTrans != null && renewalTrans.Length > 0)
                {
                    foreach (Spark.Common.RenewalService.RenewalTransaction tran in renewalTrans)
                    {
                        AccountHistory accountHistory = new AccountHistory(tran, g.TargetBrand.BrandID, AccountHistoryType.NonFinancial);
                        colAccountHistory.Add(accountHistory);
                    }
                }

                // Freezing and unfreezing transaction history 
                Spark.Common.RenewalService.RenewalTransaction[] frozenRenewalTrans = Spark.Common.Adapter.RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetRenewalFrozenTransactionHistory(g.TargetMemberID, g.TargetBrand.Site.SiteID, 1, 100);
                if (frozenRenewalTrans != null && frozenRenewalTrans.Length > 0)
                {
                    foreach (Spark.Common.RenewalService.RenewalTransaction tran in frozenRenewalTrans)
                    {
                        AccountHistory accountHistory = new AccountHistory(tran, g.TargetBrand.BrandID, AccountHistoryType.NonFinancial);
                        colAccountHistory.Add(accountHistory);
                    }
                }

                // Only show the free trial information at the top of the account history only if
                // there is no renewal subscription for this member 
                Spark.Common.AccessService.AccessTransaction freeTrialAccessTransaction = null;

                // For Non-Financials: Admin Adjustments, TrialPay Adjustments
                Spark.Common.AccessService.AccessTransaction[] accessTrans = Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstanceForBedrock().GetAccessTransactionHistory(g.TargetMemberID, g.TargetBrand.Site.SiteID, 1, 100);
                if (accessTrans != null && accessTrans.Length > 0)
                {
                    foreach (Spark.Common.AccessService.AccessTransaction tran in accessTrans)
                    {
                        AccountHistory accountHistory = null;

                        if (tran.UnifiedTransactionTypeID == (int)Spark.Common.AccessService.TransactionType.GiftRedeem)
                        {
                            Gift redeemedGift = getRedeemedGift(tran.AccessTransactionID);
                            accountHistory = new AccountHistory(tran, g.Brand.BrandID, redeemedGift);
                        }
                        else
                        {
                            accountHistory = new AccountHistory(tran, g.TargetBrand.BrandID, AccountHistoryType.NonFinancial);
                        }
                        
                        colAccountHistory.Add(accountHistory);

                        if (tran.UnifiedTransactionTypeID == (int)TransactionType.PrivilegeAuthorization
                            || tran.UnifiedTransactionTypeID == (int)TransactionType.CancelFreeTrial)
                        {
                            if (freeTrialAccessTransaction == null)
                            {
                                freeTrialAccessTransaction = tran;
                            }
                            else if (freeTrialAccessTransaction.AccessTransactionDetails[0].EndDateUTC > tran.AccessTransactionDetails[0].EndDateUTC)
                            {
                                // Set to the most recent free trial taken 
                                freeTrialAccessTransaction = tran;
                            }
                        }
                    }
                }                

                // for the financial account history
                //List<OrderInfo> colOrderInfo = new List<OrderInfo>();
                OrderInfo[] arrOrderInfo = OrderHistoryServiceWebAdapter.GetProxyInstanceForBedrock().GetMemberOrderHistoryByMemberID(g.TargetMemberID, g.TargetBrand.Site.SiteID, 5000, 1);
                foreach (OrderInfo orderInfo in arrOrderInfo)
                {
                    if (orderInfo.OrderDetail.Length > 0)
                    {
                        // Only add orders that have order details  
                        AccountHistory accountHistory = new AccountHistory(orderInfo, g.TargetBrand.BrandID, AccountHistoryType.Financial);
                        colAccountHistory.Add(accountHistory);
                    }
                }
		
                /*
				if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0)
				{
                    RenewalDate.Visible = false;
                    //RenewalDate.Text = FrameworkGlobals.GetOffsetAdjustedDateDisplay(renewalSub.RenewalDatePST, g.Brand);

                    if (renewalSub.IsRenewalEnabled)
                    {
                        EndDate.Visible = false;
                        EndDateTxt.Visible = false;
                        lnkUpdate.Visible = false;
                    }
                    else
                    {
                        Spark.Common.RenewalService.RenewalTransaction terminateTransaction = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetRenewalTransactionMostRecent(g.TargetMemberID, g.TargetBrand.Site.SiteID, 5);
                        if (terminateTransaction != null && terminateTransaction.RenewalTransactionID > 0)
                            EndDate.Text = FrameworkGlobals.GetOffsetAdjustedDateDisplay(FrameworkGlobals.ConvertUTCToPST(terminateTransaction.InsertDateUTC), g.Brand);

                        if(renewalSub.RenewalDatePST > DateTime.Now)
                            {lnkUpdate.Visible=true;}
                        else
                            { lnkUpdate.Visible = false; }

                    }
				}
                */

				if (colAccountHistory != null && colAccountHistory.Count > 0)
                {
                    //create a list of orderID's and discount information 
                    List<DiscountTransaction> dts = new List<DiscountTransaction>();
                    SettingsManager settingsManager = new SettingsManager();
                    if (settingsManager.GetSettingBool("USE_ORDER_HISTORY_JSON_SERVICE", g.Brand))
                    {
                        var client = new RestClient(ConfigurationManager.AppSettings["OrderHistoryRestAuthority"]);
                        var requestString = "GetMemberOrderHistoryByMemberID" + "?MemberID=" + memberID + "&CallingSystemID=" + g.Brand.Site.SiteID + "&PageSize=5000&PageNumber=1";
                        var request = new RestRequest(requestString, Method.GET);
                        request.AddHeader("Accept", "version=V2.1");
                        var jsonResponce = client.Execute(request).Content;
                        var fixedResponce = jsonResponce.Replace("-79228162514264337593543950335", "0.0");
                        var orderHistorys = JsonConvert.DeserializeObject<List<OrderHistory>>(fixedResponce);

                        DiscountTransaction dt = null;
                        foreach (OrderHistory oh in orderHistorys)
                        {
                            dt = GetDiscountInfo(oh);
                            if(dt != null)
                                dts.Add(dt);
                        }

                        //lets update the discount code and discount amounts for all the AccountHistory
                        if (dts.Count > 0)
                        {
                            foreach (AccountHistory ah in colAccountHistory)
                            {
                                var dtResult = dts.Where(n => n.OrderId == ah.OrderID).FirstOrDefault();
                                if (dtResult != null)
                                {
                                    if (dtResult.DiscountType == 2) // discount type is a percentage 
                                        ah.DiscountAmountForDisplay = (dtResult.DiscountAmount * 100).ToString() + "%";
                                    else
                                        ah.DiscountAmountForDisplay = "$" + dtResult.DiscountAmount.ToString();

                                    ah.DiscountCode = dtResult.DiscountCode;
                                }
                            }
                        }
                    }

                    
                    //determine promoID for terminations and auto-renewal reopens
                    List<AccountHistory> sortedAccountHistoryList = GetSortedAccountHistory(colAccountHistory);
                    foreach (AccountHistory ahistory in sortedAccountHistoryList)
                    {
                        if (ahistory.TransactionType == TransactionType.AutoRenewalTerminate
                            || ahistory.TransactionType == TransactionType.AutoRenewalReopen)
                        {
                            //get promo id associated with most recent initial purchase
                            var query = (from AccountHistory ah in sortedAccountHistoryList
                                         where ah.TransactionType == TransactionType.InitialSubscriptionPurchase
                                         && ah.OrderStatus == OrderStatus.Successful
                                         && ah.PrimaryPackageID == ahistory.PrimaryPackageID
                                         && ah.InsertDateInPST < ahistory.InsertDateInPST
                                         orderby ah.InsertDateInPST descending
                                         select ah).Take(1);

                            foreach (var AccountHistory in query)
                            {
                                ahistory.PromoID = AccountHistory.PromoID;
                            }
                        }
                    }

                    AllFinancialRepeaterTransaction.DataSource = sortedAccountHistoryList;
                    AllFinancialRepeaterTransaction.DataBind();

                    #region Not needed unless we revert back to separating financial and nonfinancial transactions on separate tabs
                    //FinancialRepeaterTransaction.DataSource = GetSortedAccountHistoryOfSpecificType(colAccountHistory, AccountHistoryType.Financial);
                    //FinancialRepeaterTransaction.DataBind();

                    //NonFinancialRepeaterTransaction.DataSource = GetSortedAccountHistoryOfSpecificType(colAccountHistory, AccountHistoryType.NonFinancial);
                    //NonFinancialRepeaterTransaction.DataBind();
                    #endregion

                    PopulateSub(renewalSub, null, GetSortedAccountHistoryOfSpecificType(colAccountHistory, AccountHistoryType.Financial), freeTrialAccessTransaction);
                }
				else
				{
					g.Notification.AddError("NO_SUSCRIPTION");
                    //EndDate.Visible = false;
                    //RenewalDate.Visible = false;
					RenewalDateTxt.Visible = false;
                    //EndDateTxt.Visible = false;
                    NonFinancialRepeaterTransaction.Visible = false;
                    //FinancialAccountHistoryRepeaterTransaction.Visible = false;
                    //CurrentMemberID.Visible = false;
                    //TransactionRecordTxt.Visible = false;
                    //CurrentPlanTxt.Visible = false;
                    //txtUpdate.Visible = false;
				}

				EnableBuyOneClick();
				EnablePaymentProfileView();

					
			}
			catch(Exception ex)
			{
				_g.ProcessException(ex);
			}
		}


        private List<AccountHistory> GetSortedAccountHistoryOfSpecificType(List<AccountHistory> colAccountHistory, AccountHistoryType accountHistoryType)
        {
            List<AccountHistory> colSortedAccountHistory = new List<AccountHistory>();
            foreach (AccountHistory accountHistory in colAccountHistory)
            {
                if (accountHistory.AccountHistoryType == accountHistoryType)
                {
                    colSortedAccountHistory.Add(accountHistory);
                }
            }

            colSortedAccountHistory.Sort((x, y) => (y.InsertDateInPST.CompareTo(x.InsertDateInPST)));
            return colSortedAccountHistory;
        }

        private List<AccountHistory> GetSortedAccountHistory(List<AccountHistory> colAccountHistory)
        {
            colAccountHistory.Sort((x, y) => (y.InsertDateInPST.CompareTo(x.InsertDateInPST)));
            return colAccountHistory;
        }

        private DiscountTransaction GetDiscountInfo(OrderHistory oh)
        {
            DiscountTransaction dt = null;

            foreach (OrderDetail od in oh.OrderDetail)
            {
                if (od.DiscountAmount > 0.0 && !String.IsNullOrEmpty(od.DiscountCode))
                {
                    dt = new DiscountTransaction();
                    dt.DiscountAmount = od.DiscountAmount;
                    dt.DiscountCode = od.DiscountCode;
                    dt.DiscountType = od.DiscountType;
                    dt.OrderId = oh.OrderID;
                    break;
                }
            }

            return dt;
        }


		private void Page_Init(object sendere, System.EventArgs e)
		{
			try
			{
				Thread.CurrentThread.CurrentCulture = new CultureInfo(g.Brand.Site.CultureInfo.LCID, false);
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {

                #region Set Selected Tab
                //get active tab
                this._activeTab = this.DetermineActiveTab();
                this.hfAdjustTab.Value = this._activeTab.ToString(); //set active tab into hidden field, this field is updated via javascript to track active tab from client side

                //hide all content areas
                //TODO: assign css class
                this.pnlAllFinancialHistoryContent.Style.Add("display", "none");
                this.pnlFinancialHistoryContent.Style.Add("display", "none");
                this.pnlNonFinancialHistoryContent.Style.Add("display", "none");

                switch (this._activeTab)
                {
                    case AccountHistoryTabEnum.AllFinancialHistory:
                        this._selectedTabClientID = this.tabAllFinancialHistory.ClientID;
                        this._selectedTabContentClientID = this.pnlAllFinancialHistoryContent.ClientID;
                        this.pnlAllFinancialHistoryContent.Style.Remove("display");
                        this.pnlAllFinancialHistoryContent.Style.Add("display", "block");
                        break;
                    case AccountHistoryTabEnum.FinancialHistory:
                        this._selectedTabClientID = this.tabFinancialHistory.ClientID;
                        this._selectedTabContentClientID = this.pnlFinancialHistoryContent.ClientID;
                        this.pnlFinancialHistoryContent.Style.Remove("display");
                        this.pnlFinancialHistoryContent.Style.Add("display", "block");
                        break;
                    case AccountHistoryTabEnum.NonFinancialHistory:
                        this._selectedTabClientID = this.tabNonFinancialHistory.ClientID;
                        this._selectedTabContentClientID = this.pnlNonFinancialHistoryContent.ClientID;
                        this.pnlNonFinancialHistoryContent.Style.Remove("display");
                        this.pnlNonFinancialHistoryContent.Style.Add("display", "block");
                        break;
                }

                #endregion

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //register JS
            Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "AccountHistoryTabNavs", "<script type=\"text/javascript\">" +
                "AccountHistoryObject.currentTab = '" + this._activeTab.ToString() + "';" +
                "AccountHistoryObject.currentTabID = '" + this._selectedTabClientID + "';" +
                "AccountHistoryObject.currentTabContentID = '" + this._selectedTabContentClientID + "';" +
                "AccountHistoryObject.allFinancialHistoryTabContentID = '" + this.pnlAllFinancialHistoryContent.ClientID + "';" +
                "AccountHistoryObject.financialHistoryTabContentID = '" + this.pnlFinancialHistoryContent.ClientID + "';" +
                "AccountHistoryObject.nonFinancialHistoryTabContentID = '" + this.pnlNonFinancialHistoryContent.ClientID + "';" +
                "AccountHistoryObject.allFinancialHistoryTabID = '" + this.tabAllFinancialHistory.ClientID + "';" +
                "AccountHistoryObject.financialHistoryTabID = '" + this.tabFinancialHistory.ClientID + "';" +
                "AccountHistoryObject.nonFinancialHistoryTabID = '" + this.tabNonFinancialHistory.ClientID + "';" +
                "AccountHistoryObject.tabTrackerID = '" + this.hfAdjustTab.ClientID + "';" +
                "</script>");

            //set active tab css
            switch (this._activeTab)
            {
                case AccountHistoryTabEnum.AllFinancialHistory:
                    this.tabAllFinancialHistory.Attributes.Add("class", "selected");
                    break;
                case AccountHistoryTabEnum.FinancialHistory:
                    this.tabFinancialHistory.Attributes.Add("class", "selected");
                    break;
                case AccountHistoryTabEnum.NonFinancialHistory:
                    this.tabNonFinancialHistory.Attributes.Add("class", "selected");
                    break;
            }

            /*
            //Credit tab only available for sites supporting "credit" payment type
            if ((g.Brand.Site.PaymentTypeMask & ConstantsTemp.PAYMENT_TYPE_CREDITCARD) == 0)
            {
                this.phBuyCredit.Visible = false;
                this.pnlBuyByCreditCard.Visible = false;

                this.phBuyAdminOnlyCredit.Visible = false;
                this.pnlBuyAdminOnlyCredit.Visible = false;
            }

            //09172008 TL QAT-161 - Check tab only available to sites supporting "check" payment type
            if ((g.Brand.Site.PaymentTypeMask & ConstantsTemp.PAYMENT_TYPE_CHECK) == 0)
            {
                this.phBuyCheck.Visible = false;
                this.pnlBuyByCheck.Visible = false;
            }
            */

            base.OnPreRender(e);
        }

        /// <summary>
        /// Gets the active tab based on URL parameter, if any, or returns default active tab
        /// </summary>
        /// <returns>AccountHistoryTabEnum</returns>
        private AccountHistoryTabEnum DetermineActiveTab()
        {
            // Default tab
            AccountHistoryTabEnum activeTab = AccountHistoryTabEnum.AllFinancialHistory;

            string sTab = "";
            if (Request.Form[this.hfAdjustTab.UniqueID] != null)
                sTab = Request.Form[this.hfAdjustTab.UniqueID];
            else if (Request.QueryString[WebConstants.URL_PARAMETER_NAME_ACCOUNTHISTORYTAB] != null)
                sTab = Request.QueryString[WebConstants.URL_PARAMETER_NAME_ACCOUNTHISTORYTAB];

            if (sTab != "")
            {
                //get active tab from url querystring
                try
                {
                    activeTab = (AccountHistoryTabEnum)System.Enum.Parse(typeof(AccountHistoryTabEnum), sTab, true);

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }
            return activeTab;
        }

		private void EnablePaymentProfileView()
		{
            bool allowChangeToBillingInformation = false;

            if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", g.Brand.Site.Community.CommunityID,
                g.Brand.Site.SiteID, g.Brand.BrandID))))
            {
                if (g.Member != null && MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                {
                    allowChangeToBillingInformation = true;
                }
                else
                {
                    if (FrameworkGlobals.IfMemberHasSubscribed(g))
                    {
                        allowChangeToBillingInformation = true;
                    }
                }
            }
            else
            {
                allowChangeToBillingInformation = SubscriptionHelper.IsPaymentProfileEditable(g);
            }

            if (allowChangeToBillingInformation)
			{
				Framework.Ui.FormElements.FrameworkButton frameworkButton = new Framework.Ui.FormElements.FrameworkButton();
				frameworkButton.CssClass = "btn secondary activityButton";
				frameworkButton.ResourceConstant = "VIEW_PAYMENT_PROFILE_BUTTON";

                if (Matchnet.Web.Framework.Managers.SubscriberManager.Instance.IsIAPSubscriber(g.Member, g.Brand))
                    frameworkButton.Click += new System.EventHandler(this.RedirectToIAPPage);
                else
				    frameworkButton.Click += new System.EventHandler(this.PaymentProfileView_Click);
                
                
                plhPaymentProfileView.Controls.Add(frameworkButton);
			} 
		}

        /// <summary>
        /// is subscribed by IAP take them to the jump page. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void RedirectToIAPPage(object sender, EventArgs e)
        {
            if (g != null)
            {
                if (g.Brand.Site.SiteID == (int)Matchnet.Web.Framework.WebConstants.SITE_ID.JDateCoIL)
                    g.Transfer("http://static.jdate.com/Components/IAP/IL");
                else if (g.Brand.Site.SiteID == (int)Matchnet.Web.Framework.WebConstants.SITE_ID.JDateFR)
                    g.Transfer("http://static.jdate.com/Components/IAP/FR");
                else
                    g.Transfer("http://static.jdate.com/Components/IAP");
            }
        }

		public void PaymentProfileView_Click(object sender, System.EventArgs e)
		{
			try
			{
				string transferUrl = "/Applications/BillingInformation/PaymentProfile.aspx?";
				transferUrl = transferUrl + WebConstants.IMPERSONATE_MEMBERID + "=" + g.TargetMemberID;
				transferUrl = transferUrl + "&" + WebConstants.IMPERSONATE_BRANDID + "=" + g.TargetBrandID;

				// If MemberID and TargetMemberID are different, we know an Admin is modifying someone else's record
				// disable the top, left, bottom nav's
				if(g.TargetMemberID != g.Member.MemberID)
				{
					transferUrl += "&" + WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID + "=" + WebConstants.PT_POPUP_ONCE;
				}
				
				g.Transfer(FrameworkGlobals.LinkHrefSSL(transferUrl));
			}
			catch(System.Threading.ThreadAbortException)
			{
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void EnableBuyOneClick()
		{
			if (g.Brand.Site.PaymentTypeMask != ConstantsTemp.PAYMENT_TYPE_NONE)
			{
				if ( MemberPrivilegeAttr.IsCureentSubscribedMember(g) )
				{
                    int memberPaymentID = Constants.NULL_INT;// PurchaseSA.Instance.GetSuccessfulPayment(g.TargetMemberID, g.TargetBrand.Site.SiteID);

					if ( memberPaymentID != Constants.NULL_INT )
					{
						SubscribeCommon subscribeCommon = new SubscribeCommon();
						mpid.Value = subscribeCommon.EncryptNumber(memberPaymentID);
					}
				}

				Framework.Ui.FormElements.FrameworkButton frameworkButton = new Framework.Ui.FormElements.FrameworkButton();
				frameworkButton.CssClass = "activityButton";
				frameworkButton.ResourceConstant = "CHANGE_EXTEND_YOUR_SUBSCRIPTION_PLAN_519668";
				frameworkButton.Click += new System.EventHandler(this.Renew_Click);
				plhBuyOneClick.Controls.Add(frameworkButton);
			}
		}

		public void Renew_Click(object sender, System.EventArgs e)
		{
			try
			{
				if ( !g.Page.IsValid )
				{ 
					g.Notification.AddErrorString("Invalid form data");
					return;
				}

				SubscribeCommon subscribeCommon = new SubscribeCommon();
				int memberPaymentID  = 0;

				if ( mpid.Value != null && mpid.Value.Length > 0 )
				{
					memberPaymentID = subscribeCommon.GetDecryptedValue(mpid.Value);
				}

                //09162008 TL ET-92, In modifying our application to redirect requests for Renewal to Subscribe page and for ESP project,
                //there should be no impersonation on the Renewal page (not supported).
                if (!Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(WebConstants.SETTING_ENABLE_RENEWAL_TO_SUBSCRIBE_REDIRECT, _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID, _g.Brand.BrandID))
                    && memberPaymentID != 0 && (!g.ImpersonateContext)
                    )
                {
                    //Renewal page
                    string rawRenewUrl = "/Applications/Subscription/Renewal.aspx?r1=" + subscribeCommon.EncryptAndEncodeNumber(memberPaymentID)
                        + "&PRTID=82";

                    g.Transfer(FrameworkGlobals.LinkHrefSSL(rawRenewUrl));
                }
                else
                {
                    //Subscribe page
                    Matchnet.Web.Framework.Util.Redirect.Subscription(g.Brand, 82, Constants.NULL_INT, true);
                }
			}
			catch(System.Threading.ThreadAbortException)
			{
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}

        private void PopulateSub(Spark.Common.RenewalService.RenewalSubscription renewalSub, MemberTranCollection mtc, List<AccountHistory> colAccountHistory, Spark.Common.AccessService.AccessTransaction freeTrialAccessTransaction)
		{
            if (renewalSub != null)
			{
                if (renewalSub.RenewalDateUTC <= DateTime.UtcNow)
                {
                    // Renewal subscription has expired and turned off 
                    txtSubscribeLink.Href = FrameworkGlobals.GetSubscriptionLink();

                    phlFreeTrialInformation.Visible = false;
                    phlRenewalInformation.Visible = false;
                    phlDisabledRenewal.Visible = false;
                    phlNoRenewals.Visible = true;
                    return;
                }
                else if (!renewalSub.IsRenewalEnabled)
                {
                    // Renewal subscription has not expired yet but instead has been turned off 
                    DisabledRenewalEndDate.Text = FrameworkGlobals.GetOffsetAdjustedDateDisplay(renewalSub.RenewalDatePST, g.Brand);

                    phlFreeTrialInformation.Visible = false;
                    phlRenewalInformation.Visible = false;
                    phlDisabledRenewal.Visible = true;
                    phlNoRenewals.Visible = false;
                    return;                    
                }
                
			    //int promoID = Constants.NULL_INT;

                ////get promo id associated with most recent initial purchase
                //var query = (from AccountHistory ah in colAccountHistory
                //             where ah.TransactionType == TransactionType.InitialSubscriptionPurchase
                //             && ah.OrderStatus == OrderStatus.Successful
                //             && ah.PrimaryPackageID == renewalSub.PrimaryPackageID
                //             orderby ah.InsertDateInPST descending
                //             select ah).Take(1);

                //foreach (var AccountHistory in query)
                //{
                //    promoID = AccountHistory.PromoID;
                //}

                //if (promoID < 0)
                //{
                //    promoID = 0;
                //}

                RenewalDate.Text = FrameworkGlobals.GetOffsetAdjustedDateDisplay(renewalSub.RenewalDatePST, g.Brand);

                Matchnet.Purchase.ValueObjects.Plan plan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, g.TargetBrand.BrandID);
			    bool isBundledPackage = false;
                int primaryPackagRenewalDurationInMonths = plan.RenewDuration;
			    decimal totalRenewalAmount = 0.0m;

                //CurrentPlan.Text = GetResource(promoID, renewalSub.PrimaryPackageID, g.TargetBrand.BrandID, plan.PlanResourceConstant);

                List<Spark.Common.AccessService.PrivilegeType> AutoRenewalOnActivePrivilegeTypeList = new List<PrivilegeType>();
                //AutoRenewalOnActivePrivilegeTypeList.Add((PrivilegeType.BasicSubscription));
                List<Spark.Common.AccessService.PrivilegeType> AutoRenewalOffActivePrivilegeTypeList = new List<PrivilegeType>();

                AutoRenewalOnActivePrivilegeTypeList.Add((PrivilegeType.BasicSubscription));                    

                // Check the primary package
                if (plan.PremiumTypeMask != PremiumType.None)
                {
                    // Bundled plan
                    isBundledPackage = true;
                }

			    totalRenewalAmount += plan.RenewCost;

                if (renewalSub.RenewalSubscriptionDetails.Length > 1)
                {
                    Plan rsePlan = null;
                    bool addToRenewalCost = false;

                    foreach (Spark.Common.RenewalService.RenewalSubscriptionDetail rse in renewalSub.RenewalSubscriptionDetails)
                    {
                        if (!rse.IsPrimaryPackage)
                        {
                            rsePlan = PlanSA.Instance.GetPlan(rse.PackageID, g.TargetBrand.BrandID);
                            addToRenewalCost = false;

                            if ((rsePlan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
                            {
                                if (rse.IsRenewalEnabled)
                                {
                                    if (!AutoRenewalOnActivePrivilegeTypeList.Contains(PrivilegeType.HighlightedProfile))
                                    {
                                        AutoRenewalOnActivePrivilegeTypeList.Add(PrivilegeType.HighlightedProfile);
                                    }
                                    addToRenewalCost = true;
                                }
                                else
                                {
                                    AutoRenewalOffActivePrivilegeTypeList.Add(PrivilegeType.HighlightedProfile);
                                }

                                //_planHighlight = rsePlan;
                                //_rseHighlight = rse;
                                //phlALaCarteHighlight.Visible = true;
                                //phlFooterBlurbALaCarte.Visible = true;
                            }

                            if ((rsePlan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                            {
                                if (rse.IsRenewalEnabled)
                                {
                                    if (!AutoRenewalOnActivePrivilegeTypeList.Contains(PrivilegeType.SpotlightMember))
                                    {
                                        AutoRenewalOnActivePrivilegeTypeList.Add(PrivilegeType.SpotlightMember);
                                    }
                                    addToRenewalCost = true;
                                }
                                else
                                {
                                    AutoRenewalOffActivePrivilegeTypeList.Add(PrivilegeType.SpotlightMember);
                                }

                                //_planSpotlight = rsePlan;
                                //_rseSpotlight = rse;
                                //phlALaCarteSpotlight.Visible = true;
                                //phlFooterBlurbALaCarte.Visible = true;
                            }

                            if ((rsePlan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
                            {
                                if (rse.IsRenewalEnabled)
                                {
                                    if (!AutoRenewalOnActivePrivilegeTypeList.Contains(PrivilegeType.JMeter))
                                    {
                                        AutoRenewalOnActivePrivilegeTypeList.Add(PrivilegeType.JMeter);
                                    }
                                    addToRenewalCost = true;
                                }
                                else
                                {
                                    AutoRenewalOffActivePrivilegeTypeList.Add(PrivilegeType.JMeter);
                                }

                                //091310 TL
                                //TL: Noticed that this member's auto-renewal page does not have JMeter option for user to turn off.
                                //I will not add that to the UI for user since I didn't see it here originally
                                //but the admin's page does so I added it here to support UPS Renewal changes so it does not disable JMeter
                                //_planJMeter = rsePlan;
                                //_rseJMeter = rse;
                            }

                            if ((rsePlan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                            {
                                if (rse.IsRenewalEnabled)
                                {
                                    if (!AutoRenewalOnActivePrivilegeTypeList.Contains(PrivilegeType.AllAccess))
                                    {
                                        AutoRenewalOnActivePrivilegeTypeList.Add(PrivilegeType.AllAccess);
                                    }
                                    addToRenewalCost = true;
                                }
                                else
                                {
                                    AutoRenewalOffActivePrivilegeTypeList.Add(PrivilegeType.AllAccess);
                                }

                                //_planAllAccess = rsePlan;
                                //_rseAllAccess = rse;
                                //phALaCarteAllAccess.Visible = true;
                                //phlFooterBlurbALaCarte.Visible = true;
                            }

                            if ((rsePlan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
                            {
                                if (rse.IsRenewalEnabled)
                                {
                                    if (!AutoRenewalOnActivePrivilegeTypeList.Contains(PrivilegeType.ReadReceipt))
                                    {
                                        AutoRenewalOnActivePrivilegeTypeList.Add(PrivilegeType.ReadReceipt);
                                    }
                                    addToRenewalCost = true;
                                }
                                else
                                {
                                    AutoRenewalOffActivePrivilegeTypeList.Add(PrivilegeType.ReadReceipt);
                                }
                            }

                            if (addToRenewalCost)
                            {
                                totalRenewalAmount += rsePlan.RenewCost * (primaryPackagRenewalDurationInMonths / rsePlan.RenewDuration);
                            }
                        }
                    }
                }

                litRenewalAmount.Text = Convert.ToString(totalRenewalAmount).Trim();


                // Display renewal duration
                string resource = string.Empty;
                string resourceConstantToUse = string.Empty;
                if (primaryPackagRenewalDurationInMonths == 1)
                {
                    resourceConstantToUse = "SITE_PLAN_MONTLY_RENEWAL_ADD_TEMPLATE_DEFAULT";
                    resource = g.GetResource(resourceConstantToUse, this);
                }
                else
                {
                    resourceConstantToUse = "SITE_PLAN_MULTI_MONTH_RENEWAL_ADD_TEMPLATE_DEFAULT";
                    resource = String.Format(g.GetResource(resourceConstantToUse, this), primaryPackagRenewalDurationInMonths);
                }

                txtPerMonth.Text = resource;

			    string strAutoRenewalOnActivePrivilegeTypeList = GetAccessPrivilegeList(AutoRenewalOnActivePrivilegeTypeList, isBundledPackage);
                string strAutoRenewalOffActivePrivilegeTypeList = GetAccessPrivilegeList(AutoRenewalOffActivePrivilegeTypeList, isBundledPackage);

                if (!String.IsNullOrEmpty(strAutoRenewalOnActivePrivilegeTypeList))
                {
                    txtAutoRenewalOnActivePrivilegeTypeList.Text = String.Format(g.GetResource("AUTORENEWAL_ON_PRIVILEGETYPELIST", this), strAutoRenewalOnActivePrivilegeTypeList);                    
                }
                else
                {
                    txtAutoRenewalOnActivePrivilegeTypeList.Visible = false;
                }

                if (!String.IsNullOrEmpty(strAutoRenewalOffActivePrivilegeTypeList))
                {
                    txtAutoRenewalOffActivePrivilegeTypeList.Text = String.Format(g.GetResource("AUTORENEWAL_OFF_PRIVILEGETYPELIST", this), strAutoRenewalOffActivePrivilegeTypeList);
                }
                else
                {
                    txtAutoRenewalOffActivePrivilegeTypeList.Visible = false;
                }

			    //if (ms.DiscountID > 0 && ms.DiscountRemaining  > 0)
			    //{
			    //    DiscountPanel.Visible = true;
			    //    CurrentDiscount.Text = g.GetResource(ms.DiscountResourceConstant, null);
			    //}
			}
            else if (freeTrialAccessTransaction != null)
            {
                phlRenewalInformation.Visible = false;
                phlFreeTrialInformation.Visible = true;
                phlDisabledRenewal.Visible = false;
                phlNoRenewals.Visible = false;

                CurrentPlan.Text = g.GetResource("FREETRIAL_PLAN", this);
                lblFreeTrialCaptureDate.Text = FrameworkGlobals.GetOffsetAdjustedDateDisplay(new AccountHistory().ConvertUTCToPST(freeTrialAccessTransaction.AccessTransactionDetails[0].EndDateUTC), g.Brand);
            }
            else
            {
                // Renewal subscription doesn't exists or it has been archived 
                phlRenewalInformation.Visible = false;
                phlFreeTrialInformation.Visible = false;
                phlDisabledRenewal.Visible = false;
                phlNoRenewals.Visible = true;

                //CurrentPlan.Visible = false;
                //CurrentPlanTxt.Visible = false;
                //EndDate.Visible = false;
                //RenewalDate.Visible = false;
                //RenewalDateTxt.Visible = false;
                //EndDateTxt.Visible = false;
            }						
		}

        /// <summary>
        /// Looks through the MemberTransactionCollection to find the matching OrderID to the MemberSub object.
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="mtc"></param>
        /// <returns>Constants.NULL_INT if OrderID is not found and OrderID if found</returns>
        private int GetOrderIDForMemberSub(MemberSub ms, MemberTranCollection mtc)
        {
            int orderID = Constants.NULL_INT;

            if (ms != null && mtc != null)
            {
                List<TranType> tranTypes = new List<TranType>();
                tranTypes.Add(Matchnet.Purchase.ValueObjects.TranType.InitialBuy);
                //tranTypes.Add(Matchnet.Purchase.ValueObjects.TranType.Renewal);
                // Jay told me to comment this out
				// tranTypes.Add(Matchnet.Purchase.ValueObjects.TranType.AdministrativeAdjustment);

                var query = (from MemberTran mt in mtc
                             where mt.MemberSubID == ms.MemberSubID &&
                             mt.MemberTranStatus == Matchnet.Purchase.ValueObjects.MemberTranStatus.Success &&
                             mt.PlanID == ms.PlanID &&
                             tranTypes.Contains(mt.TranType)
                             orderby mt.InsertDate descending
                             select mt).Take(1);

                foreach (var memberTran in query)
                {
                    orderID = memberTran.OrderID;
                }
            }

            return orderID;
        }
	
		
        /// <summary>
        /// Looks at the Promo object to see if a plan resource can be obtained from there.  If this fails, it tries to get it from a resource file.  If this also
        /// fails, then the backup resource key reserved for site default plan template is used.
        /// </summary>
        /// <param name="promoID"></param>
        /// <param name="planID"></param>
        /// <param name="targetBrandID"></param>
        /// <param name="resourceKey"></param>
        /// <returns></returns>
        private string GetResource(int promoID, int planID, int targetBrandID, string resourceKey)
        {
            string resource = string.Empty;
            PlanTokenReplacer tokenRepl = null;
            string resourceRenewalPriceAdditional = string.Empty;

            Matchnet.Purchase.ValueObjects.Plan plan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(planID, targetBrandID);

            #region Go to the database for it first
            if (promoID != Constants.NULL_INT)
            {
                Promo promo = PromoEngineSA.Instance.GetPromoByID(promoID, targetBrandID);
                if (promo != null && PromoEngineSA.Instance.IsPromoTemplateDriven(promo.PromoID, g.TargetBrand.BrandID))
                {
                    if (planID != Constants.NULL_INT)
                    {
                        PromoPlan promoPlan = promo.PromoPlans.FindByID(planID);
                        if (promoPlan != null)
                        {
                            resource = ResourceTemplateSA.Instance.GetResource(promoPlan.ResourceTemplateID,
                                new PlanTokenReplacer(promoPlan, _decimalPrecision));

                            if (!String.IsNullOrEmpty(resource))
                            {
                                if (resource.ToLower().IndexOf("renewal") < 0)
                                {
                                    string resourceConstantToUse = string.Empty;
                                    if (plan.RenewDuration == 1
                                        && plan.RenewDurationType == DurationType.Month)
                                    {
                                        resourceConstantToUse = "SITE_PLAN_MONTLY_RENEWAL_ADD_TEMPLATE_DEFAULT";
                                    }
                                    else
                                    {
                                        resourceConstantToUse = "SITE_PLAN_MULTI_MONTH_RENEWAL_ADD_TEMPLATE_DEFAULT";
                                    }

                                    tokenRepl = new PlanTokenReplacer(promoPlan, (g.TargetBrand.Site.LanguageID == (int)Matchnet.Language.Hebrew) ? 0 : 2);
                                    resourceRenewalPriceAdditional = tokenRepl.ReplaceTokens(g.GetResource(resourceConstantToUse, null));

                                    resource = resource.Replace("</div>", resourceRenewalPriceAdditional + "</div>");
                                    //resource = resource + resourceRenewalPriceAdditional;
                                }

                            }

                        }
                    }
                }
            }
            #endregion

            // Go to the resource file with the plan resource key
            if (resource == string.Empty && resourceKey != Constants.NULL_STRING)
                resource = g.GetResource(resourceKey, null);

            #region Use the site default template here
            if ((resource == string.Empty && planID != Constants.NULL_INT)
                || (plan != null && plan.PlanTypeMask == PlanType.ALaCarte))
            {
                PromoPlan promoPlan = new PromoPlan(plan);
                tokenRepl = new PlanTokenReplacer(promoPlan, (g.TargetBrand.Site.LanguageID == (int)Matchnet.Language.Hebrew) ? 0 : 2);
                resource = tokenRepl.ReplaceTokens(g.GetResource("SITE_PLAN_TEMPLATE_DEFAULT", null));
            }
            #endregion

            return resource;
        }

        private Gift getRedeemedGift(int accesstransactionID)
        {
            Gift gift = null;

            try
            {
                gift = Spark.Common.Adapter.DiscountServiceWebAdapter.GetProxyInstanceForBedrock().GetGiftByAccessTransactionID(accesstransactionID);
            }
            finally
            {
                Spark.Common.Adapter.DiscountServiceWebAdapter.CloseProxyInstance();
            }

            return gift;

        }

        public void showItem(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
                {
                    AccountHistory accountHistory = (AccountHistory)e.Item.DataItem;

                    Label insertDateText = null;
                    if (e.Item.Parent.ID == "FinancialRepeaterTransaction")
                    {
                        insertDateText = (Label)e.Item.FindControl("FinancialInsertDate");
                    }
                    else if (e.Item.Parent.ID == "NonFinancialRepeaterTransaction")
                    {
                        insertDateText = (Label)e.Item.FindControl("NonFinancialInsertDate");
                    }
                    else if (e.Item.Parent.ID == "AllFinancialRepeaterTransaction")
                    {
                        insertDateText = (Label)e.Item.FindControl("AllFinancialInsertDate");
                    }
                    insertDateText.Text = FrameworkGlobals.GetOffsetAdjustedDateDisplay(accountHistory.InsertDateInPST, g.Brand);

                    Label tranTypeText = null;
                    if (e.Item.Parent.ID == "FinancialRepeaterTransaction")
                    {
                        tranTypeText = (Label)e.Item.FindControl("FinancialTranType");
                    }
                    else if (e.Item.Parent.ID == "NonFinancialRepeaterTransaction")
                    {
                        tranTypeText = (Label)e.Item.FindControl("NonFinancialTranType");
                    }
                    else if (e.Item.Parent.ID == "AllFinancialRepeaterTransaction")
                    {
                        tranTypeText = (Label)e.Item.FindControl("AllFinancialTranType");
                    }
                    tranTypeText.Text = g.GetResource(accountHistory.TransactionTypeResourceConstant, null);

                    Label accountText = null;
                    Link viewLink = null;
                    if (e.Item.Parent.ID == "FinancialRepeaterTransaction")
                    {
                        accountText = (Label)e.Item.FindControl("FinancialAccount");
                        viewLink = (Link)e.Item.FindControl("FinancialView");
                    }
                    else if (e.Item.Parent.ID == "NonFinancialRepeaterTransaction")
                    {
                        accountText = (Label)e.Item.FindControl("NonFinancialAccount");
                        viewLink = (Link)e.Item.FindControl("NonFinancialView");
                    }
                    else if (e.Item.Parent.ID == "AllFinancialRepeaterTransaction")
                    {
                        accountText = (Label)e.Item.FindControl("AllFinancialAccount");
                        viewLink = (Link)e.Item.FindControl("AllFinancialView");
                    }

                    bool accountNumberExists = false;
                    if (accountHistory.LastFourAccountNumber != null && accountHistory.LastFourAccountNumber.Length > 0)
                    {
                        accountNumberExists = true;
                        accountText.Text = accountHistory.LastFourAccountNumber;
                    }

                    if (accountNumberExists)
                    {
                        viewLink.Visible = false;
                        if (g.HasPrivilege(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), (int)Matchnet.Member.ValueObjects.Privilege.PrivilegeType.PRIVILEGE_VIEW_CREDITCARD))
                        {
                            //only allow to view for transaction that requires going to payment providers.
                            if (TransactionType.InitialSubscriptionPurchase == accountHistory.TransactionType
                                || TransactionType.PrivilegeAuthorization == accountHistory.TransactionType
                                || TransactionType.Renewal == accountHistory.TransactionType
                              )
                            {
                                viewLink.Visible = true;
                                //viewLink.NavigateUrl = "/Applications/Subscription/Subscribe.aspx?OverrideTransactionHistory=1&chargeview=1&plid=" + accountHistory.CallingSystemID.ToString() + "&tran=" + accountHistory.OrderID.ToString() + "&" + WebConstants.IMPERSONATE_MEMBERID + "=" + g.TargetMemberID.ToString();
                                viewLink.NavigateUrl = "javascript:launchWindow('" + FrameworkGlobals.LinkHrefSSL("/Applications/Subscription/HistoryPaymentProfile.aspx?" +
                                WebConstants.URL_PARAMETER_NAME_SITEID + "=" + accountHistory.CallingSystemID.ToString() +
                                "&" + WebConstants.URL_PARAMETER_NAME_TRANID + "=" + accountHistory.OrderID.ToString() +
                                "&" + WebConstants.IMPERSONATE_MEMBERID + "=" + g.TargetMemberID.ToString() +
                                "&" + WebConstants.URL_PARAMETER_NAME_ORDERID + "=" + accountHistory.OrderID.ToString() +
                                "&LayoutTemplateID=" + (int)Matchnet.Content.ValueObjects.PageConfig.LayoutTemplate.Popup) +
                                "','Transaction Payment Profile', 500, 632, '')";
                            }
                        }
                    }

                    Label paymentTypeText = null;
                    if (e.Item.Parent.ID == "FinancialRepeaterTransaction")
                    {
                        paymentTypeText = (Label)e.Item.FindControl("FinancialPaymentType");
                    }
                    else if (e.Item.Parent.ID == "NonFinancialRepeaterTransaction")
                    {
                        paymentTypeText = (Label)e.Item.FindControl("NonFinancialPaymentType");
                    }
                    else if (e.Item.Parent.ID == "AllFinancialRepeaterTransaction")
                    {
                        paymentTypeText = (Label)e.Item.FindControl("AllFinancialPaymentType");
                    }
                    if (accountHistory.PaymentTypeResourceConstant != null && accountHistory.PaymentTypeResourceConstant != String.Empty)
                    {
                        paymentTypeText.Text = g.GetResource(accountHistory.PaymentTypeResourceConstant, null);
                    }
                    else
                    {
                        paymentTypeText.Text = "&nbsp;";
                    }

                    Label memberTranStatusLabel = null;
                    if (e.Item.Parent.ID == "FinancialRepeaterTransaction")
                    {
                        memberTranStatusLabel = (Label)e.Item.FindControl("FinancialMemberTranStatus");
                    }
                    else if (e.Item.Parent.ID == "NonFinancialRepeaterTransaction")
                    {
                        memberTranStatusLabel = (Label)e.Item.FindControl("NonFinancialMemberTranStatus");
                    }
                    else if (e.Item.Parent.ID == "AllFinancialRepeaterTransaction")
                    {
                        memberTranStatusLabel = (Label)e.Item.FindControl("AllFinancialMemberTranStatus");
                    }
                    memberTranStatusLabel.Text = g.GetResource(accountHistory.OrderStatusResourceConstant, null);

                    Label adminEmail = (Label)e.Item.FindControl("NonFinancialAdminEmail");
                    if (e.Item.Parent.ID == "FinancialRepeaterTransaction")
                    {
                        adminEmail = (Label)e.Item.FindControl("FinancialAdminEmail");
                    }
                    else if (e.Item.Parent.ID == "NonFinancialRepeaterTransaction")
                    {
                        adminEmail = (Label)e.Item.FindControl("NonFinancialAdminEmail");
                    }
                    else if (e.Item.Parent.ID == "AllFinancialRepeaterTransaction")
                    {
                        adminEmail = (Label)e.Item.FindControl("AllFinancialAdminEmail");
                    }
                    if ((accountHistory.AdminID != Constants.NULL_INT && accountHistory.AdminID > 0) && g.IsAdmin && g.Member != null)
                    {
                        adminEmail.Text = MemberSA.Instance.GetMember(accountHistory.AdminID, MemberLoadFlags.None).EmailAddress;
                    }
                    else
                    {
                        if (e.Item.Parent.ID == "FinancialRepeaterTransaction")
                        {
                            ((PlaceHolder)(e.Item.FindControl("plFinancialAdminEmail"))).Visible = false;
                        }
                        else if (e.Item.Parent.ID == "NonFinancialRepeaterTransaction")
                        {
                            ((PlaceHolder)(e.Item.FindControl("plNonFinancialAdminEmail"))).Visible = false;
                        }
                        else if (e.Item.Parent.ID == "AllFinancialRepeaterTransaction")
                        {
                            ((PlaceHolder)(e.Item.FindControl("plAllFinancialAdminEmail"))).Visible = false;
                        }
                    }

                    PlaceHolder phlAllFinancialPlan = (PlaceHolder)e.Item.FindControl("phlAllFinancialPlan");
                    if (accountHistory.TransactionType == TransactionType.Adjustment
                        || accountHistory.TransactionType == TransactionType.RenewalRecyclingTimeAdjustment
                        || accountHistory.TransactionType == TransactionType.AutoRenewalReopenALaCarte
                        || accountHistory.TransactionType == TransactionType.AutoRenewalTerminateALaCarte)
                    {
                        phlAllFinancialPlan.Visible = false;
                    }
                    else
                    {
                        phlAllFinancialPlan.Visible = false;
                        Literal planText = null;
                        if (e.Item.Parent.ID == "FinancialRepeaterTransaction")
                        {
                            planText = (Literal)e.Item.FindControl("FinancialPlanDescription");
                        }
                        else if (e.Item.Parent.ID == "NonFinancialRepeaterTransaction")
                        {
                            planText = (Literal)e.Item.FindControl("NonFinancialPlanDescription");
                        }
                        else if (e.Item.Parent.ID == "AllFinancialRepeaterTransaction")
                        {
                            planText = (Literal)e.Item.FindControl("AllFinancialPlanDescription");
                        }
                        planText.Text = GetResource(accountHistory.PromoID, accountHistory.PrimaryPackageID, g.TargetBrand.BrandID, accountHistory.PackageResourceConstant);

                        if (accountHistory.IsOneOffPurchase)
                        {
                            planText.Text += String.Format("{0:#0.00}", accountHistory.TotalAmount);
                            phlAllFinancialPlan.Visible = true;
                        }
                        else if (accountHistory.IsFreeTrialSubscription)
                        {
                            planText.Text = g.GetResource("FREETRIAL_PLAN", this);
                            phlAllFinancialPlan.Visible = true;
                        }

                        //if ( mt.DiscountID > 0 )
                        //{
                        //    planText.Text = planText.Text + " (" + g.GetResource( mt.DiscountResourceConstant, null ) + ")";
                        //}

                        # region TrialPay Hack
                        //Overriding the PlanDescription ,adminEmail , paymentTypeText and accountText if the TranType is TrialPay.  20071016 - RB
                        if (accountHistory.TransactionType == TransactionType.TrialPayAdjustment)
                        {
                            planText.Text = g.GetResource(accountHistory.TransactionTypeResourceConstant, null);
                            adminEmail.Text = string.Empty;
                            paymentTypeText.Text = string.Empty;
                            accountText.Text = string.Empty;

                            phlAllFinancialPlan.Visible = true;
                        }
                        #endregion
                    }

                    Label durationText = null;
                    PlaceHolder phlDurationText = null;
                    if (e.Item.Parent.ID == "FinancialRepeaterTransaction")
                    {
                        durationText = (Label)e.Item.FindControl("FinancialDuration");
                        phlDurationText = (PlaceHolder)e.Item.FindControl("phlFinancialDuration");
                    }
                    else if (e.Item.Parent.ID == "NonFinancialRepeaterTransaction")
                    {
                        durationText = (Label)e.Item.FindControl("NonFinancialDuration");
                        phlDurationText = (PlaceHolder)e.Item.FindControl("phlNonFinancialDuration");
                    }
                    else if (e.Item.Parent.ID == "AllFinancialRepeaterTransaction")
                    {
                        durationText = (Label)e.Item.FindControl("AllFinancialDuration");
                        phlDurationText = (PlaceHolder)e.Item.FindControl("phlAllFinancialDuration");
                    }
                    durationText.Text = getDurationString(accountHistory);

                    if (accountHistory.IsOneOffPurchase)
                    {
                        phlDurationText.Visible = false;
                    }

                    if (accountHistory.TransactionType == TransactionType.Adjustment
                        || accountHistory.TransactionType == TransactionType.RenewalRecyclingTimeAdjustment
                        || accountHistory.TransactionType == TransactionType.AutoRenewalReopenALaCarte
                        || accountHistory.TransactionType == TransactionType.AutoRenewalTerminateALaCarte)
                    {
                        if (accountHistory.TransactionType == TransactionType.AutoRenewalReopenALaCarte
                            || accountHistory.TransactionType == TransactionType.AutoRenewalTerminateALaCarte)
                        {
                            //convert plan to privilege list
                            if (accountHistory.PackageIDList != null)
                            {
                                string planIDList = accountHistory.PackageIDList;
                                string[] arrPlanID = planIDList.Split(';');

                                foreach (string singlePlanID in arrPlanID)
                                {
                                    Matchnet.Purchase.ValueObjects.Plan singlePlan = null;
                                    try
                                    {
                                        singlePlan = PlanSA.Instance.GetPlan(Convert.ToInt32(singlePlanID), g.Brand.BrandID);
                                    }
                                    catch (Exception ex)
                                    {
                                        g.ProcessException(ex);
                                        singlePlan = null;
                                    }
                                    if (singlePlan != null)
                                    {
                                        Matchnet.Purchase.ValueObjects.PlanServiceCollection planServiceCol = singlePlan.PlanServices;

                                        // A single plan can be a regular or bundled plan, an ala carte plan, or a discount plan
                                        // A plan cannot be both a bundled plan as well as a discount plan for example  
                                        if ((singlePlan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte)
                                        {
                                            if ((singlePlan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
                                            {
                                                accountHistory.PrivilegeTypeList.Add(Spark.Common.AccessService.PrivilegeType.HighlightedProfile);
                                            }
                                            else if ((singlePlan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                                            {
                                                accountHistory.PrivilegeTypeList.Add(Spark.Common.AccessService.PrivilegeType.SpotlightMember);
                                            }
                                            else if ((singlePlan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
                                            {
                                                accountHistory.PrivilegeTypeList.Add(Spark.Common.AccessService.PrivilegeType.JMeter);
                                            }
                                            else if ((singlePlan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                                            {
                                                accountHistory.PrivilegeTypeList.Add(Spark.Common.AccessService.PrivilegeType.AllAccess);
                                            }
                                            else if ((singlePlan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
                                            {
                                                accountHistory.PrivilegeTypeList.Add(Spark.Common.AccessService.PrivilegeType.ReadReceipt);
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        //show privileges adjusted in Details and hide the plan
                        PlaceHolder phlAllFinancialDetails = null;
                        phlAllFinancialDetails = (PlaceHolder)e.Item.FindControl("phlAllFinancialAlaCarte");
                        phlAllFinancialDetails.Visible = true;
                        ((PlaceHolder)e.Item.FindControl("phlAllFinancialPaymentType")).Visible = false;
                        ((PlaceHolder)e.Item.FindControl("phlAllFinancialAccount")).Visible = false;
                        if (accountHistory.PrivilegeTypeList.Count > 0)
                        {
                            StringBuilder sbDetail = new StringBuilder();
                            foreach (Spark.Common.AccessService.PrivilegeType privilege in accountHistory.PrivilegeTypeList)
                            {
                                switch (privilege)
                                {
                                    case Spark.Common.AccessService.PrivilegeType.BasicSubscription:
                                        sbDetail.Append(g.GetResource("REGULAR_BASIC_SUBSCRIPTION", this) + ", ");
                                        break;
                                    case Spark.Common.AccessService.PrivilegeType.AllAccess:
                                        sbDetail.Append(g.GetResource("ALACARTE_ALLACCESS", this) + ", ");
                                        break;
                                    case Spark.Common.AccessService.PrivilegeType.AllAccessEmails:
                                        sbDetail.Append(g.GetResource("ALACARTE_ALLACCESSEMAIL", this) + "(" + accountHistory.AdjustEmailCount.ToString() + "), ");
                                        break;
                                    case Spark.Common.AccessService.PrivilegeType.ColorAnalysis:
                                        sbDetail.Append(g.GetResource("ALACARTE_COLOR_ANALYSIS", this) + ", ");
                                        break;
                                    case Spark.Common.AccessService.PrivilegeType.HighlightedProfile:
                                        sbDetail.Append(g.GetResource("ALACARTE_PROFILE_HIGHLIGHTING", this) + ", ");
                                        break;
                                    case Spark.Common.AccessService.PrivilegeType.JMeter:
                                        sbDetail.Append(g.GetResource("ALACARTE_JMETER", this) + ", ");
                                        break;
                                    case Spark.Common.AccessService.PrivilegeType.SpotlightMember:
                                        sbDetail.Append(g.GetResource("ALACARTE_SPOTLIGHT", this) + ", ");
                                        break;
                                    case Spark.Common.AccessService.PrivilegeType.ReadReceipt:
                                        sbDetail.Append(g.GetResource("ALACARTE_READRECEIPT", this) + ", ");
                                        break;
                                }
                            }

                            Label lblAllFinancialDetails = (Label)e.Item.FindControl("lblAllFinancialAlaCarte");
                            if (sbDetail.ToString().Length > 3)
                                lblAllFinancialDetails.Text = sbDetail.ToString().Substring(0, sbDetail.ToString().Length - 2);
                        }
                    }
                    else
                    {
                        #region Show Discounts and A la Carte Items

                        PlaceHolder phlDiscount = null;
                        PlaceHolder phlAlaCarte = null;
                        if (e.Item.Parent.ID == "FinancialRepeaterTransaction")
                        {
                            phlDiscount = (PlaceHolder)e.Item.FindControl("phlFinancialDiscount");
                            phlAlaCarte = (PlaceHolder)e.Item.FindControl("phlFinancialAlaCarte");
                        }
                        else if (e.Item.Parent.ID == "NonFinancialRepeaterTransaction")
                        {
                            phlDiscount = (PlaceHolder)e.Item.FindControl("phlNonFinancialDiscount");
                            phlAlaCarte = (PlaceHolder)e.Item.FindControl("phlNonFinancialAlaCarte");
                        }
                        else if (e.Item.Parent.ID == "AllFinancialRepeaterTransaction")
                        {
                            phlDiscount = (PlaceHolder)e.Item.FindControl("phlAllFinancialDiscount");
                            phlAlaCarte = (PlaceHolder)e.Item.FindControl("phlAllFinancialAlaCarte");
                        }

                        if (accountHistory.PackageIDList != null)
                        {
                            string planIDList = accountHistory.PackageIDList;
                            string[] arrPlanID = planIDList.Split(';');
                            //List<string> arrPlanServices = new List<string>();
                            List<string> arrDiscountPlanResourceConstants = new List<string>();
                            List<string> arrAlaCartePlanResourceConstants = new List<string>();

                            //Matchnet.Purchase.ValueObjects.Plan singlePlan = null;
                            foreach (string singlePlanID in arrPlanID)
                            {
                                Matchnet.Purchase.ValueObjects.Plan singlePlan = null;
                                try
                                {
                                    Int32 outPlanId = 0;
                                    if(Int32.TryParse(singlePlanID, out outPlanId))
                                    {
                                        singlePlan = PlanSA.Instance.GetPlan(outPlanId, g.Brand.BrandID);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    g.ProcessException(ex);
                                    singlePlan = null;
                                }
                                if (singlePlan != null)
                                {
                                    Matchnet.Purchase.ValueObjects.PlanServiceCollection planServiceCol = singlePlan.PlanServices;

                                    // A single plan can be a regular or bundled plan, an ala carte plan, or a discount plan
                                    // A plan cannot be both a bundled plan as well as a discount plan for example  
                                    if ((singlePlan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte)
                                    {
                                        foreach (PlanService singlePlanService in planServiceCol)
                                        {
                                            string planServiceResourceConstant = "ALACARTE_" + (Enum.GetName(typeof(PlanServiceDefinition), singlePlanService.PlanSvcDefinition)).ToUpper();

                                            if (!arrAlaCartePlanResourceConstants.Contains(planServiceResourceConstant))
                                            {
                                                arrAlaCartePlanResourceConstants.Add(planServiceResourceConstant);
                                            }
                                        }
                                    }
                                    else if ((singlePlan.PlanTypeMask & PlanType.Discount) == PlanType.Discount)
                                    {
                                        // Show the plan description for discounts  
                                        string discountPlanResourceConstant = PlanSA.Instance.GetPlanDescription(Convert.ToInt32(singlePlanID));
                                        arrDiscountPlanResourceConstants.Add(discountPlanResourceConstant);
                                    }
                                }
                            }

                            if (accountHistory.IsOneOffPurchase)
                            {
                                string planServiceResourceConstant = "ALACARTE_COLOR_ANALYSIS";
                                arrAlaCartePlanResourceConstants.Add(planServiceResourceConstant);
                            }

                            if (arrDiscountPlanResourceConstants.Count > 0)
                            {
                                StringBuilder discountList = new StringBuilder();
                                foreach (string singleDiscountResourceConstant in arrDiscountPlanResourceConstants)
                                {
                                    if (discountList.ToString().Length < 1)
                                    {
                                        discountList.Append(g.GetResource(singleDiscountResourceConstant, null));
                                    }
                                    else
                                    {
                                        discountList.Append(" ," + g.GetResource(singleDiscountResourceConstant, null));
                                    }
                                }
                                Label lblDiscount = null;
                                if (e.Item.Parent.ID == "FinancialRepeaterTransaction")
                                {
                                    lblDiscount = (Label)e.Item.FindControl("lblFinancialDiscount");
                                }
                                else if (e.Item.Parent.ID == "NonFinancialRepeaterTransaction")
                                {
                                    lblDiscount = (Label)e.Item.FindControl("lblNonFinancialDiscount");
                                }
                                else if (e.Item.Parent.ID == "AllFinancialRepeaterTransaction")
                                {
                                    lblDiscount = (Label)e.Item.FindControl("lblAllFinancialDiscount");
                                }
                                lblDiscount.Text = discountList.ToString();
                            }
                            else
                            {
                                phlDiscount.Visible = false;
                            }

                            if (arrAlaCartePlanResourceConstants.Count > 0)
                            {
                                StringBuilder alaCarteList = new StringBuilder();
                                foreach (string singleAlaCarte in arrAlaCartePlanResourceConstants)
                                {
                                    if (alaCarteList.ToString().Length < 1)
                                    {
                                        alaCarteList.Append(g.GetResource(singleAlaCarte, this));
                                    }
                                    else
                                    {
                                        alaCarteList.Append(" ," + g.GetResource(singleAlaCarte, this));
                                    }
                                }
                                Label lblAlaCarte = null;
                                if (e.Item.Parent.ID == "FinancialRepeaterTransaction")
                                {
                                    lblAlaCarte = (Label)e.Item.FindControl("lblFinancialAlaCarte");
                                }
                                else if (e.Item.Parent.ID == "NonFinancialRepeaterTransaction")
                                {
                                    lblAlaCarte = (Label)e.Item.FindControl("lblNonFinancialAlaCarte");
                                }
                                else if (e.Item.Parent.ID == "AllFinancialRepeaterTransaction")
                                {
                                    lblAlaCarte = (Label)e.Item.FindControl("lblAllFinancialAlaCarte");
                                }
                                lblAlaCarte.Text = alaCarteList.ToString();
                            }
                            else
                            {
                                phlAlaCarte.Visible = false;
                            }

                        }
                        else
                        {
                            phlDiscount.Visible = false;
                            phlAlaCarte.Visible = false;
                        }

                        #endregion
                    }

                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


		private string getDurationString(MemberTran mt)
		{
			int duration = mt.Duration;

			return mt.Duration.ToString() + " " + g.GetResource( mt.DurationTypeResourceConstant, null );
		}

        private string getDurationString(AccountHistory accountHistory)
        {
            int duration = accountHistory.Duration;

            return Convert.ToString(duration) + " " + g.GetResource(accountHistory.DurationTypeResourceConstant, null);
        }

        private string GetAccessPrivilegeList(List<Spark.Common.AccessService.PrivilegeType> arrPrivilegeTypeList, bool isBundledPackage)
        {
            string strPrivilegeList = String.Empty;

            if (arrPrivilegeTypeList.Count > 0)
            {
                StringBuilder sbDetail = new StringBuilder();
                foreach (Spark.Common.AccessService.PrivilegeType privilege in arrPrivilegeTypeList)
                {
                    switch (privilege)
                    {
                        case Spark.Common.AccessService.PrivilegeType.BasicSubscription:
                            if (isBundledPackage)
                            {
                                sbDetail.Append(g.GetResource("PREMIUM_SUBSCRIPTION", this) + ", ");                                
                            }
                            else
                            {
                                sbDetail.Append(g.GetResource("REGULAR_BASIC_SUBSCRIPTION", this) + ", ");                                
                            }
                            break;
                        case Spark.Common.AccessService.PrivilegeType.AllAccess:
                            sbDetail.Append(g.GetResource("ALACARTE_ALLACCESS", this) + ", ");
                            break;
                        case Spark.Common.AccessService.PrivilegeType.AllAccessEmails:
                            sbDetail.Append(g.GetResource("ALACARTE_ALLACCESSEMAIL", this) + ", ");
                            break;
                        case Spark.Common.AccessService.PrivilegeType.ColorAnalysis:
                            sbDetail.Append(g.GetResource("ALACARTE_COLOR_ANALYSIS", this) + ", ");
                            break;
                        case Spark.Common.AccessService.PrivilegeType.HighlightedProfile:
                            sbDetail.Append(g.GetResource("ALACARTE_PROFILE_HIGHLIGHTING", this) + ", ");
                            break;
                        case Spark.Common.AccessService.PrivilegeType.JMeter:
                            sbDetail.Append(g.GetResource("ALACARTE_JMETER", this) + ", ");
                            break;
                        case Spark.Common.AccessService.PrivilegeType.SpotlightMember:
                            sbDetail.Append(g.GetResource("ALACARTE_SPOTLIGHT", this) + ", ");
                            break;
                        case Spark.Common.AccessService.PrivilegeType.ReadReceipt:
                            sbDetail.Append(g.GetResource("ALACARTE_READRECEIPT", this) + ", ");
                            break;
                    }
                }

                if (sbDetail.ToString().Length > 3)
                {
                    strPrivilegeList = sbDetail.ToString().Substring(0, sbDetail.ToString().Length - 2);                    
                }
                else
                {
                    strPrivilegeList = sbDetail.ToString();                    
                }
            }

            return strPrivilegeList;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init += new System.EventHandler(this.Page_Init);
            this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
    
}
