﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Lib;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Menus;

namespace Matchnet.Web.Applications.Subscription.Controls
{
    public partial class SubscriptionConfirmation20 : FrameworkControl
    {
        //protected System.Web.UI.WebControls.PlaceHolder plcConfirmation;
        //protected Matchnet.Web.Framework.Ui.FormElements.FrameworkButton ProcessAttributesAndContinue;
        //protected Matchnet.Web.Framework.Ui.FormElements.FrameworkHtmlButton Continue;
        //protected Matchnet.Web.Framework.Txt Txt22;
        //protected Matchnet.Web.Framework.Txt Txt33;
        //protected PlaceHolder phMiniProfile;
        private bool enabled = false;
        private PaymentType paymentType;
        //protected System.Web.UI.HtmlControls.HtmlTableRow RowComposeMessage;
        //protected System.Web.UI.WebControls.Panel PanelComposeMessage;
        private int planID = 0;
        private string[] _confirmationArgs;

        public PaymentType PaymentType
        {
            get { return paymentType; }
            set { paymentType = value; }
        }

        public int PlanID
        {
            get { return planID; }
            set { planID = value; }
        }

        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        public string[] ConfirmationArgs
        {
            get { return _confirmationArgs; }
            set { _confirmationArgs = value; }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
                phMiniProfile.Visible = false;

                if (g.RecipientMemberID != 0)
                {
                    Matchnet.Web.Framework.Ui.BasicElements.MicroProfile20 control =
                        (Matchnet.Web.Framework.Ui.BasicElements.MicroProfile20)LoadControl("/Framework/Ui/BasicElements/MicroProfile20.ascx");
                    control.member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(g.RecipientMemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);

                    control.DisplayContext = Framework.Ui.BasicElements.MicroProfile20.DisplayContextType.Carrot;
                    control.LoadMemberProfile();
                    phMiniProfile.Controls.Add(control);
                    phMiniProfile.Visible = true;
                    phMicroProfile.Visible = true;
                }
                else
                {
                    divMiniProfile.Style.Add("display", "none");
                    phMicroProfile.Visible = false;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (enabled)
                {

                    Matchnet.Web.Framework.Txt txt = new Txt();
                    txtRecordInfo.ExpandImageTokens = true;

                    Continue.Attributes.Add("onclick", string.Format("javascript:document.location=\"{0}\";", SubscriptionHelper.GetContinueLink(g)));

                    if (paymentType == PaymentType.Check)
                    {
                        Matchnet.Purchase.ValueObjects.Plan plan = PlanSA.Instance.GetPlan(planID, g.TargetBrand.BrandID);

                        if (plan != null)
                        {
                            txtRecordInfo.Args = new string[1] { plan.InitialCost.ToString() };
                            txtRecordInfo.ResourceConstant = "TXT_TELECHECK_PREMIUM_MEMBERSHIP_CONFIRMATION";
                        }
                    }
                    else
                    {
                        txtRecordInfo.Args = _confirmationArgs;
                        txtRecordInfo.ResourceConstant = "TXT_SUBSCRIPTION_CONFIRMATION";
                        g.HasValidCCOnFile = true;
                    }

                    //plcConfirmation.Controls.Add(txt);

                    Matchnet.Member.ServiceAdapters.Member member =
                        MemberSA.Instance.GetMember(g.TargetMemberID,
                        MemberLoadFlags.None);

                    if (member != null)
                    {
                        string aboutMe = member.GetAttributeText(g.Brand, "AboutMe");

                        if (aboutMe == null || aboutMe.Length == 0)
                        {
                            Continue.Visible = false;
                            ProcessAttributesAndContinue.Visible = true;
                        }

                        if (MemberPhotoDisplayManager.Instance.GetAllPhotosCount(member, g.Brand.Site.Community.CommunityID) == 0)
                        {
                            Continue.Visible = false;
                            ProcessAttributesAndContinue.Visible = true;
                        }
                    }

                    // slider feature for Encore for JDATE only
                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
                    {
                        if (g.IsEligibleForEncoreRebateCredit(true) && g.HasValidCCOnFile)
                        {
                            switch (g.DetermineJRewardsStatus())
                            {
                                case WebConstants.JRewardsMembershipStatus.NeverMember:
                                case WebConstants.JRewardsMembershipStatus.OldExMember:
                                case WebConstants.JRewardsMembershipStatus.FTSFailedMembership:
                                case WebConstants.JRewardsMembershipStatus.OldExFailedMembership:
                                    txtSlider.Visible = true;
                                    txtSlider.ResourceConstant = "HTML_PROMO_SLIDER";
                                    break;
                                default:
                                    txtSlider.Visible = false;
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { g.ProcessException(ex); }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                // Omniture Analytics - Event Tracking
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    // Subscriptons & Subscription Complete
                    _g.AnalyticsOmniture.Events = "purchase,event10";

                    // PurchaseID
                    _g.AnalyticsOmniture.PurchaseID = _confirmationArgs[1];

                    // Subscription - Products
                    SubscriptionResult result = PurchaseSA.Instance.GetMemberTranStatus(_g.TargetMemberID, Convert.ToInt32(_confirmationArgs[1]));
                    Matchnet.Purchase.ValueObjects.Plan plan = PlanSA.Instance.GetPlan(result.PlanID, _g.Brand.BrandID);
                    _g.AnalyticsOmniture.Products = result.PlanID.ToString() + ";" + plan.InitialDuration.ToString() + plan.InitialDurationType.ToString() + ";" + plan.RenewDuration
                        + ";" + plan.InitialCost + getPremiumServiceOmniture(plan); ;

                    // add more events to .Events if this is a All Access Package which has both time-based and count-based
                    int conditionHit = 0;
                    foreach(PlanService ps in plan.PlanServices)
                    {
                        if (ps.PlanSvcDefinition == PlanServiceDefinition.AllAccess || ps.PlanSvcDefinition == PlanServiceDefinition.AllAccessEmail)
                        {
                            conditionHit++;
                        }

                        if (conditionHit >= 2)
                            break;
                    }

                    if (conditionHit >= 2)
                    {
                        _g.AnalyticsOmniture.Events += ",event49";    
                    }

                    // Change the page name since it says Subscribe/SubscribeNew even on confirmation.
                    _g.AnalyticsOmniture.PageName = "Subscribe - SubscribeComplete";

                    int memberSubscriptionStatus = FrameworkGlobals.GetMemberSubcriptionStatus(_g.TargetMemberID, _g.Brand);
                    SubscriptionStatus enumSubscriptionStatus = (SubscriptionStatus)memberSubscriptionStatus;

                    _g.AnalyticsOmniture.Evar10 = _g.AnalyticsOmniture.ConvertSubscriptionStatusToString(enumSubscriptionStatus);
                    _g.AnalyticsOmniture.Prop10 = _g.AnalyticsOmniture.ConvertSubscriptionStatusToString(enumSubscriptionStatus);
                    _g.AnalyticsOmniture.Evar8 = _g.AnalyticsOmniture.ConvertSubscriptionStatusToString(enumSubscriptionStatus, true);
                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
        protected void ProcessAttributesAndContinue_Click(object sender, System.EventArgs e)
        {
            bool continueFlag = true;

            try
            {
                if (!_g.Page.IsValid)
                {
                    // freetrialsubscribe contains two form and in this case only checks to see if form_as is validated..
                    if (g.AppPage.ControlName.ToLower().IndexOf("freetrial") != -1)
                    {
                        bool checkASValidators = (g.Brand.BrandID == (int)WebConstants.BRAND_ID.AmericanSingles);

                        System.Web.UI.ValidatorCollection collection = g.Page.Validators;

                        for (int i = 0; i < collection.Count; i++)
                        {
                            BaseValidator validator = collection[i] as BaseValidator;

                            if ((validator.ID.EndsWith("_AS") == checkASValidators) && !validator.IsValid)
                            {
                                return;
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                }

                Matchnet.Member.ServiceAdapters.Member member =
                    MemberSA.Instance.GetMember(g.TargetMemberID,
                    MemberLoadFlags.None);

                if (member != null)
                {
                    UpdateMember();
                }

                if (continueFlag)
                {
                    SubscribeCommon sc = new SubscribeCommon();
                    g.Transfer(sc.GetContinueLink(false));
                }
            }
            catch (Exception ex) { g.ProcessException(ex); }
        }

        private void UpdateMember()
        {
            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(g.TargetMemberID,
                MemberLoadFlags.None);

            if (member != null)
            {
                Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
            }
        }

        private string getFileExtension(HttpPostedFile file)
        {
            string fileExtension = string.Empty;
            if (file != null)
            {
                int lastIndexOfPeriod = file.FileName.LastIndexOf(".");
                fileExtension = file.FileName.Substring(lastIndexOfPeriod++);
            }
            return fileExtension;
        }

        private string getPremiumServiceOmniture(Matchnet.Purchase.ValueObjects.Plan plan)
        {
            string result = "";
            try
            {
                if (plan == null || (plan.PlanTypeMask & PlanType.PremiumPlan) != PlanType.PremiumPlan)
                    return result;

                if ((plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
                {
                    result = "Highlight";

                    if ((plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                        result += " Spotlight";
                }
                else if ((plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                {
                    result += "Spotlight";
                }

                if ((plan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
                {
                    result += " JMeter";
                }

                result.Trim();

                if (!String.IsNullOrEmpty(result))
                {
                    result = ",;" + result + ";1;0.00";

                }

                return result;
            }
            catch (Exception ex)
            { return result; }


        }

    }
}