﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PaymentHistory20.ascx.cs" Inherits="Matchnet.Web.Applications.Subscription.Controls.PaymentHistory20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<input id="mpid" type="hidden" runat="server" name="mpid" />

<h1><mn:Txt id="txtAccountHistory" runat="server" resourceconstant="TXT_ACCOUNT_HISTORY" expandimagetokens="true" /></h1>
<asp:HiddenField ID="hfAdjustTab" runat="server" />

<%--HEADER TABS--%>
<div id="tabNavAdminContainer" style="display:none;">
    <ul id="tabNavAdmin" class="clearFloats">
        <asp:PlaceHolder ID="phAllFinancialHistory" Runat="server">
            <li>
	            <a id="tabAllFinancialHistory" onclick="javascript:AccountHistoryTabClick(AccountHistoryTabEnum.allfinancialhistory);return false;" href="#" runat="server">All Account History </a>
            </li>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phFinancialHistory" Runat="server">
            <li>
	            <a id="tabFinancialHistory" onclick="javascript:AccountHistoryTabClick(AccountHistoryTabEnum.financialhistory);return false;" href="#" runat="server">Financial History </a>
            </li>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phNonFinancialHistory" Runat="server">
            <li>
	            <a id="tabNonFinancialHistory" onclick="javascript:AccountHistoryTabClick(AccountHistoryTabEnum.nonfinancialhistory);return false;" href="#" runat="server">Non Financial History</a>
            </li>
        </asp:PlaceHolder>
    </ul>
</div>		

<div id="account-current" class="key-value-pair rbox-style-clear">
    <%--
    <span class="key"><mn:Txt id="TransactionRecordTxt" runat="server" ResourceConstant="TRANSACTION_RECORD"></mn:Txt></span> <asp:label id="CurrentMemberID" runat="server" /><br />
    <span class="key"><mn:Txt id="CurrentPlanTxt" runat="server" ResourceConstant="CURRENT_PLAN" /></span>
    --%>
    <asp:literal id="CurrentPlan" runat="server" />
    <asp:PlaceHolder ID="phlFreeTrialInformation" Visible="false" runat="server">
        <br />
        <span class="key"><mn:Txt id="txtFreeTrialCaptureDate" runat="server" ResourceConstant="YOU_HAVE_SUBSCRIPTION_PRIVILEGES_UNTIL"></mn:Txt></span>
	    <asp:label id="lblFreeTrialCaptureDate" runat="server" /><br />
	</asp:PlaceHolder>
    <asp:PlaceHolder ID="phlRenewalInformation" Visible="true" runat="server">
        <asp:panel id="DiscountPanel" runat="server" visible="False">
        <span class="key"><mn:Txt id="CurrentDiscountTxt" ResourceConstant="CURRENT_DISCOUNT" runat="server"></mn:Txt></span> <asp:label id="CurrentDiscount" runat="server"></asp:label><br />
        </asp:panel>
        <%--
        <span class="key"><mn:Txt id="RenewalDateTxt" runat="server" ResourceConstant="YOU_HAVE_SUBSCRIPTION_PRIVILEGES_UNTIL"></mn:Txt></span>	    
        <asp:label id="RenewalDate" runat="server" />
        <br />
        <span class="key"><mn:Txt id="EndDateTxt" runat="server" ResourceConstant="AUTOMATIC_RENEWAL_OF_YOUR_ACCOUNT_WILL_END_ON__519128"></mn:Txt></span>
	    <asp:label id="EndDate" runat="server" /> 
        <a href="/Applications/MemberServices/AutoRenewalSettings.aspx" id="lnkUpdate" runat="server" class="hide-autorenewal">
            <mn:Txt id="txtUpdate" runat="server"  ResourceConstant="TXT_AUTORENEWAL" />
        </a>
	    <br />
        --%>
        
        <span class="key"><mn:Txt id="RenewalDateTxt" runat="server" ResourceConstant="NEXT_RENEWAL_AMOUNT"></mn:Txt></span>
        <mn:Txt runat="server" ID="txtRenewalAmountCurrency" ExpandImageTokens="True" ResourceConstant="TXT_CURRENCY" />
        <asp:Literal ID="litRenewalAmount" runat="server"></asp:Literal>
        <span class="sub-copy">
            <mn:Txt runat="server" ID="txtPerMonth" />
        </span>      
        <span class="sub-copy">
            <mn:Txt runat="server" ID="txtAutoRenewalOnActivePrivilegeTypeList" />
        </span>
        <span class="sub-copy">
            <mn:Txt runat="server" ID="txtAutoRenewalOffActivePrivilegeTypeList" />
        </span>

        <p>
            <span class="key"><mn:Txt ID="Txt3" runat="server" ResourceConstant="ACCOUNT_WILL_RENEW"></mn:Txt></span>
	        <asp:label id="RenewalDate" runat="server" />             
        </p>        
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phlDisabledRenewal" Visible="false" runat="server">
        <span class="key"><mn:Txt runat="server" ResourceConstant="SUBSCRIPTION_WILL_TERMINATE_ON"></mn:Txt></span>
	    <asp:label id="DisabledRenewalEndDate" runat="server" /> 
        <br/>
        <span class="key"><mn:Txt runat="server" ResourceConstant="REACTIVATE_YOUR_SUBSCRIPTION"></mn:Txt></span>
        <a href="/Applications/MemberServices/AutoRenewalSettings.aspx" runat="server" class="hide-autorenewal">
            <mn:Txt runat="server"  ResourceConstant="TXT_CLICK_HERE" />
        </a>
        .&nbsp;
	</asp:PlaceHolder>
    <asp:PlaceHolder ID="phlNoRenewals" Visible="false" runat="server">
        <br />
        <span class="key"><mn:Txt id="txtNoActiveSubscription" runat="server" ResourceConstant="NO_ACTIVE_SUBSCRIPTION"></mn:Txt></span>
        <mn:Txt id="txtSubscribeLink" runat="server" ResourceConstant="NAV_SUBSCRIBE_HERE" href="" />
<%--        <a href="/Applications/MemberServices/AutoRenewalSettings.aspx" runat="server" class="hide-autorenewal">
            <mn:Txt runat="server"  ResourceConstant="TXT_CLICK_HERE" />
        </a>
--%>        .&nbsp;
	</asp:PlaceHolder>
    <p>
        <asp:PlaceHolder ID="plhBuyOneClick" Runat="server"></asp:PlaceHolder>
        <asp:PlaceHolder ID="plhPaymentProfileView" Runat="server"></asp:PlaceHolder>            
    </p>
</div>

<!-- ALL ACCOUNT HISTORY TAB CONTENT -->
<div id="account-history">
<asp:Panel ID="pnlAllFinancialHistoryContent" runat="server" CssClass="adminTabContent">

	<h2 class="account-history"><mn:Txt runat="server" id="txt11" ResourceConstant="TXT_YOUR_ACCOUNT_HISTORY" /></h2>
	<asp:Repeater id="AllFinancialRepeaterTransaction" runat="server" EnableViewState="False" OnItemDataBound="showItem">
		<ItemTemplate>
			<h3 class="account-history"><asp:Label EnableViewState="False" Runat="server" ID="AllFinancialInsertDate" /></h3>
			<div class="clearfix account-history-item">
			    <div class="key-value-pair inside">
		            <%--Amount--%>
		            <span class="key"><mn:Txt id="Txt28" runat="server" ResourceConstant="AMOUNT"></mn:Txt>: </span><%# String.Format("{0:#0.00}", DataBinder.Eval(Container.DataItem, "TotalAmount")) %><br />
                    <span style="<%# DataBinder.Eval(Container.DataItem, "DiscountCode") == null ? "display:none;": "display:block;"%>">
                        <span class="key"><mn:Txt id="Txt10" runat="server" ResourceConstant="DISCOUNT"></mn:Txt>: </span><%# DataBinder.Eval(Container.DataItem, "DiscountAmountForDisplay") %><br />
                        <span class="key"><mn:Txt id="Txt13" runat="server" ResourceConstant="PROMOCODE"></mn:Txt>: </span><%# DataBinder.Eval(Container.DataItem, "DiscountCode") %><br />
                     </span>
                    <%--Plan--%>
                    <asp:PlaceHolder ID="phlAllFinancialPlan" runat="server">
		            <span class="key"><mn:Txt runat="server" id="txtAllFinancialPlan" ResourceConstant="TXT_PLAN" /> </span>
		            <asp:Literal Runat="server" ID="AllFinancialPlanDescription" />
		            </asp:PlaceHolder>
                    <%--Duration--%>
		            <asp:PlaceHolder ID="phlAllFinancialDuration" runat="server">                     
		            <span class="key"><mn:Txt runat="server" id="txtAllFinancialDuration" ResourceConstant="TXT_DURATION" /> </span>
		            <asp:Label EnableViewState="False" Runat="server" ID="AllFinancialDuration" />
		            </asp:PlaceHolder>
		            <br />
		            <%--Payment Type--%>
		            <asp:PlaceHolder ID="phlAllFinancialPaymentType" runat="server">
                    <span class="key"><mn:Txt id="Txt25" runat="server" ResourceConstant="TYPE"></mn:Txt>: </span>
		            <asp:label EnableViewState="False" Runat="server" ID="AllFinancialPaymentType" /><br />
                    </asp:PlaceHolder>
                    <%--ConfirmationID--%>
		            <span class="key"><mn:Txt id="Txt26" runat="server" ResourceConstant="CONFIRMATION"></mn:Txt>:</span>
		            <%# DataBinder.Eval(Container.DataItem, "OrderID") %>
			    </div>
                
                <div class="key-value-pair outside">
                    <%--Transaction Type--%>
                    <span class="key"><mn:Txt id="Txt22" runat="server" ResourceConstant="TYPE"></mn:Txt>: </span>
			        <asp:Label EnableViewState="False" Runat="server" ID="AllFinancialTranType" /><br />
			        <%--Account--%>
			        <asp:PlaceHolder ID="phlAllFinancialAccount" runat="server">
			        <span class="key"><mn:Txt id="Txt24" runat="server" ResourceConstant="ACCOUNT"></mn:Txt>: </span>
			        <asp:label EnableViewState="False" Runat="server" ID="AllFinancialAccount" /><br />
                    </asp:PlaceHolder>
                    <%--Status--%>
			        <span class="key"><mn:Txt id="Txt27" runat="server" ResourceConstant="STATUS"></mn:Txt>: </span>
			        <asp:Label Runat="server" ID="AllFinancialMemberTranStatus" />
                    <%--Admin email--%>
			        <asp:placeholder id="plAllFinancialAdminEmail" runat="server">
			            <br /><span class="key"><mn:Txt id="Txt29" runat="server" ResourceConstant="ADMIN"></mn:Txt>: </span>
			            <asp:label EnableViewState="False" Runat="server" ID="AllFinancialAdminEmail" />
                    </asp:placeholder>
            
                    <%--Plan services--%>
			        <!--
			        <br /><span class="key"><mn:Txt id="txtPlanType" runat="server" ResourceConstant="PLANTYPE"></mn:Txt>: </span>
			        <asp:Label Runat="server" ID="lblPlanType" />
			        -->

                    <%--Discounts--%>
			        <asp:placeholder id="phlAllFinancialDiscount" runat="server" Visible="false">
			            <br /><span class="key"><mn:Txt id="txtAllFinancialDiscount" runat="server" ResourceConstant="DISCOUNT_DETAILS"></mn:Txt>: </span>
			            <asp:Label Runat="server" ID="lblAllFinancialDiscount" />
                    </asp:placeholder>  			            

                    <%--Ala carte--%>
			        <asp:placeholder id="phlAllFinancialAlaCarte" runat="server" Visible="false">
			            <br /><span class="key"><mn:Txt id="txtAllFinancialAlaCarte" runat="server" ResourceConstant="ALACARTE_DETAILS"></mn:Txt>: </span>
			            <asp:Label Runat="server" ID="lblAllFinancialAlaCarte" />
                    </asp:placeholder>  			            
                    
                </div>
                <%--View link--%>
                <mnl:Link id="allfinancialview" runat="server" Visible="False">[view]</mnl:Link>
            </div>
		</ItemTemplate>
	</asp:Repeater>

</asp:Panel>

<!-- FINANCIAL HISTORY TAB CONTENT -->
<asp:Panel ID="pnlFinancialHistoryContent" runat="server" CssClass="adminTabContent">

	<h2 class="financialaccount-history"><mn:Txt runat="server" id="txt1" ResourceConstant="TXT_YOUR_ACCOUNT_HISTORY" /></h2>
	<asp:Repeater id="FinancialRepeaterTransaction" runat="server" EnableViewState="False" OnItemDataBound="showItem">
		<ItemTemplate>
			<h3 class="financialaccount-history"><asp:Label EnableViewState="False" Runat="server" ID="FinancialInsertDate" /></h3>
			<div class="clearfix financialaccount-history-item">
			    <div class="key-value-pair inside">
		            <%--Amount--%>
		            <span class="key"><mn:Txt id="Txt18" runat="server" ResourceConstant="AMOUNT"></mn:Txt>: </span><%# String.Format("{0:#0.00}", DataBinder.Eval(Container.DataItem, "TotalAmount"))%><br />
                    <%--Plan--%>
		            <span class="key"><mn:Txt runat="server" id="txtFinancialPlan" ResourceConstant="TXT_PLAN" /> </span>
		            <asp:Literal Runat="server" ID="FinancialPlanDescription" />
                    <%--Duration--%>
		            <asp:PlaceHolder ID="phlFinancialDuration" runat="server">                                         
		            <span class="key"><mn:Txt runat="server" id="txtFinancialDuration" ResourceConstant="TXT_DURATION" /> </span>
		            <asp:Label EnableViewState="False" Runat="server" ID="FinancialDuration" />
		            </asp:PlaceHolder>
		            <br />
		            <%--Payment Type--%>
                    <span class="key"><mn:Txt id="Txt15" runat="server" ResourceConstant="TYPE"></mn:Txt>: </span>
		            <asp:label EnableViewState="False" Runat="server" ID="FinancialPaymentType" /><br />
                    <%--ConfirmationID--%>
		            <span class="key"><mn:Txt id="Txt16" runat="server" ResourceConstant="CONFIRMATION"></mn:Txt>:</span>
		            <%# DataBinder.Eval(Container.DataItem, "OrderID") %>
			    </div>
                
                <div class="key-value-pair outside">
                    <%--Transaction Type--%>
                    <span class="key"><mn:Txt id="Txt12" runat="server" ResourceConstant="TYPE"></mn:Txt>: </span>
			        <asp:Label EnableViewState="False" Runat="server" ID="FinancialTranType" /><br />
			        <%--Account--%>
			        <span class="key"><mn:Txt id="Txt14" runat="server" ResourceConstant="ACCOUNT"></mn:Txt>: </span>
			        <asp:label EnableViewState="False" Runat="server" ID="FinancialAccount" /><br />
                    <%--Status--%>
			        <span class="key"><mn:Txt id="Txt17" runat="server" ResourceConstant="STATUS"></mn:Txt>: </span>
			        <asp:Label Runat="server" ID="FinancialMemberTranStatus" />
                    <%--Admin email--%>
			        <asp:placeholder id="plFinancialAdminEmail" runat="server">
			            <br /><span class="key"><mn:Txt id="Txt19" runat="server" ResourceConstant="ADMIN"></mn:Txt>: </span>
			            <asp:label EnableViewState="False" Runat="server" ID="FinancialAdminEmail" />
                    </asp:placeholder>
            
                    <%--Plan services--%>
			        <!--
			        <br /><span class="key"><mn:Txt id="txtPlanType" runat="server" ResourceConstant="PLANTYPE"></mn:Txt>: </span>
			        <asp:Label Runat="server" ID="lblPlanType" />
			        -->

                    <%--Discounts--%>
			        <asp:placeholder id="phlFinancialDiscount" runat="server">
			            <br /><span class="key"><mn:Txt id="txtFinancialDiscount" runat="server" ResourceConstant="DISCOUNT_DETAILS"></mn:Txt>: </span>
			            <asp:Label Runat="server" ID="lblFinancialDiscount" />
                    </asp:placeholder>  			            

                    <%--Ala carte--%>
			        <asp:placeholder id="phlFinancialAlaCarte" runat="server">
			            <br /><span class="key"><mn:Txt id="txtFinancialAlaCarte" runat="server" ResourceConstant="ALACARTE_DETAILS"></mn:Txt>: </span>
			            <asp:Label Runat="server" ID="lblFinancialAlaCarte" />
                    </asp:placeholder>  			            
                    
                </div>
                <%--View link--%>
                <mnl:Link id="financialview" runat="server" Visible="False">[view]</mnl:Link>
            </div>
		</ItemTemplate>
	</asp:Repeater>

</asp:Panel>

<!-- NON FINANCIAL HISTORY TAB CONTENT -->
<asp:Panel ID="pnlNonFinancialHistoryContent" runat="server" CssClass="adminTabContent">

	<h2 class="nonfinancialaccount-history"><mn:Txt runat="server" id="txtYourNonFinancialAccountHistory" ResourceConstant="TXT_YOUR_ACCOUNT_HISTORY" /></h2>
	<asp:Repeater id="NonFinancialRepeaterTransaction" runat="server" EnableViewState="False" OnItemDataBound="showItem">
		<ItemTemplate>
			<h3 class="nonfinancialaccount-history"><asp:Label EnableViewState="False" Runat="server" ID="NonFinancialInsertDate" /></h3>
			<div class="clearfix nonfinancialaccount-history-item">
			    <div class="key-value-pair inside">
		            <%--Amount--%>
		            <span class="key"><mn:Txt id="Txt8" runat="server" ResourceConstant="AMOUNT"></mn:Txt>: </span><%# String.Format("{0:#0.00}", DataBinder.Eval(Container.DataItem, "TotalAmount"))%><br />
                    <%--Plan--%>
		            <span class="key"><mn:Txt runat="server" id="txtNonFinancialPlan" ResourceConstant="TXT_PLAN" /> </span>
		            <asp:Literal Runat="server" ID="NonFinancialPlanDescription" />
                    <%--Duration--%>
		            <asp:PlaceHolder ID="phlNonFinancialDuration" runat="server">                                         
		            <span class="key"><mn:Txt runat="server" id="txtNonFinancialDuration" ResourceConstant="TXT_DURATION" /> </span>
		            <asp:Label EnableViewState="False" Runat="server" ID="NonFinancialDuration" />
		            </asp:PlaceHolder>
		            <br />
		            <%--Payment Type--%>
                    <span class="key"><mn:Txt id="Txt5" runat="server" ResourceConstant="TYPE"></mn:Txt>: </span>
		            <asp:label EnableViewState="False" Runat="server" ID="NonFinancialPaymentType" /><br />
                    <%--ConfirmationID--%>
		            <span class="key"><mn:Txt id="Txt6" runat="server" ResourceConstant="CONFIRMATION"></mn:Txt>:</span>
		            <%# DataBinder.Eval(Container.DataItem, "OrderID") %>
			    </div>
                
                <div class="key-value-pair outside">
                    <%--Transaction Type--%>
                    <span class="key"><mn:Txt id="Txt2" runat="server" ResourceConstant="TYPE"></mn:Txt>: </span>
			        <asp:Label EnableViewState="False" Runat="server" ID="NonFinancialTranType" /><br />
			        <%--Account--%>
			        <span class="key"><mn:Txt id="Txt4" runat="server" ResourceConstant="ACCOUNT"></mn:Txt>: </span>
			        <asp:label EnableViewState="False" Runat="server" ID="NonFinancialAccount" /><br />
                    <%--Status--%>
			        <span class="key"><mn:Txt id="Txt7" runat="server" ResourceConstant="STATUS"></mn:Txt>: </span>
			        <asp:Label Runat="server" ID="NonFinancialMemberTranStatus" />
                    <%--Admin email--%>
			        <asp:placeholder id="plNonFinancialAdminEmail" runat="server">
			            <br /><span class="key"><mn:Txt id="Txt9" runat="server" ResourceConstant="ADMIN"></mn:Txt>: </span>
			            <asp:label EnableViewState="False" Runat="server" ID="NonFinancialAdminEmail" />
                    </asp:placeholder>
            
                    <%--Plan services--%>
			        <!--
			        <br /><span class="key"><mn:Txt id="txtPlanType" runat="server" ResourceConstant="PLANTYPE"></mn:Txt>: </span>
			        <asp:Label Runat="server" ID="lblPlanType" />
			        -->

                    <%--Discounts--%>
			        <asp:placeholder id="phlNonFinancialDiscount" runat="server">
			            <br /><span class="key"><mn:Txt id="txtNonFinancialDiscount" runat="server" ResourceConstant="DISCOUNT_DETAILS"></mn:Txt>: </span>
			            <asp:Label Runat="server" ID="lblNonFinancialDiscount" />
                    </asp:placeholder>  			            

                    <%--Ala carte--%>
			        <asp:placeholder id="phlNonFinancialAlaCarte" runat="server">
			            <br /><span class="key"><mn:Txt id="txtNonFinancialAlaCarte" runat="server" ResourceConstant="ALACARTE_DETAILS"></mn:Txt>: </span>
			            <asp:Label Runat="server" ID="lblNonFinancialAlaCarte" />
                    </asp:placeholder>  			            
                    
                </div>
                <%--View link--%>
                <mnl:Link id="nonfinancialview" runat="server" Visible="False">[view]</mnl:Link>
            </div>
		</ItemTemplate>
	</asp:Repeater>

</asp:Panel>

<p class="account-history"><mn:Txt ID="txtDateCACallCS" runat="server" ResourceConstant="DATECA_CALL_CS" /></p>
<hr />
<p class="account-history"><mn:Txt id="txtTermsAndConditions" runat="server" ResourceConstant="TERMS_AND_CONDITIONS"></mn:Txt></p>

</div>
