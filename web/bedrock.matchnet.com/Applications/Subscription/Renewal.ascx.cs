#region System References
using System;
using System.Web;
#endregion
	
#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Globalization;
#endregion

#region Matchnet Service References
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Member.ServiceAdapters;
#endregion

namespace Matchnet.Web.Applications.Subscription
{
	/// <summary>
	///		Summary description for SubscriptionMain.
	/// </summary>
	public class Renewal : SubscribeCommon
	{
		protected System.Web.UI.WebControls.PlaceHolder HolderRenewalNew;
		protected System.Web.UI.WebControls.PlaceHolder HolderRenewalOther;		
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{			
			if(g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles)
			{
				HolderRenewalNew.Visible = true;
			}
			else
			{				
				HolderRenewalOther.Visible = true;
			}
		}		
	}
}
