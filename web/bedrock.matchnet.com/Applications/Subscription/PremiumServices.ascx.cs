﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Subscription
{

    public partial class PremiumServices : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // stealth logon admins shouldn't be here
            if (g.StealthLogonAdmin != null)
                g.Transfer("/Applications/Home/Default.aspx");

            // in-app purchased subscribers shouldn't purchase/upgrade through us
            if (SubscriberManager.Instance.IsIAPSubscriber(g.Member, g.Brand))
                g.Transfer(SubscriberManager.Instance.GetJumpPageRedirect(g.Brand));
             
            try
            {
                PaymentUIConnector connector = new PaymentUIConnector();

                PremiumServicesPage page = new PremiumServicesPage();

                ((IPaymentUIJSON)page).PaymentType = PaymentType.CreditCard;
                ((IPaymentUIJSON)page).SaveLegacyData = true;

                connector.RedirectToPaymentUI(page);

                return;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);

                return;
            }
        }
    }
}
