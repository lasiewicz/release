﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem;

namespace Matchnet.Web.Applications.Subscription
{
    public partial class Upsale : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                PaymentUIConnector connector = new PaymentUIConnector();

                UpsalePage page = new UpsalePage();

                ((IPaymentUIJSON)page).PaymentType = PaymentType.CreditCard;
                ((IPaymentUIJSON)page).SaveLegacyData = true;

                connector.RedirectToPaymentUI(page);

                return;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);

                return;
            }
        }
    }
}
