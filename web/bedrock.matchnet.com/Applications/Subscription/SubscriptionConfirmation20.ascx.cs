﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;

using System.Collections.Specialized;
using Matchnet.Web.Framework.Managers;
using Spark.Common.Adapter;
using Spark.Common.PaymentProfileService;
using Spark.Common.OrderHistoryService;
using Spark.Common.AccessService;
using Spark.Common.DiscountService;
using System.Collections.Generic;
using Spark.Common.AuthorizationService;
using System.Net;
using System.IO;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Web.Applications.Subscription
{
    public partial class SubscriptionConfirmation20 : FrameworkControl
    {
        private bool enabled = false;
        private Spark.Common.OrderHistoryService.PaymentType paymentType;
        private int planID = 0;
        private string[] _confirmationArgs;
        private int memberTranID = Constants.NULL_INT;
        private bool isColorCode = false;
        private bool isFreeTrial = false;
        private bool isGiftRedemption = false;
        //private SubscriptionResult result;
        private int orderID = Constants.NULL_INT;
        private OrderInfo orderInfo = null;
        private int promoID = Constants.NULL_INT;
        private int purchaseReasonTypeID = Constants.NULL_INT;
        private string products = Constants.NULL_STRING;
        private AuthorizationSubscription freeTrialSubscription = null;
        ObsfucatedPaymentProfileResponse paymentProfileResponse = null;
        private AccessTransaction giftTransaction = null;
        private Gift redeemedGift = null;
        private string _mboxScript;

        public Spark.Common.OrderHistoryService.PaymentType PaymentType
        {
            get { return paymentType; }
            set { paymentType = value; }
        }

        public int PlanID
        {
            get { return planID; }
            set { planID = value; }
        }

        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        public string[] ConfirmationArgs
        {
            get { return _confirmationArgs; }
            set { _confirmationArgs = value; }
        }

        public string MBoxScript
        {
            get { return _mboxScript; }
        }



        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
                // we have to try to clear Mingle member cache here before all the possible redirects that will happen later in the code
                ClearMingleMemberCache();

                bool tryToUpsale = false;
                if (HttpContext.Current.Request["upsale"] != null)
                {
                    if (HttpContext.Current.Request["upsale"] == "true")
                    {
                        tryToUpsale = true;
                    }

                    System.Diagnostics.Trace.WriteLine("Sent to the purchase confirmation page where upsale was set to [" + HttpContext.Current.Request["upsale"] + "]");
                }


                // we are going the set the subAmt and subDuration session before any ups activity takes place
                if (Context.Request["oid"] != null)
                {
                    orderID = Convert.ToInt32(Context.Request["oid"]);
                    //get UPS order info
                    orderInfo = OrderHistoryServiceWebAdapter.GetProxyInstance().GetOrderInfo(orderID);
                    OrderHistoryServiceWebAdapter.CloseProxyInstance();							

                    if (orderInfo != null)
                    {
                        _g.Session.Add("SubAmt", orderInfo.TotalAmount.ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.Persistent);
                        _g.Session.Add("SubDuration", orderInfo.Duration.ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.Persistent);
                    }
                    else
                    {
                        throw new Exception("OrderID: " + orderID.ToString() + ", did not return a valid order from UPS.");
                    }
                }
                else if (Context.Request["gr"] != null && Context.Request["gr"] == "true" && Context.Request["aid"] != null)
                {
                    //this was a gift redemption, get the details from the accesstransaction
                    int accesstransactionID = Convert.ToInt32(Context.Request["aid"]);
                    AccessTransaction accessTransaction = AccessServiceWebAdapter.GetProxyInstance().GetAccessTransaction(accesstransactionID);
                    AccessServiceWebAdapter.CloseProxyInstance();
                    
                    if(accessTransaction != null && accessTransaction.AccessTransactionID >= 0)
                    {
                        //since this is a gift redemption and we currently only have one gift which is a free sub, 
                        //assume the sub amount is 0

                        _g.Session.Add("SubAmt", "0.00", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Persistent);

                        foreach (AccessTransactionDetail detail in accessTransaction.AccessTransactionDetails)
                        {
                            if((PrivilegeType)detail.UnifiedPrivilegeTypeID == PrivilegeType.BasicSubscription)
                            {
                                _g.Session.Add("SubDuration", detail.Duration.ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.Persistent);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("AccessTransactionID: " + Context.Request["aid"] + " did not return a valid transaction from UPS.");
                    }

                }
                // end of setting the session. 

                if (Convert.ToBoolean((RuntimeSettings.GetSetting("UPS_DIRECT_TO_UPSALE_WITH_NO_CONFIRMATION_PAGE", g.TargetBrand.Site.Community.CommunityID,
                    g.TargetBrand.Site.SiteID, g.Brand.BrandID))) && tryToUpsale)
                {
                    if (Context.Request["oid"] == null)
                    {
                        throw new Exception("Order ID is missing");
                    }

                    string href;
                    if (g.GetDestinationURL() != null && g.GetDestinationURL().Length > 0)
                    {
                        href = "/Applications/Subscription/Upsale.aspx?oid=" + Context.Request["oid"] + "&upsale=true&DestinationURL=" + System.Web.HttpUtility.UrlEncode(g.GetDestinationURL());
                    }
                    else
                    {
                        href = "/Applications/Subscription/Upsale.aspx?oid=" + Context.Request["oid"] + "&upsale=true";
                    }

                    System.Diagnostics.Trace.WriteLine("Redirect to the upsale page from the order confirmation page to the following URL: " + href);

                    Response.Redirect(href);
                }

                System.Diagnostics.Trace.WriteLine("Sent to the purchase confirmation page without redirecting to the upsale page");

                paymentType = Spark.Common.OrderHistoryService.PaymentType.CreditCard;
                phMiniProfile.Visible = false;

                if (g.RecipientMemberID != 0)
                {
                    Matchnet.Web.Framework.Ui.BasicElements.MicroProfile20 control =
                        (Matchnet.Web.Framework.Ui.BasicElements.MicroProfile20)LoadControl("/Framework/Ui/BasicElements/MicroProfile20.ascx");
                    control.member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(g.RecipientMemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);

                    control.DisplayContext = Framework.Ui.BasicElements.MicroProfile20.DisplayContextType.Carrot;
                    control.LoadMemberProfile();
                    phMiniProfile.Controls.Add(control);
                    phMiniProfile.Visible = true;
                    phMicroProfile.Visible = true;
                }
                else
                {
                    divMiniProfile.Style.Add("display", "none");
                    phMicroProfile.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Txt22.Visible = false;
                PlaceHolderOtherStuff.Visible = false;
                g.ProcessException(ex);
            }
        }

        private string[] GetColorCodeArgs()
        {
            string userName = g.Member.GetUserName(_g.Brand);
            string confirmationNumber = Convert.ToInt32(Context.Request["oid"]).ToString();
            string transactionDate = DateTime.Now.ToString();

            return new string[3]
				{
					userName,
					confirmationNumber, 
					transactionDate
				};
        }

        private string[] GetFreeTrialArgs()
        {
            string userName = g.Member.GetUserName(_g.Brand);
            string confirmationNumber = Convert.ToString(freeTrialSubscription.AuthorizationSubscriptionID);
            string creditCardNumber = Constants.NULL_STRING;
            string creditCardType = Constants.NULL_STRING;
            string transactionDate = DateTime.Now.ToString();

            try
            {
                if (freeTrialSubscription != null)
                {
                    ObsfucatedPaymentProfileResponse paymentProfileResponse = PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByUserPaymentID(freeTrialSubscription.PaymentProfileID, freeTrialSubscription.CallingSystemID);

                    if (paymentProfileResponse.Code == "0")
                    {
                        ObsfucatedCreditCardPaymentProfile creditCardPaymentProfile = paymentProfileResponse.PaymentProfile as ObsfucatedCreditCardPaymentProfile;
                        ObfuscatedCreditCardShortFormFullWebSitePaymentProfile creditCardShortFormFullWebSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormFullWebSitePaymentProfile;
                        ObfuscatedCreditCardShortFormMobileSitePaymentProfile creditCardShortFormMobileSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormMobileSitePaymentProfile;
                        ObfuscatedDebitCardPaymentProfile debitCardPaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedDebitCardPaymentProfile;

                        if (creditCardPaymentProfile != null)
                        {
                            creditCardNumber = creditCardPaymentProfile.LastFourDigitsCreditCardNumber;

                            creditCardType = creditCardPaymentProfile.CardType;
                            try
                            {
                                Matchnet.Purchase.ValueObjects.CreditCardCollection ccc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetCreditCardTypes(g.TargetBrand.Site.SiteID);

                                if (ccc != null)
                                {
                                    Matchnet.Purchase.ValueObjects.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                                    if (cardType != Matchnet.Purchase.ValueObjects.CreditCardType.None)
                                    {
                                        creditCardType = g.GetResource(ccc.FindByID(
                                            Convert.ToInt32(cardType)).ResourceConstant, null);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                creditCardType = creditCardPaymentProfile.CardType;
                            }
                        }
                        else if (creditCardShortFormFullWebSitePaymentProfile != null)
                        {
                            creditCardNumber = creditCardShortFormFullWebSitePaymentProfile.LastFourDigitsCreditCardNumber;

                            creditCardType = creditCardShortFormFullWebSitePaymentProfile.CardType;
                            try
                            {
                                Matchnet.Purchase.ValueObjects.CreditCardCollection ccc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetCreditCardTypes(g.TargetBrand.Site.SiteID);

                                if (ccc != null)
                                {
                                    Matchnet.Purchase.ValueObjects.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                                    if (cardType != Matchnet.Purchase.ValueObjects.CreditCardType.None)
                                    {
                                        creditCardType = g.GetResource(ccc.FindByID(
                                            Convert.ToInt32(cardType)).ResourceConstant, null);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                creditCardType = creditCardShortFormFullWebSitePaymentProfile.CardType;
                            }
                        }
                        else if (creditCardShortFormMobileSitePaymentProfile != null)
                        {
                            creditCardNumber = creditCardShortFormMobileSitePaymentProfile.LastFourDigitsCreditCardNumber;

                            creditCardType = creditCardShortFormMobileSitePaymentProfile.CardType;
                            try
                            {
                                Matchnet.Purchase.ValueObjects.CreditCardCollection ccc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetCreditCardTypes(g.TargetBrand.Site.SiteID);

                                if (ccc != null)
                                {
                                    Matchnet.Purchase.ValueObjects.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                                    if (cardType != Matchnet.Purchase.ValueObjects.CreditCardType.None)
                                    {
                                        creditCardType = g.GetResource(ccc.FindByID(
                                            Convert.ToInt32(cardType)).ResourceConstant, null);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                creditCardType = creditCardShortFormMobileSitePaymentProfile.CardType;
                            }
                        }
                        else if (debitCardPaymentProfile != null)
                        {
                            creditCardNumber = debitCardPaymentProfile.LastFourDigitsCreditCardNumber;

                            creditCardType = debitCardPaymentProfile.CardType;
                            try
                            {
                                Matchnet.Purchase.ValueObjects.CreditCardCollection ccc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetCreditCardTypes(g.TargetBrand.Site.SiteID);

                                if (ccc != null)
                                {
                                    Matchnet.Purchase.ValueObjects.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                                    if (cardType != Matchnet.Purchase.ValueObjects.CreditCardType.None)
                                    {
                                        creditCardType = g.GetResource(ccc.FindByID(
                                            Convert.ToInt32(cardType)).ResourceConstant, null);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                creditCardType = debitCardPaymentProfile.CardType;
                            }

                        }
            }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error in getting the free trial subscription details, Error message: " + ex.Message);
            }
            finally
            {
                PaymentProfileServiceWebAdapter.CloseProxyInstance();
            }

            return new string[5]
				{
					userName,
					confirmationNumber, 
					creditCardType,
					creditCardNumber,
					transactionDate
				};
        }

        private string[] GetGiftArgs()
        {
            string userName = g.Member.GetUserName(_g.Brand);
            string confirmationNumber = Constants.NULL_STRING;
            string creditCardNumber = Constants.NULL_STRING;
            string creditCardType = Constants.NULL_STRING;
            string transactionDate = DateTime.Now.ToString();
            int memberID = Constants.NULL_INT;

            if(paymentProfileResponse != null && giftTransaction != null && redeemedGift != null)
            {
                memberID = redeemedGift.GiftRedemption.CustomerID;

                if (paymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile
                    || paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormFullWebSitePaymentProfile
                    || paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormMobileSitePaymentProfile)
                {
                    ObsfucatedCreditCardPaymentProfile ccPaymentProfile = paymentProfileResponse.PaymentProfile as ObsfucatedCreditCardPaymentProfile;
                    ObfuscatedCreditCardShortFormFullWebSitePaymentProfile ccShortFormFullWebSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormFullWebSitePaymentProfile;
                    ObfuscatedCreditCardShortFormMobileSitePaymentProfile ccShortFormMobileSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormMobileSitePaymentProfile;

                    if (ccPaymentProfile != null)
                    {
                        creditCardType = ccPaymentProfile.CardType;
                        creditCardNumber = ccPaymentProfile.LastFourDigitsCreditCardNumber;
                    }
                    else if (ccShortFormFullWebSitePaymentProfile != null)
                    {
                        creditCardType = ccShortFormFullWebSitePaymentProfile.CardType;
                        creditCardNumber = ccShortFormFullWebSitePaymentProfile.LastFourDigitsCreditCardNumber;
                    }
                    else if (ccShortFormMobileSitePaymentProfile != null)
                    {
                        creditCardType = ccShortFormMobileSitePaymentProfile.CardType;
                        creditCardNumber = ccShortFormMobileSitePaymentProfile.LastFourDigitsCreditCardNumber;
                    }

                    confirmationNumber = giftTransaction.AccessTransactionID.ToString();// memberTranID.ToString();

                    // For security
                    if (memberID != g.TargetMemberID)
                        throw new Exception("Current member ID and member ID inside gift does not match."
                            + " CurrentMemberID:" + g.TargetMemberID.ToString() + ", Gift MemberID:" +
                            memberID.ToString() + ", AccessTransaction:" + giftTransaction.AccessTransactionID.ToString());

                    try
                    {
                        Matchnet.Purchase.ValueObjects.CreditCardCollection ccc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetCreditCardTypes(g.TargetBrand.Site.SiteID);

                        if (ccc != null)
                        {
                            Matchnet.Purchase.ValueObjects.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                            if (cardType != Matchnet.Purchase.ValueObjects.CreditCardType.None)
                            {
                                creditCardType = g.GetResource(ccc.FindByID(
                                    Convert.ToInt32(cardType)).ResourceConstant, null);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ccPaymentProfile != null)
                        {
                            creditCardType = ccPaymentProfile.CardType;
                        }
                        else if (ccShortFormFullWebSitePaymentProfile != null)
                        {
                            creditCardType = ccShortFormFullWebSitePaymentProfile.CardType;
                        }
                        else if (ccShortFormMobileSitePaymentProfile != null)
                        {
                            creditCardType = ccShortFormMobileSitePaymentProfile.CardType;
                        }
                    }
                }
                else if (paymentProfileResponse.PaymentProfile is ObfuscatedDebitCardPaymentProfile)
                {
                    ObfuscatedDebitCardPaymentProfile debitCardPaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedDebitCardPaymentProfile;
                    creditCardType = debitCardPaymentProfile.CardType;
                    creditCardNumber = debitCardPaymentProfile.LastFourDigitsCreditCardNumber;

                    confirmationNumber = orderID.ToString();// memberTranID.ToString();

                    // For security
                    if (memberID != g.TargetMemberID)
                        throw new Exception("Current member ID and member ID inside gift does not match."
                            + " CurrentMemberID:" + g.TargetMemberID.ToString() + ", Gift MemberID:" +
                            memberID.ToString() + ", AccessTransaction:" + giftTransaction.AccessTransactionID.ToString());

                    try
                    {
                        Matchnet.Purchase.ValueObjects.CreditCardCollection ccc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetCreditCardTypes(g.TargetBrand.Site.SiteID);

                        if (ccc != null)
                        {
                            Matchnet.Purchase.ValueObjects.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                            if (cardType != Matchnet.Purchase.ValueObjects.CreditCardType.None)
                            {
                                creditCardType = g.GetResource(ccc.FindByID(
                                    Convert.ToInt32(cardType)).ResourceConstant, null);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        creditCardType = debitCardPaymentProfile.CardType;
                    }
                }
            }

            return new string[5]
				{
					userName,
					confirmationNumber, 
					creditCardType,
					creditCardNumber,
					transactionDate
				};
        }


        private string[] GetArgs()
        {
            string userName = g.Member.GetUserName(_g.Brand);
            string confirmationNumber = Constants.NULL_STRING;
            string creditCardNumber = Constants.NULL_STRING;
            string creditCardType = Constants.NULL_STRING;
            string transactionDate = DateTime.Now.ToString();
            int memberID = Constants.NULL_INT;

            if (paymentProfileResponse != null && orderInfo != null)
            {
                memberID = orderInfo.CustomerID;

                // Store necessary values in session cookie for use in pixels.
                _g.Session.Add("SubAmt", orderInfo.TotalAmount.ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.Persistent);
                _g.Session.Add("SubDuration", orderInfo.Duration.ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.Persistent);

                if (paymentProfileResponse.PaymentProfile is ObsfucatedCheckPaymentProfile)
                {
                    paymentType = Spark.Common.OrderHistoryService.PaymentType.Check;
                    return new string[1] { orderInfo.TotalAmount.ToString() };
                }
                else if (paymentProfileResponse.PaymentProfile is ObsfucatedPaypalPaymentProfile)
                {
                    //Paypal Litle
                    paymentType = Spark.Common.OrderHistoryService.PaymentType.PayPalLitle;
                    confirmationNumber = orderID.ToString();
                    return new string[4]
			            {
				            userName,
				            confirmationNumber, 
				            orderInfo.TotalAmount.ToString(),
				            transactionDate
			            };
                }
                else if (paymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile
                        || paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormFullWebSitePaymentProfile
                        || paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormMobileSitePaymentProfile)
                {
                    ObsfucatedCreditCardPaymentProfile ccPaymentProfile = paymentProfileResponse.PaymentProfile as ObsfucatedCreditCardPaymentProfile;
                    ObfuscatedCreditCardShortFormFullWebSitePaymentProfile ccShortFormFullWebSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormFullWebSitePaymentProfile;
                    ObfuscatedCreditCardShortFormMobileSitePaymentProfile ccShortFormMobileSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormMobileSitePaymentProfile;

                    if (ccPaymentProfile != null)
                    {
                        creditCardType = ccPaymentProfile.CardType;
                        creditCardNumber = ccPaymentProfile.LastFourDigitsCreditCardNumber;
                    }
                    else if (ccShortFormFullWebSitePaymentProfile != null)
                    {
                        creditCardType = ccShortFormFullWebSitePaymentProfile.CardType;
                        creditCardNumber = ccShortFormFullWebSitePaymentProfile.LastFourDigitsCreditCardNumber;
                    }
                    else if (ccShortFormMobileSitePaymentProfile != null)
                    {
                        creditCardType = ccShortFormMobileSitePaymentProfile.CardType;
                        creditCardNumber = ccShortFormMobileSitePaymentProfile.LastFourDigitsCreditCardNumber;
                    }

                    confirmationNumber = orderID.ToString();// memberTranID.ToString();

                    // For security
                    if (memberID != g.TargetMemberID)
                        throw new Exception("Current member ID and member ID inside OrderInfo does not match."
                            + " CurrentMemberID:" + g.TargetMemberID.ToString() + ", OrderInfo MemberID:" +
                            memberID.ToString() + ", OrderID:" + orderID.ToString());

                    try
                    {
                        Matchnet.Purchase.ValueObjects.CreditCardCollection ccc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetCreditCardTypes(g.TargetBrand.Site.SiteID);

                        if (ccc != null)
                        {
                            Matchnet.Purchase.ValueObjects.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                            if (cardType != Matchnet.Purchase.ValueObjects.CreditCardType.None)
                            {
                                creditCardType = g.GetResource(ccc.FindByID(
                                    Convert.ToInt32(cardType)).ResourceConstant, null);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ccPaymentProfile != null)
                        {
                            creditCardType = ccPaymentProfile.CardType;
                        }
                        else if (ccShortFormFullWebSitePaymentProfile != null)
                        {
                            creditCardType = ccShortFormFullWebSitePaymentProfile.CardType;
                        }
                        else if (ccShortFormMobileSitePaymentProfile != null)
                        {
                            creditCardType = ccShortFormMobileSitePaymentProfile.CardType;
                        }
                    }
                }
                else if (paymentProfileResponse.PaymentProfile is ObfuscatedDebitCardPaymentProfile)
                {
                    ObfuscatedDebitCardPaymentProfile debitCardPaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedDebitCardPaymentProfile;
                    creditCardType = debitCardPaymentProfile.CardType;
                    creditCardNumber = debitCardPaymentProfile.LastFourDigitsCreditCardNumber;

                    confirmationNumber = orderID.ToString();// memberTranID.ToString();

                    // For security
                    if (memberID != g.TargetMemberID)
                        throw new Exception("Current member ID and member ID inside OrderInfo does not match."
                            + " CurrentMemberID:" + g.TargetMemberID.ToString() + ", OrderInfo MemberID:" +
                            memberID.ToString() + ", OrderID:" + orderID.ToString());

                    try
                    {
                        Matchnet.Purchase.ValueObjects.CreditCardCollection ccc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetCreditCardTypes(g.TargetBrand.Site.SiteID);

                        if (ccc != null)
                        {
                            Matchnet.Purchase.ValueObjects.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                            if (cardType != Matchnet.Purchase.ValueObjects.CreditCardType.None)
                            {
                                creditCardType = g.GetResource(ccc.FindByID(
                                    Convert.ToInt32(cardType)).ResourceConstant, null);
                            }
                        }
                }
                    catch (Exception ex)
                    {
                        creditCardType = debitCardPaymentProfile.CardType;
            }
                }
            }

            return new string[5]
				{
					userName,
					confirmationNumber, 
					creditCardType,
					creditCardNumber,
					transactionDate
				};
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                g.SetTopAuxMenuVisibility(false, true);
                g.HeaderControl.ShowBlankHeader(Matchnet.Web.Framework.Ui.HeaderImageType.Default);

                enabled = true;
                
                string primaryPlanID = "0";

                if (Context.Request["freetrial"] != null)
                {
                    try
                    {
                        int accessTransactionID = Convert.ToInt32(Context.Request["freetrial"]);

                        freeTrialSubscription = AuthorizationServiceWebAdapter.GetProxyInstance().GetAuthorizationSubscriptionByAccessTransactionID(accessTransactionID);
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.WriteLine("Error in getting the free trial subscription details, Error message: " + ex.Message);
                    }
                    finally
                    {
                        AuthorizationServiceWebAdapter.CloseProxyInstance();
                    }

                    isFreeTrial = true;
                }
                else if (Context.Request["gr"] != null && Context.Request["gr"] == "true" && Context.Request["aid"] != null)
                {
                    isGiftRedemption = true;
                    int accesstransactionID = Convert.ToInt32(Context.Request["aid"]);
                    giftTransaction = AccessServiceWebAdapter.GetProxyInstance().GetAccessTransaction(accesstransactionID);
                    AccessServiceWebAdapter.CloseProxyInstance();

                    if (giftTransaction != null && giftTransaction.AccessTransactionID <= 0) giftTransaction = null;
                    
                    if (giftTransaction != null)
                    {
                        
                        //first, get the gift the transaction was created for
                        redeemedGift =DiscountServiceWebAdapter.GetProxyInstance().GetGiftByAccessTransactionID(accesstransactionID);
                        DiscountServiceWebAdapter.CloseProxyInstance();

                        if (redeemedGift != null && redeemedGift.GiftRedemption != null)
                        {

                            primaryPlanID = redeemedGift.GiftRedemption.PackageID.ToString();
                            //get payment profile info
                            paymentProfileResponse =
                                PaymentProfileServiceWebAdapter.GetProxyInstance().
                                    GetObsfucatedPaymentProfileByUserPaymentID(redeemedGift.GiftRedemption.UserPaymentGUID, giftTransaction.CallingSystemID);
                            PaymentProfileServiceWebAdapter.CloseProxyInstance();
                        }
                        else
                        {
                            throw new Exception("AccessTransactionID: " + Context.Request["aid"] + " did not return a valid gift from UPS.");
                        }
                    }
                    else
                    {
                        throw new Exception("AccessTransactionID: " + Context.Request["aid"] + " did not return a valid transaction from UPS.");
                    }
                }
                else if (Context.Request["oid"] == null)
                {
                    throw new Exception("OrderID cannot be null.");
                }
                else
                {
                    orderID = Convert.ToInt32(Context.Request["oid"]);
                    //get UPS order info
                    orderInfo = OrderHistoryServiceWebAdapter.GetProxyInstance().GetOrderInfo(orderID);
                    OrderHistoryServiceWebAdapter.CloseProxyInstance();

                    if (orderInfo != null)
                    {
                        //set duration
                        foreach (OrderDetailInfo odi in orderInfo.OrderDetail)
                        {
                            //current business rule requires all package items in an order to have same duration
                            if (odi.Duration > 0)
                            {
                                orderInfo.Duration = odi.Duration;
                                orderInfo.DurationTypeID = odi.DurationTypeID;
                                break;
                            }
                        }

                        //get payment profile info
                        paymentProfileResponse = PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByOrderID(orderID, orderInfo.CustomerID, orderInfo.CallingSystemID);
                        PaymentProfileServiceWebAdapter.CloseProxyInstance();
                    }
                    else
                    {
                        throw new Exception("OrderID: " + orderID.ToString() + ", did not return a valid order from UPS.");
                    }

                }

                string mBoxNamePrefix = RuntimeSettings.GetSetting("T_N_T_SUB_CONFIRM_MBOX_NAME_PREFIX",
                                                                                 g.Brand.Site.Community.CommunityID,
                                                                                 g.Brand.Site.SiteID);
                if (!string.IsNullOrEmpty(mBoxNamePrefix))
                {
                    if (isFreeTrial)
                    {
                        _mboxScript = "mboxCreate('{0}_sub_complete','productPurchasedId={1}','orderTotal={2}','orderId={3}');";
                        _mboxScript = string.Format(_mboxScript, mBoxNamePrefix, Convert.ToString(freeTrialSubscription.PrimaryPackageID), Convert.ToString(freeTrialSubscription.AuthorizationAmount), Convert.ToString(freeTrialSubscription.AuthorizationSubscriptionID));
                    }
                    else if(isGiftRedemption)
                    {
                        _mboxScript = "mboxCreate('{0}_sub_complete','productPurchasedId={1}','orderTotal={2}','orderId={3}');";
                        _mboxScript = string.Format(_mboxScript, mBoxNamePrefix, UPSGetPrimaryPlan().PlanID.ToString(), "0", Context.Request["aid"]);
                    }
                    else
                    {
                        _mboxScript = "mboxCreate('{0}_sub_complete','productPurchasedId={1}','orderTotal={2}','orderId={3}');";
                        _mboxScript = string.Format(_mboxScript, mBoxNamePrefix, UPSGetPrimaryPlan().PlanID.ToString(), orderInfo.TotalAmount.ToString(), orderID.ToString());
                    }
                }
                else
                {
                    PlaceHolderOmnitureMboxSubscriptionConfirmation.Visible = false;
                }

                if (isFreeTrial)
                {
                    plcFreeTrialConfirmation.Visible = true;
                    plcFreeTrialConfirmationAdditional.Visible = true;                    					
                    PlaceHolderOtherStuff.Visible = true;
                    txtFreeTrialConfirmation.Args = GetFreeTrialArgs();
                    btnFreeTrialConfirmation.Visible = false;
                    Txt22.ResourceConstant = "TXT_H1_FREE_TRIAL_CONFIRMATION";
                    Continue.Attributes.Add("onclick", string.Format("javascript:document.location=\"{0}\";", SubscriptionHelper.GetContinueLink(g)));
                    //btnFreeTrialConfirmation.Attributes.Add("onclick", string.Format("javascript:document.location=\"{0}\";", SubscriptionHelper.GetContinueLink(g)));
                    return;
                }

                if (orderInfo != null)
                {
                    //Check ColorAnalysis Purchase
                    foreach (OrderDetailInfo orderDetailInfo in orderInfo.OrderDetail)
                    {
                        if (orderDetailInfo.ItemID == 1003)
                        {
                            isColorCode = true;
                            break;
                        }
                    }
                }

                if (isColorCode)
                {
                    PlaceHolderColorCodeConfirmation.Visible = true;
                    PlaceHolderOtherStuff.Visible = false;
                    TxtColorCodeSubscriptionConfirmation.Args = GetColorCodeArgs();
                    FrameworkButtonStartTest.Attributes.Add("onclick",
                                                            string.Format(
                                                                "javascript:document.location=\"{0}\";",
                                                                FrameworkGlobals.LinkHref(
                                                                    "/Applications/ColorCode/ComprehensiveAnalysis.aspx",
                                                                    true))
                        );
                    return;
                }

                string smsAlertsEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SMS_ALERTS_SUB_REDIRECT", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID);
                if (smsAlertsEnabled.ToLower() == "true" && Request.QueryString["FromSMSAlerts"] == null)
                {
                    bool isSMSAlertsConfirmed = g.TargetMember.GetAttributeBool(g.TargetBrand.Site.Community.CommunityID, g.TargetBrand.Site.SiteID, g.TargetBrand.BrandID, "SMSPhoneIsValidated");
                    if (!isSMSAlertsConfirmed)
                    {
                        string phoneNumber = string.Empty;

                        if (paymentProfileResponse != null)
                        {
                            if (paymentProfileResponse.PaymentProfile is ObsfucatedCheckPaymentProfile)
                            {
                                phoneNumber = ((ObsfucatedCheckPaymentProfile)paymentProfileResponse.PaymentProfile).Phone;
                            }
                            else if (paymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile)
                            {
                                phoneNumber = ((ObsfucatedCreditCardPaymentProfile)paymentProfileResponse.PaymentProfile).PhoneNumber;
                            }
                            else if (paymentProfileResponse.PaymentProfile is ObfuscatedDebitCardPaymentProfile)
                            {
                                phoneNumber = ((ObfuscatedDebitCardPaymentProfile)paymentProfileResponse.PaymentProfile).PhoneNumber;
                            }
                        }
                        g.Transfer(string.Format("/Applications/MemberServices/SMSAlert.aspx?FromSub=1&oid={0}&SubPhone={1}", Context.Request["oid"], phoneNumber));
                    }
                }

                if (enabled)
                {

                    Matchnet.Web.Framework.Txt txt = new Txt();
                    txtRecordInfo.ExpandImageTokens = true;

                    Continue.Attributes.Add("onclick", string.Format("javascript:document.location=\"{0}\";", SubscriptionHelper.GetContinueLink(g)));

                    if(isGiftRedemption)
                    {
                        txtRecordInfo.Args = GetGiftArgs();
                    }
                    else
                    {
                        txtRecordInfo.Args = GetArgs();
                    }


                    if (paymentType == Spark.Common.OrderHistoryService.PaymentType.Check)
                    {
                        txtRecordInfo.ResourceConstant = "TXT_TELECHECK_PREMIUM_MEMBERSHIP_CONFIRMATION";
                    }
                    else if (paymentType == Spark.Common.OrderHistoryService.PaymentType.PayPalLitle)
                    {
                        txtRecordInfo.ResourceConstant = "TXT_PAYPAL_SUBSCRIPTION_CONFIRMATION";
                    }
                    else
                    {
                        txtRecordInfo.ResourceConstant = "TXT_SUBSCRIPTION_CONFIRMATION";
                        g.HasValidCCOnFile = true;
                    }

                    Matchnet.Member.ServiceAdapters.Member member =
                        MemberSA.Instance.GetMember(g.TargetMemberID,
                        MemberLoadFlags.None);

                    // slider feature for Encore for JDATE only
                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate
                        && paymentType == Spark.Common.OrderHistoryService.PaymentType.CreditCard)
                    {
                        //if (g.IsEligibleForEncoreRebateCredit(true) && g.HasValidCCOnFile)
                        if (g.IsEligibleForEncoreRebateCredit(true) && paymentProfileResponse != null && (paymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile || paymentProfileResponse.PaymentProfile is ObfuscatedDebitCardPaymentProfile || paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormFullWebSitePaymentProfile || paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormMobileSitePaymentProfile))
                        {
                            switch (g.DetermineJRewardsStatus())
                            {
                                case WebConstants.JRewardsMembershipStatus.NeverMember:
                                case WebConstants.JRewardsMembershipStatus.OldExMember:
                                case WebConstants.JRewardsMembershipStatus.FTSFailedMembership:
                                case WebConstants.JRewardsMembershipStatus.OldExFailedMembership:
                                    txtSlider.Visible = true;
                                    txtSlider.ResourceConstant = "HTML_PROMO_SLIDER";
                                    break;
                                default:
                                    txtSlider.Visible = false;
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Txt22.Visible = false;
                PlaceHolderOtherStuff.Visible = false;
                g.Notification.AddError("MNCHARGE_606004");
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                // Omniture Analytics - Event Tracking
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {


                    _g.AnalyticsOmniture.PageName = "Subscribe - SubscribeComplete";

                    // Subscriptons & Subscription Complete  

                    if(isFreeTrial) //check for free trial 
                        _g.AnalyticsOmniture.AddEvent("purchase,event89"); 
                    else
                        _g.AnalyticsOmniture.AddEvent("purchase,event10"); 

                    // ColorCode confirmation is a hack.
                    if (isColorCode)
                        return;

                    if (orderInfo.PromoID > 0)
                    {
                        _g.AnalyticsOmniture.Evar4 = Convert.ToString(orderInfo.PromoID);
                    }
                    else
                    {
                        _g.AnalyticsOmniture.Evar4 = "";
                    }

                    if (orderInfo.PurchaseReasonTypeID > 0)
                    {
                        _g.AnalyticsOmniture.Evar6 = (!string.IsNullOrEmpty(orderInfo.SecondaryPurchaseReasonTypeIDs) ?
                                                orderInfo.PurchaseReasonTypeID.ToString() + '-' + orderInfo.SecondaryPurchaseReasonTypeIDs.Replace('|', '-')
                                                : orderInfo.PurchaseReasonTypeID.ToString());
                    }
                    else
                    {
                        _g.AnalyticsOmniture.Evar6 = "";
                    }

                    // PurchaseID
                    _g.AnalyticsOmniture.PurchaseID = orderID.ToString();

                    // Subscription - Products
                    Matchnet.Purchase.ValueObjects.Plan plan = UPSGetPrimaryPlan();
                    if (plan != null)
                    {
                        _g.AnalyticsOmniture.Products = plan.PlanID.ToString() + ";" + plan.InitialDuration.ToString() + plan.InitialDurationType.ToString() + ";" + plan.RenewDuration
                            + ";" + orderInfo.TotalAmount;

                        //_g.AnalyticsOmniture.Products = plan.PlanID.ToString() + ";" + plan.InitialDuration.ToString() + plan.InitialDurationType.ToString() + ";" + plan.RenewDuration
                        //    + ";" + plan.InitialCost + getPremiumServiceOmniture(plan);
                    }

                    // Change the page name since it says Subscribe/SubscribeNew even on confirmation.
                    _g.AnalyticsOmniture.PageName = "Subscribe - SubscribeComplete";

                    int memberSubscriptionStatus = FrameworkGlobals.GetMemberSubcriptionStatus(_g.TargetMemberID, _g.Brand);
                    Matchnet.Purchase.ValueObjects.SubscriptionStatus enumSubscriptionStatus = (Matchnet.Purchase.ValueObjects.SubscriptionStatus)memberSubscriptionStatus;

                    _g.AnalyticsOmniture.Evar10 = _g.AnalyticsOmniture.ConvertSubscriptionStatusToString(enumSubscriptionStatus);
                    //_g.AnalyticsOmniture.Prop10 = _g.AnalyticsOmniture.ConvertSubscriptionStatusToString(enumSubscriptionStatus);

                    _g.AnalyticsOmniture.Evar8 = _g.AnalyticsOmniture.ConvertSubscriptionStatusToString(enumSubscriptionStatus, true);

                    _g.AnalyticsOmniture.Evar22 = (_g.TargetMember.IsPayingMember(_g.TargetBrand.Site.SiteID)) ? "Subscriber" : "Registered";
                    _g.AnalyticsOmniture.Prop22 = (_g.TargetMember.IsPayingMember(_g.TargetBrand.Site.SiteID)) ? "Subscriber" : "Registered";

                    if (HttpContext.Current.Request["purchasedUpsale"] != null)
                    {
                        if (HttpContext.Current.Request["purchasedUpsale"] == "true")
                        {
                            if (Context.Request["oid"] != null)
                            {
								_g.AnalyticsOmniture.Events = _g.AnalyticsOmniture.Events.Replace(",event10", "");
							
                                orderID = Convert.ToInt32(Context.Request["oid"]);

                                System.Diagnostics.Trace.WriteLine("Get order information for Order ID [" + Convert.ToString(orderID) + "]");

                                //get UPS order info
                                orderInfo = OrderHistoryServiceWebAdapter.GetProxyInstance().GetOrderInfo(orderID);
                                OrderHistoryServiceWebAdapter.CloseProxyInstance();
                                
                                if (orderInfo != null)
                                {
                                    System.Diagnostics.Trace.WriteLine("Successfully retrieved order information for Order ID [" + Convert.ToString(orderID) + "]");

                                    if (orderInfo.OrderStatusID == 2)
                                    {
                                        _g.AnalyticsOmniture.Events += ",event49";
                                    }

                                    if (!String.IsNullOrEmpty(orderInfo.OriginalSubscriptionStatusBeforeUpsale))
                                    {
                                        _g.AnalyticsOmniture.Evar8 = Convert.ToString(orderInfo.OriginalSubscriptionStatusBeforeUpsale);
                                    }
                                    else
                                    {
                                        _g.AnalyticsOmniture.Evar8 = String.Empty;
                                    }

                                    if (orderInfo.OriginalTemplateIDBeforeUpsale > 0)
                                    {
                                        _g.AnalyticsOmniture.Evar31 = Convert.ToString(orderInfo.OriginalTemplateIDBeforeUpsale);
                                    }
                                    else
                                    {
                                        _g.AnalyticsOmniture.Evar31 = String.Empty;
                                    }
                                }
                            }
                        }
                    }
                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
        protected void ProcessAttributesAndContinue_Click(object sender, System.EventArgs e)
        {
            bool continueFlag = true;

            try
            {
                if (!_g.Page.IsValid)
                {
                    // freetrialsubscribe contains two form and in this case only checks to see if form_as is validated..
                    if (g.AppPage.ControlName.ToLower().IndexOf("freetrial") != -1)
                    {
                        bool checkASValidators = (g.Brand.BrandID == (int)WebConstants.BRAND_ID.AmericanSingles);

                        System.Web.UI.ValidatorCollection collection = g.Page.Validators;

                        for (int i = 0; i < collection.Count; i++)
                        {
                            BaseValidator validator = collection[i] as BaseValidator;

                            if ((validator.ID.EndsWith("_AS") == checkASValidators) && !validator.IsValid)
                            {
                                return;
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                }

                Matchnet.Member.ServiceAdapters.Member member =
                    MemberSA.Instance.GetMember(g.TargetMemberID,
                    MemberLoadFlags.None);

                if (member != null)
                {
                    UpdateMember();
                }

                if (continueFlag)
                {
                    SubscribeCommon sc = new SubscribeCommon();
                    g.Transfer(sc.GetContinueLink(false));
                }
            }
            catch (Exception ex) { g.ProcessException(ex); }
        }

        private void UpdateMember()
        {
            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(g.TargetMemberID,
                MemberLoadFlags.None);

            if (member != null)
            {
                Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
            }
        }

        private string getFileExtension(HttpPostedFile file)
        {
            string fileExtension = string.Empty;
            if (file != null)
            {
                int lastIndexOfPeriod = file.FileName.LastIndexOf(".");
                fileExtension = file.FileName.Substring(lastIndexOfPeriod++);
            }
            return fileExtension;
        }

        private string getPremiumServiceOmniture(Matchnet.Purchase.ValueObjects.Plan plan)
        {
            string result = "";
            try
            {
                if (plan == null || (plan.PlanTypeMask & Matchnet.Purchase.ValueObjects.PlanType.PremiumPlan) != Matchnet.Purchase.ValueObjects.PlanType.PremiumPlan)
                    return result;

                if ((plan.PremiumTypeMask & Matchnet.Purchase.ValueObjects.PremiumType.HighlightedProfile) == Matchnet.Purchase.ValueObjects.PremiumType.HighlightedProfile)
                {
                    result = "Highlight";

                    if ((plan.PremiumTypeMask & Matchnet.Purchase.ValueObjects.PremiumType.SpotlightMember) == Matchnet.Purchase.ValueObjects.PremiumType.SpotlightMember)
                        result += " Spotlight";
                }
                else if ((plan.PremiumTypeMask & Matchnet.Purchase.ValueObjects.PremiumType.SpotlightMember) == Matchnet.Purchase.ValueObjects.PremiumType.SpotlightMember)
                {
                    result += "Spotlight";
                }

                if ((plan.PremiumTypeMask & Matchnet.Purchase.ValueObjects.PremiumType.JMeter) == Matchnet.Purchase.ValueObjects.PremiumType.JMeter)
                {
                    result += " JMeter";
                }

                if ((plan.PremiumTypeMask & Matchnet.Purchase.ValueObjects.PremiumType.ReadReceipt) == Matchnet.Purchase.ValueObjects.PremiumType.ReadReceipt)
                {
                    result += " ReadReceipt";
                }

                result.Trim();

                if (!String.IsNullOrEmpty(result))
                {
                    result = ",;" + result + ";1;0.00";

                }

                return result;
            }
            catch (Exception ex)
            { return result; }


        }

        private Matchnet.Purchase.ValueObjects.Plan UPSGetPrimaryPlan()
        {
            int legacyPlanIDFound = Constants.NULL_INT;
            int alaCartePlanIDFound = Constants.NULL_INT;
            int discountPlanIDFound = Constants.NULL_INT;

            if(isGiftRedemption && redeemedGift != null && redeemedGift.GiftRedemption != null)
            {
                return Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(redeemedGift.GiftRedemption.PackageID, _g.Brand.BrandID);
            }
            else if (orderInfo.OrderDetail.Length == 1)
            {
                return Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(orderInfo.OrderDetail[0].PackageID, _g.Brand.BrandID);
            }
            else
            {
                foreach (OrderDetailInfo odi in orderInfo.OrderDetail)
                {
                    Matchnet.Purchase.ValueObjects.Plan plan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(odi.PackageID, _g.Brand.BrandID);

                    if (plan != null)
                    {
                        if ((plan.PlanTypeMask & Matchnet.Purchase.ValueObjects.PlanType.ALaCarte) == Matchnet.Purchase.ValueObjects.PlanType.ALaCarte)
                        {
                            // An ala carte item exists in the order
                            if (alaCartePlanIDFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                alaCartePlanIDFound = odi.PackageID;
                            }
                        }
                        else if ((plan.PlanTypeMask & Matchnet.Purchase.ValueObjects.PlanType.Discount) == Matchnet.Purchase.ValueObjects.PlanType.Discount)
                        {
                            // A discount item exists in the order
                            if (discountPlanIDFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                discountPlanIDFound = odi.PackageID;
                            }
                        }
                        else
                        {
                            // The primary PlanID will be the item that is not an ala carte 
                            // or discount package  
                            if (legacyPlanIDFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                legacyPlanIDFound = odi.PackageID;
                                break;
                            }
                        }
                    }
                }

                if (legacyPlanIDFound != Constants.NULL_INT)
                {
                    // Return any PlanID as the primary PlanID as long as this is not an ala carte 
                    // package or a discount package
                    return Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(legacyPlanIDFound, _g.Brand.BrandID);
                }
                else if (alaCartePlanIDFound != Constants.NULL_INT)
                {
                    // If the member only purchased an ala carte package and no base subscription packages,
                    // than return the first ala carte package found
                    return Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(alaCartePlanIDFound, _g.Brand.BrandID);
                }
                else
                {
                    // If the order did not have any base subscriptions or ala carte packages, than there
                    // no primary PlanID was found
                    // Do not return the discount PlanID since a discount cannot exists without a base 
                    // subscription or ala carte purchase  
                    return null;
                }
            }
        }

        private Matchnet.Purchase.ValueObjects.CreditCardType UPSGetMatchnetCreditCardType(string upsCreditCardType)
        {
            Matchnet.Purchase.ValueObjects.CreditCardType cardType = Matchnet.Purchase.ValueObjects.CreditCardType.None;
            //this is the UPS credit card type text
            switch (upsCreditCardType)
            {
                case "Visa":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.Visa;
                    break;
                case "Mastercard":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.MasterCard;
                    break;
                case "AmericanExpress":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.AmericanExpress;
                    break;
                case "Discover":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.Discover;
                    break;
                case "Switch":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.Switch;
                    break;
                case "CarteBlanche":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.CarteBlanche;
                    break;
                case "Solo":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.Solo;
                    break;
                case "DirectDebit":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.DirectDebit;
                    break;
                case "Delta":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.Delta;
                    break;
                case "Electron":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.Electron;
                    break;
                case "DinersClub":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.DinersClub;
                    break;
                case "YoterClubLeumiCard":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.YoterClubLeumiCard;
                    break;
                case "LeumiCardStudentCard":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.LeumiCardStudentCard;
                    break;
                case "YoterClubIsraCard":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.YoterClubIsraCard;
                    break;
                case "IsraCardCampusCardStudent":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.IsraCardCampusCardStudent;
                    break;
                case "VisaCalSoldiersClub":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.VisaCalSoldiersClub;
                    break;
                case "IsraCard":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.IsraCard;
                    break;
            }

            return cardType;
        }

        private string ClearMingleMemberCache()
        {
            string response = string.Empty;
            int siteID = 0;
                int memberID = 0;

            try
            {
                string clearCacheURL = RuntimeSettings.GetSetting("MINGLE_CLEAR_CACHE_URL");
                string clearCacheKey = RuntimeSettings.GetSetting("MINGLE_CLEAR_CACHE_KEY");

                var brands = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandsByCommunity(g.Brand.Site.Community.CommunityID);

                foreach (Brand brand in brands)
                {
                    siteID = brand.Site.SiteID;
                    memberID = g.Member.MemberID;

                    string baseUri = clearCacheURL +
                                     string.Format("?key={0}&siteid={1}&memberid={2}", clearCacheKey, brand.Site.SiteID, g.Member.MemberID);
                    Uri serviceUri = new Uri(baseUri, UriKind.Absolute);
                    HttpWebRequest webRequest = (HttpWebRequest)System.Net.WebRequest.Create(serviceUri);
                    webRequest.Timeout = 2000;
                    webRequest.ContentType = "application/x-www-form-urlencoded";

                    try
                    {
                        HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                        StreamReader sr = new StreamReader(webResponse.GetResponseStream());
                        response += sr.ReadToEnd();
                    }
                    catch (Exception ex)
                    {
                        response += "An error has occurred while trying to clear Mingle's cache." + " siteID=" + brand.Site.SiteID + "\n";
                        System.Diagnostics.EventLog.WriteEntry("WWW", response + " Ex: " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                // do nothing for exceptions because we want the sub confirmation page to load even if there was a problem
                // busting Mingle's member cache
                System.Diagnostics.EventLog.WriteEntry("WWW", "Error from ClearMingleMemberCache() siteID: " + siteID + " memberID: " + memberID + " ex: " + ex.Message);
            }

            return response;
        }

    }
}

