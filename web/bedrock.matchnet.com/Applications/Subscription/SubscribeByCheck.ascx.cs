using System;
using System.Web.UI.WebControls;
	
using Matchnet.Web.Framework;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.Subscription
{
	public class SubscribeByCheck : SubscribeCommon
	{
		#region MEMBER

		protected System.Web.UI.WebControls.PlaceHolder PlaceHolderNew;
		protected System.Web.UI.WebControls.PlaceHolder PlaceHolderOld;		

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}

		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{	
			if(g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles||g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
			{
				PlaceHolderNew.Visible = true;
			}
			else
			{				
				PlaceHolderOld.Visible = true;
			}
		}
	}
}


