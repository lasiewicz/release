using System;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
	
using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Framework.Util;


namespace Matchnet.Web.Applications.Subscription
{
	public class SubscribeByCheckOld : SubscribeCommon
	{
		#region FormElements

		// Delete designer elements ending here
		protected Matchnet.Web.Framework.Ui.FormElements.FrameworkButton ProcessPurchase;
		#endregion

		#region Static Content 

		protected Matchnet.Web.Framework.Image Img1;
		protected Matchnet.Web.Framework.Image Img5;
		protected Matchnet.Web.Framework.Image Img2;
		protected Matchnet.Web.Framework.Txt PayByCreditCardTxt;
		protected Matchnet.Web.Framework.Txt InitialTermTxt;
		protected Matchnet.Web.Framework.Txt FirstNameTxt;
		protected Matchnet.Web.Framework.Txt LastNameTxt;
		protected Matchnet.Web.Framework.Txt AddressLine1Txt;
		protected Matchnet.Web.Framework.Txt AddressLine2Txt;
		protected Matchnet.Web.Framework.Txt CityTxt;
		protected Matchnet.Web.Framework.Txt StateTxt;
		protected Matchnet.Web.Framework.Txt ZipCodeTxt;
		protected Matchnet.Web.Framework.Txt PhoneNumberTxt;
		protected Matchnet.Web.Framework.Txt DriversLincenseTxt;
		protected Matchnet.Web.Framework.Txt StateOfIssueTxt;
		protected Matchnet.Web.Framework.Txt BankNameTxt;
		protected Matchnet.Web.Framework.Txt BankRoutingNumberTxt;
		protected Matchnet.Web.Framework.Txt MicrInstructionsTxt;
		protected Matchnet.Web.Framework.Txt AccountNumberTxt;
		protected Matchnet.Web.Framework.Txt CheckNumberTxt;
		protected Matchnet.Web.Framework.Txt TelecheckHeaderTxt;
		protected Matchnet.Web.Framework.Txt CheckWarningTxt;
		protected Matchnet.Web.Framework.Txt ReturnCheckFeeByStateTxt;
		protected Matchnet.Web.Framework.Txt ReturnCheckFeesTxt;
		protected Matchnet.Web.Framework.Txt FeeShowTxt;
		protected Matchnet.Web.Framework.Txt CheckWriterTxt;
		protected Matchnet.Web.Framework.Txt TransactionAuthorizationTxt;
		protected Matchnet.Web.Framework.Txt ACHTermsTxt;
		protected Matchnet.Web.Framework.Txt TelecheckTermsTxt;
		protected Matchnet.Web.Framework.Txt SubscriptionTermsTxt;
		protected Matchnet.Web.Framework.Txt CreditCardPurchaseTxt;
		protected Matchnet.Web.Framework.Txt ChargeInProgressTxt;
		protected Matchnet.Web.Framework.Txt PlanDescriptionTxt;
		protected Matchnet.Web.Framework.Txt ContinueLinkTxt;
		protected Matchnet.Web.Framework.Txt SubscribeNowTxt;
		protected Matchnet.Web.Framework.Txt DriversLincenseInputInstructions;
		protected Matchnet.Web.Framework.Txt DriversLincenseStateInputInstructions;
		protected Matchnet.Web.Framework.Txt BankAccountTypeTxt;
		protected Matchnet.Web.Framework.Txt CheckInputInstructions;
		protected Matchnet.Web.Framework.Txt txtTopBenefits;
		protected System.Web.UI.WebControls.CustomValidator BankRoutingNumberCustomValidator;
		protected Matchnet.Web.Framework.MultiValidator AddressLine1Validator;
		protected Matchnet.Web.Framework.MultiValidator StateValidator;
		protected Matchnet.Web.Framework.MultiValidator ZipCodeValidator;
		protected Matchnet.Web.Framework.MultiValidator PhoneValidator;
		protected Matchnet.Web.Framework.MultiValidator DriversLicenseValidator;
		protected Matchnet.Web.Framework.MultiValidator DriversLicenseStateValidator;
		protected Matchnet.Web.Framework.MultiValidator BankNameValidator;
		protected Matchnet.Web.Framework.MultiValidator BankRoutingNumberValidator;
		protected Matchnet.Web.Framework.MultiValidator CheckingAccountNumberValidator;
		protected Matchnet.Web.Framework.MultiValidator CheckNumberValidator;
		protected Matchnet.Web.Framework.MultiValidator FirstNameValidator;
		protected Matchnet.Web.Framework.MultiValidator LastNameValidator;
		protected Matchnet.Web.Framework.MultiValidator AddressLine2Validator;
		protected Matchnet.Web.Framework.MultiValidator CityValidator;
		protected Matchnet.Web.Framework.MultiValidator BankAccountTypeValidator;
		protected Matchnet.Web.Framework.Popup SubscribePopup;
		protected Matchnet.Web.Applications.Subscription.Controls.SinglePlanList SinglePlanList1;
		protected Matchnet.Web.Applications.Subscription.Controls.SubscriptionConfirmation SubscriptionConfirmation1;
		protected PlaceHolder phMiniProfile;

		private bool _telecheck;
		private string _telecheckPrivacyText; 
		private string _telecheckPrivacyArticleLink; 
		private string _telecheckPrivacyWindowLink; 
		private string _internetFAQText;
		private string _internetFAQArticleLink;
		private string _internetFAQWindowLink;
        protected bool adminDisplayMode = false; //this is used to determine whether we are displaying in admin mode, affects css and displays

		#endregion

		#region Control Populators

		public Content.ValueObjects.Region.RegionCollection GetStates()
		{
			//	TOFIX - Should we move the Abbreviation trimming up into the RegionSA? - dcornell
			//ToDo: Move the abbreviation trimming into the RegionSA - chenyard
			Content.ValueObjects.Region.RegionCollection rc = RegionSA.Instance.RetrieveChildRegions(USA_REGIONID, g.Brand.Site.LanguageID);
			
			foreach ( Matchnet.Content.ValueObjects.Region.Region r in rc )
			{
				r.Abbreviation = r.Abbreviation.ToString().Trim();
			}

			return rc;
		}

		public string GetPayByCreditCardURL()
		{
			try
			{
				//getting rid of extra ampersand at end
				return LinkFactory.Instance.GetLink("/Applications/Subscription/Subscribe.aspx?OverrideTransactionHistory=1"
					+ g.AppendDestinationURL(), g.Brand, Request.Url.ToString());
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
				return "";
			}
		}

		public string[] GetTelecheckSuccessArgs()
		{
			string[] planDescription = GetPlanDescription();
			return new string[2] {_internetFAQWindowLink, planDescription[0]};
		}

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}

		private void InitializeComponent()
		{
			this.ProcessPurchase.Click += new System.EventHandler(this.ProcessPurchase_Click);
			this.Load += new System.EventHandler(this.Page_Load);
			this.Init += new System.EventHandler(this.Page_Init);

		}
		#endregion

		private void Page_Init(object sender, System.EventArgs e)
		{
			try
			{
			//TT#21164
			//	if(g.Member == null || !MemberPrivilegeAttr.IsCureentSubscribedMember(g))
			//	{
					g.HeaderControl.ShowBlankHeader(Matchnet.Web.Framework.Ui.HeaderImageType.Default);
                    g.SetTopAuxMenuVisibility(false,true);
					g.LeftNavControl.HideUserNameLogout();
			//	}

				// Check if we are we need to show someone's profile (i.e. if you clicked "email me" on someone's profile)
				int memberID = g.RecipientMemberID;
				if (memberID > 0)
				{
					if(phMiniProfile != null)
						this.phMiniProfile.Visible = true;

					if(txtTopBenefits != null)
						this.txtTopBenefits.Visible = false;

					Matchnet.Web.Framework.Ui.BasicElements.MiniProfile control = (Matchnet.Web.Framework.Ui.BasicElements.MiniProfile) LoadControl("../../Framework/Ui/BasicElements/MiniProfile.ascx");
					control.Member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
					
					control.Transparency = true;
					if(phMiniProfile !=null)
						this.phMiniProfile.Controls.Add(control);
					//this.SubscriptionBenefits1.Visible = false;
				}

				_telecheck = false;
				if (BankName == null)
					_telecheck = true;

				DriversLicenseState.DataSource = GetStates();
				DriversLicenseState.DataValueField = "Abbreviation";
				DriversLicenseState.DataTextField = "Abbreviation";
				DriversLicenseState.DataBind();

				BankAccountType.DataSource = Option.GetOptions("BankAccountType", g);
				BankAccountType.DataValueField = "Value";
				BankAccountType.DataTextField = "Content";
				BankAccountType.DataBind();

				SubscriptionTermsTxt.Args = GetTermsAndConditionsLink();
				
				if (_telecheck)
				{
					_telecheckPrivacyText = 
						g.GetResource("TELECHECK_PRIVACY_POLICY", this);

					_telecheckPrivacyArticleLink = 
						FrameworkGlobals.GetArticleLink("TELECHECK_PRIVACY_POLICY_DETAILS",
						LayoutTemplate.Popup,
						_telecheckPrivacyText,
						true);

					_telecheckPrivacyWindowLink = 
						FrameworkGlobals.GetLaunchWindowLink(
						_telecheckPrivacyArticleLink,
						_telecheckPrivacyText,
						_telecheckPrivacyText,
						650,
						500,
						string.Empty);


					_internetFAQText = 
						g.GetResource("INTERNET_CHECK_FAQ", this);

					_internetFAQArticleLink = 
						FrameworkGlobals.GetArticleLink("INTERNET_CHECKS_FREQUENTLY_ASKED_QUESTIONS",
						LayoutTemplate.Popup,
						_internetFAQText,
						true);

					_internetFAQWindowLink = 
						FrameworkGlobals.GetLaunchWindowLink(
						_internetFAQArticleLink,
						_internetFAQText,
						_internetFAQText,
						650,
						500,
						string.Empty);

					TelecheckTermsTxt.Args = new string[2]
						{
							_telecheckPrivacyWindowLink,
							_internetFAQWindowLink
						};
				}

				SinglePlanList1.Visible = true;
				PlanListControl1.Visible = false;
				PlanListControl1.Enabled = false;

				if (!SinglePlanList1.Enabled)
				{
					SinglePlanList1.Visible = false;
					PlanListControl1.Visible = true;
					PlanListControl1.Enabled = true;
				}

				if (BankRoutingNumberCustomValidator != null)
					BankRoutingNumberCustomValidator.ErrorMessage = g.GetResource("INVALID_ROUTINGNUMBER", this);

				//This 5dft popup code was taken from Subscribe.cs
				SubscribePopup = new Popup();
				_g.Page.Controls.Add(SubscribePopup);

				SubscribePopup.Width = FreeTrialCtrl.FREETRIALPOPUP_WIDTH;
				SubscribePopup.Height = FreeTrialCtrl.FREETRIALPOPUP_HEIGHT;

				SetFTPopupStatus(SubscribePopup);

				DataBind();
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}

		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				// this logic has to be in Page_Load, otherwise Footer has not processed in Init
			
			
				if(g.Member == null || !MemberPrivilegeAttr.IsCureentSubscribedMember(g))
				{
					g.FooterControl.HideFooterLinks();
				}

				if ( MemberPrivilegeAttr.IsCureentSubscribedMember(g) &&
					(Request["OverrideTransactionHistory"] == null || Request["OverrideTransactionHistory"] != "1") )
				{
					ShowTransactionHistory();
				}

				if ( !IsPostBack )
				{
					if ( ShowError())
						return;
	                				
					DataBind();

					if (g.IsDevMode)
					{
						//AddressLine1.Text = "1831 Murdock Drive";
						AddressLine1.Text = "0";
						City.Text = "Hollywood";
						State.Text = "CA";
						DriversLicenseState.SelectedValue = "CA";
						PostalCode.Text = "90069";
						Phone.Text = "3238363000";
						
						
						if (!_telecheck)
						{
							FirstName.Text = "BA";
							LastName.Text = "Barracus";
							BankRoutingNumber.Text = "051000017";
							BankName.Text = "El Banco con mas pesos";
							CheckingAccountNumber.Text = "1010101017";
							DriversLicenseNumber.Text = "3ATM3OU812";
						}
						else
						{
							FirstName.Text = "Consumer";
							LastName.Text = "One";
							BankRoutingNumber.Text = "121000358";
							DriversLicenseNumber.Text = "C1111111";
							CheckingAccountNumber.Text = "1111111111";
						}
						
						CheckNumber.Text = "1101";
					}
				}

				if ( Request["CheckStatus"] == "1" )
				{

					int memberTranID = GetDecryptedValue(Request["mtid"]);
					int memberPaymentID = GetDecryptedValue(Request["mpid"]);

					try
					{
						int attempt = 0;
						
						if ( Request["Attempt"] != null )
						{
							attempt = Matchnet.Conversion.CInt(Request["Attempt"]);
						}
						
						SubscriptionResult sr = PurchaseSA.Instance.GetMemberTranStatus(g.TargetMemberID, memberTranID);
						LastSelectedPlanID = sr.PlanID;
						
						switch (sr.MemberTranStatus)
						{
							case MemberTranStatus.Success:
								SubscriptionConfirmation1.Enabled = true;
								SubscriptionConfirmation1.PlanID = sr.PlanID;
								ShowSuccess();
								if (_telecheck)
								{
									SendSuccessEmail(memberPaymentID);
								}
								break;
							case MemberTranStatus.Failure:
								RedirectFailure("SubscribeByCheck", sr.ResourceConstant, memberPaymentID, memberTranID);
								break;
							case MemberTranStatus.Pending:
								ShowInProgress( memberTranID, "SubscribeByCheck", attempt, sr.MemberPaymentID);
								break;
						}
					}
					catch(System.Threading.ThreadAbortException)
					{
					}
				}
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void SendSuccessEmail(int memberPaymentID)
		{
			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(g.TargetMemberID, MemberLoadFlags.None);

			if (member != null)
			{		
				string[] planDescription = GetPlanDescription();
				ExternalMailSA.Instance.SendCheckSubscriptionConfirmation(member.MemberID, g.Brand.BrandID, memberPaymentID, planDescription[0]);
			}
		}

		private bool ValidateRoutingNumber(string val) 
		{
			try 
			{
				char[] chars = val.ToCharArray();
				int sum = 0;

				for (int i = 0; i < chars.Length; i += 3) 
				{
					sum += Matchnet.Conversion.CInt(System.Convert.ToString(chars[i])) * 3
						+  Matchnet.Conversion.CInt(System.Convert.ToString(chars[i + 1])) * 7
						+  Matchnet.Conversion.CInt(System.Convert.ToString(chars[i + 2]));
				}

				if (sum != 0 && sum % 10 == 0) 
					return true;
			} 
			catch {}

			return false;
		}

		private void ProcessPurchase_Click(object sender, System.EventArgs e)
		{
			try
			{
				string phoneNumber = ConvertToRegex(Phone.Text, "[0-9]");
				string addressLine2 = null;

				if ( AddressLine2.Text.Length > 0 )
				{
					addressLine2 = AddressLine2.Text;
				}

				if (phoneNumber.Length < 10)
				{
					PhoneValidator.IsValid = false;
				}

				if (!ValidateRoutingNumber(BankRoutingNumber.Text))
				{
					BankRoutingNumberCustomValidator.IsValid = false;
				}

				if ( !g.Page.IsValid )
				{
					g.Notification.AddErrorString("Invalid data");
					return;
				}

				int selectedPlan = Conversion.CInt(Request["SelectedPlan"]);
				int adminMemberID = Constants.NULL_INT;
				int conversionMemberID = Constants.NULL_INT;
				int sourceID = Conversion.CInt( Request["srid"] ) != Constants.NULL_INT ? Conversion.CInt( Request["srid"] ) : Constants.NULL_INT;
				int purchaseReasonTypeID = Conversion.CInt( Request["prtid"] ) != Constants.NULL_INT ? Conversion.CInt( Request["prtid"] ) : Constants.NULL_INT;

				if (g.IsAdmin && g.Member != null)
				{
					adminMemberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
				}

				if (g.Session["ClickMemberID"] != null)
				{
					conversionMemberID = Conversion.CInt(g.Session["ClickMemberID"]);
				}

				SubscriptionResult sr = PurchaseSA.Instance.BeginCheckPurchase(
					g.TargetMemberID,
					adminMemberID,
					selectedPlan,
					Constants.NULL_INT,
					g.TargetBrand.Site.SiteID,
					g.TargetBrand.BrandID,
					FirstName.Text,
					LastName.Text,
					phoneNumber,
					AddressLine1.Text,
					addressLine2,
					City.Text,
					State.Text,
					PostalCode.Text,
					DriversLicenseNumber.Text,
					DriversLicenseState.SelectedValue,
					null,
					BankRoutingNumber.Text,
					CheckingAccountNumber.Text,
					Convert.ToInt32(CheckNumber.Text),
					conversionMemberID,
					(BankAccountType)Convert.ToInt32(BankAccountType.SelectedValue),
					sourceID,
					purchaseReasonTypeID,
					g.ClientIP);
				
				g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_CHECK_ACCTNO,
					CheckingAccountNumber.Text,
					Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

				g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_CHECK_ROUTINGNO,
					BankRoutingNumber.Text,
					Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

				g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_CHECK_DLNO,
					DriversLicenseNumber.Text,
					Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

				if ( sr.ReturnValue == Constants.RETURN_OK )
				{
					ShowInProgress( sr.MemberTranID, "SubscribeByCheck", 0 , sr.MemberPaymentID);
				}
				else
				{
					g.Notification.AddError( sr.ResourceConstant );
				}
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}
		protected override void OnPreRender(EventArgs e)
		{		
			base.OnPreRender (e);
			g.LeftNavControl.Visible = true;

            //admin style
            if (g.IsAdmin && g.Member != null)
            {
                if (Request.QueryString["impmid"] != null)
                {
                    if (Request.QueryString["tran"] == null)
                    {
                        this.adminDisplayMode = true;
                    }
                }
            }
		}
	}
}


