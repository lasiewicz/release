using System;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
	
using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Framework.Util;
using Matchnet.Content.ValueObjects;

using Matchnet.Web.Applications.Subscription.Controls;
using Matchnet.Web.Framework.Enumerations;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters.TokenReplacement;

namespace Matchnet.Web.Applications.Subscription
{
	/// <summary>
	///		Summary description for SubscribeByCheckNew.
	/// </summary>
	public class SubscribeByCheckNew : SubscribeCommon
	{		
		#region MEMBER

		protected System.Web.UI.WebControls.Label lblAge;
		protected System.Web.UI.WebControls.Label lblLocation;
		protected System.Web.UI.WebControls.Label lblUserName;
		protected System.Web.UI.WebControls.Image imgUserPix;
		protected System.Web.UI.WebControls.PlaceHolder phTopCarrot;
		protected System.Web.UI.WebControls.PlaceHolder phRightCarrot;
		protected System.Web.UI.WebControls.Panel PanelNormalPlanSelection;
        protected System.Web.UI.WebControls.Panel PanelAdminPlanForMember;
		protected System.Web.UI.WebControls.PlaceHolder phlPlanHeaders;
		protected System.Web.UI.WebControls.Panel pnlPlanDisplay;
		protected System.Web.UI.WebControls.Panel PanelCarrotPlanSelection;
		protected System.Web.UI.WebControls.PlaceHolder phlCarrotPlanHeaders;
		protected System.Web.UI.WebControls.Panel pnlCarrotPlanDisplay;
		//protected System.Web.UI.WebControls.DataList dlistMorePlans;
		protected System.Web.UI.WebControls.CustomValidator custValPlanSelection;
		protected System.Web.UI.WebControls.CustomValidator custValAccountNumber;
		protected System.Web.UI.WebControls.CustomValidator custValCheckNumber;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divPlanGrid;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divParentContainer;
		protected System.Web.UI.WebControls.RequiredFieldValidator rqfValFName;
		protected System.Web.UI.WebControls.RequiredFieldValidator rqfValLName;
		protected System.Web.UI.WebControls.RequiredFieldValidator rqfValAddress;
		protected System.Web.UI.WebControls.RequiredFieldValidator rqfValCity;
		protected System.Web.UI.WebControls.RequiredFieldValidator rqfValState;
		protected System.Web.UI.WebControls.RequiredFieldValidator rqfValZip;
		protected System.Web.UI.WebControls.RequiredFieldValidator rqfValPhone;
		protected System.Web.UI.WebControls.RequiredFieldValidator rqfValDriverLicense;
		protected System.Web.UI.WebControls.HyperLink lnkPayByCreditCard;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divPayByCheck;
		protected System.Web.UI.HtmlControls.HtmlTable tblInputFields;
		protected System.Web.UI.HtmlControls.HtmlTable tblInputFields2;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divPayByPhone;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divPayByCheckInfo;
		protected Matchnet.Web.Framework.Txt txtTopBenefits;
		protected System.Web.UI.WebControls.PlaceHolder phMiniProfile;
		protected Matchnet.Web.Framework.Txt Txt11;
		protected Matchnet.Web.Framework.Image imgArrow;
		protected Matchnet.Web.Framework.Txt txtPlan;
		protected Matchnet.Web.Framework.SingleFormSubmitValidator SingleFormSubmitValidator2;
		protected Matchnet.Web.Framework.Txt Txt10;
		protected Matchnet.Web.Framework.MultiValidator FirstNameValidator;
		protected Matchnet.Web.Framework.Txt LastNameTxt;
		protected Matchnet.Web.Framework.MultiValidator LastNameValidator;
		protected Matchnet.Web.Framework.Txt AddressLine1Txt;
		protected Matchnet.Web.Framework.MultiValidator AddressLine1Validator;
		protected Matchnet.Web.Framework.Txt AddressLine2Txt;
		protected Matchnet.Web.Framework.MultiValidator AddressLine2Validator;
		protected Matchnet.Web.Framework.Txt CityTxt;
		protected Matchnet.Web.Framework.MultiValidator CityValidator;
		protected Matchnet.Web.Framework.Txt StateTxt;
		protected Matchnet.Web.Framework.MultiValidator StateValidator;
		protected Matchnet.Web.Framework.Txt ZipCodeTxt;
		protected Matchnet.Web.Framework.MultiValidator ZipCodeValidator;
		protected Matchnet.Web.Framework.Txt PhoneNumberTxt;
		protected Matchnet.Web.Framework.MultiValidator PhoneValidator;
		protected Matchnet.Web.Framework.Txt DriversLincenseTxt;
		protected Matchnet.Web.Framework.MultiValidator DriversLicenseValidator;
		protected Matchnet.Web.Framework.Txt Txt5;
		protected Matchnet.Web.Framework.Txt StateOfIssueTxt;
		protected Matchnet.Web.Framework.MultiValidator DriversLicenseStateValidator;
		protected Matchnet.Web.Framework.Txt Txt6;
		protected Matchnet.Web.Framework.Txt BankAccountTypeTxt;
		protected Matchnet.Web.Framework.MultiValidator BankAccountTypeValidator;
		protected Matchnet.Web.Framework.Txt Txt2;
		protected Matchnet.Web.Framework.Txt Txt3;
		protected Matchnet.Web.Framework.Txt Txt4;
		protected Matchnet.Web.Framework.Image Image2;
		protected Matchnet.Web.Framework.MultiValidator CheckNumberValidator;
		protected Matchnet.Web.Framework.Image Image3;
		protected Matchnet.Web.Framework.Image Image4;
		protected Matchnet.Web.Framework.Image Image5;
		protected Matchnet.Web.Framework.Image Image6;
		protected Matchnet.Web.Framework.MultiValidator BankRoutingNumberValidator;
		protected System.Web.UI.WebControls.CustomValidator BankRoutingNumberCustomValidator;
		protected Matchnet.Web.Framework.MultiValidator CheckingAccountNumberValidator;
		protected Matchnet.Web.Framework.Image Img3;
		protected Matchnet.Web.Framework.Txt Txt8;
		protected Matchnet.Web.Framework.Txt Txt9;
		protected Matchnet.Web.Framework.Txt ReturnCheckFeeByStateTxt;
		protected Matchnet.Web.Framework.Txt ReturnCheckFeesTxt;
		protected Matchnet.Web.Framework.Txt FeeShowTxt;
		protected Matchnet.Web.Framework.Txt CheckWriterTxt;
		protected Matchnet.Web.Framework.Txt TransactionAuthorizationTxt;
		protected Matchnet.Web.Framework.Txt SubscriptionTermsTxt;
		protected Matchnet.Web.Framework.Ui.FormElements.FrameworkButton ProcessPurchase;
		protected Matchnet.Web.Framework.Txt Txt7;
		protected Matchnet.Web.Framework.Image imgStep2;
		protected Matchnet.Web.Framework.Image imgIconHelper;
		protected Matchnet.Web.Framework.Image imgCloseHelpInfo;
		protected Matchnet.Web.Framework.Txt Txt1;
		protected System.Web.UI.WebControls.RequiredFieldValidator rqfValCheckNumber;
		protected System.Web.UI.WebControls.RequiredFieldValidator rqfValRoutingNumber;
		protected System.Web.UI.WebControls.RequiredFieldValidator rqfValAccountNumber;
		protected Matchnet.Web.Framework.Image Image1;
		protected System.Web.UI.WebControls.CustomValidator custValRoutingNumber;
		protected System.Web.UI.WebControls.LinkButton lbtnProcessOrder;
		protected System.Web.UI.WebControls.CustomValidator custValPhone;
		protected System.Web.UI.WebControls.PlaceHolder phlPlanHeadersInUse;
		protected System.Web.UI.WebControls.Panel pnlPlanDisplayInUse;
        protected System.Web.UI.WebControls.Literal literalRemainingCredit;
		
        private System.Text.StringBuilder m_strPlanIDList = null; 

        private PlansDisplayMode mode = PlansDisplayMode.DefaultSubscription;
        private bool adminDisplayMode = false; //this is used to determine whether we are displaying in admin mode, affects css and displays
        private Int32 _promoID = Constants.NULL_INT;

		#endregion
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			this.TogglePlanSection();
			this.BindPlans();
//			this.RestoreSelectedPlan();
			base.g.LeftNavControl.Visible = false;			
			this.lnkPayByCreditCard.NavigateUrl = SubscriptionHelper.GetPayByCreditCardURL(base.g);
			//TT#21164	 hiding the top nav
			//modified by RB- 20070416
			//g.HeaderControl.ShowBlankHeader(Matchnet.Web.Framework.Ui.HeaderImageType.Default);
            //g.SetTopAuxMenuVisibility(false,true);
			/* Had to roll this change back RB- 20070420
			g.FooterControl.HideFooterLinks();			
			*/
			//TT#21164	

			if(! IsPostBack)
			{
				if(base.ShowError())
				{
					return;
				}
			}
	
			this.CheckProcessStatus();
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
			
			//Matchnet.Web.Framework.Popup SubscribePopup = new Popup();
			//base.g.Page.Controls.Add(SubscribePopup);
			//SubscribePopup.Width = FreeTrialCtrl.FREETRIALPOPUP_WIDTH;
			//SubscribePopup.Height = FreeTrialCtrl.FREETRIALPOPUP_HEIGHT;
			//base.SetFTPopupStatus(SubscribePopup);		
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			//this.custValPlanSelection.ServerValidate += new ServerValidateEventHandler(custValPlanSelection_ServerValidate);
			this.lbtnProcessOrder.Click+=new EventHandler(lbtnProcessOrder_Click);// += new System.Web.UI.ImageClickEventHandler(this.lbtnProcessOrder_Click);
			this.Load += new System.EventHandler(this.Page_Load);
			//this.dlistMorePlans.ItemDataBound += new DataListItemEventHandler(dlistMorePlans_ItemDataBound);
		}
		#endregion

		#region HELPER METHOD

		private void CheckProcessStatus()
		{
			if ( Request["CheckStatus"] == "1" )
			{

				int memberTranID = GetDecryptedValue(Request["mtid"]);
				int memberPaymentID = GetDecryptedValue(Request["mpid"]);

				try
				{
					int attempt = 0;
						
					if ( Request["Attempt"] != null )
					{
						attempt = Matchnet.Conversion.CInt(Request["Attempt"]);
					}
						
					SubscriptionResult sr = PurchaseSA.Instance.GetMemberTranStatus(g.TargetMemberID, memberTranID);
					LastSelectedPlanID = sr.PlanID;
						
					switch (sr.MemberTranStatus)
					{
						case MemberTranStatus.Success:
							ShowSuccess();
							break;
						case MemberTranStatus.Failure:
							RedirectFailure("SubscribeByCheck", sr.ResourceConstant, memberPaymentID, memberTranID);
							break;
						case MemberTranStatus.Pending:
							ShowInProgress( memberTranID, "SubscribeByCheck", attempt, sr.MemberPaymentID);
							break;
					}
				}
				catch(System.Threading.ThreadAbortException)
				{
				}
			}
		}

		private void LoadPopupCarrotData(int memberID)
		{
			Matchnet.Web.Framework.Ui.BasicElements.MiniProfile control = (Matchnet.Web.Framework.Ui.BasicElements.MiniProfile) LoadControl("~/Framework/Ui/BasicElements/MiniProfile.ascx");
			control.Member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
			control.Transparency = true;
			control.MemberID = memberID;
			this.phTopCarrot.Controls.Add(control);
		}
		private void TogglePlanSection()
		{			 
			bool openedByPopup = (base.g.LayoutTemplate == LayoutTemplate.Popup) || (base.g.LayoutTemplate == LayoutTemplate.PopupProfile);
			this.Page.RegisterClientScriptBlock("inPopup", "<script language='javascript'>var inPopup = " + openedByPopup.ToString().ToLower() + ";</script>");

			if(g.RecipientMemberID > 0)
			{
				PanelCarrotPlanSelection.Visible = true;
				PanelNormalPlanSelection.Visible = false;

				phlPlanHeadersInUse = phlCarrotPlanHeaders;
				pnlPlanDisplayInUse = pnlCarrotPlanDisplay;
					
//				this._rdoPlanTopInUse = this.rdoPlanTop2;
//				this._rdoPlanMediumInUse = this.rdoPlanMedium2;
//				
//				this._lblPlanInfoTopInUse = this.lblPlanTopInfo2;
//				this._lblPlanInfoMediumInUse = this.lblPlanMediumInfo2;
				
				if(openedByPopup)
				{					
					this.phTopCarrot.Visible = true;
					this.phRightCarrot.Visible = false;					
					this.LoadPopupCarrotData(g.RecipientMemberID);
					this.TogglePopupCss(this.divPlanGrid, "subTopPop");
					this.TogglePopupCss(this.tblInputFields, "subRightPop");
					this.TogglePopupCss(this.tblInputFields2, "subLeftPop");
					this.TogglePopupCss(this.divPayByPhone, "regularPageViewBorder");
					this.TogglePopupCss(this.divPayByCheck, "regularPageViewBorder");
					this.TogglePopupCss(this.divPayByCheckInfo, "regularPageViewBorder");
					
					this.TogglePopupCss(this.divParentContainer, "");
				}
				else
				{					
					this.phTopCarrot.Visible = false;
					this.phRightCarrot.Visible = true;
					this.LoadCarrotData(g.RecipientMemberID);
					this.TogglePopupCss(this.tblInputFields, "subRight");
					this.TogglePopupCss(this.tblInputFields2, "subLeft");
					this.TogglePopupCss(this.divPlanGrid, "subTopCarrot");
					this.TogglePopupCss(this.divPayByPhone, "fullPageViewBorder");
					this.TogglePopupCss(this.divPayByCheck, "fullPageViewBorder");		
					this.TogglePopupCss(this.divPayByCheckInfo, "fullPageViewBorder");
					this.TogglePopupCss(this.divParentContainer, "fullPageViewAS");
				}				
			}
			else				
			{
				PanelNormalPlanSelection.Visible = true;
				PanelCarrotPlanSelection.Visible = false;				

				phlPlanHeadersInUse = phlPlanHeaders;
				pnlPlanDisplayInUse = pnlPlanDisplay;

//				this._rdoPlanTopInUse = this.rdoPlanTop;
//				this._rdoPlanMediumInUse = this.rdoPlanMedium;				
//
//				this._lblPlanInfoTopInUse = this.lblPlanTopInfo;
//				this._lblPlanInfoMediumInUse = this.lblPlanMediumInfo;				
			}			
		}
		
		private void TogglePopupCss(HtmlControl control, string className)
		{
			control.Attributes["class"] = className;
		}

		private void LoadCarrotData(int memberID)
		{
			Member.ServiceAdapters.Member member = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, 
				Member.ServiceAdapters.MemberLoadFlags.None);
						
			Matchnet.Member.ValueObjects.Photos.Photo photo = MemberPhotoHelper.GetPhoto(member, base.g.Brand);
			if(photo != null && photo.ThumbFileWebPath != null && photo.ThumbFileWebPath != "")
			{
				this.imgUserPix.ImageUrl = photo.ThumbFileWebPath + "?siteID=" + base.g.Brand.Site.SiteID.ToString();
			}
			else
			{
				this.imgUserPix.ImageUrl = MemberPhotoHelper.GetTinyThumbPhotoURL(member, base.g.Brand);
			}

			if(member.Username != null && member.Username.Length > 10)
			{
				lblUserName.Text = member.Username.Substring(0, 6) + "...";
			}
			else
			{
				this.lblUserName.Text = member.Username;		
			}
			
			this.lblLocation.Text = Framework.FrameworkGlobals.GetRegionString(member.GetAttributeInt(g.Brand, "regionid"), g.Brand.Site.LanguageID, false,true,false);

			DateTime dob = member.GetAttributeDate(base.g.Brand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE);
			int age = DateTime.Today.Year - dob.Year;
			if(DateTime.Today.DayOfYear > dob.DayOfYear)
			{
				age++;
			}
			this.lblAge.Text = age.ToString();
		}

		private string GetPayByCreditCardURL()
		{
			try
			{
				//getting rid of extra ampersand at end
				return LinkFactory.Instance.GetLink("/Applications/Subscription/Subscribe.aspx?OverrideTransactionHistory=1"
					+ g.AppendDestinationURL(), g.Brand, Request.Url.ToString());
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
				return "";
			}
		}

//		private void RestoreSelectedPlan()
//		{
//			int planID = SubscriptionHelper.GetSelectedPlan(base.g);
//			if(planID != int.MinValue)
//			{
//				if(_rdoPlanTopInUse.Attributes["pid"] == planID.ToString())
//					_rdoPlanTopInUse.Checked = true;
//				if(_rdoPlanMediumInUse.Attributes["pid"] == planID.ToString())
//					_rdoPlanMediumInUse.Checked = true;
//			}
//		}

        /*
        private int GetSelectedPlanIDFromPlanList()
        {
            foreach (DataListItem item in this.dlistMorePlans.Items)
            {
                RadioButton rdoPlan = (RadioButton)item.FindControl("rdoPlan");
                if (rdoPlan.Checked)
                {
                    return int.Parse(rdoPlan.Attributes["pid"]);
                }
            }

            return 0;
        }
        */

//		private int GetSelectedPlanID()
//		{
//			foreach(DataListItem item in this.dlistMorePlans.Items)
//			{
//				RadioButton rdoPlan = (RadioButton)item.FindControl("rdoPlan");
//				if(rdoPlan.Checked)
//				{
//					return int.Parse(rdoPlan.Attributes["pid"]);
//				}
//			}
//
//			if(this._rdoPlanTopInUse.Checked)
//				return int.Parse(this._rdoPlanTopInUse.Attributes["pid"]);
//			else if(this._rdoPlanMediumInUse.Checked)
//				return int.Parse(this._rdoPlanMediumInUse.Attributes["pid"]);
//			else
//				return 0;
//		}

		private void BindPlans()
		{
			MemberSub memSub = null;

			if(! this.IsPostBack )
			{
				if(MemberPrivilegeAttr.IsCureentSubscribedMember(g))
				{						
					memSub = PurchaseSA.Instance.GetSubscription(base.g.Member.MemberID, base.g.Brand.Site.SiteID);

					#region Due to PM change, no plan should be selected for new users.
//					if(memSub == null)
//					{
//						this._rdoPlanTopInUse.Checked = true;
//					}
					#endregion
				}

				#region Due to PM change, no plan should be selected for new users.
//				else
//				{
//					this._rdoPlanTopInUse.Checked = true;
//				}
				#endregion
			}

			// Get all the available plans.
			PromoPlanCollection plans = null;
            Promo promo = null;
            mode = PlansDisplayMode.DefaultSubscription;
            bool showAllAdminPlans = false;

			if (g.IsAdmin && g.Member != null)
			{
                int memberIDtoLoad = Constants.NULL_INT;

				if(Request.QueryString["impmid"] != null)
				{
					if (Request.QueryString["tran"] == null)
					{
                        // Admin is viewing plans for a target user from the admin page.
                        // SubscribeNew is inside a IFRAME.
						showAllAdminPlans = true;
					}
					else
					{
                        // Arrived at this page by clicking on the "View" link on the account history page.
                        // Case 1 - In the case of admin looking at his own account history, impmid is actually the memberID of the admin account.
                        // Case 2 - In the case of regular user, impmid is the memberID of the target member.
						showAllAdminPlans = false;
					}

                    // Either case, targetMember should be loaded based on impmid, so just set memberIDtoLoad targetMember's ID
                    memberIDtoLoad = g.TargetMemberID;
				}
				else
				{
                    // No impersonation is happening here
					showAllAdminPlans = false;
                    memberIDtoLoad = g.Member.MemberID;
				}

				if (showAllAdminPlans)
				{
                    // This flag is used to control which panel to load.  I could have skipped detecting for showAllAdminPlans and set adminDisplayMode directly, but
                    // wanted to preserve the logic for capturing showAllAdminPlans just in case.
                    this.adminDisplayMode = true;
				}

                promo = Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetMemberPromo(memberIDtoLoad, g.TargetBrand, PaymentType.Check);
                plans = promo.PromoPlans;
			}
			else
			{
                promo = Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetMemberPromo(g.Member.MemberID, g.Brand, PaymentType.Check);
                plans = promo.PromoPlans;
			}

			if(Request.QueryString["impmid"] != null)
			{
				if(Request.QueryString["tran"] != null)
				{
					NameValueCollection nvc = PurchaseSA.Instance.GetMemberPaymentByMemberTranID(g.TargetMemberID, int.Parse(Request.QueryString["tran"]));
					if ( nvc != null && nvc.Count > 0 )
					{
						System.Web.UI.Control curControl;
						TextBox tb;
						DropDownList ddl;
							
						foreach (string key in nvc.Keys)
						{
							curControl = FindControl(key);
							if (curControl is TextBox)
							{
								tb = (TextBox)curControl;
								tb.Text = nvc[key];
							}
							else if (curControl is DropDownList)
							{
								ddl = (DropDownList)curControl;
								try
								{
									ddl.SelectedValue = nvc[key];
								}
								catch
								{
									// nada
								}
							}
						}						
					}
				}
				
			}

            // Let the promoID here before binding. We need this later when purchasing
            _promoID = promo.PromoID;

            #region Bind Plans
            //Admin version
            if (this.adminDisplayMode)
				{
                //display total remaining credit
                decimal remainingCredit = 0m;
                if (plans.Count > 0 && plans[0].CreditAmount != Constants.NULL_DECIMAL)
                    remainingCredit = plans[0].CreditAmount;
                this.literalRemainingCredit.Text = "Unused credit as of " + DateTime.Now.ToShortDateString() + ": " + remainingCredit.ToString("c");

                PlanGroupDisplay ucPlancGroupDisplayAdmin = null;
                ucPlancGroupDisplayAdmin = (PlanGroupDisplay)Page.LoadControl(@"/Applications/Subscription/Controls/PlanGroupDisplay.ascx");
                ucPlancGroupDisplayAdmin.ColumnGroup = 1;
                ucPlancGroupDisplayAdmin.ShowRowHeadersOnly = false; //admin page does not show the row headers
                ucPlancGroupDisplayAdmin.ShowPlanColumnHeader = false; //admin page does not show the column header (plan type)
                ucPlancGroupDisplayAdmin.PlanTypeDisplayMode = PlansDisplayMode.AdminMode; //this will instruct the control not to select any default plan

                // Change css when there is only a single group of plans
                ucPlancGroupDisplayAdmin.SinglePlanGroupMode = true;

                ucPlancGroupDisplayAdmin.DataSource = promo;
                PanelAdminPlanForMember.Visible = true;
                PanelAdminPlanForMember.Controls.Add(ucPlancGroupDisplayAdmin);

					this.txtTopBenefits.Visible = this.PanelCarrotPlanSelection.Visible = this.PanelNormalPlanSelection.Visible = false;

					SetPlanListForOmniture(plans);

				}					
            else
            {
                //Member version
			if (plans != null)
			{
				PlanGroupDisplay ucPlancGroupDisplay = null;

				ucPlancGroupDisplay = (PlanGroupDisplay) Page.LoadControl(@"/Applications/Subscription/Controls/PlanGroupDisplay.ascx");
				ucPlancGroupDisplay.ShowRowHeadersOnly = true;
				ucPlancGroupDisplay.DataSource = promo;
				phlPlanHeadersInUse.Controls.Add(ucPlancGroupDisplay);

                foreach (Matchnet.PromoEngine.ValueObjects.ResourceTemplate.HeaderResourceTemplate headerResourceTemplate in promo.ColHeaderResourceTemplateCollection)			
				{
					ucPlancGroupDisplay = (PlanGroupDisplay) Page.LoadControl(@"/Applications/Subscription/Controls/PlanGroupDisplay.ascx");
                    ucPlancGroupDisplay.ColumnGroup = headerResourceTemplate.HeaderNumber;
                    if (promo.ColHeaderResourceTemplateCollection.Count == 1)
					{
						// Change css when there is only a single group of plans
						ucPlancGroupDisplay.SinglePlanGroupMode = true;
					}				
					int sessionPlanID = SubscriptionHelper.GetSelectedPlan(base.g);
					if(sessionPlanID != int.MinValue)
					{
						// Select plan from session first
						ucPlancGroupDisplay.PlanTypeDisplayMode = PlansDisplayMode.RestoreFromSession;
						ucPlancGroupDisplay.DefaultPlanID = sessionPlanID;					
					}
					else
					{
						// If selected plan is not in session, use the default mode 
						ucPlancGroupDisplay.PlanTypeDisplayMode = PlansDisplayMode.DefaultSubscription;
					}					
					ucPlancGroupDisplay.DataSource = promo;
					pnlPlanDisplayInUse.Controls.Add(ucPlancGroupDisplay);
				}

				SetPlanListForOmniture(plans);
			}


		}

            #endregion
        }

		
		#endregion

        /*
		private int GetSelectedPlanIDFromAllGroups(Control c)
		{
			System.Web.UI.HtmlControls.HtmlInputRadioButton rdoPlan;
			int planID = Constants.NULL_INT;

			if (c is PlanRadioButton)
			{
				rdoPlan = c as PlanRadioButton;

				if (rdoPlan.Checked)
				{
					if (rdoPlan.Value != Constants.NULL_STRING)
					{
						planID = int.Parse(rdoPlan.Value);
					}
				}
			}
			else
			{
				foreach(Control child in c.Controls)
				{
					if (planID == Constants.NULL_INT)
					{
						planID = GetSelectedPlanIDFromAllGroups(child);
					}
					else
					{
						break;
					}
				}
			}
			
			return planID;
		}
        */

		private void lbtnProcessOrder_Click(object sender, EventArgs e)
		{
			try
			{
				Page.Validate();
				if(Page.IsValid)
				{				
					
					int adminMemberID = Constants.NULL_INT;
					int conversionMemberID = Constants.NULL_INT;
					string phoneNumber = ConvertToRegex(Phone.Text, "[0-9]");
					int sourceID = Conversion.CInt( Request["srid"] ) != Constants.NULL_INT ? Conversion.CInt( Request["srid"] ) : Constants.NULL_INT;
					int purchaseReasonTypeID = Conversion.CInt( Request["prtid"] ) != Constants.NULL_INT ? Conversion.CInt( Request["prtid"] ) : Constants.NULL_INT;

                    int planIDFromAllGroups = Constants.NULL_INT;

                    //get selected plan (admin or member view)
                    SelectedPlanInformation selectedPlanInformation = new SelectedPlanInformation(Constants.NULL_INT, PurchaseMode.None, Constants.NULL_DECIMAL);
                    if (this.adminDisplayMode)
                    {
                        selectedPlanInformation = base.GetSelectedPlanIDFromAllGroups(this.PanelAdminPlanForMember);
                    }
                    else
                    {
                        selectedPlanInformation = base.GetSelectedPlanIDFromAllGroups(this.pnlPlanDisplayInUse);
                    }
                    int planID = selectedPlanInformation.PlanID;

					SubscriptionHelper.StoreSelectedPlan(base.g, planID);

					if (g.IsAdmin && g.Member != null)
						adminMemberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
			
					if (g.Session["ClickMemberID"] != null)
						conversionMemberID = Conversion.CInt(g.Session["ClickMemberID"]);


					SubscriptionResult sr = PurchaseSA.Instance.BeginCheckPurchase(	g.TargetMemberID, adminMemberID, planID,
						Constants.NULL_INT, g.TargetBrand.Site.SiteID, 	g.TargetBrand.BrandID,
						FirstName.Text, LastName.Text, phoneNumber, AddressLine1.Text, string.Empty,
						City.Text, State.Text, PostalCode.Text, DriversLicenseNumber.Text,
						DriversLicenseState.SelectedValue, null, BankRoutingNumber.Text, 
						CheckingAccountNumber.Text, Convert.ToInt32(CheckNumber.Text),
						conversionMemberID, (BankAccountType)Convert.ToInt32(BankAccountType.SelectedValue),
						sourceID, purchaseReasonTypeID, g.ClientIP,
                        selectedPlanInformation.CreditAmount, selectedPlanInformation.PurchaseMode, _promoID);
			
					g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_CHECK_ACCTNO,
						CheckingAccountNumber.Text,
						Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

					g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_CHECK_ROUTINGNO,
						BankRoutingNumber.Text,
						Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

					g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_CHECK_DLNO,
						DriversLicenseNumber.Text,
						Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

					if ( sr.ReturnValue == Constants.RETURN_OK )
					{
						ShowInProgress( sr.MemberTranID, "SubscribeByCheck", 0 , sr.MemberPaymentID);
					}
					else
					{
						g.Notification.AddMessage( sr.ResourceConstant );
					}
				}
			
			}
			catch(Exception ex)
			{
				
				g.ProcessException(ex);
			}
		}

		
//        private void custValPlanSelection_ServerValidate(object source, ServerValidateEventArgs args)
//        {
//            if (this.dlistMorePlans.DataSource != null)
//            {
//                args.IsValid = (base.GetSelectedPlanIDFromPlanList(this.dlistMorePlans) != 0);
//            }
//            else
//            {
//                args.IsValid = (base.GetSelectedPlanIDFromAllGroups(this.pnlPlanDisplayInUse).PlanID != Constants.NULL_INT);
//            }

////			args.IsValid = (this.GetSelectedPlanID() != 0);
//        }


		private void dlistMorePlans_ItemDataBound(object sender, DataListItemEventArgs e)
		{
			if(e.Item.DataItem != null)
			{				
				Label lblPlanTitle = (Label)e.Item.FindControl("lblPlanTitle");
				RadioButton rdoPlan = (RadioButton)e.Item.FindControl("rdoPlan");
                PromoPlan plan = (PromoPlan)e.Item.DataItem;	
								
				if((int)plan.CurrencyType != g.Brand.Site.CurrencyID)
				{
					e.Item.Controls.Clear();
				}
				else
				{
					if(plan.EndDate != DateTime.MinValue)
					{
						lblPlanTitle.ForeColor = System.Drawing.Color.Red;
					}
				
					rdoPlan.Attributes["pid"] = plan.PlanID.ToString();
                    lblPlanTitle.Text = ResourceTemplateSA.Instance.GetResource(plan.ResourceTemplateID, new PlanTokenReplacer(plan));
                    
					if(rdoPlan.Attributes["pid"] == SubscriptionHelper.GetSelectedPlan(base.g).ToString())
					{
						rdoPlan.Checked = true;
					}
				}				
			}
		}
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender (e);

            //admin style
            if (this.adminDisplayMode)
            {
                this.divParentContainer.Attributes.Remove("class");
                this.divParentContainer.Attributes.Add("class", "fullPageViewAS adminDisplay");
            }

			// Omniture Analytics - Event Tracking
			if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
			{
				// Without this check, this renders even on confirmation, pending, etc :| ...
				if ((SuccessPanel.Visible == false) && (PollingPanel.Visible == false))
				{
					// Subscription Start
					_g.AnalyticsOmniture.Events = "event9";

					// Subscription Plans
					string plans = Constants.NULL_STRING;

//					plans += this.rdoPlanMedium.Attributes["pid"] + ",";
//					plans += this.rdoPlanTop.Attributes["pid"];

					if (m_strPlanIDList != null)
					{
						plans = m_strPlanIDList.ToString();
						_g.AnalyticsOmniture.Evar3 = plans.TrimEnd(new char[]{','});
					}						

//					_g.AnalyticsOmniture.Evar3 = plans.TrimEnd(new char[]{','});
				}
			}
		}

		private void SetPlanListForOmniture(Matchnet.PromoEngine.ValueObjects.PromoPlanCollection plans)
		{
			// For setting up Omniture plan list
			m_strPlanIDList = new System.Text.StringBuilder();
			foreach (Purchase.ValueObjects.Plan displayedPlan in plans)
			{
				if (displayedPlan != null)
				{
					if (displayedPlan.PlanID > 0)
					{
						m_strPlanIDList.Append(displayedPlan.PlanID.ToString() + ",");
					}
				}
			}
		}

	}
}
