﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionConfirmation20.ascx.cs" Inherits="Matchnet.Web.Applications.Subscription.SubscriptionConfirmation20" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<asp:PlaceHolder ID="PlaceHolderOmnitureMboxSubscriptionConfirmation" runat="server">
<div class="mboxDefault"></div><script language="JavaScript1.2"><%=MBoxScript%></script>  

 <script type="text/javascript">
     //set TNT variable when page loads (needs to be after mboxes have loaded) 
     var tntInput = s.trackTNT();

     //call this function after everything else has loaded 
     trackTNT(s_account, tntInput);

     /* For tracking TNT data in SC */
     function trackTNT(s_account, tntInput) {
         var s = s_gi(s_account);

         s.linkTrackVars = "tnt, eVar16"
         s.linkTrackEvents = "None"

         //variable for TNT classifications 
         s.eVar16 = s.tnt = tntInput;

         s.tl(false, 'o', 'For tracking TNT');
     } 
    </script>
    
</asp:PlaceHolder>

<h1><mn:txt runat="server" id="Txt22" ResourceConstant="TXT_H1_SUB_CONFIRMATION" /></h1>
<asp:PlaceHolder runat="server" Visible="false" ID="phMicroProfile">
<div class="carrot-profile" id="divMiniProfile" runat="server">
    <div class="rbox-style-clear clear-floats-after">
        <asp:PlaceHolder id="phMiniProfile" Runat="server" />
    </div>
</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="plcConfirmation" Runat="server">
    <mn:Txt id="txtRecordInfo" runat="server" />
</asp:PlaceHolder>

<asp:PlaceHolder ID="PlaceHolderColorCodeConfirmation" runat="server" Visible="false">
    <mn:Txt id="TxtColorCodeSubscriptionConfirmation" ResourceConstant="TXT_COLORCODE_SUBSCRIPTION_CONFIRMATION"
     ExpandImageTokens="true" runat="server" />
    <mn2:FrameworkHtmlButton runat="server" id="FrameworkButtonStartTest" ResourceConstant="BUTTON_START_READING_NOW" class="primary_new" />
</asp:PlaceHolder>

<asp:PlaceHolder ID="plcFreeTrialConfirmation" runat="server" Visible="false">
    <mn:Txt id="txtFreeTrialConfirmation" ResourceConstant="TXT_FREETRIAL_SUBSCRIPTION_CONFIRMATION"
     ExpandImageTokens="true" runat="server" />
    <mn2:FrameworkHtmlButton runat="server" id="btnFreeTrialConfirmation" ResourceConstant="BUTTON_START_READING_NOW" class="primary_new" />
</asp:PlaceHolder>

<asp:PlaceHolder ID="PlaceHolderOtherStuff" runat="server">

<div id="sub-conf-continue" style="position:relative;text-align:center;width:100%;padding-bottom:10px;">
    <style>input.primary_new {margin:0 auto;}</style>
    <mn2:frameworkbutton id="ProcessAttributesAndContinue" Visible="false" runat="server" class="primary_new" ResourceConstant="BTN_CONTINUE_RAQUO" OnClick="ProcessAttributesAndContinue_Click" />
    <mn2:FrameworkHtmlButton runat="server" id="Continue" ResourceConstant="BTN_CONTINUE" class="primary_new" />
</div>

<asp:PlaceHolder ID="plcFreeTrialConfirmationAdditional" runat="server" Visible="false">
    <mn:Txt id="txtFreeTrialConfirmationAdditional" ResourceConstant="TXT_FREETRIAL_SUBSCRIPTION_CONFIRMATION_ADDITIONAL"
     ExpandImageTokens="true" runat="server" />
</asp:PlaceHolder>

<div class="tips border-gen items-03 rbox-style-clear">
    <h2><mn:txt runat="server" id="txtNextStep" ResourceConstant="TXT_NEXT_STEP" /></h2>
    <p><mn:txt id="txtPlan" runat="server" ResourceConstant="TXT_MAKE_THE_MOST" /></p>
    <div class="item">
	    <mn:txt runat="server" id="txtCell1" ExpandImageTokens="true" ResourceConstant="HTML_PHOTO_UPLOAD" />
    </div>
    <div class="item">
	    <mn:txt runat="server" id="txtCell2" ExpandImageTokens="true" ResourceConstant="HTML_EMAIL" />
    </div>
    <div class="item last">
	    <mn:txt runat="server" id="txtCell3" ExpandImageTokens="true" ResourceConstant="HTML_HURRYDATE" />
    </div>
</div>

<mn:txt runat="server" ID="txtSlider" ExpandImageTokens="true" Visible="false" />

</asp:PlaceHolder>

<%--<div runat="server" id="RowComposeMessage">
    <asp:Panel Runat="server" ID="PanelComposeMessage">
        <h2>Your message is ready to send &#9658; Review it now</h2>			
        <mn:image id="Image2" runat="server" filename="icon_email.gif" />
        <p>Now that you're a Premium Member, you're ready to connect. Click Continue to review your message and then send away.</p>
    </asp:Panel>
</div>--%>

 