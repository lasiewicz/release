#region System References
using System;
using System.Web;
using System.Web.UI;
#endregion
	
#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Web.Applications.Subscription.Controls;
using Matchnet.Web.Framework.Enumerations;
#endregion

#region Matchnet Service References
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Member.ServiceAdapters;
#endregion

namespace Matchnet.Web.Applications.Subscription
{
	/// <summary>
	///		Summary description for SubscriptionMain.
	/// </summary>
	public class RenewalNew : SubscribeCommon
	{
		#region MEMBER

		protected Matchnet.Web.Framework.Txt lblHeader;
		protected System.Web.UI.WebControls.Panel PanelNormalPlanSelection;
		protected System.Web.UI.WebControls.PlaceHolder phlPlanHeaders;
		protected System.Web.UI.WebControls.Panel pnlPlanDisplay;
//		protected System.Web.UI.WebControls.Label lblPlanTopInfo;
//		protected System.Web.UI.WebControls.Label lblPlanMediumInfo;
//		protected System.Web.UI.WebControls.Label lblPlanLowInfo;
		protected Matchnet.Web.Framework.Txt txtAccount;
		protected System.Web.UI.HtmlControls.HtmlInputHidden Hidden1;
//		protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdoPlanTop;
//		protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdoPlanMedium;
		protected System.Web.UI.WebControls.LinkButton lbtnProcessOrder;
		protected Matchnet.Web.Framework.Image imgStep2;
		protected System.Web.UI.WebControls.HyperLink lnkPayByCheck;
		protected System.Web.UI.WebControls.HyperLink lnkSubscribePage;
//		protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdoPlanLow;
		protected System.Web.UI.WebControls.CustomValidator custValPlanSelection;
//		protected System.Web.UI.WebControls.PlaceHolder PlaceHolder5MonthGuaranteed;

		private System.Text.StringBuilder m_strPlanIDList = null;
        private Int32 _promoID = Constants.NULL_INT;
 
		#endregion

		#region HELPER METHOD
		
		private void RenderHeaderText()
		{
			lblHeader.ResourceConstant = "SUB_TOP_BENEFITS";
		}

		private void BindPlans()
		{
			MemberSub memSub = null;

			if(! this.IsPostBack )
			{
				if(MemberPrivilegeAttr.IsCureentSubscribedMember(g))
				{						
					memSub = PurchaseSA.Instance.GetSubscription(base.g.Member.MemberID, base.g.Brand.Site.SiteID);

					#region Due to PM change, no plan should be selected for new users.
//					if(memSub == null)
//					{
//						this.rdoPlanTop.Checked = true;
//					}
					#endregion
				}

				#region Due to PM change, no plan should be selected for new users.
//				else
//				{
//					this.rdoPlanTop.Checked = true;
//				}
				#endregion
			}

			// Get all the available plans.
			PromoPlanCollection plans = null;
            Promo promo = null;

            //Note: There should not be impersonation on the renewal pages, since admins will never get to this page on behalf of member.
            // If no impersonation, the way we retrieve the plans are the same for admin and a normal member now.
//            if (g.IsAdmin && g.Member != null)
//            {
//                //get plans for admin viewing their own subscription
//                plans = PlanAdminSA.Instance.GetAdminDisplayedPlans(g.TargetBrand, PaymentType.CreditCard, g.Member.MemberID);			
////				plans = PlanAdminSA.Instance.GetPlans(g.TargetBrand.BrandID, PaymentType.CreditCard);			
//            }
//            else
//            {
//                //get plans for member
//                plans = PlanSA.Instance.GetMemberDisplayedPlans(g.TargetBrand, PlanType.Regular, PaymentType.CreditCard, g.Member.MemberID);
//            }

            promo = Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetMemberPromo(g.Member.MemberID, g.Brand, PaymentType.CreditCard);
            _promoID = promo.PromoID;
            plans = promo.PromoPlans;

			if (plans != null)
			{
				PlanGroupDisplay ucPlancGroupDisplay = null;

				ucPlancGroupDisplay = (PlanGroupDisplay) Page.LoadControl(@"/Applications/Subscription/Controls/PlanGroupDisplay.ascx");
				ucPlancGroupDisplay.ShowRowHeadersOnly = true;
				ucPlancGroupDisplay.DataSource = promo;
				phlPlanHeaders.Controls.Add(ucPlancGroupDisplay);

                foreach (Matchnet.PromoEngine.ValueObjects.ResourceTemplate.HeaderResourceTemplate headerResourceTemplate in promo.ColHeaderResourceTemplateCollection)			
				{
					ucPlancGroupDisplay = (PlanGroupDisplay) Page.LoadControl(@"/Applications/Subscription/Controls/PlanGroupDisplay.ascx");
                    ucPlancGroupDisplay.ColumnGroup = headerResourceTemplate.HeaderNumber;
                    if (promo.ColHeaderResourceTemplateCollection.Count == 1)
					{
						// Change css when there is only a single group of plans
						ucPlancGroupDisplay.SinglePlanGroupMode = true;
					}				
					int sessionPlanID = SubscriptionHelper.GetSelectedPlan(base.g);
					if(sessionPlanID != int.MinValue)
					{
						// Select plan from session first
						ucPlancGroupDisplay.PlanTypeDisplayMode = PlansDisplayMode.RestoreFromSession;
						ucPlancGroupDisplay.DefaultPlanID = sessionPlanID;					
					}
					else
					{
						// If selected plan is not in session, use the default mode 
						ucPlancGroupDisplay.PlanTypeDisplayMode = PlansDisplayMode.DefaultSubscription;
					}					
					ucPlancGroupDisplay.DataSource = promo;
					pnlPlanDisplay.Controls.Add(ucPlancGroupDisplay);
				}

				// For setting up Omniture plan list
				m_strPlanIDList = new System.Text.StringBuilder();
				foreach (Purchase.ValueObjects.Plan displayedPlan in plans)
				{
					if (displayedPlan != null)
					{
						if (displayedPlan.PlanID > 0)
						{
							m_strPlanIDList.Append(displayedPlan.PlanID.ToString() + ",");
						}
					}
				}
			}
		}
		private void CheckProcessStatus()
		{
			// Put user code to initialize the page here
			if ( Request["CheckStatus"] != null && Request["CheckStatus"] == "1" )
			{
				int memberTranID = GetDecryptedValue(Request["mtid"]);
				int memberPaymentID = GetDecryptedValue(Request["mpid"]);

				try
				{
					if ( memberTranID == 0 )
					{
						if ( (Request["MD"] != null && Request["MD"].Length > 0) && (Request["PaRes"] != null && Request["PaRes"].Length > 0) )
						{
							memberTranID = Convert.ToInt32( Request["MD"] );
						}

						if ( memberTranID == 0 )
						{
							RedirectFailure("renewal", "MNCHARGE___GENERAL_DATABASE_FAILURE_13076", memberPaymentID, 0, false);
						}
					}

					int attempt = 0;
					if ( Request["Attempt"] != null )
						attempt = Matchnet.Conversion.CInt(Request["Attempt"]);

					// Now let's check the status and choose the outcome...
					SubscriptionResult sr;
					sr = PurchaseSA.Instance.GetMemberTranStatus(_g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), memberTranID);

					LastSelectedPlanID = sr.PlanID;

					switch (sr.MemberTranStatus)
					{
						case MemberTranStatus.Success:
							ShowSuccess();
							break;

						case MemberTranStatus.Failure:
							RedirectFailure("renewal", sr.ResourceConstant, memberPaymentID, memberTranID, false);
							break;

						case MemberTranStatus.Pending:
						case MemberTranStatus.VerifiedByVisaPending:
						case MemberTranStatus.AuthenticationPending:
							ShowInProgress(memberTranID, "Subscribe", attempt, memberPaymentID, false);
							break;
					}
				}
				catch(System.Threading.ThreadAbortException)
				{
				}
				catch
				{
					RedirectFailure("renewal", "MNCHARGE___GENERAL_DATABASE_FAILURE_13076", memberPaymentID, memberTranID, false);
				}
			}
		}

//		private void RestoreSelectedPlan()
//		{
//			int planID = SubscriptionHelper.GetSelectedPlan(base.g);
//			if(planID != int.MinValue)
//			{				
//				if(this.rdoPlanLow.Attributes["pid"] == planID.ToString())
//					this.rdoPlanLow.Checked = true;
//				if(this.rdoPlanMedium.Attributes["pid"] == planID.ToString())
//					this.rdoPlanMedium.Checked = true;
//				if(this.rdoPlanTop.Attributes["pid"] == planID.ToString())
//					this.rdoPlanTop.Checked = true;
//			}
//		}
	
		#endregion
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.custValPlanSelection.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(custValPlanSelection_ServerValidate);
			this.lbtnProcessOrder.Click+=new EventHandler(lbtnProcessOrder_Click);//  += new System.Web.UI.ImageClickEventHandler(this.lbtnProcessOrder_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion		

		/// <summary>
		/// Get configuration to to set the 5 months guaranteed base on the site settings.
		/// </summary>
//		private void Show5MonthsGuaranteed()
//		{
//			// default to false
//			PlaceHolder5MonthGuaranteed.Visible = false;
//			
//			string strConfigSetting = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_5_MONTHS_GUARANTEE",g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
//			if(strConfigSetting.ToUpper() == "TRUE")
//			{
//				PlaceHolder5MonthGuaranteed.Visible = true;
//			}
//		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            #region Redirect to Subscribe page
            //09162008 TL ET-92
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(WebConstants.SETTING_ENABLE_RENEWAL_TO_SUBSCRIBE_REDIRECT, _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID, _g.Brand.BrandID)))
            {
                int prtidTransfer = 82; //default is the the user tried to extend/change subscription
                if (Request.QueryString["prtid"] != null && !int.TryParse(Request.QueryString["prtid"], out prtidTransfer))
                {
                    prtidTransfer = 82;
                }

                Matchnet.Web.Framework.Util.Redirect.Subscription(g.Brand, prtidTransfer, Constants.NULL_INT, true);
            }
            #endregion

//			this.Show5MonthsGuaranteed();
			this.BindPlans();
//			this.RestoreSelectedPlan();
			this.RenderHeaderText();
			base.g.LeftNavControl.Visible = false;
			this.CheckProcessStatus();
			this.lnkPayByCheck.NavigateUrl = SubscriptionHelper.GetPayByCheckURL(base.g);
			this.lnkSubscribePage.NavigateUrl = SubscriptionHelper.GetPayByCreditCardURL(base.g);

			g.HeaderControl.ShowBlankHeader(Matchnet.Web.Framework.Ui.HeaderImageType.Default);
            g.SetTopAuxMenuVisibility(false,true);
			g.LeftNavControl.HideUserNameLogout();

		}

        /*
		private int GetSelectedPlanIDFromAllGroups(Control c)
		{
			System.Web.UI.HtmlControls.HtmlInputRadioButton rdoPlan;
			int planID = Constants.NULL_INT;

			if (c is PlanRadioButton)
			{
				rdoPlan = c as PlanRadioButton;

				if (rdoPlan.Checked)
				{
					if (rdoPlan.Value != Constants.NULL_STRING)
					{
						planID = int.Parse(rdoPlan.Value);
					}
				}
			}
			else
			{
				foreach(Control child in c.Controls)
				{
					if (planID == Constants.NULL_INT)
					{
						planID = GetSelectedPlanIDFromAllGroups(child);
					}
					else
					{
						break;
					}
				}
			}
			
			return planID;
		}
        */
	
		private void lbtnProcessOrder_Click(object sender, EventArgs e)
		{
			try
			{
				this.Page.Validate();
				if(this.Page.IsValid)
				{
					int memberPaymentID = (Request["r1"] != null)? GetDecryptedValue(Request["r1"]): Constants.NULL_INT;				
					int conversionID = (g.Session["ClickMemberID"] != null)? Conversion.CInt(g.Session["ClickMemberID"]): Constants.NULL_INT;
					int prtID = Conversion.CInt( Request["prtid"] ) != Constants.NULL_INT ? Conversion.CInt(Request["prtid"]) : Constants.NULL_INT;
					int adminID = (g.IsAdmin && g.Member != null)? g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID): Constants.NULL_INT;
					int sourceID = Conversion.CInt( Request["srid"] ) != Constants.NULL_INT ? Conversion.CInt( Request["srid"] ) : Constants.NULL_INT;
					int purchaseReasonTypeID = Conversion.CInt( Request["prtid"] ) != Constants.NULL_INT ? Conversion.CInt( Request["prtid"] ) : Constants.NULL_INT;

					//int planID = (base.GetSelectedPlanIDFromAllGroups(this.PanelNormalPlanSelection)).PlanID;

                    SelectedPlanInformation selectedPlanInformation = new SelectedPlanInformation(Constants.NULL_INT, PurchaseMode.None, Constants.NULL_DECIMAL);

                    selectedPlanInformation = (base.GetSelectedPlanIDFromAllGroups(this.PanelNormalPlanSelection));

//					int planID = 0;
//					if(this.rdoPlanTop.Checked)
//						planID = int.Parse(this.rdoPlanTop.Attributes["pid"]);
//					else if(this.rdoPlanMedium.Checked)
//						planID = int.Parse(this.rdoPlanMedium.Attributes["pid"]);
//					else if(this.rdoPlanLow.Checked)
//						planID = int.Parse(this.rdoPlanLow.Attributes["pid"]);			
	
                    SubscriptionHelper.StoreSelectedPlan(base.g, selectedPlanInformation.PlanID);
			
                    SubscriptionResult sr = PurchaseSA.Instance.BeginRenewalPurchase(g.TargetMemberID, selectedPlanInformation.PlanID, Constants.NULL_INT, 
						g.TargetBrand.Site.SiteID, g.TargetBrand.Site.Community.CommunityID,
						g.TargetBrand.BrandID, adminID, memberPaymentID, conversionID,
						sourceID, purchaseReasonTypeID, g.ClientIP,
                        selectedPlanInformation.CreditAmount, selectedPlanInformation.PurchaseMode, _promoID);

					if ( sr.ReturnValue == Constants.RETURN_OK )
					{
						ShowInProgress(sr.MemberTranID, "Subscribe", 0, memberPaymentID, false);
					}
					else
					{
						g.Notification.AddMessage( sr.ResourceConstant );
					}
				}				
			}
			catch(Exception)
			{
				// MNCHARGE___GENERAL_DATABASE_FAILURE_13076
				g.Notification.AddError("MNCHARGE___GENERAL_DATABASE_FAILURE_13076");
			}
		}
		private void custValPlanSelection_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
		{
			args.IsValid = (base.GetSelectedPlanIDFromAllGroups(this.PanelNormalPlanSelection).PlanID != Constants.NULL_INT);
//			args.IsValid = (rdoPlanLow.Checked) || (rdoPlanMedium.Checked) || (rdoPlanTop.Checked);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender (e);

			// Omniture Analytics - Event Tracking
			if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
			{
				// Without this check, this renders even on confirmation, pending, etc :| ...
				if ((SuccessPanel.Visible == false) && (PollingPanel.Visible == false))
				{
					// Subscription Start
					_g.AnalyticsOmniture.Events = "event9";

					// Subscription Plans
					string plans = Constants.NULL_STRING;

//					plans += this.rdoPlanLow.Attributes["pid"] + ",";
//					plans += this.rdoPlanMedium.Attributes["pid"] + ",";
//					plans += this.rdoPlanTop.Attributes["pid"];

					if (m_strPlanIDList != null)
					{
						plans = m_strPlanIDList.ToString();
						_g.AnalyticsOmniture.Evar3 = plans.TrimEnd(new char[]{','});
					}

//					_g.AnalyticsOmniture.Evar3 = plans.TrimEnd(new char[]{','});				
				}
			}
		}
	}
}
