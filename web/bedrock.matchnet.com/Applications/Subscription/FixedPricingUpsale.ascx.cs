﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem;

namespace Matchnet.Web.Applications.Subscription
{
    public partial class FixedPricingUpsale : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", g.Brand.Site.Community.CommunityID,
               g.Brand.Site.SiteID, g.Brand.BrandID))))
            {
                try
                {
                    PaymentUIConnector connector = new PaymentUIConnector();

                    FixedPricingUpsalePage page = new FixedPricingUpsalePage();

                    ((IPaymentUIJSON)page).PaymentType = PaymentType.CreditCard;
                    ((IPaymentUIJSON)page).SaveLegacyData = true;

                    connector.RedirectToPaymentUI(page);

                    return;
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);

                    return;
                }
            }

            return;
        }
    }
}