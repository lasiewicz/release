﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DontGo.ascx.cs" Inherits="Matchnet.Web.Applications.Subscription.DontGo" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div id="sub-dont-go" class="page-container editorial">
    
    <h1><mn:txt runat="server" id="Txt7" resourceconstant="TXT_H1" expandimagetokens="true" /></h1>

	<h2><mn:txt runat="server" id="Txt1" resourceconstant="TXT_H2_WHEN_YOU_SUBSCRIBE" expandimagetokens="true" /></h2>

    <mn:txt runat="server" id="Txt2" resourceconstant="HTML_REASONS_LIST" expandimagetokens="true" />

	<h2><mn:txt runat="server" id="Txt3" resourceconstant="TXT_H2_ARE_YOU_A_SMART" expandimagetokens="true" /></h2>
	<div class="text-center padding-light dontgo-subscribe">
    <asp:HyperLink ID="lnkSubscribe" runat="server" CssClass="btn link-primary xlarge">
		<mn:txt runat="server" id="Txt4" resourceconstant="TXT_SUBSCRIBE_BUTTON_LINK" expandimagetokens="true" /></asp:HyperLink>
	</div>
	<div class="text-center padding-light"><asp:HyperLink ID="lnkCancel" runat="server"><mn:txt runat="server" id="Txt5" resourceconstant="TXT_NO_THANKS" expandimagetokens="true" /></asp:HyperLink></div>
	
	<div class="get-details"><a href="http://www.jdate.com/blog/2011-09/jdate-infographic/" rel="external"><mn:txt runat="server" id="Txt6" resourceconstant="TXT_GET_DETAILS" expandimagetokens="true" /></a></div>

</div>

<script>
(function(){
    var subscriptionLink = document.getElementById('<%=lnkSubscribe.ClientID.ToString() %>');

    subscriptionLink.onclick = function(){
        SubscribeButttonSendOmnitureEvent();
        window.setTimeout(function(){
            document.location.href = subscriptionLink.href;
        }, 500);
        return false;
    }

    function SubscribeButttonSendOmnitureEvent() {
        var s = s_gi(s_account);
        PopulateS(true);
        s.linkTrackEvents = 'event64';
        s.events = 'event64';
        s.t(true); 
    }
}());
</script>