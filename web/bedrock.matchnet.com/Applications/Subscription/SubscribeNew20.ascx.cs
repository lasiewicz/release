﻿/*
 * 
 * WARNING: This codebehind is also used for FreeTrialSubscribe.ascx so any changes made 
 * here need to be tested on that page as well.
 * 
 */

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Lib;
using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Applications.Subscription.Controls;
using Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Enumerations;
using Matchnet.Web.Framework.Util;
using System.Text;
using Spark.Common;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Subscription
{

    /// <summary>.
    ///		Summary description for SubscriptionMain.
    /// </summary>
    public partial class SubscribeNew20 : FrameworkControl
    {

       #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
          
        }

        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        
        #endregion


        private void Page_Load(object sender, System.EventArgs e)
        {
            new IPBlockerClient().CheckAccessAndRedirectIfBlocked(this.ClientIP, IPBlockerAccessType.Subscription, _g);
            
            // stealth logon admins shouldn't be here
            if (g.StealthLogonAdmin != null)
                g.Transfer("/Applications/Home/Default.aspx");

            // in-app purchased subscribers shouldn't purchase/upgrade through us
            if (SubscriberManager.Instance.IsIAPSubscriber(g.Member, g.Brand))
                g.Transfer(SubscriberManager.Instance.GetJumpPageRedirect(g.Brand));

            System.Diagnostics.Trace.WriteLine("Start SubscribeNew20.ascx Page_Load()");
            if (g.Member != null && SubscriptionDontGoThrottler.IsFeatureEnabled(_g.Member, g))
            {
                new CookieHelper().AddDontGoCookie();
            }

            try
            {
                System.Diagnostics.Trace.WriteLine("SubscribeNew20.ascx UPS Integration");
                string prtid = Context.Request["prtid"] != null ? Convert.ToString(Context.Request["prtid"]) : "0";
                g.Session.Add(WebConstants.SESSION_PROPERTY_LAST_SUB_PRTID, prtid, SessionPropertyLifetime.Temporary);

                int memberSubscriptionStatus = FrameworkGlobals.GetMemberSubcriptionStatus(g.Member.MemberID, g.Brand);
                SubscriptionStatus enumSubscriptionStatus = (SubscriptionStatus) memberSubscriptionStatus;

                if (enumSubscriptionStatus == SubscriptionStatus.PausedSubscriber)
                {
                    // Do not allow the member to get to the subscription page if he has a frozen subscription 
                    g.Notification.AddError("SUBSCRIPTION_IS_PAUSED_NOTICE");
                    //g.Notification.AddError("Your subscription is paused. You must first resume your subscription in order to purchase another subscription.");

                    throw new Exception();
                }

                System.Diagnostics.Trace.WriteLine("SubscribeNew20.ascx create PaymentUIConnector");
                PaymentUIConnector connector = new PaymentUIConnector();

               
                if ((Request["PaymentTypeID"] != null) && (Request["PaymentTypeID"] == "2"))
                {
                    System.Diagnostics.Trace.WriteLine("SubscribeNew20.ascx create CheckPage");
                    CheckPage page = new CheckPage(Context, PaymentType.Check);
                    page.Page = this;
                    g.Session.Add(WebConstants.SESSION_PROPERTY_LAST_SUB_PROMOID, page.PromoID.ToString(),
                                  SessionPropertyLifetime.Temporary);
                    connector.RedirectToPaymentUI(page);
                }
                else
                {
                    System.Diagnostics.Trace.WriteLine("SubscribeNew20.ascx create CreditCardPage");
                    CreditCardPage page = new CreditCardPage(Context, PaymentType.CreditCard);
                    
                    g.Session.Add(WebConstants.SESSION_PROPERTY_LAST_SUB_PROMOID, page.PromoID.ToString(),
                                  SessionPropertyLifetime.Temporary);
                    connector.RedirectToPaymentUI(page);
                }

                return;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return;
            }
        }
    }

}

