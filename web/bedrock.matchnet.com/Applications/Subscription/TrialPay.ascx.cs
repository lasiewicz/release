namespace Matchnet.Web.Applications.Subscription
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using Matchnet.Web.Framework;

	/// <summary>
	///		Summary description for TrialPay.
	/// </summary>
	public class TrialPay : FrameworkControl
	{
		protected System.Web.UI.WebControls.Button Button1;

		protected string GetTrialPayLink()
		{
			string strResult=string.Empty;
			string strLink=string.Empty;
			strLink=_g.GetResource("TRIALPAY_LINK");
			if (!strLink.Equals(string.Empty))
			{
				strResult=string.Format(strLink,_g.Member.MemberID.ToString(),_g.Brand.Site.SiteID.ToString());
			}
			return strResult;
		}
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			g.LeftNavControl.HideUserNameLogout();	
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
