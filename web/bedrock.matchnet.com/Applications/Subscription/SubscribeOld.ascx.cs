/*
 * 
 * WARNING: This codebehind is also used for FreeTrialSubscribe.ascx so any changes made 
 * here need to be tested on that page as well.
 * 
 */

using System;
using System.Collections.Specialized;
using System.Data;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Lib;
using Matchnet.Lib.Util;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Globalization;
using Matchnet.SharedLib.Utils;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.Content.ServiceAdapters.Links;

namespace Matchnet.Web.Applications.Subscription
{
    /// <summary>
    ///		Summary description for SubscriptionMain.
    /// </summary>
    public class SubscribeOld : SubscribeCommon
    {
        #region FormElements

        // Buttons
        protected Matchnet.Web.Framework.Ui.FormElements.FrameworkButton ProcessPurchase;
        protected System.Web.UI.WebControls.PlaceHolder CVCPanel;
        protected System.Web.UI.WebControls.PlaceHolder IsraeliIDPanel;
        protected System.Web.UI.HtmlControls.HtmlInputButton NoThanks;

        // Validators
        protected Matchnet.Web.Framework.MultiValidator FirstNameValidator;
        protected Matchnet.Web.Framework.MultiValidator LastNameValidator;
        protected Matchnet.Web.Framework.MultiValidator PhoneValidator;
        protected Matchnet.Web.Framework.MultiValidator AddressLine1Validator;
        protected Matchnet.Web.Framework.MultiValidator CityValidator;
        protected Matchnet.Web.Framework.MultiValidator StateValidator;
        protected Matchnet.Web.Framework.MultiValidator PostalCodeValidator;
        protected Matchnet.Web.Framework.RequiredSelectListValidator CreditCardTypeRequiredValidator;
        protected Matchnet.Web.Framework.MultiValidator CreditCardValidator;
        protected Matchnet.Web.Framework.RequiredFieldValidator MonthRequiredFieldValidator;
        protected Matchnet.Web.Framework.RequiredFieldValidator YearRequiredFieldValidator;
        protected Matchnet.Web.Framework.MultiValidator CVCValidator;
        protected Matchnet.Web.Framework.MultiValidator IsraeliIDValidator;
        protected System.Web.UI.WebControls.CustomValidator CreditCardTypeAndNumberValidator;
        protected Matchnet.Web.Framework.MultiValidator TermsAndConditionsValidator;

        protected Matchnet.Web.Framework.Popup SubscribePopup;
        protected Matchnet.Web.Applications.Subscription.Controls.SinglePlanList SinglePlanList1;
        protected PlaceHolder phMiniProfile;
        protected HtmlTableCell tdPaymentAlternativesLeft;
        protected HtmlTableCell tdPaymentAlternativesRight;
        protected HtmlTableCell tdSubBenefits;

        #endregion

        #region Static Content
        protected System.Web.UI.WebControls.PlaceHolder CreditCardImages;
        protected System.Web.UI.HtmlControls.HtmlInputHidden PageName;
        protected Matchnet.Web.Framework.SingleFormSubmitValidator Singleformsubmitvalidator1;
        protected System.Web.UI.WebControls.PlaceHolder HeaderType1Panel;
        protected System.Web.UI.WebControls.PlaceHolder HeaderType2Panel;
        protected System.Web.UI.WebControls.PlaceHolder CCPurchasePanel;
        protected System.Web.UI.WebControls.PlaceHolder AltSignUpType1Panel;
        protected System.Web.UI.WebControls.PlaceHolder AltSignUpType2Panel;
        protected HyperLink AltSignUpLink;
        protected System.Web.UI.HtmlControls.HtmlTable Table3;
        protected Matchnet.Web.Applications.Subscription.Controls.SubscriptionConfirmation SubscriptionConfirmation1;
        protected Matchnet.Web.Framework.Image Img1;
        protected Matchnet.Web.Framework.Image Img2;
        protected Matchnet.Web.Framework.Image HeaderType2Img;
        protected Matchnet.Web.Framework.Txt Txt2;
        protected Matchnet.Web.Framework.Txt Txt4;
        protected Matchnet.Web.Framework.SingleFormSubmitValidator SingleFormSubmitValidator2;
        protected Matchnet.Web.Framework.Txt Txt3;
        protected Matchnet.Web.Framework.Image Img5;
        protected Matchnet.Web.Framework.Txt Txt5;
        protected Matchnet.Web.Framework.Txt Txt6;
        protected Matchnet.Web.Framework.Txt Txt25;
        protected Matchnet.Web.Framework.Txt Txt9;
        protected Matchnet.Web.Framework.Txt Txt10;
        protected Matchnet.Web.Framework.Txt Txt11;
        protected Matchnet.Web.Framework.Txt Txt12;
        protected Matchnet.Web.Framework.Txt Txt13;
        protected Matchnet.Web.Framework.Txt Txt14;
        protected Matchnet.Web.Framework.Txt Txt15;
        protected Matchnet.Web.Framework.Txt Txt16;
        protected Matchnet.Web.Framework.Txt Txt17;
        protected Matchnet.Web.Framework.Txt Txt18;
        protected Matchnet.Web.Framework.Image Img4;
        protected Matchnet.Web.Framework.Txt Txt1;
        protected Matchnet.Web.Framework.Txt Txt19;
        protected Matchnet.Web.Framework.Txt Txt23;
        protected Matchnet.Web.Framework.Txt Txt24;
        protected Matchnet.Web.Framework.Ui.FormElements.FrameworkButton Cancel;
        protected System.Web.UI.HtmlControls.HtmlInputHidden mpid;
        protected Matchnet.Web.Framework.Txt Txt7;
        protected Matchnet.Web.Framework.Txt Txt8;
        protected Matchnet.Web.Framework.Txt Txt26;
        protected Matchnet.Web.Framework.Txt Txt35;
        protected Matchnet.Web.Framework.Txt Txt36;
        protected Matchnet.Web.Framework.Image Image2;
        protected Matchnet.Web.Framework.Image Image1;
        protected Matchnet.Web.Framework.Txt Txt21;
        protected Matchnet.Web.Framework.Txt txtTermsAndConditions;
        protected System.Web.UI.WebControls.CheckBox chkTermsAndConditions;
        protected Matchnet.Web.Framework.Txt txtAccount;
        protected Matchnet.Web.Framework.Txt txtTopBenefits;
        protected Matchnet.Web.Framework.Ui.FormElements.FrameworkLabel lblCWFError;
        protected Matchnet.Web.Framework.Ui.FormElements.FrameworkLabel lblUsedTranType;

        protected System.Web.UI.HtmlControls.HtmlInputHidden Hidden1;
        protected System.Web.UI.HtmlControls.HtmlTableCell CreditCardImage1;
        protected System.Web.UI.HtmlControls.HtmlTableCell CreditCardImage2;
        protected System.Web.UI.HtmlControls.HtmlTableCell CreditCardImage3;
        protected System.Web.UI.HtmlControls.HtmlTableCell CreditCardImage4;
        protected System.Web.UI.HtmlControls.HtmlTableCell CreditCardImage5;
        protected System.Web.UI.HtmlControls.HtmlTableCell CreditCardImage6;
        protected System.Web.UI.HtmlControls.HtmlTableCell CreditCardImage7;
        protected System.Web.UI.HtmlControls.HtmlTableCell CreditCardImage8;
        protected System.Web.UI.HtmlControls.HtmlTable PaymentVerification;


        private string _pageName;
        protected bool adminDisplayMode = false; //this is used to determine whether we are displaying in admin mode, affects css and displays
        #endregion

        #region Control Populators

        private Content.ValueObjects.Region.RegionCollection GetCountries()
        {
            // Lib.Region.RegionLanguage regions = new Lib.Region.RegionLanguage();
            // DataTable dt = regions.GetCountries(g.Brand.Site.LanguageID);
            // return dt;
            Content.ValueObjects.Region.RegionCollection retVal = RegionSA.Instance.RetrieveCountries(g.Brand.Site.LanguageID);

            return (retVal);
        }

        private DataTable GetCreditCardExpirationYears()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("Content");
            dt.Columns.Add("Value");

            int startYear = DateTime.Now.Year;

            dt.Rows.Add(new object[] { _g.GetResource("YEAR", this), "" });

            for (int year = startYear; year < startYear + 9; year++)
            {
                dt.Rows.Add(new object[] { year, year });
            }

            return dt;
        }

        private DataTable GetCreditCardExpirationMonths()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("Content");
            dt.Columns.Add("Value");

            dt.Rows.Add(new object[] { _g.GetResource("MONTHABBR", this), "" });

            for (int month = 1; month < 13; month++)
            {
                dt.Rows.Add(new object[] { month.ToString("0#"), month });
            }

            return dt;
        }

        public string GetPayByCheckURL()
        {
            return SubscriptionHelper.GetPayByCheckURL(g);
        }

        public string GetWhatIsCVCURL()
        {
            return SubscriptionHelper.GetWhatIsCVCURL(g);
        }

        /// <summary>
        /// 18014 Returns the height of the What is the CVC Card Verification Code pop-up window. Hebrew
        /// and UK sites have less content in window (there is no AMEX card explanation), so make 
        /// the window shorter.
        /// </summary>
        /// <returns></returns>
        public string GetWhatIsCVCWindowHeight()
        {
            if ((g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
                || (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.MatchnetUK))
            {
                return "300";
            }
            else
            {
                return "500";
            }
        }

        public string GetPayByELVURL()
        {
            try
            {
                return FrameworkGlobals.LinkHrefSSL(
                    "/Applications/Subscription/SubscribeByDirectDebit.aspx?OverrideTransactionHistory=1&"
                    + g.AppendDestinationURL());
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                return "";
            }
        }

        public int GetFreeTrialPlanID()
        {
            string ftg = (Request["ftg"] != null) ? Request["ftg"] : string.Empty;

            SubscribePopup.Enabled = false;

            // Check for 5dft session variable and remove it if present (we are already on 5dft if this method is called)
            if (FrameworkGlobals.IsValid5DFTMember(g))
            {
                g.Session.Remove(WebConstants.SESSION_PROPERTY_NAME_VIEWED_SUBSCRIPTION);
            }

            return PlanSA.Instance.GetFreeTrialGroupPlan(FreeTrial.GetGroupFromQuerystring(ftg), g.Brand.BrandID).PlanID;
        }

        private CreditCardCollection GetCreditCardTypes()
        {
            CreditCardCollection ccc = PurchaseSA.Instance.GetCreditCardTypes(g.TargetBrand.Site.SiteID);

            if (ccc != null)
            {
                foreach (CreditCard cc in ccc)
                {
                    cc.Content = g.GetResource(cc.ResourceConstant, null);
                }
            }
            return ccc;
        }

        #endregion

        #region Event Handlers
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                // TT#16346 - change the size of the state box per specific sites
                SetStateTextBoxSize();

                if (tdSubBenefits != null)
                {
                    if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
                    {
                        tdSubBenefits.Visible = true;
                    }
                    else
                    {
                        tdSubBenefits.Visible = false;
                    }
                }

                // per TT#20551 hiding the top nav
                //if(g.Member == null || !MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                //{
                g.HeaderControl.ShowBlankHeader(Matchnet.Web.Framework.Ui.HeaderImageType.Default);
                g.SetTopAuxMenuVisibility(false, true);
                g.LeftNavControl.HideUserNameLogout();
                //}

                if (phMiniProfile != null)
                {
                    this.phMiniProfile.Visible = false;
                }

                int memberID = g.RecipientMemberID;

                if (memberID > 0)
                {
                    if (phMiniProfile != null)
                        this.phMiniProfile.Visible = true;

                    if (txtTopBenefits != null)
                        this.txtTopBenefits.Visible = false;

                    Matchnet.Web.Framework.Ui.BasicElements.MiniProfile control = (Matchnet.Web.Framework.Ui.BasicElements.MiniProfile)LoadControl("../../Framework/Ui/BasicElements/MiniProfile.ascx");
                    control.Member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);

                    control.Transparency = true;
                    if (phMiniProfile != null)
                        this.phMiniProfile.Controls.Add(control);
                    //this.SubscriptionBenefits1.Visible = false;
                }

                _pageName = "Subscribe";
                if (PageName != null)
                    _pageName = PageName.Value;

                SetupCreditCardType();

                Country.DataSource = GetCountries();
                Country.DataTextField = "Description";
                Country.DataValueField = "RegionID";
                Country.SelectedValue = g.Brand.Site.DefaultRegionID.ToString();

                if (CVCPanel != null)
                {
                    if (PurchaseSA.Instance.RenderCVC(g.TargetBrand.Site.SiteID, (CurrencyType)Enum.Parse(typeof(CurrencyType), g.TargetBrand.Site.CurrencyID.ToString()), PaymentType.CreditCard))
                    {
                        CVCPanel.Visible = true;
                    }
                }

                if (IsIsraeliBrand(g.Brand.BrandID))
                {
                    StateValidator.IsRequired = false;
                    PostalCodeValidator.IsRequired = false;
                    if (IsraeliIDPanel != null)
                    {
                        IsraeliIDPanel.Visible = true;
                    }
                }

                // There are different formats on the same code-in-front.  The are private label specific and isolated by
                // panels.  Turn the appropriate ones on/off.  This was done in this manner as opposed to creating another code-in-front
                // because 1) some of the code-behind-code (this page) make references to code-in-front objects that would not be used on Type2
                // and 2) in order to keep all logic on a single page as opposed to multiple calling page(s).
                // Some of the code-in-front pages may not instantiate these objects.  If this fails, ignore.
                try
                {
                    switch (g.Brand.Site.Community.CommunityID)
                    {
                        case (int)WebConstants.COMMUNITY_ID.Cupid:	// cupid.co.il.
                            HeaderType1Panel.Visible = false;
                            HeaderType2Panel.Visible = true;
                            CCPurchasePanel.Visible = true;
                            AltSignUpType1Panel.Visible = false;
                            AltSignUpType2Panel.Visible = true;
                            AltSignUpLink.NavigateUrl = _g.GetResource("ALT_SIGN_UP_LINK_URL", this);
                            break;
                        default:	// all other private labels.
                            HeaderType1Panel.Visible = true;
                            HeaderType2Panel.Visible = false;
                            CCPurchasePanel.Visible = false;
                            AltSignUpType1Panel.Visible = true;
                            AltSignUpType2Panel.Visible = false;
                            break;
                    }
                }
                catch
                {
                    // If the properties could not be set that probably means the controls do not exist in the code-in-front.  Ignore these errors.
                }

                //For Cupid hide the Telecheck stuff
                if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Cupid)
                {
                    if (tdPaymentAlternativesLeft != null)
                    {
                        tdPaymentAlternativesLeft.Visible = false;
                    }
                    if (tdPaymentAlternativesRight != null)
                    {
                        tdPaymentAlternativesRight.ColSpan = 4;
                    }
                }

                CreditCardExpirationYear.DataSource = GetCreditCardExpirationYears();
                CreditCardExpirationYear.DataTextField = "Content";
                CreditCardExpirationYear.DataValueField = "Value";

                CreditCardExpirationMonth.DataSource = GetCreditCardExpirationMonths();
                CreditCardExpirationMonth.DataTextField = "Content";
                CreditCardExpirationMonth.DataValueField = "Value";

                FreeTrialGroupType ftg = FreeTrial.GetGroupFromQuerystring(Request["ftg"]);
                if (ftg != FreeTrialGroupType.None)
                {

                    g.SetBodyTagAttribute("onload",
                        new string[] { "javascript:window.resizeTo(700,850);", 
										  "javascript:window.resizeTo(700,850);" },
                        new ContextGlobal.Pages[] { ContextGlobal.Pages.PAGE_INSTANT_MESSENGER_MAIN,
													  ContextGlobal.Pages.PAGE_EMAIL_COMPOSE});

                }

                if (NoThanks != null)
                {
                    NoThanks.Visible = false;
                    if (ftg == FreeTrialGroupType.Group4)
                    {
                        NoThanks.Visible = true;
                        string url = "javascript:document.location='" + (g.Brand.Site.DefaultHost + g.Brand.Uri) + "/default.asp?p=7100'";
                        NoThanks.Attributes.Add("onclick", url);
                        NoThanks.Value = g.GetResource("NO_THANKS", this);
                        NoThanks.CausesValidation = false;
                        ProcessPurchase.Text = g.GetResource("PLEASE_PROCESS", this);
                    }
                }

                if (PlanListControl1 != null)
                {
                    SinglePlanList1.Visible = true;
                    PlanListControl1.Visible = false;
                    PlanListControl1.Enabled = false;

                    if (!SinglePlanList1.Enabled)
                    {
                        SinglePlanList1.Visible = false;
                        PlanListControl1.Visible = true;
                        PlanListControl1.Enabled = true;
                    }
                }

                // TT 15156:  See Comment dated 10/7/2005.
                //				// If there is an ExitPopup then use it.  If not, then create a popup.
                //				// Note that even though an ExitPopup may exist, by default it is only added as a control
                //				// to the page if the page is of type NORMAL (see RenderNormalPage in default.aspx).  Thus,
                //				// any other page type requires that the ExitPopup (whose values will be overwritten by the values
                //				// set on this page) be explicitly added.
                //				if (_g.Promotion.ExitPopup != null)
                //				{
                //					if (_g.LayoutTemplate != LayoutTemplate.Standard)
                //					{
                //						_g.Page.Controls.Add(_g.Promotion.ExitPopup);
                //					}
                //
                //					SubscribePopup = _g.Promotion.ExitPopup;
                //				}
                //				else
                //				{
                //					SubscribePopup = new Popup();
                //
                //					_g.Page.Controls.Add(SubscribePopup);
                //				}

                SubscribePopup = new Popup();
                _g.Page.Controls.Add(SubscribePopup);

                SubscribePopup.Width = FreeTrialCtrl.FREETRIALPOPUP_WIDTH;
                SubscribePopup.Height = FreeTrialCtrl.FREETRIALPOPUP_HEIGHT;

                SetFTPopupStatus(SubscribePopup);

                DataBind();

                // If the user ever visits Subscribe, add this session var that indicates they were here
                // This is used to ensure that users will hit 5dft on a later page hit if they escape the javascript somehow.
                if (FrameworkGlobals.IsValid5DFTMember(g)
                    && g.AppPage.ControlName != "FreeTrialSubscribe"
                    && g.AppPage.ControlName != "FreeTrial")
                {
                    g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_VIEWED_SUBSCRIPTION, "true", SessionPropertyLifetime.Temporary);
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (!(Request.ServerVariables["SERVER_PORT"] == "443"))
                {
                    string path = Request.Url.AbsoluteUri;
                    if (!path.ToLower().StartsWith("https"))
                    {
                        path = path.Remove(0, 4);
                        path = "https" + path;
                        Response.Redirect(path);
                    }
                }


                if (!IsPostBack)
                {
                    CupidWomenFree(lblCWFError, lblUsedTranType, false);
                }

                //require confirmation of Terms & Conditions on all sites EXCEPT cupid and jdate.co.il
                if (g.Brand.BrandID != (int)WebConstants.BRAND_ID.Cupid)
                {
                    //18404 set error message Require terms and conditions
                    TermsAndConditionsValidator.IsRequiredErrorMessage = g.GetResource("ERROR_MUST_AGREE_TO_TERMS_AND_CONDITIONS");
                }
                else
                {
                    this.chkTermsAndConditions.Visible = false;
                    this.TermsAndConditionsValidator.Visible = false;
                }

                // this logic has to be in Page_Load, otherwise Footer has not processed in Init
                if (g.Member == null || !MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                {
                    g.FooterControl.HideFooterLinks();
                }

                // Always show transaction history based on the subscription status of a member
                if (MemberPrivilegeAttr.IsCureentSubscribedMember(g) &&
                    (Request["OverrideTransactionHistory"] == null || Request["OverrideTransactionHistory"] != "1"))
                {
                    ShowTransactionHistory();
                }

                // Payment alternative link 
                if (PaymentAlternativeTable != null) // this is because all subclasses don't have this table
                {
                    if (g.ImpersonateContext == false)
                    {
                        PaymentAlternativeTable.Visible = true;
                    }
                    else
                    {
                        PaymentAlternativeTable.Visible = false;
                    }
                }

                // decide if the pay by check option is available
                if (CheckTable != null) // this is because all subclasses don't have this table
                {
                    if ((g.ImpersonateContext == false) &&
                        ((g.Brand.Site.PaymentTypeMask & ConstantsTemp.PAYMENT_TYPE_CHECK)
                        == ConstantsTemp.PAYMENT_TYPE_CHECK))
                    {
                        CheckTable.Visible = true;
                    }
                    else
                    {
                        CheckTable.Visible = false;
                    }
                }

                // ELV table
                if (ELVTable != null) // this is because all subclasses don't have this table
                {
                    if ((g.ImpersonateContext == false) &&
                        ((g.Brand.Site.PaymentTypeMask & ConstantsTemp.PAYMENT_TYPE_DIRECTDEBIT)
                        == ConstantsTemp.PAYMENT_TYPE_DIRECTDEBIT))
                    {
                        ELVTable.Visible = true;
                    }
                    else
                    {
                        ELVTable.Visible = false;
                    }
                }

                // in admin chargeback view mode?
                if (g.Member != null && (g.HasPrivilege(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), (int)PrivilegeType.PRIVILEGE_VIEW_CREDITCARD)
                    && (Request["chargeview"] == "1")
                    ))
                {
                    if (Request["Tran"] != null && Request["Tran"].Length > 0)
                    {
                        int memberTranID = Conversion.CInt(Request["Tran"].ToString());
                        NameValueCollection nvc = PurchaseSA.Instance.GetMemberPaymentByMemberTranID(g.TargetMemberID,
                            memberTranID);

                        if (nvc != null && nvc.Count > 0)
                        {
                            System.Web.UI.Control curControl;
                            TextBox tb;
                            DropDownList ddl;

                            foreach (string key in nvc.Keys)
                            {
                                curControl = FindControl(key);
                                if (curControl is TextBox)
                                {
                                    tb = (TextBox)curControl;
                                    tb.Text = nvc[key];
                                }
                                else if (curControl is DropDownList)
                                {
                                    ddl = (DropDownList)curControl;
                                    try
                                    {
                                        ddl.SelectedValue = nvc[key];
                                    }
                                    catch
                                    {
                                        // nada
                                    }
                                }
                            }

                        }
                        this.ProcessPurchase.Enabled = false;
                    }
                }

                // Put user code to initialize the page here
                if (!IsPostBack)
                {
                    if (ShowError())
                        return;

                    if (g.IsDevMode)
                    {
                        FirstName.Text = "BA";
                        LastName.Text = "Barracus";
                        //AddressLine1.Text = "1831 Murdock Drive";
                        AddressLine1.Text = "0";
                        City.Text = "Hollywood";
                        State.Text = "CA";
                        PostalCode.Text = "90069";
                        if (IsraeliID != null)
                            IsraeliID.Text = "123";
                        Phone.Text = "(323) 836-3000";
                        CreditCardNumber.Text = "4000300020001000";
                        CreditCardType.SelectedValue = "64";
                        CreditCardExpirationMonth.SelectedValue = "1";
                        CreditCardExpirationYear.SelectedValue = System.DateTime.Now.AddYears(1).Year.ToString();
                        if (CVC != null)
                            CVC.Text = "123";
                    }

                }

                if (Request["CheckStatus"] == "1")
                {
                    int memberTranID = GetDecryptedValue(Request["mtid"]);
                    int memberPaymentID = GetDecryptedValue(Request["mpid"]);
                    bool repopulatePaymentFlag = GetDecryptedValue(Request["rmpid"]) == 1 ? true : false;

                    if (memberTranID == 0)
                    {
                        if ((Request["MD"] != null && Request["MD"].Length > 0) && (Request["PaRes"] != null && Request["PaRes"].Length > 0))
                        {
                            memberTranID = Convert.ToInt32(Request["MD"]);
                            repopulatePaymentFlag = true;
                        }

                        if (memberTranID == 0)
                        {
                            RedirectFailure(_pageName, "MNCHARGE___GENERAL_DATABASE_FAILURE_13076", memberPaymentID, memberTranID, repopulatePaymentFlag);
                        }
                    }

                    int attempt = 0;
                    if (Request["Attempt"] != null)
                        attempt = SharedLib.Utils.Conversion.CInt(Request["Attempt"]);

                    // Now let's check the status and choose the outcome...
                    SubscriptionResult sr;
                    sr = PurchaseSA.Instance.GetMemberTranStatus(_g.TargetMemberID, memberTranID);

                    //ensure SubscriptionExpirationDate attribute has been updated
                    if (sr.MemberTranStatus == MemberTranStatus.Success)
                    {
                        if (_g.Member.GetAttributeDate(_g.Brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE) < DateTime.Now)
                        {
                            sr.MemberTranStatus = MemberTranStatus.Pending;
                        }
                    }

                    LastSelectedPlanID = sr.PlanID;

                    // Store necessary values in session cookie for use in pixels.
                    _g.Session.Add("SubAmt", sr.Amount.ToString(), SessionPropertyLifetime.Persistent);
                    _g.Session.Add("SubDuration", sr.Duration.ToString(), SessionPropertyLifetime.Persistent);

                    // Do not show popup.
                    SubscribePopup.Enabled = false;

                    try
                    {
                        switch (sr.MemberTranStatus)
                        {
                            case MemberTranStatus.Success:
                                SubscriptionConfirmation1.Enabled = true;
                                SubscriptionConfirmation1.PlanID = sr.PlanID;
                                ShowSuccess();
                                break;
                            case MemberTranStatus.Failure:
                                RedirectFailure(_pageName, sr.ResourceConstant, sr.MemberPaymentID, memberTranID, repopulatePaymentFlag);
                                break;
                            case MemberTranStatus.Pending:
                            case MemberTranStatus.VerifiedByVisaPending:
                            case MemberTranStatus.AuthenticationPending:
                                ShowInProgress(memberTranID, _pageName, attempt, sr.MemberPaymentID, repopulatePaymentFlag);
                                break;
                        }
                    }
                    catch (System.Threading.ThreadAbortException)
                    {
                    }
                }

                /*
                 * this is a total hack to determine if someone is coming to subscription
                 * by responding to an email
                 * */
                string queryString = Request.Params.Get("QUERY_STRING");
                if (0 > queryString.IndexOf("ReplyMailID"))
                {
                    this.PlanListControl1.removePlan(408);
                }

            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
            /* Had to roll this change back RB- 20070420
            // TT#02471 by RB 20070413
            g.FooterControl.HideFooterLinks();
            */
        }

        private void SetStateTextBoxSize()
        {
            if (g.Brand.BrandID == (int)WebConstants.BRAND_ID.MatchnetUK ||
                g.Brand.BrandID == (int)WebConstants.BRAND_ID.DateCA)
            {
                State.Columns = 20;
            }
        }

        private void SetupCreditCardType()
        {
            CreditCardCollection ccc = GetCreditCardTypes();

            // add a blank choice first
            CreditCardType.Items.Add(new ListItem("", ""));

            foreach (CreditCard cc in ccc)
            {
                CreditCardType.Items.Add(new ListItem(cc.Content, cc.CreditCardID.ToString()));
            }
        }

        private bool ValidateIsraCard()
        {
            const string YOTER_CLUB_ISRACARD = "72";
            const string ISRACARD_CAMPUS_CARD_STUDENT = "74";
            const string CREDIT_CARD_ISRACARD = "128";

            switch (CreditCardType.SelectedValue)
            {
                case YOTER_CLUB_ISRACARD:
                    return (new Regex("\\d*").IsMatch(CreditCardNumber.Text));
                //break;
                case ISRACARD_CAMPUS_CARD_STUDENT:
                    return (new Regex("\\d*").IsMatch(CreditCardNumber.Text));
                //break;
                case CREDIT_CARD_ISRACARD:
                    return (new Regex("\\d*").IsMatch(CreditCardNumber.Text));
                //break;				
            }

            return false;
        }

        private void ProcessPurchase_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (CreditCardValidator.IsValid == false)
                {
                    CreditCardValidator.IsValid = ValidateIsraCard();
                }

                if (!_g.Page.IsValid)
                {
                    return;
                }

                int selectedPlan = Conversion.CInt(Request["SelectedPlan"]);
                int adminMemberID = Constants.NULL_INT;
                int conversionMemberID = Constants.NULL_INT;
                int sourceID = Conversion.CInt(Request["srid"]) != Constants.NULL_INT ? Conversion.CInt(Request["srid"]) : Constants.NULL_INT;
                int purchaseReasonTypeID = Conversion.CInt(Request["prtid"]) != Constants.NULL_INT ? Conversion.CInt(Request["prtid"]) : Constants.NULL_INT;

                if (g.IsAdmin && g.Member != null)
                {
                    adminMemberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
                }

                if (g.Session["ClickMemberID"] != null)
                {
                    conversionMemberID = Conversion.CInt(g.Session["ClickMemberID"]);
                }

                int countryRegionID;
                string israeliID;
                string cvc = Matchnet.Lib.Util.Util.CString(CVC.Text);

                if (IsIsraeliBrand(g.Brand.BrandID))
                {
                    countryRegionID = 105; //default to Israel
                    israeliID = Matchnet.Lib.Util.Util.CString(IsraeliID.Text);
                }
                else
                {
                    countryRegionID = Conversion.CInt(Country.SelectedValue);
                    israeliID = null;
                }

                Int32 promoID = Constants.NULL_INT;
                if (PlanListControl1 != null)
                {
                    promoID = PlanListControl1.PromoID;
                }

                SubscriptionResult subRes = PurchaseSA.Instance.BeginCreditCardPurchase(g.TargetMemberID,
                    adminMemberID,
                    selectedPlan,
                    Constants.NULL_INT,
                    g.TargetBrand.Site.SiteID,
                    g.TargetBrand.BrandID,
                    FirstName.Text,
                    LastName.Text,
                    israeliID,
                    Phone.Text,
                    AddressLine1.Text,
                    City.Text,
                    State.Text,
                    countryRegionID,
                    PostalCode.Text,
                    (CreditCardType)Enum.Parse(typeof(CreditCardType), CreditCardType.SelectedValue.ToString()),
                    CreditCardNumber.Text,
                    Convert.ToInt32(CreditCardExpirationMonth.SelectedValue),
                    Convert.ToInt32(CreditCardExpirationYear.SelectedValue),
                    cvc,
                    conversionMemberID,
                    sourceID,
                    purchaseReasonTypeID,
                    g.ClientIP, promoID);

                // Check for 5dft session variable, remove it if present
                // A purchase attempt has been made so we don't need to send the user to 5dft.
                if (FrameworkGlobals.IsValid5DFTMember(g))
                {
                    g.Session.Remove(WebConstants.SESSION_PROPERTY_NAME_VIEWED_SUBSCRIPTION);
                }

                //shove full cc/cvc into seesion to display if tran is declined (per philip's request)
                g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_CCNO,
                    CreditCardNumber.Text,
                    Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

                // add this per TT 20489 to display israeili id when failed.
                g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_IsraeliID,
                    IsraeliID.Text,
                    Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

                //philip says do not enable this yet
                /*
                g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_CVCNO,
                    cvc,
                    Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                */

                if (subRes.ReturnValue == Constants.RETURN_OK)
                {
                    /*                   
                    Matchnet.Purchase.ValueObjects.Plan subscribedPlan = PlanSA.Instance.GetPlan(selectedPlan, g.Brand.BrandID);
                    if (((subscribedPlan.PlanTypeMask & PlanType.Premium) == PlanType.Premium))
                    {
                        // Member has purchased a premium plan  
                        // Just set the expiration date for the HighlightedExpirationDate attribute
                        // to a year from today
                        // If the plan is good for 3 months, then after 3 months, the member mini profile
                        // will not be highlighted even if the HighlightedExpirationDate attribute is still 
                        // valid for another 9 months.  The member must also be currently subscribed to
                        // have his mini profiles highlighted.  
                        DateTime dteExpirationDate = DateTime.Now.AddYears(1);                                                         
                        
                        switch (subscribedPlan.InitialDurationType)
                        {
                            case DurationType.Minute:
                                dteExpirationDate.AddMinutes(subscribedPlan.InitialDuration);
                                break;
                            case DurationType.Hour:
                                dteExpirationDate.AddHours(subscribedPlan.InitialDuration);
                                break;
                            case DurationType.Day:
                                dteExpirationDate.AddDays(subscribedPlan.InitialDuration);
                                break;
                            case DurationType.Week:
                                dteExpirationDate.AddDays((subscribedPlan.InitialDuration * 7));
                                break;
                            case DurationType.Month:
                                dteExpirationDate.AddMonths(subscribedPlan.InitialDuration);
                                break;
                            case DurationType.Year:
                                dteExpirationDate.AddYears(subscribedPlan.InitialDuration);
                                break;
                            default:
                                break;
                        }

                        g.TargetMember.SetAttributeDate(g.TargetBrand, "HighlightedExpirationDate", dteExpirationDate);

                        MemberSaveResult memberSaveResult = MemberSA.Instance.SaveMember(g.TargetMember);
                    }
                    */

                    ShowInProgress(subRes.MemberTranID, _pageName, 0, subRes.MemberPaymentID);
                }
                else
                {
                    g.Notification.AddError(subRes.ResourceConstant);
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }

        }

        /// <summary>
        /// Compares credit card type to the number entered. Returns false if number entered does not match the format expected by the
        /// chosen credit card type (e.g., MasterCard must start with a 5).
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        private void CreditCardTypeAndNumberValidator_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            bool result = true;

            //only check this if passed requirements for choosing type and entering some number
            if (CreditCardTypeRequiredValidator.IsValid && CreditCardValidator.IsValid)
            {
                switch ((Matchnet.Purchase.ValueObjects.CreditCardType)(Convert.ToInt32(CreditCardType.SelectedValue)))
                {
                    case Matchnet.Purchase.ValueObjects.CreditCardType.AmericanExpress:
                        //AMERICAN_EXPRESS cards must start with 34 or 37
                        result = CreditCardNumber.Text.StartsWith("34") || CreditCardNumber.Text.StartsWith("37");
                        break;
                    case Matchnet.Purchase.ValueObjects.CreditCardType.DinersClub:
                        //DINERS cards must start with 36 or 38 
                        result = CreditCardNumber.Text.StartsWith("36") || CreditCardNumber.Text.StartsWith("38");
                        break;
                    case Matchnet.Purchase.ValueObjects.CreditCardType.Visa:
                    case Matchnet.Purchase.ValueObjects.CreditCardType.Delta:
                    case Matchnet.Purchase.ValueObjects.CreditCardType.Electron:
                        //VISA cards must start with a 4
                        //DELTA cards must start with a 4
                        //ELECTRON cards must start with a 4
                        result = CreditCardNumber.Text.StartsWith("4");
                        break;
                    case Matchnet.Purchase.ValueObjects.CreditCardType.MasterCard:
                        //MASTERCARD cards must start with a 5
                        result = CreditCardNumber.Text.StartsWith("5");
                        break;
                    case Matchnet.Purchase.ValueObjects.CreditCardType.Discover:
                        //DISCOVER cards must start with a 6
                        result = CreditCardNumber.Text.StartsWith("6");
                        break;
                }
                //set site/language specific error message
                this.CreditCardTypeAndNumberValidator.ErrorMessage = g.GetResource("ERROR_CARD_NUMBER_DOESNT_MATCH_CARD_TYPE", this);
                args.IsValid = result;
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            if (this.ProcessPurchase != null)
                this.ProcessPurchase.Click += new System.EventHandler(this.ProcessPurchase_Click);

            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);
            this.CreditCardTypeAndNumberValidator.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.CreditCardTypeAndNumberValidator_ServerValidate);
        }
        #endregion

        private bool IsIsraeliBrand(int brandID)
        {
            if (brandID == (int)WebConstants.BRAND_ID.JDateCoIL ||
                brandID == (int)WebConstants.BRAND_ID.Cupid ||
                brandID == (int)WebConstants.BRAND_ID.Nana)
                return true;
            else
                return false;
        }


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            g.LeftNavControl.Visible = true;

            //admin style
            if (g.IsAdmin && g.Member != null)
            {
                if (Request.QueryString["impmid"] != null)
                {
                    if (Request.QueryString["tran"] == null)
                    {
                        this.adminDisplayMode = true;
                    }
                }
            }


            // Omniture Analytics - Event Tracking
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                // Without this check, this renders even on confirmation, pending, etc :| ...
                if ((SuccessPanel.Visible == false) && (PollingPanel.Visible == false))
                {
                    // Subscription Start
                    _g.AnalyticsOmniture.Events = "event9";

                    // Subscription Plans
                    string plans = Constants.NULL_STRING;

                    foreach (Matchnet.Purchase.ValueObjects.Plan plan in this.PlanListControl1.Plans)
                    {
                        plans += plan.PlanID + ",";
                    }

                    _g.AnalyticsOmniture.Evar3 = plans.TrimEnd(new char[] { ',' });
                    if (this.PlanListControl1.PromoID != Constants.NULL_INT)
                    {
                        _g.AnalyticsOmniture.Evar4 = this.PlanListControl1.PromoID.ToString();
                    }
                }
            }
        }
    }
}

