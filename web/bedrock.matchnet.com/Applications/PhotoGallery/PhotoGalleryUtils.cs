using System;
using Matchnet.Session.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.PhotoSearch.ValueObjects;
namespace Matchnet.Web.Applications.PhotoGallery
{
	/// <summary>
	/// Summary description for PhotoGalleryUtils.
	/// </summary>
	public class PhotoGalleryUtils
	{
		private static int _defaultPersistenceDays = 3;
        
        public PhotoGalleryUtils()
		{
			//
			// TODO: Add constructor logic here
			//
		}

         public static void SaveQueryToSession(PhotoSearch.ValueObjects.PhotoGalleryQuery q, Brand brand, int memberID)
         {
             SaveQueryToSession(q, brand, memberID, _defaultPersistenceDays);
         }

        public static void SaveQueryToSession(PhotoSearch.ValueObjects.PhotoGalleryQuery q, Brand brand, int memberID, int persistenceDays)
        {
            int memID = 0;
            if (memberID > 0)
                memID = memberID;
            PhotoGalleryPersistence pres = new PhotoGalleryPersistence(memID, persistenceDays, brand);
            pres.Gender = (int)q.Gender;
            pres.SiteID = (int)q.SiteID;
            pres.CommunityID = (int)q.CommunityID;
            pres.AgeMin = q.AgeMin;
            pres.AgeMax = q.AgeMax;
            pres.RegionID = q.RegionID;
            pres.RegionType = q.RegionType;
            pres.Radius = q.Radius;
            pres.CountryRegionID = q.CountryRegionID;
            pres.InsertDays = q.InsertDays;
            pres.ReloadFlag = q.ReloadFlag;
            pres.GuestFlag = q.GuestFlag;
            if (brand.GetStatusMaskValue(Matchnet.Content.ValueObjects.BrandConfig.StatusType.PrivatePhotos))
            {
                pres.GuestFlag = false;
            }
            pres.SortBy = (int)q.SortBy;

            pres.PersistValues();
            System.Web.HttpContext.Current.Items["UserSession.COOKIE_UPDATED_KEY"] = "true";
		}

        private static string getDomain(Matchnet.Web.Framework.ContextGlobal g)
        {
            string domainName = String.Empty;

            if ((g.Brand.StatusMask & (int)StatusType.CoBrand) == (int)StatusType.CoBrand)
            {
                domainName = g.Brand.Site.Name;
            }
            else if (g.BrandAlias == null)
            {
                domainName = g.Brand.Uri;
            }
            else
            {
                domainName = g.BrandAlias.Uri;
            }

            return domainName;
        }

        public static PhotoSearch.ValueObjects.PhotoGalleryQuery GetQueryFromSession(Brand brand, int memberID)
        {
            return GetQueryFromSession(brand, memberID, _defaultPersistenceDays);
        }

        public static PhotoSearch.ValueObjects.PhotoGalleryQuery GetQueryFromSession(Brand brand, int memberID, int persistenceDays)
		{
			PhotoSearch.ValueObjects.PhotoGalleryQuery query=new PhotoSearch.ValueObjects.PhotoGalleryQuery();

            int memID = 0;
            if (memberID > 0)
                memID = memberID;
            PhotoGalleryPersistence pres = new PhotoGalleryPersistence(memID, persistenceDays, brand);

			query.SiteID = brand.Site.SiteID;
			query.CommunityID = brand.Site.Community.CommunityID;
            query.Gender = pres.Gender;
			query.AgeMin = pres.AgeMin;
			query.AgeMax = pres.AgeMax;
			query.RegionID = pres.RegionID;
			query.RegionType=  pres.RegionType;//(PhotoSearch.ValueObjects.SearchRegionType)g.Session.GetInt(PhotoSearch.ValueObjects.PhotoGalleryQuery.PHOTOGALL_SEARCH_TYPE);
			query.Radius = pres.Radius;//g.Session.GetInt(PhotoSearch.ValueObjects.PhotoGalleryQuery.PHOTOGALL_RADIUS);
			query.CountryRegionID = pres.CountryRegionID;//g.Session.GetInt(PhotoSearch.ValueObjects.PhotoGalleryQuery.PHOTOGALL_COUNTRY_REGIONID);
			query.InsertDays= pres.InsertDays;//g.Session.GetInt(PhotoSearch.ValueObjects.PhotoGalleryQuery.PHOTOGALL_INSERTDAYS);
			query.ReloadFlag=  pres.ReloadFlag;//(PhotoSearch.ValueObjects.ReLoadFlags)g.Session.GetInt(PhotoSearch.ValueObjects.PhotoGalleryQuery.PHOTOGALL_RELOADFLAG);
            query.GuestFlag = pres.GuestFlag; //Conversion.CBool( g.Session.GetString(PhotoSearch.ValueObjects.PhotoGalleryQuery.PHOTOGALL_GUEST_FLAG));
            if (brand.GetStatusMaskValue(Matchnet.Content.ValueObjects.BrandConfig.StatusType.PrivatePhotos))
            {
                query.ShowHiddenPhotosFlag = false;
            }
            query.SortBy = (SortByType)Enum.Parse(typeof(SortByType),pres.SortBy.ToString());
			return query;

		}
	}
}
