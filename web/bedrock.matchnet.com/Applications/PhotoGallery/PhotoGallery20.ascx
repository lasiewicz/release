﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PhotoGallery20.ascx.cs" Inherits="Matchnet.Web.Applications.PhotoGallery.PhotoGallery20" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/BasicElements/ResultList20.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mp" TagName="PhotoGalleryFilter" Src="Controls/PhotoGalleryFilter20.ascx" %>

<script type="text/javascript">
	var Page_BlockSubmit = false;
</script>
<div class="header-options clearfix">
	<mn:Title runat="server" id="ttlOnlineNow" ResourceConstant="TXT_PHOTO_GALLERY" />
	<div class="pagination"><asp:label id="lblListNavigationTop" Runat="server" /></div>
</div>
<mp:photogalleryfilter id="photogalleryfilter" runat="server"></mp:photogalleryfilter>
<input id="StartRow" type="hidden" name="StartRow" runat="server">

<div class="clearfix clear-both" id="results-container">
    <result:resultlist id="PhotoGalleryList" runat="server" resultlistcontexttype="PhotoGallery" PageSize=12 ChapterSize=4></result:resultlist>
</div>

<div class="pagination">
    <asp:label id="lblListNavigationBottom" Runat="server" />
</div>