﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.PhotoSearch.ValueObjects;

namespace Matchnet.Web.Applications.PhotoGallery
{
    public class PhotoGalleryFilterSettings
    {
        public int Gender { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int RegionID { get; set; }
        public int CountryRegionID { get; set; }
        public SearchRegionType RegionType { get; set; }
        public int PostedDays { get; set; }
        public int SortBy { get; set; }

    }
}