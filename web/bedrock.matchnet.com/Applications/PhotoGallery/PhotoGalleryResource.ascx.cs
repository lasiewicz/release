﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.PhotoGallery
{
    /// <summary>
    /// Empty control to provide resource text for photo gallery
    /// </summary>
    public partial class PhotoGalleryResource : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}