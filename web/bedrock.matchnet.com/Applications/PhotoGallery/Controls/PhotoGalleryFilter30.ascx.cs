﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.PhotoGallery.Controls
{
    public partial class PhotoGalleryFilter30 : FrameworkControl
    {
        public PhotoGalleryFilterSettings filterSettings = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            btnSearch.Text = g.GetResource("SEARCH", this);
            
            LoadGenderOptions();
            LoadPostedDays();
            LoadRegionOptions();
            LoadSortOptions();
            SetInitialOptions();
        }

        private void LoadGenderOptions()
        {
            GenderMask.DataSource = PhotoGalleryManager.Instance.GetGenderOptions(this, g.Brand);
            GenderMask.DataTextField = "Content";
            GenderMask.DataValueField = "Value";
            GenderMask.DataBind();
        }

        private void LoadPostedDays()
        {
            PostedDays.DataSource = PhotoGalleryManager.Instance.GetPostedDaysOptions(PhotoGalleryManager.Instance.GetPeriodOptions(g.Brand), this, g.Brand);
            PostedDays.DataTextField = "Content";
            PostedDays.DataValueField = "Value";
            PostedDays.DataBind();
        }

        private void LoadRegionOptions()
        {
            RegionID.DataSource = PhotoGalleryManager.Instance.GetRegionOptions(this, g.Brand, g.Member);
            RegionID.DataTextField = "Content";
            RegionID.DataValueField = "Value";
            RegionID.DataBind();
        }

        private void LoadSortOptions()
        {
            SortBy.DataSource = PhotoGalleryManager.Instance.GetSortOptions(this, g.Brand);
            SortBy.DataTextField = "Content";
            SortBy.DataValueField = "Value";
            SortBy.DataBind();
        }

        private void SetInitialOptions()
        {
            filterSettings = PhotoGalleryManager.Instance.GetFilterSettings(g.Brand, g.Member);

            if (filterSettings.Gender != 0)
                GenderMask.Items.FindByValue(filterSettings.Gender.ToString()).Selected = true;
            else
                GenderMask.SelectedIndex = 0;

            AgeMin.Value = filterSettings.MinAge.ToString();
            AgeMax.Value = filterSettings.MaxAge.ToString();

            if (filterSettings.RegionType != PhotoSearch.ValueObjects.SearchRegionType.anywhere && RegionID.Items.FindByValue(filterSettings.RegionID.ToString()) != null)
            {
                RegionID.Items.FindByValue(filterSettings.RegionID.ToString()).Selected = true;
            }
            else
            {
                RegionID.Items.FindByValue(Constants.NULL_INT.ToString()).Selected = true;
            }

            if (PostedDays.Items.FindByValue(filterSettings.PostedDays.ToString()) != null)
            {
                PostedDays.Items.FindByValue(filterSettings.PostedDays.ToString()).Selected = true;
            }

            if (filterSettings.SortBy >= 0)
                SortBy.Items.FindByValue(filterSettings.SortBy.ToString()).Selected = true;
            else
                SortBy.SelectedIndex = 0;

            hdnInitialGender.Value = filterSettings.Gender.ToString();
            hdnInitialPostedDays.Value = filterSettings.PostedDays.ToString();
            hdnInitialRegion.Value = filterSettings.RegionID.ToString();
            hdnInitialMinAge.Value = filterSettings.MinAge.ToString();
            hdnInitialMaxAge.Value = filterSettings.MaxAge.ToString();
            hdnInitialSortBy.Value = filterSettings.SortBy.ToString();
            
        }

	}
}