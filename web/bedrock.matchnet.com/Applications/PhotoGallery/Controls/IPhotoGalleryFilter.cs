﻿#region System References
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
#endregion

#region Matchnet Service References
using Matchnet.Session.ValueObjects;
#endregion

namespace Matchnet.Web.Applications.PhotoGallery.Controls
{
    public interface IPhotoGalleryFilter
    {

         System.Web.UI.HtmlControls.HtmlSelect SelGenderMask { get; }
         System.Web.UI.HtmlControls.HtmlSelect SelRegionID { get; }
         System.Web.UI.HtmlControls.HtmlSelect SelPostedDays { get; }
         System.Web.UI.HtmlControls.HtmlInputText TxtAgeMin { get; }
         System.Web.UI.HtmlControls.HtmlInputText TxtAgeMax { get; }


         System.Web.UI.WebControls.RangeValidator ValAgeMinRange { get; }
         System.Web.UI.WebControls.RangeValidator ValAgeMaxRange { get; }
         System.Web.UI.WebControls.RequiredFieldValidator ValAgeMinReq { get; }
         System.Web.UI.WebControls.RequiredFieldValidator ValAgeMaxReq { get; }
         System.Web.UI.WebControls.ValidationSummary ValSummary { get; }
     
         Matchnet.Web.Framework.Txt TxtShow { get; }
         Matchnet.Web.Framework.Txt TxtFrom { get; }
         Matchnet.Web.Framework.Txt TxtAges { get; }
         Matchnet.Web.Framework.Txt TxtTo { get; }
 



         int SiteID { get; set; }
         int BrandID { get; set; }
         int CommunityiID { get; set; }
         int QueryRegionID { get; set; }
         int MemberRegionID { get; set; }
         int MemberCountryRegionID { get; set; }
         PhotoSearch.ValueObjects.PhotoGalleryQuery Query { get; set; }

         FrameworkControl ResourceControl { get;}
    }
}
