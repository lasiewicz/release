var IMTrackingSiteID = null;
var IMTrackingPageID = null;
var IMTrackingMemberID = null;
var IMTrackingDestinationMemberID = null;

function focusIt()
	{
		window.focus();
	
		var icObject = getICObject();
		
		if( icObject != null && icObject.focus != undefined )
		{
			icObject.focus();
		}
	}
	
	function getICObject()
	{
		if(document.all)
		{
			return document.all["ic"];
		}
		else if(document.layers)
		{
			return document.ic;
		}
		else if(document.getElementById)
		{
			return document.getElementById("ic");
		}
		
		return null;
	}
	
	function sendCommand( commandIn, valueIn )
	{	
		if( commandIn == "focus" )
		{
			/**************************************
			DISABLED FOCUS COMMAND - ISSUE 16246 
			Focus interrupts typing while maintaining multiple IM/WebMessenger conversations 
			even in English version of IM */
			/***************************************
			// DO NOT EDIT
			var icObject = getICObject();
			// only do the focus if we are sure it is not going remove focus from typing area
			if( icObject != null && ( icObject.focus != undefined || ( navigator.userAgent.indexOf( "MSIE" ) >= 0 && navigator.userAgent.indexOf( "Mac" ) >= 0 ) ) )
			{
				try 
				{	//check for existence of this variable which is in the userplaneIMHebrewDHTMLsupport.js file
					IsHebrewOnWin32IE = IsHebrewOnWin32IE;	
					//in Hebrew DHTML IM - setting focus on flash object and/or window causes focus to be lost from the DHTML typing area - so don't set focus. 15855 and 15801
				}
				catch (ex) 
				{	
					//bring window into focus (to alert user that received IM) if they are in other app.
					window.focus();
					//In the English "ALL Flash" IM - bring flash movie into focus so that cursor will be in focus in flash, otherwise it won't be (Userplane support: Doing a window.focus only focus the IE window.  It does not tell IE to focus on the flash movie.)  
					icObject.focus();
				}
			}
			****************************************/
		}
		else if( commandIn == "print" )
		{
			// DO NOT EDIT
			
			openIMPrintWindow( valueIn );
		}
		else
		{
			// EDIT HERE: you will need to handle the following commands from the ic client
			if( commandIn == "viewProfile" )
			{
					
					if( valueIn == "-1" )
					{	//Viewing their own profile.
					
						//profileLink variable value is set in asp.net control
						//profileLink = "<%=ViewMemberProfile%>";
						
						// bug 15532 don't try and open own profile in opener
						// because it may be a pop-up window, and then you'll see own profile with nav and header in pop-up
	
						/* 
						try
						{
							window.opener.location.href = profileLink;
						}
						catch(e)
						{ */
							launchWindow(profileLink, 'ProfileFromIM', 800, 600
								, "scrollbars=yes,resizable=yes,menubar=yes,location=yes,directories=yes,toolbar=yes");
						
					}
					else
					{	// Viewing the recipient's profile, will be a javascript call to launch a popup window.
						// destinationProfileLink variable value is set in asp.net control
						//destinationProfileLink = "<%=ViewDestinationMemberProfile%>";
						// Although the value passed is the destinationMember's ID
					    // we are using the .Net method to create link instead of a client side method
					    // For both popup enabled and non popup sites, we have to make sure this call works by making sure
					    // the javascript call is included within this variable already.
					    if (destinationProfileLink.toLowerCase().indexOf("launchwindow") > 0) {
					        eval(destinationProfileLink);
					    }
					    else {
					        launchWindow(destinationProfileLink, 'ProfileFromIM', 800, 600
								, "scrollbars=yes,resizable=yes,menubar=yes,location=yes,directories=yes,toolbar=yes");
					    }
					}
			}
			else if( commandIn == "help" )
			{			        
				var helpLink = "/Applications/Article/ArticleView.aspx?CategoryID=1944";
				//can't open in opener because opener may be chat window or pop-up profile
					launchWindow(helpLink, 'ArticlesFromIM', 800, 600
						, "scrollbars=yes,resizable=yes,menubar=yes,location=yes,directories=no,toolbar=yes");

			}
			else if( commandIn == "buddyList" )
			{	// view their buddy list (we've turned this feature off)
				var favoritesLink = "/Applications/HotList/View.aspx?CategoryID=0";
				try
				{
					window.opener.location.href = favoritesLink;
				}
				catch(e)
				{
					launchWindow(favoritesLink, 'HotListsFromIM', 800, 600
						, "scrollbars=yes,resizable=yes,menubar=yes,location=yes,directories=no,toolbar=yes");
				}				
			}
			else if( commandIn == "preferences" )
			{
				// view the preferences
			}
			else if( commandIn == "addBuddy" )
			{
				var memberID = valueIn;
				// add memberID to their buddy list 
				//(Actual member add ishandled through ICXML interface - not here)
				// Show some confirmation to user that their hotlist/favorites was updated. bug 15858 Variable YOUR_LIST_HAS_BEEN_UPDATED is set in code-behind.
				alert(YOUR_LIST_HAS_BEEN_UPDATED); //WARNING: will show confirmation even if add failed
			}
			else if( commandIn == "Game.Open" )
			{
				openGameWindow( valueIn );
			}   
			else
			{
				// this happens when they are blocking/unblocking the recipient.
			}
		}
	}
	
	function openGameWindow( qs )
    {
        var newWindow = window.open("http://www.userplane.com/chatlite/games/?" + qs,"game","width=600,height=555,scrollbars=yes,resizable=yes,menubar=yes,location=no,status=no,directories=no,toolbar=no");
        var trackingImage = newWindow.document.createElement('img');

        trackingImage.setAttribute("src", "http://pixel.spark-networks.com/pixit.aspx?sid=" + IMTrackingSiteID + "&pid=19430&mid=" + IMTrackingMemberID + "&vid=" + IMTrackingDestinationMemberID);
        trackingImage.setAttribute("height", "1");
        trackingImage.setAttribute("width", "1");
        trackingImage.setAttribute("border", "0");

        //alert(trackingImage.src);
        //trackingImage.setAttribute("src", "http://stb.msn.com/i/9B/9C5B35ACD1264A2EFC468CE3E34217.jpg");

        newWindow.document.body.appendChild(trackingImage);
        newWindow.document.close();

        if (newWindow == null)
        {
            alert( "Your popup blocker stopped the game window from opening" );
        }
    }

	//don't know whether in use??
	function openIMPrintWindow( strContents )
	{
		var newWindow = window.open("","imPrint","width=550,height=480,scrollbars=yes,resizable=yes,menubar=yes,location=no,status=no,directories=no,toolbar=no");
		newWindow.opener = self;
		newWindow.document.open();
		newWindow.document.write( strContents );
		newWindow.document.close();
		newWindow.print();
		newWindow.close();
	}
	
	// resize the window back if the user has disable resizable=no in the DOM, as FF permits. 
	function resizeWindowBack()
	{
		newWidth = 368;
		newHeight = 516;
		
		// Working around the Netscape 4 resizeTo bug
		if (document.layers)
		{
			tmp1 = parent.outerWidth - parent.innerWidth;
			tmp2 = parent.outerHeight - parent.innerHeight;
			newWidth -= tmp1;
			newHeight -= tmp2;
		}
		parent.window.resizeTo(newWidth,newHeight);
	}
	
