using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.InstantMessenger
{

	/// <summary>
	///		ICXML is the starting point for requests that originate from the Flash Comm Server
	///		or Flash IC control to request IM related data from our system.
	///		
	///		This control examines the request and delegates the responsibility of fulfilling
	///		the request to the appropriate response control.  K.Landrus 3/2/2005
	///</summary>
	public class ICXML : FrameworkControl
	{
		protected System.Web.UI.WebControls.PlaceHolder phResponseControl;

		private const string RESPONSE_CONTROL_PATH				= "/Applications/InstantMessenger/XMLResponses/{0}.ascx";
		private const string ACTION_ADDFRIEND					= "addfriend";
		private const string ACTION_GETBLOCKEDSTATUS			= "getblockedstatus";
		private const string ACTION_GETDOMAINPREFERENCES		= "getdomainpreferences";
		private const string ACTION_GETMEMBERID					= "getmemberid";
		private const string ACTION_NOTIFYCONNECTIONCLOSED		= "notifyconnectionclosed";
		private const string ACTION_SENDPENDINGMESSAGES			= "sendpendingmessages";
		private const string ACTION_SETBLOCKEDSTATUS			= "setblockedstatus";
		private const string ACTION_STARTCONVERSATION			= "startconversation";
		private const string ACTION_STARTIC						= "startic";
		private const string ACTION_GETCONFPHONENUMBER			= "getconfphonenumber";

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				g.SaveSession = false;

				// examine the request to determine the correct response control.
				string action = Request["action"];
			
				if (action == null)
				{	// this will result in an valid respose to this unknown request.
					action = string.Empty;
				}
				generateResponse(action);

			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		/// <summary>
		/// This method will load the response control that corresponds to the requested action.
		/// </summary>
		/// <param name="pAction"></param>
		private void generateResponse(string pAction)
		{
			string responseControlName = string.Empty;

			switch(pAction.ToLower())
			{
				case ACTION_ADDFRIEND:
					responseControlName = "AddFriendResponse";
					break;

				case ACTION_GETDOMAINPREFERENCES:
					responseControlName = "GetDomainPreferences";
					break;

				case ACTION_GETMEMBERID:
					responseControlName = "GetMemberID";
					break;

				case ACTION_NOTIFYCONNECTIONCLOSED:
				case ACTION_SENDPENDINGMESSAGES:
					responseControlName = "NotifyConnectionClosed";
					break;

				case ACTION_SETBLOCKEDSTATUS:
				case ACTION_GETBLOCKEDSTATUS:
					responseControlName = "BlockedStatus";
					break;

				case ACTION_STARTCONVERSATION:
					responseControlName = "StartConversation";
					break;

				case ACTION_STARTIC:
					responseControlName = "StartIC";
					break;
				
				case ACTION_GETCONFPHONENUMBER:
					responseControlName = "GetConfPhoneNumber";
					break;

				default:
					responseControlName = "";
					break;
			}

			// execute the control.
			phResponseControl.Controls.Add(LoadControl(string.Format(RESPONSE_CONTROL_PATH, responseControlName)));
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
