using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.InstantMessenger
{

    /// <summary>
    ///		Summary description for upInstantCommunicator.
    /// </summary>
    public class upInstantCommunicator : FrameworkControl
    {

        private const string SWF = "swf.";
        private string _ApplicationName = "InstantCommunicator";
        private string _DestinationMemberID = string.Empty;
        private bool _IsInitiating = false;

        protected System.Web.UI.WebControls.PlaceHolder phUserplaneWebMessenger;
        protected PlaceHolder NotificationView;
        protected Label Notification;
        protected Matchnet.Web.Framework.Image logo;
        protected Matchnet.Web.Framework.Image Image1;
        protected System.Web.UI.WebControls.PlaceHolder phHebrewSupport;
        protected System.Web.UI.WebControls.Literal litHebrewCompatParam;
        protected System.Web.UI.WebControls.Literal litFocusOnLoad;
        protected System.Web.UI.WebControls.Literal litUserplaneJscriptValues;
        protected System.Web.UI.WebControls.Literal litRedirectIfNotPopUpJscript;
        protected System.Web.UI.WebControls.Literal litHotlistUpdatedResourceValues;
        protected System.Web.UI.WebControls.PlaceHolder phHeaderLogoCupid;
        protected System.Web.UI.WebControls.LinkButton btnOpenIMEvenThoughOffline;
        protected System.Web.UI.WebControls.HyperLink btnRecipientOfflineDontIM;
        protected System.Web.UI.WebControls.PlaceHolder phHeaderLogoGeneral;
        protected PlaceHolder plcRejectIM;


        private void Page_Load(object sender, System.EventArgs e)
        {
            _DestinationMemberID = Request["DestinationMemberID"];
            string inviteKey = Request["ConversationKey"];
            //get required parameters

            if (Request["ConversationKey"] == null)
            {
                _IsInitiating = true;
            }

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["RejectIM"] != null)
                {
                    plcRejectIM.Visible = true;
                }
                else
                {
                    ShowInstantMessengerApplication(false);
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="openIMEvenIfRecipientOffline">If false, show message first that recipient is offline. Give user a choice to try again. If true, will open IM normally.</param>
        private void ShowInstantMessengerApplication(bool openIMEvenIfRecipientOffline)
        {
            string errorMessage = IMPrerequisitesOK(_IsInitiating, openIMEvenIfRecipientOffline);
            if (errorMessage != null)
            {
                Notification.Text = errorMessage;
                NotificationView.Visible = true;
                phUserplaneWebMessenger.Visible = false;
                return;
            }
            else
            {
                phUserplaneWebMessenger.Visible = true;
            }

            if (IsHebrewOnWin32IE)
            {
                // The wmode parameter is only applicable to the DHTML version of the tool,
                // it's presence causes issues with the standard flash version.
                litHebrewCompatParam.Visible = true;
                phHebrewSupport.Visible = true;
                //Calling the focusIt() method on body load event causes focus to go to the typing area when the IM window opens (body onload) - see 15466.
                //However, if this is a Hebrew DHTML IM client, then focus on typing area is set on the createLayer call, 
                //not on body=onload, because it's a HTML text input -  see 16855.
                //So, don't call flash object into focus if this is Hebrew client... Also, in Cupid 
                //there is another side effect of setting focus on flash object - it seems to make flash jump upwards and
                // cover the ad banner IFRAME on the top of the window 16858. So, if we ever add an IFRAME ad banner to the
                //English sites, we will have to revisit this problem.
                litFocusOnLoad.Visible = false;
            }

            // Set jscript variables to link to view member's own profile and destination member profile.
            // (The flash IM client calls jscript (userplaneIM.js) invoking viewProfile command.)
            // Note: apparently, RegisterStartupScript does not work in this environment?
            this.litUserplaneJscriptValues.Text = string.Format(@"
				var profileLink = ""{0}"";
				var destinationProfileLink = ""{1}"";
			", this.ViewMemberProfile, this.ViewDestinationMemberProfile);

            // set jscript variable to show some confirmation to user that their hotlist/favorites list
            // has been updated. bug 15858
            this.litHotlistUpdatedResourceValues.Text = string.Format(@"var YOUR_LIST_HAS_BEEN_UPDATED = ""{0}"";"
                , g.GetResource("YOUR_LIST_HAS_BEEN_UPDATED", null));

        }
        /// <summary>
        /// Try and change the html TITLE of the page to reflect the username of the destination
        /// member. It shouldn't be this hard. Really.
        /// First get the layout template, then find the head, then find title control.
        /// Will cease to function if names of controls and the expected template change.
        /// </summary>
        /// <param name="e"></param>
        protected void SetPageTitle()
        {
            System.Web.UI.Control layoutTemplate = this.Page.Controls[0];
            if (layoutTemplate != null)
            {
                Matchnet.Web.Framework.Ui.Head20 head = (Matchnet.Web.Framework.Ui.Head20)layoutTemplate.FindControl("Head20");
                if (head != null)
                {
                    Literal litTitle = (Literal)head.FindControl("litTitle");
                    if (litTitle != null)
                    {
                        string destinationMemberName = MemberSA.Instance.GetMember(Convert.ToInt32(_DestinationMemberID), MemberLoadFlags.None).GetUserName(_g.Brand);
                        //show destinations member's name so that user's can differentiate between multiple IM windows 16257
                        litTitle.Text = g.GetResource("HTTPIM_INSTANT_MESSAGE_TITLE", this) + " " + destinationMemberName;
                    }
                }
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            //set the page title to reflect username of destination member
            SetPageTitle();
            base.OnPreRender(e);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenIMEvenThoughOffline.Click += new System.EventHandler(this.btnOpenIMEvenThoughOffline_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        public string ApplicationName
        {
            get
            {
                return _ApplicationName;
            }
        }

        /// <summary>
        /// This implementation is based on the documentation provided by Userplane, they only need the
        /// 'real' browser if it's IE on Win32, otherwise this property returns an empty string.
        /// </summary>
        public string Browser
        {
            get
            {
                if (IsHebrewOnWin32IE)
                {
                    return Request.Browser.Browser;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string DestinationMemberID
        {
            get
            {
                return _DestinationMemberID;
            }
        }

        public string FlashComServer
        {
            get
            {
                return Configuration.ServiceAdapters.RuntimeSettings.GetSetting("INSTANTMESSENGERSVC_UP_FLASH_COM_SERVER");
            }
        }

        public string FlashVars
        {
            get
            {
                return "server=" + FlashComServer
                    + "&swfServer=" + SwfServer
                    + "&domainID=" + getDomainIDForUserplane()
                    + "&applicationName=" + ApplicationName
                    + "&sessionGUID=" + SessionGUID
                    + "&key="
                    + "&destinationMemberID=" + DestinationMemberID
                    + "&locale=" + GetLocale()
                    + "&browser=" + Browser
                    + "&memberID=" + g.Member.MemberID.ToString()// from Nate@userplane 9/18/05: "You need to have &memberID={memberID} at the end of the Flashvars in the IC HTML.  This variable is only used by the Load Balancer code and will not disrupt the security in any way." Added after JDate launch traffic increase warranted Userplane moving to Load Balanced solution, which requires this variable.
                    + "&resizable=false"	// resizable must be false for forcedwidth and forcedheight to take effect
                    + "&forcedwidth=360"
                    + "&forcedheight=" + SwfHeight;
            }
        }

        /// <summary>
        /// Returns the Flash variables necessary to pass to the "CMD.swf" flash movie when it opens to send a rejection message
        /// to the sender. The key parameters (different than IM client) here are strEvent = "User.ConversationRefused" and the strParameter which
        /// contains the message to send to the sender.
        /// </summary>
        public string FlashVarsIMReject
        {
            get
            {
                //perform check if this is a JD/JDIL communication. Send rejection in Hebrew if to a JDIL member. Send in English if to a JD member. TT 18078
                if ((int)WebConstants.COMMUNITY_ID.JDate == g.Brand.Site.Community.CommunityID)
                {
                    Member.ServiceAdapters.Member destinationMember = MemberSA.Instance.GetMember(Conversion.CInt(DestinationMemberID), MemberLoadFlags.None);
                    //get instance of the BaseXMLResponse class because it has the GetLastBrandJDate() instance method in it. Could make it static, but...
                    XMLResponses.BaseXMLResponse obj = new Matchnet.Web.Applications.InstantMessenger.XMLResponses.BaseXMLResponse();
                    //HACK alert! Force change g.brand to the last logged in brand of the *destination* member (so they get rejection message in correct language)
                    g.Brand = obj.GetLastBrandJDate(destinationMember);
                }

                return
                "strServer=" + FlashComServer
                + "&strSwfServer=" + SwfServer
                + "&strDomainID=" + getDomainIDForUserplane()
                + "&strSessionGUID=" + SessionGUID
                + "&strKey="
                + "&strLocale=" + GetLocale()
                + "&strEvent=" + "User.ConversationRefused"
                + "&strParameter=" + Server.UrlEncode(g.GetResource("IM_REJECT", this, new string[] { g.Member.GetUserName(_g.Brand) }))
                + "&strMemberID=" + g.Member.MemberID.ToString()
                + "&strDestinationMemberID=" + DestinationMemberID;

            }
        }

        public bool IsHebrewOnWin32IE
        {
            get
            {
                if (g.Brand.Site.LanguageID != (int)Matchnet.Language.Hebrew)
                {
                    return false;
                }
                else
                {
                    return ((Request.Browser.Browser == "IE") && (Request.Browser.Win32));
                }
            }
        }
        public string SessionGUID
        {
            get
            {
                return g.Session.Key.ToString();
            }
        }

        public string SwfHeight
        {
            get
            {
                return "420";
            }
        }

        /// <summary>
        /// We use an DNS alias to swf.userplane.com server instead of swf.userplane.com to serve the Flash movie
        /// for apparently one reason only: so when you set the flash settings for video camera so that flash asks:
        /// "Allow jdate.com" to have access to your camera instead of "Allow userplane.com."
        /// </summary>
        public string SwfServer
        {
            get
            {
                return SWF + g.Brand.Site.Name.Trim();
            }
        }

        public string GetLatestUserplaneVersion
        {
            get
            {
                return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IM_LATESTVERSION", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
            }
        }

        public string GetIMTrackingSiteID
        {
            get
            {
                return g.Brand.Site.SiteID.ToString();
            }
        }

        public string GetIMTrackingMemberID
        {
            get
            {
                return g.Member.MemberID.ToString();
            }
        }

        /// <summary>
        /// Gets the url of a  member profile window. Uses overloaded 
        /// method of BreadCrumbHelper.MakeViewProfileLink that returns url string
        /// because we're passing true to forceSameWindow
        /// </summary>
        private string GetDestinationMemberProfileURL
        {
            get
            {
                return BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.NoArgs,
                    Convert.ToInt32(_DestinationMemberID),
                    0,
                    null,
                    0,
                    true
                    );
            }
        }

        /// <summary>
        /// Gets the javascript to call launch window to pop-open a member profile 
        /// window. Uses overloaded method of BreadCrumbHelper.MakeViewProfileLink
        /// that returns javascript to open new window instead of url 
        /// (using window.open from flash is blocked by default by pop-up blocker in Win XP SP2 (IE 6))
        /// </summary>
        public string ViewDestinationMemberProfile
        {
            get
            {
                return BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.NoArgs, Convert.ToInt32(DestinationMemberID));
            }
        }

        public string ViewMemberProfile
        {
            get
            {
                return BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.NoArgs, g.Member.MemberID);
            }
        }


        // The bedrock sites correspond to 'domains' in the userplane environment
        // an exception to this case is development, where individual developer workstations
        // will be routed through the dev.site.com XML server.
        private string getDomainIDForUserplane()
        {
            // As of 9/7/05, g.Brand.Site.DefaultHost returns either www or dev
            // In order to test on stage, we need to leave stage in the URL - related bug: 14779)
            string subDomainPrefix = g.Brand.Site.DefaultHost;

            string requestUrlBeg = string.Empty;
            if (Request.Url.Host.ToLower().Length >= 8)
                requestUrlBeg = Request.Url.Host.ToLower().Substring(0, 8);


            if (requestUrlBeg != string.Empty && requestUrlBeg != null)
            {
                if (requestUrlBeg.Substring(0, 3).IndexOf("stg") > -1 || requestUrlBeg.Substring(0, 3).IndexOf("dev") > -1)
                {

                    if (requestUrlBeg.Substring(0, 4).IndexOf("stgv") > -1)
                    {
                        //and now we have stgv101.....
                        subDomainPrefix = requestUrlBeg.Substring(0, 7);
                    }
                    else
                    {
                        // To support the new stage env that starts with stg, we need to be able to accept this format
                        // stg## --> Examples: stg01, stg02, etc.
                        subDomainPrefix = requestUrlBeg.Substring(0, 5);
                    }
                }
                else if (requestUrlBeg.IndexOf("preprod") > -1)
                {
                    subDomainPrefix = "preprod";
                }
#if DEBUG
                else if (requestUrlBeg.IndexOf("local") > -1) // TODO: REMOVE THIS LATER (ONLY FOR DEV)
                {
                    subDomainPrefix = "bhupdev";
                }
#endif
            }

            // Jdate.com and Jdate.co.il belong to the same IM domain.
            switch (g.Brand.Site.Community.CommunityID)
            {
                case (int)WebConstants.COMMUNITY_ID.JDate:
                    return subDomainPrefix + ".jdate.com";
                case (int)WebConstants.COMMUNITY_ID.AmericanSingles:
                    return subDomainPrefix + ".spark.com";//american singles community sites belong to the same IM domain (date.ca, date.co.uk, americansingles.com) 16762
                case (int)WebConstants.COMMUNITY_ID.Cupid:
                    return subDomainPrefix + ".cupid.co.il";
                default:
                    return subDomainPrefix + "." + g.Brand.Site.Name;//cupid.co.il, glimpse.com, and collegeluv.com are each their own domains too.

            }

        }

        /// <summary>
        /// The locale parameter specifies to userplane which locale XML file to load on their end.
        /// The locale XML file contains the text elements that appear in IM app. 
        /// As of 10/25/05 there are six locale files: english-glimpse.xml, english-jdate.xml, english-spark.xml and 
        /// their hebrew equivalents. See ReadMe.html (hebrew-spark.xml is not in use obviously)
        /// </summary>
        /// <returns></returns>
        private string GetLocale()
        {
            string locale = "english-spark";
            #region old hardcoded locale switch to use sitesetting
            //switch (g.Brand.Site.Community.CommunityID)
            //{
            //    case ((int)WebConstants.COMMUNITY_ID.AmericanSingles):
            //        locale = "english-spark"; // All AmericanSingles community sites use this
            //        break;
            //    case (int)WebConstants.COMMUNITY_ID.Glimpse:
            //        locale = "english-glimpse";// Glimpse uses this
            //        break;
            //    case (int)WebConstants.COMMUNITY_ID.Mingle:
            //        locale = "english-jewishmingle";
            //        break;
            //    case (int)WebConstants.COMMUNITY_ID.JDate:
            //        if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            //        {
            //            locale = "hebrew-jdate";// Hebrew JDate.co.il uses this
            //        }
            //        else if (g.Brand.Site.LanguageID == (int)Matchnet.Language.French)
            //        {
            //            locale = "french-jdate";
            //        }
            //        else if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateUK)
            //        {
            //            locale = "english-jdatecouk";
            //        }
            //        else
            //        {
            //            locale = "english-jdate";// English JDate uses this
            //        }
            //        break;
            //    case (int)WebConstants.COMMUNITY_ID.Cupid:
            //        locale = "hebrew-cupid";// Cupid uses this
            //        break;
            //    default:
            //        locale = "english-spark"; // everything else (e.g., CollegeLuv, Spark.com etc.)
            //        break;
            //}
            #endregion

            locale = SettingsManager.GetSettingString(SettingConstants.USER_PLANE_LOCALE_FILE, g.Brand);
            
            return locale.ToLower();
        }

        /// <summary>
        /// Checks whether the user is allowed to IM.
        /// </summary>
        /// <param name="pInitiate"></param>
        /// <returns>Null string if allowed or the error message to display otherwise.</returns>
        private string IMPrerequisitesOK(bool pInitiate, bool openIMEvenIfRecipientOffline)
        {
            int destinationMemberID = Conversion.CInt(_DestinationMemberID);
            Matchnet.Member.ServiceAdapters.Member recipientMember = MemberSA.Instance.GetMember(destinationMemberID, MemberLoadFlags.None);

            // Is a Subscription required for this contact?
            if (pInitiate && !g.ValidateMemberContact(destinationMemberID))
            {
                LinkParent linkParent;
                if (Request["LinkParent"] != null)
                {
                    linkParent = (LinkParent)Conversion.CInt(Request["LinkParent"]);
                }
                else
                {
                    linkParent = LinkParent.FullProfile;
                }

                int prtid = Constants.NULL_INT;
                switch (linkParent)
                {
                    case LinkParent.MiniProfile:
                        prtid = (int)PurchaseReasonType.AttemptToIMMiniProfile;
                        break;
                    case LinkParent.GalleryMiniProfile:
                        prtid = (int)PurchaseReasonType.AttemptToIMGalleryMiniProfile;
                        break;
                    case LinkParent.FullProfile:
                        prtid = (int)PurchaseReasonType.AttemptToIMFullProfile;
                        break;
                    case LinkParent.MatchMeterOneOnOne:
                        prtid = (int)PurchaseReasonType.JmeterOneOnOneIMAttempt;
                        break;
                    case LinkParent.FavoritsOnline:
                        prtid = (int)PurchaseReasonType.AttemptToIMFavoritsOnline;
                        break;
                }
                // 15761 If user is not a subscriber, then we redirect them to subscribe,
                //however, upon returning, we cannot pop-open the IM window they desire
                // (pop-blockers being main reason) so we actually redirect them back to
                // the View Profile page of the member they were interested in. (bug was that IM was appearing in main parent window) 
                // (Removed code here that relied on UrlReferrer to check if coming from subscription that simply was not working) Adam 8/31/05
                string viewProfileLink = GetDestinationMemberProfileURL;

                //Determine if original click was not inside pop-up window by checking if link that initiated Instant Messenger was not from a FullProfile (bug 15941)
                if (linkParent != LinkParent.FullProfile)
                {
                    //since link was not inside a full-profile pop-up, then remove template 11
                    viewProfileLink = viewProfileLink.Replace("&PersistLayoutTemplate=1", string.Empty).Replace("&LayoutTemplateID=11", string.Empty);
                }

                Redirect.Subscription(g.Brand, prtid, destinationMemberID, false, Server.UrlEncode(viewProfileLink));
            }
            else
            {
                // Show IM as usual, but check that we are in a window named
                // IM[DestinationMemberID], if not, then we're not in a pop-up
                // so redirect to profile page in this window (window name comes from
                // launchIMWindow method in spark.js. IMPORTANT that names be in-synch
                // (See bug 15761 for scenarios to arrive at IM not in pop-up)
                // HACK alert: stripping out PersistLayoutTemplate and LayoutTemplateID so profile appears with nav and header
                // HACK alert 2: do not render this script in Safari browsers as window.name is not supported! 16478
                //http_user_agent = Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en-us) AppleWebKit/85.8.2 (KHTML, like Gecko) Safari/85.8.1
                if (Request.ServerVariables["http_user_agent"].ToLower().IndexOf("safari") == -1)
                    this.litRedirectIfNotPopUpJscript.Text = string.Format(@"if (self.name != 'IM{0}' )  
				{{
				var destinationProfileURL = ""{1}"";
				window.location.replace(destinationProfileURL); 
				}}"
                    , destinationMemberID, GetDestinationMemberProfileURL.Replace("&PersistLayoutTemplate=1", string.Empty).Replace("&LayoutTemplateID=11", string.Empty));


            }
            // Recipient is not the same as the sender
            if (g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) == destinationMemberID)
            {
                return g.GetResource("ERR_YOU_CANT_IM_YOURSELF", this);
            }

            // if the initiator of the IM is attempting to contact a blocked member, proactively
            // remove the blocked member from the hotlist
            if (pInitiate && g.List.IsHotListed(HotListCategory.IgnoreList, g.Brand.Site.Community.CommunityID, destinationMemberID))
            {
                ListSA.Instance.RemoveListMember(HotListCategory.IgnoreList,
                    g.Brand.Site.Community.CommunityID,
                    g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                    destinationMemberID);
            }

            StringDictionary tokens = new StringDictionary();

            //copy the global tokens to our local dictionary first. 
            foreach (DictionaryEntry item in _g.ExpansionTokens) { 
                tokens.Add(item.Key.ToString(), item.Value.ToString()); 
            } 
            
            tokens.Add("RECIPIENT_MEMBERNAME", recipientMember.GetUserName(_g.Brand));

            // is this user attempting to contact a member that is blocking them?
            Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(destinationMemberID);
            if (pInitiate &&
                list.IsHotListed(HotListCategory.IgnoreList, g.Brand.Site.Community.CommunityID, g.Member.MemberID))
            {
                return g.GetResource("THE_USER_YOU_RE_TRYING_TO_MESSAGE_IS_IGNORING_YOU_X", this, tokens);
            }
            // check if user is self-suspended

            int selfSuspended = recipientMember.GetAttributeInt(g.Brand, "SelfSuspendedFlag");
            if (selfSuspended == 1)
            {
                return g.GetResource("THE_USER_YOURE_TRYING_TO_IM_IS_SELF_SUSPENDED", this, tokens);
            }

            // check if user has logged-off (no longer online)
            bool isOnline = Matchnet.MembersOnline.ServiceAdapters.MembersOnlineSA.Instance.IsMemberOnline(g.Brand.Site.Community.CommunityID, destinationMemberID);
            // If user is Initiating a conversation with another member who has happened to log out in meantime,
            // then give them choice to continue or not. Ignore warning if accepting an invitation (because initiating
            // user may have hidden themselves and appears offline always 16872)
            if (!isOnline && !openIMEvenIfRecipientOffline && pInitiate)
            {
                this.btnOpenIMEvenThoughOffline.Text = g.GetResource("BUTTON_RECIPIENT_OFFLINE_IM_ANYWAY", this);
                this.btnRecipientOfflineDontIM.Text = g.GetResource("BUTTON_RECIPIENT_OFFLINE_DONT_IM", this);
                this.btnOpenIMEvenThoughOffline.Visible = true;
                this.btnRecipientOfflineDontIM.Visible = true;

                return g.GetResource("RECIPIENT_MEMBER_IS_OFFLINE", this, tokens);

            }

            bool checkFraud = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_IM_FRAUD_CHECK", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            if (checkFraud)
            {
                bool passedFraud = Conversion.CBool(g.Member.GetAttributeInt(g.Brand, "PassedFraudCheckSite", 1));
                if (!passedFraud)
                {
                    return g.GetResource("NOT_PASSED_FRAUD", this, tokens);
                }
            }

            // If they fail this test, they most likely tried to hack this page by loading it directly in a browser.
            if (!pInitiate)
            {
                // remember recipientMember is actually the sender/initiator of this IM
                if (!Matchnet.Web.Applications.InstantMessenger.IMHelper.CanMemberReplyIM(g.Member, recipientMember, g.Brand,g))
                {
                    return g.GetResource("NOT_PASSED_FRAUD", this, tokens);
                }
            }


            return null;
        }

        /// <summary>
        /// Show IM application anyway because user chose to try an IM someone 
        /// even though it appears they are offline.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpenIMEvenThoughOffline_Click(object sender, System.EventArgs e)
        {
            this.ShowInstantMessengerApplication(true);
        }
    }
}
