<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="upInstantCommunicator.ascx.cs" Inherits="Matchnet.Web.Applications.InstantMessenger.upInstantCommunicator" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<body class="imBody"  <asp:Literal ID="litFocusOnLoad" Runat="server">onload="focusIt();"</asp:Literal> onResize="javascript:resizeWindowBack();">
<asp:PlaceHolder id="phHebrewSupport" runat="server" Visible="False">
	<!-- for Hebrew DHTML overlay support. Used by CreateLayer method -->
	<script type="text/javascript" src="javascript/userplaneIMHebrewDHTMLsupport.js"></script>
	<Div id="dynlayer" style="BORDER-RIGHT:0px solid; BORDER-TOP:0px solid; LEFT:0px; BORDER-LEFT:0px solid; BORDER-BOTTOM:0px solid; POSITION:absolute; TOP:0px"></Div>
	<script language="VBScript">
		<!--
		'Map VB script events to the JavaScript method - Netscape will ignore this... 
		'Since FSCommand fires a VB event under ActiveX, we respond here 
		Sub ic_FSCommand(ByVal command, ByVal args)
			call ic_DoFSCommand(command, args)
		end sub
		-->
	</script>
</asp:PlaceHolder>
	<!-- include common userplane javascript functions -->
	<script type="text/javascript" src="javascript/userplaneIM.js"></script>
	<!-- include some variables definitions defined in code-behind -->
	<script type="text/javascript">
	<!--
		<asp:Literal ID="litUserplaneJscriptValues" Runat="server"></asp:Literal>
		<asp:Literal ID="litRedirectIfNotPopUpJscript" Runat="server"></asp:Literal>
		<asp:Literal ID="litHotlistUpdatedResourceValues" Runat="server"></asp:Literal>
	-->
	</script>
<asp:PlaceHolder ID="phHeaderLogoGeneral" Runat="server">
	<mn:AdUnit id="adShortFullBanner" Size="ShortFullBanner" runat="server" visible="true" expandImageTokens="false" GAMAdSlot="IM_bottom_468x60" />
	<mn:AdUnit id="adShortFullBannerIL" runat="server" visible="false" expandImageTokens="false" GAMAdSlot="IM_bottom_360x48" />
</asp:PlaceHolder>

	<asp:PlaceHolder ID="phUserplaneWebMessenger" Visible="False" Runat="server">
	    <object 
			classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
			codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
			width="360" 
			height="<%=SwfHeight%>" 
			name="ic"
			id="ic" 
			align="" VIEWASTEXT>
			<param name="movie" value="http://<%=SwfServer%>/<%= ApplicationName %>/ic.swf?<%= GetLatestUserplaneVersion %>">
			<param name="quality" value="best"> 
			<param name="scale" value="noborder"> 
			<param name="bgcolor" value="#FFFFFF">
			<param name="menu" value="0">
			<param name="AllowScriptAccess" value="always">
			<asp:Literal ID="litHebrewCompatParam" Runat="server" Visible="False"><param name="wmode" value="transparent" /></asp:Literal>
			<param name="FlashVars" value="<%= FlashVars %>">
			<embed 
				src="http://<%=SwfServer%>/<%= ApplicationName %>/ic.swf?<%= GetLatestUserplaneVersion %>"
				quality="best "
				scale="noborder"
				bgcolor="#FFFFFF"
				menu="0"
				AllowScriptAccess="always"
				width="360" 
				height="<%=SwfHeight%>" 
				name="ic" 
				align=""
				type="application/x-shockwave-flash" 
				pluginspage="http://www.macromedia.com/go/getflashplayer"
				flashvars="<%= FlashVars %>">
			</EMBED>
		</object>		 
		<!-- COPYRIGHT Userplane 2004 (http://www.userplane.com) -->
		<!-- IC version 1.8.5 -->						
	</asp:PlaceHolder>
	<asp:PlaceHolder ID="NotificationView" Visible="False" Runat="server">
	<form runat="server" ID="Form1">
		<mn:image id="Image1" runat="server" FileName="im_header_right.gif"></mn:image>
		<br />
		<br />
		<div class="imBodyPadding">
		<mn:image FileName="page_message.gif" id="imgMessage" runat="server" /><asp:Label id="Notification" Runat="server" cssclass=""></asp:Label>
<br /><br /><asp:LinkButton id="btnOpenIMEvenThoughOffline" Visible="False" Runat="server" ></asp:LinkButton>&nbsp;<br />
<asp:HyperLink id="btnRecipientOfflineDontIM" runat="server" Visible="false" NavigateUrl="javascript:window.close()"></asp:HyperLink>
	</div></form></asp:PlaceHolder>
	
	<asp:PlaceHolder ID="plcRejectIM" Visible="False" Runat="server">
	<!-- calls a special flash movie that passes an automatic rejection message to sender -->
	<object 
	classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
	codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
	width="1" 
	height="1" 
	name="cmd"
	id="cmd" 
	align="" VIEWASTEXT>
	<param name="movie" value="http://<%= SwfServer %>/CommunicationSuite/cmd.swf"> 
	<param name="quality" value="best"> 
	<param name="scale" value="noborder"> 
	<param name="bgcolor" value="#FFFFFF">
	<param name="menu" value="0">
	<param name="salign" value="LT"> 
	<param name="FlashVars" value="<%= FlashVarsIMReject %>">
	<embed 
		src="http://<%= SwfServer %>/CommunicationSuite/cmd.swf"
		quality="best"
		scale="noborder"
		bgcolor="#FFFFFF"
		menu="0"
		width="1" 
		height="1"
		name="cmd" 
		align=""
		salign="LT"
		type="application/x-shockwave-flash" 
		pluginspage="http://www.macromedia.com/go/getflashplayer"
		flashvars="<%= FlashVarsIMReject %>">
	</EMBED>
</object>
	
	
	</asp:PlaceHolder>


    <script type="text/javascript">

        IMTrackingSiteID = '<%= GetIMTrackingSiteID %>';
        IMTrackingMemberID = '<%= GetIMTrackingMemberID %>';
        IMTrackingDestinationMemberID = '<%= DestinationMemberID %>';
    	
    </script>    

</body>
<!-- add closing HTML tag because this is using the "Frameset" layout template -->
</HTML>
