using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.InstantMessenger.XMLResponses
{

	/// <summary>
	///		Summary description for BlockedStatus.
	///		
	///		Handles the Userplane IM class for:
	///		- getBlockedStatus
	///		- setBlockedStatus
	///		
	///		Parameters:
	///		- domainID			(ignored)
	///		- memberID			The member's ID
	///		- targetMemberID	The memberID of the user to block
	///		- trueFalse			Whether or not to block the member.
	/// </summary>
	public class BlockedStatus : BaseXMLResponse
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			string action = null;
			Matchnet.List.ServiceAdapters.List memberList = null;
			try
			{
				action = Request["action"];

				base.startResponse();

				if ("getblockedstatus".Equals(action.ToLower()))
				{
					// lookup to see if the target member is blocked by member
					memberList = ListSA.Instance.GetList(MemberID, ListLoadFlags.IngoreSACache);
					bool isBlocked = memberList.IsHotListed(HotListCategory.IgnoreList
						, g.Brand.Site.Community.CommunityID
						, TargetMemberID);
					writeElement("blocked", isBlocked.ToString());
				}
				else	// action=setBlockedStatus
				{	
					bool blocking = Convert.ToBoolean(Request["trueFalse"]);
					if (blocking)	// blocking the TargetMember
					{
						ListSA.Instance.AddListMember(HotListCategory.IgnoreList,
							g.Brand.Site.Community.CommunityID,
							Constants.NULL_INT,
							MemberID,
							TargetMemberID,
							string.Empty,
							Constants.NULL_INT,
							false,
							false);
					}
					else	//unblocking the TargetMember
					{	
						ListSA.Instance.RemoveListMember(HotListCategory.IgnoreList,
							g.Brand.Site.Community.CommunityID,
							MemberID,
							TargetMemberID);
					}
				}
			}
			finally
			{
				base.endResponse();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
