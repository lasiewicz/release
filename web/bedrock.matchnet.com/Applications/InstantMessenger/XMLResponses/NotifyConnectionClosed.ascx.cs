using System;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using System.Text.RegularExpressions;

using Matchnet.Email.ValueObjects;
using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework;


namespace Matchnet.Web.Applications.InstantMessenger.XMLResponses
{

	/// <summary>
	///		Summary description for NotifyConnectionClosed.
	///		
	///		Handles the XML Response for a notifyConnectionClosed message 
	///		from the Userplane IM system.
	///		
	///		Parameters:
	///		- domainID
	///		- memberID 
	///		- targetMemberID
	///		- xmlData (by POST)
	///		
	/// </summary>
	public class NotifyConnectionClosed : BaseXMLResponse
	{

		private void Page_Load(object sender, System.EventArgs e)
		{

			string xmlData = null;

			try
			{

				base.startResponse();

				// If there are undelivered messages, send them as an onsite email message.
				xmlData = Request["xmlData"];
				if (xmlData != null)
				{
					deliverMessagesViaEmail(MemberID, TargetMemberID, xmlData);
				}
				// remove any pending conversation invitations because initiator has closed window 16615
				// HACK note: JoinConversation is effectively same as Delete invitation as of this writing 10/24/05
				InstantMessengerSA.Instance.JoinConversation(
					this.TargetMemberID
					, this.MemberID
					, g.Brand.Site.Community.CommunityID);



			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
			finally
			{
				base.endResponse();
			}
		}

		private void deliverMessagesViaEmail(int pMemberID, int pTargetMemberID, string pXmlData)
		{
			MessageSave messageSave = new MessageSave(pMemberID,
				pTargetMemberID,
				g.Brand.Site.Community.CommunityID,
                g.Brand.Site.SiteID,
				MailType.MissedIM,
				GetSubject(),
				transformUndeliveredMessages(pXmlData));

			bool saveCopy = false;
			AttributeOptionMailboxPreference mailboxMask = (AttributeOptionMailboxPreference) Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_MAILBOXPREFERENCE);
			if((mailboxMask & AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) == AttributeOptionMailboxPreference.SaveCopiesOfSentMessages)
			{
				saveCopy = true;
			}

			InstantMessengerSA.Instance.SendMissedIM(messageSave, saveCopy, g.Brand, TransactionID, GetQuotaErrorMessage(MemberSA.Instance.GetMember(pTargetMemberID, MemberLoadFlags.None).GetUserName(g.Brand), messageSave));
		}

		/// <summary>
		/// Returns the a MessageSave object that tells the sender s/he has gone over the quota of sending missed IMs in the correct language!
		/// </summary>
		/// <param name="targetMemberUsername"></param>
		/// <param name="originalMessageSave"></param>
		/// <returns></returns>
		private MessageSave GetQuotaErrorMessage(string targetMemberUsername, MessageSave originalMessageSave)
		{
			//18127 Make sure quota message is in the correct language of the SENDING member (not the intended recipient Member)
			SetBrandIfJDIL(Member);
			StringDictionary expansionTokens = new StringDictionary();
			expansionTokens.Add("USERNAME", targetMemberUsername);
			expansionTokens.Add("ORIGINALMESSAGE", originalMessageSave.MessageBody);

			return new MessageSave(originalMessageSave.FromMemberID,
				originalMessageSave.FromMemberID,
				originalMessageSave.GroupID,
                g.Brand.Site.SiteID,
				originalMessageSave.MailType,
				g.GetResource("AT_QUOTA_ERROR_SUBJECT", this),
				g.GetResource("AT_QUOTA_ERROR_MESSAGE", this, expansionTokens));

		}

		/* Userplane will post all undelivered messages for the conversation in the following
		 * XML format:
		 * 
		 * 		<?xml version='1.0' encoding='iso-8859-1'?>
		 *		<undeliveredMessages>
		 *			<message>This was the first typed message.</message>
		 *			<message>This was the second typed message.</message>
		 *		</undeliveredMessages>
		 *
		 */
		private string transformUndeliveredMessages(string pXMLData)
		{
			//Refactored this method: removed unnecessary HtmlDecode (16119) and 
			//improved handling of empty strings, close StringReader and stopped using catch block
			//as part of handling logic! (no longer count on ReadElementString to fail!)
			StringReader sr = new StringReader(pXMLData);
			XmlTextReader reader = new XmlTextReader(sr);
			reader.WhitespaceHandling = WhitespaceHandling.None;
			StringBuilder builder = new StringBuilder();
			try 
			{
				while (reader.Read()) 
				{
					//read until we hit the message element
					if (reader.MoveToContent() == XmlNodeType.Element && reader.Name == "message")
					{
						builder.Append(reader.ReadString() + " "); 
					}
				}
			}
			catch (Exception exXML) 
			{
				//catch badly formed XML errors, such as <> tags inside 
				g.ProcessException(new ApplicationException("Error parsing userplane xml: " + pXMLData,exXML));
				//write output so error will show up in UP app inspector as invalid XML response.
				//DO NOT write out error! Or else Userplane will keep trying ad infinitum... 17191
				//Response.Write("<![CDATA[Error parsing XML: " + ex.ToString() + "]]>");
			}
			finally 
			{
				if (reader != null) reader.Close();
				if (sr != null) sr.Close();

			}

			//mike roberts 7/12/05: get rid of &apos; is a hack to see if we can eliminate the &apos; that keep getting through. See bug 16686 and its predecessor 14273�
			//apparently, &apos; is a XHTML specification not supported by HTML 4.0. Google on &apos; for more info.
			string originalMessages = builder.ToString().Trim().Replace("&apos;", "'");;
			//16119 remove all html tags that might be coming from Userplane 
			//(they probably come HtmlEncoded already, or XML would be invalid)
			//First decode back into HTML (e.g., convert &lt; into <) - 
			string cleanedMessages = HttpUtility.HtmlDecode(originalMessages);
			//see Compose.ascx.cs - uses same pattern - HACK alert - ideally this would be common method
			cleanedMessages = Regex.Replace(cleanedMessages, "<[^>]*>", ""); 
			//HtmlEncode it back, just in case
			cleanedMessages =  HttpUtility.HtmlEncode(cleanedMessages);
			//check if result is the same or if HTML tags were found:
			if (Regex.IsMatch(HttpUtility.HtmlDecode(originalMessages),"<[^>]*>"))
			{	
				//throw an exception so that we can know if Userplane is sending us HTML when it shouldn't
				g.ProcessException(new ApplicationException("Found HTML tags in IM messages from Userplane NotifyConnectionClosed: " + originalMessages));
				//Don't output any unexpeced XML to Userplane! see 16961 and 17191
				//Response.Write("<![CDATA[HTML tags found in message: " + originalMessages + "]]>");
			}
			
			//return missed messages, cleaned of HTML
			return cleanedMessages;


		}

		/// <summary>
		/// Returns correct subject text for the targetMember - can't rely on the context of the url
		/// because of bug 14572, so must check last logged in brand of member.
		/// </summary>
		/// <returns></returns>
		private string GetSubject() 
		{
			//16402 Make sure resources are in Hebrew if this is a JDIL member we are notifying - base brand on TargetMember
			SetBrandIfJDIL(TargetMember);

			string subject = string.Empty;
			//Set the default Resource Constant to use for subject line
			string resourceconstant = "IM_MESSAGES_MISSED";
			
			subject = g.GetResource(resourceconstant, this);
								
			return subject;

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
