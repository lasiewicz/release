using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.ResponseTypes;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.InstantMessenger.XMLResponses
{
	/// <summary>
	///		Summary description for GetConfPhoneNumber.
	///		
	///		This control handles the XML response for getConfPhoneNumber call.  This should
	///		only get called for IL sites.  For non-IL sites, Userplane handles the conference
	///		number generation.
	///		
	///		Request Parameters (both unused at this time):
	///		-conferenceID
	///		-app
	/// </summary>
	public class GetConfPhoneNumber : BaseXMLResponse
	{
		private const string GET_URL_SETTING_CONSTANT = "TELECLAL_GET_URL";
		private const string CONF_PHONE_GENERATE_USERPLANE_KEYWORD = "[[GENERATE]]"; // when this is written, userplane generates the conf phone number for us
		private const string PARAM_APP = "app";

		private string _appString = Constants.NULL_STRING;
		
		/// <summary>
		/// app request query string value looks like this:
		/// icconferenceid={memberID}_{targetMemberID}
		/// </summary>
		protected string AppString
		{
			get
			{
				if(_appString == Constants.NULL_STRING)
				{
					if(Request.QueryString[PARAM_APP] != null)
					{
						_appString = Request.QueryString[PARAM_APP];
					}
					else
					{
						Exception ex = new Exception("Required parameter: " + PARAM_APP + " not found in querystring.");
						g.ProcessException(ex);
						Response.Write(ex.ToString()); //write to the output so that userplane will get invalid XML response
					}
				}
				
				return _appString;
			}
		}

		/// <summary>
		/// Since the memberID is inside the app request parameter, we need this function to parse the memberID
		/// plus initialize it on the base class so that the member get accessor will work correctly.
		/// </summary>
		private void InitializeMemberIDFromAppString()
		{
			if(_MemberID == Constants.NULL_INT)
			{
				int start = -1;
				int end = -1;

				start = AppString.IndexOf('=');
				end = AppString.IndexOf('_', start);

				if (start > -1 && end > -1)
				{
					string strMemberID = AppString.Substring(start + 1, end - start - 1);
					try
					{
						_MemberID = Convert.ToInt32(strMemberID);
					}
					catch (System.FormatException ex1)
					{
						Exception ex = new Exception("Parse MemberID string is not an integer value. " , ex1);
						g.ProcessException(ex);
						Response.Write(ex.ToString()); //write to the output so that userplane will get invalid XML response
					}
				}
				else
				{
					Exception ex = new Exception("Unable to parse the memberID from " + PARAM_APP + ".  Cannot continue. ");
					g.ProcessException(ex);
					Response.Write(ex.ToString()); //write to the output so that userplane will get invalid XML response
				}
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			base.startResponse();

			try
			{
				writeElement("confPhoneNumber", GetConferenceNumber());									
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
			finally
			{
				base.endResponse();
			}			
		}

		private string GetConferenceNumber()
		{
			// If it's the www.jdate.com domain, which actually includes all the jdate community, it gets tricky
			// For jdate.com & JDFR, we let Userplane handle the phone number generation, but for jdate.co.il, we have to take over
			// and call Teleclal to get the number and the extension number for the users to call.  Since for all of the jdate
			//sites, the request comes through www.jdate.com, siteID of g will be always jdate.com even if
			// it's really for jdate.co.il or JDFR.
			if(g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
			{
				// We now need member's information, let's do our initialization since this request came from
				// userplane, g does not have the member info.
				InitializeMemberIDFromAppString();

				// If the last brand id for the member is jdate.com or JDFR, just let Userplane handle the phone number
				// generation.
				int siteID = base.GetLastBrandJDate(base.Member).Site.SiteID;
				if(siteID == (int)WebConstants.SITE_ID.JDate || siteID == (int)WebConstants.SITE_ID.JDateFR)
					return CONF_PHONE_GENERATE_USERPLANE_KEYWORD;
				else
					SetBrandIfJDIL(Member); // If it's JDIL, we need to set the brand so that settings and such won't mess up.
			}
			
			// Get the GET URL for Teleclal that will give us the conf phone number as well as the extension
			string getURL = RuntimeSettings.GetSetting(GET_URL_SETTING_CONSTANT, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

			// Since getconfphonenumber is only allowed for the www.cupid.co.il & the www.jdate.com domains, if we don't have
			// this setting, this is a problem so throw an exception
			if(getURL == "none")
				throw new ApplicationException("Missing " + GET_URL_SETTING_CONSTANT + " cannot continue.");

            // Do a GET on the URL and parse the phone number + the extension that the user should call
			string rawXml = SMSProxy.SendGet(getURL);
            TeleclalResponse resp = SMSParser.ParseTeleclalGet(rawXml);

			return "<![CDATA[" + resp.PhoneNumber + g.GetResource("hebrewExtensionText", this) + " " + resp.ExtensionCode + "]]>";

			//return "<![CDATA[888-555-1212x12345]]>";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
