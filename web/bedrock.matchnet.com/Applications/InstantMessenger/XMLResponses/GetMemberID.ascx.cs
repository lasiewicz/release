using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.InstantMessenger.XMLResponses
{
	/// <summary>
	///		Summary description for GetMemberID.
	///		
	///		This control handles the XML response for a Userplane getMemberID call.
	///		
	///		Request Parameters:
	///		- sessionGUID
	///		- key (optional: not in use)
	///		
	///		The sessionGUID is used to lookup the MemberID for the member, if not found,
	///		INVALID is returned per the userplane interface specification.
	///		
	/// </summary>
	public class GetMemberID : BaseXMLResponse
	{
		private const string PARAM_SESSIONGUID = "sessionGUID";
		private string _sessionGUID = Constants.NULL_STRING;

		private void Page_Load(object sender, System.EventArgs e)
		{
			base.startResponse();
			
			if (AuthenticateIMUser())
			{
				writeElement("memberID", _MemberID.ToString());
			}
			else
			{
				writeElement("memberID", "INVALID");
			}

			base.endResponse();
		}

		/// <summary>
		/// Required Param. Returns session guid from querystring. SessionGUID is required for getMember.
		/// Note: Userplane is sometimes sending empty string for SessionGUID. This should not happen,
		/// but we need to handle it anyway.
		/// </summary>
		protected string SessionGUID
		{
			get 
			{
				if (_sessionGUID == Constants.NULL_STRING)
				{
					//get SessionGUID from querystring (required parameter)
					if(Request.QueryString[PARAM_SESSIONGUID] != null)
					{
						_sessionGUID = Request.QueryString[PARAM_SESSIONGUID];
						//SessionGUID can sometimes be an empty string we've seen
						if (_sessionGUID == string.Empty) 
						{
							_sessionGUID = Constants.NULL_STRING; //set to Null String, even though it's equivalent to string.Empty, just in case null string definition changes
						}
					}
					else
					{
						//show exception so we can know that UP is sending bad request... but continue onward
						g.ProcessException(new ApplicationException(string.Format("Querystring parameter {0} is required.",PARAM_SESSIONGUID)));
					}
				}
				return _sessionGUID;
			}
		}

		/// <summary>
		/// Tries to get a valid member id based on supplied SessionGUID.
		/// Returns false if session is invalid, blank or badly formed. 
		/// Returns true if sessionGUID supplied is still valid.
		/// MUST call this before accessing MemberID (or Member) in getMemberID.
		/// </summary>
		protected bool AuthenticateIMUser()
		{
			bool authenticated = false;
			if (SessionGUID != Constants.NULL_STRING) 
			{
				try 
				{
					//returns Constants.NULL_INT if session is invalid
					_MemberID = Matchnet.Session.ServiceAdapters.SessionSA.Instance.GetMemberID(SessionGUID);
				}
				catch 
				{
					//HACK: catch badly formed SessionGUID errors (such as incorrect number of characters and such)
					//Service should really return NULL_INT regardless of badly formed SessionGUID in my opinion.
					authenticated = false;
				}
			}
			
			if (_MemberID > 0) 
			{
				authenticated = true;
			}
			else 
			{
				//create an exception so that we can log this error. TODO: we need to limit the circumstances 
				//that this case can occur. Currently, authentication fails when
				//user closes main window, but leaves IM open, and IM app restarts on Userplane's end
				//which causes WebMessenger to attempt to re-authenticate, but session is no longer valid (see 16960, 16928, 16923).
				//By rights, authentication should never fail if a user is using our site.
				g.ProcessException(new ApplicationException("Userplane supplied invalid sessionGUID: " + SessionGUID + ". Either session expired, blank or malformed."));

			}
			return authenticated;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
