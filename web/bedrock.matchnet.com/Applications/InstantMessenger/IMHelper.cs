﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Applications.Email;
using Matchnet.Web.Framework;
namespace Matchnet.Web.Applications.InstantMessenger
{
    /// <summary>
    /// For certain business rules to execute, we need to run through the same logic at different places.  Use this class to write such code.
    /// </summary>
    public class IMHelper
    {
        /// <summary>
        /// Return true if the current member can reply to an IM request.
        /// The decision is made based on both the sender and the receiver. 
        /// </summary>
        /// <param name="receiver"></param>
        /// <param name="sender"></param>
        /// <param name="receiverBrand"></param>
        /// <returns></returns>
        public static bool CanMemberReplyIM(Matchnet.Member.ServiceAdapters.Member receiver,
            Matchnet.Member.ServiceAdapters.Member sender,
            Matchnet.Content.ValueObjects.BrandConfig.Brand receiverBrand, ContextGlobal g)
        {
            bool canReply = false;

            // If the feature (the original IM roadblock) is activated for this site
            if (bool.Parse(RuntimeSettings.GetSetting("IM_ROADBLOCK_UPGRADE", receiverBrand.Site.Community.CommunityID, receiverBrand.Site.SiteID, receiverBrand.BrandID)))
            {
                if (receiver.IsPayingMember(receiverBrand.Site.SiteID))
                    canReply = true; // If the receiver is a paying user, then go ahead and allow him to reply
                else if (sender.GetInsertDate(receiverBrand.Site.Community.CommunityID) <= DateTime.Parse(RuntimeSettings.GetSetting("IM_ROADBLOCK_CUTOFF_DATE", receiverBrand.Site.Community.CommunityID, receiverBrand.Site.SiteID, receiverBrand.BrandID)))
                {
                    // if grandfathering turned on, skip for members that joined before cut-off date
                    if (bool.Parse(RuntimeSettings.GetSetting("IM_ROADBLOCK_LEGACY_OK", receiverBrand.Site.Community.CommunityID, receiverBrand.Site.SiteID, receiverBrand.BrandID)))
                        canReply = true; // If the user is not a paying member but the sender is a member since before the cutoff date, same deal
                    else
                        canReply = false;
                }
                else
                    canReply = false; // For all other cases, prompt user to upgrade
            }
            else
            {
                // If the feature is not activated, revert to previous behavior
                canReply = true;
            }
            
            // Another IM block feature based on last sub purchase date
            if (Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_NON_SUB_IM_REPLY_BLOCK", receiverBrand.Site.Community.CommunityID, receiverBrand.Site.SiteID, receiverBrand.BrandID)))
            {
                canReply = false;
                if (receiver.IsPayingMember(receiverBrand.Site.SiteID))
                    canReply = true; // If the receiver is a paying user, then go ahead and allow him to reply
                else
                {
                    DateTime grandfatherDate = Conversion.CDateTime(RuntimeSettings.GetSetting("NON_SUB_IM_REPLY_BLOCK_GRANDFATHER_DATE", receiverBrand.Site.Community.CommunityID, receiverBrand.Site.SiteID, receiverBrand.BrandID));

                    // if the IM initiator has a SubscriptionLastInitialPurchaseDate value before our granfather
                    // date, we will let this IM go through, otherwise we direct the nonsub to our sub page.
                    int lastBrandID = Constants.NULL_INT;
                    sender.GetLastLogonDate(receiverBrand.Site.Community.CommunityID, out lastBrandID);
                    Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(lastBrandID);
                    DateTime subLastInitPurchaseDate = sender.GetAttributeDate(brand, "SubscriptionLastInitialPurchaseDateAfterLapse", DateTime.MaxValue);

                    if (subLastInitPurchaseDate > DateTime.MinValue && subLastInitPurchaseDate <= grandfatherDate)
                    {
                        canReply = true;
                    }
                }                
            }
            if (!canReply)
            {
                try
                {
                    if (VIPMailUtils.IsVIPEnabledSite(g.Brand))
                    {
                        int senderbrandid = g.Brand.BrandID;
                        sender.GetLastLogonDate(g.Brand.Site.Community.CommunityID, out senderbrandid);
                        Matchnet.Content.ValueObjects.BrandConfig.Brand senderbrand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(senderbrandid);
                        if (senderbrand != null)
                        {
                            if (VIPMailUtils.IsVIPMember(g, sender.MemberID, senderbrand))
                            {
                                canReply = true;
                            }

                        }


                    }
                }
                catch (Exception ex) { }

            }
            return canReply; 
        }
    }
}
