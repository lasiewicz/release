using System;
using System.Collections;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Web.Framework;
using Matchnet.Lib.Util;

namespace Matchnet.Web.Applications.MemberProfile
{
    public class CityList : FrameworkControl
    {
        private const int REGION_ID_ISRAEL = 105;
        private int _ParentRegionID = Constants.NULL_INT;
        protected System.Web.UI.WebControls.DataList resultList;
        protected System.Web.UI.WebControls.Literal litLetterList;
        private string _Description = Matchnet.Constants.NULL_STRING;
        private string _cityControlName = Matchnet.Constants.NULL_STRING;

        protected CityList()
        {
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                _ParentRegionID = Matchnet.Conversion.CInt(Request["ParentRegionID"]);
                _Description = Matchnet.Lib.Util.Util.CString(Request["Description"]);
                _cityControlName = Matchnet.Lib.Util.Util.CString(Request["CityControlName"]);
                StringBuilder script = new StringBuilder();
                script.Append(@"<script type='text/javascript'>");
                script.Append(@"function InsertCity(cityName, regionID){");
                script.Append(@"var elem=window.opener.document." + WebConstants.HTML_FORM_ID + ".elements['" + _cityControlName + "'];");
                script.Append(@"elem.value = cityName;");
                script.Append(@"if(elem.focus) {elem.focus();}");
                script.Append(@"window.close();}");
                script.Append(@"</script>");
                Page.RegisterStartupScript("closecityscript", script.ToString());

                litLetterList.Text = listLetters().ToString();

                // set first letter to be default
                if (_Description == Matchnet.Constants.NULL_STRING)
                {
                    if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
                    {
                        _Description = "\u05D0%";
                    }
                    else
                    {
                        _Description = Convert.ToString(Convert.ToChar(65) + "%");
                    }
                }
                populateGrid();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private StringBuilder listLetters()
        {
            StringBuilder list = new StringBuilder();
            RegionCollection myRegions = RegionSA.Instance.RetrieveChildRegions(_ParentRegionID, _g.Brand.Site.LanguageID);

            // MPR-385 --> French letter E with apostrophe is treated the same as regular E can't rely on last letter anymore
            ArrayList letterList = new ArrayList();

            foreach (Matchnet.Content.ValueObjects.Region.Region region in myRegions)
            {
                string curLetter = region.Description.Substring(0, 1).ToLower();
                if (!letterList.Contains(curLetter))
                {
                    string navURL = "/Applications/MemberProfile/CityList.aspx?CityControlName=" + _cityControlName + "&ParentRegionID=" + _ParentRegionID.ToString() + "&Description=" + curLetter.ToUpper() + "%";
                    //if (g.IsSecureRequest())
                    //{
                    //    navURL = "https://" + Request.Url.Host + navURL;
                    //}

                    list.Append(" " + Framework.FrameworkGlobals.Link(curLetter.ToUpper(), navURL, "") + " ");
                    letterList.Add(curLetter);
                }
            }
            return list;
        }

        private void populateGrid()
        {
            RegionCollection myRegions = RegionSA.Instance.RetrieveChildRegions(_ParentRegionID, _g.Brand.Site.LanguageID, _Description);
            resultList.RepeatColumns = 4;
            resultList.DataSource = myRegions;
            resultList.DataBind();
        }

        private void getCityLink(object sender, DataListItemEventArgs e)
        {
            HyperLink temp = (HyperLink)e.Item.FindControl("cityLink");
            temp.NavigateUrl = "javascript:InsertCity(\"" +
                Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Description")) + "\"," +
                Convert.ToString(DataBinder.Eval(e.Item.DataItem, "RegionID")) + ")";
        }

        public string TextDirection
        {
            get { return g.Brand.Site.Direction.ToString(); }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            this.resultList.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.getCityLink);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
