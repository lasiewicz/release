<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ProfileUnavailable.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileUnavailable" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../../Framework/Ui/PageElements/NoPhoto.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto20" Src="../../Framework/Ui/PageElements/NoPhoto.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div id="rightNew" class="page-container">
    <asp:PlaceHolder ID="phSite20Nav" runat="server" Visible="false">
        <!--new site 20 nav, new css file-->
        <div class="pagination-buttons clearfix image-text-pair">
            <div class="float-inside"> 
		        <asp:HyperLink CssClass="btn link-secondary small" ID="lnkBacktoResults" runat="server">
		            <span class="direction"><mn:Txt runat="server" ResourceConstant="NAV_BACK_DIRECTION2" /></span>
		            <span class="back">
		                <mn2:FrameworkLiteral id="literalBacktoResults" ResourceConstant="TXT_BACK_TO_RESULTS" runat="server"></mn2:FrameworkLiteral>
		                <mn2:FrameworkLiteral id="literalMoreMatches" ResourceConstant="TXT_SHOW_MORE_MATCHES" runat="server" Visible="false"></mn2:FrameworkLiteral>
		            </span>
		        </asp:HyperLink>
	        </div>
	        <div class="float-outside">
	            <asp:HyperLink CssClass="btn link-secondary small" ID="lnkPrev" Runat="server" Visible="False">
	                <span class="direction">&#9668;</span><span class="prev"><mn2:FrameworkLiteral id="literalPrevProfile" ResourceConstant="TXT_PREV_PROFILE" runat="server"></mn2:FrameworkLiteral></span>
	            </asp:HyperLink>&nbsp;
	            <asp:HyperLink CssClass="btn link-secondary small" ID="lnkNext2" Runat="server" Visible="False">
		            <span class="next"><mn2:FrameworkLiteral id="literalNextProfile2" ResourceConstant="TXT_NEXT_PROFILE" runat="server"></mn2:FrameworkLiteral></span><span class="direction"><mn:Txt runat="server" ResourceConstant="NAV_NEXT_DIRECTION" /></span>
	            </asp:HyperLink>&nbsp;
	        </div>
        </div>
    </asp:PlaceHolder>
	<asp:PlaceHolder ID="phNextProfile" Runat="server" Visible="False">
	    <!-- current nav, current css file-->
		<div class="nextProfile">
			<asp:HyperLink ID="lnkNext" Runat="server">
			<mn2:FrameworkLiteral id="literalNextProfile" ResourceConstant="TXT_NEXT_PROFILES" runat="server"></mn2:FrameworkLiteral>
		</asp:HyperLink>
		</div>
	</asp:PlaceHolder>
	
	<!--text block, new or current css file-->
	<h1><asp:Literal Runat="server" ID="ltlMemberName"/><mn:txt id="txtProfileUnavailableWho" runat="server" ResourceConstant="TXT_PROFILE_UNAVAILABLE_WHO" /></h1>
	<div class="rbox-style-clear" id="profile-unavailable">
	    <mn:Txt id="txtProfileUnavailableTipsOneBold" runat="server" ResourceConstant="TXT_PROFILE_UNAVAILABLE_LIST" />
	</div>
</div>
