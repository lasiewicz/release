﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnePageCompleteProfile1.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.OnePageCompleteProfile1" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="EditLocation" Src="/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditLocation.ascx" %>
<%= GetForcedPageCode("Start")%>
<asp:PlaceHolder runat="server" ID="plcCallToAction2" Visible="False">
    <div id="profile-overlay-content">
        <div id="title">
            הרשמתך נקלטה. תודה.
        </div>
</asp:PlaceHolder>

<div id="complete-profile">
    <mn:Txt ID="Txt9" runat="server" CssClass="box-title" ResourceConstant="TXT_COMPLETE_PROFILE_HEADER" />
    <mn:Txt ID="Txt10" runat="server" ResourceConstant="TXT_DETAILS_SAVED" />
    <asp:PlaceHolder ID="plcRegionReligion"  Visible="False" runat="server">
        <div id="top-row">
            <div class="Country send-attribute">
                <div class="item-title item-title-select">
                    <mn:EditLocation ID="EditLocation" runat="server"></mn:EditLocation>
                </div>
            </div>

            <div id="religion-wrapper" class="">
                <div class="item-title item-title-select">
                    <mn:Txt runat="server" ID="Txt21" ResourceConstant="TXT_JDATE_RELIGION" />
                </div>
                <div class="Religion send-attribute" key="ATTR_JDateReligion">
                    <select>
                        <%=GetListItems("JDateReligion", "select")%></ul>
                    </select>
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
    <div id="first-row">
        <div class="item-title">
            <mn:Txt runat="server" ID="Txt7" ResourceConstant="TXT_RELATIONSHIP_STATUS" />
        </div>
        <div class="item-title">
            <mn:Txt runat="server" ID="Txt20" ResourceConstant="TXT_KEEP_KOSHER" />
        </div>
        <div class="item-title">
            <mn:Txt runat="server" ID="Txt2" ResourceConstant="TXT_SMOKING_HABITS" />
        </div>
        <div class="item-title">
            <mn:Txt runat="server" ID="Txt3" ResourceConstant="TXT_DRINKING_HABITS" />
        </div>
        <div class="RelationshipStatus send-attribute" key="ATTR_MaritalStatus">
            <ul>
                <%=GetListItems("MaritalStatus")%>
            </ul>
        </div>
        <div class="KeepKosher send-attribute" key="ATTR_KeepKosher">
            <ul>
                <%=GetListItems("KeepKosher")%>
            </ul>
        </div>
        <div class="SmokingHabits send-attribute" key="ATTR_SmokingHabits">
            <ul>
                <%=GetListItems("SmokingHabits")%>
            </ul>
        </div>
        <div class="DrinkingHabits send-attribute" key="ATTR_DrinkingHabits">
            <ul>
                <%=GetListItems("DrinkingHabits")%>
            </ul>
        </div>
    </div>
    <div id="second-row">
        <div class="item-title">
            <mn:Txt runat="server" ID="Txt8" ResourceConstant="TXT_CHILDREN_COUNT" />
        </div>
        <div class="item-title">
            <mn:Txt runat="server" ID="Txt4" ResourceConstant="TXT_ETHNICITY" />
        </div>
        <div class="item-title">
            <mn:Txt runat="server" ID="Txt5" ResourceConstant="TXT_EDUCATION_LEVEL" />
        </div>
        <div class="item-title">
            <mn:Txt runat="server" ID="Txt6" ResourceConstant="TXT_HEIGHT" />
        </div>
        <div class="ChildrenCount send-attribute" key="ATTR_ChildrenCount">
            <ul>
                <%=GetListItems("ChildrenCount")%>
            </ul>
        </div>
        <div class="JDateEthnicity send-attribute" key="ATTR_JDateEthnicity">
            <ul>
                <%=GetListItems("JDateEthnicity")%>
            </ul>
        </div>
        <div class="EducationLevel send-attribute" key="ATTR_EducationLevel">
            <ul>
                <%=GetListItems("EducationLevel")%>
            </ul>
        </div>
        <div class="Height send-attribute" key="ATTR_Height">
            <ul>
                <%=GetListItems("Height")%>
            </ul>
        </div>
    </div>
    <asp:PlaceHolder ID="plcSaveDetailsWithImg" runat="server" Visible="false">
        <mn:Image ID="Image1" runat="server" FileName="save-details-with-hearts-bt.png" TitleResourceConstant="ALT_SAVE_DETAILS" />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcSaveDetailsWithLnk" runat="server">
        <div id="send-attr-btn">
            <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_SEND_ATTR_BTN" />
        </div>
    </asp:PlaceHolder>
</div>
<asp:PlaceHolder runat="server" ID="plcCallToAction" Visible="False">
    <div id="leftbox-1" class="box-content">
        <mn:Txt ID="Txt11" runat="server" ResourceConstant="TXT_SUBSCRIBE_BOX" />
        <div class="disabled-matches-btn">
            <mn:Txt ID="Txt12" runat="server" ResourceConstant="TXT_SUBSCRIBE_BTN" />
        </div>
        <div class="enabled-matches-btn">
            <a id="subscribe-btn" href="/Applications/Subscription/Subscribe.aspx?prtid=41">
                <mn:Txt ID="Txt13" runat="server" ResourceConstant="TXT_SUBSCRIBE_BTN" />
            </a>
        </div>
    </div>
    <div id="leftbox-2" class="box-content">
        <div class="newtext-matches-btn">
            <mn:Txt ID="Txt14" runat="server" ResourceConstant="TXT_BACK_TO_SITE_BTN" />
        </div>
        <div class="disabled-matches-btn">
            <mn:Txt ID="Txt15" runat="server" ResourceConstant="TXT_BACK_TO_SITE_BTN" />
        </div>
        <div class="enabled-matches-btn">
            <asp:HyperLink runat="server" ID="lnkContinue">
                <mn:Txt ID="Txt16" runat="server" ResourceConstant="TXT_BACK_TO_SITE_BTN" />
            </asp:HyperLink>
        </div>
    </div>
    </div> </asp:PlaceHolder>
<%= GetForcedPageCode("End") %>


<script type="text/javascript">
    //Location related
    $j('#' + EditLocationObj.ddlEditCountry).change(function () {
        //alert('hi from region country change');
        EditLocation_CountryChanged("", "");
        return false;
    });


    $j('#' + EditLocationObj.txtEditZipCode).keyup(function () {
        EditLocation_ZipCodeChanged();
        return false;
    });

    $j('#' + EditLocationObj.ddlEditState).change(function () {
        var newStateRegionID = $j('#' + EditLocationObj.ddlEditState).val();

        $j('#' + EditLocationObj.txtEditCity).val('');
        EditLocation_LoadCityAutoComplete(newStateRegionID, "");

        return false;
    });

    $j('#' + EditLocationObj.lnkCityLookup).click(function () {
        try {
            var parentRegionID = $j('#' + EditLocationObj.ddlEditCountry).val();
            if ($j('#' + EditLocationObj.ddlEditState).is(':visible')) {
                parentRegionID = $j('#' + EditLocationObj.ddlEditState).val();
            }
            window.open('/Applications/MemberProfile/CityList.aspx?plid=' + EditLocationObj.BrandID
                + '&CityControlName=' + EditLocationObj.txtEditCity + '&ParentRegionID=' + parentRegionID, '', 'height=415px,width=500px,scrollbars=yes');
        }
        catch (e) {
            if (EditLocationObj.IsVerbose) { alert(e); }
        }
        return false;
    });

    function EditLocation_CountryChanged(preSelectedStateRegionID, preSelectedCityName) {
        var newCountryRegionID = $j('#' + EditLocationObj.ddlEditCountry).val();

        if (newCountryRegionID == "38" || newCountryRegionID == "223") {
            //USA or Canada enters in zip code for profile
            $j('#' + EditLocationObj.txtEditZipCode).val('');
            var ddlCityForZip = $j('#' + EditLocationObj.ddlEditCityForZip);
            ddlCityForZip.empty();
            $j('#' + EditLocationObj.PanelEditByZipCode).show();
            $j('#' + EditLocationObj.pnlEditByCityForZip).hide();
            $j('#' + EditLocationObj.PanelEditByStateCity).hide();

        }
        else {
            $j('#' + EditLocationObj.PanelEditByZipCode).hide();
            $j('#' + EditLocationObj.pnlEditByCityForZip).hide();
            $j('#' + EditLocationObj.PanelEditByStateCity).show();

            //get new state list, if applicable
            $j.ajax({
                type: "POST",
                url: "/Applications/API/RegionService.asmx/GetRegionStates",
                data: "{countryRegionId:" + newCountryRegionID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var regionResult = (typeof result.d == 'undefined') ? result : result.d;
                    if (regionResult.Status == 2) {
                        var divState = $j('#' + EditLocationObj.PanelEditState);
                        var ddlState = $j('#' + EditLocationObj.ddlEditState);
                        ddlState.empty();

                        $j.each(regionResult.StatesList, function (key, value) {
                            $j("<option>").attr("value", key).text(value).appendTo(ddlState);
                        });

                        if (regionResult.hasStates) {
                            divState.show();
                            if (preSelectedStateRegionID != "") {
                                ddlState.val(preSelectedStateRegionID);
                            }
                            EditLocation_LoadCityAutoComplete(ddlState.val(), "");
                        } else {
                            divState.hide();
                            EditLocation_LoadCityAutoComplete(newCountryRegionID, "");
                        }

                        if (preSelectedCityName != "") {
                            $j('#' + EditLocationObj.txtEditCity).val(preSelectedCityName);
                        }
                        else {
                            $j('#' + EditLocationObj.txtEditCity).val('');
                        }

                        //$j('#errorMessage').hide();
                    }
                    else {
                        //$j('#errorMessage').show().html(regionResult.StatusMessage);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (EditLocationObj.IsVerbose) { alert(textStatus); }
                }
            });
        }
    }

    function EditLocation_ZipCodeChanged() {
        var newZipCode = $j('#' + EditLocationObj.txtEditZipCode).val();
        var newCountryRegionID = $j('#' + EditLocationObj.ddlEditCountry).val();

        var divCityForZip = $j('#' + EditLocationObj.pnlEditByCityForZip);

        //Set cities if Canada postal code is at least 6 or USA and postal code is at least 5
        if ((newCountryRegionID == "38" && newZipCode.length >= 6) || (newCountryRegionID == "223" && newZipCode.length >= 5)) {
            var zipCodeData = "{zipCode:'" + newZipCode + "'}";
            //get new City for Zip list, if applicable
            $j.ajax({
                type: "POST",
                url: "/Applications/API/RegionService.asmx/GetCities",
                data: zipCodeData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var regionResult = (typeof result.d == 'undefined') ? result : result.d;

                    var ddlCityForZip = $j('#' + EditLocationObj.ddlEditCityForZip);
                    ddlCityForZip.empty();

                    regionResult = $j.parseJSON(regionResult);

                    var i = 0;
                    $j.each(regionResult, function (index, element) {
                        i = i + 1;
                        $j("<option>").attr("value", element.RegionID).text(element.Description).appendTo(ddlCityForZip);
                    });

                    //Display the City for Zip dropdown if more than one value:
                    if (i > 1) {
                        //                      ddlCityForZip.prepend("<option value='' selected='selected'></option>");  //Only need this if defaulting to no value
                        divCityForZip.show();
                    }
                    else {
                        divCityForZip.hide();
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (EditLocationObj.IsVerbose) { alert(textStatus); }
                }
            });

        }
        else {
            divCityForZip.hide();
        }
    }

    function EditLocation_LoadCityAutoComplete(parentRegionID, description) {
        //get new city list
        $j.ajax({
            type: "POST",
            url: "/Applications/API/RegionService.asmx/GetRegionCities",
            data: "{parentRegionID:" + parentRegionID + ", description: \"" + description + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                var regionResult = (typeof result.d == 'undefined') ? result : result.d;
                if (regionResult.Status == 2) {
                    if (regionResult.CityList != null) {
                        $j('#' + EditLocationObj.txtEditCity).autocomplete({
                            source: regionResult.CityList,
                            minLength: 3
                        });

                        $j('.ui-autocomplete').click(function (e) {
                            e.stopPropagation();
                        });
                    }

                    //$j('#errorMessage').hide();
                }
                else {
                    //$j('#errorMessage').show().html(regionResult.StatusMessage);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (EditLocationObj.IsVerbose) { alert(textStatus); }
            }
        });
    }

    //prepopulate initial autocomplete for city
    if (EditLocationObj.AccountCountryRegionID != "" && EditLocationObj.AccountCountryRegionID != "38" && EditLocationObj.AccountCountryRegionID != "223") {
        if (EditLocationObj.InitialCityParentRegionID != "") {
            EditLocation_LoadCityAutoComplete(EditLocationObj.InitialCityParentRegionID, "");
        }
    }

</script>
