﻿using System;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.MemberProfile
{
    public partial class RegistrationWelcomeContent : FrameworkControl
    {
        private bool _raiseOmnitureEvent39 = false; // JDate Mobile registration error event

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                setupForPopupProfile();
                showFiveDFTButton();
                
                if (g.IsSiteMingle)
                {
                    pnlFirstLink.Visible = false;
                    pnlFirstLinkMingle.Visible = true;
                }
                else
                {
                    pnlFirstLink.Visible = true;
                    pnlFirstLinkMingle.Visible = false;
                }

                if (SettingsManager.GetSettingBool(SettingConstants.ENABLE_COLORCODE_REGISTRATION, g.Brand))
                {

                    phColorCode.Visible = true;
                    phMatches.Visible = false;
                    MemberQuiz _memberquiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);
                    g.AddExpansionToken("MEMBER_COLORCODE", _memberquiz.PrimaryColor.ToString());
                    regColorCodeResults.LoadReport(_memberquiz, ReportType.none);
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                // JDate Mobile registration error event
                if (_raiseOmnitureEvent39)
                {
                    _g.AnalyticsOmniture.AddEvent("event39");
                }
            }

            base.OnPreRender(e);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        void uc2MobilePhoneSelector_messmoSubscriptionRequestEvent(bool subscriptionAttemptMade, bool subscriptionSuccess, string notificationResourceConstant)
        {
            if (subscriptionAttemptMade && subscriptionSuccess)
            {
                // Set this here so that when the member lands on MobileSettings page event31 will be rendered.
                g.Session.Add(WebConstants.SESSION_MOBILE_SETUP_COMPLETE, 1, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                g.Session.Add(WebConstants.SESSION_MOBILE_SETUP_SOURCE, "Mobile Setup:Registration Confirmation", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                g.Transfer("/Applications/Mobile/MobileSettings.aspx");
            }
            else
            {
                _raiseOmnitureEvent39 = true;
            }
        }

        private void setupForPopupProfile()
        {
            if (g.LayoutTemplate == LayoutTemplate.PopupProfile)
            {
                // btnContinue.Text = g.GetResource("CONTINUE", this);
                // txtSearchLink.Visible = false;
            }
        }

        private void showFiveDFTButton()
        {
            lnkFiveDFT.NavigateUrl = "/Applications/Subscription/FreeTrialSubscribe.aspx?ftg=1&prtid=" + (Int32)Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.FiveDayFreeTrialReg;
            if (Conversion.CInt(Request[WebConstants.URL_PARAMETER_NAME_FIVEDFT]) == 1)
            {
                lnkFiveDFT.Visible = true;
            }
        }
    }
}