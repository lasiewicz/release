﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MemberPhotoSingleUpload.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.MemberPhotoSingleUpload" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn1" TagName="TopAuxNav" Src="/Framework/Ui/HeaderNavigation20/TopAuxNav.ascx" %>

<div id="site-container">
<div id="min-max-container">
    <script type="text/javascript">
    function blockSubmits() {
        $j('#photo-save-button').block({
            message: null,
            overlayCSS:  {backgroundColor: '#fff',opacity:0.8,cursor: 'default'}
        });
    }

    </script>

    <mn1:TopAuxNav id="topAuxNav" runat="server" />

<div id="content-container">

<div id="nav"><ul class="sf-navbar sf-menu sf-js-enabled"></ul></div>

<div id="page-container">

    <div id="pics-forced-main">
        <mn:Title runat="server" id="ttlOnlineNow" ResourceConstant="TXT_PHOTO_UPLOAD_HEADER" />
        <mn:txt runat="server" id="txtPhotoUpload" ResourceConstant="TXT_PHOTO_UPLOAD" />
        
        <div id="pics-forced-instructions" class="border-gen">
            <div id="divControl" runat="server">

            
               
                <uc2:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
                
                <h2><mn:txt runat="server" id="txtCaption" ResourceConstant="MAIN_CAPTION" /></h2>
                <mn:txt id="txtPhotoUploadInstructions" runat="server" resourceconstant="TXT_PHOTO_UPLOAD_INSTRUCTIONS" expandimagetokens="true" />

                <div id="photo-upload-control">
                    <anthem:FileUpload ID="fileupload" runat="server" AutoUpdateAfterCallBack="false" />
                </div>
                
                <div id="photo-save-button">
                    <asp:LinkButton Runat="server" ID="btnSaveChanges" OnClick="btnUpload_OnClick"><mn:image id="btnSaveImage" runat="server" filename="btn-save-changes.png" resourceconstant="ALT_CLICK_TO_SUBMIT" /></asp:LinkButton>
                </div>
            
             
            </div>

        <script type="text/javascript">
            blockSubmits();
            $j(document).ready(function() {
                $j('#photo-upload-control input').change(function() {
                    $thisValue = $j(this).val();
                    if ($thisValue != "") {
                        $j('#photo-save-button').unblock();
                    }
                });

            });
        </script>

    </div>
        <div id="pics-forced-guidelines">
            <mn:txt runat="server" id="txtPhotoGuideliens" ResourceConstant="TXT_PHOTO_GUIDELINES" />
        </div>
    </div>
    
    <div id="pics-forced-cs">
        <mn:txt runat="server" id="txtCS" ResourceConstant="TXT_CS" />    
    </div>
    
</div>
</div>

</div>
    <div id="footer">
    <div id="footer-container">
        <mn1:Footer20 ID="Footer20" Runat="server" />
    </div>
</div>