﻿using System;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileData;
using Matchnet.Web.Framework;
using Spark.Common.AccessService;

namespace Matchnet.Web.Applications.MemberProfile
{
    public partial class OnePageCompleteProfile1 : FrameworkControl
    {
        private bool _isForcedPage;

        public OnePageCompleteProfile1()
        {
            _isForcedPage = false;
        }

        public OnePageCompleteProfile1(bool isForcedPage)
        {
            _isForcedPage = isForcedPage;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["overlayreg"]))
            {
                plcCallToAction.Visible = true;
                plcCallToAction2.Visible = true;
                lnkContinue.NavigateUrl = g.GetDestinationURL();
            }

            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                plcRegionReligion.Visible = true;

                var profileDataItem = new ProfileDataItem();
                profileDataItem.ResourceConstant = "TXT_LOCATION";
                profileDataItem.AttributeName = "RegionID";
                profileDataItem.EditControlType = EditControlTypeEnum.Custom;
                profileDataItem.EditDataType = EditDataTypeEnum.Int;
                var profileDataGroup = new ProfileDataGroup();
                profileDataGroup.EditRefreshIncludeGroupType = null;
                EditLocation.LoadEditControl(profileDataGroup, null, profileDataItem, "OnePageCompleteProfile_Obj", this);
            }

            //Omniture
            if (SettingsManager.GetSettingBool("REGISTRATION_USE_NEW_SITE", g.Brand) &&
                (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateUK || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR))
            {
                _g.AnalyticsOmniture.PageName = "More Reg Info";
            }
        }

        protected string GetListItems(string attributeName, string elementType = "ul")
        {
            StringBuilder sb = new StringBuilder();

            ListItemCollection collection = null;
            if (attributeName.ToLower() == "height")
            {
                collection = FrameworkGlobals.CreateHeightListItems(g, false);
            }
            else
            {
                bool skipBlanks = attributeName.ToLower() == "jdatereligion";
                collection = GetAttributeOptionCollectionAsListItems(g, null, skipBlanks, attributeName);
            }

            if (collection != null)
            {
                int selectedValue = g.Member.GetAttributeInt(g.Brand, attributeName, Constants.NULL_INT);

                foreach (ListItem li in collection)
                {
                    bool alreadySelected = false;

                    if (!string.IsNullOrEmpty(li.Value))
                    {
                        string selectedClass = string.Empty;
                        if (!alreadySelected && selectedValue != Constants.NULL_INT && selectedValue == int.Parse(li.Value))
                        {
                            alreadySelected = true;
                        }

                        if (elementType == "ul")
                        {
                            if(alreadySelected)
                                selectedClass = "class=\"selected\"";

                            sb.Append("\t<li value=\"" + li.Value + "\" " + selectedClass + ">");
                            sb.Append(li.Text);
                            sb.AppendLine("</li>");
                        }
                        else if (elementType == "select")
                        {
                            if (alreadySelected)
                                selectedClass = "selected=\"selected\"";

                            sb.Append("\t<option value=\"" + li.Value + "\" " + selectedClass + ">");
                            sb.Append(li.Text);
                            sb.AppendLine("</option>");
                        }
                    }
                }
            }

            return sb.ToString();
        }

        private ListItemCollection GetAttributeOptionCollectionAsListItems(ContextGlobal g, FrameworkControl resourceControl, bool skipBlanks, string AttributeName)
        {
            ListItemCollection items = new ListItemCollection();

            bool useList = false;

            if (AttributeName != Constants.NULL_STRING)
            {
                AttributeOptionCollection attOptions = AttributeOptionSA.Instance.GetAttributeOptionCollection(AttributeName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

                foreach (AttributeOption option in attOptions)
                {
                    if (option.Value <= Constants.NULL_INT && skipBlanks)
                    {
                        continue;
                    }

                    ListItem item = new ListItem();
                    item.Value = option.Value.ToString();
                    item.Text = g.GetTargetResource(option.ResourceKey, BrandConfigSA.Instance.GetBrandByID(g.Brand.BrandID));

                    items.Add(item);
                }
            }

            return items;
        }

        protected string GetForcedPageCode(string location)
        {
            string result = string.Empty;

            if (_isForcedPage)
            {
                if (location == "Start")
                {
                    result = "<div class=\"bg-cover-fill-details\">&nbsp;</div><div class=\"fill-details-box\">";
                }
                else
                {
                    result = "</div>";
                }
            }

            return result;
        }

        public static bool ShowProfileCompleteOverlay(ContextGlobal g)
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["ForceProfileCompleteOverlay"]))
                return true;

            bool showOverlay = false;

            if (Boolean.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                                      "ENABLE_PROFILE_COMPLETE_FORCED_PAGE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
            {
                if (g.Member.GetAttributeInt(g.Brand, "MaritalStatus", Constants.NULL_INT) == Constants.NULL_INT ||
                    g.Member.GetAttributeInt(g.Brand, "KeepKosher", Constants.NULL_INT) == Constants.NULL_INT ||
                    g.Member.GetAttributeInt(g.Brand, "SmokingHabits", Constants.NULL_INT) == Constants.NULL_INT ||
                    g.Member.GetAttributeInt(g.Brand, "DrinkingHabits", Constants.NULL_INT) == Constants.NULL_INT ||
                    g.Member.GetAttributeInt(g.Brand, "ChildrenCount", Constants.NULL_INT) == Constants.NULL_INT ||
                    g.Member.GetAttributeInt(g.Brand, "JDateEthnicity", Constants.NULL_INT) == Constants.NULL_INT ||
                    g.Member.GetAttributeInt(g.Brand, "EducationLevel", Constants.NULL_INT) == Constants.NULL_INT ||
                    g.Member.GetAttributeInt(g.Brand, "Height", Constants.NULL_INT) == 0)
                {
                    showOverlay = true;
                }
            }

            return showOverlay;
        }
    }
}