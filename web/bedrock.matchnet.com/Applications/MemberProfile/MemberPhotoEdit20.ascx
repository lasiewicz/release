﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MemberPhotoEdit20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.MemberPhotoEdit20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../../Framework/Ui/PageElements/NoPhoto20.ascx" %>
<%@ Register TagPrefix="mlft" TagName="MemberLikeAnswerClientModule" Src="~/Applications/MemberLike/MemberLikeAnswerClientModule.ascx" %>


<div id="page-container">


<script type="text/javascript">

//<![CDATA[
/* jquery  scripts for d and d */
var photoResourceArray = new Array();
photoResourceArray[0] = "<asp:Literal runat='server' id='litPhoto1'/>";
photoResourceArray[1] = "<asp:Literal runat='server' id='litPhoto2'/>";
photoResourceArray[2] = "<asp:Literal runat='server' id='litPhoto3'/>";
photoResourceArray[3] = "<asp:Literal runat='server' id='litPhoto4'/>";

var tabResourceArray = new Array();
tabResourceArray[0] = "<asp:Literal runat='server' id='litTab1'/>";
tabResourceArray[1] = "<asp:Literal runat='server' id='litTab2'/>";
tabResourceArray[2] = "<asp:Literal runat='server' id='litTab3'/>";
tabResourceArray[3] = "<asp:Literal runat='server' id='litTab4'/>";

var deleteMsg =  "<asp:Literal runat='server' id='litDeleteMessage'/>";
var savingMsg = "<asp:Literal runat='server' id='litSavingMessage'/>";
var loadingMsg = "<asp:Literal runat='server' id='litLoadingMessage'/>";
var defaultCaption = "<asp:Literal runat='server' id='litDefaultCaption'/>";
var confirmationMsg= "<asp:Literal runat='server' id='litConfirmationMsg'/>";
var prefixHeroTextAreaMsg = "<asp:Literal runat='server' id='litPrefixHeroTextAreaMsg'/>";
var captionCharLimit = 100;
var photoLimit = <%=MaxPhotoCount.ToString()%>;

var isMainPhotoSet = false;

var needToConfirm = false;
var deleteClickEvent = false;
window.onbeforeunload = confirmExit;

var currentHiddenTextArea = null;

function clearFileInputField(tagId) {
    document.getElementById(tagId).innerHTML = document.getElementById(tagId).innerHTML;
} 

function confirmExit() {
   //debugger;
       if (needToConfirm) {
          return confirmationMsg;
      }
  }

function limitChars(e, limit){
    var text = $j(e).val(); 
    var textlength = text.length;
    //alert(textlength);
    if(textlength > limit) {
        //$j('#' + infodiv).html('You cannot write more then '+limit+' characters!');
        $j(e).val(text.substr(0,limit));
        return false;
    }
    else {
        //$j('#' + infodiv).html('You have '+ (limit - textlength) +' characters left.');
        return true;
    }
}
function tableSerialize(e) {
    var order = $j(e).tableDnDSerialize();
    //alert(order);
    return order;
}
function blockSubmits() {
    $j('.pics-management-button-container').block({
        message: null,
        overlayCSS:  {backgroundColor: '#fff',opacity:0.8,cursor: 'default'}
    });
}
function blockTable(messageContent, blocker) {
    $j("#page-container #pics-management-blocker-content h2").text(messageContent);
    if (blocker == true){
        $j('#page-container #pics-management-blocker, #page-container #pics-management-blocker-content').show();
    } else {
        $j('#page-container #pics-management-blocker, #page-container #pics-management-blocker-content').hide();
    }
}
function browseButtonTableConfig() {
    $j('#pics-management tbody tr.pic-empty .browse').each(function() {
        $thisValue = $j(this).val();
        if ($thisValue != ""){
            $j(this).parents('tr').removeClass('nodrag nodrop pic-empty').addClass('pic-active upload');
            $j(this).parents('tr').find('td.td-move, td.td-photo, td.td-status, td.td-delete').addClass('drag');
            $j(this).parents('tr').find('a.upload-clear').css({display: "block"});
        }
    });
    
    // re-initialize the table
    initializeTable();
    $j.fn.photoPropUpdate(false, true, true);
}

function facebookButtonToggler(){
    //okay, hide all of them
    $j('#pics-management tbody tr').find('.pic-upload-facebook').hide();

    //show the one in the first empty slot
    $j('#pics-management tbody tr.pic-empty:first').find('.pic-upload-facebook').show();

    //make sure only one facebook button is visible by hiding all but the first one
    $j('.pic-upload-facebook').filter(function(index){
        return $j(this).css('display') === "block";
    }).filter(':gt(0)').hide();
}

function uploadGuidelinesLayer(onQ){

    var $picUpload = $j('#pics-management tbody tr');
    var $guide = $j('.pics-management-guidlines-layer');
    var slotsFull = photoLimit === $j('#pics-management tbody tr.pic-active').length ? true : false;

    if (onQ == true){
        $j('#pics-management tbody tr.pic-empty:first').find('.td-caption').append($guide);
        $j($guide).css({display: "block"});
    } else {
        $j($picUpload).each(function() {
            if ($j(this).hasClass('upload')) {
                $j(this).find('.pics-management-guidlines-layer').remove();
                $j(this).next().find('.td-caption').append($guide);
                $j($guide).css({display: "block"});
            }
        });
    }

    //hide the help guide layer if all slots are full. this is not ideal, but it fixes the bug where the guide layer shows up after all slots
    if(slotsFull){
       $guide.hide();
    }
}

jQuery.fn.photoPropUpdate = function(initialize, edited, reordered) {
    //   debugger;
    if (reordered == true) {
        // Update status
        var $picsManStatus = $j(".status", "#pics-management tbody");

        $j($picsManStatus).each(function(intIndex) {
            intIndex++;
            var $this = $j(this);
            switch (intIndex) {
                case 1: $this.html("<h3>" + photoResourceArray[0] + "</h3>" + tabResourceArray[0] + ""); break;
                case 2: $this.html("<h3>" + photoResourceArray[1] + "</h3>" + tabResourceArray[1] + ""); break;
                case 3: $this.html("<h3>" + photoResourceArray[2] + "</h3>" + tabResourceArray[2] + ""); break;
                case 4: $this.html("<h3>" + photoResourceArray[3] + "</h3>" + tabResourceArray[3] + ""); break;
                default: $this.html("<h3><mn:txt runat='server' resourceconstant='HDR_PHOTO' /> " + (intIndex) + "</h3>");
            }
        });

        //Keep style consistent with primary slots
        $j("#pics-management tbody tr").removeClass('pic-primary').filter(':lt(4)').addClass('pic-primary');

        //keep move images in proper slots

        
        $j("#pics-management tbody tr.pic-active:first img.move-up, #pics-management tbody tr.pic-active:last img.move-down").css({visibility: 'hidden'});
        $j("#pics-management tbody tr.pic-active:not(:first) img.move-up, #pics-management tbody tr.pic-active:not(:last) img.move-down").css({visibility: 'visible'});
        
        
       
      
    }
    if (edited == true) {
       
        $j(".pics-management-edited").css({ visibility: 'visible' });
        
        var $notifDisplay = $j(".PageMessage.notification").css('display');
        if($notifDisplay != "none"){
            //FF and IE are throwing exceptions when 'blind' is used.
            try {
                $j(".PageMessage.notification").hide('blind', 1000);
            } catch(e) {
                $j(".PageMessage.notification").hide();
            }
        }

        //unblock submit
        $j(".pics-management-button-container").unblock();

        //block Facebook button
        var $picUploadFacebook = $j('#pics-management tbody tr.pic-empty:first').find('.pic-upload-facebook');
        var picUploadFacebookBlocked = $picUploadFacebook.data('blockUI.isBlocked');

        if(!picUploadFacebookBlocked){
            $j('#pics-management tbody tr.pic-empty:first').find('.pic-upload-facebook').block({
                message: null,
                overlayCSS:  {backgroundColor: '#fff',opacity:0.7,cursor: 'default'}
            }).attr('title', spark.facebook.photoImportMsgSaveBefore);
        }
    }
    // re-initialize the table
    if (initialize == true) {
        initializeTable();
    }
    needToConfirm = edited;
}

//initialize table
function initializeTable() {

    var dataOnDragStart = '';
    var $pics = $j("#pics-management tbody");
    $j($pics).tableDnD({
        dragHandle: "drag",
        onDragClass: "pics-management-drag",
        onDragStart: function(table, row) {
            dataOnDragStart = $j.tableDnD.serialize();
        },
        onDrop: function(table, row) {
            var dataOnOpen = $j.tableDnD.serialize();
 
            if(dataOnDragStart != dataOnOpen){
                $j.fn.photoPropUpdate(false, true, true);
            }
        }
    });
}

$j(document).ready(function() {

    var $pics = $j("#pics-management tbody");

    /* updates on page load */
    $j("#pics-management tbody tr.pic-active:first a.move-up, #pics-management tbody tr.pic-active:last a.move-down").css({visibility: 'hidden'});
    $j("#pics-management tbody").not('#pics-management tbody#pics-management-loading').attr('id', 'pics-cont-row');
    $j("#pics-management tbody tr.pic-active:first img.move-up, #pics-management tbody tr.pic-active:last img.move-down").css({visibility: 'hidden'});
    $j("#pics-management tbody .pic-empty .drag").removeClass('drag');

    var $activePicRow = $j("#pics-management tbody tr.pic-active");

    if ($activePicRow.length == 1) {
        $j("#pics-management tbody tr .drag").removeClass('drag');
    }

    // initialize the table
    initializeTable();
    // set the table
    $j.fn.photoPropUpdate(false, false, true);

    /* move */
    $j('#pics-management tbody img.move-up').click(function() {

        showUpArrowforSecondRow();
     
        var $thisRow = $j(this).parents('tr');
        $thisRow.insertBefore($thisRow.prev()).photoPropUpdate(false, true, true);
        
        if(isMainPhotoSet)
        {
            hideUpArrowforSecondRow();
        }

        return false;
    });

    $j('#pics-management tbody img.move-down').click(function() {
       
        showUpArrowforSecondRow();
        
        var $thisRow = $j(this).parents('tr');
        $thisRow.insertAfter($thisRow.next()).photoPropUpdate(false, true, true);
        
        if(isMainPhotoSet)
        {
            hideUpArrowforSecondRow();
        }

        return false;
    });

    /* delete and clear */
    $j('#pics-management tbody tr').click(function(event) {
      // debugger;
        if ($j(event.target).is('a.delete')) {

            if ($j(event.target).hasClass('spDelete')) {

                $j(this).find('a.spDelete.delete').css({ display: "none" });
                $j(this).find('a.spUndelete.delete').css({ display: "block" });
                $j(this).addClass('nodrag nodrop pic-delete').removeClass('pic-active');

                $j(this).find('.td-photo').removeClass('drag');

                var photoH3Text = $j(this).find('h3').text();
                var deleteMessage = '<div class="pics-management-delete-message"><b>' + photoH3Text + '</b> ' + deleteMsg + '</div>';
                $j(this).find('div.move').css({ position: 'relative' }).append(deleteMessage);
                $j.fn.photoPropUpdate(false, true, false);
              
                return false;
            }
            if ($j(event.target).hasClass('spUndelete')) {
                $j(this).find('a.spUndelete.delete').css({ display: "none" });
                $j(this).find('a.spDelete.delete').css({ display: "block" });
                $j(this).removeClass('nodrag nodrop pic-delete').addClass('pic-active');
                $j(this).find('.td-photo').addClass('drag');
                $j(this).find('.pics-management-delete-message').remove();
                $j.fn.photoPropUpdate(false, true, false);
                return false;
            }
            event.stopPropagation();
        }
        if ($j(event.target).is('a.upload-clear')) {
            $j(this).find('textarea').val("");
            
            var arg2 = $j(this).find('.pic-upload').attr('id');
            clearFileInputField(arg2);
                    
            $j(this).addClass('nodrag nodrop pic-empty').removeClass('pic-active');
            $j(this).find('td.td-move, td.td-photo, td.td-status, td.td-delete').removeClass('drag');
            $j(this).find('a.upload-clear').css({ display: "none" });
         
           // event.stopPropagation();
            $j.fn.photoPropUpdate(true, true, true);
            return false;
           // deleteClickEvent = true;
        }
    });

    /* Hard character limit */
    $j('#pics-management tbody tr').keyup(function(event) {
        if ($j(event.target).is('textarea')) {
            var $this = $j(this).find('textarea');
            limitChars($this, captionCharLimit);
        }
    });

    /* Add caption text */
    //var textareaVal = $j("#pics-management tbody tr.pic-empty textarea:first").val();
    $j("#pics-management tbody tr textarea:contains(" + defaultCaption + ")").css({ color: "#999999" });
    $j("#pics-management tbody tr textarea").focus(function() {
        if ($j(this).val() == defaultCaption) {
            $j(this).val("");
            $j(this).css({ color: "#000000" });
        }
    }).blur(function() {
        if ($j(this).val() == "") {
            $j(this).val(defaultCaption);
            $j(this).css({ color: "#999999" });
        }
    });

    uploadGuidelinesLayer(true);
    facebookButtonToggler();

    /* update edited field 
    $j("#pics-management tbody textarea").keyup(function() {
        $j.fn.photoPropUpdate(false, true, false);
    });*/
    
    $j("#pics-management tbody textarea").one("keyup", function() {
        $j.fn.photoPropUpdate(false, true, false);
    });
    
  
    /* Submit buttons */
    $j(".pics-management-button-container a[id$=btnSaveTop], .pics-management-button-container a[id$=btnSaveBottom]").click(function() {
        updateOrder();
        blockTable(savingMsg, true);
    });



     //add the new hero tr. // we are asured that on load row-1 is the main guy
     if($j("#row-1").find(".currentMainStyle").val() == '1') 
     {

        isMainPhotoSet = true;

        var $thisRow1 =$j("#row-1"); 

        setHeroSlot($thisRow1.find(".td-photo").find("img:first").attr('src') ,$thisRow1.find(".td-status").find("h3:first").html(),$thisRow1.find('.hiddenDelete').attr('id'),$thisRow1.find('.caption').find('textarea:first').val(),$thisRow1.find(".like-your-answer").html());
        
        hideUpArrowforSecondRow(); 

        //hide the row
        $thisRow1.hide();

     }
     else
     {
        setHeroSlot('http://www.jdate.com/img/no-photo-l-m.png' ,'','',defaultCaption,'');
        
     }

     $j("#hero-table").show();

    
     $j("#hero-table textarea").one("keyup", function() {
        $j.fn.photoPropUpdate(false, true, false);
     });

     /* Hard character limit */
     currentHiddenTextArea = $j("#row-1").find('.caption').find('textarea:first');

    $j('#hero-table').keyup(function(event) {
        if ($j(event.target).is('textarea')) {
            var $this = $j(this).find('textarea');
            limitChars($this, captionCharLimit);

            //set the hidden textarea value.
             currentHiddenTextArea.val($this.val());

        }
    });


    $j("#hero-table textarea:contains(" + defaultCaption + ")").css({ color: "#999999" });
    $j("#hero-table  textarea").focus(function() {
        if ($j(this).val() == defaultCaption) {
            $j(this).val("");
            $j(this).css({ color: "#000000" });
        }
    }).blur(function() {
        if ($j(this).val() == "") {
            $j(this).val(defaultCaption);
            $j(this).css({ color: "#999999" });
        }
    });



});

/* end of jquery d and d */

function markdelete(elid)
{//debugger;
    var hiddenDel = document.getElementById(elid);
 
       hiddenDel.value = "1";
    return false;
}
function markundelete(elid) {
   // debugger;
    var hiddenDel = document.getElementById(elid);
   
    hiddenDel.value = "0";
   
    return false;
}

function onBrowse(captionid, listorder, clientids, isPrivatePhoto) {
  //  debugger;
    var captionControl = document.getElementById(captionid);

    if (captionControl != null) {
           captionControl.style.display = "block";
    }
    var clientidsstring = document.getElementById(clientids).value;
    var arrids = clientidsstring.split(",");

    var nextid = parseInt(listorder);
    if (nextid < arrids.length - 1) {
        //var nextelementid = arrids[nextid];
        //var nextelement = document.getElementById(nextelementid);
        var $nextup = $j('#pics-management div.pic-upload')[nextid];
        $j($nextup).css({display: "block"});
        //nextelement.style.display = "block";

        if (isPrivatePhoto == "true") {
            var $nextup = $j('#pics-management div.pic-upload-private')[nextid];
            $j($nextup).css({ display: "block" });
        }
    }

    var rowElement = document.getElementById("row-" + listorder.toString());
    rowElement.className = "pic-empty pos-" + listorder.toString() + " pic-normal upload nodrag nodrop";
    
    browseButtonTableConfig();
    uploadGuidelinesLayer(false);
    facebookButtonToggler();
}

function updateOrder() 
{
    //hack fix for frozen animated gif in IE - basically re-loads the image
    var aniId = $j('img[id$=ajaxLoaderGif]').attr('id');
    setTimeout("document.images['" + aniId + "'].src=document.images['" + aniId + "'].src",10);
        
    var $pics = $j("#pics-management tbody");
    var listOrders=tableSerialize($pics);
    var hidEl=document.getElementById("hidListOrder");
    hidEl.value = listOrders;
    needToConfirm = false;
}


function setMain(theAnchor)
{
    showUpArrowforSecondRow();

    //update all Hidden fields and set the is main flag to 0
     $j('.currentMainStyle').each(function() {
        $thisValue = $j(this).val('0');
    });

    //remove the styles from the current hero spot
    var currentHero =  $j("#pics-cont-row").find("tr:first"); 
    currentHero.find(".isMainPhotoDiv").hide();
    currentHero.find(".mainPhotoDiv").show();
    markundelete(currentHero.find('.hiddenDelete').attr('id'));
    currentHero.show();
    

    //add the new hero tr. add to the top of the row. 
     var $thisRow = $j(theAnchor).closest('tr');
     $thisRow.find(".currentMainStyle").val('1');
     $thisRow.prependTo('#pics-cont-row').photoPropUpdate(false, true, true);
     currentHiddenTextArea = $thisRow.find('.caption').find('textarea:first');
     $thisRow.hide();

     //update the hero slot
     setHeroSlot($thisRow.find(".td-photo").find("img:first").attr('src') ,$thisRow.find(".td-status").find("h3:first").html(),$thisRow.find('.hiddenDelete').attr('id'),$thisRow.find('.caption').find('textarea:first').val(),$thisRow.find(".like-your-answer").html());
     

     //update the up arrow on the second list to show
     hideUpArrowforSecondRow(); 
     

     return false;

}

function setHeroSlot(theSrc, photoNum, photoId,textAreaText,likehtml)
{
    $j('#heroImage').attr('src',theSrc);
    //$j('#heroPhotoNum').html(photoNum);
   $j('.heroTextArea').val(textAreaText);

    if(photoId != '')
    {
        $j('#heroDelete').show();
         $j('#heroDelete').unbind('click').click(function(event) {
        
               
                $j('#hero-row').find('a.spDelete.delete').css({ display: "none" });
                $j('#hero-row').find('a.spUndelete.delete').css({ display: "block" });
                $j('#hero-row').find('a.spUndelete.delete').css({ position: "relative" });

                $j('#hero-info-row').addClass('block-on-delete-hero');
                $j('.heroPic').addClass('block-on-delete-hero');
                
                var photoH3Text = $j('#hero-row').find('h3').text();
                var deleteMessage = '<div class="pics-management-delete-message">' + prefixHeroTextAreaMsg + ' ' + deleteMsg + '</div>';
                $j('#hero-row').find('div.move').css({ position: 'relative' }).append(deleteMessage);
                
                markdelete(photoId);

                $j.fn.photoPropUpdate(false, true, false);
        });

         $j('#heroUndelete').unbind('click').click(function(event) {
        
                $j('#hero-row').find('a.spUndelete.delete').css({ display: "none" });
                $j('#hero-row').find('a.spDelete.delete').css({ display: "block" });
                $j('#hero-info-row').removeClass('block-on-delete-hero');
                $j('.heroPic').removeClass('block-on-delete-hero');
                $j('#hero-row').find('.td-photo').addClass('drag');
                $j('#hero-row').find('.pics-management-delete-message').remove();
                
                markundelete(photoId);
                 $j.fn.photoPropUpdate(false, true, false);
        });

        $j('#hero-table textarea').show();
        $j('#mainDesc').hide();
        $j('#heroLike').html(likehtml);
    }
    else
    {
        $j('#hero-table textarea').hide();
        $j('#mainDesc').show();
        $j('#heroDelete').hide();
        
    }
}


function hideUpArrowforSecondRow()
{
    //set the up arrow on #row-2 to hide
    var $theSecondRow = $j("#pics-cont-row").children(":eq(1)"); // tr:nth-child(2)");
    $theSecondRow.find("img.move-up").hide(); 
}

function showUpArrowforSecondRow()
{
    var $theSecondRow = $j("#pics-cont-row").children(":eq(1)"); // tr:nth-child(2)");
    $theSecondRow.find("img.move-up").show(); 
}

 //]]>
</script>
<mn:Title runat="server" ID="ttlManageYourPhotos" ResourceConstant="TTL_MANAGE_YOUR_PHOTOS" /> 
<asp:PlaceHolder runat="server" ID="plcError" Visible="false">
<mn:Txt ID="txtError" runat="server" ResourceConstant="TXT_UPLOAD_ERROR" />
</asp:PlaceHolder>


<table id="hero-table" style="display:none" width="100%">
 <tr id="hero-row">
        <td>
            <div class="heroPic" style="margin-right:10px; padding:">
                    <img id="heroImage" alt=""  width="112px" height="144px" />
            </div>
            
        </td>
        <td >
            <div class="move"> </div>
            <table  id="hero-info" style="width:95%;">
                <tr id="hero-info-row">
                    <td>
                            <div class="status"><h3 id="heroPhotoNum"></h3> <a href="#" style=" text-decoration:none; font-size:16px; cursor:default;"><mn:txt runat="server" id="Txt4" resourceconstant="TXT_THIS_IS_YOUR_MAIN_PHOTO" expandimagetokens="true" /></a> </div>
                            <div class="status"><mn:txt runat="server" id="Txt5" resourceconstant="TXT_THIS_WILL_DISPLAY_SEARCH_THUMBNAIL" expandimagetokens="true" /></div>
                            <div id="heroLike" class="heroFaceBookLikeCss"></div>
                    </td>
                    <td class="heroTextArea">
                        <textarea class="heroTextArea" style="resize: none;"  cols="45" rows="4" style="color: rgb(153, 153, 153);"><mn:txt runat="server" id="Txt7" resourceconstant="TXT_ADD_CAPTION_HERE" expandimagetokens="true" /></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><div id="mainDesc" style="font-size:10px;"><br /><br /><mn:txt runat="server" id="Txt6" resourceconstant="TXT_MAIN_PHOTO_ELIGIBLE_DESCRIPTION" expandimagetokens="true" /></div></td>
                </tr>
            </table>
        </td>
        <td >
            <table width="100%"  >
                <tr>
                    <td class="td-delete">
                        <div class="delete">
                            <a class="delete spDelete" id="heroDelete" style="cursor:pointer;" ><mn:Txt ID="btnDelete" runat="server" ResourceConstant="DELETE" /></a>
                            <a class="delete spUndelete" id="heroUndelete" style="display:none;"><mn:Txt ID="btnUnDelete" runat="server" ResourceConstant="UNDELETE" /></a>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
   </tr>
</table>


<div class="clearfix pics-management-button-container">
    <div class="wrapper">
	    <div class="pics-management-edited-liquid">
		    <div class="pics-management-edited"><div class="pics-management-edited-inner"><mn:image id="mnimage8171" runat="server" filename="icon-page-message.gif" resourceconstant="ALT_PAGE_MESSAGE_SAVE_CHANGES" />
            <mn:Txt ID="txtSaveChanges1" runat="server" ResourceConstant="TXT_SAVE_CHANGES" /></div></div>
	    </div>
    </div>
    <div class="pics-management-edited-fixed">
	    <asp:LinkButton Runat="server" ID="btnSaveTop"><mn:image id="btnSaveImage" runat="server" filename="btn-save-changes.png" resourceconstant="ALT_CLICK_TO_SUBMIT" /></asp:LinkButton>
    </div>
</div>
<input type="hidden" name="hidListOrder" id="hidListOrder" />
<div id="pics-management-container">
<div id="pics-management-blocker"></div>
<div id="pics-management-blocker-content"><mn:image id="ajaxLoaderGif" runat="server" filename="ajax-loader.gif" titleresourceconstant="" resourceconstant="ATL_PROCESSING" /><h2><mn:txt id="mntxt5626" runat="server" resourceconstant="TXT_LOADING_MSG" expandimagetokens="true" /></h2>
</div>


<table id="pics-management"><asp:Repeater ID="rptPhotos" runat="server">

<HeaderTemplate>
<thead class="nodrop nodrag">
    <tr>
        <th class="th-move"><mn:Txt ID="txtHdrMove1" runat="server" ResourceConstant="HDR_MOVE" /></th>
        <th class="th-photo"><mn:Txt ID="txtHdrPhoto1" runat="server" ResourceConstant="HDR_PHOTO" /></th>
        <th class="th-status"><mn:Txt ID="txtHdrStatus1" runat="server" ResourceConstant="HDR_STATUS" /></th>
        <th class="th-caption"><mn:Txt ID="txtHdrCaption1" runat="server" ResourceConstant="HDR_CAPTION" /></th>
        <th class="th-delete"><mn:Txt ID="txtHdrDelete1" runat="server" ResourceConstant="HDR_DELETE" />
        <input type="hidden" runat="server" id="txtUploadClientIds" /></th>
    </tr>
</thead>
</HeaderTemplate>
<FooterTemplate>
<tfoot class="nodrop nodrag">
    <tr>
        <th class="th-move"><mn:Txt ID="txtHdrMove2" runat="server" ResourceConstant="HDR_MOVE" /></th>
        <th class="th-photo"><mn:Txt ID="txtHdrPhoto2" runat="server" ResourceConstant="HDR_PHOTO" /></th>
        <th class="th-status"><mn:Txt ID="txtHdrStatus2" runat="server" ResourceConstant="HDR_STATUS" /></th>
        <th class="th-caption"><mn:Txt ID="txtHdrCaption2" runat="server" ResourceConstant="HDR_CAPTION" /></th>
        <th class="th-delete"><mn:Txt ID="txtHdrDelete" runat="server" ResourceConstant="HDR_DELETE" /></th>
    </tr>
</tfoot>
</FooterTemplate>

<ItemTemplate>
    <tr id="row-<%# Eval("ListOrder") %>" class="<asp:Literal runat="server" ID="litCss" />">
        <td class="td-move drag">
            <div class="move">
                <mn:Image runat="server" FileName="icon-move-up.gif" ID="iconMoveUp" resourceconstant="ALT_MOVE_UP" CssClass="move-up block-on-delete" />                
                <mn:Image runat="server" FileName="icon-move-down.gif" ID="iconMoveDown" resourceconstant="ALT_MOVE_DOWN" CssClass="move-down block-on-delete" />
            </div>
            <input type="hidden" runat="server" id="txtPhotoID" />
            <input type="hidden" runat="server" id="txtFileID" />
            <input type="hidden" runat="server" id="txtListOrder" />
            <input type="hidden" runat="server" id="txtDelete" value="0" class="hiddenDelete" />
            <input type="hidden" runat="server" id="txtApprovedForMain" />
            <input type="hidden" runat="server" id="txtIsMain" class="currentMainStyle" />
        </td>
        <td class="td-photo drag">
            <div class="pic block-on-delete">
	            <asp:image id="thumbPhoto" alt="" runat="server" /> 
	            <div id="divNoPhoto" runat="server">
	            <uc1:NoPhoto runat="server" id="ucNoPhoto1" visible="true" TextResourceConstant="TXT_UPLOAD_NEW_PHOTO" />
	            <%--<div class="no-photo-nolink"><mn:txt id="mntxt9408" runat="server" resourceconstant="TXT_UPLOAD_NEW_PHOTO" expandimagetokens="true" /></div>--%>
	            </div>
            </div>
        </td>
        <td class="td-status drag">
                <div class="status block-on-delete"><h3 id="phStatus" runat="server"><mn:txt id="photoText" runat="server"  Visible="true"/></h3>
                    <p><mn:txt id="systemStatus" runat="server"  Visible="true"></mn:txt></p>
                </div>
	                <p class="block-on-delete"><mn:Txt id="systemStatusMessage" runat="server" Visible="true"></mn:Txt></p>
	            <div id="phBrowse" runat="server" class="pic-upload">
                    <span class="spr-b s-icon-b-computer"></span>
                    <h4><mn:txt runat="server" id="uploadFromComputer" resourceconstant="TXT_UPLOAD_FROM_COMPUTER" expandimagetokens="true" /></h4>
                    <div class="btn link-quinary"><input class="browse" type="file" runat="server" id="upBrowse" /></div>
	            </div>
	             <div class="block-on-delete">
                    <mn:multivalidator id="PhotoValidator" runat="server" RequiredTypeResourceConstant="INVALID_PHOTO_TYPE" ErrorMessage="MultiValidator" Display="Dynamic" FieldNameResourceConstant="PHOTO" RequiredType="PhotoType" Text="" ControlToValidate="upBrowse" IsRequired="False" OnServerValidate="IsValidImage"></mn:multivalidator>
                    <asp:Label ID="ErrorMsg" CssClass="error" Runat="server" Visible="False" />
                </div>
                <div id="divPrivatePhoto" runat="server" class="hide block-on-delete privatePhotoDiv pic-upload-private">
                <asp:CheckBox runat="server" ID="chkPrivatePhoto" />
	            <mn:Txt ID="Txt2" runat="server" ResourceConstant="TXT_PRIVATE_PHOTO" />
            </div>
            <mlft:MemberLikeAnswerClientModule ID="memberPhotoLikeClientModule" runat="server" Enabled="true" Visible="false"/>
            <div id="pnlMakeMain" runat="server" class="mainPhotoDiv block-on-delete" visible="false">
                    <a id='makeMain' onclick='setMain(this)'; class='mainPhotoAnchor'><mn:txt runat="server" id="Txt4" resourceconstant="TXT_MAKE_THIS_MY_MAIN_PHOTO" expandimagetokens="true" /></a>
            </div>
            <div id="pnlIsMain" runat="server" class="isMainPhotoDiv block-on-delete" visible="false"></div>
            <div id="pnlCantBeMain" runat="server" class="block-on-delete" visible="false"><mn:txt runat="server" id="Txt8" resourceconstant="TXT_APPROVED_BUT_NOT_ELIGIBLE_FOR_MAIN" expandimagetokens="true" /></div>
        </td>
        <td class="td-caption nodrag">
            <div class="caption block-on-delete">
                <span ID="phCaption" runat="server"> <asp:TextBox runat="server" ID="txtCaption" TextMode="MultiLine"></asp:TextBox></span>
            </div>
            <%--Matt: div#phBrowserFacebook needs to show only in first open slot, like div#phBrowse--%>
            <div id="phBrowseFacebook" runat="server" class="pic-upload-facebook">
                <span class="spr-b s-icon-b-fb"></span>
                <h4><mn:txt runat="server" id="Txt3" resourceconstant="TXT_ADD_PHOTOS_FROM_FACEBOOK" expandimagetokens="true" /></h4>
                <a href="#" id="FB_idUploadButton" class="btn link-quinary" title='<mn:txt runat="server" id="Txt9" resourceconstant="TXT_FACEBOOK_NOTHING_POSTED" expandimagetokens="true" />'>
                <mn:txt runat="server" id="txtAddToFacebookBtn" resourceconstant="TXT_ADD_TO_FACEBOOK_BTN" expandimagetokens="true" /></a>
                <mn:txt runat="server" id="Txt10" resourceconstant="TXT_APPROVE_POPUPS_FACEBOOK" expandimagetokens="true" />
            </div>
        </td>
        <td class="td-delete drag">
            <div class="delete">
                <a class="delete spDelete" href="javascript:void(0)" <asp:Literal ID="litDeleteScript" runat="server" />><mn:Txt ID="btnDelete" runat="server" ResourceConstant="DELETE" /></a>
                <a class="delete spUndelete" style="display: none;" href="javascript:void(0)" <asp:Literal ID="litUndeleteScript" runat="server" />><mn:Txt ID="btnUnDelete" runat="server" ResourceConstant="UNDELETE" /></a>
            </div>
            <div class="clear">
                <a class="upload-clear" href="javascript:void(0)"><mn:Txt ID="Txt1" runat="server" ResourceConstant="CLEAR" /></a>
            </div>
        </td>
    </tr>
</ItemTemplate>

</asp:Repeater>
</table>
</div><%--end table-container--%>

<div class="clearfix pics-management-button-container">
    <div class="wrapper">
	    <div class="pics-management-edited-liquid">
	        <div class="pics-management-edited"><div class="pics-management-edited-inner"><mn:image id="Image2" runat="server" filename="icon-page-message.gif" resourceconstant="ALT_PAGE_MESSAGE_SAVE_CHANGES" />
	        <mn:Txt ID="txtSaveChanges2" runat="server" ResourceConstant="TXT_SAVE_CHANGES" /></div></div>
        </div>
    </div>
    <div class="pics-management-edited-fixed">
        <asp:LinkButton Runat="server" ID="btnSaveBottom"><mn:image id="Image1" runat="server" filename="btn-save-changes.png" resourceconstant="ALT_CLICK_TO_SUBMIT" /></asp:LinkButton>
    </div>
</div>

<script type="text/javascript">
    $j("#pics-management tbody").attr('id', 'pics-cont-row');
    blockSubmits();
    $j(document).ready(function () {
        blockTable(loadingMsg, false);
    });
</script>

<div class="clearfix pics-management-guidlines-layer rel-layer-container" runat="server" ID="divFacebookHelp">
    <button class="btn textlink" type="button" rel="hover"><mn:txt runat="server" id="txtNeedHelpBtn" resourceconstant="TXT_NEED_HELP_BTN" expandimagetokens="true" /></button>
    <div class="rel-layer-div">
        <mn:txt id="mntxt8300" runat="server" resourceconstant="TXT_PHOTO_UPLOAD_INSTRUCTIONS_FACEBOOK" expandimagetokens="true" />
    </div>
</div>


<div class="clearfix pics-management-guidlines-layer" runat="server" ID="divNoFacebookHelp">
<mn:txt id="mntxt8299" runat="server" resourceconstant="TXT_PHOTO_UPLOAD_INSTRUCTIONS" expandimagetokens="true" />
</div>



</div><%--end page-container--%>















<div id="fb-photo-uploader" class="fb-photo-up" style="display:none;">

	<div class="fb-photo-col-1">
		<div id="fb-browser-container">
			<div id="fb-browser">
                <mn:image runat="server" id="fbLogo" FileName="ui-facebook-logo.jpg" CssClass="fb-logo" ResourceConstant="" />
				<p><mn:txt runat="server" id="txtFacebookModalHeader" resourceconstant="TXT_FACEBOOK_MODAL_HEADER" expandimagetokens="true" /></p>
				<div id="fb-browser-photos" class="clearfix"><mn:image runat="server" id="loader" FileName="ajax-loader.gif" ResourceConstant="" /></div>
			</div>
		</div>
	</div>
	<div class="fb-photo-col-2">
        <mn:image runat="server" id="fbArrow" FileName="ui-facebook-arrow.gif" ResourceConstant="" />
	</div>
	<div class="fb-photo-col-3">
		<div id="fb-photos-to-add-container">
			<div id="fb-photos-to-add">
                <mn:image runat="server" id="siteLogo" CssClass="fb-site-logo" FileName="logo-header-trans.png" ResourceConstant="" />
				<div id="fb-photo-slots">
                    <%--these slots get built dynamically by FBPhotoImport.createSlotHtml()--%>
				</div>
				<p class="padding-medium">
					<a id="savePhotosBtn" href="javascript:FBPhotoImport.upload_photos();" class="btn link-primary"><mn:txt runat="server" id="txtFacebookModalSaveBtn" resourceconstant="TXT_FACEBOOK_MODAL_SAVE_BTN" expandimagetokens="true" /></a>
				</p>
			</div>
		</div>
	</div>
</div>
<input type="hidden" runat="server" id="hdnFacebookURLs" />

<script>
// Content to be added to spark namespace
__addToNamespace__('spark.facebook', {
    photoImportMsgSaveBefore: "<%=g.GetResource("MSG_PHOTO_IMPORT_MSG_SAVE_BEFORE",this) %>",
    fbAuthStatus: function(ifConnected){
        //this was created for fixing a bug in the photo upload that used an inline function specific to MemberPhotoEdit
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                ifConnected();
            } else {
                FB.login();//Log back in to Facebook
            }
        }, true);
	}
});

$j('#page-container').bind('click',function(event){
    var $target = $j(event.target);

    if($target.is('#FB_idUploadButton')){
        event.preventDefault();

		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				FBPhotoImport.show_fb_photo_box();
			} else {
				spark.facebook.fbAuth(function(){FBPhotoImport.show_fb_photo_box()}, null, 'user_birthday,user_photos,email');
			}
		}, true);
    }
});

var FBPhotoImport = (function ($) {
	var hello = {},
//Private namespace
		shh = {},
//Private variables
		FBAlbumData = [],
		FBPhotoUploadQueueBigUrls = [],
		FBPhotoUploadQueueIds = [],
		FBPhotoUploadQueueUrls = [],
		spark_photos_loaded = 0,
		$content = $('#fb-photo-uploader');

//public properties
	hello.full_slots = 0;
	hello.total_slots = 0;
	hello.spark_photos = [];
	hello.first_open_slot = hello.spark_photos.length,
	
// public content property
	hello.content = {},
	hello.content.closeConfirm = '<%=g.GetResource("TXT_CLOSE_CONFIRM",this) %>',
	hello.content.linkBackToAlbums = '<%=g.GetResource("TXT_LINK_BACK_TO_ALBUMS",this) %>',
    hello.content.linkRemove = '<%=g.GetResource("TXT_LINK_REMOVE",this) %>',
    hello.content.modalTitle = '<%=g.GetResource("TXT_MODAL_TITLE",this) %>',
    hello.content.txt_photos = '<%=g.GetResource("TXT_TXT__PHOTOS",this) %>',
    hello.content.noneSelectedForImport =  '<%=g.GetResource("TXT_NO_PHOTOS_SELECTED_FOR_IMPORT",this) %>',
    hello.content.errorAlbum = '<%=g.GetResource("TXT_ERROR_FB_ALBUM",this) %>',
    hello.content.ajaxLoader = '<mn:image id="modalLoader" runat="server" filename="ajax-loader.gif" titleresourceconstant="" resourceconstant="ATL_PROCESSING" />';


//private functions/code
	// add indexOf to browsers that don't have it...i'm looking at you IE7!
	// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/indexOf
	if (!Array.prototype.indexOf)
	{
		Array.prototype.indexOf = function(elt)
		{
			var len = this.length;

			var from = Number(arguments[1]) || 0;
			from = (from < 0)
					 ? Math.ceil(from)
					 : Math.floor(from);
			if (from < 0)
				from += len;

			for (; from < len; from++)
			{
				if (from in this &&
						this[from] === elt)
					return from;
			}
			return -1;
		};
	}
	
	function fireUpPhotoModal(){
		$content.dialog({
			width: 765,
			modal: true,
			title:hello.content.modalTitle,
			dialogClass: 'facebook-photo-import',
			beforeClose: function(event, ui) {
				
				// check to see if any photos were added
				if (FBPhotoUploadQueueIds.length > 0){
					//if photos were added, ask the user if they want to cancel the close
					var closeDialog = confirm(hello.content.closeConfirm);
					
					if(closeDialog){
						//reset any selected imports
						resetImports();
						//if the user wants to continue with the close, return true to close the modal
						return true;
					} else {
						return false;
					}
				} else {
					return true;
				}
			}
		});
	}

function resetImports(){
		FBPhotoUploadQueueIds = [];
		FBPhotoUploadQueueUrls = [];
		FBPhotoUploadQueueBigUrls = [];

		for (var i = 0; i < hello.total_slots - hello.first_open_slot; i++){
			document.getElementById('fb-photo-slot-'+(i+hello.first_open_slot)).innerHTML = '';
		}
		$('#savePhotosBtn').show();
		$('#savePhotosBtnLoader').remove();
}

//public functions
    hello.createSlotHtml = function(total_slots){
        var $slotCont = $('#fb-photo-slots'),
            $slotDiv = $('<div>').attr('class', 'fb-photo-slot'),
            $slotDivCol = $();

        for(var i = 0; i < total_slots; i++){
            var slotId = 'fb-photo-slot-' + i,
                $clone = $slotDiv.clone().attr('id', slotId);

            $slotDivCol = $slotDivCol.add($clone);
        }
        return $slotDivCol;
    }

	hello.show_fb_photo_box = function(){
		if(spark_photos_loaded == 0){
			for (var i = 0; i < hello.spark_photos.length; i++){
				document.getElementById('fb-photo-slot-' + i).innerHTML = '<img src="' + hello.spark_photos[i] + '" border=0 />';
			}
			spark_photos_loaded = 1;
			if(hello.total_slots == hello.full_slots + FBPhotoUploadQueueIds.length){
				$('input[type=checkbox]').attr('disabled','true');
			}
		}
		FBPhotoImport.get_fb_albums();
		//fb_pop_in.show();
		fireUpPhotoModal();
	}

	hello.get_fb_photos = function(album){
        
        var html = '<div class="fb-album-link"><a href="javascript:FBPhotoImport.get_fb_albums()">' + hello.content.linkBackToAlbums + '</a></div><div id="fb-browser-photos">' + hello.content.ajaxLoader + '</div>';
        $('#fb-browser-photos').html(html);
        $('#fb-browser').scrollTop(0);

        FB.api('/' + album + '/photos', function(resp1) {
            var html = '<div class="fb-album-link"><a href="javascript:FBPhotoImport.get_fb_albums()">' + hello.content.linkBackToAlbums + '</a></div>',
                photo_len = resp1.data.length,
                photo_num = 0;
            
            if (resp1.data == undefined) {
                html += '<div style="color: red; font-size: 20px; padding: 40px; line-height: 1em;">' + hello.content.errorAlbum + '</div>';
                $('#fb-browser-photos').html(html);
                $('#fb-browser').scrollTop(0);
                return;
            }

            $.each(resp1.data, function(index, response){
                
                photo_num++;
                if (response.picture != undefined) {
                    html += '<div class="fb-album-photos-container"><div class="fb-album-photos"><div id="photo"><img src="' + response.picture + '" alt=""/></div>';
                    html += '<div id="checkbox"><input type="checkbox" name="fb-photo-' + response.id + '" id="fb-photo-' + response.id + '" value="' + response.id + '" onclick="FBPhotoImport.fb_photo_move(\'' + response.id + '\', \'' + response.picture + '\', \'' + response.source + '\')"';
                    if (FBPhotoUploadQueueIds.indexOf(response.id) > -1) {
                        html += ' checked ';
                    }
                    html += '/></div>';
                    html += '</div></div>';
                }
                if (photo_num == photo_len) {
                    $('#fb-browser-photos').html(html);
                    $('#fb-browser').scrollTop(0);
                    if (hello.total_slots == hello.full_slots + FBPhotoUploadQueueIds.length) {
                        $('input[type=checkbox]').attr('disabled', 'true');
                    }
                }
            
            }); // end $.each

        });

	}

	hello.fb_photo_move = function(pid, url, big_url){

		if(document.getElementById('fb-photo-'+pid).checked == true && hello.full_slots+FBPhotoUploadQueueIds.length < hello.total_slots && FBPhotoUploadQueueIds.indexOf(pid) == -1){
			FBPhotoUploadQueueIds.push(pid);
			FBPhotoUploadQueueUrls.push(url);
			FBPhotoUploadQueueBigUrls.push(big_url);
            
            if(FBPhotoUploadQueueIds.length + hello.full_slots < hello.total_slots){
			    for (var i = 0; i < FBPhotoUploadQueueIds.length; i++){ 
				    document.getElementById('fb-photo-slot-' + (i + hello.first_open_slot)).innerHTML = '<img src="' + FBPhotoUploadQueueUrls[i] + '" border=0 /><br/><a href="javascript:FBPhotoImport.fb_remove_photo(' + i + ')">' + hello.content.linkRemove + '</a>';
			    }
            }
			if(hello.total_slots - 1 === hello.full_slots+FBPhotoUploadQueueIds.length){
				$('input[type=checkbox]').attr('disabled','true');
			}
		}

	}

	hello.get_fb_albums = function(){
        
        var album_li = '';
        if (FBAlbumData.length > 0) {
            $.each(FBAlbumData, function(index, resp4){
                album_li += '<div class="fb-album-container"><div class="fb-album-cover"><a href="javascript:FBPhotoImport.get_fb_photos(\'' + resp4.albumID + '\');"><img id="fb-album-img" src="' + resp4.cover_url + '" border=0 /></a></div><div class="fb-album-info"><div class="fb-album-name">' + resp4.albumName + '</div>' + resp4.size + ' photos</div></div>';
                $('#fb-browser-photos').html(album_li);
                $('#fb-browser').scrollTop(0);
            
            });
        } else {
            FBAlbumData = [];

            FB.api('/me/albums', function(resp1) {
                
                var album_len = resp1.data.length,
                    album_num = 0;

                if (resp1.data == undefined) {
                    html = '<div style="color: red; font-size: 16px; padding: 40px; line-height: 1em;">' + hello.content.errorAlbum + '</div>';
                    $('#fb-browser-photos').html(html);
                    $('#fb-browser').scrollTop(0);
                    return;
                }

                $.each(resp1.data, function(index, resp2){

                    album_num++;

                    if(resp2.count == undefined){
                        return true;
                    }

                    FB.api('/' + resp2.cover_photo, function(resp3) {
                        FBAlbumData.push({
                            albumID: resp2.id,
                            albumName: resp2.name,
                            size: resp2.count,
                            object_id: resp2.id,
                            cover: resp2.cover_photo,
                            cover_url: resp3.picture
                        });
                        album_li += '<div class="fb-album-container"><div class="fb-album-cover"><a href="javascript:FBPhotoImport.get_fb_photos(\'' + resp2.id + '\');"><img id="fb-album-img" src="' + resp3.picture + '" border=0 /></a></div><div class="fb-album-info"><div class="fb-album-name">' + resp2.name + '</div>' + resp2.count + ' photos</div></div>';
                        if (album_num == album_len) {
                            $('#fb-browser-photos').html(album_li);
                            $('#fb-browser').scrollTop(0);
                        }
                    });

                }); // end $.each

            });
            
        }

	}
	
	hello.fb_remove_photo = function(place){

		var pid = FBPhotoUploadQueueIds[place];

		FBPhotoUploadQueueIds.splice(place,1);
		FBPhotoUploadQueueUrls.splice(place,1);
		FBPhotoUploadQueueBigUrls.splice(place,1);

		$('input[id=fb-photo-'+pid+']').removeAttr('checked');

		for (var i = 0; i < hello.total_slots - hello.first_open_slot; i++){
			document.getElementById('fb-photo-slot-' + (i + hello.first_open_slot)).innerHTML = '';
		}
		
		for (var i = 0; i < FBPhotoUploadQueueIds.length; i++){
			document.getElementById('fb-photo-slot-' + (i + hello.first_open_slot)).innerHTML = '<img src="' + FBPhotoUploadQueueUrls[i] + '" border=0 /><br/><a href="javascript:FBPhotoImport.fb_remove_photo(' + i + ')">' + hello.content.linkRemove + '</a>';
		}

		if(hello.total_slots > hello.full_slots+FBPhotoUploadQueueIds.length){
			$('input[type=checkbox]').removeAttr('disabled');
		}
	}

hello.upload_photos = function(){
    if(FBPhotoUploadQueueBigUrls.length < 1){
        alert(hello.content.noneSelectedForImport);
    } else{
	    $content.block({
            message: hello.content.ajaxLoader,
            overlayCSS:  { 
                backgroundColor:'#fff',
                opacity:0.7 
            },
            css: {
                width:'auto',
                top:'0',
                left:'0px',
                backgroundColor:null,
                border:null,
                padding:null,
                color:null,
                margin:null,
                textAlign:null
            }
        });
        $("#<%=hdnFacebookURLs.ClientID %>").val(FBPhotoUploadQueueBigUrls);
        // Fire omniture event77
        s.events = "event77";
        s.t();
        <%=Page.ClientScript.GetPostBackEventReference(btnSaveTop, string.Empty)%>;
    }
}
	 
	return hello;
}(jQuery));
</script>


<script type="text/javascript">
	FBPhotoImport.total_slots = <%=MaxPhotoCount.ToString()%>;

	
	<asp:Repeater ID="rptFacebookPhotos" runat="server">
        <ItemTemplate>
            FBPhotoImport.spark_photos.push('<asp:Literal ID="litPhotoPath" runat="server"></asp:Literal>');
        </ItemTemplate>
    </asp:Repeater>
        
	FBPhotoImport.full_slots = FBPhotoImport.spark_photos.length - 1;
	FBPhotoImport.first_open_slot = FBPhotoImport.spark_photos.length;

    //build the photo slots
    $j('#fb-photo-uploader').find('#fb-photo-slots').html(
        FBPhotoImport.createSlotHtml(FBPhotoImport.total_slots)
    );
</script>
