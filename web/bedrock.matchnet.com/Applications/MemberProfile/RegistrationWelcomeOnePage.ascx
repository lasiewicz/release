﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegistrationWelcomeOnePage.ascx.cs"
    Inherits="Matchnet.Web.Applications.MemberProfile.RegistrationWelcomeOnePage" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register Src="~/Applications/MemberProfile/OnePageCompleteProfile1.ascx"
    TagPrefix="mn1" TagName="OnePageCompleteProfile1" %>
<script type="text/javascript" src="/javascript20/library/jquery.json-2.2.min.js"></script>
<div id="one-page-reg-welcome-container">
    <div id="header-logo">
        <a href="/">
            <mn:Image ID="Image1" CssClass="logo" runat="server" TitleResourceConstant="SITE_LOGO_ALT"
                ResourceConstant="SITE_LOGO_ALT" FileName="logo-header-img.png" />
        </a>
    </div>
    <div id="page-content">
        <div id="title">
            <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_HEADER" />
        </div>
        <mn1:OnePageCompleteProfile1 runat="server" ID="OnePageCompleteProfile1" />
        <div id="leftbox-1" class="box-content">
            <mn:Txt ID="Txt2" runat="server" ResourceConstant="TXT_SUBSCRIBE_BOX" />
            <div class="disabled-matches-btn">
                <mn:Txt ID="Txt7" runat="server" ResourceConstant="TXT_SUBSCRIBE_BTN" />
            </div>
            <div class="enabled-matches-btn">
                <a id="subscribe-btn" href="/Applications/Subscription/Subscribe.aspx?prtid=41">
                    <mn:Txt ID="Txt3" runat="server" ResourceConstant="TXT_SUBSCRIBE_BTN" />
                </a>
            </div>
        </div>
        <asp:PlaceHolder runat="server" ID="plcDefault">
            <div id="leftbox-2" class="box-content">
                <mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_MATCHES_BOX" />
                <div class="disabled-matches-btn">
                    <mn:Txt ID="Txt6" runat="server" ResourceConstant="TXT_MATCHES_BTN" />
                </div>
                <div class="enabled-matches-btn">
                    <a id="matches-btn" href="/Applications/Search/SearchResults.aspx?NavPoint=sub">
                        <mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_MATCHES_BTN" />
                    </a>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcFromRegOverlay" Visible="False">
            <div id="leftbox-2" class="box-content mol">
                <mn:Txt ID="Txt8" runat="server" ResourceConstant="TXT_MOL_BOX" />
                <div class="disabled-matches-btn">
                    <mn:Txt ID="Txt9" runat="server" ResourceConstant="TXT_MOL_BTN" />
                </div>
                <div class="enabled-matches-btn">
                    <a id="A1" href="/Applications/MembersOnline/MembersOnline.aspx">
                        <mn:Txt ID="Txt10" runat="server" ResourceConstant="TXT_MOL_BTN" />
                    </a>
                </div>
            </div>
        </asp:PlaceHolder>
    </div>
</div>
<div id="footer">
    <div id="footer-container">
        <mn1:Footer20 ID="Footer20" runat="server" />
    </div>
</div>
