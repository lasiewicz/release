﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnePageCompleteProfile2.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.OnePageCompleteProfile2" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="EditLocation" Src="/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditLocation.ascx" %>
<%= GetForcedPageCode("Start")%>
<asp:PlaceHolder runat="server" ID="plcCallToAction2" Visible="False">
    <div id="profile-overlay-content">
        <div id="title">
            הרשמתך נקלטה. תודה.
        </div>
</asp:PlaceHolder>

<div id="complete-profile" class="circles">
    <mn:Txt ID="Txt9" runat="server" CssClass="box-title" ResourceConstant="TXT_COMPLETE_PROFILE_HEADER" />
    <mn:Txt ID="Txt10" runat="server" ResourceConstant="TXT_DETAILS_SAVED" />
    <asp:PlaceHolder ID="plcRegionReligion"  Visible="False" runat="server">
        <div id="top-row">
            <div class="Country send-attribute">
                <div class="custom">
                    <div class="select-list country-select" id="country-dd-custom">
                        <div class="control">
                            <div class="arrow"><div class="shape"></div></div>
                            <div class="title"></div>
                        </div>
                        <ul class="list"></ul>
                    </div>

                    <div class="select-list region-select" id="region-dd-custom">
                        <div class="control">
                            <div class="arrow"><div class="shape"></div></div>
                            <div class="title"></div>
                        </div>
                        <ul class="list"></ul>
                    </div>                
                </div>

                <div class="item-title item-title-select">
                    <mn:EditLocation ID="EditLocation" runat="server"></mn:EditLocation>
                    <select id="israeliRegionsDD"></select>
                    <input type="text" value="תל אביב" id="israeliRegionsInput" class="ui-autocomplete-input" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" placeholder="עיר" />
                </div>
            </div>

            <div id="religion-wrapper" class="custom">
                <div class="select-list religion-select Religion send-attribute" key="ATTR_JDateReligion">
                    <select>
                        <%=GetListItems("JDateReligion", "select")%></ul>
                    </select>

                    <div class="control">
                        <div class="arrow"><div class="shape"></div></div>
                        <div class="title"><mn:Txt runat="server" ID="Txt21" ResourceConstant="TXT_JDATE_RELIGION" /></div>
                    </div>
                    <ul class="list"><%=GetListItems("JDateReligion", "ul")%></ul>
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
    <div id="first-row" class="category">
        <div class="inner-row cat-title">
            <div class="item-title" key="ATTR_MaritalStatus">
                <div class="inner-txt"><mn:Txt runat="server" ID="Txt7" ResourceConstant="TXT_RELATIONSHIP_STATUS" /></div>
            </div>
            <div class="item-title" key="ATTR_KeepKosher">
                <div class="inner-txt"><mn:Txt runat="server" ID="Txt20" ResourceConstant="TXT_KEEP_KOSHER" /></div>
            </div>
            <div class="item-title" key="ATTR_SmokingHabits">
                <div class="inner-txt"><mn:Txt runat="server" ID="Txt2" ResourceConstant="TXT_SMOKING_HABITS" /></div>
            </div>
        </div>

        <div class="inner-row">
            <div class="RelationshipStatus send-attribute" key="ATTR_MaritalStatus">
                <a class="scroll up"></a>
                <ul>
                    <%=GetListItems("MaritalStatus")%>
                </ul>
                <a class="scroll down"></a>
            </div>
            <div class="KeepKosher send-attribute" key="ATTR_KeepKosher">
                <a class="scroll up"></a>
                <ul>
                    <%=GetListItems("KeepKosher")%>
                </ul>
                <a class="scroll down"></a>
            </div>
            <div class="SmokingHabits send-attribute" key="ATTR_SmokingHabits">
                <a class="scroll up"></a>
                <ul>
                    <%=GetListItems("SmokingHabits")%>
                </ul>
                <a class="scroll down"></a>
            </div>
        </div>
    </div>
    <div id="second-row" class="category">
        <div class="inner-row cat-title">
            <div class="item-title" key="ATTR_ChildrenCount">
                <div class="inner-txt"><mn:Txt runat="server" ID="Txt8" ResourceConstant="TXT_CHILDREN_COUNT" /></div>
            </div>
            <div class="item-title" key="ATTR_EducationLevel">
                <div class="inner-txt"><mn:Txt runat="server" ID="Txt5" ResourceConstant="TXT_EDUCATION_LEVEL" /></div>
            </div>
            <div class="item-title" key="ATTR_Height">
                <div class="inner-txt"><mn:Txt runat="server" ID="Txt6" ResourceConstant="TXT_HEIGHT" /></div>
            </div>
        </div>

        <div class="inner-row">
            <div class="ChildrenCount send-attribute" key="ATTR_ChildrenCount">
                <a class="scroll up"></a>
                <ul>
                    <%=GetListItems("ChildrenCount")%>
                </ul>
                <a class="scroll down"></a>
            </div>
            <div class="EducationLevel send-attribute" key="ATTR_EducationLevel">
                <a class="scroll up"></a>
                <ul>
                    <%=GetListItems("EducationLevel")%>
                </ul>
                <a class="scroll down"></a>
            </div>
            <div class="Height send-attribute" key="ATTR_Height">
                <a class="scroll up"></a>
                <ul>
                    <%=GetListItems("Height")%>
                </ul>
                <a class="scroll down"></a>
            </div>
        </div>
    </div>
    <asp:PlaceHolder ID="plcSaveDetailsWithImg" runat="server" Visible="false">
        <mn:Image ID="Image1" runat="server" FileName="save-details-with-hearts-bt.png" TitleResourceConstant="ALT_SAVE_DETAILS" />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcSaveDetailsWithLnk" runat="server">
        <div id="send-attr-btn">
            <div class="inner-txt"><mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_SEND_ATTR_BTN" /></div>
        </div>
    </asp:PlaceHolder>
</div>
<asp:PlaceHolder runat="server" ID="plcCallToAction" Visible="False">
    <div id="leftbox-1" class="box-content">
        <mn:Txt ID="Txt11" runat="server" ResourceConstant="TXT_SUBSCRIBE_BOX" />
        <div class="disabled-matches-btn">
            <mn:Txt ID="Txt12" runat="server" ResourceConstant="TXT_SUBSCRIBE_BTN" />
        </div>
        <div class="enabled-matches-btn">
            <a id="subscribe-btn" href="/Applications/Subscription/Subscribe.aspx?prtid=41">
                <mn:Txt ID="Txt13" runat="server" ResourceConstant="TXT_SUBSCRIBE_BTN" />
            </a>
        </div>
    </div>
    <div id="leftbox-2" class="box-content">
        <div class="newtext-matches-btn">
            <mn:Txt ID="Txt14" runat="server" ResourceConstant="TXT_BACK_TO_SITE_BTN" />
        </div>
        <div class="disabled-matches-btn">
            <mn:Txt ID="Txt15" runat="server" ResourceConstant="TXT_BACK_TO_SITE_BTN" />
        </div>
        <div class="enabled-matches-btn">
            <asp:HyperLink runat="server" ID="lnkContinue">
                <mn:Txt ID="Txt16" runat="server" ResourceConstant="TXT_BACK_TO_SITE_BTN" />
            </asp:HyperLink>
        </div>
    </div>
    </div> </asp:PlaceHolder>
<%= GetForcedPageCode("End") %>


<script type="text/javascript">
    //New implementation of the force page (April 2015) on top of the current one.
    var regionResultCityList = [];
    var israeliCitiesObj = {};

    $j(function () {
        var $cp = '.fill-details-box #complete-profile.circles '; //complete registarion prefix selector

        $j($cp + '.category .send-attribute').each(function () {
            if ($j(this).find('ul li').length > 5) {
                $j(this).find('.scroll').show();
            }
        });

        $j($cp + '.category .send-attribute .scroll.up').mousedown(function (e) {
            e.preventDefault();
            var content = $j(this).next('ul');
            setTimeout(function () { content.animate({ scrollTop: content.scrollTop() - 60 }) }, 10);
        });
        $j($cp + '.category .send-attribute .scroll.down').mousedown(function (e) {
            e.preventDefault();
            var content = $j(this).prev('ul');
            setTimeout(function () { content.animate({ scrollTop: content.scrollTop() + 60 }) }, 10);
        });

        function toggleVisibilityAutoCompleteInputs(israelAC) {
            //.txt-edit-city //AC for all the rest places
            //#israeliRegionsDD //AC for israeli places 
            //AC = Auto Complete
            $othersAC = $j('.txt-edit-city');
            $israelAC = $j('#israeliRegionsInput');

            if (israelAC) { 
                $othersAC.hide(); $israelAC.show();
            } else {
                $othersAC.show(); $israelAC.hide();
            }
        };

        function getAllPlacesDOM(selector) {
            var options = $j(selector);
            var places = [];
            options.each(function (i) {
                places.push({ id: $j(this).val(), name: $j(this).text() });
            });

            return places;
        };

        function setCountriesDOM() {
            var countries = getAllPlacesDOM('select.txt-edit-country option');
            var list = $j('.country-select ul.list');
            setListItemsDOM(countries, list);
        };

        function setRegionsDOM() {
            var regions = getAllPlacesDOM('select.ddl-edit-state option');
            var list = $j('.region-select ul.list');
            setListItemsDOM(regions, list);
        };

        function setListItemsDOM(items, list) {
            $j.each(items, function (i, item) {
                list.append('<li value=' + item.id + '>' + item.name + '</li>');
            });
        };

        function saveIsraeliRegionsDOM() {
            $j('select.ddl-edit-state option').appendTo('#israeliRegionsDD');
        };

        function setSelectDefaults() {
            var $page = '.fill-details-box #complete-profile.circles';

            //Set selected country
            var $selectCountry = $j($page + ' .item-title-select .txt-edit-country option:selected');
            var $titleCountry = $j($page + ' #top-row .custom .country-select .control .title');
            var countrySelectedName = $selectCountry.text();
            var countrySelectedValue = $selectCountry.val();
            $titleCountry.text(countrySelectedName);

            //Set selected region
            var $selectRegion = $j($page + ' .item-title-select .ddl-edit-state option:selected');
            var $titleRegion = $j($page + ' #top-row .custom .region-select .control .title');
            var regionSelectedName = $selectRegion.text();
            var regionSelectedValue = $selectRegion.val();
            $titleRegion.text(regionSelectedName);
        };

        function setNativeSelectbox(selector, placeID) {
            $j(selector + ' option').attr("selected", false);
            $j(selector).val(placeID).trigger('change');
            $j(selector + ' option[value=' + placeID + ']').attr("selected", "selected");
        };

        function bindEvents() {
            var $toprowSelector = '.fill-details-box #complete-profile.circles #top-row';
            var $categorySelector = '.fill-details-box #complete-profile.circles .category';
            var $listLi = $j($toprowSelector + ' ul.list li');
            var $SelectBox = $j($toprowSelector + ' .select-list .control');
            var $catListLi = $j($categorySelector + ' .inner-row ul li');
            var $religionSelect = $j($toprowSelector + ' .custom .religion-select');

            // Top row select boxes functionality
            $listLi.on("click", function () {
                var country = $j(this).text();
                $listLi.removeClass('selected');
                $j(this).addClass('selected');
                $j(this).parent('.list').siblings('.control').find('.title').text(country);

                //select the matched option in the native select box (which is hidden)
                var countryID = $j(this).val();
                var currentList = $j(this).parents('.select-list').attr('class').split(' ')[1];

                if (currentList == 'country-select') {
                    var countrySelector = '.item-title-select select.txt-edit-country';
                    setNativeSelectbox(countrySelector, countryID);

                    //If the country is Israel, display autocomplete input for Israeli places, otherwise display AC for all the others.
                    countryID == 105 ? toggleVisibilityAutoCompleteInputs(true) : toggleVisibilityAutoCompleteInputs(false);
                } else if (currentList == 'region-select') {
                    var regionSelector = '.item-title-select select.ddl-edit-state';
                    setNativeSelectbox(regionSelector, countryID);
                }
                $j(this).parent('ul.list').hide();

                // Match the native religion select box with the custom one.
                var $religionNativeSelect = $religionSelect.find('select');
                $religionNativeSelect.find('option:selected').removeAttr('selected');
                $religionNativeSelect.find('option[value=' + countryID + ']').attr('selected', 'selected');
                $religionNativeSelect.val(countryID).trigger('change');

                // Placeholder attribute: jQuery Fix for All Browsers
                $j('[placeholder]').focus(function () {
                    var input = $j(this);
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                        input.removeClass('placeholder');
                    }
                }).blur(function () {
                    var input = $j(this);
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.addClass('placeholder');
                        input.val(input.attr('placeholder'));
                    }
                }).blur();
            });           

            // Categories (circles) functionality
            $catListLi.on("click", function () {
                var catKey = $j(this).parents('.send-attribute').attr('key');
                
                // "Paint" the current category circle (as selected).
                $j($categorySelector + ' .cat-title .item-title[key=' + catKey + ']').addClass('cat-selected');

                // Trigger SAVE button functionality.
                showBtnTextboxFilled($j(this));
            });

            //Display drop down/select box options
            $SelectBox.on("click", function () {
                var selectID = $j(this).parents('.select-list').attr('id');
                if (selectID == 'country-dd-custom') {
                    $j('#region-dd-custom').hide();
                    $j('#first-row').removeClass('gap');
                }

                $j(this).siblings('.list').slideToggle("fast", function () {
                    // Animation complete.
                });
            });

            //Set default religion text to Secular
            var selectedReligionTxt = $religionSelect.find('.list li[class=selected]').text();
            $religionSelect.find('.title').text(selectedReligionTxt);
        };

        function setAutoCompleteCities() {
            //populate autocomplete for cities
            if (EditLocationObj.AccountCountryRegionID == "105") { //ISRAEL
                var $toprowSelector = '.fill-details-box #complete-profile.circles #top-row';
                var countrySelectedVal = $j($toprowSelector + ' .item-title-select .txt-edit-country').val();

                if (countrySelectedVal == 105) {
                    var regionsSelector = $j($toprowSelector + ' .item-title-select .ddl-edit-state option');
                    var mapRegions = getAllPlacesDOM(regionsSelector);
                    var $list = [];
                    var $listConcat = '';

                    $j.each(mapRegions, function (i, item) {
                        //item.id == region ID לדוגמא: דרום
                        EditLocation_LoadCityAutoComplete(item.id, '', true, null, true);
                    });
                    $j.each(regionResultCityList, function (i, item) {
                        $listConcat += item.concat(',');
                    });

                    $list = $listConcat.split(','); //Array of all israeli places (יישובים)
                    EditLocation_LoadCityAutoComplete(105, "", true, $list);
                }
            }
            else if (EditLocationObj.AccountCountryRegionID != "" && EditLocationObj.AccountCountryRegionID != "38" && EditLocationObj.AccountCountryRegionID != "223") {
                if (EditLocationObj.InitialCityParentRegionID != "") {
                    EditLocation_LoadCityAutoComplete(EditLocationObj.InitialCityParentRegionID, "");
                }
            }
        };

        function checkCategoriesSelected() {
            var selectedAttrCount = jQuery(".send-attribute ul li.selected").length;
            if (selectedAttrCount == 7) {
                showBtn();
                return false;
            } else {
                jQuery("#send-attr-btn").removeClass("enabled");
            }
        };

        function getRegionsByParentRegionID() {
            //get new city list
            $j.ajax({
                type: "POST",
                url: "/Applications/API/RegionService.asmx/GetRegionCities",
                data: "{parentRegionID:" + parentRegionID + ", description: \"" + description + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var regionResult = (typeof result.d == 'undefined') ? result : result.d;

                    if (regionResult.Status == 2) { 

                    }
                    else {
                        //$j('#errorMessage').show().html(regionResult.StatusMessage);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //
                }
            });
        };

        function init() {
            setCountriesDOM();
            setRegionsDOM();
            setSelectDefaults();
            setAutoCompleteCities();
            checkCategoriesSelected();
            bindEvents();

            $j('.txt-edit-city').attr('placeholder', 'עיר');
            $j('.txt-edit-zipcode').attr('placeholder', 'מיקוד');

            //copy the value of the default city that is being passed from a scenario, unless there's a city default that is being set elsewhere.            
            var defaultCityStr = $j('.txt-edit-city').val();
            $j('#israeliRegionsInput').val(defaultCityStr); 
        };

        //Location related
        $j('#' + EditLocationObj.ddlEditCountry).change(function () {
            //alert('hi from region country change');
            EditLocation_CountryChanged("", "");
            return false;
        });


        $j('#' + EditLocationObj.txtEditZipCode).keyup(function () {
            EditLocation_ZipCodeChanged();
            return false;
        });

        $j('#' + EditLocationObj.ddlEditState).change(function () {
            var newStateRegionID = $j('#' + EditLocationObj.ddlEditState).val();

            $j('#' + EditLocationObj.txtEditCity).val('');
            EditLocation_LoadCityAutoComplete(newStateRegionID, "");

            return false;
        });

        $j('#' + EditLocationObj.lnkCityLookup).click(function () {
            try {
                var parentRegionID = $j('#' + EditLocationObj.ddlEditCountry).val();
                if ($j('#' + EditLocationObj.ddlEditState).is(':visible')) {
                    parentRegionID = $j('#' + EditLocationObj.ddlEditState).val();
                }
                window.open('/Applications/MemberProfile/CityList.aspx?plid=' + EditLocationObj.BrandID
                    + '&CityControlName=' + EditLocationObj.txtEditCity + '&ParentRegionID=' + parentRegionID, '', 'height=415px,width=500px,scrollbars=yes');
            }
            catch (e) {
                if (EditLocationObj.IsVerbose) { alert(e); }
            }
            return false;
        });

        function EditLocation_CountryChanged(preSelectedStateRegionID, preSelectedCityName) {
            var newCountryRegionID = $j('#' + EditLocationObj.ddlEditCountry).val();
            var $toprowSelector = '.fill-details-box #complete-profile.circles #top-row';
            var customStateList = $j($toprowSelector + ' .select-list.region-select');
            var customStateListOptions = customStateList.find('.list');

            if (newCountryRegionID == "38" || newCountryRegionID == "223") {
                //USA or Canada enters in zip code for profile
                $j('#' + EditLocationObj.txtEditZipCode).val('');
                var ddlCityForZip = $j('#' + EditLocationObj.ddlEditCityForZip);
                ddlCityForZip.empty();
                $j('#' + EditLocationObj.PanelEditByZipCode).show();
                $j('#' + EditLocationObj.pnlEditByCityForZip).hide();
                $j('#' + EditLocationObj.PanelEditByStateCity).hide();
                customStateList.hide();
            }
            else {
                $j('#' + EditLocationObj.PanelEditByZipCode).hide();
                $j('#' + EditLocationObj.pnlEditByCityForZip).hide();
                $j('#' + EditLocationObj.PanelEditByStateCity).show();

                if (newCountryRegionID == "105") {
                    customStateList.hide();
                    toggleVisibilityAutoCompleteInputs(true);
                }            
                //get new state list, if applicable
                $j.ajax({
                    type: "POST",
                    url: "/Applications/API/RegionService.asmx/GetRegionStates",
                    data: "{countryRegionId:" + newCountryRegionID + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        var regionResult = (typeof result.d == 'undefined') ? result : result.d;
                        if (regionResult.Status == 2) {
                            var divState = $j('#' + EditLocationObj.PanelEditState);
                            var ddlState = $j('#' + EditLocationObj.ddlEditState);

                            ddlState.empty();
                            customStateListOptions.empty();

                            $j.each(regionResult.StatesList, function (key, value) {
                                $j("<option>").attr("value", key).text(value).appendTo(ddlState);
                                $j("<li>").attr("value", key).text(value).appendTo(customStateListOptions);
                            });
                            var listFirstOption = customStateListOptions.find('li:eq(0)').text();
                            customStateList.find('.title').text(listFirstOption);

                            $j('#region-dd-custom ul.list li').on("click", function () {
                                var country = $j(this).text();
                                $j('#region-dd-custom ul.list li').removeClass('selected');
                                $j(this).addClass('selected');
                                $j(this).parent('.list').siblings('.control').find('.title').text(country);

                                //select the matched option in the native select box (which is hidden)
                                var countryID = $j(this).val();
                                setNativeSelectbox('.item-title-select select.ddl-edit-state', countryID);
                                $j(this).parent('.list').slideUp();
                                //$j('#region-dd-custom').hide();
                                //$j('#first-row').removeClass('gap');
                            });

                            if (regionResult.hasStates) {
                                divState.show();
                                if (preSelectedStateRegionID != "") {
                                    ddlState.val(preSelectedStateRegionID);
                                }
                                EditLocation_LoadCityAutoComplete(ddlState.val(), "");

                                if (newCountryRegionID != "105") {
                                    $j($toprowSelector + ' .select-list.region-select').show();
                                    $j('#first-row').addClass('gap');
                                }
                            } else {
                                divState.hide();
                                EditLocation_LoadCityAutoComplete(newCountryRegionID, "");
                            }

                            console.log('preSelectedCityName 1: ', preSelectedCityName);
                            if (preSelectedCityName != "") {
                                console.log('preSelectedCityName 2: ', preSelectedCityName);
                                $j('#' + EditLocationObj.txtEditCity).val(preSelectedCityName);
                                $j('#israeliRegionsInput').val(preSelectedCityName);
                            }
                            else {
                                $j('#' + EditLocationObj.txtEditCity).val('');
                                $j('#israeliRegionsInput').val('');
                            }

                            //$j('#errorMessage').hide();
                        }
                        else {
                            //$j('#errorMessage').show().html(regionResult.StatusMessage);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (EditLocationObj.IsVerbose) { alert(textStatus); }
                    }
                });
            }
        }

        function EditLocation_ZipCodeChanged() {
            var newZipCode = $j('#' + EditLocationObj.txtEditZipCode).val();
            var newCountryRegionID = $j('#' + EditLocationObj.ddlEditCountry).val();

            var divCityForZip = $j('#' + EditLocationObj.pnlEditByCityForZip);

            //Set cities if Canada postal code is at least 6 or USA and postal code is at least 5
            if ((newCountryRegionID == "38" && newZipCode.length >= 6) || (newCountryRegionID == "223" && newZipCode.length >= 5)) {
                var zipCodeData = "{zipCode:'" + newZipCode + "'}";
                //get new City for Zip list, if applicable
                $j.ajax({
                    type: "POST",
                    url: "/Applications/API/RegionService.asmx/GetCities",
                    data: zipCodeData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        var regionResult = (typeof result.d == 'undefined') ? result : result.d;
                        var ddlCityForZip = $j('#' + EditLocationObj.ddlEditCityForZip);
                        ddlCityForZip.empty();

                        regionResult = $j.parseJSON(regionResult);

                        var i = 0;
                        $j.each(regionResult, function (index, element) {
                            i = i + 1;
                            $j("<option>").attr("value", element.RegionID).text(element.Description).appendTo(ddlCityForZip);
                        });

                        //Display the City for Zip dropdown if more than one value:
                        if (i > 1) {
                            //                      ddlCityForZip.prepend("<option value='' selected='selected'></option>");  //Only need this if defaulting to no value
                            divCityForZip.show();
                        }
                        else {
                            divCityForZip.hide();
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (EditLocationObj.IsVerbose) { alert(textStatus); }
                    }
                });

            }
            else {
                divCityForZip.hide();
            }
        }

        function stringToAscii(s) {
            var ascii = "";
            if (s.length > 0)
                for (i = 0; i < s.length; i++) {
                    var c = "" + s.charCodeAt(i);
                    while (c.length < 3)
                        c = "0" + c;
                    ascii += c;
                }
            return (ascii);
        }

        function queryObject(needle, set) {
            var regionID;
            jQuery.each(set, function (index, value) {
                jQuery.each(value.data, function (i, city) {
                    if (city === needle) {
                    regionID = value.area;
                        return;
                    }
                });                
            });
            return regionID;
        }

        function EditLocation_LoadCityAutoComplete(parentRegionID, description, list, source, mapRegions) {
            var ajaxVal = (list === true && list !== undefined) ? false : true;
            var areaID = parentRegionID;
            //get new city list
            $j.ajax({
                type: "POST",
                url: "/Applications/API/RegionService.asmx/GetRegionCities",
                data: "{parentRegionID:" + parentRegionID + ", description: \"" + description + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: ajaxVal,
                cache: ajaxVal,
                success: function (result) {
                    var regionResult = (typeof result.d == 'undefined') ? result : result.d;

                    if (regionResult.Status == 2) {
                        if (list === true && list !== undefined) { //if a custom json object list of places is being passed
                            if (mapRegions) {
                                israeliCitiesObj[areaID] = { area: areaID, data: regionResult.CityList };
                            }
                            regionResultCityList.push(regionResult.CityList);

                            if ($j.isArray(source)) {
                                $j('#israeliRegionsInput').autocomplete({
                                    source: source,
                                    minLength: 3,
                                    select: function (event, ui) {
                                        var regionID = queryObject(ui.item.value, israeliCitiesObj); //e.g. areaID
                                        $j('.ddl-edit-state option:selected').removeAttr('selected');
                                        $j('.ddl-edit-state').val(regionID).change();
                                        $j('.ddl-edit-state option[value=' + regionID + ']').attr('selected', 'selected');
                                        
                                    }
                                });
                            }
                        } else if (regionResult.CityList != null) {
                            $j('#' + EditLocationObj.txtEditCity).autocomplete({
                                source: regionResult.CityList,
                                minLength: 3
                            });

                            $j('.ui-autocomplete').click(function (e) {
                                e.stopPropagation();
                            });
                        }

                        //$j('#errorMessage').hide();
                    }
                    else {
                        //$j('#errorMessage').show().html(regionResult.StatusMessage);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (EditLocationObj.IsVerbose) { alert(textStatus); }
                }
            });
        }

        init();
    });
</script>
