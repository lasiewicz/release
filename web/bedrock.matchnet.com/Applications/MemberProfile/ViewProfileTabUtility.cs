﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Web.Applications.CompatibilityMeter;
using Matchnet.Web.Applications.Home;
using Matchnet.Web.Framework;
using System.Reflection;
using Matchnet.MemberSlideshow.ServiceAdapters;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.Web.Framework.Search;
using System.Text;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.MemberProfile
{
    public class ViewProfileTabUtility
    {
        #region Static Methods
        /// <summary>
        /// Sets the member ID, ensuring that it exists, otherwise user will be redirected.
        /// </summary>
        public static int GetMemberIdForProfileView(ContextGlobal g, bool doNotRedirect)
        {
            int _MemberID = 0;

            try
            {
                if (IsKeywordSearchNavRequest())
                {
                    _MemberID = GetMemberIdFromKeywordSearch(g);
                }
                else if (IsSlideshowNavRequest())
                {
                    if (IsSlideshowFilmstripNavRequest())
                    {
                        _MemberID = GetMemberIDFromSlideshowFilmstrip(g, doNotRedirect);
                    }
                    else if (IsSlideshowFilmstripMOLNavRequest())
                    {
                        _MemberID = GetMemberIDFromSlideshowFilmstripMOL(g, doNotRedirect);
                    }
                    else
                    {
                        _MemberID = GetMemberIDFromSlideshow(g, doNotRedirect);
                    }
                }
                else if (HttpContext.Current.Request.Params.Get("memberId") != null)
                {	// Viewing specific profile
                    _MemberID = Conversion.CInt(HttpContext.Current.Request.Params.Get("MemberId"));
                }
                else
                {
                    _MemberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID); // Viewing their own.
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                _MemberID = 0;
            }

            return _MemberID;

        }

        public static Int32 GetMemberIdFromKeywordSearch(ContextGlobal g)
        {
            Int32 ordinal;

            Int32.TryParse(HttpContext.Current.Request.Params["Ordinal"], out ordinal);

            var searchParameters = SearchPageHelper.GetKeywordSearchPreferences(HttpContext.Current.Request);

            var results = KeywordSearcher.GetKeywordSearchResults(
                g.Member == null ? Constants.NULL_INT : g.Member.MemberID,
                Int32.Parse(searchParameters.AgeRange),
                searchParameters.Location,
                String.Join(
                    Framework.Search.Constants.Literals.Two,
                    new[]
					{
						searchParameters.Seeker, 
						searchParameters.Seeking
					}
                ),
                searchParameters.Query,
                ordinal - 1,
                1,
                g.Brand.Site.Community.CommunityID
            );

            if (results == null || results.total < 1 || results.getSearchResult.Length < 1) return 0;

            Int32 memberId;

            Int32.TryParse(results.getSearchResult[0].memberId, out memberId);

            return memberId;
        }

        public static Int32 GetMemberIDFromSlideshow(ContextGlobal g, bool doNotRedirect)
        {
            int VMID = Conversion.CInt(HttpContext.Current.Request["VMID"]);
            Int32 slideshowMemberID = 0;
            if (g != null && g.Member != null)
            {
                Slideshow slideshow = MemberSlideshowSA.Instance.GetSlideshow(g.Member.MemberID, g.Brand);
                IMember slideshowMember = MemberSlideshowSA.Instance.GetValidMemberFromSlideshow(slideshow, g.Brand, false);

                if (VMID == slideshowMember.MemberID)
                {
                    slideshowMember = MemberSlideshowSA.Instance.GetValidMemberFromSlideshow(slideshow, g.Brand, true);
                }

                if (slideshowMember == null)
                {
                    if (!doNotRedirect)
                        g.Transfer("http://" + HttpContext.Current.Request.Url.Host + "/");
                }
                else
                {
                    slideshowMemberID = slideshowMember.MemberID;
                }
            }

            return slideshowMemberID;
        }

        public static Int32 GetMemberIDFromSlideshowFilmstrip(ContextGlobal g, bool doNotRedirect)
        {
            int memberID = 0;
            if (g != null && g.Member != null)
            {
                Int32 ordinal = 1;

                Int32.TryParse(HttpContext.Current.Request.Params["Ordinal"], out ordinal);
                if (ordinal <= 0)
                {
                    ordinal = 1;
                }

                MatchnetQueryResults searchResults =  HomeUtil.GetFilmstripMatchesProfile(g.Member.MemberID, g.Brand, g.SearchPreferences, ordinal, 1, true);
                if (searchResults != null && searchResults.Items.Count > 0)
                {
                    memberID = searchResults.Items[0].MemberID;
                }
            }

            if (memberID <= 0 && !doNotRedirect)
            {
                g.Transfer("http://" + HttpContext.Current.Request.Url.Host + "/Applications/HotList/SlideShow.aspx");
            }

            return memberID;
        }

        public static Int32 GetMemberIDFromSlideshowFilmstripMOL(ContextGlobal g, bool doNotRedirect)
        {
            int memberID = 0;
            if (g != null && g.Member != null)
            {
                Int32 ordinal = 1;

                Int32.TryParse(HttpContext.Current.Request.Params["Ordinal"], out ordinal);
                if (ordinal <= 0)
                {
                    ordinal = 1;
                }

                MOCollection mocollection = null;
                Matchnet.MembersOnline.ValueObjects.MOLQueryResult molResults = HomeUtil.GetFilmstripMembersOnlineProfiles(g.Member, g.Brand, g.SearchPreferences, g.Session, ordinal, 1, true, out mocollection);

                if (molResults != null && molResults.ToArrayList().Count > 0)
                {
                    memberID = Convert.ToInt32(molResults.ToArrayList()[0]);
                }
            }

            if (memberID <= 0 && !doNotRedirect)
            {
                g.Transfer("http://" + HttpContext.Current.Request.Url.Host + "/Applications/HotList/SlideShow.aspx");
            }

            return memberID;
        }

        public static Boolean IsReverseSearchNavRequest()
        {
            Boolean isNavRequest;

            return Boolean.TryParse(
                        HttpContext.Current.Request.Params[Framework.Search.Constants.Parameters.ReverseSearchNavigationRequest],
                        out isNavRequest) && isNavRequest;
        }

        public static Boolean IsKeywordSearchNavRequest()
        {
            Boolean isNavRequest;

            return Boolean.TryParse(
                        HttpContext.Current.Request.Params[Framework.Search.Constants.Parameters.KeywordNavigationRequest],
                        out isNavRequest) && isNavRequest;
        }

        public static Boolean IsSlideshowNavRequest()
        {
            Boolean isSlideshowNavRequest;

            return Boolean.TryParse(
                        HttpContext.Current.Request.Params[Framework.Search.Constants.Parameters.SlideshowNavigationRequest],
                        out isSlideshowNavRequest) && isSlideshowNavRequest;
        }

        public static Boolean IsSlideshowFilmstripNavRequest()
        {
            Boolean isSlideshowFilmstripNavRequest;

            return Boolean.TryParse(
                        HttpContext.Current.Request.Params[Framework.Search.Constants.Parameters.SlideShowFilmstripNavigationRequest],
                        out isSlideshowFilmstripNavRequest) && isSlideshowFilmstripNavRequest;
        }

        public static Boolean IsSlideshowFilmstripMOLNavRequest()
        {
            Boolean isSlideshowFilmstripNavRequest;

            return Boolean.TryParse(
                        HttpContext.Current.Request.Params[Framework.Search.Constants.Parameters.SlideShowFilmstripMOLNavigationRequest],
                        out isSlideshowFilmstripNavRequest) && isSlideshowFilmstripNavRequest;
        }

        private static Int32 GetPageNumber()
        {
            Int32 pageFromParameter;

            if (Int32.TryParse(HttpContext.Current.Request.Params[
                    Framework.Search.Constants.ReverseSearch.Parameter.Page],
                    out pageFromParameter))
            {
                return pageFromParameter < 1 ? Framework.Search.Constants.Paging.DefaultPage : pageFromParameter;
            }
            return Framework.Search.Constants.Paging.DefaultPage;
        }

        private static Boolean OnlyPhotos
        {
            get
            {
                return String.IsNullOrEmpty(SearchPageHelper.GetParamValue(
                    Framework.Search.Constants.ReverseSearch.Parameter.OnlyPhotos, HttpContext.Current.Request)) == false;
            }
        }

        public static string getDateDisplay(DateTime pDate, ContextGlobal g)
        {
            if ((int)Matchnet.Language.Hebrew == g.Brand.Site.LanguageID)
            {
                string month = string.Empty;
                string day = string.Empty;

                if (pDate.Month < 10)
                {
                    month = "0" + pDate.Month;
                }
                else
                {
                    month = pDate.Month.ToString();
                }

                if (pDate.Day < 10)
                {
                    day = "0" + pDate.Day;
                }
                else
                {
                    day = pDate.Day.ToString();
                }

                return day + "/" + month + "/" + pDate.Year;
            }
            else
            {
                return pDate.ToShortDateString();
            }
        }

        /// <summary>
        /// Gets the active tab based on URL parameter, if any, or returns default active tab
        /// </summary>
        /// <returns></returns>
        public static ProfileTabEnum DetermineActiveTab()
        {
            return DetermineActiveTab(ProfileTabEnum.Basic);
        }

        /// <summary>
        /// Gets the active tab based on URL parameter, if any, or returns default active tab
        /// </summary>
        /// <returns></returns>
        public static ProfileTabEnum DetermineActiveTab(ProfileTabEnum defaultTabType)
        {
            if (defaultTabType == ProfileTabEnum.None)
                defaultTabType = ProfileTabEnum.Basic;

            ProfileTabEnum activeTab = defaultTabType;

            if (HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_NAME_PROFILETAB] != null)
            {
                //get active tab from url querystring
                try
                {
                    activeTab = (ProfileTabEnum)Enum.Parse(typeof(ProfileTabEnum), HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_NAME_PROFILETAB], true);

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }

            return activeTab;
        }

        private static string escapeCharsForJavascript(string s)
        {
            s = s.Replace("&quot;", "\"");
            return FrameworkGlobals.JavaScriptEncode(s);
        }

        public static string GetEnumDescription(Enum value)
        {
            string ret = string.Empty;

            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
                ret = attributes[0].Description;
            else
                ret = value.ToString();

            return ret;
        }

        /// <summary>
        /// Standard alphanumeric check - letters and numbers only
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsAlphanumeric(string text)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(text, "^[a-zA-Z0-9א-ת]*$"))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Special alphanumeric check - letters, numbers, hyphen, and spaces
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsAlphanumericForName(string text)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(text, "^[a-zA-Z- 0-9א-ת]*$"))
            {
                return false;
            }

            return true;
        }

        public static bool IsActivityLogViewProfileEnabled(ContextGlobal g)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_VIEWPROFILE_ACTIVITYLOG", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        public static bool IsEmptyEssaySuppressEnabled(ContextGlobal g)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_PROFILE_VIEW_EMPTY_ESSAY_SUPPRESS", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        public static bool IsSuppressEmptyAttributesEnabled(ContextGlobal g)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SUPPRESS_EMPTY_ATTRIBUTES", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        public static bool IsEssayRequestEmailEnabled(ContextGlobal g)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_PROFILE_ESSAY_REQUEST_EMAIL", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        #endregion

    }

    /// <summary>
    /// Enums for representing the various profile tabs
    /// </summary>
    public enum ProfileTabEnum
    {
        None = 0,
        [Description("Basic Tab")]
        Basic = 1,
        [Description("Lifestyle Tab")]
        Lifestyle = 2,
        [Description("Interests Tab")]
        Interests = 3,
        [Description("Relationship Tab")]
        Relationship = 4,
        [Description("Photo Tab")]
        Photo = 5,
        [Description("ColorCode Tab")]
        ColorCode = 6,
        [Description("Questions Tab")]
        Questions = 7,
        [Description("In My Own Words Tab")]
        Essays = 8, //aka. In my Own Words
        [Description("Icebreakers Tab")]
        Icebreakers = 9,
        [Description("More About Me Tab")]
        MoreEssays = 10, //aka. More About Me
        [Description("The Details Tab")]
        TheDetails = 11,
        [Description("JMeter Tab")]
        JMeter = 12
    }

    /// <summary>
    /// Enum representing the different groupings of profile data,
    /// allows us to have data groups independent from tabs or any other containers
    /// </summary>
    public enum ProfileDataGroupEnum
    {
        None = 0,
        Basic = 1,
        Dealbreakers = 2,
        TheDetails = 3,
        Essays = 4,
        MoreEssays = 5,
        IdealMatch = 6,
        VisitorViewEssay = 7,
        LikesInterests = 8
    }

    /// <summary>
    /// Enum for the different sections within a data group, primarily for the sake
    /// of giving a section a defined name
    /// </summary>
    public enum ProfileDataSectionEnum
    {
        None = 0,
        BasicInfo = 1,
        Dealbreakers = 2,
        Essays = 3,
        PhysicalInfo = 4,
        Lifestyle = 5,
        Background = 6,
        IdealMatch = 7,
        MoreEssays = 8,
        Interests = 9
    }

    /// <summary>
    /// Specifies whose profile
    /// </summary>
    public enum WhoseProfileEnum
    {
        None = 0,
        Self = 1,
        Other = 2
    }

    /// <summary>
    /// List of various basic edit control types to control what edit control to use for editing
    /// Custom type indicates that the code will determine a specific control to use based on attribute
    /// </summary>
    public enum EditControlTypeEnum
    {
        None = 0,
        Textbox = 1,
        Radio = 2,
        Checkbox = 3,
        Dropdown = 4,
        MultiSelect = 5,
        TextArea = 6,
        Custom = 20
    }

    /// <summary>
    /// List of misc properties that can be used by edit controls to determine behaviors within
    /// </summary>
    public enum EditControlPropertyEnum
    {
        None = 0,
        Double = 1
    }

    /// <summary>
    /// List of basic data types for attributes to control editing process
    /// </summary>
    public enum EditDataTypeEnum
    {
        None = 0,
        Int = 1,
        Mask = 2,
        Text = 3,
        Date = 4,
        Essay = 5
    }


}
