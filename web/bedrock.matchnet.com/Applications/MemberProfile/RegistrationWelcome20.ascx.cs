﻿using System;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Enumerations;
using Spark.Common;
using Spark.Common.MemberLevelTracking;

namespace Matchnet.Web.Applications.MemberProfile
{
    public partial class RegistrationWelcome20 : FrameworkControl
    {
        private const string QSPARAM_DESTINATIONMEMBERID = "destinationmemberid";


        #region Event handlers
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                g.LayoutTemplateBase.DisableGamOverlay();
                
                if (Request[WebConstants.URL_PARAMETER_NAME_SPARKWS_CONTEXT] == "true")
                {
                    Page.RegisterHiddenField(WebConstants.URL_PARAMETER_NAME_SPARKWS_CONTEXT, "true");
                }
                
                string registrationConfirmationType = RuntimeSettings.GetSetting("T_N_T_REG_CONFIRM_MBOX_NAME",
                                                                                 g.Brand.Site.Community.CommunityID,
                                                                                 g.Brand.Site.SiteID);
                if (!string.IsNullOrEmpty(registrationConfirmationType))
                {
                    PlaceHolderOmnitureMboxRegEnd.Visible = true;
                    plcMBoxDefaultStart.Visible = true;
                    plcMBoxDefaultEnd.Visible = true;

                    if (RegistrationWelcomePageThrottler.IsFeatureEnabled(g.Member, g))
                    {
                        plcMBoxDefaultEnd.Visible = false;
                        g.SetTopAuxMenuVisibility(false, true);
                        g.HeaderControl.ShowBlankHeader();
                        g.FooterControl20.HideFooterLinks();
                        g.FooterControl20.Visible = false;
                        g.HeaderControl.Visible = false;
                        plcRedirectToSubPage.Visible = true;
                    }

                    this.Page.RegisterClientScriptBlock("RegistrationConfirmationType", "<script type='text/javascript'>var registrationConfirmationType='" + registrationConfirmationType + "';</script>");
                }

                /*
                // delegation by community not necessary per Lybrand/Viney, all sites utilize SP
                string pagename = Matchnet.Content.ServiceAdapters.PageConfigSA.Instance.GetPageName(g.GetDestinationURL());
                if (!string.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION]))
                {
                    phRegistrationWelcome.Controls.Add(LoadControl("/Applications/MemberProfile/Registration/RegistrationConfirmationFacebook.ascx"));
                }
                else
                {
                    if ((g.RecipientMemberID > 0) && (pagename == "tease" || pagename == "view" || pagename == "upinstantcommunicator" || pagename == "compose"
        || pagename == "viewprofile" || pagename == "astroview" || pagename == "categories")
        && Conversion.CInt(Request[WebConstants.URL_PARAMETER_NAME_FIVEDFT]) != 1) //Show 5 day free trial banner on reg conf. page?
                    {
                        phRegistrationWelcome.Controls.Add(LoadControl("/Applications/MemberProfile/Registration/RegistrationConfirmation20.ascx"));
                    }
                    else
                    {
                        if (pagename == "subscribe")
                        {

                            phRegistrationWelcome.Controls.Add(LoadControl("/Applications/MemberProfile/Registration/RegistrationConfirmation20.ascx"));
                        }
                        else
                        {
                            if (_g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                            {
                                phRegistrationWelcome.Controls.Add(LoadControl("/Applications/MemberProfile/Registration/RegistrationWelcome_1.ascx"));
                            }
                            else
                            {
                                phRegistrationWelcome.Controls.Add(LoadControl("/Applications/MemberProfile/Registration/RegistrationWelcome_SP20.ascx"));
                            }
                        }
                    }
                }*/

                if (_g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid || _g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                {
                    phRegistrationWelcome.Controls.Add(LoadControl("/Applications/MemberProfile/RegistrationWelcomeContent.ascx"));
                }

                g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_REGISTRATIONDATE, Convert.ToString(DateTime.Now), Matchnet.Session.ValueObjects.SessionPropertyLifetime.TemporaryDurable);

                UpdateMemberLevelTrackingAttributes();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// For Analytics
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                // Omniture Analytics - Event and property variable tracking
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    // Gender of the member
                    _g.AnalyticsOmniture.Prop18 = GetGender(_g.Member.GetAttributeInt(_g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK));
                    // Age
                    _g.AnalyticsOmniture.Prop19 = FrameworkGlobals.GetAge(_g.Member.GetAttributeDate(_g.Brand, WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString();
                    // Ethnicity
                    if (_g.Brand.Site.Community.CommunityID == (int)Matchnet.Web.Framework.WebConstants.COMMUNITY_ID.JDate)
                    {
                        _g.AnalyticsOmniture.Prop20 = ProfileDisplayHelper.GetOptionValue(_g.Member, _g, "JDateEthnicity", "JDateEthnicity");
                    }
                    else
                    {
                        _g.AnalyticsOmniture.Prop20 = ProfileDisplayHelper.GetOptionValue(_g.Member, _g, "Ethnicity", "Ethnicity");
                    }
                    // Region
                    // _g.AnalyticsOmniture.Prop21 = ProfileDisplayHelper.GetRegionDisplay(_g.Member, _g);
                    int languageID = _g.Brand.Site.LanguageID;
                    if (_g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
                    {
                        languageID = (int)Matchnet.Language.English;
                    }
                    g.AnalyticsOmniture.Prop21 = FrameworkGlobals.GetRegionString(_g.Member.GetAttributeInt(_g.Brand, "regionid"), languageID, false, true, false).Replace("\"", string.Empty);
                    // MemberID
                    _g.AnalyticsOmniture.Prop23 = _g.Member.MemberID.ToString();
                    // Subscriber Status -- This is also used on Logon.
                    _g.AnalyticsOmniture.Prop22 = (_g.Member.IsPayingMember(_g.Brand.Site.SiteID)) ? "Subscriber" : "Registered";
                    // Registration Complete
                    _g.AnalyticsOmniture.AddEvent("event8");
                    bool photoUploaded = Conversion.CBool(g.Session.GetString("REG_PHOTO_UPLOADED"));
                    if (photoUploaded)
                    {
                        _g.AnalyticsOmniture.AddEvent("event28");
                    }
                    g.Session.Remove("REG_PHOTO_UPLOADED");

                    // Does this page have mobile setup control
                    if (Conversion.CBool(g.Session.Get(WebConstants.SESSION_MOBILE_SETUP_START)))
                    {
                        _g.AnalyticsOmniture.AddEvent("event30");
                    }
                    g.Session.Remove(WebConstants.SESSION_MOBILE_SETUP_START);

                    if(!string.IsNullOrEmpty(g.Session[WebConstants.SESSION_PROPERTY_REG_SCENARIO_NAME]))
                    {
                        _g.AnalyticsOmniture.Evar48 = g.Session[WebConstants.SESSION_PROPERTY_REG_SCENARIO_NAME];
                        g.Session.Remove(WebConstants.SESSION_PROPERTY_REG_SCENARIO_NAME);
                    }

                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private string GetGender(int genderMask)
        {
            string gender;

            genderMask = genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE;

            gender = (genderMask == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) ? "Female" : "Male";

            return gender;
        }

        private string GetSeekingGender(int genderMask)
        {
            string gender;

            genderMask = genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_SEEKING_FEMALE;

            gender = (genderMask == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) ? "Female" : "Male";

            return gender;
        }

        private void UpdateMemberLevelTrackingAttributes()
        {
            g.Member.SetAttributeInt(g.Brand, Key.TrackingRegApplication.ToString(), (int)Spark.Common.MemberLevelTracking.Application.FullSite);
            g.Member.SetAttributeText(g.Brand, Key.TrackingRegOs.ToString(), MemberLevelTrackingHelper.GetOs(Request), TextStatusType.None);
            g.Member.SetAttributeText(g.Brand, Key.TrackingRegFormFactor.ToString(), MemberLevelTrackingHelper.GetFormFactor(Request), TextStatusType.None);
            
            MemberSA.Instance.SaveMember(g.Member);
        }


        #endregion

    }
}