﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using System.Web;
using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Search;
using Matchnet.Web.Framework.Ui;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.MemberSlideshow.ServiceAdapters;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using System.Text;
using Matchnet.Web.Framework.Ui.PageAnnouncements;
using Spark.Common.AccessService;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileTabConfig;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30;
using Matchnet.ActivityRecording.ValueObjects.Types;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Applications.Home;

namespace Matchnet.Web.Applications.MemberProfile
{
    /// <summary>
    /// Control to represent the new member profile page in SiteRedesign 2.0
    /// </summary>
    public partial class ViewTabbedProfile20 : BaseProfileViewControl
    {
        private ProfileTabEnum _activeTab = ProfileTabEnum.None;
        private int _MemberID;

        public int MemberID
        {
            get { return _MemberID; }
            set { _MemberID = value; }
        }
        #region Event Handlers
        /// <summary>
        /// Handler for Page Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoadPageContents();

                if (SettingsManager.GetSettingBool("SUBSCRIPTION_EXPIRATION_ALERT_SHOW_IN_PROFILE_PAGE", g.Brand))
                {
                    if (SubscriptionExpirationManager.Instance.DisplaySubscriptionExpirationAlert(g.Brand, g.Member, g))
                    {
                        SubscriptionExpirationAlert subscriptionExpirationAlert = Page.LoadControl("/Framework/Ui/PageAnnouncements/SubscriptionExpirationAlert.ascx") as SubscriptionExpirationAlert;
                        g.LayoutTemplateBase.AddPageAccouncementControl(subscriptionExpirationAlert);

                    }
                }

            }
            catch (ThreadAbortException tex)
            {
                System.Diagnostics.Debug.WriteLine(tex.Message);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);

                if (MemberProfile == null)
                {
                    TransferIfMemberDoesNotExist();
                }
                else
                {
                    TransferToUnavailablePage();
                }
            }
        }

        private Boolean IsAdminSuspendedMember()
        {
            var globalStatusMask = MemberProfile.GetAttributeInt(g.TargetBrand, "GlobalStatusMask", 0);
            return Matchnet.Lib.Util.Util.CBool((globalStatusMask & ConstantsTemp.ADMIN_SUSPEND), false);
        }

        private Boolean IsSelfSuspendedMember()
        {
            return MemberProfile.GetAttributeInt(g.TargetBrand, "SelfSuspendedFlag", 0) == 1;
        }

        private Boolean IsSuspendedMember()
        {
            return IsSelfSuspendedMember() || IsAdminSuspendedMember();
        }

        private void TransferToUnavailablePage()
        {
            g.Transfer(
                String.Join(
                    String.Empty,
                    new[]
					{
						"/Applications/MemberProfile/ProfileUnavailable.aspx?MemberID=",
						MemberProfile.MemberID.ToString(),
						ProfileDisplayHelper.GetURLParamsNeededByNextProfile(),
						"&",
						WebConstants.URL_PARAMETER_NAME_FROM_PROFILE_WITH_BTR_PREV,
						"=true"
					}
                )
            );
        }

        private void TransferToProfileForAnotherCommunity()
        {
            g.Transfer(
                "/Applications/MemberProfile/ProfileUnavailable.aspx?MemberID=" +
                MemberProfile.MemberID +
                ProfileDisplayHelper.GetURLParamsNeededByNextProfile()
            );
        }

        private void TransferIfMemberDoesNotExist()
        {
            if (MemberProfile == null) g.Transfer("/Default.aspx?RC=PAGE_MEMBER_PROFILE_TEMPORARILY_UNAVAILABLE");
        }

        private void LoadPageContents()
        {
            if (g == null) return;

            string mode = (string.IsNullOrEmpty(Request["Mode"])) ? "view" : Request["Mode"].ToLower();
            SetMemberIdForProfileView();

            CheckForMatchMailViewer();

            VisitorLimitHelper.CheckVisitorLimit(ref _g, VisitorLimitHelper.VisitorLimitTypeEnum.FullProfile);

            MemberProfile = (g != null && g.Member != null && g.Member.MemberID == _MemberID) ? g.Member : MemberSA.Instance.GetMember(_MemberID, MemberLoadFlags.None);

            TransferIfMemberDoesNotExist();

            if (!FrameworkGlobals.IsValidMember(MemberProfile, g.Brand) && (!"edit".Equals(mode)))
            {
                g.Transfer("/Applications/MemberProfile/ProfileUnavailable.aspx?MemberID=" + MemberProfile.MemberID + ProfileDisplayHelper.GetURLParamsNeededByNextProfile() + "&" + WebConstants.URL_PARAMETER_NAME_FROM_PROFILE_WITH_BTR_PREV + "=true");
                return;
            }


            // TT15768 - prep back link in article page
            if (g.Session["ArticleRefUrl"] != null && !g.Session["ArticleRefUrl"].Equals(String.Empty))
            {
                g.Session.Remove("ArticleRefUrl");
            }

            //Do not show profile unavailable page for suspended members when admin is trying to view the profile
            if (IsSuspendedMember() && !g.AdminImpersonate)
            {
                TransferToUnavailablePage();
                return;
            }

            if (IsBlocked())
            {
                TransferToUnavailablePage();
            }
            if (MemberProfile != null && MemberProfile.IsCommunityMember(g.TargetBrand.Site.Community.CommunityID) || ("edit".Equals(mode)))
            {
                DisplayMemberProfile();
            }
            else
            {
                TransferToProfileForAnotherCommunity();
                return;
            }
            
            insertPixel(); //output pixel for featured member / promotional profile tracking, if applicable

            BreadCrumbHelper.StaticSetBreadCrumbs(_MemberProfile.MemberID, Request, g);

            #region Toma Access Test
            //Toma Access Test
            bool IsTomaAccessTestOn = false;
            if (IsTomaAccessTestOn)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Is Access Read Kill Switch on: " + FrameworkGlobals.IsUPSAccessMasterReadKillSwitchEnabled().ToString());
                sb.AppendLine("<br/>Is Site using Access Privileges:" + FrameworkGlobals.IsUPSAccessEnabled(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID).ToString());
                sb.AppendLine("<br/>Is Site using Access Privileges v2:" + FrameworkGlobals.IsUPSAccessEnabled(Constants.NULL_INT, g.Brand.Site.SiteID, Constants.NULL_INT).ToString());
                sb.AppendLine("<br/>Is Paying Member:" + _MemberProfile.IsPayingMember(g.Brand.Site.SiteID).ToString());
                sb.AppendLine("<br/><br/>Access Privileges through Member.GetUnifiedAccessPrivilege()");
                AccessPrivilege ap = null;
                ap = MemberProfile.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (ap != null)
                {
                    sb.AppendLine("<br/>Basic Subscription: " + ap.EndDatePST);
                    sb.AppendLine("<br/>Basic Subscription InsertDate: " + ap.InsertDatePST);
                }
                ap = MemberProfile.GetUnifiedAccessPrivilege(PrivilegeType.HighlightedProfile, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (ap != null)
                {
                    sb.AppendLine("<br/>Highlight: " + ap.EndDatePST);
                }
                ap = MemberProfile.GetUnifiedAccessPrivilege(PrivilegeType.SpotlightMember, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (ap != null)
                {
                    sb.AppendLine("<br/>Spotlight: " + ap.EndDatePST);
                }
                ap = MemberProfile.GetUnifiedAccessPrivilege(PrivilegeType.AllAccess, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (ap != null)
                {
                    sb.AppendLine("<br/>AllAccess: " + ap.EndDatePST);
                }
                ap = MemberProfile.GetUnifiedAccessPrivilege(PrivilegeType.AllAccessEmails, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (ap != null)
                {
                    sb.AppendLine("<br/>AllAccessEmail: " + ap.RemainingCount.ToString());
                }
                ap = MemberProfile.GetUnifiedAccessPrivilege(PrivilegeType.JMeter, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (ap != null)
                {
                    sb.AppendLine("<br/>JMeter: " + ap.EndDatePST);
                }

                lblToma.Text = sb.ToString();
                lblToma.Visible = true;
            }
            #endregion
        }
        
        private void DisplayMemberProfile()
        {

            //Load Profile            
            _activeTab = ProfileTabEnum.Basic;
            
            ProfileTabConfigs tabConfigs = ProfileUtility.GetProfileTabConfigs(g.Brand.Site.SiteID, g);
            if (tabConfigs != null && tabConfigs.ProfileTabConfigList != null && tabConfigs.ProfileTabConfigList.Count > 0)
            {
                _activeTab = ViewProfileTabUtility.DetermineActiveTab(tabConfigs.ProfileTabConfigList[0].ProfileTabType);
            }
            if (FrameworkGlobals.IsValidMember(g.Member, g.Brand))
            {
                StandardProfileTemplate1.Visible = true;
                StandardProfileTemplate1.LoadProfile(MemberProfile, _activeTab);
            }
            else
            {
                VisitorProfileTemplate1.Visible = true;
                VisitorProfileTemplate1.LoadProfile(MemberProfile, _activeTab);
            }
           

            int entrypoint = Conversion.CInt(Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT], Constants.NULL_INT);

            //save to view profile hotlist
            if (g.Member != null && // logged in
                 g.Member.MemberID != _MemberID && // not looking at self
                 entrypoint != (int)BreadCrumbHelper.EntryPoint.AdminView) // not looking as admin
            { // then add member to Viewed list.
                // adding preventative measures to make sure a failed List call doesn't ruin the page that is being viewed
                try
                {
                    if (!ListSA.Instance.GetList(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID)).IsHotListed(HotListCategory.ExcludeFromList, g.TargetBrand.Site.Community.CommunityID, _MemberID))
                    {
                        ListSA.Instance.AddListMember(HotListCategory.MembersYouViewed,
                             g.TargetBrand.Site.Community.CommunityID,
                             g.TargetBrand.Site.SiteID,
                             g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                             _MemberID,
                             null,
                             Constants.NULL_INT,
                             !((g.Member.GetAttributeInt(g.TargetBrand, "HideMask", 0) & (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists),
                             false);

                        if (UserNotificationFactory.IsUserNotificationsEnabled(g))
                        {
                            UserNotificationParams unParams = UserNotificationParams.GetParamsObject(_MemberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                            UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new ViewedYourProfileUserNotification(), _MemberID, g.Member.MemberID, g);
                            if (notification.IsActivated())
                            {
                                UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }
            }

        }

        /// <summary>
        /// Handler for PreRender event of control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                BreadCrumbHelper.EntryPoint entryPoint = BreadCrumbHelper.GetEntryPoint(Conversion.CInt(Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT]));

                if (Convert.ToBoolean(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
                {
                    g.AnalyticsOmniture.Evar1 = entryPoint.ToString();

                    switch (entryPoint)
                    {
                        case BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMatches:
                        case BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVoting:
                            g.AnalyticsOmniture.Evar1 = "Home Page - Your Matches - Filmstrip";
                            break;
                        case BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMOL:
                        case BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVotingMOL:
                            g.AnalyticsOmniture.Evar1 = "Home Page - Members Online - Filmstrip";
                            break;
                        case BreadCrumbHelper.EntryPoint.HomeHeroProfileMatches:
                            g.AnalyticsOmniture.Evar1 = "Home Page - Your Matches - Hero";
                            break;
                        case BreadCrumbHelper.EntryPoint.HomeHeroProfileMOL:
                            g.AnalyticsOmniture.Evar1 = "Home Page - Members Online - Hero";
                            break;
                        case BreadCrumbHelper.EntryPoint.MemberLikeLink:
                            g.AnalyticsOmniture.Evar1 = "Seeing who liked your answer";
                            break;
                        case BreadCrumbHelper.EntryPoint.HomeViewedYouWidget:
                            g.AnalyticsOmniture.Evar1 = "Home Page - Hotlist - Viewed You";
                            break;
                    }

                    // Profile Name
                    g.AnalyticsOmniture.Prop24 = (MemberProfile != null) ? MemberProfile.GetUserName(_g.Brand) : String.Empty;

                    //04082008 TL - MPR-102
                    //10302008 TL - MPR-638 - Do not render profile analytics
                    if (g.Member != null && g.Member.MemberID != _MemberID) //no popup analytic for viewing your own profile
                    {
                        // Viewed as Popup vs Non-popup
                        _g.AnalyticsOmniture.Evar5 = BreadCrumbHelper.IsProfilePopupDisabled(_MemberID) ? "no pop-up" : "pop-up";

                        // Profile Views
                        if (ProfileUtility.IsDisplayingProfile30(g))
                        {
                            g.AnalyticsOmniture.AddEvent("event2");
                            g.AnalyticsOmniture.AddEvent("event12");
                        }
                        else
                        {
                            if (_activeTab != ProfileTabEnum.Basic)
                            {
                                g.AnalyticsOmniture.AddEvent("event4"); //secondary tabs
                            }
                            else
                            {
                                _g.AnalyticsOmniture.AddEvent("event12"); //basic tab
                            }
                        }
                        g.AnalyticsOmniture.Evar9 = _activeTab.ToString(); //specify tab name

                        //omniture setting for member slideshow
                        if (entryPoint == BreadCrumbHelper.EntryPoint.MemberSlideshow ||
                            entryPoint == BreadCrumbHelper.EntryPoint.SlideShowPage ||
                            HomeUtil.DisplaySecretAdmirerGameFromHomepageFilmstrip(entryPoint))
                        {

                            if (Conversion.CInt(Request["MemberID"]) == 0)
                            {
                                int ynmClickType = Conversion.CInt(Request["YNMClickType"]);
                                string ynmClickString = string.Empty;

                                switch (ynmClickType)
                                {
                                    case 1:
                                        ynmClickString = "Y";
                                        break;
                                    case 2:
                                        ynmClickString = "N";
                                        break;
                                    case 3:
                                        ynmClickString = "M";
                                        break;
                                }

                                g.AnalyticsOmniture.Evar1 = "YNM Advance Profile";
                                g.AnalyticsOmniture.Prop30 = ynmClickString + "- Advance Profile";
                            }
                            else
                            {
                                g.AnalyticsOmniture.Evar1 = "Home Page Slideshow";
                            }

                            g.AnalyticsOmniture.Prop31 = _MemberID.ToString();
                        }

                    }
                    else
                    {
                        _g.AnalyticsOmniture.Evar5 = String.Empty;
                        _g.AnalyticsOmniture.Evar9 = String.Empty;

                        //notify client side that member is viewing own profile, to not render analytics during tab clicks
                        Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "ViewingOwnProfile", "<script type=\"text/javascript\">tabProfileObject.ViewingOwnProfile = true;</script>");
                    }

                    // Page Name
                    g.AnalyticsOmniture.PageName = "View Profile - " +
                            ViewProfileTabUtility.GetEnumDescription(_activeTab);
                    
                    g.AnalyticsOmniture.PageName = "";
                    ProfileTabConfigs tabConfigs = ProfileUtility.GetProfileTabConfigs(g.Brand.Site.SiteID, g);
                    if (tabConfigs != null && tabConfigs.ProfileTabConfigList != null && tabConfigs.ProfileTabConfigList.Count > 0)
                    {
                        for (int i = 0; i < tabConfigs.ProfileTabConfigList.Count; i++)
                        {
                            //View Profile - Tab 1 - Description
                            ProfileTabConfig tabConfig = tabConfigs.ProfileTabConfigList[i];
                            if (tabConfig.ProfileTabType == _activeTab)
                            {
                                g.AnalyticsOmniture.PageName = "View Profile - Tab " + (i + 1).ToString();
                                if (tabConfig.Omniture != null)
                                    g.AnalyticsOmniture.PageName = g.AnalyticsOmniture.PageName + " - " + tabConfig.Omniture.OmniTabName;
                                break;
                            }
                        }
                    }
                    
                    string premEntryPoint = Request["PremEntPoint"];
                    if (premEntryPoint != null)
                    {
                        if (Convert.ToInt16(premEntryPoint) > 0)
                        {
                            var enumPremEntryPoint = (BreadCrumbHelper.PremiumEntryPoint)Convert.ToInt32(premEntryPoint);

                            // Only track when the viewing a member that has premium service tracking on
                            // PremEntPoint is 0 when viewing a member that does not have premium service tracking
                            // on
                            _g.AnalyticsOmniture.Prop1 = Enum.GetName(typeof(BreadCrumbHelper.PremiumEntryPoint), enumPremEntryPoint);
                            //_g.AnalyticsOmniture.Evar1 = Enum.GetName(typeof(BreadCrumbHelper.PremiumEntryPoint), enumPremEntryPoint);

                            //_g.AnalyticsOmniture.Prop1 = _g.AnalyticsOmniture.ConvertPremiumMaskToString(enumPremEntryPoint);
                            //_g.AnalyticsOmniture.Evar1 = _g.AnalyticsOmniture.ConvertPremiumMaskToString(enumPremEntryPoint);

                            if ((enumPremEntryPoint & BreadCrumbHelper.PremiumEntryPoint.Highlight) == BreadCrumbHelper.PremiumEntryPoint.Highlight)
                            {
                                _g.AnalyticsOmniture.AddEvent("event6");
                            }
                            else if ((enumPremEntryPoint & BreadCrumbHelper.PremiumEntryPoint.Spotlight) == BreadCrumbHelper.PremiumEntryPoint.Spotlight)
                            {
                                _g.AnalyticsOmniture.AddEvent("event1");
                            }
                        }

                        //_g.AnalyticsOmniture.Evar1 = premEntryPoint;
                    }

                    //hero profile clickthrough (per Jason, this may overwrite premium service tracking on this evar1)
                    if (!String.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_NAME_HERO_PROFILE]))
                        g.AnalyticsOmniture.Evar1 = Request.QueryString[WebConstants.URL_PARAMETER_NAME_HERO_PROFILE];

                    if (!String.IsNullOrEmpty(Request["ClickFrom"]))
                    {
                        g.AnalyticsOmniture.Evar1 = Request["ClickFrom"];
                    }

                    AddEvar1DataForProfilesArrowsNav();
                }

                //register scrolling and related js
                //Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "TabProfile_ScrollingDetector", "<script type=\"text/javascript\">window.onscroll=scrollingDetector;</script>");

                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand))
                {
                    //notify client side that ColorCodeTab will be available
                    Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "IsColorCodeEnabled", "<script type=\"text/javascript\">tabProfileObject.IsColorCodeEnabled = true;</script>");
                }

                if (QuestionAnswer.QuestionAnswerHelper.IsQuestionAnswerEnabled(g.Brand))
                {
                    //notify client side that QuestionAnswerTab will be available
                    Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "IsQuestionAnswerEnabled", "<script type=\"text/javascript\">tabProfileObject.IsQuestionAnswerEnabled = true;</script>");
                }

                //activity log for profile view
                if (g.Member != null && ViewProfileTabUtility.IsActivityLogViewProfileEnabled(g))
                {
                    bool viewingOwnProfile = g.Member.MemberID == _MemberID;
                    int pageNumber = viewingOwnProfile ? 1 : GetSearchPageNumber();
                    Matchnet.Search.Interfaces.SearchType searchType = viewingOwnProfile ? Matchnet.Search.Interfaces.SearchType.None : Matchnet.Web.Applications.Search.SearchUtil.GetSearchTypeForActivityLogging(entryPoint);
                    LogActivityProfileView(g.Member.MemberID, _MemberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID, entryPoint, searchType, pageNumber);
                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void AddEvar1DataForProfilesArrowsNav()
        {
            // add evar1 data for profiles arrows nav 
            if (!string.IsNullOrEmpty(Request.QueryString["arrowsnav"]))
            {
                string evar1Value = "www_profile_profile:Profile ";
                string arrowsNavEvent = Request.QueryString["arrowsnav"];
                if (!string.IsNullOrEmpty(Request.QueryString["omniKeyboard"]))
                {
                    arrowsNavEvent += "k";
                }
                switch (arrowsNavEvent)
                {
                    case "an":
                        evar1Value += "arrow-Next";
                        break;
                    case "ap":
                        evar1Value += "arrow-Previous";
                        break;
                    case "ank":
                        evar1Value += "kbd-arrow-Next";
                        break;
                    case "apk":
                        evar1Value += "kbd-arrow-Previous";
                        break;
                }
                g.AnalyticsOmniture.Evar1 = evar1Value;
            }
        }

        private bool IsBlocked()
        {
            try
            {
                if (g.Member == null || g.Member.MemberID == MemberID)
                    return false;


                Matchnet.List.ServiceAdapters.List memberList = ListSA.Instance.GetList(g.Member.MemberID, ListLoadFlags.None);
                bool isblocked = memberList.IsHotListed(HotListCategory.ExcludeListInternal, g.Brand.Site.Community.CommunityID, MemberID);
                if (isblocked)
                {
                    Matchnet.List.ServiceAdapters.List targetMemberList = ListSA.Instance.GetList(MemberID, ListLoadFlags.None);
                    isblocked = targetMemberList.IsHotListed(HotListCategory.ExcludeFromList, g.Brand.Site.Community.CommunityID, g.Member.MemberID);
                }
                return isblocked;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return false;
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Sets the member ID, ensuring that it exists, otherwise user will be redirected.
        /// </summary>
        private void SetMemberIdForProfileView()
        {
            // pv: only initialize if it hasn't been initialized already
            if (_MemberID == 0)
            {
                _MemberID = ViewProfileTabUtility.GetMemberIdForProfileView(g, false);
            }
            
            if (_MemberID == 0)
            {	//Not viewing another member AND not logged in, send to LogonRedirect.
                g.LogonRedirect(Context.Items["PagePath"].ToString());
            }
        }

        /// <summary>
        /// Check if profile is being viewed from a matchmail link
        /// </summary>
        private void CheckForMatchMailViewer()
        {
            if (Request.QueryString["mmvid"] == null) return;

            try
            {
                // Legacy code does the following, so our url capture must be compatible:
                //		Generating the MMVID parm in the url:  lngViewingMemberID = lngViewingMemberID xor (lngViewProfileMemberID + XOR_SALT_ADD_VALUE) 
                //		After grabbing parm:  lngViewingMemberID = lngViewingMemberID - XOR_SALT_ADD_VALUE

                int viewerMemberID = _MemberID ^ Conversion.CInt(Request.QueryString["mmvid"]);
                viewerMemberID -= Int32.Parse(WebConstants.XOR_SALT_ADD_VALUE);

                // Check if the member even exists 
                var tempMember = MemberSA.Instance.GetMember(viewerMemberID, MemberLoadFlags.None);

                if (tempMember.EmailAddress == null) return;

                // then add member to Who You Viewed list.
                ListSA.Instance.AddListMember(
                    HotListCategory.MembersYouViewed,
                    g.TargetBrand.Site.Community.CommunityID,
                    g.TargetBrand.Site.SiteID,
                    viewerMemberID,
                    _MemberID,
                    null,
                    Constants.NULL_INT,
                    !((tempMember.GetAttributeInt(g.TargetBrand, "HideMask", 0) &
                    (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists),
                    false
                );

                if (UserNotificationFactory.IsUserNotificationsEnabled(g))
                {
                    UserNotificationParams unParams = UserNotificationParams.GetParamsObject(_MemberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                    UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new ViewedYourProfileUserNotification(), _MemberID, viewerMemberID, g);
                    if (notification.IsActivated())
                    {
                        UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                    }
                }

                if (_g.Member == null && Request.QueryString[WebConstants.URL_PARAMETER_NAME_LOGON_PAGE_FLAG] != null)
                {
                    string redirectURL = Request.CurrentExecutionFilePath + "?" + Request.QueryString;
                    g.LogonRedirect(redirectURL);
                }

            }
            catch (Exception ex)
            {
                // allow the user the view the profile nonetheless
                g.ProcessException(ex);
            }
        }

        /// <summary>
        /// Inserts a pixel used for tracking promotional profiles
        /// </summary>
        private void insertPixel()
        {
            try
            {
                if (g.PromotionalProfileEnabled() && g.PromotionalProfileMember() == this.MemberProfile.MemberID)
                {
                    var pixelResx = g.GetResource("PROMOTION_FULLPROFILE_PIXEL");
                    if (!String.IsNullOrEmpty(pixelResx))
                    { phPixel.Text = pixelResx; }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        private void LogActivityProfileView(int viewerMemberID, int viewedMemberID, int siteID, int communityID, Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint entryPoint, Matchnet.Search.Interfaces.SearchType searchType, int pageNumber)
        {
            CallingSystem callingSystem = CallingSystem.Web;

            int matchScore = int.MinValue;
            if (viewerMemberID != viewedMemberID && MatchRatingManager.Instance.DisplayMatchRatingOnProfile(viewerMemberID, g.Brand))
            {
                //Calculate match rating
                MatchRatingResult matchRatingResult = MatchRatingManager.Instance.CalculateMatchPercentage(g.Member.MemberID,
                                                                                        _MemberProfile.MemberID,
                                                                                        g.Brand.Site.Community
                                                                                        .CommunityID,
                                                                                        g.Brand.Site.SiteID, searchType);
                if (matchRatingResult != null && matchRatingResult.MatchScore >= 0)
                {
                    matchScore = matchRatingResult.MatchScore;
                }
            }

            //get xml
            StringBuilder xmlForLog = GetProfileViewedActivityLogXML(siteID, communityID, entryPoint, searchType, pageNumber, matchScore);

            //save to activity log
            ActivityRecordingSA.Instance.RecordActivity(viewerMemberID, viewedMemberID, siteID, ActionType.ViewProfile, callingSystem, xmlForLog.ToString());
        }

        private StringBuilder GetProfileViewedActivityLogXML(int siteID, int communityID, Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint entryPoint, Matchnet.Search.Interfaces.SearchType searchType, int pageNumber, int matchScore)
        {
            StringBuilder xmlForLog = new StringBuilder("<ViewProfile>");
            xmlForLog.Append(string.Format("<SearchType>{0}</SearchType>", ((int)searchType).ToString()));
            xmlForLog.Append(string.Format("<EntryPoint>{0}</EntryPoint>", ((int)entryPoint).ToString()));
            xmlForLog.Append(string.Format("<PageNumber>{0}</PageNumber>", pageNumber.ToString()));
            xmlForLog.Append(string.Format("<TimeStamp>{0}</TimeStamp>", DateTime.Now));
            if (matchScore >= 0)
            {
                xmlForLog.Append(string.Format("<Score>{0}</Score>", matchScore.ToString()));
            }
            xmlForLog.Append("</ViewProfile>");
            return xmlForLog;
        }

        private int GetSearchPageNumber()
        {
            int PageSize =  16;
            int startRow = GetStartRow(PageSize);
            int pageNum = (startRow / PageSize) + 1;
            return pageNum;
        }

        private int GetStartRow(int pageSize)
        {
            int ordinal = 1;
            Int32.TryParse(Request["Ordinal"], out ordinal);
            if (ordinal < 1)
            {
                ordinal = 1;
            }

            int startRow = Conversion.CInt(Request["StartRow"], 0); //from url

            if (startRow < 1)
            {
                //determine startRow based on ordinal
                int page = ((ordinal % pageSize) == 0) ? (ordinal / pageSize) : (ordinal / pageSize + 1);
                startRow = ((page - 1) * pageSize) + 1;
            }

            if (startRow < 1) startRow = 1;

            return startRow;
        }


        #endregion


    }
}
