using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.Web.Framework.Managers;


namespace Matchnet.Web.Applications.MemberProfile
{
	public class AttractionBarSave : FrameworkControl
	{
        private int _voteType;
        
        public AttractionBarSave() 
		{
			base.SaveSession = false;
		}
		
		private int QueryStringInt(string name) 
		{
			return Conversion.CInt(Request[name]);
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				Response.Expires = 0;

				YNMVoteParameters parameters = new YNMVoteParameters();
				string ynmp = Request["ynmp"];
				if (ynmp != null)
				{
					parameters.DecodeParams(ynmp.Replace(" ", "+"));   //.Net replaces "+" in the URL param with " ".  We need to undo this so that we get the original encrypted string back.
					SaveYNMVote(QueryStringInt("YNMLT"), parameters);
				}
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
				Response.Clear();
				Response.Write(ex.ToString());
			}
		}

		public void SaveYNMVote(int voteType, YNMVoteParameters parameters)
		{
            _voteType = voteType;
            if (parameters.FromMemberID > 0)
			{
				ListManager listManager = new ListManager();
                listManager.YNMVote(voteType, parameters.FromMemberID, parameters.ToMemberID, g.Brand, g);

				Response.Write(DateTime.Now.ToString());
			}
		}

        protected override void OnPreRender(EventArgs e)
        {
            string source = Request["source"];
            string prop30Value = string.Empty;

            if(source == "fullprofile")
            {
                switch (_voteType)
                {
                    case 1:
                        prop30Value = "Y- Full Profile";
                        break;
                    case 2:
                        prop30Value = "N- Full Profile";
                        break;
                    case 3:
                        prop30Value = "M- Full Profile";
                        break;
                }
            }
            else if (source == "miniprofile")
            {
                switch (_voteType)
                {
                    case 1:
                        prop30Value = "Y- Mini Profile";
                        break;
                    case 2:
                        prop30Value = "N- Mini Profile";
                        break;
                    case 3:
                        prop30Value = "M- Mini Profile";
                        break;
                }
            }
            g.AnalyticsOmniture.Prop30 = prop30Value;
            base.OnPreRender(e);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
