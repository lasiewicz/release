﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Globalization;
using System.Collections.Specialized;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30
{
    /// <summary>
    /// Base profile class for common profile related implementation used by most profile controls
    /// </summary>
    public class BaseProfile : Matchnet.Web.Applications.MemberProfile.BaseProfileViewControl
    {
        public WhoseProfileEnum WhoseProfile { get; set; }

        public BaseProfile()
        {
            WhoseProfile = WhoseProfileEnum.Other;
        }

        public int MemberId
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.MemberID;
                }
                return 0;
            }
        }

        public string MemberUserName
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.GetUserName(g.Brand);
                }
                return string.Empty;
            }
        }

        public int ProfileMemberID
        {
            get
            {
                if (MemberProfile != null && MemberProfile.MemberID > 0)
                {
                    return MemberProfile.MemberID;
                }
                return 0;
            }
        }

        public bool IsPayingMember
        {
            get
            {
                if (g.Member != null)
                {
                    return g.Member.IsPayingMember(g.Brand.Site.SiteID);
                }
                return false;
            }
        }

        public bool IsLoggedIn
        {
            get { return null != g.Member && g.Member.MemberID > 0; }
        }

        #region Public/Protected Methods
        /// <summary>
        /// Gets the attribute value based on the specified name
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        protected override string GetMemberAttributeValue(string attributeName, Matchnet.Web.Framework.FrameworkControl resourceControl, bool noBlankText)
        {
            string attributeValue = String.Empty;

            switch (attributeName.ToLower())
            {
                case "aboutme":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "height":
                    attributeValue = ProfileDisplayHelper.GetHeightDisplay(this.MemberProfile, this.g);
                    if (attributeValue == Localizer.GetStringResource("N_A", null, g.Brand, new StringDictionary()))
                    {
                        attributeValue = GetNoAnswer(resourceControl);
                    }
                    break;
                case "weight":
                    attributeValue = ProfileDisplayHelper.GetWeightDisplay(this.MemberProfile, this.g);
                    break;
                case "haircolor":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "eyecolor":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "bodytype":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "relocateflag":
                    attributeValue = getWillingToRelocateDisplay();
                    break;
                case "maritalstatus":
                    attributeValue = getMaritalStatus();
                    break;
                case "custody":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "morechildrenflag":
                    attributeValue = getMoreChildrenFlag(this, true);
                    break;
                case "smokinghabits":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "drinkinghabits":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "activitylevel":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "grewupin":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "jdateethnicity":
                case "ethnicity":
                    if (FrameworkGlobals.IsMingleSite(g.Brand.Site.Community.CommunityID))
                    {
                        attributeValue = getMaskContent(attributeName);
                    }
                    else
                    {
                        attributeValue = getEthnicity();
                    }
                    break;
                case "languagemask":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "jdatereligion":
                case "religion":
                    attributeValue = getReligion();
                    break;
                case "studiesemphasis":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "educationlevel":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "industrytype":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "occupationdescription":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "incomelevel":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "personalitytrait":
                    attributeValue = getPersonalityTraits();
                    break;
                case "leisureactivity":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "entertainmentlocation":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "physicalactivity":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "cuisine":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "music":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "reading":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "perfectfirstdateessay":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "idealrelationshipessay":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "learnfromthepastessay":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "childrencount":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "keepkosher":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "synagogueattendance":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "zodiac":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "politicalorientation":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "pets":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "perfectmatchessay":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "desiredminage":
                    attributeValue = getDesiredAgeRange();
                    break;
                case "desiredmaritalstatus":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "desiredjdatereligion":
                case "desiredreligious":
                    attributeValue = getDesiredReligious();
                    break;
                case "desirededucationlevel":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "desireddrinkinghabits":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "desiredsmokinghabits":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "desiredmorechildrenflag":
                    // Retrieve the int representation of the relocation attribute
                    int MoreChildrenFlag = MemberProfile.GetAttributeInt(g.Brand, "DesiredMoreChildrenFlag");

                    // if the user wants children
                    if (MoreChildrenFlag == 1)	// Yes	
                    {
                        attributeValue = g.GetResource("MORECHILDREN_YES", resourceControl);
                    }
                    // if the user does not want children
                    else if (MoreChildrenFlag == 0)	// No
                    {
                        attributeValue = g.GetResource("MORECHILDREN_NO", resourceControl);
                    }
                    else if (MoreChildrenFlag == 2) 	// Not sure
                    {
                        attributeValue = g.GetResource("MORECHILDREN_UNSURE", resourceControl);
                    }
                    else if (MoreChildrenFlag == 4) 	// Doesn't matter
                    {
                        attributeValue = g.GetResource("MORECHILDREN_DOES_NOT_MATTER", resourceControl);
                    }
                    else
                    {
                        attributeValue = String.Empty;
                    }
                    break;
                case "desiredchildrencount":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "majortype":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "schoolid":
                    attributeValue = getSchool();
                    break;
                case "memberid":
                    attributeValue = _MemberProfile.MemberID.ToString();
                    break;
                case "age":
                    attributeValue = GetFormattedAge(resourceControl);
                    break;
                case "location":
                    attributeValue = ProfileDisplayHelper.GetRegionDisplay(this.MemberProfile, g);
                    break;
                case "agelocation":
                    attributeValue = GetFormattedAgeLocation(resourceControl);
                    break;
                case "gendermask":
                    attributeValue = ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(this.MemberProfile, this.g);
                    break;
                case "lookingfor":
                    attributeValue = g.GetResource("FOR_A", resourceControl) + " " + getMaskContent("RelationshipMask");
                    break;
                case "seekingmask":
                case "relationshipmask":
                    attributeValue = getMaskContent("RelationshipMask");
                    break;
                case "zodiacsign":
                    attributeValue = AstrologyHelper.GetZodiacSign(g, _MemberProfile.GetAttributeDate(g.Brand, "birthdate"), true);
                    break;
                case "gender":
                    attributeValue = ProfileDisplayHelper.GetGender(this.MemberProfile, this.g, resourceControl);
                    break;
                case "seekinggendermask":
                    attributeValue = ProfileDisplayHelper.GetSeekingGenderDisplay(this.MemberProfile, this.g);
                    break;
                case "yearsold":
                    attributeValue = GetAgeYears(resourceControl);
                    break;
                case "pref_age":
                    attributeValue = GetRangePreference("MINAGE", "MAXAGE", "AGE_PREFERENCES", g, resourceControl);
                    break;
                case "pref_location":
                    attributeValue = GetDistancePreference(g, resourceControl);
                    break;
                case "pref_height":
                    attributeValue = GetHeightPreference("HEIGHT_PREFERENCES", g, resourceControl);
                    break;

                case "pref_smoking":
                    attributeValue = GetOptionPreference("SMOKINGHABITS", g, resourceControl);
                    break;
                case "pref_drinking":
                    attributeValue = GetOptionPreference("DRINKINGHABITS", g, resourceControl);
                    break;
                case "pref_education":
                    attributeValue = GetOptionPreference("EDUCATIONLEVEL", g, resourceControl);
                    break;
                case "pref_relationship":
                    attributeValue = GetOptionPreference("MARITALSTATUS", g, resourceControl);
                    break;
                case "pref_bodytype":
                    attributeValue = GetOptionPreference("BODYTYPE", g, resourceControl);
                    break;
                case "favoritebands":
                case "favoritemovies":
                case "favoritetv":
                case "mygreattripidea":
                case "favoriterestaurant":
                case "schoolsattended":
                case "essayfirstdate":
                case "essaygettoknowme":
                case "essaypastrelationship":
                case "essayimportantthings":
                case "essayperfectday":
                case "essaymorethingstoadd":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;

                case "timeliness":
                case "fashion":
                case "moviegenremask":
                    attributeValue = getMaskContent(attributeName);
                    break;
            }

            if (String.IsNullOrEmpty(attributeValue))
            {
                attributeValue = base.GetMemberAttributeValue(attributeName, resourceControl, noBlankText);
            }

            if (String.IsNullOrEmpty(attributeValue) && noBlankText)
            {
                attributeValue = GetNoAnswer(resourceControl);
                if (String.IsNullOrEmpty(attributeValue) || attributeValue.ToLower().IndexOf("resource") >= 0)
                    attributeValue = "&nbsp;"; //output non-breaking space for correct alignment with css

            }

            if (attributeValue == null)
                attributeValue = "";

            return attributeValue;
        }

        public virtual string GetAgeYears(Matchnet.Web.Framework.FrameworkControl resourceControl)
        {
            try
            {
                string age = Matchnet.Web.Framework.FrameworkGlobals.GetAge(this.MemberProfile, g.Brand).ToString();
                if (String.IsNullOrEmpty(age))
                {
                    age = "&nbsp;";
                }


                return age;

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return "";
            }

        }


        public virtual string GetRangePreference(string preferencemin, string preferencemax, string resource, ContextGlobal g, FrameworkControl resourceControl)
        {
            string ret = "";
            try
            {
                string min = SearchPreferences[preferencemin];
                string max = SearchPreferences[preferencemax];
                int countany = 0;
                if (Conversion.CInt(min) < 0)
                {
                    min = g.GetResource("PREF_ANY", resourceControl);
                    countany += 1;
                }
                if (Conversion.CInt(max) < 0)
                {
                    max = g.GetResource("PREF_ANY", resourceControl);
                    countany += 1;
                }

                if (countany == 2)
                {
                    ret = min;
                }
                else
                {
                    string resx = g.GetResource(resource, resourceControl);
                    ret = String.Format(resx, min, max);
                }
                return ret;


            }
            catch (Exception ex)
            { return ""; }

        }

        public virtual string GetHeightPreference(string resource, ContextGlobal g, FrameworkControl resourceControl)
        {
            string ret = "";

            try
            {
                string min = SearchPreferences["MINHEIGHT"];
                string max = SearchPreferences["MAXHEIGHT"];
                string hmin = "";
                string hmax = "";
                int countany = 0;
                if (Conversion.CInt(min) < 0)
                {
                    min = g.GetResource("PREF_ANY", resourceControl);
                    countany += 1;
                    hmin = min;

                }
                else
                {
                    hmin = FrameworkGlobals.GlobalHeight(g.Brand.Site.CultureInfo.LCID, int.Parse(min), g.Brand);
                }

                if (Conversion.CInt(max) < 0)
                {
                    max = g.GetResource("PREF_ANY", resourceControl);
                    countany += 1;
                    hmax = max;
                }
                else
                {
                    hmax = FrameworkGlobals.GlobalHeight(g.Brand.Site.CultureInfo.LCID, int.Parse(max), g.Brand);
                }

                if (countany == 2)
                {
                    ret = min;
                }
                else
                {

                    string resx = g.GetResource(resource, resourceControl);
                    ret = String.Format(resx, hmin, hmax);
                }
                return ret;


            }
            catch (Exception ex)
            { return ""; }

        }
        public virtual string GetDistancePreference(ContextGlobal g, FrameworkControl resourceControl)
        {
            try
            {
                string regionid = SearchPreferences["REGIONID"];
                string distance = SearchPreferences["DISTANCE"];

                string resx = g.GetResource("LOCATION_PREFERENCES", resourceControl);
                string location = FrameworkGlobals.GetRegionString(Conversion.CInt(regionid), g.Brand.Site.LanguageID);
                return String.Format(resx, distance, location);


            }
            catch (Exception ex)
            { return ""; }

        }
        public virtual string GetOptionPreference(string optionname, ContextGlobal g, FrameworkControl resourceControl)
        {
            try
            {
                string option = SearchPreferences[optionname];


                string optionstring = Matchnet.Web.Framework.Util.Option.GetMaskContent(optionname, Conversion.CInt(option), g);
                if (String.IsNullOrEmpty(optionstring))
                    optionstring = g.GetResource("PREF_ANY", resourceControl);
                return optionstring;


            }
            catch (Exception ex)
            { return ""; }

        }

        #endregion

    }
}
