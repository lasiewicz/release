﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="M2MCommunication.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.M2MCommunication" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="Add2List" Src="/Framework/Ui/BasicElements/Add2List.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WatermarkTextbox" Src="/Framework/Ui/FormElements/WatermarkTextbox.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdUnit" Src="~/Framework/Ui/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="mnicb" TagName="IceBreaker" Src="/Framework/Ui/BasicElements/IceBreaker.ascx" %>

<script language="javascript" type="text/javascript">
    var m2mSendEmailInProgress = false;
    var m2mIsVIPEmailEnabled = <%=_isVIPEnabled.ToString().ToLower() %>;
    var m2mDisplayVIPOverlay = <%=_displayVIPOverlay.ToString().ToLower() %>;
    var m2mMaxDisplayOverlayPerSession = <%=_MaxDisplayOverlayPerSession%>;;
    var m2mCurrentDisplayOverlayPerSession = <%=_CurrentDisplayOverlayPerSession%>;;
    var m2mIsVIPMember = <%=_IsVIPMember.ToString().ToLower() %>;
    var m2mVIPEmailRemaining = <%=_remainingVIPCount%>;
    var m2mVIPoverlay = null;
    var m2mOneClickAllAccess = <%=_isOnClickAllAccessEnabled.ToString().ToLower() %>;
    var useYourVIPCBText =  '<%=_useYourVIPCBText%>';
    var useYourVIPLBLText = '<%=_useYourVIPLBLText%>';
        
    function M2MSendMessage(sendAsVIP, fromLayoverButton, fromConf, oneClickAction) {
        
        if (m2mVIPoverlay != null  && !m2mOneClickAllAccess)
        {
            m2mVIPoverlay.hide();
        }
        if (m2mSendEmailInProgress == true){
            return false;
        }
        
        var $texMsgtBox = $j('#<%=txtEmailMessage.TextBox.ClientID%>');
        var $txtSubjectBox = $j('#<%=txtEmailSubject.TextBox.ClientID%>');
        var isValid = true;
        var isVerbose = false;
        var targetMemberID = <%=MemberProfile.MemberID%>;
        var messageSubject = ($txtSubjectBox.length)?$j.trim($txtSubjectBox.val()):'<%=_EmailSubject%>';
        var message = $j.trim($texMsgtBox.val());
        var waterMarkText = '<%=_EmailWatermarkText%>';
        var waterMarkSubject = '<%=_EmailSubjectWatermarkText%>';
        var saveAsDraft = <%=CanSaveAsDraftEnabled.ToString().ToLower()%>;
        var canNonsubSendemail = <%=CanNonsubSendFreeEmail.ToString().ToLower() %>;

        if (canNonsubSendemail) {
            saveAsDraft = false;
        }

        if (messageSubject == waterMarkSubject || messageSubject == '') {
            messageSubject = '<%=_EmailSubject%>';
        }

        if (message == waterMarkText || message == '') {
            isValid = false;
            $j('#emailEmpty').css("display", "inline").delay('2000').fadeOut('slow');
        }
        
        if (oneClickAction !== undefined) {
            if (oneClickAction == 'no-display') {
                if (sendAsVIP && m2mIsVIPMember && m2mVIPEmailRemaining <= 0){
                    M2M_ShowRanOutVIPOverlay();   
                    return;
                }
            }
            else if (oneClickAction == 'no-extra-msg') {
                $j('.vip-m2m-overlay.one-click').css({ "display":"none" , "z-index":"-1" });
            }
        } else {
            if (fromConf)
                $j('.vip-m2m-overlay.one-click').css({ "display":"none" , "z-index":"-1" });
        }

        if (isValid) {
            $j('#divHoverSendingMessage').css("display", "inline");
            m2mSendEmailInProgress = true;
            
            if (sendAsVIP && m2mVIPEmailRemaining <= 0) {
                saveAsDraft = true;
            }

            if (m2mOneClickAllAccess) {
                //If the user ran out of messages - charge him.
                if (oneClickAction == 'ran-out' || oneClickAction == 'vip-purchase') { 
                    var ajaxURL = (oneClickAction !== undefined && oneClickAction == 'ran-out') ? 'AddAllAccess' : 'PurchaseAllAccess'
                    $j('#divHoverSendingMessage').css({ "display":"block" , "z-index":"1600", "top":"0" }); 
                    $j.ajax({
                        type: "POST",
                        url: "/Applications/API/AutoRenewal.asmx/" + ajaxURL,
                        data: "{ memberID: '" + <%= MemberProfile.MemberID %>+"'}",
                        datatype: "json",
                        contentType: "application/json; charset=utf-8",
                        //breforeSend: function() {
                        //    $j('#divHoverSendingMessage').css({ "display":"block" , "z-index":"1500", "top":"-9px", "left":"27px" });
                        //},
                        error: function(jqXHR, textStatus, errorThrown){
                            $j('#emailError').css("display", "inline").delay('2000').fadeOut('slow');
                            $j('#divHoverSendingMessage').css({ "display":"none" , "z-index":"-1" });
                            if (isVerbose) { alert(textStatus); }
                        },
                        complete: function() {
                            m2mSendEmailInProgress = false;
                            $j('#divHoverSendingMessage').css({ "display":"none" , "z-index":"-1" });
                        },
                        success: function(res) {
                            if (res.d.Status == 2) {               
                                var container = '#divOneClickNonVIPOverlay';

                                if (oneClickAction !== undefined) {
                                    if (oneClickAction == 'ran-out')
                                        var container = '#divOneClickRanOutVIPOverlay';
                                    else if (oneClickAction == 'vip-purchase')
                                        var container = '#divOneClickNonVIPOverlay';

                                    m2mVIPEmailRemaining = 10;
                                    m2mIsVIPMember = true;
                                    sendAsVIP = true;
                                    // replace text
                                    $j('#mail-send-as-vip > label').html(useYourVIPCBText);
                                    $j('#_ctl0__ctl4_StandardProfileTemplate1_m2mCommunication1_lblVIPRemaining').html(useYourVIPLBLText);
                                    
                                    try{
                                        $j('#txtUseYourVIPRemaining1').html(m2mVIPEmailRemaining);
                                        $j('#txtUseYourVIPRemaining2').html(m2mVIPEmailRemaining);
                                    }
                                    catch (e){
                                    }
                                    try{
                                        $j('#txtVipOverlayRemaining').html(m2mVIPEmailRemaining);    
                                    }
                                    catch (e){
                                    }    

                                }

                                var resHtml = $j('#msgAASuccessPurchaseOneCLick').html();
                                $j('#msgAASuccessPurchaseOneCLick').empty();
                                $j('.vip-m2m-overlay.one-click').html(resHtml);

                                $j(container + ' .oc-data-username').text(res.d.Username);
                                $j(container + ' .oc-data-payment-conf').text(res.d.OrderID);
                                $j(container + ' .oc-data-credit-type').text(res.d.CreditCardType);
                                $j(container + ' .oc-data-credit-num').text(res.d.LastFourDigitsCreditCardNumber + '************');
                                $j(container + ' .oc-data-payment-date').text(res.d.TransactionDate);
                                $j(container).show();
                                if (s != null) {
                                    PopulateS(true);
                                    s.events = "event2";

                                    if (oneClickAction !== undefined && oneClickAction == 'ran-out') {
                                        s.pageName = "Add Extra AA emails overlay – confirmation ";
                                        s.eVar24 = 'all access emails on profile- additional';

                                    } else {
                                        s.pageName = "Extra AA emails overlay - confirmation";
                                        s.eVar24 = 'all access emails on profile';
                                    }

                                    s.t();
                                }                            
                            } else {
                                var resHtml = $j('#msgAAFailedPurchaseOneCLick').html();
                                $j('#msgAAFailedPurchaseOneCLick').empty();
                                $j('.vip-m2m-overlay.one-click').html(resHtml);
                            }
                        }
                    });
                }
            } 

            //Don't send an message
            if (m2mOneClickAllAccess) {
                if (oneClickAction == 'ran-out' || oneClickAction == 'vip-purchase') { 
                    return;
                }
            }
                
            $j.ajax({
                type: "POST",
                url: "/Applications/API/IMailService.asmx/SendMessage",
                data: "{ targetMemberID: " + targetMemberID
                    + ", messageSubject: \"" + spark.util.uniescape(messageSubject) + "\""
                    + ", message: \"" + spark.util.uniescape(message) + "\""
                    + ", sendAsVIP: " + sendAsVIP
                    + ", sendAsVIPFromLayoverButton: " + fromLayoverButton
                    + ", saveAsDraft: " + saveAsDraft + "}",
                contentType: "application/json; charset=utf-8",
                complete: function() {
                    m2mSendEmailInProgress = false;
                    //setTimeout("$j('#divHoverSendingMessage').css('display', 'none');", 5000); //make ajax loader stay up for testing
                    $j('#divHoverSendingMessage').css("display", "none");
                    m2mVIPoverlay.hide();
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $j('#emailError').css("display", "inline").delay('2000').fadeOut('slow');
                    if (isVerbose) { alert(textStatus); }
                },
                success: function(result) {
                    var sendMessageResultObj = (typeof result.d == 'undefined') ? result : result.d;
                    var resultStatus = sendMessageResultObj.Status;
                    
                    if (resultStatus == 3) {
                        if (sendMessageResultObj.MessageSendErrorID == 0)
                        {
                            $j('#emailErrorCapLimit').css("display", "inline").delay('2000').fadeOut('slow');
                        }
                        else
                        {
                            $j('#emailError').css("display", "inline").delay('2000').fadeOut('slow');
                        }
                        if (isVerbose) { alert(sendMessageResultObj.StatusMessage, 3); }
                    }
                    else if (resultStatus == 4) {
                        var upsaleURL = sendMessageResultObj.UpsaleURL;
                        window.location.href = upsaleURL;
                    }
                    else if (resultStatus == 5){
                        //session expired
                        window.location.href = '/Applications/Logon/Logon.aspx?DestinationURL=' + encodeURIComponent(window.location.href.replace('http://'+window.location.host,''));
                    }
                    else if (resultStatus == 2) {
                        if ((sendAsVIP && m2mVIPEmailRemaining <= 0) || saveAsDraft)
                        {
                            //redirect to upsale
                            var upsaleURL = sendMessageResultObj.UpsaleURL;
                            window.location.href = upsaleURL;
                        }
                        else
                        {
                            $j('#emailSent').css("display", "inline").delay('2000').fadeOut('slow');
                            $txtSubjectBox.val(waterMarkSubject);
                            $texMsgtBox.val(waterMarkText);
                            //$j.watermark.showAll();
                            var omniVIP = false;
                            if (sendAsVIP && m2mIsVIPMember)
                            {
                                //update vip email remaining
                                omniVIP = true;
                                m2mVIPEmailRemaining = m2mVIPEmailRemaining - 1;
                                try{
                                    $j('#txtUseYourVIPRemaining1').html(m2mVIPEmailRemaining);
                                    $j('#txtUseYourVIPRemaining2').html(m2mVIPEmailRemaining);
                                }
                                catch (e){
                                }
                                try{
                                    $j('#txtVipOverlayRemaining').html(m2mVIPEmailRemaining);    
                                }
                                catch (e){
                                }                          
                            }
                            
                            //update omniture
                            if (s != null) {
                                PopulateS(true); //clear existing values in omniture "s" object
                                s.prop35 = targetMemberID;
                                s.events = "event11";
                                s.eVar26 = omniVIP ? "imail-Send VIP mail" : "imail";
                                s.t(); //send omniture updated values as page load
                            }
                        }
                    }
                }
            });          
        }        
    }
    
    function M2M_ShowVIPOverlay(){
        if (m2mDisplayVIPOverlay && (m2mMaxDisplayOverlayPerSession <= 0 || m2mCurrentDisplayOverlayPerSession < m2mMaxDisplayOverlayPerSession)){
            if (m2mIsVIPMember && m2mVIPEmailRemaining > 0){
                M2M_ShowSendVIPOverlay();
            }
            else if (m2mIsVIPMember && m2mVIPEmailRemaining <= 0){
                M2M_ShowRanOutVIPOverlay();
            }
            else{
                M2M_ShowNonVIPOverlay();
            }
        }
        else{
            M2MSendMessage($j('input:checkbox', '#mail-send-as-vip').is(":checked"), false, false, 'no-display');
        }
    }
    
    function M2M_ShowSendVIPOverlay() {
        if (!$j('input:checkbox', '#mail-send-as-vip').is(":checked"))
        {
            m2mVIPoverlay = m2mOneClickAllAccess ? $j('#divOneClickSendVIPOverlay') : $j('#divSendVIPOverlay');
            
            m2mVIPoverlay.detach().appendTo('#aspnetForm').after('<div class="ui-widget-overlay" style="position:fixed;" />').show();
                      
            M2M_UpdateShownVIPOverlay();
        }
        else
        {
            M2MSendMessage(true, false);
        }
    }
    
    function M2M_ShowRanOutVIPOverlay() {
        if (!$j('input:checkbox', '#mail-send-as-vip').is(":checked") || m2mOneClickAllAccess)
        {
            m2mVIPoverlay =  m2mOneClickAllAccess ? $j('#divOneClickRanOutVIPOverlay') : $j('#divRanOutVIPOverlay');
            if (m2mOneClickAllAccess) {
                // Get amount from server
                var ajaxURL = 'GetPackagePricingDetails';
                $j.ajax({
                    type: "POST",
                    url: "/Applications/API/AutoRenewal.asmx/" + ajaxURL,
                    data: "{ package: 'AllAccessEmail'}",
                    datatype: "json",
                    contentType: "application/json; charset=utf-8",
                    breforeSend: function() {
		   
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        if (isVerbose) { alert(textStatus); }
                        m2mVIPoverlay = $j('#divRanOutVIPOverlay');
                    },
                    complete: function() {
                    },
                    success: function(res) {
                        if (res.d.Status == 2) {
                            if (m2mOneClickAllAccess) {
                                $j('.vip-m2m-overlay.one-click.ran-out').css({ "display":"block" , "z-index":"1500" });

                                if (s != null) {
                                    PopulateS(true); //clear existing values in omniture "s" object

                                    s.events = "event2";
                                    s.pageName = "Add Extra AA emails overlay";
                                    s.eVar23= 'all access emails on profile- additional';
                                    s.t();
                                }
                            }
                            $j("#divOneClickRanOutVIPOverlay #amount").text(res.d.PackageOverrideTotalAmount);
                            
                        }  
                        else {
                            m2mVIPoverlay = $j('#divRanOutVIPOverlay');
                        }
                    } 
                }); 
            }

            m2mVIPoverlay.detach().appendTo('#aspnetForm').after('<div class="ui-widget-overlay" style="position:fixed;" />').show();
          
            M2M_UpdateShownVIPOverlay();
        }
        else
        {
            M2MSendMessage(true, false);
        }
    }
    
    function M2M_ShowNonVIPOverlay() {
        if (!$j('input:checkbox', '#mail-send-as-vip').is(":checked") || m2mOneClickAllAccess)
        {
            m2mVIPoverlay = m2mOneClickAllAccess ? $j('#divOneClickNonVIPOverlay') : $j('#divNonVIPOverlay');
            
            if (m2mOneClickAllAccess) {
                // Get amount from server
                var ajaxURL = 'GetPackagePricingDetails';
                $j.ajax({
                    type: "POST",
                    url: "/Applications/API/AutoRenewal.asmx/" + ajaxURL,
                    data: "{ package: 'AllAccess'}",
                    datatype: "json",
                    contentType: "application/json; charset=utf-8",
                    breforeSend: function() {
		   
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        if (isVerbose) { alert(textStatus); }
                        m2mVIPoverlay = $j('#divNonVIPOverlay');
                    },
                    complete: function() {
                    },
                    success: function(res) {
                        if (res.d.Status == 2) {
                            if (m2mOneClickAllAccess) {
                                m2mVIPoverlay.css({'z-index':'1500'});
                                if (s != null) {
                                    PopulateS(true); //clear existing values in omniture "s" object

                                    s.events = "event2";
                                    s.pageName = "Extra AA emails overlay";
                                    s.eVar23= 'all access emails on profile';
                                    s.t();
                                }
                            }
                            $j("#divOneClickNonVIPOverlay #amount").text(res.d.PackageOverrideTotalAmount);
                            
                        }  
                        else {
                            m2mVIPoverlay = $j('#divNonVIPOverlay');
                        }
                    } 
                }); 
            }

            m2mVIPoverlay.detach().appendTo('#aspnetForm').after('<div class="ui-widget-overlay" style="position:fixed;" />').show();
            M2M_UpdateShownVIPOverlay();
        }
        else
        {
            M2MSendMessage(true, false);
        }                  
    }
    
    function M2M_UpdateShownVIPOverlay(){
        m2mCurrentDisplayOverlayPerSession = m2mCurrentDisplayOverlayPerSession + 1;
        $j.ajax({
                type: "POST",
                url: "/Applications/API/SessionUpdate.asmx/UpdateSessionIntWithIncrement",
                data: "{key:'VIP_COMPOSE_OVERLAY_PER_SESSION',value:1,lifetime:0}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {},
                error: function(msg) { }
            })
    }
    
    function M2M_ProcessYNM(parentClientID, type, encryptedParams){
        if (ConfirmYNMVote(parentClientID, type)) {
            SaveYNMVote(type, encryptedParams);
            
            var externalMutualYesElementID = '<%=MutualYesElementID%>';
            $j('#secretAdmirerPopup').css("display", "none");
            
            $j('#<%=secretAdmirerPopup_desc.ClientID%>').css("display", "none");
            $j('#<%=secretAdmirerPopup_descY.ClientID%>').css("display", "none");
            $j('#<%=secretAdmirerPopup_descN.ClientID%>').css("display", "none");
            $j('#<%=secretAdmirerPopup_descM.ClientID%>').css("display", "none");
            $j('#<%=secretAdmirerPopup_descYY.ClientID%>').css("display", "none");
            $j('#<%=spanSecretAdmirerYY.ClientID%>').css("display", "none");
            $j('#<%=spanYes.ClientID%>').removeClass().addClass('spr s-icon-click-y-off');
            $j('#<%=spanNo.ClientID%>').removeClass().addClass('spr s-icon-click-n-off');
            $j('#<%=spanMaybe.ClientID%>').removeClass().addClass('spr s-icon-click-m-off');
            if (externalMutualYesElementID != ''){
                $j('#' + externalMutualYesElementID).css("display", "none");
            }

            var currentStatus = $j('#<%=YNMVoteStatus.ClientID%>');
            var ynmType = '';
            //apply it when new YNM is ready. currently BBW, BS & SP are not ready ['1001', '90410', '90510']
            var isNewYNMReday = $j.inArray(spark.favorites.BrandId, ['1003', '1004', '1105', '1107']) > -1;

            switch (type) {
                case "1":                    
                    ynmType = "Y";
                    if ((currentStatus.val() & 2) == 2) {
                        currentStatus.val(currentStatus.val() ^ 2);
                    }
                    if ((currentStatus.val() & 4) == 4) {
                        currentStatus.val(currentStatus.val() ^ 4);
                    }
                    currentStatus.val(currentStatus.val() | 1);

                    //If the other user has clicked yes, display the "you both clicked yes" icon:
                    if ((currentStatus.val() & 8) == 8)
                    {
                        $j('#<%=secretAdmirerPopup_descYY.ClientID%>').css("display", "block");
                        $j('#<%=spanSecretAdmirerYY.ClientID%>').css("display", "inline-block");
                        if (externalMutualYesElementID != ''){
                            $j('#' + externalMutualYesElementID).css("display", "inline-block");
                        }
                    }
                    else
                    {
                        $j('#<%=secretAdmirerPopup_descY.ClientID%>').css("display", "block");
                    }
                    $j('#<%=spanYes.ClientID%>').removeClass().addClass('spr s-icon-click-y-on');
                    
                    if (isNewYNMReday) {
                        SecretAdmirer_UiUpdateYNM($j('#<%=spanYes.ClientID%>').closest('.click20-horizontal'));
                    }

                    break;

                case "2":
                    ynmType = "N";
                    if ((currentStatus.val() & 1) == 1) {
                        currentStatus.val(currentStatus.val() ^ 1);
                    }
                    if ((currentStatus.val() & 4) == 4) {
                        currentStatus.val(currentStatus.val() ^ 4);
                    }
                    currentStatus.val(currentStatus.val() | 2);

                    $j('#<%=secretAdmirerPopup_descN.ClientID%>').css("display", "block");
                    $j('#<%=spanNo.ClientID%>').removeClass().addClass('spr s-icon-click-n-on');

                    if (isNewYNMReday) {
                        SecretAdmirer_UiUpdateYNM($j('#<%=spanNo.ClientID%>').closest('.click20-horizontal'));
                    }

                    break;

                case "3":
                    ynmType = "M";
                    if ((currentStatus.val() & 1) == 1) {
                        currentStatus.val(currentStatus.val() ^ 1);
                    }
                    if ((currentStatus.val() & 2) == 2) {
                        currentStatus.val(currentStatus.val() ^ 2);
                    }
                    currentStatus.val(currentStatus.val() | 4);

                    $j('#<%=secretAdmirerPopup_descM.ClientID%>').css("display", "block");
                    $j('#<%=spanMaybe.ClientID%>').removeClass().addClass('spr s-icon-click-m-on');

                    if (isNewYNMReday) {
                        SecretAdmirer_UiUpdateYNM($j('#<%=spanMaybe.ClientID%>').closest('.click20-horizontal'));
                    }

                    break;
            }
            
            //update omniture
            if (s != null) {
                PopulateS(true); //clear existing values in omniture "s" object

                //s.events = "event41";
                s.prop30 = ynmType + " - Full Profile";
                s.prop31 = '<%= MemberProfile.MemberID %>';
                s.t(); //send omniture updated values as page load
            }
        }
    }
    
    function toggleYNMPopup()
    {
        $j('#secretAdmirerPopup').toggle();
    }
    
    $j(document).ready(function(){
        var $allaccessTooltip = $j('#allaccess-tooltip');
        $allaccessTooltip.appendTo('body');
        $j('.allaccess-help').hover(function(e) {
               var x = e.pageX;
            var y = e.pageY - $j(window).scrollTop();
            var tooltipWidth = $allaccessTooltip.outerWidth();
            if ($j('html').attr('dir') == 'rtl') {
                $allaccessTooltip.css({ 'position': 'fixed', 'left': x - tooltipWidth, 'top': y - 10 }).show();
            } else {

                var leftSet;
                if (x < 540)
                    leftSet = x + 5;
                else
                    leftSet = x + 2;

                $allaccessTooltip.css({ 'position': 'fixed', 'left': leftSet, 'top': y - 10 }).show();
            };
        },
            function () {
                
                    setTimeout(function(){ $allaccessTooltip.hide();},4000);
                   
            }
        );
        $j('.btn, .textlink', '.vip-m2m-overlay').click(function(e) {
            $j('.ui-widget-overlay').hide();
        });

        $j(window).scroll(function() {
            $allaccessTooltip.hide();
        });

    });

</script>
<div id="profile30Comm" class="profile30-comm">
    <asp:PlaceHolder ID="phHideM2M" runat="server" Visible="false">
        <%--Block it if see your own profile--%>
        <script type="text/javascript">
            $j(function () {
                BlockElem($j('#profile30Comm'));
            });
        </script>
    </asp:PlaceHolder>
    <ul class="clearfix">
        <li class="online">
            <!--online-->
            <asp:HyperLink ID="lnkIMOnline" runat="server">
                <mn:Txt ID="txtIMOnline" ResourceConstant="PRO_IM" TitleResourceConstant="ALT_IM_ONLINE" runat="server" />
            </asp:HyperLink>
            <!--offline-->
            <mn:Txt ID="txtIMOffline" ResourceConstant="PRO_IM_OFF" TitleResourceConstant="PRO_OFFLINE" runat="server" CssClass="offline" />
        </li>
        <li class="flirt">
            <%--<asp:HyperLink ID="lnkFlirt" runat="server">--%>
            <asp:HyperLink ID="lnkFlirt" runat="server">
                <mn:Txt ID="txtFlirt" ResourceConstant="PRO_TEASE" TitleResourceConstant="ALT_TEASE_ME" runat="server" />
            </asp:HyperLink>
        </li>
        <asp:PlaceHolder ID="phEcardLink" runat="server">
            <li class="ecard">
                <asp:HyperLink ID="lnkEcard" runat="server">
                    <mn:Txt ID="txtEcard" ResourceConstant="PRO_ECARDS" TitleResourceConstant="ALT_ECARDS" runat="server" />
                </asp:HyperLink>
            </li>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phSecretAdmirer" runat="server">
            <li class="secret-admirer">
                <div class="bubble-area trigger-on-click">
                    <asp:HyperLink ID="lnkSecretAdmirer" runat="server" CssClass="trigger" NavigateUrl="#">
                        <mn:Txt ID="txtSeccretAdmirer" ResourceConstant="PRO_SECRETADMIRER" TitleResourceConstant="ALT_SECRETADMIRER" runat="server" />
                    </asp:HyperLink>
                    <div class="click20-horizontal">
                        <div class="click20-option">
                            <asp:HyperLink ID="lnkYes" runat="server">
                                <span id="spanYes" runat="server" class="spr s-icon-click-y-off"><span>
                                    <mn:Txt ID="txt5" runat="server" ResourceConstant="YNM_Y" />
                                </span></span>
                            </asp:HyperLink>
                        </div>
                        <div class="click20-option">
                            <asp:HyperLink ID="lnkNo" runat="server">
                                <span id="spanNo" runat="server" class="spr s-icon-click-n-off"><span>
                                    <mn:Txt ID="txt6" runat="server" ResourceConstant="YNM_N" />
                                </span></span>
                            </asp:HyperLink>
                        </div>
                        <div class="click20-option">
                            <asp:HyperLink ID="lnkMaybe" runat="server">
                                <span id="spanMaybe" runat="server" class="spr s-icon-click-m-off"><span>
                                    <mn:Txt ID="txt7" runat="server" ResourceConstant="YNM_M" />
                                </span></span>
                            </asp:HyperLink>
                        </div>
                    </div>
                    <div id="secretAdmirerPopup" style="display: none" class="secret-admirer bubble-layer bottom">
                        <h2 id="secretAdmirerPopup_title">
                            <span id="spanSecretAdmirerYY" runat="server" style="display: none;" class="spr s-icon-click-yy">
                                <span>
                                    <mn:Txt ID="txt4" runat="server" ResourceConstant="YNM_YY" />
                                </span></span>
                            <mn2:FrameworkLiteral ID="literalSecretAdmirerTitle" ResourceConstant="TXT_SECRETADMIRER_TITLE"
                                runat="server"></mn2:FrameworkLiteral>
                        </h2>
                        <p id="secretAdmirerPopup_desc" style="display: none" runat="server" class="description">
                            <mn2:FrameworkLiteral ID="literalSecretAdmirerDesc" ResourceConstant="TXT_SECRETADMIRER_DESC" runat="server"></mn2:FrameworkLiteral></p>
                        <p id="secretAdmirerPopup_descY" style="display: none" runat="server" class="description">
                            <mn2:FrameworkLiteral ID="literalSecretAdmirerDescY" ResourceConstant="TXT_SECRETADMIRER_DESC_YES" runat="server"></mn2:FrameworkLiteral></p>
                        <p id="secretAdmirerPopup_descN" style="display: none" runat="server" class="description">
                            <mn2:FrameworkLiteral ID="literalSecretAdmirerDescN" ResourceConstant="TXT_SECRETADMIRER_DESC_NO" runat="server"></mn2:FrameworkLiteral></p>
                        <p id="secretAdmirerPopup_descM" style="display: none" runat="server" class="description">
                            <mn2:FrameworkLiteral ID="literalSecretAdmirerDescM" ResourceConstant="TXT_SECRETADMIRER_DESC_MAYBE" runat="server"></mn2:FrameworkLiteral></p>
                        <p id="secretAdmirerPopup_descYY" style="display: none" runat="server" class="description">
                            <mn2:FrameworkLiteral ID="literalSecretAdmirerDescYY" ResourceConstant="TXT_SECRETADMIRER_DESC_MUTUALYES" runat="server"></mn2:FrameworkLiteral></p>
                    </div>
                    <%--input to hold current vote status, used by js--%>
                    <input id="YNMVoteStatus" type="hidden" name="YNMVoteStatus" runat="server" />
                </div>
            </li>
        </asp:PlaceHolder>
        <li class="favorite">
            <uc1:Add2List ID="add2List" runat="server" ClearIconInlineAlignments="true" Orientation="Horizontal"
                HotlistResourceConstant="TXT_FAVORITE" UnHotlistResourceConstant="TXT_UNFAVORITE"
                IncludeSpanAroundDescriptionText="true" ExcludeBlockAsFavorited="true" DisplayLargeIcon="false"
                EnableSprite="true" SpriteHotlistCSS="" SprintUnHotlistCSS="spr s-icon-favorites-added">
            </uc1:Add2List>
        </li>
    </ul>
    <div id="profileCommentBox" class="comment clearfix form-set">
        <%--Begin Icebreakers--%>
        <mnicb:IceBreaker runat="server" id="IceBreaker" Visible="true"/>
        <%--End Icebreakers--%>
        <h3><mn2:FrameworkLiteral ID="literalEmailTitle" runat="server" ResourceConstant="PRO_EMAILTITLE"></mn2:FrameworkLiteral></h3>
        <fieldset class="form-set-stacked">
            <asp:PlaceHolder ID="phSubjectAndTitleTags" runat="server" Visible="false">
                <label for="txtEmailSubject">Subject: <span class="optional">(optional)</span></label>
                <uc1:WatermarkTextbox ID="txtEmailSubject" runat="server" TextMode="SingleLine" CssClass="filled full"
                    WatermarkCssClass="watermark" WatermarkTextResourceConstant="TXT_EMAIL_WATERMARKSUBJECT" Visible="false">
                </uc1:WatermarkTextbox>
                <label for="txtEmailMessage">Message:</label>
                <%--this script to be removed once subject line is either adopted or removed. classes should be hardcoded on #profileCommentBox accordingly--%>
                <script type="text/javascript">
                    //document.getElementById("profileCommentBox").setAttribute("class","comment clearfix form-set comment-with-subject");
                    $j('#profileCommentBox').addClass('comment-with-subject');
                </script>
            </asp:PlaceHolder>
            <uc1:WatermarkTextbox ID="txtEmailMessage" runat="server" TextMode="MultiLine" CssClass="filled"
                WatermarkCssClass="watermark" WatermarkTextResourceConstant="TXT_EMAIL_WATERMARK">
            </uc1:WatermarkTextbox>
        </fieldset>
        <div id="emailSent" style="display: none" class="comment-sent">
            <mn:Image ID="imgIcon" runat="server" FileName="icon-email-confirmation.gif" />
            <%=g.GetResource("MESSAGE_SENT", this) %></div>
        <div id="emailEmpty" style="display: none" class="comment-sent comment-error">
            <mn:Image ID="Image2" runat="server" FileName="icon-page-message.gif" />
            <%=g.GetResource("MESSAGE_EMPTY", this) %></div>
        <div id="emailError" style="display: none" class="comment-sent comment-error">
            <mn:Image ID="Image3" runat="server" FileName="icon-page-message.gif" />
            <%=g.GetResource("MESSAGE_ERROR", this) %></div>
        <div id="emailErrorCapLimit" style="display: none" class="comment-sent comment-error">
            <mn:Image ID="Image1" runat="server" FileName="icon-page-message.gif" />
            <%=g.GetResource("ERROR_EMAIL_QUOTA_REACHED", this)%></div>
        <div id="msgAASuccessPurchaseOneCLick" style="display: none" class="comment-sent comment-error">
            <mn:Image ID="Image4" runat="server" FileName="icon-page-message.gif" />
            <%=g.GetResource("MESSAGE_ONE_CLICK_SUCCESS_PURCHASE_AA", this)%>
            <mn2:FrameworkButton CssClass="textlink msgOneClickSuccPurchaseAA" runat="server" ID="btnMsgOneClickSuccPurchaseAA" ResourceConstant="BTN_ONE_CLICK_CONTINUE"
                OnClientClick="M2MSendMessage(true, true, true);return false;" />
        </div>
        <div id="msgAAFailedPurchaseOneCLick" style="display: none" class="comment-sent comment-error">
            <mn:Image ID="Image5" runat="server" FileName="icon-page-message.gif" />
            <%=g.GetResource("MESSAGE_ONE_CLICK_FAILED_PURCHASE_AA", this)%>
             <mn2:FrameworkButton CssClass="textlink msgOneClickFailPurchaseAA" runat="server" ID="btnMsgOneClickFailPurchaseAA" ResourceConstant="BTN_ONE_CLICK_CONTINUE"
                OnClientClick="M2MSendMessage(false, true, true);return false;" />
        </div>                
        <%--All Access --%>
        <asp:PlaceHolder ID="phSendAsVIP" runat="server" Visible="false">
            <div id="mail-send-as-vip" >
                <asp:CheckBox runat="server" ID="cbSendAsVIP" Checked="false" />
                <asp:Label runat="server" ID="lblVIPRemaining" CssClass="allaccess-remaining"/> 
              
                <%--The two potential overlays for existing All Access member --%>
                <asp:PlaceHolder ID="phExistingVIPOverlay" runat="server" Visible="false">
                    <div id="divSendVIPOverlay" class="vip-m2m-overlay" style="display: none">
                        <mn2:FrameworkLiteral ID="literalVIPOverlay" runat="server"></mn2:FrameworkLiteral>
                        <div class="cta">
                            <mn2:FrameworkButton CssClass="btn primary" runat="server" ID="btnVIPOverlayYes"
                                ResourceConstant="BTN_OVERLAY_YES" OnClientClick="M2MSendMessage(true, true);return false;" />
                            <mn2:FrameworkButton CssClass="textlink" runat="server" ID="btnVIPOverlayNo" ResourceConstant="BTN_OVERLAY_NO"
                                OnClientClick="M2MSendMessage(false, true);return false;" />
                        </div>
                    </div>
                    <div id="divRanOutVIPOverlay" class="vip-m2m-overlay" style="display: none">
                        <mn2:FrameworkLiteral ID="literalRanoutVIPOverlay" runat="server"></mn2:FrameworkLiteral>
                        <div class="cta">
                            <mn2:FrameworkButton CssClass="btn primary" runat="server" ID="btnRanOutVIPOverlayYes"
                                ResourceConstant="BTN_LEARN_MORE" OnClientClick="M2MSendMessage(true, true);return false;" />
                            <mn2:FrameworkButton CssClass="textlink" runat="server" ID="btnRanOutVIPOverlayNo"
                                ResourceConstant="BTN_OVERLAY_NO" OnClientClick="M2MSendMessage(false, true);return false;" />
                        </div>
                    </div>
                </asp:PlaceHolder>

                <!-- One Click All Access Overlay - The two potential overlays for existing All Access member -->
                <asp:PlaceHolder runat="server" ID="plcOneClickAllAccessExistingVIP" Visible="False">
                    <div id="divOneClickSendVIPOverlay" class="vip-m2m-overlay one-click send-vip" style="display: none">
                        <mn2:FrameworkLiteral ID="literaOneClicklSendVIPOverlay" runat="server"></mn2:FrameworkLiteral>
                        <div class="cta">
                            <mn2:FrameworkButton CssClass="btn primary" runat="server" ID="FrameworkButton1"
                                ResourceConstant="BTN_OVERLAY_YES" OnClientClick="M2MSendMessage(true, true);return false;" />
                            <mn2:FrameworkButton CssClass="textlink" runat="server" ID="FrameworkButton2" ResourceConstant="BTN_OVERLAY_NO"
                                OnClientClick="M2MSendMessage(false, true);return false;" />
                        </div>
                    </div>
                    <div id="divOneClickRanOutVIPOverlay" class="vip-m2m-overlay one-click ran-out" style="display: none">
                        <mn2:FrameworkLiteral ID="literaOneClicklRanoutVIPOverlay" runat="server"></mn2:FrameworkLiteral>
                        <div class="cta">
                            <mn2:FrameworkLiteral ID="litOneClickRanOutInfo" runat="server"></mn2:FrameworkLiteral>
                        </div>
                        <div class="cta">
                            <mn2:FrameworkButton CssClass="btn primary" runat="server" ID="FrameworkButton3"
                                ResourceConstant="BTN_ONE_CLICK_RANOUT_PURCHASE" OnClientClick="M2MSendMessage(true, true, false, 'ran-out');return false;" />
                        </div>
                        <div class="cta">
                            <mn2:FrameworkButton CssClass="textlink" runat="server" ID="FrameworkButton7"
                                ResourceConstant="BTN_OVERLAY_NO" OnClientClick="M2MSendMessage(false, true, false, 'no-extra-msg');return false;" />
                        </div>
                    </div>
                </asp:PlaceHolder>

                <!-- One Click All Access Overlay - The single potential overlay for non All Access member -->
                <asp:PlaceHolder runat="server" ID="plcOneClickAllAccessNonVIP" Visible="False">
                    <div id="divOneClickNonVIPOverlay" class="vip-m2m-overlay one-click" style="display: none">
                        <mn2:FrameworkLiteral ID="literalOneClickNonVIPOverlay" runat="server"></mn2:FrameworkLiteral>
                        <div class="cta">
                            <mn2:FrameworkButton CssClass="btn primary" runat="server" ID="btnOneClickNonVIPOverlayYes"
                                ResourceConstant="BTN_ONE_CLICK_LEARN_MORE" OnClientClick="M2MSendMessage(true, true, false, 'vip-purchase');return false;" />
                        </div>
                        <div class="cta">
                            <mn2:FrameworkButton CssClass="textlink" runat="server" ID="FrameworkButton6" ResourceConstant="BTN_OVERLAY_NO"
                                OnClientClick="M2MSendMessage(false, true, false, 'no-extra-msg');return false;" />
                        </div>
                        <div class="cta">
                            <mn2:FrameworkLiteral ID="litOneClickPaymentInfo" runat="server"></mn2:FrameworkLiteral>
                        </div>
                        <div class="cta">
                            <mn:Txt ID="mntxtOneClickInfo" runat="server" ResourceConstant="HTML_ONE_CLICK_INFO" />
                        </div>
                    </div>
                </asp:PlaceHolder>                
                
                <%--The single potential overlay for non All Access member --%>
                <asp:PlaceHolder ID="phNonVIPOverlay" runat="server" Visible="false">
                    <div id="divNonVIPOverlay" class="vip-m2m-overlay " style="display: none">
                        <mn2:FrameworkLiteral ID="literalNonVIPOverlay" runat="server"></mn2:FrameworkLiteral>
                        <div class="cta">
                            <mn2:FrameworkButton CssClass="btn primary" runat="server" ID="btnNonVIPOverlayYes"
                                ResourceConstant="BTN_LEARN_MORE" OnClientClick="M2MSendMessage(true, true);return false;" />
                            <mn2:FrameworkButton CssClass="textlink" runat="server" ID="btnNonVIPOverlayNo" ResourceConstant="BTN_OVERLAY_NO"
                                OnClientClick="M2MSendMessage(false, true);return false;" />
                        </div>
                    </div>
                </asp:PlaceHolder>
            </div>
            <mn2:FrameworkButton ID="buttonSendVIPEmail" runat="server" CssClass="btn primary button-show"
                ResourceConstant="TXT_SEND" UseSubmitBehavior="false" OnClientClick="M2M_ShowVIPOverlay();return false;" />
        </asp:PlaceHolder>
        <%--Non All Access--%>
        <asp:PlaceHolder ID="phSendNonVIP" runat="server">
            <mn2:FrameworkButton ID="buttonSendEmail" runat="server" CssClass="btn primary button-show"
                ResourceConstant="TXT_SEND" UseSubmitBehavior="false" OnClientClick="M2MSendMessage(false, false);return false;" />
        </asp:PlaceHolder>
        <asp:Panel runat="server" ID="panelNonSub" CssClass="upgrade" Visible="false">
            <div class="txt-upgrade">
                <mn:Txt runat="server" ID="txtSubscribe" />
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="panelNotPassFraud" CssClass="upgrade" Visible="false">
            <div class="txt-upgrade fraud">
                <mn:Txt runat="server" ID="txtFraudCheck" />
            </div>
        </asp:Panel>
        <span id="draftStatus" class="hide float-outside"><mn:Txt runat="server" ResourceConstant="TXT_SAVE_TO_DRAFT" /></span>
        <div id="divHoverSendingMessage" style="display: none" class="pre-loader">
            <mn:Image ID="imageHoverBlock" runat="server" FileName="ajax-loader.gif" />
        </div>
    </div>
</div>
<script>
    var saveDraftMessage = {
        targetMemId: "<%=MemberProfile.MemberID%>",
        memMailId: "-1",
        inputSubject: null,
        inputBody: null,
        defaultMessageSubject: "<%=DefaultSubject%>",
        messageSubject: "<%=DefaultSubject%>",
        defaultMessageBody: "<%=DefaultBodyMessage%>",
        messageBody: "<%=DefaultBodyMessage%>",
        indicator: null,
        oldSubject: "",
        oldBody: "",
        sendNewDraft: function (callback) {
            $j.ajax({
                type: "POST",
                url: "/Applications/API/IMailService.asmx/SendMessage",
                data: "{'targetMemberID':'" + saveDraftMessage.targetMemId + "','messageSubject':'" + saveDraftMessage.encodeURIComponent(saveDraftMessage.messageSubject) + "','message':'" + saveDraftMessage.encodeURIComponent(saveDraftMessage.messageBody) + "','sendAsVIP':'false','sendAsVIPFromLayoverButton':'false','saveAsDraft':'true'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                    //console.log("NEW DRAFT");
                },
                error: function (e) {
                    window.clearInterval(saveDraftInterval);
                    //console.log("ERROR: ", e);
                }
            });
        },
        saveDraft: function(callback){
            $j.ajax({
                type: "POST",
                url: "/Applications/API/IMailService.asmx/SaveMessageDraft",
                data: "{'targetMemberID':'" + saveDraftMessage.targetMemId + "','messageID':'" + saveDraftMessage.memMailId + "','messageSubject':'" + saveDraftMessage.encodeURIComponent(saveDraftMessage.messageSubject) + "','message':'" + saveDraftMessage.encodeURIComponent(saveDraftMessage.messageBody) + "','sendAsVIP':'false','sendAsVIPFromLayoverButton':'false','saveAsDraft':'true'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    callback(response);
                    //console.log("UPDATE DRAFT");
                },
                error: function (e) {
                    window.clearInterval(saveDraftInterval);
                    //console.log("ERROR: ", e);
                }
            });
        },
        updateData: function (data) {
            // callback function that fired when ajax returned
            var sdm = saveDraftMessage;

            sdm.memMailId = data.d.MemberMailID;
            sdm.oldBody = sdm.inputBody.val();
            sdm.updateIndicator();

            //console.log("MAILID: ", data.d.MemberMailID);
            //console.log("AJAX RESPONSE: ", data.d);
            //console.log("OBJECT: ", saveDraftMessage);
        },
        updateIndicator: function () {
            saveDraftMessage.indicator.show().delay(6000).fadeOut();
        },
        getParameterByName: function(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },
        encodeURIComponent: function (str) {
            return encodeURIComponent(str).replace(/[!'()]/g, escape).replace(/\*/g, "%2A");
        },
        composeMessage: function () {
            var sdm = saveDraftMessage;
            var theBody = sdm.inputBody.val();

            // if body is empty or unchanged return false
            if (theBody == "" || sdm.oldBody == theBody)
            {
                return false;
            }
            else
            {
                if (theBody) {
                    sdm.messageBody = theBody;
                }
                else
                {
                    sdm.messageBody = sdm.defaultMessageBody;
                }

                return true;
            }

        }
    };

    $j(function () {
        // update initial data of saveDraftMessage object at DOM ready
        saveDraftMessage.inputBody = $j("[id*='txtEmailMessage']");
        saveDraftMessage.indicator = $j("#draftStatus").css({ 'font-weight': 'bold', 'margin': '8px 12px -8px 0' });

        if (saveDraftMessage.getParameterByName('MemberMailID') !== "") {
            saveDraftMessage.memMailId = saveDraftMessage.getParameterByName('MemberMailID');
        }

    });

    var saveDraftInterval = window.setInterval(function () {
        var sdm = saveDraftMessage;

        if (sdm.composeMessage())  // if both subject and body are not empty lets send.
        {
            if (sdm.memMailId < 0)
                sdm.sendNewDraft(sdm.updateData);
            else
                sdm.saveDraft(sdm.updateData);
        }

    }, 5000);
</script>
<asp:PlaceHolder runat="server" ID="plcGAMAdSlotBelowCompose" Visible="false">
    <div id="iframe_container">
        <uc1:AdUnit ID="adHalfBanner" runat="server" Size="HalfBanner" GAMAdSlot="profile_myownwords_belowcompose_234x60"
            GAMPageMode="None" GAMIframe="true" GAMIframeWidth="234" GAMIframeHeight="60"
            ExpandImageTokens="true" />
    </div>
</asp:PlaceHolder>