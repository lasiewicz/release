﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Lib;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.MatchTest.ValueObjects.MatchMeter;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.CompatibilityMeter;
using Matchnet.Web.Applications.Email;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab
{
    public partial class JMeterTab : BaseTab
    {
        private const int MESSAGE_BODY_MAX_CONCURRENT = 30;

        public JMeterTab()
        {
            this.ProfileTabType = ProfileTabEnum.JMeter;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public override void LoadTab(IMemberDTO member, bool isActive)
        {
            try
            {
                this._MemberProfile = member;
                this.IsActive = isActive;
                if (g.Member != null && member.MemberID == g.Member.MemberID)
                    this.WhoseProfile = WhoseProfileEnum.Self;

                CompatibilityMeterHandler cmh = new CompatibilityMeterHandler(g);

                MatchTestStatus viewingMemberMTS = cmh.GetMatchTestStatus(g.Member);
                
                if (IsViewingOwnProfile)
                {
                    switch (viewingMemberMTS)
                    {
                        case MatchTestStatus.NotTaken:
                            plcWelcome.Visible = true;
                            break;
                        case MatchTestStatus.NotCompleted:
                            plcTest.Visible = true;
                            break;
                        case MatchTestStatus.MinimumCompleted:
                        case MatchTestStatus.Completed:
                            plcPersonalFeedback.Visible = true;
                            lnkMatches.NavigateUrl = "/Applications/CompatibilityMeter/matches.aspx";
                            if (!cmh.IsJmeterPremium())
                            {
                                lnkMatches.NavigateUrl = FrameworkGlobals.GetSubscriptionLink(false, 1412);
                            }
                            var pfb = MatchMeterSA.Instance.GetPersonalFeedback(g.Member.MemberID, g.Brand, MemberPrivilegeAttr.IsCureentSubscribedMember(g));
                            personalFeedbackTop.PFB = pfb;
                            personalFeedbackTop.ViewProfileMode = true;
                            break;
                        default:
                            break;
                    }
                    lnkMatches.Visible = cmh.IsJMeterMatchesEnabled();
                }
                else
                {
                    if (viewingMemberMTS == MatchTestStatus.NotTaken || viewingMemberMTS == MatchTestStatus.NotCompleted)
                    {
                        plcWelcome.Visible = true;
                    }
                    else
                    {
                        // var pfbViewedMember = MatchMeterSA.Instance.GetPersonalFeedback(_MemberProfile.MemberID, g.Brand, MemberPrivilegeAttr.IsCureentSubscribedMember(g));
                        var viewedMemberStatus = cmh.GetMatchTestStatus(_MemberProfile);

                        switch (viewedMemberStatus)
                        {
                            case MatchTestStatus.NotTaken:
                            case MatchTestStatus.NotCompleted:
                                SetSendInvitation(member);
                                break;
                            case MatchTestStatus.MinimumCompleted:
                            case MatchTestStatus.Completed:
                                if (cmh.IsJmeterPremium() || !cmh.CheckJMeterFlag(WebConstants.MatchMeterFlags.PayFor1On1) || !cmh.CheckJMeterFlag(WebConstants.MatchMeterFlags.PremiumFor1On1))
                                {
                                    plcOneOnOne.Visible = true;
                                    oneOneOneTop.ViewdMember = _MemberProfile;
                                    var ooofb = MatchMeterSA.Instance.GetOneOnOne(g.Member.MemberID, MemberPrivilegeAttr.IsCureentSubscribedMember(g), _MemberProfile.MemberID, _MemberProfile.IsPayingMember(g.Brand.Site.SiteID), g.Brand);
                                    oneOneOneTop.OOOFB = ooofb;
                                }
                                else
                                {
                                    plcOneOnOneNonPremuium.Visible = true;
                                    lnkGetPremium.NavigateUrl = FrameworkGlobals.GetSubscriptionLink(false, 659);
                                }
                                break;
                            default:
                                break;
                        }
                    }

                    lnkMatches1.Visible = cmh.IsJMeterMatchesEnabled();
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void SetSendInvitation(IMemberDTO member)
        {
            plcInvitation.Visible = true;
            int memberID = member.MemberID;

            IMemberDTO aMember = member;

            txtMessage.Text = g.GetResource("TEXTBOX_CONTENT", this).Replace("[USERNAME]", g.Member.GetUserName(_g.Brand));
            if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
            {
                txtMessage.Enabled = true;
            }


            CompatibilityMeterHandler cmh = new CompatibilityMeterHandler(g);
            string notification = cmh.CheckSendInvitationNotification(aMember);
            if (string.IsNullOrEmpty(notification))
            {

                int memGenderMask = g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                int targetMemGenderMask = aMember.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                string currentMemberSex = ((memGenderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE) ? "M" : "F";
                string targetMemberSex = ((targetMemGenderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE) ? "M" : "F";

                string txt = g.GetResource("TXT_TEXT_MALE", this);
                if (targetMemberSex == "F")
                {
                    txt = g.GetResource("TXT_TEXT_FEMALE", this);
                }

                lblText.Text = txt.Replace("[USERNAME]", aMember.GetUserName(_g.Brand));
                string txt2 = g.GetResource("TXT_TEXT2_MALE2FEMALE", this); // M sends to F
                imgSendInvitation.FileName = "jmeter-tab-invite-her.png";
                imgSendInvitation.TitleResourceConstant = "TTL_INVITE_MF";
                if (currentMemberSex == "M" && targetMemberSex == "M") // M sends to M
                {
                    txt2 = g.GetResource("TXT_TEXT2_MALE2MALE", this);
                    imgSendInvitation.FileName = "jmeter-tab-invite-him-male.png";
                    imgSendInvitation.TitleResourceConstant = "TTL_INVITE_MM";
                }
                else
                {
                    if (currentMemberSex == "F" && targetMemberSex == "M") // F sends to M
                    {
                        txt2 = g.GetResource("TXT_TEXT2_FEMALE2MALE", this);
                        imgSendInvitation.FileName = "jmeter-tab-invite-him.png";
                        imgSendInvitation.TitleResourceConstant = "TTL_INVITE_FM";
                    }
                    else
                    {
                        if (currentMemberSex == "F" && targetMemberSex == "F") // F sends to F
                        {
                            txt2 = g.GetResource("TXT_TEXT2_FEMALE2FEMALE", this);
                            imgSendInvitation.FileName = "jmeter-tab-invite-her-female.png";
                            imgSendInvitation.TitleResourceConstant = "TTL_INVITE_FF";
                        }
                    }
                }
                lblText2.Text = txt2;

            }
            else
            {
                ShowNotification(notification);
            }

        }

        protected void lnkSendInvitation_Click(object sender, EventArgs e)
        {
            if (compileSendMessage())
            {
                // Update sending member quota
                QuotaSA.Instance.AddQuotaItem(g.Brand.Site.Community.CommunityID, g.Member.MemberID, QuotaType.SendJmeterInvitation);
                // Upadte source member quota 
                //QuotaSA.Instance.AddQuotaItem(g.Brand.Site.Community.CommunityID, getMemberId(), QuotaType.ReceiveJmeterInvite);

                // save the to member in the current member's hotlist
                /* ListSA.Instance.AddListMember(HotListCategory.MembersYouEmailed,
                      g.Brand.Site.Community.CommunityID,
                      g.Brand.Site.SiteID,
                      g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                      messageSave.ToMemberID,
                      null,
                      Constants.NULL_INT,
                      true,
                      ignoreQuota);*/


                //Save the member in the member you sent jmeter invitation to
                ListSA.Instance.AddListMember(HotListCategory.MemberYouSentJmeterInvitationTo,
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID,
                    g.Member.MemberID,
                    _MemberProfile.MemberID,
                    string.Empty,
                    0,
                    false,
                    true);

                ShowNotification("JMETER_INVITATION_SUCCESSFULLY_SENT");
            }
            else
            {
                //g.Notification.AddError("TXT_MESSAGE_SENT_FAILURE");
                ShowNotification("TXT_MESSAGE_SENT_FAILURE");
            }

        }

        private void ShowNotification(string notificationResource)
        {
            g.Notification.AddMessage(notificationResource);
        }


        public bool IsViewingOwnProfile
        {
            get
            {
                return (this.WhoseProfile == WhoseProfileEnum.Self);
            }
        }

        // compiles and attempts to send/save the message
        private bool compileSendMessage()
        {
            string messageSubject;
            string messageBody;

            try
            {
                messageSubject = g.GetResource("INVITATION_SUBJECT", this).Trim();
                messageBody = txtMessage.Text.Trim();

                if (messageSubject.Length <= 0)
                {
                    g.Notification.AddError("TXT_MESSAGE_SUBJECT_BLANK");
                    return false;
                }

                if (messageBody.Length <= 0)
                {
                    g.Notification.AddError("TXT_MESSAGE_BODY_BLANK");
                    return false;
                }

                Int32 toMemberID;

                toMemberID = _MemberProfile.MemberID;

                // parse out all html tags
                messageSubject = Regex.Replace(messageSubject, "<[^>]*>", "");
                messageBody = Regex.Replace(formatMessageBody(messageBody), "<[^>]*>", "");

                MailType mailType = MailType.CompatibilityMeterEmail;

                MessageSave messageSave = new MessageSave(g.Member.MemberID,
                    toMemberID,
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID,
                    mailType,
                    messageSubject,
                    messageBody);

                messageSave.MessageListID = 0;// getMessageId();

                // If the sender is blocked by the receiver, send a blocked message to the sender and halt sending to the receiver
                bool blocked = false;
                try
                {
                    Member.ServiceAdapters.Member toMember = MemberSA.Instance.GetMember(messageSave.ToMemberID, MemberLoadFlags.None);
                    MessageSendResult sendResult = EmailMessageSA.Instance.CheckAndSendMessageForBlockedSenderInRecipientList(messageSave.GroupID, string.Format(_g.GetResource("TXT_IGNORE_SUBJECT", this), messageSave.MessageHeader), string.Format(_g.GetResource("TXT_IGNORE_CONTENT", this), toMember.GetUserName(_g.Brand)),
                                                                                                                              toMember.MemberID, _g.Member.MemberID, _g.Brand.Site.SiteID,
                                                                                                                              _g.Brand.Site.Community.CommunityID);
                    blocked = (null != sendResult && sendResult.Status == MessageSendStatus.Success);
                    ComposeHandler.LogIgnoreEMail(_g, _g.Member.MemberID.ToString(), messageSave.ToMemberID.ToString());
                }
                catch (Exception e)
                {
                    g.LoggingManager.LogException(e, _g.Member, _g.Brand, "JMeterTab");
                }
                if (blocked) return true;

                // Try to send email message
                MessageSendResult messageSendResult = EmailMessageSA.Instance.SendMessage(messageSave, true, 0, true);

                switch (messageSendResult.Status)
                {
                    case MessageSendStatus.Success:
                        // Check to see if we need to send a "Recipient on Vacation" email.
                        if (IsMemberOnVacation(messageSave.ToMemberID))
                        {
                            Member.ServiceAdapters.Member toMember = MemberSA.Instance.GetMember(messageSave.ToMemberID, MemberLoadFlags.None);

                            MessageSave vacationMessage = new MessageSave(messageSave.ToMemberID,
                                g.Member.MemberID,
                                g.Brand.Site.Community.CommunityID,
                                g.Brand.Site.SiteID,
                                MailType.Email,
                                String.Format(g.GetResource("TXT_VACATION_SUBJECT", this), messageSave.MessageHeader),
                                String.Format(g.GetResource("TXT_VACATION_CONTENT", this), toMember.GetUserName(_g.Brand)));

                            EmailMessageSA.Instance.SendMessage(vacationMessage, false, Constants.NULL_INT, false);
                        }

                        // send an external mail to let recipient know that he/she has a message

                        return true;

                    case MessageSendStatus.Failed:
                        // one cause of failure is the per day send limit
                        if (messageSendResult.Error == MessageSendError.ComposeLimitReached)
                        {

                            /*   g.Transfer("/Applications/Email/TeaseTooMany.aspx?" + TeaseTooMany.QSPARAM_ERROR_OCCURRED + "=true&" + TeaseTooMany.QSPARAM_ERROR_TYPE + "=email");*/
                        }
                        // everything else is unknown
                        else
                        {
                            throw new Exception("Unknown exception occured in compileSendMessage().");
                        }
                        return false;

                    default:
                        return false;
                }
            }
            catch (Exception anException)
            {
                g.ProcessException(anException);
            }
            return false;
        }

        private Boolean IsMemberOnVacation(Int32 toMemberID)
        {
            // Retrieve the ToMember's VacationStartDate.
            Member.ServiceAdapters.Member toMember = MemberSA.Instance.GetMember(toMemberID, MemberLoadFlags.None);

            DateTime vacationStartDate = toMember.GetAttributeDate(g.Brand, "VacationStartDate");

            if (vacationStartDate == DateTime.MinValue)
                return false;
            else
                return true;
        }

        // buffer out the message body with spaces for every 30 characters encountered.
        private string formatMessageBody(string messageBody)
        {
            int currentCount = 0;

            for (int i = 0; i < messageBody.Length; i++)
            {
                if (messageBody[i].Equals(' ') || messageBody[i].Equals('\n'))
                {
                    currentCount = 0;
                }
                else
                {
                    currentCount++;
                    if (currentCount == MESSAGE_BODY_MAX_CONCURRENT)
                    {
                        messageBody = messageBody.Insert(i, " ");
                        currentCount = 0;
                    }
                }
            }
            return messageBody;
        }
    }
}
