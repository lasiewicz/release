﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab
{
    /// <summary>
    /// This control loads likes and interests data
    /// </summary>
    public partial class InterestsTab : BaseTab
    {
        public InterestsTab()
        {
            this.ProfileTabType = ProfileTabEnum.Interests;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public override void LoadTab(IMemberDTO member, bool isActive)
        {
            this._MemberProfile = member;
            this.IsActive = isActive;
            if (g.Member != null && member.MemberID == g.Member.MemberID)
            {
                this.WhoseProfile = WhoseProfileEnum.Self;                
            }

            if (FacebookManager.Instance.IsFacebookLikesInterestsDataEnabled(g.Brand))
            {
                FBLikesWidget.Visible = true;
                FBLikesWidget.TabContentContainerID = TabContentContainerID;
                FBLikesWidget.LoadLikesWidget(member);
            }

            phPopupAd.Visible = !BreadCrumbHelper.IsProfilePopupDisabled(member.MemberID);
            if (g.LayoutTemplate == Content.ValueObjects.PageConfig.LayoutTemplate.WidePopup)
                phPopupAd.Visible = true;

            essaysDataGroup.LoadDataGroup(member, ProfileUtility.GetProfileDataGroup(ProfileDataGroupEnum.LikesInterests, g.Brand.Site.SiteID, g));
        }
    }
}