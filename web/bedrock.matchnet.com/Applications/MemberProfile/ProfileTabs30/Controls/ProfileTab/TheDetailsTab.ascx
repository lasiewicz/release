﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TheDetailsTab.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab.TheDetailsTab" %>
<%@ Register TagPrefix="uc1" TagName="ProfileDetails" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDetails.ascx" %>

<asp:PlaceHolder ID="phPopupAd" runat="server" Visible="false">
    <div class="ad-reservior"></div>
</asp:PlaceHolder>

<uc1:ProfileDetails ID="profileDetails" runat="server" />