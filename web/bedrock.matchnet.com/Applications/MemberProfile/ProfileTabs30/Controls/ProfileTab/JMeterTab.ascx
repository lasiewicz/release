﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JMeterTab.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab.JMeterTab" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="PersonalFeedbackTop" Src="~/Applications/CompatibilityMeter/Controls/PersonalFeedbackTop.ascx" %>
<%@ Register TagPrefix="uc1" TagName="OneOnOneTop" Src="~/Applications/CompatibilityMeter/Controls/OneOnOneTop.ascx" %>
<div class="jmeter-tab-left">
    <asp:PlaceHolder runat="server" ID="plcSidebar">
        <mn:Image runat="server" ID="imgJmeterLogo" FileName="jmeter-logo.png" />
    </asp:PlaceHolder>
</div>
<div class="jmeter-tab-main">
    <asp:PlaceHolder runat="server" ID="plcWelcome" Visible="False">
        <h1 class="narrow">
            <mn:Txt runat="server" ID="ttlJmeterTitle" ResourceConstant="TTL_WELCOME_TITLE" />
        </h1>
        <div class="jmeter-tab-welcome">
            <mn:Txt runat="server" ID="txtJmeterbWelcome" ResourceConstant="TAB_WELCOME_TEXT" />
        </div>
        <a class="jmeter-tab-start" href="/Applications/CompatibilityMeter/Welcome.aspx">
        </a></asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcTest" Visible="False">
        <div class="jmeter-tab-complete">
            <mn:Txt runat="server" ID="txtTabPreUser" ResourceConstant="TAB_FINISH_PRE" />
            <%=g.Member.GetUserName(g.Brand) %>
            <mn:Txt runat="server" ID="txtTabFinish" ResourceConstant="TAB_FINISH_TEXT" />
            <a href="/Applications/CompatibilityMeter/PersonalityTest.aspx"></a>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcPersonalFeedback" Visible="False">
        <div class="one-on-one-titles">
            <mn:Title runat="server" ID="ttlOneOnOne" ResourceConstant="TTL_FEEDBACK" />
            <h2>
                <mn:Txt runat="server" ID="ttlSubOneOnOne" ResourceConstant="TTL_SUB_FEEDBACK" />
            </h2>
        </div>
        <uc1:PersonalFeedbackTop runat="server" ID="personalFeedbackTop" />
        <div class="jmeter-tab-buttons">
            <asp:HyperLink runat="server" ID="lnkMatches" class="jmeter-tab-matches"></asp:HyperLink>
            <a href="/Applications/CompatibilityMeter/Completion.aspx" class="jmeter-tab-feedback">
            </a>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcReturningUser" Visible="False">Returning User
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcInvitation" Visible="False">
        <div class="tab-invitation">
            <h1 class="narrow">
                <mn:Txt runat="server" ID="txtInvitationTtl" ResourceConstant="TTL_INVITATION_TITLE" />
            </h1>
            <div class="sub-title">
                <mn:Txt runat="server" ID="txtInvitationSub" ResourceConstant="TTL_INVITATION_SUB" />
            </div>
            <p>
                <asp:Label runat="server" ID="lblText"></asp:Label>
            </p>
            <p>
                <asp:Label runat="server" ID="lblText2"></asp:Label>
            </p>
            <p>
                <asp:TextBox runat="server" ID="txtMessage" Enabled="false" TextMode="MultiLine"
                    Rows="5"></asp:TextBox></p>
            <p>
                <asp:LinkButton runat="server" ID="lnkSendInvitation" OnClick="lnkSendInvitation_Click">
                    <mn:Image runat="server" ID="imgSendInvitation" FileName="jmeter_btn_invite_male.gif"
                        ResourceConstant="TXT_SEND_INVITATION" />
                </asp:LinkButton>
            </p>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcOneOnOne" Visible="False">
        <div class="jm-tab-1on1">
            <h1 class="narrow">
                <mn:Txt runat="server" ID="txt1" ResourceConstant="TTL_1ON1_TEXT" />
            </h1>
            <div class="sub-title">
                <mn:Txt runat="server" ID="txt2" ResourceConstant="TTL_1ON1_SUB" />
            </div>
            <uc1:OneOnOneTop runat="server" ID="oneOneOneTop" />
            <asp:HyperLink runat="server" NavigateUrl="/Applications/CompatibilityMeter/Matches.aspx"
                ID="lnkMatches1" CssClass="jmeter-tab-matches-small"></asp:HyperLink>
            <a href="/Applications/CompatibilityMeter/Completion.aspx" class="jmeter-tab-feedback">
            </a>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcOneOnOneNonPremuium" Visible="False">
        <div class="jm-tab-cta">
            <asp:HyperLink runat="server" ID="lnkGetPremium" />
        </div>
    </asp:PlaceHolder>
</div>
