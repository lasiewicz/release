﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ValueObjects.Interfaces;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab
{
    /// <summary>
    /// Base class for Tab controls, which are responsible for organizing controls 
    /// to display content within the tab
    /// </summary>
    public class BaseTab : BaseProfile
    {
        #region Properties
        protected bool IsActive { get; set; }
        public ProfileTabEnum ProfileTabType { get; protected set; }
        public string TabContentContainerID { get; set; }
        #endregion

        public BaseTab()
        {
            this.ProfileTabType = ProfileTabEnum.None;
        }

        /// <summary>
        /// Loads the member's profile information; Derived classes should override and provide specific implementation.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="isActive"></param>
        public virtual void LoadTab(IMemberDTO member, bool isActive)
        {
        }
    }
}
