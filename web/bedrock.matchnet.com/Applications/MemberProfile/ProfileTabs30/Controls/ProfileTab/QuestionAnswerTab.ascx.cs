﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using Matchnet.Web.Applications.QuestionAnswer;
using Matchnet.Web.Applications.QuestionAnswer.Controls;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.Web.Applications.MemberLike;
using Matchnet.Member.ServiceAdapters;
using System.Collections;
using System.Web.UI.HtmlControls;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab
{
    public partial class QuestionAnswerTab : BaseTab
    {
        private Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question[] questions;
        private List<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> nonMatchingQuestions;
        private Hashtable myQuestions;

        public QuestionAnswerTab()
        {
            this.ProfileTabType = ProfileTabEnum.Questions;
        }

        #region Properties
        public bool IsCommentsEnabled
        {
            get { return QuestionAnswerHelper.IsQuestionAnswerCommentEnabled(g.Brand); }
        }

        public bool IsMemberLikeEnabled
        {
            get { return MemberLikeHelper.Instance.IsMemberLikeEnabled(g.Brand); }
        }

        public bool IsLoggedIn
        {
            get { return null != g.Member && g.Member.MemberID > 0; }
        }

        public int MemberId
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.MemberID;
                }
                return 0;
            }
        }

        public string MemberUserName
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.GetUserName(g.Brand);
                }
                return string.Empty;
            }
        }

        public bool IsPayingMember
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.IsPayingMember(g.Brand.Site.SiteID);
                }
                return false;
            }
        }

        public bool IsViewingOwnProfile
        {
            get
            {
                return (this.WhoseProfile == WhoseProfileEnum.Self);
            }
        }

        #endregion

        

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Handler for PreRender event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {

            base.OnPreRender(e);
        }

        protected void repeaterQuestionAnswers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question = e.Item.DataItem as Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question;
                Literal literalQuestion = (Literal)e.Item.FindControl("literalQuestion");
                HyperLink linkQuestion = (HyperLink)e.Item.FindControl("lnkQuestion");
                Literal literalAnswer = (Literal)e.Item.FindControl("literalAnswer");
                PlaceHolder phPendingAnswer = (PlaceHolder)e.Item.FindControl("phPendingAnswer");
                LastTime lastTimeAction = (LastTime)e.Item.FindControl("lastTimeAction");
                
                if (WhoseProfile == WhoseProfileEnum.Self)
                {
                    //show the "remove" button, we need to update this to ajax later
                    HtmlInputButton btnDelete = (HtmlInputButton)e.Item.FindControl("btnDelete");
                    btnDelete.Value = g.GetResource("TXT_DELETE", this);
                    btnDelete.Attributes.Add("onclick", "ShowQADeleteConfirmation(" + question.Answers[0].AnswerID.ToString() + ", " + question.QuestionID.ToString() + ")");
                }

                literalQuestion.Text = question.Text;
                linkQuestion.NavigateUrl += "?" + WebConstants.URL_PARAMETER_NAME_QUESTION_ID + "=" + question.QuestionID;
                if (question.Answers.Count > 0)
                {
                    literalAnswer.Text = question.Answers[0].AnswerValue.Replace(Environment.NewLine, "<br />"); ;
                    lastTimeAction.LoadLastTime(question.Answers[0].UpdateDate);
                    if (question.Answers[0].AnswerStatus == QuestionAnswerEnums.AnswerStatusType.Pending)
                    {
                        phPendingAnswer.Visible = true;
                    }
                    else
                    {
                        phPendingAnswer.Visible = false;
                        if (IsViewingOwnProfile)
                        {
                            MemberLikeAnswerClientModule memberLikeClientModule = e.Item.FindControl("memberLikeClientModule") as MemberLikeAnswerClientModule;
                            if (null != memberLikeClientModule)
                            {
                                memberLikeClientModule.MyEntryPoint = Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.MemberLikeLink;
                                memberLikeClientModule.Ordinal = e.Item.ItemIndex;
                                memberLikeClientModule.LoadMemberLikeClentModule(question.Answers[0]);
                            }
                        }
                    }
                    if (IsCommentsEnabled)
                    {
                        Literal literalCommentId = (Literal)e.Item.FindControl("commentAnswerId");
                        Literal literalCommentMemberId = (Literal)e.Item.FindControl("commentMemberId");
                        Literal literalCommentLabelId = (Literal)e.Item.FindControl("commentLabelId");
                        FrameworkButton literalCommentBtn = (FrameworkButton)e.Item.FindControl("buttonAnswerId");
                        literalCommentId.Text = question.Answers[0].AnswerID.ToString();
                        literalCommentMemberId.Text = this.MemberProfile.MemberID.ToString();
                        literalCommentBtn.ID = string.Format("|{0}|{1}|{2}", question.Answers[0].AnswerID, this.MemberProfile.MemberID, question.QuestionID);
                        literalCommentLabelId.Text = string.Format("comment-{0}-{1}", question.Answers[0].AnswerID, this.MemberProfile.MemberID);
                    }
                    if (IsMemberLikeEnabled && !IsViewingOwnProfile)
                    {
                        Literal literalQuestionId = (Literal)e.Item.FindControl("questionId");
                        Literal literalLikeAnswerId = (Literal)e.Item.FindControl("likeAnswerId");
                        Literal literalLikeText = (Literal)e.Item.FindControl("likeTxt");
                        int answerId = question.Answers[0].AnswerID;
                        literalLikeAnswerId.Text = answerId.ToString();
                        literalQuestionId.Text = question.QuestionID.ToString();

                        MemberLikeParams likeParams = new MemberLikeParams();
                        likeParams.SiteId = g.Brand.Site.SiteID;
                        likeParams.MemberId = MemberId;
                        List<MemberLikeObject> likes = MemberLikeHelper.Instance.GetLikesByObjectIdAndType(likeParams, answerId, Convert.ToInt32(LikeObjectTypes.Answer));
                        int likeCount = 0;
                        bool hasLike = false;
                        if (null != likes)
                        {
                            likes = likes.FindAll(delegate(MemberLikeObject likeObject)
                            {
                                Matchnet.Member.ServiceAdapters.Member _Member = MemberSA.Instance.GetMember(likeObject.MemberId, MemberLoadFlags.None);
                                return !MemberLikeHelper.Instance.IsMemberSuppressed(_Member, g);
                            });
                            MemberLikeObject match = likes.Find(delegate(MemberLikeObject obj)
                            {
                                bool b = (obj.MemberId == MemberId);
                                return b;
                            });
                            hasLike = (null != match);
                            likeCount = likes.Count;
                        }

                        string likeNumber = string.Empty;
                        if (likeCount > 0)
                        {
                            likeNumber = string.Format(g.GetResource("TXT_LIKE_NUM", this), likeCount);
                        }

                        string likeResKey = (hasLike) ? "TXT_ANSWER_UNLIKE" : "TXT_ANSWER_LIKE";
                        literalLikeText.Text = string.Format(g.GetResource(likeResKey, this), likeNumber);
                        literalLikeText.Visible = true;
                    }
                }
            }
        }

        #endregion

        #region Public Methods
        public override void LoadTab(IMemberDTO member, bool isActive)
        {
            this._MemberProfile = member;
            this.IsActive = isActive;
            if (g.Member != null && member.MemberID == g.Member.MemberID)
                this.WhoseProfile = WhoseProfileEnum.Self;

            btnNo.Value = g.GetResource("TXT_NO", this);
            btnYes.Value = g.GetResource("TXT_YES", this);

            //this processes postback for removing an answer
            //note: change to ajax later
            if (Page.IsPostBack && !String.IsNullOrEmpty(Request.Form["hidAction"]) && Request.Form["hidAction"] == "remove")
            {
                RemoveAnswer();
            }

            //display Q&A
            if (QuestionAnswerHelper.IsQuestionAnswerEnabled(g.Brand))
            {
                if (!IsViewingOwnProfile)
                {
                    //get question and answers for member                
                    myQuestions = QuestionAnswerHelper.GetLoggedInMemberQuestions(g);
                }

                IComparer<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> comparer = new UpdateDateQuestionComparer();
                if (g.Member != null && g.Member.MemberID == this.MemberProfile.MemberID)
                {
                    questions = QuestionAnswerHelper.GetMemberQuestions(MemberProfile.MemberID, g.Brand.Site.SiteID, false, comparer);
                }
                else
                {
                    questions = QuestionAnswerHelper.GetMemberQuestions(MemberProfile.MemberID, g.Brand.Site.SiteID, true, comparer);
                }

                if (questions.Length > 0)
                {
                    if (isQandADoubleEnabled())
                    {
                        List<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> matchingQuestions = null;
                        phHasAnswers.Visible = false;

                        if (!IsViewingOwnProfile)
                        {
                            foreach (Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question q in questions)
                            {
                                if (null != myQuestions && myQuestions.ContainsKey(q.QuestionID))
                                {
                                    if (null == matchingQuestions) matchingQuestions = new List<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question>();
                                    matchingQuestions.Add(q);
                                }
                                else
                                {
                                    if (null == nonMatchingQuestions) nonMatchingQuestions = new List<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question>();
                                    nonMatchingQuestions.Add(q);
                                }
                            }

                            BothAnsweredQuestionModule bothAnswered = this.FindControl("BothAnsweredQuestionModule1") as BothAnsweredQuestionModule;
                            bothAnswered.LoadModule(_MemberProfile, matchingQuestions);
                            bothAnswered.Visible = true;

                            BlockedAnsweredQuestionModule blockedAnswers = this.FindControl("BlockedAnsweredQuestionModule1") as BlockedAnsweredQuestionModule;
                            blockedAnswers.HasMatchingQuestions = (null != matchingQuestions && matchingQuestions.Count > 0);
                            blockedAnswers.LoadModule(_MemberProfile, nonMatchingQuestions);                            
                            blockedAnswers.Visible = true;
                        }
                        else
                        {
                            phHasAnswersDouble.Visible = false;
                            phHasAnswers.Visible = true;
                            repeaterQuestionAnswers.DataSource = questions;
                            repeaterQuestionAnswers.DataBind();
                        }
                    }
                    else
                    {
                        phHasAnswersDouble.Visible = false;
                        phHasAnswers.Visible = true;
                        repeaterQuestionAnswers.DataSource = questions;
                        repeaterQuestionAnswers.DataBind();
                    }
                }
                else
                {
                    phHasAnswersDouble.Visible = false;
                    phHasAnswers.Visible = false;

                    if (g.Member != null && g.Member.MemberID == this.MemberProfile.MemberID)
                    {
                        phOwnProfileNoAnswers.Visible = true;
                    }
                    else
                    {
                        phOtherProfileNoAnswers.Visible = true;
                    }
                }
            }

        }

        public string GetResourceForJS(string resConst)
        {
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, this));
        }

        public string GetRedirectUrl(int prtId)
        {
            string redirectUrl = string.Empty;
            if (g.Member != null && g.Member.MemberID > 0)
            {
                if (!g.Member.IsPayingMember(g.Brand.Site.SiteID))
                {
                    Matchnet.Content.ServiceAdapters.Links.ParameterCollection parameterCollection = new Matchnet.Content.ServiceAdapters.Links.ParameterCollection();
                    if (_g.ImpersonateContext)
                    {
                        parameterCollection.Add(WebConstants.IMPERSONATE_MEMBERID, _g.TargetMemberID.ToString());
                        parameterCollection.Add(WebConstants.IMPERSONATE_BRANDID, _g.TargetBrandID.ToString());
                    }
                    redirectUrl = Framework.Util.Redirect.GetSubscriptionUrl(_g.Brand, prtId, g.Member.MemberID, false, HttpContext.Current.Server.UrlEncode(HttpContext.Current.Items["FullURL"].ToString()), parameterCollection);
                }
            }
            return redirectUrl;
        }

        public bool isQandADoubleEnabled()
        {
            return QuestionAnswer.QuestionAnswerHelper.IsQuestionAnswerDoubleEnabled(g.Brand) && IsLoggedIn;
        }

        protected void RemoveAnswer()
        {
            try
            {
                int answerID = 0;
                int.TryParse(Request.Form["hidAnswerID"], out answerID);

                int questionID = 0;
                int.TryParse(Request.Form["hidQuestionID"], out questionID);

                if (questionID > 0 && answerID > 0)
                {
                    QuestionAnswerSA.Instance.RemoveAnswer(answerID, questionID, g.Brand.Site.SiteID, g.Member.MemberID);

                    //display confirmation message
                    g.Notification.AddMessageString(g.GetResource("TXT_REMOVE_SUCCESS", this));
                    //clear logged in member's qandas from cache
                    QuestionAnswerHelper.ResetLoggedInMemberQuestions(g);
                }
                else
                {
                    throw new Exception(String.Format("Invalid or missing Question and AnswerID to delete.  AnswerID: {0}, QuestionID: {1}", answerID.ToString(), questionID.ToString()));
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
        #endregion

        #region Private Methods

        #endregion
    }
}