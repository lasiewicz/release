﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColorCodeTab.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab.ColorCodeTab" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register src="/Applications/MemberProfile/ProfileTabs30/Controls/ColorCode/Chart.ascx" tagname="Chart" tagprefix="uc1" %>
<%@ Register src="/Applications/MemberProfile/ProfileTabs30/Controls/ColorCode/Profile.ascx" tagname="Profile" tagprefix="uc1" %>
<%@ Register src="/Applications/MemberProfile/ProfileTabs30/Controls/ColorCode/ColorColorCopy.ascx" tagname="ColorColorCopy" tagprefix="uc1" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div class="info color-code">

    <%--******All these are potential sections are for members viewing somebody else's profile*******--%>
    <asp:PlaceHolder ID="phViewerOnlyHasTaken" runat="server" Visible="false">
        <div id="cc-fp-invitation" class="editorial cc-fp-marketing">
	        <h2 class="serif"><strong><%=_UserName %></strong> has yet to take the Color Code&nbsp;Test.</h2>
	        <asp:PlaceHolder ID="phSendInvitationButtons" runat="server" Visible="false">
	            <p><asp:LinkButton ID="btnSendColorCodeInvitation" runat="server" OnClick="btnSendColorCodeInvitation_Click">It's FREE, so invite them to take it and share their results with you. You two may be a great&nbsp;match!</asp:LinkButton></p>
	            <p><asp:LinkButton ID="btnSendColorCodeInvitation2" runat="server" OnClick="btnSendColorCodeInvitation2_Click" class="link-primary">Email a Color Code Invitation</asp:LinkButton></p>
	        </asp:PlaceHolder>
	        <asp:PlaceHolder ID="phInvitationAlreadySent" runat="server" Visible="false">
	            <p><mn2:FrameworkLiteral id="literalInvitationSent" ResourceConstant="TXT_INVITESENTCONFIRMATION" runat="server"></mn2:FrameworkLiteral></p>
	        </asp:PlaceHolder>
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phNeitherHasTaken" runat="server" Visible="false">
        <div id="cc-fp-take" class="editorial cc-fp-marketing">
	        <p><big><span class="serif">Discover the Power of the Color Code</span></big><br />Take this insightful personality test to learn more about your motivations and gain a deeper understanding of your potential&nbsp;matches.</p>
	        <p><a href="/Applications/ColorCode/Landing.aspx?colortracking=ProfileViewerNotTaken" class="link-primary">Take the Color Code Test Now</a></p>
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phMemberOnlyHasTaken" runat="server" Visible="false">
        <div class="editorial">
	        <h3>The Color Code</h3>
            <p>Compare Color Codes to see how you two will click. Remember, differences are not only okay, they can provide the spark in a relationship.</p>
        </div>

        <div id="cc-fp-charts" class="clearfix">

            <div id="cc-fp-theirs" class="two-column">

                <!--chart-->
                <uc1:Chart ID="ChartMemberOnlyHasTaken" runat="server" />

                <div class="cc-fp-breakdown-theirs">

                <!--color profile-->
                <uc1:Profile ID="ProfileMemberOnlyHasTaken" runat="server" />
                </div>

            </div><!-- end cc-fp-theirs column -->


            <div id="cc-fp-yours" class="two-column">

                <h3 class="cc-color-heading-none"><a href="/Applications/ColorCode/Landing.aspx?colortracking=ProfileViewerNotTaken">What's Your Color?</a></h3>
                <div id="cc-fp-yourcolor" class="editorial cc-fp-marketing text-center">
	                <h2 class="serif">How Do Your Personalities Match?</h2>
	                <p><a href="/Applications/ColorCode/Landing.aspx?colortracking=ProfileViewerNotTaken">Take the Color Code Test and find out.</a></p>

	                <p>Then, check out <%=_UserName %> and see if you're compatible.</p>
                </div>

            </div><!-- end cc-fp-yours column -->
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phBothHasTaken" runat="server" Visible="false">
        <div class="editorial">
	        <h3>The Color Code</h3>
            <p>Compare Color Codes to see how you two will click. Remember, differences are not only okay, they can provide the spark in a relationship.</p>
        
            <!--color/color specific content-->
            <h3>Relationships with a <%=_MemberColorText.ToUpper() %></h3>
            <uc1:ColorColorCopy ID="ColorColorCopyBothHasTaken" runat="server" />
            
            <h3>Your Core Personality Traits</h3>
        </div>

        <div id="cc-fp-charts" class="clearfix">
            <div id="cc-fp-theirs" class="two-column">
                <!--chart here-->
                <uc1:Chart ID="ChartBothHasTakenMember" runat="server" />
                <!--color profile-->
                <div class="cc-fp-breakdown-theirs">
                    <uc1:Profile ID="ProfileBothHasTakenMember" runat="server" />
                </div>

            </div><!-- end cc-fp-theirs column -->

            <div id="cc-fp-yours" class="two-column<%=_BlockElementCSS %>">
                <!--chart here-->
                <uc1:Chart ID="ChartBothHasTakenViewer" runat="server" />

                <!--color profile-->
                <div class="cc-fp-breakdown-yours">
                    <uc1:Profile ID="profileBothHasTakenViewer" runat="server" />
                </div>

            </div><!-- end cc-fp-yours column -->

        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phColorCodeFooter" runat="server" Visible="false">
        <div id="cc-fp-footer" class="editorial">
	        <h3>Want to get to know <%=_UserName %>?</h3>
	        <!--email link-->
            <p><a id="lnkEmailMe" Runat="server"><mn:image id="ImageEmailMe" runat="server" resourceconstant="ALT_EMAIL" filename="btn-email-fullprofile.png"></mn:image></a></p>
        </div>
    </asp:PlaceHolder>
    
    <%--******All these are potential sections are for members viewing their own profile*******--%>
    <asp:PlaceHolder ID="phOwnProfileMemberNotTaken" runat="server" Visible="false">
        <div id="cc-fp-take" class="editorial cc-fp-marketing">
	        <p><big><span class="serif">Discover the Power of the Color Code</span></big><br />Take this insightful personality test to learn more about your motivations and gain a deeper understanding of your potential matches.</p>
	        <p><a href="/Applications/ColorCode/Landing.aspx?colortracking=ProfileMemberNotTaken" class="link-primary">Take the Color Code Test Now &raquo;</a></p>
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phOwnProfileMemberTaken" runat="server" Visible="false">
        <a href="/Applications/MemberServices/ChemistrySettings.aspx" class="link-secondary">
            <asp:Literal ID="literalShowSetting1" runat="server" Text="Show your Color Code results"></asp:Literal>
            <asp:Literal ID="literalHideSetting1" runat="server" Text="Hide your Color Code results"></asp:Literal>
        </a>
        <div class="editorial">
	        <h3>The Color Code</h3>
            <p>Compare Color Codes to see how you two will click. Remember, differences are not only okay, they can provide the spark in a relationship.</p>
        </div>

        <div id="cc-fp-charts" class="clearfix<%=_BlockElementCSS %>">

            <div id="cc-fp-theirs" class="two-column">

                <!--chart-->
                <uc1:Chart ID="ChartOwnProfile" runat="server" />

                <div class="cc-fp-breakdown-theirs">

                <!--color profile-->
                <uc1:Profile ID="ProfileOwnProfile" runat="server" />
                </div>

            </div>
            <div id="cc-fp-yours" class="two-column">
                <asp:PlaceHolder ID="phComprehensiveAnalysis" runat="server">
                    <h3 class="cc-color-heading-none"><mn:Txt runat="server" ID="txtCCCHeader1" ResourceConstant="TXT_COMPREHENSIVE_ANALYSIS_HEAD" /></h3>
                    <div id="cc-fp-comprehensive" class="editorial cc-fp-marketing text-center">
                        <mn:Txt runat="server" ID="txt1" ResourceConstant="TXT_COMPREHENSIVE_ANALYSIS1" />
                        <asp:HyperLink runat="server" ID="lnkComprehensiveAnalysis" NavigateUrl="/Applications/ColorCode/ComprehensiveAnalysis.aspx" CssClass="link-primary"><mn:Txt runat="server" ID="txtComprehensiveAnalysis" ResourceConstant="TXT_COMPREHENSIVE_ANALYSIS2" /></asp:HyperLink>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phComprehensiveAnalysisPurchase" runat="server">
                    <h3 class="cc-color-heading-none"><mn:Txt runat="server" ID="txtCCCHeader2" ResourceConstant="TXT_COMPREHENSIVE_ANALYSIS_HEAD" /></h3>
                    <div id="cc-fp-comprehensive-your-own" class="editorial cc-fp-marketing text-center">
                        <mn:Txt runat="server" ID="txt2" ResourceConstant="TXT_BUY_COMPREHENSIVE_ANALYSIS1" />
                        <asp:HyperLink runat="server" ID="HyperLink1" NavigateUrl="/applications/colorcode/comppurchase.aspx" CssClass="link-primary"><mn:Txt runat="server" ID="txt3" ResourceConstant="TXT_BUY_COMPREHENSIVE_ANALYSIS2" /></asp:HyperLink> 
                    </div>
                </asp:PlaceHolder>
            </div><%-- end #cc-fp-yours --%>
        </div><%-- end #cc-fp-charts --%>
    </asp:PlaceHolder>
    
</div><!-- end .info.color-code -->	

<script type="text/javascript">
$j(document).ready(function(){

    if ($j('#cc-fp-hidden-message').length){
	    var $ccFPBlock = $j('#cc-fp-hidden-message');
	    $j('.blockelement').block({
		    message: $ccFPBlock,
		    overlayCSS: { 
				    backgroundColor:'#000',
				    opacity:'0.8',
				    cursor:'auto'
			    },
			    css: {
				    top: '60px',
				    left: '20px',
				    width:'90%',
				    textAlign:'left',
				    color:'#fff',
				    border:'0px solid #aaa',
				    backgroundColor:'transparent',
				    cursor:'auto'
			    },
			    centerX: false,
			    centerY: false
		    }
	    );
	}
	$j('#cc-fp-picture-tag.blockelement').block({
		message: null,
		overlayCSS: { 
				backgroundColor:'#000',
				opacity:'0.8',
				cursor:'auto'
			},
			css: {
				cursor:'auto'
			}
		}
	);
});
</script>

<div id="cc-fp-hidden-message">
	<h2 class="serif">Your Color Code results are currently hidden from other members.</h2><p><a href="/Applications/MemberServices/ChemistrySettings.aspx">Click here to change your display settings</a></p>	
</div>