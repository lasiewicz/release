﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.ColorCode;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ColorCode
{
    public partial class Profile : FrameworkControl
    {
        protected Color _Color = Color.none;
        protected string _ColorText = "";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadProfile(MemberQuiz memberQuiz)
        {
            _Color = memberQuiz.PrimaryColor;
            _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);

            switch (_Color)
            {
                case Color.blue:
                    phBlueProfile.Visible = true;
                    break;
                case Color.red:
                    phRedProfile.Visible = true;
                    break;
                case Color.white:
                    phWhiteProfile.Visible = true;
                    break;
                case Color.yellow:
                    phYellowProfile.Visible = true;
                    break;
            }
        }
    }
}