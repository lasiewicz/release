﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ColorCode
{
    public partial class ColorColorCopy : FrameworkControl
    {
        protected Color _MemberColor = Color.none;
        protected Color _ViewerColor = Color.none;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadColorCodeCopy(Color memberColor, Color viewerColor)
        {
            if (memberColor == Color.blue)
            {
                if (viewerColor == Color.blue)
                    phBlueBlue.Visible = true;
                else if (viewerColor == Color.red)
                    phBlueRed.Visible = true;
                else if (viewerColor == Color.white)
                    phBlueWhite.Visible = true;
                else if (viewerColor == Color.yellow)
                    phBlueYellow.Visible = true;
            }
            else if (memberColor == Color.red)
            {
                if (viewerColor == Color.blue)
                    phRedBlue.Visible = true;
                else if (viewerColor == Color.red)
                    phRedRed.Visible = true;
                else if (viewerColor == Color.white)
                    phRedWhite.Visible = true;
                else if (viewerColor == Color.yellow)
                    phRedYellow.Visible = true;
            }
            else if (memberColor == Color.white)
            {
                if (viewerColor == Color.blue)
                    phWhiteBlue.Visible = true;
                else if (viewerColor == Color.red)
                    phWhiteRed.Visible = true;
                else if (viewerColor == Color.white)
                    phWhiteWhite.Visible = true;
                else if (viewerColor == Color.yellow)
                    phWhiteYellow.Visible = true;
            }
            else if (memberColor == Color.yellow)
            {
                if (viewerColor == Color.blue)
                    phYellowBlue.Visible = true;
                else if (viewerColor == Color.red)
                    phYellowRed.Visible = true;
                else if (viewerColor == Color.white)
                    phYellowWhite.Visible = true;
                else if (viewerColor == Color.yellow)
                    phYellowYellow.Visible = true;
            }
        }
    }

}