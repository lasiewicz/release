<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhotoSlider.ascx.cs"
    Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.PhotoSlider" %>
<%@ Register TagPrefix="mn" TagName="NoPhoto" Src="/Framework/UI/PageElements/NoPhoto.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="OmnidateAvatarButton" Src="~/Applications/Omnidate/Controls/OmnidateAvatarButton.ascx" %>
<script type="text/javascript">
    //define objects
    //1. Photo
    //2. PhotoManager
    function spark_profile30_PhotoSlider_Photo() {
        this.photoIndex = null;
        this.photoCaption = '';
        this.thumbnailID = null;
        this.largeImageURL = '';
        this.thumbnailImageURL = '';
        this.isPhotoApproved = true;
        this.commentAndLikeDivID = '';
    }
    
    function spark_profile30_PhotoSlider_PhotoManager() {
        this.photoList = [];
        this.OmniturePage = 'Profile';
        this.MaxCaptionLength = <%=_MaxCaptionLength%>;
        this.isViewingOwnProfile = null;
        this.captionID = '<%=txtCaption.ClientID %>';
        this.pendingID = '<%=pPendingPhoto.ClientID %>';
        this.largeImageID = '<%=imgPhotoLarge.ClientID %>';
        this.largeNoPhotoID = '<%=pnlUploadAPhotoToday.ClientID %>';
        this.previousSelectedPhotoIndex = 0;
        this.isGAM = true;
        this.GAMAdslotRight = 'profile_photos_right_300x250';
        this.GAMAdslotTop = 'profile_photos_top_728x90';
        this.isLoggedIn = false;
        this.RedirectUrl='';
        this.targetMemberID=<%=MemberProfile.MemberID%>;
        this.sendingMessageInProgress = false;
        this.noPhotoLink = '<%=lnkUploadAPhotoToday.ClientID%>';
        this.noPhotoFlirtSent = "<%=_TXT_NOPHOTO_FLIRT_SENT%>";
        this.noPhotoFlirtError = "<%=_TXT_NOPHOTO_FLIRT_ERROR%>";
        this.noPhotoFlirtQuota = "<%=_TXT_NOPHOTO_FLIRT_QUOTA%>";
        this.noPhotoFlirtAlreadySent = "<%=_TXT_NOPHOTO_FLIRT_ALREADYSENT%>";
        this.noPhotoFlirtSending = "<%=_TXT_NOPHOTO_FLIRT_SENDING%>";
        this.refreshAds = function() {
            //refresh ad
            if (this.GAMAdslotTop.length > 0) {
                try {
                    updateTop728by90SR(topAd728x90IFrameID, this.GAMAdslotTop, this.isGAM);
                } catch (e) { }
            }
            if (this.GAMAdslotRight.length > 0) {
                try {
                    updateRight300by250SR(rightAd300x250IFrameID, this.GAMAdslotRight, this.isGAM);
                } catch (e) { }
            }
        }
        this.onThumbnailClick = function(selectedIndex) {
            if (selectedIndex != this.previousSelectedPhotoIndex){
                var photo = this.photoList[selectedIndex];
                var previousPhoto = this.photoList[this.previousSelectedPhotoIndex];
                
                //highlight thumbnail
                $j('#' + previousPhoto.thumbnailID).removeClass("active");
                $j('#' + photo.thumbnailID).addClass("active");
                
                if (photo.largeImageURL != '')
                {
                    //show large photo
                    $j('#memberPhotoLarge').css("display", "block");
                    var swapped = false;
                    var largeImageObj = document.getElementById(this.largeImageID);
                    $j(largeImageObj).attr({ src: photo.largeImageURL }).load(function() {
                            profile_resizePhoto30(largeImageObj);
                            swapped = true;
                        });
                    if (!swapped)
                    {
                        profile_resizePhoto30(largeImageObj);
                    }
                    $j('#' + this.largeNoPhotoID).css("display", "none");
                    
                }
                else
                {
                    //show no photo
                    $j('#memberPhotoLarge').css("display", "none");
                    $j('#' + this.largeNoPhotoID).css("display", "block");
                }
                
                //show caption
                if (photo.photoCaption.length <= this.MaxCaptionLength){
                    $j('#' + this.captionID).text(photo.photoCaption);
                }
                else {
                    $j('#' + this.captionID).html("<span title=\"" + photo.photoCaption + "\">" + photo.photoCaption.substring(0, this.MaxCaptionLength - 1) + "&hellip;</span>");
                }
                //show pending
                if (photo.isPhotoApproved)
                    $j('#' + this.pendingID).hide();
                else
                 $j('#' + this.pendingID).show();

                //update omniture
                if (s != null) {
                    PopulateS(true); //clear existing values in omniture "s" object
                    
                    s.events = "event2,event4";
                    s.prop14 = "profile_miniphotos";
                    s.pageName = spark_profile30_OmnitureCurrentPageName;
                    s.t(); //send omniture updated values as page load
                }
                
                //refresh ads
                this.refreshAds();
                this.previousSelectedPhotoIndex = selectedIndex;
            }
        }
        this.onSlideShowClick = function() {
            //update omniture
            if (s != null) {
                PopulateS(true); //clear existing values in omniture "s" object
                
                s.events = "event63,event4";
                s.t(); //send omniture updated values as page load
            }
            
            //refresh ads
            this.refreshAds();
        }
        this.addPhoto = function(photoObj) {
            spark.util.addItem(this.photoList, photoObj);
        }
        this.sendNoPhotoRequestMessage = function(){
            if (!photoManager.sendingMessageInProgress){
                //display loading
                $j('#' + photoManager.noPhotoLink).text(photoManager.noPhotoFlirtSending);
                photoManager.sendingMessageInProgress = true;
                $j.ajax({
                    type: "POST",
                    url: "/Applications/API/Flirts.asmx/SendNoPhotoRequestMessageAsFlirt",
                    data: "{ targetMemberID: " + photoManager.targetMemberID + "}",
                    contentType: "application/json; charset=utf-8",
                    complete: function() {
                        photoManager.sendingMessageInProgress = false;
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        //ajax error
                        if (typeof(console) !== "undefined") {
                            console.log(textStatus);
                        }
                    },
                    success: function(result) {
                        var sendMessageResultObj = (typeof result.d == 'undefined') ? result : result.d;
                        var resultStatus = sendMessageResultObj.Status;
                        if (typeof(console) !== "undefined") {
                            console.log("Flirt operation result: " + resultStatus);
                        }

                        if (resultStatus == 1) {
                            //flirt sent
                            $j('#' + photoManager.noPhotoLink).text(photoManager.noPhotoFlirtSent);
                        }
                        else if (resultStatus == 4){
                            //already sent
                            $j('#' + photoManager.noPhotoLink).text(photoManager.noPhotoFlirtAlreadySent);
                        }
                        else if (resultStatus == 5){
                            //session expired
                            window.location.href = '/Applications/Logon/Logon.aspx?DestinationURL=' + encodeURIComponent(window.location.href.replace('http://'+window.location.host,''));
                        }
                        else if (resultStatus == 32){
                            //daily quota reached
                            $j('#' + photoManager.noPhotoLink).text(photoManager.noPhotoFlirtQuota);
                        }
                        else{
                            //error or not sent due to other reasons
                            $j('#' + photoManager.noPhotoLink).text(photoManager.noPhotoFlirtError);
                        }
                    }
                });
            }
        }
    }

    //initialize objects
    var photoManager = new spark_profile30_PhotoSlider_PhotoManager();
    photoManager.isViewingOwnProfile = <%=_IsViewingOwnProfile.ToString().ToLower() %>;
    photoManager.isLoggedIn = <%=IsLoggedIn.ToString().ToLower() %>;
    photoManager.RedirectUrl = '<%=GetRedirectUrl()%>';
</script>
<div class="photos-placeholder">
    <div id="sPop" class="spop">
        <div class="spop-outer">
            <div class="spop-stage">
                <div class="spop-nav">
                    <div class="spop-navwrap">
                        <a class="<%=SPopLeftButtonClass %>" title="<%=SPopLeftButtonTitle %>">
                            <mn:Txt runat="server" ID="txtSPopLeftButtonText" ResourceConstant="TXT_SPOP_PREVIOUS_IMAGE" />
                        </a>
                        <div class="spop-counter">
                            <strong class="spop-current"></strong>
                            <mn:Txt runat="server" ResourceConstant="TXT_SPOP_OF" />
                            <strong class="spop-total"></strong>
                            <mn:Txt runat="server" ResourceConstant="TXT_SPOP_NEXT_PHOTOS" />
                        </div>
                        <%--<a class="spop-switch-enlarge" title="Enlarge">Enlarge</a>--%>
                        <a class="<%=SPopRightButtonClass %>" title="<%=SPopRightButtonTitle %>">
                            <mn:Txt runat="server" ID="txtSPopRightButtonText" ResourceConstant="TXT_SPOP_NEXT_IMAGE" />
                        </a>
                    </div>
                    <div class="spop-closewrap">
                        <a class="spop-switch-compact" title='<mn:txt runat="server"  resourceconstant="TXT_SPOP_CLOSE" />'>
                            <mn:Txt runat="server" ResourceConstant="TXT_SPOP_CLOSE" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="spop-caption">
                <div class="spop-text">
                </div>
            </div>
        </div>
    </div>
    <%-- /#spop --%>
    <div id="profile30PhotoSlider" class="profile30-photos clearfix">
        <!--edit photo link-->
        <div id="memberPhotoEdit" class="edit">
            <asp:HyperLink ID="lnkEditPhotos" Visible="false" runat="server" class="link-primary"
                NavigateUrl="/Applications/MemberProfile/MemberPhotoEdit.aspx"></asp:HyperLink>
            <uc2:OmnidateAvatarButton runat="server" ID="btnOmnidateAvatar" />
        </div>
        <%--large photo--%>
        <div class="main-photo">
            <div id="memberPhotoSwap" style="display: none;">
                <mn:Image ID="imgPhotoLarge" runat="server" alt=""></mn:Image>
            </div>
            <div id="memberPhotoLarge">
            </div>
            <!--no photo with link to upload-->
            <asp:Panel ID="pnlUploadAPhotoToday" runat="server" class="no-photo">
                <asp:HyperLink ID="lnkUploadAPhotoToday" runat="server" NavigateUrl="/Applications/MemberProfile/MemberPhotoUpload.aspx"></asp:HyperLink>
            </asp:Panel>
            <a href="#" class="enlarge" title="<%=g.GetResource("TXT_CLICKTOENLARGE", this) %>">
                <%=g.GetResource("TXT_CLICKTOENLARGE", this) %></a>
        </div>
        <%--color code--%>
        <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
            <div id="cc-fp-picture-tag" class="<%=_BlockElementCSS %>">
                <div class="<%=_CCPicTagCSS %>cc-<%=_ColorText.ToLower() %>-bg cc-pic-tag-lrg">
                    <span title="<%=_CCTip%>">Color: <strong>
                        <%=_ColorText %></strong></span>
                    <div class="cc-pic-tag-help">
                        <b>What is this?</b> Click here to find out &raquo;</div>
                </div>
            </div>
        </asp:PlaceHolder>
        <%--caption--%>
        <p id="txtCaption" runat="server" class="photo-caption">
        </p>
        <%--pending photo message--%>
        <p class="pending-photo-message" id="pPendingPhoto" runat="server">
            <mn:Txt ID="txtPendingPhoto" runat="server" ResourceConstant="TXT_PENDING_PHOTO" />
        </p>
        <%--thumbnail sliders--%>
        <div class="thumbs-area clearfix">
            <asp:PlaceHolder ID="phThumbnails" runat="server">
                <%--<button class="prev" id="btnPrev" runat="server">&#9668;</button>
                <button class="next" id="btnNext" runat="server">&#9658;</button>--%>
                
                <div class="thumbs">
                    <ul>
                        <asp:Repeater ID="repeaterThumbnails" runat="server" OnItemDataBound="repeaterThumbnails_ItemDataBound">
                            <ItemTemplate>
                                <li>
                                    <asp:HyperLink ID="lnkThumbnail" runat="server">
                                        <asp:Image ID="imgThumbnail" Width="58" Height="75" runat="server" alt="" />
                                    </asp:HyperLink>
                                    <asp:Panel ID="photolikeAndComment" runat="server" CssClass="hide">
                                        <div class="like-container clearfix">
                                            <div class="like-module float-outside">
                                                <% if (IsLoggedIn && IsPhotoCommentEnabled && IsPayingMember && !IsViewingOwnProfile)
                                                   { %><span class="comment-label"><label onclick="spark_commentBoxBinding(this);" class="link-label"><mn:txt runat="server" id="txtComment" resourceconstant="TXT_COMMENT" expandimagetokens="true" /></label> &#x25AA; </span>
                                                <% }
                                                   else if (IsLoggedIn && IsPhotoCommentEnabled && !IsViewingOwnProfile)
                                                   { %><asp:HyperLink id="commentLink" runat="server"><mn:txt runat="server" id="txtCommentSubLink" resourceconstant="TXT_COMMENT" expandimagetokens="true" /></asp:HyperLink> &#x25AA; 


                                                <% } %>
                                                <% if (IsLoggedIn && IsPhotoLikeEnabled && !IsViewingOwnProfile)
                                                   { %><span class="like" title="<%=g.GetResource("TXT_LIKE", this) %>" onclick="spark.qanda.LikePhoto(this, <asp:Literal ID="photoObjRef" runat="server"></asp:Literal>, <%=MemberId%>,<%=ProfileMemberID %>, <asp:Literal ID="photoID" runat="server"></asp:Literal>, {QuestionId:<%=ProfileMemberID%>,Event:'event51',Overwrite:true,Props:[{Num:18,Value:'Photo'}]})"><span class="spr s-icon-like"><span><mn:txt runat="server" id="txtLike" resourceconstant="TXT_LIKE" expandimagetokens="true" /></span></span>
                                                        <span class="clickable"><mn:txt runat="server" id="txtLike2" resourceconstant="TXT_LIKE" expandimagetokens="true" /></span>
                                                        <asp:Literal ID="likeTxt" visible="false" runat="server"/>
                                                   </span>
                                                <% } %>
                            

                                            </div>
                                            <% if (IsLoggedIn && !IsViewingOwnProfile)
                                                   { %>
                                            <div class="comment-block hide">
                                                <h2><mn:txt runat="server" id="txtCommentHeader" resourceconstant="TXT_COMMENT_HEADER" expandimagetokens="true" /></h2>
                                                <span class="close"><mn:txt runat="server" id="txtCommentCloseX" resourceconstant="TXT_COMMENT_CLOSE" epandimagetokens="true" /></span>
	                                            <textarea id="comment-<%=MemberId%>-<%=ProfileMemberID%>-<asp:Literal ID="photoID3" runat="server"></asp:Literal>" rows="2" class="watermark"></textarea>
	                                            <div class="text-outside">
                                                    <input onclick="return send_photo_comment(this, event);" type="button" id="id-<%=MemberId%>-<%=ProfileMemberID%>-<asp:Literal ID="photoID2" runat="server"></asp:Literal>" name="sendComment" class="btn secondary photoCommentSubmit" value="<%=g.GetResource("TXT_COMMENT_SUBMIT",this) %>" />
	                                            </div>
	                                        </div>
                                            <% } %>
                                        </div>
                                    </asp:Panel>
                                </li>
                                <script type="text/javascript">
                                    <asp:Literal ID="literalPhotoJS" runat="server"></asp:Literal>
                                </script>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                
                <div id="divLoading" class="loading">
                    <p>
                        <mn:Txt ID="txtlLoading" runat="server" ResourceConstant="TXT_LOADING" />
                    </p>
                    <mn:Image ID="Image1" runat="server" FileName="ajax-loader.gif" alt="" />
                    <div class="visual-blocker">
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
        <script type="text/javascript">
            $j(function () {
                $j('#divLoading').hide(); //hide thumbnail preloader
                var totalPhotos = $j('.thumbs li', '#profile30PhotoSlider').length,
                    splitNum = 5,
                    photoIndex = 0;

                // Set container min width & height 
                $j('.photos-placeholder').css({
                    'min-height': $j('#profile30PhotoSlider').outerHeight(),
                    'min-width': $j('#profile30PhotoSlider').outerWidth()
                });

                if (totalPhotos >= splitNum) {
                    var photoHeight = $j('.thumbs', '#profile30PhotoSlider').height(),
                        colNum = 4,
                        morePhotosHtml = "<span class='more-photo'><em>(" + totalPhotos + ")</em></span>",
                        setHeight = (Math.ceil(totalPhotos / colNum)) * photoHeight;

                    $j('.thumbs ul li:eq(2)').append(morePhotosHtml).addClass('more-photo-list');

                    $j('.thumbs-area', '#profile30PhotoSlider').bind('mouseenter mouseleave', thumbCont);
                }

                function thumbCont(e) {
                    if ($j('#sPop').is('.spop-expanded')) {
                        //disable thumbcontrol
                    } else {
                        if (e.type == 'mouseenter') {
                            $j('#profile30PhotoSlider').css('position', 'absolute').children('.thumbs-area').addClass('hover');
                            $j('.thumbs').stop().animate({
                                height: setHeight
                            }, 500, 'easeOutExpo');
                        } else {
                            $j('.thumbs-area').removeClass('hover');
                            $j('.thumbs').stop().animate({
                                height: photoHeight
                            }, 250, 'easeOutExpo');
                        }
                    }
                }

                $j("#memberPhotoLarge").hover(function () {
                    var $self = $j(this);
                    $self.closest('.main-photo').find('.enlarge').appendTo($self).addClass('active');
                }, function () {
                    var $self = $j(this);
                    $self.find('.enlarge').appendTo($self.parent()).removeClass('active');
                });

                //Popeye effect
                $j("#memberPhotoLarge").live('click', function () {
                    if (!photoManager.isLoggedIn) {
                        window.location = photoManager.RedirectUrl;
                    } else {
                        // get the image objects in Spark namespace
                        var imgNs = photoManager.photoList,
                        totalImgNum = imgNs.length,
                        imgPath = [],
                        captionList = [];

                        for (i = 0; i < totalImgNum; i++) {
                            if (imgNs[i].largeImageURL == '') {
                                break;
                            }
                            imgPath[i] = imgNs[i].largeImageURL;
                            captionList[i] = imgNs[i].photoCaption;// +$j("#" + imgNs[i].commentAndLikeDivID).html();
                        }

                        // define sPop options
                        var options = {
                            //debug: true,
                            caption: 'permanent',
                            //opacity: 1,
                            imgObjList: imgPath,
                            capList: captionList,
                            photoList: imgNs,
                            enlarge: true,
                            curIndex: photoManager.previousSelectedPhotoIndex,
                            minW: 400,
                            maxW: 700,
                            minH: 400,
                            maxH: 525,
                            direction: '<%=SPopOptionsDirection %>'
                        }
                        $j('#sPop').spop(options);
                        //Update omniture and Refresh Ad inside of sPop
                        $j('#sPop .spop-nav a , #sPop .spop-stage').click(function () {
                            photoManager.onSlideShowClick();
                        });

                        //Update omniture and Refresh Ad at sPop initiation
                        photoManager.onSlideShowClick();
                    }
                });
            });

            function spark_commentBoxBinding(el){
                //cache this and the parent object in variables
                var $this = $j(el),
                    $spopText = $this.parents('.spop-text');
                
                //add placeholder text
                $spopText.find(".comment-block textarea").watermark('<%=g.GetResource("TXT_COMMENT_PLACEHOLDER",this) %>');

                //show/hide the comemnt block
                $spopText.find('.comment-block').toggleClass('hide');

                //bind 'click' to close the comment block from within the comment block
                //using .one() to only bind once since we are binding on click and the user could toggle .hide multiple times
                $spopText.find('.close').one('click', function(){
                    $j(this).closest('.comment-block').toggleClass('hide');
                });
            };
        </script>
    </div>
</div>
