﻿using System;
using System.Threading;
using Matchnet.Web.Framework;
using System.Collections.Generic;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls
{
    /// <summary>
    /// Throttles the NonSubMessageBlock feature.
    /// </summary>
    public static class ShowSubjectLineFeatureThrottler
    {
        private const String ModSettingName = "ShowSubjectlineFeatureMods"; // example: 0,2,4,6,8

        private const Int32 ModValue = 10;

        private const Int32 SiteId = 103;

        private const Int32 ExpirationInSeconds = 15;

        private static readonly TimeSpan thresholdExpiration = new TimeSpan(0, 0, 0, ExpirationInSeconds);

        private static Int64 lastUpdate = DateTime.UtcNow.Ticks;

        private static List<int> thresholds = GetNewThreshold();

        private static readonly Object lockObject = new Object();

        /// <summary>
        /// Gets or sets the threshold. If the threshold is
        /// equal to -1, everything's getting through.
        /// equal to empty, nothing's getting through.
        /// </summary>
        /// <value>The threshold.</value>
        public static List<int> Thresholds
        {
            get
            {
                if (DateTime.UtcNow.Subtract(new DateTime(lastUpdate, DateTimeKind.Utc)) > thresholdExpiration)
                {
                    lock (lockObject)
                    {
                        Interlocked.Exchange(ref thresholds, GetNewThreshold());
                        Interlocked.Exchange(ref lastUpdate, DateTime.UtcNow.Ticks);
                    }
                }

                return thresholds;
            }
        }

        /// <summary>
        /// Determines whether this feature is enabled for a global context (hint: it looks
        /// at the <see cref="ContextGlobal"/>'s Member). Wait... that sounds just wrong.
        /// </summary>
        /// <param name="contextGlobal">The global context.</param>
        /// <returns>
        /// 	<c>true</c> if the feature is enabled; otherwise, <c>false</c>.
        /// </returns>
        public static Boolean IsFeatureEnabled(ContextGlobal contextGlobal)
        {
            List<int> thresholds = GetNewThreshold(contextGlobal);
            return IsEnabled(thresholds, contextGlobal);
        }



        /// <summary>
        /// Determines whether this feature is enabled for a particular member.
        /// </summary>
        /// <param name="member">The member.</param>
        /// <returns>
        /// 	<c>true</c> if the this feature is enabled; otherwise, <c>false</c>.
        /// </returns>
        public static Boolean IsFeatureEnabled(Member.ServiceAdapters.Member member, ContextGlobal g)
        {
            List<int> thresholds = GetNewThreshold(g);
            return IsEnabled(thresholds, member);
        }

        /// <summary>
        /// Determines whether this feature is enabled for a particular member id.
        /// </summary>
        /// <param name="memberId">The member id.</param>
        /// <returns>
        /// 	<c>true</c> if the feature is enabled; otherwise, <c>false</c>.
        /// </returns>
        public static Boolean IsFeatureEnabled(Int32 memberId, ContextGlobal g)
        {
            List<int> thresholds = GetNewThreshold(g);

            return IsEnabled(thresholds, memberId);
        }

        private static Boolean IsEnabled(List<int> featureThresholds, ContextGlobal contextGlobal)
        {
            if (contextGlobal == null || contextGlobal.Brand == null || contextGlobal.Brand.Site == null) return false;

            //if (contextGlobal.Brand.Site.SiteID != SiteId) return false;

            return IsEnabled(featureThresholds, contextGlobal.Member);
        }

        private static Boolean IsEnabled(List<int> featureThresholds, Member.ServiceAdapters.Member member)
        {
            //if (featureThresholds == ModValue) return true;

            return member != null && IsEnabled(featureThresholds, member.MemberID);
        }

        private static Boolean IsEnabled(List<int> featureThresholds, Int32 memberId)
        {
            if (featureThresholds.Contains(-1)) return true;
            if (featureThresholds.Count == 0) return false;

            return memberId > 0 && featureThresholds.Contains(memberId % ModValue);
        }

        private static List<int> GetNewThreshold()
        {
            string setting = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                    ModSettingName,
                    SiteId
            );

            List<int> list = new List<int>();
            string[] mods = setting.Split(',');
            foreach (string part in mods)
            {
                if (!part.Equals(""))
                {
                    list.Add(int.Parse(part));
                }
            }

            return list;
        }

        private static List<int> GetNewThreshold(ContextGlobal g)
        {
            string setting = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                    ModSettingName,
                    g.Brand.Site.SiteID
            );

            List<int> list = new List<int>();
            string[] mods = setting.Split(',');
            foreach (string part in mods)
            {
                if (!part.Equals(""))
                {
                    list.Add(int.Parse(part));
                }
            }

            return list;
        }

    }
}