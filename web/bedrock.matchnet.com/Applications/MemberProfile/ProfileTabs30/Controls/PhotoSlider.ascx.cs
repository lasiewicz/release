﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Applications.MemberProfile;
using System.Web.UI.HtmlControls;
using System.Text;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.Web.Applications.MemberLike;
using Matchnet.Web.Framework.Managers;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls
{
    public partial class PhotoSlider : BaseProfile
    {
        protected Color _Color = Color.none;
        protected string _ColorText = "";
        protected string _BlockElementCSS = "";
        protected string _CCPicTagCSS = "cc-pic-tag ";
        protected string _CCTip = "";
        protected string _siteIDParam = "";
        protected bool _IsViewingOwnProfile = false;
        protected bool _DisplayAllPhotosAsNoPrivate = false;
        protected int _MaxCaptionLength = 40;
        protected string _TXT_NOPHOTO_FLIRT_SENDING = "";
        protected string _TXT_NOPHOTO_FLIRT_ERROR = "";
        protected string _TXT_NOPHOTO_FLIRT_SENT = "";
        protected string _TXT_NOPHOTO_FLIRT_ALREADYSENT = "";
        protected string _TXT_NOPHOTO_FLIRT_QUOTA = "";

        #region Properties

        public Color Color
        {
            get { return this._Color; }
            set { this._Color = value; }
        }

        protected string SPopOptionsDirection
        {
            get
            {
                return (g.Brand.Site.Direction == Matchnet.Content.ValueObjects.BrandConfig.DirectionType.ltr
                            ? "right"
                            : "left");
            }
        }

        protected string SPopLeftButtonClass
        {
            get
            {
                return (g.Brand.Site.Direction == Matchnet.Content.ValueObjects.BrandConfig.DirectionType.ltr
                            ? "spop-prev"
                            : "spop-next");
            }
        }

        protected string SPopRightButtonClass
        {
            get
            {
                return (g.Brand.Site.Direction == Matchnet.Content.ValueObjects.BrandConfig.DirectionType.ltr
                            ? "spop-next"
                            : "spop-prev");
            }
        }

        protected string SPopLeftButtonTitle
        {
            get
            {
                return (g.Brand.Site.Direction == Matchnet.Content.ValueObjects.BrandConfig.DirectionType.ltr
                            ? g.GetResource("TXT_SPOP_PREVIOUS_IMAGE", this)
                            : g.GetResource("TXT_SPOP_NEXT_IMAGE", this));
            }
        }

        protected string SPopRightButtonTitle
        {
            get
            {
                return (g.Brand.Site.Direction == Matchnet.Content.ValueObjects.BrandConfig.DirectionType.ltr
                            ? g.GetResource("TXT_SPOP_NEXT_IMAGE", this)
                            : g.GetResource("TXT_SPOP_PREVIOUS_IMAGE", this));
            }
        }

        public bool IsViewingOwnProfile
        {
            get
            {
                return _IsViewingOwnProfile;
            }
        }


        public bool IsPhotoCommentEnabled
        {
            get
            {
                bool isEnabled = false;
                try
                {
                    isEnabled = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_PHOTO_COMMENT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                }
                catch (Exception ex)
                {
                    //setting missing
                    isEnabled = false;
                }

                return isEnabled;
            }
        }

        public bool IsPhotoLikeEnabled
        {
            get
            {
                bool isEnabled = false;
                try
                {
                    isEnabled = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_PHOTO_LIKE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                }
                catch (Exception ex)
                {
                    //setting missing
                    isEnabled = false;
                }

                return isEnabled;
            }
        }

        #endregion

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if (g.Brand.Site.Direction == Matchnet.Content.ValueObjects.BrandConfig.DirectionType.rtl)
            {
                txtSPopLeftButtonText.ResourceConstant = "TXT_SPOP_NEXT_IMAGE";
                txtSPopRightButtonText.ResourceConstant = "TXT_SPOP_PREVIOUS_IMAGE";
            }

            _TXT_NOPHOTO_FLIRT_SENDING = g.GetResource("TXT_NOPHOTO_FLIRT_SENDING", this);
            _TXT_NOPHOTO_FLIRT_ERROR = g.GetResource("TXT_NOPHOTO_FLIRT_ERROR", this);
            _TXT_NOPHOTO_FLIRT_SENT = g.GetResource("TXT_NOPHOTO_FLIRT_SENT", this);
            _TXT_NOPHOTO_FLIRT_ALREADYSENT = g.GetResource("TXT_NOPHOTO_FLIRT_ALREADYSENT", this);
            _TXT_NOPHOTO_FLIRT_QUOTA = g.GetResource("TXT_NOPHOTO_FLIRT_QUOTA", this);

        }

        protected void repeaterThumbnails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                Photo photo = e.Item.DataItem as Photo;
                bool isPhotoApproved = photo.IsApproved;

                //set thumbnail photos and links
                HyperLink lnkThumbnail = e.Item.FindControl("lnkThumbnail") as HyperLink;
                System.Web.UI.WebControls.Image imgThumbnail = e.Item.FindControl("imgThumbnail") as System.Web.UI.WebControls.Image;

                if (IsLoggedIn)
                {
                    lnkThumbnail.NavigateUrl = "javascript:photoManager.onThumbnailClick(" + e.Item.ItemIndex + ");";
                }
                else
                {
                    lnkThumbnail.NavigateUrl = GetRedirectUrl();
                }

                if (e.Item.ItemIndex == 0)
                    lnkThumbnail.CssClass = "active";

                bool noPhoto = false;
                string largePhotoURL = string.Empty;

                if (MemberPhotoDisplayManager.Instance.IsPhotosAvailableToGuests(g.Member, MemberProfile, g.Brand))
                {
                    if (MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Full, g.Brand))
                    {
                        isPhotoApproved = true;
                    }
                    else
                    {
                        largePhotoURL = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, MemberProfile, g.Brand, photo, PhotoType.Full,
                            PrivatePhotoImageType.Full, NoPhotoImageType.BackgroundThumb, false);
                    }
                }

                if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Thumbnail, g.Brand))
                {
                    imgThumbnail.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, MemberProfile, g.Brand, photo, PhotoType.Thumbnail, 
                        PrivatePhotoImageType.Thumb, NoPhotoImageType.ThumbV2, false);
                }
                else if (!string.IsNullOrEmpty(largePhotoURL))
                {
                    imgThumbnail.ImageUrl = largePhotoURL;
                }
                else
                {
                    imgThumbnail.ImageUrl = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.ThumbV2, MemberProfile, g.Brand);
                }
               
                HyperLink commentLink = e.Item.FindControl("commentLink") as HyperLink;
                commentLink.NavigateUrl = GetRedirectUrl(1255);


                Literal litPhotoObj = e.Item.FindControl("photoObjRef") as Literal;
                litPhotoObj.Text = "photoObj" + e.Item.ItemIndex;

                //create JS Photo object
                Literal literalPhotoJS = e.Item.FindControl("literalPhotoJS") as Literal;
                StringBuilder s = new StringBuilder("try {");
                s.Append("var photoObj" + e.Item.ItemIndex + "=new spark_profile30_PhotoSlider_Photo();");
                s.Append("photoObj" + e.Item.ItemIndex + ".photoIndex = " + e.Item.ItemIndex + ";");
                if ((!_DisplayAllPhotosAsNoPrivate && photo.IsCaptionApproved) || _IsViewingOwnProfile)
                {
                    s.Append("photoObj" + e.Item.ItemIndex + ".photoCaption = '" + FrameworkGlobals.JavaScriptEncode(photo.Caption) + "';");
                }
                s.Append("photoObj" + e.Item.ItemIndex + ".thumbnailID = '" + lnkThumbnail.ClientID + "';");

                s.Append("photoObj" + e.Item.ItemIndex + ".largeImageURL = '" + largePhotoURL + "';");
                s.Append("photoObj" + e.Item.ItemIndex + ".isPhotoApproved = " + isPhotoApproved.ToString().ToLower() + ";");
                s.Append("photoObj" + e.Item.ItemIndex + ".thumbnailImageURL = '" + imgThumbnail.ImageUrl + "';");
                
                Panel phPhotolikeAndComment = e.Item.FindControl("photolikeAndComment") as Panel;
                s.Append("photoObj" + e.Item.ItemIndex + ".commentAndLikeDivID = '" + phPhotolikeAndComment.ClientID + "';");

                Literal literalPhotoID = e.Item.FindControl("photoID") as Literal;
                literalPhotoID.Text = photo.MemberPhotoID.ToString();

                Literal literalPhotoID2 = e.Item.FindControl("photoID2") as Literal;
                literalPhotoID2.Text = photo.MemberPhotoID.ToString();

                Literal literalPhotoID3 = e.Item.FindControl("photoID3") as Literal;
                literalPhotoID3.Text = photo.MemberPhotoID.ToString();

                //add to photo manager
                s.Append("photoManager.addPhoto(photoObj" + e.Item.ItemIndex + ");\n");
                s.Append("}catch(e){}\n");

                literalPhotoJS.Text = s.ToString();


                MemberLikeParams mlp = new MemberLikeParams();
                mlp.MemberId = MemberId;
                mlp.SiteId = g.Brand.Site.SiteID;
                int likeCounts = MemberLikeHelper.Instance.GetCountOfLikesByObjectIdandType(mlp, photo.MemberPhotoID, (int)LikeObjectTypes.Photo);
                if (likeCounts > 0)
                {
                    Literal LiteralLikeTxtCount = e.Item.FindControl("likeTxt") as Literal;
                    LiteralLikeTxtCount.Text = "(" + likeCounts + ")";
                    LiteralLikeTxtCount.Visible = true;
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry("WWW", ex.ToString());
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Loads the photos for the specified member
        /// </summary>
        /// <param name="member"></param>
        /// <param name="activeTab"></param>
        public void LoadPhotoSlider(IMemberDTO member)
        {
            this.MemberProfile = member;

            if (g.Member != null && g.Member.MemberID == member.MemberID)
            {
                _IsViewingOwnProfile = true;
                WhoseProfile = WhoseProfileEnum.Self;
            }

            // This must be appended to all photos that hit the file servers in order for the site-specific
            // "No Photo" images to appear.  Appending this to any other photos should not cause errors.
            _siteIDParam = "?" + WebConstants.URL_PARAMETER_NAME_SITEID + "=" + g.Brand.Site.SiteID.ToString();

            //Load photos
            this.LoadMemberPhoto();

            //Color code
            if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(MemberProfile, g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(MemberProfile, g.Brand))
            {
                _Color = ColorCodeHelper.GetPrimaryColor(MemberProfile, g.Brand);
                _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);

                phColorCode.Visible = true;
                switch (_Color)
                {
                    case Color.blue:
                        _CCTip = g.GetResource("HTML_TIP_BLUE", this);
                        break;
                    case Color.red:
                        _CCTip = g.GetResource("HTML_TIP_RED", this);
                        break;
                    case Color.white:
                        _CCTip = g.GetResource("HTML_TIP_WHITE", this);
                        break;
                    case Color.yellow:
                        _CCTip = g.GetResource("HTML_TIP_YELLOW", this);
                        break;
                }

                if (ColorCodeHelper.IsMemberColorCodeHidden(MemberProfile, g.Brand))
                {
                    _BlockElementCSS = "blockelement";
                }

                if (g.Member != null && g.Member.MemberID == MemberProfile.MemberID)
                {
                    _CCPicTagCSS = "";
                }
            }

            btnOmnidateAvatar.TargetMember = MemberProfile;
        }
        #endregion

        #region Private methods

        public string GetRedirectUrl(int prtId)
        {
            string redirectUrl = string.Empty;
            if (g.Member != null && g.Member.MemberID > 0)
            {
                if (!g.Member.IsPayingMember(g.Brand.Site.SiteID))
                {
                    Matchnet.Content.ServiceAdapters.Links.ParameterCollection parameterCollection = new Matchnet.Content.ServiceAdapters.Links.ParameterCollection();
                    if (_g.ImpersonateContext)
                    {
                        parameterCollection.Add(WebConstants.IMPERSONATE_MEMBERID, _g.TargetMemberID.ToString());
                        parameterCollection.Add(WebConstants.IMPERSONATE_BRANDID, _g.TargetBrandID.ToString());
                    }
                    redirectUrl = Framework.Util.Redirect.GetSubscriptionUrl(_g.Brand, prtId, ProfileMemberID, false, HttpContext.Current.Server.UrlEncode(HttpContext.Current.Items["FullURL"].ToString()), parameterCollection);
                }
            }
            return redirectUrl;
        }


        private void LoadMemberPhoto()
        {
            List<Photo> photoList = null;

            /* Only approved photos should be shown to both viewing member,
             * but the member who owns the photos will see all*/
            if(_IsViewingOwnProfile)
            {
                photoList = MemberPhotoDisplayManager.Instance.GetAllPhotos(MemberProfile, g.Brand, PhotoType.Full);
            }
            else
            {
                photoList = MemberPhotoDisplayManager.Instance.GetApprovedPhotos(g.Member, MemberProfile, g.Brand, PhotoType.Full);
            }

            if(photoList == null)
            {
                photoList = new List<Photo>();
            }

            photoList.Sort(delegate(Photo p1, Photo p2)
            { return p1.ListOrder.CompareTo(p2.ListOrder); });

            //Set default "no photo message"
            lnkUploadAPhotoToday.Text = g.GetResource("TXT_ASK_ME_FOR_MY_PHOTO", this);
            lnkUploadAPhotoToday.NavigateUrl = ProfileDisplayHelper.GetEmailLink(this.MemberProfile.MemberID, false);
            if (g.Member != null)
            {
                if (MessageManager.Instance.IsNoPhotoRequestMessageEnabled(g.Brand))
                {
                    if (g.Member.IsPayingMember(g.Brand.Site.SiteID))
                    {
                        lnkUploadAPhotoToday.NavigateUrl += "&" + WebConstants.URL_PARAMETER_MAILTYPE + "=" + ((int)MailType.NoPhotoRequest).ToString();
                    }
                    else
                    {
                        lnkUploadAPhotoToday.NavigateUrl = "#";
                        lnkUploadAPhotoToday.Attributes.Add("onclick", "photoManager.sendNoPhotoRequestMessage();return false;");
                    }
                }
            }
            else
            {
                lnkUploadAPhotoToday.NavigateUrl = "/Applications/Logon/Logon.aspx?DestinationURL=" + HttpUtility.UrlEncode(Request.RawUrl);
            }

            while (photoList.Count < 4)
            {
                //add nophoto placeholders
                photoList.Add(CreateNoPhoto());
            }

            //check for private photo setting or display initial large photo
            _DisplayAllPhotosAsNoPrivate = !MemberPhotoDisplayManager.Instance.IsPhotosAvailableToGuests(g.Member, MemberProfile, g.Brand);
            pPendingPhoto.Style.Add("display", "none");
            if (_DisplayAllPhotosAsNoPrivate)
            {
                imgPhotoLarge.Style.Add("display", "none");
                lnkUploadAPhotoToday.Text = g.GetResource("TXT_ONLY_MEMBERS_CAN_SEE_MY_PHOTOS", this);
                lnkUploadAPhotoToday.NavigateUrl = "/Applications/Logon/Logon.aspx?DestinationURL=" + HttpUtility.UrlEncode(Request.RawUrl);

            }
            else
            {
                //display initial large photo
                Photo photo = (Photo)photoList[0];

                if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Full, g.Brand))
                {
                    if (photo.IsCaptionApproved || _IsViewingOwnProfile)
                    {
                        HtmlGenericControl caption = (HtmlGenericControl)this.FindControl("txtCaption");
                        if (photo.Caption.Length <= _MaxCaptionLength)
                            caption.InnerHtml = HttpUtility.HtmlEncode(photo.Caption);
                        else
                            caption.InnerHtml = "<span title=\"" + photo.Caption + "\">" + HttpUtility.HtmlEncode(photo.Caption.Substring(0, _MaxCaptionLength)) + "&hellip;</span>";
                    }

                    imgPhotoLarge.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, MemberProfile, g.Brand, photo, PhotoType.Full, 
                        PrivatePhotoImageType.Full, NoPhotoImageType.ThumbV2, false);

                    //add javascript to resize photo
                    imgPhotoLarge.Attributes.Add("onload", "profile_resizePhoto30(this);");
                    pnlUploadAPhotoToday.Style.Add("display", "none");

                    if (_IsViewingOwnProfile)
                    {
                        this.lnkEditPhotos.Visible = true;
                        this.lnkEditPhotos.Text = g.GetResource("BTN_EDIT_PHOTO", this);
                        if (!photo.IsApproved)
                        {
                            pPendingPhoto.Style.Remove("display");
                        }
                    }
                }
                else
                {
                    //no photos, so hide photo
                    imgPhotoLarge.Style.Add("display", "none");

                    if (_IsViewingOwnProfile)
                    {
                        this.lnkEditPhotos.Visible = true;
                        this.lnkEditPhotos.Text = g.GetResource("BTN_EDIT_PHOTO", this);
                        pnlUploadAPhotoToday.Style.Add("display", "none");
                    }
                    else
                    {
                        //display ask me for my photo link
                        pnlUploadAPhotoToday.Style.Add("display", "block");
                    }
                }

            }

            //load thumbnails
            this.repeaterThumbnails.DataSource = photoList;
            this.repeaterThumbnails.DataBind();

        }

        public string GetRedirectUrl()
        {
            return g.GetLogonRedirect(HttpContext.Current.Request.RawUrl.ToString(), null);
        }

        #endregion

        #region Private methods
        private Photo CreateNoPhoto()
        {
            return new Photo(Constants.NULL_INT, Constants.NULL_INT, "", (byte)0);
        }
        #endregion
    }
}