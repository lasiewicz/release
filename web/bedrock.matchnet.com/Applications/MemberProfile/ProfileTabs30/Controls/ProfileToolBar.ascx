﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileToolBar.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileToolBar" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ContactHistory" Src="/Framework/Ui/ProfileElements/ContactHistory.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Block" Src="/Framework/Ui/ProfileElements/Block.ascx" %>

<div id="profile30ToolBar" class="profile30-toolbar clearfix">
	<ul>
	    <asp:PlaceHolder ID="phToolBarLabel" runat="server" Visible="false">
		<li><mn2:FrameworkLiteral ID="literalToolbar" runat="server" ResourceConstant="TXT_TOOLBAR"></mn2:FrameworkLiteral></li>
		</asp:PlaceHolder>
		<li class="history item tabbed">
		    <div class="tabbed-wrapper">
		        <uc1:ContactHistory ID="contactHistory1" runat="server" 
		            HidePopupHeaderText = "true"
		            IsContentLazyLoaded="false" 
		            IconCSS="spr s-icon-hover-contact-active s-icon-hover-contact-performed" 
		            IconNoHistoryCSS="spr s-icon-A-hover-contact">
		        </uc1:ContactHistory>
		    </div>
        </li>
		<li><asp:HyperLink ID="lnkSendToFriend" runat="server" Visible="False">
            <span><mn2:FrameworkLiteral ID="literalSendToFriend" ResourceConstant="PRO_SEND_TO_FRIEND" runat="server"></mn2:FrameworkLiteral></span>
            </asp:HyperLink>
        </li>
		<li><asp:HyperLink ID="lnkReportConcern" runat="server" Visible="False"><mn2:FrameworkLiteral ID="literalReportConcern" ResourceConstant="REPORT_MEMBER" runat="server"></mn2:FrameworkLiteral></asp:HyperLink></li>
		<li class="block item tabbed">
		    <div class="tabbed-wrapper right">
		        <%--Block--%>
                <uc1:Block ID="blockProfile" runat="server" 
                    HideLinkText = "false"
                    LinkResourceConstant="TXT_BLOCK" 
                    LinkTitleResourceConstant="ALT_BLOCK"
                    HeaderResourceConstant="TXT_BLOCK_FROM"
                    IconCSS="spr s-icon-A-hover-block" 
                    IconBlockedCSS = "spr s-icon-hover-block-active s-icon-hover-block-performed">
                </uc1:Block>
            </div>
		</li>
	</ul>

</div>
<script type="text/javascript">
    TabbedMenuToggler($j('#profile30ToolBar'), 'no');
</script>

<asp:PlaceHolder ID="phHideToolBar" runat="server" Visible="false">
<%--Block it if see your own profile--%>
<script type="text/javascript">
    $j(function () {
        BlockElem($j('#profile30ToolBar'));
    });
</script>
</asp:PlaceHolder>