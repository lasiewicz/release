﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecretAdmirerGame.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.SecretAdmirerGame" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>




<div id="fp-click20" class="clearfix clear-both spr-bg-x s-bg-bar">
    <div id="fp-click20-back">
        <asp:HyperLink ID="lnkBackToHome" runat="server" NavigateUrl="#">
            <mn2:FrameworkLiteral ID="literalBackToHome" runat="server"></mn2:FrameworkLiteral>
        </asp:HyperLink>
    </div>
    <div class="click20 click20-horizontal">
        <h3>
            <mn2:FrameworkLiteral ID="literalDoWeClick" runat="server" ResourceConstant="TXT_DO_WE_CLICK"></mn2:FrameworkLiteral></h3>
        <div class="click20-ynm">
            <div class="click20-option">
                <asp:HyperLink ID="lnkYes" runat="server" CssClass="click-btn">
                    <mn:Txt ID="mntxt5163" runat="server" ResourceConstant="TXT_CLICK_SPRITE_YES" ExpandImageTokens="true" />
                </asp:HyperLink>
            </div>
            <div class="click20-option">
                <asp:HyperLink ID="lnkNo" runat="server" CssClass="click-btn">
                    <mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_CLICK_SPRITE_NO" ExpandImageTokens="true" />
                </asp:HyperLink>
            </div>
            <div class="click20-option">
                <asp:HyperLink ID="lnkMaybe" runat="server" CssClass="click-btn">
                    <mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_CLICK_SPRITE_MAYBE" ExpandImageTokens="true" />
                </asp:HyperLink>
            </div>
        </div>
    </div>
</div>