﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Lib;
using Matchnet.Member.ValueObjects.Interfaces;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls
{
    public partial class ProfileDetails : BaseProfile
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadProfileDetails(IMemberDTO member)
        {
            MemberProfile = member;
            if (g.Member != null && g.Member.MemberID == member.MemberID)
                WhoseProfile = WhoseProfileEnum.Self;

            //memberID
            literalMemberIDValue.Text = member.MemberID.ToString();

            //details title
            int genderMask = member.GetAttributeInt(g.Brand, "gendermask");
            if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                literalDetailsTitle.ResourceConstant = "HIS_DETAILS";
            else
                literalDetailsTitle.ResourceConstant = "HER_DETAILS";

            //load details data
            nvDataDetailsInfo.LoadDataGroup(member, ProfileUtility.GetProfileDataGroup(ProfileDataGroupEnum.TheDetails, g.Brand.Site.SiteID, g));

            //load ideal match data
            nvDataIdealMatch.LoadDataGroup(member, ProfileUtility.GetProfileDataGroup(ProfileDataGroupEnum.IdealMatch, g.Brand.Site.SiteID, g));

        }


    }
}