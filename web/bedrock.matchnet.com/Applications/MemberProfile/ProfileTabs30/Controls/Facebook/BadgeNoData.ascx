﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BadgeNoData.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Facebook.BadgeNoData" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div class="fb-likes-badge">
	<a href="#" class="title" title="<%=g.GetResource("TXT_BADGE_TITLE", this)%>"><span><mn:Txt ID="badgeTitle" ResourceConstant="TXT_BADGE_TITLE" runat="server" /></span></a>
	<blockquote><mn:Txt ID="descText" ResourceConstant="TXT_NON_CONNECTED" runat="server" /></blockquote>
	<span class="spr s-icon-fb-sm"></span>
	<ul class="fb-likes-list">
		<mn:Txt ID="txtLikesList" ResourceConstant="TXT_DEFAULT_LIKES_LIST" runat="server" />
	</ul>
</div>

<script type="text/javascript">
    $j('.fb-likes-badge').click(function (e) {
        e.preventDefault();
        FBBadgeBumpToLikesInterests();
    });
    function FBBadgeBumpToLikesInterests() {
        if (tabGroup != null) {
            tabGroup.goToTab(1);
        }
        return false;
    }
</script>

