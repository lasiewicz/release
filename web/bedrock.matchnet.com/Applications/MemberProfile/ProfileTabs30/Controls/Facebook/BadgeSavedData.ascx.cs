﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Interfaces;
using Spark.FacebookLike.ValueObjects;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Facebook
{
    public partial class BadgeSavedData : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadBadge(Member.ServiceAdapters.Member member)
        {
            if (null != member)
            {
                int randomFacebookLikeCount = FacebookManager.Instance.GetRandomFacebookLikeCount(g.Brand);
                bool approvedOnly = true;
                if (g.Member != null && g.Member.MemberID == member.MemberID)
                {
                    approvedOnly = false;
                }
                List<FacebookLike> randomFacebookSavedLikes = FacebookManager.Instance.GetRandomFacebookSavedLikes(member, g.Brand, randomFacebookLikeCount, approvedOnly);
                LoadBadge(randomFacebookSavedLikes);
            }
        }

        public void LoadBadge(List<FacebookLike> fbLikes)
        {
            if (null == fbLikes || fbLikes.Count == 0) return;
            int randomFacebookLikeCount = FacebookManager.Instance.GetRandomFacebookLikeCount(g.Brand);
            if (fbLikes.Count < randomFacebookLikeCount)
            {
                while (fbLikes.Count < randomFacebookLikeCount)
                {
                    FacebookLike facebookLike = new FacebookLike();
                    facebookLike.MemberId = Constants.NULL_INT;
                    fbLikes.Add(facebookLike);
                }
            }

            FacebookLike firstFbLike = fbLikes.First();
            if (null != g.Member && g.Member.MemberID == firstFbLike.MemberId)
            {
                PlaceHolder phPublishStatus = this.FindControl("phPublishStatus") as PlaceHolder;
                Literal badgeStatusValue = this.FindControl("badgeStatusValue") as Literal;
                Literal badgeStatusValueDescription = this.FindControl("badgeStatusValueDescription") as Literal;
                FacebookManager.FacebookConnectStatus facebookConnectStatus = FacebookManager.Instance.GetFacebookConnectStatus(g.Member, g.Brand);
                if (facebookConnectStatus == FacebookManager.FacebookConnectStatus.ConnectedShowData)
                {
                    badgeStatusValue.Text = ResourceManager.GetResource("TXT_BADGE_STATUS_DISPLAY", this);
                    badgeStatusValueDescription.Text = ResourceManager.GetResource("TXT_BADGE_STATUS_DISPLAY_DESC", this);
                } 
                else
                {
                    badgeStatusValue.Text = ResourceManager.GetResource("TXT_BADGE_STATUS_HIDDEN", this);
                    badgeStatusValueDescription.Text = ResourceManager.GetResource("TXT_BADGE_STATUS_HIDDEN_DESC", this);                    
                }
                phPublishStatus.Visible = true;
            }

            Repeater savedFBLikesRepeater = this.FindControl("savedFBLikes") as Repeater;
            savedFBLikesRepeater.DataSource = fbLikes;
            savedFBLikesRepeater.DataBind();
        }

        public void savedFBLikes_OnItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                FacebookLike facebookLike = e.Item.DataItem as FacebookLike;
                Literal likeCssClass = e.Item.FindControl("likeCssClass") as Literal;

                if (facebookLike.MemberId == Constants.NULL_INT)
                {
                    likeCssClass.Text = "none";
                }
                else
                {
                    switch (e.Item.ItemIndex)
                    {
                        case 0:
                            likeCssClass.Text = "one";
                            break;
                        case 1:
                            likeCssClass.Text = "two";
                            break;
                        case 2:
                            likeCssClass.Text = "three";
                            break;
                        case 3:
                            likeCssClass.Text = "four";
                            break;
                        default:
                            break;
                    }

                    System.Web.UI.WebControls.Image likeImage = e.Item.FindControl("likeImage") as System.Web.UI.WebControls.Image;
                    likeImage.ImageUrl = facebookLike.FacebookImageUrl;
                    likeImage.AlternateText = facebookLike.FacebookName;
                    likeImage.Visible = true;
                }
            }
        }
    }
}
