﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit
{
    /// <summary>
    /// This is an empty user control that will be used for resource file data for all the various Edit controls
    /// </summary>
    public partial class EditResourceControl : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}