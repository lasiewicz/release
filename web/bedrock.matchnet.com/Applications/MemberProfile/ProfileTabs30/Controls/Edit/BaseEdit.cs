﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit
{
    public class BaseEdit : BaseProfile
    {
        protected XMLProfileData.ProfileDataGroup _ProfileDataGroup;
        protected XMLProfileData.ProfileDataSection _ProfileDataSection;
        protected XMLProfileData.ProfileDataItem _ProfileDataItem;

        //This is usually the container passing itself in as the control to be used for resources
        protected System.Web.UI.Control _ResourceControl;

        //This is an internal control used for resources common to all edit controls regardless of container
        protected static System.Web.UI.Control _InternalEditResourceControl;

        //Determines whether to render control as a html <DT>
        public bool RenderAsDTItem { get; set; }

        //These are used for rendering JS objects to represent the edit item on the client side
        protected string _JavascriptSectionVarName;
        public string ViewItemContainerID { get; set; }
        public string EditItemContainerID { get; set; }
        public int CurrentIndex { get; set; }
        public BaseEdit()
        {
            //All edit controls default to render as DT Item
            RenderAsDTItem = true;
            ViewItemContainerID = "";
            EditItemContainerID = "";

            if (g != null)
            {
                MemberProfile = g.Member;
            }

            if (_InternalEditResourceControl == null)
            {
                System.Web.UI.Page p = new System.Web.UI.Page();
                _InternalEditResourceControl = p.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditResourceControl.ascx");
            }
        }

        public virtual void LoadEditControl(XMLProfileData.ProfileDataGroup ProfileDataGroup, XMLProfileData.ProfileDataSection ProfileDataSection, XMLProfileData.ProfileDataItem ProfileDataItem, string JavascriptSectionVarName, System.Web.UI.Control ResourceControl)
        {
        }

        protected void AddEditItemJS(string editControlID, bool isEditControlIDJSON, string errorContainerID)
        {
            AddEditItemJS(editControlID, isEditControlIDJSON, EditItemContainerID, ViewItemContainerID, errorContainerID);

        }

        protected void AddEditItemJS(string editControlID, bool isEditControlIDJSON, string editContainerID, string viewContainerID, string errorContainerID)
        {
            if (g.ProfileEditItemJS == null)
            {
                g.ProfileEditItemJS = new StringBuilder();
            }

            g.ProfileEditItemJS.Append(EditProfileUtility.GetJSEditItem(g, _ProfileDataGroup, _ProfileDataSection, _ProfileDataItem, _JavascriptSectionVarName, editControlID, isEditControlIDJSON, editContainerID, viewContainerID, errorContainerID, CurrentIndex));

        }
    }
}
