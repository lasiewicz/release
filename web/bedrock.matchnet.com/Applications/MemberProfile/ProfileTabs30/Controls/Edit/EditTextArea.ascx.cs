﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit
{
    public partial class EditTextArea : BaseEdit
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Public Methods
        public override void LoadEditControl(XMLProfileData.ProfileDataGroup ProfileDataGroup, XMLProfileData.ProfileDataSection ProfileDataSection, XMLProfileData.ProfileDataItem ProfileDataItem, string JavascriptSectionVarName, System.Web.UI.Control ResourceControl)
        {
            _ProfileDataGroup = ProfileDataGroup;
            _ProfileDataSection = ProfileDataSection;
            _ProfileDataItem = ProfileDataItem;
            _JavascriptSectionVarName = JavascriptSectionVarName;
            _ResourceControl = ResourceControl;

            //populate edit fields
            literalName.Text = g.GetResource(String.IsNullOrEmpty(ProfileDataItem.EditResourceConstant) ? ProfileDataItem.ResourceConstant : ProfileDataItem.EditResourceConstant, ResourceControl);

            if (ProfileDataItem.EditDataType != EditDataTypeEnum.Essay)
            {
                txtEdit.Text = g.Member.GetAttributeText(g.Brand, ProfileDataItem.AttributeName);
            }

            //display edit note
            if (!String.IsNullOrEmpty(ProfileDataItem.EditNoteResourceConstant))
            {
                litEditNote.Text = g.GetResource(ProfileDataItem.EditNoteResourceConstant, ResourceControl).Replace("{min}", ProfileDataItem.MinLength.ToString());
            }

            //render JS Edit Item object to contain info on this edit control
            AddEditItemJS(txtEdit.ClientID, false, itemErrorContainer.ClientID);
        }

        #endregion

    }
}