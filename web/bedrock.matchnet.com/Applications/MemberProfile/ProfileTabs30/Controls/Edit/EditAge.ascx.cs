﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit
{
    public partial class EditAge : BaseEdit
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Public Methods
        public override void LoadEditControl(XMLProfileData.ProfileDataGroup ProfileDataGroup, XMLProfileData.ProfileDataSection ProfileDataSection, XMLProfileData.ProfileDataItem ProfileDataItem, string JavascriptSectionVarName, System.Web.UI.Control ResourceControl)
        {
            _ProfileDataGroup = ProfileDataGroup;
            _ProfileDataSection = ProfileDataSection;
            _ProfileDataItem = ProfileDataItem;
            _JavascriptSectionVarName = JavascriptSectionVarName;
            _ResourceControl = ResourceControl;

            //populate label
            litEdit.Text = g.GetResource(String.IsNullOrEmpty(ProfileDataItem.EditResourceConstant) ? ProfileDataItem.ResourceConstant : ProfileDataItem.EditResourceConstant, ResourceControl);
            litFrom.Text = g.GetResource("FROM", ResourceControl);
            litTo.Text = g.GetResource("TO", ResourceControl);

            //required indicator
            if (_ProfileDataItem.IsRequired)
            {
                litRequired.Text = g.GetResource("TXT_REQUIRED", _InternalEditResourceControl);
            }

            int minAge = MemberProfile.GetAttributeInt(g.Brand, "DesiredMinAge");
            int maxAge = MemberProfile.GetAttributeInt(g.Brand, "DesiredMaxAge");

            if (minAge > 0)
                txtAgeMin.Text = minAge.ToString();

            if (maxAge > 0)
                txtAgeMax.Text = maxAge.ToString();

            //render JS Edit Item object to contain info on this edit control
            AddEditItemJS("{minAgeID: '" + txtAgeMin.ClientID + "',maxAgeID:'" + txtAgeMax.ClientID + "'}", true, itemErrorContainer.ClientID);

        }

        #endregion
    }
}