﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditAge.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit.EditAge" %>

<%--Custom: Age range--%>
<div class="form-item">
    <div class="form-label">
        <asp:Label ID="lblEdit" runat="server" AssociatedControlID="txtAgeMin">
            <asp:Literal ID="litEdit" runat="server"></asp:Literal>
        </asp:Label>
    </div>    
    <div class="form-input">
        <label><asp:Literal ID="litFrom" runat="server"></asp:Literal></label>
        <asp:TextBox ID="txtAgeMin" Runat="server" MaxLength="2" Width="28" />
        <label><asp:Literal ID="litTo" runat="server"></asp:Literal></label>
        <asp:TextBox ID="txtAgeMax" Runat="server" MaxLength="2" Width="28" />
        <asp:Literal ID="litRequired" runat="server"></asp:Literal>
        <div id="itemErrorContainer" runat="server" style="display:none;" class="form-error-inline"></div>
    </div>
</div>