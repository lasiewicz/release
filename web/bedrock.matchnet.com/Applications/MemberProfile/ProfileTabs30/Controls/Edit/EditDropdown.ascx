﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditDropdown.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit.EditDropdown" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<%--Dropdown--%>
<div class="form-item">
    <div class="form-label">
        <asp:Label ID="lblEdit" runat="server" AssociatedControlID="ddlEdit">
            <asp:Literal ID="litEdit" runat="server"></asp:Literal>
        </asp:Label>
    </div>
    <div class="form-input">
        <asp:DropDownList ID="ddlEdit" runat="server" CssClass="xlarge"></asp:DropDownList>
        <asp:Literal ID="litRequired" runat="server"></asp:Literal>
        <div id="itemErrorContainer" runat="server" style="display:none;" class="form-error-inline"></div>
    </div>
</div>

<asp:HiddenField ID="hidOriginalValue" runat="server" />