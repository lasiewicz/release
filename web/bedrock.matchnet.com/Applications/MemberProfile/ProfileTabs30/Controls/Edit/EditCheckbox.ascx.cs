﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework.Util;
using System.Data;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit
{
    public partial class EditCheckbox : BaseEdit
    {
        protected int _CurrentMaskValue = Constants.NULL_INT;
        protected string _CheckBoxGroupName = "";

        #region Event handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rptOptions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;

                CheckBox chkItemEdit = e.Item.FindControl("chkItemEdit") as CheckBox;
                chkItemEdit.Text = row["Content"].ToString();
                int optionValue = Conversion.CInt(row["Value"]);
                chkItemEdit.Checked = ((_CurrentMaskValue & optionValue) == optionValue);
                chkItemEdit.InputAttributes.Add("Value", optionValue.ToString());
                chkItemEdit.Attributes.Add("onclick", "EditCheckbox_Click('" + chkItemEdit.ClientID + "', '" + hidEditValue.ClientID + "', " + optionValue.ToString() + ")");
            }
        }

        #endregion

        #region Public Methods
        public override void LoadEditControl(XMLProfileData.ProfileDataGroup ProfileDataGroup, XMLProfileData.ProfileDataSection ProfileDataSection, XMLProfileData.ProfileDataItem ProfileDataItem, string JavascriptSectionVarName, System.Web.UI.Control ResourceControl)
        {
            _ProfileDataGroup = ProfileDataGroup;
            _ProfileDataSection = ProfileDataSection;
            _ProfileDataItem = ProfileDataItem;
            _JavascriptSectionVarName = JavascriptSectionVarName;
            _ResourceControl = ResourceControl;

            //populate label
            litEdit.Text = g.GetResource(String.IsNullOrEmpty(ProfileDataItem.EditResourceConstant) ? ProfileDataItem.ResourceConstant : ProfileDataItem.EditResourceConstant, ResourceControl);
            literalEditName.Text = g.GetResource(String.IsNullOrEmpty(ProfileDataItem.EditResourceConstant) ? ProfileDataItem.ResourceConstant : ProfileDataItem.EditResourceConstant, ResourceControl);

            //populate edit fields
            DataTable dataTable = null;
            switch (ProfileDataItem.AttributeName.ToLower())
            {
                case "seekingmask":
                    _CurrentMaskValue = g.Member.GetAttributeInt(g.Brand, "RelationshipMask", 0);
                    dataTable = Option.GetOptions("RelationshipMask", g);
                    break;
                default:
                    _CurrentMaskValue = g.Member.GetAttributeInt(g.Brand, ProfileDataItem.AttributeName, 0);
                    dataTable = Option.GetOptions(ProfileDataItem.AttributeName, g);
                    break;
            }

            if (ProfileDataItem.EditControlProperty == EditControlPropertyEnum.Double)
            {
                //two columns view
                Repeater selectedRepeaterA;
                Repeater selectedRepeaterB;
                if (this.RenderAsDTItem)
                {
                    selectedRepeaterA = repeaterDTCheckboxEditDoubleA;
                    selectedRepeaterB = repeaterDTCheckboxEditDoubleB;
                    phDTEditItem.Visible = true;
                    phDTEditItemDouble.Visible = true;
                }
                else
                {
                    selectedRepeaterA = repeaterEditItemDoubleA;
                    selectedRepeaterB = repeaterEditItemDoubleB;
                    phEditItem.Visible = true;
                    phEditItemDouble.Visible = true;
                }

                int remainder = 0;
                int columnOneCount = Math.DivRem(dataTable.Rows.Count, 2, out remainder);
                if (remainder > 0)
                    columnOneCount++;
                int columnTwoCount = dataTable.Rows.Count - columnOneCount;

                DataTable dataTableA = new DataTable();
                dataTableA.Columns.Add("Value", typeof(System.String));
                dataTableA.Columns.Add("Content", typeof(System.String));

                for (int i = 0; i < columnOneCount; i++)
                {
                    DataRow newRow = dataTableA.NewRow();
                    newRow["Value"] = dataTable.Rows[i]["Value"];
                    newRow["Content"] = dataTable.Rows[i]["Content"];
                    dataTableA.Rows.Add(newRow);
                }
                selectedRepeaterA.DataSource = dataTableA;
                selectedRepeaterA.DataBind();

                if (columnTwoCount > 0)
                {
                    DataTable dataTableB = new DataTable();
                    dataTableB.Columns.Add("Value", typeof(System.String));
                    dataTableB.Columns.Add("Content", typeof(System.String));

                    for (int i = columnOneCount; i < dataTable.Rows.Count; i++)
                    {
                        DataRow newRow = dataTableB.NewRow();
                        newRow["Value"] = dataTable.Rows[i]["Value"];
                        newRow["Content"] = dataTable.Rows[i]["Content"];
                        dataTableB.Rows.Add(newRow);
                    }
                    selectedRepeaterB.DataSource = dataTableB;
                    selectedRepeaterB.DataBind();
                }
            }
            else
            {
                //one column view
                Repeater selectedRepeater;
                if (this.RenderAsDTItem)
                {
                    selectedRepeater = repeaterDTCheckBoxEdit;
                    phDTEditItem.Visible = true;
                    phDTEditItemSingle.Visible = true;
                }
                else
                {
                    selectedRepeater = repeaterCheckBoxEdit;
                    phEditItem.Visible = true;
                    phEditItemSingle.Visible = true;
                }

                selectedRepeater.DataSource = dataTable;
                selectedRepeater.DataBind();
            }

            hidEditValue.Value = _CurrentMaskValue.ToString();
            hidOriginalValue.Value = _CurrentMaskValue.ToString();

            //render JS Edit Item object to contain info on this edit control
            AddEditItemJS("{HidEditValueID:'" + hidEditValue.ClientID + "',HidOriginalValueID:'" + hidOriginalValue.ClientID + "'}", true, (RenderAsDTItem ? itemErrorContainer.ClientID : itemErrorContainer2.ClientID));
        }


        #endregion
    }
}