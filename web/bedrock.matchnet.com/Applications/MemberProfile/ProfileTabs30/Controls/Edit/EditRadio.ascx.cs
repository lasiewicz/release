﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework.Util;
using System.Data;
using Matchnet.Lib;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit
{
    public partial class EditRadio : BaseEdit
    {
        protected int _CurrentValue = Constants.NULL_INT;
        protected string _CurrentRadioGroupName = "";

        #region Event handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rptOptions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;

                Literal radioItemEdit = e.Item.FindControl("radioItemEdit") as Literal;
                int optionValue = Conversion.CInt(row["Value"]);
                string currentRadioID = _CurrentRadioGroupName + e.Item.ItemIndex.ToString();
                string radioSelected = (_CurrentValue == optionValue) ? "checked=\"checked\"" : "";
                radioItemEdit.Text = "<input id=\"" + currentRadioID + "\" type=\"radio\" name=\"" + _CurrentRadioGroupName + "\" value=\"" + optionValue.ToString() + "\"" + radioSelected + " /><label for=\"" + currentRadioID + "\">" + row["Content"].ToString() + "</label>";
            }
        }

        #endregion

        #region Public Methods
        public override void LoadEditControl(XMLProfileData.ProfileDataGroup ProfileDataGroup, XMLProfileData.ProfileDataSection ProfileDataSection, XMLProfileData.ProfileDataItem ProfileDataItem, string JavascriptSectionVarName, System.Web.UI.Control ResourceControl)
        {
            _ProfileDataGroup = ProfileDataGroup;
            _ProfileDataSection = ProfileDataSection;
            _ProfileDataItem = ProfileDataItem;
            _JavascriptSectionVarName = JavascriptSectionVarName;
            _ResourceControl = ResourceControl;

            _CurrentRadioGroupName = "radioItemEdit_" + _JavascriptSectionVarName + "_" + _ProfileDataItem.AttributeName;

            //populate label
            litEdit.Text = g.GetResource(String.IsNullOrEmpty(ProfileDataItem.EditResourceConstant) ? ProfileDataItem.ResourceConstant : ProfileDataItem.EditResourceConstant, ResourceControl);

            Repeater selectedRepeater = repeaterDTRadioEdit;

            //populate edit fields
            switch (ProfileDataItem.AttributeName.ToLower())
            {
                case "seekinggendermask":
                    int genderMask = g.Member.GetAttributeInt(g.Brand, "gendermask");
                    if ((genderMask & ConstantsTemp.GENDERID_SEEKING_FEMALE) == ConstantsTemp.GENDERID_SEEKING_FEMALE)
                    {
                        _CurrentValue = ConstantsTemp.GENDERID_SEEKING_FEMALE;
                    }
                    else if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MALE) == ConstantsTemp.GENDERID_SEEKING_MALE)
                    {
                        _CurrentValue = ConstantsTemp.GENDERID_SEEKING_MALE;
                    }

                    DataTable dtSeekingGender = new DataTable();
                    dtSeekingGender.Columns.Add("Value", typeof(System.String));
                    dtSeekingGender.Columns.Add("Content", typeof(System.String));
                    dtSeekingGender.Columns.Add("ListOrder", typeof(System.Int32));

                    DataRow rowMale = dtSeekingGender.NewRow();
                    rowMale["Value"] = ConstantsTemp.GENDERID_SEEKING_MALE;
                    rowMale["Content"] = g.GetResource("SEEKING_MALE");
                    rowMale["ListOrder"] = 0;
                    dtSeekingGender.Rows.Add(rowMale);

                    DataRow rowFemale = dtSeekingGender.NewRow();
                    rowFemale["Value"] = ConstantsTemp.GENDERID_SEEKING_FEMALE;
                    rowFemale["Content"] = g.GetResource("SEEKING_FEMALE");
                    rowFemale["ListOrder"] = 1;
                    dtSeekingGender.Rows.Add(rowFemale);

                    selectedRepeater.DataSource = dtSeekingGender;
                    selectedRepeater.DataBind();
                    break;
                case "desiredmorechildrenflag":
                    _CurrentValue = MemberProfile.GetAttributeInt(g.Brand, ProfileDataItem.AttributeName);
                    DataTable dtDesiredMoreChildren = Option.GetDesiredMoreChildrenOptions(g, _ResourceControl);

                    selectedRepeater.DataSource = dtDesiredMoreChildren;
                    selectedRepeater.DataBind();

                    break;
                default:
                    _CurrentValue = g.Member.GetAttributeInt(g.Brand, ProfileDataItem.AttributeName, 0);
                    selectedRepeater.DataSource = Option.GetOptions(ProfileDataItem.AttributeName, g);
                    selectedRepeater.DataBind();
                    break;
            }

            //render JS Edit Item object to contain info on this edit control
            AddEditItemJS(_CurrentRadioGroupName, false, itemErrorContainer.ClientID);
        }


        #endregion
    }
}