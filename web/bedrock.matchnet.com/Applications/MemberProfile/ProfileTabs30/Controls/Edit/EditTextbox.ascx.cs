﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit
{
    public partial class EditTextbox : BaseEdit
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Public Methods
        public override void LoadEditControl(XMLProfileData.ProfileDataGroup ProfileDataGroup, XMLProfileData.ProfileDataSection ProfileDataSection, XMLProfileData.ProfileDataItem ProfileDataItem, string JavascriptSectionVarName, System.Web.UI.Control ResourceControl)
        {
            _ProfileDataGroup = ProfileDataGroup;
            _ProfileDataSection = ProfileDataSection;
            _ProfileDataItem = ProfileDataItem;
            _JavascriptSectionVarName = JavascriptSectionVarName;
            _ResourceControl = ResourceControl;

            //populate label
            litEdit.Text = g.GetResource(String.IsNullOrEmpty(ProfileDataItem.EditResourceConstant) ? ProfileDataItem.ResourceConstant : ProfileDataItem.EditResourceConstant, ResourceControl);

            //required indicator
            if (_ProfileDataItem.IsRequired)
            {
                litRequired.Text = g.GetResource("TXT_REQUIRED", _InternalEditResourceControl);
            }

            //populate edit fields
            switch (ProfileDataItem.AttributeName.ToLower())
            {
                case "username":
                    txtEdit.Text = g.Member.GetUserName(g.Brand);
                    break;
                default:
                    txtEdit.Text = g.Member.GetAttributeText(g.Brand, ProfileDataItem.AttributeName);
                    break;
            }

            //display edit note
            if (!String.IsNullOrEmpty(ProfileDataItem.EditNoteResourceConstant))
            {
                litEditNote.Text = g.GetResource(ProfileDataItem.EditNoteResourceConstant, ResourceControl);
            }
            
            //render JS Edit Item object to contain info on this edit control
            AddEditItemJS(txtEdit.ClientID, false, itemErrorContainer.ClientID);
        }

        #endregion


    }
}