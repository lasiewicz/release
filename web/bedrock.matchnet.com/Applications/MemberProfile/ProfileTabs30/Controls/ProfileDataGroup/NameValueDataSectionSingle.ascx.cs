﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileData;
using Matchnet.Web.Framework;
using System.Web.UI.HtmlControls;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileDataGroup
{
    public partial class NameValueDataSectionSingle : BaseProfile
    {
        public int MaxDisplayLength { get; set; }
        public bool IsPartialRender { get; set; }
        public BaseDataGroup ResourceControl { get; set; }

        protected XMLProfileData.ProfileDataGroup _ProfileDataGroup = null;
        protected ProfileDataSection _ProfileDataSection = null;
        protected string _JSSectionObjName = "";
        protected string _ViewContainerEditCSSClass = "prof-edit-region prof-edit-active";
        private bool _hasRequiredField = false;
        private string _NoAnswer = "";

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            litRequiredMessage.Visible = _hasRequiredField;

            base.OnPreRender(e);
        }

        protected void repeaterItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                XMLProfileData.ProfileDataItem profileDataItem = e.Item.DataItem as XMLProfileData.ProfileDataItem;
                if (profileDataItem.IsRequired)
                {
                    _hasRequiredField = true;
                }

                Literal literalName = e.Item.FindControl("literalName") as Literal;
                literalName.Text = g.GetResource(profileDataItem.ResourceConstant, ResourceControl);

                Literal literalValue = e.Item.FindControl("literalValue") as Literal;
                string fullDisplayValue = ResourceControl.GetDataGroupAttributeDisplayValue(_MemberProfile, profileDataItem.AttributeName, ResourceControl, false);

                //remove &nbsp;
                fullDisplayValue = fullDisplayValue.Replace("&nbsp;", " ");

                if (String.IsNullOrEmpty(fullDisplayValue) || fullDisplayValue == _NoAnswer)
                {
                    literalValue.Text = _NoAnswer;
                    if (ViewProfileTabUtility.IsSuppressEmptyAttributesEnabled(g))
                    {
                        var ctrlPlcShow = e.Item.FindControl("plcShow") as PlaceHolder;
                        if (ctrlPlcShow != null)
                        {
                            ctrlPlcShow.Visible = false;
                        }
                    }
                }
                else if (MaxDisplayLength > 0 && fullDisplayValue.Length > MaxDisplayLength && !ProfileUtility.IgnoreMaxLength(fullDisplayValue, GetNoAnswer(ResourceControl)))
                {
                    literalValue.Text = "<span title=\"" + Server.HtmlEncode(fullDisplayValue) + "\">" + fullDisplayValue.Substring(0, MaxDisplayLength - 1) + "&hellip;</span>";
                }
                else
                {
                    literalValue.Text = fullDisplayValue.Replace("/", " / ");
                }

                HtmlGenericControl ddValue = e.Item.FindControl("ddValue") as HtmlGenericControl;
                if (profileDataItem.KeywordSearchable && literalValue.Text != _NoAnswer)
                {
                    ddValue.Attributes["class"] = "highlight-text";
                }

                if (this.WhoseProfile == WhoseProfileEnum.Self && !IsPartialRender)
                {
                    BaseEdit editControl = EditProfileUtility.GetEditControl(Page, profileDataItem.EditControlType, profileDataItem.AttributeName);
                    if (editControl != null)
                    {
                        phEditItemControl.Controls.Add(editControl);
                        editControl.LoadEditControl(_ProfileDataGroup, _ProfileDataSection, profileDataItem, _JSSectionObjName, ResourceControl);
                    }

                }

            }
        }

        #endregion

        public void LoadDataSection(IMemberDTO member, XMLProfileData.ProfileDataGroup profileDataGroup, ProfileDataSection profileDataSection)
        {
            if (member != null && profileDataSection != null)
            {
                _MemberProfile = member;
                _ProfileDataSection = profileDataSection;
                _ProfileDataGroup = profileDataGroup;
                _NoAnswer = GetNoAnswer(ResourceControl);

                if (g.Member != null && member.MemberID == g.Member.MemberID)
                    this.WhoseProfile = WhoseProfileEnum.Self;

                //display section title
                if (!String.IsNullOrEmpty(profileDataSection.SectionTitleResourceConstant))
                {
                    phTitle.Visible = true;
                    literalTitle.Text = g.GetResource(profileDataSection.SectionTitleResourceConstant, ResourceControl);
                }

                //render support for edit
                if (WhoseProfile == WhoseProfileEnum.Self)
                {
                    phEditIcon.Visible = true;
                    litEditIcon.Text = g.GetResource("EDIT_ICON", ResourceControl);
                    viewSectionContainer.Attributes["class"] = _ViewContainerEditCSSClass;
                    litRequiredMessage.Text = g.GetResource("TXT_REQUIRED_MESSAGE", ResourceControl);
                }

                if (this.WhoseProfile == WhoseProfileEnum.Self && !IsPartialRender)
                {
                    phEditSection.Visible = true;

                    viewSectionContainer.Attributes.Add("editsectionname", profileDataSection.ProfileDataSectionName.ToString());
                    viewSectionContainer.Attributes.Add("edittype", "modal");

                    buttonEditSave.Text = g.GetResource("BTN_EDIT_SAVE", ResourceControl);
                    buttonEditSave.OnClientClick = "editProfileManager.SaveProfile('" + profileDataSection.ProfileDataSectionName.ToString() + "', '');return false;";

                    buttonEditCancel.Text = g.GetResource("BTN_EDIT_CANCEL", ResourceControl);
                    buttonEditCancel.OnClientClick = "editProfileManager.CancelSectionEdit('" + profileDataSection.ProfileDataSectionName.ToString() + "');return false;";

                    //render JS section object
                    EditProfileUtility.AddEditSectionJS(g, EditProfileUtility.GetJSEditSection(g, ResourceControl, profileDataSection, profileDataGroup.ProfileDataGroupType,
                        viewSectionContainer.ClientID, editSectionContainer.ClientID, errorSectionContainer.ClientID, true, out _JSSectionObjName));

                }

                //render data for view
                repeaterItem.DataSource = profileDataSection.ProfileDataItemList;
                repeaterItem.DataBind();
            }
        }
    }
}