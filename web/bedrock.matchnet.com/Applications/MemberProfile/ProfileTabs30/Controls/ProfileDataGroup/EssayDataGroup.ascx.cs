﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.MemberLike;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit;
using System.Text;
using Matchnet.Web.Framework.Ui.FormElements;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Lib;
using Matchnet.List.ValueObjects;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileDataGroup
{
    /// <summary>
    /// Renders profile data in a format meant for essays or large text
    /// </summary>
    public partial class EssayDataGroup : BaseDataGroup
    {
        public int MaxDisplayLength { get; set; }
        protected string _CurrentJSSectionObjName = "";
        protected XMLProfileData.ProfileDataSection _CurrentProfileDataSection = null;
        protected string _ViewContainerEditCSSClass = "prof-edit-region prof-edit-inline prof-edit-active";
        protected const int ESSAY_CHARACTERS_PER_LINE = 40;
        private bool? _isCommentEnabled = null;
        private bool _hasAnswerEssay = false;
        private bool _isEssayOnly = true;
        protected int _ViewerMemberID = 0;

        public EssayDataGroup()
        {
            MaxDisplayLength = 0;
        }

        public bool IsFreeTextCommentEnabled
        {
            get
            {
                if (!_isCommentEnabled.HasValue)
                {
                    _isCommentEnabled = IsFreeTextCommentsEnabled();
                }

                return (bool)_isCommentEnabled;
            }
        }
        public bool IsFreeTextCommentsEnabled()
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_FREETEXT_COMMENT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        public bool IsFreeTextMemberLikeEnabled
        {
            get { return MemberLikeHelper.Instance.IsFreeTextMemberLikeEnabled(g.Brand); }
        }

        public string GetRedirectUrl(int prtId)
        {
            string redirectUrl = string.Empty;
            if (g.Member != null && g.Member.MemberID > 0)
            {
                if (!g.Member.IsPayingMember(g.Brand.Site.SiteID))
                {
                    Matchnet.Content.ServiceAdapters.Links.ParameterCollection parameterCollection = new Matchnet.Content.ServiceAdapters.Links.ParameterCollection();
                    if (_g.ImpersonateContext)
                    {
                        parameterCollection.Add(WebConstants.IMPERSONATE_MEMBERID, _g.TargetMemberID.ToString());
                        parameterCollection.Add(WebConstants.IMPERSONATE_BRANDID, _g.TargetBrandID.ToString());
                    }
                    redirectUrl = Framework.Util.Redirect.GetSubscriptionUrl(_g.Brand, prtId, ProfileMemberID, false, HttpContext.Current.Server.UrlEncode(HttpContext.Current.Items["FullURL"].ToString()), parameterCollection);
                }
            }
            return redirectUrl;
        }

        public bool IsViewingOwnProfile
        {
            get
            {
                return (this.WhoseProfile == WhoseProfileEnum.Self);
            }
        }

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void repeaterSection_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                XMLProfileData.ProfileDataSection profileDataSection = e.Item.DataItem as XMLProfileData.ProfileDataSection;
                _CurrentProfileDataSection = profileDataSection;

                if (!String.IsNullOrEmpty(profileDataSection.SectionTitleResourceConstant))
                {
                    PlaceHolder phTitle = e.Item.FindControl("phTitle") as PlaceHolder;
                    phTitle.Visible = true;
                    Literal literalTitle = e.Item.FindControl("literalTitle") as Literal;
                    literalTitle.Text = g.GetResource(profileDataSection.SectionTitleResourceConstant, this);
                }

                if (this.WhoseProfile == WhoseProfileEnum.Self && !IsPartialRender)
                {
                    //render JS section object
                    EditProfileUtility.AddEditSectionJS(g, EditProfileUtility.GetJSEditSection(g, this, profileDataSection, _ProfileDataGroup.ProfileDataGroupType, "", "", "", false, out _CurrentJSSectionObjName));
                }

                Repeater repeaterItem = e.Item.FindControl("repeaterItem") as Repeater;
                repeaterItem.DataSource = profileDataSection.ProfileDataItemList;
                repeaterItem.DataBind();

            }
        }

        protected void repeaterItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                XMLProfileData.ProfileDataItem profileDataItem = e.Item.DataItem as XMLProfileData.ProfileDataItem;

                Literal literalName = e.Item.FindControl("literalName") as Literal;
                literalName.Text = g.GetResource(profileDataItem.ResourceConstant, this);
                HtmlGenericControl notificationDiv = e.Item.FindControl("notification") as HtmlGenericControl;

                Literal literalValue = e.Item.FindControl("literalValue") as Literal;
                string fullDisplayValue = "";
                if (profileDataItem.EditDataType == EditDataTypeEnum.Essay)
                {
                    Matchnet.Member.ValueObjects.TextStatusType textStatus = Member.ValueObjects.TextStatusType.None;
                    fullDisplayValue = ProfileDisplayHelper.GetFormattedEssayText(MemberProfile.GetAttributeText(g.Brand, profileDataItem.AttributeName, out textStatus), ESSAY_CHARACTERS_PER_LINE);
                    //if member is viewing own profile, initialize pending notification
                    if (ProfileMemberID == MemberId)
                    {
                        notificationDiv.Visible = true;
                        notificationDiv.Style.Add("visibility", "hidden");
                        notificationDiv.Style.Add("display", "none");
                    }

                    if (!string.IsNullOrEmpty(fullDisplayValue))
                    {
                        //if text is not approved
                        if (textStatus != Member.ValueObjects.TextStatusType.Auto && textStatus != Member.ValueObjects.TextStatusType.Human)
                        {
                            //if member is viewing own profile, show pending notification
                            if (ProfileMemberID == MemberId)
                            {
                                notificationDiv.Visible = true;
                                notificationDiv.Style.Remove("visibility");
                                notificationDiv.Style.Remove("display");
                            }
                            else
                            {
                                //if other member is viewing, show old answer (if exists)
                                fullDisplayValue = ProfileDisplayHelper.GetFormattedEssayText(MemberProfile.GetAttributeText(g.Brand, profileDataItem.AttributeName + "-old", out textStatus), ESSAY_CHARACTERS_PER_LINE);
                                //make certain old answer is approved
                                if (string.IsNullOrEmpty(fullDisplayValue) || textStatus != Member.ValueObjects.TextStatusType.Auto && textStatus != Member.ValueObjects.TextStatusType.Human)
                                {
                                    fullDisplayValue = g.GetResource("FREETEXT_NOT_APPROVED", null);
                                }
                            }
                        }
                    }
                }
                else
                {
                    fullDisplayValue = GetMemberAttributeValue(profileDataItem.AttributeName, this, false);
                }

                //convert new lines to html breaks
                if (!String.IsNullOrEmpty(fullDisplayValue) && profileDataItem.EditDataType == EditDataTypeEnum.Essay)
                {
                    fullDisplayValue = fullDisplayValue.Replace("\r\n", "<br />");
                    fullDisplayValue = fullDisplayValue.Replace("\n", "<br />");
                    fullDisplayValue = fullDisplayValue.Replace("\r", "<br />");
                }

                //display text
                literalValue.Text = fullDisplayValue;
                if (String.IsNullOrEmpty(fullDisplayValue))
                {
                    literalValue.Text = GetNoAnswer(this);
                }

                //add keyword highlighting support
                HtmlGenericControl pEssayValue = e.Item.FindControl("pEssayValue") as HtmlGenericControl;
                if (profileDataItem.KeywordSearchable && literalValue.Text != GetNoAnswer(this))
                {
                    pEssayValue.Attributes["class"] = "essay-value highlight-text";
                }

                if ((ProfileMemberID != MemberId)
                    && profileDataItem.CommentsEnabled
                    && fullDisplayValue != g.GetResource("FREETEXT_NOT_APPROVED", null)
                    && fullDisplayValue != g.GetResource("NOT_ANSWERED_YET", this)
                    && fullDisplayValue != "")
                {
                    Panel LikeAndCommentPanel = e.Item.FindControl("pLikeAndComment") as Panel;
                    LikeAndCommentPanel.Visible = true;

                    HyperLink commentLink = e.Item.FindControl("commentLink") as HyperLink;
                    commentLink.NavigateUrl = GetRedirectUrl(1234);

                    Literal LiteralAttributeName = e.Item.FindControl("literalAttributeName") as Literal;
                    LiteralAttributeName.Text = profileDataItem.AttributeName.ToString();

                    Literal LiteralAttributeName2 = e.Item.FindControl("literalAttributeName2") as Literal;
                    LiteralAttributeName2.Text = profileDataItem.AttributeName.ToString();

                    Literal LiteralSectionAttribute = e.Item.FindControl("sectionAttribute") as Literal;
                    LiteralSectionAttribute.Text = profileDataItem.AttributeName.ToString();

                    Literal LiteralHashCodeInt = e.Item.FindControl("hashCodeInt") as Literal;
                    string uniqueid = ProfileMemberID.ToString() + "-" + profileDataItem.AttributeName;
                    int intUniqueId = uniqueid.GetHashCode();
                    LiteralHashCodeInt.Text = intUniqueId.ToString();

                    MemberLikeParams mlp = new MemberLikeParams();
                    mlp.MemberId = MemberId;
                    mlp.SiteId = g.Brand.Site.SiteID;
                    int likeCounts = MemberLikeHelper.Instance.GetCountOfLikesByObjectIdandType(mlp, intUniqueId, (int)LikeObjectTypes.ProfileFreeText);
                    if (likeCounts > 0)
                    {
                        Literal LiteralLikeTxtCount = e.Item.FindControl("likeTxt") as Literal;
                        LiteralLikeTxtCount.Text = "(" + likeCounts + ")";
                        LiteralLikeTxtCount.Visible = true;
                    }

                }

                bool showThoseWhoLikedMyEssay = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_THOSE_WHO_LIKED_MY_ESSAY", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                if (showThoseWhoLikedMyEssay)
                {
                    if (this.WhoseProfile == WhoseProfileEnum.Self && profileDataItem.CommentsEnabled &&
                        fullDisplayValue != g.GetResource("FREETEXT_NOT_APPROVED", null)
                        && fullDisplayValue != g.GetResource("NOT_ANSWERED_YET", this)
                        && fullDisplayValue != "")
                    {
                        Literal LiteralHashCodeInt = e.Item.FindControl("hashCodeInt") as Literal;
                        string uniqueid = ProfileMemberID.ToString() + "-" + profileDataItem.AttributeName;
                        int intUniqueId = uniqueid.GetHashCode();

                        MemberLikeParams mlp = new MemberLikeParams();
                        mlp.MemberId = MemberId;
                        mlp.SiteId = g.Brand.Site.SiteID;
                        int likeCounts = MemberLikeHelper.Instance.GetCountOfLikesByObjectIdandType(mlp, intUniqueId, (int)LikeObjectTypes.ProfileFreeText);
                        if (likeCounts > 0)
                        {
                            Panel LikeAndCommentPanel = e.Item.FindControl("pLikeAndComment") as Panel;
                            LikeAndCommentPanel.Visible = true;

                            MemberLikeAnswerClientModule memberLikeClientModule = e.Item.FindControl("memberLikeClientModuleFreeText") as MemberLikeAnswerClientModule;
                            if (null != memberLikeClientModule)
                            {
                                memberLikeClientModule.Visible = true;
                                memberLikeClientModule.MyEntryPoint = Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.MemberLikeLink;
                                memberLikeClientModule.Ordinal = e.Item.ItemIndex;
                                memberLikeClientModule.LoadMemberLikeClientModule(intUniqueId);
                            }
                        }
                    }
                }

                if (this.WhoseProfile == WhoseProfileEnum.Self)
                {
                    PlaceHolder phEditIcon = e.Item.FindControl("phEditIcon") as PlaceHolder;
                    phEditIcon.Visible = true;

                    Literal litEditIcon = e.Item.FindControl("litEditIcon") as Literal;
                    litEditIcon.Text = g.GetResource("EDIT_ICON", this);
                }

                if (this.WhoseProfile == WhoseProfileEnum.Self && !IsPartialRender)
                {
                    PlaceHolder phEditItem = e.Item.FindControl("phEditItem") as PlaceHolder;
                    phEditItem.Visible = true;

                    HtmlGenericControl essayItemDiv = e.Item.FindControl("essayItem") as HtmlGenericControl;
                    HtmlGenericControl essayItemEditDiv = e.Item.FindControl("editItemContainer") as HtmlGenericControl;
                    essayItemDiv.Attributes.Add("editsectionname", _CurrentProfileDataSection.ProfileDataSectionName.ToString());
                    essayItemDiv.Attributes.Add("edititemname", profileDataItem.AttributeName.ToLower());
                    essayItemDiv.Attributes.Add("edittype", "inline");
                    essayItemDiv.Attributes["class"] = _ViewContainerEditCSSClass;

                    PlaceHolder phEditItemControl = e.Item.FindControl("phEditItemControl") as PlaceHolder;
                    BaseEdit editControl = EditProfileUtility.GetEditControl(Page, profileDataItem.EditControlType, profileDataItem.AttributeName);
                    if (editControl != null)
                    {
                        editControl.RenderAsDTItem = false;
                        phEditItemControl.Controls.Add(editControl);
                        editControl.EditItemContainerID = essayItemEditDiv.ClientID;
                        editControl.ViewItemContainerID = essayItemDiv.ClientID;
                        editControl.CurrentIndex = e.Item.ItemIndex;
                        editControl.LoadEditControl(_ProfileDataGroup, _CurrentProfileDataSection, profileDataItem, _CurrentJSSectionObjName, this);
                    }

                    FrameworkButton btnEditSave = e.Item.FindControl("btnEditSave") as FrameworkButton;
                    btnEditSave.OnClientClick = "editProfileManager.SaveProfile('" + _CurrentProfileDataSection.ProfileDataSectionName.ToString() + "', '" + profileDataItem.AttributeName.ToLower() + "');return false;";

                    FrameworkButton btnEditCancel = e.Item.FindControl("btnEditCancel") as FrameworkButton;
                    btnEditCancel.OnClientClick = "editProfileManager.CancelItemEdit('" + _CurrentProfileDataSection.ProfileDataSectionName.ToString() + "', '" + profileDataItem.AttributeName.ToLower() + "');return false;";
                }

                //suppress if essay is empty
                if (ViewProfileTabUtility.IsEmptyEssaySuppressEnabled(g))
                {
                    if (this.WhoseProfile == WhoseProfileEnum.Other
                        && (profileDataItem.EditDataType == EditDataTypeEnum.Essay || (ViewProfileTabUtility.IsSuppressEmptyAttributesEnabled(g) && profileDataItem.EditDataType == EditDataTypeEnum.Mask)))
                    {
                        if (fullDisplayValue == g.GetResource("FREETEXT_NOT_APPROVED", null)
                            || fullDisplayValue == GetNoAnswer(this)
                            || fullDisplayValue == "")
                        {
                            HtmlGenericControl essayItemDiv = e.Item.FindControl("essayItem") as HtmlGenericControl;
                            essayItemDiv.Visible = false;
                        }
                    }
                }

                if (profileDataItem.EditDataType == EditDataTypeEnum.Essay
                        && fullDisplayValue != g.GetResource("FREETEXT_NOT_APPROVED", null)
                        && fullDisplayValue != GetNoAnswer(this)
                        && fullDisplayValue != "")
                {
                    _hasAnswerEssay = true;
                }
                else if (profileDataItem.EditDataType != EditDataTypeEnum.Essay)
                {
                    _isEssayOnly = false;
                }
            }
        }

        #endregion

        #region Public Methods
        public override void LoadDataGroup(IMemberDTO member, XMLProfileData.ProfileDataGroup profileDataGroup)
        {
            _MemberProfile = member;
            _ProfileDataGroup = profileDataGroup;

            if (g.Member != null)
                _ViewerMemberID = g.Member.MemberID;

            if (g.Member != null && member.MemberID == g.Member.MemberID)
                this.WhoseProfile = WhoseProfileEnum.Self;

            if (profileDataGroup != null && profileDataGroup.ProfileDataSectionList != null && profileDataGroup.ProfileDataSectionList.Count > 0)
            {
                repeaterSection.DataSource = profileDataGroup.ProfileDataSectionList;
                repeaterSection.DataBind();
            }

            if (ViewProfileTabUtility.IsEmptyEssaySuppressEnabled(g))
            {
                if (this.WhoseProfile == WhoseProfileEnum.Other
                    && !_hasAnswerEssay && (_isEssayOnly || ViewProfileTabUtility.IsSuppressEmptyAttributesEnabled(g)))
                {
                    //NO ESSAYS ANSWERED
                    phNoAnsweredEssays.Visible = true;
                    bool memberProfileMale = (_MemberProfile.GetAttributeInt(g.Brand, "gendermask") & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE;
                    bool memberMale = (g.Member.GetAttributeInt(g.Brand, "gendermask") & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE;
                    string tokenUsername = _MemberProfile.GetUserName(g.Brand);
                    string tokenHimHer = memberProfileMale ? g.GetResource("TXT_HIM", this) : g.GetResource("TXT_HER", this);
                    string tokenWrite = memberProfileMale ? g.GetResource("TXT_HIM_WRITE", this) : g.GetResource("TXT_HER_WRITE", this);
                    string tokenSelf = memberProfileMale ? g.GetResource("TXT_HIM_SELF", this) : g.GetResource("TXT_HER_SELF", this);
                    string tokenTo = memberProfileMale ? g.GetResource("TXT_HIM_TO", this) : g.GetResource("TXT_HER_TO", this);
                    string tokenFrom = memberProfileMale ? g.GetResource("TXT_HIM_FROM", this) : g.GetResource("TXT_HER_FROM", this);
                    string tokenYou = memberMale ? g.GetResource("TXT_HIM_YOU", this) : g.GetResource("TXT_HER_YOU", this);
                    string tokenOn = memberProfileMale ? g.GetResource("TXT_HIM_ON", this) : g.GetResource("TXT_HER_ON", this);
                    string tokenLikeTo = memberMale ? g.GetResource("TXT_HIM_LIKETO", this) : g.GetResource("TXT_HER_LIKETO", this);
                    string tokenAble = memberMale ? g.GetResource("TXT_HIM_ABLE", this) : g.GetResource("TXT_HER_ABLE", this);
                    string tokenAsk = memberMale ? g.GetResource("TXT_HIM_ASK", this) : g.GetResource("TXT_HER_ASK", this);
                    string tokenComplete = memberProfileMale ? g.GetResource("TXT_HIM_COMPLETE", this) : g.GetResource("TXT_HER_COMPLETE", this);
                    string tokenYours = memberProfileMale ? g.GetResource("TXT_HIM_YOURS", this) : g.GetResource("TXT_HER_YOURS", this);
                    string tokenWriteNow = memberMale ? g.GetResource("TXT_HIM_WRITENOW", this) : g.GetResource("TXT_HER_WRITENOW", this);
                    string tokenPush = memberMale ? g.GetResource("TXT_HIM_PUSH", this) : g.GetResource("TXT_HER_PUSH", this);

                    litNoAnsweredEssaysText.Text = g.GetResource("TXT_NO_ANSWERED_ESSAYS", this).Replace("[username]", tokenUsername).Replace("[herhim]", tokenHimHer).Replace("[herhim_write]", tokenWrite).Replace("[herhim_self]", tokenSelf).Replace("[herhim_to]", tokenTo).Replace("[herhim_ask]", tokenAsk).Replace("[herhim_from]", tokenFrom).Replace("[herhim_complete]", tokenComplete).Replace("[herhim_yours]", tokenYours).Replace("[herhim_writenow]", tokenWriteNow).Replace("[herhim_push]", tokenPush);


                    //essay request email
                    if (ViewProfileTabUtility.IsEssayRequestEmailEnabled(g) && g.Member != null)
                    {
                        //check if viewer has already previously sent this request
                        if (!HasMemberPreviouslySentEssayRequest(g.Member, _MemberProfile.MemberID))
                        {
                            _g.AnalyticsOmniture.Evar65 = "request_essay_link_ON";
                            phEssayRequestEmail.Visible = true;

                            litNoAnsweredEssaysRequestText.Text = g.GetResource("TXT_NO_ANSWERED_REQUEST", this).Replace("[username]", tokenUsername).Replace("[herhim]", tokenHimHer).Replace("[herhim_to]", tokenTo).Replace("[herhim_you]", tokenYou).Replace("[herhim_liketo]", tokenLikeTo).Replace("[herhim_on]", tokenOn).Replace("[herhim_push]", tokenPush);
                            litNoAnsweredEssaysConfirmationText.Text = g.GetResource("TXT_NO_ANSWERED_CONFIRMATION", this).Replace("[username]", tokenUsername).Replace("[herhim]", tokenHimHer).Replace("[herhim_to]", tokenTo).Replace("[herhim_you]", tokenYou).Replace("[herhim_on]", tokenOn);

                            if (!g.List.IsHotListed(HotListCategory.Default, g.Brand.Site.Community.CommunityID, _MemberProfile.MemberID))
                            {
                                litNoAnsweredEssaysConfirmationFavoritesText.Text = g.GetResource("TXT_NO_ANSWERED_CONFIRMATION_FAVORITES", this).Replace("[herhim]", tokenHimHer).Replace("[herhim_able]", tokenAble);
                                phEssayRequestEmailFavoritesScript.Visible = true;
                            }
                        }
                        else
                        {
                            _g.AnalyticsOmniture.Evar65 = "request_essay_link_OFF";
                        }
                    }
                }
                else if (this.WhoseProfile == WhoseProfileEnum.Other
                    && _isEssayOnly)
                {
                    //essay request email
                    if (ViewProfileTabUtility.IsEssayRequestEmailEnabled(g) && g.Member != null)
                    {
                        //even though member has essay, we want to still indicate the request essay link is off for omniture
                        _g.AnalyticsOmniture.Evar65 = "request_essay_link_OFF";
                    }
                }
            }

            //TEMP: Uncomment to show the no essay section for creative
            //phNoAnsweredEssays.Visible = true;
            //phEssayRequestEmail.Visible = true;
        }

        #endregion

        #region Private methods
        private bool HasMemberPreviouslySentEssayRequest(Member.ServiceAdapters.Member viewerMember, int viewedMemberID)
        {
            bool hasSent = false;

            string memberList = viewerMember.GetAttributeText(g.Brand, "EssayRequestEmailMemberList", "");
            if (!string.IsNullOrEmpty(memberList))
            {
                string[] memberListArr = memberList.Split(new char[] { ',' });
                foreach (string s in memberListArr)
                {
                    if (s == viewedMemberID.ToString())
                    {
                        hasSent = true;
                        break;
                    }
                }
            }
            return hasSent;
        }
        #endregion

    }
}
