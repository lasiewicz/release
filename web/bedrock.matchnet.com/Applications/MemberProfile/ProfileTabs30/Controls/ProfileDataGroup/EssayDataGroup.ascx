﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EssayDataGroup.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileDataGroup.EssayDataGroup" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mlft" TagName="MemberLikeAnswerClientModule" Src="~/Applications/MemberLike/MemberLikeAnswerClientModule.ascx" %>

<asp:Repeater ID="repeaterSection" runat="server" OnItemDataBound="repeaterSection_ItemDataBound">
    <ItemTemplate>
        <asp:PlaceHolder ID="phTitle" runat="server" Visible="false">
            <div><asp:Literal ID="literalTitle" runat="server"></asp:Literal></div>
        </asp:PlaceHolder>
        <asp:Repeater ID="repeaterItem" runat="server" OnItemDataBound="repeaterItem_ItemDataBound">
            <ItemTemplate>
                <div id="essayItem" class="prof-edit-region prof-edit-inline" runat="server">
				    <asp:PlaceHolder ID="phEditIcon" runat="server" Visible="false">
                    <div class="prof-edit-button">
                        <asp:Literal ID="litEditIcon" runat="server"></asp:Literal>
                    </div>
                    </asp:PlaceHolder>
                    <h3><asp:Literal ID="literalName" runat="server"></asp:Literal></h3>
                    <p id="pEssayValue" runat="server" class="essay-value"><asp:Literal ID="literalValue" runat="server"></asp:Literal></p>
                
                    <div id ="notification" class="notification" runat="server" visible="false">
                        <mn:Image ID="imgAnsweredNotification" runat="server" FileName="icon-page-message.gif" />
                        <mn2:FrameworkLiteral ID="pendingLiteral" ResourceConstant="TXT_PENDING_APPROVAL" runat="server"/>
                    </div>
                
                </div>
                
                <asp:Panel ID="pLikeAndComment" runat="server" Visible="false" CssClass="padding-light">
                    <div class="like-container clearfix">
                        <div class="like-module float-outside">
                            <% if (IsLoggedIn && IsFreeTextCommentEnabled && IsPayingMember && !IsViewingOwnProfile)
                               { %><span class="comment-label"><label class="link-label"><mn:txt runat="server" id="txtComment" resourceconstant="TXT_COMMENT" expandimagetokens="true" /></label> &#x25AA; </span>
                            <% }
                               else if (IsLoggedIn && IsFreeTextCommentEnabled && !IsViewingOwnProfile)
                               { %><asp:HyperLink id="commentLink" runat="server"><mn:txt runat="server" id="txtCommentSubLink" resourceconstant="TXT_COMMENT" expandimagetokens="true" /></asp:HyperLink> &#x25AA; 


                            <% } %>
                            <% if (IsLoggedIn && IsFreeTextMemberLikeEnabled && !IsViewingOwnProfile)
                               { %><span class="like" title="<%=g.GetResource("TXT_LIKE", this) %>" onclick="spark.qanda.LikeAnswerFreeText(this,<%=MemberId%>,<%=ProfileMemberID %>, <asp:Literal ID="hashCodeInt" runat="server"></asp:Literal>, '<asp:Literal ID="sectionAttribute" runat="server"></asp:Literal>', {QuestionId:<%=ProfileMemberID%>,Event:'event51,event52',Overwrite:true,Props:[{Num:38,Value:'<%=ProfileMemberID%>'}]})"><span class="spr s-icon-like"><span><mn:txt runat="server" id="txtLike" resourceconstant="TXT_LIKE" expandimagetokens="true" /></span></span>
                                    <span class="clickable"><mn:txt runat="server" id="txtLike2" resourceconstant="TXT_LIKE" expandimagetokens="true" /></span>
                                    <asp:Literal ID="likeTxt" visible="false" runat="server"/>
                               </span>
                            <% } %>
                            
                            <% if (IsLoggedIn && IsViewingOwnProfile)
                               { %>
                            <mlft:MemberLikeAnswerClientModule ID="memberLikeClientModuleFreeText" runat="server" Enabled="true" Visible="false"/>
                            <% } %>
                        </div>
                        <% if (IsLoggedIn && !IsViewingOwnProfile)
                               { %>
                        <div class="comment-block hide">
                            <h2><mn:txt runat="server" id="txtCommentHeader" resourceconstant="TXT_COMMENT_HEADER" expandimagetokens="true" /></h2>
                            <span class="close"><mn:txt runat="server" id="txtCommentCloseX" resourceconstant="TXT_COMMENT_CLOSE" epandimagetokens="true" /></span>
	                        <textarea id="comment-<%=MemberId%>-<%=ProfileMemberID%>-<asp:Literal ID="literalAttributeName" runat="server"></asp:Literal>" rows="2" class="watermark"></textarea>
	                        <div class="text-outside">
                                <input type="submit" id="id-<%=MemberId%>-<%=ProfileMemberID%>-<asp:Literal ID="literalAttributeName2" runat="server"></asp:Literal>" name="sendComment" class="btn secondary freeTextCommentSubmit" value="<%=g.GetResource("TXT_COMMENT_SUBMIT",this) %>" />
	                        </div>
	                    </div>
                        <% } %>
                    </div>
                </asp:Panel>
                <asp:PlaceHolder ID="phEditItem" runat="server" Visible="false">
                    <div id="editItemContainer" runat="server" class="hide">
                        <asp:PlaceHolder ID="phEditItemControl" runat="server"></asp:PlaceHolder>
                        <%--Submit--%>
                        <mn2:FrameworkButton ID="btnEditSave" runat="server" CssClass="btn primary button-show edit-submit" ResourceConstant="BTN_EDIT_SAVE" UseSubmitBehavior="false" />
                        <%--Cancel--%>
                        <mn2:FrameworkButton ID="btnEditCancel" runat="server" CssClass="btn secondary button-show edit-cancel" ResourceConstant="BTN_EDIT_CANCEL" UseSubmitBehavior="false" />
                    </div>
                </asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>
    </ItemTemplate>
</asp:Repeater>

<asp:PlaceHolder ID="phNoAnsweredEssays" runat="server" Visible="false">
    <div class="noanswer a-module">
        <h2>
            <asp:Literal ID="litNoAnsweredEssaysText" runat="server"></asp:Literal>
        </h2>
    <asp:PlaceHolder ID="phEssayRequestEmail" runat="server" Visible="false">
    <%--Profile Essay Request Email--%>
        <p>
            <asp:Literal ID="litNoAnsweredEssaysRequestText" runat="server"></asp:Literal>
        </p>
        <div class="confirmation hide">
            <p>
                <asp:Literal ID="litNoAnsweredEssaysConfirmationText" runat="server"></asp:Literal>
                <asp:Literal ID="litNoAnsweredEssaysConfirmationFavoritesText" runat="server"></asp:Literal>
            </p>
        </div>
        <script type="text/javascript">
            var essayEmailRequestInProgress = false;
            (function () {
                var trigger = $j('.send-email', '#tab-Essays'),
                confirmationText = $j('.confirmation', '#tab-Essays');

                trigger.click(function (e) {
                    e.preventDefault();
                    $j(this).parent().hide();
                    confirmationText.show();

                    //essayEmailRequestInProgress = true;
                    if (!essayEmailRequestInProgress){
                        $j.ajax({
                            type: "POST",
                            url: "/Applications/API/Member.asmx/ProcessNoEssayEmailRequest",
                            data: "{profileMemberID:<%=_MemberProfile.MemberID%>,viewerMemberID:<%=_ViewerMemberID%>}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                essayEmailRequestInProgress = true;
                            },
                            complete: function () {
                                essayEmailRequestInProgress = false;
                            },
                            success: function (result) {
                                var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                                //console.log(updateResult);
                                if (updateResult.Status != 2) {
                                    //error
                                    alert(updateResult.StatusMessage);
                                }
                                else {
                                    //success
                                    //update omniture
                                    if (s != null) {
                                        PopulateS(true); //clear existing values in omniture "s" object
                                        var existingPageName = s.pageName;
                                        s.pageName = "request_essay_link_Confirmation";
                                        s.events = "event31";
                                        s.prop23 = '<%=_ViewerMemberID%>';
                                        s.prop35 = '<%=_MemberProfile.MemberID%>';
                                        s.t(); //send omniture updated values as page load
                                        s.pageName = existingPageName;
                                    }
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    }
                });

            })();
        </script>

        <asp:PlaceHolder ID="phEssayRequestEmailFavoritesScript" runat="server" Visible="false">
        <script type="text/javascript">
            var essayEmailRequestFavoritesInProgress = false;
            (function () {
                var triggerFavs = $j('#lnkNoAnsweredConfirmationFavorites', '#tab-Essays');

                triggerFavs.click(function (e) {
                    e.preventDefault();

                    if (!essayEmailRequestFavoritesInProgress){
                        $j.ajax({
                            type: "POST",
                            url: "/Applications/API/Favorites.asmx/AddFavorite",
                            data: "{memberID:<%=_ViewerMemberID%>,favoriteMemberID:<%=_MemberProfile.MemberID%>}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                essayEmailRequestFavoritesInProgress = true;
                            },
                            complete: function () {
                                essayEmailRequestFavoritesInProgress = false;
                            },
                            success: function (result) {
                                var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                                //console.log(updateResult);
                                var rc = "rc=YOUR_LIST_HAS_BEEN_UPDATED_ADD";
                                window.location.href = window.location.href + "&" + rc;
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    }
                });

            })();
        </script>
        </asp:PlaceHolder>
    
    </asp:PlaceHolder>
    </div>
</asp:PlaceHolder>