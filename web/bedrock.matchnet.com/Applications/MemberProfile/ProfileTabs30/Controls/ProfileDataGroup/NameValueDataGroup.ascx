﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NameValueDataGroup.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileDataGroup.NameValueDataGroup" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NameValueDataSectionSingle" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/NameValueDataSectionSingle.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NameValueDataSectionDouble" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/NameValueDataSectionDouble.ascx" %>

<asp:PlaceHolder ID="phOneColumn" runat="server" Visible="false">
    <div class="name-value one-column">

    <asp:Repeater ID="repeaterSectionOneColumn" runat="server" OnItemDataBound="repeaterSection_ItemDataBound">
        <ItemTemplate>
            <uc1:NameValueDataSectionSingle ID="nameValueDataSection" runat="server" />
        </ItemTemplate>
    </asp:Repeater>
    
    </div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phTwoColumn" runat="server" Visible="false">
    <div class="name-value two-column">

    <asp:Repeater ID="repeaterSectionTwoColumn" runat="server" OnItemDataBound="repeaterSection_ItemDataBound">
        <ItemTemplate>
            <uc1:NameValueDataSectionDouble ID="nameValueDataSection" runat="server" />
        </ItemTemplate>
    </asp:Repeater>
    
    </div>
</asp:PlaceHolder>