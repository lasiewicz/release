﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileData
{
    /// <summary>
    /// This class represents all profile data groups for a site.
    /// It is deserialized from xml defined in the ProfileData_SiteID.xml files.
    /// </summary>
    [Serializable]
    public class ProfileDataGroups
    {
        [XmlElement("DataGroup")]
        public List<ProfileDataGroup> ProfileDataGroupList { get; set; }

        #region Public Methods
        public ProfileDataGroup GetProfileDataGroup(ProfileDataGroupEnum profileDataGroupType)
        {
            ProfileDataGroup dataGroup = null;
            if (ProfileDataGroupList != null)
            {
                foreach (ProfileDataGroup pdg in ProfileDataGroupList)
                {
                    if (pdg.ProfileDataGroupType == profileDataGroupType)
                    {
                        return pdg;
                    }
                }
            }

            return dataGroup;
        }
        #endregion
    }
}
