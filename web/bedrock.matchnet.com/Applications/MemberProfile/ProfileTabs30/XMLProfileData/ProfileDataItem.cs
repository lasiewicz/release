﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileData
{
    /// <summary>
    /// This class represents a single item within a section of a data group.
    /// It is deserialized from a subset of xml defined in the ProfileData_SiteID.xml files.
    /// </summary>
    [Serializable]
    public class ProfileDataItem
    {
        [XmlAttribute()]
        public string AttributeName { get; set; }
        [XmlAttribute()]
        public string ResourceConstant { get; set; }
        [XmlAttribute()]
        public string EditResourceConstant { get; set; }
        [XmlAttribute()]
        public string EditNoteResourceConstant { get; set; }
        [XmlAttribute()]
        public EditControlTypeEnum EditControlType { get; set; }
        [XmlAttribute()]
        public EditDataTypeEnum EditDataType { get; set; }
        [XmlAttribute()]
        public string EditValues { get; set; }
        [XmlAttribute()]
        public string EditResources { get; set; }
        [XmlAttribute()]
        public bool IsRequired { get; set; }
        [XmlAttribute()]
        public int MinLength { get; set; }
        [XmlAttribute()]
        public EditControlPropertyEnum EditControlProperty { get; set; }
        [XmlAttribute()]
        public bool CommentsEnabled { get; set; }
        [XmlAttribute()]
        public bool KeywordSearchable { get; set; }
    }
}
