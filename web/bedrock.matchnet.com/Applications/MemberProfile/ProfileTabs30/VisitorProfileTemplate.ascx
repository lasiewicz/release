﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VisitorProfileTemplate.ascx.cs"
    Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.VisitorProfileTemplate" %>
<%@ Register TagPrefix="uc1" TagName="LastLoginDate" Src="/Framework/Ui/BasicElements/LastLoginDate.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="LastUpdateDate" Src="/Framework/Ui/BasicElements/LastUpdateDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NameValueDataGroup" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/NameValueDataGroup.ascx" %>
<%@ Register TagPrefix="uc1" TagName="EssayDataGroup" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/EssayDataGroup.ascx" %>
<%@ Register TagPrefix="mn" TagName="M2MCommunication" Src="/Applications/MemberProfile/ProfileTabs30/Controls/M2MCommunication.ascx" %>
<%@ Register TagPrefix="mn" TagName="PhotoSlider" Src="/Applications/MemberProfile/ProfileTabs30/Controls/PhotoSlider.ascx" %>
<%@ Register TagPrefix="mn" TagName="BasicInfo" Src="/Applications/MemberProfile/ProfileTabs30/Controls/BasicInfo.ascx" %>

<script type="text/javascript">
    if (s != null) {
        spark_profile30_OmnitureCurrentPageName = s.pageName;
    }
</script>

<div class="clearfix">
    <%--Photo area--%>
    <mn:PhotoSlider ID="PhotoSlider1" runat="server" />
    
    <!--Profile Data-->
    <div id="profile30Important" class="profile30-overview clearfix">
        
        <%--Basic Info--%>
        <mn:BasicInfo ID="basicInfo1" runat="server" />
        
        <%--Timestamps --%>
        <ul class="timestamp">
            <li><uc1:LastLoginDate ID="lastLoginDate" runat="server"></uc1:LastLoginDate></li>
            <li><uc1:LastUpdateDate ID="lastUpdateDate" runat="server"></uc1:LastUpdateDate></li>
        </ul>
        
        <!--DEALBREAKER INFO-->
        <div class="profile30-details clearfix">
            <uc1:NameValueDataGroup ID="nvDataGroupDealBreakerInfo" runat="server" DisplayMode="TwoColumn" />
        </div>
        
        <!--M2MCommunication-->
        <mn:M2MCommunication ID="m2mCommunication1" runat="server" />
    </div>
</div>
<!--About Me Essay-->
<div class="prof-essays-visitor">
    <uc1:EssayDataGroup ID="essayDataGroupAboutMe" runat="server" />
</div>

<%--Join Now--%>
<div class="action-email clearfix">
    <h3><mn2:FrameworkLiteral ID="literalLikeWhatYouSee" runat="server" ResourceConstant="TXT_LIKE_WHAT_YOU_SEE" /></h3>
    <asp:HyperLink ID="lnkJoinNowBottom" runat="server" CssClass="btn link-primary large">
        <mn2:FrameworkLiteral ID="literalJoinNowBottom" runat="server" ResourceConstant="TXT_JOIN_NOW_AND_BROWSE" />
    </asp:HyperLink>
</div>
