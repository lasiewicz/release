﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.MemberProfile
{
    public class BaseProfileViewControl : FrameworkControl
    {
        protected IMemberDTO _MemberProfile;
        protected Matchnet.Search.ValueObjects.SearchPreferenceCollection _SearchPreferences;
        #region MemberProfile Attribute
        /// <summary>
        /// MemberProfile
        /// </summary>
        public IMemberDTO MemberProfile
        {
            get { return _MemberProfile; }
            set { _MemberProfile = value; }
        }
        public Matchnet.Search.ValueObjects.SearchPreferenceCollection SearchPreferences
        {
            get
            {
                if ((_SearchPreferences == null) && (_MemberProfile != null))
                {
                    _SearchPreferences = Matchnet.Search.ServiceAdapters.SearchPreferencesSA.Instance.GetSearchPreferences(_MemberProfile.MemberID, g.Brand.Site.Community.CommunityID);
                }
                return _SearchPreferences;
            }
            set { _SearchPreferences = value; }
        }
        #endregion MemberProfile Attribute

        #region IsEditEnabled Attribute
        public bool IsEditEnabled
        {
            get
            {
                if (_MemberProfile == null)
                {
                    return false;
                }
                return g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) == _MemberProfile.MemberID;
            }
        }
        #endregion

        protected string getDateDisplay(DateTime pDate)
        {
            return Matchnet.Web.Applications.MemberProfile.ProfileTabs30.ProfileUtility.GetDateDisplay(pDate, g);
        }

        protected string getFormattedEssayText(string pAttributeName)
        {
            return ProfileDisplayHelper.GetFormattedEssayText(_MemberProfile, g, pAttributeName);
        }

        protected string getMaskContent(string pAttributeName)
        {
            return ProfileDisplayHelper.GetMaskContent(_MemberProfile, g, pAttributeName);
        }

        protected string getOptionValue(string pAttributeName)
        {
            return ProfileDisplayHelper.GetOptionValue(_MemberProfile, g, pAttributeName, pAttributeName);
        }

        protected string getOptionValue(string pAttributeName, string pOptionName)
        {
            return ProfileDisplayHelper.GetOptionValue(_MemberProfile, g, pAttributeName, pOptionName);
        }

        protected bool IsNotAmericanSingles()
        {
            return ((int)WebConstants.COMMUNITY_ID.Spark != g.Brand.Site.Community.CommunityID);
        }

        protected bool IsNotCupidCoIl()
        {
            return ((int)WebConstants.COMMUNITY_ID.Cupid != g.Brand.Site.Community.CommunityID);
        }

        protected bool IsNotJDate()
        {
            // encompasses JD and JDIL communities
            return ((int)WebConstants.COMMUNITY_ID.JDate != g.Brand.Site.Community.CommunityID);
        }

        protected bool isValidContext()
        {
            if (_MemberProfile != null)
            {
                return true;
            }
            else
            {
                if (this.Visible)
                {
                    g.Notification.AddError("PAGE_MEMBER_PROFILE_TEMPORARILY_UNAVAILABLE");
                }
                return false;
            }
        }

        protected void configureDisplay(Table pTable)
        {
            // interate each visible row in the Interests Table setting the correct CssClass on each.
            bool setCssClass = true;
            foreach (TableRow row in pTable.Rows)
            {
                if (row.Visible)
                {
                    if (setCssClass)
                    {
                        row.CssClass = "ViewProfileAlternateRow";
                        setCssClass = false;
                    }
                    else
                    {
                        setCssClass = true;
                    }
                }
            }
        }

        #region Protected Helper Methods for attribute retrieval
        /// <summary>
        /// This method has been copied from PersonalityView.ascx.  
        /// Even though the difference for Cupid shouldn't be necessary here, it is 
        /// still here to preserve its importance when all community conditional checks are removed.
        /// </summary>
        /// <returns>The string value of the personality trait attribute.</returns>
        protected string getPersonalityTraits()
        {
            if ((int)WebConstants.COMMUNITY_ID.Cupid == g.Brand.Site.Community.CommunityID)
            {
                return getMaskContent("IsraeliPersonalityTrait");
            }
            else
            {
                return getMaskContent("PersonalityTrait");
            }
        }

        /// <summary>
        /// Helper method to return the appropriate string text depending on one of three user selections for "more children".
        /// Factors in the existence of any current children to display alternate yes/no answers.
        /// </summary>
        /// <returns>A string representing the user's repsonse.</returns>
        protected string getMoreChildrenFlag()
        {
            return getMoreChildrenFlag(this);
        }

        protected string getMoreChildrenFlag(Matchnet.Web.Framework.FrameworkControl resourceControl)
        {
            return getMoreChildrenFlag(resourceControl, false);
        }

        protected string getMoreChildrenFlag(Matchnet.Web.Framework.FrameworkControl resourceControl, bool ignoreMissingCustody)
        {
            int currentChildren = MemberProfile.GetAttributeInt(g.Brand, "Custody");

            if (ignoreMissingCustody && currentChildren == Constants.NULL_INT)
            {
                currentChildren = 2;
            }

            // only precede if "Custody" has been set
            if (currentChildren != Constants.NULL_INT)
            {
                bool hasChildren = false;
                if (currentChildren != 2)	// 2 is the default value for no children
                {
                    hasChildren = true;
                }

                // Retrieve the int representation of the relocation attribute
                int MoreChildrenFlag = MemberProfile.GetAttributeInt(g.Brand, "MoreChildrenFlag");

                // if the user wants children
                if (MoreChildrenFlag == 1)	// Yes	
                {
                    // if the user already has children
                    if (hasChildren)
                    {
                        return g.GetResource("MORECHILDREN_YES_MORE", resourceControl);
                    }
                    // if the user does not have children
                    else
                    {
                        return g.GetResource("MORECHILDREN_YES", resourceControl);
                    }
                }
                // if the user does not want children
                else if (MoreChildrenFlag == 0)	// No
                {
                    // if the user already has children
                    if (hasChildren)
                    {
                        return g.GetResource("MORECHILDREN_NO_MORE", resourceControl);
                    }
                    // if the user does not have children
                    else
                    {
                        return g.GetResource("MORECHILDREN_NO", resourceControl);
                    }
                }
                else if (MoreChildrenFlag == 2) 	// Not sure
                {
                    return g.GetResource("MORECHILDREN_UNSURE", resourceControl);
                }
                else
                {
                    return String.Empty;
                }
            }
            // if "Custody" is not set then don't return anything
            else
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Helper method to return the appropriate string text depending on one of three user selections for "willing to relocate"
        /// </summary>
        /// <returns>A string representing the user's repsonse.</returns>
        protected string getWillingToRelocateDisplay()
        {
            return getWillingToRelocateDisplay(this);
        }

        protected string getWillingToRelocateDisplay(Matchnet.Web.Framework.FrameworkControl resourceControl)
        {
            // Retrieve the int representation of the relocation attribute
            int RelocateValue = MemberProfile.GetAttributeInt(g.Brand, "RelocateFlag");

            if (RelocateValue == 1)	// Yes	
            {
                return g.GetResource("RELOCATE_YES", resourceControl);
            }
            else if (RelocateValue == 2)	// No  (this accommodates values set by both radio btn and list ctr binding methods) 
            {
                return g.GetResource("RELOCATE_NO", resourceControl);
            }
            else if (RelocateValue == 4)		// Not sure
            {
                return g.GetResource("RELOCATE_UNSURE", resourceControl);
            }
            else
            {
                return String.Empty;
            }
        }

        protected string getMaritalStatus()
        {
            if (((int)Matchnet.Web.Framework.WebConstants.COMMUNITY_ID.Glimpse == g.Brand.Site.Community.CommunityID)
                 || ((int)Matchnet.Web.Framework.WebConstants.COMMUNITY_ID.College == g.Brand.Site.Community.CommunityID))
            {	// CollegeLuv & Glimpse display RelationshipStatus in place of MaritalStatus
                return getOptionValue("RelationshipStatus");
            }
            else
            {
                return getOptionValue("MaritalStatus");
            }
        }

        protected string getReligion()
        {
            // encompasses JD JDIL and CU
            if (((int)WebConstants.COMMUNITY_ID.JDate == g.Brand.Site.Community.CommunityID)
                 || ((int)WebConstants.COMMUNITY_ID.Cupid == g.Brand.Site.Community.CommunityID)
                 || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JewishMingle)
            {
                return getOptionValue("JdateReligion");
            }
            else
            {
                return getOptionValue("Religion");
            }
        }

        protected string getEthnicity()
        {
            // encompasses JD and JDIL
            if ((g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate) || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JewishMingle)
            {
                return getOptionValue("JDateEthnicity");
            }
            else
            {
                return getOptionValue("Ethnicity");
            }
        }

        protected string getDesiredAgeRange()
        {
            return getDesiredAgeRange(this);
        }

        protected string getDesiredAgeRange(Matchnet.Web.Framework.FrameworkControl resourceControl)
        {
            string sMinAge = String.Empty;
            string sMaxAge = String.Empty;

            int minAge = MemberProfile.GetAttributeInt(g.Brand, "DesiredMinAge");
            int maxAge = MemberProfile.GetAttributeInt(g.Brand, "DesiredMaxAge");

            if (minAge != Constants.NULL_INT)
            {
                if (maxAge != Constants.NULL_INT)
                {
                    return minAge.ToString() + " " + g.GetResource("TO_", resourceControl) + " " + maxAge.ToString();
                }
            }

            return null;
        }

        protected string getDesiredReligious()
        {
            //TT# 7162
            if (g.Brand.Site.Community.CommunityID != (int)WebConstants.COMMUNITY_ID.Cupid && g.Brand.Site.Community.CommunityID != (int)WebConstants.COMMUNITY_ID.AmericanSingles)
            {
                return getMaskContent("DesiredJDateReligion");
            }
            else
            {
                return getMaskContent("DesiredReligious");
            }
        }

        protected string getSchool()
        {
            int schoolID = MemberProfile.GetAttributeInt(g.Brand, "SchoolID", 0);
            string schoolName = string.Empty;
            if (schoolID > 0)
            {
                // schoolName = Matchnet.Lib.Region.RegionLanguage.GetSchoolName(schoolID);
                return RegionSA.Instance.RetrieveSchoolName(schoolID).SchoolName;
            }
            return schoolName;
        }

        protected string getBirthCountry()
        {
            int birthCountryRegionID = MemberProfile.GetAttributeInt(g.Brand, "BirthCountryRegionID", 0);
            string birthCountry = string.Empty;
            if (birthCountryRegionID > 0)
            {
                birthCountry = RegionSA.Instance.RetrieveRegionByID(birthCountryRegionID, g.Brand.Site.LanguageID).Description;
            }
            return birthCountry;
        }


        #endregion


        /// <summary>
        /// Gets the attribute value based on the specified name
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        protected string GetMemberAttributeValue(string attributeName, Matchnet.Web.Framework.FrameworkControl resourceControl)
        {
            return GetMemberAttributeValue(attributeName, resourceControl, true);
        }

        protected virtual string GetMemberAttributeValue(string attributeName, Matchnet.Web.Framework.FrameworkControl resourceControl, bool noBlankText)
        {
            string attributeValue = String.Empty;

            switch (attributeName.ToLower())
            {
                case "aboutme":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "height":
                    attributeValue = ProfileDisplayHelper.GetHeightDisplay(this.MemberProfile, this.g);
                    break;
                case "weight":
                    attributeValue = ProfileDisplayHelper.GetWeightDisplay(this.MemberProfile, this.g);
                    break;
                case "haircolor":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "eyecolor":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "bodytype":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "relocateflag":
                    attributeValue = getWillingToRelocateDisplay(resourceControl);
                    break;
                case "maritalstatus":
                    attributeValue = getMaritalStatus();
                    break;
                case "custody":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "morechildrenflag":
                    attributeValue = getMoreChildrenFlag(resourceControl);
                    break;
                case "smokinghabits":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "drinkinghabits":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "activitylevel":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "grewupin":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "ethnicity":
                    attributeValue = getEthnicity();
                    break;
                case "languagemask":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "religion":
                    attributeValue = getReligion();
                    break;
                case "studiesemphasis":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "educationlevel":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "industrytype":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "occupationdescription":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "incomelevel":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "personalitytrait":
                    attributeValue = getPersonalityTraits();
                    break;
                case "leisureactivity":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "entertainmentlocation":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "physicalactivity":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "cuisine":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "music":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "reading":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "perfectfirstdateessay":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "idealrelationshipessay":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "learnfromthepastessay":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "childrencount":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "keepkosher":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "synagogueattendance":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "zodiac":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "politicalorientation":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "pets":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "perfectmatchessay":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
                case "desiredminage":
                    attributeValue = getDesiredAgeRange(resourceControl);
                    break;
                case "desiredmaritalstatus":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "desiredreligious":
                    attributeValue = getDesiredReligious();
                    break;
                case "desirededucationlevel":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "desireddrinkinghabits":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "desiredsmokinghabits":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "desiredmorechildrenflag":
                    // Retrieve the int representation of the relocation attribute
                    int MoreChildrenFlag = MemberProfile.GetAttributeInt(g.Brand, "DesiredMoreChildrenFlag");

                    // if the user wants children
                    if (MoreChildrenFlag == 1)	// Yes	
                    {
                        attributeValue = g.GetResource("MORECHILDREN_YES", resourceControl);
                    }
                    // if the user does not want children
                    else if (MoreChildrenFlag == 0)	// No
                    {
                        attributeValue = g.GetResource("MORECHILDREN_NO", resourceControl);
                    }
                    else if (MoreChildrenFlag == 2) 	// Not sure
                    {
                        attributeValue = g.GetResource("MORECHILDREN_UNSURE", resourceControl);
                    }
                    else
                    {
                        attributeValue = String.Empty;
                    }
                    break;
                case "desiredchildrencount":
                    attributeValue = getMaskContent(attributeName);
                    break;
                case "majortype":
                    attributeValue = getOptionValue(attributeName);
                    break;
                case "schoolid":
                    attributeValue = getSchool();
                    break;
                case "memberid":
                    attributeValue = _MemberProfile.MemberID.ToString();
                    break;
                case "age":
                    attributeValue = GetFormattedAge(resourceControl);
                    break;
                case "location":
                    attributeValue = ProfileDisplayHelper.GetRegionDisplay(this.MemberProfile, g);
                    break;
                case "gendermask":
                    attributeValue = ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(this.MemberProfile, this.g);
                    break;
                case "lookingfor":
                    attributeValue = g.GetResource("FOR_A", resourceControl) + " " + getMaskContent("RelationshipMask");
                    break;
                case "jdatereligion":
                    if (g.Brand.Site.SiteID == (int)Matchnet.Web.Framework.WebConstants.SITE_ID.JDate)
                    {
                        attributeValue = g.GetResource("MY_RELIGION", resourceControl) +
                        ProfileDisplayHelper.GetOptionValue(this.MemberProfile, _g, "JDateReligion", "JDateReligion");
                    }
                    break;
                case "zodiacsign":
                    attributeValue = AstrologyHelper.GetZodiacSign(g, _MemberProfile.GetAttributeDate(g.Brand, "birthdate"), true);
                    break;
                case "username":
                    attributeValue = this.MemberProfile.GetUserName(g.Brand);
                    break;
                case "emailaddress":
                case "sitefirstname":
                case "sitelastname":
                    attributeValue = this.MemberProfile.GetAttributeText(g.Brand, attributeName);
                    break;
                case "LifeAndAbmitionEssay":
                case "HistoryOfMyLifeEssay":
                case "OnOurFirstDateRemindMeToEssay":
                case "ThingsCantLiveWithoutEssay":
                case "FavoriteBooksMoviesEtcEssay":
                case "CoolestPlacesVisitedEssay":
                case "ForFunILikeToEssay":
                case "OnFriSatITypicallyEssay":
                case "MessageMeIfYouEssay":
                    attributeValue = getFormattedEssayText(attributeName);
                    break;
            }

            if (String.IsNullOrEmpty(attributeValue) && noBlankText)
            {
                attributeValue = GetNoAnswer(resourceControl);
                if (String.IsNullOrEmpty(attributeValue) || attributeValue.ToLower().IndexOf("resource") >= 0)
                    attributeValue = "&nbsp;"; //output non-breaking space for correct alignment with css

            }

            return attributeValue;
        }

        public virtual string GetNoAnswer(Matchnet.Web.Framework.FrameworkControl resourceControl)
        {
            string returnValue = g.GetResource("NOT_ANSWERED_YET", resourceControl);
            if (returnValue.ToLower().IndexOf("resource") >= 0)
                returnValue = "";

            return returnValue;
        }

        public virtual string GetFormattedAge(Matchnet.Web.Framework.FrameworkControl resourceControl)
        {
            try
            {
                string age = Matchnet.Web.Framework.FrameworkGlobals.GetAge(this.MemberProfile, g.Brand).ToString();
                if (String.IsNullOrEmpty(age))
                {
                    age = "&nbsp;";
                }
                else
                {
                    if (Matchnet.Web.Framework.FrameworkGlobals.isHebrewSite(g.Brand))
                    {
                        age = g.GetResource("TXT_YEARS_OLD", resourceControl) + " " + age;
                    }
                    else
                    {
                        age += " " + g.GetResource("TXT_YEARS_OLD", resourceControl);
                    }
                }

                return age;

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return "";
            }

        }

        public virtual string GetFormattedAgeLocation(Matchnet.Web.Framework.FrameworkControl resourceControl)
        {
            try
            {
                StringBuilder age = new StringBuilder();
                age.Append(Matchnet.Web.Framework.FrameworkGlobals.GetAge(this.MemberProfile, g.Brand).ToString());
                if (String.IsNullOrEmpty(age.ToString()))
                {
                    age.Append(ProfileDisplayHelper.GetRegionDisplay(this.MemberProfile, g));
                }
                else
                {
                    if (Matchnet.Web.Framework.FrameworkGlobals.isHebrewSite(g.Brand))
                    {
                        age.Insert(0, g.GetResource("TXT_YEARS_OLD", resourceControl) + " ");
                    }
                    else
                    {
                        age.Append(" " + g.GetResource("TXT_YEARS_OLD", resourceControl));
                    }

                    age.Append(" " + g.GetResource("TXT_FROM", resourceControl));
                    age.Append((g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew ? string.Empty : " ") + ProfileDisplayHelper.GetRegionDisplay(this.MemberProfile, g));
                }

                return age.ToString();

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return "";
            }

        }


        public ContextGlobal GetContextGlobal()
        {
            return this.g;
        }

        public System.Web.UI.Page GetPage()
        {
            return this.Page;
        }

    }
}