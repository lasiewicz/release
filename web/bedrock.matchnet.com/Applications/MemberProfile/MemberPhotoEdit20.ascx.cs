﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.PageElements;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Interfaces;
using System.Text.RegularExpressions;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.Web.Applications.MemberLike;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.MemberProfile
{
    public partial class MemberPhotoEdit20 : FrameworkControl
    {
        private int _count = 0;
        private int _memberid = 0;
        private List<Photo> _photos = null;
        private int _maxPhotos = Constants.NULL_INT;
        private int _memberPhotosCount = Constants.NULL_INT;
        private string _uploadFileClientIds = "";
        private HtmlInputHidden txtUploadIds;
        private int _browseRow = Constants.NULL_INT;
        private string[] _tabs = new string[] { "basic", "lifestyle", "interests", "relationship" };
        private int _nophotosCount = 0;
        private string FORMAT_TAB_CSS = "pic-active pos-{0} pic-primary tab-{1}";
        private string FORMAT_NORMAL_CSS = "pic-active pos-{0} pic-normal";
        private string FORMAT_BLOCKED_CSS = "pic-empty pos-{0} pic-normal blocked nodrag nodrop";
        private List<string> _errors;
        private int _photoUploads = 0;
        private bool _uploadFromFacebookEnabled = false;

        public int MaxPhotoCount
        {
            get
            {
                if (_maxPhotos < 0)
                {
                    IPhotoRules photoRules = new PhotoRulesManager();
                    _maxPhotos = photoRules.GetMaxPhotoCount(g.Brand);
                }
                return _maxPhotos;
            }

        }

        public int CurrentItem
        {

            get { return _count; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _uploadFromFacebookEnabled = Convert.ToBoolean(RuntimeSettings.Instance.GetSettingFromSingleton("ENABLE_UPLOAD_PHOTOS_FROM_FACEBOOK",
                g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));

            if (_uploadFromFacebookEnabled)
            {
                g.LayoutTemplateBase.LoadFacebookScript();
            }

            Populate();

            if (g.BreadCrumbTrailHeader != null)
            {
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TTL_MANAGE_YOUR_PHOTOS", this),
                                                        g.AppPage.App.DefaultPagePath);

            }
            if (g.BreadCrumbTrailFooter != null)
            {
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TTL_MANAGE_YOUR_PHOTOS", this),
                                                        g.AppPage.App.DefaultPagePath);
            }
        }


        public void Populate()
        {
            try
            {
                _count = 0;
                _nophotosCount = 0;
                if (_photos != null)
                    _photos.Clear();

                MemberPhotoUpdateManager updateManager = new MemberPhotoUpdateManager(g.Member, g.Brand, true);
                _photos = updateManager.GetPaddedToMaxPhotos();
                _memberPhotosCount = updateManager.GetPhotos() == null ? 0 : updateManager.GetPhotos().Count;

                if (!String.IsNullOrEmpty(Request["onbrowse"]))
                {
                    _browseRow = Conversion.CInt(Request["sourcerow"]);

                }

                Literal litDeleteMsgResource = (Literal)this.FindControl("litDeleteMessage");
                if (litDeleteMsgResource != null)
                    litDeleteMsgResource.Text = g.GetResource("TXT_DELETE_MSG", this);

                Literal litSavingMsgResource = (Literal)this.FindControl("litSavingMessage");
                if (litSavingMsgResource != null)
                    litSavingMsgResource.Text = g.GetResource("TXT_SAVING_MSG", null, true, this);

                Literal litLoadingMessage = (Literal)this.FindControl("litLoadingMessage");
                if (litLoadingMessage != null)
                    litLoadingMessage.Text = g.GetResource("TXT_LOADING_MSG", null, true, this);

                Literal litDefaultCaption = (Literal)this.FindControl("litDefaultCaption");

                if (litDefaultCaption != null)
                    litDefaultCaption.Text = g.GetResource("TXT_CAPTION_DEFAULT", this);

                Literal litConfirmationMsg = (Literal)this.FindControl("litConfirmationMsg");

                if (litConfirmationMsg != null)
                    litConfirmationMsg.Text = g.GetResource("TXT_CONFIRMATION_MSG", this);


                Literal litPrefixHeroTextAreaMsg = (Literal)this.FindControl("litPrefixHeroTextAreaMsg");
                if (litPrefixHeroTextAreaMsg != null)
                    litPrefixHeroTextAreaMsg.Text = g.GetResource("TXT_PREFIX_HERO_TEXTAREA_MSG", this);
                
                
                for (int i = 1; i <= 4; i++)
                {
                    Literal litPhotoResource = (Literal)this.FindControl(String.Format("litPhoto{0}", i.ToString()));
                    if (litPhotoResource != null)
                        litPhotoResource.Text = g.GetResource("PHOTO_TAB", this, new string[] { i.ToString() });

                    Literal litTabResource = (Literal)this.FindControl(String.Format("litTab{0}", i.ToString()));
                    if (litTabResource != null)
                        litTabResource.Text = g.GetResource(String.Format("PROFILE_TAB_{0}", i.ToString()), this);


                }

                rptPhotos.DataSource = _photos;
                rptPhotos.DataBind();

                if (_uploadFromFacebookEnabled)
                {
                    rptFacebookPhotos.Visible = true;
                    rptFacebookPhotos.DataSource = updateManager.GetPhotos();
                    rptFacebookPhotos.DataBind();
                }

                divFacebookHelp.Visible = _uploadFromFacebookEnabled;
                divNoFacebookHelp.Visible = !_uploadFromFacebookEnabled;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }

        protected override void OnInit(EventArgs e)
        {
            try
            {
                InitializeComponent();
                base.OnInit(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }

        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
            this.rptPhotos.ItemDataBound += new RepeaterItemEventHandler(rptPhotos_OnItemDataBound);
            this.rptFacebookPhotos.ItemDataBound += new RepeaterItemEventHandler(rptFacebookPhotos_OnItemDataBound);
            this.btnSaveTop.Click += new EventHandler(this.btnSave_Click);
            this.btnSaveBottom.Click += new EventHandler(this.btnSave_Click);
        }


        public void rptFacebookPhotos_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Photo p = (Photo)e.Item.DataItem;
                    Literal litPhotoPath = (Literal)e.Item.FindControl("litPhotoPath");
                    //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                    litPhotoPath.Text = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, g.Member, g.Brand, p,
                                                                    PhotoType.Full,
                                                                    PrivatePhotoImageType.None, true);

                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public void rptPhotos_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Photo p = (Photo)e.Item.DataItem;
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    txtUploadIds.Value = _uploadFileClientIds;

                    return;

                }
                else if (e.Item.ItemType == ListItemType.Header)
                {
                    txtUploadIds = (HtmlInputHidden)e.Item.FindControl("txtUploadClientIds");

                    return;
                }
                _count++;

                Literal litCss = (Literal)e.Item.FindControl("litCss");


                HtmlGenericControl phStatus = (HtmlGenericControl)e.Item.FindControl("phStatus");
                HtmlGenericControl phBrowse = (HtmlGenericControl)e.Item.FindControl("phBrowse");
                HtmlGenericControl phBrowseFacebook = (HtmlGenericControl)e.Item.FindControl("phBrowseFacebook");
                HtmlInputHidden txtPhotoID = (HtmlInputHidden)e.Item.FindControl("txtPhotoID");
                HtmlInputHidden txtFileID = (HtmlInputHidden)e.Item.FindControl("txtFileID");
                HtmlInputHidden txtListOrder = (HtmlInputHidden)e.Item.FindControl("txtListOrder");
                HtmlInputHidden txtDelete = (HtmlInputHidden)e.Item.FindControl("txtDelete");
                HtmlInputHidden txtApprovedForMain = (HtmlInputHidden)e.Item.FindControl("txtApprovedForMain");
                HtmlInputHidden txtIsMain = (HtmlInputHidden)e.Item.FindControl("txtIsMain");
                HtmlGenericControl pnlMakeMain = (HtmlGenericControl)e.Item.FindControl("pnlMakeMain");
                HtmlGenericControl pnlIsMain = (HtmlGenericControl)e.Item.FindControl("pnlIsMain");
                HtmlGenericControl pnlCantBeMain = (HtmlGenericControl)e.Item.FindControl("pnlCantBeMain");



                HtmlInputFile upBrowse = (HtmlInputFile)e.Item.FindControl("upBrowse");

                MultiValidator PhotoValidator = (MultiValidator)e.Item.FindControl("PhotoValidator");

                Txt txtSystemMsg = (Txt)e.Item.FindControl("systemStatusMessage");
                Txt txtSystemStatus = (Txt)e.Item.FindControl("systemStatus");
                Txt txtPhotoText = (Txt)e.Item.FindControl("photoText");
                TextBox txtCaption = (TextBox)e.Item.FindControl("txtCaption");

                System.Web.UI.WebControls.Image thumb = (System.Web.UI.WebControls.Image)e.Item.FindControl("thumbPhoto");

                CheckBox chkPrivatePhoto = (CheckBox)e.Item.FindControl("chkPrivatePhoto");
                HtmlGenericControl divPrivatePhoto = (HtmlGenericControl)e.Item.FindControl("divPrivatePhoto");

                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                HtmlGenericControl divNoPhoto = (HtmlGenericControl)e.Item.FindControl("divNoPhoto");
                NoPhoto20 ucNoPhoto1 = (NoPhoto20)e.Item.FindControl("ucNoPhoto1");
                ucNoPhoto1.Mode = NoPhoto20.PhotoMode.NoPhotoNoLink;
                ucNoPhoto1.MemberID = _memberid;
                ucNoPhoto1.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, true);
                ucNoPhoto1.EnableLink = false;

                GetThumbPhoto(p, thumb, divNoPhoto);

                phBrowseFacebook.Visible = _uploadFromFacebookEnabled;
                if (p.MemberPhotoID < 0)
                {

                    txtCaption.Style.Add("display", "none");


                    if (_count == _memberPhotosCount + 1 || _count == _browseRow + 1 ||
                        (upBrowse.PostedFile != null && upBrowse.PostedFile.ContentLength > 0))
                    {
                        phBrowse.Style.Add("display", "block");
                        if (_uploadFromFacebookEnabled)
                        {
                            phBrowseFacebook.Style.Add("display", "block");
                        }

                    }
                    else
                    {
                        phBrowse.Style.Add("display", "none");
                        phBrowseFacebook.Style.Add("display", "none");

                    }

                    string err = GetErrorMsg(_nophotosCount);
                    if (!String.IsNullOrEmpty(err))
                    {
                        PhotoValidator.ErrorMessage = err;
                        PhotoValidator.IsValid = false;

                    }
                    txtPhotoID.Value = Constants.NULL_INT.ToString();
                    txtFileID.Value = Constants.NULL_INT.ToString();
                    txtListOrder.Value = _count.ToString();
                    txtApprovedForMain.Value = "0";
                    txtIsMain.Value = "0";
                    litCss.Text = string.Format(FORMAT_BLOCKED_CSS, _count.ToString());
                    txtPhotoText.Text = g.GetResource("PHOTO", this, new string[] { _count.ToString() });
                    _nophotosCount += 1;
                }
                else
                {
                    phStatus.Style.Add("display", "block");
                    phBrowse.Style.Add("display", "none");
                    phBrowseFacebook.Style.Add("display", "none");
                    txtSystemMsg.Text = g.GetResource(GetPhotoStatusResx(p), this);
                    if (_count <= 4)
                    {
                        txtSystemStatus.Text = g.GetResource("PROFILE_TAB_" + _count.ToString(), this);
                        litCss.Text = string.Format(FORMAT_TAB_CSS, _count.ToString(), _tabs[_count - 1]);

                    }
                    else
                    {
                        litCss.Text = string.Format(FORMAT_NORMAL_CSS, _count.ToString());

                    }
                    txtPhotoText.Text = g.GetResource("PHOTO", this, new string[] { _count.ToString() });

                    txtPhotoID.Value = p.MemberPhotoID.ToString();
                    txtFileID.Value = p.FileID.ToString();
                    txtListOrder.Value = p.ListOrder.ToString();
                    txtApprovedForMain.Value = p.IsApprovedForMain ? "1" : "0";
                    txtIsMain.Value = p.IsMain ? "1" : "0";

                    if (p.IsApproved)
                    {
                        if (p.IsApprovedForMain)
                        {
                            pnlMakeMain.Visible = true;
                            pnlIsMain.Visible = true;
                        }
                        else
                        {

                            pnlCantBeMain.Visible = true;
                        }


                        if (p.IsApprovedForMain && !p.IsMain)
                        {
                            pnlIsMain.Style.Add("display", "none");
                        }
                        else if (p.IsMain)
                        {
                            pnlMakeMain.Style.Add("display", "none");
                        }
                    }


                    if (!string.IsNullOrEmpty(p.Caption))
                    {
                        p.Caption = p.Caption.Replace("\r\n", " ");
                        txtCaption.Text = p.Caption;
                    }
                    else
                    {
                        txtCaption.Text = string.Empty;
                    }

                    bool showThoseWhoLikedMyPhoto = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_THOSE_WHO_LIKED_MY_PHOTO", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    if (showThoseWhoLikedMyPhoto)
                    {
                        MemberLikeParams mlp = new MemberLikeParams();
                        mlp.MemberId = _memberid;
                        mlp.SiteId = g.Brand.Site.SiteID;
                        int likeCounts = MemberLikeHelper.Instance.GetCountOfLikesByObjectIdandType(mlp, p.MemberPhotoID, (int)LikeObjectTypes.Photo);
                        if (likeCounts > 0)
                        {
                            MemberLikeAnswerClientModule memberLikeClientModule = e.Item.FindControl("memberPhotoLikeClientModule") as MemberLikeAnswerClientModule;
                            if (null != memberLikeClientModule)
                            {
                                memberLikeClientModule.Visible = true;
                                memberLikeClientModule.MyEntryPoint = Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.MemberLikeLink;
                                memberLikeClientModule.Ordinal = e.Item.ItemIndex;
                                memberLikeClientModule.LoadMemberPhotoLikeClientModule(p.MemberPhotoID);
                            }
                        }
                    }

                }
                if (String.IsNullOrEmpty(txtCaption.Text))
                {
                    txtCaption.Text = g.GetResource("TXT_CAPTION_DEFAULT", this);
                }
                Literal litDelete = (Literal)e.Item.FindControl("litDeleteScript");
                Literal litUnDelete = (Literal)e.Item.FindControl("litUnDeleteScript");

                string deletescript = "onclick=\"javascript:markdelete(\'" + txtDelete.ClientID + "\');return false;\"";
                litDelete.Text = deletescript;
                string undeletescript = "onclick=\"javascript:markundelete(\'" + txtDelete.ClientID +
                                        "\');return false;\"";
                litUnDelete.Text = undeletescript;

                bool isPrivatePhotoEnabled = _g.Brand.GetStatusMaskValue(StatusType.PrivatePhotos);

                string showcaptionscript = "javascript:onBrowse(\"" + txtCaption.ClientID + "\"," + _count.ToString() +
                                           ",\"" + txtUploadIds.ClientID + "\", \"" +
                                           isPrivatePhotoEnabled.ToString().ToLower() + "\")";
                upBrowse.Attributes.Add("onchange", showcaptionscript);
                _uploadFileClientIds += upBrowse.ClientID + ",";

                chkPrivatePhoto.Checked = p.IsPrivate;
                if ((txtCaption.Style["display"] != "none" || phBrowse.Style["display"] == "block")
                    && isPrivatePhotoEnabled)
                {
                    divPrivatePhoto.Style.Add("display", "block");
                }
                else
                {
                    divPrivatePhoto.Style.Add("display", "none");
                }


            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }


        private void GetThumbPhoto(Photo photo, System.Web.UI.WebControls.Image thumbPhoto,
                                   HtmlGenericControl divNoPhoto)
        {
            try
            {
                if (photo.MemberPhotoID < 0)
                {
                    thumbPhoto.Visible = false;
                    return;
                }
                divNoPhoto.Style.Add("display", "none");

                bool useFullUrl = false;

#if DEBUG
                var environment = SettingsManager.GetSettingString(SettingConstants.ENVIRONMENT_TYPE, g.Brand);
                useFullUrl = environment.ToLower() == "dev" ? true : false;
#endif

                if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Thumbnail, g.Brand))
                {
                    thumbPhoto.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, g.Member, g.Brand, photo,
                                                                               PhotoType.Thumbnail,
                                                                               PrivatePhotoImageType.Thumb,
                                                                               NoPhotoImageType.Thumb, useFullUrl);
                    thumbPhoto.Visible = true;
                }
                else if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Full, g.Brand))
                {
                    thumbPhoto.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, g.Member, g.Brand, photo,
                                                                               PhotoType.Full,
                                                                               PrivatePhotoImageType.Thumb,
                                                                               NoPhotoImageType.Thumb, useFullUrl);
                    thumbPhoto.Visible = true;
                }
                else
                {
                    thumbPhoto.Visible = false;
                    divNoPhoto.Style.Add("display", "block");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private string GetPhotoStatusResx(Photo p)
        {
            if (p.IsApproved)
                return "APPROVED";
            else
                return "AWAITING_APPROVAL";

        }

        public bool IsValidImage(HttpPostedFile file, MultiValidator PhotoValidator)
        {
            IPhotoRules PhotoRules = new PhotoRulesManager();

            if (!PhotoRules.IsFileSizeAllowed(file.ContentLength))
            {
                PhotoValidator.Text = g.GetResource("MAXIMUM_FILE_SIZE_EXCEEDED", this);
                PhotoValidator.IsValid = false;
                return false;
            }

            if (!PhotoRules.IsValidImage(file.InputStream, (int)file.ContentLength))
            {
                PhotoValidator.Text = g.GetResource("INVALID_PHOTO_TYPE", this);
                PhotoValidator.IsValid = false;
                return false;
            }

            return true;
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool photosChanged = false;
                bool successfulSave = true;
                bool invalidPhotos = false;
                _errors = new List<string>();
                string defaultCaption = g.GetResource("TXT_CAPTION_DEFAULT", this).ToLower();

                MemberPhotoUpdateManager updateManager = new MemberPhotoUpdateManager(g.Member, g.Brand, true);
                updateManager.DefaultCaption = defaultCaption;

                if (_uploadFromFacebookEnabled && !String.IsNullOrEmpty(hdnFacebookURLs.Value))
                {
                    photosChanged = true;
                    string[] urls = hdnFacebookURLs.Value.Split(',');
                    foreach (string url in urls)
                    {
                        updateManager.AddRemotePhoto(url);
                        photosChanged = true;
                    }
                }

                int numberOfUploadedPhotos = 0;
                int numberOfAddedOrChangedCaptions = 0;

                List<int> currentOrder = ParseOrderString();

                if (currentOrder != null)
                {
                    for (int i = 0; i < MaxPhotoCount; i++)
                    {
                        int item = currentOrder[i];
                        RepeaterItem rptItem = (RepeaterItem)rptPhotos.Items[item - 1];
                        HtmlInputFile file = (HtmlInputFile)rptItem.FindControl("upBrowse");
                        MultiValidator validator = (MultiValidator)rptItem.FindControl("PhotoValidator");

                        HtmlInputHidden txtPhotoID = (HtmlInputHidden)rptItem.FindControl("txtPhotoID");
                        HtmlInputHidden txtFileID = (HtmlInputHidden)rptItem.FindControl("txtFileID");
                        HtmlInputHidden txtListOrder = (HtmlInputHidden)rptItem.FindControl("txtListOrder");
                        HtmlInputHidden txtDelete = (HtmlInputHidden)rptItem.FindControl("txtDelete");
                        HtmlInputHidden txtApprovedForMain = (HtmlInputHidden)rptItem.FindControl("txtApprovedForMain");
                        HtmlInputHidden txtIsMain = (HtmlInputHidden)rptItem.FindControl("txtIsMain");

                        TextBox txtCaption = (TextBox)rptItem.FindControl("txtCaption");
                        CheckBox chkPrivatePhoto = (CheckBox)rptItem.FindControl("chkPrivatePhoto");

                        int memberPhotoID = Conversion.CInt(txtPhotoID.Value);
                        string caption = txtCaption.Text;
                        Regex regex = new Regex("<(.|\n)+?>", RegexOptions.IgnoreCase);
                        caption = regex.Replace(caption, "");
                        caption = caption.Replace("\r\n", " ");

                        txtCaption.Text = caption;

                        Byte[] fileBytes = null;
                        bool isPrivate = chkPrivatePhoto.Checked;
                        var isApprovedForMain = txtApprovedForMain.Value == "1" ? true : false;
                        var isMain = txtIsMain.Value == "1" ? true : false;

                        if (file.PostedFile.ContentLength > 0)
                        {
                            if (txtDelete.Value == "1")
                                continue;
                            if (IsValidImage(file.PostedFile, validator))
                            {
                                if (_memberPhotosCount + _photoUploads >= MaxPhotoCount)
                                    continue;
                                HttpPostedFile newFile = file.PostedFile;
                                Stream fileStream = newFile.InputStream;
                                int length = (int)newFile.ContentLength;
                                fileStream.Position = 0;
                                fileBytes = new Byte[length];
                                fileStream.Read(fileBytes, 0, length);
                                newFile.InputStream.Flush();

                                updateManager.AddUpdate(memberPhotoID, txtCaption.Text, isPrivate, Convert.ToByte(i + 1),
                                                        fileBytes, false);

                                _photoUploads += 1;
                                numberOfUploadedPhotos++;
                                photosChanged = true;
                                if (!string.IsNullOrEmpty(caption))
                                {
                                    numberOfAddedOrChangedCaptions++;
                                }
                            }
                            else
                            {
                                invalidPhotos = true;
                                _errors.Add(validator.Text);

                            }
                        }
                        else if (memberPhotoID > 0)
                        {
                            Photo photo = _photos.Find(delegate(Photo p) { return p.MemberPhotoID == memberPhotoID; });
                            if (photo != null)
                            {
                                if (txtDelete.Value == "1")
                                {
                                    updateManager.AddDelete(memberPhotoID, Convert.ToByte(i + 1));
                                    photosChanged = true;
                                }
                                else
                                {
                                    if (caption.ToLower() != defaultCaption.ToLower() && photo.Caption != caption)
                                    {
                                        numberOfAddedOrChangedCaptions++;
                                        photosChanged = true;
                                    }

                                    if (photo.ListOrder != (i + 1))
                                    {
                                        photosChanged = true;
                                        //list order changed so we need to make sure the updates go through.
                                    }

                                    updateManager.AddUpdate(memberPhotoID, caption, isPrivate, Convert.ToByte(i + 1),
                                                            null, isMain);
                                }

                            }
                        }
                    }
                }

                if (photosChanged)
                {
                    PhotoUpdateResult uploadResult = updateManager.SaveUpdates();

                    if (uploadResult == PhotoUpdateResult.Success)
                    {
                        //TODO: this should eventually be moved out to manager class!!
                        #region UserNotification
                        if (UserNotificationFactory.IsUserNotificationsEnabled(g))
                        {
                            Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(g.Member.MemberID);
                            Int32 temp = 0;
                            System.Collections.ArrayList whoAddedYou =
                                list.GetListMembers(HotListCategory.WhoAddedYouToTheirFavorites,
                                                    g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, 1, 500,
                                                    out temp);

                            foreach (Int32 whoAddedYouId in whoAddedYou)
                            {
                                UserNotificationParams unParams = UserNotificationParams.GetParamsObject(whoAddedYouId,
                                                                                                         g.Brand.Site.
                                                                                                             SiteID,
                                                                                                         g.Brand.Site.
                                                                                                             Community.
                                                                                                             CommunityID);
                                UserNotification notification =
                                    UserNotificationFactory.Instance.GetUserNotification(
                                        new FavoriteUploadedPhotoUserNotification(), whoAddedYouId, g.Member.MemberID, g);
                                if (notification.IsActivated())
                                {
                                    UserNotificationsServiceSA.Instance.AddUserNotification(unParams,
                                                                                            notification.
                                                                                                CreateViewObject());
                                }
                            }
                        }
                        #endregion
                    }
                    else if (uploadResult == PhotoUpdateResult.TooManyPhotos)
                    {
                        g.Notification.AddError("SOME_PHOTOS_INVALID");
                    }
                    else
                    {
                        ShowErrorMessage();
                    }
                }

                if (invalidPhotos)
                {
                    g.Notification.AddError("SOME_PHOTOS_INVALID");
                }

                #region Omniture sets
                if (_photoUploads > 0)
                    g.AnalyticsOmniture.AddEvent("event29");

                if (numberOfUploadedPhotos > 0)
                {
                    g.AnalyticsOmniture.AddProductEvent("event32=" + numberOfUploadedPhotos.ToString());
                    g.AnalyticsOmniture.AddEvent("event32");

                }
                if (numberOfAddedOrChangedCaptions > 0)
                {
                    g.AnalyticsOmniture.AddEvent("event33,event34");
                    if (numberOfUploadedPhotos > 0)
                    {
                        g.AnalyticsOmniture.AddProductEvent("event32=" + numberOfUploadedPhotos.ToString() + "|event33=" +
                                                            numberOfAddedOrChangedCaptions.ToString());
                    }
                    else
                    {
                        g.AnalyticsOmniture.AddProductEvent("event33=" + numberOfAddedOrChangedCaptions.ToString());
                    }
                }
                #endregion

                rptPhotos.DataSource = null;
                rptPhotos.DataBind();
                Populate();
                // txtSaveChanges1.Text = g.GetResource("TXT_PROFILESAVED_MSG", this);
                if (successfulSave)
                {
                    g.Notification.AddMessageString(g.GetResource("TXT_PROFILESAVED_MSG", this));
                }
            }
            catch (Exception ex)
            {
                ShowErrorMessage();
                g.ProcessException(ex);
            }
            finally
            {
                //make sure that this gets cleared so the same list doesn't get uploaded again on postback.
                hdnFacebookURLs.Value = string.Empty;
            }
        }

        private void ShowErrorMessage()
        {
            g.Notification.AddError("TXT_UPLOAD_PHOTO_ERROR");
        }

        private List<int> ParseOrderString()
        {
            try
            {
                string requestListOrder = Request["hidListOrder"];
                if (String.IsNullOrEmpty(requestListOrder))
                    return null;
                List<int> orderList = new List<int>(MaxPhotoCount);

                string[] firstSplit = requestListOrder.Split(new char[] { '&' });
                for (int i = 0; i < firstSplit.Length; i++)
                {
                    string[] secondSplit = firstSplit[i].Split(new char[] { '=' });
                    int order = Conversion.CInt(secondSplit[1]);
                    orderList.Add(order);
                }
                return orderList;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return null;
            }

        }

        private string GetErrorMsg(int ind)
        {
            try
            {
                return _errors[ind];

            }
            catch (Exception)
            { return ""; }

        }

    }
}
