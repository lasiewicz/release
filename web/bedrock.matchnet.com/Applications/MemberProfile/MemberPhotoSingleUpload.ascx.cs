﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.TemplateControls;
using System.IO;
using Matchnet.Session.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Applications.MemberProfile
{
    public partial class MemberPhotoSingleUpload : FrameworkControl
    {
        private const int MAX_PHOTO_SIZE = 5120000;

        protected void Page_Load(object sender, EventArgs e)
        {
            topAuxNav.SetTopNavVisibility(false, true);  
        }

        #region Event Handlers
        protected void btnUpload_OnClick(object sender, EventArgs e)
        {
            bool valid = Validate();
        }
        #endregion

        #region Public Methods
        public bool Validate()
        {

            bool valid = IsValidImage();
            if (valid)
                valid = uploadMemberPhoto();

            return valid;
        }

        public bool IsValidImage()
        {
            bool valid = true;

            string phototypes = @"(.*\.[j/J][p/P][e/E][g/G]$)|(.*\.[j/J][p/P][g/G]$)|(.*\.[g/G][i/I][f/F]$)|(.*\.[b/B][m/M][p/P]$)";

            string filename = fileupload.FileName;
            if (String.IsNullOrEmpty(filename))
                return false;

            System.Text.RegularExpressions.Regex regValidation = new System.Text.RegularExpressions.Regex(phototypes);
            if (!regValidation.IsMatch(filename))
            {
                txtValidation.Text = g.GetResource("ERR_PHOTO_FORMAT_INVALID");
                txtValidation.Visible = true;
                return false;
            }
            HttpPostedFile file = fileupload.PostedFile;

            // Size check
            if (file == null)
                return false;
            if (file.ContentLength > MAX_PHOTO_SIZE)
            {
                txtValidation.Text = g.GetResource("ERR_MAXIMUM_FILE_SIZE_EXCEEDED");
                valid = false;
                txtValidation.Visible = true;
                return valid;
            }

            // File format check
            try
            {
                System.Drawing.Image image = System.Drawing.Image.FromStream(file.InputStream);
            }
            catch (ArgumentException)
            {
                txtValidation.Text = g.GetResource("ERR_UNABLE_TO_UPLOAD_A_PHOTO");
                valid = false;
                txtValidation.Visible = true;
                return valid;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine((string)ex.ToString());

                txtValidation.Text = g.GetResource("INVALID_PHOTO_TYPE");
                valid = false;
                return valid;
            }

            return valid;
        }
        #endregion

        private bool uploadMemberPhoto()
        {
            try
            {
                HttpPostedFile newFile = null;

                if (fileupload != null)
                {
                    newFile = fileupload.PostedFile;
                }
                else if (g.IsRemoteRegistration && Request.Files.Count > 0)
                {
                    newFile = Request.Files[0];
                }

                // don't attempt save if no uploaded file and no current file 
                if ((newFile != null) && (newFile.ContentLength > 0))
                {
                    try
                    {
                        //don't attempt if file is too big
                        if (newFile.ContentLength > MAX_PHOTO_SIZE)
                        {
                            txtValidation.Text = g.GetResource("MAXIMUM_FILE_SIZE_EXCEEDED");
                            txtValidation.Visible = true;
                            return false;
                        }

                        Stream fileStream = newFile.InputStream;
                        fileStream.Position = 0;
                        byte[] newFileBytes = new byte[newFile.ContentLength];
                        fileStream.Read(newFileBytes, 0, newFile.ContentLength);
                        newFile.InputStream.Flush();

                        PhotoUpdate photoUpdate = new PhotoUpdate(newFileBytes,
                            false,
                            Constants.NULL_INT,
                            Constants.NULL_INT,
                            Constants.NULL_STRING,
                            Constants.NULL_INT,
                            Constants.NULL_STRING,
                            1,
                            false,
                            false,
                            Constants.NULL_INT,
                            Constants.NULL_INT);

                        if (photoUpdate != null)
                        {
                            MemberSA.Instance.SavePhotos(g.Brand.BrandID, g.Brand.Site.SiteID,
                                g.Brand.Site.Community.CommunityID, g.Member.MemberID, new PhotoUpdate[] { photoUpdate });
                            
                            
                            // add something to Session to allow for them to browse the site without being block
                            g.Session.Add(WebConstants.SESSION_PHOTO_REQUIRE_TEMP_EXEMPT, 1, SessionPropertyLifetime.Temporary);

                            g.Session.Add(WebConstants.SESSION_NOTIFICATION_MESSAGE, "PHOTO_WAS_UPLOADED", SessionPropertyLifetime.Temporary);

                            g.Transfer("/Applications/Home/Default.aspx");
                            
                        }

                    }
                    catch (Exception exIn)
                    {
                        g.ProcessException(exIn);
                        //Member Registration will succeed, adding notification message.
                        txtValidation.Text = g.GetResource("MAXIMUM_FILE_SIZE_EXCEEDED");
                        txtValidation.Visible = true;
                        return false;
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return false;
            }
        }

        
    }
}
