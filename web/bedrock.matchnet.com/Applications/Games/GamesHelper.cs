using System;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Games.ServiceAdapters;
using Matchnet.Games.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework;
using System.Security.Cryptography;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Games
{
    public class GamesHelper
    {
        public static string SendInvitation(int initiatingMemberID, int recipientMemberID, int communityID, string gameID)
        {
            return GamesSA.Instance.InitiateGame(initiatingMemberID, recipientMemberID, communityID, gameID).ToString();
        }

        internal static string AcceptInvite(int recipientMemberID, string inviteKey, int communityID) //return GameID
        {
            string gameID = string.Empty;
            GameInvites invites = GamesSA.Instance.GetInvites(recipientMemberID, communityID);
            if (invites != null && invites.Count > 0)
            {
                Guid guid = new Guid(inviteKey);
                invites[guid].IsAccepted = true;
                gameID = invites[guid].GameID;
                GamesSA.Instance.Save(invites[guid]);
            }
            return gameID;
        }

        internal static void RejectInvite(int recipientMemberID, string inviteKey, int communityID) //return GameID
        {
            GameInvites invites = GamesSA.Instance.GetInvites(recipientMemberID, communityID);
            if (invites != null && invites.Count > 0)
            {
                Guid guid = new Guid(inviteKey);
                invites[guid].IsRejected = true;
                GamesSA.Instance.Save(invites[guid]);
            }
        }

        internal static void DeleteInvite(int recipientMemberID, string inviteKey, int communityID)
        {
            GameInvites invites = GamesSA.Instance.GetInvites(recipientMemberID, communityID);
            if (invites != null && invites.Count > 0)
            {
                Guid guid = new Guid(inviteKey);
                if (invites[guid] != null)
                {
                    GamesSA.Instance.DeclineInvitation(invites[guid]);
                }
            }
        }

        internal static string GetGameURL(ContextGlobal g, Matchnet.Member.ServiceAdapters.Member member, int destinationMemberID, string gameID, bool isInvitee, HttpRequest request)
        {
            string gameURL =
                "http://www.come2play.com/channel_auth.asp?method_type=matching&channel_id={0}&uid={1}&nick_name={2}&avatar_url={3}&date_of_birth={4}&matching_uids={5}&matching_game_id={6}&auth_sig={7}&city={8}&matching_stake=0";
            string channelID = RuntimeSettings.GetSetting("GAMES_CHANNEL_ID", g.Brand.Site.Community.CommunityID);


            string matching_uids = member.MemberID + "," + destinationMemberID;
            if (isInvitee)
            {
                matching_uids = destinationMemberID + "," + member.MemberID;
            }

            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, member, g.Brand);
            string imgURL = FrameworkGlobals.GetHomepageAbsURL(g, request.ServerVariables.Get("HTTP_HOST")) +
               MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, member, g.Brand, photo,
                    PhotoType.Thumbnail,PrivatePhotoImageType.TinyThumb,NoPhotoImageType.BackgroundTinyThumb);

            //string nickName = member.GetUserName(_g.Brand) + " " + ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(member, g);
            //string nickName = member.GetUserName(_g.Brand) + " " +
            //                  GetAgeString(member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue)) +
            //                  GetCityString(member.GetAttributeInt(g.Brand, "RegionID"), g.Brand.Site.LanguageID);
            string nickName = member.GetUserName(g.Brand);

            gameURL = string.Format(gameURL,
                                    HttpUtility.UrlEncode(channelID),
                                    HttpUtility.UrlEncode(member.MemberID.ToString()),
                                    HttpUtility.UrlEncode(nickName),
                                    HttpUtility.UrlEncode(imgURL),
                                    HttpUtility.UrlEncode(member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue).ToShortDateString()),
                                    HttpUtility.UrlEncode(matching_uids),
                                    HttpUtility.UrlEncode(gameID),
                                    HttpUtility.UrlEncode(getAuthSig(member.MemberID.ToString(), matching_uids, "0", gameID, channelID)),
                                    HttpUtility.UrlEncode(ProfileDisplayHelper.GetRegionDisplay(member, g)));

            bool inviterSub = false;
            bool inviteeSub = false;
            if (isInvitee)
            {
                inviteeSub = member.IsPayingMember(g.Brand.Site.SiteID);
                inviterSub = MemberSA.Instance.GetMember(destinationMemberID, MemberLoadFlags.None).IsPayingMember(g.Brand.Site.SiteID);
            }
            else
            {
                inviterSub = member.IsPayingMember(g.Brand.Site.SiteID);
                inviteeSub = MemberSA.Instance.GetMember(destinationMemberID, MemberLoadFlags.None).IsPayingMember(g.Brand.Site.SiteID);
            }

            bool isGuest = false;

            if(!inviterSub)
            {
                if(!isInvitee)
                {
                    isGuest = true;
                }
                else
                {
                    if(!inviteeSub)
                    {
                        isGuest = true;
                    }
                }
            }

            if(isGuest)
            {
                gameURL += "&is_guest=1";
            }
            else
            {
                gameURL += "&is_guest=0";
            }

            return gameURL;
        }

        private static string GetCityString(int regionID, int languageID)
        {
            string result = " ";
            try
            {
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);
                result += regionLanguage.CityName;
            }
            catch (Exception)
            {
                result = string.Empty;
            }
            return result;

        }

        private static string GetAgeString(DateTime dateOfBirth)
        {
            string result = " ";
            try
            {
                result += FrameworkGlobals.GetAge(dateOfBirth);
            }
            catch (Exception)
            {
                result = string.Empty;
            }
            return result;
        }

        private static string getAuthSig(string uid, string matchingUIDs, string matchingStake, string matchingGameID, string channelKey)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(uid + matchingUIDs + matchingStake + matchingGameID + channelKey));

            md5Hasher.ComputeHash(data);
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        internal static bool CheckInviteAccepted(int inviteeMemberID, int communityID, string invitekey)
        {
            bool result = false;
            GameInvites invites = GamesSA.Instance.GetInvites(inviteeMemberID, communityID);
            if (invites != null && invites.Count > 0)
            {
                Guid guid = new Guid(invitekey);
                if (invites[guid] != null)
                {
                    result = invites[guid].IsAccepted;
                }

            }
            return result;
        }

        internal static bool CheckInviteRejected(int inviteeMemberID, int communityID, string invitekey)
        {
            bool result = false;
            GameInvites invites = GamesSA.Instance.GetInvites(inviteeMemberID, communityID);
            if (invites != null && invites.Count > 0)
            {
                Guid guid = new Guid(invitekey);
                if (invites[guid] != null)
                {
                    result = invites[guid].IsRejected;
                }
            }
            return result;
        }

        internal static void SendMissedGameMessage(Matchnet.Member.ServiceAdapters.Member senderMember, int inviteeMemberID, ContextGlobal g, string subject,
            StringDictionary expansionTokens,
            string atQuotaErrorSubject,
            string atQuotaErrorMessage)
        {
            MessageSave msg = new MessageSave(senderMember.MemberID, inviteeMemberID, g.Brand.Site.Community.CommunityID, MailType.MissedGame, subject,
                                              string.Empty);
            bool _saveInSent = false;
            AttributeOptionMailboxPreference mailboxMask = (AttributeOptionMailboxPreference)senderMember.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_MAILBOXPREFERENCE);
            if ((mailboxMask & AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) == AttributeOptionMailboxPreference.SaveCopiesOfSentMessages)
            {
                _saveInSent = true;
            }

            if (QuotaSA.Instance.IsAtQuota(msg.GroupID, msg.FromMemberID, QuotaType.MissedGame))
            {

                msg = GetQuotaErrorMessage(MemberSA.Instance.GetMember(inviteeMemberID, MemberLoadFlags.None).GetUserName(g.Brand), msg, g,
                    expansionTokens,
                    atQuotaErrorSubject,
                    atQuotaErrorMessage);

            }
            else
            {
                QuotaSA.Instance.AddQuotaItem(msg.GroupID, msg.FromMemberID, QuotaType.MissedGame);
            }

            MessageSendResult messageSendResult = EmailMessageSA.Instance.SendMessage(msg, _saveInSent,
                                                                                      Constants.NULL_INT, false);
            if (messageSendResult.Status == MessageSendStatus.Success)
            {
                ExternalMailSA.Instance.SendInternalMailNotification(messageSendResult.EmailMessage, g.Brand.BrandID,
                                                                     messageSendResult.RecipientUnreadCount);
            }
        }

        private static MessageSave GetQuotaErrorMessage(string targetMemberUsername, MessageSave originalMessageSave, ContextGlobal g, StringDictionary expansionTokens, string atQuotaErrorSubject,
            string atQuotaErrorMessage)
        {
            //18127 Make sure quota message is in the correct language of the SENDING member (not the intended recipient Member)
            //SetBrandIfJDIL(Member);
            // expansionTokens.Add("USERNAME", targetMemberUsername);
            expansionTokens.Add("ORIGINALMESSAGE", originalMessageSave.MessageBody);

            return new MessageSave(originalMessageSave.FromMemberID,
                originalMessageSave.FromMemberID,
                originalMessageSave.GroupID,
                g.Brand.Site.SiteID,
                originalMessageSave.MailType,
                atQuotaErrorSubject,
                atQuotaErrorMessage);

        }
    }
}