﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Game.ascx.cs" Inherits="Matchnet.Web.Applications.Games.Game" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<body class="gamesBody">
    <form runat="server" id="form1">
    <asp:PlaceHolder runat="server" ID="plcChooseGame">
        <div class="inviteGameDiv">
            <div id="inviteGameHeader" class="inviteGameHeader">
                <mn:Image ID="imgHeaderLogo" CssClass="imgHeaderLogo" FileName="logo_nav.jpg" align="absmiddle"
                    Vspace="0" Border="0" runat="server" />
            </div>
            <div id="chooseGameDiv" class="chooseGameDiv">
                <div id="chooseGameHeader" class="chooseGameHeader">
                    <mn:Image ID="Image7" CssClass="dotBeforeTxt" FileName="redDot.gif" align="absmiddle"
                        Vspace="0" Border="0" runat="server" />
                    <mn:Txt ID="TxtChooseGame" runat="server" CssClass="gameMessageTxt" ResourceConstant="TXT_CHOOSE_GAME" />
                </div>
                <div id="gamesDiv">
                    <asp:Repeater runat="server" ID="rptrGamesList" OnItemDataBound="rptrGamesList_ItemDataBound"
                        OnItemCommand="rptrGamesList_ItemCommand">
                        <HeaderTemplate>
                            <table cellpadding="0" cellspacing="0" border="0" class="chooseGameTbl">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#  OpenTR(Container.ItemIndex)%>
                            <td class="chooseGameTblTd">
                                <asp:ImageButton runat="server" ID="imgGame" CssClass="imgGame" BorderStyle="None"
                                    CommandName="GameImageClicked" />
                                <br />
                                <asp:LinkButton ID="lnkGame" runat="server" CommandName="GameLinkClicked"></asp:LinkButton>
                            </td>
                            <%# CloseTR(Container.ItemIndex)%>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div id="chooseGameBanner" class="gameBanner">
                    <mn:AdUnit id="adChooseGame" Size="Games" runat="server" GAMAdSlot="games_bottom_lobby_590x60" />
                </div>
            </div>
        </div>
         <mn:AdUnit Visible="false" GAMAdSlot="games_overlay_1x1" runat="server"></mn:AdUnit>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcWaitingForPlayer">
        <div class="inviteGameDiv">
            <div id="Div1" class="inviteGameHeader">
                <mn:Image ID="Image1" CssClass="imgHeaderLogo" FileName="logo_nav.jpg" align="absmiddle"
                    Vspace="0" Border="0" runat="server" />
            </div>
            <div id="Div2" class="chooseGameDiv">
                <div class="mainContentDiv">
                    <div class="mainContentTxt">
                        <mn:Txt ID="Txt1" runat="server" CssClass="gameMessageTxt" ResourceConstant="TXT_PENDING_GAME" />
                        <br />
                        <br />
                        <br />
                        <mn:Image ID="Image2" FileName="gameLoader.gif" align="absmiddle" Vspace="0" Border="0"
                            runat="server" />
                    </div>
                </div>
                <div id="Div4" class="gameBanner">
                    <mn:AdUnit id="adWaitingForPlayer" Size="Games" runat="server" />
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcGame">
        <iframe runat="server" id="ifrmGame" width="100%" height="100%" frameborder="0">
        </iframe>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcInvitationDeclined">
        <div class="inviteGameDiv">
            <div id="Div5" class="inviteGameHeader">
                <mn:Image ID="Image3" CssClass="imgHeaderLogo" FileName="logo_nav.jpg" align="absmiddle"
                    Vspace="0" Border="0" runat="server" />
            </div>
            <div id="Div6" class="chooseGameDiv">
                <div class="mainContentDiv">
                    <div class="mainContentTxt">
                        <mn:Txt ID="Txt2" runat="server" CssClass="gameMessageTxt" ResourceConstant="TXT_ERROR_MESSAGE" />
                        <br />
                        <br />
                        <br />
                        <mn:Image ID="Image4" CssClass="closeGameWindowBtn" FileName="btn_close_window.gif"
                            align="absmiddle" Vspace="0" Border="0" runat="server" onclick="javascript:window.close();" />
                    </div>
                </div>
                <div id="Div8" class="gameBanner">
                    <mn:AdUnit id="adInvitationDeclined" Size="Games" runat="server" GAMAdSlot="games_bottom_refusepage_590x60" />
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcError">
        <div class="inviteGameDiv">
            <div id="Div3" class="inviteGameHeader">
                <mn:Image ID="Image5" CssClass="imgHeaderLogo" FileName="logo_nav.jpg" align="absmiddle"
                    Vspace="0" Border="0" runat="server" />
            </div>
            <div id="Div7" class="chooseGameDiv">
                <div class="mainContentDiv">
                    <div class="mainContentTxt">
                        <mn:Txt ID="Txt3" runat="server" CssClass="gameMessageTxt" ResourceConstant="TXT_ERROR_GAME" />
                        <br />
                        <br />
                        <br />
                        <mn:Image ID="Image6" CssClass="closeGameWindowBtn" FileName="btn_close_window.gif"
                            align="absmiddle" Vspace="0" Border="0" runat="server" onclick="javascript:window.close();" />
                    </div>
                </div>
                <div id="Div9" class="gameBanner">
                    <mn:AdUnit id="adError" Size="Games" runat="server" GAMAdSlot="games_bottom_errorpage_590x60" />
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:HiddenField runat="server" ID="hfStep" EnableViewState="true" />
    <asp:HiddenField runat="server" ID="hfGameID" EnableViewState="true" />
    <asp:HiddenField runat="server" ID="hfInviteKey" EnableViewState="true" />
    </form>
</body>
