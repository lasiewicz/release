﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Mobile
{
    public partial class MobileLanding : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //based to is the user if logged in show the nav .   if not show basic stuff. 
           if(g == null || g.Member == null)
           {
               navScript.Visible = true;
           }

           pnlMain.Visible = true;
        }
         
        protected void lbMSeeFeatures_Click(object sender, EventArgs e)
        {
            pnlMobile.Visible = true;
            pnlJpix.Visible = false;
            pnlMain.Visible = false;
            pnlIOS.Visible = false;
        }

        protected void lbJSeeFeatures_Click(object sender, EventArgs e)
        {
            pnlJpix.Visible = true;
            pnlMobile.Visible = false;
            pnlMain.Visible = false;
            pnlIOS.Visible = false;
        }

        protected void lbISeeFeatures_Click(object sender, EventArgs e)
        {
            pnlIOS.Visible = true;
            pnlJpix.Visible = false;
            pnlMobile.Visible = false;
            pnlMain.Visible = false;
        }

        protected void lbSeeFeatures_Click(object sender, EventArgs e)
        {
            pnlMain.Visible = true;
            pnlJpix.Visible = false;
            pnlMobile.Visible = false;
            pnlIOS.Visible = false;
        }

        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            

        }

     override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }


    }
}