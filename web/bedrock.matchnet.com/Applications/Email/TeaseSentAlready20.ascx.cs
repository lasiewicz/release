﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.Email
{
    public partial class TeaseSentAlready20 : FrameworkControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                txtSubscribeNowLC.Href = FrameworkGlobals.LinkHrefSSL("/Applications/Subscription/Subscribe.aspx?prtid=139");

                string memberId = Request.Params.Get("MemberID");
                string startRow = Request.Params.Get("StartRow");

                if (memberId != null && memberId.Trim().Length > 0)
                {
                    string toUsername = MemberSA.Instance.GetMember(Convert.ToInt32(memberId), MemberLoadFlags.None).GetUserName(g.Brand);

                    string alreadyFlirtedText = string.Format(g.GetResource("TXT_YOU_ALREADY_SENT_A_TEASE_TO_USERNAME", this), toUsername);
                    YouHaveAlreadySentATeaseToThisMember.Text = alreadyFlirtedText;

                    string backToUserProfile = string.Format(g.GetResource("TXT_BACK_TO_USERS_PROFILE", this), toUsername);
                    txtBackToProfile.Text = backToUserProfile;


                    string keepConvGoing = string.Format(g.GetResource("TXT_KEEP_THE_CONVERSATION_GOING_WITH_USERNAME", this), toUsername);
                    txtKeepTheConvGoing.Text = keepConvGoing;

                    
                    if (g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY] != null && !g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY].Equals(String.Empty))
                    {
                        backToProfileLink.NavigateUrl = g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY];

                        if (startRow != null && startRow.Trim().Length > 0)
                        {
                            // Remove all existing occurrences of "StartRow" in the URL, but leave everything else
                            string originalURL = backToProfileLink.NavigateUrl;

                            // See if the navigatUrl has params already
                            string qm = "&";
                            if ((originalURL.IndexOf("?") == -1))
                            {
                                qm = "?";
                                backToProfileLink.NavigateUrl = originalURL + qm + "StartRow=" + startRow;
                            }
                            else
                            {
                                if (originalURL.IndexOf("StartRow") != -1)
                                {
                                    originalURL = g.RemoveParamFromURL(originalURL, "StartRow", false);

                                    if ((originalURL.IndexOf("?") == -1))
                                        qm = "?";

                                    backToProfileLink.NavigateUrl = originalURL + qm + "StartRow=" + startRow;
                                }
                                else
                                {
                                    backToProfileLink.NavigateUrl += qm + "StartRow=" + startRow;
                                }
                            }
                        }

                        if (Matchnet.Content.ServiceAdapters.PageConfigSA.Instance.GetPageName(g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY]).ToLower() != "viewprofile")
                        {
                            txtBackToProfile.ResourceConstant = "TXT_BACK";
                            txtBackToProfile.TitleResourceConstant = "TTL_BACK";
                        }

                        // Remove session key so it doesn't interfere with future calls
                        g.Session.Remove(Tease.TEASE_ORIGIN_URL_SESSION_KEY);

                    }
                    else
                    {
                        backToProfileLink.NavigateUrl = "/Applications/MemberProfile/ViewProfile.aspx?MemberId=" + memberId;
                    }

                    keepTheConversationGoingLink.NavigateUrl = backToProfileLink.NavigateUrl;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}