﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Email
{
    public partial class PushFlirtsPopupConfirmation : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void ShowConfirmationSuccess()
        {
            pnlConfirmationSuccess.Visible = true;
        }

        public void ShowQuotaReached()
        {
            pnlQuotaReached.Visible = true;
        }

        public void ShowNotEnoughResults()
        {
            pnlNotEnoughResults.Visible = true;
        }
    }
}