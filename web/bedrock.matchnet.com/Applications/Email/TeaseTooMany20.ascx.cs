﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;
using Matchnet.Content.ServiceAdapters;
namespace Matchnet.Web.Applications.Email
{
    public partial class TeaseTooMany20 : FrameworkControl
    {
        public const String QSPARAM_ERROR_TYPE = "errortype";
        public const String QSPARAM_ERROR_OCCURRED = "erroroccurred";
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                string memberId = Request.Params.Get("MemberID");
                string startRow = Request.Params.Get("StartRow");

                if (Request[QSPARAM_ERROR_OCCURRED] == "true")
                {
                    if (Request[QSPARAM_ERROR_TYPE] == "email")
                    {
                        g.BreadCrumbTrailHeader.SetThreeLinkCrumb(g.GetResource(g.AppPage.App.ResourceConstant, null), g.AppPage.App.DefaultPagePath,
                             g.GetResource("EMAILTOOMANY_BREADCRUMB", this), null);
                        g.BreadCrumbTrailFooter.SetThreeLinkCrumb(g.GetResource(g.AppPage.App.ResourceConstant, null), g.AppPage.App.DefaultPagePath,
                            g.GetResource("EMAILTOOMANY_BREADCRUMB", this), null);
                        txtMessage.ResourceConstant = "ERROR_EMAIL_QUOTA_REACHED";



                        // Show back to profile link TT#15545
                        if (memberId != null && memberId.Trim().Length > 0)
                        {
                            if (g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY] != null && !g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY].Equals(String.Empty))
                            {
                                backToProfileLink.NavigateUrl = g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY];
                                if (Matchnet.Content.ServiceAdapters.PageConfigSA.Instance.GetPageName(g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY]).ToLower() != "viewprofile")
                                {
                                    txtBackToProfile.ResourceConstant = "TXT_BACK";
                                }
                            }
                            else
                            {
                                backToProfileLink.NavigateUrl = "/Applications/MemberProfile/ViewProfile.aspx?MemberId=" + memberId;
                            }
                        }
                        else if (startRow != null && startRow.Trim().Length > 0)
                        {
                            backToProfileLink.NavigateUrl = "/Applications/MemberProfile/View.aspx?CategoryID=-6&StartRow=" + startRow;
                        }
                        else if (g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY] != null && !g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY].Equals(String.Empty))
                        {
                            backToProfileLink.NavigateUrl = g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY];
                        }
                        else if (g.Session[Compose20.MESSAGE_ORIGIN_URL_SESSION_KEY] != null && !g.Session[Compose20.MESSAGE_ORIGIN_URL_SESSION_KEY].Equals(string.Empty))
                        {
                            backToProfileLink.NavigateUrl = g.Session[Compose20.MESSAGE_ORIGIN_URL_SESSION_KEY];

                            // TT#16047 - set the correct text
                            if (PageConfigSA.Instance.GetPageName(g.Session[Compose20.MESSAGE_ORIGIN_URL_SESSION_KEY]).ToString().ToLower() == "viewprofile")
                            {
                                txtBackToProfile.ResourceConstant = "TXT_BACK_TO_PROFILE";
                            }
                            else
                            {
                                txtBackToProfile.ResourceConstant = "TXT_BACK";
                                txtBackToProfile.TitleResourceConstant = "TTL_BACK";
                            }

                            // Remove session key so it doesn't interfere with future calls
                            g.Session.Remove(Compose20.MESSAGE_ORIGIN_URL_SESSION_KEY);
                        }
                    }

                    else if (Request[QSPARAM_ERROR_TYPE] == "ignore")
                    {

                        setIgnoreMessage(memberId);
                        //						g.BreadCrumbTrailHeader.SetThreeLinkCrumb( g.GetResource(g.AppPage.App.ResourceConstant, null), g.AppPage.App.DefaultPagePath,
                        //							g.GetResource("IGNORELIST_BREADCRUMB", this), null );
                        //						int id=Int32.Parse(memberId);	
                        //						Member.ServiceAdapters.Member toMember = Member.ServiceAdapters.MemberSA.Instance.GetMember(id,Member.ServiceAdapters.MemberLoadFlags.None);
                        //					
                        //						string[] args=new string[]{toMember.GetUserName(_g.Brand)};
                        //						txtMessage.Args=args;
                        //						txtMessage.ResourceConstant ="TXT_IGNORE_CONTENT";
                        //						backToProfileLink.NavigateUrl = g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY];



                    }
                    else
                    {
                        txtMessage.ResourceConstant = "ERROR_TEASE_NOT_SENT";
                    }
                }
                else if (memberId != null && memberId.Trim().Length > 0)
                {
                    backToProfileLink.NavigateUrl = "/Applications/MemberProfile/ViewProfile.aspx?MemberId=" + memberId;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void setIgnoreMessage(string memberId)
        {
            try
            {

                //g.BreadCrumbTrailHeader.SetThreeLinkCrumb(g.GetResource(g.AppPage.App.ResourceConstant, null), g.AppPage.App.DefaultPagePath,
                //    g.GetResource("IGNORELIST_BREADCRUMB", this), null);
                int id = Int32.Parse(memberId);
                Member.ServiceAdapters.Member toMember = Member.ServiceAdapters.MemberSA.Instance.GetMember(id, Member.ServiceAdapters.MemberLoadFlags.None);

                string[] args = new string[] { toMember.GetUserName(_g.Brand) };
                txtMessage.Args = args;
                txtMessage.ResourceConstant = "TXT_IGNORE_CONTENT";
                backToProfileLink.NavigateUrl = g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY];
            }
            catch (Exception ex)
            { }


        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}