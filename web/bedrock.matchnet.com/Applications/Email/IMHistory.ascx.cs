﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using System.Configuration;
using Spark.Common.RestConsumer.V2.Models.OAuth2;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.Email
{
    public partial class IMHistory : FrameworkControl
    {
        protected int _MemberID = 0;
        protected string _RESTAuthority = "";
        protected string _APItoken = "";
        protected string _NoPhotoMale = "";
        protected string _NoPhotoFemale = "";
        protected int _BrandId = 0;
        protected string _IMUrl = "";
        protected string _EmailUrl = "";
        protected string _ProfileUrl = "";
        protected string _NoHistoryMessage = "";
        protected string _MaritalMtoF = "";
        protected string _MaritalFtoM = "";

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (g.Member == null)
                {
                    g.LogonRedirect(HttpContext.Current.Request.RawUrl.ToString(), null);
                }
                else
                {
                    if (!IMManager.Instance.IsImHistoryPageAvailableToNonSubs(g.Brand))
                    {
                        if (g.Brand.BrandID == 1004)
                            MailBoxHandler.ValidMemberAccess(g, false,(int)Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.SubscriptionPurchaseFromIMHistory,WebConstants.MailAccessArea.IMHistory);
                        else
                            MailBoxHandler.ValidMemberAccess(g, false, (int)Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.AttemptToAccessInbox,WebConstants.MailAccessArea.IMHistory);
                    }

                    _MemberID = g.Member.MemberID;
                    _RESTAuthority = ConfigurationManager.AppSettings["RESTAuthority"];
                    _NoPhotoFemale = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.ThumbV2, false);
                    _NoPhotoMale = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.ThumbV2, true);
                    _BrandId = g.Brand.BrandID;
                    if (g.ValidateMemberContact(g.Member.MemberID))
                    {
                        _IMUrl = OnlineLinkHelper.GetIMLinkPath(g, g.Member, "").Replace(g.Member.MemberID.ToString(), "{memberid}").Replace(g.Member.GetUserName(g.Brand), "{username}");
                    }
                    else
                    {
                        _IMUrl = OnlineLinkHelper.GetSubscriptionLinkPath(g.Member, Constants.NULL_INT, Content.ServiceAdapters.Links.LinkParent.MiniProfileTop).Replace(g.Member.MemberID.ToString(), "{memberid}");
                    }
                    _EmailUrl = "/Applications/Email/Compose.aspx?MemberId={memberid}&EntryPoint=" + (int)BreadCrumbHelper.EntryPoint.Messages;
                    _ProfileUrl = "/applications/memberprofile/viewprofile.aspx?memberid={memberid}";

                    //no IM History message copy, move to resources later
                    _NoHistoryMessage = g.GetResource("TXT_NO_HISTORY", this);
                    _MaritalMtoF = g.GetResource("TXT_MALE_TO_FEMALE", this);
                    _MaritalFtoM = g.GetResource("TXT_FEMALE_TO_MALE", this);
                        

                    LogonManager logonManager = new LogonManager(_g);
                    OAuthTokens oAuthTokens = logonManager.GetTokensFromCookies();
                    if (oAuthTokens != null)
                    {
                        _APItoken = oAuthTokens.AccessToken;
                    }

                    //enable CORS
                    if (g.Head20 != null)
                    {
                        g.Head20.EnableCORS = true;
                    }

                    SetupTabBar();
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion

        #region Private Methods
        private void SetupTabBar()
        {
            FrameworkControl resourceControl = this.Page.LoadControl("/Applications/Email/MailboxResource.ascx") as FrameworkControl;
            MailBoxHandler.SetupTabBar(mailBoxTabBar, true, resourceControl, g, -1);
        }

        #endregion
    }
}