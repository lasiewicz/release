﻿using System;
using System.Diagnostics;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.UPS;
using Matchnet.Web.Framework.Ui.Advertising;
using Spark.Common.AccessService;
using System.Collections.Generic;


namespace Matchnet.Web.Applications.Email
{
    public class InboxShutdownHelper
    {
        private ContextGlobal g;
        private bool? _isCurrentMemberTargeted = null;

        public bool IsCurrentMemberTargeted
        {
            get
            {
                try
                {
                    if (_isCurrentMemberTargeted == null)
                    {
                        InboxShutdownTargeting ist = new InboxShutdownTargeting(g);
                        _isCurrentMemberTargeted = ist.IsTargeted();
                    }
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }

                return _isCurrentMemberTargeted.GetValueOrDefault(false);
            }
            set { _isCurrentMemberTargeted = value; }
        }


        public InboxShutdownHelper(ContextGlobal _g)
        {
            g = _g;
        }

        public bool IsInboxShutdownEnabled()
        {
            return IsInboxShutdownEnabledSite();
        }

        private bool IsInboxShutdownEnabledSite()
        {
            return
                (RuntimeSettings.GetSetting("ENABLE_INBOX_SHUTDOWN", g.Brand.Site.Community.CommunityID,
                                            g.Brand.Site.SiteID).ToLower() == "true") &&
                                                                                            (DateTime.Now >=
                                                                                             Conversion.CDateTime(
                                                                                                 RuntimeSettings.
                                                                                                     GetSetting(
                                                                                                         "INBOX_SHUTDOWN_LAUNCH_DATE",
                                                                                                         g.Brand.Site.
                                                                                                             Community.
                                                                                                             CommunityID,
                                                                                                         g.Brand.Site.
                                                                                                             SiteID,
                                                                                                         g.Brand.BrandID)));
        }

        public bool IsMessageBlocked(EmailMessage emailMessage, Member.ServiceAdapters.Member member, int fromMemberID,
                                     int currentMemberFolderID)
        {
            //return true;
            bool isMessageBlocked = false;
            if (emailMessage.InsertDate >= Conversion.CDateTime(RuntimeSettings.GetSetting("INBOX_SHUTDOWN_LAUNCH_DATE",
                                                                        g.Brand.Site.Community.CommunityID,
                                                                        g.Brand.Site.SiteID, g.Brand.BrandID)))
            {
                // If we got this far that means that Inbox Shutdown is enabled for this site
                if (currentMemberFolderID != (int)SystemFolders.Sent)
                {
                    if (this.IsCurrentMemberTargeted)
                    {
                        if (!IsSenderGrandfathered(fromMemberID))
                        {
                            isMessageBlocked = true;
                        }
                    }
                }
            }

            return isMessageBlocked;
        }

        // true if member was paying since launch date (DB SWITCH) without stopping
        private bool IsSenderGrandfathered(int senderMemberId)
        {
            bool isGrandfathered = false;
            bool writeEventLog = false;

            Member.ServiceAdapters.Member fromMember =
                Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(senderMemberId,
                                                                            MemberLoadFlags.IngoreSACache);
            string debug = string.Empty;

            if (fromMember != null)
            {
                if (MemberPrivilegeAttr.IsCureentSubscribedMember(fromMember, g))
                {
                    int lastBrandID = Constants.NULL_INT;
                    fromMember.GetLastLogonDate(g.Brand.Site.Community.CommunityID, out lastBrandID);
                    Matchnet.Content.ValueObjects.BrandConfig.Brand senderBrand =
                        Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(lastBrandID);
                    DateTime grandfatherDate =
                        Conversion.CDateTime(RuntimeSettings.GetSetting("INBOX_SHUTDOWN_LAUNCH_DATE",
                                                                        g.Brand.Site.Community.CommunityID,
                                                                        g.Brand.Site.SiteID, g.Brand.BrandID));
                    DateTime subscriptionLastInitialPurchaseDateAfterLapse = fromMember.GetAttributeDate(senderBrand,
                                                                                                         "SubscriptionLastInitialPurchaseDateAfterLapse",
                                                                                                         DateTime.
                                                                                                             MaxValue);

                    DateTime subscriptionLastInitialPurchaseDate = fromMember.GetAttributeDate(senderBrand,
                                                                                               "SubscriptionLastInitialPurchaseDate",
                                                                                               DateTime.MaxValue);

                    DateTime initialDate = subscriptionLastInitialPurchaseDate;
                    if (subscriptionLastInitialPurchaseDateAfterLapse != DateTime.MinValue)
                    {
                        if (subscriptionLastInitialPurchaseDateAfterLapse < subscriptionLastInitialPurchaseDate)
                        {
                            initialDate = subscriptionLastInitialPurchaseDateAfterLapse;
                        }
                    }

                    debug =
                        string.Format(
                            "memberid={0}\nemail={1}\nIsCureentSubscribedMember=true\ngrandfatherDate={2}\ninitialDate={3}\n",
                            fromMember.MemberID, fromMember.EmailAddress, grandfatherDate.ToString(),
                            initialDate);

                    if (initialDate <= grandfatherDate)
                    {
                        if (initialDate == DateTime.MinValue)
                        {
                            writeEventLog = true;
                            debug += "subLastInitPurchaseDate == DateTime.MinValue\n";

                            AccessPrivilege ap = null;
                            ap = fromMember.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);

                            if (ap != null)
                            {
                                if (ap.InsertDatePST <= grandfatherDate)
                                {
                                    isGrandfathered = true;
                                }
                            }

                            /*
                            Spark.Common.AccessService.AccessTransaction[] accessTrans = Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstanceForBedrock().GetAccessTransactionHistory(fromMember.MemberID, senderBrand.Site.SiteID, 1, 100);
                            if (accessTrans != null && accessTrans.Length > 0)
                            {
                                debug += "accessTrans.Length=" + accessTrans.Length + "\n";
                                AccountHistory latestAccountHistory = new AccountHistory(accessTrans[0], senderBrand.BrandID, AccountHistoryType.NonFinancial);
                                foreach (Spark.Common.AccessService.AccessTransaction tran in accessTrans)
                                {
                                    AccountHistory accountHistory = new AccountHistory(tran, senderBrand.BrandID, AccountHistoryType.NonFinancial);
                                    if (latestAccountHistory.InsertDateInPST < accountHistory.InsertDateInPST)
                                    {
                                        latestAccountHistory = accountHistory;
                                    }
                                }

                                if (latestAccountHistory != null)
                                {
                                    debug += "latestAccountHistory: " + latestAccountHistory.InsertDateInPST.ToString() +
                                             "\n";
                                    if (latestAccountHistory.InsertDateInPST <= grandfatherDate)
                                    {
                                        isGrandfathered = true;
                                    }
                                }
                            }
                            */
                        }
                        else
                        {
                            isGrandfathered = true;
                        }

                    }
                }
            }

            if (writeEventLog)
            {
                // EventLog.WriteEntry("InboxShutdown IsSenderGrandfathered", debug + "isGrandfathered=" + isGrandfathered.ToString(), EventLogEntryType.Information);
            }

            return isGrandfathered;
        }

        private class InboxShutdownTargeting
        {
            public class TargetingRule
            {
                public int SubStatus { get; set; }
                public int DaysSinceReg { get; set; }
                public int DaysSinceRegOperatorFlag { get; set; }
                public int DaysSinceSub { get; set; }
                public int DaysSinceSubOperatorFlag { get; set; }
                public string Gender { get; set; } // m/f/both
            }

            [Flags]
            private enum RelationalOperators
            {
                Equalto = 1,
                GreaterThan = 2,
                LessThan = 4
            }

            private string _targetingString;
            private ContextGlobal g;

            private List<TargetingRule> _Rules;

            public InboxShutdownTargeting(ContextGlobal _g)
            {
                g = _g;
                _targetingString = RuntimeSettings.GetSetting("INBOX_SHUTDOWN_TARGETING",
                                                              g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID,
                                                              g.Brand.BrandID);
                _Rules = new List<TargetingRule>();
                ParseTargetingString();
            }

            public bool IsTargeted()
            {
                bool isTargeted = false;

                if (!MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                {
                    int subStatus = FrameworkGlobals.GetMemberSubcriptionStatus(g.Member.MemberID, g.Brand);
                    string genderMask = Convert.ToString(g.Member.GetAttributeInt(g.Brand, "Gendermask"));
                    string gender = "m";
                    switch (genderMask)
                    {
                        case "6":
                            gender = "f";
                            break;
                        case "10":
                            gender = "f";
                            break;
                        case "5":
                            gender = "m";
                            break;
                        case "9":
                            gender = "m";
                            break;
                    }

                    DateTime regDate = Convert.ToDateTime(g.Member.GetAttributeDate(g.Brand, "BrandInsertDate"));
                    int daysSinceReg = DateTime.Now.Subtract(regDate).Days;
                    int daysSinceSub =
                        (g.Member.GetAttributeDate(g.Brand, "SubscriptionExpirationDate") - DateTime.Now).Days;

                    int i = 0;
                    while (isTargeted == false && i < _Rules.Count)
                    {
                        TargetingRule tr = _Rules[i];
                        if (subStatus == tr.SubStatus)
                        {
                            if (gender == tr.Gender || tr.Gender == "both")
                            {
                                if (CheckIntAttribute(daysSinceReg, tr.DaysSinceReg, (int)tr.DaysSinceRegOperatorFlag) || CheckIntAttribute(daysSinceSub, tr.DaysSinceSub, (int)tr.DaysSinceSubOperatorFlag))
                                {
                                    isTargeted = true;
                                }
                            }
                        }

                        i++;
                    }
                }

                return isTargeted;
            }

            private bool CheckIntAttribute(int memValue, int ruleValue, int ruleOperatorFlag)
            {
                bool result = false;

                if ((ruleOperatorFlag & (int)RelationalOperators.Equalto) == (int)RelationalOperators.Equalto)
                {
                    if (memValue == ruleValue)
                        return true;
                }

                if ((ruleOperatorFlag & (int)RelationalOperators.GreaterThan) == (int)RelationalOperators.GreaterThan)
                {
                    if (memValue > ruleValue)
                        return true;
                }

                if ((ruleOperatorFlag & (int)RelationalOperators.LessThan) == (int)RelationalOperators.LessThan)
                {
                    if (memValue < ruleValue)
                        return true;
                }

                return result;
            }

            private void ParseTargetingString()
            {
                string[] rules = _targetingString.Split(new char[] { ',' });
                foreach (string rule in rules)
                {
                    TargetingRule targetingRule = new TargetingRule();
                    string trimmedRule = rule.Trim(new char[] { '(', ')' });
                    string[] attributes = trimmedRule.Split(new char[] { '/' });
                    targetingRule.SubStatus = int.Parse(attributes[0]);
                    targetingRule.Gender = attributes[3];

                    int daysSinceReg;
                    int daysSinceRegOperatorFlag;


                    ParseAttributeAndOperator(attributes[1], out daysSinceReg,
                                               out daysSinceRegOperatorFlag);

                    targetingRule.DaysSinceReg = daysSinceReg;
                    targetingRule.DaysSinceRegOperatorFlag = daysSinceRegOperatorFlag;

                    int daysSinceSub;
                    int daysSinceSubOperatorFlag;

                    ParseAttributeAndOperator(attributes[2], out daysSinceSub,
                                               out  daysSinceSubOperatorFlag);

                    targetingRule.DaysSinceSub = daysSinceSub;
                    targetingRule.DaysSinceSubOperatorFlag = daysSinceSubOperatorFlag;

                    _Rules.Add(targetingRule);
                }
            }

            private void ParseAttributeAndOperator(string attribute, out int days, out int operatorFlag)
            {
                operatorFlag = (int)RelationalOperators.Equalto;
                if (attribute.Contains("<"))
                {
                    operatorFlag = (int)RelationalOperators.LessThan;
                    attribute = attribute.Replace("<", string.Empty);
                }
                if (attribute.Contains(">"))
                {
                    operatorFlag = (int)RelationalOperators.GreaterThan;
                    attribute = attribute.Replace(">", string.Empty);
                }
                if (attribute.Contains("="))
                {
                    operatorFlag += (int)RelationalOperators.Equalto;
                    attribute = attribute.Replace("=", string.Empty);
                }

                days = Convert.ToInt32(attribute.Trim());
            }
        }
    }
}


