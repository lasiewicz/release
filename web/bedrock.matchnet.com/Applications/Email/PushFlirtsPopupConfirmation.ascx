﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PushFlirtsPopupConfirmation.ascx.cs" Inherits="Matchnet.Web.Applications.Email.PushFlirtsPopupConfirmation" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<asp:Panel ID="pnlConfirmationSuccess" runat="server" Visible="false">
    <mn:txt runat="server" id="htmlConfirmationSuccess" resourceconstant="HTML_CONFIRMATION_SUCCESS" expandimagetokens="true" />
</asp:Panel>

<asp:Panel ID="pnlQuotaReached" runat="server" Visible="false">
    <mn:txt runat="server" id="htmlQuotaReached" resourceconstant="HTML_QUOTA_REACHED" expandimagetokens="true" />
</asp:Panel>

<asp:Panel ID="pnlNotEnoughResults" runat="server" Visible="false">
   <mn:txt runat="server" id="htmlNoEnoughResults" resourceconstant="HTML_NOT_ENOUGH_RESULTS" expandimagetokens="true" /> 
</asp:Panel>