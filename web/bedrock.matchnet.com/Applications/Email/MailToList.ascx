<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="noResults" Src="../../Framework/Ui/BasicElements/NoResults.ascx" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/BasicElements/ResultList.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MailToList.ascx.cs" Inherits="Matchnet.Web.Applications.Email.MailToList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div id="rightNew">
	<script type="text/javascript">
	function SelectMemberToEmailFromHotlist()
	{
		var lngToMemberID;
		var blnFound = false;
		if (document.forms[HTML_FORM_ID].rdbSelected.length ==null)
		{
				if (document.forms[HTML_FORM_ID].rdbSelected.checked) {
					lngToMemberID = document.forms[HTML_FORM_ID].rdbSelected.value;
					blnFound = true;
				}
		}
		else{
			for (i = 0; i < document.forms[HTML_FORM_ID].rdbSelected.length; i++) {
				if (document.forms[HTML_FORM_ID].rdbSelected[i].checked) {
					lngToMemberID = document.forms[HTML_FORM_ID].rdbSelected[i].value;
					blnFound = true;
				}
			}
		}

		if (blnFound == false) {
			alert("Please select a Member first. To select a member, click on the radio button to the left of the Member.");
			return;
		}
		
		window.opener.document.forms[HTML_FORM_ID].<asp:literal id="litToTextFieldID" runat="server" />.value= lngToMemberID;
		window.close();
	}
	</script>
	
	<table width="100%" class="frame" cellpadding="0" cellspacing="0">
		<tr>
			<td>Show Me
				<asp:DropDownList ID="ddCategoryID" EnableViewState="True" Runat="server" AutoPostBack="True" />
			</td>
			<td align="right">
				<table height="21" width="138" background="/img/d/12/button_shade.gif" class="stdButton"
					cellpadding="0" cellspacing="0">
					<tr>
						<td nowrap align="center" valign="middle">
							<a href="javascript:SelectMemberToEmailFromHotlist();"
								class="btnLink">
								<div width="138">Select Member</div>
							</a>
						</td>
					</tr>
				</table>
			</td>
			<td width="23"><img src="/img/d/12/27/arrow_down.gif" width="23" height="19" alt=" [arrow_down.gif]">
			</td>
		</tr>
		<tr>
			<td colspan="3" class="profile"><asp:Label Runat="server" id="Label1"></asp:Label></td>
		</tr>
	</table>
	<br />
	<result:ResultList id="ResultHotList" runat="server" EnableSingleSelect="True" ResultListContextType="HotList" />
	<mn:noResults runat="server" id="NoSearchResults" visible="False" />
	<table width="100%" class="frame" cellpadding="0" cellspacing="0">
		<tr>
			<td align="right">
				<table height="21" width="138" background="/img/d/12/button_shade.gif" class="stdButton"
					cellpadding="0" cellspacing="0">
					<tr>
						<td nowrap align="center" valign="middle"><a href="javascript:SelectMemberToEmailFromHotlist();"
								class="btnLink"><div width="138">Select Member</div>
							</a>
						</td>
					</tr>
				</table>
			</td>
			<td width="23">
				<img src="/img/d/12/27/arrow_up.gif" width="23" height="19" alt=" [arrow_up.gif]">
			</td>
		</tr>
		<tr>
			<td colspan="2" class="profile"><asp:Label Runat="server" id="Label2"></asp:Label></td>
		</tr>
	</table>
</div>
