﻿#region System References

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.JavaScript;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Util;

#endregion

#region Matchnet Web App References
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.Web.Applications.UserNotifications.Controls;
using Matchnet.EmailTracker.ServiceAdapters;
#endregion

#region Matchnet Service References

#endregion

namespace Matchnet.Web.Applications.Email
{
    public class ComposeHandler : FrameworkControl
    {
        private ICompose _composeControl;
        private ContextGlobal g;
        public const int MAX_REPLY_DAYS = 30;
        private int _replyDraftID = Constants.NULL_INT;
        private string ToUserName = "";
        public bool VIPFreeReply = false;
        public bool VIPMail = false;
        private int draftMemberMailID = int.MinValue;
        private bool IsPostBack = false;
        public bool IsReply = false;
        private static LoggingManager _logManager = null;
        private static SettingsManager _settingsManager = null;
        private MailType newMessageMailType = MailType.Email;

        public static LoggingManager LogManager
        {
            get
            {
                if (null == _logManager)
                {
                    _logManager = new LoggingManager();
                }
                return _logManager;
            }
            set { _logManager = value; }
        }

        public static SettingsManager SettingsManagerObj
        {
            get
            {
                if (null == _settingsManager) _settingsManager = new SettingsManager();
                return _settingsManager;
            }
            set { _settingsManager = value; }
        }

        public int DraftMemberMailID
        {
            get { return draftMemberMailID; }
            set { draftMemberMailID = value; }
        }

        public bool _isCalledFromViewMessagePage = false;

        private TextBox _subjectField = null;

        public TextBox TxtSubjectField
        {
            get { return _subjectField; }
        }

        private bool? nonSubCanSaveAsDraft;

        public ComposeHandler(ICompose control, ContextGlobal context)
        {
            _composeControl = control;
            g = context;
        }

        public bool CalledFromViewMessagePage
        {
            get { return _isCalledFromViewMessagePage; }
            set { _isCalledFromViewMessagePage = value; }
        }

        public bool CanNonSubSendFreeEmail
        {
            get
            {
                return canNonSubSendFreeEmail(g);
            }
        }

        public static bool canNonSubSendFreeEmail(ContextGlobal _g)
        {
            bool retVal = false;

            bool isSettingEnabled = Conversion.CBool(RuntimeSettings.GetSetting("NONSUB_CAN_SEND_FREE_EMAIL",
                                                                           _g.Brand.Site.Community.CommunityID,
                                                                           _g.Brand.Site.SiteID, _g.Brand.BrandID));
            if (isSettingEnabled)
            {
                // make sure member never had subscription
                if (!FrameworkGlobals.IfHadSubscription(_g))
                {
                    // check if the feature is enabled for this memberID
                    if (NonSubSendFreeEmailFeatureThrottler.IsFeatureEnabled(_g.Member, _g))// MID check
                    {
                        // check to see if 'n' days have passed since registration date
                        DateTime registrationDate = _g.Member.GetAttributeDate(_g.Brand, "BrandInsertDate");
                        int daysForRegistrationFactor =
                                Convert.ToInt32(RuntimeSettings.GetSetting("NONSUB_CAN_SEND_FREE_EMAIL_REGISTRATION_DAYS",
                                                               _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID,
                                                               _g.Brand.BrandID));
                        int daysSinceReg = DateTime.Now.Subtract(registrationDate).Days;
                        if (daysSinceReg == daysForRegistrationFactor)
                        {
                            // check to see if the member hasn't sent too many emails
                            Int32 MaxSendFreeEmailLimit = Conversion.CInt(RuntimeSettings.GetSetting("NONSUB_CAN_SEND_FREE_EMAIL_MAX_COUNT_LIMIT",
                                                                               _g.Brand.Site.Community.CommunityID,
                                                                               _g.Brand.Site.SiteID, _g.Brand.BrandID));
                            Int32 totalSent = _g.Member.GetAttributeInt(_g.Brand, "SentFreeEmailCount");
                            if (MaxSendFreeEmailLimit > totalSent)
                            {
                                retVal = true;
                            }
                        }
                    }
                }
            }

            return retVal;
        }

        public bool CanNonSubSaveAsDraft
        {
            get { return canNonsubSaveAsDraft(g); }
        }

        public static bool canNonsubSaveAsDraft(ContextGlobal _g)
        {
            bool cansavedraft = false;

            if (_g != null && _g.Member != null)
            {
                cansavedraft = !_g.Member.IsPayingMember(_g.Brand.Site.SiteID)
                               &&
                               Conversion.CBool(RuntimeSettings.GetSetting("COMPOSE_SAVE_DRAFT_BFR_SUB",
                                                                           _g.Brand.Site.Community.CommunityID,
                                                                           _g.Brand.Site.SiteID, _g.Brand.BrandID))
                               && NonSubMessageBlockFeatureThrottler.IsFeatureEnabled(_g.Member, _g);
                // if allowed to save as draft

                if (cansavedraft)
                {
                    if (FrameworkGlobals.IfHadSubscription(_g))
                    {
                        if (
                            Conversion.CBool(RuntimeSettings.GetSetting("ALLOW_NON_SUB_FORMER_SUB_TO_SAVE_AS_DRAFT",
                                                                        _g.Brand.Site.Community.CommunityID,
                                                                        _g.Brand.Site.SiteID, _g.Brand.BrandID)))
                        {
                            cansavedraft = true;
                        }
                        else
                        {
                            cansavedraft = false;
                        }
                    }
                }
            }

            return cansavedraft;
        }

        public void InitPage()
        {
            if (_composeControl.IsPagePostBack || true)
            {
                // on a page init, repopulate any existing component entries
                if (HttpContext.Current.Request.Params.Get("Application:toTextField") != null &&
                    HttpContext.Current.Request.Params.Get("Application:toTextField").Trim().Length > 0)
                {
                    _composeControl.TxtToTextField.Text =
                        HttpContext.Current.Request.Params.Get("Application:toTextField").Trim();
                }
                if (HttpContext.Current.Request.Params.Get("Application:subjectTextField") != null &&
                    HttpContext.Current.Request.Params.Get("Application:subjectTextField").Trim().Length > 0)
                {
                    _composeControl.TxtSubjectTextField.Text =
                        HttpContext.Current.Request.Params.Get("Application:subjectTextField").Trim();
                }
                if (HttpContext.Current.Request.Params.Get("Application:messageBodyTextField") != null &&
                    HttpContext.Current.Request.Params.Get("Application:messageBodyTextField").Trim().Length > 0)
                {
                    _composeControl.TxtMessageBodyTextField.Text =
                        HttpContext.Current.Request.Params.Get("Application:messageBodyTextField").Trim();
                }

            }

            // TT16864 - set default cursor location in message body textbox
            if (HttpContext.Current.Request.Params.Get("ReplyMailID") != null && _composeControl.FefControl != null)
            {
                _composeControl.FefControl.ElementClientId = _composeControl.TxtMessageBodyTextField.ClientID;
            }
            else if (_composeControl.FefControl != null)
            {
                _composeControl.FefControl.ElementClientId = _composeControl.TxtSubjectTextField.ClientID;
            }

            //set nophotorequest mailtype for new message
            if (MessageManager.Instance.IsNoPhotoRequestMessageEnabled(g.Brand) && !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_MAILTYPE]))
            {
                int mailTypeID = 0;
                int.TryParse(HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_MAILTYPE], out mailTypeID);
                if (mailTypeID == (int)MailType.NoPhotoRequest)
                {
                    newMessageMailType = MailType.NoPhotoRequest;
                }
            }

        }

        public void ValidMemberAccess()
        {

            int replyId = getReplyId();
            int messageId = getMessageId();
            EmailMessage aMessage = null;
            TimeSpan timeSpan = TimeSpan.MinValue;
            bool subToMember = false;
            bool permitmember = false;

            _composeControl.MemberValid = ComposeHelper.PassesSendNonReplyIMailCheck(g);
            permitmember = _composeControl.MemberValid;

            if (replyId > 0)
            {
                aMessage = EmailMessageSA.Instance.RetrieveMessage(g.Brand.Site.Community.CommunityID,
                    g.TargetMemberID,
                    replyId,
                    null,
                    Direction.None,
                    false,
                    g.Brand);
            }
            else if (messageId > 0)
            {
                aMessage = EmailMessageSA.Instance.RetrieveMessage(g.Brand.Site.Community.CommunityID,
                    g.TargetMemberID,
                    messageId,
                    null,
                    Direction.None,
                    false,
                    g.Brand);
            }

            if (aMessage != null)
            {
                // if the aMessage object is populated, we can assume that this is a reply
                _composeControl.IsReply = true;

                Member.ServiceAdapters.Member targetToMember = null;

                _replyDraftID = aMessage.MemberMailID;

                if (aMessage.MemberFolderID == (int)SystemFolders.Draft)
                    targetToMember = MemberSA.Instance.GetMember(aMessage.ToMemberID, MemberLoadFlags.None);
                else
                    targetToMember = MemberSA.Instance.GetMember(aMessage.FromMemberID, MemberLoadFlags.None);

                subToMember = MemberPrivilegeAttr.IsPermitMember(targetToMember, g);

                // If the message type is an email, then check further on to see if access is allowed.  
                bool teaseReply = aMessage.MailTypeID == MailType.Tease;
                if (aMessage.MailTypeID == MailType.Email
                    || aMessage.MailTypeID == MailType.PrivatePhotoEmail
                    || aMessage.MailTypeID == MailType.MissedIM 
                    || teaseReply
                    || aMessage.MailTypeID == MailType.NoPhotoRequest)
                {
                    if (!g.TargetBrand.IsPayToReplySite && !teaseReply)
                    {
                        timeSpan = DateTime.Now.Subtract(aMessage.InsertDate);
                        if (timeSpan.Days < MAX_REPLY_DAYS)
                        {
                            _composeControl.MemberValid = true;
                        }
                    }

                    if ((aMessage.StatusMask & MessageStatus.Show) == MessageStatus.Show &&
                        (aMessage.StatusMask & MessageStatus.Replied) == MessageStatus.Nothing)
                    {
                        if (Convert.ToBoolean(RuntimeSettings.GetSetting("SUB_PAY_TO_REPLY_LIGHT_SITE_NO_RESTRICTION", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
                        {
                            _composeControl.MemberValid = _composeControl.MemberValid | subToMember;
                        }

                        if (!_composeControl.MemberValid && !teaseReply &&
                            Convert.ToBoolean(RuntimeSettings.GetSetting("SUBSCRIPTION_PAY_TO_REPLY_LIGHT_SITE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
                        {
                            Member.ServiceAdapters.Member member =
                                MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                                MemberLoadFlags.None);

                            RegionLanguage memberRegion = RegionSA.Instance.RetrievePopulatedHierarchy(member.GetAttributeInt(g.Brand, "RegionID"), g.Brand.Site.LanguageID);

                            if (memberRegion.CountryRegionID == g.Brand.Site.DefaultRegionID)
                            {
                                Member.ServiceAdapters.Member targetMember =
                                    MemberSA.Instance.GetMember(g.TargetMemberID,
                                    MemberLoadFlags.None);

                                RegionLanguage targetMemberRegion = RegionSA.Instance.RetrievePopulatedHierarchy(targetMember.GetAttributeInt(g.Brand, "RegionID"), g.Brand.Site.LanguageID);
                                if (targetMemberRegion.CountryRegionID == g.Brand.Site.DefaultRegionID)
                                {
                                    _composeControl.MemberValid = true;
                                }
                            }
                        }
                    }
                }
            }

            if (!_composeControl.MemberValid && VIPMailUtils.IsVIPEnabledSite(g.Brand))
            {
                if (aMessage != null && aMessage.MemberFolderID == (int)SystemFolders.VIPInbox)
                {
                    Member.ServiceAdapters.Member fromMember = MemberSA.Instance.GetMember(aMessage.FromMemberID, MemberLoadFlags.None);
                    if (fromMember != null)
                    {
                        VIPMessageAccess vipaccess = VIPMailUtils.GetVIPMessageAccess(g, aMessage);
                        if ((vipaccess & VIPMessageAccess.reply) == VIPMessageAccess.reply)
                            _composeControl.MemberValid = true;
                    }
                }
            }

            if (!_composeControl.MemberValid)
            {
                if (g.Member != null && !MemberPrivilegeAttr.IsPermitMember(g))
                {
                    LinkParent linkParent;
                    if (HttpContext.Current.Request["LinkParent"] != null)
                    {
                        linkParent = (LinkParent)Conversion.CInt(HttpContext.Current.Request["LinkParent"]);
                    }
                    else
                    {
                        linkParent = LinkParent.FullProfile;
                    }

                    int prtid = Constants.NULL_INT;
                    switch (linkParent)
                    {
                        case LinkParent.MiniProfile:
                            prtid = (int)PurchaseReasonType.AttemptToEmailMiniProfile;
                            break;
                        case LinkParent.GalleryMiniProfile:
                            prtid = (int)PurchaseReasonType.AttemptToEmailGalleryMiniProfile;
                            break;
                        case LinkParent.FullProfile:
                            prtid = (int)PurchaseReasonType.AttemptToEmailFullProfile;
                            break;
                        case LinkParent.HomePageMicroProfile:
                            prtid = (int)PurchaseReasonType.AttemptToEmailHomePageMicroProfile;
                            break;
                        case LinkParent.HomePageSpotlightProfile:
                            prtid = (int)PurchaseReasonType.AttemptToEmailHomePageSpotlightProfile;
                            break;
                        case LinkParent.HomePageHeroProfile:
                            prtid = (int)PurchaseReasonType.AttemptToEmailHomePageHeroProfile;
                            break;
                        case LinkParent.HomePageMicroProfileMatches:
                            prtid = (int)PurchaseReasonType.AttemptToEmailHomePageMatches;
                            break;
                        case LinkParent.MiniProfileTop:
                            prtid = (int)PurchaseReasonType.AttemptToEmailMiniProfileBIG;
                            break;
                        case LinkParent.GalleryMiniProfileTop:
                            prtid = (int)PurchaseReasonType.AttemptToEmailGalleryMiniProfileBIG;
                            break;
                        case LinkParent.MatchMeterOneOnOne:
                            prtid = (int)PurchaseReasonType.JmeterOneOnOneEmailAttempt;
                            break;
                        case LinkParent.QuestionAnswerEmailButton:
                            prtid = (int)PurchaseReasonType.QuestionAnswerEmailButton;
                            break;
                    }

                    //pass prtid through from url if it exists
                    if (!string.IsNullOrEmpty(HttpContext.Current.Request["PrtId"]))
                    {
                        int prtIdNum = int.MinValue;
                        try
                        {
                            prtIdNum = Int32.Parse(HttpContext.Current.Request["PrtId"]);
                        }
                        catch (Exception ex) { }

                        if (prtIdNum != int.MinValue)
                        {
                            prtid = prtIdNum;
                        }
                    }

                    bool allowSaveDraft = Conversion.CBool(RuntimeSettings.GetSetting("COMPOSE_SAVE_DRAFT_BFR_SUB", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    //xxxxx
                    if (allowSaveDraft && subToMember && ((aMessage.StatusMask & MessageStatus.Replied) != MessageStatus.Replied))
                    {
                        _composeControl.CbSaveCopyCheckBox.Visible = false;
                        _composeControl.CbSendPrivatePhotos.Visible = false;
                        _composeControl.BtnSaveDraftButton.Visible = false;
                        if (_composeControl.RowSaveCopyCheckBox != null)
                            _composeControl.RowSaveCopyCheckBox.Visible = false;
                        _composeControl.BtnSendMessageButton.Text = g.GetResource("TXT_CONTINUE", _composeControl.ResourceControl);
                    }
                    // Allow non-subs to enter a message, then display subscription block / MPR-2788 
                    // Also allow VIP message be viewed unlimited number of times and saved as draft accordingly
                    else if (CanNonSubSaveAsDraft || (aMessage != null && aMessage.MemberFolderID == (int)SystemFolders.VIPInbox))
                    {
                        _composeControl.CbSaveCopyCheckBox.Visible = false;
                        _composeControl.CbSendPrivatePhotos.Visible = false;
                        _composeControl.BtnSaveDraftButton.Visible = false;
                        if (_composeControl.RowSaveCopyCheckBox != null)
                            _composeControl.RowSaveCopyCheckBox.Visible = false;
                        _composeControl.BtnSendMessageButton.Text = g.GetResource("TXT_CONTINUE", _composeControl.ResourceControl);
                    }
                    else
                    {
                        if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL ||
                            g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                        {
                            if (!CalledFromViewMessagePage)
                            {
                                Redirect.Subscription(g.Brand, prtid, GetMemberId(), false,
                                                      HttpContext.Current.Server.UrlEncode(
                                                          HttpContext.Current.Items["FullURL"].ToString()));
                            }
                        }
                        else
                        {
                            Redirect.Subscription(g.Brand, prtid, GetMemberId(), false,
                                                  HttpContext.Current.Server.UrlEncode(
                                                      HttpContext.Current.Items["FullURL"].ToString()));
                        }
                    }
                }
            }
            if (aMessage != null)
            {
                if ((aMessage.MemberFolderID == (int)SystemFolders.Draft || (aMessage.StatusMask & MessageStatus.SavedToDraft) == MessageStatus.SavedToDraft) && !permitmember)
                    _composeControl.BtnSaveDraftButton.Visible = false;
            }

        }

        public void LoadPage(bool IsPostBack)
        {
            this.IsPostBack = IsPostBack;

            Member.ServiceAdapters.Member _Member = null;
            EmailMessage aMessage = null;
            int _ReplyMailID = 0;
            int _MemberMailID = 0;
            int _MemberID = 0;
            int _MailID = 0;

            _ReplyMailID = getReplyId();
            _MemberMailID = getMessageId();
            _MemberID = GetMemberId();
            _MailID = getMailID();

            if (CalledFromViewMessagePage)
            {
                _ReplyMailID = _MemberMailID;
                _MemberMailID = 0;
            }
            _composeControl.HrefMailToList.HRef = "javascript:launchWindow('/Applications/Email/MailToList.aspx?ToTextFieldID=" + _composeControl.TxtToTextField.ClientID + "', 'View Profile',650,650,'')";

            // if a memberid was specified in the request, display the proper components
            if (_MemberID > 0 && !(_MemberMailID > 0))
            {
                _Member = MemberSA.Instance.GetMember(_MemberID
                    , MemberLoadFlags.None);

                displayMessage(null, _MemberID, _Member.GetUserName(_g.Brand));
            }
            // if a membermailID was specified in the request, a draft compose was requested
            else if (_MemberMailID > 0)
            {
                aMessage = EmailMessageSA.Instance.RetrieveMessage(g.Brand.Site.Community.CommunityID,
                    g.TargetMemberID,
                    _MemberMailID,
                    null,
                    Direction.None,
                    false,
                    g.Brand);


                if (!IsPostBack && aMessage != null)
                {

                    _composeControl.TxtToTextField.Text = aMessage.ToMemberID.ToString();
                    displayMessage(aMessage, 0, null);
                }
            }
            else if (CalledFromViewMessagePage)
            {
                Direction dir = Direction.None;
                if (HttpContext.Current.Request.QueryString["PageDirection"] != null && HttpContext.Current.Request.QueryString["PageDirection"].Trim().Length > 0)
                {
                    try
                    {
                        dir = (Direction)int.Parse(HttpContext.Current.Request.QueryString["PageDirection"]);
                    }
                    catch
                    {
                        dir = Direction.None;
                    }
                }

                string orderby = "";
                if (HttpContext.Current.Request.QueryString["OrderBy"] != null && HttpContext.Current.Request.QueryString["OrderBy"].Trim().Length > 0)
                {
                    orderby = HttpContext.Current.Request.QueryString["OrderBy"].Trim();
                }

                aMessage = EmailMessageSA.Instance.RetrieveMessage(g.Brand.Site.Community.CommunityID,
                    g.TargetMemberID,
                    _ReplyMailID,
                    orderby,
                    dir,
                    true,
                    g.Brand);


                if (aMessage != null)
                {
                    _composeControl.TxtToTextField.Text = aMessage.FromMemberID.ToString();
                    displayMessage(aMessage, aMessage.FromMemberID, aMessage.FromUserName);
                }
            }
            // if a replyMailID was specified in the request, a message reply was requested
            else if (_ReplyMailID > 0)
            {
                aMessage = EmailMessageSA.Instance.RetrieveMessage(g.Brand.Site.Community.CommunityID,
                    g.TargetMemberID,
                    _ReplyMailID,
                    null,
                    Direction.None,
                    true,
                    g.Brand);


                if (aMessage != null)
                {
                    _composeControl.TxtToTextField.Text = aMessage.FromMemberID.ToString();
                    displayMessage(aMessage, aMessage.FromMemberID, aMessage.FromUserName);
                }
            }
            else
            {
                if (_composeControl.ViewProfileTable != null)
                    _composeControl.ViewProfileTable.Visible = false;

                _composeControl.RptMemberProfile.Visible = false;

                if (_composeControl.TblComposeMessage != null)
                    _composeControl.TblComposeMessage.Visible = false;

                if (_composeControl.TblToHeader != null)
                    _composeControl.TblToHeader.Visible = false;

                // only display the compose message table if the current member is an Admin
                if (g.IsAdmin)
                {
                    if (_composeControl.TblComposeMessage != null)
                    {
                        _composeControl.TblComposeMessage.Visible = true;
                        _composeControl.TblComposeMessage.FindControl("toMemberRow").Visible = true;
                    }
                }
                else
                {
                    if (Request.QueryString["rc"] == null)
                    {
                        g.Notification.AddError("TXT_NO_PROFILE_FAILURE");
                    }
                }
            }

            //moving here for compose so that _Member is not null
            if (!_composeControl.MemberValid)
            {
                if (_composeControl.PhRegistrantMessage != null)
                {
                    _composeControl.PhRegistrantMessage.Visible = true;
                }
                //composeMessage.Text="Hey, looks like you're ready to connect with<strong> "+_Member.GetUserName(_g.Brand)+"</strong>. First, write your message below then become a Premium Member so you can send it. It only takes a few moments, and when you're done, you can email any of our members!";
            }
            if (_ReplyMailID > 0)
            {
                ShowVIPAllAccessUI(aMessage, IsPostBack);
            }
            else
            {
                ShowVIPAllAccessUI(null, IsPostBack);
            }



        }

        public bool CompileSendMessage(bool pSaveAsDraft, bool pAdminSend)
        {
            bool result = false;
            try
            {
                string messageSubject = _composeControl.TxtSubjectTextField.Text.Trim();
                string messageBody = _composeControl.TxtMessageBodyTextField.Text.Trim();

                Int32 toMemberID;

                if (pAdminSend)
                {
                    toMemberID = getAdminToField();

                    if (toMemberID == Constants.NULL_INT)
                    {
                        g.Notification.AddError("TXT_MESSAGE_TO_INVALID");
                        return false;
                    }
                }
                else
                {
                    toMemberID = GetMemberId();
                }

                // For some reason, toMemberID never gets populated.  If we don't have it by here, we need to get it.
                if (toMemberID == 0 || toMemberID == Constants.NULL_INT)
                {
                    int memberMailId = getMessageId();
                    if (memberMailId > 0)
                    {

                        var aMessage = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveMessage(g.Brand.Site.Community.CommunityID,
                            g.Member.MemberID,
                            memberMailId,
                            null,
                            Direction.None,
                            false,
                            g.Brand);

                        if (aMessage != null)
                            toMemberID = aMessage.ToMemberID;
                    }
                }

                bool saveCopySent = _composeControl.SaveSendMessage();

                MailType mailType;
                if (_composeControl.SendPrivatePhotos())
                {
                    mailType = MailType.PrivatePhotoEmail;
                }
                else
                {
                    mailType = newMessageMailType;
                }

                ComposeResult composeResult;

                if (CalledFromViewMessagePage)
                {
                    composeResult = ComposeHelper.CompileSendMessage(pSaveAsDraft, pAdminSend,
                        g.Member.MemberID, toMemberID, messageSubject, messageBody, mailType,
                        saveCopySent, getReplyId(), _replyDraftID, getMessageId(), _composeControl.SendAsVIP,
                        _composeControl.ResourceControl, g);
                }
                else
                {
                    composeResult = ComposeHelper.CompileSendMessage(pSaveAsDraft, pAdminSend,
                       g.Member.MemberID, toMemberID, messageSubject, messageBody, mailType,
                       saveCopySent, getMessageId(), _replyDraftID, getReplyId(), _composeControl.SendAsVIP,
                       _composeControl.ResourceControl, g);
                }

                VIPFreeReply = composeResult.VIPFreeReply;
                VIPMail = composeResult.VIPMail;

                if (!composeResult.ComposeSuccess)
                {
                    // one cause of failure is the per day send limit
                    if (composeResult.messageSendResult.Error == MessageSendError.ComposeLimitReached)
                    {
                        // TeaseTooMany is going to use the referring URL value stored in MESSAGE_ORIGIN_URL_SESSION_KEY as it's redirect link.
                        // MESSAGE_ORIGIN_URL_SESSION_KEY was set when this page (Compose) first loads.

                        // Remove session key so it doesn't interfere with TeaseTooMany redirect link creation. Tthis is the result of TeaseTooMany 
                        // being used as the "limit reached" page for both emailing and flirting.
                        if (_g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY] != null)
                        {
                            _g.Session.Remove(Tease.TEASE_ORIGIN_URL_SESSION_KEY);
                        }

                        if (CanNonSubSendFreeEmail)
                        {
                            Redirect.Subscription(g.Brand, (int)PurchaseReasonType.AttemptToEmailFullProfile
                                , toMemberID, false,
                                                  HttpContext.Current.Server.UrlEncode(
                                                      HttpContext.Current.Items["FullURL"].ToString()));
                        }
                        else
                        {
                            _g.Transfer("/Applications/Email/TeaseTooMany.aspx?" + TeaseTooMany20.QSPARAM_ERROR_OCCURRED + "=true&" + TeaseTooMany20.QSPARAM_ERROR_TYPE + "=email");
                        }
                    }

                }
                else
                {
                    DraftMemberMailID = composeResult.membermailid;
                }

                result = composeResult.ComposeSuccess;

            }
            catch (Exception anException)
            {
                g.ProcessException(anException);
            }


            if (!result)
            {
                // deduct from session All Access Overlay Counter
                if (VIPMailUtils.isDisplayOverlayMaxReached(g))
                {
                    g.UpdateSessionInt("VIP_COMPOSE_OVERLAY_PER_SESSION", -1, 0);
                    GetVIPAllAccessOverlay(IsPostBack);
                }
            }
            else
            {
                if (VIPMailUtils.isDisplayOverlayMaxReached(g))
                {
                    g.UpdateSessionInt("VIP_COMPOSE_OVERLAY_PER_SESSION", 1, 0);
                }
            }

            return result;
        }

        public int ApplyIMail()
        {
            int prtid = 0;
            if (ApplyImailPermissions())
            {
                int replyID = getReplyId();

                if (replyID != Constants.NULL_INT)
                {
                    EmailMessage message = EmailMessageSA.Instance.RetrieveMessage(g.Brand.Site.Community.CommunityID,
                        g.TargetMemberID,
                        replyID,
                        null,
                        Direction.None,
                        false,
                        g.Brand);
                    
                    if (message != null)
                    {
                        switch (message.MailTypeID)
                        {
                            case MailType.Tease:
                                prtid = (int)PurchaseReasonType.IMailPermisisonsComposePromptReplyToFlirt;
                                break;

                            default:
                                break;
                        }
                    }
                }
            }

            return prtid;
        }

        public bool ApplyImailPermissions()
        {
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMailPermissions", g.Brand.Site.Community.CommunityID,
                g.Brand.Site.SiteID, g.Brand.BrandID)))
            {
                if (g.TargetBrand.IsPaySite)
                {
                    if (MemberPrivilegeAttr.IsCureentSubscribedMember(g) || g.IsAdmin)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    // free site.
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        // populate and display a memberMail
        private void displayMessage(EmailMessage aMessage, int memberID, string userName)
        {
            Member.ServiceAdapters.Member aMember = null;
            Label toLabel = null;


            toLabel = _composeControl.GetToLabel();
            if (aMessage != null)
            {
                IsReply = true;

                // reply message
                if (memberID > 0 && userName != null)
                {
                    string subjectPrependReply = "";
                    if (!aMessage.Subject.StartsWith("RE:"))
                    {
                        subjectPrependReply = "RE: ";
                    }
                    string bodyPrependReply = Environment.NewLine + Environment.NewLine + g.GetResource("TXT_ORIGINAL_MESSAGE", _composeControl) + Environment.NewLine;

                    aMember = MemberSA.Instance.GetMember(memberID
                        , MemberLoadFlags.None);

                    if (toLabel != null)
                        toLabel.Text = userName;

                    //Get the possessive form of the username (e.g., user's)
                    g.ExpansionTokens.Add("USER", FrameworkGlobals.GetUnicodeText(userName));

                    if (_composeControl.LblMemberProfileUserName != null)
                        _composeControl.LblMemberProfileUserName.Text = g.GetResource("TXT_USER_S", _composeControl.ResourceControl);

                    // actively parse out all html tags for subject
                    _composeControl.TxtSubjectTextField.Text = Regex.Replace(subjectPrependReply + aMessage.Subject, "<[^>]*>", "");

                    // actively parse out all html tags for body content for issue 12939
                    AttributeOptionMailboxPreference mailboxPreference = (AttributeOptionMailboxPreference)g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_MAILBOXPREFERENCE);
                    if ((mailboxPreference & AttributeOptionMailboxPreference.IncludeOriginalMessagesInReplies) == AttributeOptionMailboxPreference.IncludeOriginalMessagesInReplies)
                    {
                        string msgBody = Regex.Replace(bodyPrependReply + HttpUtility.HtmlDecode(aMessage.Content), "<[^>]*>", "");
                        msgBody = Regex.Replace(msgBody, "\\{.*\\}", "");
                        _composeControl.TxtMessageBodyTextField.Text = msgBody;
                    }
                }
                // compose message
                else
                {
                    IsReply = false;

                    aMember = MemberSA.Instance.GetMember(aMessage.ToMemberID
                        , MemberLoadFlags.None);

                    if (toLabel != null)
                        toLabel.Text = aMessage.ToUserName;

                    //Get the possessive form of the username (e.g., user's)
                    g.ExpansionTokens.Add("USER", aMessage.ToUserName);

                    if (_composeControl.LblMemberProfileUserName != null)
                        _composeControl.LblMemberProfileUserName.Text = g.GetResource("TXT_USER_S", _composeControl);

                    // parse out all html tags
                    _composeControl.TxtSubjectTextField.Text = Regex.Replace(FrameworkGlobals.GetUnicodeText(aMessage.Subject), "<[^>]*>", "");
                    //subjectTextField.Text = FrameworkGlobals.GetUnicodeText(aMessage.Subject);
                    _composeControl.TxtMessageBodyTextField.Text = Regex.Replace(FrameworkGlobals.GetUnicodeText(aMessage.Content), "<[^>]*>", "");
                    //messageBodyTextField.Text = FrameworkGlobals.GetUnicodeText(aMessage.Content);
                }
            }
            // new message
            else
            {
                aMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                toLabel.Text = userName;

                //Get the possessive form of the username (e.g., user's)
                g.ExpansionTokens.Add("USER", FrameworkGlobals.GetUnicodeText(userName));
                _composeControl.LblMemberProfileUserName.Text = g.GetResource("TXT_USER_S", _composeControl);

                if (!this.IsPostBack)
                {
                    if (MessageManager.Instance.IsNoPhotoRequestMessageEnabled(g.Brand) && !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_MAILTYPE]))
                    {
                        int mailTypeID = 0;
                        int.TryParse(HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_MAILTYPE], out mailTypeID);
                        if (mailTypeID == (int)MailType.NoPhotoRequest)
                        {
                            //prepopulate subject and body
                            string senderUsername = g.Member.GetUserName(g.Brand);
                            _composeControl.TxtSubjectTextField.Text = string.Format(g.GetResource("TXT_NOPHOTOREQUEST_SUBJECT", _composeControl), senderUsername);
                            _composeControl.TxtMessageBodyTextField.Text = string.Format(g.GetResource("TXT_NOPHOTOREQUEST_BODY", _composeControl), senderUsername);

                        }
                    }
                }
            }
            if (toLabel != null)
                ToUserName = toLabel.Text;
            //PM-50 TL:Hide carrot and classic profile details if site is set to view tabbed profiles
            try
            {
                //hide classic profile
                if (_composeControl.RptMemberProfile != null)
                {
                    _composeControl.RptMemberProfile.Visible = false;
                    _composeControl.PhClassicProfileDetail.Visible = false;

                }
                
                if (aMessage.MemberFolderID == (int) SystemFolders.VIPDraft)
                {
                    int allAccessEmailsLeft = 0;
                    VIPMember vipMember = VIPMailUtils.GetVIPMember(g, g.Member.MemberID);
                    if (vipMember != null)
                    {
                        allAccessEmailsLeft = vipMember.EmailCount;
                    }

                    //check to see if the message was created post cancel link on pui upsell page. 
                    if (HttpContext.Current.Request.QueryString["NoAllAccess"] != null && allAccessEmailsLeft <= 0)
                    {
                        _composeControl.SendAsVIP = false;
                    }
                    else
                        _composeControl.SendAsVIP = true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            if (_composeControl.TblComposeMessage != null)
            {
                _composeControl.TblComposeMessage.Visible = true;
                _composeControl.TblComposeMessage.FindControl("toMemberRow").Visible = false;
            }

            _subjectField = _composeControl.TxtSubjectTextField;

        }

        private Boolean IsMemberOnVacation(Int32 toMemberID)
        {
            return ComposeHelper.IsMemberOnVacation(toMemberID, g.Brand);
        }

        public Int32 getAdminToField()
        {
            Int32 memberID = Constants.NULL_INT;
            string toField = _composeControl.TxtToTextField.Text.Trim();

            memberID = Conversion.CInt(toField);

            if (memberID == Constants.NULL_INT)
            {
                memberID = MemberSA.Instance.GetMemberID(toField, g.Brand.Site.Community.CommunityID);
            }

            return memberID;
        }

        private int getMailID()
        {
            if (HttpContext.Current.Request.Params.Get("MailID") != null)
            {
                return int.Parse(HttpContext.Current.Request.Params.Get("MailID"));
            }
            else
            {
                return Constants.NULL_INT;
            }
        }

        // handles resolving the requested reply message id
        private int getReplyId()
        {
            return Conversion.CInt(HttpContext.Current.Request["ReplyMailID"]);
        }

        // handles resolving the requested message id
        private int getMessageId()
        {
            return Conversion.CInt(HttpContext.Current.Request["MemberMailID"]);
        }

        public int GetMemberId()
        {
            try
            {
                if (HttpContext.Current.Request.Params.Get("MemberID") != null)
                {
                    return int.Parse(HttpContext.Current.Request.Params.Get("MemberID"));
                }
                else if (_composeControl.TxtToTextField.Text.Trim().Length > 0)
                {
                    return int.Parse(_composeControl.TxtToTextField.Text.Trim());
                }
            }
            catch (FormatException)
            {
                return 0;
            }
            return 0;
        }

        public int GetToMemberID(bool isAdminSend)
        {
            int toMemberID = Constants.NULL_INT;
            if (isAdminSend)
            {
                toMemberID = getAdminToField();
            }
            else
            {
                toMemberID = GetMemberId();
            }

            return toMemberID;
        }

        public void ShowVIPAllAccessUI(EmailMessage replyToMessage, bool isPostBack)
        {
            if (VIPMailUtils.IsVIPEnabledSite(g.Brand))
            {
                if (replyToMessage != null && !CalledFromViewMessagePage && (replyToMessage.StatusMask & MessageStatus.OneFreeReply) == MessageStatus.OneFreeReply)
                {
                    _composeControl.PHSendAsVIP.Visible = false;
                    _composeControl.PHSendVIPOverlay.Visible = false;
                    _composeControl.BtnSendMessageButton.Visible = true;
                    _composeControl.BtnSendVIPMessage.Visible = false;
                    _composeControl.CbSaveCopyCheckBox.Visible = false;
                    _composeControl.BtnSaveDraftButton.Visible = false;
                    _composeControl.PHSendAsFREEReplyVIP.Visible = true;
                }
                else if (VIPMailUtils.IsVIPSendEnabledSite(g.Brand) && replyToMessage != null
                    && (replyToMessage.StatusMask & MessageStatus.OneFreeReply) == MessageStatus.OneFreeReply
                    && (replyToMessage.StatusMask & MessageStatus.Replied) != MessageStatus.Replied
                    )
                {
                    _composeControl.PHSendAsVIP.Visible = false;
                    _composeControl.PHSendVIPOverlay.Visible = false;
                    _composeControl.CbSaveCopyCheckBox.Visible = false;
                    //check here if overlays are enabled
                    GetVIPAllAccessOverlay(isPostBack);
                }
                else if (VIPMailUtils.IsVIPSendEnabledSite(g.Brand))
                {
                    _composeControl.PHSendAsVIP.Visible = true;
                    _composeControl.PHSendVIPOverlay.Visible = true;
                    _composeControl.CbSaveCopyCheckBox.Visible = false;
                    //check here if overlays are enabled
                    GetVIPAllAccessOverlay(isPostBack);
                }
                else
                {
                    _composeControl.PHSendAsVIP.Visible = false;
                    _composeControl.PHSendVIPOverlay.Visible = false;
                    _composeControl.BtnSendMessageButton.Visible = true;
                    _composeControl.BtnSendVIPMessage.Visible = false;
                    _composeControl.CbSaveCopyCheckBox.Visible = false;
                    _composeControl.BtnSaveDraftButton.Visible = false;
                    _composeControl.PHSendAsFREEReplyVIP.Visible = false;

                }

            }
            else
            {
                _composeControl.PHSendAsVIP.Visible = false;
                _composeControl.PHSendVIPOverlay.Visible = false;
                _composeControl.BtnSendMessageButton.Visible = true;
                _composeControl.BtnSendVIPMessage.Visible = false;
                //_composeControl.CbSaveCopyCheckBox.Visible = false;
                _composeControl.BtnSaveDraftButton.Visible = false;
                _composeControl.PHSendAsFREEReplyVIP.Visible = false;
            }
        }

        public void GetVIPAllAccessOverlay(bool isPostback)
        {
            try
            {
                // VIPMailUtils.GiveAccessForTesting(g);
                //  VIPMailUtils.GiveAccessCountForTesting(g);

                string vip_overlay_txt = "";
                string vip_btn_yes_text = "";
                string emailcount = "";
                string vipcheckboxlabel = "";
                string vipcheckbox = "";
                bool displayoverlay = false;
                VIPMember vipmember = VIPMailUtils.GetVIPMember(g, g.Member.MemberID);

                if (VIPMailUtils.IsVIPMember(g, vipmember))
                {
                    if (vipmember.EmailCount > 0)
                    {
                        vip_overlay_txt = "TXT_VIP_OVERLAY_USE_VIP";
                        vip_btn_yes_text = "BTN_OVERLAY_YES";
                        displayoverlay = VIPMailUtils.DisplayOverlay(g, VIPOverlayMode.vipuseemail);
                        vipcheckboxlabel = "TXT_USE_YOUR_VIP";
                        vipcheckbox = "CHK_USE_YOUR_VIP";
                    }
                    else
                    {
                        vip_overlay_txt = "TXT_VIP_OVERLAY_UPSELL";
                        vip_btn_yes_text = "BTN_LEARN_MORE";
                        displayoverlay = VIPMailUtils.DisplayOverlay(g, VIPOverlayMode.vipupsell);
                        vipcheckboxlabel = "TXT_USE_YOUR_VIP";
                        vipcheckbox = "CHK_USE_YOUR_VIP";
                    }
                    emailcount = vipmember.EmailCount.ToString();

                }
                else
                {
                    if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                    {
                        vip_overlay_txt = "TXT_VIP_OVERLAY_BUY_VIP";
                        vip_btn_yes_text = "BTN_LEARN_MORE";
                        vipcheckboxlabel = "TXT_SEND_AS_VIP";
                        vipcheckbox = "CHK_SEND_AS_VIP";
                        displayoverlay = VIPMailUtils.DisplayOverlay(g, VIPOverlayMode.subscriberbuy);
                    }
                    else if (CanNonSubSendFreeEmail)
                    {
                        vip_overlay_txt = "TXT_VIP_OVERLAY_BUY_VIP";
                        vip_btn_yes_text = "BTN_LEARN_MORE";
                        vipcheckboxlabel = "TXT_SEND_AS_VIP";
                        vipcheckbox = "CHK_SEND_AS_VIP";
                        displayoverlay = VIPMailUtils.DisplayOverlay(g, VIPOverlayMode.subscriberbuy);
                    }
                    else if (CanNonSubSaveAsDraft) // if allowed to save as draft
                    {
                        vip_overlay_txt = "TXT_VIP_OVERLAY_BUY_VIP";
                        vip_btn_yes_text = "BTN_LEARN_MORE";
                        vipcheckboxlabel = "TXT_SEND_AS_VIP";
                        vipcheckbox = "CHK_SEND_AS_VIP";
                        displayoverlay = VIPMailUtils.DisplayOverlay(g, VIPOverlayMode.subscriberbuy);
                    }
                    else
                    {
                        _composeControl.PHSendAsVIP.Visible = false;
                    }


                }
                if (displayoverlay)
                {
                    _composeControl.BtnSendMessageButton.Visible = false;
                    _composeControl.BtnSendVIPMessage.Visible = true;
                    _composeControl.BtnVIPMessageOverlayYes.Text = g.GetResource(vip_btn_yes_text, _composeControl);
                    _composeControl.BtnVIPMessageOverlayNo.Text = g.GetResource("BTN_OVERLAY_NO", _composeControl);
                }
                //else if (CanNonSubSaveAsDraft)
                //{
                //    _composeControl.BtnSendMessageButton.Visible = false;
                //    _composeControl.BtnSendVIPMessage.Visible = true;
                //    _composeControl.BtnVIPMessageOverlayYes.Text = g.GetResource(vip_btn_yes_text, _composeControl);
                //    _composeControl.BtnVIPMessageOverlayNo.Text = g.GetResource("BTN_OVERLAY_NO", _composeControl);
                //}
                else
                {
                    _composeControl.BtnSendVIPMessage.Visible = false;
                }


                if (vip_overlay_txt != "")
                    _composeControl.VIPOverlayText.Text = g.GetResource(vip_overlay_txt, _composeControl, new string[] { ToUserName, emailcount });

                if (vipcheckboxlabel != "")
                    _composeControl.LBLVIPCheckbox.Text = g.GetResource(vipcheckboxlabel, new string[] { emailcount, emailcount }, true, _composeControl);

                if (vipcheckbox != "")
                    _composeControl.VIPCheckbox.Text = g.GetResource(vipcheckbox, new string[] { emailcount }, true, _composeControl);

                // default binding is checked if member has vip emails not checked if the member isn't an All Access member
                //if (!isPostback)
                //{
                //    _composeControl.VIPCheckbox.Checked = (vipcheckbox == "CHK_USE_YOUR_VIP");
                //}
            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }

        public bool SaveVIPDraft(out int mailid)
        {
            mailid = Constants.NULL_INT;
            try
            {

                string messageSubject = _composeControl.TxtSubjectTextField.Text.Trim();
                string messageBody = _composeControl.TxtMessageBodyTextField.Text.Trim();

                messageSubject = _composeControl.TxtSubjectTextField.Text.Trim();
                messageBody = _composeControl.TxtMessageBodyTextField.Text.Trim();

                if (messageSubject.Length <= 0)
                {
                    g.Notification.AddError("TXT_MESSAGE_SUBJECT_BLANK");
                    return false;
                }

                if (messageBody.Length <= 0)
                {
                    g.Notification.AddError("TXT_MESSAGE_BODY_BLANK");
                    return false;
                }

                messageSubject = Regex.Replace(messageSubject, "<[^>]*>", "");
                messageBody = Regex.Replace(ComposeHelper.formatMessageBody(messageBody), "<[^>]*>", "");
                MessageSave messageSave = new MessageSave(g.Member.MemberID,
                 GetMemberId(),
                 g.Brand.Site.Community.CommunityID,
                 g.Brand.Site.SiteID,
                 newMessageMailType,
                 messageSubject,
                 messageBody);

                messageSave.MessageListID = getMessageId();
                int replyid = getReplyId();

                if (CalledFromViewMessagePage)
                {
                    messageSave.MessageListID = int.MinValue;
                    replyid = getMessageId();
                }

                messageSave.MailOption = messageSave.MailOption | MailOption.VIP;

                int membermailid = 0;
                //modified to account for returnig to save draft after subscription for compose project.
                bool saved = EmailMessageSA.Instance.SaveMessage(messageSave, out membermailid);

                if (saved && replyid > 0)
                {
                    EmailMessageSA.Instance.MarkDraftSaved(g.Member.MemberID, g.Brand.Site.Community.CommunityID, saved, replyid);
                }
                mailid = membermailid;
                return saved;

            }
            catch (Exception ex)
            { g.ProcessException(ex); return false; }


        }

        public static void SendQuickMessage(ContextGlobal _g)
        {
            try
            {
                if (
                    Boolean.Parse(RuntimeSettings.GetSetting("ENABLE_SEARCH_RESULTS_QUICK_MESSAGE",
                                                             _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    // Read cookie
                    HttpCookie cookie = HttpContext.Current.Request.Cookies["QuickMessage"];
                    if (cookie != null)
                    {
                        // Parse cookie
                        string[] values = cookie.Value.Split(new char[] { '&' });
                        Hashtable entries = new Hashtable();
                        foreach (string entry in values)
                        {
                            string[] keyval = entry.Split(new char[] { '=' });
                            if (keyval.Length > 1)
                            {
                                entries.Add(keyval[0], HttpUtility.UrlDecode(keyval[1]));
                            }
                        }

                        string messageSubject = (entries["MessageSubject"] != null) ? entries["MessageSubject"] as string : "";
                        string message = (entries["MessageBody"] != null) ? entries["MessageBody"] as string : "";
                        int targetMemberID = (entries["TargetMemberID"] != null) ? int.Parse(entries["TargetMemberID"] as string) : 0;
                        int senderMemberID = (entries["SenderMemberID"] != null) ? int.Parse(entries["SenderMemberID"] as string) : 0;

                        // Send message
                        if ((_g.Member.MemberID == senderMemberID || senderMemberID == -1) && !string.IsNullOrEmpty(messageSubject) && !string.IsNullOrEmpty(message) &&
                            targetMemberID > 0)
                        {
                            if (ComposeHelper.PassesIMailFraudCheck(_g) &&
                                ComposeHelper.PassesSendNonReplyIMailCheck(_g))
                            {
                                // Delete cookie
                                cookie.Expires = DateTime.Now.AddDays(-2);
                                HttpContext.Current.Response.Cookies.Set(cookie);

                                messageSubject = CleanQuickMessageSubject(messageSubject);
                                message = CleanQuickMessageBody(message);

                                MessageSave messageSave = new MessageSave(_g.Member.MemberID, targetMemberID,
                                                                          _g.Brand.Site.Community.CommunityID,
                                                                          _g.Brand.Site.SiteID,
                                                                          MailType.Email,
                                                                          messageSubject,
                                                                          message);


                                bool blocked = false;
                                try
                                {
                                    var toMember = MemberSA.Instance.GetMember(messageSave.ToMemberID, MemberLoadFlags.None);
                                    MessageSendResult sendResult = EmailMessageSA.Instance.CheckAndSendMessageForBlockedSenderInRecipientList(messageSave.GroupID, string.Format(_g.GetResource("TXT_IGNORE_SUBJECT"), messageSave.MessageHeader), string.Format(_g.GetResource("TXT_IGNORE_CONTENT"), toMember.GetUserName(_g.Brand)),
                                                                                                                                              toMember.MemberID, _g.Member.MemberID, _g.Brand.Site.SiteID,
                                                                                                                                              _g.Brand.Site.Community.CommunityID);
                                    blocked = (null != sendResult && sendResult.Status == MessageSendStatus.Success);
                                    LogIgnoreEMail(_g, _g.Member.MemberID.ToString(), toMember.MemberID.ToString());
                                }
                                catch (Exception e)
                                {
                                    LogManager.LogException(e, _g.Member, _g.Brand, "ComposeHandler");
                                }

                                if (blocked)
                                {
                                    // Add notification  
                                    _g.Notification.AddMessageString(
                                        _g.GetResource("TXT_MESSAGE_SUCCESSFULLY_SENT_TO_USERNAME").Replace(
                                            "((USERNAME))",
                                            MemberSA.Instance.GetMember(targetMemberID, MemberLoadFlags.None).
                                                GetUserName(_g.Brand) ?? targetMemberID.ToString()));
                                }
                                else
                                {
                                    MessageSendResult sendResult = EmailMessageSA.Instance.SendMessage(messageSave, true,
                                                                                                       Constants.
                                                                                                           NULL_INT,
                                                                                                       true);
                                    if (sendResult.Status == MessageSendStatus.Success)
                                    {
                                        // save the to member in the current member's hotlist
                                        ListSA.Instance.AddListMember(HotListCategory.MembersYouEmailed,
                                                                      _g.Brand.Site.Community.CommunityID,
                                                                      _g.Brand.Site.SiteID,
                                                                      _g.Member.MemberID,
                                                                      messageSave.ToMemberID,
                                                                      null,
                                                                      Constants.NULL_INT,
                                                                      true,
                                                                      false);

                                        SendQuickMessageNotification(messageSave, _g);

                                        // Add notification  
                                        _g.Notification.AddMessageString(
                                            _g.GetResource("TXT_MESSAGE_SUCCESSFULLY_SENT_TO_USERNAME").Replace(
                                                "((USERNAME))",
                                                MemberSA.Instance.GetMember(targetMemberID, MemberLoadFlags.None).
                                                    GetUserName(_g.Brand) ?? targetMemberID.ToString()));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }


        public static string CleanQuickMessageSubject(string messageSubject)
        {
            return FrameworkGlobals.DecodeHexEntities(messageSubject);
        }

        public static string CleanQuickMessageBody(string message)
        {
            message = FrameworkGlobals.DecodeHexEntities(message.Trim());
            message = System.Text.RegularExpressions.Regex.Replace(message, "<[^>]*>", "");
            return message;
        }

        public static void LogIgnoreEMail(ContextGlobal _g, string fromMember, string toMember)
        {
            string membersIDs = SettingsManagerObj.GetSettingString("DEBUG_IGNORE_EMAIL_ISSUE", _g.Brand);
            if (!string.IsNullOrEmpty(membersIDs))
            {
                List<string> idsList = new List<string>(membersIDs.Split(','));

                if (idsList.Contains(fromMember) || idsList.Contains(toMember))
                {
                    string message = string.Format("Debug ignore email issue. from member:{0} to member:{1} url:{2} referrer url:{3}",
                    fromMember,
                    toMember,
                   HttpContext.Current.Request.RawUrl,
                   HttpContext.Current.Request.UrlReferrer.AbsolutePath);
                   _g.ProcessException(new Exception(message), EventLogEntryType.Information);
                }
            }
        }

        public static void SendQuickMessageNotification(MessageSave messageSave, ContextGlobal _g)
        {
            if (UserNotificationFactory.IsUserNotificationsEnabled(_g))
            {
                UserNotificationParams unParams = UserNotificationParams.GetParamsObject(messageSave.ToMemberID,
                                                           _g.Brand.Site.SiteID,
                                                           _g.Brand.Site.Community.
                                                               CommunityID);
                UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new EmailedYouUserNotification(), messageSave.ToMemberID, messageSave.FromMemberID, _g);
                if (notification.IsActivated()){
                    UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                }
            }
        }

        
    }
}
