﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Flirt20.ascx.cs" Inherits="Matchnet.Web.Applications.Email.Flirt20" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

	<mn:Title runat="server" id="ttlMatches" ResourceConstant="TXT_SEND_A_TEASE"/>
	<asp:PlaceHolder id="miniProfile" Runat="server" />
	
	<div id="prefTab">
		<asp:Repeater ID="repTeaseCategories" Runat="server">
			<ItemTemplate>
				<asp:HyperLink ID="lnkLink" Runat="server">
					<asp:Label ID="lblText" Runat="server" CssClass="prefTabItem" />
				</asp:HyperLink>
			</ItemTemplate>
		</asp:Repeater>
	</div>
	<div id="flirt-options">
		<asp:RadioButtonList ID="rblTeaseContent" Runat="server" class="margin-medium" />
		<%--<asp:Button ID="btnSubmit" Runat="server" Text="[Tease Away!]" CssClass="activityButton" />--%>
		<div class="promo-insert">
			<mn:Txt ID="txtPromo" runat="server" visible="false" />
		</div>
        <asp:HiddenField ID="hdnSelectedCategoryID" runat="server" />
		<mn2:FrameworkButton runat="server" id="btnSubmit" CssClass="btn primary margin-medium" ResourceConstant="BTN_TEASE_AWAY" />
	</div>
	
 	<mn:txt id="txtCallToAction" runat="server" ExpandImageTokens="true" ResourceConstant="FLIRT_CALL_TO_ACTION" Visible=false />
 	
