﻿#region System References

using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.JavaScript;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Util;

#endregion

#region Matchnet Web App References
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
#endregion


namespace Matchnet.Web.Applications.Email
{
    public partial class ComposeOverlay : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}