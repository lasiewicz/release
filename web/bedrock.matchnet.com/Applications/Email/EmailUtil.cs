﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Framework.Util;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Email
{
    public class EmailUtil
    {
        public static ControlVersion GetMessageSettingsVersion(Member.ServiceAdapters.Member member, Brand brand)
        {
            ControlVersion version = ControlVersion.Current;

            SettingsManager settingManager = new SettingsManager();

            if (settingManager.GetSettingBool("USE_MESSAGESETTINGSPAGE_30", brand))
            {
                version = ControlVersion.Version30;
            }

            return version;
        }
    }
}