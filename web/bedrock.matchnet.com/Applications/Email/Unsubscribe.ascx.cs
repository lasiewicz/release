﻿
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Email
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Matchnet.Web.Framework;
    using Matchnet.Member.ServiceAdapters;
    using Matchnet.Member.ValueObjects;
    using Matchnet.ExternalMail.ValueObjects;
    using Matchnet.Security;

    public class Unsubscribe : FrameworkControl
    {
        #region CONSTANTS
        
        private const int MESSAGE_OPTION_NEVER = 0;
        private const string MESSAGE_OPTED_OUT_NAME = "MESSAGE_OPTED_OUT";
        private const string MESSAGE_OOPS_NAME = "MESSAGE_OOPS";
        private const string MESSAGE_YOU_CAN_ALSO_NAME = "MESSAGE_YOU_CAN_ALSO";
        protected const string MESSAGE_SETTINGS_URL = "/applications/email/messagesettings.aspx";
        
        #endregion

        #region CONTROLS
        
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lbl48Hours;
        protected System.Web.UI.WebControls.Label lblOptedOut;
        protected System.Web.UI.WebControls.Label lblOops;
        protected System.Web.UI.WebControls.Label lblYouCanAlso;
        protected System.Web.UI.WebControls.Button btnRevise;
        
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            const string siteidText = "sid";
            const string encryptedMemberIDText = "emid";
            const string emailTypeText = "et";

            string siteID, encryptedMemberID, emailType, messageTypeText = Constants.NULL_STRING;

            try
            {
                // Validate query parameters
                siteID = String.IsNullOrEmpty(Request[siteidText]) ? Constants.NULL_STRING : Request[siteidText];
                encryptedMemberID = String.IsNullOrEmpty(Request[encryptedMemberIDText]) ? Constants.NULL_STRING : Request[encryptedMemberIDText];
                emailType = String.IsNullOrEmpty(Request[emailTypeText]) ? Constants.NULL_STRING : Request[emailTypeText];

                if(siteID == Constants.NULL_STRING || encryptedMemberID == Constants.NULL_STRING || emailType == Constants.NULL_STRING)
                    throw new ApplicationException("Invalid query string parameter");

                // verify user, and load user object
                Member member = MemberSA.Instance.GetMember(MessageManager.Instance.DecryptMemberID(encryptedMemberID), MemberLoadFlags.None);
                if (member == null)
                {
                    throw new ApplicationException("Invalid MemberID");
                }
                if (!member.GetSiteIDList().Contains<int>(int.Parse(siteID)))
                {
                    throw new ApplicationException("Bad SiteID");
                }
                
                // Calculate value of setting (using tables) and Set appropriate values
                switch (emailType.ToUpper())
                {
                    case "MM":
                        member.SetAttributeInt(g.Brand, "MatchNewsletterPeriod", MESSAGE_OPTION_NEVER);
                        break;
                    case "CM":
                        member.SetAttributeInt(g.Brand, "GotAClickEmailPeriod", MESSAGE_OPTION_NEVER);
                        break;
                    case "CA":
                        MessageManager.Instance.UnmarkMaskValues(member, g.Brand,
                                                                 EmailAlertConstant.ALERT_ATTRIBUTE_NAME,
                                                                 new[]
                                                                     {
                                                                         (int) EmailAlertMask.GotClickAlert
                                                                     });
                        break;
                    case "HL":
                        MessageManager.Instance.UnmarkMaskValues(member, g.Brand,
                                                                 EmailAlertConstant.ALERT_ATTRIBUTE_NAME,
                                                                 new[]
                                                                     {
                                                                         (int) EmailAlertMask.HotListedAlert
                                                                     });
                        break;
                    case "EA":
                        MessageManager.Instance.UnmarkMaskValues(member, g.Brand,
                                                                 EmailAlertConstant.ALERT_ATTRIBUTE_NAME,
                                                                 new[]
                                                                     {
                                                                         (int) EmailAlertMask.NewEmailAlert
                                                                     });
                        break;
                    case "EC":
                        MessageManager.Instance.UnmarkMaskValues(member, g.Brand,
                                                                 EmailAlertConstant.ALERT_ATTRIBUTE_NAME,
                                                                 new[]
                                                                     {
                                                                         (int) EmailAlertMask.ECardAlert
                                                                     });
                        break;
                    case "VY":
                        // This is is backwards; you set the mask value to opt out of it
                        MessageManager.Instance.MarkMaskValues(member, g.Brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME,
                                                               new[]
                                                                   {
                                                                       (int) EmailAlertMask.ProfileViewedAlertOptOut
                                                                   });
                        break;
                    case "RE":
                        MessageManager.Instance.UnmarkMaskValues(member, g.Brand,
                                                                 EmailAlertConstant.ALERT_ATTRIBUTE_NAME,
                                                                 new[]
                                                                     {
                                                                         (int) EmailAlertMask.NoEssayRequestEmail
                                                                     });
                        break;
                    case "NL":
                        // Newsletter and Offers are becoming 1 option now for JDate community only
                        if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
                        {
                            MessageManager.Instance.UnmarkMaskValues(member, g.Brand, "NewsletterMask",
                                                                     new[]
                                                                         {
                                                                             (int) NewsEventOfferMask.News,
                                                                             (int) NewsEventOfferMask.Offers
                                                                         });
                        }
                        else
                        {
                            MessageManager.Instance.UnmarkMaskValues(member, g.Brand, "NewsletterMask",
                                                                     new[]
                                                                         {
                                                                             (int) NewsEventOfferMask.News
                                                                         });
                        }
                        break;
                    case "IE":
                        MessageManager.Instance.UnmarkMaskValues(member, g.Brand, "NewsletterMask", new[]
                            {
                                (int) NewsEventOfferMask.Events
                            });
                        break;
                    case "EO":
                        // Newsletter and Offers are becoming 1 option now for JDate community only
                        if (g.Brand.Site.Community.CommunityID == (int) WebConstants.COMMUNITY_ID.JDate)
                        {
                            MessageManager.Instance.UnmarkMaskValues(member, g.Brand, "NewsletterMask",
                                                                     new[]
                                                                         {
                                                                             (int) NewsEventOfferMask.News,
                                                                             (int) NewsEventOfferMask.Offers
                                                                         });
                        }
                        else
                        {
                            MessageManager.Instance.UnmarkMaskValues(member, g.Brand, "NewsletterMask",
                                                                     new[]
                                                                         {
                                                                             (int) NewsEventOfferMask.Offers
                                                                         });
                        }
                        break;
                }

                // Save data
                MemberSA.Instance.SaveMember(member);
                //g.Notification.AddMessage("EMAIL_SETTINGS_SUCCESSFULLY_SAVED");

                //Display confirmation message
                var messageSettingsURL = MESSAGE_SETTINGS_URL + string.Format("?{0}={1}&{2}={3}", siteidText, siteID, encryptedMemberIDText, HttpUtility.UrlEncode(encryptedMemberID));

                messageTypeText = g.GetResource(emailType + "_TEXT", this);
                lblHeader.Text = g.GetResource("SETTINGS_UPDATED_MESSAGE", this);
                lbl48Hours.Text = g.GetResource("ALLOW_48_HOURS_TO_UPDATE", this);
                lblOops.Text = String.Format(g.GetResource(MESSAGE_OOPS_NAME, this), messageSettingsURL);
                lblOptedOut.Text = String.Format(g.GetResource(MESSAGE_OPTED_OUT_NAME, this), g.GetResource(emailType + "_TEXT", this));
                lblYouCanAlso.Text = String.Format(g.GetResource(MESSAGE_YOU_CAN_ALSO_NAME, this), messageSettingsURL);
                btnRevise.Text = g.GetResource("CTA_TEXT", this);
            }
            catch (ApplicationException)
            {
                //If anything goes wrong, go to message settings page
                Response.Redirect(MESSAGE_SETTINGS_URL);
            }
        }

        #region PRIVATE METHODS

        //private int DecryptMemberID(string encMemberID)
        //{
        //    string decMemberID = Constants.NULL_STRING;
        //    try
        //    {
        //        // Because the Request.QueryString object interprets the '+' sign as a space, we must use the replace function
        //        decMemberID = Crypto.Decrypt(WebConstants.OUTBOUND_EMAIL_CRYPT_KEY, encMemberID.Replace(" ", "+"));
        //    }
        //    catch (Exception)
        //    {
        //        throw new ApplicationException("Invalid memberID");
        //    }
        //    return int.Parse(decMemberID);
        //}

        //private void CalculateAndSetAttributeValue(Member member, string message, int value)
        //{
        //    int oldValue = member.GetAttributeInt(g.Brand, message);
        //    if ((oldValue & value) == value)
        //    {
        //        member.SetAttributeInt(g.Brand, message, oldValue - value);
        //    }
        //}
        #endregion
    }
}