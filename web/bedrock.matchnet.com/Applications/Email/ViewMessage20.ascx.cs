﻿using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements.Links;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.Configuration.ServiceAdapters;
using System;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Matchnet.Web.Applications.Email
{
    public partial class ViewMessage20 : Compose20, IViewMessage
    {
        private const string BREAD_CRUMB_BANNER_LABEL = "lblMiddlePages";

        private string myClass = "";

        protected string MyClass
        {
            get { return myClass; }
        }

        private int memberMailID = 0;
        private int memberFolderID = 0;
        private string orderBy = null;
        private Direction currentDirection = Direction.None;

        
        private EmailMessage _CurrentMessage = null;
        private int _CurrentMessageNumber = 0;
        private int _MessageCount = 0;
        private int fromMemberID = Constants.NULL_INT;

        private bool _isFreeReply = false;

        #region IViewMessage impleentation

        public FrameworkControl ResourceControl { get { return this; } }
        public System.Web.UI.WebControls.Label LblMemberProfileUserName { get { return null; } }
        public System.Web.UI.WebControls.Repeater RptMemberProfile { get { return null; } }
        public Link LnkFromHeaderLink { get { return null; } }
        public Matchnet.Web.Framework.Txt LblViewMessage { get { return lblViewMessage; } }
        public Matchnet.Web.Framework.Txt LblAllAccessReplyIcon { get { return lblAllAccessReplyIcon; } }
        public PlaceHolder PhAboutMeViewPlaceHolder { get { return null; } }
        public PlaceHolder PhPersonalInfoViewPlaceHolder { get { return null; } }
        public PlaceHolder PhBasicsViewPlaceHolder { get { return null; } }
        public PlaceHolder PhPersonalityViewPlaceHolder { get { return null; } }
        public PlaceHolder PhInterestsViewPlaceHolder { get { return null; } }
        public PlaceHolder PhActivityViewPlaceHolder { get { return null; } }
        public PlaceHolder PhIdealMatchViewPlaceholder { get { return null; } }
        public PlaceHolder ViewTabbedProfile { get { return phViewTabbedProfile; } }
        public HyperLink LnkPreviousMessageLink { get { return previousMessageLink; } }
        public HyperLink LnkNextMessageLink { get { return nextMessageLink; } }
        public HyperLink LnkViewMessageLink { get { return viewMessageLink; } }

        public Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnReportMember2 { get { return btnReportMember2; } }
        public HyperLink LnkAppLink { get { return null; } }
        public Literal MessageDateLiteral { get { return messageDateLiteral; } }
        public Literal MessageSubjectLiteral { get { return messageSubjectLiteral; } }
        public Literal MessageContentLiteral { get { return messageContentLiteral; } }


        public int MemberMailID { get { return memberMailID; } set { memberMailID = value; } }
        public int MemberFolderID { get { return memberFolderID; } set { memberFolderID = value; } }
        public string OrderBy { get { return orderBy; } set { orderBy = value; } }
        public Direction CurrentDirection { get { return currentDirection; } set { currentDirection = value; } }
        public EmailMessage CurrentMessage { get { return _CurrentMessage; } set { _CurrentMessage = value; } }
        public int CurrentMessageNumber { get { return _CurrentMessageNumber; } set { _CurrentMessageNumber = value; } }
        public int MessageCount { get { return _MessageCount; } set { _MessageCount = value; } }
        public int FromMemberID { get { return fromMemberID; } set { fromMemberID = value; } }
        public PlaceHolder PhMessageAppend { get { return this.phMessageAppend; } }
        public PlaceHolder PLCInboxShutdownOverlay { get { return this.plcInboxShutdownOverlay; } }

        public PlaceHolder PHSendAsVIP { get { return plcSendAsVIP; } }
        protected string _AllAccessMsg = "";

        public string JSMessage { get; set; }
        public bool JSDoMessageConversion { get; set; }

        protected bool returnToMember
        {
            get
            {
                bool shouldReturn = false;

                if (Request["ReturnToMember"] != null)
                {
                    if (Request["ReturnToMember"].ToLower() == "true")
                    {
                        shouldReturn = true;
                    }
                }

                return shouldReturn;
            }
        }

        // iterates the collection of member photos and extracts his private photos
        // and appends them to the message's private photo table row
        public void ProcessPrivatePhotos()
        {
            Photo currentPhoto = null;
            IEnumerator enumerator = null;
            TableRow privatePhotoRow = null;
            Matchnet.Web.Framework.Image privateImage = null;
            Matchnet.Member.ServiceAdapters.Member fromMember = null;
            HtmlTable imageTable = new HtmlTable();
            HtmlTableRow imageTableRow = new HtmlTableRow();
            HtmlTableCell imageTableCell = new HtmlTableCell();

            fromMember = MemberSA.Instance.GetMember(_CurrentMessage.FromMemberID,
                MemberLoadFlags.None);

            plcPrivatePhoto.Visible = true;

            if (fromMember.GetPhotos(g.Brand.Site.Community.CommunityID) != null && fromMember.GetPhotos(g.Brand.Site.Community.CommunityID).Count > 0)
            {
                enumerator = fromMember.GetPhotos(g.Brand.Site.Community.CommunityID).GetEnumerator();
                while (enumerator.MoveNext())
                {
                    currentPhoto = (Photo)enumerator.Current;
                    if (currentPhoto.IsPrivate)
                    {
                        //if (currentPhoto.IsApproved)
                        {
                            HyperLink link = new HyperLink();

                            privateImage = new Matchnet.Web.Framework.Image();

                            privateImage.Width = 80;
                            privateImage.Height = 104;
                            privateImage.Border = 0;
                            privateImage.CssClass = "profileImageHover";
                            privateImage.ImageUrl = currentPhoto.FileWebPath;

                            string launchLink = "launchPhotoWindow(\'" + currentPhoto.FileWebPath + "\', \'ViewPhoto\');";

                            link.Attributes.Add("onclick", launchLink);
                            link.NavigateUrl = "";
                            link.Controls.Add(privateImage);


                            imageTableCell = new HtmlTableCell();
                            imageTableCell.Controls.Add(link);
                            imageTableRow.Controls.Add(imageTableCell);
                            imageTable.Controls.Add(imageTableRow);
                        }
                    }
                }
                imagePlaceHolder.Controls.Add(imageTable);
            }
        }
        #endregion

        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                _handler = new ComposeHandler(this, g);
                _handler.InitPage();
                _handler.CalledFromViewMessagePage = true;

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        ViewMessageHandler _viewhandler;
        
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                bool isInlineReplyEnabled = (bool)Convert.ToBoolean(RuntimeSettings.GetSetting("COMPOSE_EMAIL_IN_VIEWMESSAGE_PAGE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));
                if (isInlineReplyEnabled) // if the inline reply is enabled
                {
                    onPage_Load(sender, e);
                    phComposeEmail.Visible = true;
                }

                bool readFlagUpdatedIL = false;
                FrameworkGlobals.SaveBackToResultsURL(g);
                //validMemberAccess();
                _viewhandler = new ViewMessageHandler(this, g);
                _viewhandler.GetRequestParameters();
                

                if (memberMailID > 0 && memberFolderID > 0)
                {
                    bool updateReadFlag = true;
                   

                    // Anything other than Flirt is going to redirect to sub, so all messages except for Flirt should be marked unread.
                    if (_viewhandler.ApplyImailPermissions() && !MemberPrivilegeAttr.IsPermitMember(g))
                    {
                        updateReadFlag = false;
                    }

                    _CurrentMessage = EmailMessageSA.Instance.RetrieveMessage(g.Brand.Site.Community.CommunityID,
                        g.TargetMemberID,
                        memberMailID,
                        orderBy,
                        currentDirection,
                        updateReadFlag,
                        g.Brand);

                    // don't update again for il sites

                    if (updateReadFlag && (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL ||
                        g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR ||
                        g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid))
                    {
                        readFlagUpdatedIL = true;
                    }

                    int senderMemberID = _CurrentMessage.FromMemberID;
                    Matchnet.Member.ServiceAdapters.Member emailSender = MemberSA.Instance.GetMember(senderMemberID, MemberLoadFlags.None);

                    int languageMask = emailSender.GetAttributeInt(g.Brand, "LanguageMask");
                    bool senderSpeaksFrench = false;

                    if (languageMask != Constants.NULL_INT)
                    {
                        if ((languageMask & (int)Matchnet.Language.French) == (int)Matchnet.Language.French)
                        {
                            senderSpeaksFrench = true;

                            // REDIRECT TO THE SUBSCRIPTION PAGE IF THE RECIPIENT IS NOT ALLOWED TO VIEW THE MESSAGE  
                            //Redirect.Subscription(g.Brand, (Int32) Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.AttemptToAccessInbox, 0);
                        }
                    }

                    if (_CurrentMessage != null)
                    {
                        // There isn't a way to find out the mail type previously, so got to make another call for Flirt.
                        if (_viewhandler.ApplyImailPermissions() && (_CurrentMessage.MailTypeID == MailType.Tease))
                        {

                            ArrayList memberMailIDs = new ArrayList();
                            memberMailIDs.Add(_CurrentMessage.MemberMailID);
                            EmailMessageSA.Instance.MarkRead(g.TargetMemberID, g.TargetCommunityID, true, memberMailIDs);
                        }

                        // Adds an Omniture event only if the OpenDate is no more than 10 seconds ago,
                        // which means that this is the first time this message is opened
                        if (DateTime.Now.Subtract(_CurrentMessage.OpenDate) < new TimeSpan(0, 0, 10))
                        {
                            g.AnalyticsOmniture.AddEvent("event62");
                        }

                        fromMemberID = _CurrentMessage.FromMemberID;
                        toUserName.Text = _CurrentMessage.FromUserName;
                        // check to see if paging has reached an end
                        if (_CurrentMessage.MemberMailID == memberMailID && currentDirection != Direction.None)
                        {
                            g.Transfer("/Applications/Email/MailBox.aspx?MemberFolderID=" + memberFolderID + "&OrderBy=" + orderBy);
                        }
                        else
                        {
                            if (_CurrentMessageNumber > _MessageCount)
                            {	// Handles the situation where the paging gets out of sequence from the number of mesages in the mailbox.
                                g.Transfer("/Applications/Email/MailBox.aspx?MemberFolderID=" + memberFolderID + "&OrderBy=" + orderBy);
                            }
                            //setup page title.
                            lblViewMessage.Text = g.GetResource("TXT_MESSAGE", this) + " " + _CurrentMessageNumber;
                            setBreadCrumbs();
                            //setFromHeader();

                            _viewhandler.PopulateMessage();
                            _viewhandler.ExaminePagingLinks();
                            
                            //setup page title.
                            if (!VIPMailUtils.IsVIPFolder(_CurrentMessage.MemberFolderID))
                                lblViewMessage.Text = g.GetResource("TXT_MESSAGE", this) + " " + _CurrentMessageNumber;
                            setAppLink();
                        }
                        if (!String.IsNullOrEmpty(Request["VIPOverlay"]) && Request["VIPOverlay"].ToString() == "1")
                        {
                            LnkNextMessageLink.Visible = false;
                            LnkPreviousMessageLink.Visible = false;
                            lblViewMessage.ResourceConstant = "TXT_MESSAGE_VIP";
                        }
                        
                        if(string.IsNullOrEmpty(Request["CurrentMessageNumber"]) && (string.IsNullOrEmpty(Request["MessageCount"])))
                        {
                            LnkNextMessageLink.Visible = false;
                            LnkPreviousMessageLink.Visible = false;
                        }

                        if(_CurrentMessageNumber == 0)
                        {
                            lblViewMessage.Visible = false;
                        }
                    }
                }
                else
                {
                    // viewMessageTable.Visible = false;
                    // viewProfileTable.Visible = false;
                    //   fromHeaderTable.Visible = false;
                    g.Notification.AddError("TXT_NO_MESSAGE_FAILURE");
                }

                _viewhandler.ValidMemberAccess(fromMemberID, _CurrentMessage, !readFlagUpdatedIL);
                
                myClass = "clearfix margin-light";
                sendVIPMessageButton.Visible = false;
                sendMessageButton.Visible = true;

                SetReportMemberLinkVisibility();
                EmailFolder folder = MailBoxHandler.GetCurrentFolder(g, MailBoxHandler.GetCurrentFolderIDEx());
                if (VIPMailUtils.IsVIPFolder(_CurrentMessage.MemberFolderID)
                    && ((_CurrentMessage.StatusMask & MessageStatus.Replied) != MessageStatus.Replied)
                    && ((_CurrentMessage.StatusMask & MessageStatus.OneFreeReply) == MessageStatus.OneFreeReply)
                    )
                {
                    lblViewMessage.Text = g.GetResource("TXT_MESSAGE_VIP", null, true, this);
                    lblAllAccessReplyIcon.Text = g.GetResource("TXT_MESSAGE_REPLY_VIP", null, true, this);
                    myClass = "clearfix margin-light allaccess-msg";
                    _AllAccessMsg = " all-access";
                    
                    sendVIPMessageButton.Visible = true;
                    sendMessageButton.Visible = false;
                    LnkNextMessageLink.Visible = false;
                    LnkPreviousMessageLink.Visible = false;
                    if (!MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                    {
                        if ((_CurrentMessage.StatusMask & MessageStatus.OneFreeReply) == MessageStatus.OneFreeReply)
                        {
                            btnReply.Text = g.GetResource("TXT_REPLY_FREE", this);
                            _isFreeReply = true;
                        }
                    }


                }

                if (isInlineReplyEnabled) // if the inline reply is enabled
                {
                    //plcSendAsVIP.Visible = true;
                    btnReply.Visible = false;
                    Txt2.Visible = false;
                }

                bool showMicroProfile = Conversion.CBool(RuntimeSettings.GetSetting("SHOW_MICRO_PROFILE_ON_VIEW_MESSAGE_PAGE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                if (false && showMicroProfile)
                {
                    // micromicroprofile
                    this.microMicroProfile.Visible = true;
                    this.microMicroProfile.member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(_CurrentMessage.FromMemberID, MemberLoadFlags.None);
                    this.microMicroProfile.TxtFromLabel.Visible = true;

                    bool showMiniProfileHistoryIcon = Conversion.CBool(RuntimeSettings.GetSetting("SHOW_MICRO_MICRO_PROFILE_HISTORY_ICON", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    this.microMicroProfile.LoadMemberProfile(showMiniProfileHistoryIcon);
                }
                
            }
            catch (Exception anException)
            {
                g.ProcessException(anException);
            }
        }
        
        private void setAppLink()
        {
            if (_CurrentMessage.MailTypeID == MailType.CompatibilityMeterEmail)
            {
                //TableRowLink.Visible = true;
                lnkAppLink.Text = "link to jmeter";
                lnkAppLink.NavigateUrl = FrameworkGlobals.LinkHref("/Applications/CompatibilityMeter/Welcome.aspx", false);
            }
        }

        
        #region button behavior
        // handles redirecting a current message view to Compose.aspx for reply
        protected void replyClick(object sender, System.EventArgs e)
        {
            if (_CurrentMessage != null)
            {
                string link = "/Applications/Email/Compose.aspx?ReplyMailID=" + _CurrentMessage.MemberMailID;
                // if this isn't a free reply situation, add the PrtId for this action
                if (!_isFreeReply)
                {
                    link += "&PrtId=" + ((int)PurchaseReasonType.AttemptToReplyToRepliedVIPEmail).ToString();
                }
                link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewMessage, WebConstants.Action.ReplyTop, link, "");
                g.Transfer(link);
            }
        }

        // handles marking message as unread
        protected void markUnreadClick(object sender, System.EventArgs e)
        {
            try
            {
                if (_CurrentMessage != null)
                {
                    ArrayList memberMailIDs = new ArrayList();
                    memberMailIDs.Add(_CurrentMessage.MemberMailID);

                    if (EmailMessageSA.Instance.MarkRead(g.TargetMemberID, g.TargetCommunityID, false, memberMailIDs))
                    {
                        string link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _CurrentMessage.MemberFolderID + "&OrderBy=" + orderBy + "&rc=TXT_UNREAD_MESSAGE_SUCCESS";
                        link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewMessage, WebConstants.Action.MsgMarkTop, link, "");
                        g.Transfer(link);

                    }
                    else
                    {
                        string link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _CurrentMessage.MemberFolderID + "&OrderBy=" + orderBy + "&rc=TXT_UNREAD_MESSAGE_FAILURE";
                        link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewMessage, WebConstants.Action.MsgMarkTop, link, "");
                        g.Transfer(link);

                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        // handles message deletion
        protected void deleteClick(object sender, System.EventArgs e)
        {
            try
            {
                if (_CurrentMessage != null)
                {
                    ArrayList memberMailIDs = new ArrayList();
                    memberMailIDs.Add(_CurrentMessage.MemberMailID);

                    bool result = false;
                    Int32 memberFolderID = _CurrentMessage.MemberFolderID;

                    if (_CurrentMessage.MemberFolderID == (Int32)SystemFolders.Trash)
                    {
                        result = EmailMessageSA.Instance.DeleteMessage(g.TargetMemberID, g.Brand.Site.Community.CommunityID, memberMailIDs);
                    }
                    else
                    {
                        result = EmailMessageSA.Instance.MoveToFolder(g.TargetMemberID, g.Brand.Site.Community.CommunityID, memberMailIDs, (Int32)SystemFolders.Trash);
                    }

                    string navURL = "/Applications/Email/MailBox.aspx?MemberFolderID=" + memberFolderID + "&OrderBy=" +
                                  orderBy;
                    if (!string.IsNullOrEmpty(g.Session[MailBoxHandler.MESSAGE_ORIGIN_MAIL_BOX_URL_SESSION_KEY]))
                    {
                        navURL = g.Session[MailBoxHandler.MESSAGE_ORIGIN_MAIL_BOX_URL_SESSION_KEY];
                    }

                    navURL += navURL.Contains("?") ? "&" : "?";

                    if (result)
                    {
                        //string link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + memberFolderID + "&OrderBy=" + orderBy + "&rc=TXT_DELETE_MESSAGE_SUCCESS";
                        navURL += "rc=TXT_DELETE_MESSAGE_SUCCESS";
                        string link = navURL;
                        link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewMessage, WebConstants.Action.MsgDeleteTop, link, "");
                        g.Transfer(link);


                    }
                    else
                    {
                        //string link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + memberFolderID + "&OrderBy=" + orderBy + "&rc=TXT_DELETE_MESSAGE_FAILURE";
                        navURL += "rc=TXT_DELETE_MESSAGE_FAILURE";
                        string link = navURL;
                        link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewMessage, WebConstants.Action.MsgDeleteTop, link, "");
                        g.Transfer(link);

                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public void SetReportMemberLinkVisibility()
        {

            btnReportMember2.Visible = false;

            if (g.Member.MemberID > 0 && FrameworkGlobals.DetermineReportLinkVisibility(g.Member.MemberID, g.Brand))
            {

                btnReportMember2.Visible = true;
            }
        }

        protected void RedirectToReportMember(object sender, System.EventArgs e)
        {
            try
            {
                string LinkUrl = "/Applications/MemberServices/ReportMember.aspx?MemberID=" + fromMemberID;
                if (memberMailID != Constants.NULL_INT)
                {
                    LinkUrl += "&MemberMailID=" + memberMailID;
                }

                g.Transfer(LinkUrl);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        // handles message ignore with logic for disallowing ignoring the member himself
        protected void ignoreClick(object sender, System.EventArgs e)
        {
            try
            {
                bool ignoreSelf = false;

                if (_CurrentMessage != null)
                {
                    if (_CurrentMessage.MemberFolderID == (int)SystemFolders.Sent || _CurrentMessage.MemberFolderID == (int)SystemFolders.Draft)
                    {
                        if (_CurrentMessage.ToMemberID == g.TargetMemberID)
                        {
                            ignoreSelf = true;
                        }
                    }
                    else
                    {
                        if (_CurrentMessage.FromMemberID == g.TargetMemberID)
                        {
                            ignoreSelf = true;
                        }
                    }
                    if (!ignoreSelf)
                    {
                        ListSaveResult result = ListSA.Instance.AddListMember(HotListCategory.IgnoreList,
                            g.TargetCommunityID,
                            g.Brand.Site.SiteID,
                            g.TargetMemberID,
                            EmailMessageSA.Instance.GetMemberIDToBlock(_CurrentMessage, g.TargetMemberID),
                            string.Empty,
                            Constants.NULL_INT,
                            false,
                            true);

                        if (result.Status == ListActionStatus.Success)
                        {
                            string link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _CurrentMessage.MemberFolderID + "&OrderBy=" + orderBy + "&rc=TXT_IGNORE_MESSAGE_SUCCESS";
                            link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewMessage, WebConstants.Action.MsgBlockTop, link, "");
                            g.Transfer(link);

                        }
                        else
                        {
                            string link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _CurrentMessage.MemberFolderID + "&OrderBy=" + orderBy + "&rc=TXT_IGNORE_MESSAGE_FAILURE";
                            link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewMessage, WebConstants.Action.MsgBlockTop, link, "");
                            g.Transfer(link);

                        }
                    }
                    else
                    {
                        string link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _CurrentMessage.MemberFolderID + "&OrderBy=" + orderBy + "&rc=TXT_YOU_CANT_IGNORE_YOURSELF";
                        link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewMessage, WebConstants.Action.MsgBlockTop, link, "");
                        g.Transfer(link);

                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
        #endregion

        #region page population



        private void setBreadCrumbs()
        {
            // set the correct folder link in the breadcrumbs
            int currentFolderSelected = -1;

            if (Request["MemberFolderID"] != null && !Request["MemberFolderID"].Equals(string.Empty))
            {
                currentFolderSelected = Conversion.CInt(Request["MemberFolderID"]);
            }

            string currentFolderPath = "/Applications/Email/MailBox.aspx?MemberFolderID=" + currentFolderSelected;

            if (_CurrentMessage != null)
            {
                // TT#16469 - bold and link the appropriate breadcrumb items when viewing a message.
                g.BreadCrumbTrailHeader.SetFourLinkCrumb(g.GetResource("TXT_MESSAGES", this), g.AppPage.App.DefaultPagePath, _viewhandler.GetFolderName(_CurrentMessage), currentFolderPath);
                g.BreadCrumbTrailFooter.SetFourLinkCrumb(g.GetResource("TXT_MESSAGES", this), g.AppPage.App.DefaultPagePath, _viewhandler.GetFolderName(_CurrentMessage), currentFolderPath);
            }

            // find the middle control and add some message text to it.
            Label breadCrumbLabel;
            breadCrumbLabel = (Label)g.BreadCrumbTrailHeader.FindControl(BREAD_CRUMB_BANNER_LABEL);

            g.ExpansionTokens.Remove("MESSAGES");
            g.ExpansionTokens.Add("MESSAGES", _MessageCount.ToString());

            if (breadCrumbLabel != null)
            {
                breadCrumbLabel.Text = " <strong>" + g.GetResource("TXT_MESSAGE", this) + " " + _CurrentMessageNumber + "</strong> " + g.GetResource("TXT_OF_MESSAGES", this)
                    + _viewhandler.BuildNextMessageLink();
            }

            breadCrumbLabel = (Label)g.BreadCrumbTrailFooter.FindControl(BREAD_CRUMB_BANNER_LABEL);

            if (breadCrumbLabel != null)
            {
                breadCrumbLabel.Text = " <strong>" + g.GetResource("TXT_MESSAGE", this) + " " + _CurrentMessageNumber + "</strong> " + g.GetResource("TXT_OF_MESSAGES", this)
                    + _viewhandler.BuildNextMessageLink();
            }
        }



        // sets the header to show the message sender
        private void setFromHeader()
        {
            //TableCell headerCell = null;
            //string userName = null;
            //string navigateUrl = null;

            //if (_CurrentMessage.MemberFolderID == (int)SystemFolders.Sent)
            //{
            //    headerCell = (TableCell)fromHeaderTable.FindControl("toHeader");
            //    userName = _CurrentMessage.ToUserName;
            //    navigateUrl = BreadCrumbHelper.MakeViewProfileLink(_CurrentMessage.ToMemberID);
            //}
            //else
            //{
            //    headerCell = (TableCell)fromHeaderTable.FindControl("fromHeader");
            //    userName = _CurrentMessage.FromUserName;
            //    navigateUrl = BreadCrumbHelper.MakeViewProfileLink(_CurrentMessage.FromMemberID);
            //}
            //headerCell.Visible = true;
            ////ViewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.Entrypoint.Messages, _CurrentMessage.FromMemberID, 0, MyMOCollection, (int)_hotListCategory);

            //fromHeaderLink.Text = userName;
            //fromHeaderLink.NavigateUrl = navigateUrl;
        }

        public void miniProfileDataBind(object sender, RepeaterItemEventArgs args)
        {
            // currently...nothing to do
        }

        #endregion

    }
}
