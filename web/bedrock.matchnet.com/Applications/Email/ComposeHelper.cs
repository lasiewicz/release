﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using System.Text.RegularExpressions;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.EmailTracker.ServiceAdapters;
using System.Collections;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Email
{
    /// <summary>
    /// Helper class for shared compose related utilities
    /// </summary>
    public static class ComposeHelper
    {
        public const int MESSAGE_BODY_MAX_CONCURRENT = 30;
        private static LoggingManager _logManager = null;

        public static LoggingManager LogManager
        {
            get
            {
                if (null == _logManager)
                {
                    _logManager = new LoggingManager();
                }
                return _logManager;
            }
            set { _logManager = value; }
        }

        public static bool PassesIMailFraudCheck(ContextGlobal g)
        {
            bool passedFraud = true;

            if (Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_FRAUD_CHECK_BEFORE_IMAIL_SEND", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
            {
                passedFraud = Conversion.CBool(g.Member.GetAttributeInt(g.Brand, "PassedFraudCheckSite", 1));
            }

            return passedFraud;
        }

        public static bool PassesSendNonReplyIMailCheck(ContextGlobal g)
        {
            bool passed = false;

            if (!g.Brand.IsPaySite)
            {
                //this site check may be unnecessary.  Probably can set true for all non-pay sites
                //but i want to limit footprint of this change for hotfix
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.CollegeLuv
                   || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                {
                    passed = true;
                }
            }

            if (!passed)
            {
                // If the member has a subscription then MemberPrivilegeAttr.IsPermitMember(g) is true
                // so access will be allowed.  
                if (MemberPrivilegeAttr.IsPermitMember(g) || g.IsAdmin)
                {
                    passed = true;
                }
            }

            if (!passed)
            {
                passed = ComposeHandler.canNonSubSendFreeEmail(g);
            }

            return passed;
        }

        public static Boolean IsMemberOnVacation(Int32 toMemberID, Brand brand)
        {
            // Retrieve the ToMember's VacationStartDate.
            Member.ServiceAdapters.Member toMember = MemberSA.Instance.GetMember(toMemberID, MemberLoadFlags.None);

            DateTime vacationStartDate = toMember.GetAttributeDate(brand, "VacationStartDate");

            if (vacationStartDate == DateTime.MinValue)
                return false;
            else
                return true;
        }

        public static string formatMessageBody(string messageBody)
        {
            int currentCount = 0;

            for (int i = 0; i < messageBody.Length; i++)
            {
                if (messageBody[i].Equals(' ') || messageBody[i].Equals('\n'))
                {
                    currentCount = 0;
                }
                else
                {
                    currentCount++;
                    if (currentCount == MESSAGE_BODY_MAX_CONCURRENT)
                    {
                        messageBody = messageBody.Insert(i, " ");
                        currentCount = 0;
                    }
                }
            }
            return messageBody;
        }

        public static ComposeResult CompileSendMessage(bool pSaveAsDraft,
            bool pAdminSend,
            int fromMemberId,
            int toMemberId,
            string messageSubject,
            string messageBody,
            MailType mailType,
            bool saveCopySent,
            int messageListId,
            int replyDraftId,
            int replyMessageId,
            bool sendAsVIP,
            object resourceControl,
            ContextGlobal _g)
        {
            ComposeResult composeResult = new ComposeResult();
            try
            {

                if (messageSubject.Length <= 0)
                {
                    _g.Notification.AddError("TXT_MESSAGE_SUBJECT_BLANK");
                    return composeResult;
                }

                if (messageBody.Length <= 0)
                {
                    _g.Notification.AddError("TXT_MESSAGE_BODY_BLANK");
                    return composeResult;
                }

                // parse out all html tags
                messageSubject = Regex.Replace(messageSubject, "<[^>]*>", "");
                messageBody = Regex.Replace(ComposeHelper.formatMessageBody(messageBody), "<[^>]*>", "");

                int replyid = replyMessageId;
                if (replyid < 0 && replyDraftId > 0)
                    replyid = replyDraftId;

                VIPMember vipmember = VIPMailUtils.GetVIPMember(_g, _g.Member.MemberID);
                sendAsVIP = IsVIPMail(pSaveAsDraft, sendAsVIP, replyid, _g);

                MessageSave messageSave = new MessageSave(fromMemberId,
                    toMemberId,
                    _g.Brand.Site.Community.CommunityID,
                    _g.Brand.Site.SiteID,
                    mailType,
                    messageSubject,
                    messageBody);

                messageSave.MessageListID = messageListId;

                // Should this be a VIP email?
                if (sendAsVIP)
                {
                    messageSave.MailOption = messageSave.MailOption | MailOption.VIP;
                }

                if (pSaveAsDraft)
                {
                    int membermailid = 0;
                    //modified to account for returnig to save draft after subscription for compose project.
                    bool saved = EmailMessageSA.Instance.SaveMessage(messageSave, out membermailid);
                    if (saved && replyDraftId > 0)
                    {
                        EmailMessageSA.Instance.MarkDraftSaved(fromMemberId, _g.Brand.Site.Community.CommunityID, saved, replyDraftId);
                    }
                    composeResult.ComposeSuccess = saved;
                    composeResult.membermailid = membermailid;
                    return composeResult;
                }

                // If the sender is blocked by the receiver, send a blocked message to the sender and halt sending to the receiver
                try
                {
                    Member.ServiceAdapters.Member toMember = MemberSA.Instance.GetMember(messageSave.ToMemberID, MemberLoadFlags.None);
                    composeResult.messageSendResult = EmailMessageSA.Instance.CheckAndSendMessageForBlockedSenderInRecipientList(messageSave.GroupID, string.Format(_g.GetResource("TXT_IGNORE_SUBJECT"), messageSave.MessageHeader), string.Format(_g.GetResource("TXT_IGNORE_CONTENT"), toMember.GetUserName(_g.Brand)),
                                                                                                                                 toMemberId, fromMemberId, _g.Brand.Site.SiteID,
                                                                                                                                 _g.Brand.Site.Community.CommunityID);
                    composeResult.ComposeSuccess = (null != composeResult.messageSendResult && composeResult.messageSendResult.Status == MessageSendStatus.Success);
                    ComposeHandler.LogIgnoreEMail(_g, fromMemberId.ToString(), messageSave.ToMemberID.ToString());
                }
                catch (Exception e)
                {
                    LogManager.LogException(e,_g.Member,_g.Brand,"ComposeHelper");
                }
                if (composeResult.ComposeSuccess)
                {
                    return composeResult;
                }

                // Before the message is sent, we need to check to see if this is a free reply or not.  If you check this after the message is sent,
                // you will never get FreeReply bool to come back true since when the reply is made, the FreeReply mask bit is set to 0 on the original/being replied
                // to message
                bool freeReplyToAllAccess = IsThisFreeReply(replyid, _g);

                // Try to send email message
                composeResult.messageSendResult = EmailMessageSA.Instance.SendMessage(messageSave, saveCopySent, replyid, true);

                switch (composeResult.messageSendResult.Status)
                {
                    case MessageSendStatus.Success:
                        bool isFraudHold = (composeResult.messageSendResult.MailOptions & MailOption.FraudHold) == MailOption.FraudHold;
                        composeResult.ComposeSuccess = true;
                        int senderMemberId = _g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
                        bool ignoreQuota = (replyMessageId > 0); //TT#16468 - Do not increment email quota if sending a reply

                        // if this is a FraudHold email, we don't want the hotlist to be updated because the member
                        // will be confused since the email won't appear in their inbox
                        if (!isFraudHold)
                        {
                            // save the to member in the current member's hotlist
                            if (!sendAsVIP)
                                ListSA.Instance.AddListMember(HotListCategory.MembersYouEmailed,
                                    _g.Brand.Site.Community.CommunityID,
                                    _g.Brand.Site.SiteID,
                                    senderMemberId,
                                    messageSave.ToMemberID,
                                    null,
                                    Constants.NULL_INT,
                                    true,
                                    ignoreQuota);

                            // if this was a vip message send, decrement from the member's balance
                            if (sendAsVIP)
                            {
                                if (VIPMailUtils.IsVIPMember(_g, senderMemberId) && !freeReplyToAllAccess)
                                    VIPMailUtils.AdjustMemberAccessEmailCount(_g, senderMemberId, -1);

                                ListSA.Instance.AddListMember(HotListCategory.MembersYouSentAllAccessEmail,
                               _g.Brand.Site.Community.CommunityID,
                               _g.Brand.Site.SiteID,
                               senderMemberId,
                               messageSave.ToMemberID,
                               null,
                               Constants.NULL_INT,
                               true,
                               ignoreQuota);
                            }
                        }
                        else
                        {
                            // If this is a FraudHold email, the HotList never gets updated, so we need to deduct the quota separately
                            // so that the member can't bypass the quota rule.  Normally quota is updated inside AddListMember method.
                            QuotaSA.Instance.AddQuotaItem(_g.Brand.Site.Community.CommunityID, _g.Member.MemberID, QuotaType.SentEmail);

                            // if this was a vip message send, decrement from the member's balance
                            if(sendAsVIP)
                            {
                                if (VIPMailUtils.IsVIPMember(_g, senderMemberId) && !freeReplyToAllAccess)
                                    VIPMailUtils.AdjustMemberAccessEmailCount(_g, senderMemberId, -1);
                            }
                        }

                        if (!FrameworkGlobals.IfHadSubscription(_g))
                        {
                            if (ComposeHandler.canNonSubSendFreeEmail(_g))
                            {
                                _g.Member.SetAttributeInt(_g.Brand, "SentFreeEmailCount", _g.Member.GetAttributeInt(_g.Brand, "SentFreeEmailCount", 0) + 1);
                                Member.ServiceAdapters.MemberSA.Instance.SaveMember(_g.Member);
                            }
                        }

                        // I'm not sure if this call is even needed since BL takes care of this when the reply is made to a FreeReply
                        // reply, but will leave it in just to be safe since it's late in the game to switch
                        RemoveFreeReply(replyid, _g);

                        // suppress the UserNotification entry from being created if this is a FraudHold email
                        if (!isFraudHold)
                            SendUserNotifications(messageSave, composeResult.messageSendResult, sendAsVIP && !freeReplyToAllAccess, _g);

                        Member.ServiceAdapters.Member toMember = MemberSA.Instance.GetMember(messageSave.ToMemberID, MemberLoadFlags.None);
                        
                        //use the BrandID of site the recipient last logged into, may be different from sender's brandID
                        int brandid = _g.Brand.BrandID;
                        toMember.GetLastLogonDate(_g.Brand.Site.Community.CommunityID, out brandid);

                        // Check to see if we need to send a "Recipient on Vacation" email.
                        if (ComposeHelper.IsMemberOnVacation(messageSave.ToMemberID, _g.Brand))
                        {
                            MessageSave vacationMessage = new MessageSave(messageSave.ToMemberID,
                                fromMemberId,
                                _g.Brand.Site.Community.CommunityID,
                                _g.Brand.Site.SiteID,
                                MailType.Email,
                                String.Format(_g.GetResource("TXT_VACATION_SUBJECT"), messageSave.MessageHeader),
                                String.Format(_g.GetResource("TXT_VACATION_CONTENT"), toMember.GetUserName(_g.Brand)));

                            EmailMessageSA.Instance.SendMessage(vacationMessage, false, Constants.NULL_INT, false);
                        }

                        // send an external mail to let recipient know that he/she has a message
                        if (!sendAsVIP && !isFraudHold)
                            ExternalMailSA.Instance.SendInternalMailNotification(composeResult.messageSendResult.EmailMessage, _g.Brand.BrandID, composeResult.messageSendResult.RecipientUnreadCount);

                        else if (sendAsVIP && freeReplyToAllAccess)
                        {
                            // if this email is a fraud hold email then the external email notification should be sent when the email is un frauded
                            if (!isFraudHold)
                                ExternalMailSA.Instance.SendAllAccessReplyToInitialEmail(senderMemberId, _g.Brand.BrandID, composeResult.messageSendResult.RecipientMessageListID, messageSave.ToMemberID, brandid, messageSave.MessageHeader, messageSave.MessageBody);

                        }
                        else if (sendAsVIP && !freeReplyToAllAccess)
                        {
                            // if this email is a fraud hold email then the external email notification should be sent when the email is un frauded
                            if (!isFraudHold)
                                ExternalMailSA.Instance.SendAllAccessInitialEmail(senderMemberId, _g.Brand.BrandID, composeResult.messageSendResult.RecipientMessageListID, messageSave.ToMemberID, brandid, messageSave.MessageHeader, messageSave.MessageBody);

                            // if this email is a fraud hold, start the tracker with "READ" status so that tracker ignores this entry
                            // only when this is marked as unread when unfraud happens, EmailTracker should pay attention to it
                            EmailTrackerSA.Instance.AddTrackedEmail(senderMemberId, messageSave.ToMemberID,
                                composeResult.messageSendResult.RecipientMessageListID, _g.Brand.BrandID, (int)Spark.Common.AccessService.PrivilegeType.AllAccessEmails, isFraudHold);

                        }
                        composeResult.VIPFreeReply = freeReplyToAllAccess;
                        composeResult.VIPMail = sendAsVIP;
                        return composeResult;

                    default:
                        return composeResult;
                }
            }
            catch (Exception anException)
            {
                _g.ProcessException(anException);
            }
            return composeResult;
        }

        public static void SendUserNotifications(MessageSave messageSave, MessageSendResult messageSendResult, bool vipmessage, ContextGlobal g)
        {
            try
            {
                var additionalParams = new Hashtable();
                additionalParams.Add("messageid", messageSendResult.RecipientMessageListID.ToString());

                if (UserNotificationFactory.IsUserNotificationsEnabled(g))
                {
                    UserNotificationParams unParams = UserNotificationParams.GetParamsObject(messageSave.ToMemberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                    UserNotification notification = null;
                    if (vipmessage)
                    {
                        additionalParams.Add("messagefolderid", SystemFolders.VIPInbox.ToString("d"));
                        notification = UserNotificationFactory.Instance.GetUserNotification(new AllAccessEmailedYouUserNotification(), messageSave.ToMemberID, messageSave.FromMemberID, g, additionalParams);
                    }
                    else
                    {
                        additionalParams.Add("messagefolderid", SystemFolders.Inbox.ToString("d"));
                        notification =UserNotificationFactory.Instance.GetUserNotification(new EmailedYouUserNotification(), messageSave.ToMemberID, messageSave.FromMemberID, g, additionalParams);
                    }
                    if (notification.IsActivated())
                    {
                        UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                    }
                }
            }
            catch (Exception ex)
            {

                g.ProcessException(ex);
            }


        }

        #region All Access Related Helpers
        /// <summary>
        /// This method checks the message being replied to has the FreeReply mask or not.
        /// </summary>
        /// <param name="pReplyMailId"></param>
        /// <returns></returns>
        public static bool IsThisFreeReply(int pReplyMailId, ContextGlobal g)
        {
            bool freeReply = false;

            if (!VIPMailUtils.IsVIPEnabledSite(g.Brand) || pReplyMailId <= 0)
                return false;

            EmailMessage aMessage = EmailMessageSA.Instance.RetrieveMessage(g.Brand.Site.Community.CommunityID,
                  g.TargetMemberID,
                  pReplyMailId,
                  null,
                  Direction.None,
                  false,
                  g.Brand);

            if (aMessage == null) return false;

            if ((aMessage.StatusMask & MessageStatus.OneFreeReply) == MessageStatus.OneFreeReply)
                freeReply = true;

            return freeReply;
        }

        /// <summary>
        /// Please be careful when you call this method.  If you call it at the wrong time, it could mess up the logic of determining if
        /// the mail being sent is a free reply or not
        /// </summary>
        /// <param name="replyMailId"></param>
        /// <returns></returns>
        public static bool RemoveFreeReply(int replyMailId, ContextGlobal g)
        {
            bool wasFreeReply = false;
            try
            {
                if (!VIPMailUtils.IsVIPEnabledSite(g.Brand) || replyMailId <= 0)
                    return false;


                EmailMessage aMessage = EmailMessageSA.Instance.RetrieveMessage(g.Brand.Site.Community.CommunityID,
                  g.TargetMemberID,
                  replyMailId,
                  null,
                  Direction.None,
                  false,
                  g.Brand);

                if (aMessage == null) return false;

                if ((aMessage.StatusMask & MessageStatus.OneFreeReply) == MessageStatus.OneFreeReply)
                {

                    Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.MarkOneFreeReply(g.TargetMemberID, g.Brand.Site.Community.CommunityID, false, new ArrayList { replyMailId });
                    wasFreeReply = true;
                }
                return wasFreeReply;
            }
            catch (Exception ex)
            { g.ProcessException(ex); return wasFreeReply; }


        }

        public static bool IsVIPMail(bool isDraft, bool SendAsVIP, int replyMailID, ContextGlobal g)
        {
            bool isvipmail = false;
            try
            {
                if (isDraft && SendAsVIP) return true;

                VIPMember vipmember = VIPMailUtils.GetVIPMember(g, g.Member.MemberID);
                if (VIPMailUtils.IsVIPMember(g, vipmember))
                {
                    if (vipmember.EmailCount > 0 && SendAsVIP)
                    {
                        isvipmail = true;
                    }
                    else
                    {
                        isvipmail = IsFreeReply(replyMailID, g);
                    }

                }
                else
                {
                    isvipmail = IsFreeReply(replyMailID, g);
                }

                return isvipmail;

            }
            catch (Exception ex)
            { g.ProcessException(ex); return isvipmail; }


        }

        public static bool IsFreeReply(int replyMailId, ContextGlobal g)
        {
            bool isfreereply = false;
            try
            {
                if (!VIPMailUtils.IsVIPEnabledSite(g.Brand))
                    return isfreereply;


                EmailMessage aMessage = EmailMessageSA.Instance.RetrieveMessage(g.Brand.Site.Community.CommunityID,
                  g.TargetMemberID,
                  replyMailId,
                  null,
                  Direction.None,
                  true,
                  g.Brand);

                if (aMessage == null) return isfreereply;

                isfreereply = (aMessage.StatusMask & MessageStatus.OneFreeReply) == MessageStatus.OneFreeReply;
                return isfreereply;

            }
            catch (Exception ex)
            { g.ProcessException(ex); return isfreereply; }


        }
        #endregion
    }
}
