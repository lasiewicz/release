﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Email.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Spark.Common.AccessService;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Web.Framework.Util;
using Matchnet.Content.ValueObjects.BrandConfig;
namespace Matchnet.Web.Applications.Email
{
    public enum VIPMessageAccess : int
    {
        na = 0,
        read = 1,
        reply = 2

    }

    public enum VIPOverlayMode : int
    {
        none = 0,
        vipuseemail = 1,
        vipupsell = 2,
        subscriberbuy = 4
    }


    public class VIPMember
    {
        public DateTime AllAccessExpireDate { get; set; }
        public int EmailCount { get; set; }

    }
    public class VIPMailUtils
    {
        private static SettingsManager _settingsManager = new SettingsManager();
        
        public static bool IsVIPEnabledSite(Matchnet.Content.ValueObjects.BrandConfig.Brand pBrand)
        {
            return _settingsManager.GetSettingBool(SettingConstants.VIP_EMAIL_ENABLED, pBrand);
        }

        public static bool IsVIPSendEnabledSite(Matchnet.Content.ValueObjects.BrandConfig.Brand pBrand)
        {
            return _settingsManager.GetSettingBool(SettingConstants.VIP_EMAIL_SEND_ENABLED, pBrand);
        }

        public static VIPMember GetVIPMember(ContextGlobal g, int memberid)
        {

            VIPMember member = null;
            try
            {
                int siteid = g.Brand.Site.SiteID;
                int communityid = g.Brand.Site.Community.CommunityID;
                int brandid = g.Brand.BrandID;
                Matchnet.Member.ServiceAdapters.Member samember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                AccessPrivilege privdate = samember.GetUnifiedAccessPrivilege(PrivilegeType.AllAccess, brandid, siteid, communityid);
                AccessPrivilege privcount = samember.GetUnifiedAccessPrivilege(PrivilegeType.AllAccessEmails, brandid, siteid, communityid);

                // if we can't even get the date, this person doesn't have vip access
                if (privdate != null)
                {
                    member = new VIPMember();
                    member.AllAccessExpireDate = privdate.EndDatePST;

                    // you can have all access time-based but the count-based portion could be null so check for null here
                    if (privcount != null)
                        member.EmailCount = privcount.RemainingCount;
                }

                return member;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return null;
            }


        }

        public static VIPMember GetVIPMember(ContextGlobal g, Matchnet.Member.ServiceAdapters.Member samember)
        {

            VIPMember member = null;
            try
            {
                int siteid = g.Brand.Site.SiteID;
                int communityid = g.Brand.Site.Community.CommunityID;
                int brandid = g.Brand.BrandID;
                AccessPrivilege privdate = samember.GetUnifiedAccessPrivilege(PrivilegeType.AllAccess, brandid, siteid, communityid);
                AccessPrivilege privcount = samember.GetUnifiedAccessPrivilege(PrivilegeType.AllAccessEmails, brandid, siteid, communityid);

                // if we can't even get the date, this person doesn't have vip access
                if (privdate != null)
                {
                    member = new VIPMember();
                    member.AllAccessExpireDate = privdate.EndDatePST;

                    // you can have all access time-based but the count-based portion could be null so check for null here
                    if (privcount != null)
                        member.EmailCount = privcount.RemainingCount;
                }

                return member;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return null;
            }


        }


        public static VIPMember GetVIPMember(ContextGlobal g, int memberid, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {

            VIPMember member = null;
            try
            {
                int siteid = brand.Site.SiteID;
                int communityid = brand.Site.Community.CommunityID;
                int brandid = brand.BrandID;
                Matchnet.Member.ServiceAdapters.Member samember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                AccessPrivilege privdate = samember.GetUnifiedAccessPrivilege(PrivilegeType.AllAccess, brandid, siteid, communityid);
                AccessPrivilege privcount = samember.GetUnifiedAccessPrivilege(PrivilegeType.AllAccessEmails, brandid, siteid, communityid);

                member = new VIPMember();
                if(privdate != null)
                    member.AllAccessExpireDate = privdate.EndDatePST;
                if(privcount != null)
                    member.EmailCount = privcount.RemainingCount;

                return member;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return null;
            }


        }

        public static bool IsVIPMember(ContextGlobal g, VIPMember vipmember)
        {
            bool vip = false;
            if (vipmember != null)
                vip = vipmember.AllAccessExpireDate > DateTime.Now;

            return vip;
        }
        public static bool IsVIPMember(ContextGlobal g, int memberid)
        {
            bool vip = false;
            VIPMember vipmember = GetVIPMember(g, memberid);

            if (vipmember != null)
                vip = vipmember.AllAccessExpireDate > DateTime.Now;

            return vip;
        }

        public static bool IsVIPMember(ContextGlobal g, int memberid, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            bool vip = false;
            VIPMember vipmember = GetVIPMember(g, memberid, brand);

            if (vipmember != null)
                vip = vipmember.AllAccessExpireDate > DateTime.Now;

            return vip;
        }

        public static Int32 GetSystemFolderIDForVIPFolder(int pFolderID)
        {
            int ret = Constants.NULL_INT;

            switch (pFolderID)
            {
                case (int)SystemFolders.VIPDraft:
                    ret = (int)SystemFolders.Draft;
                    break;
                case (int)SystemFolders.VIPInbox:
                    ret = (int)SystemFolders.Inbox;
                    break;
                case (int)SystemFolders.VIPSent:
                    ret = (int)SystemFolders.Sent;
                    break;
                case (int)SystemFolders.VIPTrash:
                    ret = (int)SystemFolders.Trash;
                    break;
                default:
                    ret = pFolderID;
                    break;
            }

            return ret;
        }

        public static bool IsVIPFolder(int pFolderID)
        {
            bool ret = false;

            switch (pFolderID)
            {
                case (int)SystemFolders.VIPDraft:
                    ret = true;
                    break;
                case (int)SystemFolders.VIPInbox:
                    ret = true;
                    break;
                case (int)SystemFolders.VIPSent:
                    ret = true;
                    break;
                case (int)SystemFolders.VIPTrash:
                    ret = true;
                    break;
                default:

                    break;
            }

            return ret;
        }

        public static int GetMatchingVIPFolderId(int pSystemFolderId)
        {
            int ret = Constants.NULL_INT;

            switch (pSystemFolderId)
            {
                case (int)SystemFolders.Inbox:
                    ret = (int)SystemFolders.VIPInbox;
                    break;
                case (int)SystemFolders.Draft:
                    ret = (int)SystemFolders.VIPDraft;
                    break;
                case (int)SystemFolders.Sent:
                    ret = (int)SystemFolders.VIPSent;
                    break;
                case (int)SystemFolders.Trash:
                    ret = (int)SystemFolders.VIPTrash;
                    break;
            }

            return ret;
        }


        private static Spark.Common.AccessService.AccessServiceClient GetAccessProxy(ContextGlobal g)
        {
            Spark.Common.AccessService.AccessServiceClient proxy = null;

            try
            {
                proxy = Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstance();

                return proxy;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return null;
            }
            finally
            {
                if (proxy != null)
                {
                    Spark.Common.Adapter.AccessServiceWebAdapter.CloseProxyInstance();
                }
            }


        }

        private static void CloseAccessProxy(Spark.Common.AccessService.AccessServiceClient proxy)
        {
            if (proxy != null)
            {
                Spark.Common.Adapter.AccessServiceWebAdapter.CloseProxyInstance();
            }

        }

        public static void GiveAccessForTesting(ContextGlobal g)
        {
            Spark.Common.AccessService.AccessServiceClient proxy = null;

            try
            {
                proxy = Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstance();
                // DurationTypeID 5 -> month, PrivilegeTypeID 6 -> All Access
                var result = proxy.AdjustTimePrivilege(g.Member.MemberID, new int[] { 6 }, 1, 5, Constants.NULL_INT, Constants.NULL_STRING, Constants.NULL_INT, g.Brand.Site.SiteID, 1);

                string resultString = result.response;
            }
            catch (Exception ex)
            { }
            finally
            {
                if (proxy != null)
                {
                    Spark.Common.Adapter.AccessServiceWebAdapter.CloseProxyInstance();
                }
            }
        }

        public static void GiveAccessCountForTesting(ContextGlobal g)
        {
            Spark.Common.AccessService.AccessServiceClient proxy = null;

            try
            {
                proxy = Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstance();

                // PrivilegeTypeID 7 -> All Access Emails
                var result = proxy.AdjustCountPrivilege(g.Member.MemberID, new int[] { 7 }, 10, Constants.NULL_INT, Constants.NULL_STRING, Constants.NULL_INT, g.Brand.Site.SiteID, 1);

                string resultString = result.response;

            }
            catch (Exception ex)
            { }
            finally
            {
                if (proxy != null)
                {
                    Spark.Common.Adapter.AccessServiceWebAdapter.CloseProxyInstance();
                }
            }
        }

        public static void AdjustMemberAccessEmailCount(ContextGlobal g, int pMemberID, int count)
        {
            #region Called Access Service directly
            //Spark.Common.AccessService.AccessServiceClient proxy = null;
            //try
            //{
            //    proxy = Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstance();
            //    var result = proxy.AdjustCountPrivilege(pMemberID, new int[] { (int)Spark.Common.AccessService.PrivilegeType.AllAccessEmails }, count, Constants.NULL_INT, Constants.NULL_STRING,
            //        Constants.NULL_INT, g.Brand.Site.SiteID, 1);

            //    if (result.response.ToLower() != "success")
            //    {
            //        throw new ApplicationException(string.Format("AdjustCountPrivilege failed MemberID:{0}, PrivilegeType:{1}, SiteID:{2}",
            //            pMemberID, (int)Spark.Common.AccessService.PrivilegeType.AllAccessEmails, g.Brand.Site.SiteID));

            //    }


            //}
            //catch (Exception ex)
            //{
            //    g.ProcessException(ex);
            //}
            //finally
            //{
            //    if (proxy != null)
            //    {
            //        Spark.Common.Adapter.AccessServiceWebAdapter.CloseProxyInstance();
            //    }
            //}
            #endregion

            //Call MemberSA to adjust count, this will update local cache while waiting for synchronization from Access
            try
            {
                bool result = MemberSA.Instance.AdjustUnifiedAccessCountPrivilege(pMemberID, Constants.NULL_INT, "", new int[] { (int)Spark.Common.AccessService.PrivilegeType.AllAccessEmails }, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID, count);
                if (!result)
                {
                    throw new ApplicationException(string.Format("AdjustCountPrivilege failed MemberID:{0}, PrivilegeType:{1}, SiteID:{2}",
                        pMemberID, (int)Spark.Common.AccessService.PrivilegeType.AllAccessEmails, g.Brand.Site.SiteID));

                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public static VIPMessageAccess GetVIPMessageAccess(ContextGlobal g, EmailMessage message)
        {
            VIPMessageAccess access = VIPMessageAccess.na;
            try
            {
                if (message == null)
                    return access;

                if (message.MemberFolderID == (int)SystemFolders.VIPInbox)
                {
                    access = access | VIPMessageAccess.read;

                }

                if ((message.StatusMask & MessageStatus.OneFreeReply) == MessageStatus.OneFreeReply)
                {
                    if (message.InsertDate.AddDays(15) > DateTime.Now)
                    {
                        access = access | VIPMessageAccess.reply;
                    }
                }


                return access;

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return VIPMessageAccess.na;
            }



        }


        public static VIPOverlayMode OverlayDisplayMode(ContextGlobal g)
        {
            VIPOverlayMode overlaymode = VIPOverlayMode.none;
            try
            {
                string mode = _settingsManager.GetSettingString(SettingConstants.VIP_COMPOSE_OVERLAY_MODE, g.Brand);
                overlaymode = (VIPOverlayMode)Enum.Parse(typeof(VIPOverlayMode), mode);
                return overlaymode;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex); return overlaymode;
            }
             
        }


        public static bool DisplayOverlay(ContextGlobal g, VIPOverlayMode overlaymode)
        {
            bool display = false;
            try
            {
                VIPOverlayMode mode = OverlayDisplayMode(g);
                if (((int)mode & (int)overlaymode) == (int)overlaymode)
                    display = true;


                if (!display) return display;

                //now check session settings
                int timestoshow = _settingsManager.GetSettingInt(SettingConstants.VIP_COMPOSE_OVERLAY_PER_SESSION, g.Brand);
                string sessiontimes = g.Session.GetString("VIP_COMPOSE_OVERLAY_PER_SESSION");

                if (timestoshow < 0)
                    return true;

                if (!String.IsNullOrEmpty(sessiontimes))
                {
                    int timesalreadyshown = Conversion.CInt(sessiontimes, 0);
                    if (timesalreadyshown < timestoshow)
                    {
                        display = true;

                    }
                    else
                        display = false;
                }
                else
                {

                    display = true;
                }

                return display;
            }
            catch (Exception ex)
            { g.ProcessException(ex); return display; }

        }

        public static int GetMaxDisplayOverlayPerSession(ContextGlobal g, out int CurrentDisplayOverlayPerSession)
        {
            int timestoshow = _settingsManager.GetSettingInt(SettingConstants.VIP_COMPOSE_OVERLAY_PER_SESSION, g.Brand);
            string sessiontimes = g.Session.GetString("VIP_COMPOSE_OVERLAY_PER_SESSION");

            CurrentDisplayOverlayPerSession = 0;
            if (!String.IsNullOrEmpty(sessiontimes))
            {
                CurrentDisplayOverlayPerSession = Conversion.CInt(sessiontimes, 0);
            }

            return timestoshow;
        }

        public static bool isDisplayOverlayMaxReached(ContextGlobal g)
        {
            bool reached = false;
            try
            {
                //now check session settings
                int timestoshow = _settingsManager.GetSettingInt(SettingConstants.VIP_COMPOSE_OVERLAY_PER_SESSION, g.Brand);
                string sessiontimes = g.Session.GetString("VIP_COMPOSE_OVERLAY_PER_SESSION");

                if (!String.IsNullOrEmpty(sessiontimes))
                {
                    int timesalreadyshown = Conversion.CInt(sessiontimes, 0);
                    if (timesalreadyshown == timestoshow)
                    {
                        reached = true;
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return reached;

        }

        public static string GetUpsaleURL(ContextGlobal g, bool fromOverlay, Brand pBrand, int toMemberID, bool pOverrideTransactionHistory, int pMemberMailID)
        {
            string pDestinationURL = "/Applications/Email/Compose.aspx?MemberMailID=" + pMemberMailID.ToString() + "&MemberID=" + toMemberID.ToString() + "&" + WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID + "=" + HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID] + "&NoAllAccess=1"; 

            //if (fromOverlay) // set the cancel continue without all acess link to load the compose page with out the checkbox checked. 
            //{
            //    pDestinationURL = "/Applications/Email/Compose.aspx?MemberMailID=" + pMemberMailID.ToString() + "&MemberID=" + toMemberID.ToString() + "&" + WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID + "=" + HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID] + "&NoAllAccess=1";
            //}
            //else
            //{
            //    pDestinationURL = "/Applications/Email/Compose.aspx?MemberMailID=" + pMemberMailID.ToString() + "&MemberID=" + toMemberID.ToString() + "&" + WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID + "=" + HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID];   
            //}

            int pPurchaseReasonTypeID = (int)PurchaseReasonType.RebuyAdditionalAllAccessEmailsViewProfileCompose;

            VIPMember vipmember = VIPMailUtils.GetVIPMember(g, g.Member.MemberID);
            bool isVariablePricing = false;
            if (VIPMailUtils.IsVIPMember(g, vipmember))
            {
                g.LoggingManager.LogInfoMessage("VIPMailUtils", "GetUpsaleURL - IsVIPMember Member " + g.Member.MemberID.ToString());
                if (fromOverlay)
                    pPurchaseReasonTypeID = (int)PurchaseReasonType.RebuyAdditionalAllAccessEmailsViewProfileComposeYesOnOverlay;
            }
            else
            {
                g.LoggingManager.LogInfoMessage("VIPMailUtils", "GetUpsaleURL - Not IsVIPMember Member " + g.Member.MemberID.ToString());
                isVariablePricing = true;
                pPurchaseReasonTypeID = (int)PurchaseReasonType.WantToGuaranteeAllAccessViewProfileComposeInitialPurchase;

                if (fromOverlay)
                    pPurchaseReasonTypeID = (int)PurchaseReasonType.WantToGuaranteeAllAccessViewProfileComposeInitialPurchaseYesOnOverlay;
            }

            ParameterCollection parameterCollection = new ParameterCollection();

            if (g.ImpersonateContext)
            {
                parameterCollection.Add(WebConstants.IMPERSONATE_MEMBERID, g.TargetMemberID.ToString());
                parameterCollection.Add(WebConstants.IMPERSONATE_BRANDID, g.TargetBrandID.ToString());
            }
            parameterCollection.Add(WebConstants.URL_PARAMETER_NAME_MEMBERMAILID, pMemberMailID.ToString());

            string subURL = Redirect.GetUpsaleUrl(pBrand, pPurchaseReasonTypeID, toMemberID, pOverrideTransactionHistory, HttpContext.Current.Server.UrlEncode(pDestinationURL), parameterCollection, isVariablePricing);
            return subURL;
        }

        public static string GetRegularSubscriptionURL(ContextGlobal g, bool fromOverlay, Brand pBrand, int toMemberID, bool pOverrideTransactionHistory, int pMemberMailID)
        {
            string pDestinationURL = "/Applications/Email/Compose.aspx?MemberMailID=" + pMemberMailID.ToString() + "&MemberID=" + toMemberID.ToString() + "&" + WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID + "=" + HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID];
            int pPurchaseReasonTypeID = (int)PurchaseReasonType.RebuyAdditionalAllAccessEmailsViewProfileCompose;

            VIPMember vipmember = VIPMailUtils.GetVIPMember(g, g.Member.MemberID);
            bool isVariablePricing = false;
            if (VIPMailUtils.IsVIPMember(g, vipmember))
            {
                if (fromOverlay)
                    pPurchaseReasonTypeID = (int)PurchaseReasonType.RebuyAdditionalAllAccessEmailsViewProfileComposeYesOnOverlay;
            }
            else
            {
                isVariablePricing = true;
                pPurchaseReasonTypeID = (int)PurchaseReasonType.AttemptToEmailFullProfile;

                if (fromOverlay)
                    pPurchaseReasonTypeID = (int)PurchaseReasonType.AttemptToEmailFullProfile;
            }

            if (ComposeHandler.canNonsubSaveAsDraft(g)) // if allowed to save as draft
            {
                pPurchaseReasonTypeID = (int)PurchaseReasonType.AttemptToEmailSubBlockProfilePage;
            }

            ParameterCollection parameterCollection = new ParameterCollection();

            if (g.ImpersonateContext)
            {
                parameterCollection.Add(WebConstants.IMPERSONATE_MEMBERID, g.TargetMemberID.ToString());
                parameterCollection.Add(WebConstants.IMPERSONATE_BRANDID, g.TargetBrandID.ToString());
            }
            parameterCollection.Add(WebConstants.URL_PARAMETER_NAME_MEMBERMAILID, pMemberMailID.ToString());

            string subURL = Redirect.GetSubscriptionUrl(pBrand, pPurchaseReasonTypeID, toMemberID, pOverrideTransactionHistory, HttpContext.Current.Server.UrlEncode(pDestinationURL), parameterCollection);
            return subURL;
        }
    }
}
