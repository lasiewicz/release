﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="TeaseSent20.ascx.cs" Inherits="Matchnet.Web.Applications.Email.TeaseSent20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="HotlistProfileStrip" Src="/Applications/Hotlist/HotlistProfileStrip.ascx" %>

<div id="page-container" class="hotlist-non-home">
<mn:Title runat="server" id="ttlTease" ResourceConstant="TXT_TEASE"/>
    <div class="confirmation-message clearfix editorial">
	    <mn:Image id="GrnTease" ResourceConstant="ALT_TEASE_SENT" titleResourceConstant="TTL_YOUR_TEASE_HAS_BEEN_SENT" FileName="icon-flirt-confirmation.gif" runat="server" />
	    <div class="confirmation-message-content">
	        <h2><mn:txt id="txtYourTeaseHasBeenSent" runat="server" ResourceConstant="TXT_YOUR_TEASE_HAS_BEEN_SENT" /></h2>
	        <p class="capacity"><mn:txt id="txtYouFlirtYouHave" runat="server" ResourceConstant="TXT_YOU_FLIRT_YOU_HAVE" />
	        <strong><asp:Literal ID="litTeasesLeft" Runat="server" /></strong> <mn:txt id="txtTeasesLeft" runat="server" ResourceConstant="TXT_TEASES_LEFT" /></p>
		    <p><mn:txt id="txtMemberMessage" Runat="server" ResourceConstant="TXT_MEMBER_MESSAGE" /></p>
	    </div>
    </div>
	
    <p><asp:HyperLink Runat="server" ID="backToProfileLink">
        <mn:txt id="txtBackToProfile" runat="server" titleResourceConstant="TTL_BACK_TO_PROFILE" ResourceConstant="TXT_BACK_TO_PROFILE" />
    </asp:HyperLink></p>

  <uc1:HotlistProfileStrip id="profileStrip" runat="server" NoResultsResource="MEMBER_WHO_HAVE_MARKETING" HotlistDirection="1" />
</div>
	
