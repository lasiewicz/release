﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using System.Text.RegularExpressions;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Email
{
    public enum GridType
    {
        None = 0, // this value is to support MailBox.ascx since it doesn't have the VIP grid
        Mail = 1,
        VIPMail = 2
    }

    public enum VIPColumns : int
    {
        VIP_COL_SELECT = 0, //0
        VIP_COL_STATUS,//1
        VIP_COL_FROM_USERNAME,//2
        VIP_COL_SUBJECT,//3
        VIP_COL_ALL_ACCESS_ICON,//4
        VIP_COL_INSERT_DATE,//5
        VIP_COL_OPENED_DATE,//6
        VIP_COL_STATUSMASK,//7
        VIP_COL_MAILID,//8
        VIP_COL_FOLDERID,//9
        VIP_COL_SUBJECT1,//10
        VIP_COL_FROMUSERNAME,//11
        VIP_COL_INSERTDATE,//12
        VIP_COL_FROMMEMBERID,//13
        VIP_COL_TOUSERNAME,//14
        VIP_COL_TOMEMBERID,//15
        VIP_COL_OPENEDDATE//16


    }

    public enum Columns : int
    {
        COL_SELECT = 0, //0
        COL_STATUS,//1
        COL_FROM_USERNAME,//2
        COL_SUBJECT,//3
        COL_INSERT_DATE,//4
        COL_OPENED_DATE,//5
        COL_OPENED_DATE_FOR_DISPLAY,//6
        COL_STATUSMASK,//7 
        COL_MAILID,//8
        COL_FOLDERID,//9
        COL_SUBJECT1,//10
        COL_FROMUSERNAME,//11
        COL_INSERTDATE,//12
        COL_FROMMEMBERID,//13
        COL_TOUSERNAME,//14
        COL_TOMEMBERID,//15
        COL_OPENEDDATE//16
    }
    public class MailBoxHandler : FrameworkControl
    {
        private const string BREAD_CRUMB_BANNER_LABEL = "lblMiddlePages";
        private const int MESSAGE_LIMIT_BYTES = 3000;
        private const int MESSAGE_SUBJECT_CROP_SIZE = 16;
        private const int MAILBOX_PAGE_SIZE = 10;
        private const int VIP_MAILBOX_PAGE_SIZE = 10;
        private const int MAILBOX_PAGE_BREAK_SEGMENTS = 3;
        private const string SESSION_PHOTOS_FLAG = "SESSION_PHOTOS_FLAG";
        private const string TXT_TOGGLE_PHOTO_SETTING_ON = "TXT_TOGGLE_PHOTO_SETTING_ON";
        private const string TXT_TOGGLE_PHOTO_SETTING_OFF = "TXT_TOGGLE_PHOTO_SETTING_OFF";
        public static string MESSAGE_ORIGIN_MAIL_BOX_URL_SESSION_KEY = "OriginMailBoxUrl";
        public static string ORDER_BY = "OrderBy";
        public static string VIP_ORDER_BY = "VIPOrderBy";
        public static string MESSSAGE_PAGE = "msgpg";
        public static string VIP_MESSAGE_PAGE = "vipmsgpg";

        IMailBox _mailBox;
        ContextGlobal g;

        public MailBoxHandler(IMailBox mailBox, ContextGlobal context)
        {
            _mailBox = mailBox;
            g = context;
        }

        public static void ValidMemberAccess(ContextGlobal g, bool pRunVIPCheck, int purchaseReasonType, WebConstants.MailAccessArea area)
        {
            bool validate = ValidateMemberAccessOnly(pRunVIPCheck, g, area);

            if (!validate)
            {
                Matchnet.Web.Framework.Util.Redirect.Subscription(g.Brand, purchaseReasonType,0);
            }
        }

        public static bool ValidateMemberAccessOnly(bool pRunVIPCheck, ContextGlobal g, WebConstants.MailAccessArea area)
        {
            bool validate = false;

            if (ApplyImailPermissions(g))
            {
                validate = true;
            }

            // If it is free to reply then validate.
            if (!g.TargetBrand.IsPayToReplySite)
                validate = true;

            if (g.TargetBrand.IsPaySite)
            {
                if (MemberPrivilegeAttr.IsPermitMember(g) || g.IsAdmin)
                {
                    validate = true;
                }

                #region site specific rules commented old
                //if (g.Brand.BrandID == (int) WebConstants.BRAND_ID.JDateCoIL ||
                //    g.Brand.BrandID == (int) WebConstants.BRAND_ID.Cupid)
                //{
                //    Matchnet.Member.ServiceAdapters.Member member = 
                //        Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                //        MemberLoadFlags.None);

                //    RegionLanguage region = RegionSA.Instance.RetrievePopulatedHierarchy(member.GetAttributeInt(g.Brand, "RegionID"), g.Brand.Site.LanguageID);

                //    if ( region.CountryRegionID  == g.Brand.Site.DefaultRegionID)
                //    {
                //        validate = true;
                //    }
                //}

                //if (g.Brand.Site.SiteID == (int) WebConstants.SITE_ID.JDateFR)
                //{
                //     Only members that speak French can open the mailbox folder
                //     Check access at the folder level first before checking access 
                //     at the item level
                //    if (FrameworkGlobals.recipientSpeaksSiteDefaultLanguage(g.Member.MemberID, g.Brand))
                //    {
                //        validate = true;
                //    }					
                //}
                #endregion

                #region site specific rules new
                if (!validate)
                    validate = FrameworkGlobals.MailAccessException(area, g, null);
                #endregion

                #region VIP All Access rule - if the member has a vip message, the member can view the folder
                if (!validate && pRunVIPCheck)
                    validate = MailBoxHandler.ContainsVIPMessage(MailBoxHandler.GetCurrentFolderIDEx(), g);
                #endregion
            }

            return validate;
        }

        /// <summary>
        /// Checks to see if a VIP folder corresponding to a system folder contains messages
        /// </summary>
        /// <param name="pFolderId"></param>
        /// <returns></returns>
        private static bool ContainsVIPMessage(int pFolderId, ContextGlobal g)
        {
            if (!VIPMailUtils.IsVIPEnabledSite(g.Brand))
            {
                return false;
            }

            int systemFolderId = pFolderId;
            int vipFolderId = VIPMailUtils.GetMatchingVIPFolderId(systemFolderId);
            if (vipFolderId < 0 && VIPMailUtils.IsVIPFolder(systemFolderId))
            {
                vipFolderId = systemFolderId;
            }
            var msgs = EmailMessageSA.Instance.RetrieveMessages(g.Member.MemberID, g.Brand.Site.Community.CommunityID, vipFolderId);
            if (msgs == null)
                return false;

            return msgs.Count > 0;
        }
        
        public string ResolveSortRequest(string pRequestParmName)
        {
            string orderBy = HttpContext.Current.Request.QueryString[pRequestParmName];

            if (orderBy == null || orderBy.Trim().Length < 1)
            {
                orderBy = "insertDate desc";
            }

            return orderBy;
        }

        public Int32 GetCurrentFolderID()
        {
            Int32 memberFolderID = Conversion.CInt(HttpContext.Current.Request.QueryString["MemberFolderID"]);

            if (memberFolderID == Constants.NULL_INT)
            {
                memberFolderID = (Int32)SystemFolders.Inbox;
            }

            return memberFolderID;
        }

        /// <summary>
        /// Retrieves the folder ID of the corresponding vip folder for the current folder ID that is a system folder
        /// </summary>
        /// <returns></returns>
        public static Int32 GetCurrentFolderIDEx()
        {
            Int32 memberFolderID = Conversion.CInt(HttpContext.Current.Request.QueryString["MemberFolderID"]);

            if (memberFolderID == Constants.NULL_INT)
            {
                memberFolderID = (Int32)SystemFolders.Inbox;
            }

            return VIPMailUtils.GetSystemFolderIDForVIPFolder(memberFolderID);
        }

        public void LoadNavigation()
        {
            g.RightNavigationVisible = true;
        }

        public void TogglePhotos()
        {
            try
            {
                _mailBox.PhotosFlag = !_mailBox.PhotosFlag;
                g.Session.Add(SESSION_PHOTOS_FLAG, _mailBox.PhotosFlag.ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.TemporaryDurable);
                _mailBox.TxtTogglePhotos.ResourceConstant = GetPhotoSettingConstant();

            }
            catch (Exception ex)
            { }
        }

        public void InitializePhotosSetting()
        {
            try
            {
                string sessionFlag = "";
                string flag = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MAILBOX_PHOTO_FLAG", g.Brand.Site.Community.CommunityID,
                g.Brand.Site.SiteID, g.Brand.BrandID);
                _mailBox.PhotosSettingFlag = Convert.ToBoolean(flag);
                if (_mailBox.PhotosSettingFlag)
                {
                    sessionFlag = g.Session.GetString(SESSION_PHOTOS_FLAG, null);
                    if (sessionFlag != null)
                    { _mailBox.PhotosFlag = Convert.ToBoolean(sessionFlag); }
                    else
                    { _mailBox.PhotosFlag = _mailBox.PhotosSettingFlag; }

                }

            }
            catch (Exception ex)
            { }
        }

        public static bool ApplyImailPermissions(ContextGlobal g)
        {
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMailPermissions", g.Brand.Site.Community.CommunityID,
                g.Brand.Site.SiteID, g.Brand.BrandID)))
            {
                if (g.TargetBrand.IsPaySite)
                {
                    if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    // free site.
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public string GetPhotoSettingConstant()
        {
            string resxConst = "";
            try
            {
                string folder = "_SENDER";

                int folderId = GetCurrentFolderID();

                if (folderId == (int)SystemFolders.Sent || folderId == (int)SystemFolders.Draft)
                {
                    folder = "_RECP";
                }

                resxConst = (_mailBox.PhotosFlag ? TXT_TOGGLE_PHOTO_SETTING_OFF + folder : TXT_TOGGLE_PHOTO_SETTING_ON + folder);
                return resxConst;
            }
            catch (Exception ex)
            { return resxConst; }
        }

        public string GetMailBoxName(EmailFolder pCurrentFolder)
        {
            // figure out what mailbox header to display...all system folders are resource directed
            string folderName = string.Empty;
            if (pCurrentFolder != null && pCurrentFolder.SystemFolder)
            {
                folderName = GetMailBoxName(pCurrentFolder.MemberFolderID);
            }
            else
            {
                if (pCurrentFolder != null)
                {
                    folderName = FrameworkGlobals.GetUnicodeText(pCurrentFolder.Description);
                }
            }
            return folderName;
        }

        public string GetMailBoxName(int folderID)
        {
            // figure out what mailbox header to display...all system folders are resource directed
            string folderName = string.Empty;

            switch (folderID)
            {
                case (int)SystemFolders.Inbox:
                    folderName = g.GetResource("NAV_SUB_INBOX", _mailBox.ResourceControl);
                    break;

                case (int)SystemFolders.Sent:
                    folderName = g.GetResource("TXT_SENT", _mailBox.ResourceControl);
                    break;

                case (int)SystemFolders.Draft:
                    folderName = g.GetResource("TXT_DRAFT", _mailBox.ResourceControl);
                    break;

                case (int)SystemFolders.Trash:
                    folderName = g.GetResource("TXT_TRASH", _mailBox.ResourceControl);
                    break;
            }

            return folderName;
        }

        public static string GetMailBoxName(int folderID, ContextGlobal g, FrameworkControl resourceControl)
        {
            // figure out what mailbox header to display...all system folders are resource directed
            string folderName = string.Empty;

            switch (folderID)
            {
                case (int)SystemFolders.Inbox:
                    folderName = g.GetResource("NAV_SUB_INBOX", resourceControl);
                    break;

                case (int)SystemFolders.Sent:
                    folderName = g.GetResource("TXT_SENT", resourceControl);
                    break;

                case (int)SystemFolders.Draft:
                    folderName = g.GetResource("TXT_DRAFT", resourceControl);
                    break;

                case (int)SystemFolders.Trash:
                    folderName = g.GetResource("TXT_TRASH", resourceControl);
                    break;
            }

            return folderName;
        }
        
        public void InitializeMessageList(int folderID, string OrderBy, int currentPage, string VIPOrderBy, int VIPCurrentPage, bool isMultiGrid, bool isPayingMember)
        {
            ICollection foundMessages = null;
            ICollection foundMessagesVIP = null;
            IEnumerator folderEnumerator = null;
            Label pageLabel = null;
            HyperLink fromHeader = null;
            HyperLink fromHeaderVIP = null;
            EmailFolder vipEmailFolder = null;
            bool accessValidationWithoutVIP = ValidateMemberAccessOnly(false, g, WebConstants.MailAccessArea.mailBox);

            // attempt to pull the custom mail box identifier from the initialized folders collection
            // used during the drop down list create
            //if (g.MemberEmailFolders != null && g.MemberEmailFolders.Count > 0)
            //{
            //    folderEnumerator = g.MemberEmailFolders.GetEnumerator();

            //    while (folderEnumerator.MoveNext() && _mailBox.CurrentEmailFolder == null)
            //    {
            //        _mailBox.CurrentEmailFolder = (EmailFolder)folderEnumerator.Current;
            //        if (_mailBox.CurrentEmailFolder.MemberFolderID != folderID)
            //        {
            //            _mailBox.CurrentEmailFolder = null;
            //        }
            //    }
            //}

            _mailBox.CurrentEmailFolder = GetEmailFolderByFolderID(folderID);

            #region Message retrieval
            if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USERNAME_APPROVAL_FLAG", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID).ToLower() == "true")
            {
                foundMessages = EmailMessageSA.Instance.RetrieveMessages(g.TargetMemberID,
                                                                        g.TargetCommunityID,
                                                                                        folderID,
                                                                                        currentPage,
                                                                                        MAILBOX_PAGE_SIZE,
                                                                                                    OrderBy,
                                                                                                        g.Brand);

                if (isMultiGrid) // VIP message enabled site that uses multi grids?
                {
                    int vipFolderId = VIPMailUtils.GetMatchingVIPFolderId(folderID);
                    if (vipFolderId != Constants.NULL_INT)
                    {
                        foundMessagesVIP = EmailMessageSA.Instance.RetrieveMessages(g.TargetMemberID,
                                                                            g.TargetCommunityID,
                                                                                            vipFolderId,
                                                                                            VIPCurrentPage,
                                                                                            VIP_MAILBOX_PAGE_SIZE,
                                                                                                        VIPOrderBy,
                                                                                                            g.Brand);
                    }
                }
            }
            else
            {
                foundMessages = EmailMessageSA.Instance.RetrieveMessages(g.TargetMemberID,
                                                                        g.TargetCommunityID,
                                                                                        folderID,
                                                                                        currentPage,
                                                                                        MAILBOX_PAGE_SIZE,
                                                                                                    OrderBy,
                                                                                                        g.Brand);

                if (isMultiGrid)
                {
                    int vipFolderId = VIPMailUtils.GetMatchingVIPFolderId(folderID);
                    if (vipFolderId != Constants.NULL_INT)
                    {
                        foundMessagesVIP = EmailMessageSA.Instance.RetrieveMessages(g.TargetMemberID,
                                                                            g.TargetCommunityID,
                                                                                            vipFolderId,
                                                                                            VIPCurrentPage,
                                                                                            VIP_MAILBOX_PAGE_SIZE,
                                                                                                        VIPOrderBy,
                                                                                                            g.Brand);
                    }
                }
            }
            #endregion

            #region Breadcrumb and some parameter setting for datagrid(s) binding
            _mailBox.CurrentFolderName = GetMailBoxName(_mailBox.CurrentEmailFolder);
            //Site Redesign - commenting BreadCrumb references basedon Layout template
            if (g.BreadCrumbTrailHeader != null)
            {
                g.BreadCrumbTrailHeader.SetThreeLinkCrumb(g.GetResource("TXT_MESSAGES", _mailBox.ResourceControl), g.AppPage.App.DefaultPagePath, _mailBox.CurrentFolderName, string.Empty);

            }
            if (g.BreadCrumbTrailFooter != null)
            {
                g.BreadCrumbTrailFooter.SetThreeLinkCrumb(g.GetResource("TXT_MESSAGES", _mailBox.ResourceControl), g.AppPage.App.DefaultPagePath, _mailBox.CurrentFolderName, string.Empty);
            }

            _mailBox.CurrentMessageNumber = currentPage - 1;	// used for request string parameter building during the datagrid data binding.
            _mailBox.MessageCount = EmailMessageSA.Instance.RetrieveMessages(g.TargetMemberID, g.TargetCommunityID, folderID, g.Brand.Site.SiteID).Count;

            if (isMultiGrid)
            {
                int vipFolderId = VIPMailUtils.GetMatchingVIPFolderId(folderID);
                if (vipFolderId != Constants.NULL_INT)
                {
                    // these seem to be used as request parameters for the view message link for the datagrid item data binding
                    _mailBox.CurrentVIPMessageNumber = VIPCurrentPage - 1;
                    _mailBox.VIPMessageCount = EmailMessageSA.Instance.RetrieveMessages(g.TargetMemberID, g.TargetCommunityID, vipFolderId, g.Brand.Site.SiteID).Count;
                    vipEmailFolder = GetEmailFolderByFolderID(vipFolderId);
                }
            }
            #endregion

            #region Pagination code
            // establish the breadcrumb trail and apply the specified stylesheetBra
            if (_mailBox.UseBreadCrumb && g.BreadCrumbTrailHeader != null)
            {
                pageLabel = (Label)g.BreadCrumbTrailHeader.FindControl(BREAD_CRUMB_BANNER_LABEL);
                //handleMessagePaging(_CurrentEmailFolder, pageLabel, "whiteLink", currentPage);
                if (pageLabel != null)
                {
                    HandleMessagePaging(_mailBox.CurrentEmailFolder, pageLabel, "breadCrumbPagesLink", currentPage);
                    pageLabel = (Label)g.BreadCrumbTrailFooter.FindControl(BREAD_CRUMB_BANNER_LABEL);
                    HandleMessagePaging(_mailBox.CurrentEmailFolder, pageLabel, "breadCrumbLower", currentPage);

                    if (isMultiGrid && _mailBox.LnkVIPPaging != null)
                    {
                        HandleVIPMessagePaging(vipEmailFolder, _mailBox.LnkVIPPaging, "breadCrumbPagesLink", VIPCurrentPage);
                    }
                }
            }
            else
            {
                if (VIPMailUtils.IsVIPEnabledSite(g.Brand))
                {
                    _mailBox.LnkTopPaging.Visible = false;
                    HandleMessagePaging(_mailBox.CurrentEmailFolder, _mailBox.LnkBottomPaging, "breadCrumbPagesLink", currentPage);
                }
                else
                {
                    HandleMessagePaging(_mailBox.CurrentEmailFolder, _mailBox.LnkTopPaging, "breadCrumbPagesLink", currentPage);
                    HandleMessagePaging(_mailBox.CurrentEmailFolder, _mailBox.LnkBottomPaging, "breadCrumbPagesLink", currentPage);
                }
                if (isMultiGrid && _mailBox.LnkVIPPaging != null)
                {
                    HandleVIPMessagePaging(vipEmailFolder, _mailBox.LnkVIPPaging, "breadCrumbPagesLink", VIPCurrentPage);
                }
            }
            #endregion
            //TT#21344 Ecard 
            #region  turn off e-card messages for websites which are not configured for ECard

            if (!g.EcardsEnabled)
            {
                System.Collections.ArrayList hold = new System.Collections.ArrayList();

                foreach (EmailMessage message in foundMessages)
                {
                    if (message.MailTypeID == MailType.ECard)
                    {
                        _mailBox.MessageCount--;
                    }
                    else
                    {
                        hold.Add(message);
                    }
                }
                foundMessages = hold;
            }
            #endregion

            #region Actual data binding happens here for both VIP and regular messages
            // if VIP messages found and the page supports it, then let's bind
            if (foundMessagesVIP != null && foundMessagesVIP.Count > 0 && _mailBox.VIPMailDataGrid != null)
            {
                _mailBox.VIPMailDataGrid.DataSource = foundMessagesVIP;
                _mailBox.VIPMailDataGrid.DataBind();
                ICollection vipheader = new ArrayList();
                
                _mailBox.VIPMailHeaderDataGrid.DataSource = vipheader;
                _mailBox.VIPMailHeaderDataGrid.DataBind();
                _mailBox.VIPMailFooterDataGrid.DataSource = vipheader;
                _mailBox.VIPMailFooterDataGrid.DataBind();
                // for mailboxes where the message direction is send rather than receive, flip the "from" column to "to"
                if (folderID == (int)SystemFolders.Draft || folderID == (int)SystemFolders.Sent || folderID == (int)SystemFolders.VIPSent)
                {
                    fromHeaderVIP = (HyperLink)_mailBox.VIPMailHeaderDataGrid.Controls[0].Controls[0].FindControl("itemFromSortLink");
                    if (fromHeaderVIP != null)
                    {
                        fromHeaderVIP.NavigateUrl = BuildHeaderSortLink("tousername desc", "tousername asc", GridType.VIPMail);
                        fromHeaderVIP.Text = g.GetResource("TXT_TO", _mailBox.ResourceControl);
                    }
                }
            }

            if (foundMessages.Count > 0)
            {
                // hide gridlines for cupid display...
                if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Cupid)
                {
                    _mailBox.MailDataGrid.GridLines = System.Web.UI.WebControls.GridLines.None;
                }

                // regular mail message bind
                _mailBox.MailDataGrid.DataSource = foundMessages;
                _mailBox.MailDataGrid.DataBind();

                // for mailboxes where the message direction is send rather than receive, flip the "from" column to "to"
                if (folderID == (int)SystemFolders.Draft || folderID == (int)SystemFolders.Sent)
                {
                    fromHeader = (HyperLink)_mailBox.MailDataGrid.Controls[0].Controls[0].FindControl("itemFromSortLink");
                    if (fromHeader != null)
                    {
                        GridType gridType = isMultiGrid ? GridType.Mail : GridType.None;
                        fromHeader.NavigateUrl = BuildHeaderSortLink("tousername desc", "tousername asc", gridType);
                        fromHeader.Text = g.GetResource("TXT_TO", _mailBox.ResourceControl);
                    }
                }

                // if this member got inbox access due to the VIP message rule, only allow them to view the VIP messages
                //if (!accessValidationWithoutVIP && !isPayingMember)
                //{
                //    _mailBox.MailDataGrid.Visible = false;
                //}

            }
            else
            {
                // todo: i don't know what this is.  this seems to be some sort of rewind 1 page back logic, but i don't know when it kicks in
                // look into it later when i have more time
                if (g.Page.Request.Url.AbsoluteUri.ToLower().Contains("msgpg"))
                {
                    int msgpg = int.Parse(g.Page.Request["msgpg"]);
                    if (msgpg > MAILBOX_PAGE_SIZE)
                    {
                        string newURL =
                            g.Page.Request.Url.AbsoluteUri.ToLower().Replace("msgpg=" + g.Page.Request["msgpg"],
                                                                             "msgpg=" +
                                                                             (msgpg - MAILBOX_PAGE_SIZE).ToString());
                        g.Transfer(newURL);
                    }
                }
                
            }

            bool isFolderEmpty = false;
            if (foundMessagesVIP == null)
            {
                isFolderEmpty = foundMessages.Count < 1;

            }
            else
            {
                isFolderEmpty = (foundMessages.Count < 1) && (foundMessagesVIP.Count < 1);
            }

            if (isFolderEmpty)
            {
                // if no messages are in the current folder, hide all message related page
                // components and display 'empty folder' panel
                _mailBox.MailBarPanel.Visible = false;
                _mailBox.MailDataGrid.Visible = false;
                _mailBox.EmptyFolderPanel.Visible = true;
            }
            #endregion

            //buildAccountUsage();

            if (_mailBox.UseBreadCrumb)
                _mailBox.TxtMailBox.Text = _mailBox.CurrentFolderName + " " + _mailBox.SelectedPageSegment + " " + _mailBox.MessageCountTitle;
            else
            {
                if (_mailBox.CurrentEmailFolder.SystemFolder)
                    _mailBox.TxtMailBox.Text = g.GetResource("TTL_" + _mailBox.CurrentFolderName.ToUpper().Trim(), _mailBox.ResourceControl);
                else
                    _mailBox.TxtMailBox.Text = g.GetResource("TTL_CUSTOM", _mailBox.ResourceControl);

            }

            if (folderID == (Int32)SystemFolders.Trash)
            {
                _mailBox.BtnDeleteMessage.Visible = false;
                _mailBox.BtnEmptyFolder.Visible = true;
                _mailBox.BtnDeleteAllMessages.Visible = false;
            }
            else
            {
                _mailBox.BtnEmptyFolder.Visible = false;
            }
        }

        /// <summary>
        /// If the passed in folder id is one of the VIP folder id's, the corresponding system folder id is returned.
        /// The reason why we have to do this is because we are using 1 page to display 2 mail folders.
        /// A lot of the UI logic only looks for 1 folder id to operate on, so we are tricking those elements to think nothing has changed.
        /// </summary>
        /// <param name="pFolderID"></param>
        /// <returns></returns>
       

        public static EmailFolder GetCurrentFolder(ContextGlobal g, int currentFolderId)
        {
            IEnumerator folderEnumerator = null;
            if (g.MemberEmailFolders != null && g.MemberEmailFolders.Count > 0)
            {
                folderEnumerator = g.MemberEmailFolders.GetEnumerator();

                while (folderEnumerator.MoveNext())
                {

                    if (((EmailFolder)folderEnumerator.Current).MemberFolderID == currentFolderId)
                    {
                        return (EmailFolder)folderEnumerator.Current;
                    }
                }
            }
            return null;
        }

        public string BuildHeaderSortLink(string defaultRequestParameter, string alternateParameter, GridType pGridType)
        {
            StringBuilder linkBuilder = new StringBuilder();

            if (pGridType == GridType.None) // default old behavior
            {
                if (HttpContext.Current.Request.QueryString["OrderBy"] != null && HttpContext.Current.Request.QueryString["OrderBy"].Trim().Equals(defaultRequestParameter))
                {
                    linkBuilder.Append("/Applications/Email/MailBox.aspx?OrderBy=" + alternateParameter);
                }
                else
                {
                    linkBuilder.Append("/Applications/Email/MailBox.aspx?OrderBy=" + defaultRequestParameter);
                }
            }
            else
            {
                // we have 2 grids on a single page, so we want to preserve the grid sort/paging status on the other grid
                string changeSort;
                string preserveSort;
                string preservePageNum;

                if (pGridType == GridType.Mail)
                {
                    changeSort = ORDER_BY;
                    preserveSort = VIP_ORDER_BY;
                    preservePageNum = VIP_MESSAGE_PAGE;
                }
                else
                {
                    changeSort = VIP_ORDER_BY;
                    preserveSort = ORDER_BY;
                    preservePageNum = MESSSAGE_PAGE;
                }

                if (HttpContext.Current.Request.QueryString[changeSort] != null && HttpContext.Current.Request.QueryString[changeSort].Trim().Equals(defaultRequestParameter))
                {
                    linkBuilder.Append("/Applications/Email/MailBox.aspx?" + changeSort + "=" + alternateParameter);
                }
                else
                {
                    linkBuilder.Append("/Applications/Email/MailBox.aspx?" + changeSort + "=" + defaultRequestParameter);
                }

                if (HttpContext.Current.Request.QueryString[preserveSort] != null)
                {
                    linkBuilder.Append("&" + preserveSort + "=" + HttpContext.Current.Request.QueryString[preserveSort]);
                }

                if (HttpContext.Current.Request.QueryString[preservePageNum] != null)
                {
                    linkBuilder.Append("&" + preservePageNum + "=" + HttpContext.Current.Request.QueryString[preservePageNum]);
                }

            }

            linkBuilder.Append("&MemberFolderID=" + GetCurrentFolderID());
            return linkBuilder.ToString();
        }

        public void BuildOutMoveMailDropDownList(int bypassFolder)
        {
            ListItem dropDownListItem = null;
            IEnumerator folderEnumerator = null;
            EmailFolder currentFolder = null;

            folderEnumerator = g.MemberEmailFolders.GetEnumerator();

            // fill out the move to drop down list
            dropDownListItem = new ListItem(g.GetResource("TXT_MOVE_TO_FOLDER_ELLIPSIS", _mailBox.ResourceControl), "None");
            _mailBox.DropDownListMove.Items.Add(dropDownListItem);

            while (folderEnumerator.MoveNext())
            {
                currentFolder = (EmailFolder)folderEnumerator.Current;

                // only populate the drop down list with the any custom folders and inbox/trash depending on current folder
                if (currentFolder.MemberFolderID != (int)SystemFolders.Sent && currentFolder.MemberFolderID != (int)SystemFolders.Draft)
                {
                    if (currentFolder.MemberFolderID != bypassFolder && !VIPMailUtils.IsVIPFolder(currentFolder.MemberFolderID))
                    {
                        if (currentFolder.SystemFolder)
                        {	// Must translate the folder description
                            switch (currentFolder.MemberFolderID)
                            {
                                case (int)SystemFolders.Inbox:
                                    currentFolder.Description = g.GetResource("TOK_INBOX", _mailBox.ResourceControl);
                                    break;
                                case (int)SystemFolders.Trash:
                                    currentFolder.Description = g.GetResource("TXT_TRASH", _mailBox.ResourceControl);
                                    break;
                            }
                        }
                        dropDownListItem = new ListItem(currentFolder.Description, currentFolder.MemberFolderID.ToString());
                        _mailBox.DropDownListMove.Items.Add(dropDownListItem);
                    }
                }
            }
        }

        public ArrayList GetSelectedMessages(bool bypassCurrentMember, ref ArrayList memberIDs)
        {
            DataGridItem currentItem = null;
            HtmlInputCheckBox currentCheckBox = null;
            IEnumerator dataGridEnumerator = null;
            ArrayList selectedMessages = new ArrayList();
            memberIDs = new ArrayList();
            int currentFolder = GetCurrentFolderID();
            int compareID = 0;
            bool foundDuplicate = false;

            #region Enumerate through the regular message grid to pick up the memberid's and the messagelistid's
            dataGridEnumerator = _mailBox.MailDataGrid.Items.GetEnumerator();
            while (dataGridEnumerator.MoveNext())
            {
                currentItem = (DataGridItem)dataGridEnumerator.Current;
                currentCheckBox = (HtmlInputCheckBox)currentItem.FindControl("itemCheckBox");

                if (currentCheckBox != null && currentCheckBox.Checked)
                {
                    if (bypassCurrentMember)
                    {
                        if (currentFolder == (int)SystemFolders.Sent || currentFolder == (int)SystemFolders.Draft)
                        {
                            // this is the sender's memberID although the constant name says otherwise
                            compareID = int.Parse(currentItem.Cells[(int)Columns.COL_OPENEDDATE].Text);
                        }
                        else
                        {
                            // this is the sender's memberID although the constant name says otherwise
                            compareID = int.Parse(currentItem.Cells[(int)Columns.COL_TOUSERNAME].Text);
                        }

                        if (g.TargetMemberID == compareID)
                        {
                            foundDuplicate = true;
                        }
                    }

                    if (!foundDuplicate)
                    {
                        selectedMessages.Add(Conversion.CInt(currentItem.Cells[(int)Columns.COL_MAILID].Text));
                        memberIDs.Add(compareID);
                    }
                    else
                    {
                        _mailBox.IgnoreSelf = true;
                        foundDuplicate = false;
                    }
                }
            }
            #endregion

            #region Enumerate through VIP message grid and do the same
            if (_mailBox.VIPMailDataGrid != null)
            {
                dataGridEnumerator = _mailBox.VIPMailDataGrid.Items.GetEnumerator();
                while (dataGridEnumerator.MoveNext())
                {
                    currentItem = (DataGridItem)dataGridEnumerator.Current;
                    currentCheckBox = (HtmlInputCheckBox)currentItem.FindControl("itemCheckBox");

                    if (currentCheckBox != null && currentCheckBox.Checked)
                    {
                        if (bypassCurrentMember)
                        {
                            if (currentFolder == (int)SystemFolders.Sent || currentFolder == (int)SystemFolders.Draft)
                            {
                                compareID = int.Parse(currentItem.Cells[(int)VIPColumns.VIP_COL_OPENEDDATE].Text);
                            }
                            else
                            {
                                compareID = int.Parse(currentItem.Cells[(int)VIPColumns.VIP_COL_TOMEMBERID].Text);
                            }

                            if (g.TargetMemberID == compareID)
                            {
                                foundDuplicate = true;
                            }
                        }

                        if (!foundDuplicate)
                        {
                            selectedMessages.Add(Conversion.CInt(currentItem.Cells[(int)VIPColumns.VIP_COL_MAILID].Text));
                            memberIDs.Add(compareID);
                        }
                        else
                        {
                            _mailBox.IgnoreSelf = true;
                            foundDuplicate = false;
                        }
                    }
                }
            }
            #endregion

            return selectedMessages;
        }

        public ArrayList GetSelectedMessages(bool bypassCurrentMember)
        {
            ArrayList memberIDs = null;
            return GetSelectedMessages(bypassCurrentMember, ref memberIDs);
        }

        public int GetFolderMessageCount(int folderID)
        {
            int count = EmailMessageSA.Instance.RetrieveMessages(g.TargetMemberID, g.TargetCommunityID, folderID, g.Brand.Site.SiteID).Count;
            return count;
        }

        public static int GetFolderMessageCount(int folderID, ContextGlobal g)
        {
            int count = EmailMessageSA.Instance.RetrieveMessages(g.TargetMemberID, g.TargetCommunityID, folderID, g.Brand.Site.SiteID).Count;
            return count;
        }

        public string GetOmniturePageName()
        {
            return "Messages - " + _mailBox.CurrentFolderName;
        }

        public static void SetupTabBar(Framework.Ui.BasicElements.TabBar mailBoxTabBar, bool systemFolder, FrameworkControl resourceControl, ContextGlobal g, int selectedTabID)
        {
            try
            {
                mailBoxTabBar.HrefBase = "/Applications/Email/MailBox.aspx?MemberFolderID";
                mailBoxTabBar.HrefFormat = "{0}={1}";
                mailBoxTabBar.ClassSelected = "selected";
                mailBoxTabBar.MaxTextLen = 20;
                mailBoxTabBar.ResourceControl = resourceControl;
                if (selectedTabID != Constants.NULL_INT)
                {
                    mailBoxTabBar.SelectedTabID = selectedTabID;
                }
                else
                {
                    mailBoxTabBar.SelectedTabID = GetCurrentFolderIDEx();
                }
                mailBoxTabBar.Clear();
                int communityid = g.Brand.Site.Community.CommunityID;
                int siteid = g.Brand.Site.SiteID;
                int count = 0;
                bool isVIPSite = VIPMailUtils.IsVIPEnabledSite(g.Brand);
                int tabOrder = 1;
                if (systemFolder)
                {
                    count = GetFolderMessageCount((int)Matchnet.Email.ValueObjects.SystemFolders.Inbox, g);
                    if (isVIPSite)
                    {
                        count += GetFolderMessageCount((int)Matchnet.Email.ValueObjects.SystemFolders.VIPInbox, g);
                    }
                    mailBoxTabBar.AddTab((int)Matchnet.Email.ValueObjects.SystemFolders.Inbox, tabOrder, count, false, "", "", GetMailBoxName((int)Matchnet.Email.ValueObjects.SystemFolders.Inbox, g, resourceControl));
                    tabOrder++;

                    if (IMManager.Instance.IsImHistoryEnabled(g.Brand))
                    {
                        mailBoxTabBar.AddTab(-1, tabOrder, Constants.NULL_INT, false, "/Applications/Email/IMHistory.aspx", "", g.GetResource("TXT_IMHISTORY", resourceControl), 50);
                        tabOrder++;
                    }

                    count = GetFolderMessageCount((int)Matchnet.Email.ValueObjects.SystemFolders.Draft, g);
                    if (isVIPSite)
                    {
                        count += GetFolderMessageCount((int)Matchnet.Email.ValueObjects.SystemFolders.VIPDraft, g);
                    }
                    mailBoxTabBar.AddTab((int)Matchnet.Email.ValueObjects.SystemFolders.Draft, tabOrder, count, false, "", "", GetMailBoxName((int)Matchnet.Email.ValueObjects.SystemFolders.Draft, g, resourceControl));
                    tabOrder++;

                    count = GetFolderMessageCount((int)Matchnet.Email.ValueObjects.SystemFolders.Sent, g);
                    if (isVIPSite)
                    {
                        count += GetFolderMessageCount((int)Matchnet.Email.ValueObjects.SystemFolders.VIPSent, g);
                    }
                    mailBoxTabBar.AddTab((int)Matchnet.Email.ValueObjects.SystemFolders.Sent, tabOrder, count, false, "", "", GetMailBoxName((int)Matchnet.Email.ValueObjects.SystemFolders.Sent, g, resourceControl));
                    tabOrder++;

                    count = GetFolderMessageCount((int)Matchnet.Email.ValueObjects.SystemFolders.Trash, g);
                    if (isVIPSite)
                    {
                        count += GetFolderMessageCount((int)Matchnet.Email.ValueObjects.SystemFolders.VIPTrash, g);
                    }
                    mailBoxTabBar.AddTab((int)Matchnet.Email.ValueObjects.SystemFolders.Trash, tabOrder, count, false, "", "", GetMailBoxName((int)Matchnet.Email.ValueObjects.SystemFolders.Trash, g, resourceControl));
                    tabOrder++;

                }
                else
                {
                    int i = 0;
                    foreach (EmailFolder folder in g.MemberEmailFolders)
                    {
                        if (!folder.SystemFolder)
                        {
                            i++;
                            count = GetFolderMessageCount(folder.MemberFolderID, g);
                            mailBoxTabBar.AddTab(folder.MemberFolderID, i, count, false, "", "", folder.Description);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (g != null)
                {
                    g.ProcessException(ex);
                }
            }
        }

        public void AddMailBoxURLToSession(string url)
        {
            g.Session.Add(MESSAGE_ORIGIN_MAIL_BOX_URL_SESSION_KEY, url, SessionPropertyLifetime.Temporary);
        }

        public EmailMessage GetFirstUnreadVIPMessage()
        {
            EmailMessage vipmessage=null;
            try
            {
                if(g.Member==null) return vipmessage;
                ArrayList vipunreadmessages = EmailMessageSA.Instance.RetrieveMessagesByStatus(g.Member.MemberID, g.Brand, (int)SystemFolders.VIPInbox, (MessageStatus.Read | MessageStatus.Replied), false, "insertdate asc", 1);

                if (vipunreadmessages != null && vipunreadmessages.Count > 0)
                    vipmessage = (EmailMessage)vipunreadmessages[0];

                return vipmessage;
            }
            catch (Exception ex)
            { g.ProcessException(ex); return vipmessage; }


        }

        /// <summary>
        /// this site can receive vip messages
        /// </summary>
       
        /// <summary>
        /// this site can send vip messages
        /// </summary>
        public static bool IsVIPSendEnabledSite(Matchnet.Content.ValueObjects.BrandConfig.Brand pBrand)
        {
            return Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("VIP_EMAIL_SEND_ENABLED", pBrand.Site.Community.CommunityID, pBrand.Site.SiteID, pBrand.BrandID));
        }
        
        private void HandleMessagePaging(EmailFolder currentFolder, Label pageLabel, string cssClass, int currentPage)
        {
            Label seperator = null;
            Label messageCountHyperLink = null;
            HyperLink pageLink = null;
            string firstPageInSegment = null;
            int totalPages = 0;
            int startPage = 0;
            bool finalSegment = false;
            Int32 totalMessages = EmailMessageSA.Instance.RetrieveMessages(g.Member.MemberID,
                                                                g.Brand.Site.Community.CommunityID,
                                                                            currentFolder.MemberFolderID,
                                                                                    g.Brand.Site.SiteID).Count;

            if (pageLabel != null && currentFolder != null)
            {
                totalPages = (Int32)Math.Ceiling((double)totalMessages / MAILBOX_PAGE_SIZE);

                // clear out the datagrid footer page links
                _mailBox.PreviousPageLink = null;
                _mailBox.NextPageLink = null;

                // set the start and end position for the current segment to page thru
                if (totalPages > MAILBOX_PAGE_BREAK_SEGMENTS)
                {
                    if (currentPage < ((MAILBOX_PAGE_BREAK_SEGMENTS - 1) * MAILBOX_PAGE_SIZE))
                    {
                        totalPages = MAILBOX_PAGE_BREAK_SEGMENTS;
                    }
                    else if (currentPage > ((totalPages - (MAILBOX_PAGE_BREAK_SEGMENTS - 1)) * MAILBOX_PAGE_SIZE))
                    {
                        startPage = (totalPages - MAILBOX_PAGE_BREAK_SEGMENTS);
                        totalPages = (startPage + MAILBOX_PAGE_BREAK_SEGMENTS);
                        finalSegment = true;
                    }
                    else
                    {
                        startPage = (currentPage / MAILBOX_PAGE_SIZE - 1);
                        totalPages = (startPage + MAILBOX_PAGE_BREAK_SEGMENTS);
                    }
                }
                else
                {
                    finalSegment = true;
                }

                // no messages are found
                if (totalPages == 0)
                {
                    pageLink = new HyperLink();
                    pageLink.Text = _mailBox.SelectedPageSegment = "0";
                    pageLink.CssClass = cssClass;
                    pageLabel.Controls.Add(pageLink);
                }

                // build out the paging components on the page provided a start and stop point
                for (int i = startPage; i < totalPages; i++)
                {

                    // establish the first page in this segment, this is identical for all paging components
                    firstPageInSegment = (i * MAILBOX_PAGE_SIZE + 1).ToString();

                    // attach a leading '/' to the paging link if this segment is not the first page
                    if (i != 0 && i < (startPage + 1))
                    {
                        seperator = new Label();
                        seperator.Text = " / ";
                        seperator.CssClass = cssClass;
                        pageLabel.Controls.Add(seperator);
                    }

                    pageLink = new HyperLink();

                    if (HttpContext.Current.Request.QueryString["OrderBy"] != null && HttpContext.Current.Request.QueryString["OrderBy"].Trim().Length > 0)
                    {
                        if (HttpContext.Current.Request.QueryString["MemberFolderID"] != null && HttpContext.Current.Request.QueryString["MemberFolderID"].Trim().Length > 0)
                        {
                            pageLink.NavigateUrl = "/Applications/Email/MailBox.aspx?msgpg=" + firstPageInSegment + "&OrderBy=" + HttpContext.Current.Request.QueryString["OrderBy"] + "&MemberFolderID=" + HttpContext.Current.Request.QueryString["MemberFolderID"];
                        }
                        else
                        {
                            pageLink.NavigateUrl = "/Applications/Email/MailBox.aspx?msgpg=" + firstPageInSegment + "&OrderBy=" + HttpContext.Current.Request.QueryString["OrderBy"] + "&MemberFolderID=" + (int)SystemFolders.Inbox;
                        }
                    }
                    else
                    {
                        if (HttpContext.Current.Request.QueryString["MemberFolderID"] != null && HttpContext.Current.Request.QueryString["MemberFolderID"].Trim().Length > 0)
                        {
                            pageLink.NavigateUrl = "/Applications/Email/MailBox.aspx?msgpg=" + firstPageInSegment + "&OrderBy=" + ResolveSortRequest("OrderBy") + "&MemberFolderID=" + HttpContext.Current.Request.QueryString["MemberFolderID"];
                        }
                        else
                        {
                            pageLink.NavigateUrl = "/Applications/Email/MailBox.aspx?msgpg=" + firstPageInSegment + "&OrderBy=" + ResolveSortRequest("OrderBy") + "&MemberFolderID=" + (int)SystemFolders.Inbox;
                        }
                    }

                    // preserve the VIP grid sorting and pagination since this method only gets called from the regular datagrid
                    if (HttpContext.Current.Request.QueryString[VIP_ORDER_BY] != null && HttpContext.Current.Request.QueryString[VIP_ORDER_BY].Trim().Length > 0)
                    {
                        pageLink.NavigateUrl += "&" + VIP_ORDER_BY + "=" +
                                                HttpContext.Current.Request.QueryString[VIP_ORDER_BY];
                    }

                    if (HttpContext.Current.Request.QueryString[VIP_MESSAGE_PAGE] != null && HttpContext.Current.Request.QueryString[VIP_MESSAGE_PAGE].Trim().Length > 0)
                    {
                        pageLink.NavigateUrl += "&" + VIP_MESSAGE_PAGE + "=" +
                                                HttpContext.Current.Request.QueryString[VIP_MESSAGE_PAGE];
                    }

                    // ensure the current page link being generated is for a true previous page and set
                    // previousPageLink which is expected by the message datagrid for footer paging
                    if (currentPage > 1)
                    {
                        if ((currentPage - MAILBOX_PAGE_SIZE) == i * MAILBOX_PAGE_SIZE + 1)
                        {
                            _mailBox.PreviousPageLink = pageLink.NavigateUrl;
                        }
                    }

                    // ensure the current page link being generated is for a true next page and set
                    // nextPageLink which is expected by the message datagrid for footer paging
                    if ((currentPage + MAILBOX_PAGE_SIZE) <= totalMessages)
                    {
                        if (i > 0 && ((MAILBOX_PAGE_SIZE + currentPage) == i * MAILBOX_PAGE_SIZE + 1))
                        {
                            _mailBox.NextPageLink = pageLink.NavigateUrl;
                        }
                    }

                    // if the current page is selected, strong type the font
                    if (currentPage != i * MAILBOX_PAGE_SIZE + 1)
                    {

                        // check to see if this is the final time through the paging logic, if so see if there are remainder pages
                        if ((i == (totalPages - 1)) && (totalMessages % MAILBOX_PAGE_SIZE != 0) && finalSegment)
                        {
                            pageLink.Text = firstPageInSegment + "-" + (totalMessages % MAILBOX_PAGE_SIZE + MAILBOX_PAGE_SIZE * i).ToString();
                        }
                        else
                        {
                            pageLink.Text = firstPageInSegment + "-" + (MAILBOX_PAGE_SIZE + (MAILBOX_PAGE_SIZE * i)).ToString();
                        }
                    }
                    else
                    {
                        string lastPageInSegment = string.Empty;
                        // check to see if this is the final time through the paging logic, if so see if there are remainder pages
                        if ((i == (totalPages - 1)) && (totalMessages % MAILBOX_PAGE_SIZE != 0) && finalSegment)
                        {
                            lastPageInSegment = (totalMessages % MAILBOX_PAGE_SIZE + MAILBOX_PAGE_SIZE * i).ToString();
                        }
                        else
                        {
                            lastPageInSegment = (MAILBOX_PAGE_SIZE + (MAILBOX_PAGE_SIZE * i)).ToString();
                        }
                        pageLink.Text = "<strong>" + firstPageInSegment + "-" + lastPageInSegment + "</strong>";
                        _mailBox.SelectedPageSegment = pageLink.Text;
                    }

                    pageLink.CssClass = cssClass;
                    pageLabel.Controls.Add(pageLink);

                    // append a footer '/' if the last page and segment has not been reached
                    if (i < (totalPages - 1) || !finalSegment)
                    {
                        seperator = new Label();
                        seperator.Text = " / ";
                        seperator.CssClass = cssClass;
                        pageLabel.Controls.Add(seperator);
                    }
                }

                // finish out the display with total messages in folder
                messageCountHyperLink = new Label();
                messageCountHyperLink.CssClass = cssClass;
                g.ExpansionTokens.Remove("MESSAGES");
                g.ExpansionTokens.Add("MESSAGES", totalMessages.ToString());
                messageCountHyperLink.Text = " " + g.GetResource("TXT_OF_MESSAGES", _mailBox.ResourceControl);
                pageLabel.Controls.Add(messageCountHyperLink);


                // This info is now used as part of the page title.
                if (_mailBox.MessageCountTitle == null)
                {
                    _mailBox.MessageCountTitle = messageCountHyperLink.Text;
                }
            }
        }

        private void HandleVIPMessagePaging(EmailFolder currentFolder, Label pageLabel, string cssClass, int currentPage)
        {
            Label seperator = null;
            Label messageCountHyperLink = null;
            HyperLink pageLink = null;
            string firstPageInSegment = null;
            int totalPages = 0;
            int startPage = 0;
            bool finalSegment = false;
            Int32 totalMessages = 0;

            if (currentFolder != null)
            {
                totalMessages = EmailMessageSA.Instance.RetrieveMessages(g.Member.MemberID,
                                                                g.Brand.Site.Community.CommunityID,
                                                                            currentFolder.MemberFolderID,
                                                                                    g.Brand.Site.SiteID).Count;
            }

            if (pageLabel != null && currentFolder != null)
            {
                totalPages = (Int32)Math.Ceiling((double)totalMessages / VIP_MAILBOX_PAGE_SIZE);

                // clear out the datagrid footer page links
                _mailBox.VIPPreviousPageLink = null;
                _mailBox.VIPNextPageLink = null;

                // set the start and end position for the current segment to page thru
                if (totalPages > MAILBOX_PAGE_BREAK_SEGMENTS)
                {
                    if (currentPage < ((MAILBOX_PAGE_BREAK_SEGMENTS - 1) * VIP_MAILBOX_PAGE_SIZE))
                    {
                        totalPages = MAILBOX_PAGE_BREAK_SEGMENTS;
                    }
                    else if (currentPage > ((totalPages - (MAILBOX_PAGE_BREAK_SEGMENTS - 1)) * VIP_MAILBOX_PAGE_SIZE))
                    {
                        startPage = (totalPages - MAILBOX_PAGE_BREAK_SEGMENTS);
                        totalPages = (startPage + MAILBOX_PAGE_BREAK_SEGMENTS);
                        finalSegment = true;
                    }
                    else
                    {
                        startPage = (currentPage / VIP_MAILBOX_PAGE_SIZE - 1);
                        totalPages = (startPage + MAILBOX_PAGE_BREAK_SEGMENTS);
                    }
                }
                else
                {
                    finalSegment = true;
                }

                // no messages are found
                if (totalPages == 0)
                {
                    pageLink = new HyperLink();
                    pageLink.Text = _mailBox.VIPSelectedPageSegment = "0";
                    pageLink.CssClass = cssClass;
                    pageLabel.Controls.Add(pageLink);
                }

                // build out the paging components on the page provided a start and stop point
                for (int i = startPage; i < totalPages; i++)
                {

                    // establish the first page in this segment, this is identical for all paging components
                    firstPageInSegment = (i * VIP_MAILBOX_PAGE_SIZE + 1).ToString();

                    // attach a leading '/' to the paging link if this segment is not the first page
                    if (i != 0 && i < (startPage + 1))
                    {
                        seperator = new Label();
                        seperator.Text = " / ";
                        seperator.CssClass = cssClass;
                        pageLabel.Controls.Add(seperator);
                    }

                    pageLink = new HyperLink();

                    if (HttpContext.Current.Request.QueryString[VIP_ORDER_BY] != null && HttpContext.Current.Request.QueryString[VIP_ORDER_BY].Trim().Length > 0)
                    {
                        if (HttpContext.Current.Request.QueryString["MemberFolderID"] != null && HttpContext.Current.Request.QueryString["MemberFolderID"].Trim().Length > 0)
                        {
                            pageLink.NavigateUrl = "/Applications/Email/MailBox.aspx?" + VIP_MESSAGE_PAGE + "=" + firstPageInSegment +
                                "&" + VIP_ORDER_BY + "=" + HttpContext.Current.Request.QueryString[VIP_ORDER_BY] + "&MemberFolderID=" +
                                HttpContext.Current.Request.QueryString["MemberFolderID"];
                        }
                        else
                        {
                            pageLink.NavigateUrl = "/Applications/Email/MailBox.aspx?" + VIP_MESSAGE_PAGE + "=" + firstPageInSegment +
                                "&" + VIP_ORDER_BY + "=" + HttpContext.Current.Request.QueryString[VIP_ORDER_BY] + "&MemberFolderID=" +
                                 (int)SystemFolders.Inbox;
                        }
                    }
                    else
                    {
                        if (HttpContext.Current.Request.QueryString["MemberFolderID"] != null && HttpContext.Current.Request.QueryString["MemberFolderID"].Trim().Length > 0)
                        {
                            pageLink.NavigateUrl = "/Applications/Email/MailBox.aspx?" + VIP_MESSAGE_PAGE + "=" + firstPageInSegment +
                                "&" + VIP_ORDER_BY + "=" + ResolveSortRequest(VIP_ORDER_BY) + "&MemberFolderID=" +
                                HttpContext.Current.Request.QueryString["MemberFolderID"];
                        }
                        else
                        {
                            pageLink.NavigateUrl = "/Applications/Email/MailBox.aspx?" + VIP_MESSAGE_PAGE + "=" + firstPageInSegment +
                                "&" + VIP_ORDER_BY + "=" + ResolveSortRequest(VIP_ORDER_BY) + "&MemberFolderID=" +
                                 (int)SystemFolders.Inbox;
                        }
                    }

                    // preserve the VIP grid sorting and pagination since this method only gets called from the regular datagrid
                    if (HttpContext.Current.Request.QueryString[ORDER_BY] != null && HttpContext.Current.Request.QueryString[ORDER_BY].Trim().Length > 0)
                    {
                        pageLink.NavigateUrl += "&" + ORDER_BY + "=" +
                                                HttpContext.Current.Request.QueryString[ORDER_BY];
                    }

                    if (HttpContext.Current.Request.QueryString[MESSSAGE_PAGE] != null && HttpContext.Current.Request.QueryString[MESSSAGE_PAGE].Trim().Length > 0)
                    {
                        pageLink.NavigateUrl += "&" + MESSSAGE_PAGE + "=" +
                                                HttpContext.Current.Request.QueryString[MESSSAGE_PAGE];
                    }

                    // ensure the current page link being generated is for a true previous page and set
                    // previousPageLink which is expected by the message datagrid for footer paging
                    if (currentPage > 1)
                    {
                        if ((currentPage - VIP_MAILBOX_PAGE_SIZE) == i * VIP_MAILBOX_PAGE_SIZE + 1)
                        {
                            _mailBox.VIPPreviousPageLink = pageLink.NavigateUrl;
                        }
                    }

                    // ensure the current page link being generated is for a true next page and set
                    // nextPageLink which is expected by the message datagrid for footer paging
                    if ((currentPage + VIP_MAILBOX_PAGE_SIZE) <= totalMessages)
                    {
                        if (i > 0 && ((VIP_MAILBOX_PAGE_SIZE + currentPage) == i * VIP_MAILBOX_PAGE_SIZE + 1))
                        {
                            _mailBox.VIPNextPageLink = pageLink.NavigateUrl;
                        }
                    }

                    // if the current page is selected, strong type the font
                    if (currentPage != i * VIP_MAILBOX_PAGE_SIZE + 1)
                    {

                        // check to see if this is the final time through the paging logic, if so see if there are remainder pages
                        if ((i == (totalPages - 1)) && (totalMessages % VIP_MAILBOX_PAGE_SIZE != 0) && finalSegment)
                        {
                            pageLink.Text = firstPageInSegment + "-" + (totalMessages % VIP_MAILBOX_PAGE_SIZE + VIP_MAILBOX_PAGE_SIZE * i).ToString();
                        }
                        else
                        {
                            pageLink.Text = firstPageInSegment + "-" + (VIP_MAILBOX_PAGE_SIZE + (VIP_MAILBOX_PAGE_SIZE * i)).ToString();
                        }
                    }
                    else
                    {
                        string lastPageInSegment = string.Empty;
                        // check to see if this is the final time through the paging logic, if so see if there are remainder pages
                        if ((i == (totalPages - 1)) && (totalMessages % VIP_MAILBOX_PAGE_SIZE != 0) && finalSegment)
                        {
                            lastPageInSegment = (totalMessages % VIP_MAILBOX_PAGE_SIZE + VIP_MAILBOX_PAGE_SIZE * i).ToString();
                        }
                        else
                        {
                            lastPageInSegment = (VIP_MAILBOX_PAGE_SIZE + (VIP_MAILBOX_PAGE_SIZE * i)).ToString();
                        }
                        pageLink.Text = "<strong>" + firstPageInSegment + "-" + lastPageInSegment + "</strong>";
                        _mailBox.VIPSelectedPageSegment = pageLink.Text;
                    }

                    pageLink.CssClass = cssClass;
                    pageLabel.Controls.Add(pageLink);

                    // append a footer '/' if the last page and segment has not been reached
                    if (i < (totalPages - 1) || !finalSegment)
                    {
                        seperator = new Label();
                        seperator.Text = " / ";
                        seperator.CssClass = cssClass;
                        pageLabel.Controls.Add(seperator);
                    }
                }

                // finish out the display with total messages in folder
                messageCountHyperLink = new Label();
                messageCountHyperLink.CssClass = cssClass;
                g.ExpansionTokens.Remove("MESSAGES");
                g.ExpansionTokens.Add("MESSAGES", totalMessages.ToString());
                messageCountHyperLink.Text = " " + g.GetResource("TXT_OF_MESSAGES", _mailBox.ResourceControl);
                pageLabel.Controls.Add(messageCountHyperLink);


                //// This info is now used as part of the page title.
                //if (_mailBox.MessageCountTitle == null)
                //{
                //    _mailBox.MessageCountTitle = messageCountHyperLink.Text;
                //}
            }
        }

        private EmailFolder GetEmailFolderByFolderID(int pFolderId)
        {
            IEnumerator folderEnumerator = null;
            EmailFolder folder = null;

            if (g.MemberEmailFolders != null && g.MemberEmailFolders.Count > 0)
            {
                folderEnumerator = g.MemberEmailFolders.GetEnumerator();

                while (folderEnumerator.MoveNext() && folder == null)
                {
                    folder = (EmailFolder)folderEnumerator.Current;
                    if (folder != null && folder.MemberFolderID != pFolderId)
                    {
                        folder = null;
                    }
                }
            }

            return folder;
        }


    }
}
