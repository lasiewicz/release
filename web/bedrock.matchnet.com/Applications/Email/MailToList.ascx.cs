namespace Matchnet.Web.Applications.Email
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using Matchnet.Lib;
	using Matchnet.Lib.Util;
	using Matchnet.Web.Framework;
	using Matchnet.Web.Framework.Ui;

	using Matchnet.List.ServiceAdapters;
	using Matchnet.List.ValueObjects;

	/// <summary>
	///		Summary description for MailToList.
	/// </summary>
	public class MailToList : FrameworkControl
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DropDownList ddCategoryID;
		protected System.Web.UI.WebControls.Literal litToTextFieldID;
		protected Matchnet.Web.Framework.Ui.BasicElements.ResultList ResultHotList;

		private void Page_Init(object sender, System.EventArgs e){
			try
			{
				this.LoadCategoryList();
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{	
				ResultHotList.GalleryView = "false";

				if( Request["ToTextFieldID"] != null )
					litToTextFieldID.Text = Request["ToTextFieldID"].ToString();

			}
			catch (Exception ex)
			{
				_g.ProcessException(ex);
			}
		}

		private void ddCategoryID_SelectedIndexChange(object sender, System.EventArgs e)
		{
			//ResultList looks for value in querystring.
			g.Transfer("/Applications/Email/MailToList.aspx?TotalRows=7&CategoryId=" + ddCategoryID.SelectedValue);
		}

		private void LoadCategoryList()
		{
//			if (Page.IsPostBack != true)
//			{
				ddCategoryID.Items.Add(
					new ListItem(
					g.GetTargetResource("YOUR_FAVORITES", this), 
					"0"));
				ddCategoryID.Items.Add(
					new ListItem(
					g.GetTargetResource("YOUR_MATCHES_-_SENT_VIA_EMAIL", this), 
					"-10"));
				ddCategoryID.Items.Add(
					new ListItem(
					g.GetTargetResource("WHOS_VIEWED_YOUR_PROFILE", this), 
					"-1"));
				ddCategoryID.Items.Add(
					new ListItem(
					g.GetTargetResource("WHOS_ADDED_YOU_TO_THEIR_HOT_L", this), 
					"-2"));
				ddCategoryID.Items.Add(
					new ListItem(
					g.GetTargetResource("WHOS_TEASED_YOU", this), 
					"-3"));
				ddCategoryID.Items.Add(
					new ListItem(
					g.GetTargetResource("WHOS_EMAILED_YOU", this), 
					"-4"));
				ddCategoryID.Items.Add(
					new ListItem(
					g.GetTargetResource("WHOS_IMD_YOU", this), 
					"-5"));
				ddCategoryID.Items.Add(
					new ListItem(
					g.GetTargetResource("MEMBERS_YOUVE_VIEWED", this), 
					"-9"));
				ddCategoryID.Items.Add(
					new ListItem(
					g.GetTargetResource("MEMBERS_YOUVE_TEASED", this), 
					"-6"));
				ddCategoryID.Items.Add(
					new ListItem(
					g.GetTargetResource("MEMBERS_YOUVE_SENT_EMAILS_TO", this), 
					"-7"));
				ddCategoryID.Items.Add(
					new ListItem(
					g.GetTargetResource("MEMBERS_YOUVE_IMD", this), 
					"-8"));
				ddCategoryID.Items.Add(
					new ListItem(
					g.GetTargetResource("MEMBERS_YOUVE_IGNORED", this), 
					"-13"));
			//}
			if (Request["CategoryID"] != null)
			{
				ddCategoryID.SelectedValue = Request["CategoryID"].ToString();
			}
	}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ddCategoryID.SelectedIndexChanged += new System.EventHandler(this.ddCategoryID_SelectedIndexChange);
			this.Load += new System.EventHandler(this.Page_Load);
			this.Init += new System.EventHandler(this.Page_Init);

		}
		#endregion
	}
}
