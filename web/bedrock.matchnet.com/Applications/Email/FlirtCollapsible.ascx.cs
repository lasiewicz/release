﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;

namespace Matchnet.Web.Applications.Email
{
    /// <summary>
    /// This page renders the flirt/smile in a collapsible view
    /// </summary>
    public partial class FlirtCollapsible : FrameworkControl, IFlirt
    {
        private Matchnet.Member.ServiceAdapters.Member _member;
        private FlirtHandler _handler = null;
        private int _selectedCategoryID = Constants.NULL_INT;
        private int _selectedTeaseID = Constants.NULL_INT;
        private int _currentBindingCategoryID = 0;
        private bool _alreadySelectedDefaultTease = false;
        private bool _alreadySelectedDefaultCategory = false;

        #region IFlirt implementation
        public Matchnet.Member.ServiceAdapters.Member Member { get { return _member; } }
        public System.Web.UI.WebControls.Repeater RepTeaseCategories { get { return null; } }
        public System.Web.UI.WebControls.RadioButtonList RblTeaseContent { get { return null; } }

        public FrameworkControl ResourceControl { get { return this; } }
        #endregion

        #region Event Handlers
        protected override void OnInit(EventArgs e)
        {
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);

            base.OnInit(e);
        }

        private void Page_Init(object sender, EventArgs e)
        {
            _handler = new FlirtHandler(this, g);
            if (!IsPostBack)
            {
                // Get referring url and store in session, but only do this upon loading tease.ascx the first time,
                // you don't want this to happen if/when someone tabs thru the flirt messages, which postbacks.
                _handler.GetRefURL();
            }

            try
            {
                bool isHotListed = false;
                int memberID = 0;
                string startRow = string.Empty;

                if (Request["MemberID"] != null
                    && Request["MemberID"] != "")
                {
                    try
                    {
                        memberID = Convert.ToInt32(Request.Params.Get("MemberID"));
                        startRow = Request.Params.Get("StartRow");

                        if (memberID != 0 && memberID != int.MinValue)
                        {
                            this._member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                            if (this._member == null)
                            {
                                g.Notification.AddErrorString("[Member not found.]");
                                return;
                            }

                            //TT# 16688 check if sender member is blocked
                            isHotListed = _handler.CheckIgnoreList(this._member.MemberID);
                            if (isHotListed)
                            {
                                g.Transfer("/Applications/Email/TeaseTooMany.aspx?erroroccurred=true&errortype=ignore&MemberID=" + this._member.MemberID);
                            }

                            //TT#16754 check if member already flirted with this member
                            isHotListed = _handler.CheckIfAlreadyTeased(g.Member.MemberID, this._member.MemberID);

                            if (isHotListed)
                            {
                                if (startRow != null && startRow.Trim().Length > 0)
                                {
                                    g.Transfer("/Applications/Email/TeaseSentAlready.aspx?MemberID=" + memberID + "&StartRow=" + startRow);
                                }
                                else
                                {
                                    g.Transfer("/Applications/Email/TeaseSentAlready.aspx?MemberID=" + memberID);
                                }
                            }
                        }
                        else
                        {
                            Exception ex = new Exception("MemberID was not found in the request.");
                            g.ProcessException(ex);
                            g.Transfer("/Default.aspx?rc=WE_WERE_UNABLE_TO_FIND_THE_PAGE_YOU_ARE_LOOKING_FOR_X");
                        }
                    }
                    catch (FormatException fe)
                    {
                        g.ProcessException(fe);
                        g.Transfer("/Default.aspx?rc=WE_WERE_UNABLE_TO_FIND_THE_PAGE_YOU_ARE_LOOKING_FOR_X");
                    }
                }
                else
                {
                    Exception ex = new Exception("MemberID was not found in the request.");
                    g.ProcessException(ex);
                    g.Transfer("/Default.aspx?rc=WE_WERE_UNABLE_TO_FIND_THE_PAGE_YOU_ARE_LOOKING_FOR_X");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string controlPath = "../../Framework/Ui/BasicElements/MiniProfile20.ascx";
                MiniProfile20 control = (MiniProfile20)LoadControl(controlPath);

                if (control != null)
                {
                    control.Member = this._member;
                    control.Transparency = true;

                    this.miniProfile.Controls.Add(control);
                }

                if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                {
                    txtCallToAction.Visible = false;
                }

                if (Page.IsPostBack)
                {
                    //get selected category and tease, if exists
                    if (!String.IsNullOrEmpty(Request.Form["Radio_TeaseItem"]))
                    {
                        string categoryID_TeaseID = Request.Form["Radio_TeaseItem"];
                        _selectedCategoryID = Convert.ToInt32(categoryID_TeaseID.Substring(0, categoryID_TeaseID.IndexOf("_")));
                        _selectedTeaseID = Convert.ToInt32(categoryID_TeaseID.Substring(categoryID_TeaseID.IndexOf("_") + 1));
                    }
                }

                BindCategoryAndContentItems();

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void repeaterCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                TeaseCategory tc = e.Item.DataItem as TeaseCategory;

                //set category name and link
                string ulItemGroupID = "categoryItems" + tc.TeaseCategoryID.ToString();

                System.Web.UI.HtmlControls.HtmlAnchor lnkCategoryName = e.Item.FindControl("lnkCategoryName") as System.Web.UI.HtmlControls.HtmlAnchor;
                //lnkCategoryName.Attributes.Add("onclick", "return ExpandCollapseCategoryItems('" + ulItemGroupID + "');");

                Literal literalCategoryName = e.Item.FindControl("literalCategoryName") as Literal;
                literalCategoryName.Text = g.GetResource(tc.ResourceKey, this);

                //set opening ul element containing tease items
                Literal literalULStart = e.Item.FindControl("literalULStart") as Literal;
                string ulStartStyle = "style=\"display:none;\"";
                if (_selectedCategoryID == tc.TeaseCategoryID || (!_alreadySelectedDefaultCategory && _selectedCategoryID == Constants.NULL_INT))
                {
                    _alreadySelectedDefaultCategory = true;
                    ulStartStyle = "style=\"display:block;\"";
                }
                literalULStart.Text = "<ul ID=\"" + ulItemGroupID + "\"" + ulStartStyle + ">";

                //bind category tease items
                _currentBindingCategoryID = tc.TeaseCategoryID;
                TeaseCollection teaseCollection = TeaseSA.Instance.GetTeaseCollection(tc.TeaseCategoryID, g.Brand.Site.SiteID);
                Repeater repeaterItems = e.Item.FindControl("repeaterItems") as Repeater;
                repeaterItems.DataSource = teaseCollection;
                repeaterItems.DataBind();

                //category promo
                if (!string.IsNullOrEmpty(tc.PromoResourceKey))
                {
                    PlaceHolder phCategoryPromo = e.Item.FindControl("phCategoryPromo") as PlaceHolder;
                    phCategoryPromo.Visible = true;
                    Txt txtPromo = e.Item.FindControl("txtPromo") as Txt;
                    txtPromo.ResourceConstant = tc.PromoResourceKey;
                }
                
            }
        }

        protected void repeaterItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Matchnet.Email.ValueObjects.Tease tease = e.Item.DataItem as Matchnet.Email.ValueObjects.Tease;

                //render radio button and label
                Literal literalItemRadioButton = e.Item.FindControl("literalItemRadioButton") as Literal;

                string radioID = "tease" + tease.TeaseID.ToString();
                if ((!_alreadySelectedDefaultTease && _selectedTeaseID == Constants.NULL_INT) || tease.TeaseID == _selectedTeaseID)
                {
                    _alreadySelectedDefaultTease = true;
                    literalItemRadioButton.Text = "<input id=\"" + radioID + "\" type=\"radio\" name=\"Radio_TeaseItem\" value=\"" + _currentBindingCategoryID.ToString() + "_" + tease.TeaseID.ToString() + "\" checked=\"checked\" />";
                }
                else
                {
                    literalItemRadioButton.Text = "<input id=\"" + radioID + "\" type=\"radio\" name=\"Radio_TeaseItem\" value=\"" + _currentBindingCategoryID.ToString() + "_" + tease.TeaseID.ToString() + "\" />";
                }
                literalItemRadioButton.Text += "<label for=\"" + radioID + "\">" + g.GetResource(tease.ResourceKey, this) + "</label>";
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //int cat = _selectedCategoryID;
                //int tease = _selectedTeaseID;
                //string combinedcattease = cat.ToString() + tease.ToString();
                //g.Notification.AddMessageString(combinedcattease);
                _handler.SendTease(_selectedCategoryID, _selectedTeaseID);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion

        #region Private Methods
        private void BindCategoryAndContentItems()
        {
            TeaseCategoryCollection teaseCategoryCollection = TeaseSA.Instance.GetTeaseCategoryCollection(g.Brand.Site.SiteID);

            repeaterCategories.DataSource = teaseCategoryCollection;
            repeaterCategories.DataBind();
        }

        #endregion
    }
}