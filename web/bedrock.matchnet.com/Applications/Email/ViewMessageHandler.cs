﻿using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements.Links;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.FormElements;

using System;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Applications.Email
{
    public class ViewMessageHandler
    {
        IViewMessage _control = null;
        ContextGlobal g;


        public ViewMessageHandler(IViewMessage control, ContextGlobal context)
        {
            g = context;
            _control = control;
        }


        public void PopulateMessage()
        {
            _control.JSMessage = "";
            _control.JSDoMessageConversion = false;

            _control.MessageDateLiteral.Text = FrameworkGlobals.GetOffsetAdjustedDateDisplay(_control.CurrentMessage.InsertDate, g.Brand);

            _control.MessageSubjectLiteral.Text = FrameworkGlobals.GetUnicodeText(_control.CurrentMessage.Subject);

            switch (_control.CurrentMessage.MailTypeID)
            {
                case MailType.QandAComment:
                    string messageBody = _control.CurrentMessage.Content;
                    MatchCollection matches = Regex.Matches(messageBody, "\\{.*\\}");
                    Match match = null;
                    if (null != matches && matches.Count > 0)
                    {
                        match = matches[matches.Count - 1];
                        messageBody = messageBody.Replace(match.Value, "");
                    }
                    if (null != _control.PhMessageAppend)
                    {
                        string jsonString = (null != match && string.IsNullOrEmpty(match.Value)) ? string.Empty : match.Value.Replace(" ", "");
                        Email.Controls.QandAEmailMessage qandaMessage = _control.Page.LoadControl("/Applications/Email/Controls/QandAEmailMessage.ascx") as Email.Controls.QandAEmailMessage;
                        qandaMessage.LoadQAndAMessage(jsonString);
                        _control.PhMessageAppend.Controls.Add(qandaMessage);
                        _control.PhMessageAppend.Visible = true;
                        //setup page title.
                        _control.LblViewMessage.Text = string.Format(g.GetResource("TXT_COMMENT_HEADER", _control.ResourceControl), qandaMessage.QAndA.FromMemberName);
                    }

                    _control.MessageContentLiteral.Text = messageBody;
                    break;
                case MailType.FreeTextComment:
                    string messageBody2 = _control.CurrentMessage.Content;
                    MatchCollection matches2 = Regex.Matches(messageBody2, "\\{.*\\}");
                    Match match2 = null;
                    if (null != matches2 && matches2.Count > 0)
                    {
                        match2 = matches2[matches2.Count - 1];
                        messageBody2 = messageBody2.Replace(match2.Value, "");
                    }
                    if (null != _control.PhMessageAppend)
                    {
                        string jsonString = (null != match2 && string.IsNullOrEmpty(match2.Value)) ? string.Empty : match2.Value.Replace(" ", "");
                        Email.Controls.FreeTextEmailMessage freeTextMessage = _control.Page.LoadControl("/Applications/Email/Controls/FreeTextEmailMessage.ascx") as Email.Controls.FreeTextEmailMessage;
                        freeTextMessage.LoadFreeTextMessage(jsonString, g);
                        _control.PhMessageAppend.Controls.Add(freeTextMessage);
                        _control.PhMessageAppend.Visible = true;
                    }

                    _control.MessageContentLiteral.Text = messageBody2;
                    break;
                case MailType.PhotoComment:
                    string messageBody3 = _control.CurrentMessage.Content;
                    MatchCollection matches3 = Regex.Matches(messageBody3, "\\{.*\\}");
                    Match match3 = null;
                    if (null != matches3 && matches3.Count > 0)
                    {
                        match3 = matches3[matches3.Count - 1];
                        messageBody3 = messageBody3.Replace(match3.Value, "");
                    }
                    if (null != _control.PhMessageAppend)
                    {
                        string jsonString = (null != match3 && string.IsNullOrEmpty(match3.Value)) ? string.Empty : match3.Value.Replace(" ", "");
                        Email.Controls.PhotoCommentEmailMessage photoCommentMessage = _control.Page.LoadControl("/Applications/Email/Controls/PhotoCommentEmailMessage.ascx") as Email.Controls.PhotoCommentEmailMessage;
                        photoCommentMessage.LoadPhotoCommentMessage(jsonString, g, _control.CurrentMessage.FromMemberID, _control.CurrentMessage.FromUserName);
                        _control.PhMessageAppend.Controls.Add(photoCommentMessage);
                        _control.PhMessageAppend.Visible = true;
                    }

                    _control.MessageContentLiteral.Text = messageBody3;
                    break;
                case MailType.MissedIM:
                    if (_control.CurrentMessage.Content.IndexOf("sparktag") > 0)
                    {
                        _control.JSMessage = _control.CurrentMessage.Content;
                        _control.JSDoMessageConversion = true;
                    }
                    else
                    {
                        _control.MessageContentLiteral.Text = FrameworkGlobals.GetUnicodeText(FormatMessage(_control.CurrentMessage.Content));
                    }
                    break;
                default:
                    _control.MessageContentLiteral.Text = FrameworkGlobals.GetUnicodeText(FormatMessage(_control.CurrentMessage.Content));
                    break;
            }

            if (g.Brand.GetStatusMaskValue(StatusType.PrivatePhotos))
            {
                if (_control.CurrentMessage.MailTypeID == MailType.PrivatePhotoEmail)
                {
                    _control.ProcessPrivatePhotos();
                }
            }
        }


        // binds the single member profile for mini profile control
        public void PopulateProfile()
        {
            Matchnet.Member.ServiceAdapters.Member aMember = null;
            ArrayList toMember = new ArrayList();

            if (_control.CurrentMessage.MemberFolderID == (int)SystemFolders.Sent)
            {
                //Get the possessive form of the username (e.g., user's)
                g.ExpansionTokens.Add("USER", FrameworkGlobals.GetUnicodeText(_control.CurrentMessage.ToUserName));
                _control.LblMemberProfileUserName.Text = g.GetResource("TXT_USER_S", _control.ResourceControl);

                aMember = MemberSA.Instance.GetMember(_control.CurrentMessage.ToMemberID, MemberLoadFlags.None);
            }
            else
            {
                //Get the possessive form of the username (e.g., user's)
                g.ExpansionTokens.Add("USER", FrameworkGlobals.GetUnicodeText(_control.CurrentMessage.FromUserName));
                _control.LblMemberProfileUserName.Text = g.GetResource("TXT_USER_S", _control.ResourceControl);

                aMember = MemberSA.Instance.GetMember(_control.CurrentMessage.FromMemberID
                    , MemberLoadFlags.None);
            }

            // Check to see if aMember (recipient) is valid
            if (FrameworkGlobals.IsValidMember(aMember, g.Brand))
            {
                toMember.Add(aMember);
                _control.RptMemberProfile.DataSource = toMember;
                _control.RptMemberProfile.DataBind();

                //_control.ViewProfileInfo.MemberProfile = aMember;
            }
            else
            {
                Exception ex = new Exception("Invalid MemberID in view message.");
                g.ProcessException(ex);
            }

        }





        public void ExaminePagingLinks()
        {
            _control.LnkPreviousMessageLink.NavigateUrl = GetPreviousMessageLink();

            _control.LnkNextMessageLink.NavigateUrl = GetNextMessageLink();

            _control.LnkViewMessageLink.NavigateUrl = GetMailBoxLink();

        }

        public string GetPreviousMessageLink()
        {
            return CheckIfNextMessageIsBlockedByInboxShutdown(Direction.Previous) ? "javascript:ShowInboxshutdownOverlay();" :
            "/Applications/Email/ViewMessage.aspx?MemberMailID=" + _control.CurrentMessage.MemberMailID +
                "&MemberFolderID=" + _control.CurrentMessage.MemberFolderID + "&OrderBy=" + _control.OrderBy + "&pageDirection=" + (int)Direction.Previous
                + "&CurrentMessageNumber=" + (_control.CurrentMessageNumber - 1) + "&MessageCount=" + _control.MessageCount;

        }


        public string GetNextMessageLink()
        {
            return CheckIfNextMessageIsBlockedByInboxShutdown(Direction.Next) ? "javascript:ShowInboxshutdownOverlay();" :
             "/Applications/Email/ViewMessage.aspx?MemberMailID=" + _control.CurrentMessage.MemberMailID +
                "&MemberFolderID=" + _control.CurrentMessage.MemberFolderID + "&OrderBy=" + _control.OrderBy + "&pageDirection=" + (int)Direction.Next
                + "&CurrentMessageNumber=" + (_control.CurrentMessageNumber + 1) + "&MessageCount=" + _control.MessageCount;

        }

        private bool CheckIfNextMessageIsBlockedByInboxShutdown(Direction direction)
        {
            bool blocked = false;
            try
            {
                InboxShutdownHelper inboxShutdownHelper = new InboxShutdownHelper(g);
                if (inboxShutdownHelper.IsInboxShutdownEnabled())
                {
                    EmailMessage nextMessage = EmailMessageSA.Instance.RetrieveMessage(g.Brand.Site.Community.CommunityID,
                       g.TargetMemberID,
                       _control.CurrentMessage.MemberMailID,
                       _control.OrderBy,
                       direction,
                       false,
                       g.Brand);

                    if (nextMessage != null)
                    {
                        blocked = inboxShutdownHelper.IsMessageBlocked(nextMessage, g.Member, nextMessage.FromMemberID,
                                                                    _control.MemberFolderID);

                        if (blocked)
                        {
                            _control.PLCInboxShutdownOverlay.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return blocked;
        }


        public string GetMailBoxLink()
        {
            string link = "";
            if (HttpContext.Current.Request.QueryString["MemberFolderId"] != null && HttpContext.Current.Request.QueryString["OrderBy"] != null)
            {
                int folderid = Conversion.CInt(HttpContext.Current.Request.QueryString["MemberFolderId"]);
                if (VIPMailUtils.IsVIPFolder(folderid))
                    folderid = VIPMailUtils.GetSystemFolderIDForVIPFolder(folderid);
                link = "/Applications/Email/MailBox.aspx?MemberFolderId=" + folderid.ToString() + "&OrderBy=" + HttpContext.Current.Request.QueryString["OrderBy"];
            }
            else
            {
                link = "/Applications/Email/MailBox.aspx";
            }
            return link;
        }
        /// Refer to IMail Permissions project specs. Also check to see if member is a non subscriber.
        /// </summary>
        /// <returns>True if this feature is activated and member is not a subscriber.</returns>
        public bool ApplyImailPermissions()
        {
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMailPermissions", g.Brand.Site.Community.CommunityID,
                g.Brand.Site.SiteID, g.Brand.BrandID)))
            {
                if (g.TargetBrand.IsPaySite)
                {
                    if (MemberPrivilegeAttr.IsCureentSubscribedMember(g) || g.IsAdmin)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    // free site.
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        // filter out to see if the current member has access
        public void ValidMemberAccess(int fromMemberID, EmailMessage message, bool markAsUnread)
        {
            SettingsManager settingsManager = new SettingsManager();

            bool validate = false;

            if (VIPMailUtils.IsVIPEnabledSite(g.Brand))
            {
                if (VIPMailUtils.IsVIPFolder(message.MemberFolderID))
                    return;

            }
            // Flirt is free now with IMailPermissons
            if (ApplyImailPermissions())
            {
                if ((_control.CurrentMessage != null) && (_control.CurrentMessage.MailTypeID == MailType.Tease))
                {
                    validate = true;
                }
            }

            // If it is free to reply then validate.
            if (!g.TargetBrand.IsPayToReplySite)
                validate = true;

            if (g.TargetBrand.IsPaySite)
            {
                if (MemberPrivilegeAttr.IsPermitMember(g) || g.IsAdmin)
                {
                    validate = true;
                }

                // check for FREE_TO_VIEW_IMAIL, sites with this setting should be able to view IMail with no restriction
                if (!validate)
                {
                    bool freeToViewIMail = Convert.ToBoolean(RuntimeSettings.GetSetting("FREE_TO_VIEW_IMAIL", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));
                    validate = freeToViewIMail;
                }
                
                #region sitespecific rules old
                //if (g.Brand.BrandID == (int)WebConstants.BRAND_ID.JDateCoIL ||
                //    g.Brand.BrandID == (int)WebConstants.BRAND_ID.Cupid)
                //{
                //    Matchnet.Member.ServiceAdapters.Member member =
                //        Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                //        MemberLoadFlags.None);

                //    RegionLanguage region = RegionSA.Instance.RetrievePopulatedHierarchy(member.GetAttributeInt(g.Brand, "RegionID"), g.Brand.Site.LanguageID);

                //    if (region.CountryRegionID == g.Brand.Site.DefaultRegionID)
                //    {
                //        Matchnet.Member.ServiceAdapters.Member fromMember =
                //            Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(fromMemberID, MemberLoadFlags.None);

                //        RegionLanguage fromRegion = RegionSA.Instance.RetrievePopulatedHierarchy(fromMember.GetAttributeInt(g.Brand, "RegionID"), g.Brand.Site.LanguageID);

                //        // if not a paying member and fromMember is Iraeli then its ok to see message
                //        if (!MemberPrivilegeAttr.IsPermitMember(g) && (fromRegion.CountryRegionID == g.Brand.Site.DefaultRegionID))
                //        {
                //            validate = true;
                //        }
                //    }
                //}
                //else if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                //{
                //    if (FrameworkGlobals.senderSpeaksSiteDefaultLanguage(fromMemberID, g.Brand))
                //    {
                //        // SENDER SPEAKS FRENCH SO ALLOW THE RECIPIENT TO VIEW THIS MESSAGE
                //        validate = true;
                //    }
                //    else if (!MemberPrivilegeAttr.IsPermitMember(g))
                //    {
                //        if (_control.CurrentMessage.MailTypeID == MailType.Email)
                //        {
                //            // SENDER DOES NOT SPEAK FRENCH SO THE RECIPIENT CANNOT OPEN THE EMAIL AND WILL BE
                //            // REDIRECTED TO THE SUBSCRIPTION PAGE WITH A PURCHASE REASON TYPE OF 
                //            // ATTEMPT TO READ MESSAGE FROM A NON FRENCH SPEAKER  
                //            Redirect.Subscription(g.Brand, (Int32)Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.AttemptToReadMessageFromNonFrenchSpeaker, 0);
                //        }
                //        else if (_control.CurrentMessage.MailTypeID == MailType.Tease)
                //        {
                //            // SENDER DOES NOT SPEAK FRENCH SO THE RECIPIENT CANNOT OPEN THE FLIRT AND WILL BE
                //            // REDIRECTED TO THE SUBSCRIPTION PAGE WITH A PURCHASE REASON TYPE OF 
                //            // ATTEMPT TO READ FLIRT FROM A NON FRENCH SPEAKER  
                //            Redirect.Subscription(g.Brand, (Int32)Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.AttemptToReadFlirtFromNonFrenchSpeaker, 0);
                //        }
                //    }
                //}

                #endregion
                #region site specific rules new
                // if validation fails and the calling method wants the message to be marked as Unread, then perform marking as unread action
                if (markAsUnread && !validate)
                {
                    Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.MarkRead(g.Member.MemberID, g.Brand.Site.Community.CommunityID, false, new ArrayList() { message.MessageList.MessageListID });
                }

                if (!validate)
                {
                    Matchnet.Member.ServiceAdapters.Member fromMember =
                                  Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(fromMemberID, MemberLoadFlags.None);


                    validate = FrameworkGlobals.MailAccessException(WebConstants.MailAccessArea.viewMessage, g, fromMember);
                }

                //Check if sender is grandfathered (jdil inboxshutdown)
                InboxShutdownHelper inboxShutdownHelper = new InboxShutdownHelper(g);
                if (inboxShutdownHelper.IsInboxShutdownEnabled()) //updated to make sure we check is the user has access to inbox. 
                {
                    if (!inboxShutdownHelper.IsMessageBlocked(message, g.Member, fromMemberID, message.MemberFolderID))
                    {
                        validate = true;
                    }
                }

                if (!validate)
                    if (!MemberPrivilegeAttr.IsPermitMember(g))
                    {
                        int exceptionMask = settingsManager.GetSettingInt(SettingConstants.VIEWMSG_ACCESS_MASK, g.Brand);
                        if ((exceptionMask & (int)WebConstants.MailAccessExceptionRule.defaultLanguage) == (int)WebConstants.MailAccessExceptionRule.defaultLanguage)
                        {
                            if (_control.CurrentMessage.MailTypeID == MailType.Email)
                            {
                                // SENDER DOES NOT SPEAK FRENCH SO THE RECIPIENT CANNOT OPEN THE EMAIL AND WILL BE
                                // REDIRECTED TO THE SUBSCRIPTION PAGE WITH A PURCHASE REASON TYPE OF 
                                // ATTEMPT TO READ MESSAGE FROM A NON FRENCH SPEAKER  
                                
                                Redirect.Subscription(g.Brand, (Int32)Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.AttemptToReadMessageFromNonFrenchSpeaker, 0);
                            }
                            else if (_control.CurrentMessage.MailTypeID == MailType.Tease)
                            {
                                // SENDER DOES NOT SPEAK FRENCH SO THE RECIPIENT CANNOT OPEN THE FLIRT AND WILL BE
                                // REDIRECTED TO THE SUBSCRIPTION PAGE WITH A PURCHASE REASON TYPE OF 
                                // ATTEMPT TO READ FLIRT FROM A NON FRENCH SPEAKER  
                                
                                Redirect.Subscription(g.Brand, (Int32)Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.AttemptToReadFlirtFromNonFrenchSpeaker, 0);
                            }
                        }
                    }

                #endregion
                if (!validate)
                {
                    // need to send specific PRTIDs on different message types
                    if (ApplyImailPermissions())
                    {
                        // default value
                        PurchaseReasonType purchaseReasonType = PurchaseReasonType.AttemptToAccessInbox;

                        if (_control.CurrentMessage != null)
                        {
                            switch (_control.CurrentMessage.MailTypeID)
                            {
                                case MailType.Email:
                                    purchaseReasonType = PurchaseReasonType.IMailPermissionsEmailSubjectLine;
                                    // if this ViewMessage link came from a masked email
                                    if (HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_NAME_VIP_STANDARD_MAIL_MASKED] != null)
                                        purchaseReasonType = PurchaseReasonType.AttemptToReadMaskedEmail;
                                    break;

                                case MailType.MissedIM:
                                    purchaseReasonType = PurchaseReasonType.IMailPermissionsMissedIMSubjectLine;
                                    break;

                                default:
                                    purchaseReasonType = PurchaseReasonType.AttemptToAccessInbox;
                                    break;
                            }
                        }
                        
                        Redirect.Subscription(g.Brand, (Int32)purchaseReasonType, 0);

                    }
                    else
                    {
                        PurchaseReasonType purchaseReason = PurchaseReasonType.AttemptToAccessInbox;
                        // if this ViewMessage link came from a masked email
                        if (HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_NAME_VIP_STANDARD_MAIL_MASKED] != null)
                            purchaseReason = PurchaseReasonType.AttemptToReadMaskedEmail;
                        
                        Redirect.Subscription(g.Brand, (Int32)purchaseReason, 0);
                    }
                }
            }
            
        }
        #region utilities

        public string BuildNextMessageLink()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("&nbsp;<a href='/Applications/Email/ViewMessage.aspx?MemberMailID=" + _control.CurrentMessage.MemberMailID +
                    "&MemberFolderID=" + _control.CurrentMessage.MemberFolderID + "&OrderBy=" + _control.OrderBy + "&pageDirection=" + (int)Direction.Next
                    + "&CurrentMessageNumber=" + (_control.CurrentMessageNumber + 1) + "&MessageCount=" + _control.MessageCount + "'");
            builder.Append(" class=\"breadCrumbPagesLink\">>></a>");
            return builder.ToString();
        }



        public string GetFolderName(EmailMessage pEmailMessage)
        {
            string folderName = string.Empty;
            if (pEmailMessage != null)
            {
                switch (pEmailMessage.MemberFolderID)
                {
                    case (int)SystemFolders.Inbox:
                        folderName = g.GetResource("TXT_INBOX", _control.ResourceControl);
                        break;

                    case (int)SystemFolders.Sent:
                        folderName = g.GetResource("TXT_SENT", _control.ResourceControl);
                        break;

                    case (int)SystemFolders.Draft:
                        folderName = g.GetResource("TXT_DRAFT", _control.ResourceControl);
                        break;
                    case (int)SystemFolders.Trash:
                        folderName = g.GetResource("TXT_TRASH", _control.ResourceControl);
                        break;
                    case (int)SystemFolders.VIPInbox:
                        folderName = g.GetResource("TXT_VIP_INBOX", _control.ResourceControl);
                        break;

                    case (int)SystemFolders.VIPSent:
                        folderName = g.GetResource("TXT_VIP_SENT", _control.ResourceControl);
                        break;

                    case (int)SystemFolders.VIPDraft:
                        folderName = g.GetResource("TXT_VIP_DRAFT", _control.ResourceControl);
                        break;

                    case (int)SystemFolders.VIPTrash:
                        folderName = g.GetResource("TXT_VIP_TRASH", _control.ResourceControl);
                        break;
                    default:
                        // Not a system folder, we need to look it up to get the description.
                        EmailFolder folder = EmailFolderSA.Instance.RetrieveFolder(g.Member.MemberID, g.Brand.Site.Community.CommunityID, pEmailMessage.MemberFolderID, false);
                        folderName = FrameworkGlobals.GetUnicodeText(folder.Description);
                        break;
                }
            }
            return folderName;
        }

        // removes all carriage returns and inserts a <br /> for proper display
        public string FormatMessage(string messageContent)
        {
            StringBuilder messageBuilder = new StringBuilder(HttpUtility.HtmlDecode(messageContent));

            messageBuilder.Replace("\n", "<br />", 0, messageBuilder.Length);
            return messageBuilder.ToString();
        }

        // pulls the expected parameters from the request query string
        public void GetRequestParameters()
        {
            if (HttpContext.Current.Request.QueryString["MemberMailID"] != null && HttpContext.Current.Request.QueryString["MemberMailID"].Trim().Length > 0)
            {
                try
                {
                    _control.MemberMailID = int.Parse(HttpContext.Current.Request.QueryString["MemberMailID"].Trim());
                }
                catch (FormatException)
                {
                    _control.MemberMailID = 0;
                }
            }

            if (HttpContext.Current.Request.QueryString["MemberFolderID"] != null && HttpContext.Current.Request.QueryString["MemberFolderID"].Trim().Length > 0)
            {
                try
                {
                    _control.MemberFolderID = int.Parse(HttpContext.Current.Request.QueryString["MemberFolderID"].Trim());
                }
                catch (FormatException)
                {
                    _control.MemberFolderID = 0;
                }
            }

            if (HttpContext.Current.Request.QueryString["OrderBy"] != null && HttpContext.Current.Request.QueryString["OrderBy"].Trim().Length > 0)
            {
                _control.OrderBy = HttpContext.Current.Request.QueryString["OrderBy"].Trim();
            }

            if (HttpContext.Current.Request.QueryString["PageDirection"] != null && HttpContext.Current.Request.QueryString["PageDirection"].Trim().Length > 0)
            {
                try
                {
                    _control.CurrentDirection = (Direction)int.Parse(HttpContext.Current.Request.QueryString["PageDirection"]);
                }
                catch
                {
                    _control.CurrentDirection = Direction.None;
                }
            }

            if (HttpContext.Current.Request.QueryString["CurrentMessageNumber"] != null && HttpContext.Current.Request.QueryString["CurrentMessageNumber"].Trim().Length > 0)
            {
                try
                {
                    _control.CurrentMessageNumber = int.Parse(HttpContext.Current.Request.QueryString["CurrentMessageNumber"]);
                }
                catch
                {
                    _control.CurrentMessageNumber = 1;	// ????
                }
            }

            if (HttpContext.Current.Request.QueryString["MessageCount"] != null && HttpContext.Current.Request.QueryString["MessageCount"].Trim().Length > 0)
            {
                try
                {
                    _control.MessageCount = int.Parse(HttpContext.Current.Request.QueryString["MessageCount"]);
                }
                catch
                {
                    _control.MessageCount = 1;	// ????
                }
            }

        }
        #endregion
    }
}
