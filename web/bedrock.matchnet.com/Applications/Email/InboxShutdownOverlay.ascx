﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InboxShutdownOverlay.ascx.cs"
    Inherits="Matchnet.Web.Applications.Email.InboxShutdownOverlay" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<!-- ***************************** Forced overlay start ***************************** -->
<div id="inbox-shutdown-inbox-overlay" class="clearfix">
    <div class="cta" style="position: absolute; right: 15px; top: 35px; width: 22px;
        height: 20px; text-align: right">
        <a href="javascript:document.getElementById('inbox-shutdown-inbox-overlay').style.display='none'; void 0">
            <mn:Image ID="imgClose" runat="server" FileName="close.jpg" ResourceConstant="CLOSE_TITLE_TEXT"
                TitleResourceConstant="CLOSE_TITLE_TEXT" />
        </a>
    </div>
    <div style="padding: 40px 0 0 26px;">
        <asp:HyperLink ID="lnkSub" runat="server" Style="display: block; position: absolute; top: 350px;
            left: 176px; text-decoration: none; width: 144px; height: 40px;">
            <mn:Image ID="imgLink" runat="server" ResourceConstant="LINK_TITLE_TEXT" FileName="link.jpg"
                TitleResourceConstant="LINK_TITLE_TEXT" />
        </asp:HyperLink>
    </div>
</div>
<!-- ***************************** Forced overlady end ***************************** -->
<script type="text/javascript">
    function ShowInboxshutdownOverlay() {
        var $vipinboxoverlay = $j('#inbox-shutdown-inbox-overlay');
        $j.blockUI({
            message: $vipinboxoverlay,
            css: {
                border: '',
                width: 'auto',
                cursor: '',
                color: '',
                backgroundColor: 'transparent'
            },
            overlayCSS: {
                cursor: null
            }
        });
        $vipinboxoverlay.find('.cta').find('a').click(function () {
            $j.unblockUI();
        });
        var overlaywidth = $vipinboxoverlay.width();
        var overlayoffset = overlaywidth / 2;
        $vipinboxoverlay.parent('.blockMsg').css({ 'left': '50%', 'top': '30%', 'margin-left': '-' + overlayoffset + 'px' });
    }
</script>
