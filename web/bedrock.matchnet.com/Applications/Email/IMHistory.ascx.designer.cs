﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Matchnet.Web.Applications.Email {
    
    
    public partial class IMHistory {
        
        /// <summary>
        /// txtIMHistoryTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Txt txtIMHistoryTitle;
        
        /// <summary>
        /// mailBoxTabBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Ui.BasicElements.TabBar mailBoxTabBar;
        
        /// <summary>
        /// txtIMHistoryListTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Txt txtIMHistoryListTitle;
    }
}
