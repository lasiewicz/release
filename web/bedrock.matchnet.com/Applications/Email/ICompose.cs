﻿#region System References

using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.JavaScript;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Util;

#endregion

namespace Matchnet.Web.Applications.Email
{
    public interface ICompose
    {
        Title TtlCompose { get; }

         Repeater RptMemberProfile { get; }
         Panel PnlToMemberTextField { get; }
         TextBox TxtToTextField { get; }
         TextBox TxtSubjectTextField { get; }
         TextBox TxtMessageBodyTextField { get; }
         Button BtnSaveDraftButton { get; }
         Button BtnSendMessageButton { get; }
         Table TblViewProfile { get; }
         Label LblMemberProfileUserName { get; }
         Panel LblMemberProfileUserNamePanel { get; }
         Table TblToHeader { get; }
         Table ViewProfileTable { get; }
         Table TblComposeMessage { get; }
         HtmlAnchor HrefMailToList { get; }
         PlaceHolder PhViewProfileInfo { get; }
         TableRow TrViewProfile { get; }
         FirstElementFocus FefControl{ get; }
         PlaceHolder PhClassicProfileDetail{ get; }
        
        //VIP All Access controls
         bool SendAsVIP { get; set; }
         PlaceHolder PHSendAsFREEReplyVIP { get; }
        PlaceHolder PHSendAsVIP { get; }
        PlaceHolder PHSendVIPOverlay { get; }
        Button BtnSendVIPMessage { get; }
        Txt VIPOverlayText { get; }
        Button BtnVIPMessageOverlayYes { get; }
        Button BtnVIPMessageOverlayNo { get; }
        Label LBLVIPCheckbox { get; }
        CheckBox VIPCheckbox { get; }
        bool IsPagePostBack { get; }
        #region for compose project
         CheckBox CbSaveCopyCheckBox{ get; }
         CheckBox CbSendPrivatePhotos { get; }
         bool MemberValid { get; set; }
         TableRow RowSaveCopyCheckBox { get; }
         PlaceHolder PhRegistrantMessage { get; }
         int MemberMailID { get; set; }
         TableCell TcComposeMessage { get; }

         Label GetToLabel();
         bool SaveSendMessage();
         bool SendPrivatePhotos();
        #endregion

         bool IsReply { get; set; }

         FrameworkControl ResourceControl { get; }
    }
}
