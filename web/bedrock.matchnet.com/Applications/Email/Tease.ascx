<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Tease.ascx.cs" Inherits="Matchnet.Web.Applications.Email.Tease" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div id="rightNew">
	<mn:Title runat="server" id="ttlMatches" ResourceConstant="TXT_SEND_A_TEASE" ImageName="hdr_message_sent.gif" CommunitiesForImage="CI;" />
	<asp:PlaceHolder id="miniProfile" Runat="server" />
	<P></P>
	<div id="prefTab">
		<asp:Repeater ID="repTeaseCategories" Runat="server">
			<ItemTemplate>
				<asp:HyperLink ID="lnkLink" Runat="server">
					<asp:Label ID="lblText" Runat="server" CssClass="prefTabItem" />
				</asp:HyperLink>
			</ItemTemplate>
		</asp:Repeater>
	</div>
	<div id="prefBorder">
		<asp:RadioButtonList ID="rblTeaseContent" Runat="server" /><br />
		<%--<asp:Button ID="btnSubmit" Runat="server" Text="[Tease Away!]" CssClass="activityButton" />--%>
		&nbsp;&nbsp;<mn2:FrameworkButton runat="server" id="btnSubmit" CssClass="activityButton" style="margin-bottom:16px;" ResourceConstant="BTN_TEASE_AWAY" />
	</div>
	
 	<mn:txt id="txtCallToAction" runat="server" ExpandImageTokens="true" ResourceConstant="FLIRT_CALL_TO_ACTION" Visible=false />
 	
</div>