﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Email.ValueObjects;
namespace Matchnet.Web.Applications.Email
{
    public interface IMailBox
    {
        Matchnet.Web.Framework.Txt TxtReviseMessageAlertsSettings{get;}
        System.Web.UI.WebControls.DropDownList DropDownListMove{get;}
        System.Web.UI.WebControls.Label TxtMailBox{get;}
        Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnEmptyFolder{get;}
        Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnDeleteMessage{get;}
        Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnDeleteAllMessages { get; }
        Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnIgnoreMessage{get;}
        Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnMarkUnreadMessage{get;}
        System.Web.UI.WebControls.Table TableMailList{get;}
        System.Web.UI.WebControls.DataGrid MailDataGrid{get;}
        System.Web.UI.WebControls.DataGrid VIPMailDataGrid { get; }
        System.Web.UI.WebControls.DataGrid VIPMailHeaderDataGrid { get; }
        System.Web.UI.WebControls.DataGrid VIPMailFooterDataGrid { get; }
        System.Web.UI.HtmlControls.HtmlImage ImgDownArrow{get;}

        Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnMoveMessages{get;}

        Matchnet.Web.Framework.Txt TxtEditfolders{get;}
        Matchnet.Web.Framework.Image ImgNewMessages{get;}
        System.Web.UI.WebControls.Panel MailBarPanel{get;}
        System.Web.UI.WebControls.Panel EmptyFolderPanel{get;}
        Matchnet.Web.Framework.Txt TxtEmptyFolder{get;}
        Matchnet.Web.Framework.Image ImgTip{get;}
        Matchnet.Web.Framework.Txt TxtTip{get;}

        bool IgnoreSelf { get; set; }
        string MessageCountTitle{ get; set; }
        EmailFolder CurrentEmailFolder { get; set; }
        string CurrentFolderName { get; set; }
        string SelectedPageSegment { get; set; }
        int CurrentMessageNumber { get; set; }
        int CurrentVIPMessageNumber { get; set; }
        int MessageCount { get; set; }
        int VIPMessageCount { get; set; }
        string PreviousPageLink { get; set; }
        string NextPageLink { get; set; }
        string VIPPreviousPageLink { get; set; }
        string VIPNextPageLink { get; set; }
        string VIPSelectedPageSegment { get; set; }
        //photos controls
        System.Web.UI.WebControls.LinkButton LnkTogglePhotos{get;}
        Matchnet.Web.Framework.Txt TxtTogglePhotos{get;}

        bool PhotosFlag { get;set; }
        bool PhotosSettingFlag { get; set; }
        FrameworkControl ResourceControl{get;}
        bool UseBreadCrumb { get; set; }
        Label LnkTopPaging { get; }
        Label LnkBottomPaging { get; }
        Label LnkVIPPaging { get; }

        Matchnet.Web.Framework.Txt TxtVIPEmpty { get; }
    }
}
