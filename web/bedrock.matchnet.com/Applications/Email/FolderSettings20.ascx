﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="FolderSettings20.ascx.cs" Inherits="Matchnet.Web.Applications.Email.FolderSettings20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<mn:Title runat="server" id="ttlFolderSettings" ResourceConstant="TXT_YOUR_MESSAGE_FOLDERS"/>
<div id="folder-settings">
    <div class="folder-settings-form border-gen">
		    <mn:txt runat="server" id="txtFolderName" ResourceConstant="TXT_FOLDER_NAME" />
		    <asp:TextBox ID="newFolderName" Runat="server" MaxLength="20" />&nbsp; 
		    <cc1:FrameworkButton class="btn primary" id="newFolderButton" runat="server" ResourceConstant="BTN_ADD_TO_THE_LIST" />
	        <mn:MultiValidator id="newFolderNameValidator" tabIndex="-1" runat="server" MinimumLength="1" MaximumLength="20"
		    RequiredType="StringType" ControlToValidate="newFolderName" FieldNameResourceConstant="VLD_FOLDER_NAME" IsRequired="false" Display="Dynamic" />
    </div>

    <table id="folder-table" border="0">
        <tbody>
	    <tr class="header">
		    <td><strong><mn:txt runat="server" id="txtName" ResourceConstant="TXT_NAME" /></strong></td>
		    <td><strong><mn:txt runat="server" ID="txtMessage" ResourceConstant="TXT_MESSAGE" /></strong></td>
		    <td><strong><mn:txt runat="server" id="txtUnread" ResourceConstant="TXT_UNREAD" /></strong></td>
	    </tr>
	    
	<asp:Repeater ID="systemFolderListRepeater" Runat="server" Visible="True" OnItemDataBound="systemFolderListDataBound">
		<ItemTemplate>
			<tr class="repeater" runat="server" id="trRepeaterRow">
					<td class="first" ID="tdCustomFolderNormal" Runat="server">
						<a href="/Applications/Email/MailBox.aspx?MemberFolderID=<%# DataBinder.Eval(Container.DataItem, "MemberFolderID") %>">
							<asp:Literal ID="litCustomFolderName" Runat="server" /></a>
						<a href="/Applications/Email/FolderSettings.aspx?rid=<%# DataBinder.Eval(Container.DataItem, "MemberFolderID") %>">
							<mn:txt runat="server" id="txtRename" ResourceConstant="TXT_RENAME" /></a>
						<a href="/Applications/Email/FolderSettings.aspx?did=<%# DataBinder.Eval(Container.DataItem, "MemberFolderID") %>">
							<mn:txt runat="server" id="txtDelete" ResourceConstant="TXT_DELETE" /></a>
					</td>
					<td ID="tdCustomFolderRename" Runat="server">
						<input id="customFolderName" runat="server" maxlength="20" />
						<asp:Button CssClass="btn secondary" Runat="server" ID="customFolderRenameSave" OnClick="renameFolder" />
						<mn:MultiValidator id="customFolderNameValidator" tabIndex="-1" runat="server" MinimumLength="1" MaximumLength="20"
							RequiredType="StringType" ControlToValidate="customFolderName" FieldNameResourceConstant="VLD_FOLDER_NAME"
							IsRequired="false" Display="Dynamic" />
					</td>
					<td id="tdCustomFolderDelete" Runat="server">
						<mn:txt runat="server" id="txtDeleteFolder" ResourceConstant="DELETE" /> <b><asp:Literal ID="litDeleteFolderName" Runat="server" /></b>?
						<cc1:FrameworkButton class="btn secondary" id="customFolderDeleteConfirm" runat="server" OnClick="confirmDeleteFolder" ResourceConstant="TXT_CONFIRM_BRACKETS"></cc1:FrameworkButton>
						<cc1:FrameworkButton class="btn secondary" id="customFolderDeleteCancel" runat="server" OnClick="cancelDeleteFolder" ResourceConstant="TXT_CANCEL_BRACKETS"></cc1:FrameworkButton>
					</td>
					<td>
					<asp:Literal ID="litTotalMessages" Runat="server" />
				</td>
				<td>
					<asp:Literal ID="litUnreadMessages" Runat="server" />
				</td>
				
			</tr>
			
		</ItemTemplate>
	</asp:Repeater>
	</tbody>
</table>



</div>