using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Email
{
    /// <summary>
    /// This page renders the flirt/smile in a collapsible view
    /// </summary>
    public partial class FlirtCollapsiblePopup : FrameworkControl, IFlirt
    {
        private Matchnet.Member.ServiceAdapters.Member _member;
        private FlirtHandler _handler = null;
        private int _selectedCategoryID = Constants.NULL_INT;
        private int _selectedTeaseID = Constants.NULL_INT;
        private int _currentBindingCategoryID = 0;
        private bool _alreadySelectedDefaultTease = false;
        private bool _alreadySelectedDefaultCategory = false;
        private bool _isResultPanelVisible = false;

        #region IFlirt implementation
        public Matchnet.Member.ServiceAdapters.Member Member { get { return _member; } }
        public System.Web.UI.WebControls.Repeater RepTeaseCategories { get { return null; } }
        public System.Web.UI.WebControls.RadioButtonList RblTeaseContent { get { return null; } }

        public FrameworkControl ResourceControl { get { return this; } }
        #endregion

        #region Event Handlers
        protected override void OnInit(EventArgs e)
        {
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);

            base.OnInit(e);
        }

        private void Page_Init(object sender, EventArgs e)
        {
            _handler = new FlirtHandler(this, g);
            _handler.Page_Initialize(IsPostBack, true);
            this._member = _handler.Member;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                {
                    txtCallToAction.Visible = false;
                }

                if (Page.IsPostBack)
                {
                    //get selected category and tease, if exists
                    if (!String.IsNullOrEmpty(Request.Form["Radio_TeaseItem"]))
                    {
                        string categoryID_TeaseID = Request.Form["Radio_TeaseItem"];
                        _selectedCategoryID = Convert.ToInt32(categoryID_TeaseID.Substring(0, categoryID_TeaseID.IndexOf("_")));
                        _selectedTeaseID = Convert.ToInt32(categoryID_TeaseID.Substring(categoryID_TeaseID.IndexOf("_") + 1));
                    }
                }

                BindCategoryAndContentItems();

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void repeaterCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                TeaseCategory tc = e.Item.DataItem as TeaseCategory;

                //set category name and link
                string ulItemGroupID = "categoryItems" + tc.TeaseCategoryID.ToString();

                System.Web.UI.HtmlControls.HtmlAnchor lnkCategoryName = e.Item.FindControl("lnkCategoryName") as System.Web.UI.HtmlControls.HtmlAnchor;
                //lnkCategoryName.Attributes.Add("onclick", "return ExpandCollapseCategoryItems('" + ulItemGroupID + "');");

                Literal literalCategoryName = e.Item.FindControl("literalCategoryName") as Literal;
                literalCategoryName.Text = g.GetResource(tc.ResourceKey, this);

                //set opening ul element containing tease items
                Literal literalULStart = e.Item.FindControl("literalULStart") as Literal;
                string ulStartStyle = "style=\"display:none;\"";
                if (_selectedCategoryID == tc.TeaseCategoryID || (!_alreadySelectedDefaultCategory && _selectedCategoryID == Constants.NULL_INT))
                {
                    _alreadySelectedDefaultCategory = true;
                    ulStartStyle = "style=\"display:block;\"";
                }
                literalULStart.Text = "<ul ID=\"" + ulItemGroupID + "\"" + ulStartStyle + " class=\"checkbox-list\">";

                //bind category tease items
                _currentBindingCategoryID = tc.TeaseCategoryID;
                TeaseCollection teaseCollection = TeaseSA.Instance.GetTeaseCollection(tc.TeaseCategoryID, g.Brand.Site.SiteID);
                Repeater repeaterItems = e.Item.FindControl("repeaterItems") as Repeater;
                repeaterItems.DataSource = teaseCollection;
                repeaterItems.DataBind();

                //category promo
                if (!string.IsNullOrEmpty(tc.PromoResourceKey))
                {
                    PlaceHolder phCategoryPromo = e.Item.FindControl("phCategoryPromo") as PlaceHolder;
                    phCategoryPromo.Visible = true;
                    Txt txtPromo = e.Item.FindControl("txtPromo") as Txt;
                    txtPromo.ResourceConstant = tc.PromoResourceKey;
                }

            }
        }

        protected void repeaterItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Matchnet.Email.ValueObjects.Tease tease = e.Item.DataItem as Matchnet.Email.ValueObjects.Tease;

                //render radio button and label
                Literal literalItemRadioButton = e.Item.FindControl("literalItemRadioButton") as Literal;

                string radioID = "tease" + tease.TeaseID.ToString();
                if ((!_alreadySelectedDefaultTease && _selectedTeaseID == Constants.NULL_INT) || tease.TeaseID == _selectedTeaseID)
                {
                    _alreadySelectedDefaultTease = true;
                    literalItemRadioButton.Text = "<input id=\"" + radioID + "\" type=\"radio\" name=\"Radio_TeaseItem\" value=\"" + _currentBindingCategoryID.ToString() + "_" + tease.TeaseID.ToString() + "\" checked=\"checked\" />";
                }
                else
                {
                    literalItemRadioButton.Text = "<input id=\"" + radioID + "\" type=\"radio\" name=\"Radio_TeaseItem\" value=\"" + _currentBindingCategoryID.ToString() + "_" + tease.TeaseID.ToString() + "\" />";
                }
                literalItemRadioButton.Text += "<label for=\"" + radioID + "\">" + g.GetResource(tease.ResourceKey, this) + "</label>";
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                _handler.SendTease(_selectedCategoryID, _selectedTeaseID);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public FlirtOperationResult sendTease(int selectedCategoryID, int selectedTeaseID, int memberID)
        {
            try
            {

                _handler = new FlirtHandler(this, g);
                if (!IsPostBack)
                {
                    // Get referring url and store in session, but only do this upon loading tease.ascx the first time,
                    // you don't want this to happen if/when someone tabs thru the flirt messages, which postbacks.
                    _handler.GetRefURL();
                }

                bool isHotListed = false;

                if (memberID != 0 && memberID != int.MinValue)
                {
                    this._member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                    if (this._member == null)
                    {
                        return FlirtOperationResult.MemberNotFound;
                    }

                    //TT# 16688 check if sender member is blocked
                    isHotListed = _handler.CheckIgnoreList(this._member.MemberID);
                    if (isHotListed)
                    {
                        return FlirtOperationResult.YouAreBlocked;
                    }

                    //TT#16754 check if member already flirted with this member
                    isHotListed = _handler.CheckIfAlreadyTeased(g.Member.MemberID, this._member.MemberID);

                    if (isHotListed)
                    {
                        return FlirtOperationResult.FlirtSentAlready;
                    }

                    int teasesLeft;
                    FlirtManager flirtManager = new FlirtManager(g);
                    TeaseCollection teases = TeaseSA.Instance.GetTeaseCollection(selectedCategoryID, g.Brand.Site.SiteID);
                    Matchnet.Email.ValueObjects.Tease selectedTease = null;
                    foreach (Matchnet.Email.ValueObjects.Tease tease in teases)
                    {
                        if (tease.TeaseID == selectedTeaseID)
                        {
                            selectedTease = tease;
                            break;
                        }
                    }
                    return flirtManager.SendFlirt(selectedCategoryID, selectedTeaseID, _g.GetResource("TXT_TEASE", ResourceControl), g.GetResource(selectedTease.ResourceKey, ResourceControl), this._member.MemberID, out teasesLeft);

                }
                else
                {
                    return FlirtOperationResult.MemberNotFound;
                }
            }
            catch (Exception ex)
            {
                return FlirtOperationResult.OperationFailed;
            }


            return FlirtOperationResult.FlirtSent;
        }

        public void setResultResource(FlirtOperationResult flirtOperationResult)
        {
            string resource = "FLIRT_OPERATION_FAILED";
            switch (flirtOperationResult)
            {
                case FlirtOperationResult.OperationFailed:
                    resource = "FLIRT_OPERATION_FAILED";
                    _isResultPanelVisible = false;
                    phFlirtFailedOmniture.Visible = true;
                    break;
                case FlirtOperationResult.FlirtSent:
                    resource = "FLIRT_OPERATION_SUCCESSFUL";
                    _isResultPanelVisible = true;
                    phFlirtSuccessfulOmniture.Visible = true;
                    break;
                case FlirtOperationResult.TooManyFlirts:
                    resource = "FLIRT_OPERATION_TOO_MANY_FLIRTS";
                    _isResultPanelVisible = false;
                    phFlirtFailedOmniture.Visible = true;
                    break;
                case FlirtOperationResult.FlirtSentAlready:
                    resource = "FLIRT_OPERATION_SENT_ALREADY";
                    _isResultPanelVisible = true;
                    phFlirtFailedOmniture.Visible = true;
                    break;
                case FlirtOperationResult.MemberNotFound:
                    resource = "FLIRT_OPERATION_MEMBER_NOT_FOUND";
                    _isResultPanelVisible = true;
                    phFlirtFailedOmniture.Visible = true;
                    break;
                case FlirtOperationResult.YouAreBlocked:
                    resource = "FLIRT_OPERATION_YOU_ARE_BLOCKED";
                    _isResultPanelVisible = true;
                    phFlirtFailedOmniture.Visible = true;
                    break;
                case FlirtOperationResult.AlreadyUsedUpAllFreeTeases:
                    resource = "FLIRT_OPERATION_ALREADY_USED_UP_FREE_TEASES";
                    _isResultPanelVisible = false;
                    phFlirtFailedOmniture.Visible = true;
                    break;
            }

            flirtHeader.ResourceConstant = resource;

        }

        public void LoadSuggestedProfiles()
        {
            pnlflirtForm.Visible = false;
            phFlirtResult.Visible = true;

            if (_isResultPanelVisible)
            {
                phProfileSuggestion.Visible = true;
                SearchResultList.PageSize = 3;
                SearchResultList.GalleryView = "true";
                SearchResultList.IsPartialRender = true;
                SearchResultList.LoadResultList();
            }
        }

        #endregion

        #region Private Methods
        private void BindCategoryAndContentItems()
        {
            TeaseCategoryCollection teaseCategoryCollection = TeaseSA.Instance.GetTeaseCategoryCollection(g.Brand.Site.SiteID);

            repeaterCategories.DataSource = teaseCategoryCollection;
            repeaterCategories.DataBind();
        }

        #endregion

        protected string GetAdditionalOmnitureData()
        {
            string omnitureValues = string.Empty;
            if (FrameworkGlobals.SearchResultsM2MUpgradeEnabled(g))
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("s.pageName = \"Flirt Compose overlay\";");
                sb.AppendLine("s.eVar2 = \"Flirt Compose overlay\";");
                string viewMode = "Gallery View)";
                if (!string.IsNullOrEmpty(Request.QueryString["ActionCallPage"]) &&
                    Request.QueryString["ActionCallPage"].ToLower().Contains("list"))
                {
                    viewMode = "List View)";
                }

                string _evar27 = "Flirt (Match_Results - ";
                if (!string.IsNullOrEmpty(Request.QueryString["ControlName"]) &&
                   Request.QueryString["ControlName"] == "membersonline20" &&
                 MembersOnlineManager.Instance.IsMOLRedesign30Enabled(g.Member, g.Brand))
                {
                    _evar27 = "Flirt(MOL - ";
                }
                _evar27 += viewMode;
                sb.AppendLine("s.eVar27 =\"" + _evar27 + "\";");

                omnitureValues = sb.ToString();
            }
            return omnitureValues;
        }

        protected string GetAdditionalOmnitureDataSuccess()
        {
            string omnitureValues = string.Empty;
            if (FrameworkGlobals.SearchResultsM2MUpgradeEnabled(g))
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("s.pageName = \"Flirt Confirmation overlay\";");
                sb.AppendLine("s.eVar2 = \"Flirt Confirmation overlay\";");
                string viewMode = "Gallery View)";
                if (!string.IsNullOrEmpty(Request.QueryString["ActionCallPage"]) &&
                    Request.QueryString["ActionCallPage"].ToLower().Contains("list"))
                {
                    viewMode = "List View)";
                }

                string _evar27 = "Flirt (Match_Results - ";
                if (!string.IsNullOrEmpty(Request.QueryString["ControlName"]) &&
                    Request.QueryString["ControlName"] == "membersonline20" &&
                  MembersOnlineManager.Instance.IsMOLRedesign30Enabled(g.Member, g.Brand))
                {
                    _evar27 = "Flirt(MOL - ";
                }
                _evar27 += viewMode;
                sb.AppendLine("s.eVar27 =\"" + _evar27 + "\";");

                omnitureValues = sb.ToString();
            }
            return omnitureValues;
        }
    }
}