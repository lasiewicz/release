﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhotoCommentEmailMessage.ascx.cs" Inherits="Matchnet.Web.Applications.Email.Controls.PhotoCommentEmailMessage" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<asp:Panel ID="photoCommentReplyText" runat="server">
<div class="message-view-essay-comment clearfix">
    <asp:Image ID="imgThumbnail" CssClass="photo" Width="58" Height="75" runat="server" alt="" />

    <h2><mn:Txt id="txtOrigMessageLabel" runat="server" /></h2>
    <div class="essay-body">
    <asp:PlaceHolder ID="phCaption" runat="server">
    <p class="photo-comment-caption"><strong><mn:txt runat="server" id="txtCaptionLabel" resourceconstant="TXT_CAPTION_LABEL" expandimagetokens="true" /></strong> <asp:Literal ID="commentText" runat="server"/></p>
    </asp:PlaceHolder>
    </div>

</div>
</asp:Panel>