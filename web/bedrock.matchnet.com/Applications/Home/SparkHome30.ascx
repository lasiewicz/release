﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SparkHome30.ascx.cs" Inherits="Matchnet.Web.Applications.Home.SparkHome30" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="qa" TagName="MiniQuestionModule" Src="~/Applications/QuestionAnswer/Controls/MiniQuestionModule.ascx" %>
<%@ Register TagPrefix="qa" TagName="MiniAnswerModule" Src="~/Applications/QuestionAnswer/Controls/MiniAnswerModule.ascx" %>
<%@ Register tagprefix="uc2" tagname="HeroProfileFilmStrip" src="~/Framework/Ui/BasicElements/HeroProfileFilmStrip.ascx"  %>
<%@ Register tagprefix="uc2" tagname="ProfileActivity" src="~/Framework/Ui/BasicElements/ProfileActivity.ascx"  %>
<%@ Register TagPrefix="uc1" TagName="HotlistProfileStrip" Src="/Applications/Hotlist/HotlistProfileStrip.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MicroProfileList" Src="../../Framework/Ui/BasicElements/MicroProfileList.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MicroProfileStrip" Src="../../Framework/Ui/BasicElements/MicroProfileStrip.ascx" %>
<%@ Register TagPrefix="uc1" TagName="HotListBar" Src="/Applications/Hotlist/HotListBar.ascx" %>
<%@ Register TagPrefix="qa" TagName="SubmitQuestionClientModule" Src="~/Applications/QuestionAnswer/Controls/SubmitQuestionClientModule.ascx" %>
<%@ Register TagPrefix="qa2" TagName="MiniAnswerClientModule" Src="~/Applications/QuestionAnswer/Controls/MiniAnswerClientModule.ascx" %>
<%@ Register tagprefix="uc1" Tagname="SecretAdmirerPopup" src="~/Applications/Home/Controls/SecretAdmirerPopup.ascx"   %>

<div id="page-container" class="sparkhome30">
<style type="text/css">
/* Temp styles for hiding components to be removed*/
#hp-hero,#content-promo #activity-center{display:none;}

/* homestat */
.homestat{
    margin-top:7px;
}
.homestat .item{
    float:left;
    width:21%;
    border:1px solid #ccc;
    /**/border-top:1px solid #ccc;
    border-left:1px solid #ccc;
    border-bottom:1px solid #96b1cc;
    border-right:1px solid #96b1cc;
    border-radius:4px;
    background-color:#eceff2;
    -moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;
    /*box-shadow:2px 2px 2px rgba(0,0,0,0.3),inset 2px 2px 2px rgba(255,255,255,0.7),inset -2px -2px 5px rgba(112, 128, 144,0.2);
    box-shadow:2px 2px 2px rgba(0,0,0,0.3),inset 2px 2px 4px rgba(255,255,255,0.8),inset -4px -4px 12px rgba(37, 66, 107, 0.2);
    box-shadow:2px 2px 2px rgba(0,0,0,0.2),inset 2px 2px 4px rgba(255,255,255,0.6),inset -4px -4px 12px rgba(37, 66, 107, 0.1);*/
    box-shadow:2px 2px 2px rgba(0,0,0,0.2);
    padding:6px 2px 8px;
    margin:0 1% .5em 0;
}
.homestat .item:last-child{margin-right:0}
.homestat .item:hover{background-color:white;}

/* homestat custom widths */
.homestat #homestat-emailed.item{width:22%;}
.homestat #homestat-flirted.item{width:24%;}
.homestat #homestat-imed.item{width:18.5%;}
.homestat #activity-center.item{width:32.5%;}

.ie8 .homestat #homestat-emailed.item{width:21.5%;}
.ie8 .homestat #homestat-imed.item{width:18%;}
.ie7 .homestat #homestat-emailed.item{width:21.0%;}
.ie7 .homestat #homestat-flirted.item{width:23.0%;}
.ie7 .homestat #homestat-imed.item{width:17.5%;}
.ie7 .homestat #activity-center.item{width:31.5%;}

/* homestat activity center custom styles */
.homestat #activity-center.item{
    font-size:11px;
    padding:0;
}
.homestat #activity-center.item .manage-item{
    font-weight:normal;
}
.homestat #activity-center.item .activity-profile-info{
    width:73%;
}
.homestat #activity-center.item .activity-profile-pic{
    width:40px;
}
.homestat #activity-center.item .no-photo-container{
    display:table-row;
}
.homestat #activity-center.item .no-photo{
    display:table-cell;
    vertical-align:middle;
    padding-top:0;
    font-size:10px;
}
.ie7 .homestat #activity-center.item .no-photo{
    width:auto;
    height:100%;
    margin-top:-1px;
    vertical-align:bottom;
}
/* homestat custom styles */
.homestat .value,.homestat .type{
    float:left;
    -moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;
    width:50%;
    padding:2px;
    font-family:Georgia;
}
.lte8 .value,.homestat .type{width:47.5%;}
.homestat .value{
    text-align:right;
}
.homestat .type{
    text-align:center;
    font-size:16px;
    padding-top:6px;
    line-height:1.2;
}
.homestat .num{
    font-size:26px;
    font-weight:normal;
    line-height:1;
}
.homestat .label{
    font-family:Arial;
    line-height:1;
    font-size:10px
}
.homestat .value > a{
    display:inline-block;
    text-align:left;
    width:50px;
}
.homestat .value > a:hover{
    text-decoration:none;
}
.homestat .value > a sup{
    display:inline-block;
    background-color:#c11900;
    padding:0 2px;
    color:white;
    font-size:11px;
    font-family:Arial;
    font-weight:bold;
    top:4px;
    position:relative;
}

/* ++Com--Member Filmstrip */
.filmstrip{ visibility: hidden; position: relative; margin: 0 auto; }
#filmstrip-yourmatches{ position:relative; overflow:hidden; height:145px; }
#filmstrip-yourmatches .ajax-loading{ background-color:white; height:100px; left:0; position:absolute; top:0; width:636px; z-index:3; padding:12px; text-align:center; }
.filmstrip li.item{ display:block; float:left; border:1px solid #ccc; margin:4px; height:auto !important; position:relative; border-radius:6px;-moz-border-radius:6px;-webkit-border-radius:6px; }
.ie7 .filmstrip li.item{width:92px;}
.filmstrip li.item.style-basic{ -moz-box-shadow:2px 2px 2px rgba(0,0,0,0.2);-webkit-box-shadow:2px 2px 2px rgba(0,0,0,0.2);box-shadow:2px 2px 2px rgba(0,0,0,0.2); background-image: -moz-linear-gradient(#fff, #eceff2 90%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0.0, white), color-stop(1.0, #eceff2)); background-color: #eceff2; }

.filmstrip li.item.style-basic.highlighted{ background-image: -moz-radial-gradient(#F2F7E8 20%, #D6F0A1 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0.0, #F2F7E8), color-stop(1.0, #D6F0A1)); background-color: #d6f0a1; }

.filmstrip li.item.view-more a.memberpic{ display:block; font-family:georgia; font-size:13px; height:77px; padding:50px 8px 8px 22px; }
.results.filmstrip .picture{ margin:10px; }
.results.filmstrip .picture img{ width:70px; height:92px; }
.results.filmstrip .picture .no-photo{ width:70px; height:48px; }
.results.filmstrip .cc-pic-tag-sm { position:absolute; bottom:29px; left:11px; width:70px; }
.filmstrip h2{ text-align: center; font-size: 12px; }

button.filmstrip-next, button.filmstrip-prev{ margin-top:60px; }
button.filmstrip-prev{float:left;}
button.filmstrip-next{float:right;}
button.disabled{ background-color: #ccc; color:#999; border:1px solid #ccc; cursor: auto; text-decoration:none; outline:none; }

button.filmstrip-next, button.filmstrip-prev{margin-top:0px;position:absolute;top:44px;font-size:18px}
button.filmstrip-prev{border-radius:8px 0px 0px 8px;left:8px;padding:12px 12px 12px 6px;}
button.filmstrip-next{border-radius:0px 8px 8px 0px;right:8px;padding:12px 6px 12px 12px;}

.sparkhome30 #member-filmstrip .header-options{margin-left:12px;margin-top:12px;}

/* update existing styles */
#filmstrip-yourmatches .ajax-loading{width:100%;height:100%;}

</style>

<div class="homestat clearfix">
	<div id="homestat-emailed" class="item">
		<div class="value"><a href="#"><span class="num">37</span><sup class="new-messages">3</sup> <span class="label">members</span></a></div>
		<div class="type">Emailed You!</div>
	</div>
	<div id="homestat-flirted" class="item">
		<div class="value"><a href="#"><span class="num">53</span> <span class="label">members</span></a></div>
		<div class="type">Flirted with You!</div>
	</div>
	<div id="homestat-imed" class="item">
		<div class="value"><a href="#"><span class="num">25</span> <span class="label">members</span></a></div>
		<div class="type">IM'd You!</div>
	</div>
	<div id="activity-center" class="item">
		<div class="activity-content">
			<div class="activity-profile-wrapper">
				<div class="activity-profile-pic">
					<div class="no-photo-container" style="width: 40px; height: 52px; background-image: url(http://www.jdate.com/img/no-photo-s-m.png)">
						<a href="/Applications/MemberProfile/MemberPhotoEdit.aspx" class="no-photo">
							Upload a photo now
						</a>
					</div>
				</div>
				<!-- logged in -->
				<div class="activity-profile-info clearfix">
					<h2>Michael</h2>

					<a href="/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit" class="manage-item">Complete/Edit Profile</a><a href="/Applications/MemberProfile/MemberPhotoEdit.aspx" class="manage-item">Manage Photos</a>
				</div>
			</div>
		</div>
	</div>
</div>











<div class="clearfix" id="member-filmstrip">
<div class="header-options clearfix">
<h2 class="component-header">Your Matches</h2><div class="links"><a href="/Applications/Search/SearchPreferences.aspx">[Edit]</a> | <a href="/Applications/Search/SearchResults.aspx">View More»</a></div></div><div id="filmstrip-yourmatches"><div class="ajax-loading"><img border="0" src="/img/Community/BlackSingles/ajax-loader.gif" /><p>Loading…</p></div>
<button class="filmstrip-prev disabled">◄</button>
<button class="filmstrip-next">►</button>

<div class="filmstrip results">
<ul>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=1&amp;MemberID=127231405&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/04/15/09/165829972.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=1&amp;MemberID=127231405&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">mswaiting70</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=2&amp;MemberID=119906030&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/04/10/10/165780103.jpg" class="profileImageHover" title="View my profile"></a><div class="cc-pic-tag cc-pic-tag-sm cc-pic-white"><span>White</span></div></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=2&amp;MemberID=119906030&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">cuddlingbug</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=3&amp;MemberID=127219852&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/04/09/19/165375242.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=3&amp;MemberID=127219852&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">Justwanna...</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=4&amp;MemberID=119696463&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/04/09/08/165766198.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=4&amp;MemberID=119696463&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">ceodiva</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=5&amp;MemberID=119899490&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/04/07/11/165173430.jpg" class="profileImageHover" title="View my profile"></a><div class="cc-pic-tag cc-pic-tag-sm cc-pic-blue"><span>Blue</span></div></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=5&amp;MemberID=119899490&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">TruBluCal...</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=6&amp;MemberID=127211251&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/04/06/06/165163482.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=6&amp;MemberID=127211251&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches" >tifsempire</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=7&amp;MemberID=119888195&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/04/04/04/165719758.jpg" class="profileImageHover" title="View my profile"></a><div class="cc-pic-tag cc-pic-tag-sm cc-pic-blue"><span>Blue</span></div></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=7&amp;MemberID=119888195&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">cyngle1</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=8&amp;MemberID=119885678&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/03/31/12/165283487.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=8&amp;MemberID=119885678&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches" >prophet2012</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=9&amp;MemberID=119882936&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/03/30/10/165672871.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=9&amp;MemberID=119882936&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches" >kgipson2006</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=10&amp;MemberID=127194970&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/03/28/14/165656872.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=10&amp;MemberID=127194970&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">SEXY4000</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=11&amp;MemberID=119667486&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/03/25/21/165633514.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=11&amp;MemberID=119667486&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">Sweetshor...</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=12&amp;MemberID=119663229&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/03/28/17/165659431.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=12&amp;MemberID=119663229&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">leadingla...</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=13&amp;MemberID=119860964&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/03/19/09/164999499.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=13&amp;MemberID=119860964&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">LQQKN2BFO...</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=14&amp;MemberID=119650659&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/03/18/09/164987838.jpg" class="profileImageHover" title="View my profile"></a><div class="cc-pic-tag cc-pic-tag-sm cc-pic-white"><span>White</span></div></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=14&amp;MemberID=119650659&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">DeeDeeA1B1</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=15&amp;MemberID=119851916&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/03/14/14/165125969.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=15&amp;MemberID=119851916&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">shellylov...</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=16&amp;MemberID=119844410&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/03/11/06/165490741.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=16&amp;MemberID=119844410&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">BillyJean</a></h2></li>
<li class="item style-basic"><div class="picture"><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=17&amp;MemberID=127157071&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches"><img border="0" alt="profile picture" src="http://photos.sparksites.com/2012/03/10/08/164909310.jpg" class="profileImageHover" title="View my profile"></a></div><h2><a href="http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&amp;EntryPoint=4&amp;Ordinal=17&amp;MemberID=127157071&amp;LayoutTemplateID=15&amp;PremEntPoint=1&amp;ClickFrom=Homepage - Your Matches">MzAnnB518</a></h2></li>
<li class="item style-basic view-more"><a class="memberpic" href="/Applications/Search/SearchResults.aspx">View More »</a></li></ul>
</div></div></div>




<div id="hp-hero" class="clearfix rbox-style-high">
    <div id="hp-hotlist-stats">
    <asp:PlaceHolder ID="phHotLists" runat="server">
            <uc1:HotListBar id="ucHotlist2" runat="server" Direction="2" ClassSelected="selected" Class="hp-hotlist-stats" ListItemClass="" HideIfAllListsEmpty="true" />
            <%--<uc1:HotListBar id="ucHotlist1" runat="server" Direction="1" ClassSelected="selected" Class="hp-hotlist-stats" ListItemClass="" />--%>
            <p class="view-more"><a href="/Applications/HotList/View.aspx?CategoryID=-1"><mn2:FrameworkLiteral ID="FrameworkLiteral6" runat="server" ResourceConstant="TXT_VIEW_ALL_HOTLISTS"></mn2:FrameworkLiteral></a></p>
            
            <asp:PlaceHolder ID="phShortcutList" runat="server" Visible="false">
                <h2 class="component-header"><mn:Txt ID="txtShortcutHeader" runat="server" ResourceConstant="TXT_SHORTCUT" /></h2>
                <ul class="hp-hotlist-stats">
                    <li class="clearfix">
                        <a name="noclick" class="hotlist-stat-icon"><span class="spr s-icon-history-blank"><span></span></span></a><a href="/Applications/HotList/View.aspx?CategoryID=-9" class="hotlist-stat-category"><mn:txt id="txtShortcutYourCheckingOut" runat="server" resourceconstant="TXT_SHORTCUT_YOUR_CHECKING_OUT" expandimagetokens="true" />
                      </a></li>
                    <li class="clearfix"><a name="noclick" class="hotlist-stat-icon">
                        <asp:MultiView ID="multiviewShortcutMailIcon" runat="server" EnableViewState="false" ActiveViewIndex="0">
                            <asp:View ID="viewShortcutMailBlankIcon" runat="server"><span class="spr s-icon-history-blank"><span></span></span></asp:View>
                            <asp:View ID="viewShortcutMailFlashingIcon" runat="server"><span class="spr s-icon-icon-email-new"><span></span></span></asp:View>
                        </asp:MultiView></a><a href="/Applications/Email/MailBox.aspx" class="hotlist-stat-category"><mn:Txt ID="txtShortcutMail" runat="server" ResourceConstant="TXT_SHORTCUT_MAIL" /><asp:Literal ID="literalShortcutNewMailCount" runat="server"></asp:Literal></a>
                    </li>
                    <li class="clearfix"><a name="noclick" class="hotlist-stat-icon"><span class="spr s-icon-history-blank"><span></span></span></a><a href="/Applications/HotList/View.aspx?CategoryID=-10" class="hotlist-stat-category"><mn:Txt ID="txtShortcutMatches" runat="server" ResourceConstant="TXT_SHORTCUT_MATCHES" /> <asp:Literal ID="literalShortcutNewMatchesCount" runat="server"></asp:Literal></a></li>
                    <li class="clearfix"><a name="noclick" class="hotlist-stat-icon"><span class="spr s-icon-history-blank"><span></span></span></a><a href="/Applications/PhotoGallery/PhotoGallery.aspx" class="hotlist-stat-category"><mn:Txt ID="txtShortcutPhotos" runat="server" ResourceConstant="TXT_SHORTCUT_PHOTOS" /> <asp:Literal ID="literalShortcutNewPhotosCount" runat="server"></asp:Literal></a></li>
                    <asp:PlaceHolder runat="server" ID="pushFlirts">
                    <li class="clearfix stats-new"><span class="stats-new-tag"><mn:txt runat="server" id="txtNew" resourceconstant="TXT_NEW" expandimagetokens="true" /></span> <button type="button" id="pushFlirtsButton" class="textlink"><mn:txt runat="server" id="txtButtonStartFlirting" resourceconstant="TXT_BUTTON_START_FLIRTING" expandimagetokens="true" /></button>
                        <div id="pushFlirtsContainer" class="hide">
                            <div id="pushFlirtsContInner" class="padding-heavy"><!--filled via ajax --></div>
                        </div><!-- end pushFlirtsContainer -->

                        <script>
                            __addToNamespace__('spark.pushFlirts', {
                                sendAmount: '0',
                                dialogTitle: '<%=g.GetResource("TXT_FLIRT_MODAL_TITLE",this) %>',
                                messageNoneSelected: '<%=g.GetResource("TXT_FLIRT_MESSAGE_NONE_SELECTED",this) %>',
                                messageErrorGeneral: '<%=g.GetResource("TXT_FLIRT_MESSAGE_UNKNOWN_ERROR",this) %>'
                            });

                            $j(spark.pushFlirts).bind("pushFlirtsRefresh", function (event) {
                                $j.ajax({
                                    type: "POST",
                                    url: "/Applications/API/Flirts.asmx/GetPushFlirts",
                                    contentType: "application/json; charset=ISO-8859-1",
                                    dataType: "json",
                                    beforeSend: function () {
                                        $j('#pushFlirtsContInner').html(spark.util.ajaxLoader);
                                    },
                                    success: function (response) {
                                        $j('#pushFlirtsContInner').html(response.d).find('#pushFlirtsSend').bind('click', function (event) {
                                            $j(spark.pushFlirts).trigger("pushFlirtsValidate", $j('#pushFlirtsContainer'));
                                        });
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        $j(spark).trigger("jqueryUIDialogMessaging", [spark.pushFlirts.messageErrorGeneral, "notification error"]);
                                    }
                                });
                            });

                            $j(spark.pushFlirts).bind("pushFlirtsValidate", function (event, obj) {
                                var $checkBoxes = $j(obj).find('input[type=checkbox]'),
                                $checkedCheckboxes = $j(obj).find('input[type=checkbox]:checked');

                                $checkBoxes.bind('change', function () {
                                    $j('#pushFlirtsButtonContainer').unblock();
                                });

                                // set number of selected members to flirt with
                                spark.pushFlirts.sendAmount = $checkedCheckboxes.length;

                                if ($checkedCheckboxes.length) {
                                    $j(spark.pushFlirts).trigger("pushFlirtsSubmit");
                                }
                                else {
                                    $j('#pushFlirtsButtonContainer').block({
                                        message: spark.pushFlirts.messageNoneSelected,
                                        overlayCSS: {
                                            backgroundColor: '#fff',
                                            opacity: 0.9
                                        },
                                        css: {
                                            width: 'auto',
                                            top: '0',
                                            left: '0px',
                                            backgroundColor: null,
                                            border: null,
                                            padding: null,
                                            color: null,
                                            margin: null,
                                            textAlign: null
                                        }
                                    }).find('.blockMsg').addClass('notification');
                                }
                            });

                            $j(spark.pushFlirts).bind("pushFlirtsSubmit", function (event) {

                                var targetMembersString = "";
                                var count = 0;

                                $j('#pushFlirtsContainer').find('input[type=checkbox]:checked').each(function () {
                                    if (targetMembersString == "") {
                                        targetMembersString = $j(this).data('memberid');
                                    }
                                    else {
                                        targetMembersString = targetMembersString + "," + $j(this).data('memberid');
                                    }
                                    count++;
                                });

                                sendPushFlirtsOmniture(count, targetMembersString);

                                $j.ajax({
                                    type: "POST",
                                    url: "/Applications/API/Flirts.asmx/ProcessPushFlirts",
                                    data: "{ 'memberIDs': '" + targetMembersString + "'}",
                                    contentType: "application/json; charset=ISO-8859-1",
                                    dataType: "json",
                                    beforeSend: function () {
                                        $j('#pushFlirtsContInner').html(spark.util.ajaxLoader);
                                    },
                                    success: function (response) {
                                        $j('#pushFlirtsContInner').html(response.d);
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        $j(spark).trigger("jqueryUIDialogMessaging", [spark.pushFlirts.messageErrorGeneral, "notification error"]);

                                    }
                                });
                            });

                            /* bind feature specific custom behaviors */
                            $j(spark.pushFlirts).bind("pushFlirtsDialog", function (event, title) {

                                if ($j('#pushFlirtsContainer').length === 0) {
                                    $j('<div id="pushFlirtsContainer" class="hide"></div>').appendTo('body');
                                }

                                $j('#pushFlirtsContainer')
                                .dialog({
                                    width: 669,
                                    modal: true,
                                    title: spark.pushFlirts.dialogTitle,
                                    position: ['center', 'top'],
                                    dialogClass: 'modal-push-flirts',
                                    minHeight: 400,
                                    open: function () {
                                        var $this = $j(this),
                                            ajax_load = '<div class="loading spinner-only" />';
                                        $j(spark.pushFlirts).trigger('pushFlirtsRefresh');
                                    },
                                    close: function () {
                                        $j('#pushFlirtsContainer').find('.notification, .conf-flirt').remove();
                                    }
                                })
                                .parent('.ui-dialog').css('top', function () {
                                    var x = $j(this).position();
                                    return x.top + 12;
                                });
                            });

                            /* bind custom behaviors on dom ready */
                            $j(function () {
                                $j('#pushFlirtsButton').bind('click', function (event) {
                                    $j(spark.pushFlirts).trigger("pushFlirtsDialog");
                                });
                            });


                            function sendPushFlirtsOmniture(numberSent, targetMembersString) {

                                var sentString = numberSent + " flirts sent";
                                var s = s_gi(s_account);
                                PopulateS(true);
                                s.prop48 = sentString;
                                s.events = 'event41,event5,event2';
                                s.t(true);

                                var i = 0;
                                for (i = 0; i < numberSent; i++) {
                                    s = s_gi(s_account);
                                    s.prop48 = '';
                                    s.linkTrackVars = 'events';
                                    s.linkTrackEvents = 'event11';
                                    s.events = 'event11';
                                    s.eVar26 = "Tease";
                                    s.prop35 = targetMembersString;
                                    s.t(true);
                                }
                            }
                        
                        </script>    
                    </li>                
                    
                    </asp:PlaceHolder>
                    <li><asp:Label runat="server" ID="lblPushflirts" Visible="false">Shown</asp:Label></li>
                </ul>


    <asp:PlaceHolder ID="plcPopupPushFlirtsDialog" runat="server" Visible="false">
        <script type="text/javascript">
            $j(function () {
                $j(spark.pushFlirts).trigger("pushFlirtsDialog");
            });
        </script>
    </asp:PlaceHolder>


            </asp:PlaceHolder>
      </asp:PlaceHolder>
      <asp:PlaceHolder ID="phEmptyListDisplay" runat="server" Visible="false">
      <mn:Txt ID="txtEMptyListDisplay" ResourceConstant="TXT_EMPTY_LISTS_TEXT" runat="server" />
      </asp:PlaceHolder>
        </div>
       <%-- <uc2:ProfileActivity ID="ProfileActivity1" runat="server" TrackingParam="ClickFrom=Homepage%20%u2013%20Profile%20Activity%20Module" />--%>
    <uc2:HeroProfileFilmStrip ID="HeroProfileFilmStripMatches" runat="server" TrackingParam="ClickFrom=Homepage - Your Matches" />
</div>


<uc1:SecretAdmirerPopup ID="ucSecretAdmirerPopup" runat="server" />


    <asp:PlaceHolder ID="phQuestionAnswer" runat="server" Visible="false">
        <div id="qanda" class="qanda border-box clearfix">
            <h2 class="component-header"><mn2:FrameworkLiteral ID="literalQuestionAnswerHeader" runat="server" ResourceConstant="TXT_TODAYSPARKS"></mn2:FrameworkLiteral>&#160;<mn:Txt runat="server"  ResourceConstant="TXT_TODAYS_SPARKS" ID="mnTxtTodaysSparks" />
            </h2>
            
            <!--Question module-->
            <qa:MiniQuestionModule ID="MiniQuestionModule1" runat="server" />
            <!--Answer module-->
            <qa:MiniAnswerModule ID="MiniAnswerModule1" runat="server" TrackingParam="ClickFrom=Homepage%20%u2013%20Question%20of%20the%20Week" />

            <asp:PlaceHolder ID="phNoAnswers" runat="server" Visible="false">
                <div id="mini-a-module" class="mini-a-module clearfix">
                    <qa2:MiniAnswerClientModule ID="MiniAnswerClientModule1" ElementId="answers-list" runat="server" />    
                    <div id="answers-list">
                        <div id="qanda-no-answers"><mn:Txt ID="txtNoAnswers" runat="server" ResourceConstant="TXT_NO_ANSWERS" /></div>
                    </div>
                </div>
                <script type="text/javascript">
                    $j("#qanda").find('.header-options').hide();
                </script>        
            </asp:PlaceHolder>
            <qa:SubmitQuestionClientModule ID="sendQuestionClientModule" runat="server" />
        </div>
    </asp:PlaceHolder>


<script type="text/javascript">
    adroll_adv_id = "JCRCKLJSYNGSBPBGKO3MK6";
    adroll_pix_id = "7TJ6IFAMERA27BDOPR5K4V";
    (function () {
        var oldonload = window.onload;
        window.onload = function () {
            __adroll_loaded = true;
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
            document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
            if (oldonload) { oldonload() }
        };
    }());
 </script>

<!--iframe used by SaveYNMVote function-->
<iframe name="FrameYNMVote" tabindex="-1" frameborder="0" scrolling="no" width="1" height="1">
	<layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
</iframe>
</div>
