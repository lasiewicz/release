﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.Home.Controls;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.PageAnnouncements;


namespace Matchnet.Web.Applications.Home
{
    public partial class SparkHome40b : FrameworkControl
    {
        private int _memberID = 0;
        private const string DOMAINID = "DomainID";
        private int _highlightedProfileCount = 0;

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //load homepage; ensure member is logged in (not available for visitors)
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    _memberID = g.Member.MemberID;

                    if (g.LayoutTemplateBase != null)
                    {
                        //load page top
                        var sparkHomeTop = Page.LoadControl("/Applications/Home/Controls/SparkHomeTopB.ascx") as SparkHomeTopB;
                        g.LayoutTemplateBase.AddPageTopControl(sparkHomeTop);

                        //hide "what are these icons"
                        g.LayoutTemplateBase.HideIconLinks();
                    }

                    //save results url used in view profile's "back to results" feature
                    FrameworkGlobals.SaveBackToResultsURL(g);

                    // if any notification messages that came through session, display it
                    HandleNotificationMessage();

                    //initialize connect links
                    InitializeConnects();

                    //load newsfeed widget
                    ucNewsfeed.LoadNewsfeed(0, 6, true);

                    var emptyNewsfeed = ucNewsfeed.NumberOfLoadedNotifications == 0 || ucNewsfeed.NumberOfLoadedNonSystemNotifications == 0;

                    newsfeedContainer.Visible = !emptyNewsfeed;
                    noNewsfeedContainer.Visible = emptyNewsfeed;

                    //subscription expiration alert
                    if (SubscriptionExpirationManager.Instance.DisplaySubscriptionExpirationAlert(g.Brand, g.Member, g))
                    {
                        SubscriptionExpirationAlert subscriptionExpirationAlert = Page.LoadControl("/Framework/Ui/PageAnnouncements/SubscriptionExpirationAlert.ascx") as SubscriptionExpirationAlert;
                        g.LayoutTemplateBase.AddPageAccouncementControl(subscriptionExpirationAlert);

                    }

                    //push flirts
                    LoadPushFlirtsPopup();

                    g.RightNavControl.ShowMiniMemberSlideShowBelowAd();

                }
                else
                {
                    //member is not logged in, send to the splash page
                    g.Transfer("/");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                // Omniture Analytics - Event and property variable tracking
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    // Track total profiles that are highlighted in the search result
                    _g.AnalyticsOmniture.AddEvent("event5");
                    _g.AnalyticsOmniture.AddProductEvent("event5=" + Convert.ToString(this._highlightedProfileCount));

                    // MPR-940 JewishSearch
                    if ((_g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate))
                    {
                        int optionValue = _g.Member.GetAttributeInt(g.Brand, "JdateReligion", Constants.NULL_INT);

                        // Selected Any
                        if (optionValue == Constants.NULL_INT)
                        {
                            _g.AnalyticsOmniture.Evar41 = "Standard";
                        }
                        else
                        {
                            if (((optionValue & 16384) == 16384) || //Willing to Convert
                            ((optionValue & 2048) == 2048) || //Not Willing to Convert
                            ((optionValue & 512) == 512)) //Not sure if I’m willing to convert
                            {
                                _g.AnalyticsOmniture.Evar41 = "Standard";
                            }
                            else
                            {
                                _g.AnalyticsOmniture.Evar41 = "Jewish Only";
                            }
                        }
                    }

                    // _g.AnalyticsOmniture.Prop32 = (this.ProfileActivity1.HasHotListActivity) ? "Has Hot List Activity" : "Has No Activity";
                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion

        #region Private Methods
        private void InitializeConnects()
        {
            lnkNewMemberPhotos.ToolTip = this.ResourceManager.GetResource("TXT_CONNECT_NEW_MEMBER_PHOTOS_TITLE", this);
            lnkNewMemberPhotos.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.PhotoGallery, WebConstants.Action.ViewPageButton, lnkNewMemberPhotos.NavigateUrl, "Connect Module");

            lnkMemberOnline.ToolTip = this.ResourceManager.GetResource("TXT_CONNECT_MEMBERS_ONLINE_TITLE", this);
            lnkMemberOnline.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MembersOnline, WebConstants.Action.ViewPageButton, lnkMemberOnline.NavigateUrl, "Connect Module");

            lnkKibitz.ToolTip = this.ResourceManager.GetResource("TXT_CONNECT_KIBITZ_CORNER_TITLE", this);
            lnkKibitz.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.QuestionAnswerHome, WebConstants.Action.ViewPageButton, lnkKibitz.NavigateUrl, "Connect Module");

            lnkSecretAdmirer.ToolTip = this.ResourceManager.GetResource("TXT_CONNECT_SECRET_ADMIRER_TITLE", this);
            lnkSecretAdmirer.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SlideShow, WebConstants.Action.ViewPageButton, lnkSecretAdmirer.NavigateUrl, "Connect Module");
        }

        private void HandleNotificationMessage()
        {
            if (g.Session.Get(WebConstants.SESSION_NOTIFICATION_MESSAGE) != null)
            {
                g.Notification.AddMessage(g.Session.Get(WebConstants.SESSION_NOTIFICATION_MESSAGE).ToString());
                g.Session.Remove(WebConstants.SESSION_NOTIFICATION_MESSAGE);
            }
        }

        private void LoadPushFlirtsPopup()
        {
            FlirtManager flirtManager = new FlirtManager(g);

            if (flirtManager.ShowPushFlirtsToMember(PushFlirtViewMode.ForcedOverlay))
            {
                phPushFlirtsJS.Visible = true;
                g.LayoutTemplateBase.DisableGamOverlay();
                flirtManager.IncrementPushFlirtsShownCount();
            }
            else
            {
                phPushFlirtsJS.Visible = false;
                pushFlirts.Visible = false;
            }
        }
        #endregion

    }
    
    
}