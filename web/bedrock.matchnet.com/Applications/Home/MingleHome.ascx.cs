﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.QuestionAnswer;
using System.Collections;
using Matchnet.Search.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework.Ui;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Applications.Home
{
    /// <summary>
    /// Control represents new redesign home page for Mingle.
    /// 
    /// Note: There is expectation that Mingle homepages will be radically different from other BH sites, and possibly even
    /// within Mingle so we'll start with this.
    /// </summary>
    public partial class MingleHome : FrameworkControl
    {
        #region Fields
        private int _memberID = 0;
        private const string DOMAINID = "DomainID";
        private int _highlightedProfileCount = 0;
        private bool _hasMatches = false;

        #endregion

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //load homepage; ensure member is logged in (not available for visitors)
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    _memberID = g.Member.MemberID;

                    //load profile film strip
                    LoadProfileFilmStrip();

                    //load profile activity
                    LoadProfileActivity();

                    //question and answers
                    LoadQuestionAnswer();

                    bool enableMiniSlideshow = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_CONFIGURABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)); ;

                    if (enableMiniSlideshow)
                    {
                        g.RightNavControl.ShowMiniMemberSlideShow();
                    }

                    //save results url used in view profile's "back to results" feature
                    FrameworkGlobals.SaveBackToResultsURL(g);

                    // if any notification messages that came through session, display it
                    HandleNotificationMessage();
                }
                else
                {
                    //member is not logged in, send to the splash page
                    g.Transfer("/");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                // Omniture Analytics - Event and property variable tracking
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    // Track total profiles that are highlighted in the search result
                    _g.AnalyticsOmniture.AddEvent("event5");
                    _g.AnalyticsOmniture.AddProductEvent("event5=" + Convert.ToString(this._highlightedProfileCount));

                    // MPR-940 JewishSearch
                    if ((_g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate))
                    {
                        int optionValue = _g.Member.GetAttributeInt(g.Brand, "JdateReligion", Constants.NULL_INT);

                        // Selected Any
                        if (optionValue == Constants.NULL_INT)
                        {
                            _g.AnalyticsOmniture.Evar41 = "Standard";
                        }
                        else
                        {
                            if (((optionValue & 16384) == 16384) || //Willing to Convert
                            ((optionValue & 2048) == 2048) || //Not Willing to Convert
                            ((optionValue & 512) == 512)) //Not sure if I’m willing to convert
                            {
                                _g.AnalyticsOmniture.Evar41 = "Standard";
                            }
                            else
                            {
                                _g.AnalyticsOmniture.Evar41 = "Jewish Only";
                            }
                        }
                    }

                    _g.AnalyticsOmniture.Prop32 = (this.ProfileActivity1.HasHotListActivity) ? "Has Hot List Activity" : "Has No Activity";
                    _g.AnalyticsOmniture.Prop33 = (this._hasMatches) ? "Has Matches" : "Has No Matches";

                    //Add omniture tracking for profile in Activity Center
                    _g.RightNavControl.ActivityCenter.TrackingParam = "ClickFrom=" + Server.UrlEncode("Homepage - Activity Center");
                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion


        #region Private Methods
        private void HandleNotificationMessage()
        {
            if (g.Session.Get(WebConstants.SESSION_NOTIFICATION_MESSAGE) != null)
            {
                g.Notification.AddMessage(g.Session.Get(WebConstants.SESSION_NOTIFICATION_MESSAGE).ToString());
                g.Session.Remove(WebConstants.SESSION_NOTIFICATION_MESSAGE);
            }
        }

        private void LoadQuestionAnswer()
        {
            if (QuestionAnswerHelper.IsQuestionAnswerEnabled(g.Brand))
            {
                Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question;
                question = QuestionAnswer.QuestionAnswerHelper.GetActiveQuestion(g.Member, g, true);
                if (question != null)
                {
                    phQuestionAnswer.Visible = true;
                    MiniQuestionModule1.LoadQuestion(question);
                    MiniAnswerModule1.LoadAnswers(question);
                }
                else
                {
                    phQuestionAnswer.Visible = false;
                }
            }
            else
            {
                phQuestionAnswer.Visible = false;
            }
        }

        private void LoadProfileActivity()
        {
            ProfileActivity1.LoadProfileActivity();
        }

        private void LoadProfileFilmStrip()
        {
            ProfileFilmStripYourMatches.LoadYourMatches(BreadCrumbHelper.EntryPoint.HomePage);
            _hasMatches = ProfileFilmStripYourMatches.HasMatches;
        }

        private List<ProfileHolder> GetYourMatches(SearchPreferenceCollection prefs, int maxProfileDisplay)
        {
            // declare local varialbes for resuse and clarity
            const string HASPHOTOFLAG = "HasPhotoFlag";
            int startRow = 0;
            int pageSize = maxProfileDisplay;
            MatchnetQueryResults searchResults = null;

            //get current photo preference before we modify it
            string photoPreference = prefs.Value(HASPHOTOFLAG);
            prefs.Add(HASPHOTOFLAG, "1");

            //Ensure that domainID is set on search preferences for the correct Community.
            prefs.Add(DOMAINID, g.Brand.Site.Community.CommunityID.ToString());
            prefs.Add("ColorCode", "");
            try
            {
                // Potential for exception due to invalid search preferences on this member's stored set of preferences.
                searchResults = MemberSearchSA.Instance.Search(
                    prefs,
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID,
                    startRow,
                    pageSize);
            }
            catch (Exception)
            {
                //invalid search preferences, initialize an empty results list so the home page will render correctly.
                searchResults = new MatchnetQueryResults(0);
            }

            //set photo preference back to old value
            prefs.Add(HASPHOTOFLAG, photoPreference);

            List<ProfileHolder> profileList = new List<ProfileHolder>();
            ArrayList members = new ArrayList();
            if (searchResults != null && searchResults.ToArrayList().Count > 0)
            {
                members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(),
                         MemberLoadFlags.None);

                foreach (Member.ServiceAdapters.Member member in members)
                {
                    //add member
                    ProfileHolder ph = new ProfileHolder();
                    ph.Member = member;
                    ph.Ordinal = startRow + 1;
                    ph.HotlistCategory = HotListCategory.YourMatches;
                    ph.MyEntryPoint = BreadCrumbHelper.EntryPoint.HomePage;

                    profileList.Add(ph);

                    startRow++;
                }
            }

            if (profileList.Count > 0)
            {
                _hasMatches = true;
            }

            return profileList;

        }

        #endregion
    }
}