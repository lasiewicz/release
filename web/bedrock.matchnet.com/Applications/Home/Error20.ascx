﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Error20.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Error20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div id="page-container">
    <div class="error-page-container">
        <div class="error-page-person">
            <h1><mn:Txt runat="server" id="txtErrorPageTitle" ExpandImageTokens="True" ResourceConstant="TXT_ERROR_PAGE_TITLE" /></h1>
            <p class="editorial clear-both">
                <mn:txt runat="server" id="Txt2" ExpandImageTokens="true" ResourceConstant="TXT_ERROR_PAGE_CS_TEXT_ONE" /><br />
                <mn:txt runat="server" id="Txt1" ExpandImageTokens="true" ResourceConstant="TXT_ERROR_PAGE_CS_TEXT_TWO" />
                
                <mn:txt runat="server" id="TxtIPBlocked" ExpandImageTokens="true" ResourceConstant="TXT_ERROR_PAGE_IP_BLOCKED" Visible="false" />
                
            </p>
            <asp:LinkButton ID="btnVisitHome" href="/Default.aspx" runat="server" CssClass="link-primary float-inside clear-both">
                <mn:Txt runat="server" ID="txtRegister" ResourceConstant="TXT_VISIT_HOME_PAGE" />
            </asp:LinkButton>
        </div>
    </div>
</div>