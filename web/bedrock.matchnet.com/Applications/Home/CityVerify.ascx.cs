using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;

using Matchnet.Web.Framework;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;

namespace Matchnet.Web.Applications.Home
{
	/// <summary>
	///		Summary description for CityVerify.
	/// </summary>
	public class CityVerify : FrameworkControl
	{

		// TODO: rename this control RegionVerify, since it verifies cities and postalcodes

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				// first validate the region
				bool isValid = false;
			
				if( Request.QueryString["cityName"] != null && Request.QueryString["cityName"] != string.Empty &&
					Request.QueryString["parentRegionID"] != null && Request.QueryString["parentRegionID"] != string.Empty )
				{
					isValid = verifyCity( Conversion.CInt(Request.QueryString["parentRegionID"]), Request.QueryString["cityName"] );
				}
				else if( Request.QueryString["postalCode"] != null && Request.QueryString["postalCode"] != string.Empty &&
						Request.QueryString["countryRegionID"] != null && Request.QueryString["countryRegionID"] != string.Empty )
				{
					isValid = verifyPostalCode( Conversion.CInt(Request.QueryString["countryRegionID"]), Request.QueryString["postalCode"] );
				}

				// then create an xml doc to send the result back in
				XmlDocument xmlDoc = new XmlDocument();

				xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", "yes");

				XmlElement response = xmlDoc.CreateElement("response");
				xmlDoc.AppendChild(response);
				XmlNode root = xmlDoc.DocumentElement;
				XmlElement valid = xmlDoc.CreateElement("valid");
				valid.InnerText = isValid.ToString().ToLower();
				root.AppendChild(valid);
				Response.ContentType = "text/xml";

				// need to do clear and end on Response to remove the extra stuff bedrock puts in
				Response.Clear();
				xmlDoc.Save(Response.Output);
				Response.End();	
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private bool verifyCity( int parentRegionID, string cityName )
		{
			int cityID = RegionSA.Instance.FindRegionIdByCity( parentRegionID, cityName, g.Brand.Site.LanguageID ).ID;

			if( cityID != 0 )
			{
				return true;
			}

			return false;
		}

		private bool verifyPostalCode( int countryRegionID, string postalCode )
		{
			RegionID regionID = RegionSA.Instance.FindRegionIdByPostalCode(countryRegionID, postalCode);

			if ( regionID == null || regionID.ID == Constants.NULL_INT )
			{
				return false;
			}

			return true;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
