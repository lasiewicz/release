﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SparkHome50.ascx.cs" Inherits="Matchnet.Web.Applications.Home.SparkHome50" %>
<%@ Import Namespace="Matchnet.Web.Applications.Home" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc" TagName="MatchesSlider" Src="/Applications/Home/Controls/MatchesSlider20.ascx" %>
<%@ Register TagPrefix="uc" TagName="MatchesSliderYNM" Src="/Applications/Home/Controls/MatchesSliderYNM.ascx" %>
<%@ Register TagPrefix="uc" TagName="HotlistScroller" Src="/Applications/Home/Controls/HotlistScroller.ascx" %>
<%@ Register tagprefix="uc" tagname="Newsfeed" src="/Applications/UserNotifications/Controls/NewsfeedDouble.ascx"  %>
<%@ Register src="/Framework/Ui/ProfileStatusSummary.ascx" tagname="ProfileStatusSummary" tagprefix="uc" %>
<%@ Register src="/Applications/Home/Controls/HotListButton.ascx" tagname="HotListButton" tagprefix="uc" %>

<div id="page-container" class="spark-home-50">
    <div id="hotlistStats" class="hotlist-stats">
        <ul>
            <li class="profile"><%--Profile Activity--%> 
                <uc:ProfileStatusSummary runat="server" ID="ucProfileStatusSummary"/>
            </li>
            <li class="email clickable"><%--Emailed You--%>
                <uc:HotListButton runat="server" ID="hotListButtonEmailed"/>
            </li>
            <li class="flirt clickable"><%--Flirted with You--%>
                <uc:HotListButton runat="server" ID="hotListButtonFlirted"/>
            </li>
            <li class="im clickable"><%--Viewed You--%>
                 <uc:HotListButton runat="server" ID="hotListButtonViewed"/>
            </li>
            <li class="favorite clickable"><%--My Favorites--%>
                <uc:HotListButton runat="server" ID="hotListButtonFavorites"/>
            </li>
        </ul>
    </div>
    <script type="text/javascript">
        $j('#hotlistStats .clickable').on('click', function (e) {
            window.location = $j(this).find("a").first().attr("href");
        });
    </script>

    <div class="match-slider-20-wrap">
        <asp:PlaceHolder runat="server" ID="phMatchesSlider" Visible="False">
            <uc:MatchesSlider runat="server" ID="ucMatchesSlider" />
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="phMatchesSliderYNM" Visible="False">
            <uc:MatchesSliderYNM runat="server" ID="ucMatchesSliderYNM" />
        </asp:PlaceHolder>
    </div>
    <div class="communications">
        <ul id="homeTabs" class="tabs-primary">
            <li id="homeTabNewsfeed" class="active">
                <a href="#">
                    <mn:Txt runat="server" ID="txt0" ResourceConstant="TXT_YOUR_NEWSFEED"/> 
                    <asp:PlaceHolder runat="server" ID="phNewsfeedNotificationCount" Visible="False">
                        <em class="hint"><asp:Literal runat="server" ID="litNewsfeedNotificationCount"></asp:Literal></em>
                    </asp:PlaceHolder>
                </a>
            </li>
            <li id="homeTabVisitors">
                <a href="#">
                    <mn:Txt runat="server" ID="txt1" ResourceConstant="TXT_YOUR_VISITORS"/> 
                    <asp:PlaceHolder runat="server" ID="phYourVisitorsNotificationCount" Visible="False">
                        <em class="hint"><asp:Literal runat="server" ID="litYourVisitorsNotificationCount"></asp:Literal></em>
                    </asp:PlaceHolder>
                </a>
            </li>
            <li id="homeTabLists">
                <a href="#">
                    <mn:Txt runat="server" ID="txt2" ResourceConstant="TXT_YOUR_LISTS"/> 
                    <asp:PlaceHolder runat="server" ID="phYourListsNotificationCount" Visible="False">
                        <em class="hint"><asp:Literal runat="server" ID="litYourListNotificationCount"></asp:Literal></em>
                    </asp:PlaceHolder>
                </a>
            </li>
        </ul>
        <div class="content">
            <div id="homeTabContentNewsfeed" class="feeds">
                <uc:Newsfeed ID="newsfeedDoubleControl" runat="server"></uc:Newsfeed>
            </div>
            <div id="homeTabContentVisitors" style="display: none;" class="feeds">
                <asp:PlaceHolder runat="server" ID="phVisitorsScrollers">
                    <uc:HotlistScroller runat="server" ID="hotlistViewedYourProfile" HotlistCategory="WhoViewedYourProfile" />
                    <uc:HotlistScroller runat="server" ID="hotlistFlirtedWithYou" HotlistCategory="WhoTeasedYou" />
                    <uc:HotlistScroller runat="server" ID="hotlistEmailedYou" HotlistCategory="WhoEmailedYou" />
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="phVisitorsNoScrollerContent" Visible="false">
                    <%--Note: We want to initially re-use the no newsfeed copy on your visitors tab --%>
                    <blockquote><mn:txt ID="Txt3" ResourceConstant="TXT_NO_NEWSFEED_INTRO_COPY" runat="server" /></blockquote>
	                <ul class="no-newsfeed">
		                <li class="clearfix">
			                <a id="lnkNewsfeedMemberPhotoEdit" runat="server" href="/Applications/MemberProfile/MemberPhotoEdit.aspx" title="Upload a Photo" class="image">
                                <img src="/img/Community/JDate/ui-newsfeed-mood-img-01.jpg" alt="Upload a Photo" /></a>
			                <h3><a id="lnkNewsfeedMemberPhotoEdit2" runat="server" href="/Applications/MemberProfile/MemberPhotoEdit.aspx" title="Upload a Photo">
                                <mn:txt ID="Txt4" ResourceConstant="TXT_NO_NEWSFEED_HEADER_ONE" runat="server" /></a></h3>
			                <p><mn:txt ID="Txt5" ResourceConstant="TXT_NO_NEWSFEED_BODY_ONE" runat="server" /></p>
		                </li>
                        <li class="clearfix">
			                <a id="lnkNewsfeedMatchPreferences" runat="server" href="/Applications/Search/SearchResults.aspx" title="Set Your Match Preferences" class="image">
                                <img src="/img/Community/JDate/ui-newsfeed-mood-img-03.jpg" alt="Set Your Match Preferences" /></a>
			                <h3><a id="lnkNewsfeedMatchPreferences2" runat="server" href="/Applications/Search/SearchResults.aspx" title="Set Your Match Preferences">
                                <mn:txt ID="Txt8" ResourceConstant="TXT_NO_NEWSFEED_HEADER_THREE" runat="server" /></a></h3>
			                <p><mn:txt ID="Txt9" ResourceConstant="TXT_NO_NEWSFEED_BODY_THREE" runat="server" /></p>
		                </li>
		                <li class="clearfix">
			                <a id="lnkNewsfeedEditProfile" runat="server" href="/Applications/MemberProfile/ViewProfile.aspx" title="Write a Few Words" class="image">
                            <img src="/img/Community/JDate/ui-newsfeed-mood-img-02.jpg" alt="Write a Few Words" /></a>
			                <h3><a id="lnkNewsfeedEditProfile2" runat="server" href="/Applications/MemberProfile/ViewProfile.aspx" title="Write a Few Words">
                                <mn:txt ID="Txt6" ResourceConstant="TXT_NO_NEWSFEED_HEADER_TWO" runat="server" /></a></h3>
			                <p><mn:txt ID="Txt7" ResourceConstant="TXT_NO_NEWSFEED_BODY_TWO" runat="server" /></p>
		                </li>
		                <li class="clearfix">
			                <a id="lnkNewsfeedSearch" runat="server" href="/Applications/Search/SearchResults.aspx" title="Don't Be Shy" class="image">
                                <img src="/img/Community/JDate/ui-newsfeed-mood-img-04.jpg" alt="Don't Be Shy" /></a>
			                <h3><a id="lnkNewsfeedSearch2" runat="server" href="/Applications/Search/SearchResults.aspx" title="Don't Be Shy">
                                <mn:txt ID="Txt10" ResourceConstant="TXT_NO_NEWSFEED_HEADER_FOUR" runat="server" /></a></h3>
			                <p><mn:txt ID="Txt11" ResourceConstant="TXT_NO_NEWSFEED_BODY_FOUR" runat="server" /></p>
		                </li>
	                </ul>
                </asp:PlaceHolder>
            </div>
            <div id="homeTabContentLists" style="display: none;" class="feeds">
                <uc:HotlistScroller runat="server" ID="hotlistYourFavorites" HotlistCategory="Default" />
                <uc:HotlistScroller runat="server" ID="hotlistYouViewed" HotlistCategory="MembersYouViewed" />
                <div class="panel-scroll">
                    <h2>
                        <mn:Txt runat="server" ID="txtHotlistTitle" ResourceConstant="TXT_TITLE_SECRET_ADMIRER" TitleResourceConstant="TXT_TITLE_SECRET_ADMIRER"/> 
                        <asp:PlaceHolder runat="server" ID="phListYNMNotificationCount" Visible="False">
                            <em class="hint"><asp:Literal runat="server" ID="litListYNMNotificationCount"></asp:Literal></em>
                        </asp:PlaceHolder>
                    </h2>
                    <ul class="secret-admirer-list">
                        <%--Mutual list--%>
                        <li>
                            <span class="spr-b s-icon-b-secret-y-both icon">y</span>
                            <h3><mn:Txt ID="txtViewAllMutualTitle" ResourceConstant="TXT_YOU_EACH_SAID_YES" TitleResourceConstant="TXT_YOU_EACH_SAID_YES" runat="server" /></h3>
                            <mn:Txt ID="txtViewAllMutual" ResourceConstant="TXT_VIEW_HERE" TitleResourceConstant="TXT_VIEW_HERE_ALT" runat="server" />
                        </li>
                        <%--Yes list--%>
                        <li>
                            <span class="secret-y icon">y</span>
                            <h3><mn:Txt ID="txtViewAllYesTitle" ResourceConstant="TXT_YOU_SAID_YES" TitleResourceConstant="TXT_YOU_SAID_YES" runat="server" /></h3>
                            <mn:Txt ID="txtViewAllYes" ResourceConstant="TXT_VIEW_HERE" TitleResourceConstant="TXT_VIEW_HERE_ALT" runat="server" />
                        </li>
                        <%--Maybe list--%>
                        <li>
                            <span class="secret-m icon">m</span>
                            <h3><mn:Txt ID="txtViewAllMaybeTitle" ResourceConstant="TXT_YOU_SAID_MAYBE" TitleResourceConstant="TXT_YOU_SAID_MAYBE" runat="server" /></h3>
                            <mn:Txt ID="txtViewAllMaybe" ResourceConstant="TXT_VIEW_HERE" TitleResourceConstant="TXT_VIEW_HERE_ALT" runat="server" />
                        </li>
                    </ul>
                    
                    <mn:Txt ID="txtViewAllYNM" ResourceConstant="TXT_VIEW_ALL" TitleResourceConstant="TXT_VIEW_ALL_ALT" CssClass="view-more" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div class="aside">
        <ul>
            <li>
                <a href="/jmag/" title="read more"><img src="http://static.jdate.com/Components/jd-more-items/aside-01.jpg" alt="JMag" /></a>
                <h2><a href="/jmag/" title="JMag">JMag</a></h2>
                <p class="desc">As JDate's online magazine, JMag is where celebs, experts and JDaters<sup>&reg;</sup> come to kibitz about all things Jewish! <a href="/jmag/" title="read more" class="view-more">View More »</a></p>
                <ol>
                    <li><a href="/jmag/category/expertadvice/" title="Expert Advice">Expert Advice</a></li>
                    <li><a href="/jmag/category/forjdatersbyjdaters/" title="For JDaters by JDaters">For JDaters by JDaters</a></li>
                    <li><a href="/jmag/category/interviews/" title="Interviews">Interviews</a></li>
                </ol>
            </li>
            <li>
                <a href="/jmag/category/successstories/" title="read more"><img src="http://static.jdate.com/Components/jd-more-items/aside-02.jpg" alt="Success Stories" /></a>
                <h2><a href="/jmag/category/successstories/" title="Success Stories">Success Stories</a></h2>
                <p class="desc">Meet real JDate Success Stories and find out why we're responsible for more Jewish marriages than any other site! <a href="/jmag/category/successstories/" title="read more" class="view-more">View More »</a></p>
                <ol>
                    <li><a href="/jmag/category/successstories/" title="Success Stories">Success Stories</a></li>
                    <li><a href="/jmag/submitstory/" title="Submit Your Story">Submit Your Story</a></li>
                    <li><a href="/jmag/best-of/" title="Best Of">Best Of</a></li>
                </ol>
            </li>
            <li>
                <a href="/Applications/Events/Events.aspx" title="read more" id="lnkViewMoreEventsPhoto" runat="server"><img src="http://static.jdate.com/Components/jd-more-items/aside-03.jpg" alt="Travel & Events" /></a>
                <h2><a href="/Applications/Events/Events.aspx" title="read more" id="lnkViewMoreEventsHeader" runat="server">Travel & Events</a></h2>
                <p class="desc">Get together with JDaters in your area for mixers, classes, speed dating parties and more! <a href="/Applications/Events/Events.aspx" title="read more" class="view-more" runat="server" id="lnkViewMoreEvents">View More »</a></p>
                <ol>
                    <li><a href="/Applications/Events/Events.aspx" title="Travel Adventures" runat="server" id="lnkTravelAdventures">Travel Adventures</a></li>
                    <li><a href="/Applications/Events/Events.aspx" title="Parties &amp; Events" runat="server" id="lnkPartiesEvents">Parties &amp; Events</a></li>
                    <li><a href="http://www.hurrydate.com/index.cfm?fuseaction=landing.jdate&promotionid=922" title="Speed Dating" runat="server" id="lnkSpeedDating">Speed Dating</a></li>
                </ol>
            </li>
        </ul>
    </div>

    
<script type="text/javascript">
    adroll_adv_id = "JCRCKLJSYNGSBPBGKO3MK6";
    adroll_pix_id = "7TJ6IFAMERA27BDOPR5K4V";
    (function () {
        var oldonload = window.onload;
        window.onload = function(){
            __adroll_loaded=true;
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
            document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
            if(oldonload){oldonload()}};
    }());
 </script>

    <%--Push Flirt Overlay--%>
    <asp:PlaceHolder runat="server" ID="pushFlirts">
        <div class="clearfix stats-new">
            <div id="pushFlirtsContainer" class="hide">
                <div id="pushFlirtsContInner" class="padding-heavy"><!--filled via ajax --></div>
            </div><!-- end pushFlirtsContainer -->                
        </div>                
                    
    </asp:PlaceHolder>
    
</div>


<script type="text/javascript">
    var homeTabManager = {
        tabs: [],
        currentIndex: 0,
        previousINdex: 0,
        initializingTab: true,
        memberIsSub: <%=_memberIsSub.ToString().ToLower()%>,
        viewDate: '<%=DateTime.Now.ToString()%>',
        updateInProgress: false,
        tabDefaultIndex: <%=HomeUtil.HOME_TABS_DEFAULT_INDEX%>,
        tabCookieName: '<%=HomeUtil.HOME_TABS_COOKIE_NAME%>',
        onTabClick: function (selectedIndex) {
            homeTabManager.previousIndex = homeTabManager.currentIndex;
            homeTabManager.currentIndex = selectedIndex;

            var selectedTab = homeTabManager.tabs[selectedIndex];
            if (selectedTab != null && (selectedIndex != homeTabManager.previousIndex)) {
                //update previous tab
                var previousTab = homeTabManager.tabs[homeTabManager.previousIndex];
                $j('#' + previousTab.tabID).removeClass('active');
                $j('#' + previousTab.tabContentID).hide();

                //update selected tab
                $j('#' + selectedTab.tabID).removeClass('active').addClass('active');
                $j('#' + selectedTab.tabContentID).show();

                if (!selectedTab.isScrollInitialized) {
                    if (jQuery.fn.jScrollPane) {
                        // check if jquery scroll plugin is being used then reinitialise scroll panel
                        $j('#' + selectedTab.tabContentID).find('.scroll')
                            .each(function (index) {
                                var paneApi = $j(this).data('jsp'); // grab current settings
                                //console.log('paneApi', index, ': ', paneApi);
                                paneApi.reinitialise();
                            });
                    }
                    selectedTab.isScrollInitialized = true;
                }

                //mark content as viewed
                if (!homeTabManager.initializingTab && !selectedTab.markedAsViewed) {
                    if (selectedTab.isHotlist) {
                        homeTabManager.markHotlistAsViewed(selectedTab);
                    }
                    else if (selectedTab.isNewsFeed) {
                        homeTabManager.markNewsfeedAsViewed(selectedTab);
                    }
                }
                
                if (!homeTabManager.initializingTab)
                {
                    //update omniture
                    if (s != null) {
                        PopulateS(true); //clear existing values in omniture "s" object
                        var originalPageName = s.pageName;
                        s.pageName = s.pageName + " - " + selectedTab.omnitureTabName;
                        s.t();
                        s.pageName = originalPageName;
                    }
                }
                homeTabManager.addTabIndexToCookie(selectedIndex);
            }

            homeTabManager.initializingTab = false;
        },
        addTabIndexToCookie: function (indexValue) {
            var expires = new Date(new Date().getTime() + 365 * 24 * 60 * 60 * 1000);
            spark_setCookie(homeTabManager.tabCookieName, indexValue, expires, "/", null, false);
        },
        getTabIndexFromCookie: function () {
            var tabIndex = spark_getCookie(homeTabManager.tabCookieName);
            if (tabIndex) {
                if (tabIndex >= homeTabManager.tabs.length || tabIndex < 0) {
                    tabIndex = homeTabManager.tabDefaultIndex;
                }
            } else {
                tabIndex = homeTabManager.tabDefaultIndex;
            }

            return tabIndex;
        },
        markNewsfeedAsViewed: function (selectedTab) {

            if (!selectedTab.markedAsViewed && !homeTabManager.updateInProgress && selectedTab.isNewsFeed) {
                $j.ajax({
                    type: "POST",
                    url: "/Applications/API/NewsfeedAPI.asmx/MarkNewsfeedAsViewed",
                    data: "{clearDate:'" + homeTabManager.viewDate + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        homeTabManager.updateInProgress = true;
                    },
                    complete: function () {
                        homeTabManager.updateInProgress = false;
                    },
                    success: function (result) {
                        var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                        if (updateResult.Status != 2) {
                            try {
                                console.log('Error updating newsfeed tab as viewed');
                            } catch (e) {
                            }
                        } else {
                            selectedTab.markedAsViewed = true;
                            try {
                                console.log('Updated newsfeed tab as viewed');
                            } catch (e) {
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        try {
                            console.log('Error updating newsfeed tab as viewed: ' + textStatus);
                        } catch (e) {
                        }
                    }
                });
            }
        },
        markHotlistAsViewed: function (selectedTab) {

            if (!selectedTab.markedAsViewed && !homeTabManager.updateInProgress && selectedTab.isHotlist) {
                $j.ajax({
                    type: "POST",
                    url: "/Applications/API/Hotlist.asmx/MarkHotListLastViewed",
                    data: "{clearDate:'" + homeTabManager.viewDate + "', hotlistCategoryCommaDelimited: '" + selectedTab.hotlistCategoryCommaDelimited + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        homeTabManager.updateInProgress = true;
                    },
                    complete: function () {
                        homeTabManager.updateInProgress = false;
                    },
                    success: function (result) {
                        var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                        if (updateResult.Status != 2) {
                            try {
                                console.log('Error updating hotlist tab as viewed');
                            } catch (e) {
                            }
                        } else {
                            selectedTab.markedAsViewed = true;
                            try {
                                console.log('Updated hotlist tab as viewed');
                            } catch (e) {
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        try {
                            console.log('Error updating hotlist tab as viewed: ' + textStatus);
                        } catch (e) {
                        }
                    }
                });
            }
        },
        getNewTab: function () {
            return {
                tabID: '',
                tabContentID: '',
                isScrollInitialized: false,
                markedAsViewed: false,
                isNewsFeed: false,
                isHotlist: false,
                hotlistCategoryCommaDelimited: '',
                omnitureTabName: ''
            };
        },
        initialize: function () {
            $j('#homeTabs').on("click", "a", function (e) {
                e.preventDefault();
            });

            //add tab: newsfeed
            var tabNewsFeed = homeTabManager.getNewTab();
            tabNewsFeed.tabID = 'homeTabNewsfeed';
            tabNewsFeed.tabContentID = 'homeTabContentNewsfeed';
            tabNewsFeed.isNewsFeed = true;
            tabNewsFeed.omnitureTabName = '<%=HomeUtil.HomeTabOmnitureName[0]%>';
            homeTabManager.tabs.push(tabNewsFeed);
            $j('#homeTabNewsfeed').bind('click', function () {
                homeTabManager.onTabClick(0);
            });

            //add tab: visitors
            var tabVisitors = homeTabManager.getNewTab();
            tabVisitors.tabID = 'homeTabVisitors';
            tabVisitors.tabContentID = 'homeTabContentVisitors';
            tabVisitors.isHotlist = true;
            tabVisitors.omnitureTabName = '<%=HomeUtil.HomeTabOmnitureName[1]%>';
            if (homeTabManager.memberIsSub) {
                tabVisitors.hotlistCategoryCommaDelimited = '-1,-3,-4';
            } else {
                tabVisitors.hotlistCategoryCommaDelimited = '-1';
            }
            homeTabManager.tabs.push(tabVisitors);
            $j('#homeTabVisitors').bind('click', function () {
                homeTabManager.onTabClick(1);
            });

            //add tab: your lists
            var tabYourLists = homeTabManager.getNewTab();
            tabYourLists.tabID = 'homeTabLists';
            tabYourLists.tabContentID = 'homeTabContentLists';
            tabYourLists.isHotlist = true;
            tabYourLists.omnitureTabName = '<%=HomeUtil.HomeTabOmnitureName[2]%>';
            tabYourLists.hotlistCategoryCommaDelimited = '0,-9,-15,-20,-21';
            homeTabManager.tabs.push(tabYourLists);
            $j('#homeTabLists').bind('click', function () {
                homeTabManager.onTabClick(2);
            });

            //set initial tab
            homeTabManager.onTabClick(homeTabManager.getTabIndexFromCookie());

        }
    };

    $j(function () {
        homeTabManager.initialize();
    });

</script>

<asp:PlaceHolder ID="phPushFlirtsJS" runat="server" Visible="false">
<script type="text/javascript">
    __addToNamespace__('spark.pushFlirts', {
        sendAmount: '0',
        dialogTitle: '<%=g.GetResource("TXT_FLIRT_MODAL_TITLE",this) %>',
        messageNoneSelected: '<%=g.GetResource("TXT_FLIRT_MESSAGE_NONE_SELECTED",this) %>',
        messageErrorGeneral: '<%=g.GetResource("TXT_FLIRT_MESSAGE_UNKNOWN_ERROR",this) %>'
    });

    $j(spark.pushFlirts).bind("pushFlirtsRefresh", function (event) {
        $j.ajax({
            type: "POST",
            url: "/Applications/API/Flirts.asmx/GetPushFlirts",
            contentType: "application/json; charset=ISO-8859-1",
            dataType: "json",
            beforeSend: function () {
                $j('#pushFlirtsContInner').html(spark.util.ajaxLoader);
            },
            success: function (response) {
                $j('#pushFlirtsContInner').html(response.d).find('#pushFlirtsSend').bind('click', function (event) {
                    $j(spark.pushFlirts).trigger("pushFlirtsValidate", $j('#pushFlirtsContainer'));
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $j(spark).trigger("jqueryUIDialogMessaging", [spark.pushFlirts.messageErrorGeneral, "notification error"]);
            }
        });
    });

    $j(spark.pushFlirts).bind("pushFlirtsValidate", function (event, obj) {
        var $checkBoxes = $j(obj).find('input[type=checkbox]'),
            $checkedCheckboxes = $j(obj).find('input[type=checkbox]:checked');

        $checkBoxes.bind('change', function () {
            $j('#pushFlirtsButtonContainer').unblock();
        });

        // set number of selected members to flirt with
        spark.pushFlirts.sendAmount = $checkedCheckboxes.length;

        if ($checkedCheckboxes.length) {
            $j(spark.pushFlirts).trigger("pushFlirtsSubmit");
        }
        else {
            $j('#pushFlirtsButtonContainer').block({
                message: spark.pushFlirts.messageNoneSelected,
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.9
                },
                css: {
                    width: 'auto',
                    top: '0',
                    left: '0px',
                    backgroundColor: null,
                    border: null,
                    padding: null,
                    color: null,
                    margin: null,
                    textAlign: null
                }
            }).find('.blockMsg').addClass('notification');
        }
    });

    $j(spark.pushFlirts).bind("pushFlirtsSubmit", function (event) {

        var targetMembersString = "";
        var count = 0;

        $j('#pushFlirtsContainer').find('input[type=checkbox]:checked').each(function () {
            if (targetMembersString == "") {
                targetMembersString = $j(this).data('memberid');
            }
            else {
                targetMembersString = targetMembersString + "," + $j(this).data('memberid');
            }
            count++;
        });

        sendPushFlirtsOmniture(count, targetMembersString);

        $j.ajax({
            type: "POST",
            url: "/Applications/API/Flirts.asmx/ProcessPushFlirts",
            data: "{ 'memberIDs': '" + targetMembersString + "'}",
            contentType: "application/json; charset=ISO-8859-1",
            dataType: "json",
            beforeSend: function () {
                $j('#pushFlirtsContInner').html(spark.util.ajaxLoader);
            },
            success: function (response) {
                $j('#pushFlirtsContInner').html(response.d);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $j(spark).trigger("jqueryUIDialogMessaging", [spark.pushFlirts.messageErrorGeneral, "notification error"]);

            }
        });
    });

    /* bind feature specific custom behaviors */
    $j(spark.pushFlirts).bind("pushFlirtsDialog", function (event, title) {

        if ($j('#pushFlirtsContainer').length === 0) {
            $j('<div id="pushFlirtsContainer" class="hide"></div>').appendTo('body');
        }

        $j('#pushFlirtsContainer')
                    .dialog({
                        width: 669,
                        modal: true,
                        title: spark.pushFlirts.dialogTitle,
                        position: ['center', 'top'],
                        dialogClass: 'modal-push-flirts',
                        minHeight: 400,
                        open: function () {
                            var $this = $j(this),
                                ajax_load = '<div class="loading spinner-only" />';
                            $j(spark.pushFlirts).trigger('pushFlirtsRefresh');
                        },
                        close: function () {
                            $j('#pushFlirtsContainer').find('.notification, .conf-flirt').remove();
                        }
                    })
                    .parent('.ui-dialog').css('top', function () {
                        var x = $j(this).position();
                        return x.top + 12;
                    });
    });

    function sendPushFlirtsOmniture(numberSent, targetMembersString) {

        var sentString = numberSent + " flirts sent";
        var s = s_gi(s_account);
        PopulateS(true);
        s.prop48 = sentString;
        s.events = 'event41,event5,event2';
        s.t(true);

        var i = 0;
        for (i = 0; i < numberSent; i++) {
            s = s_gi(s_account);
            s.prop48 = '';
            s.linkTrackVars = 'events';
            s.linkTrackEvents = 'event11';
            s.events = 'event11';
            s.eVar26 = "Tease";
            s.prop35 = targetMembersString;
            s.t(true);
        }
    }

    $j(function () {
        //lazy load push flirts
        $j(spark.pushFlirts).trigger("pushFlirtsDialog");
    });
</script>
</asp:PlaceHolder>

