﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Managers;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.MemberSlideshow.ServiceAdapters;
using Spark.SAL;
using Matchnet.Web.Analytics;

namespace Matchnet.Web.Applications.Home
{
    public partial class SlideshowProfileAPI : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SlideshowDisplayType displayType = (SlideshowDisplayType)Conversion.CInt(Request.QueryString[WebConstants.URL_PARAMETER_SLIDESHOW_DISPLAY_TYPE]);
            ucSlideshowProfile.Visible = (displayType == SlideshowDisplayType.Full || displayType == SlideshowDisplayType.ModalPopup);
            ucMiniSlideshowProfile.Visible = (displayType == SlideshowDisplayType.Mini);

            MemberSlideshowManager manager = new MemberSlideshowManager(g.Member.MemberID, g.Brand);

            if (Request.QueryString.AllKeys.Contains("ynmp") && Request.QueryString.AllKeys.Contains("YNMLT"))
            {
                SaveYNMVote();
            }

            bool advanceProfile = false;

            if (Request.QueryString.AllKeys.Contains(WebConstants.URL_PARAMETER_MINISLIDESHOW_MINAGE)
                && Request.QueryString.AllKeys.Contains(WebConstants.URL_PARAMETER_MINISLIDESHOW_MAXAGE)
                && Request.QueryString.AllKeys.Contains(WebConstants.URL_PARAMETER_MINISLIDESHOW_REGIONID))
            {
                int minAge = Conversion.CInt(Request.QueryString[WebConstants.URL_PARAMETER_MINISLIDESHOW_MINAGE]);
                int maxAge = Conversion.CInt(Request.QueryString[WebConstants.URL_PARAMETER_MINISLIDESHOW_MAXAGE]);
                
                int regionID = Request.QueryString[WebConstants.URL_PARAMETER_MINISLIDESHOW_REGIONID] == null ?
                        Constants.NULL_INT : Conversion.CInt(Request.QueryString[WebConstants.URL_PARAMETER_MINISLIDESHOW_REGIONID]);

                SearchType searchType = Request.QueryString[WebConstants.URL_PARAMETER_MINISLIDESHOW_SEARCHTYPE] == null ?
                    SearchType.Region : (SearchType)Conversion.CInt(Request.QueryString[WebConstants.URL_PARAMETER_MINISLIDESHOW_SEARCHTYPE]);

                int areaCode1 = Constants.NULL_INT;
                int areaCode2 = Constants.NULL_INT;
                int areaCode3 = Constants.NULL_INT;
                int areaCode4 = Constants.NULL_INT;
                int areaCode5 = Constants.NULL_INT;
                int areaCode6 = Constants.NULL_INT;

                if (Request.QueryString.AllKeys.Contains(WebConstants.URL_PARAMETER_MINISLIDESHOW_AREACODE_LIST) && !string.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_MINISLIDESHOW_AREACODE_LIST]))
                {
                    string[] arrAreaCodes = Request.QueryString[WebConstants.URL_PARAMETER_MINISLIDESHOW_AREACODE_LIST].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    if(arrAreaCodes.Length >0)
                    {
                        areaCode1 = Conversion.CInt(arrAreaCodes[0]);
                    }
                    if (arrAreaCodes.Length > 1)
                    {
                        areaCode2 = Conversion.CInt(arrAreaCodes[1]);
                    }
                    if (arrAreaCodes.Length > 2)
                    {
                        areaCode3 = Conversion.CInt(arrAreaCodes[2]);
                    }
                    if (arrAreaCodes.Length > 3)
                    {
                        areaCode4 = Conversion.CInt(arrAreaCodes[3]);
                    }
                    if (arrAreaCodes.Length > 4)
                    {
                        areaCode5 = Conversion.CInt(arrAreaCodes[4]);
                    }
                    if (arrAreaCodes.Length > 5)
                    {
                        areaCode6 = Conversion.CInt(arrAreaCodes[5]);
                    }
                }
                

                if (minAge != Constants.NULL_INT && maxAge != Constants.NULL_INT && regionID != Constants.NULL_INT
                    && minAge >= MemberSlideshowManager.MIN_AGE_LIMIT && maxAge <= MemberSlideshowManager.MAX_AGE_LIMIT)
                {
                    //only change preferences and advance slideshow if new prefs are valid

                    manager.SaveConfigurableSlideshowSettings(minAge, maxAge, regionID, searchType, areaCode1, areaCode2, areaCode3, areaCode4, areaCode5, areaCode6);
                    manager.RemoveSlideshowFromCache(g.Member.MemberID, g.Brand.Site.Community.CommunityID);
                    advanceProfile = true;
                }
            }
            
            if (Request.QueryString.AllKeys.Contains("asl"))
            {
                advanceProfile = Conversion.CBool(Request["asl"]);
            }
            if (Request.QueryString.AllKeys.Contains("omniturepage"))
            {
                string omniturePage = "";
                omniturePage = Request.QueryString["omniturepage"];

                //When the slide show preferences are saved, g.AnalyticsOmniture becomes null when the API is called.
                if (g.AnalyticsOmniture == null)
                {
                    if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
                    {
                        Matchnet.Web.Analytics.Omniture omniture = (Matchnet.Web.Analytics.Omniture)Page.LoadControl("/Analytics/Omniture.ascx");
                        g.AnalyticsOmniture = omniture;
                        g.AnalyticsOmniture.PageName = omniturePage;
                    }
                }
            }

            bool enableConfiguration = manager.IsConfigurableSlideshowEnabled();
            
            if(displayType == SlideshowDisplayType.Full || displayType == SlideshowDisplayType.ModalPopup)
            {
                ucSlideshowProfile.SlideshowDisplayType = displayType;
                ucSlideshowProfile.ReloadAds = true;
                
                if (!String.IsNullOrEmpty(Request.QueryString["displayedRight"]))
                {
                    ucSlideshowProfile.DisplayedInRightControl = Conversion.CBool(Request.QueryString["displayedRight"]);
                }
                if (Request.QueryString["slideshowmode"] == SlideShowModeType.SecretAdmirerGame.ToString("d"))
                {
                    ucSlideshowProfile.SlideShowMode = SlideShowModeType.SecretAdmirerGame;
                    ucSlideshowProfile.PopulateYNMBuckets = true;
                }
                else
                {
                    ucSlideshowProfile.PopulateYNMBuckets = false;
                }

                if (!String.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_SLIDESHOW_OUTER_CONTAINER_DIV]))
                {
                    ucSlideshowProfile.SlideshowOuterContainerDivID =Request.QueryString[WebConstants.URL_PARAMETER_SLIDESHOW_OUTER_CONTAINER_DIV];
                }

                if (!String.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_SLIDESHOW_OMNITURE_PAGE_NAME]))
                {
                    ucSlideshowProfile.OmniturePageName = Request.QueryString[WebConstants.URL_PARAMETER_SLIDESHOW_OMNITURE_PAGE_NAME];
                }

                ucSlideshowProfile.Load(advanceProfile);
                if (enableConfiguration && displayType != SlideshowDisplayType.ModalPopup)
                {
                    ucSlideshowProfile.LoadPreferences();
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_SLIDESHOW_OUTER_CONTAINER_DIV]))
                {
                    ucMiniSlideshowProfile.SlideshowOuterContainerDivID = Request.QueryString[WebConstants.URL_PARAMETER_SLIDESHOW_OUTER_CONTAINER_DIV];
                }

                if (!String.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_SLIDESHOW_OMNITURE_PAGE_NAME]))
                {
                    ucMiniSlideshowProfile.OmniturePageName = Request.QueryString[WebConstants.URL_PARAMETER_SLIDESHOW_OMNITURE_PAGE_NAME];
                }

                if (!String.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_SLIDESHOW_SHOW_UPDATED_WIDE_STYLE]))
                {
                    ucMiniSlideshowProfile.ShowUpdatedWideStyle = true;
                }

                ucMiniSlideshowProfile.Load(advanceProfile);
                if (enableConfiguration)
                {
                    ucMiniSlideshowProfile.LoadPreferences();
                }
            }

        }

        public void SaveYNMVote()
        {
            YNMVoteParameters parameters = new YNMVoteParameters();

            parameters.DecodeParams(Request["ynmp"].Replace(" ", "+"));   //.Net replaces "+" in the URL param with " ".  We need to undo this so that we get the original encrypted string back.
            int voteType = Conversion.CInt(Request["YNMLT"]);
            
            if (parameters.FromMemberID > 0)
            {
                ListManager listManager = new ListManager();
                listManager.YNMVote(voteType, parameters.FromMemberID, parameters.ToMemberID, g.Brand, g);
            }
        }
    }
}
