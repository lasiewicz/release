using System;
using System.Drawing.Imaging;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using System.Drawing;

namespace Matchnet.Web.Applications.Home
{
	public class JpegImage: FrameworkControl
	{
		private void Page_Load(object sender, EventArgs e)
		{
			Random random = new Random();
			int randomFont = random.Next(0, 2);
			string[] fontNames = new string[] {"Times New Roman", "Arial", "Verdana"};
			
			string fontName = fontNames[randomFont];
			string captchaText;
			
			//get and decrypt the text
			if (g.Session.Get("CAPTCHAText") == null)
			{
				//throw new Exception("CAPTCHA text has not been set in the session.");
				
				//HACK: for some reason when someone lands from HTTPS addresses the session does
				//not get set
				//generate and encrypt the text
				captchaText = RandomTextGenerator.Generate(4,7);
				g.Session.Add("CAPTCHAText", captchaText, SessionPropertyLifetime.Temporary);
				
			}
			else
			{
				captchaText = (string)g.Session.Get("CAPTCHAText");				
			}
			
			// Create a CAPTCHA image using the text stored in the Session object.
            CaptchaImage ci;
            if(!String.IsNullOrEmpty(Request["onePageReg"]))
            {
                ci = new CaptchaImage(captchaText, 200, 50, fontName,Color.Black);
            }
            else{
                ci = new CaptchaImage(captchaText, 200, 50, fontName);
            }
			

			// Change the response headers to output a JPEG image.
			this.Response.Clear();
			this.Response.ContentType = "image/jpeg";

			// Write the image to the response stream in JPEG format.
			ci.Image.Save(this.Response.OutputStream, ImageFormat.Jpeg);

			// Dispose of the CAPTCHA image object.
			ci.Dispose();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
