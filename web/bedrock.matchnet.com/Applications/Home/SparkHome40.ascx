﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SparkHome40.ascx.cs" Inherits="Matchnet.Web.Applications.Home.SparkHome40" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register tagprefix="uc" tagname="Newsfeed" src="~/Applications/UserNotifications/Controls/Newsfeed.ascx"  %>
<%@ Register TagPrefix="uc" TagName="SparkMore" Src="/Applications/Home/Controls/SparkMore.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MiniSlideshowProfile" Src="/Framework/Ui/BasicElements/MiniSlideshowProfile.ascx" %>

<div id="page-container" class="spark-home-40">
    <div id="slideshowOuter">
        <div id="slideshow" class="slideshow block-on-load">
            <uc1:MiniSlideshowProfile id="ucMiniSlideshowProfile" runat="server"></uc1:MiniSlideshowProfile>
        </div>
    </div>

    <%--Connect buttons--%>
    <%--<h2 class="component-header"><mn:txt ID="Txt10" runat="server" ResourceConstant="TXT_CONNECT_HEADER" /></h2>--%>
    <ul class="connect-members vertical">
        <li class="photo"><asp:HyperLink runat="server" id="lnkNewMemberPhotos" NavigateUrl="/Applications/PhotoGallery/PhotoGallery.aspx" CssClass="grad-emboss clearfix" > <span class="spr-b s-icon-b-photo"></span><span><mn:txt ID="txtNewMemberPhotos" runat="server" ResourceConstant="TXT_CONNECT_NEW_MEMBER_PHOTOS" /></span></asp:HyperLink></li>
        <li class="online"><asp:HyperLink ID="lnkMemberOnline" runat="server" NavigateUrl="/Applications/MembersOnline/MembersOnline.aspx" CssClass="grad-emboss clearfix"><span class="spr-b s-icon-b-online"></span><span><mn:txt ID="txtMemberOnline" runat="server" ResourceConstant="TXT_CONNECT_MEMBERS_ONLINE" /></span></asp:HyperLink></li>
        <li class="kibitz"><asp:HyperLink ID="lnkKibitz" runat="server" NavigateUrl="/Applications/QuestionAnswer/Home.aspx" CssClass="grad-emboss clearfix"><span class="spr-b s-icon-b-kibitz"></span><span><mn:txt ID="txtKibitz" runat="server" ResourceConstant="TXT_CONNECT_KIBITZ_CORNER" /></span></asp:HyperLink></li>
    </ul>

    <%--Newsfeed--%>
    <h2 class="component-header"><mn:txt ID="Txt1" runat="server" ResourceConstant="TXT_NEWSFEED_HEADER" /></h2>
    <div id="newsfeedContainer" class="newsfeed-container clearfix" runat="server">
        <uc:Newsfeed runat="server" ID="ucNewsfeed" />
        <div id="moreNewsfeedContainer"></div>
        <p id="noMoreNewsfeedResults" class="no-more-message"><span class="spr s-icon-page-message"></span> <mn:txt ID="txtNoMoreNewsfeedResults" runat="server" resourceconstant="TXT_NO_MORE_NEWSFEED_RESULTS" /></p>
        <a href="#" id="lnkMoreNewsfeed" class="more-link"><span class="spr s-icon-arrow-down-color"></span> <mn:txt runat="server" id="txtMoreNewsfeed" resourceConstant="TXT_VIEW_MORE_NEWSFEED_ITEMS" /></a>
    </div>

    <div id="noNewsfeedContainer" class="no-newsfeed-container clearfix" runat="server">
	    <em><mn:txt ResourceConstant="TXT_NO_NEWSFEED_INTRO_COPY" runat="server" /></em>
	    <ul>
		    <li class="clearfix">
			    <a href="/Applications/MemberProfile/MemberPhotoEdit.aspx?NavPoint=sub" title="Upload a Photo">
                    <img src="/img/Community/JDate/ui-newsfeed-mood-img-01.jpg" alt="Upload a Photo" /></a>
			    <h3><a href="/Applications/MemberProfile/MemberPhotoEdit.aspx?NavPoint=sub" title="Upload a Photo">
                    <mn:txt ID="Txt2" ResourceConstant="TXT_NO_NEWSFEED_HEADER_ONE" runat="server" /></a></h3>
			    <p><mn:txt ID="Txt6" ResourceConstant="TXT_NO_NEWSFEED_BODY_ONE" runat="server" /></p>
		    </li>
		    <li class="clearfix">
			    <a href="/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit&NavPoint=sub" title="Write a Few Words">
                <img src="/img/Community/JDate/ui-newsfeed-mood-img-02.jpg" alt="Write a Few Words" /></a>
			    <h3><a href="/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit&NavPoint=sub" title="Write a Few Words">
                    <mn:txt ID="Txt3" ResourceConstant="TXT_NO_NEWSFEED_HEADER_TWO" runat="server" /></a></h3>
			    <p><mn:txt ID="Txt7" ResourceConstant="TXT_NO_NEWSFEED_BODY_TWO" runat="server" /></p>
		    </li>
		    <li class="clearfix">
			    <a href="/Applications/Search/SearchResults.aspx?NavPoint=top" title="Set Your Match Preferences">
                    <img src="/img/Community/JDate/ui-newsfeed-mood-img-03.jpg" alt="Set Your Match Preferences" /></a>
			    <h3><a href="/Applications/Search/SearchResults.aspx?NavPoint=top" title="Set Your Match Preferences">
                    <mn:txt ID="Txt4" ResourceConstant="TXT_NO_NEWSFEED_HEADER_THREE" runat="server" /></a></h3>
			    <p><mn:txt ID="Txt8" ResourceConstant="TXT_NO_NEWSFEED_BODY_THREE" runat="server" /></p>
		    </li>
		    <li class="clearfix">
			    <a href="/Applications/Search/SearchResults.aspx?NavPoint=top" title="Don't Be Shy">
                    <img src="/img/Community/JDate/ui-newsfeed-mood-img-04.jpg" alt="Don't Be Shy" /></a>
			    <h3><a href="/Applications/Search/SearchResults.aspx?NavPoint=top" title="Don't Be Shy">
                    <mn:txt ID="Txt5" ResourceConstant="TXT_NO_NEWSFEED_HEADER_FOUR" runat="server" /></a></h3>
			    <p><mn:txt ID="Txt9" ResourceConstant="TXT_NO_NEWSFEED_BODY_FOUR" runat="server" /></p>
		    </li>
	    </ul>
    </div>

    <uc:SparkMore runat="server" ID="ucSparkMore" />

    <%--Push Flirt Overlay--%>
    <asp:PlaceHolder runat="server" ID="pushFlirts">
        <div class="clearfix stats-new">
            <div id="pushFlirtsContainer" class="hide">
                <div id="pushFlirtsContInner" class="padding-heavy"><!--filled via ajax --></div>
            </div><!-- end pushFlirtsContainer -->                
        </div>                
                    
    </asp:PlaceHolder>
    
<script type="text/javascript">
    adroll_adv_id = "JCRCKLJSYNGSBPBGKO3MK6";
    adroll_pix_id = "7TJ6IFAMERA27BDOPR5K4V";
    (function () {
        var oldonload = window.onload;
        window.onload = function () {
            __adroll_loaded = true;
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
            document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
            if (oldonload) { oldonload() }
        };
    }());
 </script>
    <!--iframe used by SaveYNMVote function-->
    <iframe name="FrameYNMVote" tabindex="-1" frameborder="0" scrolling="no" width="1" height="1">
	    <layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
    </iframe>
</div>


<script type="text/javascript">
(function () {
    var pageSize = 6,
    currentPage = 1,
    $newsfeedContainer = $j('div[id$="newsfeedContainer"]'),
    $moreNewsfeedContainer = $j('#moreNewsfeedContainer'),
    $noMoreMessage = $j('#noMoreNewsfeedResults'),
    $moreLink = $j('#lnkMoreNewsfeed');

    function populateAdditionalNewsfeedItems() {
        var newStartNum = pageSize * currentPage;
        currentPage++;

        $j.ajax({
            type: "POST",
            url: "/Applications/API/NewsfeedAPI.asmx/LoadNewsfeed",
            data: "{startNum:'" + newStartNum + "', pageSize: '" + pageSize + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",

            beforeSend: function () {
                $newsfeedContainer.prepend(spark.util.ajaxLoader);
            },
            success: function (result) {
                if (result.d.HasItems == true) {
                    //add content from result.NewsfeedHTML into newsfeedDiv
                    $moreNewsfeedContainer.append(result.d.NewsfeedHTML);
                    $newsfeedContainer.find('.loading').remove();

                    //pagination omniture
                    if (s != null) {
                        PopulateS(true); //clear existing values in omniture "s" object

                        //As an omniture "link" which does not redirect the page, we set a temporary pageName attribute
                        var originalPageName = s.pageName;
                        s.pageName = s.pageName + " - Newsfeed - bottom";
                        s.eVar25 = "" + currentPage + "";
                        s.t();
                        s.pageName = originalPageName;
                    }
                }
                else {
                    //display no more data message in div
                    //also, disable more button
                    $newsfeedContainer.find('.loading').remove();
                    $noMoreMessage.show();
                    $moreLink.hide();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //galleryModalFavoritesError
                console.log(errorThrown);
            }

        });

    } //populateAdditionalNewsfeedItems

    $moreLink.click(function (e) {
        e.preventDefault();
        populateAdditionalNewsfeedItems();
    });

})();
</script>

<asp:PlaceHolder ID="phPushFlirtsJS" runat="server" Visible="false">
<script type="text/javascript">
    __addToNamespace__('spark.pushFlirts', {
        sendAmount: '0',
        dialogTitle: '<%=g.GetResource("TXT_FLIRT_MODAL_TITLE",this) %>',
        messageNoneSelected: '<%=g.GetResource("TXT_FLIRT_MESSAGE_NONE_SELECTED",this) %>',
        messageErrorGeneral: '<%=g.GetResource("TXT_FLIRT_MESSAGE_UNKNOWN_ERROR",this) %>'
    });

    $j(spark.pushFlirts).bind("pushFlirtsRefresh", function (event) {
        $j.ajax({
            type: "POST",
            url: "/Applications/API/Flirts.asmx/GetPushFlirts",
            contentType: "application/json; charset=ISO-8859-1",
            dataType: "json",
            beforeSend: function () {
                $j('#pushFlirtsContInner').html(spark.util.ajaxLoader);
            },
            success: function (response) {
                $j('#pushFlirtsContInner').html(response.d).find('#pushFlirtsSend').bind('click', function (event) {
                    $j(spark.pushFlirts).trigger("pushFlirtsValidate", $j('#pushFlirtsContainer'));
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $j(spark).trigger("jqueryUIDialogMessaging", [spark.pushFlirts.messageErrorGeneral, "notification error"]);
            }
        });
    });

    $j(spark.pushFlirts).bind("pushFlirtsValidate", function (event, obj) {
        var $checkBoxes = $j(obj).find('input[type=checkbox]'),
            $checkedCheckboxes = $j(obj).find('input[type=checkbox]:checked');

        $checkBoxes.bind('change', function () {
            $j('#pushFlirtsButtonContainer').unblock();
        });

        // set number of selected members to flirt with
        spark.pushFlirts.sendAmount = $checkedCheckboxes.length;

        if ($checkedCheckboxes.length) {
            $j(spark.pushFlirts).trigger("pushFlirtsSubmit");
        }
        else {
            $j('#pushFlirtsButtonContainer').block({
                message: spark.pushFlirts.messageNoneSelected,
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.9
                },
                css: {
                    width: 'auto',
                    top: '0',
                    left: '0px',
                    backgroundColor: null,
                    border: null,
                    padding: null,
                    color: null,
                    margin: null,
                    textAlign: null
                }
            }).find('.blockMsg').addClass('notification');
        }
    });

    $j(spark.pushFlirts).bind("pushFlirtsSubmit", function (event) {

        var targetMembersString = "";
        var count = 0;

        $j('#pushFlirtsContainer').find('input[type=checkbox]:checked').each(function () {
            if (targetMembersString == "") {
                targetMembersString = $j(this).data('memberid');
            }
            else {
                targetMembersString = targetMembersString + "," + $j(this).data('memberid');
            }
            count++;
        });

        sendPushFlirtsOmniture(count, targetMembersString);

        $j.ajax({
            type: "POST",
            url: "/Applications/API/Flirts.asmx/ProcessPushFlirts",
            data: "{ 'memberIDs': '" + targetMembersString + "'}",
            contentType: "application/json; charset=ISO-8859-1",
            dataType: "json",
            beforeSend: function () {
                $j('#pushFlirtsContInner').html(spark.util.ajaxLoader);
            },
            success: function (response) {
                $j('#pushFlirtsContInner').html(response.d);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $j(spark).trigger("jqueryUIDialogMessaging", [spark.pushFlirts.messageErrorGeneral, "notification error"]);

            }
        });
    });

    /* bind feature specific custom behaviors */
    $j(spark.pushFlirts).bind("pushFlirtsDialog", function (event, title) {

        if ($j('#pushFlirtsContainer').length === 0) {
            $j('<div id="pushFlirtsContainer" class="hide"></div>').appendTo('body');
        }

        $j('#pushFlirtsContainer')
                    .dialog({
                        width: 669,
                        modal: true,
                        title: spark.pushFlirts.dialogTitle,
                        position: ['center', 'top'],
                        dialogClass: 'modal-push-flirts',
                        minHeight: 400,
                        open: function () {
                            var $this = $j(this),
                                ajax_load = '<div class="loading spinner-only" />';
                            $j(spark.pushFlirts).trigger('pushFlirtsRefresh');
                        },
                        close: function () {
                            $j('#pushFlirtsContainer').find('.notification, .conf-flirt').remove();
                        }
                    })
                    .parent('.ui-dialog').css('top', function () {
                        var x = $j(this).position();
                        return x.top + 12;
                    });
    });

    function sendPushFlirtsOmniture(numberSent, targetMembersString) {

        var sentString = numberSent + " flirts sent";
        var s = s_gi(s_account);
        PopulateS(true);
        s.prop48 = sentString;
        s.events = 'event41,event5,event2';
        s.t(true);

        var i = 0;
        for (i = 0; i < numberSent; i++) {
            s = s_gi(s_account);
            s.prop48 = '';
            s.linkTrackVars = 'events';
            s.linkTrackEvents = 'event11';
            s.events = 'event11';
            s.eVar26 = "Tease";
            s.prop35 = targetMembersString;
            s.t(true);
        }
    }

    $j(function () {
        //lazy load push flirts
        $j(spark.pushFlirts).trigger("pushFlirtsDialog");
    });
</script>
</asp:PlaceHolder>
