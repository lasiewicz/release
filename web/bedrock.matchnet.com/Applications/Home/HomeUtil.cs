﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Search.Interfaces;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Web.Framework.Ui;
using System.Collections;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Analytics;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Applications.Home
{
    public class HomeUtil
    {
        public const int MATCH_SLIDER_USERNAME_MAX = 12;
        public const string HOME_TABS_COOKIE_NAME = "sparkhometab";
        public const int HOME_TABS_DEFAULT_INDEX = 1;

        public static string[] HomeTabOmnitureName = new string[]
            {"NewsFeed Tab", "Your Visitors Tab", "Your Lists Tab"};

        #region Settings Methods
        public static bool IsHomepageChannelAdsOverrideEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_HOMEPAGE_OVERRIDE_OF_RIGHT_CHANNEL_ADS", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        public static bool IsHome30Enabled(Brand brand)
        {
            SettingsManager settingManager = new SettingsManager();
            return settingManager.GetSettingBool("SPARK_30_HOMEPAGE_ENABLED", brand);
        }

        public static bool IsHome40Enabled(Brand brand)
        {
            SettingsManager settingManager = new SettingsManager();
            return settingManager.GetSettingBool("USE_HOMEPAGE_40", brand);
        }

        public static bool IsHome50Enabled(Brand brand)
        {
            SettingsManager settingManager = new SettingsManager();
            return settingManager.GetSettingBool("USE_HOMEPAGE_50", brand);
        }

        public static bool IsDisplayingHome40(Member.ServiceAdapters.Member member, Content.ServiceAdapters.Page appPage, Brand brand)
        {
            bool result = false;

            if (appPage.App.ID == (int)WebConstants.APP.Home && appPage.ControlName.ToLower() == "sparkhome")
            {
                ControlVersion version = GetHomepageVersion(member, brand);
                if (version == ControlVersion.Version40a || version == ControlVersion.Version40b)
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool IsDisplayingHome50(Member.ServiceAdapters.Member member, Content.ServiceAdapters.Page appPage, Brand brand)
        {
            bool result = false;

            if (appPage.App.ID == (int)WebConstants.APP.Home && appPage.ControlName.ToLower() == "sparkhome")
            {
                ControlVersion version = GetHomepageVersion(member, brand);
                if (version == ControlVersion.Version50 || version == ControlVersion.Version50b)
                {
                    result = true;
                }
            }

            return result;
        }

        public static ControlVersion GetHomepageVersion(Member.ServiceAdapters.Member member, Brand brand)
        {
            ControlVersion version = ControlVersion.Current;

            if (member != null)
            {
                if (IsHome50Enabled(brand))
                {
                    //Version 5.0 has two versions
                    FeatureThrottler ft = new FeatureThrottler(ThrottleMode.LastDigit, "USE_HOMEPAGE_50a_LASTDIGITS",
                                                               brand, member);
                    if (ft.IsFeatureEnabled())
                    {
                        version = ControlVersion.Version50;
                    }
                    else if (ft.IsFeatureEnabled("USE_HOMEPAGE_50b_LASTDIGITS"))
                    {
                        version = ControlVersion.Version50b;
                    }
                }

                if (version == ControlVersion.Current && IsHome40Enabled(brand))
                {
                    //Version 4.0 has two versions
                    FeatureThrottler ft = new FeatureThrottler(ThrottleMode.LastDigit, "USE_HOMEPAGE_40a_LASTDIGITS",
                                                               brand, member);
                    if (ft.IsFeatureEnabled())
                    {
                        version = ControlVersion.Version40a;
                    }
                    else if (ft.IsFeatureEnabled("USE_HOMEPAGE_40b_LASTDIGITS"))
                    {
                        version = ControlVersion.Version40b;
                    }
                }

                if (version == ControlVersion.Current && IsHome30Enabled(brand))
                {
                    version = ControlVersion.Version30;
                }
            }

            //TEMP: Creative to see version 5 for all accounts
            //version = ControlVersion.Version50;

            return version;
        }

        public static bool DisplaySecretAdmirerGameFromHomepageFilmstrip(BreadCrumbHelper.EntryPoint entryPoint)
        {
            bool displaySecretAdmirerGame = false;
            if (entryPoint == BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVoting
                || entryPoint == BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVotingMOL)
            {
                displaySecretAdmirerGame = true;
            }

            //disable display of secret admirer game from filmstrip
            displaySecretAdmirerGame = false;

            return displaySecretAdmirerGame;
        }
        #endregion

        #region Slider/Filmstrip methods
        public static GetSearchResult GetSliderProfilesJSON(IMemberDTO member, Brand brand, ContextGlobal g,
                                                            SearchPreferenceCollection prefs, UserSession session,
                                                            int startRow, int pageSize, bool checkMatches, bool checkMOL,
                                                            int usernameMaxLength, object resourceControl)
        {
            return GetSliderProfilesJSON(member, brand, g, prefs, session, startRow, pageSize, checkMatches, checkMOL,
                                         usernameMaxLength, resourceControl,
                                         BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMatches);
        }
        public static GetSearchResult GetSliderProfilesJSON(IMemberDTO member, Brand brand, ContextGlobal g, SearchPreferenceCollection prefs, UserSession session, int startRow, int pageSize, bool checkMatches, bool checkMOL, int usernameMaxLength, object resourceControl, BreadCrumbHelper.EntryPoint entrypoint)
        {
            GetSearchResult getSearchResult = new GetSearchResult();
            getSearchResult.SearchProfileList = new List<SearchProfile>();
            getSearchResult.Status = (int)JSONStatus.Success;
            MOLQueryResult molQueryResult = null;
            ArrayList members = new ArrayList();
            MOCollection mocollection = null;
            ArrayList results = new ArrayList();
            bool isFilmstripVoting = false;

            if (entrypoint == BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVoting || entrypoint == BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVotingMOL)
            {
                isFilmstripVoting = true;
            }

            if (startRow <= 0)
            {
                startRow = 1;
            }

            //get matches
            MatchnetQueryResults searchResults = null;
            if (checkMatches)
            {
                searchResults = GetFilmstripMatchesProfile(member.MemberID, brand, prefs, startRow, pageSize, isFilmstripVoting);
                getSearchResult.IsMatchResults = true;
                getSearchResult.IsMOLResults = false;
            }

            if (searchResults == null || searchResults.ToArrayList().Count <= 0)
            {
                //get mol
                if (checkMOL)
                {
                    if (isFilmstripVoting)
                    {
                        entrypoint = BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVotingMOL;
                        
                    }
                    else
                    {
                        entrypoint = BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMOL;
                    }
                    
                    molQueryResult = GetFilmstripMembersOnlineProfiles(member, brand, prefs, session, startRow, pageSize, isFilmstripVoting, out mocollection);
                    if (molQueryResult != null)
                    {
                        results = molQueryResult.ToArrayList();
                        getSearchResult.TotalResults = molQueryResult.TotalMembersOnline;
                    }
                    getSearchResult.IsMatchResults = false;
                    getSearchResult.IsMOLResults = true;
                }
            }
            else if (searchResults != null)
            {
                results = searchResults.ToArrayList();
                getSearchResult.TotalResults = searchResults.MatchesFound;
            }

            if (results != null && results.Count > 0)
            {
                members = MemberDTOManager.Instance.GetIMemberDTOs(brand, results, MemberType.Search, MemberLoadFlags.None);
                foreach (IMemberDTO m in members)
                {
                    int ordinal = startRow;

                    //add member
                    SearchProfile ph = new SearchProfile();
                    ph.MemberID = m.MemberID;
                    ph.Ordinal = ordinal;
                    Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(member, m, brand);
                    ph.PhotoURL = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(member, m, brand, photo, PhotoType.Thumbnail, PrivatePhotoImageType.Thumb, NoPhotoImageType.Thumb, false, false);
                    ph.LargePhotoURL = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(member, m, brand, photo, PhotoType.Full, PrivatePhotoImageType.Full, NoPhotoImageType.Thumb, false, false);
                    ph.Highlighted = FrameworkGlobals.memberHighlighted(m, brand);
                    ph.ProfileURL = BreadCrumbHelper.MakeViewProfileLink(entrypoint, m.MemberID, ph.Ordinal, mocollection, 0, false, ph.Highlighted ? (int)BreadCrumbHelper.PremiumEntryPoint.Highlight : (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
                    ph.ProfileURLWithoutOrdinal = BreadCrumbHelper.MakeViewProfileLink(entrypoint, m.MemberID, Constants.NULL_INT, mocollection, 0, false, ph.Highlighted ? (int)BreadCrumbHelper.PremiumEntryPoint.Highlight : (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
                    ph.Username = m.GetUserName(brand);
                    ph.UsernameShort = FrameworkGlobals.Ellipsis(ph.Username, usernameMaxLength, "&hellip;");
                    ph.Birthdate = m.GetAttributeDate(brand, "birthdate");
                    ph.RegionID = m.GetAttributeInt(brand, "regionid");
                    ph.NumberApprovedPhotos = MemberPhotoDisplayManager.Instance.GetApprovedPhotosCount(member, m, brand);
                    ph.Age = FrameworkGlobals.GetAgeCaption(ph.Birthdate, g, resourceControl, "AGE_UNSPECIFIED");
                    ph.Location = FrameworkGlobals.Ellipsis(FrameworkGlobals.GetRegionString(ph.RegionID, brand.Site.LanguageID, false, true, false), 22, "&hellip;");
                    int genderMask = m.GetAttributeInt(brand, "gendermask");
                    if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                    {
                        ph.Gender = g.GetResource("TXT_MALE", resourceControl);
                    }
                    else if ((genderMask & ConstantsTemp.GENDERID_FEMALE) == ConstantsTemp.GENDERID_FEMALE)
                    {
                        ph.Gender = g.GetResource("TXT_FEMALE", resourceControl);
                    }
                    ph.ViewMorePhotos = HttpContext.Current.Server.HtmlDecode(FrameworkGlobals.GetMorePhotoLink(ph.NumberApprovedPhotos, g, resourceControl));
                    ph.YNMCurrentVote = (int)g.List.GetClickMask(brand.Site.Community.CommunityID, brand.Site.SiteID, m.MemberID);
                    if (MatchRatingManager.Instance.DisplayMatchRatingOnFilmstrip(member.MemberID, brand))
                    {
                        ph.MatchRating = MatchRatingManager.Instance.CalculateMatchPercentage(member.MemberID,
                                                                                              m.MemberID,
                                                                                              brand.Site.Community
                                                                                               .CommunityID,
                                                                                              brand.Site.SiteID,
                                                                                              SearchType.None).MatchScore;
                    }
                    getSearchResult.SearchProfileList.Add(ph);

                    startRow++;

                }
            }

            return getSearchResult;

        }

        public static MatchnetQueryResults GetFilmstripMatchesProfile(int memberID, Brand brand, SearchPreferenceCollection prefs, int startRow, int pageSize, bool filmstripVoting)
        {
            MatchnetQueryResults searchResults = null;

            //get current photo preference before we modify it
            string currentPhotoPreference = prefs["HasPhotoFlag"];
            prefs["HasPhotoFlag"] = "1";

            //get current sort before we modify it
            string currentOrderBy = prefs["SearchOrderBy"];
            prefs["SearchOrderBy"] = ((Int32)Matchnet.Search.Interfaces.QuerySorting.LastLogonDate).ToString();

            //Ensure that domainID is set on search preferences for the correct Community.
            prefs.Add("DomainID", brand.Site.Community.CommunityID.ToString());
            prefs.Add("ColorCode", "");

            if (filmstripVoting)
            {
                //set exclude ynm no list
                prefs["excludeynmnoflag"] = "1";
            }

            //Get matches
            try
            {
                // Potential for exception due to invalid search preferences on this member's stored set of preferences.
                searchResults = MemberSearchSA.Instance.Search(prefs
                     , brand.Site.Community.CommunityID
                     , brand.Site.SiteID
                     , startRow - 1 //starts at 0
                     , pageSize
                     , memberID
                     , Matchnet.Search.Interfaces.SearchEngineType.FAST
                     , Matchnet.Search.Interfaces.SearchType.ProfileFilmStripWebSearch
                     , Matchnet.Search.Interfaces.SearchEntryPoint.ProfileFilmStripMatches
                     , false
                     , false
                     , false
                     );
            }
            catch (Exception)
            {
                //invalid search preferences, initialize an empty results list so the home page will render correctly.
                searchResults = new MatchnetQueryResults(0);
            }

            //set temp preference back to old value
            prefs["HasPhotoFlag"] = currentPhotoPreference;
            prefs["SearchOrderBy"] = currentOrderBy;
            prefs["excludeynmnoflag"] = "";

            return searchResults;
        }

        public static Matchnet.MembersOnline.ValueObjects.MOLQueryResult GetFilmstripMembersOnlineProfiles(IMemberDTO member, Brand brand, SearchPreferenceCollection prefs, UserSession session, int startRow, int pageSize, bool isVoting, out MOCollection mocollection)
        {
            Matchnet.MembersOnline.ValueObjects.MOLQueryResult molQueryResult = null;
            List<HotListCategory> filterOutHotlist = null;
            if (isVoting)
            {
                filterOutHotlist = new List<HotListCategory>();
                filterOutHotlist.Add(HotListCategory.MembersClickedNo);
            }

            mocollection = GetMembersOnlineCriteria(prefs, session, brand, member.GetAttributeInt(brand, "RegionID"));
            molQueryResult = GetMembersOnline(member.MemberID, brand, session, mocollection, startRow, pageSize, filterOutHotlist);
            if (molQueryResult == null || molQueryResult.ToArrayList().Count <= 1)
            {
                mocollection = GetMembersOnlineCriteria(prefs, session, brand, brand.Site.DefaultRegionID);
                molQueryResult = GetMembersOnline(member.MemberID, brand, session, mocollection, startRow, pageSize, filterOutHotlist);
            }

            return molQueryResult;
        }

        public static MOCollection GetMembersOnlineCriteria(SearchPreferenceCollection prefs, UserSession session, Brand brand, int regionid)
        {
            try
            {
                int gendermask = Conversion.CInt(prefs["gendermask"]);

                int agemin = session.GetInt("MembersOnlineAgeMin", brand.DefaultAgeMin);
                int agemax = session.GetInt("MembersOnlineAgeMax", brand.DefaultAgeMax);
                int languagemask = session.GetInt("MembersOnlineLanguage", 0);

                MOCollection mocollection = new MOCollection(SortFieldType.HasPhoto.ToString(), gendermask, regionid, agemin, agemax, languagemask);

                return mocollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static Matchnet.MembersOnline.ValueObjects.MOLQueryResult GetMembersOnline(int memberID, Brand brand, UserSession session, MOCollection molcollection, int startRow, int pageSize, List<HotListCategory> filterOutHotlist)
        {
            try
            {
                Matchnet.MembersOnline.ValueObjects.MOLQueryResult MOLResults = MembersOnlineSA.Instance.GetMemberIDs(session.Key.ToString(),
                 brand.Site.Community.CommunityID,
                 (short)molcollection.GenderMask,
                 molcollection.RegionID,
                 (Int16)molcollection.AgeMin,
                 (Int16)((Int16)molcollection.AgeMax + 1),	// See TT 15298.
                 molcollection.LanguageMask,
                 SortFieldType.HasPhoto,
                 SortDirectionType.Asc,
                 startRow - 1, //mol starts at 0
                 pageSize,
                 memberID,
                 filterOutHotlist);

                return MOLResults;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
