﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MingleHome.ascx.cs" Inherits="Matchnet.Web.Applications.Home.MingleHome" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="qa" TagName="MiniQuestionModule" Src="~/Applications/QuestionAnswer/Controls/MiniQuestionModule.ascx" %>
<%@ Register TagPrefix="qa" TagName="MiniAnswerModule" Src="~/Applications/QuestionAnswer/Controls/MiniAnswerModule.ascx" %>
<%@ Register tagprefix="uc2" tagname="ProfileFilmStrip" src="~/Framework/Ui/BasicElements/ProfileFilmStrip.ascx"  %>
<%@ Register tagprefix="uc2" tagname="ProfileActivity" src="~/Framework/Ui/BasicElements/ProfileActivity.ascx"  %>
<%@ Register TagPrefix="qa" TagName="SubmitQuestionClientModule" Src="~/Applications/QuestionAnswer/Controls/SubmitQuestionClientModule.ascx" %>

<div id="page-container">

    <uc2:ProfileActivity ID="ProfileActivity1" runat="server" TrackingParam="ClickFrom=Homepage%20%u2013%20Profile%20Activity%20Module" />
    <uc2:ProfileFilmStrip ID="ProfileFilmStripYourMatches" runat="server" TrackingParam="ClickFrom=Homepage - Your Matches" />

    <asp:PlaceHolder ID="phQuestionAnswer" runat="server" Visible="false">
        <div id="qanda" class="qanda border-box clearfix">
            <h2 class="component-header"><mn2:FrameworkLiteral ID="literalQuestionAnswerHeader" runat="server" ResourceConstant="TXT_TODAYSPARKS"></mn2:FrameworkLiteral></h2>
            
            <!--Question module-->
            <qa:MiniQuestionModule ID="MiniQuestionModule1" runat="server" />
            <!--Answer module-->
            <qa:MiniAnswerModule ID="MiniAnswerModule1" runat="server" TrackingParam="ClickFrom=Homepage%20%u2013%20Question%20of%20the%20Week" />
            <qa:SubmitQuestionClientModule ID="sendQuestionClientModule" runat="server" />
        </div>
    </asp:PlaceHolder>

<!--iframe used by SaveYNMVote function-->
<iframe name="FrameYNMVote" tabindex="-1" frameborder="0" scrolling="no" width="1" height="1">
	<layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
</iframe>
</div>