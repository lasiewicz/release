﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchesSlider20.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Controls.MatchesSlider20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<h2 class="component-header"><a href="/Applications/Search/SearchResults.aspx?SearchOrderBy=2" id="lnkFilmstripTitle" runat="server"><mn:Txt ID="txtComponentTitle" runat="server" ResourceConstant="TXT_COMPONENT_HEADER_MATCHES" /></a> <small><a href="/Applications/Search/SearchResults.aspx?SearchOrderBy=2" id="lnkViewMore" runat="server" class="view-more"><mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_VIEW_MORE" /> </a></small></h2>
<div id="match-slider" class="match-slider-20 clearfix">
    <div id="ajax-loader" class="ajax-loader">
        <p class="info">
            <mn:Image id="mnimage4704" runat="server" filename="ajax-loader.gif" titleresourceconstant="" resourceconstant="" />
            <mn:Txt id="mntxt7220" runat="server" resourceconstant="TXT_LOADING" expandimagetokens="true" />
        </p>
    </div>

    <button id="match-slider-button-prev" class="spr-btn btn-arrow-lg-prev-20 disabled">&#9668;</button>
    <button id="match-slider-button-next" class="spr-btn btn-arrow-lg-next-20">&#9658;</button>

    <%--Profiles--%>
    <div id="match-slider-profiles-container" class="match-slider-20-container clearfix"></div>
</div>
<div id="match-slider-none" class="match-slider-20 clearfix" style="display:none;">
    <%--No Matches--%>
    <div class="tips">
        <p><mn:Txt ID="txtNoMatches" runat="server" ResourceConstant="TXT_NOMATCHES_BODY" /></p>
    </div>
</div>
<ul class="nav-away">
    <li><a href="/Applications/Search/SearchPreferences.aspx" id="lnkMatchPreferences" runat="server"><mn:Txt ID="txtUpdatePreferences" runat="server" ResourceConstant="TXT_UPDATE_PREFRENCES" /></a></li>
    <li><a href="/Applications/HotList/SlideShow.aspx" id="lnkSlideshow" runat="server"><mn:Txt ID="txtViewYNM" runat="server" ResourceConstant="TXT_VIEW_YNM" /></a></li>
    <li><a href="/Applications/AdvancedSearch/AdvancedSearch.aspx" id="lnkSearch" runat="server"><mn:Txt ID="txtSearch" runat="server" ResourceConstant="TXT_SEARCH" /></a></li>
    <li><a href="/Applications/PhotoGallery/PhotoGallery.aspx" id="lnkPhotoGallery" runat="server"><mn:Txt ID="txtPhotoGallery" runat="server" ResourceConstant="TXT_PHOTO_GALLERY" /></a></li>
</ul>

<script id="profile_template" type="text/template">
    <li class="mini-profile">
        <a href="{{photoProfileURL}}" title="{{username}}">
            <span class="photo">
                <img src="{{photoURL}}" alt="{{username}}" />
            </span>
        </a>
        <a href="{{usernameProfileURL}}" title="{{username}}">
            <span class="info">{{usernameShort}}</span>
        </a>
        <span class="info">{{age}} - {{location}}</span>
        <a href="{{viewMorePhotosURL}}" title="{{username}}" class="more-photo">
            {{viewMorePhotos}}
        </a>
    </li>
</script>

<script id="profile_ratings_template" type="text/template">
    <li class="mini-profile">
        <a href="{{photoProfileURL}}" title="{{username}}">
            <span class="photo">
                <img src="{{photoURL}}" alt="{{username}}" />
            </span>
        </a>
        <span class="rate">{{matchRatingDisplay}}%</span>
        <a href="{{usernameProfileURL}}" title="{{username}}" class="name">
            {{usernameShort}}
        </a>
        <span class="info">{{age}} - {{location}}</span>
        <a href="{{viewMorePhotosURL}}" title="{{username}}" class="more-photo">
            {{viewMorePhotos}}
        </a>
    </li>
</script>

<script type="text/javascript">
    var matchSliderManager = {
        profileList : [],
        memberID : 0,
        startRow : 1,
        previousStartRow : 1,
        pageSize : 0,
        totalResultCount : 0,
        updateInProgress : false,
        isVerbose : true,
        matchRatingEnabled : false,
        checkMatches : true,
        checkMOL : true,
        omnitureUsernameProfileURLTrackingParams : '',
        omniturePhotoProfileURLTrackingParams : '',
        omnitureViewMorePhotosProfileURLTrackingParams : '',
        omnitureControlName : '',
        preloaded : true,
        loadingModeTypes : {
            0 : 'CappedBrowserCacheOnly',
            1 : 'AjaxNoBrowserCache',
            2 : 'AjaxWithBrowserCache'
        },
        loadingModeDefault : '',
        loadingModeActive : '',
        loadingCheckDuplicate : true,
        profileContainerID : 'match-slider-profiles-container',
        yesSliderContainerID : 'match-slider',
        noSliderContainerID : 'match-slider-none',
        buttonNextID : 'match-slider-button-next',
        buttonPrevID : 'match-slider-button-prev',
        ajaxLoaderID : 'ajax-loader',
        createProfile : function (searchProfile) {
            var profile1 = {
                profileIndex: matchSliderManager.profileList.length,
                profileOrdinal: searchProfile.Ordinal,
                photoURL: searchProfile.PhotoURL,
                memberID: searchProfile.MemberID,
                usernameProfileURL: searchProfile.ProfileURL + "&" + matchSliderManager.omnitureUsernameProfileURLTrackingParams,
                photoProfileURL: searchProfile.ProfileURL + "&" + matchSliderManager.omniturePhotoProfileURLTrackingParams,
                username: searchProfile.Username,
                highlighted: searchProfile.Highlighted,
                usernameShort: searchProfile.UsernameShort,
                matchRating: searchProfile.MatchRating,
                matchRatingDisplay: searchProfile.MatchRating,
                gender: searchProfile.Gender,
                age: searchProfile.Age,
                location: searchProfile.Location,
                viewMorePhotos: searchProfile.ViewMorePhotos,
                viewMorePhotosURL: searchProfile.ProfileURL + "&" + matchSliderManager.omnitureViewMorePhotosProfileURLTrackingParams
            };

            if (searchProfile.MatchRating < 50){
                profile1.matchRatingDisplay = "&lt;50";
            }
            return profile1;
        },
        getCurrentPageNumber : function(){
            var currentPage = 1;
            if ((matchSliderManager.startRow - 1) > 0){
                currentPage = Math.ceil(((matchSliderManager.startRow - 1) + matchSliderManager.pageSize) / matchSliderManager.pageSize);
            }
            return currentPage;
        },
        onNextPageClick : function () {
            //paginate to next page
            if (!matchSliderManager.updateInProgress) {
                matchSliderManager.getSliderProfiles(matchSliderManager.startRow + matchSliderManager.pageSize);
            }
        },
        onPrevPageClick : function () {
            //paginate to previous page
            if (!matchSliderManager.updateInProgress) {
                matchSliderManager.getSliderProfiles(matchSliderManager.startRow - matchSliderManager.pageSize);
            }
        },
        getSliderProfiles : function(newStartRow){
            if (!matchSliderManager.updateInProgress) {
                var requiresAjax = true;
                //set startRow
                matchSliderManager.previousStartRow = matchSliderManager.startRow;
                matchSliderManager.startRow = newStartRow;
                if (matchSliderManager.profileList.length <= 0 || matchSliderManager.startRow <= matchSliderManager.pageSize) {
                    matchSliderManager.startRow = 1;
                }

                if (matchSliderManager.loadingModeActive == matchSliderManager.loadingModeTypes["0"]) {
                    //CappedBrowserCacheOnly
                    requiresAjax = false;
                }
                else if (matchSliderManager.loadingModeActive == matchSliderManager.loadingModeTypes["1"]) {
                    //AjaxNoBrowserCache
                    if (matchSliderManager.startRow <= matchSliderManager.totalResultCount) {
                        requiresAjax = true;
                    } else {
                        requiresAjax = false;
                    }
                } 
                else {
                    //AjaxWithBrowserCache
                    if (matchSliderManager.startRow < matchSliderManager.profileList.length) {
                        requiresAjax = false;
                    }
                }

                if (requiresAjax){
                    $j('#' + matchSliderManager.ajaxLoaderID).show();

                    //get profiles from service
                    $j.ajax({
                        type: "POST",
                        url: "/Applications/API/SearchResults.asmx/GetSliderProfiles",
                        data: "{memberID:" + matchSliderManager.memberID + ", startRow:" + matchSliderManager.startRow + ", pageSize:" + matchSliderManager.pageSize + ", checkMatches: " + matchSliderManager.checkMatches + ", checkMOL: " + matchSliderManager.checkMOL + "}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            matchSliderManager.updateInProgress = true;
                        },
                        complete: function () {
                            matchSliderManager.updateInProgress = false;
                        },
                        success: function (result) {
                            var getSearchResult = (typeof result.d == 'undefined') ? result : result.d;
                            if (getSearchResult.Status != 2) {
                                $j('#' + matchSliderManager.ajaxLoaderID).hide();
                                if (getSearchResult.IsMemberNull) {
                                    //member is not logged in, will refresh page
                                    window.location.href = window.location.href;
                                }
                                else if (matchSliderManager.isVerbose) {
                                    alert(getSearchResult.StatusMessage);
                                }
                            }
                            else {
                                ////load profiles
                                ////load profiles
                                try{
                                    console.log('next page - getSearchResult: ', getSearchResult);
                                }
                                catch(e) {}
                                matchSliderManager.loadPage(getSearchResult);

                                //update omniture
                                if (s != null) {
                                    PopulateS(true); //clear existing values in omniture "s" object
                                    var originalPageName = s.pageName;
                                    s.pageName = s.pageName + " - " + matchSliderManager.omnitureControlName;
                                    s.eVar25 = "" + matchSliderManager.getCurrentPageNumber() + "";
                                    s.t();
                                    s.pageName = originalPageName;
                                }
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $j('#' + matchSliderManager.ajaxLoaderID).hide();
                            if (matchSliderManager.isVerbose) {
                                alert(textStatus);
                            }
                            try{
                                console.log('Error getting next page: ' + textStatus);
                            }
                            catch(e){}
                        }
                    });
                }
                else{
                    matchSliderManager.loadPage(null);
                }
            }
        },
        loadPage : function (getSearchResult) {
            var $ajaxLoader = $j('#' + matchSliderManager.ajaxLoaderID),
                $yesSliderContainer = $j('#' + matchSliderManager.yesSliderContainerID),
                $noSliderContainer = $j('#' + matchSliderManager.noSliderContainerID);

            $ajaxLoader.show();

            ////load profiles from JSON
            if (getSearchResult != null) {
                if (getSearchResult.TotalResults > 0 || matchSliderManager.totalResultCount == 0){
                    matchSliderManager.totalResultCount = getSearchResult.TotalResults;
                }
                matchSliderManager.checkMatches = getSearchResult.IsMatchResults;
                matchSliderManager.checkMOL = getSearchResult.IsMOLResults;
                if (getSearchResult.SearchProfileList.length > 0) {
                    var tempProfileList = [];
                    
                    for (var i = 0; i < getSearchResult.SearchProfileList.length; i++) {
                        var searchProfile = getSearchResult.SearchProfileList[i];
                        var profile1 = matchSliderManager.createProfile(searchProfile);
                        
                        if (matchSliderManager.loadingCheckDuplicate && matchSliderManager.loadingModeActive == matchSliderManager.loadingModeTypes["2"]) {
                            if (matchSliderManager.startRow > 1 && matchSliderManager.startRow > matchSliderManager.previousStartRow) {
                                //check for duplicate profiles, which means server side cache expired, we'll switch over to ajax only temporarily
                                //to avoid duplicate profile issues with existing profiles cached in js
                                var checkNum = matchSliderManager.pageSize;
                                var checkCount = 1;
                                for (var k = matchSliderManager.startRow - 2; k < matchSliderManager.profileList.length && k >= 0; k--) {
                                    checkCount++;
                                    if (matchSliderManager.profileList[k].memberID == profile1.memberID) {
                                        matchSliderManager.loadingModeActive = matchSliderManager.loadingModeTypes["1"];
                                        break;
                                    } else if (checkCount >= checkNum) {
                                        break;
                                    }
                                }
                            }
                        }
                        matchSliderManager.profileList.push(profile1);
                        tempProfileList.push(profile1);
                    }
                    
                    if (matchSliderManager.loadingModeActive == matchSliderManager.loadingModeTypes["1"]) {
                        //AjaxNoBrowserCache, we'll clear out browser cached list by replacing it with current profiles only
                        matchSliderManager.profileList = tempProfileList;
                    }
                }
            }

            if (matchSliderManager.profileList.length > 0){
                
                //show profiles
                $yesSliderContainer.show();
                $noSliderContainer.hide();
                
                //recheck startRow
                if (matchSliderManager.startRow <= matchSliderManager.pageSize) {
                    matchSliderManager.startRow = 1;
                } else if (matchSliderManager.startRow > matchSliderManager.totalResultCount && matchSliderManager.startRow > matchSliderManager.profileList.length) {
                    matchSliderManager.startRow = matchSliderManager.previousStartRow;
                }

                //load page html
                var startIndex = matchSliderManager.startRow - 1,
                    endIndex = null;
                
                if (startIndex <= 0){
                    startIndex = 0;
                }
                
                if (matchSliderManager.loadingModeActive == matchSliderManager.loadingModeTypes["1"]) {
                    //AjaxNoBrowserCache, the profileList always only contains one page of profile
                    startIndex = 0;
                }
                
                endIndex = startIndex + matchSliderManager.pageSize - 1;

                if (endIndex >= matchSliderManager.profileList.length){
                    endIndex = matchSliderManager.profileList.length - 1;
                }

                var photohtml = '',
                    phototemplate = '';
                
                if (matchSliderManager.matchRatingEnabled) {
                    phototemplate = $j('#profile_ratings_template').html();
                } else {
                    phototemplate = $j('#profile_template').html();
                }
                
                for (var i = startIndex; i <= endIndex; i++){
                    photohtml += Mustache.to_html(phototemplate, matchSliderManager.profileList[i]);  
                }

                var $newItems = $j('<ul>').html(photohtml),
                    $container = $j('#' + matchSliderManager.profileContainerID),
                    scrollWidth = $container.innerWidth();
                
                function scroller($items, dir, callback){
                    $items.animate({
                        left: (dir == 'left') ? '-=' + scrollWidth : '+=' + scrollWidth
                    }, {queue: false, duration:1500, complete: callback});
                }

                if(matchSliderManager.previousStartRow < matchSliderManager.startRow){
                    var $oldItems = $container.find('ul');

                    $container.append($newItems);
                    $newItems.css({left: scrollWidth + 'px'});
                    scroller($oldItems, 'left', function(){$oldItems.remove()});
                    scroller($newItems, 'left');

                } else if(matchSliderManager.previousStartRow > matchSliderManager.startRow){
                    var $oldItems = $container.find('ul');

                    $container.prepend($newItems);
                    $newItems.css({left: '-' + scrollWidth + 'px'});
                    scroller($oldItems, '', function(){$oldItems.remove()});
                    scroller($newItems, '');

                } else {
                    $container.html($newItems);
                }

                try { console.log('Loaded match slider page: ' + matchSliderManager.getCurrentPageNumber() + ', Total Results: ' + matchSliderManager.totalResultCount + ', Mode: ' + matchSliderManager.loadingModeActive);
                } catch(e) {}

                if (matchSliderManager.totalResultCount > 0) {
                    //hide or show next/prev
                    var currentPageNumber = matchSliderManager.getCurrentPageNumber();
                    var totalPages = Math.ceil(matchSliderManager.totalResultCount / matchSliderManager.pageSize);

                    if (currentPageNumber > 1) {
                        $j('#' + matchSliderManager.buttonPrevID).removeClass('disabled');
                    } else {
                        $j('#' + matchSliderManager.buttonPrevID).addClass('disabled');
                    }

                    if (currentPageNumber < totalPages) {
                        $j('#' + matchSliderManager.buttonNextID).removeClass('disabled');
                    } else {
                        $j('#' + matchSliderManager.buttonNextID).addClass('disabled');
                    }

                    //this is first page, we'll check if we should switch back to ajax with caching
                    if (currentPageNumber == 1 && matchSliderManager.loadingModeDefault == matchSliderManager.loadingModeTypes["2"]) {
                        matchSliderManager.loadingModeActive = matchSliderManager.loadingModeTypes["2"];
                    }
                }
            }
            else{
                //show no profiles content
                $yesSliderContainer.hide();
                $noSliderContainer.show();
            }

            $ajaxLoader.hide();
        },
        initialize : function (){
            //initialize
            matchSliderManager.memberID = <%=_MemberID%>;
            matchSliderManager.pageSize = <%=_PageSize%>;
            matchSliderManager.loadingModeDefault = matchSliderManager.loadingModeTypes["<%=_LoadingTypeID%>"];
            matchSliderManager.loadingModeActive = matchSliderManager.loadingModeTypes["<%=_LoadingTypeID%>"];
            matchSliderManager.omnitureUsernameProfileURLTrackingParams = '<%=_OmnitureUsernameProfileURLTrackingParams%>';
            matchSliderManager.omniturePhotoProfileURLTrackingParams = '<%=_OmniturePhotoProfileURLTrackingParams%>';
            matchSliderManager.omnitureViewMorePhotosProfileURLTrackingParams = '<%=_OmnitureViewMorePhotosProfileURLTrackingParams%>';
            matchSliderManager.omnitureControlName = '<%=_OmnitureControlName%>';
            matchSliderManager.matchRatingEnabled = <%=_matchRatingEnabled.ToString().ToLower()%>;
                        
            $j('#match-slider-button-prev').click(function(e) {
                e.preventDefault();
                if (!$j('#match-slider-button-prev').hasClass('disabled')){
                    matchSliderManager.onPrevPageClick();
                }
            });

            $j('#match-slider-button-next').click(function(e) {
                e.preventDefault();
                if (!$j('#match-slider-button-next').hasClass('disabled')){
                    matchSliderManager.onNextPageClick();
                }
            });
            
            //no preload
            //matchSliderManager.getSliderProfiles(1);

            //yes preloaded
            <asp:Literal ID="litPreloadJS" runat="server"></asp:Literal>
        }
    };

    $j(function() {
        matchSliderManager.initialize();
    });
    
    
</script>