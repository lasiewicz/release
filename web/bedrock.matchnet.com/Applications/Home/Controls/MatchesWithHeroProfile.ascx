﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchesWithHeroProfile.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Controls.MatchesWithHeroProfile" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<h2 class="component-header"><mn:txt ID="txtComponentTitle" runat="server" ResourceConstant="TXT_COMPONENT_HEADER_MATCHES" /> <small><a href="/Applications/Search/SearchResults.aspx?SearchOrderBy=2" id="lnkViewMore" runat="server"><mn:txt ID="Txt2" runat="server" ResourceConstant="TXT_VIEW_MORE" /> </a></small></h2>
<div id="matches-hero-slider" class="new-matches clearfix grad-base">
    <div id="matches-hero-slider-ajax-loader" class="ajax-loader">
        <mn:image id="mnimage4704" runat="server" filename="ajax-loader.gif" titleresourceconstant="" resourceconstant="" />
        <p><mn:txt id="mntxt7220" runat="server" resourceconstant="TXT_LOADING" expandimagetokens="true" /></p>
    </div>
    <span class="prev button"><button id="matches-hero-slider-button-prev" class="spr-btn btn-arrow-cir-prev disabled">&#9668;</button></span>
    <span class="next button"><button id="matches-hero-slider-button-next" class="spr-btn btn-arrow-cir-next">&#9658;</button></span>

    <div id="matches-hero-slider-profiles-container">
        <div id="hero-profile-container" class="mini-profile simple hero"></div>
        <div id="matches-profile-container" class="profiles clearfix"></div>
    </div>

</div>
<div id="matches-hero-slider-none" class="new-matches clearfix grad-base" style="display:none;">
    <%--No Matches--%>
    <div class="tips">
        <p><mn:txt id="txtNoMatches" runat="server" resourceconstant="TXT_NOMATCHES_BODY" expandimagetokens="true" /></p>
    </div>
</div>

<script type="text/javascript">
    function spark_home_match_slider_profile() {
        this.profileIndex = 0;
        this.profileOrdinal = 0;
        this.photoURL = '';
        this.largePhotoURL = '';
        this.memberID = 0;
        this.profileURL = '';
        this.username = '';
        this.usernameShort = '';
        this.highlighted = false;
        this.viewMore = '';
        this.age = '';
        this.location = '';
        this.omnitureUsernameProfileURLTrackingParams = '';
        this.omniturePhotoProfileURLTrackingParams = '';
        this.omnitureHeroUsernameProfileURLTrackingParams = '';
        this.omnitureHeroPhotoProfileURLTrackingParams = '';
        this.omnitureHeroViewMoreProfileURLTrackingParams = '';
    }

    function spark_home_match_slider_manager() {
        this.profileList = [];
        this.memberID = 0;
        this.startRow = 1;
        this.pageSize = 9;
        this.currentStartIndex = 0;
        this.totalResultCount = 0;
        this.updateInProgress = false;
        this.isVerbose = true;
        this.yesSliderContainerID = '';
        this.noSliderContainerID = '';
        this.profilesContainerID = '';
        this.heroContainerID = '';
        this.matchesContainerID = '';
        this.buttonPrevID = '';
        this.buttonNextID = '';
        this.ajaxLoaderID = '';
        this.checkMatches = true;
        this.checkMOL = true;
        this.omnitureUsernameProfileURLTrackingParams = '';
        this.omniturePhotoProfileURLTrackingParams = '';
        this.omnitureHeroUsernameProfileURLTrackingParams = '';
        this.omnitureHeroPhotoProfileURLTrackingParams = '';
        this.omnitureHeroViewMoreProfileURLTrackingParams = '';
        this.addProfile = function (profileObj) {
            spark.util.addItem(this.profileList, profileObj);
        }
        this.createProfile = function (searchProfile){
            var profile1 = new spark_home_match_slider_profile();
            profile1.profileIndex = matchSliderManager.profileList.length;
            profile1.profileOrdinal = searchProfile.Ordinal;
            profile1.photoURL = searchProfile.PhotoURL;
            profile1.largePhotoURL = searchProfile.LargePhotoURL;
            profile1.memberID = searchProfile.MemberID;
            profile1.profileURL = searchProfile.ProfileURL;
            profile1.username = searchProfile.Username;
            profile1.highlighted = searchProfile.Highlighted;
            profile1.usernameShort = searchProfile.UsernameShort;
            profile1.viewMore = searchProfile.ViewMorePhotos;
            profile1.age = searchProfile.Age;
            profile1.location = searchProfile.Location;
            profile1.omnitureUsernameProfileURLTrackingParams = matchSliderManager.omnitureUsernameProfileURLTrackingParams;
            profile1.omniturePhotoProfileURLTrackingParams = matchSliderManager.omniturePhotoProfileURLTrackingParams;
            profile1.omnitureHeroUsernameProfileURLTrackingParams = matchSliderManager.omnitureHeroUsernameProfileURLTrackingParams;
            profile1.omnitureHeroPhotoProfileURLTrackingParams = matchSliderManager.omnitureHeroPhotoProfileURLTrackingParams;
            profile1.omnitureHeroViewMoreProfileURLTrackingParams = matchSliderManager.omnitureHeroViewMoreProfileURLTrackingParams;
            return profile1;
        }
        this.getCurrentPageNumber = function(){
            var currentPage = 1;
            if (this.currentStartIndex > 0){
                currentPage = Math.ceil((this.currentStartIndex + this.pageSize) / this.pageSize);
            }
            return currentPage;
        }
        this.onNextPageClick = function () {
            //paginate to next page
            if (!this.updateInProgress) {
                this.getSliderProfiles(this.startRow + this.pageSize);
            }
        }
        this.onPrevPageClick = function () {
            //paginate to previous page
            if (!this.updateInProgress) {
                this.getSliderProfiles(this.startRow - this.pageSize);
            }
        }
        this.getSliderProfiles = function(newStartRow){
            if (!this.updateInProgress) {
                var requiresAjax = true;
                //set startRow
                var existingStartRow = this.startRow;
                this.startRow = newStartRow;
                if (this.profileList.length <= 0){
                    this.startRow = 1;
                }

                if (this.startRow < this.profileList.length){
                    requiresAjax = false;
                }

                if (!this.checkMatches && !this.checkMOL){
                    requiresAjax = false;
                }
                            
                if (requiresAjax){
                    $j('#' + matchSliderManager.ajaxLoaderID).show();

                    //get profiles from service
                    $j.ajax({
                        type: "POST",
                        url: "/Applications/API/SearchResults.asmx/GetSliderProfiles",
                        data: "{memberID:" + matchSliderManager.memberID + ", startRow:" + matchSliderManager.startRow + ", pageSize:" + matchSliderManager.pageSize + ", checkMatches: " + matchSliderManager.checkMatches + ", checkMOL: " + matchSliderManager.checkMOL + "}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            matchSliderManager.updateInProgress = true;
                        },
                        complete: function () {
                            matchSliderManager.updateInProgress = false;
                        },
                        success: function (result) {
                            var getSearchResult = (typeof result.d == 'undefined') ? result : result.d;
                            if (getSearchResult.Status != 2) {
                                $j('#' + matchSliderManager.ajaxLoaderID).hide();
                                if (matchSliderManager.isVerbose) {
                                    alert(getSearchResult.StatusMessage);
                                }
                            }
                            else {
                                ////load profiles
                                if (getSearchResult.SearchProfileList.length > 0) {
                                    try{
                                        console.log('getSearchResult: ', getSearchResult);
                                    }
                                    catch(e) {}
                                    matchSliderManager.loadPage(getSearchResult);

                                    //update omniture
                                    if (s != null) {
                                        PopulateS(true); //clear existing values in omniture "s" object
                                        var originalPageName = s.pageName;
                                        s.pageName = s.pageName + " - Hero Filmstrip";
                                        s.eVar25 = "" + matchSliderManager.getCurrentPageNumber() + "";
                                        s.t();
                                        s.pageName = originalPageName;
                                    }
                                }
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $j('#' + matchSliderManager.ajaxLoaderID).hide();
                            if (matchSliderManager.isVerbose) {
                                alert(textStatus);
                            }
                            try{
                                console.log('Error getting next page: ' + textStatus);
                            }
                            catch(e){}
                        }
                    });
                }
                else{
                    matchSliderManager.loadPage(null);
                }
            }
        }
        this.loadPage = function (getSearchResult){
            var $ajaxLoader = $j('#' + this.ajaxLoaderID),
                $yesSliderContainer = $j('#' + this.yesSliderContainerID),
                $noSliderContainer = $j('#' + this.noSliderContainerID);

            $ajaxLoader.show();

            ////load profiles from JSON
            if (getSearchResult != null) {
                if (getSearchResult.TotalResults > 0 || matchSliderManager.totalResultCount == 0){
                    matchSliderManager.totalResultCount = getSearchResult.TotalResults;
                }
                matchSliderManager.checkMatches = getSearchResult.IsMatchResults;
                matchSliderManager.checkMOL = getSearchResult.IsMOLResults;
                if (getSearchResult.SearchProfileList.length > 0){
                    for (var i = 0; i < getSearchResult.SearchProfileList.length; i++) {
                        var searchProfile = getSearchResult.SearchProfileList[i];
                        var profile1 = matchSliderManager.createProfile(searchProfile);
                        matchSliderManager.addProfile(profile1);
                    }
                }
            }

            if (this.profileList.length > 0){
                
                //show profiles
                $yesSliderContainer.show();
                $noSliderContainer.hide();

                //load page html
                var startIndex = this.startRow - 1,
                    existingStartIndex = this.currentStartIndex,
                    endIndex = null;
                
                if (startIndex <= 0){
                    startIndex = 0;
                }
                this.currentStartIndex = startIndex;

                endIndex = startIndex + this.pageSize - 1;

                if (endIndex >= this.profileList.length){
                    endIndex = this.profileList.length - 1;
                }

                //various containers
                var $container = $j('#' + this.profilesContainerID),
                    $heroContainer = $j('#' + this.heroContainerID),
                    $matchesContainer = $j('#' + this.matchesContainerID);

                //hero item template
                var herohtml = '';
                var herotemplate = '<span class="photo"><a href="{{profileURL}}&{{omnitureHeroPhotoProfileURLTrackingParams}}"><img src="{{largePhotoURL}}" border="0" /></a></span><ul class="info"><li><h2><a title="{{username}}" href="{{profileURL}}&{{omnitureHeroUsernameProfileURLTrackingParams}}">{{usernameShort}}</a></h2></li><li>{{age}}</li><li>{{location}}</li><li><a href="{{profileURL}}&ActionLink=ViewMorePhotos&{{omnitureHeroViewMoreProfileURLTrackingParams}}" class="view-more">{{viewMore}}</a></li></ul>';

                //matches slider item template
                var matcheshtml = '';
                var matchestemplate = '<li class="mini-profile simple"><a href="{{profileURL}}&{{omniturePhotoProfileURLTrackingParams}}"><span class="photo"><img src="{{photoURL}}" border="0" /></span></a><h4><a title="{{username}}" href="{{profileURL}}&{{omnitureUsernameProfileURLTrackingParams}}">{{usernameShort}}</a></h4></li>';
                
                for (var i = startIndex; i <= endIndex; i++){
                    if (i == startIndex){
                        //hero
                        herohtml = Mustache.to_html(herotemplate, this.profileList[i]);
                    }
                    else{
                        //matches slider
                        matcheshtml += Mustache.to_html(matchestemplate, this.profileList[i]);
                    }
                }

                //update containers with new profiles
                $heroContainer.html(herohtml);
                $matchesContainer.html('<ul class="clearfix">' + matcheshtml + "</ul>");

                try { console.log('Loaded matches-hero-slider page: ' + this.getCurrentPageNumber() + ', Total Results: ' + this.totalResultCount);
                } catch(e) {}

                //hide or show next/prev
                if (startIndex > 0){
                    $j('#' + this.buttonPrevID).removeClass('disabled');
                }else{
                    $j('#' + this.buttonPrevID).addClass('disabled');
                }

                if ((endIndex < (this.profileList.length - 1)) || (endIndex < this.totalResultCount - 1)){
                    $j('#' + this.buttonNextID).removeClass('disabled');
                }else{
                    $j('#' + this.buttonNextID).addClass('disabled');
                }
            }
            else{
                //show no profiles content
                $yesSliderContainer.hide();
                $noSliderContainer.show();
            }

            $ajaxLoader.hide();
        }
    }

    //initialize objects
    var matchSliderManager = new spark_home_match_slider_manager();
    matchSliderManager.memberID = <%=_MemberID%>;
    matchSliderManager.pageSize = <%=_PageSize%>;
    matchSliderManager.profilesContainerID = 'matches-hero-slider-profiles-container';
    matchSliderManager.heroContainerID = 'hero-profile-container';
    matchSliderManager.matchesContainerID = 'matches-profile-container';
    matchSliderManager.yesSliderContainerID = 'matches-hero-slider';
    matchSliderManager.noSliderContainerID = 'matches-hero-slider-none';
    matchSliderManager.buttonNextID = 'matches-hero-slider-button-next';
    matchSliderManager.buttonPrevID = 'matches-hero-slider-button-prev';
    matchSliderManager.ajaxLoaderID = 'matches-hero-slider-ajax-loader';
    matchSliderManager.omnitureUsernameProfileURLTrackingParams = '<%=_OmnitureUsernameProfileURLTrackingParams%>';
    matchSliderManager.omniturePhotoProfileURLTrackingParams = '<%=_OmniturePhotoProfileURLTrackingParams%>';
    matchSliderManager.omnitureHeroUsernameProfileURLTrackingParams = '<%=_OmnitureHeroUsernameProfileURLTrackingParams%>';
    matchSliderManager.omnitureHeroPhotoProfileURLTrackingParams = '<%=_OmnitureHeroPhotoProfileURLTrackingParams%>';
    matchSliderManager.omnitureHeroViewMoreProfileURLTrackingParams = '<%=_OmnitureHeroViewMoreProfileURLTrackingParams%>';

    $j(function () {                
        $j('#matches-hero-slider-button-prev').click(function(e) {
            e.preventDefault();
            if (!$j('#matches-hero-slider-button-prev').hasClass('disabled')){
                matchSliderManager.onPrevPageClick();
            }
        });

        $j('#matches-hero-slider-button-next').click(function(e) {
            e.preventDefault();
            if (!$j('#matches-hero-slider-button-next').hasClass('disabled')){
                matchSliderManager.onNextPageClick();
            }
        });

        //no preload
        //matchSliderManager.getSliderProfiles(1);

        //yes preloaded
        <asp:Literal ID="litPreloadJS" runat="server"></asp:Literal>
    });

</script>
