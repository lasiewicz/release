﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Targeting;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Applications.Home.Controls
{
    public partial class SecretAdmirerPopup : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool enabled;

            TargetingEngine targetingEngine = new TargetingEngine(g.Member, g.Brand);
            enabled = targetingEngine.EnableFeature("SecretAdmirerOverlay");

            slideshowSecretAdmirerPopupData.Attributes.Add("data-launch-modal", enabled.ToString().ToLower());

            if(enabled)
            {
                string sessiontimes = _g.Session.GetString(WebConstants.SESSION_PROPERTY_SECRET_ADMIRER_MODAL_SHOW_COUNT);
                int timesalreadyshown = 0;

                if (!String.IsNullOrEmpty(sessiontimes))
                {
                    timesalreadyshown = Conversion.CInt(sessiontimes, 0);
                }

                g.UpdateSessionInt(WebConstants.SESSION_PROPERTY_SECRET_ADMIRER_MODAL_SHOW_COUNT, timesalreadyshown+1, 0); 
                slideshowSecretAdmirerPopupData.Attributes.Add("data-modal-close", base.ResourceManager.GetResource("TXT_CLOSE", this));
                slideshowSecretAdmirerPopupData.Attributes.Add("data-omniturepagename", "pop_up_SecretAdmirer");

                ucSlideshowProfileSAPopup.SlideshowDisplayType = SlideshowDisplayType.ModalPopup;
                ucSlideshowProfileSAPopup.SlideShowMode = SlideShowModeType.SecretAdmirerGame;
                ucSlideshowProfileSAPopup.SlideshowOuterContainerDivID = "slideshowSecretAdmirerPopup";
                ucSlideshowProfileSAPopup.ReloadAds = false;
                ucSlideshowProfileSAPopup.OmniturePageName = "pop_up_SecretAdmirer";
                ucSlideshowProfileSAPopup.PopulateYNMBuckets = false;
                ucSlideshowProfileSAPopup.Load(true);
            }
        }
    }
}