﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using System.Text;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Web.Analytics;
using Newtonsoft.Json;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Home.Controls
{
    public partial class MatchesSlider20 : FrameworkControl
    {
        protected int _MemberID = 0;
        protected int _PageSize = 5;
        protected string _OmnitureUsernameProfileURLTrackingParams = "";
        protected string _OmniturePhotoProfileURLTrackingParams = "";
        protected string _OmnitureViewMorePhotosProfileURLTrackingParams = "";
        protected string _OmnitureControlName = "Filmstrip";
        protected bool _hasMatches = false;
        protected bool _matchRatingEnabled = false;
        protected int _LoadingTypeID = 0; //JS loading type - 0: capped no ajax, 1: ajax only, 2: ajax on new pages only
        protected int _CappedPreloadSize = 100; //20 pages worth

        protected void Page_Load(object sender, EventArgs e)
        {
            if (g.Member != null)
            {
                _MemberID = g.Member.MemberID;
                _matchRatingEnabled = MatchRatingManager.Instance.DisplayMatchRatingOnFilmstrip(_MemberID, g.Brand);

                try
                {
                    //set up omniture tracking params for profile links
                    _OmnitureUsernameProfileURLTrackingParams = Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, g.AnalyticsOmniture.PageName, _OmnitureControlName);
                    _OmniturePhotoProfileURLTrackingParams = Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfilePhoto, g.AnalyticsOmniture.PageName, _OmnitureControlName);
                    _OmnitureViewMorePhotosProfileURLTrackingParams = Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMorePhotos, g.AnalyticsOmniture.PageName, _OmnitureControlName);

                    //get slider profiles
                    bool hasResults = false;
                    GetSearchResult getSearchResult = null;

                    SettingsManager settingManager = new SettingsManager();
                    _LoadingTypeID = settingManager.GetSettingInt("FILMSTRIP_PAGINATION_LOADING_SCENARIO", g.Brand, 2);                 

                    if (_LoadingTypeID == 0)
                    {
                        getSearchResult = HomeUtil.GetSliderProfilesJSON(g.Member, g.Brand, g, g.SearchPreferences, g.Session, 1,
                                                       _CappedPreloadSize, true, true, HomeUtil.MATCH_SLIDER_USERNAME_MAX, this);
                        if (getSearchResult != null && getSearchResult.TotalResults > _CappedPreloadSize)
                        {
                            getSearchResult.TotalResults = _CappedPreloadSize;
                        }
                    }
                    else
                    {
                        getSearchResult = HomeUtil.GetSliderProfilesJSON(g.Member, g.Brand, g, g.SearchPreferences, g.Session, 1,
                                                       _PageSize, true, true, HomeUtil.MATCH_SLIDER_USERNAME_MAX, this);   
                    }

                    //render js profile objects
                    StringBuilder preloadJS = new StringBuilder();
                    if (getSearchResult != null && getSearchResult.SearchProfileList != null)
                    {
                        if (getSearchResult.SearchProfileList.Count > 0)
                        {
                            hasResults = true;
                            if (getSearchResult.IsMatchResults)
                            {
                                _hasMatches = true;
                            }
                        }
                        string getSearchResultJSON = JsonConvert.SerializeObject(getSearchResult);
                        preloadJS.AppendLine("var getSearchResult = " + getSearchResultJSON + ";");
                        preloadJS.AppendLine("matchSliderManager.loadPage(getSearchResult);");
                    }
                    litPreloadJS.Text = preloadJS.ToString();

                    //update title
                    lnkViewMore.Title = g.GetResource("TXT_VIEW_MORE", this);
                    if (hasResults && getSearchResult.IsMOLResults)
                    {
                        txtComponentTitle.ResourceConstant = "TXT_COMPONENT_HEADER_MOL";
                        lnkViewMore.HRef = "/Applications/MembersOnline/MembersOnline.aspx";
                        lnkViewMore.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MembersOnline, WebConstants.Action.ViewMoreLink, lnkViewMore.HRef, _OmnitureControlName);
                        lnkFilmstripTitle.HRef = "/Applications/MembersOnline/MembersOnline.aspx";
                        lnkFilmstripTitle.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MembersOnline, WebConstants.Action.ViewPageTitle, lnkFilmstripTitle.HRef, _OmnitureControlName);

                    }
                    else
                    {
                        lnkViewMore.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewMoreLink, lnkViewMore.HRef, _OmnitureControlName);
                        lnkFilmstripTitle.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewPageTitle, lnkFilmstripTitle.HRef, _OmnitureControlName);
                    }

                    //update links (inlcude omniture)
                    lnkMatchPreferences.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchPreferences, WebConstants.Action.ViewPageLink, lnkMatchPreferences.HRef, _OmnitureControlName);
                    lnkSlideshow.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SlideShow, WebConstants.Action.ViewPageLink, lnkSlideshow.HRef, _OmnitureControlName);
                    lnkSearch.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.AdvanceSearch, WebConstants.Action.ViewPageLink, lnkSearch.HRef, _OmnitureControlName);
                    lnkPhotoGallery.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.PhotoGallery, WebConstants.Action.ViewPageLink, lnkPhotoGallery.HRef, _OmnitureControlName);

                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }
            }
        }
    }
}