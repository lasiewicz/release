using System;
using System.Web.UI.WebControls;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.PageElements;

namespace Matchnet.Web.Applications.Home.Controls
{
    /// <summary>
    ///		Summary description for UserProfile.
    /// </summary>
    public class UserProfile : FrameworkControl
    {
        protected HyperLink lnkUserName;
        protected Label lblRegion;
        protected Label lblAge;
        protected Matchnet.Web.Framework.Image imgProfile;
        protected Matchnet.Web.Framework.Ui.PageElements.NoPhoto noPhoto;
        protected Label lblTitle;
        protected System.Web.UI.HtmlControls.HtmlGenericControl spnUploadPhoto;
        protected Label lblGender;
        protected Matchnet.Web.Framework.Txt txtQuickTip;
        protected Matchnet.Web.Framework.Txt lnkUploadPhoto;
        protected Matchnet.Web.Framework.Txt txtProfile;
        protected Matchnet.Web.Framework.Txt txtUploadPhoto;
        protected Matchnet.Web.Framework.Txt txtEdit;
        protected Matchnet.Web.Framework.Image imgTip;
        protected PlaceHolder plcTextTip;
        protected PlaceHolder plcImageTip;
        int MAX_WORD_LENGTH = 10;
        int ESSAY_CHARACTERS_PER_BLOCK = 20;

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                BindYourProfile(g.SearchPreferences);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        private void BindYourProfile(SearchPreferenceCollection prefs)
        {
            Matchnet.Member.ServiceAdapters.Member member;

            member = MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), MemberLoadFlags.None);

            lnkUserName.Text = member.GetUserName(_g.Brand);
            lblAge.Text = FrameworkGlobals.GetAge(member, g.Brand).ToString();
            lblRegion.Text = FrameworkGlobals.GetRegionString(member.GetAttributeInt(g.Brand, "RegionID"), g.Brand.Site.LanguageID, false);

            string headline = ProfileDisplayHelper.GetFormattedEssayText(member, g, "AboutMe", ESSAY_CHARACTERS_PER_BLOCK);

            // HtmlEncode the formatted text incase the user somehow put htmlencoded characters into the essay.
            lblTitle.Text = System.Web.HttpUtility.HtmlEncode(Matchnet.Lib.Util.StringUtil.MaxWordLength(FrameworkGlobals.Ellipsis(headline, 75), MAX_WORD_LENGTH));

            //strAboutMeIntro = ProfileDisplayHelper.GetFormattedEssayText(_MemberProfile, g, "AboutMe", ESSAY_CHARACTERS_PER_BLOCK);
            //AboutMeIntro.Text = FrameworkGlobals.Ellipsis(strAboutMeIntro, MAX_ABOUT_ME_INTRO_LENGTH);


            // Thumbnail only shows when it's approved and it's set to 1st in list order
            var photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, member, g.Brand);

            if(photo != null)
            {
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                imgProfile.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, member, g.Brand,
                                                                                      photo, PhotoType.Thumbnail,
                                                                                      PrivatePhotoImageType.Thumb,
                                                                                      NoPhotoImageType.Thumb);

                if (MemberPhotoDisplayManager.Instance.IsPrivatePhoto(photo, g.Brand))
                {
                    imgProfile.Visible = false;
                    noPhoto.MemberID = member.MemberID;
                    noPhoto.Mode = NoPhoto.PhotoMode.NoPrivatePhoto;
                    noPhoto.DestURL = "/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999";
                    noPhoto.Visible = true;

                    spnUploadPhoto.Visible = false;
                }
                else
                {
                    imgProfile.Visible = true;
                    noPhoto.Visible = false;
                    spnUploadPhoto.Visible = false;
                }
            }
            else
            {
                // viewing current member, display proper text and establish upload image link
                imgProfile.Visible = false;
                noPhoto.MemberID = member.MemberID;
                noPhoto.Mode = NoPhoto.PhotoMode.UploadPhotoNow;
                noPhoto.DestURL = "/Applications/MemberProfile/MemberPhotoUpload.aspx";
                noPhoto.Visible = true;
            }

            if (spnUploadPhoto.Visible)
            {
                if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_PHOTO_UPLOAD_TIP_AS_IMAGE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToLower() == "true")
                {
                    plcTextTip.Visible = false;
                    plcImageTip.Visible = true;
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
