<%@ Control Language="c#" AutoEventWireup="false" Codebehind="HotListDetails.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Controls.HotListDetails" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="homePref">
<span class="homePrefTitle">
		<mn:txt runat="server" id="txtHotLists" resourceConstant="TXT_HOT_LISTS" expandImageTokens="True" /></span>
		<span class="homePrefText">
		<mn:Image height="14" ResourceConstant="ALT_CLICK_ICON" titleResourceConstant="TTL_CLICK_YOU_BOTH_SAID_YES_EXCLAMATION" hspace="0" FileName="icon-click-yy.gif" width="25"
			align="absMiddle" vspace="4" border="0" runat="server" id="imgYY" />
			<asp:HyperLink Runat="server" ID="eachSaidYesLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-15" /></span>

	<!-- "Members You've" Title  -->
	<span class="homePrefSubTitle">
		&nbsp;<mn:Image runat="server" id="imgHotMem" border="0" FileName="icon_gry_mem_hotlist.gif" align="absmiddle" />
			 <strong><mn:TXT runat="server" id="txtMembersWho" resourceConstant="MEMBERS_WHOVE" /></strong>
	</span>

	<!-- Hotlist links and count-->
		<asp:HyperLink Runat="server" CssClass="homePrefText" ID="emailedYouLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-4" />
		<asp:HyperLink Runat="server" CssClass="homePrefText" ID="listedYouLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-2" />
		<asp:HyperLink Runat="server" CssClass="homePrefText" ID="teasedYouLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-3" />
		<asp:HyperLink Runat="server" CssClass="homePrefText" ID="imdYouLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-5" />
		<asp:HyperLink Runat="server" CssClass="homePrefText" ID="viewedYouLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-1" />                
                <asp:HyperLink Runat="server" CssClass="homePrefText" ID="ECardYouLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-25" />
	
	<!-- "Members You've" Title  -->
	<span class="homePrefSubTitle">
		&nbsp;<mn:Image runat="server" id="imgHotYou" border="0" FileName="icon_gry_you_hotlist.gif" align="absmiddle" />
			<strong><mn:txt runat="server" id="Txt3" ResourceConstant="MEMBERS_YOUVE" /></strong>
	</span>
	
	<!-- Hotlist links and count-->
	<asp:HyperLink Runat="server" CssClass="homePrefText" ID="emailedLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-7" />
	<asp:HyperLink Runat="server" CssClass="homePrefText" ID="hotListedFavoriteLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-0" />
	<asp:HyperLink Runat="server" CssClass="homePrefText" ID="teasedLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-6" />
	<asp:HyperLink Runat="server" CssClass="homePrefText" ID="imdLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-8" />
	<asp:HyperLink Runat="server" CssClass="homePrefText" ID="viewedLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-9" />
        <asp:HyperLink Runat="server" CssClass="homePrefText" ID="YouECardLink" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-24" />
</div>
