﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Home.Controls
{
    public partial class SparkHomeTopB : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            IntitializePage();
        }

        private void IntitializePage()
        {
            try
            {
                //profile status summary
                ucProfileStatusSummary.EntryPoint = Framework.Ui.BreadCrumbHelper.EntryPoint.HomePage;

                //hotlist buttons
                var listManager = new ListManager();
                var emailNotificationCount = listManager.GetUnseenNotificationCount(g.List, g.Member, g.Brand,
                                                                                    HotListCategory.WhoEmailedYou);
                var flirtedNotificationCount = listManager.GetUnseenNotificationCount(g.List, g.Member, g.Brand,
                                                                                      HotListCategory.WhoTeasedYou);
                var viewedNotificationCount = listManager.GetUnseenNotificationCount(g.List, g.Member, g.Brand,
                                                                                     HotListCategory.
                                                                                         WhoViewedYourProfile);

                hotListButtonEmailed.LoadHotListButton(HotListCategory.WhoEmailedYou, "TXT_EMAILED_YOU",
                                                       emailNotificationCount);
                hotListButtonFlirted.LoadHotListButton(HotListCategory.WhoTeasedYou, "TXT_FLIRTED_WITH_YOU",
                                                       flirtedNotificationCount);
                hotListButtonViewed.LoadHotListButton(HotListCategory.WhoViewedYourProfile, "TXT_VIEWED_YOU",
                                                      viewedNotificationCount);
                hotListButtonFavorites.LoadHotListButton(HotListCategory.Default, "TXT_YOUR_FAVORITES");

                //view more link
                hotlistLnkViewMore.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.View, WebConstants.Action.ViewMoreLink, hotlistLnkViewMore.HRef, "Hotlist Module");
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}