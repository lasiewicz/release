﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotlistScroller.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Controls.HotlistScroller" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="LastTime" Src="/Framework/Ui/BasicElements/LastTime.ascx" %>


<asp:PlaceHolder runat="server" ID="phSubscriberProfiles" Visible="False">
<div class="panel-scroll">
    <h2>
        <mn:Txt runat="server" ID="txtHotlistTitle"/> 
        <asp:PlaceHolder runat="server" ID="phHotlistNotificationCount" Visible="False">
            <em class="hint"><asp:Literal runat="server" ID="litNotificationCount"></asp:Literal></em>
        </asp:PlaceHolder>
    </h2>
    <div id="scrollContainer" runat="server" class="scroll">
        <ul>
            <asp:Repeater runat="server" ID="rpProfiles" OnItemDataBound="rpProfiles_ItemDataBound">
                <ItemTemplate>
                    <li>
                        <div class="picture">
                            <asp:HyperLink runat="server" ID="lnkProfilePhoto">
                                <span class="photo">
                                    <asp:Image runat="server" ID="imageProfilePhoto"/>
                                </span>
                            </asp:HyperLink>
                        </div>
                        <div class="info">
                            <asp:HyperLink runat="server" ID="lnkUsername">
                                <asp:Literal runat="server" ID="litUsername"></asp:Literal>
                            </asp:HyperLink>
                            <span><asp:Literal runat="server" ID="litHotlistText"></asp:Literal></span>
                            <em class="time-stamp"><uc2:LastTime ID="lastTimeHotlistDate" runat="server" /></em>
                        </div>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
    <mn:Txt ID="txtViewAll" ResourceConstant="TXT_VIEW_ALL" TitleResourceConstant="TXT_VIEW_ALL_ALT" CssClass="view-more" runat="server" />
</div>
    <script type="text/javascript">
        $j(function () {
            var hotlistScrollerContainerID = '<%=scrollContainer.ClientID%>',
                $scroller = $j('#' + hotlistScrollerContainerID),
                $scrollerApi = null,
                elmHeight = 0,
                scrollBy = 3,
                scrollSettings = {
                    showArrows: true,
                    verticalArrowPositions: 'split'
                };

            $scroller.bind('jsp-initialised', function () {
                //update values after initialisation
                $scrollerApi = $scroller.data('jsp');
                elmHeight = $scroller.find('li').eq(0).outerHeight();
                //console.log('elmHeight: ', elmHeight);


                $scroller.find('.jspArrowDown').on('click', function () {
                    $scrollerApi.scrollByY(elmHeight * scrollBy);
                    //console.log('scrollByY: ', elmHeight * scrollBy);
                });
                $scroller.find('.jspArrowUp').on('click', function () {
                    $scrollerApi.scrollByY(elmHeight * scrollBy * -1);
                    //console.log('scrollByY: ', elmHeight * scrollBy * -1);
                });
            }).jScrollPane(scrollSettings);

        });
    </script>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="phNonSubscribers" Visible="false">
    <div class="panel non-sub">
        <h2>
            <mn:Txt runat="server" ID="txtHotlistTitleNonSub" Href="/Applications/Subscription/Subscribe.aspx?prtid={0}"/>
            <asp:PlaceHolder runat="server" ID="phHotlistNotificationCountNonSub" Visible="False">
                <em class="hint"><asp:Literal runat="server" ID="litNotificationCountNonSub"></asp:Literal></em>
            </asp:PlaceHolder>
        </h2>
        
        <asp:PlaceHolder runat="server" ID="phNonSubSubScribe" Visible="false">
            <div class="image">
                <%--Female photo --%>
                <asp:PlaceHolder runat="server" ID="phFemalePhotoNonSub" Visible="false">
                    <a href="/Applications/Subscription/Subscribe.aspx?prtid={0}" runat="server" id="lnkSearchFemaleNonSub"><img src="/img/no-photo-m-f.png"/></a>
                </asp:PlaceHolder>
        
                <%--Male photo --%>
                <asp:PlaceHolder runat="server" ID="phMalePhotoNonSub" Visible="false">
                    <a href="/Applications/Subscription/Subscribe.aspx?prtid={0}" runat="server" id="lnkSearchMaleNonSub"><img src="/img/no-photo-m-m.png"/></a>
                </asp:PlaceHolder>
            </div>
            <div class="cta-wrapper">
                <div class="ribbon-fold-both">
                    <a href="/Applications/Subscription/Subscribe.aspx?prtid={0}" runat="server" id="lnkSubscribe" class="cta">
                        <mn:Txt runat="server" ID="txtSubscribeNow" ResourceConstant="TXT_SUBSCRIBE_NOW"/>
                    </a>
                </div>
            </div>
            <p><mn:Txt runat="server" ID="txtMembersHave"/></p> 
        </asp:PlaceHolder>
        
        <asp:PlaceHolder runat="server" ID="phNonSubAllAccess" Visible="false">
            <div class="image">
                <%--Female photo --%>
                <asp:PlaceHolder runat="server" ID="phFemalePhotoNonSubAllAccess" Visible="false">
                    <a href="/Applications/Email/MailBox.aspx" runat="server" id="lnkSearchFemaleNonSubAllAccess"><img src="/img/no-photo-m-f.png"/></a>
                </asp:PlaceHolder>
        
                <%--Male photo --%>
                <asp:PlaceHolder runat="server" ID="phMalePhotoNonSubAllAccess" Visible="false">
                    <a href="/Applications/Email/MailBox.aspx" runat="server" id="lnkSearchMaleNonSubAllAccess"><img src="/img/no-photo-m-m.png"/></a>
                </asp:PlaceHolder>
            </div>
            <asp:Literal runat="server" ID="litAllAccessCopy"></asp:Literal>
        </asp:PlaceHolder>
        
                       
        
    </div>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="phNoProfiles" Visible="False">
    <div class="panel no-activity">
        <h2>
            <mn:Txt runat="server" ID="txtHotlistTitleNoContent" Href="/Applications/Search/SearchResults.aspx"/> 
        </h2>
        <div class="image">
            <%--Female photo --%>
            <asp:PlaceHolder runat="server" ID="phFemalePhotoNoContent" Visible="false">
                <a href="/Applications/Search/SearchResults.aspx" runat="server" id="lnkSearchFemaleNoContent"><img src="/img/no-photo-m-f.png"/></a>
            </asp:PlaceHolder>
        
            <%--Male photo --%>
            <asp:PlaceHolder runat="server" ID="phMalePhotoNoContent" Visible="false">
                <a href="/Applications/Search/SearchResults.aspx" runat="server" id="lnkSearchMaleNoContent"><img src="/img/no-photo-m-m.png"/></a>
            </asp:PlaceHolder>
        </div>
        <asp:Literal runat="server" ID="litHotlistNoContentCopy"></asp:Literal>
    </div>
</asp:PlaceHolder>
