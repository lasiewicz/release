﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchesSlider.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Controls.MatchesSlider" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<h2 class="component-header"><mn:Txt ID="txtComponentTitle" runat="server" ResourceConstant="TXT_COMPONENT_HEADER_MATCHES" /> <small><a href="/Applications/Search/SearchResults.aspx?SearchOrderBy=2" id="lnkViewMore" runat="server"><mn:Txt runat="server" ResourceConstant="TXT_VIEW_MORE" /> </a></small></h2>
<div id="match-slider" class="match-slider clearfix grad-base">
    <div id="ajax-loader" class="ajax-loader">
        <mn:Image id="mnimage4704" runat="server" filename="ajax-loader.gif" titleresourceconstant="" resourceconstant="" />
        <p><mn:Txt id="mntxt7220" runat="server" resourceconstant="TXT_LOADING" expandimagetokens="true" /></p>
    </div>
    <span class="prev button"><button id="match-slider-button-prev" class="spr-btn btn-arrow-cir-prev disabled">&#9668;</button></span>
    <span class="next button"><button id="match-slider-button-next" class="spr-btn btn-arrow-cir-next">&#9658;</button></span>

    <%--Profiles--%>
    <div id="match-slider-profiles-container" class="match-slider-container"></div>
</div>
<div id="match-slider-none" class="match-slider clearfix grad-base" style="display:none;">
    <%--No Matches--%>
    <div class="tips">
        <p><mn:Txt ID="txtNoMatches" runat="server" ResourceConstant="TXT_NOMATCHES_BODY" /></p>
    </div>
</div>

<script type="text/javascript">
    function spark_home_match_slider_profile() {
        this.profileIndex = 0;
        this.profileOrdinal = 0;
        this.photoURL = '';
        this.memberID = 0;
        this.usernameProfileURL = '';
        this.photoProfileURL = '';
        this.username = '';
        this.usernameShort = '';
        this.highlighted = false;
        this.matchRating = 0;
        this.matchRatingDisplay = '';
    }

    function spark_home_match_slider_manager() {
        this.profileList = [];
        this.memberID = 0;
        this.startRow = 1;
        this.pageSize = 7;
        this.currentStartIndex = 0;
        this.totalResultCount = 0;
        this.updateInProgress = false;
        this.isVerbose = true;
        this.yesSliderContainerID = '';
        this.noSliderContainerID = '';
        this.profileContainerID = '';
        this.buttonPrevID = '';
        this.buttonNextID = '';
        this.ajaxLoaderID = '';
        this.checkMatches = true;
        this.checkMOL = true;
        this.matchRatingEnabled = false;
        this.omnitureUsernameProfileURLTrackingParams = '';
        this.omniturePhotoProfileURLTrackingParams = '';
        this.addProfile = function (profileObj) {
            spark.util.addItem(this.profileList, profileObj);
        }
        this.createProfile = function (searchProfile){
            var profile1 = new spark_home_match_slider_profile();
            profile1.profileIndex = matchSliderManager.profileList.length;
            profile1.profileOrdinal = searchProfile.Ordinal;
            profile1.photoURL = searchProfile.PhotoURL;
            profile1.memberID = searchProfile.MemberID;
            profile1.usernameProfileURL = searchProfile.ProfileURL + "&" + matchSliderManager.omnitureUsernameProfileURLTrackingParams;
            profile1.photoProfileURL = searchProfile.ProfileURL + "&" + matchSliderManager.omniturePhotoProfileURLTrackingParams;
            profile1.username = searchProfile.Username;
            profile1.highlighted = searchProfile.Highlighted;
            profile1.usernameShort = searchProfile.UsernameShort;
            profile1.matchRating = searchProfile.MatchRating;
            profile1.matchRatingDisplay = searchProfile.MatchRating;
            if (searchProfile.MatchRating < 50){
                profile1.matchRatingDisplay = "&lt;50";
            }
            return profile1;
        }
        this.getCurrentPageNumber = function(){
            var currentPage = 1;
            if (this.currentStartIndex > 0){
                currentPage = Math.ceil((this.currentStartIndex + this.pageSize) / this.pageSize);
            }
            return currentPage;
        }
        this.onNextPageClick = function () {
            //paginate to next page
            if (!this.updateInProgress) {
                this.getSliderProfiles(this.startRow + this.pageSize);
            }
        }
        this.onPrevPageClick = function () {
            //paginate to previous page
            if (!this.updateInProgress) {
                this.getSliderProfiles(this.startRow - this.pageSize);
            }
        }
        this.getSliderProfiles = function(newStartRow){
            if (!this.updateInProgress) {
                var requiresAjax = true;
                //set startRow
                var existingStartRow = this.startRow;
                this.startRow = newStartRow;
                if (this.profileList.length <= 0){
                    this.startRow = 1;
                }

                if (this.startRow < this.profileList.length){
                    requiresAjax = false;
                }

                if (!this.checkMatches && !this.checkMOL){
                    requiresAjax = false;
                }
                            
                if (requiresAjax){
                    $j('#' + matchSliderManager.ajaxLoaderID).show();

                    //get profiles from service
                    $j.ajax({
                        type: "POST",
                        url: "/Applications/API/SearchResults.asmx/GetSliderProfiles",
                        data: "{memberID:" + matchSliderManager.memberID + ", startRow:" + matchSliderManager.startRow + ", pageSize:" + matchSliderManager.pageSize + ", checkMatches: " + matchSliderManager.checkMatches + ", checkMOL: " + matchSliderManager.checkMOL + "}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            matchSliderManager.updateInProgress = true;
                        },
                        complete: function () {
                            matchSliderManager.updateInProgress = false;
                        },
                        success: function (result) {
                            var getSearchResult = (typeof result.d == 'undefined') ? result : result.d;
                            if (getSearchResult.Status != 2) {
                                $j('#' + matchSliderManager.ajaxLoaderID).hide();
                                if (matchSliderManager.isVerbose) {
                                    alert(getSearchResult.StatusMessage);
                                }
                            }
                            else {
                                ////load profiles
                                if (getSearchResult.SearchProfileList.length > 0) {
                                    try{
                                        console.log('getSearchResult: ', getSearchResult);
                                    }
                                    catch(e) {}
                                    matchSliderManager.loadPage(getSearchResult);

                                    //update omniture
                                    if (s != null) {
                                        PopulateS(true); //clear existing values in omniture "s" object
                                        var originalPageName = s.pageName;
                                        s.pageName = s.pageName + " - Filmstrip";
                                        s.eVar25 = "" + matchSliderManager.getCurrentPageNumber() + "";
                                        s.t();
                                        s.pageName = originalPageName;
                                    }
                                }
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $j('#' + matchSliderManager.ajaxLoaderID).hide();
                            if (matchSliderManager.isVerbose) {
                                alert(textStatus);
                            }
                            try{
                                console.log('Error getting next page: ' + textStatus);
                            }
                            catch(e){}
                        }
                    });
                }
                else{
                    matchSliderManager.loadPage(null);
                }
            }
        }
        this.loadPage = function (getSearchResult){
            var $ajaxLoader = $j('#' + this.ajaxLoaderID),
                $yesSliderContainer = $j('#' + this.yesSliderContainerID),
                $noSliderContainer = $j('#' + this.noSliderContainerID);

            $ajaxLoader.show();

            ////load profiles from JSON
            if (getSearchResult != null) {
                if (getSearchResult.TotalResults > 0 || matchSliderManager.totalResultCount == 0){
                    matchSliderManager.totalResultCount = getSearchResult.TotalResults;
                }
                matchSliderManager.checkMatches = getSearchResult.IsMatchResults;
                matchSliderManager.checkMOL = getSearchResult.IsMOLResults;
                if (getSearchResult.SearchProfileList.length > 0){
                    for (var i = 0; i < getSearchResult.SearchProfileList.length; i++) {
                        var searchProfile = getSearchResult.SearchProfileList[i];
                        var profile1 = matchSliderManager.createProfile(searchProfile);
                        matchSliderManager.addProfile(profile1);
                    }
                }
            }

            if (this.profileList.length > 0){
                
                //show profiles
                $yesSliderContainer.show();
                $noSliderContainer.hide();

                //load page html
                var startIndex = this.startRow - 1,
                    existingStartIndex = this.currentStartIndex,
                    endIndex = null;
                
                if (startIndex <= 0){
                    startIndex = 0;
                }
                this.currentStartIndex = startIndex;

                endIndex = startIndex + this.pageSize - 1;

                if (endIndex >= this.profileList.length){
                    endIndex = this.profileList.length - 1;
                }

                var photohtml = '',
                    phototemplate = '<li class="mini-profile simple"><a href="{{photoProfileURL}}" title="{{username}}"><span class="photo"><img src="{{photoURL}}" alt="{{username}}" /></span></a><a href="{{usernameProfileURL}}" title="{{username}}"><span class="info">{{usernameShort}}</span></a></li>',
                    phototemplateWithMatchRating = '<li class="mini-profile simple"><span class="rate"><b>{{matchRatingDisplay}}</b>%</span><a href="{{photoProfileURL}}" title="{{username}}"><span class="photo"><img src="{{photoURL}}" alt="{{username}}" /></span></a><a href="{{usernameProfileURL}}" title="{{username}}"><span class="info">{{usernameShort}}</span></a></li>';
                
                for (var i = startIndex; i <= endIndex; i++){
                    if (this.matchRatingEnabled) {
                        photohtml += Mustache.to_html(phototemplateWithMatchRating, this.profileList[i]);
                    } else {
                        photohtml += Mustache.to_html(phototemplate, this.profileList[i]);  
                    }
                }

                var $newItems = $j('<ul>').html(photohtml),
                    $container = $j('#' + this.profileContainerID),
                    scrollWidth = $container.innerWidth();
                
                function scroller($items, dir, callback){
                    $items.animate({
                        left: (dir == 'left') ? '-=' + scrollWidth : '+=' + scrollWidth
                    }, {queue: false, duration:1500, complete: callback});
                }

                if(existingStartIndex < this.currentStartIndex){
                    var $oldItems = $container.find('ul');

                    $container.append($newItems);
                    $newItems.css({left: scrollWidth + 'px'});
                    scroller($oldItems, 'left', function(){$oldItems.remove()});
                    scroller($newItems, 'left');

                } else if(existingStartIndex > this.currentStartIndex){
                    var $oldItems = $container.find('ul');

                    $container.prepend($newItems);
                    $newItems.css({left: '-' + scrollWidth + 'px'});
                    scroller($oldItems, '', function(){$oldItems.remove()});
                    scroller($newItems, '');

                } else if(existingStartIndex == 0){
                    $container.html($newItems);
                }

                try { console.log('Loaded match slider page: ' + this.getCurrentPageNumber() + ', Total Results: ' + this.totalResultCount);
                } catch(e) {}

                //hide or show next/prev
                if (startIndex > 0){
                    $j('#' + this.buttonPrevID).removeClass('disabled');
                }else{
                    $j('#' + this.buttonPrevID).addClass('disabled');
                }

                if ((endIndex < (this.profileList.length - 1)) || (endIndex < this.totalResultCount - 1)){
                    $j('#' + this.buttonNextID).removeClass('disabled');
                }else{
                    $j('#' + this.buttonNextID).addClass('disabled');
                }
            }
            else{
                //show no profiles content
                $yesSliderContainer.hide();
                $noSliderContainer.show();
            }

            $ajaxLoader.hide();
        }
    }

    //initialize objects
    var matchSliderManager = new spark_home_match_slider_manager();
    matchSliderManager.memberID = <%=_MemberID%>;
    matchSliderManager.pageSize = <%=_PageSize%>;
    matchSliderManager.profileContainerID = 'match-slider-profiles-container';
    matchSliderManager.yesSliderContainerID = 'match-slider';
    matchSliderManager.noSliderContainerID = 'match-slider-none';
    matchSliderManager.buttonNextID = 'match-slider-button-next';
    matchSliderManager.buttonPrevID = 'match-slider-button-prev';
    matchSliderManager.ajaxLoaderID = 'ajax-loader';
    matchSliderManager.omnitureUsernameProfileURLTrackingParams = '<%=_OmnitureUsernameProfileURLTrackingParams%>';
    matchSliderManager.omniturePhotoProfileURLTrackingParams = '<%=_OmniturePhotoProfileURLTrackingParams%>';
    matchSliderManager.matchRatingEnabled = <%=_matchRatingEnabled.ToString().ToLower()%>;

    $j(function () {                
        $j('#match-slider-button-prev').click(function(e) {
            e.preventDefault();
            if (!$j('#match-slider-button-prev').hasClass('disabled')){
                matchSliderManager.onPrevPageClick();
            }
        });

        $j('#match-slider-button-next').click(function(e) {
            e.preventDefault();
            if (!$j('#match-slider-button-next').hasClass('disabled')){
                matchSliderManager.onNextPageClick();
            }
        });

        //no preload
        //matchSliderManager.getSliderProfiles(1);

        //yes preloaded
        <asp:Literal ID="litPreloadJS" runat="server"></asp:Literal>
    });

</script>
