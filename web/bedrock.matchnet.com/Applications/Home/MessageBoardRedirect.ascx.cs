using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Home
{
	using System;
	/// <summary>
	///		Summary description for MessageBoardRedirect.
	/// </summary>
	public class MessageBoardRedirect : FrameworkControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			string redirectTo = "boards";
			
			if (Request.QueryString["redirectTo"] != null)
			{
				if (Request.QueryString["redirectTo"].Length > 0)
				{
					redirectTo = Request.QueryString["redirectTo"];
				}
			}				
				
			string url = FrameworkGlobals.BuildConnectFrameworkLink(redirectTo, g, true);
			
			if(g.Member == null)
			{
				Response.Redirect("/Applications/Logon/logon.aspx?" + WebConstants.URL_PARAMETER_NAME_SPARKWS_CONTEXT +"=true&DestinationUrl=" +Server.UrlEncode(url));
			}
			else
			{
				Response.Redirect(url, true);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
