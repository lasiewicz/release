﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Jump
{
    public partial class JumpEncore : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string encoreSiteID;
            if (Request["encsiteid"] != null)
                encoreSiteID = Request["encsiteid"];
            else
                encoreSiteID = Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENCORE_SITEID", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

            if(encoreSiteID == "-1")
            {
                throw new ApplicationException("Page does not exist.");
            }
        
            if (g.Member != null && g.Brand != null)
            {
                // zip code determination based on RegionID
                int regionID = g.Member.GetAttributeInt(g.Brand, "RegionID", Constants.NULL_INT);
                Matchnet.Content.ValueObjects.Region.RegionLanguage region = Content.ServiceAdapters.RegionSA.Instance.RetrievePopulatedHierarchy(regionID, g.Brand.Site.LanguageID);

                if (region != null)
                {
                    string clientOrderID = g.Member.GetAttributeText(g.Brand, "EncoreClientOrderID", string.Empty);

                    // if member doesn't have the OID yet, generate and store
                    if (clientOrderID == string.Empty)
                    {    
                        clientOrderID = GenerateOID();
                        g.Member.SetAttributeText(g.Brand, "EncoreClientOrderID", clientOrderID, Matchnet.Member.ValueObjects.TextStatusType.None);
                        Member.ServiceAdapters.MemberSA.Instance.SaveMember(g.Member);
                    }

                    string link = string.Format(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENCORE_LANDING_LINK") +
                    "?siteID={0}&OID={1}&email={2}&zip={3}", encoreSiteID, clientOrderID, g.Member.EmailAddress, region.PostalCode);

                    // Redirect with the appropriate response code: 302
                    g.Transfer(link);
                }
            }
            else
            {
                throw new ApplicationException("Current request cannot be processed.");
            }
            
        }

        /// <summary>
        /// Generates a random OID
        /// </summary>
        /// <returns></returns>
        private string GenerateOID()
        {
            // clientOrderID = 10 digits of random characters plus last 5 digits used for brandID
            Guid aguid = new Guid();
            aguid = Guid.NewGuid();
            string brandString;
            if (g.Brand.BrandID.ToString().Length < 5)
            {
                brandString = "0" + g.Brand.BrandID.ToString();
            }
            else
            {
                brandString = g.Brand.BrandID.ToString();
            }
            
            return aguid.ToString("N").Substring(0, 10) + brandString;
        }
    }
}