﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Applications.Search.XMLPreference
{
    public class SearchSectionPreference
    {
        [XmlAttribute()]
        public string Name { get; set; }
        [XmlAttribute()]
        public string ResourceConstant { get; set; }
        [XmlAttribute()]
        public bool Collapsible { get; set; }
        [XmlAttribute()]
        public bool Required { get; set; }
        [XmlAttribute()]
        public string MaskValues { get; set; }
        [XmlAttribute()]
        public string MaskResources { get; set; }
        [XmlAttribute()]
        public bool AutoSelectHigherValues { get; set; }
        [XmlIgnore()]
        public bool IncludedInSearch { get; set; }

        public SearchSectionPreference()
        {
            Required = false;
        }
    }
}
