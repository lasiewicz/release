﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Applications.Search.XMLPreference
{
    [Serializable]
    public class SearchSection
    {
        [XmlAttribute()]
        public string Name { get; set; }
        [XmlElement("Preference")]
        public List<SearchSectionPreference> SearchSectionPreferenceList { get; set; }

    }
}
