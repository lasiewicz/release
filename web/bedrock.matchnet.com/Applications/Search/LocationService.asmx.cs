﻿using System;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using Matchnet.Web.Facelink.CitySearch;

namespace Matchnet.Web.Applications.Search
{
	/// <summary>
	/// Provides methods for getting locations from partial locations.
	/// </summary>
	[WebService(Namespace = "http://jdate.com/services/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	[ScriptService]
	public class LocationService : WebService
	{
		/// <summary>
		/// Returns a list of locations from a partial location.
		/// </summary>
		/// <param name="partialLocation">The partial location.</param>
		/// <returns>A list of locations</returns>
		[WebMethod]
		[ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
		public String[] GetLocations(String partialLocation)
		{
			if (String.IsNullOrEmpty(partialLocation)) return new String[] { };

			var client = new SearchPortTypeClient();

			return client.getSearchResults(
				new searchRequest { partialCity = partialLocation }
				).Select(result => result.city).ToArray();
		}
	}
}
