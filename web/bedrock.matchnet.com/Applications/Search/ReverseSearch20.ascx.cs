﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Framework.Util;
using System.Data;
using Matchnet.Web.Framework.Ui.ProfileElements;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework.Search;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.Search
{
    public partial class ReverseSearch20 : FrameworkControl
    {
        protected Int32 SearchOrderBy;
        protected string ChangeVoteMessage;
        protected PlaceHolder plcPromotionProfile;
        private bool _hasSpotlightProfile = false;
        private bool _hasSearchOrderByChanged = false;
        private Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType _ListViewType = ResultsViewType.ListViewType.gallery;

        protected bool ForcedOnlyPhotos
        {
            get
            {
                return Conversion.CBool(RuntimeSettings.GetSetting("REVERSE_SEARCH_FORCE_PHOTOS", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            }
        }

        protected Boolean OnlyPhotos
        {
            get
            {
                if (ForcedOnlyPhotos) return true;

                if (!String.IsNullOrEmpty(Request.QueryString[Framework.Search.Constants.ReverseSearch.Parameter.OnlyPhotos]))
                {
                    if (Request.QueryString[Framework.Search.Constants.ReverseSearch.Parameter.OnlyPhotos].ToLower() == "true")
                        return true;
                    else
                        return false;
                }

                return false;
            }
        }

        #region Event Handlers
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                ChangeVoteMessage = g.GetResource("ARE_YOU_SURE_YOU_WANT_TO_CHANGE", this);
                //SearchPreferences.Href = "/Applications/Search/SearchPreferences.aspx";

                // check to see if the current user access the site has any search preferences specified,
                // if not then the user is redirected to SearchPrefs for search specifications, otherwise
                // the results of the search are displayed
                SearchPreferenceCollection searchPreferences = g.SearchPreferences;

                //Check to see if a search order has been specified -- default to Most Popular if not.
                if (Request[WebConstants.URL_PARAMETER_NAME_SEARCHORDERBY] == null)
                {
                    //07062011 TL: Do not override saved search order with default
                    if (!String.IsNullOrEmpty(g.SearchPreferences["SearchOrderBy"]))
                    {
                        SearchOrderBy = Convert.ToInt32(g.SearchPreferences["SearchOrderBy"]);
                    }

                    if (SearchOrderBy <= 0)
                    {
                        SearchOrderBy = (Int32)Matchnet.Search.Interfaces.QuerySorting.JoinDate;
                    }
                }
                else
                {
                    SearchOrderBy = Convert.ToInt32(Request[WebConstants.URL_PARAMETER_NAME_SEARCHORDERBY]);

                    //check if search order by has changed
                    if (!String.IsNullOrEmpty(g.SearchPreferences["SearchOrderBy"])
                        && g.SearchPreferences["SearchOrderBy"] != SearchOrderBy.ToString())
                    {
                        _hasSearchOrderByChanged = true;
                    }
                }

                if (SearchOrderBy == (int)Matchnet.Search.Interfaces.QuerySorting.Popularity
                    && !SearchUtil.IsSearchSortByPopularityEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                {
                    SearchOrderBy = (Int32)Matchnet.Search.Interfaces.QuerySorting.JoinDate;
                }

                g.SearchPreferences["SearchOrderBy"] = SearchOrderBy.ToString();

                // Wire up the List Navigation. (Pagination uses these labels in ResultListHandler.cs)
                g.ListNavigationTop = lblListNavigationTop;
                g.ListNavigationBottom = lblListNavigationBottom;

                // Set results page size
                SearchResultList.PageSize = SearchUtil.GetSearch30PageSize(g);

                // Add handler to perform additional tasks after search results have been loaded
                SearchResultList.ResultsLoaded += new Matchnet.Web.Framework.Ui.SearchElements.ResultList.ResultsLoadedEventHandler(SearchResultList_ResultsLoaded);

            }
            catch (Exception ex) { g.ProcessException(ex); }
        }

        public void SearchResultList_ResultsLoaded(System.Collections.Generic.List<int> SearchIDs)
        {
            showSpotlightProfile(SearchIDs);
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                string breadCrumbTrailResourceConstant = "NAV_SUB_MATCHES";

                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource(breadCrumbTrailResourceConstant, this),
                                                            g.AppPage.App.DefaultPagePath);

                }
                if (g.BreadCrumbTrailFooter != null)
                {
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource(breadCrumbTrailResourceConstant, this),
                                                           g.AppPage.App.DefaultPagePath);
                }

                //this is used for QA/TEST purpose, allows to ignore search cache
                if (!Page.IsPostBack && !String.IsNullOrEmpty(Request["ignoreCachedSearchResults"]) && Request["ignoreCachedSearchResults"] == "true")
                {
                    SearchResultList.IgnoreAllSearchResultsCache = true;
                }

                BindSortDropDown();
                _ListViewType = idResultsViewType.GetSearchResultViewMode();
                SearchResultList.GalleryView = idResultsViewType.GalleryViewFlag;

                SearchResultList.PhotoOnly = OnlyPhotos;
                SearchResultList.LoadResultList();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            if (ForcedOnlyPhotos)
            {
                pnlOnlyPhotos.Visible = false;
            }
            onlyProfilesWithPhotos.Checked = OnlyPhotos;
            hidPhotoOnly.Value = OnlyPhotos ? "true" : "false";

            // Omniture page name override. This cannot be done in Omniture.ascx.cs because the gallery/list view mode is set here which gets executed after Omniture code
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                _g.AnalyticsOmniture.PageName = " Matches_Results_Reverse";
                _g.AnalyticsOmniture.Evar2 = " Matches_Results_Reverse";
                _g.AnalyticsOmniture.AddEvent("event2");
                _g.AnalyticsOmniture.Prop45 = GetSearchOrderBy();
                if (_hasSearchOrderByChanged)
                {
                    _g.AnalyticsOmniture.Prop45 = "changed_" + _g.AnalyticsOmniture.Prop45;
                }

                _g.AnalyticsOmniture.Prop45 += "_" + _ListViewType.ToString();

                if (_hasSpotlightProfile)
                {
                    _g.AnalyticsOmniture.AddEvent("event13");
                    _g.AnalyticsOmniture.AddProductEvent("event13=1");
                }

            }
                
        }

        private void rptSearchOption_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            DataRowView row = (DataRowView)e.Item.DataItem;
            Literal litOption = (Literal)e.Item.FindControl("litOption");
            string selectedText = (row["Value"].ToString() == SearchOrderBy.ToString()) ? "selected=\"selected\"" : "";
            litOption.Text = "<option value=\"" + row["Value"].ToString() + "\" " + selectedText + ">" + row["Content"].ToString() + "</option>";

        }

        #endregion

        #region Private Methods
        private void BindSortDropDown()
        {
            DataTable cct = GetSearchSortOptions();

            // Proximity sorting option must be removed when searching by AreaCode or College.
            if (g.SearchPreferences["SearchTypeID"] != null)
            {
                int iSearchTypeID = Matchnet.Conversion.CInt(g.SearchPreferences["SearchTypeID"], Constants.NULL_INT);

                if ((SearchTypeID.AreaCode.Equals((SearchTypeID)iSearchTypeID))
                    || (SearchTypeID.College.Equals((SearchTypeID)iSearchTypeID)))
                {
                    int proximity = (int)Matchnet.Search.Interfaces.QuerySorting.Proximity;
                    DataRow proximitySortOption = null;

                    foreach (DataRow row in cct.Rows)
                    {
                        if (Conversion.CInt(row["Value"]) == proximity)
                        {
                            proximitySortOption = row;
                        }
                    }

                    if (proximitySortOption != null)
                    {
                        proximitySortOption.Delete();
                    }
                }
            }

            rptSearchOption.DataSource = cct;
            rptSearchOption.DataBind();
        }

        private DataTable GetSearchSortOptions()
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("Value", typeof(System.String));
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("ListOrder", typeof(System.Int32));

            for (int i = 0; i < 4; i++)
            {
                DataRow row = dt.NewRow();
                string sort = g.GetResource(String.Format("TXT_SEARCH_SORT_{0}", i + 1), this);
                int sorting = GetSorting(sort);
                if (sorting == (int)Matchnet.Search.Interfaces.QuerySorting.Popularity
                    && !SearchUtil.IsSearchSortByPopularityEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                {
                    continue;
                }
                row["Content"] = g.GetResource(String.Format("TXT_SEARCH_SORT_{0}_CONTENT", sort.ToUpper()), this);
                row["ListOrder"] = i + 1;
                row["Value"] = sorting;
                dt.Rows.Add(row);
            }

            return dt;

        }

        private int GetSorting(string sorting)
        {
            try
            {
                return (int)Enum.Parse(typeof(Matchnet.Search.Interfaces.QuerySorting), sorting);

            }
            catch (Exception ex)
            { throw ex; }

        }

        private string GetSearchOrderBy()
        {
            string ret = string.Empty;
            string searchOrder = string.Empty;

            // check the request for change in SearchOrderBy
            if (Request["SearchOrderBy"] != null)
            {
                searchOrder = Request["SearchOrderBy"];
                _g.SearchPreferences["SearchOrderBy"] = searchOrder;
            }
            else
                searchOrder = _g.SearchPreferences["SearchOrderBy"];

            //This string returned is used only for omniture, internally we only use the ID which is the integer
            switch (searchOrder)
            {
                case "1":
                    ret = "Newest";
                    break;
                case "2":
                    ret = "Most Active";
                    break;
                case "3":
                    ret = "Closest To You";
                    break;
                case "4":
                    ret = "Most Popular";
                    break;
                case "5":
                    ret = "Color Code";
                    break;
            }

            if (String.IsNullOrEmpty(ret))
            {

                ret = "Newest";
            }

            return ret;
        }

        private bool showSpotlightProfile(System.Collections.Generic.List<int> searchIDs)
        {
            bool result = false; ;
            try
            {
                bool promoMemberFlag = false;
                Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("ReverseSearch20", "Start retrieving spotlight profile", g);
                IMemberDTO m = FrameworkGlobals.GetSpotlightProfile(g, searchIDs, out promoMemberFlag);
                //Matchnet.Member.ServiceAdapters.Member m = g.Member;
                //Matchnet.Member.ServiceAdapters.Member m = MemberSA.Instance.GetMember(100046782, MemberLoadFlags.None);

                Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("ReverseSearch20", "End retrieving spotlight profile", g);
                if (m != null)
                {
                    Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("ReverseSearch20", "Spotlight profile=" + m.MemberID, g);
                    plcPromotionalProfile.Visible = true;
                    SpotlightProfile pr = (SpotlightProfile)LoadControl("/Framework/Ui/ProfileElements/SpotlightProfile.ascx");
                    pr.Member = m;
                    pr.EntryPoint = Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.SearchResults;
                    pr.IsPromotionalMember = promoMemberFlag;
                    pr.OnlineImageName = "icon-status-online.gif";
                    pr.OfflineImageName = "icon-status-offline.gif";
                    plcPromotionalProfile.Controls.Add(pr);
                    result = true;
                    _hasSpotlightProfile = true;
                }

                return result;
            }
            catch (Exception ex) { return result; }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rptSearchOption.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptSearchOption_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);
            this.PreRender += new System.EventHandler(this.Page_PreRender);

        }
        #endregion
    }
}