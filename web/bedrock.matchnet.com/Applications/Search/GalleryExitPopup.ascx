<%@ Control Language="c#" AutoEventWireup="false" Codebehind="GalleryExitPopup.ascx.cs" Inherits="Matchnet.Web.Applications.Search.GalleryExitPopup" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="gmp" TagName="GalleryMiniProfile" Src="../../Framework/Ui/BasicElements/GalleryMiniProfile.ascx" %>
<div align="center">
	<a href="../../Default.aspx" target="_blank" id="LogoAnchor" runat="server">
		<mn:image id="LogoImage" runat="server" FileName="galleryexitpopup_head.gif" border="0" App="Search" align="middle"></mn:image></a>
	<div class="galleryExitPopupHeaderImage">
		<mn:image id="HeaderImage" runat="server" FileName="galleryexitpopup_leaving.gif" border="0" App="Search" align="right"></mn:image>
	</div>
	<div class="galleryExitPopupHeaderText">
		<mn:txt id="PhraseText" runat="server" ResourceConstant="BEFORE_YOU_GO"></mn:txt>
	</div>
	<asp:repeater id="GalleryMemberRepeater" runat="server" Visible="true" OnItemDataBound="GalleryMemberRepeater_OnItemDataBound">
		<itemtemplate>
			<asp:Literal Runat="server" ID="rptrNewRow" Visible="False">
<!-- new row -->			
				<table border="0" cellpadding="3" cellspacing="0" width="592">
					<tr>
			</asp:Literal>
			<asp:Literal Runat="server" ID="rptrNewCell" Visible="False">
<!-- new cell -->
						<td align="left" valign="top" width="33%">
			</asp:Literal>
<!-- profile -->
				<gmp:GalleryMiniProfile runat="server" id="GalleryMiniProfile" member="<%# Container.DataItem %>" />
<!-- endprofile -->
			<asp:Literal Runat="server" ID="rptrEndCell" Visible="False">
<!-- end cell -->
						</td>
			</asp:Literal>
			<asp:Literal Runat="server" ID="rptrEndRow" Visible="False">
					</tr>
				</table>
<!-- end row -->			
			</asp:Literal>	
			<asp:Literal Runat="server" ID="rptrEndOneCell" Visible="False">
						<td align="left" valign="top" width="33%">
						</td>
					</tr>
				</table>
<!-- end one cell and row -->			
			</asp:Literal>
			<asp:Literal Runat="server" ID="rptrEndTwoCells" Visible="False">
						<td align="left" valign="top" width="33%">
						</td>
						<td align="left" valign="top" width="33%">
						</td>
					</tr>
				</table>
<!-- end two cells and row -->			
			</asp:Literal>
		</itemtemplate>
	</asp:repeater>
</div>