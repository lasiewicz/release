﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchPreferenceSummary.ascx.cs" Inherits="Matchnet.Web.Applications.Search.Controls.SearchPreferenceSummary" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc" TagName="EditLocationPreference" Src="/Applications/Search/Controls/EditLocationPreference.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WatermarkTextbox" Src="/Framework/Ui/FormElements/WatermarkTextbox.ascx" %>
<%@ Register TagPrefix="uc2" TagName="Sprite" Src="/Framework/Ui/PageElements/Sprite.ascx" %>

<div class="match-pref-summary clearfix">
    <%--Summary--%>
    <h3><mn2:FrameworkLiteral ID="literalCurrentlyViewing" runat="server" ResourceConstant="TXT_CURRENTLY_VIEWING"></mn2:FrameworkLiteral></h3>
    <p><asp:Literal ID="literalCurrentPreferences" runat="server"></asp:Literal></p>   
</div>
<script type="text/javascript">
    // Match summary ellipsis
    $j('.match-pref-summary > p', '#content-main').expander({
        slicePoint: 180,
        expandEffect: 'show',
        expandPrefix: '&nbsp;',
        expandText: '[...]',
        userCollapsePrefix: '&nbsp;',
        userCollapseText: '[^]'
    });
</script>
<div class="match-pref-edit clearfix" <%=GetStyle() %>>
    <%--Edit--%>
    <a id="lnkEditPreference" href="#" class="edit">
        <mn2:FrameworkLiteral ID="literalEditPreferences" runat="server" ResourceConstant="TXT_EDIT_MATCHES"></mn2:FrameworkLiteral>
    </a>
    <a href="#" class="spr-parent close">
        <span class="spr s-icon-closethick-color"><span>X</span></span>
    </a>
    
    <%--Edit layer--%>
    <div id="editPrefLayer" style="display:none" runat="server" class="match-pref-body">
        <%--Default Preferences--%>
        
        <fieldset>
            <div>
                <%--validation messages--%>
                <mn:MultiValidator id="AgeMinValidator" tabIndex="-1" runat="server" Display="Dynamic" RequiredType="IntegerType"
	                ControlToValidate="tbAgeMin" MinValResourceConstant="MIN_AGE_NOTICE" MaxValResourceConstant="MIN_AGE_NOTICE"  FieldNameResourceConstant="MIN_AGE_NOTICE" 
	                IsRequired="True" MinVal="18" MaxVal="99" AppendToErrorMessage="<br />" />
                <mn:MultiValidator id="AgeMaxValidator" tabIndex="-1" runat="server" Display="Dynamic" RequiredType="IntegerType"
	                ControlToValidate="tbAgeMax" MinValResourceConstant="MAX_AGE_NOTICE" MaxValResourceConstant="MAX_AGE_NOTICE" FieldNameResourceConstant="MAX_AGE_NOTICE" IsRequired="True" MinVal="18"
	                MaxVal="99" AppendToErrorMessage="<br />" />
            </div>
        
            <div class="seeking">
                <%--GenderMask--%>
                <label><mn2:FrameworkLiteral ID="literalGenderMaskLabel" runat="server" ResourceConstant="TXT_GENDERMASK_LABEL"></mn2:FrameworkLiteral></label>
                <asp:DropDownList ID="ddlBoxGenderMask" runat="server"></asp:DropDownList>
                <%--Age--%>
	            <label><mnTxt id="txtAgeLabel" runat="server" ResourceConstant="TXT_AGE_LABEL" /></label>
                <asp:TextBox ID="tbAgeMin" Runat="server" MaxLength="2" Width="28" />
                <label><mnTxt id="txtAgeMax" runat="server" ResourceConstant="TO_" /></label>
                <asp:TextBox ID="tbAgeMax" Runat="server" MaxLength="2" Width="28" />
	            <%--Has photos--%>
	            <asp:CheckBox ID="cbHasPhoto" Runat="server" Visible="false" />

               

            </div>
            <div class="clearfix">
                <%--Location--%>
                <uc:EditLocationPreference ID="EditLocationPreference1" runat="server"></uc:EditLocationPreference>
            </div>
             <div>
                 <asp:CheckBox ID="cbxRamahDate" Runat="server" Visible="false" />
            </div>
            <asp:PlaceHolder ID="phKeyword" runat="server" Visible="false">
            <div class="keyword">
                <%--keyword--%>
                <label class="float-inside"><mnTxt id="txtKeywordLabel" runat="server" ResourceConstant="TXT_KEYWORD_LABEL" /></label>
                <div class="rel-layer-container float-inside">
                    <a href="#" rel="click"><mn:Image runat="server" filename="icon-help.gif" align="absmiddle" ID="Image2" /></a>
	                <div class="rel-layer-div">
			            <p class="float-outside"><a href="#" class="click-close"><mnTxt runat="server" id="Txt2" ResourceConstant="TXT_HELP_LAYER_CLOSE_LINK" /></a></p>
		                <h5><mnTxt runat="server" id="txtKeywordTooltipHeader" ResourceConstant="TXT_KEYWORD_TOOLTIP_HEADER" /></h5>
                        <mnTxt runat="server" id="txtKeywordTooltip" ResourceConstant="TXT_KEYWORD_TOOLTIP" />
	                </div>
	            </div>
                <uc1:WatermarkTextbox ID="txtKeyword" runat="server" TextMode="SingleLine" MaxLength="200"
                    CssClass="filled" WatermarkCssClass="watermark" WatermarkTextResourceConstant="TXT_KEYWORD_EXAMPLE">
                </uc1:WatermarkTextbox>
                <span class="optional"><mnTxt ID="txtOptional" runat="server" ResourceConstant="TXT_OPTIONAL" /></span>
            </div>
            </asp:PlaceHolder>
            <div class="cta">
                <%--Buttons--%>
                <mn2:FrameworkButton ID="btnSaveSearchPreferences1" runat="server" OnClick="btnSaveSearchPreferences_Click" ResourceConstant="BTN_SAVE_SEARCH_PREFERENCES" CssClass="btn primary" />
            </div>
            <%--Advance Link--%>
                <a href="#" id="lnkMore" class="spr-parent more-option">
                    <span class="spr s-icon-arrow-down-color"><span></span></span>
                    
                    <span class="more"><mn2:FrameworkLiteral ID="literalAddMore" runat="server" ResourceConstant="TXT_ADD_MORE_PREFERENCES"></mn2:FrameworkLiteral></span>
                    <span class="fewer"><mnTxt ID="txtFewerOptions" runat="server" ResourceConstant="TXT_FEWER_PREFERENCES"></mnTxt></span>
                </a>
        </fieldset> 
        
        <%--Advance Preferences--%>
        <div id="addMoreLayer" class="add-more" style="display:none;">     
            <a href="#" class="collapse-all-cont">
                <span class="spr s-icon-arrow-down-color"><span></span></span>
                <span class="expand"><mn:Txt ID="txtExpandAll12" ResourceConstant="TXT_EXPAND_ALL" runat="server" /></span><span class="collapse hide"><mn:Txt ID="txtCollapseAll2" ResourceConstant="TXT_COLLAPSE_ALL" runat="server" /></span>
            </a>
            
            <div class="search-pref-container" id="prefSearchToggleContainer">
                <mn2:SearchPreferenceRepeater ID="rptSection" Runat="server" />
            </div>

            <a href="#" class="collapse-all-cont">
                <span class="spr s-icon-arrow-down-color"><span></span></span>
                <span class="expand"><mn:Txt ID="txtExpandAll1" ResourceConstant="TXT_EXPAND_ALL" runat="server" /></span><span class="collapse hide"><mn:Txt ID="txtCollapseAll1" ResourceConstant="TXT_COLLAPSE_ALL" runat="server" /></span>
            </a> 
            
            <%--Bottom Buttons--%>
            
            <div class="cta">
                <mn2:FrameworkButton ID="btnSaveSearchPreferences2" runat="server" OnClick="btnSaveSearchPreferences_Click" ResourceConstant="BTN_SAVE_SEARCH_PREFERENCES" CssClass="btn primary" />
            </div>

        </div>
            
    </div>
    
    <%--Edit--%>
    <a id="lnkEditPreference2" href="#" class="edit second">
        <mn2:FrameworkLiteral ID="FrameworkLiteral1" runat="server" ResourceConstant="TXT_EDIT_MATCHES"></mn2:FrameworkLiteral>
    </a>

    <a href="#" class="spr-parent close second" title="<mnTxt id='txtClose2' ResourceConstant='TXT_CLOSE' runat='server'></mnTxt>">
        <span class="spr s-icon-closethick-color"><span>X</span></span>
    </a>
    
    <asp:HiddenField ID="hidAdvancePref" runat="server" Value="" />
</div>

<asp:PlaceHolder ID="phEditSearchPrefJS" runat="server">

<script type="text/javascript">

    var expanded = false,
        fullExpanded = false,
        multiSelected = false,
        $layer = $j('#<%=editPrefLayer.ClientID %>'),
        $layerMore = $j('#addMoreLayer'),
        $close = $j('.match-pref-edit > a.close'),
        $linkMore = $j('#lnkMore'),
        $linkEdit = $j('#lnkEditPreference'),
        $linkEditSecond = $j('#lnkEditPreference2'),
        $locationLayer = $j('#locationLayer');

    function selectCount() {
        var visibleCount = $j('#prefSearchToggleContainer .search-pref-open').length;
        visibleCount >= 2 ? multiSelected = true : multiSelected = false;
    }

    $close.click(function () {
        $locationLayer.hide();
        $layer.slideToggle(400);
        if (fullExpanded && expanded) {
            $linkMore.trigger('click');
        }
        $close.hide();
        expanded = false;
        return false;
    });

    $linkEdit.click(function () {
        $locationLayer.hide();
        selectCount();  //check if options are multiselected
        if (multiSelected && !expanded) {
            $layer.slideToggle(400);
            $linkMore.trigger('click');
        } else {
            $layer.slideToggle(400);
        }

        $j($close[0]).toggle();

        if (fullExpanded && expanded) {
            $linkMore.trigger('click');
        }

        expanded = !expanded;
        return false;
    });

    $linkEditSecond.click(function () {
        $locationLayer.hide();
        $layer.slideUp(400);
        $linkMore.trigger('click');
        $j($close[0]).hide();
        expanded = false;
        fullExpanded = false;
        return false;
    });

    $linkMore.click(function () {
        var $option = $j('#lnkMore > .spr');

        $layerMore.slideToggle(400);
        $j($close[1]).toggle();
        $option.hasClass('s-icon-arrow-down-color') ? $option.attr('class', 'spr s-icon-arrow-up-color') : $option.attr('class', 'spr s-icon-arrow-down-color');
        $j(this).toggleClass('more');
        $linkEditSecond.toggleClass('visible');
        fullExpanded = !fullExpanded;

        if (fullExpanded)
            $j('#<%=hidAdvancePref.ClientID %>').val('true');
        else
            $j('#<%=hidAdvancePref.ClientID %>').val('false');

        return false;
    });

    $j(document).ready(function () {
        // Expand first preference
        var firstClicker = $j('#prefSearchToggleContainer .search-pref-header:first');
        if(firstClicker.is('.search-pref-closed')){
            firstClicker.click();
        }

        // Toggle all: preferenceToggleAll($clicker, $container, clickerOpenClassName, clickerCloseClassName, arrowOpenClassName, arrowCloseClassName, openClassName, closeClassName)
        preferenceToggleAll(
            $j("#addMoreLayer .collapse-all-cont"),
            $j("#prefSearchToggleContainer"),
            "spr s-icon-arrow-up-color",
            "spr s-icon-arrow-down-color",
            "spr s-icon-arrow-down",
            "spr s-icon-arrow-right",
            "search-pref-open",
            "search-pref-closed"
            );
    });

    // fix firefox 'enter' key bug
    $j('.match-pref-body').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $j('input[type=submit]', this).trigger('click');
        }
    });

    //preference slider translated text (var defined in spark.js)
    preferenceSliderTextArray = [<%=_g.GetResource("SEARCH_PREFERENCE_WEIGHTS_LIST", null)%>];
		
</script>
</asp:PlaceHolder>
