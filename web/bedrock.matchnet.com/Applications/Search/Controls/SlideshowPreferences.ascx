﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SlideshowPreferences.ascx.cs" Inherits="Matchnet.Web.Applications.Search.Controls.SlideshowPreferences" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc" TagName="EditLocationPreference" Src="/Applications/Search/Controls/EditSlideshowLocationPreference.ascx" %>
<script type="text/javascript">
    function SaveSettings() {
        //NOTE: BeginSaveSettings must be defined in the control/page that contains this control 
        BeginSaveSettings();
        
        var minAge = jQuery('#<%=tbAgeMin.ClientID %>').val();

        if (minAge < 18) {
            jQuery('#<%=tbAgeMin.ClientID %>').text(18);
        }

        var maxAge = jQuery('#<%=tbAgeMax.ClientID %>').val();

        if (maxAge > 99) {
            jQuery('#<%=tbAgeMax.ClientID %>').text(99);
        }

        var tempMin = minAge;
        var tempMax = maxAge;

        if (minAge > maxAge) {
            maxAge = tempMin;
            minAge = tempMax;
        }

        var regionID = jQuery('#<%=EditLocationPreferences.RegionClientID %>').val();
        var searchType = jQuery('#<%=EditLocationPreferences.SearchTypeClientID %>').val();
        var areaCodes = jQuery('#<%=EditLocationPreferences.AreaCodeListClientID %>').val();
        var displayType = '<%=GetDisplayType() %>';

        var omniturePageName = '';
        if (s != null) {
            omniturePageName = s.pageName;
        }

        jQuery.get(encodeURI("/Applications/Home/SlideshowProfileAPI.aspx?<%=WebConstants.URL_PARAMETER_MINISLIDESHOW_MINAGE %>=" + minAge + "&<%=WebConstants.URL_PARAMETER_MINISLIDESHOW_MAXAGE %>=" + maxAge + "&<%=WebConstants.URL_PARAMETER_MINISLIDESHOW_REGIONID %>=" + regionID + "&<%=WebConstants.URL_PARAMETER_MINISLIDESHOW_SEARCHTYPE %>=" + searchType + "&<%=WebConstants.URL_PARAMETER_MINISLIDESHOW_AREACODE_LIST %>=" + areaCodes + "&<%=WebConstants.URL_PARAMETER_SLIDESHOW_DISPLAY_TYPE %>=" + displayType + "&slideshowmode=SecretAdmirerMode" + "&omniturepage=" + omniturePageName), function (data) {
            //NOTE: EndSaveSettings must be defined in the control/page that contains this control            
            EndSaveSettings(data);
            
        });
    }
</script>

<div id="divSlideshowPreferencesData" class="clearfix" runat="server">
        <div>
            <%--validation messages--%>
            <mn:MultiValidator id="AgeMinValidator" tabIndex="-1" runat="server" Display="Dynamic" RequiredType="IntegerType"
	            ControlToValidate="tbAgeMin" MinValResourceConstant="MIN_AGE_NOTICE" MaxValResourceConstant="MIN_AGE_NOTICE"  FieldNameResourceConstant="MIN_AGE_NOTICE" 
	            IsRequired="True" MinVal="18" MaxVal="99" AppendToErrorMessage="<br />" />
            <mn:MultiValidator id="AgeMaxValidator" tabIndex="-1" runat="server" Display="Dynamic" RequiredType="IntegerType"
	            ControlToValidate="tbAgeMax" MinValResourceConstant="MAX_AGE_NOTICE" MaxValResourceConstant="MAX_AGE_NOTICE" FieldNameResourceConstant="MAX_AGE_NOTICE" IsRequired="True" MinVal="18"
	            MaxVal="99" AppendToErrorMessage="<br />" />
        </div>
            <div class="seeking">
                
            <%--Age--%>
	        <label class="pref-form-label"><mn:txt id="txtAgeLabel" runat="server" ResourceConstant="TXT_AGE_LABEL" /></label>
            <asp:TextBox CssClass="pref-form-input" ID="tbAgeMin" Runat="server" MaxLength="2" Width="28" />
            <label class="pref-form-label"><mn:txt id="txtAgeMax" runat="server" ResourceConstant="TO_" /></label>
            <asp:TextBox CssClass="pref-form-input" ID="tbAgeMax" Runat="server" MaxLength="2" Width="28" />&nbsp;
            <a href="#" class="btn link-primary small"><mn:txt runat="server" id="txtSave" resourceconstant="TXT_SAVE" expandimagetokens="true" /></a>
            <%--<mn:Image id="imgSave" runat="server" FileName="btn-save-sm.gif" />--%>
            <%--Location--%>
            <label class="pref-form-label" style="clear:both"><mn:txt runat="server" id="txtLabelLocation" resourceconstant="TXT_LABEL_LOCATION" expandimagetokens="true" /></label><uc:EditLocationPreference ID="EditLocationPreferences" runat="server"></uc:EditLocationPreference>
        </div>
        <div>
        <%--<a href="#" onclick="SaveSettings();">Save</a>--%>

        
        </div>

</div>