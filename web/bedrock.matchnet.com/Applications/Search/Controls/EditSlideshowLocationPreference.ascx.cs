﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Spark.SAL;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using System.Data;
using Matchnet.Web.Framework.Util;
using System.Text;

namespace Matchnet.Web.Applications.Search.Controls
{
    public partial class EditSlideshowLocationPreference : FrameworkControl
    {
        private RegionLanguage _Region;
        //private MemberSearch _MemberSearch;
        private bool _ResetRegion = false;
        private StringBuilder sbAreaCodes;
        private MemberSlideshowSettings _memberSlideshowSettings;

        //for JS ONLY
        protected string _InitialCityParentRegionID = "";
        protected string _AccountRegionID = "";
        protected string _AccountCountryRegionID = "";
        protected string _AccountStateRegionID = "";
        protected string _AccountCityName = "";

        #region Properties
        public SearchType SearchType { get; set; }
        public bool IsRegOverlay { get; set; }

        //Zip or City
        public int RegionID { get; set; }
        public bool HideZipCode { get; set; }
        public bool HideLookUpCity { get; set; }

        //Area Code
        public int AreaCodeCountryRegionID { get; set; }
        public List<string> AreaCodeList { get; set; }
        public bool HideAreaCode { get; set; }

        //public properties for control clientIDs enabling external controls to get references
        public string RegionClientID
        {
            get { return hidRegionID.ClientID; }
        }

        public string SearchTypeClientID
        {
            get { return hidSearchType.ClientID; }
        }

        public string AreaCodeListClientID
        {
            get { return hidAreaCodes.ClientID; }
        }
        
        #endregion

        public EditSlideshowLocationPreference()
        {
            AreaCodeList = new List<string>();
            SearchType = SearchType.PostalCode;
        }

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["regoverlay"] == "true")
                IsRegOverlay = true;
            
            rbAreaCode.Text = g.GetResource("TXT_AREACODE_RADIO", this);
            rbRegion.Text = g.GetResource("TXT_REGION_RADIO", this);
            rbZipCode.Text = g.GetResource("TXT_ZIPCODE_RADIO", this);

            //set account's regionID for "reset" behavior
            if (g.Member != null)
            {
                int accountRegionID = g.Member.GetAttributeInt(g.Brand, "RegionID");
                if (accountRegionID > 0) 
                {
                    _AccountRegionID = accountRegionID.ToString();
                    RegionLanguage accountRegion = RegionSA.Instance.RetrievePopulatedHierarchy(accountRegionID, g.Brand.Site.LanguageID);
                    _AccountCountryRegionID = accountRegion.CountryRegionID.ToString();
                    _AccountStateRegionID = accountRegion.StateRegionID.ToString();
                    _AccountCityName = accountRegion.CityName;
                }
            }

            //determine if city lookup in popup is enabled
            if (IsCityPopupEnabled() && !HideLookUpCity)
            {
                lnkCityLookup.Attributes.Remove("style");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (_ResetRegion)
            {
                //Reset region to ensure region defaults back to location due to zip code change
                bindCountries();
                bindStates();
                bindCity();
            }

            //show or hide appropriate sections
            if (_memberSlideshowSettings != null)
            {
                switch (_memberSlideshowSettings.SearchType)
                {
                    case SearchType.PostalCode:
                        literalLocation.Text = FrameworkGlobals.GetRegionString(_memberSlideshowSettings.RegionID, g.Brand.Site.LanguageID);
                        rbZipCode.Checked = true;
                        divZipCode.Style.Remove("display");
                        divZipCode.Style.Add("display", "block");
                        break;
                    case SearchType.Region:
                        literalLocation.Text = FrameworkGlobals.GetRegionString(_memberSlideshowSettings.RegionID, g.Brand.Site.LanguageID);
                        rbRegion.Checked = true;
                        divRegion.Style.Remove("display");
                        divRegion.Style.Add("display", "block");
                        break;
                    case SearchType.AreaCode:
                        literalLocation.Text = sbAreaCodes.ToString().Replace(",", ", ");
                        rbAreaCode.Checked = true;
                        divAreaCode.Style.Remove("display");
                        divAreaCode.Style.Add("display", "block");
                        divAreaCodeLabel.Style.Remove("display");
                        divAreaCodeLabel.Style.Add("display", "block");
                        break;
                }
            }

            literalLocationTitle.Text = literalLocation.Text;

            base.OnPreRender(e);

        }

        #endregion

        #region Public Methods

        public void LoadLocationPreference(MemberSlideshowSettings settings)
        {
            _memberSlideshowSettings = settings;
            if (Page.IsPostBack && !String.IsNullOrEmpty(Request[hidSearchType.UniqueID]))
            {
                try
                {
                    int postedRegionID = 0;
                    int.TryParse(Request[hidRegionID.UniqueID], out postedRegionID);

                    SearchType postedSearchType = (SearchType)Conversion.CInt(Request[hidSearchType.UniqueID]);
                    switch (postedSearchType)
                    {
                        case SearchType.Region:
                            if (postedRegionID > 0)
                            {
                                _memberSlideshowSettings.SearchType = SearchType.Region;
                                _memberSlideshowSettings.RegionID = postedRegionID;
                            }
                            break;
                        case SearchType.PostalCode:
                            if (postedRegionID > 0)
                            {
                                _memberSlideshowSettings.SearchType = SearchType.PostalCode;
                                _memberSlideshowSettings.RegionID = postedRegionID;
                                _ResetRegion = true;
                            }
                            break;
                        case SearchType.AreaCode:
                            _memberSlideshowSettings.SearchType = SearchType.AreaCode;
                            string areaCodeString = Request[hidAreaCodes.UniqueID];
                            int countryRegionID = Conversion.CInt(Request[ddlAreaCodeCountry.UniqueID]);
                            bool foundValidAreaCode = false;

                            if (countryRegionID > 0 && !String.IsNullOrEmpty(areaCodeString))
                            {
                                string[] arrAreaCodes = areaCodeString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                for (Int32 i = 1; i <= 6; i++)
                                {
                                    Int32 intAreaCode = (arrAreaCodes.Length >= i && Conversion.CInt(arrAreaCodes[i - 1]) > 0) ? Conversion.CInt(arrAreaCodes[i - 1]) : Constants.NULL_INT;

                                    if (intAreaCode > 0 && RegionSA.Instance.IsValidAreaCode(intAreaCode, countryRegionID))
                                    {
                                        foundValidAreaCode = true;
                                    }
                                }
                            }

                            if (!foundValidAreaCode)
                            {
                                _memberSlideshowSettings.SearchType = SearchType.Region;
                            }

                            break;
                    }
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }
            }

            RegionID = _memberSlideshowSettings.RegionID;

            if (RegionID <= 0 && g.Member != null)
            {
                //default back to profile regionID
                RegionID = g.Member.GetAttributeInt(g.Brand, "RegionID");
                _memberSlideshowSettings.RegionID = RegionID;
            }

            if (RegionID > 0)
            {
                _Region = RegionSA.Instance.RetrievePopulatedHierarchy(RegionID, g.Brand.Site.LanguageID);
            }

            bindData();
        }


        #endregion

        #region Private Methods

        private void bindData()
        {
            //zip code data
            if (!HideZipCode || _memberSlideshowSettings.SearchType == SearchType.PostalCode)
            {
                bindPostalCodeCountries();
                bindPostalCode();
            }
            else
            {
                rbZipCode.Attributes.CssStyle.Add("display", "none");
            }

            //region data
            bindCountries();
            bindStates();
            bindCity();

            //area code data
            Int32 countryRegionID = 0;

            sbAreaCodes = new StringBuilder();

            if (_memberSlideshowSettings.AreaCode1 != Constants.NULL_INT)
            {
                countryRegionID = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveAreaCodes()[_memberSlideshowSettings.AreaCode1];
                AddAreaCode(_memberSlideshowSettings.AreaCode1.ToString(), countryRegionID);
                sbAreaCodes.Append(_memberSlideshowSettings.AreaCode1.ToString() + ",");
            }
            if (_memberSlideshowSettings.AreaCode2 != Constants.NULL_INT)
            {
                countryRegionID = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveAreaCodes()[_memberSlideshowSettings.AreaCode2];
                AddAreaCode(_memberSlideshowSettings.AreaCode2.ToString(), countryRegionID);
                sbAreaCodes.Append(_memberSlideshowSettings.AreaCode2.ToString() + ",");
            }
            if (_memberSlideshowSettings.AreaCode3 != Constants.NULL_INT)
            {
                countryRegionID = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveAreaCodes()[_memberSlideshowSettings.AreaCode3];
                AddAreaCode(_memberSlideshowSettings.AreaCode3.ToString(), countryRegionID);
                sbAreaCodes.Append(_memberSlideshowSettings.AreaCode3.ToString() + ",");
            }
            if (_memberSlideshowSettings.AreaCode4 != Constants.NULL_INT)
            {
                countryRegionID = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveAreaCodes()[_memberSlideshowSettings.AreaCode4];
                AddAreaCode(_memberSlideshowSettings.AreaCode4.ToString(), countryRegionID);
                sbAreaCodes.Append(_memberSlideshowSettings.AreaCode4.ToString() + ",");
            }
            if (_memberSlideshowSettings.AreaCode5 != Constants.NULL_INT)
            {
                countryRegionID = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveAreaCodes()[_memberSlideshowSettings.AreaCode5];
                AddAreaCode(_memberSlideshowSettings.AreaCode5.ToString(), countryRegionID);
                sbAreaCodes.Append(_memberSlideshowSettings.AreaCode5.ToString() + ",");
            }
            if (_memberSlideshowSettings.AreaCode6 != Constants.NULL_INT)
            {
                countryRegionID = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveAreaCodes()[_memberSlideshowSettings.AreaCode6];
                AddAreaCode(_memberSlideshowSettings.AreaCode6.ToString(), countryRegionID);
                sbAreaCodes.Append(_memberSlideshowSettings.AreaCode6.ToString() + ",");
            }

            if (sbAreaCodes.Length > 0)
            {
                sbAreaCodes.Remove(sbAreaCodes.Length - 1, 1);
            }

            this.AreaCodeCountryRegionID = countryRegionID;
            bool isAreaCodeEnabled = true;
            if (HideAreaCode || !FrameworkGlobals.IsAreaCodePreferenceEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
            {
                //if member's search type is area code already, we need to display area code support regardless of setting
                isAreaCodeEnabled = _memberSlideshowSettings.SearchType == SearchType.AreaCode;
            }

            if (isAreaCodeEnabled)
            {
                bindAreaCodeCountries();
                bindAreaCodes();
            }
            else
            {
                rbAreaCode.Attributes.CssStyle.Add("display", "none");
            }

            //set hidden values to be used as part of ajax manipulations
            if (RegionID > 0)
                hidRegionID.Value = RegionID.ToString();

            hidSearchType.Value = _memberSlideshowSettings.SearchType.ToString("d");

        }

        private bool countryHasStates(Int32 countryRegionID)
        {
            return RegionSA.Instance.RetrieveRegionByID(countryRegionID, g.Brand.Site.LanguageID).ChildrenDepth == 2;
        }

        private bool AddAreaCode(string areaCode, Int32 countryRegionID)
        {
            Int32 intAreaCode = Conversion.CInt(areaCode);
            if (intAreaCode > 0 && RegionSA.Instance.IsValidAreaCode(intAreaCode, countryRegionID))
            {
                AreaCodeList.Add(areaCode);
                return true;
            }

            return false;
        }

        #endregion

        #region Populating Textboxes and Dropdowns
        private void bindPostalCodeCountries()
        {
            ListItem li = null;
            ddlZipCountry.Items.Clear();

            li = new ListItem(g.GetResource("USA", this), Matchnet.Lib.ConstantsTemp.REGIONID_USA.ToString());
            ddlZipCountry.Items.Add(li);

            li = new ListItem(g.GetResource("CANADA", this), Matchnet.Lib.ConstantsTemp.REGIONID_CANADA.ToString());
            ddlZipCountry.Items.Add(li);

            if (_Region != null)
                FrameworkGlobals.SelectItem(ddlZipCountry, _Region.CountryRegionID.ToString());
        }

        private void bindPostalCode()
        {
            if (_Region != null)
                txtZipCode.Text = _Region.PostalCode;
        }

        private void bindCountries()
        {
            ddlRegionCountry.Items.Clear();
            ddlRegionCountry.DataSource = RegionSA.Instance.RetrieveCountries(g.Brand.Site.LanguageID);
            ddlRegionCountry.DataTextField = "Description";
            ddlRegionCountry.DataValueField = "RegionID";
            ddlRegionCountry.DataBind();

            if (RegionID > 0)
            {
                FrameworkGlobals.SelectItem(ddlRegionCountry, _Region.CountryRegionID.ToString());
            }
        }

        private void bindStates()
        {
            if (_Region != null)
            {
                int countryRegionID = _Region.CountryRegionID;

                if ((countryRegionID <= 0 || !countryHasStates(countryRegionID)))
                {
                    divState.Attributes.Add("style", "display:none;");
                    _InitialCityParentRegionID = countryRegionID.ToString();
                }
                else
                {
                    ddlState.Items.Clear();
                    RegionCollection stateRegions = RegionSA.Instance.RetrieveChildRegions(countryRegionID, g.Brand.Site.LanguageID);
                    ddlState.DataSource = stateRegions;
                    ddlState.DataTextField = "Description";
                    ddlState.DataValueField = "RegionID";
                    ddlState.DataBind();

                    if (_Region.StateRegionID > 0)
                    {
                        FrameworkGlobals.SelectItem(ddlState, _Region.StateRegionID.ToString());
                        _InitialCityParentRegionID = _Region.StateRegionID.ToString();
                    }
                }
            }
        }

        private void bindCity()
        {
            if (_Region != null)
                txtCity.Text = _Region.CityName;
        }

        private void bindAreaCodeCountries()
        {
            ddlAreaCodeCountry.Items.Add(new ListItem(g.GetResource("USA", this), Matchnet.Lib.ConstantsTemp.REGIONID_USA.ToString()));
            ddlAreaCodeCountry.Items.Add(new ListItem(g.GetResource("CANADA", this), Matchnet.Lib.ConstantsTemp.REGIONID_CANADA.ToString()));

            if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Cupid ||
                g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                ddlAreaCodeCountry.Items.Add(new ListItem(g.GetResource("ISRAEL", this), Matchnet.Lib.ConstantsTemp.REGIONID_ISRAEL.ToString()));
            }

            // prior to this code, AreaCodeCountryRegionID is correctly set
            int regionid = _Region != null ? _Region.CountryRegionID : 0;
            if (AreaCodeCountryRegionID == 0)
            {
                // assumes validity of a preset, and the preset not being overridden from the public property
                if (
                    regionid == Matchnet.Lib.ConstantsTemp.REGIONID_CANADA ||
                    regionid == Matchnet.Lib.ConstantsTemp.REGIONID_USA ||
                    regionid == Matchnet.Lib.ConstantsTemp.REGIONID_ISRAEL
                    )
                    AreaCodeCountryRegionID = regionid;
            }

            if (AreaCodeCountryRegionID > 0)
            {
                FrameworkGlobals.SelectItem(ddlAreaCodeCountry, AreaCodeCountryRegionID.ToString());
            }
        }

        private void bindAreaCodes()
        {
            TextBox[] areaCodeBoxes = new TextBox[] { txtAreaCode1, txtAreaCode2, txtAreaCode3, txtAreaCode4, txtAreaCode5, txtAreaCode6 };

            for (Int32 i = 0; i < areaCodeBoxes.GetUpperBound(0) + 1; i++)
            {
                if (i < AreaCodeList.Count && Conversion.CInt(AreaCodeList[i]) > 0)
                {
                    areaCodeBoxes[i].Text = AreaCodeList[i];
                }
                else
                {
                    areaCodeBoxes[i].Text = string.Empty;
                }
            }
        }

        private bool IsCityPopupEnabled()
        { 
            string isEnabled = "true";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_CITY_LOOKUP_POPUP", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "true";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        #endregion
    }
}