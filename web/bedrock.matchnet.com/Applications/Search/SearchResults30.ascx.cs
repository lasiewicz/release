﻿using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Search.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Ui.ProfileElements;
using System.Text;
using Matchnet.Web.Framework.Ui.Feedback;
using System.Collections.Generic;
using Matchnet.Web.Framework.Ui.PageAnnouncements;

namespace Matchnet.Web.Applications.Search
{
    public partial class SearchResults30 : FrameworkControl
    {
        protected Int32 SearchOrderBy;
        protected string ChangeVoteMessage;
        protected PlaceHolder plcPromotionProfile;
        private bool _hasSpotlightProfile = false;
        private bool _hasSearchOrderByChanged = false;
        private Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType _ListViewType = ResultsViewType.ListViewType.gallery;
        public bool IsPartialRender { get; set; }
        public bool IsRegOverlayRefresh { get; set; }
        protected string _JSKeywordSearch = "";

        #region Event Handlers
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                ChangeVoteMessage = g.GetResource("ARE_YOU_SURE_YOU_WANT_TO_CHANGE", this);
                //SearchPreferences.Href = "/Applications/Search/SearchPreferences.aspx";

                // check to see if the current user access the site has any search preferences specified,
                // if not then the user is redirected to SearchPrefs for search specifications, otherwise
                // the results of the search are displayed
                SearchPreferenceCollection searchPreferences = g.SearchPreferences;

                if ((FrameworkGlobals.IsRegOverlay() || Request["regoverlay"] == "true") && !IsPartialRender)
                {
                    //Instead of waiting for the complete registration, check IP now and redirect if blocked. 
                    //This gets around problem of redirecting from overlay
                    new IPBlockerClient().CheckAccessAndRedirectIfBlocked(g.ClientIP, Spark.Common.IPBlockerAccessType.Registration, g);
                }

                //There is no spec for the external search interface so detect an external search
                //by checking for some of the most common parameters.
                if (IsRegOverlayRefresh)
                {
                    //this is a refresh via ajax for registration overlay
                    setSearchPreferencesFromRegistration();
                }
                else if (!IsPartialRender && Request["regoverlay"] == "true" && !String.IsNullOrEmpty(Request["RegistrationGUID"]))
                {
                    bool searchprefpopulated = false;
                    Matchnet.EmailNotifier.ValueObjects.ScheduledEvent ev = Registration.RegistrationCapture.PopulateWizardFromEvent(Request["RegistrationGUID"], new RegistrationPersistence().Name, g);
                    if (ev != null)
                    {
                        g.Session.Add("REGISTRATION_POPULATED", "true", SessionPropertyLifetime.Temporary);
                        searchprefpopulated = Registration.RegistrationCapture.PopulateSearchPreferencesFromEvent(ev, g);
                        if (!searchprefpopulated)
                        {
                            g.Transfer("/default.aspx");
                        }
                    }

                }
                else if (Request["SearchTypeID"] != null || Request["ZipCode"] != null || Request["GenderID"] != null)
                {
                    // User is hitting search results directly, we're going to look for preferences identified in
                    // the query string.
                    setSearchPreferencesFromQueryString();
                }
                else if (Request["regoverlay"] == "true") //BlackSingles Overlay registration - SearchPreferences lost sometimes. In this case, set the values from Cookie
                {
                    setSearchPreferencesFromRegistration();
                }

                setSearchPreferencesFromQueryStringForFriendlyURL();

                if (!IsPartialRender)
                {
                    // If there are no search preferences in the SearchPreference object or the regionID is invalid,
                    // redirect to SearchPreferences so that the user can fill in search preferences.
                    // We look for searchPreferences.Count <= 1 because sometimes there will be 1 searchPref (searchTypeID)
                    // and that does not count as valid search preferences (see TT 13887).  Checking against a count of 0 was
                    // causing problems.
                    if (searchPreferences.Count <= 1 || (!ValidRegionID(searchPreferences["RegionID"]) && searchPreferences["SearchTypeID"] != ((Int32)SearchTypeID.AreaCode).ToString()))
                    {
                        if (!plcSector.Visible)
                        {
                            // OI-167 Allow User from MatchMail Profile View to be redirected to the login page instead of the search preferences page for visitor. Need to take the user back to search results page.
                            if ((Request["MMVID"] != null) && (g.Member == null))
                            {
                                g.Transfer("/Applications/Logon/Logon.aspx?DestinationURL=/Applications/Search/SearchResults20.aspx");
                            }
                            else
                            {
                                g.Transfer("/Applications/Search/SearchPreferences.aspx");
                            }
                        }
                    }
                }

                //Check to see if a search order has been specified.
                if (Request[WebConstants.URL_PARAMETER_NAME_SEARCHORDERBY] == null)
                {
                    //07062011 TL: Do not override saved search order with default
                    if (!String.IsNullOrEmpty(g.SearchPreferences["SearchOrderBy"]))
                    {
                        //use saved search order
                        SearchOrderBy = Convert.ToInt32(g.SearchPreferences["SearchOrderBy"]);
                    }

                    if (SearchOrderBy <= 0)
                    {
                        //default
                        SearchOrderBy = GetDefaultSearchOrderBy();
                    }
                }
                else
                {
                    SearchOrderBy = Convert.ToInt32(Request[WebConstants.URL_PARAMETER_NAME_SEARCHORDERBY]);

                    //check if search order by has changed
                    if (!String.IsNullOrEmpty(g.SearchPreferences["SearchOrderBy"])
                        && g.SearchPreferences["SearchOrderBy"] != SearchOrderBy.ToString())
                    {
                        _hasSearchOrderByChanged = true;
                    }
                }

                bool colorCodeEnabled = SettingsManager.GetSettingBool(SettingConstants.ENABLE_COLORCODE_SEARCH, g.Brand);
                if (SearchOrderBy == (Int32)Matchnet.Search.Interfaces.QuerySorting.ColorCode)
                {
                    if (colorCodeEnabled)
                    {
                        colorCodeSelect.Visible = true;
                        colorCodeSelect.ShowLink = true;
                    }
                    else
                    {
                        colorCodeSelect.Visible = false;
                        colorCodeSelect.ShowLink = false;
                        SearchOrderBy = GetDefaultSearchOrderBy();
                    }
                }
                else if (SearchOrderBy == (int)Matchnet.Search.Interfaces.QuerySorting.Popularity
                    && !SearchUtil.IsSearchSortByPopularityEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                {
                    SearchOrderBy = GetDefaultSearchOrderBy();
                }
                else if (SearchOrderBy == (int) Matchnet.Search.Interfaces.QuerySorting.KeywordRelevance)
                {
                    bool hasKeywordTerms = !string.IsNullOrEmpty(g.SearchPreferences["keywordsearch"]);
                    if (!hasKeywordTerms ||
                        !SearchUtil.IsKeywordMatchesSearchPrefEnabled(g.Brand.BrandID, g.Brand.Site.SiteID,
                                                                     g.Brand.Site.Community.CommunityID))
                    {
                        SearchOrderBy = GetDefaultSearchOrderBy();
                    }
                }

                g.SearchPreferences["SearchOrderBy"] = SearchOrderBy.ToString();

                // Wire up the List Navigation. (Pagination uses these labels in ResultListHandler.cs)
                g.ListNavigationTop = lblListNavigationTop;
                g.ListNavigationBottom = lblListNavigationBottom;

                // Set results page size
                SearchResultList.PageSize = SearchUtil.GetSearch30PageSize(g);

                // Add handler to determine when preferences are ready (since we have edit pref on same page) to load results
                SearchPreferenceSummary1.PreferencesReady += new Matchnet.Web.Applications.Search.Controls.SearchPreferenceSummary.PreferencesReadyEventHandler(SearchPreferenceSummary1_PreferencesReady);

                // Add handler to perform additional tasks after search results have been loaded
                SearchResultList.ResultsLoaded += new Matchnet.Web.Framework.Ui.SearchElements.ResultList.ResultsLoadedEventHandler(SearchResultList_ResultsLoaded);

            }
            catch (Exception ex) { g.ProcessException(ex); }
        }

        public void SearchPreferenceSummary1_PreferencesReady()
        {
            if (Page.IsPostBack && !IsPartialRender)
            {
                bool requiresPRG = SearchPreferenceSummary1.PostbackRequiresPRG;

                if (requiresPRG)
                {
                    //Also need to check for anthem postback due to registration overlay
                    string Anthem_CallBack = "false";
                    if (!string.IsNullOrEmpty(Request["Anthem_CallBack"]))
                    {
                        Anthem_CallBack = Request["Anthem_CallBack"];
                    }

                    if (Anthem_CallBack.ToLower() == "true")
                    {
                        requiresPRG = false;
                    }
                }

                if (requiresPRG)
                {
                    //This search page has edit preferences on the same page so we need to do a PRG approach to
                    //remove posted page from being in browser history to avoid page expire alert in browser on back button
                    List<string> removeParamNames = new List<string>();
                    removeParamNames.Add("startrow");
                    //[QAT-11371] remove sort order query param so it doesn't override keyword relevance sort - arod
                    removeParamNames.Add(WebConstants.URL_PARAMETER_NAME_SEARCHORDERBY);
                    FrameworkGlobals.RedirectToSamePage(removeParamNames);
                }
            }

            //Load Result List
            if (Page.IsPostBack && !String.IsNullOrEmpty(Request["hidRefreshFilteredSearchResults"]) && Request["hidRefreshFilteredSearchResults"] == "true")
            {
                SearchResultList.IgnoreSAFilteredSearchResultsCache = true;
            }

            if (SearchPreferenceSummary1.IgnoreFilteredSearchResultsCache)
            {
                SearchResultList.IgnoreSAFilteredSearchResultsCache = true;
            }

            //this is used for QA/TEST purpose, allows to ignore search cache
            if (!Page.IsPostBack && !String.IsNullOrEmpty(Request["ignoreCachedSearchResults"]) && Request["ignoreCachedSearchResults"] == "true")
            {
                SearchResultList.IgnoreAllSearchResultsCache = true;
            }

            BindSortDropDown();
            _ListViewType = idResultsViewType.GetSearchResultViewMode();
            if (!SearchUtil.IsSearch30ListViewEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
            {
                SearchResultList.GalleryView = "true";
                phViewMode.Visible = false;
            }
            else
            {
                SearchResultList.GalleryView = idResultsViewType.GalleryViewFlag;
            }

            if (MatchRatingManager.Instance.DisplayMatchRatingOnMatches(g.Member != null ? g.Member.MemberID : Constants.NULL_INT, g.Brand))
            {
                SearchResultList.DisplayMatchRating = true;
            }

            SearchResultList.IsPartialRender = IsPartialRender;
            SearchResultList.LoadResultList();

            //display profile30 Beta toggler
            SetSearchRedesignBetaToggler();

            //set keyword highlighting for JS
            if (SearchUtil.IsKeywordHighlightEnabled(g))
            {
                phKeywordHighlightJS.Visible = true;
                if (!string.IsNullOrEmpty(g.SearchPreferences["KeywordSearch"]))
                {
                    _JSKeywordSearch = g.SearchPreferences["KeywordSearch"].Replace("\"", "");
                }
            }

            if (MatchRatingManager.Instance.DisplayMatchRatingOnMatches(g.Member != null ? g.Member.MemberID : Constants.NULL_INT, g.Brand))
            {
                bool showMatchRatingAnnouncementOverride = false;
                if (!Page.IsPostBack && !String.IsNullOrEmpty(Request["showMatchRatingAnnouncement"]) && Request["showMatchRatingAnnouncement"] == "true")
                {
                    showMatchRatingAnnouncementOverride = true;
                }

                if (showMatchRatingAnnouncementOverride || !PageAnnouncementHelper.HasViewedPageAnnouncement(g.Member, g.Brand.Site.SiteID, PageAnnouncementHelper.PageAnnouncementMask.MutualMatch))
                {
                    //display Mutual Match Rating feature announcement
                    MutualMatchAnnouncement mmAnnouncement = Page.LoadControl("/Framework/Ui/PageAnnouncements/MutualMatchAnnouncement.ascx") as MutualMatchAnnouncement;
                    mmAnnouncement.LoadAnnouncement();
                    g.LayoutTemplateBase.AddPageAccouncementControl(mmAnnouncement);
                }
            }
        }

        public void SearchResultList_ResultsLoaded(System.Collections.Generic.List<int> SearchIDs)
        {
            showSpotlightProfile(SearchIDs);
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            //set memberSearch (prefs) to summary control
            if (plcSector.Visible)
            {
                SearchPreferenceSummary1.MemberSearch = Spark.SAL.MemberSearch.GetMemberSearch(SectorsManager.GetSearchpreferences(SectorsManager.GetSector(), g), g.Member != null ? g.Member.MemberID : Constants.NULL_INT, g.Brand.Site.Community.CommunityID, "Default");
            }
            else
            {
                SearchPreferenceSummary1.MemberSearch = Spark.SAL.MemberSearch.GetMemberSearch(g.SearchPreferences, g.Member != null ? g.Member.MemberID : Constants.NULL_INT, g.Brand.Site.Community.CommunityID, "Default");
            }

            SearchPreferenceSummary1.IsRegOverlayRefresh = IsRegOverlayRefresh;
            SearchPreferenceSummary1.IsPartialRender = IsPartialRender;

            //Render regular page content
            RenderPage();

            //Show Feedback widget
            if (g.LayoutTemplateBase != null && !IsPartialRender)
            {
                if (FeedbackHelper.IsSearchRedesignFeedbackEnabled(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID))
                {
                    g.LayoutTemplateBase.ShowFeedback(Matchnet.Web.Framework.Ui.Feedback.FeedbackHelper.FeedbackType.BetaMatches);
                }
            }

        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            if (!IsPartialRender)
            {
                // Omniture page name override. This cannot be done in Omniture.ascx.cs because the gallery/list view mode is set here which gets executed after Omniture code
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    _g.AnalyticsOmniture.PageName = "Match_Results";
                    _g.AnalyticsOmniture.Evar2 = "Match_Results";
                    _g.AnalyticsOmniture.AddEvent("event2");
                    _g.AnalyticsOmniture.Prop45 = GetSearchOrderBy();
                    if (_hasSearchOrderByChanged)
                    {
                        _g.AnalyticsOmniture.Prop45 = "changed_" + _g.AnalyticsOmniture.Prop45;
                    }

                    _g.AnalyticsOmniture.Prop45 += "_" + _ListViewType.ToString();

                    if (_hasSpotlightProfile)
                    {
                        _g.AnalyticsOmniture.AddEvent("event13");
                        _g.AnalyticsOmniture.AddProductEvent("event13=1");
                    }


                    // MPR-940 JewishSearch
                    int optionValue = Conversion.CInt(g.SearchPreferences["JdateReligion"], Constants.NULL_INT);

                    // Selected Any
                    if (optionValue == Constants.NULL_INT)
                    {
                        _g.AnalyticsOmniture.Evar41 = "Standard";
                    }
                    else
                    {
                        if (((optionValue & 16384) == 16384) || //Willing to Convert
                        ((optionValue & 2048) == 2048) || //Not Willing to Convert
                        ((optionValue & 512) == 512)) //Not sure if I’m willing to convert
                        {
                            _g.AnalyticsOmniture.Evar41 = "Standard";
                        }
                        else
                        {
                            _g.AnalyticsOmniture.Evar41 = "Jewish Only";
                        }
                    }
                }
            }
        }

        private void rptSearchOption_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            DataRowView row = (DataRowView)e.Item.DataItem;
            Literal litOption = (Literal)e.Item.FindControl("litOption");
            string selectedText = (row["Value"].ToString() == SearchOrderBy.ToString()) ? "selected=\"selected\"" : "";
            litOption.Text = "<option value=\"" + row["Value"].ToString() + "\" " + selectedText + ">" + row["Content"].ToString() + "</option>";

        }

        #endregion

        #region Private Methods
        private void RenderPage()
        {
            try
            {
                string breadCrumbTrailResourceConstant = "NAV_SUB_MATCHES";

                if (!string.IsNullOrEmpty(Request.QueryString["sector"]))
                {
                    string sectorTextResourceConstant = "TXT_SECTOR_TEXT_";
                    string titleResourceConstant = "TXT_TITLE_TEXT_";
                    string pageTitleResourceConstant = "TXT_PAGE_TITLE_";
                    string pageMetaDescriptionResourceConstant = "TXT_PAGE_META_DESCRIPTION_";

                    // academic dating
                    if (Request.QueryString["sector"] == "1")
                    {
                        sectorTextResourceConstant += "ACADEMIC";
                        titleResourceConstant += "ACADEMIC";
                        pageTitleResourceConstant += "ACADEMIC";
                        pageMetaDescriptionResourceConstant += "ACADEMIC";
                    }
                        // religious dating
                    else if (Request.QueryString["sector"] == "2")
                    {
                        sectorTextResourceConstant += "RELIGIOUS";
                        titleResourceConstant += "RELIGIOUS";
                        pageTitleResourceConstant += "RELIGIOUS";
                        pageMetaDescriptionResourceConstant += "RELIGIOUS";
                    }
                        // Divorced dating
                    else if (Request.QueryString["sector"] == "3")
                    {
                        titleResourceConstant += "DIVORCED";
                        //sectorTextResourceConstant += "DIVORCED";
                        //pageTitleResourceConstant += "DIVORCED";
                        //pageMetaDescriptionResourceConstant += "DIVORCED";
                        sectorTextResourceConstant = null;
                        pageTitleResourceConstant = null;
                        pageMetaDescriptionResourceConstant = null;
                    }
                        // Elderly dating
                    else if (Request.QueryString["sector"] == "4")
                    {
                        titleResourceConstant += "ELDERLY";
                        //sectorTextResourceConstant += "ELDERLY";

                        //pageTitleResourceConstant += "ELDERLY";
                        //                        pageMetaDescriptionResourceConstant += "ELDERLY"; 
                        sectorTextResourceConstant = null;
                        pageTitleResourceConstant = null;
                        pageMetaDescriptionResourceConstant = null;

                    }
                    else
                    {
                        return;
                    }

                    plcSector.Visible = true;
                    SearchResultList.ResultListContextType = ResultContextType.SectorSearchResult;
                    SearchPreferenceSummary1.IsSectorPage = true;

                    txtMatches.ResourceConstant = titleResourceConstant;
                    breadCrumbTrailResourceConstant = titleResourceConstant;

                    if (!string.IsNullOrEmpty(sectorTextResourceConstant))
                    {
                        txtSectorText.ResourceConstant = sectorTextResourceConstant;
                    }
                    if (!string.IsNullOrEmpty(pageTitleResourceConstant))
                    {
                        g.SetPageTitle(g.GetResource(pageTitleResourceConstant, this));
                    }
                    if (!string.IsNullOrEmpty(pageMetaDescriptionResourceConstant))
                    {
                        g.SetMetaDescription(g.GetResource(pageMetaDescriptionResourceConstant, this));
                    }


                    if (g.Member == null)
                    {
                        btnJoinNow.Visible = true;
                        btnJoinNow.NavigateUrl = "/Applications/Registration/Registration.aspx";
                    }



                    if (g.BreadCrumbTrailHeader != null)
                    {
                        g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource(breadCrumbTrailResourceConstant, this),
                                                                g.AppPage.App.DefaultPagePath);

                    }
                    if (g.BreadCrumbTrailFooter != null)
                    {
                        g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource(breadCrumbTrailResourceConstant, this),
                                                                g.AppPage.App.DefaultPagePath);
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private bool ValidRegionID(string RegionID)
        {
            if (RegionID != null && RegionID != string.Empty && Conversion.CInt(RegionID, Constants.NULL_INT) != Constants.NULL_INT)
            {
                return true;
            }
            return false;
        }

        private void setSearchPreferencesFromQueryString()
        {
            SearchPreferenceCollection preferences = g.SearchPreferences;

            int searchTypeVal = Conversion.CInt(Request["SearchTypeID"]);

            SearchTypeID searchTypeID;
            if (searchTypeVal != Constants.NULL_INT)
            {
                searchTypeID = (SearchTypeID)searchTypeVal;
            }
            else
            {
                searchTypeID = SearchTypeID.PostalCode;
            }
            preferences.Add("SearchTypeID", ((int)searchTypeID).ToString());

            int genderMask = Constants.NULL_INT;
            if (!string.IsNullOrEmpty(Request["GenderMask"]))
            {
                genderMask = Conversion.CInt(Request["GenderMask"]);
                preferences.Add("GenderMask", genderMask.ToString());
            }
            else
            {
                int genderID = Conversion.CInt(Request["GenderID"]);
                int seekingGenderID = Conversion.CInt(Request["SeekingGenderID"]);

                if (genderID == Constants.NULL_INT && seekingGenderID != Constants.NULL_INT)
                {
                    if (seekingGenderID == Matchnet.Lib.ConstantsTemp.GENDERID_SEEKING_MALE)
                    {
                        genderID = Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE;
                    }
                    else
                    {
                        genderID = Matchnet.Lib.ConstantsTemp.GENDERID_MALE;
                    }
                }

                genderMask = GenderUtils.FlipMaskIfHeterosexual(genderID + seekingGenderID);
                preferences.Add("GenderMask", genderMask.ToString());
            }

            //set default gendermask if missing
            if (string.IsNullOrEmpty(preferences["GenderMask"]))
                preferences.Add("GenderMask", (Matchnet.Lib.ConstantsTemp.GENDERID_MALE + Matchnet.Lib.ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString());

            int countryRegionID = Constants.NULL_INT;
            if (Request["CountryRegionID"] != null)
            {
                countryRegionID = Conversion.CInt(Request["CountryRegionID"]);
            }
            else
            {
                countryRegionID = g.Brand.Site.DefaultRegionID;
            }
            preferences.Add("CountryRegionID", countryRegionID.ToString());
            preferences.Add("RegionID", Request["RegionID"]);

            //Set RegionID based on ZipCode or City searches
            RegionID regionID = null;
            string zipCode = Request["ZipCode"];
            if (zipCode != null)
            {
                regionID = RegionSA.Instance.FindRegionIdByPostalCode(countryRegionID, zipCode);
            }

            //Sometimes " " gets replaced by "+" in encoded URL params
            string city = Request["City"];
            int stateRegionID = Conversion.CInt(Request["StateRegionID"]);
            if (stateRegionID != Constants.NULL_INT && city != null)
            {
                city = city.Replace("+", " ");
                regionID = RegionSA.Instance.FindRegionIdByCity(stateRegionID, city, g.Brand.Site.LanguageID);
            }

            if (regionID != null)
            {
                preferences.Add("RegionID", regionID.ID.ToString());
            }

            if (Request["Distance"] == null)
            {
                preferences.Add("Distance", g.Brand.DefaultSearchRadius.ToString());
            }
            else
            {
                preferences.Add("Distance", Request["Distance"]);
            }
            if (Request["SearchOrderBy"] == null || Request["SearchOrderBy"] == String.Empty)
            {
                if (string.IsNullOrEmpty(preferences["SearchOrderBy"]))
                    preferences.Add("SearchOrderBy", "1");
            }
            else
            {
                preferences.Add("SearchOrderBy", Request["SearchOrderBy"]);
            }

            int minAge = Conversion.CInt(Request["MinAge"]);
            int maxAge = Conversion.CInt(Request["MaxAge"]);

            if (Request["AgeRangeID"] != null)
            {
                GetAgeRangeFromID(Conversion.CInt(Request["AgeRangeID"]), ref minAge, ref maxAge);
            }

            preferences.Add("MinAge", minAge.ToString());
            preferences.Add("MaxAge", maxAge.ToString());

            preferences.Add("SchoolID", Request["SchoolID"]);
            preferences.Add("HasPhotoFlag", Request["HasPhotoFlag"]);
            preferences.Add("EducationLevel", Request["EducationLevel"]);
            preferences.Add("Religion", Request["Religion"]);
            preferences.Add("LanguageMask", Request["LanguageMask"]);
            preferences.Add("Ethnicity", Request["Ethnicity"]);
            preferences.Add("SmokingHabits", Request["SmokingHabits"]);
            preferences.Add("DrinkingHabits", Request["DrinkingHabits"]);
            preferences.Add("MinHeight", Request["MinHeight"]);
            preferences.Add("MaxHeight", Request["MaxHeight"]);
            preferences.Add("JDateReligion", Request["JDateReligion"]);
            preferences.Add("JDateEthnicity", Request["JDateEthnicity"]);
            preferences.Add("SynagogueAttendance", Request["SynagogueAttendance"]);
            preferences.Add("KeepKosher", Request["KeepKosher"]);
            preferences.Add("MajorType", Request["MajorType"]);
            preferences.Add("RelationshipMask", Request["RelationshipType"]);		//RelationshipType from collegeclub.com
            preferences.Add("RelationshipStatus", Request["RelationshipStatus"]);
            preferences.Add("AreaCode1", Request["AreaCode1"]);
            preferences.Add("MaritalStatus", Request["MaritalStatus"]);

            createPreferencesTabCookie("City");

            //In case of partial registration, we will override some preferences for that
            if (Request["regoverlay"] == "true")
            {
                setSearchPreferencesFromRegistration();
            }
            else
            {
                g.SaveSearchPreferences();
            }
        }

        private void setSearchPreferencesFromRegistration()
        {
            SearchPreferenceCollection preferences = g.SearchPreferences;

            //set reg defaults, if missing
            if (string.IsNullOrEmpty(preferences["SearchTypeID"]))
                preferences.Add("SearchTypeID", ((int)SearchTypeID.Region).ToString());

            if (string.IsNullOrEmpty(preferences["Distance"]))
                preferences.Add("Distance", g.Brand.DefaultSearchRadius.ToString());

            if (string.IsNullOrEmpty(preferences["SearchOrderBy"]))
                preferences.Add("SearchOrderBy", "1");

            if (string.IsNullOrEmpty(preferences["MinAge"]))
                preferences.Add("MinAge", "18");

            if (string.IsNullOrEmpty(preferences["MaxAge"]))
                preferences.Add("MaxAge", "35");

            if (string.IsNullOrEmpty(preferences["HasPhotoFlag"]))
                preferences.Add("HasPhotoFlag", "1");

            //Only if registration cookie exists, otherwise leave preferences alone
            HttpCookie regCookie = Request.Cookies[new RegistrationPersistence().Name];
            if (regCookie != null)
            {
                //check the following from registration cookie
                if (!string.IsNullOrEmpty(regCookie["GenderMask"]))
                {
                    int genderMask = Conversion.CInt(regCookie["GenderMask"]);
                    if (genderMask >= 0)
                    {
                        //need to reverse the profile gendermask value for heterosexual for search pref gendermask
                        if (genderMask == 6) //f-m
                            genderMask = 9; //m-f
                        else if (genderMask == 9) //m-f
                            genderMask = 6; //f-m

                        preferences.Add("GenderMask", genderMask.ToString());
                    }
                }

                if (!string.IsNullOrEmpty(regCookie["RegionID"]))
                {
                    int cookieRegionID = Conversion.CInt(regCookie["RegionID"]);
                    if (cookieRegionID > 0)
                    {
                        preferences.Add("RegionID", cookieRegionID.ToString());
                    }
                }

            }
            //Set RegionID to a default value.
            //RegionID 3404470 - Los Angeles
            if (string.IsNullOrEmpty(preferences["RegionID"]) || (!string.IsNullOrEmpty(preferences["RegionID"]) && Conversion.CInt(preferences["RegionID"]) < 0))
                preferences.Add("RegionID", "3404470");

            //set default gendermask
            if (string.IsNullOrEmpty(preferences["GenderMask"]))
                preferences.Add("GenderMask", (Matchnet.Lib.ConstantsTemp.GENDERID_MALE + Matchnet.Lib.ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString());

            g.SaveSearchPreferences();
        }

        private void GetAgeRangeFromID(int AgeRangeID, ref int MinAge, ref int MaxAge)
        {
            switch (AgeRangeID)
            {
                case 0:
                    MinAge = 18;
                    MaxAge = 24;
                    break;
                case 1:
                    MinAge = 25;
                    MaxAge = 34;
                    break;
                case 2:
                    MinAge = 35;
                    MaxAge = 44;
                    break;
                case 3:
                    MinAge = 45;
                    MaxAge = 54;
                    break;
                case 4:
                    MinAge = 55;
                    MaxAge = 99;
                    break;
                default:
                    MinAge = 18;
                    MaxAge = 99;
                    break;
            }
        }

        private void createPreferencesTabCookie(string pTabValue)
        {
            HttpCookie prefTab = Request.Cookies.Get("mn_searchPrefTabState");

            if (prefTab == null)
            {
                prefTab = new HttpCookie("mn_searchPrefTabState");
            }
            prefTab.Value = pTabValue;
            Response.Cookies.Add(prefTab);
        }

        private void BindSortDropDown()
        {
            string key = "SearchOrderBy";
            DataTable cct = GetSearchSortOptions();

            // Proximity sorting option must be removed when searching by AreaCode or College.
            if (g.SearchPreferences["SearchTypeID"] != null)
            {
                int iSearchTypeID = Matchnet.Conversion.CInt(g.SearchPreferences["SearchTypeID"], Constants.NULL_INT);

                if ((SearchTypeID.AreaCode.Equals((SearchTypeID)iSearchTypeID))
                    || (SearchTypeID.College.Equals((SearchTypeID)iSearchTypeID)))
                {
                    int proximity = (int)Matchnet.Search.Interfaces.QuerySorting.Proximity;
                    DataRow proximitySortOption = null;

                    foreach (DataRow row in cct.Rows)
                    {
                        if (Conversion.CInt(row["Value"]) == proximity)
                        {
                            proximitySortOption = row;
                        }
                    }

                    if (proximitySortOption != null)
                    {
                        proximitySortOption.Delete();
                    }
                }
            }

            rptSearchOption.DataSource = cct;
            rptSearchOption.DataBind();
        }

        private DataTable GetSearchSortOptions()
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("Value", typeof(System.String));
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("ListOrder", typeof(System.Int32));

            for (int i = 0; i < 4; i++)
            {
                DataRow row = dt.NewRow();
                string sort = g.GetResource(String.Format("TXT_SEARCH_SORT_{0}", i + 1), this);
                int sorting = GetSorting(sort);
                if (sorting == (int)Matchnet.Search.Interfaces.QuerySorting.Popularity
                    && !SearchUtil.IsSearchSortByPopularityEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                {
                    continue;
                }
                row["Content"] = g.GetResource(String.Format("TXT_SEARCH_SORT_{0}_CONTENT", sort.ToUpper()), this);
                row["ListOrder"] = i + 1;
                row["Value"] = sorting;
                dt.Rows.Add(row);
            }

            if (SettingsManager.GetSettingBool(SettingConstants.ENABLE_COLORCODE_SEARCH, g.Brand))
            {
                DataRow row = dt.NewRow();
                row["Content"] = "Color Code";
                row["ListOrder"] = dt.Rows.Count + 1;
                row["Value"] = (int)Matchnet.Search.Interfaces.QuerySorting.ColorCode;
                dt.Rows.Add(row);
                colorCodeSelect.NavigateURL = "/Applications/Search/SearchResults.aspx?SearchOrderBy=5";
            }

            bool hasKeywordTerms = !string.IsNullOrEmpty(g.SearchPreferences["keywordsearch"]);
            if (hasKeywordTerms && SearchUtil.IsKeywordMatchesSearchPrefEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
            {
                DataRow row = dt.NewRow();
                row["Content"] = g.GetResource("TXT_SEARCH_SORT_KEYWORD_RELEVANCE", this);
                row["ListOrder"] = dt.Rows.Count + 1;
                row["Value"] = (int)Matchnet.Search.Interfaces.QuerySorting.KeywordRelevance;
                dt.Rows.Add(row);
            }

            if (g.Member != null && MatchRatingManager.Instance.IsSearchSortByMutualMatchEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID)
                && MatchRatingManager.Instance.DisplayMatchRatingOnMatches(g.Member.MemberID, g.Brand))
            {
                DataRow row = dt.NewRow();
                row["Content"] = g.GetResource("TXT_SEARCH_SORT_MUTUAL_MATCH", this);
                row["ListOrder"] = dt.Rows.Count + 1;
                row["Value"] = (int)Matchnet.Search.Interfaces.QuerySorting.MutualMatch;
                dt.Rows.Add(row);
            }
            return dt;
        }

        private int GetSorting(string sorting)
        {
            try
            {
                return (int)Enum.Parse(typeof(Matchnet.Search.Interfaces.QuerySorting), sorting);

            }
            catch (Exception ex)
            { throw ex; }

        }

        private string GetSearchOrderBy()
        {
            string ret = string.Empty;
            string searchOrder = string.Empty;

            // check the request for change in SearchOrderBy
            if (Request["SearchOrderBy"] != null)
            {
                searchOrder = Request["SearchOrderBy"];
                _g.SearchPreferences["SearchOrderBy"] = searchOrder;
            }
            else
                searchOrder = _g.SearchPreferences["SearchOrderBy"];

            //This string returned is used only for omniture, internally we only use the ID which is the integer
            switch (searchOrder)
            {
                case "1":
                    ret = "Newest";
                    break;
                case "2":
                    ret = "Most Active";
                    break;
                case "3":
                    ret = "Closest To You";
                    break;
                case "4":
                    ret = "Most Popular";
                    break;
                case "5":
                    ret = "Color Code";
                    break;
                case "6":
                    ret = "Keyword Relevance";
                    break;
                case "7":
                    ret = "Mutual Match";
                    break;
            }

            if (String.IsNullOrEmpty(ret))
            {

                ret = "Newest";
            }

            return ret;
        }

        private int GetDefaultSearchOrderBy()
        {
            int SearchOrderBy = (Int32)Matchnet.Search.Interfaces.QuerySorting.JoinDate;
            if (g.Member != null && MatchRatingManager.Instance.IsSearchSortByMutualMatchEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID)
                && MatchRatingManager.Instance.DisplayMatchRatingOnMatches(g.Member.MemberID, g.Brand))
            {
                SearchOrderBy = (Int32)Matchnet.Search.Interfaces.QuerySorting.MutualMatch;
            }
            return SearchOrderBy;
        }

        private bool showSpotlightProfile(System.Collections.Generic.List<int> searchIDs)
        {
            bool result = false; ;
            try
            {
                bool promoMemberFlag = false;
                Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("SearchResults", "Start retrieving spotlight profile", g);
                IMemberDTO m = FrameworkGlobals.GetSpotlightProfile(g, searchIDs, out promoMemberFlag);
                //Matchnet.Member.ServiceAdapters.Member m = g.Member;
                //Matchnet.Member.ServiceAdapters.Member m = MemberSA.Instance.GetMember(100046782, MemberLoadFlags.None);

                Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("SearchResults", "End retrieving spotlight profile", g);
                if (m != null)
                {
                    Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("SearchResults", "Spotlight profile=" + m.MemberID, g);
                    plcPromotionalProfile.Visible = true;
                    SpotlightProfile pr = (SpotlightProfile)LoadControl("/Framework/Ui/ProfileElements/SpotlightProfile.ascx");
                    pr.Member = m;
                    pr.EntryPoint = Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.SearchResults;
                    pr.IsPromotionalMember = promoMemberFlag;
                    pr.OnlineImageName = "icon-status-online.gif";
                    pr.OfflineImageName = "icon-status-offline.gif";
                    plcPromotionalProfile.Controls.Add(pr);
                    result = true;
                    _hasSpotlightProfile = true;
                }

                return result;
            }
            catch (Exception ex) { return result; }
        }

        private void SetSearchRedesignBetaToggler()
        {
            if (g.Member != null)
            {
                if (BetaHelper.GetSearchRedesign30BetaTest(g) == BetaHelper.BetaTestType.SearchRedesign30_DefaultOff
                    || BetaHelper.GetSearchRedesign30BetaTest(g) == BetaHelper.BetaTestType.SearchRedesign30_DefaultOn)
                {
                    phSearchRedesign30BetaToggler.Visible = true;
                    StringBuilder searchLink = new StringBuilder("/Applications/Search/SearchResults.aspx?");
                    bool first = true;
                    foreach (string key in Request.QueryString.AllKeys)
                    {
                        if (!String.IsNullOrEmpty(Request.QueryString[key])
                            && key != WebConstants.URL_PARAMETER_BETA_TEST_ENABLE
                            && key != WebConstants.URL_PARAMETER_BETA_TEST_DISABLE)
                        {
                            if (!first)
                            {
                                searchLink.Append("&");
                            }

                            if (key == "StartRow" && g.ResetSearchStartRow)
                                searchLink.Append(key + "=1");
                            else
                                searchLink.Append(key + "=" + Server.UrlEncode(Request.QueryString[key]));
                            first = false;
                        }
                    }

                    if (!first)
                    {
                        searchLink.Append("&");
                    }

                    if (BetaHelper.GetSearchRedesign30BetaTest(g) == BetaHelper.BetaTestType.SearchRedesign30_DefaultOn)
                        lnkSearchRedesign30BetaToggler.NavigateUrl = searchLink.Append(WebConstants.URL_PARAMETER_BETA_TEST_ENABLE + "=" + ((int)BetaHelper.BetaTestType.SearchRedesign30_DefaultOn).ToString()).ToString();
                    else
                        lnkSearchRedesign30BetaToggler.NavigateUrl = searchLink.Append(WebConstants.URL_PARAMETER_BETA_TEST_DISABLE + "=" + ((int)BetaHelper.BetaTestType.SearchRedesign30_DefaultOff).ToString()).ToString();

                    lnkSearchRedesign30BetaToggler.ToolTip = g.GetResource("TTL_BETA_TOGGLER_BUTTON", this);
                }
            }
        }
        #endregion

        #region Public Methods


        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rptSearchOption.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptSearchOption_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);
            this.PreRender += new System.EventHandler(this.Page_PreRender);

        }
        #endregion

        private void setSearchPreferencesFromQueryStringForFriendlyURL()
        {

            string fullURL = Context.Request.Url.AbsoluteUri;
            SearchPreferenceCollection preferences = g.SearchPreferences;

            string sectorTextResourceConstant = "TXT_SECTOR_TEXT_";
            string titleResourceConstant = "TXT_TITLE_TEXT_";
            string pageTitleResourceConstant = "TXT_PAGE_TITLE_";
            string pageMetaDescriptionResourceConstant = "TXT_PAGE_META_DESCRIPTION_";
            string breadCrumbTrailResourceConstant = "NAV_SUB_MATCHES";

            //if (g.Member == null)
            //{
            //    btnJoinNow.Visible = true;
            //    btnJoinNow.NavigateUrl = "/Applications/Registration/Registration.aspx";
            //}

            if (fullURL.Contains("religious-dating") || Request.QueryString["sector"] == "2")
            {
                sectorTextResourceConstant += "RELIGIOUS";
                titleResourceConstant += "RELIGIOUS";
                pageTitleResourceConstant += "RELIGIOUS";
                pageMetaDescriptionResourceConstant += "RELIGIOUS";

                SearchResultList.ResultListContextType = ResultContextType.SectorSearchResult;

                //preferences.Add("SearchTypeID", "2");
                //preferences.Add("SearchOrderBy", "1");
                //preferences.Add("CountryRegionID", "105");
                //preferences.Add("HasPhotoFlag", "1");
                //preferences.Add("GenderMask", "9");
                //preferences.Add("MinAge", "18");
                //preferences.Add("MaxAge", "40");
                //preferences.Add("JDateReligion", "47");
                //preferences.Add("KeepKosher", "2");
                //preferences.Add("SynagogueAttendance", "1");
                //preferences.Add("AreaCode1", "03");


                //  return "/Applications/Search/SearchResults.aspx?SearchTypeID=2&SearchOrderBy=1&CountryRegionID=105&HasPhotoFlag=1&GenderID=1&SeekingGenderID=8&MinAge=18&MaxAge=40&JDateReligion=57&KeepKosher=2&SynagogueAttendance=1&AreaCode1=03&Sector=2";
            }
            else if (fullURL.Contains("academic-dating") || Request.QueryString["sector"] == "1")
            {
                sectorTextResourceConstant += "ACADEMIC";
                titleResourceConstant += "ACADEMIC";
                pageTitleResourceConstant += "ACADEMIC";
                pageMetaDescriptionResourceConstant += "ACADEMIC";

                SearchResultList.ResultListContextType = ResultContextType.SectorSearchResult;

                //preferences.Add("SearchTypeID", "2");
                //preferences.Add("SearchOrderBy", "1");
                //preferences.Add("CountryRegionID", "105");
                //preferences.Add("HasPhotoFlag", "1");
                //preferences.Add("GenderMask", "9");
                //if (fullURL.Contains("gender=women"))
                //{
                //    preferences.Add("GenderMask", "6");
                //}
                //preferences.Add("MinAge", "21");
                //preferences.Add("MaxAge", "30");
                //preferences.Add("EducationLevel", "120");
                //preferences.Add("AreaCode1", "03");

                // return;


                //   return "/Applications/Search/SearchResults.aspx?SearchTypeID=2&SearchOrderBy=1&CountryRegionID=105&HasPhotoFlag=1&GenderID=1&SeekingGenderID=8&MinAge=21&MaxAge=30&EducationLevel=120&AreaCode1=03&Sector=1";
            }
            else if (fullURL.Contains("Les-hommes-juifs"))
            {

                SearchResultList.ResultListContextType = ResultContextType.SectorSearchResult;

                //preferences.Add("SearchTypeID", "4");
                //preferences.Add("SearchOrderBy", "1");
                //preferences.Add("GenderMask", "9");
                //preferences.Add("HasPhotoFlag", "1");
                //preferences.Add("MinAge", "18");
                //preferences.Add("MaxAge", "90");

                //preferences.Add("CountryRegionID", "76");
                //preferences.Add("regionid", "9795409");
                //preferences.Add("geodistance", "7");
                //preferences.Add("regionidcity", "9795409");
                //preferences.Add("distance", "160");
                //preferences.Add("jdatereligion", "24575");
            }
            else if (fullURL.Contains("Femmes-juives"))
            {

                SearchResultList.ResultListContextType = ResultContextType.SectorSearchResult;

                //preferences.Add("SearchTypeID", "4");
                //preferences.Add("SearchOrderBy", "1");
                //preferences.Add("GenderMask", "6");
                //preferences.Add("HasPhotoFlag", "1");
                //preferences.Add("MinAge", "18");
                //preferences.Add("MaxAge", "90");

                //preferences.Add("CountryRegionID", "76");
                //preferences.Add("regionid", "9795409");
                //preferences.Add("geodistance", "7");
                //preferences.Add("regionidcity", "9795409");
                //preferences.Add("distance", "160");
                //preferences.Add("jdatereligion", "24575");
            }
            else if (Request.QueryString["sector"] == "3")
            {
                SearchResultList.ResultListContextType = ResultContextType.SectorSearchResult;
                titleResourceConstant += "DIVORCED";
                //sectorTextResourceConstant += "DIVORCED";
                //pageTitleResourceConstant += "DIVORCED";
                //pageMetaDescriptionResourceConstant += "DIVORCED";
                sectorTextResourceConstant = null;
                pageTitleResourceConstant = null;
                pageMetaDescriptionResourceConstant = null;
            }
            else if (Request.QueryString["sector"] == "4")
            {
                SearchResultList.ResultListContextType = ResultContextType.SectorSearchResult;
                titleResourceConstant += "ELDERLY";
                //sectorTextResourceConstant += "ELDERLY";

                //pageTitleResourceConstant += "ELDERLY";
                //                        pageMetaDescriptionResourceConstant += "ELDERLY"; 
                sectorTextResourceConstant = null;
                pageTitleResourceConstant = null;
                pageMetaDescriptionResourceConstant = null;

            }
            else
                return;

            SearchPreferenceSummary1.IsSectorPage = true;
            // g.SaveSearchPreferences();
            plcSector.Visible = true;
            txtMatches.ResourceConstant = titleResourceConstant;
            breadCrumbTrailResourceConstant = titleResourceConstant;
            if (pageTitleResourceConstant != null)
            {
                txtSectorText.ResourceConstant = sectorTextResourceConstant;
                g.SetPageTitle(g.GetResource(pageTitleResourceConstant, this));
                g.SetMetaDescription(g.GetResource(pageMetaDescriptionResourceConstant, this));
            }
        }


    }
}
