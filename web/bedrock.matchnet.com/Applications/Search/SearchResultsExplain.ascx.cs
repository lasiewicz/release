using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Search
{
	/// <summary>
	/// Summary description for SearchResultsExplain.
	/// </summary>
	public class SearchResultsExplain : FrameworkControl
	{
		protected Repeater MemberRepeater;

		private void Page_Load(object sender, System.EventArgs e)
		{
			//if (!MemberP)
			//{
			//	return;
			//}

            ArrayList results = MemberSearchSA.Instance.SearchExplainDetailed(g.SearchPreferences, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
            ArrayList goodResults = new ArrayList();
			foreach (DetailedQueryResult result in results)
			{
                try
                {
                    Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(result.MemberID, MemberLoadFlags.None);
                    result.UserName = member.GetUserName(_g.Brand);

                    //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                    Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, member, g.Brand);
                    result.ThumbPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, member, g.Brand, photo,
                                                                            PhotoType.Thumbnail,
                                                                            PrivatePhotoImageType.Thumb,
                                                                            NoPhotoImageType.Thumb);
                    result.BrandID = g.Brand.BrandID;
                    goodResults.Add(result);
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }
            }

			MemberRepeater.ItemDataBound += new RepeaterItemEventHandler(MemberRepeater_ItemDataBound);
			MemberRepeater.DataSource = goodResults;
			MemberRepeater.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		private void MemberRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			Repeater tagRepeater = (Repeater) e.Item.FindControl("TagRepeater");
			tagRepeater.DataSource = ((DetailedQueryResult) e.Item.DataItem).Tags;
			tagRepeater.DataBind();
		}
	}
}
