﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AdvancedSearch.ascx.cs" Inherits="Matchnet.Web.Applications.AdvancedSearch.AdvancedSearch" %>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<%@ Register TagPrefix="mnfe" TagName="PickRegionNew" Src="../../Framework/Ui/FormElements/PickRegionNew.ascx" %>
<%@ Register TagPrefix="mnfe" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="WatermarkTextbox" Src="/Framework/Ui/FormElements/WatermarkTextbox.ascx" %>

<mn:Title runat="server" id="ttlPreferences" ResourceConstant="NAV_PREFERENCES" ImageName="hdr_prefrences.gif" CommunitiesForImage="CI;" />

<div id="search-pref">

<p class="sub"><mn:Txt ID="txtPrefDescription" runat="server" ResourceConstant="TXT_PREF_DESC" /></p>

<h2><mn:Txt id="Txt1" runat="server" ResourceConstant="TXT_BASICS" /></h2>
<mn:MultiValidator id="AgeMinValidator" tabIndex="-1" runat="server" Display="Dynamic" RequiredType="IntegerType"
	ControlToValidate="tbAgeMin" MinValResourceConstant="MIN_AGE_NOTICE" MaxValResourceConstant="MIN_AGE_NOTICE"  FieldNameResourceConstant="MIN_AGE_NOTICE" IsRequired="True" MinVal="18"
	MaxVal="99" AppendToErrorMessage="<br />" />
<mn:MultiValidator id="AgeMaxValidator" tabIndex="-1" runat="server" Display="Dynamic" RequiredType="IntegerType"
	ControlToValidate="tbAgeMax" MinValResourceConstant="MAX_AGE_NOTICE" MaxValResourceConstant="MAX_AGE_NOTICE" FieldNameResourceConstant="MAX_AGE_NOTICE" IsRequired="True" MinVal="18"
	MaxVal="99" AppendToErrorMessage="<br />" />
<mn:MultiValidator id="LocationValidator"  runat="server" Display="Dynamic" MinimumLength="1"
		        MaximumLength="50" RequiredType="StringType" FieldNameResourceConstant="LOCATION_REQUIRED"
		        IsRequired="False">
</mn:MultiValidator>	
	
<div class="filter">
    <strong><mn:Txt id="txtGender" runat="server" ResourceConstant="TXT_YOURE_A" /></strong>
    <asp:DropDownList ID="ddlGender" Runat="server" />
    <strong><mn:Txt id="txtSeekingGender" runat="server" ResourceConstant="TXT_SEEKING_A" /></strong>
    <asp:DropDownList ID="ddlSeekingGender" Runat="server" />
    <strong><mn:Txt id="txtAgeMin" runat="server" ResourceConstant="TXT_AGE" /></strong>
    <asp:TextBox ID="tbAgeMin" Runat="server" MaxLength="2" Width="28" />
    <strong><mn:Txt id="txtAgeMax" runat="server" ResourceConstant="TO_" /></strong>
    <asp:TextBox ID="tbAgeMax" Runat="server" MaxLength="2" Width="28" />
</div>
			
<div class="filter location">
    <anthem:PlaceHolder ID="plcDistance" Runat="server">
        <strong><mn:Txt id="txtWithin" ResourceConstant="TXT_LOCATED_WITHIN" runat="server"></mn:Txt></strong>
        <asp:DropDownList id="ddlDistance" Runat="server"></asp:DropDownList>&nbsp; 
        <mn:Txt id="txtOf" ResourceConstant="TXT_OF" runat="server"></mn:Txt>
	</anthem:PlaceHolder>
	<anthem:PlaceHolder ID="plcAreaCodes" Runat="server" Visible="false">
	    <strong><mn:Txt id="txtAreaCodes" ResourceConstant="TXT_IN_AREA_CODES" runat="server"></mn:Txt></strong>
	</anthem:PlaceHolder>
	<anthem:PlaceHolder ID="plcCollege" Runat="server" Visible="false">
		<strong><mn:Txt id="txtCollege" ResourceConstant="TXT_AT_COLLEGE" runat="server"></mn:Txt></strong>
	</anthem:PlaceHolder>
	
	<mn:Image ID="Image1" FileName="icon-pref-off.gif" runat="server" />
	<strong><anthem:HyperLink ID="lnkLocation" Runat="server" /></strong>
	    <mnfe:PickRegionNew id="prSearchRegion" Runat="server" />
</div>

<asp:PlaceHolder ID="phKeyword" runat="server" Visible="false">
<div class="filter keyword">
    <%--keyword--%>
    <label class="float-inside"><mn:Txt id="txtKeywordLabel" runat="server" ResourceConstant="TXT_KEYWORD_LABEL" /></label>
    <div class="rel-layer-container float-inside">
        <a href="#" rel="click"><mn:Image runat="server" filename="icon-help.gif" align="absmiddle" ID="Image2" /></a>
	    <div class="rel-layer-div">
			<p class="float-outside"><a href="#" class="click-close"><mn:Txt runat="server" id="Txt5" ResourceConstant="TXT_HELP_LAYER_CLOSE_LINK" /></a></p>
		    <h5><mn:Txt runat="server" id="txtKeywordTooltipHeader" ResourceConstant="TXT_KEYWORD_TOOLTIP_HEADER" /></h5>
            <mn:Txt runat="server" id="txtKeywordTooltip" ResourceConstant="TXT_KEYWORD_TOOLTIP" />
	    </div>
	</div>
    <uc1:WatermarkTextbox ID="txtKeyword" runat="server" TextMode="SingleLine" MaxLength="200" 
        CssClass="filled" WatermarkCssClass="watermark" WatermarkTextResourceConstant="TXT_KEYWORD_EXAMPLE">
    </uc1:WatermarkTextbox>
    <span class="optional"><mn:Txt ID="txtOptional" runat="server" ResourceConstant="TXT_OPTIONAL_PT" /></span>
</div>
</asp:PlaceHolder>

<p class="search-pref-option-container float-inside">
    <asp:CheckBox ID="cbHasPhoto" Runat="server" />
    <span class="margin-heavy"><asp:CheckBox ID="cbxRamahDate" Runat="server" /></span>
</p>

<div class="search-pref-container" id="prefSearchToggleContainer">
	<mnfe:SearchPreferenceRepeater ID="rptBasics" Runat="server" />
</div> <!-- end search-pref-container -->

<h4><mn:Txt id="Txt4" runat="server" ResourceConstant="TXT_ADDITIONAL" /></h4>

<a href="#" class="collapse-all-cont">
    <span class="spr s-icon-arrow-down-color"><span></span></span>
    <span class="expand"><mn:Txt ID="txt2" ResourceConstant="TXT_EXPAND_ALL" runat="server" /></span><span class="collapse hide"><mn:Txt ID="txt3" ResourceConstant="TXT_COLLAPSE_ALL" runat="server" /></span>
</a>

<asp:Repeater ID="rptAdditionalSections" Runat="server">
	<ItemTemplate>
		<h3 class="float-inside"><mn:Txt id="txtSectionTitle" runat="server" /></h3>
		<mnfe:SearchPreferenceRepeater ID="rptSection" Runat="server" />
	</ItemTemplate>
</asp:Repeater>

<a href="#" class="collapse-all-cont">
    <span class="spr s-icon-arrow-down-color"><span></span></span>
    <span class="expand"><mn:Txt ID="txtExpandAll1" ResourceConstant="TXT_EXPAND_ALL" runat="server" /></span><span class="collapse hide"><mn:Txt ID="txtCollapseAll1" ResourceConstant="TXT_COLLAPSE_ALL" runat="server" /></span>
</a>

<hr />
<p class="text-center"><asp:Button ID="btnSave" CssClass="btn primary" Runat="server" /></p>

</div>
<script type="text/javascript">

    $j(document).ready(function () {
        // Expand first preference
        var firstClicker = $j('#search-pref .search-pref-header:first');
        if(firstClicker.is('.search-pref-closed')){
            firstClicker.click();
        }

        // Toggle all: preferenceToggleAll($clicker, $container, clickerOpenClassName, clickerCloseClassName, arrowOpenClassName, arrowCloseClassName, openClassName, closeClassName)
        preferenceToggleAll(
            $j("#search-pref .collapse-all-cont"),
            $j("#search-pref"),
            "spr s-icon-arrow-up-color",
            "spr s-icon-arrow-down-color",
            "spr s-icon-arrow-down",
            "spr s-icon-arrow-right",
            "search-pref-open",
            "search-pref-closed"
            );
    });

    //preference slider translated text (var defined in spark.js)
    preferenceSliderTextArray = [<%=_g.GetResource("SEARCH_PREFERENCE_WEIGHTS_LIST", null)%>];
	
</script>
