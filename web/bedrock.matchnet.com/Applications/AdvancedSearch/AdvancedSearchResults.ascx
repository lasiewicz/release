﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdvancedSearchResults.ascx.cs" Inherits="Matchnet.Web.Applications.AdvancedSearch.AdvancedSearchResults" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/BasicElements/ResultList20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="/Framework/Ui/BasicElements/MiniProfile.ascx" %>
<%@ Register TagPrefix="mn1" TagName="ResultsViewType" Src="/Framework/Ui/BasicElements/ResultsViewType.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" TagName="ColorCodeSelect" Src="/Framework/Ui/BasicElements/ColorCodeSelect.ascx" %>

<div class="header-options clearfix">
    <mn:Title runat="server" id="txtMatches" ResourceConstant="NAV_SUB_ADVANCED_SEARCH" ImageName="hdr_search_results.gif" CommunitiesForImage="CI;" />
	<div class="links">
		<mn:txt id="SearchPreferences" runat="server" href="/Applications/AdvancedSearch/AdvancedSearchPreferences.aspx" resourceconstant="REVISE_YOUR_SEARCH_PREFERENCES" />
	</div>
	<div class="pagination">
		<asp:label id="lblListNavigationTop" Runat="server" />
	</div>
</div>
   
<asp:PlaceHolder id="plcPromotionalProfile" Runat="server" Visible="false"></asp:PlaceHolder>

	<div class="sort-display" style="border:none">
		
		<ul class="nav-rounded-tabs no-click clearfix">
		    <li class="no-tab search-type"><mn:txt runat="server" id="txtView" resourceconstant="TXT_VIEW_BY" /></li>
            <asp:Repeater id="rptSearchOption" Runat="server">
		        <ItemTemplate>
		            <li class="<asp:Literal id=litSortSpan runat=server /> tab">
			            <asp:HyperLink id="lnkSort" Runat="server" />
			            <span class="x"><asp:Literal id="litSortTitle" Runat="server" Visible="False" /></span>
		            </li>
		        </ItemTemplate>
	        </asp:Repeater>
	        <li class="no-tab view-type image-text-pair">
		        <mn1:ResultsViewType id="idResultsViewType" runat="server" />   
		    </li>
	    </ul>
	    
	    <mn1:ColorCodeSelect id="colorCodeSelect" runat="server" />

		<input type="hidden" name="hidSearchOrderBy" runat="server" id="hidSearchOrderBy" />
</div>

<div id="results-container" class="clearfix clear-both columnator">
    <asp:placeholder id="Results" runat="server" visible="true">
	    <result:resultlist id="SearchResultList" runat="server" resultlistcontexttype="AdvancedSearchResult"></result:resultlist>
    	
        <mn:txt id="promoDiv" runat="server" ResourceConstant="TXT_PROMO_DIV" Visible="false" />
	    <input type="hidden" value="<% = ChangeVoteMessage %>" name="ChangeVoteMessage" /> <iframe tabindex="-1" name="FrameYNMVote" frameborder="0" width="1" scrolling="no" height="1">
		    <layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
	    </iframe>
    </asp:placeholder>
</div>
	
<div class="pagination" >
    <asp:label id="lblListNavigationBottom" Runat="server"></asp:label>
</div>
