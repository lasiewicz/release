﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.AdvancedSearch
{
    public class AdvancedSearchPersistence
    {
        private int _memberID;
        private string _cookieName = "ADVS_{0}";
        private HttpCookie _persistCookie;
        private int _expirationDays;
        
        public int GenderMask { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int Distance { get; set; }
        public int RegionID { get; set; }
        public int HasPhotoFlag { get; set; }
        public int SchoolID { get; set; }
        public int SearchTypeID { get; set; }
        public int JdateReligion { get; set; }
        public int AreaCode1 { get; set; }
        public int AreaCode2 { get; set; }
        public int AreaCode3 { get; set; }
        public int AreaCode4 { get; set; }
        public int AreaCode5 { get; set; }
        public int AreaCode6 { get; set; }
        public int SearchOrderBy { get; set; }
        public int RamahAlum { get; set; }
        // Advanced search parameters
        public int MinHeight { get; set; }
        public int MaxHeight { get; set; }
        public int Ethnicity { get; set; }
        public int JDateEthnicity { get; set; }
        public int EducationLevel { get; set; }
        public int MaritalStatus { get; set; }
        public int BodyType { get; set; }
        public int SmokingHabits { get; set; }
        public int DrinkingHabits { get; set; }
        public int Religion { get; set; }
        public int ChildrenCount { get; set; }
        public int KeepKosher { get; set; }
        public int SynagogueAttendance { get; set; }
        public int MoreChildrenFlag { get; set; }
        public int Custody { get; set; }
        public int ActivityLevel { get; set; }
        public int LanguageMask { get; set; }
        public int RelocateFlag { get; set; }
        public string KeywordSearch { get; set; }

        public int MemberID
        {
            get
            {
                return _memberID;
            }
        }

        public AdvancedSearchPersistence(int memberID, int expirationDays)
        {
            _memberID = memberID;
            string _name = string.Format(_cookieName, memberID.ToString());

            _persistCookie = HttpContext.Current.Request.Cookies[_name];

            if (_persistCookie == null)
            {
                _persistCookie = new HttpCookie(_name, _name);
                InitializeNullValues();
            }
            else
            {
                LoadValues();
            }

            _expirationDays = expirationDays;

            _persistCookie.Expires = DateTime.Now.AddDays(_expirationDays);
        }

        public void PersistValues()
        {
            _persistCookie["GenderMask"] = GenderMask.ToString();
            _persistCookie["MinAge"] = MinAge.ToString();
            _persistCookie["MaxAge"] = MaxAge.ToString();
            _persistCookie["Distance"] = Distance.ToString();
            _persistCookie["RegionID"] = RegionID.ToString();
            _persistCookie["HasPhotoFlag"] = HasPhotoFlag.ToString();
            _persistCookie["SchoolID"] = SchoolID.ToString();
            _persistCookie["SearchTypeID"] = SearchTypeID.ToString();
            _persistCookie["JdateReligion"] = JdateReligion.ToString();
            _persistCookie["AreaCode1"] = AreaCode1.ToString();
            _persistCookie["AreaCode2"] = AreaCode2.ToString();
            _persistCookie["AreaCode3"] = AreaCode3.ToString();
            _persistCookie["AreaCode4"] = AreaCode4.ToString();
            _persistCookie["AreaCode5"] = AreaCode5.ToString();
            _persistCookie["AreaCode6"] = AreaCode6.ToString();
            _persistCookie["MinHeight"] = MinHeight.ToString();
            _persistCookie["MaxHeight"] = MaxHeight.ToString();
            _persistCookie["Ethnicity"] = Ethnicity.ToString();
            _persistCookie["EducationLevel"] = EducationLevel.ToString();
            _persistCookie["MaritalStatus"] = MaritalStatus.ToString();
            _persistCookie["BodyType"] = BodyType.ToString();
            _persistCookie["SmokingHabits"] = SmokingHabits.ToString();
            _persistCookie["DrinkingHabits"] = DrinkingHabits.ToString();
            _persistCookie["Religion"] = Religion.ToString();
            _persistCookie["ChildrenCount"] = ChildrenCount.ToString();
            _persistCookie["JDateEthnicity"] = JDateEthnicity.ToString();
            _persistCookie["SynagogueAttendance"] = SynagogueAttendance.ToString();
            _persistCookie["KeepKosher"] = KeepKosher.ToString();
            _persistCookie["SearchOrderBy"] = SearchOrderBy.ToString();
            _persistCookie["MoreChildrenFlag"] = MoreChildrenFlag.ToString();
            _persistCookie["Custody"] = Custody.ToString();
            _persistCookie["ActivityLevel"] = ActivityLevel.ToString();
            _persistCookie["LanguageMask"] = LanguageMask.ToString();
            _persistCookie["RelocateFlag"] = RelocateFlag.ToString();
            _persistCookie["KeywordSearch"] = KeywordSearch;
            _persistCookie["RamahAlum"] = RamahAlum.ToString();
            
            _persistCookie.Expires = DateTime.Now.AddDays(_expirationDays);
            HttpContext.Current.Response.Cookies.Add(_persistCookie);
        }

        public void InitializeNullValues()
        {
            GenderMask = Constants.NULL_INT;
            MinAge = Constants.NULL_INT;
            MaxAge = Constants.NULL_INT;
            Distance = Constants.NULL_INT;
            RegionID = Constants.NULL_INT;
            HasPhotoFlag = Constants.NULL_INT;
            SchoolID = Constants.NULL_INT;
            SearchTypeID = Constants.NULL_INT;
            JdateReligion = Constants.NULL_INT;
            AreaCode1 = Constants.NULL_INT;
            AreaCode2 = Constants.NULL_INT;
            AreaCode3 = Constants.NULL_INT;
            AreaCode4 = Constants.NULL_INT;
            AreaCode5 = Constants.NULL_INT;
            AreaCode6 = Constants.NULL_INT;
            MinHeight = Constants.NULL_INT;
            MaxHeight = Constants.NULL_INT;
            Ethnicity = Constants.NULL_INT;
            EducationLevel = Constants.NULL_INT;
            MaritalStatus = Constants.NULL_INT;
            BodyType = Constants.NULL_INT;
            SmokingHabits = Constants.NULL_INT;
            DrinkingHabits = Constants.NULL_INT;
            Religion = Constants.NULL_INT;
            ChildrenCount = Constants.NULL_INT;
            JDateEthnicity = Constants.NULL_INT;
            SynagogueAttendance = Constants.NULL_INT;
            KeepKosher = Constants.NULL_INT;
            SearchOrderBy = Constants.NULL_INT;
            MoreChildrenFlag = Constants.NULL_INT;
            Custody = Constants.NULL_INT;
            ActivityLevel = Constants.NULL_INT;
            LanguageMask = Constants.NULL_INT;
            RelocateFlag = Constants.NULL_INT;
            KeywordSearch = "";
            RamahAlum = Constants.NULL_INT;
        }

        public string GetPersistedValue(string key)
        {
            return _persistCookie[key].ToString();
        }

        public void Clear()
        {
            if (_persistCookie != null)
                _persistCookie.Expires = DateTime.Now.AddYears(-30);
            HttpContext.Current.Response.Cookies.Add(_persistCookie);
        }

        private void LoadValues()
        {
            GenderMask = Convert.ToInt32(_persistCookie["GenderMask"]);
            MinAge = Convert.ToInt32(_persistCookie["MinAge"]);
            MaxAge = Convert.ToInt32(_persistCookie["MaxAge"]);
            Distance = Convert.ToInt32(_persistCookie["Distance"]);
            RegionID = Convert.ToInt32(_persistCookie["RegionID"]);
            HasPhotoFlag = Convert.ToInt32(_persistCookie["HasPhotoFlag"]);
            SchoolID = _persistCookie["SchoolID"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["SchoolID"]);
            SearchTypeID = _persistCookie["SearchTypeID"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["SearchTypeID"]);
            JdateReligion = _persistCookie["JdateReligion"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["JdateReligion"]);
            AreaCode1 = _persistCookie["AreaCode1"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["AreaCode1"]);
            AreaCode2 = _persistCookie["AreaCode2"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["AreaCode2"]);
            AreaCode3 = _persistCookie["AreaCode3"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["AreaCode3"]);
            AreaCode4 = _persistCookie["AreaCode4"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["AreaCode4"]);
            AreaCode5 = _persistCookie["AreaCode5"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["AreaCode5"]);
            AreaCode6 = _persistCookie["AreaCode6"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["AreaCode6"]);
            MinHeight = _persistCookie["MinHeight"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["MinHeight"]);
            MaxHeight = _persistCookie["MaxHeight"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["MaxHeight"]);
            Ethnicity = _persistCookie["Ethnicity"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["Ethnicity"]);
            EducationLevel = _persistCookie["EducationLevel"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["EducationLevel"]);
            MaritalStatus = _persistCookie["MaritalStatus"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["MaritalStatus"]);
            BodyType = _persistCookie["BodyType"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["BodyType"]);
            SmokingHabits = _persistCookie["SmokingHabits"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["SmokingHabits"]);
            DrinkingHabits = _persistCookie["DrinkingHabits"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["DrinkingHabits"]);
            Religion = _persistCookie["Religion"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["Religion"]);
            ChildrenCount = _persistCookie["ChildrenCount"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["ChildrenCount"]);
            JDateEthnicity = _persistCookie["JDateEthnicity"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["JDateEthnicity"]);
            SynagogueAttendance = _persistCookie["SynagogueAttendance"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["SynagogueAttendance"]);
            KeepKosher = _persistCookie["KeepKosher"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["KeepKosher"]);
            SearchOrderBy = _persistCookie["SearchOrderBy"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["SearchOrderBy"]);
            MoreChildrenFlag = _persistCookie["MoreChildrenFlag"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["MoreChildrenFlag"]);
            Custody = _persistCookie["Custody"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["Custody"]);
            ActivityLevel = _persistCookie["ActivityLevel"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["ActivityLevel"]);
            LanguageMask = _persistCookie["LanguageMask"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["LanguageMask"]);
            RelocateFlag = _persistCookie["RelocateFlag"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["RelocateFlag"]);
            KeywordSearch = string.IsNullOrEmpty(_persistCookie["KeywordSearch"]) ? "" : _persistCookie["KeywordSearch"];
            RamahAlum = string.IsNullOrEmpty(_persistCookie["RamahAlum"]) ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["RamahAlum"]);
        }


    }
}
