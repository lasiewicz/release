﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;
using System.Text;
using System.Xml.XPath;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using System.Diagnostics;
using System.IO;

namespace Matchnet.Web.Applications.ColorCode
{
    public class ColorCodeHelper
    {
        #region Constructor
        static ColorCodeHelper()
        {
        }
        #endregion

        #region Static Methods
        public static Quiz GetQuiz(ContextGlobal g)
        {
            Quiz quiz = null;
            string quizCacheKey = "ColorCodeQuizObject";

            try
            {
                if (g.Cache[quizCacheKey] == null)
                {
                    //get path to xml file
                    string xmlPath = System.Web.HttpContext.Current.Server.MapPath("/Applications/ColorCode/Quiz.xml");

                    //get quiz
                    System.Xml.XPath.XPathDocument xmlDoc = new System.Xml.XPath.XPathDocument(xmlPath);
                    quiz = LoadQuiz(xmlDoc, new Quiz());

                    //cache the quiz
                    //System.Web.Caching.CacheDependency cacheFileDependencies = new System.Web.Caching.CacheDependency(new string[] { xmlPath });
                    //Removing Cache dependency since the cache will be cleared on every web deployment
                    g.Cache.Insert(quizCacheKey, quiz);
                }
                else
                {
                    quiz = g.Cache[quizCacheKey] as Quiz;
                }
            }
            catch (Exception ex)
            {
                Trace.Write("Error parsing Quiz xml. Error: " + ex.Message);
            }

            return quiz;
        }

        public static MemberQuiz GetMemberQuiz(IMemberDTO member, Brand brand)
        {
            MemberQuiz memberQuiz = new MemberQuiz();

            if (member != null && brand != null)
            {
                string xmlResults = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, WebConstants.ATTRIBUTE_NAME_COLORCODEQUIZANSWERS, "");
                if (!String.IsNullOrEmpty(xmlResults))
                {
                    try
                    {
                        try
                        {
                            StringReader sr = new StringReader(xmlResults);
                            System.Xml.XPath.XPathDocument xmlDoc = new XPathDocument(sr);
                            memberQuiz = LoadQuiz(xmlDoc, memberQuiz) as MemberQuiz;
                        }
                        catch (Exception ex) { }
                        //TODO - Check if test is complete, if so, load the other properties from attribute or call calculate
                        memberQuiz.PrimaryColor = GetPrimaryColor(member, brand);
                        if (memberQuiz.PrimaryColor != Color.none)
                        {
                            memberQuiz.SecondaryColor = GetSecondaryColor(member, brand);

                            //round scores for display purposes
                            memberQuiz.Scores[Color.blue] = Math.Round(GetScore(member, brand, Color.blue));
                            memberQuiz.Scores[Color.red] = Math.Round(GetScore(member, brand, Color.red));
                            memberQuiz.Scores[Color.white] = Math.Round(GetScore(member, brand, Color.white));

                            //since we're rounding, we'll ensure it adds to 100
                            memberQuiz.Scores[Color.yellow] = 100 - memberQuiz.Scores[Color.blue] - memberQuiz.Scores[Color.red] - memberQuiz.Scores[Color.white];//GetScore(member, brand, Color.yellow);

                            memberQuiz.QuizCompleteDate = GetQuizCompleteDate(member, brand);
                        }
                    }
                    catch (Exception ex)
                    {
                        Trace.Write("Error parsing MemberQuiz xml. MemberID: " + member.MemberID.ToString() + "BrandID: " + brand.BrandID.ToString() + ". Error: " + ex.Message);
                    }
                }
            }

            return memberQuiz;
        }

        public static string GenerateQuizResultsAsXmlString(MemberQuiz memberQuiz)
        {
            StringBuilder xmlResults = new StringBuilder();

            xmlResults.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            xmlResults.Append("<quiz fixedAdjustFactor=\"" + memberQuiz.FixedAdjustFactor.ToString() + "\" fixedTotalYellow=\"" + memberQuiz.FixedTotalYellow.ToString() 
                + "\" fixedTotalWhite=\"" + memberQuiz.FixedTotalWhite.ToString() + "\" fixedTotalRed=\"" + memberQuiz.FixedTotalRed.ToString() + "\" fixedTotalBlue=\"" + memberQuiz.FixedTotalBlue.ToString() + "\">");

            foreach (QuestionType qt in memberQuiz.Questions.Keys)
            {
                xmlResults.Append("<questions questionType=\"" + qt.ToString() + "\">");
                foreach (Question q in memberQuiz.Questions[qt])
                {
                    xmlResults.Append("<question id=\"" + q.ID.ToString() + "\">");
                    foreach (Choice c in q.Choices)
                    {
                        xmlResults.Append("<choice id=\"" + c.ID.ToString() + "\"></choice>");
                    }
                    xmlResults.Append("</question>");
                }
                xmlResults.Append("</questions>");
            }
            
            xmlResults.Append("</quiz>");


            return xmlResults.ToString();
        }

        public static QuizStatus GetMemberQuizStatus(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
        {
            QuizStatus status = QuizStatus.none;

            if (HasMemberCompletedQuiz(member, brand))
            {
                status = QuizStatus.complete;
            }
            else
            {
                string xmlResults = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, WebConstants.ATTRIBUTE_NAME_COLORCODEQUIZANSWERS, "");
                if (!String.IsNullOrEmpty(xmlResults) && xmlResults != "0" && xmlResults != "none")
                {
                    status = QuizStatus.partial;
                }
            }

            return status;
        }

        public static bool HasMemberCompletedQuiz(IMemberDTO member, Brand brand)
        {
            //It is safe to assume that a member will only have a valid primary color after completing the quiz.
            Color color = GetPrimaryColor(member, brand);
            if (color != Color.none)
                return true;
            else
                return false;
        }

        public static Color GetPrimaryColor(IMemberDTO member, Brand brand)
        {
            Color color = Color.none;

            if (member != null && brand != null)
            {
                string attributeText = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, WebConstants.ATTRIBUTE_NAME_COLORCODEPRIMARYCOLOR, "");
                if (!String.IsNullOrEmpty(attributeText))
                {
                    color = (Color)Enum.Parse(typeof(Color), attributeText);
                }
            }
            return color;
        }

        public static Color GetSecondaryColor(IMemberDTO member, Brand brand)
        {
            Color color = Color.none;

            if (member != null && brand != null)
            {
                string attributeText = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, WebConstants.ATTRIBUTE_NAME_COLORCODESECONDARYCOLOR, "");
                if (!String.IsNullOrEmpty(attributeText))
                {
                    color = (Color)Enum.Parse(typeof(Color), attributeText);
                }
            }

            return color;
        }

        public static decimal GetScore(IMemberDTO member, Brand brand, Color color)
        {
            decimal score = Constants.NULL_DECIMAL;

            if (member != null && brand != null)
            {
                string attributeText = "";
                switch (color)
                {
                    case Color.blue:
                        attributeText = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, WebConstants.ATTRIBUTE_NAME_COLORCODEBLUESCORE, "");
                        break;
                    case Color.red:
                        attributeText = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, WebConstants.ATTRIBUTE_NAME_COLORCODEREDSCORE, "");
                        break;
                    case Color.white:
                        attributeText = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, WebConstants.ATTRIBUTE_NAME_COLORCODEWHITESCORE, "");
                        break;
                    case Color.yellow:
                        attributeText = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, WebConstants.ATTRIBUTE_NAME_COLORCODEYELLOWSCORE, "");
                        break;

                }

                if (!String.IsNullOrEmpty(attributeText))
                {
                    score = Convert.ToDecimal(attributeText);
                }
            }

            return score;
        }

        public static string GetColorCodeLink(IMemberDTO member, Brand brand)
        {
            string URL = "/Applications/ColorCode/Landing.aspx";
            //TODO - Determine whether we will add logic here to identify right URL based on whether member has taken test

            return URL;
        }

        public static bool IsMemberColorCodeHidden(IMemberDTO member, Brand brand)
        {
            string attributeText = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, WebConstants.ATTRIBUTE_NAME_COLORCODEHIDDEN, "");

            if (attributeText.ToLower() == "true")
                return true;
            else
                return false;

        }

        public static void SetMemberColorCodePrivacy(Matchnet.Member.ServiceAdapters.Member member, Brand brand, bool hide)
        {
            member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEHIDDEN, hide.ToString().ToLower(), Matchnet.Member.ValueObjects.TextStatusType.Auto);
            Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
        }

        public static DateTime GetQuizCompleteDate(IMemberDTO member, Brand brand)
        {
            DateTime dateComplete = DateTime.MinValue;

            if (member != null && brand != null)
            {
                dateComplete = member.GetAttributeDate(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEQUIZCOMPLETEDATE, DateTime.MinValue);

            }
            return dateComplete;
        }

        public static void SaveMemberCompletedColorCodeQuiz(Matchnet.Member.ServiceAdapters.Member member, Brand brand, MemberQuiz memberQuiz, Quiz quiz)
        {
            string quizXml = ColorCodeHelper.GenerateQuizResultsAsXmlString(memberQuiz);
            member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEQUIZANSWERS, quizXml, Matchnet.Member.ValueObjects.TextStatusType.Auto);

            //calculate results
            memberQuiz.CalculateResults(quiz);

            //save primary color
            member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEPRIMARYCOLOR, memberQuiz.PrimaryColor.ToString(), Matchnet.Member.ValueObjects.TextStatusType.Auto);

            //save secondary color
            member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODESECONDARYCOLOR, memberQuiz.SecondaryColor.ToString(), Matchnet.Member.ValueObjects.TextStatusType.Auto);

            //save scores
            member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEBLUESCORE, memberQuiz.Scores[Color.blue].ToString("N2"), Matchnet.Member.ValueObjects.TextStatusType.Auto);
            member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEREDSCORE, memberQuiz.Scores[Color.red].ToString("N2"), Matchnet.Member.ValueObjects.TextStatusType.Auto);
            member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEWHITESCORE, memberQuiz.Scores[Color.white].ToString("N2"), Matchnet.Member.ValueObjects.TextStatusType.Auto);
            member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEYELLOWSCORE, memberQuiz.Scores[Color.yellow].ToString("N2"), Matchnet.Member.ValueObjects.TextStatusType.Auto);

            //save completed date
            member.SetAttributeDate(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEQUIZCOMPLETEDATE, DateTime.Now);

            Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
        }

        public static string GetFormattedColorText(Color color)
        {
            string colorText = "";
            switch (color)
            {
                case Color.blue:
                    colorText = "Blue";
                    break;
                case Color.red:
                    colorText = "Red";
                    break;
                case Color.white:
                    colorText = "White";
                    break;
                case Color.yellow:
                    colorText = "Yellow";
                    break;
            }

            return colorText;
        }

        public static bool IsColorCodeEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_COLORCODE", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        /// <summary>
        /// Handles omniture link tracking that brought user to color code (e.g. emails, ads, banners, force)
        /// </summary>
        /// <param name="request"></param>
        /// <param name="globalContext"></param>
        public static void HandleColorCodeTestLinkTracking(HttpRequest request, ContextGlobal globalContext)
        {
            if (request.QueryString["colortracking"] == null) return;

            switch (request.QueryString["colortracking"].ToLower())
            {
                case "sidebarhomead":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - SideBar Home Ad";
                    break;
                case "footerhomead":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Footer Home Ad";
                    break;
                case "forcepage":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Force Page";
                    break;
                case "forcereminder":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Force Reminder";
                    break;
                case "emailcampaign":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Email Campaign";
                    break;
                case "emailfriend":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Email Friend";
                    break;
                case "emailinvite":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Email Invite";
                    break;
                case "emailsly":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Email Sounds Like You";
                    break;
                case "profileviewernottaken":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Profile Viewer Not Taken";
                    break;
                case "profilemembernottaken":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Profile Member Not Taken";
                    break;
                case "miniprofilewit":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Mini Profile What is This";
                    break;
                case "membersvc":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Member Service Chemistry Page";
                    break;
                case "promo":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Promo";
                    break;
                case "sub":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Profile Sub Nav";
                    break;
                case "gatepage":
                    globalContext.AnalyticsOmniture.Evar5 = "ColorCodeTestLink - Gate Page";
                    break;
            }
        }

        #endregion

        #region Private Methods
        private static Quiz LoadQuiz(XPathDocument xmlDoc, Quiz quiz)
        {
            System.Xml.XPath.XPathNavigator xmlNav = xmlDoc.CreateNavigator();
            System.Xml.XPath.XPathNodeIterator iterator = xmlNav.Select("/quiz");

            while (iterator.MoveNext())
            {
                System.Xml.XPath.XPathNavigator xmlNode = iterator.Current;
                quiz.FixedAdjustFactor = Convert.ToDecimal(xmlNode.GetAttribute("fixedAdjustFactor", ""));
                quiz.FixedTotalBlue = Convert.ToDecimal(xmlNode.GetAttribute("fixedTotalBlue", ""));
                quiz.FixedTotalRed = Convert.ToDecimal(xmlNode.GetAttribute("fixedTotalRed", ""));
                quiz.FixedTotalWhite = Convert.ToDecimal(xmlNode.GetAttribute("fixedTotalWhite", ""));
                quiz.FixedTotalYellow = Convert.ToDecimal(xmlNode.GetAttribute("fixedTotalYellow", ""));

                //get questions
                System.Xml.XPath.XPathNodeIterator questionsIterator = xmlNode.Select("./questions");
                while (questionsIterator.MoveNext())
                {
                    System.Xml.XPath.XPathNavigator xmlNodeQuestions = questionsIterator.Current;
                    List<Question> questionsList = new List<Question>();
                    QuestionType questionType = (QuestionType)Enum.Parse(typeof(QuestionType), xmlNodeQuestions.GetAttribute("questionType", ""));

                    //get question
                    System.Xml.XPath.XPathNodeIterator questionIterator = xmlNodeQuestions.Select("./question");
                    while (questionIterator.MoveNext())
                    {
                        System.Xml.XPath.XPathNavigator xmlNodeQuestion = questionIterator.Current;
                        Question question = new Question();
                        question.QuestionType = questionType;
                        question.ID = Convert.ToInt32(xmlNodeQuestion.GetAttribute("id", ""));
                        question.Text = xmlNodeQuestion.GetAttribute("text", "");

                        //get choice
                        System.Xml.XPath.XPathNodeIterator choiceIterator = xmlNodeQuestion.Select("./choice");
                        while (choiceIterator.MoveNext())
                        {
                            System.Xml.XPath.XPathNavigator xmlNodeChoice = choiceIterator.Current;
                            Choice choice = new Choice();
                            choice.ID = Convert.ToInt32(xmlNodeChoice.GetAttribute("id", ""));
                            if (xmlNodeChoice.GetAttribute("color", "") != String.Empty)
                                choice.Color = (Color)Enum.Parse(typeof(Color), xmlNodeChoice.GetAttribute("color", ""));
                            if (xmlNodeChoice.GetAttribute("weight", "") != String.Empty)
                                choice.Weight = Convert.ToDecimal(xmlNodeChoice.GetAttribute("weight", ""));
                            choice.Text = xmlNodeChoice.GetAttribute("text", "");

                            //add choice
                            question.Choices.Add(choice);
                        }

                        //add question
                        questionsList.Add(question);
                    }

                    //add questions
                    quiz.Questions.Add(questionType, questionsList);
                }

                //only single quiz element for now
                break;
            }

            return quiz;

        }

        

        #endregion

    }

    public enum Color
    {
        none = 0,
        red = 1,
        blue = 2,
        yellow = 3,
        white = 4
    }

    public enum QuestionType
    {
        none = 0,
        part1 = 1,
        part2 = 2
    }

    public enum QuizStatus
    {
        none = 0,
        partial = 1,
        complete = 2
    }

    public enum ReportType
    {
        none = -1,
        intro = 0,
        hot = 1,
        not = 2,
        need = 3,
        want = 4,
        otherIntro = 5,
        otherR = 6,
        otherB = 7,
        otherY = 8,
        otherW = 9,
        matchIntro = 10,
        matchR = 11,
        matchB = 12,
        matchY = 13,
        matchW = 14
    }

}
