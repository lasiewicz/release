﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.ColorCode
{
    public partial class Quiz2 : FrameworkControl
    {
        private Quiz _quiz = null;
        private MemberQuiz _memberQuiz = null;

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if (g.Member != null && ColorCodeHelper.IsColorCodeEnabled(g.Brand))
            {
                //show appropriate right content
                g.RightNavControl.ShowColorCodeTestHelp();
                g.RightNavControl.HideGamChannels();

                //get member's quiz
                _memberQuiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);

                //check if member has already completed the quiz
                if (!_memberQuiz.IsQuizComplete())
                {
                    //check if member finished part 1 of quiz
                    if (_memberQuiz.IsQuizPart1Complete())
                    {
                        //get quiz
                        _quiz = ColorCodeHelper.GetQuiz(g);

                        if (!Page.IsPostBack)
                        {
                            //load quiz
                            repeaterQuestions.DataSource = _quiz.Questions[QuestionType.part2];
                            repeaterQuestions.DataBind();
                        }
                    }
                    else
                    {
                        g.Transfer("/Applications/ColorCode/Quiz1.aspx");
                    }
                }
                else
                {
                    g.Transfer("/Applications/ColorCode/Analysis.aspx");
                }
            }
            else
            {
                g.Transfer("/Applications/ColorCode/Landing.aspx");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //show appropriate right content
            g.RightNavControl.ShowColorCodeTestHelp();
            g.RightNavControl.HideGamChannels();

            base.OnPreRender(e);
        }

        /// <summary>
        /// Handler for binding Questions for quiz
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RepeaterSections_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //get data object
                Question dataItem = e.Item.DataItem as Question;

                //Question text
                Literal literalQuestionText = (Literal)e.Item.FindControl("literalQuestionText");
                literalQuestionText.Text = dataItem.Text;

                //Choices
                int indexCount = 1;
                foreach (Choice c in dataItem.Choices)
                {
                    Literal radioChoice = e.Item.FindControl("radioChoice" + indexCount.ToString()) as Literal;
                    if (radioChoice != null)
                    {
                        int memberChoiceID = _memberQuiz.GetSelectedChoiceID(dataItem.ID);
                        if (c.ID == memberChoiceID)
                            radioChoice.Text = "<input id=\"choice" + dataItem.ID.ToString() + "_" + c.ID.ToString() + "\" type=\"radio\" name=\"question" + dataItem.ID.ToString() + "\" value=\"" + c.ID.ToString() + "\" checked=\"checked\" />";
                        else
                            radioChoice.Text = "<input id=\"choice" + dataItem.ID.ToString() + "_" + c.ID.ToString() + "\" type=\"radio\" name=\"question" + dataItem.ID.ToString() + "\" value=\"" + c.ID.ToString() + "\" />";

                        radioChoice.Text += "<label for=\"choice" + dataItem.ID.ToString() + "_" + c.ID.ToString() + "\">" + c.Text + "</label>";
                    }
                    indexCount++;
                }

            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //validate and save quiz
                string errorQuestions = "";

                int questionCount = 1;
                foreach (Question q in _quiz.Questions[QuestionType.part2])
                {
                    bool answered = false;
                    if (Request.Form["question" + q.ID.ToString()] != null)
                    {
                        int choiceID = Convert.ToInt32(Request.Form["question" + q.ID.ToString()]);
                        _memberQuiz.AddQuestionAnswer(q.ID, choiceID, _quiz);
                        answered = true;
                    }

                    if (!answered)
                    {
                        errorQuestions += ", " + (questionCount + 30).ToString(); //temporarily add 30 to fix error message, until we modify xml to include order number
                    }
                    questionCount++;
                }

                if (!String.IsNullOrEmpty(errorQuestions))
                {
                    //save partial test
                    string quizXml = ColorCodeHelper.GenerateQuizResultsAsXmlString(_memberQuiz);
                    g.Member.SetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_COLORCODEQUIZANSWERS, quizXml, Matchnet.Member.ValueObjects.TextStatusType.Auto);
                    Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(g.Member);

                    g.Notification.AddErrorString("Please select your answer for questions: " + errorQuestions.Substring(2));

                    //reload quiz
                    repeaterQuestions.DataSource = _quiz.Questions[QuestionType.part2];
                    repeaterQuestions.DataBind();
                }
                else
                {
                    //save completed test
                    ColorCodeHelper.SaveMemberCompletedColorCodeQuiz(g.Member, g.Brand, _memberQuiz, _quiz);

                    try
                    {
                        //send confirmation email
                        ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendColorCodeQuizConfirmation(g.Member.MemberID, g.Member.EmailAddress, g.Brand.BrandID, _memberQuiz.PrimaryColor.ToString().ToUpper(), String.IsNullOrEmpty(g.Member.GetUserName(_g.Brand)) ? g.Member.MemberID.ToString() : g.Member.GetUserName(_g.Brand));
                    }
                    catch (Exception eEmail)
                    {
                        g.ProcessException(eEmail);
                    }
                    g.Transfer("/Applications/ColorCode/QuizConfirmation.aspx");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
            
        }

        #endregion
    }
}