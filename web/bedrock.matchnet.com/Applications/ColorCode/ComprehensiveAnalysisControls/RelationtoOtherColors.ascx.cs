﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class RelationtoOtherColors : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtContent.Text = g.GetResource("TXT_CONTENT", new string[] { PrimaryColor.ToUpper(), PrimaryColor.ToUpper(), FirstName, PrimaryColor.ToUpper(), PrimaryColorCode.ToString().Substring(0,1) }, true, this);
            txtContent1.Text=g.GetResource("TXT_MOTIVE_" + PrimaryColorCode.ToString().ToUpper(),null,true ,this);
            txtSubTitle.Text=g.GetResource("TXT_SECTION_SUB_TITLE",new string[]{ PrimaryColor.ToUpper()},true,this);
           
        }
    }
}