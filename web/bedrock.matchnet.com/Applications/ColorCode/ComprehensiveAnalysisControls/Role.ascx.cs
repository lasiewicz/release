﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class Role : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtSubTitle.Text = g.GetResource("TXT_SECTION_SUB_TITLE", this, new string[] {Weight.ToUpper()});

            txtContent.Text = g.GetResource("TXT_MOTIVE_" + PrimaryColorCode.ToString().ToUpper() ,this);
            if (SecondaryColorCode != Color.none)
            {
                litBack.Text = "/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=personalsecondarystyle";
                txtBack.Text = g.GetResource("TXT_BACK1", this);
            }
            else
            {


                litBack.Text = "/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=personalstyle";
                txtBack.Text = g.GetResource("TXT_BACK2", this);
            }
          
        }
    }
}