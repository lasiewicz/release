﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class LeveragingSecondary : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            txtContent.Text = g.GetResource("TXT_CONTENT", new string[] { SecondaryColor.ToUpper(), SecondaryColor.ToUpper(), SecondaryColor.ToUpper(), PrimaryColor.ToUpper(), SecondaryColor.ToUpper(), PrimaryColor.ToUpper(), SecondaryColor.ToUpper(), PrimaryColorCode.ToString().Substring(0, 1), SecondaryColor.ToUpper(), SecondaryColor.ToUpper() }, true, this);
            txtContent1.Text = g.GetResource("TXT_CONTENT1", new string[] { FirstName, SecondaryColorCode.ToString().Substring(0, 1), SecondaryColorCode.ToString().Substring(0, 1), SecondaryColor.ToUpper(), SecondaryColor.ToUpper() }, true, this);
            rptPrimaryStrength.DataSource = SecondaryColorStrength;
            rptPrimaryStrength.DataBind();
            txtSubTitle.Text = g.GetResource("TXT_SECTION_SUB_TITLE", this,  new string[]{ SecondaryColor.ToUpper() });

            rptPrimaryLimit.DataSource = SecondaryColorLimitation;
            rptPrimaryLimit.DataBind();

            txtPrimaryColor.Text = SecondaryColor.ToUpper();
            txtPrimaryColor1.Text = SecondaryColor.ToUpper();

            litPrimaryColorClass.Text = GetTraitBackGroundClass(SecondaryColorCode);
            litPrimaryColorClass1.Text = GetTraitBackGroundClass(SecondaryColorCode);

            txtSubTitle.Text = g.GetResource("TXT_SECTION_SUB_TITLE", this, new string[] { SecondaryColor.ToUpper() });

        }
        protected void BindTraits(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                OptionTrait trait = (OptionTrait)e.Item.DataItem;

                Literal litTrait = (Literal)e.Item.FindControl("litTrait");

                litTrait.Text = trait.TraitDisplay;





            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }
    }
}