﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuildingCharacter.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.BuildingCharacter" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<%--	<h2 class="title"><mn:Txt runat="server" ID="txtTitle" ResourceConstant="TXT_SECTION_TITLE" /></h2>--%>
	<div class="<asp:Literal id=litPrimaryColorClass runat=server />">
		<mn:Txt runat="server" ID="txt1" ResourceConstant="TXT_SECTION_COLOR_TITLE" />
	</div>
	<h2 class="title">	<mn:Txt runat="server" ID="txtSubTitle" ResourceConstant="TXT_SECTION_SUB_TITLE" /></h2>
	
	<div class="margin-medium">
			<mn:Txt runat="server" ID="txtContent"/>
		
		
		<ul class="traits clearfix">
			<li>
				<h2 class="cc-white-bg">Character Traits</h2>
				<ul>
			       <asp:Repeater ID="rptCharacterTraits" runat="server" OnItemDataBound="BindTraits">
					    <ItemTemplate>
					    <li><asp:Literal runat="server" ID="litTrait" /></li>
					    </ItemTemplate>
				    </asp:Repeater>
				</ul>
			</li>
			<li>
				<h2 class="cc-white-bg">Healthy Traits</h2>
				<ul>
					 <asp:Repeater ID="rptHealthyTraits" runat="server" OnItemDataBound="BindTraits">
					    <ItemTemplate>
					    <li><asp:Literal runat="server" ID="litTrait" /></li>
					    </ItemTemplate>
				    </asp:Repeater>
				</ul>
			</li>
			<li>
				<h2 class="cc-white-bg">Unhealthy Traits</h2>
				<ul>
					 <asp:Repeater ID="rptUnhealthyTraits" runat="server" OnItemDataBound="BindTraits">
					    <ItemTemplate>
					    <li><asp:Literal runat="server" ID="litTrait" /></li>
					    </ItemTemplate>
				    </asp:Repeater>
				</ul>
			</li>
			<li>
				<h2 class="cc-white-bg">Dysfunctional Traits</h2>
				<ul>
					 <asp:Repeater ID="rptDysfuncTraits" runat="server" OnItemDataBound="BindTraits">
					    <ItemTemplate>
					    <li><asp:Literal runat="server" ID="litTrait" /></li>
					    </ItemTemplate>
				    </asp:Repeater>
				</ul>
			</li>
		</ul>
			<mn:Txt runat="server" ID="txtContent1"/>
	</div><!-- end .margin-medium -->
	<asp:PlaceHolder runat="server" ID="phNavigation" >
	<div class="bottom_nav">
		<h4 class="float-inside"><a href="<asp:Literal id=litBack runat=server/>"><mn:Txt runat="server" ID="txtForward"  /><mn:Txt runat="server" ID="txtBack"  /></a></h4>
		<h4 class="float-outside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=relatingtoothers"><mn:Txt runat="server" ID="txt5" ResourceConstant="TXT_FORWARD_LNK" /></a></h4>
	</div>
	</asp:PlaceHolder>
