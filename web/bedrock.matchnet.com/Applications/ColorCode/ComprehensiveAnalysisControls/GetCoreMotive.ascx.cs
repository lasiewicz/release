﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class GetCoreMotive : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtContent.Text = g.GetResource("TXT_CONTENT", this, new string[] {CoreMotive.ToUpper(), PrimaryColor.ToUpper(),
            PrimaryColor.ToUpper(),
            PrimaryColor.ToUpper(),
            PrimaryColor.ToUpper(),
            PrimaryColor.ToUpper(),
            PrimaryColor.ToUpper(),
            PrimaryColor.ToUpper(),
            PrimaryColor.ToUpper(),
            PrimaryColor.ToUpper()
            });
        }
    }
}