﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class Main : ComprehensiveAnalysisBase
    {
      
        const string img_format = "cc-title-{0}-photo.jpg";
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            litPrimaryColorClass.Text = BackgroundClass;
            txtTagline.Text = TagLine;
            txtTaglineColor.Text=g.GetResource("TXT_TAGLINE_COLOR",this,new string[]{PrimaryColorCode.ToString().ToUpper()});

            MemberQuiz _memberquiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);
            g.AddExpansionToken("MEMBER_COLORCODE", _memberquiz.PrimaryColor.ToString());
            chColorCodeResults.LoadReport(_memberquiz, ReportType.none);
            txtChartTitle.Text = g.GetResource(String.Format("TXT_CHART_TITLE_{0}", PrimaryColorCode.ToString().ToUpper()), this);
            txtCustomAnalysis.Text = g.GetResource("TXT_CUSTOMIZED_ANALYSIS", this, new string[] { FirstLastName });
            string resource="TXT_PROFILE_RESULTS";
            if (SecondaryColorCode == Color.none)
            {
                resource = "TXT_PROFILE_RESULTS_NO_SECONDARY";
                txtProfileResults.Text = g.GetResource(resource, this, new string[] { PrimaryColorCode.ToString().ToUpper(), CoreMotive, FirstName, CoreMotive, PrimaryColorCode.ToString().ToUpper() });
            }
            else
            {

                txtProfileResults.Text = g.GetResource(resource, this, new string[] { PrimaryColorCode.ToString().ToUpper(), CoreMotive, SecondaryColorCode.ToString().ToUpper(), FirstName, CoreMotive, PrimaryColorCode.ToString().ToUpper() });
            }
            imgColorCode.FileName = String.Format(img_format, PrimaryColorCode.ToString().Substring(0, 1).ToLower());
            
        }
    }
}