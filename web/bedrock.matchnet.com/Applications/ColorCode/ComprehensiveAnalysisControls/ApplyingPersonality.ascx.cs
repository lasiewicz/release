﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class ApplyingPersonality : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            litPrimaryColorClass.Text = BackgroundClass;
            txtSectionColorTitle.Text = g.GetResource("TXT_SECTION_COLOR_TITLE_" + PrimaryColorCode.ToString().ToUpper(), this);
            txtContent.Text = g.GetResource("TXT_CONTENT", this, new string[] { PrimaryColor.ToString().ToUpper()});
        }
    }
}