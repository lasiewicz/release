﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Framework;
namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public enum TraitTypeEnum:int
    {
        strength=1,
        limitation=2
    }

    public class Utils
    {

        public static OptionTraitMap PopulateOptionTraitMap(ContextGlobal g, FrameworkControl resourcecontrol)
        {
            OptionTraitMap map = new OptionTraitMap();
            try
            {
                map.Add(1,1,TraitTypeEnum.strength,"proactive","red",g,resourcecontrol);
                map.Add(1, 2, TraitTypeEnum.strength, "nurturing", "blue",g,resourcecontrol);
                map.Add(1, 3, TraitTypeEnum.strength, "objective", "white",g,resourcecontrol);
                map.Add(1, 4, TraitTypeEnum.strength, "insightful", "yellow",g,resourcecontrol);

                map.Add(2,1,TraitTypeEnum.limitation,"indecisive","white",g,resourcecontrol);
				map.Add(2,2,TraitTypeEnum.limitation,"arrogant","red",g,resourcecontrol);
				map.Add(2,3,TraitTypeEnum.limitation,"a perfectionist","blue",g,resourcecontrol);
                map.Add(2,4, TraitTypeEnum.limitation, "poor follow-through", "yellow",g,resourcecontrol);

                map.Add(3,1,TraitTypeEnum.strength,"enthusiastic","yellow",g,resourcecontrol);
				map.Add(3,2,TraitTypeEnum.strength,"kind","white",g,resourcecontrol);
				map.Add(3,3,TraitTypeEnum.strength,"caring","blue",g,resourcecontrol);
                map.Add(3,4, TraitTypeEnum.strength, "productive", "red",g,resourcecontrol);
                
                map.Add(4,1,TraitTypeEnum.limitation,"relentless","red",g,resourcecontrol);
				map.Add(4,2,TraitTypeEnum.limitation,"suspicious","blue",g,resourcecontrol);
				map.Add(4,3,TraitTypeEnum.limitation,"indifferent","white",g,resourcecontrol);
                map.Add(4,4, TraitTypeEnum.limitation, "naïve", "yellow",g,resourcecontrol);

                map.Add(5,1,TraitTypeEnum.strength,"peaceful","white",g,resourcecontrol);
				map.Add(5,2,TraitTypeEnum.strength,"carefree","yellow",g,resourcecontrol);
				map.Add(5,3,TraitTypeEnum.strength,"decisive","red",g,resourcecontrol);
                map.Add(5,4, TraitTypeEnum.strength, "loyal", "blue",g,resourcecontrol);

                map.Add(6,1,TraitTypeEnum.limitation,"silently stubborn","white",g,resourcecontrol);
				map.Add(6,2,TraitTypeEnum.limitation,"worry prone","blue",g,resourcecontrol);
				map.Add(6,3,TraitTypeEnum.limitation,"an interrupter","yellow",g,resourcecontrol);
                map.Add(6,4, TraitTypeEnum.limitation, "obsessive", "red",g,resourcecontrol);

                map.Add(7,1,TraitTypeEnum.strength, "sociable","yellow",g,resourcecontrol);
				map.Add(7,2,TraitTypeEnum.strength,"assertive","red",g,resourcecontrol);
				map.Add(7,3,TraitTypeEnum.strength,"intimate","blue",g,resourcecontrol);
                map.Add(7,4,TraitTypeEnum.strength, "non-discriminate", "white",g,resourcecontrol);

                map.Add(8,1,TraitTypeEnum.limitation,"self-critical","blue",g,resourcecontrol);
			    map.Add(8,2,TraitTypeEnum.limitation,"bossy","red",g,resourcecontrol);
				map.Add(8,3,TraitTypeEnum.limitation,"unfocused","yellow",g,resourcecontrol);
                map.Add(8,4, TraitTypeEnum.limitation, "avoids conflict", "white",g,resourcecontrol);

                map.Add(9,1,TraitTypeEnum.strength, "voice of reason","white",g,resourcecontrol);
				map.Add(9,2,TraitTypeEnum.strength, "flexible","yellow",g,resourcecontrol);
				map.Add(9,3,TraitTypeEnum.strength, "action-oriented","red",g,resourcecontrol);
                map.Add(9,4, TraitTypeEnum.strength, "analytical", "blue",g,resourcecontrol);


                map.Add(10, 1, TraitTypeEnum.limitation, "critical of others", "red",g,resourcecontrol);
                map.Add(10, 2, TraitTypeEnum.limitation, "disinterested", "white",g,resourcecontrol);
                map.Add(10, 3, TraitTypeEnum.limitation, "overly sensitive", "blue",g,resourcecontrol);
                map.Add(10, 4, TraitTypeEnum.limitation, "irresponsible", "yellow",g,resourcecontrol);

                map.Add(11, 1, TraitTypeEnum.strength, "determined","red",g,resourcecontrol);
                map.Add(11, 2, TraitTypeEnum.strength, "thoughtful","blue",g,resourcecontrol);
                map.Add(11, 3, TraitTypeEnum.strength, "a good listener","white",g,resourcecontrol);
                map.Add(11, 4, TraitTypeEnum.strength, "positive","yellow",g,resourcecontrol);

                map.Add(12, 1, TraitTypeEnum.limitation, "unmotivated", "white",g,resourcecontrol);
                map.Add(12, 2, TraitTypeEnum.limitation, "vain", "yellow",g,resourcecontrol);
                map.Add(12, 3, TraitTypeEnum.limitation, "demanding", "red",g,resourcecontrol);
                map.Add(12, 4, TraitTypeEnum.limitation, "unforgiving", "blue",g,resourcecontrol);

                map.Add(13, 1, TraitTypeEnum.strength, "happy","yellow",g,resourcecontrol);
                map.Add(13, 2, TraitTypeEnum.strength, "compassionate","blue",g,resourcecontrol);
                map.Add(13, 3, TraitTypeEnum.strength, "inventive","white",g,resourcecontrol);
                map.Add(13, 4, TraitTypeEnum.strength, "responsible", "red",g,resourcecontrol);

                map.Add(14, 1, TraitTypeEnum.limitation, "impulsive", "yellow",g,resourcecontrol);
                map.Add(14, 2, TraitTypeEnum.limitation, "impatient", "red",g,resourcecontrol);
                map.Add(14, 3, TraitTypeEnum.limitation, "moody", "blue",g,resourcecontrol);
                map.Add(14, 4, TraitTypeEnum.limitation, "indirect communicator", "white",g,resourcecontrol);

                map.Add(15, 1, TraitTypeEnum.strength, "a leader","red",g,resourcecontrol);
                map.Add(15, 2, TraitTypeEnum.strength, "patient","white",g,resourcecontrol);
                map.Add(15, 3, TraitTypeEnum.strength, "fun-loving","yellow",g,resourcecontrol);
                map.Add(15, 4, TraitTypeEnum.strength, "respectful", "blue",g,resourcecontrol);

                map.Add(16, 1, TraitTypeEnum.limitation, "jealous","blue",g,resourcecontrol);
                map.Add(16, 2, TraitTypeEnum.limitation, "reluctant","white",g,resourcecontrol);
                map.Add(16, 3, TraitTypeEnum.limitation, "argumentative","red",g,resourcecontrol);
                map.Add(16, 4, TraitTypeEnum.limitation, "obnoxious", "yellow",g,resourcecontrol);

                map.Add(17, 1, TraitTypeEnum.strength, "even-tempered","white",g,resourcecontrol);
                map.Add(17, 2, TraitTypeEnum.strength, "inclusive","yellow",g,resourcecontrol);
                map.Add(17, 3, TraitTypeEnum.strength, "dependable","blue",g,resourcecontrol);
                map.Add(17, 4, TraitTypeEnum.strength, "focused", "red",g,resourcecontrol);

                map.Add(18, 1, TraitTypeEnum.limitation, "overly aggressive", "red",g,resourcecontrol);
                map.Add(18, 2, TraitTypeEnum.limitation, "low self-esteem","blue",g,resourcecontrol);
                map.Add(18, 3, TraitTypeEnum.limitation, "ambivalent","white",g,resourcecontrol);
                map.Add(18, 4, TraitTypeEnum.limitation, "inconsistent","yellow",g,resourcecontrol);

                map.Add(19, 1, TraitTypeEnum.strength, "deliberate","blue",g,resourcecontrol);
                map.Add(19, 2, TraitTypeEnum.strength, "powerful","red",g,resourcecontrol);
                map.Add(19, 3, TraitTypeEnum.strength, "balanced","white",g,resourcecontrol);
                map.Add(19, 4, TraitTypeEnum.strength, "persuasive", "yellow",g,resourcecontrol);

                map.Add(20, 1, TraitTypeEnum.limitation, "undisciplined","yellow",g,resourcecontrol);
                map.Add(20, 2, TraitTypeEnum.limitation, "timid","white",g,resourcecontrol);
                map.Add(20, 3, TraitTypeEnum.limitation, "insensitive","red",g,resourcecontrol);
                map.Add(20, 4, TraitTypeEnum.limitation, "judgmental", "blue",g,resourcecontrol);

                map.Add(21, 1, TraitTypeEnum.strength, "creative thinker","yellow",g,resourcecontrol);
                map.Add(21, 2, TraitTypeEnum.strength, "clear perspective","white",g,resourcecontrol);
                map.Add(21, 3, TraitTypeEnum.strength, "detail conscious","blue",g,resourcecontrol);
                map.Add(21, 4, TraitTypeEnum.strength, "visionary", "red",g,resourcecontrol);

                map.Add(22, 1, TraitTypeEnum.limitation, "always right","red",g,resourcecontrol);
                map.Add(22, 2, TraitTypeEnum.limitation, "uncommitted","yellow",g,resourcecontrol);
                map.Add(22, 3, TraitTypeEnum.limitation, "uninvolved","white",g,resourcecontrol);
                map.Add(22, 4, TraitTypeEnum.limitation, "guilt prone", "blue",g,resourcecontrol);

                map.Add(23, 1, TraitTypeEnum.strength, "accepting","white",g,resourcecontrol);
                map.Add(23, 2, TraitTypeEnum.strength, "spontaneous","yellow",g,resourcecontrol);
                map.Add(23, 3, TraitTypeEnum.strength, "pragmatic","red",g,resourcecontrol);
                map.Add(23, 4, TraitTypeEnum.strength, "well-mannered", "blue",g,resourcecontrol);

                map.Add(24, 1, TraitTypeEnum.limitation, "disorganized","yellow",g,resourcecontrol);
                map.Add(24, 2, TraitTypeEnum.limitation, "selfish","red",g,resourcecontrol);
                map.Add(24, 3, TraitTypeEnum.limitation, "emotionally intense","blue",g,resourcecontrol);
                map.Add(24, 4, TraitTypeEnum.limitation, "detached", "white",g,resourcecontrol);

                map.Add(25, 1, TraitTypeEnum.strength, "motivated","red",g,resourcecontrol);
                map.Add(25, 2, TraitTypeEnum.strength, "sincere","blue",g,resourcecontrol);
                map.Add(25, 3, TraitTypeEnum.strength, "diplomatic","white",g,resourcecontrol);
                map.Add(25, 4, TraitTypeEnum.strength, "engaging of others", "yellow",g,resourcecontrol);

                map.Add(26, 1, TraitTypeEnum.limitation, "forgetful","yellow",g,resourcecontrol);
                map.Add(26, 2, TraitTypeEnum.limitation, "boring","white",g,resourcecontrol);
                map.Add(26, 3, TraitTypeEnum.limitation, "hard to please","blue",g,resourcecontrol);
                map.Add(26, 4, TraitTypeEnum.limitation, "tactless", "red",g,resourcecontrol);

                map.Add(27, 1, TraitTypeEnum.strength, "articulate","red",g,resourcecontrol);
                map.Add(27, 2, TraitTypeEnum.strength, "quality-oriented","blue",g,resourcecontrol);
                map.Add(27, 3, TraitTypeEnum.strength, "centered","white",g,resourcecontrol);
                map.Add(27, 4, TraitTypeEnum.strength, "forgiving", "yellow",g,resourcecontrol);

                map.Add(28, 1, TraitTypeEnum.limitation, "self-centered","yellow",g,resourcecontrol);
                map.Add(28, 2, TraitTypeEnum.limitation, "calculating","red",g,resourcecontrol);
                map.Add(28, 3, TraitTypeEnum.limitation, "self-righteous","blue",g,resourcecontrol);
                map.Add(28, 4, TraitTypeEnum.limitation, "unexpressive", "white",g,resourcecontrol);

                map.Add(29, 1, TraitTypeEnum.strength, "self-regulated","white",g,resourcecontrol);
                map.Add(29, 2, TraitTypeEnum.strength, "charismatic","yellow",g,resourcecontrol);
                map.Add(29, 3, TraitTypeEnum.strength, "confident","red",g,resourcecontrol);
                map.Add(29, 4, TraitTypeEnum.strength, "intuitive", "blue",g,resourcecontrol);

                map.Add(30, 1, TraitTypeEnum.limitation, "unrealistic expectations","blue",g,resourcecontrol);
                map.Add(30, 2, TraitTypeEnum.limitation, "unproductive","white",g,resourcecontrol);
                map.Add(30, 3, TraitTypeEnum.limitation, "afraid to face facts","yellow",g,resourcecontrol);
                map.Add(30, 4, TraitTypeEnum.limitation, "intimidating", "red",g,resourcecontrol);

                return map;

            }
            catch (Exception ex)
            { return null; }
        }

        public static OptionTraitMap PopulateMemberOptionTraitMap(MemberQuiz quiz, OptionTraitMap map)
        {
            OptionTraitMap memberMap = new OptionTraitMap();
            try
            {
                for (int i = 0; i < 30; i++)
                {
                    int option = quiz.GetSelectedChoiceID(i + 1);
                    OptionTrait trait = map.GetTrait(i + 1, option);
                    memberMap.OptionTraitList.Add(trait);

                }

                return memberMap;
            }
            catch (Exception ex)
            { return null; }



        }


    }
    public class OptionTrait
    {
        public int QuestionID { get; set; }
        public int OptionID { get; set; }
        public string Trait { get; set; }
        public string Color { get; set; }
        public string TraitDisplay { get; set; }
        public TraitTypeEnum TraitType {get;set;}

        public string TraitResource
        {
            get
            {
                string format = "TRAIT_Q_{0}_O_{1}";

                return String.Format(format, QuestionID, OptionID);
            }
        }
        public OptionTrait(int qid,int oid, string trait, string color, TraitTypeEnum traittype)
        {
            QuestionID = qid;
            OptionID = oid;
            Trait = trait;
            Color = color;
            TraitType = traittype;

        }

    }

    public class OptionTraitMap
    {
        public List<OptionTrait> OptionTraitList;
        
        public OptionTraitMap()
        {
            OptionTraitList = new List<OptionTrait>();
        }
        public void Add(int qid,int oid,TraitTypeEnum traittype,string traitstr, string color, ContextGlobal g, FrameworkControl control)
        {
            if (OptionTraitList == null)
                OptionTraitList = new List<OptionTrait>();

            OptionTrait trait = new OptionTrait(qid, oid, traitstr, color, traittype);
            trait.TraitDisplay = g.GetResource(trait.TraitResource, control);
            OptionTraitList.Add(trait);

        }

        public OptionTrait GetTrait(int qid, int oid)
        {
            OptionTrait trait = null;
            try
            {
                trait = (from o in OptionTraitList
                         where o.QuestionID == qid && o.OptionID == oid
                         select o).ToList<OptionTrait>()[0];
                return trait;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<OptionTrait> GetTraitList(string color, TraitTypeEnum traittype)
        {
            List<OptionTrait> traitlist = null;
            try
            {
                traitlist = (from o in OptionTraitList
                         where o.Color.ToLower() == color.ToLower() && o.TraitType == traittype
                         select o).ToList<OptionTrait>();
                return traitlist;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
