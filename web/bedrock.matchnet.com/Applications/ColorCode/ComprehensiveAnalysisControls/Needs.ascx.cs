﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class Needs : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtSubTitle.Text = g.GetResource("TXT_SECTION_SUB_TITLE", this, new string[] { PrimaryColor.ToUpper(), SecondaryColor.ToUpper() });

            string resx = "TXT_CONTENT";
            if (SecondaryColorCode == Color.none)
            {
                resx = "TXT_CONTENT_NO_CONTENT";
                txtContent.Text = g.GetResource(resx, this, new string[] { FirstName, PrimaryColor.ToUpper(), CoreMotive.ToUpper(), SecondaryColor.ToUpper(), PrimaryColor.ToUpper(), SecondaryColor.ToUpper(), SecondaryColor.ToUpper(), PrimaryColor.ToUpper(), PrimaryColor.ToUpper(), SecondaryColor.ToUpper(), PrimaryColor.ToUpper(), SecondaryColor.ToUpper(), PrimaryColor.ToUpper(), SecondaryColor.ToUpper() });
            }
            else
            {
                txtContent.Text = g.GetResource(resx, this, new string[] { FirstName, PrimaryColor.ToUpper(), CoreMotive.ToUpper(), SecondaryColor.ToUpper(), FirstName, SecondaryColor.ToUpper(), SecondaryColor.ToUpper(), PrimaryColor.ToUpper(), PrimaryColor.ToUpper(), SecondaryColor.ToUpper(), PrimaryColor.ToUpper(), SecondaryColor.ToUpper(), PrimaryColor.ToUpper(), SecondaryColor.ToUpper() });
            }
            txtContent1.Text = g.GetResource("TXT_CONTENT1", this, new string[] { PrimaryColor.ToUpper() });
            txtNeeds1.Text = g.GetResource("TXT_NEEDS_" + PrimaryColor.ToUpper(), this);
            txtNeeds2.Text = g.GetResource("TXT_NEEDS_" + SecondaryColor.ToUpper(), this);

            txtWants1.Text = g.GetResource("TXT_WANTS_" + PrimaryColor.ToUpper(), this);
            txtWants2.Text = g.GetResource("TXT_WANTS_" + SecondaryColor.ToUpper(), this);


            txtForward.Text = g.GetResource("TXT_FORWARD_LNK", this, new string[] { Weight });
            litCssClassNeeds1.Text = GetTraitBackGroundClass(PrimaryColorCode);
            litCssClassNeeds2.Text = GetTraitBackGroundClass(SecondaryColorCode);

            litCssClassWants1.Text = GetTraitBackGroundClass(PrimaryColorCode);
            litCssClassWants2.Text = GetTraitBackGroundClass(SecondaryColorCode);
          
            imgAppAct.FileName = String.Format("ccc-app-activity-{0}.gif", PrimaryColorCode.ToString().ToLower().Substring(0, 1));
            if (SecondaryColorCode == Color.none)
                phSecondaryNeeds.Visible = false;
            txtBack.Text = g.GetResource("TXT_BACK_LNK", this, new string[] { Weight.ToString() });
        }
    }
}