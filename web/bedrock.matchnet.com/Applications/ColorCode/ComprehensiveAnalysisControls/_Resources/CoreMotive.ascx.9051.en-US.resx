<?xml version="1.0" encoding="utf-8" ?>
<root>
	<!-- 
    Microsoft ResX Schema 
    
    Version 1.3
    
    The primary goals of this format is to allow a simple XML format 
    that is mostly human readable. The generation and parsing of the 
    various data types are done through the TypeConverter classes 
    associated with the data types.
    
    Example:
    
    ... ado.net/XML headers & schema ...
    <resheader name="resmimetype">text/microsoft-resx</resheader>
    <resheader name="version">1.3</resheader>
    <resheader name="reader">System.Resources.ResXResourceReader, System.Windows.Forms, ...</resheader>
    <resheader name="writer">System.Resources.ResXResourceWriter, System.Windows.Forms, ...</resheader>
    <data name="Name1">this is my long string</data>
    <data name="Color1" type="System.Drawing.Color, System.Drawing">Blue</data>
    <data name="Bitmap1" mimetype="application/x-microsoft.net.object.binary.base64">
        [base64 mime encoded serialized .NET Framework object]
    </data>
    <data name="Icon1" type="System.Drawing.Icon, System.Drawing" mimetype="application/x-microsoft.net.object.bytearray.base64">
        [base64 mime encoded string representing a byte array form of the .NET Framework object]
    </data>
                
    There are any number of "resheader" rows that contain simple 
    name/value pairs.
    
    Each data row contains a name, and value. The row also contains a 
    type or mimetype. Type corresponds to a .NET class that support 
    text/value conversion through the TypeConverter architecture. 
    Classes that don't support this are serialized and stored with the 
    mimetype set.
    
    The mimetype is used forserialized objects, and tells the 
    ResXResourceReader how to depersist the object. This is currently not 
    extensible. For a given mimetype the value must be set accordingly:
    
    Note - application/x-microsoft.net.object.binary.base64 is the format 
    that the ResXResourceWriter will generate, however the reader can 
    read any of the formats listed below.
    
    mimetype: application/x-microsoft.net.object.binary.base64
    value   : The object must be serialized with 
            : System.Serialization.Formatters.Binary.BinaryFormatter
            : and then encoded with base64 encoding.
    
    mimetype: application/x-microsoft.net.object.soap.base64
    value   : The object must be serialized with 
            : System.Runtime.Serialization.Formatters.Soap.SoapFormatter
            : and then encoded with base64 encoding.

    mimetype: application/x-microsoft.net.object.bytearray.base64
    value   : The object must be serialized into a byte array 
            : using a System.ComponentModel.TypeConverter
            : and then encoded with base64 encoding.
    -->
	<xsd:schema id="root" xmlns="" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
		<xsd:element name="root" msdata:IsDataSet="true">
			<xsd:complexType>
				<xsd:choice maxOccurs="unbounded">
					<xsd:element name="data">
						<xsd:complexType>
							<xsd:sequence>
								<xsd:element name="value" type="xsd:string" minOccurs="0" msdata:Ordinal="1" />
								<xsd:element name="comment" type="xsd:string" minOccurs="0" msdata:Ordinal="2" />
							</xsd:sequence>
							<xsd:attribute name="name" type="xsd:string" msdata:Ordinal="1" />
							<xsd:attribute name="type" type="xsd:string" msdata:Ordinal="3" />
							<xsd:attribute name="mimetype" type="xsd:string" msdata:Ordinal="4" />
						</xsd:complexType>
					</xsd:element>
					<xsd:element name="resheader">
						<xsd:complexType>
							<xsd:sequence>
								<xsd:element name="value" type="xsd:string" minOccurs="0" msdata:Ordinal="1" />
							</xsd:sequence>
							<xsd:attribute name="name" type="xsd:string" use="required" />
						</xsd:complexType>
					</xsd:element>
				</xsd:choice>
			</xsd:complexType>
		</xsd:element>
	</xsd:schema>
	<resheader name="resmimetype">
		<value>text/microsoft-resx</value>
	</resheader>
	<resheader name="version">
		<value>1.3</value>
	</resheader>
	<resheader name="reader">
		<value>System.Resources.ResXResourceReader, System.Windows.Forms, Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>
	</resheader>
	<resheader name="writer">
		<value>System.Resources.ResXResourceWriter, System.Windows.Forms, Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>
	</resheader>
  <data name="TXT_SECTION_SUB_TITLE" xml:space="preserve">
    <value>› Explanation of Your Driving Core Motive - %s</value>
  </data>
  <data name="TXT_CONTENT" xml:space="preserve">
    <value>
&lt;p&gt;There are three important distinctions that relate to the idea of &lt;i&gt;Core Motive&lt;/i&gt;:&lt;/p&gt;
&lt;ol&gt; 
	&lt;li&gt;Your personality is innate; it came with you at birth, and your &lt;i&gt;Core Motive&lt;/i&gt; of %s drives your behavior and acts as the lens through which you see the world.&lt;/li&gt; 
	&lt;li&gt;&lt;i&gt;Core Motive&lt;/i&gt; is not linked to heredity or environment.&lt;/li&gt; 
	&lt;li&gt;While you may have different strengths and limitations from more than one color, you have only one core personality, driven by one singular &lt;i&gt;Core Motive&lt;/i&gt; - %s.&lt;/li&gt; 
&lt;/ol&gt; 
&lt;p class=\"bold\"&gt;</value>
  </data>
  <data name="TXT_MOTIVE_BLUE" xml:space="preserve">
    <value>According to your profile results %s, as a BLUE, you are motivated by INTIMACY. It is important to note that this does not mean sexual intimacy. BLUES need connection – the sharing of rich, deep emotions that bind people together. The BLUES will often sacrifice a great deal (time, effort, personal convenience) to develop and maintain meaningful relationships throughout their lives.</value>
  </data>
  <data name="TXT_MOTIVE_YELLOW" xml:space="preserve">
    <value>As a YELLOW, you are motivated by FUN. This does not mean that you are constantly looking for a party (although you usually do know where to find one). You are instinctively happy and gravitate to people and situations that provide carefree adventure and playful interactions. Even under the most serious of circumstances or daunting tasks, you and your YELLOW friends will seek an element of personal fulfillment and "spontaneous enjoyment" in the experience. Spontaneous play and genuine "in the moment" fun are not merely important to you—they are as essential to your very being as eating.</value>
  </data>
  <data name="TXT_MOTIVE_RED" xml:space="preserve">
    <value>According to your profile results, as a RED, you are motivated by POWER. POWER means the ability to get things done, to move from A to B as quickly and directly as possible. In Spanish, the noun "power" comes from the Spanish verb "to be able to do." Often what is perceived by the other colors as insensitivity is merely your pragmatic sense of urgency to accomplish a given task.</value>
  </data>
  <data name="TXT_MOTIVE_WHITE" xml:space="preserve">
    <value>As a WHITE, you are motivated by PEACE. This is not referring to a political agenda or the absence of war. It is, however, an absence of inner conflict, much closer to the idea of serenity, and an acceptance of oneself and others. There is a strong and compelling need to keep things in balance in your life so as to maintain an internal feeling of tranquility and comfort.</value>
  </data>
  <data name="TXT_FORWARD_LNK" xml:space="preserve">
    <value>Personal Style »</value>
  </data>
  <data name="TXT_BACK_LNK" xml:space="preserve">
    <value>« Your Traits</value>
  </data>
  <data name="TXT_CONTENT2" xml:space="preserve">
    <value>&lt;/p&gt;
&lt;p&gt;
	Only people with the same innate personality fully understand how central the driving &lt;i&gt;Core Motive&lt;/i&gt; is to their very being.
	It is like breathing.
	It is innate and natural.
	No &lt;i&gt;Core Motive&lt;/i&gt; is positive or negative in and of itself.
	&lt;i&gt;Core Motive&lt;/i&gt; simply reflects what is most crucial to every individual who shares the same color personality.
&lt;/p&gt;</value>
  </data>

</root>
