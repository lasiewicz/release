﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Framework;
namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public class ComprehensiveAnalysisBase:FrameworkControl
    {
        public const string css_class = "cc-{0}-bg clearfix ccc-eye-catcher";
        public const string css_class_trait = "cc-{0}-bg";
        public Color PrimaryColorCode { get; set; }
        public Color SecondaryColorCode { get; set; }
        public Color ThirdColorCode { get; set; }
        public Color FourthColorCode { get; set; }
        public string PrimaryColor { get; set; }
        public string SecondaryColor { get; set; }
        public string ThirdColor { get; set; }
        public string FourthColor { get; set; }
        public string CoreMotive { get; set; }
        public string EQ { get; set; }
        public string Motivated { get; set; }
        public string TagLine { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FirstLastName { get { return String.Format("{0} {1}", FirstName, LastName); } }
        public string BackgroundClass { get { return String.Format(css_class, PrimaryColorCode.ToString()); } }
        public string Weight { get; set; }
        public string GetBackGroundClass(Color color)
        {
            return String.Format(css_class, color.ToString());
        }
        public string GetTraitBackGroundClass(Color color)
        {
            return String.Format(css_class_trait, color.ToString());
        }
        public List<OptionTrait> PrimaryColorStrength { get; set; }
        public List<OptionTrait> SecondaryColorStrength { get; set; }
        public List<OptionTrait> PrimaryColorLimitation { get; set; }
        public List<OptionTrait> SecondaryColorLimitation { get; set; }
        public List<OptionTrait> ThirdColorStrength { get; set; }
        public List<OptionTrait> ThirdColorLimitation { get; set; }
        public List<OptionTrait> FourthColorStrength { get; set; }
        public List<OptionTrait> FourthColorLimitation { get; set; }
    }
}
