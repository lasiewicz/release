﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class RelationshipTips : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SecondaryColorCode == Color.none)
            {
                MemberQuiz _memberquiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);




                IOrderedEnumerable<Color> scores = (from s in _memberquiz.Scores.Keys
                                                    orderby _memberquiz.Scores[s] descending
                                                    select s);


                SecondaryColorCode = scores.ElementAt<Color>(1);
                ThirdColorCode = scores.ElementAt<Color>(2);
                FourthColorCode = scores.ElementAt<Color>(3);

                SecondaryColor = scores.ElementAt<Color>(1).ToString().ToUpper();
                ThirdColor = scores.ElementAt<Color>(2).ToString().ToUpper();
                FourthColor = scores.ElementAt<Color>(3).ToString().ToUpper();


            }
            txtColor4.Text = g.GetResource("TXT_MOTIVE_" + FourthColorCode.ToString().ToUpper(), this);
            txtColor3.Text = g.GetResource("TXT_MOTIVE_" + ThirdColorCode.ToString().ToUpper(), this);
            txtColor2.Text = g.GetResource("TXT_MOTIVE_" + SecondaryColorCode.ToString().ToUpper(), this);
            txtColor1.Text = g.GetResource("TXT_MOTIVE_" + PrimaryColorCode.ToString().ToUpper(), this);
        }
    }
}