﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Role.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.Role" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>	

<%--	<h2 class="title"><mn:Txt runat="server" ID="txtTitle" ResourceConstant="TXT_SECTION_TITLE" /></h2>
	<div class="<asp:Literal id=litPrimaryColorClass runat=server />">
		<mn:Txt runat="server" ID="txt1" ResourceConstant="TXT_SECTION_COLOR_TITLE" />
	</div>--%>
	<h2 class="title"><mn:Txt runat="server" ID="txtSubTitle"  /></h2>
	
	<div class="margin-medium">
			<mn:Txt runat="server" ID="txtContent" />
				<mn:Txt runat="server" ID="txtContent1" ResourceConstant="TXT_CONTENT" />
	
	</div><!-- end .margin-medium -->
	
	<div class="bottom_nav">
		<h4 class="float-inside"><a href="<asp:Literal id=litBack runat=server/>"><mn:Txt runat="server" ID="txtBack"  /></a></h4>
		<h4 class="float-outside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=needs"><mn:Txt runat="server" ID="txtForward"  ResourceConstant="TXT_FORWARD_LNK" /></a></h4>
	</div>