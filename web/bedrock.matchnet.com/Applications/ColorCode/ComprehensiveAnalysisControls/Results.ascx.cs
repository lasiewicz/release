﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class Results : ComprehensiveAnalysisBase
    {
        string content_2_format = "TXT_CONTENT_{0}_{1}";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            litPrimaryColorClass.Text = BackgroundClass;
            txtSectionColorTitle.Text = g.GetResource("TXT_SECTION_COLOR_TITLE_" + PrimaryColorCode.ToString().ToUpper(), this);
            MemberQuiz _memberquiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);
            g.AddExpansionToken("MEMBER_COLORCODE", _memberquiz.PrimaryColor.ToString());
            chColorCodeResults.LoadReport(_memberquiz, ReportType.none);

            txtSectionSubTitle.Text = g.GetResource("TXT_SECTION_SUB_TITLE", this, new string[] { FirstName });
            string content = "TXT_CONTENT";
            if (SecondaryColorCode == Color.none)
            {
                content = "TXT_CONTENT_NO_SECONDARY";
                txtContent1.Text = g.GetResource(content, this, new string[]{PrimaryColorCode.ToString().ToUpper(),
                                                                             CoreMotive.ToUpper(),
                                                                            PrimaryColorCode.ToString().ToUpper(),
                                                                             CoreMotive.ToUpper(),
                                                                             SecondaryColorCode.ToString().ToUpper()});
            }
            else
            {
                txtContent1.Text = g.GetResource(content, this, new string[]{PrimaryColorCode.ToString().ToUpper(),
                                                                             CoreMotive.ToUpper(),
                                                                             SecondaryColorCode.ToString().ToUpper(),
                                                                            PrimaryColorCode.ToString().ToUpper(),
                                                                             CoreMotive.ToUpper(),
                                                                             SecondaryColorCode.ToString().ToUpper()});
            }
            string contentresx = String.Format(content_2_format, PrimaryColorCode.ToString().ToUpper(), SecondaryColorCode.ToString().ToUpper());
            txtContent2.Text = g.GetResource(contentresx, this);
         
        }
    }
}