﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RelationshipTips.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.RelationshipTips" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
	
	<h2 class="title">	<mn:Txt runat="server" ID="txtSubTitle" ResourceConstant="TXT_SECTION_SUB_TITLE"/></h2>
	
	<div class="margin-medium">
			<mn:Txt runat="server" ID="txtContent"  ExpandImageTokens="true" ResourceConstant="TXT_CONTENT" />
			<ul class="traits clearfix">
			<mn:Txt ID="txtColor4" runat="server" />
			<mn:Txt ID="txtColor3" runat="server" />
			<mn:Txt ID="txtColor2" runat="server" />
			<mn:Txt ID="txtColor1" runat="server" />
			
			</ul>
	</div><!-- end .margin-medium -->
	
	<div class="bottom_nav">
		<h4 class="float-inside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=successfulrelationship"><mn:Txt runat="server" ID="txt4" ResourceConstant="TXT_BACK_LNK" /></a></h4>
		<h4 class="float-outside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=evaluatingrelationship"><mn:Txt runat="server" ID="txt5" ResourceConstant="TXT_FORWARD_LNK" /></a></h4>
	</div>