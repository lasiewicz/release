﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Needs.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.Needs" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>	

<%--	<h2 class="title"><mn:Txt runat="server" ID="txtTitle" ResourceConstant="TXT_SECTION_TITLE" /></h2>
	<div class="<asp:Literal id=litPrimaryColorClass runat=server />">
		<mn:Txt runat="server" ID="txt1" ResourceConstant="TXT_SECTION_COLOR_TITLE" />
	</div>--%>
	<h2 class="title"><mn:Txt runat="server" ID="txtSubTitle"  /></h2>
	
	<div class="margin-medium">
			<mn:Txt runat="server" ID="txtContent" />
			<h3><mn:Txt runat="server" id="txtNeedsWants1" /></h3>
		   <ul class="traits clearfix">
			<li>
			    <h2 class="<asp:Literal runat=server ID=litCssClassNeeds1 />"><mn:Txt ID="txtNeedsTitle1" runat="server" ResourceConstant="TXT_NEEDS" /></h2>
		        <mn:Txt runat="server" ID="txtNeeds1" />
	        </li>
	        <li>
	            <h2 class="<asp:Literal runat=server ID=litCssClassWants1/>"><mn:Txt ID="txtWantsTitle1" runat="server" ResourceConstant="TXT_WANTS" /></h2>
	            <mn:Txt runat="server" ID="txtWants1" />
	        </li>
	        </ul>
	        <asp:PlaceHolder ID="phSecondaryNeeds" runat="server">
	        <h3><mn:Txt runat="server" id="txtNeedsWants2" /></h3>
		   <ul class="traits clearfix">
			<li>
			    <h2 class="<asp:Literal runat=server ID=litCssClassNeeds2 />"><mn:Txt ID="txtNeedsTitle2" runat="server" ResourceConstant="TXT_NEEDS" /></h2>
		        <mn:Txt runat="server" ID="txtNeeds2" />
	        </li>
	        <li>
	            <h2 class="<asp:Literal runat=server ID=litCssClassWants2 />"><mn:Txt ID="txtWantsTitle2" runat="server" ResourceConstant="TXT_WANTS" /></h2>
	            <mn:Txt runat="server" ID="txtWants2" />
	        </li>
	       </ul>
	        </asp:PlaceHolder>
	        
	        <mn:Image   runat="server" ID="imgAppAct"  height="23" width="157"  /><br />
	        <mn:Txt runat="server" ID="txtContent1" />
	</div><!-- end .margin-medium -->
	
	<div class="bottom_nav">
		<h4 class="float-inside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=role"><mn:Txt runat="server" ID="txtBack"   /></a></h4>
		<h4 class="float-outside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=naturaleqcompetency"><mn:Txt runat="server" ID="txtForward"  ResourceConstant="TXT_FORWARD_LNK" /></a></h4>
	</div>
