﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Results.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.Results" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="PieChart" Src="~/Applications/ColorCode/Controls/Analysis/PieChart.ascx" %>

<h2 class="title"><mn:Txt runat="server" ID="txtTitle" ResourceConstant="TXT_SECTION_TITLE" /></h2>
<div class="<asp:Literal id=litPrimaryColorClass runat=server />">
	<mn:Txt runat="server" ID="txtSectionColorTitle" ResourceConstant="TXT_SECTION_COLOR_TITLE" />
</div>
<h2 class="title">	<mn:Txt runat="server" ID="txtSectionSubTitle"  /></h2>

<div class="margin-medium clearfix">

	<div class="cc-content-inside-wrapper">
		<div class="cc-content-inside">
			<mn:Txt ID="txtContent1" runat="server"  />
			<p><mn:Txt ID="txtContent2" runat="server" /></p>
		</div>
	</div>

	
    <div class="float-outside" id="cc-inline-container">
        <div id="cc-inline-pie" class="ccc-inline-pie">
            <script type="text/javascript">
                $j('<div id="cc-pie-chart-loading" class="ccc-pie-chart-loading"><mn:image id="mnimage6491" runat="server" filename="ajax-loader.gif" alt="" CssClass="centered" /></div>').appendTo('.ccc-inline-pie');
            </script>
            <uc2:PieChart runat="server" ID="chColorCodeResults" />
        </div><%-- end #cc-inline-container --%>

    </div><%-- end #cc-inline-container --%>

	
</div>

<div class="bottom_nav">
	<h4 class="float-inside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=demographics"><mn:Txt runat="server" ID="txt4" ResourceConstant="TXT_BACK_LNK" /></a></h4>
	<h4 class="float-outside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=traits"><mn:Txt runat="server" ID="txt5" ResourceConstant="TXT_FORWARD_LNK" /></a></h4>
</div>
