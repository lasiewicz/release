﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.Main" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="PieChart" Src="~/Applications/ColorCode/Controls/Analysis/PieChart.ascx" %>

<div class="ccc-main">
	<div class="<asp:Literal id=litPrimaryColorClass runat=server />" >
		<mn:Image runat="server"  ID="imgColorCode" height="184" width="182" ResourceConstant="ALT_TITLE_IMG" class="float-outside"/>
		<p class="float-outside margin-medium">
		<mn:Txt ID="txtTagline" runat="server" />
			<big><mn:Txt ID="txtTaglineColor" runat="server" /></big>
		</p>
	</div>

	<mn:Txt ID="txtCustomAnalysis" runat="server" />
	<h2 class="title"><mn:Txt ID="txtTitleSum"  runat="server" ResourceConstant="TXT_TITLE_SUMMARY" /></h2>

	<div id="cc-inline-container">
        <div id="cc-inline-pie">
		    <h2><mn:Txt ID="txtChartTitle" runat ="server" /></h2>
		    
		    <script type="text/javascript">
                $j('<div id="cc-pie-chart-loading"><mn:image id="mnimage6491" runat="server" filename="ajax-loader.gif" alt="" CssClass="centered" /></div>').appendTo('#cc-inline-pie');
            </script>
		    <uc2:PieChart runat="server" ID="chColorCodeResults" />
		    
        </div><%-- end #cc-inline-pie --%>
	</div><%-- end #cc-inline-container --%>

	<div class="margin-medium">
		<mn:Txt ID="txtProfileResults" runat="server" />
	</div><%-- end .margin-medium --%>
	
	<div class="bottom_nav">
		<h4 class="float-outside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=introduction"><mn:Txt ID="txt1" runat="server" ResourceConstant="TXT_INTRODUCTION_LNK" /></a></h4>
	</div>
</div>