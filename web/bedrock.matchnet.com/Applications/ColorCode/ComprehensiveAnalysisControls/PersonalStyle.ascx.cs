﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class PersonalStyle : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtSubTitle.Text = g.GetResource("TXT_SECTION_SUB_TITLE", this, new string[] { PrimaryColor.ToUpper() });
            
            txtContent.Text = g.GetResource("TXT_MOTIVE_" + PrimaryColorCode.ToString().ToUpper(), this, new string[] { FirstName });
            
            if(SecondaryColorCode != Color.none)
            {
                litForward.Text = "/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=personalsecondarystyle";
                txtForward.Text = g.GetResource("TXT_FORWARD1", this);
            }
            else{

              
                litForward.Text = "/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=role";
                txtForward.Text = g.GetResource("TXT_FORWARD2", this,new string[]{ Weight});
            }
        }
    }
}