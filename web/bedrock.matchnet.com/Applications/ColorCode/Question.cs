﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.ColorCode
{
    [Serializable]
    public class Question
    {
        int _ID = Constants.NULL_INT;
        string _Text = "";
        List<Choice> _Choices = new List<Choice>();
        QuestionType _QuestionType = QuestionType.none;

        #region Properties
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        public List<Choice> Choices
        {
            get { return _Choices; }
            set { _Choices = value; }
        }

        public QuestionType QuestionType
        {
            get { return _QuestionType; }
            set { _QuestionType = value; }
        }

        #endregion

        #region Public Methods
        public Choice GetChoice(int choiceID)
        {
            Choice choice = null;
            if (choiceID > 0)
            {
                foreach (Choice c in Choices)
                {
                    if (c.ID == choiceID)
                    {
                        choice = c;
                        break;
                    }
                }
            }

            return choice;
        }
        #endregion
    }
}
