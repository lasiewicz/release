﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Want.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Controls.Analysis.Want" %>
<%@ Register src="AnalysisNav.ascx" tagname="AnalysisNav" tagprefix="uc1" %>
<%@ Register src="AnalysisSideBar.ascx" tagname="AnalysisSideBar" tagprefix="uc1" %>

    <div class="wrapper">
        <div id="cc-content">

            <div class="editorial">

                <asp:PlaceHolder ID="phBlue" runat="server" Visible="false">
                    <div class="cc-color-banner cc-blue-bg">Your Color Code Personality: <span class="cc-blue">BLUE</span></div>

                    <h1>More About You &ndash; Your Wants</h1>

                    <h2>You Want Security</h2>
                    <p>You like stability and security in your relationships and in life in general. You want others who communicate in word and deed so that you always feel on stable ground in those relationships. You also want someone who will establish a solid (and safe) relationship with you and not force you to take high stakes risks, although, I would recommend that you be open-minded in this area, because some risks will really do wonders to enhance the quality of your life. </p> 

                    <h2>You Want Autonomy</h2>
                    <p>It almost seems paradoxical, because while you do seek meaningful relationships in your life, and enjoy the company of others, you also enjoy your independence to do what you like to do. This is true for most <span class="cc-blue">BLUES</span> because you spend so much time caring for others, connecting with them, and worrying about making things perfect, that you like to have your free time not to have to worry about those things. </p>
                    
                    <h2>You Want Quality in All Aspects of Your Life</h2>
                    <p>Your high standards of quality are important to you. You don't like things that are tacky, ill-planned, or impersonal. You like things that are done right and done well. It is important that you find people who support this instinct of yours and can appreciate the way that it enhances both of your lives. </p>
                    
                    <h2>You Want To Reveal Insecurities</h2>
                    <p>In your effort to connect with your partner and others, you will often feel the desire to reveal yourself and your insecurities and inadequacies. To you, being vulnerable is a small price to pay for the chance to connect emotionally. You also do this in hopes that others will be as equally open and revealing. Remember, you are driven by intimacy, so look for someone who can share with you on a deep level.</p>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phRed" runat="server" Visible="false">
                    <!-- start red -->
                    <div class="cc-color-banner cc-red-bg">Your Color Code Personality: <span class="cc-red">RED</span></div>
                    <h1>More About You &ndash; Your Wants</h1>
                    <h2>You Want to Hide Your Insecurities Tightly</h2>
                    <p>You feel insecure about your various inadequacies, and feel powerless when such things are exposed. Therefore, it is your tendency to guard those feelings of insecurity and inadequacy very tightly. This is not always appropriate, and you should find someone with whom you are able to open your heart and be vulnerable so that you learn to value yourself for who you are and not solely by what you are able to produce. </p> 

                    <h2>You Want Your Productivity to Be Supported</h2>

                    <p><span class="cc-red">REDS</span> want to be productive, and you are no exception. You feel you are at your best when you are able to accomplish various goals. It is important that you find someone who allows you the time and the space that you need to continue to produce and do what you do best. </p>

                    <h2>You Want to Lead the Relationship</h2>
                    <p>You love the challenge of leadership which allows you to stretch and give the best you have to offer. You will naturally gravitate toward leading the relationship and most people will allow you to do so; however, if others have the same desire and are equally as, or even more, dominant than you, it will become important for them to support your leadership instincts in important areas of the relationship. </p> 

                    <h2>You Want Challenging Adventure</h2>
                    <p><span class="cc-red">REDS</span> like to take risks and enjoy a good challenge. A spouse who can support that side of you, or even participate in the types of activities that you are promoting would be a very good fit for you, especially if they are able to rival you by challenging or pushing you beyond what you would normally do on your own.</p> 
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phWhite" runat="server" Visible="false">
                    <!-- start white -->
                    <div class="cc-color-banner cc-white-bg">Your Color Code Personality: <span class="cc-white">WHITE</span></div>
                    <h1>More About You &ndash; Your Wants</h1>

                    <h2>You Want to Withhold Your Insecurities</h2>
                    <p>You feel insecure about your various inadequacies, and feel embarrassed and confused about what to do when such things are exposed. Therefore, it is your tendency to guard those feelings of insecurity and inadequacy very tightly. This is not always appropriate, nor does it facilitate growth, so you should find someone with whom you are able to open your heart and be vulnerable. </p>

                    <h2>You Want Kindness From Your Partner</h2>
                    <p><span class="cc-white">WHITES</span> are the nicest, kindest people in world. You don't like conflict, and usually see it as being unnecessary when it manifests itself. You don't like mean people and what they stand for. Therefore, you need people who are kind to you and don't create turmoil and unneeded stress in your life. </p>

                    <h2>You Want Independence</h2>
                    <p><span class="cc-white">WHITES</span> are very independent by nature. They like to live in their own thoughts and enjoy their own company very much. They like to work alone, and it is not uncommon for <span class="cc-white">WHITES</span> to plan trips to go do something that they like alone. Friends who can allow you some space without taking it personally or feeling threatened would be good for you. </p>

                    <h2>You Want Contentment</h2>
                    <p>You like to feel at peace, and don't see any point in always keeping up with the Joneses or otherwise outdoing the status quo. You are typically very satisfied with that which you have, which is usually great, but at times you may just be avoiding putting forth the necessary effort that it would take to enjoy a higher standard of living. Find others who can balance this out with you.</p> 
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phYellow" runat="server" Visible="false">
                    <!-- start yellow -->
                    <div class="cc-color-banner cc-yellow-bg">Your Color Code Personality: <span class="cc-yellow">YELLOW</span></div>
                    <h1>More About You &ndash; Your Wants</h1>
                    <h2>You Want to Loosely Hide Your Insecurities</h2>
                    <p>You have a tender heart and so you protect it from being hurt by hiding your insecurities. You only guard them loosely, however, and are willing to reveal if you know that others will be gentle with you. Look for somebody who makes you feel comfortable in this arena, because the emotional connection and rapport that is created when you open up is not something you want to miss out on. </p>

                    <h2>You Want Happiness</h2>
                    <p><span class="cc-yellow">YELLOWS</span> wake up every morning happy. You don't like to dwell on unpleasantries, or spend time with people who pull you down. Find people who can support your lighthearted nature, but also help you confront the negative aspects of your life in a positive, upbeat way. </p>

                    <h2>You Want Freedom</h2>
                    <p>Number one on your list of wants in life is freedom. You like to be able to go and do without the weight of the world pulling you down. Others who don't always control your schedule and can support your sense of freedom by enjoying and participating in your spontaneous adventures will be a big plus to you. </p> 

                    <h2>You Want Playful Adventure</h2>
                    <p>You are excited by adventure, but not necessarily the grueling kind that involves hard work with little fun as a payoff. You like to be playful and enjoy a good adrenaline rush. One of the best things that you can do to ensure your happiness in a relationship is to find friends who are willing to play and experience life with you.</p> 
                </asp:PlaceHolder>

                <p class="text-outside">Next, <a href="Analysis.aspx?report=otherIntro">learn the personality traits of each Color &raquo;</a></p>
            </div><!-- end .editorial-->

            <!--Analysis common navigation-->
            <uc1:AnalysisNav ID="AnalysisNavControl" runat="server"></uc1:AnalysisNav>

        </div><!-- end .cc-content -->
    </div><!-- end .wrapper -->

    <!--Analysis sidebar-->
    <uc1:AnalysisSideBar ID="AnalysisSideBar1" runat="server"></uc1:AnalysisSideBar>
