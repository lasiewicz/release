﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Intro.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Controls.Analysis.Intro" %>
<%@ Register src="AnalysisNav.ascx" tagname="AnalysisNav" tagprefix="uc1" %>
<%@ Register src="AnalysisSideBar.ascx" tagname="AnalysisSideBar" tagprefix="uc1" %>

    <div class="wrapper">
        <div id="cc-content">

            <div class="editorial">

                <asp:PlaceHolder ID="phBlue" runat="server" Visible="false">
                <!-- start blue -->
                <div class="cc-color-banner cc-blue-bg">Your Color Code Personality: <span class="cc-blue">BLUE</span></div>
                <p>Congratulations, <%= UserName%>, you are a <span class="cc-blue">BLUE</span> personality. The Core Motivation that drives you through life is "intimacy." It is important to note that this does not mean sexual intimacy. <span class="cc-blue">BLUES</span> need connection – the sharing of rich, deep emotions that bind people together. As a <span class="cc-blue">BLUE</span>, you will often sacrifice a great deal of time, effort, and/or personal convenience to develop and maintain meaningful relationships throughout your life.</p>
                <p><span class="cc-blue">BLUES</span> seek opportunities to genuinely connect with others, and need to be understood and appreciated. Everything you do as a <span class="cc-blue">BLUE</span> has to be quality-based, or you won't do it at all. You are incredibly loyal to friends, employers, employees, and above all to your spouse. Whatever or whomever you commit to is your sole (and soul) focus. As a <span class="cc-blue">BLUE</span>, you love to serve and will give freely of yourself in order to nurture the lives of others.</p>
                <p><span class="cc-blue">BLUES</span> have distinct preferences and are the most controlling of the four personalities, although they may not acknowledge (or even realize) the fact. Your code of ethics is remarkably strong and you expect others to live honest, committed lives as well. You enjoy sharing meaningful moments in conversation with your partner, as well as remembering special life events (e.g., birthdays and anniversaries).</p> 
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phRed" runat="server" Visible="false">
                <!-- start red -->
                <div class="cc-color-banner cc-red-bg">Your Color Code Personality: <span class="cc-red">RED</span></div>
                <p>Congratulations, <%= UserName%>, you are a <span class="cc-red">RED</span> personality. The Core Motivation that drives you through life is power. Power means the ability to get things done, to go from A to B as quickly and directly as possible. The word power was derived from the Old French poeir meaning "to be able." Often what is perceived by the other Colors as insensitive is simply a pragmatic sense of urgency to accomplish a given task.</p>
                <p>As a <span class="cc-red">RED</span> you naturally seek productivity and want others to see you as intellectually strong. <span class="cc-red">REDS</span> want their own way, so you like to be in the driver's seat and are willing to pay the price to be in a leadership role even in an intimate relationship. However, you can get frustrated when others cannot think for themselves or make intelligent decisions on their own. As a <span class="cc-red">RED</span>, you tend to value whatever gets you ahead in life, whether it is at work, school, or in your personal relationships. What you value, you get done. You may be a workaholic and enjoy it! You will, however, resist being forced to do anything that doesn't interest you.</p> 
                <p>As a <span class="cc-red">RED</span>, you like to be right. You value approval from others for your intelligence and solution-based, pragmatic style. You want to be respected even more than you want to be loved, and you appreciate admiration for your logical, practical mind. </p>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phWhite" runat="server" Visible="false">
                <!-- start white -->
                <div class="cc-color-banner cc-white-bg">Your Color Code Personality: <span class="cc-white">WHITE</span></div>
                <p>Congratulations, <%= UserName%>, you are a <span class="cc-white">WHITE</span> personality. The Core Motivation that guides you through life is peace. This is not referring to a political agenda or the absence of war. It is, however, an absence of inner conflict, much closer to the idea of serenity, and an acceptance of oneself and others. You have a strong and compelling need to keep things in balance in your life so as to maintain an internal feeling of tranquility and comfort.</p> 
                <p>As a <span class="cc-white">WHITE</span>, you seek independence and require kindness, especially from those with whom you are in a relationship. You resist confrontation at all costs. (To you, feeling good internally is even more important than being good.) You are quiet by nature, process things very deeply and objectively with great clarity. Of all the colors, <span class="cc-white">WHITES</span> are the best listeners. You respect people who are direct but you recoil from perceived hostility or verbal battle. </p>
                <p>You need your "alone time" and refuse to be controlled by others. <span class="cc-white">WHITES</span> want to do things their own way and in their own time. They ask little of others and resent others demanding much of them. You are probably much stronger than people think, but are not often seen for your strength because you don't easily reveal your feelings. </p>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phYellow" runat="server" Visible="false">
                <!-- start yellow -->
                <div class="cc-color-banner cc-yellow-bg">Your Color Code Personality: <span class="cc-yellow">YELLOW</span></div>
                <p>Congratulations, <%= UserName%>, you are a <span class="cc-yellow">YELLOW</span> personality with the driving Core Motivation of fun. This does not mean that you are constantly looking for a party (although you usually do know where to find one!). You are instinctively happy and gravitate to people and situations that provide carefree adventure and playful interactions. Even under the most serious of circumstances or daunting tasks, you and your <span class="cc-yellow">YELLOW</span> friends will seek an element of personal fulfillment and spontaneous enjoyment in the experience. Spontaneous play and genuine "in the moment" FUN are not merely important to you – they are as essential to your very being as eating.</p>
                <p>As a <span class="cc-yellow">YELLOW</span>, you are inviting and embrace life as a party that you're hosting. You love playful interaction and can be extremely sociable. You are highly persuasive and seek instant gratification. <span class="cc-yellow">YELLOWS</span> need to be adored and praised, especially by their loved ones. While you are carefree, you are quite sensitive and highly alert to others' agendas to control you. You carry within yourself the gift of a good heart, and are happy to share it with your significant other.</p> 
                <p>As a <span class="cc-yellow">YELLOW</span>, you need to look good socially and friendships command a high priority in your life. You are happy, articulate, engaging of others and crave adventure. Easily distracted, you can never sit still for long. You embrace each day in the "present tense" and choose people to be around whom, like you, enjoy a curious nature.</p>
                </asp:PlaceHolder>
                
                <p>Now that you know about your Color, find out how you can apply the Color Code to your everyday life. Read on to learn why you are hot (and not), how you relate to other Colors, how to get what you want and more.</p>
                <p class="text-outside">Next, find out&nbsp;<a href="Analysis.aspx?report=hot"> What Makes You Hot! &raquo;</a></p>
            </div><!-- end .editorial-->
            
            <!--Analysis common navigation-->
            <uc1:AnalysisNav ID="AnalysisNavControl" runat="server"></uc1:AnalysisNav>
            
        </div><!-- end .cc-content -->
    </div><!-- end .wrapper -->

    <!--Analysis sidebar-->
    <uc1:AnalysisSideBar ID="AnalysisSideBar1" runat="server"></uc1:AnalysisSideBar>