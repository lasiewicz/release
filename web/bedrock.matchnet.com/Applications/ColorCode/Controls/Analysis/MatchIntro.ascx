﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchIntro.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Controls.Analysis.MatchIntro" %>
<%@ Register src="AnalysisNav.ascx" tagname="AnalysisNav" tagprefix="uc1" %>
<%@ Register src="AnalysisSideBar.ascx" tagname="AnalysisSideBar" tagprefix="uc1" %>

    <div class="wrapper">
        <div id="cc-content">

        <div class="editorial">

            <div class="cc-color-banner cc-<%=ColorText.ToLower() %>-bg">Your Color Code Personality: <span>How do I Match?</span></div>

            <h1>Overview</h1>

            <p>Now that you understand the power of the Color Code, discover how you match up with all four of the Core Colors.</p>

            <p>The beauty of the Color Code is that it teaches people that any two Core Colors can be a match. We can all be compatible as long as we have the right tools to understand each other. The advantage of the Color Code is having the ability to know what personality traits to expect from people, whether they're friends, family, co-workers or the people you date.</p>

            <p>For example, according to the Color Code, a spouse who is the Color <span class="cc-white">WHITE</span> does not naturally display the qualities of being verbally protective and assertive. Although people can change their behavior, finding out their Color can help you understand that it may be more difficult or feel unnatural for them to do so. Further, some people don't want to change and are happy with themselves as they are. The Color Code gives you a better understanding as to why people behave the way they do and allows you to choose partners based on the personality attributes that suit you best.</p>

            <p>Click on one of the Color combinations below to learn what to expect from a relationship with each Color. What are the pros and cons, how should you behave and how will Your Matches behave towards you? Take a look and decide which potential relationship will bring you the most happiness.</p>
        </div>

        <div id="cc-match-buttons" class="clearfix">
	        <a href="Analysis.aspx?report=matchR" class="cc-button"><span class="cc-<%=ColorText.ToLower() %>"><%=ColorText %></span> - <span class="cc-red">Red</span></a>
	        <a href="Analysis.aspx?report=matchB" class="cc-button"><span class="cc-<%=ColorText.ToLower() %>"><%=ColorText %></span> - <span class="cc-blue">Blue</span></a>
	        <a href="Analysis.aspx?report=matchW" class="cc-button"><span class="cc-<%=ColorText.ToLower() %>"><%=ColorText %></span> - <span class="cc-white">White</span></a>
	        <a href="Analysis.aspx?report=matchY" class="cc-button"><span class="cc-<%=ColorText.ToLower() %>"><%=ColorText %></span> - <span class="cc-yellow">Yellow</span></a>
        </div>

        <!--Analysis common navigation-->
        <uc1:AnalysisNav ID="AnalysisNavControl" runat="server"></uc1:AnalysisNav>
        
        </div><!-- end .cc-content -->
    </div><!-- end .wrapper -->

    <!--Analysis sidebar-->
    <uc1:AnalysisSideBar ID="AnalysisSideBar1" runat="server"></uc1:AnalysisSideBar>