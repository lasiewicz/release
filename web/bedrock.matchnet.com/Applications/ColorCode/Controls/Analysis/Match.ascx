﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Match.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Controls.Analysis.Match" %>
<%@ Register src="AnalysisNav.ascx" tagname="AnalysisNav" tagprefix="uc1" %>
<%@ Register src="AnalysisSideBar.ascx" tagname="AnalysisSideBar" tagprefix="uc1" %>

    <div class="wrapper">
        <div id="cc-content">

            <div class="editorial">
            
            <%--Title--%>
            <asp:PlaceHolder ID="phBlue" runat="server" Visible="false">
                <div class="cc-color-banner cc-blue-bg">Your Color Code Personality: <span>How do I Match?</span></div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phRed" runat="server" Visible="false">
                <div class="cc-color-banner cc-red-bg">Your Color Code Personality: <span>How do I Match?</span></div>
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="phWhite" runat="server" Visible="false">
                <div class="cc-color-banner cc-white-bg">Your Color Code Personality: <span>How do I Match?</span></div>
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="phYellow" runat="server" Visible="false">
                <div class="cc-color-banner cc-yellow-bg">Your Color Code Personality: <span>How do I Match?</span></div>
            </asp:PlaceHolder>
            
            <%--Color-Color--%>
            <asp:PlaceHolder ID="phBlueRed" runat="server" Visible="false">
                <h1><span class="cc-badge cc-blue-bg">Blue</span> - <span class="cc-badge cc-red-bg">Red</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-red">RED</span></h2>
                <p><span class="cc-red">REDS</span> are strong-willed, logical people. In a relationship, they like to call the shots. They are also willing to fight life's battles for and with their partners. They are ultra-responsible, and are usually excellent providers.  They date well, but tend to drop the "extras" once a serious commitment is made (e.g., trips, concert tickets, expensive dinners, etc.), because now that they have someone  why waste the money? Their biggest downfalls are their lack of ability to relate emotionally, and their tendency to be selfish.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-red">RED</span>.</p>

                <div class="clearfix">
                    <div class="two-column">
                        <h3>Pros</h3>
                        <ul class="list-arrow">
                          <li>Great Loyalty - <span class="cc-blue">BLUE</span> to relationships, <span class="cc-red">RED</span> to task.</li>
                          <li><span class="cc-red">RED</span> provides vision, <span class="cc-blue">BLUE</span> provides quality.</li>
                          <li>Both are highly dependable and follow through on commitments.</li>
                        </ul>
                    </div>
                    <div class="two-column">
                        <h3>Cons</h3>
                        <ul class="list-arrow">
                          <li>Both seek to control the other, <span class="cc-blue">BLUEs</span> with emotion, <span class="cc-red">REDS</span> with logic.</li>
                          <li>They don't readily accept and understand each other.</li>
                          <li><span class="cc-blue">BLUE</span> is emotion-based in thinking and <span class="cc-red">RED</span> is logic-based in thinking.</li>
                        </ul>
                    </div>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phBlueBlue" runat="server" Visible="false">
                <h1><span class="cc-badge cc-blue-bg">Blue</span> - <span class="cc-badge cc-blue-bg">Blue</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-blue">BLUE</span></h2>
                <p><span class="cc-blue">BLUEs</span> love to be in relationships and commit deeply to their partners. To them, a relationship is all or nothing, and you can expect the utmost loyalty from them. They are exceptionally thoughtful, remember every detail, and love to serve their partners. However, they can also be excessively needy emotionally, and have the tendency to read more into things than is really there. Their biggest downfalls are their tendency to have unrealistic expectations of themselves and others, and their tendency to be overly judgmental.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-blue">BLUE</span>.</p>

                <div class="clearfix">
                    <div class="two-column">
                        <h3>Pros</h3>
                        <ul class="list-arrow">
	                        <li>They both share concern for quality and detail.</li>
	                        <li>They are both extremely committed to the relationship.</li>
	                        <li>They are very dependable in completing projects or tasks.</li>
                        </ul>
                    </div>
                    <div class="two-column">
                        <h3>Cons</h3>
                        <ul class="list-arrow">
	                        <li>They can get caught up in the details at the sacrifice of essential productivity.</li>
	                        <li>They are very perfectionistic and therefore hard on themselves and each other.</li>
	                        <li>They can be too serious and feed each other's depression.</li>
                        </ul>
                    </div>

                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phBlueWhite" runat="server" Visible="false">
                <h1><span class="cc-badge cc-blue-bg">Blue</span> - <span class="cc-badge cc-white-bg">White</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-white">WHITE</span></h2>
                <p><span class="cc-white">WHITES</span> are calm and steady. They are pleasant to be with, and always kind-hearted to their partners. You will never feel judged when dating a <span class="cc-white">WHITE</span>, as things will always feel comfortable and informal. They are balanced, even-tempered, logical, and listen exceptionally well. However, they can also seem detached or unavailable emotionally, as they have a hard time verbalizing what they feel. Their biggest downfalls are their lack of motivation, and their tendency to not communicate very often or meaningfully.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-white">WHITE</span>.</p>

                <div class="clearfix">
                    <div class="two-column">
                        <h3>Pros</h3>
                        <ul class="list-arrow">
	                        <li>They will not get in each other's way or block the other from doing what they need or want to do.</li>
	                        <li>Both can operate very well on their own.</li>
	                        <li>They are typically more low-maintenance and require little entertainment from each other.</li>
                        </ul>
                    </div>
                    <div class="two-column">
                        <h3>Cons</h3>
                        <ul class="list-arrow">
	                        <li><span class="cc-blue">BLUES</span> are emotion-based in thinking and <span class="cc-white">WHITES</span> are logic-based in thinking.</li>
	                        <li>After the initial stages of the relationship, they tend to lose passion over time.</li>
	                        <li>The <span class="cc-white">WHITE</span> can tire of the <span class="cc-blue">BLUE'S</span> need to control and the <span class="cc-blue">BLUE</span> can grow frustrated at the <span class="cc-white">WHITE'S</span> lack of communication (quality and quantity).</li>
                        </ul>
                    </div>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phBlueYellow" runat="server" Visible="false">
                <h1><span class="cc-badge cc-blue-bg">Blue</span> - <span class="cc-badge cc-yellow-bg">Yellow</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-yellow">YELLOW</span></h2>
                <p><span class="cc-yellow">YELLOWS</span> are a lot of fun.  They are exciting, loud, and charismatic. Relationships with <span class="cc-yellow">YELLOWS</span> are always filled with social activities, riotous laughter, and a meaningful emotional connection as well. Expect them to be spontaneous, and live in the moment, free of any cares. The difficulty in being with <span class="cc-yellow">YELLOW</span> is they can be irresponsible. They don't plan in advance and take shortcuts through life. Their biggest downfalls are their inability to follow through and completely commit.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-yellow">YELLOW</span>.</p>

                <div class="clearfix">
                    <div class="two-column">
                        <h3>Pros</h3>
                        <ul class="list-arrow">
	                        <li>They share a strong emotional connection.</li>
	                        <li>Together they have a high capacity for problem solving, creativity, and high quality lifestyle.</li>
	                        <li>The <span class="cc-blue">BLUE</span> provides the quality and detail which is complimented by the <span class="cc-yellow">YELLOW</span> bringing the energy and capacity for risk-taking.</li>
                        </ul>
                    </div>
                    <div class="two-column">
                        <h3>Cons</h3>
                        <ul class="list-arrow">
	                        <li>They can become too interdependent.</li>
	                        <li>Emotions can sometimes get in the way.</li>
	                        <li>They may grow resentful toward each other if the <span class="cc-blue">BLUE</span> tries to be too controlling and the <span class="cc-yellow">YELLOW</span> becomes too noncommittal.</li>
                        </ul>
                    </div>

                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phRedBlue" runat="server" Visible="false">
                <h1><span class="cc-badge cc-red-bg">RED</span> - <span class="cc-badge cc-blue-bg">BLUE</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-blue">BLUE</span></h2>
                <p><span class="cc-blue">BLUEs</span> love to be in relationships and commit deeply to their partners. To them, a relationship is all or nothing, and you can expect the utmost loyalty from them. They are exceptionally thoughtful, remember every detail, and love to serve their partners. However, they can also be excessively needy emotionally, and have the tendency to read more into things than is really there. Their biggest downfalls are their tendency to have unrealistic expectations of themselves and others, and their tendency to be overly judgmental.
                </p>
                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-blue">BLUE</span>.</p>
                
                <div class="clearfix">
                <div class="two-column">
                    <h3>Pros</h3>
                    <ul class="list-arrow">
                      <li>Great Loyalty - <span class="cc-red">RED</span> to task, <span class="cc-blue">BLUE</span> to relationships.</li>
                      <li><span class="cc-red">RED</span> provides vision, <span class="cc-blue">BLUE</span> provides quality.</li>
                      <li>Both are highly dependable and follow through on commitments.</li>
                    </ul>
                </div>
                <div class="two-column">
                    <h3>Cons</h3>
                    <ul class="list-arrow">
                      <li>Both seek to control the other, <span class="cc-red">REDS</span> with logic, <span class="cc-blue">BLUES</span> with emotion.</li>
                      <li>They don't readily accept and understand each other.</li>
                      <li><span class="cc-red">RED</span> is logic-based in thinking and <span class="cc-blue">BLUE</span> is emotion-based in thinking.</li>
                    </ul>
                </div>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phRedRed" runat="server" Visible="false">
                <h1><span class="cc-badge cc-red-bg">RED</span> - <span class="cc-badge cc-red-bg">RED</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-red">RED</span></h2>
                <p><span class="cc-red">REDs</span> are strong-willed, logical people. In a relationship, they like to call the shots. They are also willing to fight life's battles for and with their partners. They are ultra-responsible, and are usually excellent providers.  They date well, but tend to drop the "extras" once a serious commitment is made (e.g., trips, concert tickets, expensive dinners, etc.), because now that they have someone – why waste the money? Their biggest downfalls are their lack of ability to relate emotionally, and their tendency to be selfish.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-red">RED</span>.</p>
                
                <div class="clearfix">
                <div class="two-column">
                    <h3>Pros</h3>
                    <ul class="list-arrow">
                      <li>They create lots of action and results together.</li>
                      <li>Obstacles do not deter them and they are willing to take risks together.</li>
                      <li>They usually carry no emotional baggage.</li>
                    </ul>
                </div>
                <div class="two-column">
                    <h3>Cons</h3>
                    <ul class="list-arrow">
                      <li>Major ego clashes are common.</li>
                      <li>As they are both very controlling, they tend to get in each other's way.</li>
                      <li>They are often highly critical of each other.</li>
                    </ul>
                </div>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phRedWhite" runat="server" Visible="false">
                <h1><span class="cc-badge cc-red-bg">RED</span> - <span class="cc-badge cc-white-bg">WHITE</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-white">WHITE</span></h2>
                <p><span class="cc-white">WHITEs</span> are calm and steady.  They are pleasant to be with, and always kind-hearted to their partners. You will never feel judged when dating a <span class="cc-white">WHITE</span>, as things will always feel comfortable and informal. They are balanced, even-tempered, logical, and listen exceptionally well. However, they can also seem detached or unavailable emotionally, as they have a hard time verbalizing what they feel. Their biggest downfalls are their lack of motivation, and their tendency to not communicate very often or meaningfully.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-white">WHITE</span>.</p>
                
                <div class="clearfix">
                <div class="two-column">
                    <h3>Pros</h3>
                    <ul class="list-arrow">
                      <li>Both talk the language of logic and therefore communicate on the same wavelength.</li>
                      <li>Decision making can be very compatible.</li>
                      <li>The <span class="cc-red">RED</span> sees the big picture, and the <span class="cc-white">WHITE</span> is able to put it into focus.</li>
                    </ul>
                </div>
                <div class="two-column">
                    <h3>Cons</h3>
                    <ul class="list-arrow">
                      <li>They can both be too independent.</li>
                      <li>The <span class="cc-red">RED</span> can overpower the <span class="cc-white">WHITE</span>, which causes the <span class="cc-white">WHITE</span> to fight back with stubbornness and through other passive-aggressive behaviors.</li>
                      <li>The <span class="cc-white">WHITE</span> must learn to speak up, or will lose the <span class="cc-red">RED'S</span> respect and will never allow a true connection to formulate.</li>
                    </ul>
                </div>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phRedYellow" runat="server" Visible="false">
                <h1><span class="cc-badge cc-red-bg">RED</span> - <span class="cc-badge cc-yellow-bg">Yellow</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-yellow">YELLOW</span></h2>
                <p><span class="cc-yellow">YELLOWS</span> are a lot of fun.  They are exciting, loud, and charismatic. Relationships with <span class="cc-yellow">YELLOWS</span> are always filled with social activities, riotous laughter, and a meaningful emotional connection as well. Expect them to be spontaneous, and live in the moment, free of any cares. The difficulty in being with <span class="cc-yellow">YELLOW</span> is they can be irresponsible. They don't plan in advance and take shortcuts through life. Their biggest downfalls are their inability to follow through and completely commit.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-yellow">YELLOW</span>.</p>
                
                <div class="clearfix">
                <div class="two-column">
                    <h3>Pros</h3>
                    <ul class="list-arrow">
                        <li>Both possess great communication skills.</li>
                        <li>Both are outgoing and willing to take risks.</li>
                        <li><span class="cc-red">RED</span> brings focus, and <span class="cc-yellow">YELLOW</span> tempers intensity with fun.</li>
                    </ul>
                </div>
                <div class="two-column">
                    <h3>Cons</h3>
                    <ul class="list-arrow">
                        <li>They struggle with keeping the relationship committed and together long-term.</li>
                        <li>Both can be highly insensitive.</li>
                        <li><span class="cc-red">RED</span> is logic-based in thinking, <span class="cc-yellow">YELLOW</span> is emotion-based in thinking.</li>
                    </ul>
                </div>
                </div>
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="phWhiteBlue" runat="server" Visible="false">
                <h1><span class="cc-badge cc-white-bg">WHITE</span> - <span class="cc-badge cc-blue-bg">BLUE</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-blue">BLUE</span></h2>
                <p><span class="cc-blue">BLUEs</span> love to be in relationships and commit deeply to their partners. To them, a relationship is all or nothing, and you can expect the utmost loyalty from them.  They are exceptionally thoughtful, remember every detail, and love to serve their partners. However, they can also be excessively needy emotionally, and have the tendency to read more into things than is really there. Their biggest downfalls are their tendency to have unrealistic expectations of themselves and others, and their tendency to be overly judgmental.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-blue">BLUE</span>.</p>

                <div class="clearfix">
                <div class="two-column">
                    <h3>Pros</h3> 
                    <ul class="list-arrow">
                      <li>They will not get in each other's way or block the other from doing what they need or want to do.</li>
                      <li>Both can operate very well on their own.</li>
                      <li>They are typically more low-maintenance and require little entertainment from each other.</li>
                    </ul>
                </div>
                <div class="two-column">
                    <h3>Cons</h3> 
                    <ul class="list-arrow">
                      <li><span class="cc-white">WHITES</span> are logic-based in thinking and <span class="cc-blue">BLUEs</span> are emotion-based in thinking.</li>
                      <li>After the initial stages of the relationship, they tend to lose passion over time.</li>
                      <li>The <span class="cc-white">WHITE</span> can tire of the <span class="cc-blue">BLUE's</span> need to control and the <span class="cc-blue">BLUE</span> can grow frustrated at the <span class="cc-white">WHITE'S</span> lack of communication (quality and quantity).</li>
                    </ul>
                </div>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phWhiteRed" runat="server" Visible="false">
                <h1><span class="cc-badge cc-white-bg">WHITE</span> - <span class="cc-badge cc-red-bg">RED</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-red">RED</span></h2>
                <p><span class="cc-red">REDS</span> are strong-willed, logical people.  In a relationship, they like to call the shots. They are also willing to fight life's battles for and with their partners. They are ultra-responsible, and are usually excellent providers.  They date well, but tend to drop the "extras" once a serious commitment is made (e.g., trips, concert tickets, expensive dinners, etc.), because now that they have someone – why waste the money? Their biggest downfalls are their lack of ability to relate emotionally, and their tendency to be selfish.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-red">RED</span>.</p>

                <div class="clearfix">
                <div class="two-column">
                    <h3>Pros</h3> 
                    <ul class="list-arrow">
                      <li>Both talk the language of logic and therefore communicate on the same wavelength.</li>
                      <li>Decision making can be very compatible.</li>
                      <li>The <span class="cc-red">RED</span> sees the big picture, and the <span class="cc-white">WHITE</span> is able to put it into focus.</li>
                    </ul>
                </div>
                <div class="two-column">
                    <h3>Cons</h3> 
                    <ul class="list-arrow">
                      <li>They can both be too independent.</li>
                      <li>The <span class="cc-red">RED</span> can overpower the <span class="cc-white">WHITE</span>, which causes the <span class="cc-white">WHITE</span> to fight back with stubbornness and through other passive-aggressive behaviors.</li>
                      <li>The <span class="cc-white">WHITE</span> must learn to speak up, or will lose the <span class="cc-red">RED's</span> respect and will never allow a true connection to formulate.</li>
                    </ul>
                </div>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phWhiteWhite" runat="server" Visible="false">
                <h1><span class="cc-badge cc-white-bg">WHITE</span> - <span class="cc-badge cc-white-bg">WHITE</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-white">WHITE</span></h2>
                <p><span class="cc-white">WHITES</span> are calm and steady. They are pleasant to be with, and always kind-hearted to their partners. You will never feel judged when dating a <span class="cc-white">WHITE</span>, as things will always feel comfortable and informal.  They are balanced, even-tempered, logical, and listen exceptionally well. However, they can also seem detached or unavailable emotionally, as they have a hard time verbalizing what they feel. Their biggest downfalls are their lack of motivation, and their tendency to not communicate very often or meaningfully.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-white">WHITE</span>.</p>

                <div class="clearfix">
                <div class="two-column">
                    <h3>Pros</h3> 
                    <ul class="list-arrow">
                      <li>They are both excellent and inventive problem solvers.</li>
                      <li>They respect each other for being very kind and accepting.</li>
                      <li>Both are very content with their lot in life and do not struggle with the attitude of being dealt an unfair lot.</li>
                    </ul>
                </div>
                <div class="two-column">
                    <h3>Cons</h3> 
                    <ul class="list-arrow">
                      <li>Their procrastination severely limits productivity.</li>
                      <li>Refusing to have conflict, they tend not to stretch.</li>

                      <li>They lack leadership and can be easily overwhelmed by life's challenges.</li>
                    </ul>
                </div>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phWhiteYellow" runat="server" Visible="false">
                <h1><span class="cc-badge cc-white-bg">WHITE</span> - <span class="cc-badge cc-yellow-bg">Yellow</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-yellow">YELLOW</span></h2>
                <p><span class="cc-yellow">YELLOWS</span> are a lot of fun.  They are exciting, loud, and charismatic. Relationships with <span class="cc-yellow">YELLOWS</span> are always filled with social activities, riotous laughter, and a meaningful emotional connection as well. Expect them to be spontaneous, and live in the moment, free of any cares. The difficulty in being with <span class="cc-yellow">YELLOW</span> is they can be irresponsible. They don't plan in advance and take shortcuts through life. Their biggest downfalls are their inability to follow through and completely commit.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-yellow">YELLOW</span>.</p>

                <div class="clearfix">
                <div class="two-column">
                    <h3>Pros</h3> 
                    <ul class="list-arrow">
                      <li>They are both very flexible and have a way of rolling with the punches.</li>
                      <li>The <span class="cc-yellow">YELLOW</span> plugs the <span class="cc-white">WHITE</span> into life and excitement and the <span class="cc-white">WHITE</span> provides clarity of purpose and balance.</li>
                      <li>Their gentle nature makes for a positive connection.</li>
                    </ul>
                </div>
                <div class="two-column">
                    <h3>Cons</h3> 
                    <ul class="list-arrow">
                      <li>They can lack the ferocity and drive to light each other's fires in a motivational sense - leadership is an issue.</li>
                      <li>The <span class="cc-white">WHITE</span> can tire of the <span class="cc-yellow">YELLOW'S</span> noise, and the <span class="cc-yellow">YELLOW</span> can tire of the <span class="cc-white">WHITE'S</span> lack of enthusiasm.</li>
                      <li>They both are lacking in setting goals and direction for themselves or knowing what they want or where they're going in life.</li>
                    </ul>
                </div>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phYellowBlue" runat="server" Visible="false">
                <h1><span class="cc-badge cc-yellow-bg">YELLOW</span> - <span class="cc-badge cc-blue-bg">BLUE</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-blue">BLUE</span></h2>
                <p><span class="cc-blue">BLUEs</span> love to be in relationships and commit deeply to their partners. To them, a relationship is all or nothing, and you can expect the utmost loyalty from them.  They are exceptionally thoughtful, remember every detail, and love to serve their partners. However, they can also be excessively needy emotionally, and have the tendency to read more into things than is really there. Their biggest downfalls are their tendency to have unrealistic expectations of themselves and others, and their tendency to be overly judgmental.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-blue">BLUE</span>.</p>

                <div class="clearfix">
                <div class="two-column">
                    <h3>Pros</h3> 
                    <ul class="list-arrow">
                      <li>They share a strong emotional connection.</li>
                      <li>Together they have a high capacity for problem solving, creativity, and high quality lifestyle.</li>
                      <li>The <span class="cc-blue">BLUE</span> provides the quality and detail which is complimented by the <span class="cc-yellow">YELLOW</span> bringing the energy and capacity for risk-taking.</li>
                    </ul>
                </div>
                <div class="two-column">
                    <h3>Cons</h3> 
                    <ul class="list-arrow">
                      <li>They can become too interdependent.</li>
                      <li>Emotions can sometimes get in the way.</li>
                      <li>They may grow resentful toward each other if the <span class="cc-blue">BLUE</span> tries to be too controlling and the <span class="cc-yellow">YELLOW</span> becomes too noncommittal.</li>
                    </ul>
                </div>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phYellowRed" runat="server" Visible="false">
                <h1><span class="cc-badge cc-yellow-bg">YELLOW</span> - <span class="cc-badge cc-red-bg">RED</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-red">RED</span></h2>
                <p><span class="cc-red">REDS</span> are strong-willed, logical people. In a relationship, they like to call the shots. They are also willing to fight life's battles for and with their partners.  They are ultra-responsible, and are usually excellent providers.  They date well, but tend to drop the "extras" once a serious commitment is made (e.g., trips, concert tickets, expensive dinners, etc.), because now that they have someone – why waste the money? Their biggest downfalls are their lack of ability to relate emotionally, and their tendency to be selfish.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-red">RED</span>.</p>

                <div class="clearfix">
                <div class="two-column">
                    <h3>Pros</h3>
                    <ul class="list-arrow">
                      <li>Both possess great communication skills.</li>
                      <li>Both are outgoing and willing to take risks.</li>
                      <li><span class="cc-red">RED</span> brings focus, and <span class="cc-yellow">YELLOW</span> tempers intensity with fun.</li>
                    </ul>
                </div>
                <div class="two-column">
                    <h3>Cons</h3>
                    <ul class="list-arrow">
                      <li>They struggle with keeping the relationship committed and together long-term.</li>
                      <li>Both can be highly insensitive.</li>
                      <li><span class="cc-yellow">YELLOW</span> is emotion-based in thinking, <span class="cc-red">RED</span> is logic-based in thinking.</li>
                    </ul>
                </div>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phYellowWhite" runat="server" Visible="false">
                <h1><span class="cc-badge cc-yellow-bg">YELLOW</span> - <span class="cc-badge cc-white-bg">WHITE</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-white">WHITE</span></h2>
                <p><span class="cc-white">WHITES</span> are calm and steady. They are pleasant to be with, and always kind-hearted to their partners. You will never feel judged when dating a <span class="cc-white">WHITE</span>, as things will always feel comfortable and informal.  They are balanced, even-tempered, logical, and listen exceptionally well. However, they can also seem detached or unavailable emotionally, as they have a hard time verbalizing what they feel. Their biggest downfalls are their lack of motivation, and their tendency to not communicate very often or meaningfully.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can have a successful relationship with any color. Here are the pros and cons of how you get along with a <span class="cc-white">WHITE</span>.</p>
                <div class="clearfix">
                <div class="two-column">
                    <h3>Pros</h3>
                    <ul class="list-arrow">
                      <li>They are both very flexible and have a way of rolling with the punches.</li>

                      <li>The <span class="cc-yellow">YELLOW</span> plugs the <span class="cc-white">WHITE</span> into life and excitement and the <span class="cc-white">WHITE</span> provides clarity of purpose and balance.</li>
                      <li>Their gentle nature makes for a positive connection.</li>
                    </ul>
                </div>
                <div class="two-column">
                    <h3>Cons</h3>
                    <ul class="list-arrow">
                      <li>They can lack the ferocity and drive to light each other's fires in a motivational sense - leadership is an issue.</li>
                      <li>The <span class="cc-white">WHITE</span> can tire of the <span class="cc-yellow">YELLOW'S</span> noise, and the <span class="cc-yellow">YELLOW</span> can tire of the <span class="cc-white">WHITE'S</span> lack of enthusiasm.</li>

                      <li>They both are lacking in setting goals and direction for themselves or knowing what they want or where they're going in life.</li>
                    </ul>
                </div>
                </div>
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="phYellowYellow" runat="server" Visible="false">
                <h1><span class="cc-badge cc-yellow-bg">YELLOW</span> - <span class="cc-badge cc-yellow-bg">Yellow</span></h1>

                <h2>What to Expect from a Relationship with a <span class="cc-yellow">YELLOW</span></h2>
                <p><span class="cc-yellow">YELLOWS</span> are a lot of fun. They are exciting, loud, and charismatic. Relationships with <span class="cc-yellow">YELLOWS</span> are always filled with social activities, riotous laughter, and a meaningful emotional connection as well.  Expect them to be spontaneous, and live in the moment, free of any cares.  The difficulty in being with <span class="cc-yellow">YELLOW</span> is they can be irresponsible. They don't plan in advance and take shortcuts through life. Their biggest downfalls are their inability to follow through and completely commit.</p>

                <h2>How Do Your Colors Relate?</h2>
                <p>You can find your match with any color.  Here are the pros and cons of how you get along with a <span class="cc-yellow">YELLOW</span>.</p>
                <div class="clearfix">
                <div class="two-column">
                    <h3>Pros</h3>
                    <ul class="list-arrow">
                      <li>There is potential for tremendous creativity.</li>
                      <li>They are both highly communicative.</li>
                      <li>Both exude lots of enthusiasm and charisma.</li>
                    </ul>
                </div>
                <div class="two-column">
                    <h3>Cons</h3>
                    <ul class="list-arrow">
                      <li>They tend to be very flippant and uncommitted to each other.</li>
                      <li>Individually or together they can both be highly self-centered and irresponsible.</li>
                      <li>This combination may lack the discipline required to live a quality life.</li>
                    </ul>
                </div>
                </div>
            </asp:PlaceHolder>

            <%-- Match Color  --%>
            <asp:PlaceHolder ID="phMatchRed" runat="server" Visible="false">
                <div class="clearfix"><!-- red do's and don'ts-->
                    <h2>Do's and Don'ts with this Motive-Type</h2>
                    <div class="clearfix">
                    <div class="two-column">
                        <h3>For best relationships with <span class="cc-red">REDs</span>, you must:</h3>
                        <ol>
                          <li>Present issues logically.</li>
                          <li>Be direct, brief, and specific.</li>
                          <li>Prepare yourself with facts and figures.</li>
                          <li>Articulate your feelings clearly.</li>
                          <li>Demand attention and respect.</li>
                        </ol>
                    </div>
                    <div class="two-column">
                        <h3>For best relationships with <span class="cc-red">REDs</span>, you must not:</h3>
                        <ol>
                          <li>Embarrass them in front of others.</li>
                          <li>Argue from an emotional perspective.</li>
                          <li>Be slow and indecisive.</li>
                          <li>Wait for them to solicit your opinion.</li>
                          <li>Take their arguments personally.</li>
                        </ol>
                    </div>
                    </div>
                </div><!-- end red do's and don'ts-->
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phMatchBlue" runat="server" Visible="false">
                <div class="clearfix"><!-- blue do's and don'ts-->
                    <h2>Do's and Don'ts with this Motive-Type</h2>
                    <div class="clearfix">
                    <div class="two-column">
                        <h3>For best relationships with <span class="cc-blue">BLUEs</span>, you must:</h3>
                        <ol>
                          <li>Take a sensitive, sincere approach.</li>
                          <li>Allow time for them to collect their thoughts.</li>
                          <li>Help them feel secure in the relationship.</li>
                          <li>Appreciate them.</li>
                          <li>Be loyal.</li>
                        </ol>
                    </div>
                    <div class="two-column">
                        <h3>For best relationships with <span class="cc-blue">BLUEs</span>, you must not:</h3>
                        <ol>
                          <li>Make them feel guilty.</li>
                          <li>Be rude or abrupt.</li>
                          <li>Promote too much change.</li>
                          <li>Expect spontaniety.</li>
                          <li>Expect them to forgive quickly when crossed.</li>
                        </ol>
                    </div>
                    </div>
                </div><!-- end blue do's and don'ts-->
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phMatchWhite" runat="server" Visible="false">
                <div class="clearfix"><!-- white do's and don'ts-->
                    <h2>Do's and Don'ts with this Motive-Type</h2>
                    <div class="clearfix">
                    <div class="two-column">
                        <h3>For best relationships with <span class="cc-white">WHITEs</span>, you must:</h3>
                        <ol>
                          <li>Accept and support their individuality.</li>
                          <li>Create an informal, relaxed atmosphere.</li>
                          <li>Show patience; try not to rush them.</li>
                          <li>Look for nonverbal clues to their feelings.</li>
                          <li>Hear them out; listen quietly and carefully.</li>
                        </ol>
                    </div>
                    <div class="two-column">
                        <h3>For best relationships with <span class="cc-white">WHITEs</span>, you must not:</h3>
                        <ol>
                          <li>Be cruel or insensitive.</li>
                          <li>Expect them to need much social interaction.</li>
                          <li>Be domineering or too intense.</li>
                          <li>Overwhelm them with too much at once.</li>

                          <li>Force confrontation.</li>
                        </ol>
                    </div>
                    </div>
                </div><!-- end white do's and don'ts-->
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phMatchYellow" runat="server" Visible="false">
                <div class="clearfix"><!-- yellow do's and don'ts-->
                    <h2>Do's and Don'ts with this Motive-Type</h2>
                    <div class="clearfix">
                    <div class="two-column">
                        <h3>For best relationships with <span class="cc-yellow">YELLOWS</span>, you must:</h3>
                        <ol>
                          <li>Take a positive, upbeat approach.</li>
                          <li>Offer them praise and appreciation.</li>
                          <li>Accept some playful teasing, joking, or comic relief.</li>
                          <li>Reinforce trust with appropriate physical gestures.</li>
                          <li>Promote fun activities with them.</li>
                        </ol>
                    </div>
                    <div class="two-column">
                        <h3>For best relationships with <span class="cc-yellow">YELLOWS</span>, you must not:</h3>
                        <ol>
                          <li>Be too serious or somber in criticism.</li>
                          <li>Push them too intensely.</li>
                          <li>Expect them to dwell on problems.</li>
                          <li>Attack their sensitivity or be unforgiving.</li>

                          <li>Control their schedules or totally consume their time.</li>
                        </ol>
                    </div>
                    </div>

                </div><!-- end yellow do's and don'ts-->
            </asp:PlaceHolder>


            </div><!-- end editorial -->

            <div id="cc-match-buttons" class="clearfix">
	            <a href="Analysis.aspx?report=matchR" class="cc-button"><span class="cc-<%=ColorText.ToLower() %>"><%=ColorText %></span> - <span class="cc-red">Red</span></a>
	            <a href="Analysis.aspx?report=matchB" class="cc-button"><span class="cc-<%=ColorText.ToLower() %>"><%=ColorText %></span> - <span class="cc-blue">Blue</span></a>
	            <a href="Analysis.aspx?report=matchW" class="cc-button"><span class="cc-<%=ColorText.ToLower() %>"><%=ColorText %></span> - <span class="cc-white">White</span></a>
	            <a href="Analysis.aspx?report=matchY" class="cc-button"><span class="cc-<%=ColorText.ToLower() %>"><%=ColorText %></span> - <span class="cc-yellow">Yellow</span></a>
            </div>

            <!--Analysis common navigation-->
            <uc1:AnalysisNav ID="AnalysisNavControl" runat="server"></uc1:AnalysisNav>




        </div><!-- end .cc-content -->
    </div><!-- end .wrapper -->

    <!--Analysis sidebar-->
    <uc1:AnalysisSideBar ID="AnalysisSideBar1" runat="server"></uc1:AnalysisSideBar>    