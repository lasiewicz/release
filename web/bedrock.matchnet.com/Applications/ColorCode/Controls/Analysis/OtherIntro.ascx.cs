﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.Controls.Analysis
{
    public partial class OtherIntro : ColorCodeControl
    {
        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion

        #region Public Methods
        public override void LoadReport(MemberQuiz memberQuiz, ReportType reportType)
        {
            base.LoadReport(memberQuiz, reportType);
            AnalysisNavControl.LoadReport(memberQuiz, reportType);
            AnalysisSideBar1.LoadReport(memberQuiz, reportType);
        }
        #endregion
    }
}