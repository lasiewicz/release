﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PieChart.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Controls.Analysis.PieChart" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
		        
<table id="pie" class="<%=_PieCSS%>">
    <caption>Color Code</caption>
    <thead>
        <tr>
            <td>Color</td>
            <th>Percentage</th>
        </tr>
    </thead>
    <tbody>
        <%-- 
        <tr>
            <th>Blue</th>
            <td><%=_BlueScore %></td>
        </tr>
        <tr>
            <th>White</th>
            <td><%= _WhiteScore %></td>
        </tr>
        <tr>
            <th>Yellow</th>
            <td><%= _YellowScore %></td>
        </tr>
        <tr>
            <th>Red</th>
            <td><%= _RedScore %></td>
        </tr>	
        --%>
        <asp:Literal ID="LiteralPieTableRows" runat="server"></asp:Literal>	
    </tbody>
</table>