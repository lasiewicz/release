﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutColorCode.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.AboutColorCode" %>

<div id="page-container" class="clearfix cc-content-main cc-content-no-bars">

    <h2 class="tagline">The Color Code <small class="sans-serif">- Understand yourself and your matches on a deeper level.</small></h2>

    <div class="wrapper">
        <div id="cc-content" class="cc-content-full">

            <div class="editorial">


            <h1>About the Color Code</h1>

            <h2>1. The Color Code is motive-based</h2>
            <p>The Color Code works and is one of the best tools on the market today, because it is based on human motives (why you do what you do) rather than on human behaviors (what you do). Behavior (for example, the way you act in a chat room or on a date) can be imitated, copied or faked, but if you know the true motive behind the behavior (what is driving the person to behave as they do), you already have a much clearer picture of what that person is all about.</p>

            <h2>2. You only have one Core Motive or Color Code</h2>
            <p>Your personality type is driven by only ONE of four personality Colors, or driving Core Motives:</p>

            <ul class="list-paragraphs">
              <li><span class="cc-red">RED</span> (The Power-Wielders): Core Motive = Power, or the ability to move from "a" to "b" as efficiently as possible</li>
              <li><span class="cc-blue">BLUE</span> (The Do-Gooders): Core Motive = Intimacy, this doesn't mean sex, but the need to connect, share feelings, and build relationships with others</li>
              <li><span class="cc-white">WHITE</span> (The Peacekeepers): Core Motive = Peace, or calm even in the midst of conflict; clarity in the midst of confusion</li>
              <li><span class="cc-yellow">YELLOW</span> (The Fun-Lovers): Core Motive = Fun, or always enjoying the moment</li>
            </ul>

            <p>These are the four basic personality types about which you will learn. However, very few people have ever scored 100% in one single Color while taking the profile; therefore, you will find that your Core Color is often influenced by traces of the other colors. That is why no two <span class="cc-white">WHITES</span>, although driven by the same Core Motive of Peace, will ever be exactly alike.</p>

            <h2>3. Your Core Color was present at birth and it cannot be changed</h2>
            <p>You were born with your core personality Color intact (ask any woman who has given birth to more than one child and she'll tell you that her children had different personalities before they had drawn their first breath), and while parts of your personality do change over time (for example, you may have not been born a good listener, but you have learned to become one), you cannot and should not try to discard your Core Color in an attempt to trade it for another. If you were born a <span class="cc-yellow">YELLOW</span>, you will die a <span class="cc-yellow">YELLOW</span>, but you can add to yourself any strength or any limitation of any Color to your core self.</p>

            <h2>4. All Colors are of equal importance</h2>
            <p>No personality type is better than another. Each brings equally valuable, albeit, different gifts to the world.</p>

            <h2>5. All Colors are neither good nor bad</h2>
            <p>No personality type is innately good or bad. Many people who do not know the Color Code may assume that all <span class="cc-blue">BLUES</span> must be good and all <span class="cc-red">REDS</span> must be bad, for example. This couldn't be more false. The Colors are neutral and individuals are free to choose how they will use their strengths and limitations to leave either a positive or a negative legacy in life.</p>

            <p>The Color Code was created by Dr. Taylor Hartman.</p> 

            </div><!-- end editorial -->


            <p class="text-center editorial cc-button-primary">	
	            <a href="Landing.aspx" class="link-primary">Back to Color Code Profile</a>
            </p>

        </div><!-- end .cc-content -->
    </div><!-- end .wrapper -->


</div><!-- end #page-container -->

<p id="cc-copy">&copy; 2010 Color Code Communications, Inc. All rights reserved.</p>
