﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Quiz1.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Quiz1" %>

<div id="page-container" class="clearfix cc-content-main cc-content-bars-small">

    <h2 class="tagline">The Color Code <small class="sans-serif">- Understand yourself and your matches on a deeper level.</small></h2>
    <div id="cc-content">
        <div class="editorial">
            <h1>The Color Code Personality Test</h1>
            <p>There are two sections in this test: Strengths &amp; Limitations (numbers 1-30) and Situations (numbers 31-45). Please read the instructions for both sections and make sure you understand them before beginning the test.  The test may LOOK long, but it should take you less than 15 minutes to complete.</p>
            <h3>Part 1: Strengths &amp; Limitations</h3>
            <p>This section consists of 30 four-word clusters. For each cluster, choose the one trait that best describes how you behaved AS A CHILD. Try not to focus on how you wish you were, or how you would like to be. Remember, your first impression is usually the best. <a href="#" class="cc-quiz-help">Need more help?</a></p>
        </div>
        <div id="cc-questions-container">
            <asp:Repeater ID="repeaterQuestions" runat="server" OnItemDataBound="RepeaterSections_ItemDataBound">
                <HeaderTemplate><ul class="cc-questions clearfix"></HeaderTemplate>
                <ItemTemplate>
                    <li><h2><asp:Literal ID="literalQuestionText" runat="server"></asp:Literal></h2>
                    <ul>
                        <li><asp:Literal ID="radioChoice1" runat="server"></asp:Literal></li>
                        <li><asp:Literal ID="radioChoice2" runat="server"></asp:Literal></li>
                        <li><asp:Literal ID="radioChoice3" runat="server"></asp:Literal></li>
                        <li><asp:Literal ID="radioChoice4" runat="server"></asp:Literal></li>
                    </ul></li>
                </ItemTemplate>
                <FooterTemplate></ul></FooterTemplate>
            </asp:Repeater>
            <%--
            <ul class="cc-questions clearfix">
            <li>
	            <h2>Question 1</h2>
	            <ul>
		            <li><input id="q1Proactive" type="radio" name="question1" /><label for="q1Proactive">proactive</label></li>
		            <li><input id="q2Proactive" type="radio" name="question1" /><label for="q2Proactive">nurturing</label></li>
		            <li><input id="q3Proactive" type="radio" name="question1" /><label for="q3Proactive">objective</label></li>
		            <li><input id="q4Proactive" type="radio" name="question1" /><label for="q4Proactive">insightful</label></li>

	            </ul>
            </li>
            </ul>
            --%>
        </div>

    </div><!-- end .cc-content-->
    <p class="text-center editorial cc-button-primary">	
	    <asp:LinkButton ID="btnSubmit" runat="server" CssClass="link-primary" OnClick="btnSubmit_Click">Continue to Part 2</asp:LinkButton>
    </p>

</div><!-- end #page-container -->
<p id="cc-copy">&copy; 2010 Color Code Communications, Inc. All rights reserved.</p>