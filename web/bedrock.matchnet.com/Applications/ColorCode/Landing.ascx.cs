﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.ColorCode
{
    public partial class Landing : FrameworkControl
    {
        MemberQuiz _memberQuiz = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            string urlQueryString = "";
            if (!Page.IsPostBack)
            {
                //maintains sub nav in querystring, since "color code" sub nav always redirects here
                if (Request.RawUrl.IndexOf("?") >= 0)
                    urlQueryString = Request.RawUrl.Substring(Request.RawUrl.IndexOf("?"));

            }

            if (g.Member != null && ColorCodeHelper.IsColorCodeEnabled(g.Brand))
            {
                #region Temporarily allows to clear test check
                if (!String.IsNullOrEmpty(Request.QueryString["clear"]))
                {
                    ClearTest();
                }
                #endregion

                
                _memberQuiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);
                //this is hack, mingle migration will not include xml quiz attributes so we rely on completion
                //date to figure if quiz was taken.
                //which might be actually not such a bad idea for all sites, butoh well....
                bool showAnalysisForMingleMigrartion=false;;
                if( FrameworkGlobals.IsMingleSite(g.Brand.Site.Community.CommunityID))
                {
                
                    DateTime completedDate= g.Member.GetAttributeDate(g.Brand, WebConstants.ATTRIBUTE_NAME_COLORCODEQUIZCOMPLETEDATE);
                    if(completedDate > DateTime.MinValue)
                        showAnalysisForMingleMigrartion = true;


                
                }
                if (_memberQuiz.HasTakenQuiz() || showAnalysisForMingleMigrartion)
                {
                    if (_memberQuiz.IsQuizComplete() || showAnalysisForMingleMigrartion)
                    {
                        g.Transfer("/Applications/ColorCode/Analysis.aspx" + urlQueryString);
                    }
                    else
                    {
                        //commented out this transfer code due to remove QuizIntro page
                        // g.Transfer("/Applications/ColorCode/QuizIntro.aspx" + urlQueryString);
                    }
                }
            }
            else
            {
                //member is not logged in, send them on their merry way
                //if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles)
                //{
                //    //spark.com
                //    g.Transfer("http://static.spark.com/Components/Spark_LP/ColorCode/" + urlQueryString);
                //}
                //else
                //{
                //    g.Transfer("/Applications/Logon/Logon.aspx?DestinationURL=" + HttpUtility.UrlEncode(Request.RawUrl));
                //}

                g.Transfer("/Applications/Logon/Logon.aspx?DestinationURL=" + HttpUtility.UrlEncode(Request.RawUrl));
            }

        }

        protected override void  OnPreRender(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //color test link tracking (available on email, force, banner links that brought user here)
                ColorCodeHelper.HandleColorCodeTestLinkTracking(Request, g);
            }

            //show appropriate right content
            g.RightNavControl.ShowColorCodeProfileExamples();
            g.RightNavControl.HideGamChannels();

 	        base.OnPreRender(e);
        }

        /// <summary>
        /// Temporarily allows us to clear our test during dev
        /// </summary>
        protected void ClearTest()
        {
            //save quiz answers
            string quizXml = "null";
            g.Member.SetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_COLORCODEQUIZANSWERS, quizXml, Matchnet.Member.ValueObjects.TextStatusType.Auto);

            //save primary color
            g.Member.SetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_COLORCODEPRIMARYCOLOR, Color.none.ToString(), Matchnet.Member.ValueObjects.TextStatusType.Auto);

            //save secondary color
            g.Member.SetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_COLORCODESECONDARYCOLOR, Color.none.ToString(), Matchnet.Member.ValueObjects.TextStatusType.Auto);

            //save scores
            g.Member.SetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_COLORCODEBLUESCORE, "0", Matchnet.Member.ValueObjects.TextStatusType.Auto);
            g.Member.SetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_COLORCODEREDSCORE, "0", Matchnet.Member.ValueObjects.TextStatusType.Auto);
            g.Member.SetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_COLORCODEWHITESCORE, "0", Matchnet.Member.ValueObjects.TextStatusType.Auto);
            g.Member.SetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_COLORCODEYELLOWSCORE, "0", Matchnet.Member.ValueObjects.TextStatusType.Auto);

            Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(g.Member);
        }
    }


}