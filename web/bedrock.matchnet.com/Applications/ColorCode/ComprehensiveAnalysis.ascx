﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComprehensiveAnalysis.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysis" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div id="page-container" class="cc-content-main cc-content-bars-small">

<h2 class="tagline"><mn:Txt runat="server" ID="txtColorCode"  ResourceConstant="TXT_COLOR_CODE"/>
<small class="sans-serif"><mn:Txt runat="server" ID="txtUnderstandingYrself" ResourceConstant="TXT_UNDERSTANDING"/></small></h2>

<div id="cc-content">
<div class="editorial clearfix">
<h1><mn:Txt runat="server" ID="txtComprehensiveAnalysis" /></h1>

<div id="ccc-content">
<asp:PlaceHolder ID="phMenu" runat="server">
	<div id="ccc-menu">
		<ul id="ccc-nav">
    	<li><a href="ComprehensiveAnalysis.aspx?section=main"><mn:Txt runat="server" id="txtMain"  ResourceConstant="TXT_SECTION_MAIN" /></a></li>
		<li><a href="ComprehensiveAnalysis.aspx?section=introduction"><mn:Txt runat="server" id="txt1"  ResourceConstant="TXT_SECTION_INTRO" /></a></li>
		<li><a href="ComprehensiveAnalysis.aspx?section=basiccorecolors"><mn:Txt runat="server" id="txt2"  ResourceConstant="TXT_SECTION_OVERVIEW" /></a></li>
		<li><a href="ComprehensiveAnalysis.aspx?section=results"><mn:Txt runat="server" id="txt3"  ResourceConstant="TXT_SECTION_RESULTS" /></a></li>
		<li><a href="ComprehensiveAnalysis.aspx?section=applyingpersonality"><mn:Txt runat="server" id="txt4"  ResourceConstant="TXT_SECTION_APPL_PERSONALITY" /></a></li>
		<li><a href="ComprehensiveAnalysis.aspx?section=relatingtoothers"><mn:Txt runat="server" id="txt5"  ResourceConstant="TXT_SECTION_RELATING_OTHERS" /></a></li>
        <li>
        	<a href="#" class="skip">Skip Ahead</a>
            <div class="sub">
            	<ul id="skip-one">
					<li><a class="section" href="ComprehensiveAnalysis.aspx?section=introduction"><mn:Txt runat="server" id="txt6"  ResourceConstant="TXT_SECTION_INTRO_CAP" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=introduction"><mn:Txt runat="server" id="txt7"  ResourceConstant="TXT_SECTION_WHY_O_WHY" /></a></li>
					<li><a class="section" href="ComprehensiveAnalysis.aspx?section=basiccorecolors"><mn:Txt runat="server" id="txt8"  ResourceConstant="TXT_SECTION_OVERVIEW_CAP" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=basiccorecolors"><mn:Txt runat="server" id="txt9"  ResourceConstant="TXT_SECTION_4_COLORS" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=filters"><mn:Txt runat="server" id="txt10"  ResourceConstant="TXT_SECTION_FILTERS" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=demographics"><mn:Txt runat="server" id="txt11"  ResourceConstant="TXT_SECTION_DEMOGRAPHICS" /></a></li>
					<li><a class="section" href="ComprehensiveAnalysis.aspx?section=results"><mn:Txt runat="server" id="txt12"  ResourceConstant="TXT_SECTION_RESULTS" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=results"><mn:Txt runat="server" id="txt13"  ResourceConstant="TXT_SECTION_CONGRATULATIONS" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=traits"><mn:Txt runat="server" id="txt14"  ResourceConstant="TXT_SECTION_TRAITS" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=coremotive"><mn:Txt runat="server" id="txt15"  ResourceConstant="TXT_SECTION_CORE_MOTIVE" /></a></li>
				</ul>
				<ul id="skip-two">
					<li><a href="ComprehensiveAnalysis.aspx?section=personalstyle"><mn:Txt runat="server" id="txt16"  ResourceConstant="TXT_SECTION_PERSONAL_STYLE" /></a></li>
					<asp:PlaceHolder ID="phSecondaryStyle" runat="server"><li><a href="ComprehensiveAnalysis.aspx?section=personalsecondarystyle"><mn:Txt runat="server" id="txt17"  ResourceConstant="TXT_SECTION_PERSONAL_SEC_STYLE" /></a></li></asp:PlaceHolder> 
					<li><a href="ComprehensiveAnalysis.aspx?section=role"><mn:Txt runat="server" id="txtColorRole"   />&nbsp;<mn:Txt runat="server" id="txt39" ResourceConstant="TXT_ROLE"   /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=needs"><mn:Txt runat="server" id="txt18"  ResourceConstant="TXT_SECTION_NEEDS_WANTS" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=naturaleqcompetency"><mn:Txt runat="server" id="txt19"  ResourceConstant="TXT_SECTION_NAT_EQ" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=developmenttasks"><mn:Txt runat="server" id="txt20"  ResourceConstant="TXT_SECTION_DEV_TASKS" /></a></li>
					<li><a class="section" href="ComprehensiveAnalysis.aspx?section=applyingpersonality"><mn:Txt runat="server" id="txt21"  ResourceConstant="TXT_SECTION_APPL_PERSONALITIES" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=applyingpersonality"><mn:Txt runat="server" id="txt22"  ResourceConstant="TXT_SECTION_BEST_SELF" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=congruence"><mn:Txt runat="server" id="txt23"  ResourceConstant="TXT_SECTION_CONGRUENCE" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=getcoremotive"><mn:Txt runat="server" id="txt24"  ResourceConstant="TXT_SECTION_GET_MOTIVE" /></a></li>
				</ul>
				<ul id="skip-three">
					<li><a href="ComprehensiveAnalysis.aspx?section=gifts"><mn:Txt runat="server" id="txt25"  ResourceConstant="TXT_SECTION_GIFTS" /></a></li>
					<asp:PlaceHolder ID="phLeverageSecondary" runat="server"><li><a href="ComprehensiveAnalysis.aspx?section=leveragingsecondary"><mn:Txt runat="server" id="txt26"  ResourceConstant="TXT_SECTION_LEV_SECONDARY" /></a></li></asp:PlaceHolder>
					<li><a href="ComprehensiveAnalysis.aspx?section=buildingcharacter"><mn:Txt runat="server" id="txt27"  ResourceConstant="TXT_SECTION_CHARACTER" /></a></li>
					<li><a class="section" href="ComprehensiveAnalysis.aspx?section=relatingtoothers"><mn:Txt runat="server" id="txt28"  ResourceConstant="TXT_SECTION_RELATING" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=relatingtoothers"><mn:Txt runat="server" id="txt29"  ResourceConstant="TXT_SECTION_RESP_SUCCESS" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=relationtoothercolors"><mn:Txt runat="server" id="txt30"  ResourceConstant="TXT_SECTION_REL_OTHER_COLORS" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=successfulrelationship"><mn:Txt runat="server" id="txt31"  ResourceConstant="TXT_SECTION_RELATIONSHIP" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=relationshiptips"><mn:Txt runat="server" id="txt32"  ResourceConstant="TXT_SECTION_RELATIONSHIP_TIPS" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=evaluatingrelationship"><mn:Txt runat="server" id="txt33"  ResourceConstant="TXT_SECTION_EVAL_RELATIONSHIP" /></a></li>
					<li><a href="ComprehensiveAnalysis.aspx?section=conclusion"><mn:Txt runat="server" id="txt34"  ResourceConstant="TXT_SECTION_CONCLUSIONS" /></a></li>
				</ul>
            </div>
        </li>
        <li>
        	<a href="#" class="print"><mn:Txt runat="server" id="txt35"  ResourceConstant="TXT_SECTION_PRINT" /></a>
            <div class="sub">
                <ul>
                    <li><a href="<asp:Literal runat=server id=litPrintPage/>"><mn:Txt runat="server" id="txt36"  ResourceConstant="TXT_SECTION_PRINT_PAGE" /></a></li>
                    <li><a href="<asp:Literal runat=server id=litPrintSection/>"><mn:Txt runat="server" id="txt37"  ResourceConstant="TXT_SECTION_PRINT_SECTION" /></a></li>
                    <li><a href="<asp:Literal runat=server id=litPrintWhole/>"><mn:Txt runat="server" id="txt38"  ResourceConstant="TXT_SECTION_PRINT_DOC" /></a></li>
                </ul>
            </div>
        </li>
    </ul><%-- end #topnav --%>
	</div><%-- end #ccc-menu --%>
	</asp:PlaceHolder>
	<div id="ccc-body">
	    <asp:PlaceHolder ID="phContent" runat="server" />
	    <p id="cc-copy"><mn:Txt ID="txtCopyrights"  runat="server"/></p>
	</div><%-- end #ccc-body --%>
	
</div><%-- end #ccc-content --%>
</div><%-- end .editotial --%>
</div><%-- end #cc-content --%>
</div><%-- end #page-container --%>
