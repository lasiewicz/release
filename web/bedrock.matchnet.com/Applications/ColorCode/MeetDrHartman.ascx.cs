﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.ColorCode
{
    public partial class MeetDrHartman : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ColorCodeHelper.IsColorCodeEnabled(g.Brand))
            {

            }
            else
            {
                g.Transfer("/Applications/ColorCode/Landing.aspx");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //show appropriate right content
            g.RightNavControl.ShowColorCodeAbout();
            g.RightNavControl.HideGamChannels();

            base.OnPreRender(e);
        }
    }
}