﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Question.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Question" %>
<%@ Register TagPrefix="qa" TagName="QuestionModule" Src="~/Applications/QuestionAnswer/Controls/QuestionModule.ascx" %>
<%@ Register TagPrefix="qa" TagName="AnswerModule" Src="~/Applications/QuestionAnswer/Controls/AnswerModule.ascx" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="qa" TagName="SubmitQuestionClientModule" Src="~/Applications/QuestionAnswer/Controls/SubmitQuestionClientModule.ascx" %>

<div id="qanda" class="qanda border-box clearfix">
    <h2 class="component-header"><mn:txt id="mntxt7546" runat="server" resourceconstant="TXT_QUESTION_HEADING" expandimagetokens="true" /></h2>
    <!--Question module-->
    <qa:QuestionModule ID="QuestionModule1" runat="server" />
    <!--Answer module-->
    <qa:AnswerModule ID="AnswerModule1" runat="server" />
    <asp:HiddenField ID="hidQuestionID" runat="server" />
    <qa:SubmitQuestionClientModule ID="sendQuestionClientModule" runat="server" />
</div>