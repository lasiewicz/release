﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Caching;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.Web.Applications.MemberLike;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;

namespace Matchnet.Web.Applications.QuestionAnswer
{
    public class QuestionAnswerHelper
    {
        #region Static Methods
        public static bool IsQuestionAnswerEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_QUESTIONANSWER", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        public static bool IsQuestionAnswerDoubleEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_QUESTIONANSWER_DOUBLE", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }
        
        public static bool IsMingleEssayEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_MINGLE_ESSAYS", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        public static bool IsQuestionAnswerCommentEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_QUESTIONANSWER_COMMENT", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        public static bool IsQuestionAnswerSortEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_QUESTIONANSWER_SORT", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        public static bool IsQuestionAnswerMemberQuestionsEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_QUESTIONANSWER_MEMBER_QUESTIONS", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        public static bool IsQuestionAnswerNextQuestionEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_QANDA_NEXT_QUESTION", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        public static bool IsQuestionAnswerClientPaginationEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_QANDA_CLIENT_PAGINATION", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }
        
        public static int ConvertToInt(Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question)
        {
            return question.QuestionID;
        }

        public static Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question GetActiveQuestion(Member.ServiceAdapters.Member member, ContextGlobal g, bool includeAnsweredQuestion)
        {
            Brand brand = g.Brand;
            Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question = null;

            List<int> questionList = QuestionAnswerSA.Instance.GetActiveQuestionIDs(brand.Site.SiteID).QuestionIDs;
            int questionID = Constants.NULL_INT;
            if (questionList.Count > 0)
            {
                if (questionList.Count == 1)
                {
                    questionID = questionList[0];
                }
                else
                {
                    List<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> memberQuestions=QuestionAnswerSA.Instance.GetMemberQuestions(member.MemberID, brand.Site.SiteID).Questions;
                    List<int> memberQuestionIds = memberQuestions.ConvertAll<int>(new Converter<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question, int>(ConvertToInt));
                    //filter out answered questions
                    List<int> questionIdPool = new List<int>();
                    foreach (int questID in questionList)
                    {
                        if (!memberQuestionIds.Contains(questID))
                        {
                            questionIdPool.Add(questID);
                        }                        
                    }
                    //if member answered all active questions, don't filter
                    if (questionIdPool.Count == 0 && includeAnsweredQuestion)
                    {
                        questionIdPool = questionList;
                    }

                    if (questionIdPool.Count > 0)
                    {
                        //questions are randomly favored by weight
                        int[] qids = questionIdPool.ToArray();
                        Hashtable questionWeights = GetQuestionWeights(g, questionList);
                        foreach (int qid in qids)
                        {
                            int weight = ((int)questionWeights[qid] > 0) ? (int)questionWeights[qid] : 1;
                            //add the question id to pool as many times as it is weighted
                            //higher weighted questions have more of a chance being randomly chosen
                            for (int i = 0; i < weight; i++)
                            {
                                questionIdPool.Add(qid);
                            }
                        }

                        //randomly pick a question                    
                        int r = new Random().Next(questionIdPool.Count);
                        questionID = questionIdPool[r];
                    }
                }
            }

            if (questionID > 0)
            {
                question = QuestionAnswerSA.Instance.GetQuestion(questionID);
            }
            return question;
        }

        public static Hashtable GetQuestionWeights(ContextGlobal g, List<int> questionList)
        {
            Hashtable questionWeights = g.Cache.Get("questionWeights") as Hashtable;
            if (null == questionWeights) questionWeights = new Hashtable();
            bool addToCache = false;

            QuestionList activeQuestionList = Matchnet.PremiumServiceSearch.ServiceAdapters.QuestionAnswerSA.Instance.GetActiveQuestionList(g.Brand.Site.SiteID);
            foreach (int questID in questionList)
            {
                if (!questionWeights.ContainsKey(questID))
                {
                    Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question aQuestion = null;
                    if (activeQuestionList != null)
                    {
                        foreach (Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question q in activeQuestionList.Questions)
                        {
                            if (q.QuestionID == questID)
                            {
                                aQuestion = q;
                                break;
                            }
                        }
                    }

                    questionWeights[questID] = (aQuestion != null) ? aQuestion.Weight : 1;
                    addToCache = true;
                }
            }

            if (addToCache)
            {
                g.Cache.Remove("questionWeights");
                g.Cache.Add("questionWeights", questionWeights, null, DateTime.Now.Add((new TimeSpan(0, 30, 0))), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
            }
            return questionWeights;
        }

        public static Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Answer GetMemberAnswer(int memberID, Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question)
        {
            Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Answer answer = null;

            if (question.Answers != null)
            {
                foreach (Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Answer a in question.Answers)
                {
                    if (a.MemberID == memberID)
                    {
                        answer = a;
                        break;
                    }
                }
            }

            return answer;
        }

        public static Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question[] GetMemberQuestions(int memberID, int siteID, bool approvedOnly)
        {
            return GetMemberQuestions(memberID, siteID, approvedOnly, null);
        }

        public static Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question[] GetMemberQuestions(int memberID, int siteID, bool approvedOnly, IComparer<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> comparer)
        {
            List<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> questions;
            questions = QuestionAnswerSA.Instance.GetMemberQuestions(memberID, siteID).Questions;
            if (approvedOnly)
            {
                List<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> questionsApprovedOnly = new List<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question>();
                foreach (Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question q in questions)
                {
                    if (q.Answers.Count > 0 && q.Answers[0].AnswerStatus != QuestionAnswerEnums.AnswerStatusType.Pending && q.Answers[0].AnswerStatus != QuestionAnswerEnums.AnswerStatusType.Rejected)
                    {
                        questionsApprovedOnly.Add(q);
                    }
                }
                if (null != comparer)
                {
                    questionsApprovedOnly.Sort(comparer);
                }
                return questionsApprovedOnly.ToArray();
            }
            else
            {
                if (null != comparer)
                {
                    questions.Sort(comparer);
                }
                return questions.ToArray();
            }
        }

        public static Answer[] GetApprovedAnswers(Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question, int MemberIDExclude, ContextGlobal g)
        {
            return GetApprovedAnswers(question, MemberIDExclude, g, null);
        }

        public static Answer[] GetApprovedAnswers(Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question, int MemberIDExclude, ContextGlobal g, IComparer<Answer> comparer)
        {
            List<Answer> ApprovedAnswers = new List<Answer>();
            if (question != null && question.Answers != null)
            {
                foreach (Answer answer in question.Answers)
                {
                    if (answer.AnswerStatus != QuestionAnswerEnums.AnswerStatusType.Pending
                        && answer.AnswerStatus != QuestionAnswerEnums.AnswerStatusType.Rejected)
                    {
                        if (answer.MemberID != MemberIDExclude && !IsAnswerSuppressed(answer, g))
                        {
                            ApprovedAnswers.Add(answer);
                        }
                    }
                }
            }

            if (null != comparer)
            {
                ApprovedAnswers.Sort(comparer);
            }

            return ApprovedAnswers.ToArray();
        }

        public static Answer[] GetApprovedAnswers(List<Answer> answers, int MemberIDExclude, ContextGlobal g, IComparer<Answer> comparer)
        {
            List<Answer> ApprovedAnswers = new List<Answer>();
           
                foreach (Answer answer in answers)
                {
                    if (answer.AnswerStatus != QuestionAnswerEnums.AnswerStatusType.Pending
                        && answer.AnswerStatus != QuestionAnswerEnums.AnswerStatusType.Rejected)
                    {
                        if (answer.MemberID != MemberIDExclude && !IsAnswerSuppressed(answer, g))
                        {
                            ApprovedAnswers.Add(answer);
                        }
                    }
                }
           

            if (null != comparer)
            {
                ApprovedAnswers.Sort(comparer);
            }

            return ApprovedAnswers.ToArray();
        }

        public static bool IsAnswerSuppressed(Answer answer, ContextGlobal g)
        {
            bool suppress = false;
            try
            {
                if (IsPrePopulateAnswersForSortFilterEnabled(g))
                {
                    int globalStatusMask = answer.GlobalStatusMask;
                    bool adminSuspend = (globalStatusMask & Matchnet.Lib.ConstantsTemp.ADMIN_SUSPEND) == Matchnet.Lib.ConstantsTemp.ADMIN_SUSPEND;
                    bool selfSuspend = (answer.SelfSuspendedFlag == 1);
                    bool hasPhoto = answer.HasApprovedPhotos;

                    suppress = adminSuspend || selfSuspend || !hasPhoto;
                }
                else
                {
                    Member.ServiceAdapters.Member answerMember = Member.ServiceAdapters.MemberSA.Instance.GetMember(answer.MemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                    int globalStatusMask = answerMember.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK);
                    bool adminSuspend = Matchnet.Conversion.CInt(globalStatusMask & Matchnet.Lib.ConstantsTemp.ADMIN_SUSPEND) == Matchnet.Lib.ConstantsTemp.ADMIN_SUSPEND;
                    bool selfSuspend = (answerMember.GetAttributeInt(g.TargetBrand, "SelfSuspendedFlag", 0) == 1);
                    bool hasPhoto = answerMember.HasApprovedPhoto(g.Brand.Site.Community.CommunityID);

                    suppress = adminSuspend || selfSuspend || !hasPhoto;
                }
            }
            catch (Exception ex)
            {
                if (g != null)
                {
                g.ProcessException(ex);
                }
                suppress = false;
            }

            return suppress;
        }

        public static int GetMemberApprovedAnswersCount(int memberID, int siteID)
        {
            int approvedCount = 0;
            Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question[] questions = GetMemberQuestions(memberID, siteID, true);
            if (questions != null)
            {
                approvedCount = questions.Length;
            }

            return approvedCount;
        }

        public static bool IsQuestionActive(Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question)
        {
            bool isActive = false;
            if (question != null)
            {
                if (question.StartDate <= DateTime.Now && question.EndDate >= DateTime.Now
                    && question.Active)
                {
                    isActive = true;
                }
            }

            return isActive;
        }

        public static QuestionViewObject AnswerBlockedQuestion(int questionId, int memberId, int answerMemberId, string answerText, ContextGlobal g, Hashtable additionalParams)
        {
            //member id for answer should never be 0
            if (answerMemberId <= 0) return null;
            Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question theQuestion = QuestionAnswerSA.Instance.GetQuestion(questionId);
            QuestionAnswerEnums.QuestionType questionType = (QuestionAnswerEnums.QuestionType)Enum.Parse(typeof(QuestionAnswerEnums.QuestionType), theQuestion.QuestionTypeID.ToString());
            //sometimes the memberanswer doesn't come back so we create one here just in case
            Answer memberAnswer = new Answer();
            memberAnswer.MemberID = answerMemberId;
            memberAnswer.QuestionID = questionId;
            memberAnswer.AnswerID = 1;
            memberAnswer.UpdateDate = DateTime.Now;

            //answer question
            switch (questionType)
            {
                case QuestionAnswerEnums.QuestionType.FreeText:
                    string decodedAnswerText = FrameworkGlobals.DecodeHexEntities(answerText);
                    QuestionAnswerSA.Instance.AddAnswer(answerMemberId, g.Brand.Site.SiteID, questionId, decodedAnswerText);
                    memberAnswer.AnswerValue = decodedAnswerText;
                    memberAnswer.AnswerValuePending = decodedAnswerText;
                    memberAnswer.AnswerStatus = QuestionAnswerEnums.AnswerStatusType.Pending;
                    break;
                case QuestionAnswerEnums.QuestionType.MultipleChoice:
                    bool successfulSave = false;
                    foreach (StockAnswer stockAnswer in theQuestion.StockAnswers)
                    {
                        if (stockAnswer.StockAnswerID.ToString() == answerText)
                        {
                            QuestionAnswerSA.Instance.AddAnswer(answerMemberId, g.Brand.Site.SiteID, questionId, stockAnswer.AnswerValue);
                            successfulSave = true;
                            memberAnswer.AnswerValue = stockAnswer.AnswerValue;
                            memberAnswer.AnswerValuePending = stockAnswer.AnswerValue;
                            memberAnswer.AnswerStatus = QuestionAnswerEnums.AnswerStatusType.Approved;
                            break;
                        }
                    }
                    if (!successfulSave) return null;
                    break;
            }
            //clear logged in member's qandas from cache
            QuestionAnswerHelper.ResetLoggedInMemberQuestions(g);
            //populate view object for json communication
            additionalParams["oneAnswerMemberId"] = memberId;
            QuestionViewObject questionViewObject = GetQuestionViewObject(questionId, answerMemberId, g, additionalParams, true);
            //if memberanswer wasn't retrieved, use the one we kept in memory
            if (null == questionViewObject.MemberAnswer)
            {
                questionViewObject.MemberAnswer = GetAnswerViewObject(memberAnswer, g, additionalParams);
            }
            //send user notification
            if (UserNotificationFactory.IsUserNotificationsEnabled(g))
            {
                try
                {
                    Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(answerMemberId);
                    int totalFavorites = list.GetCount(HotListCategory.WhoAddedYouToTheirFavorites, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                    Int32 temp = 0;
                    System.Collections.ArrayList whoAddedYou = list.GetListMembers(HotListCategory.WhoAddedYouToTheirFavorites, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, 1, 500, out temp);

                    foreach (Int32 whoAddedYouId in whoAddedYou)
                    {
                        UserNotificationParams unParams = UserNotificationParams.GetParamsObject(whoAddedYouId, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                        UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new FavoriteAnsweredQandAUserNotification(), whoAddedYouId, answerMemberId, g);
                        if (notification.IsActivated())
                        {
                            UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                        }
                    }
                }
                catch (Exception e)
                {
                    g.ProcessException(e);
                }
            }
            return questionViewObject;
        }

        public static QuestionViewObject GetQuestionViewObject(int questionId, int memberId, ContextGlobal g)
        {
            return GetQuestionViewObject(questionId, memberId, g, null, true);
        }

        public static QuestionViewObject GetQuestionViewObject(int questionId, int memberId, ContextGlobal g, Hashtable additionalParams, bool includeAnsweredRandomQuestion)
        {
            QuestionViewObject questionViewObject = null;
            try
            {
                Brand brand = g.Brand;
                Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question = null;
                Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
                if (questionId > 0)
                {
                    question = QuestionAnswerSA.Instance.GetQuestion(questionId);
                }

                if (question == null)
                {
                    //get default random active question
                    question = GetActiveQuestion(member, g, includeAnsweredRandomQuestion);
                }

                if (question != null)
                {
                    questionViewObject = QuestionViewObject.Parse(question);
                    Answer memberAnswer = GetMemberAnswer(memberId, question);
                    Answer answer = memberAnswer;
                    if (null != answer)
                    {
                        AnswerViewObject answerViewObject = GetAnswerViewObject(answer, g, additionalParams);
                        if (null != answerViewObject)
                        {
                            questionViewObject.MemberAnswer = answerViewObject;
                        }
                    }

                    string sortBy = (null != additionalParams && additionalParams.ContainsKey("sortBy")) ? (string)additionalParams["sortBy"] : null;
                    bool reverse = (null != additionalParams && additionalParams.ContainsKey("reverse")) ? (bool)additionalParams["reverse"] : false;

                    //if oneAnswerMemberId is specified we only want to get this member's answer in the answer array
                    int oneAnswerMemberId = (additionalParams.ContainsKey("oneAnswerMemberId")) ? (int)additionalParams["oneAnswerMemberId"] : 0;
                    Answer oneAnswerForMemberId = null;
                    if (oneAnswerMemberId > 0)
                    {
                        oneAnswerForMemberId = GetMemberAnswer(oneAnswerMemberId, question);
                    }

                    IComparer<Answer> answerComparer = GetAnswerComparer(sortBy, reverse, g);
                    Answer[] answers = (null != oneAnswerForMemberId) ? new Answer[] { oneAnswerForMemberId } : GetApprovedAnswers(question, memberId, g, answerComparer);

                    if (answerComparer is YourMatchesAnswerComparer)
                    {
                        questionViewObject.HasYourMatchesAnswers = ((YourMatchesAnswerComparer)answerComparer).HasYourMatches.ToString().ToLower();
                    }

                    int pageSize = (null != additionalParams && additionalParams.ContainsKey("pageSize")) ? (int)additionalParams["pageSize"] : 9;
                    if (pageSize > answers.Length)
                    {
                        pageSize = answers.Length;
                    }

                    if (null == additionalParams) additionalParams = new Hashtable();
                    additionalParams["loggedInMemberId"] = memberId;
                    for (int i = 0; i < pageSize; i++)
                    {
                        Answer a = answers[i];
                        AnswerViewObject answerViewObject = GetAnswerViewObject(a, g, additionalParams);
                        if (null != a)
                        {
                            if (null == questionViewObject.Answers) questionViewObject.Answers = new List<AnswerViewObject>();
                            questionViewObject.Answers.Add(answerViewObject);
                        }
                    }
                    questionViewObject.TotalAnswerNum = answers.Length;

                    if (!string.IsNullOrEmpty(sortBy) && IsQuestionAnswerSortEnabled(g.Brand))
                    {
                        string sortCriteria = sortBy + "|" + reverse.ToString().ToLower();
                        string lastSort = GetLastSortForMember(brand, member);
                        if (!sortCriteria.Equals(lastSort))
                        {
                            //stored proc sorts this way so don't save last sort in db.
                            if ("date|false".Equals(lastSort))
                            {
                                member.SetAttributeText(brand, "LastQandaSort", null, Matchnet.Member.ValueObjects.TextStatusType.None);
                            }
                            else
                            {
                                member.SetAttributeText(brand, "LastQandaSort", sortCriteria, Matchnet.Member.ValueObjects.TextStatusType.None);
                            }
                            Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return questionViewObject;
        }

        public static AnswerViewObject GetAnswerViewObject(Answer answer, ContextGlobal g, Hashtable additionalParams)
        {
            if (null == answer) return null;

            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            Brand brand = g.Brand;
            int memberId = answer.MemberID;
            AnswerViewObject answerViewObject = AnswerViewObject.Parse(answer);
            System.Web.UI.Page p = new System.Web.UI.Page();
            Matchnet.Web.Framework.Ui.BasicElements.LastLoginDate lastTimeAction = p.LoadControl("/Framework/Ui/BasicElements/LastLoginDate.ascx") as Matchnet.Web.Framework.Ui.BasicElements.LastLoginDate;
            lastTimeAction.LastLoginDateValue = answer.UpdateDate;
            answerViewObject.TimeStamp = lastTimeAction.BuildLastLoginDate();
            Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            WebServices.Member.MemberData memberData = WebServices.Member.MemberData.Parse(member);
            memberData.MemberID = member.MemberID;

            memberData.DisplayLocation = ProfileDisplayHelper.GetRegionDisplay(member, brand);
            memberData.UserName = member.GetUserName(brand);
            memberData.UserNameEllipsed = FrameworkGlobals.Ellipsis(memberData.UserName, 12);
            DateTime birthDate = member.GetAttributeDate(brand, "Birthdate");
            memberData.Age = FrameworkGlobals.GetAge(birthDate);
            memberData.Seeking = ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(member, g);
            memberData.LargePhotos = new List<string>();
            memberData.ThumbPhotos = new List<string>();

            Matchnet.Member.ValueObjects.Photos.Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, member, brand);

            int ordinal = (null != additionalParams && additionalParams.ContainsKey("ordinal")) ? Convert.ToInt32(additionalParams["ordinal"]) : 0;
            BreadCrumbHelper.EntryPoint MyEntryPoint = BreadCrumbHelper.EntryPoint.Unknown;
            if(null != additionalParams && additionalParams.ContainsKey("entryPoint")) {
                MyEntryPoint = (BreadCrumbHelper.EntryPoint)additionalParams["entryPoint"];
            }
            string pageUrl = (null != additionalParams && additionalParams.ContainsKey("pageUrl")) ? (string)additionalParams["pageUrl"] : string.Empty;
            if (null != additionalParams && additionalParams.ContainsKey("isHighLighted"))
            {
                memberData.ViewProfileURL= BreadCrumbHelper.MakeViewProfileLink(MyEntryPoint, member.MemberID, ordinal, null, Constants.NULL_INT, false, (int)BreadCrumbHelper.PremiumEntryPoint.Highlight, pageUrl, Constants.NULL_INT);
            }
            else
            {
                memberData.ViewProfileURL = BreadCrumbHelper.MakeViewProfileLink(MyEntryPoint, member.MemberID, ordinal, null, Constants.NULL_INT, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium, pageUrl, Constants.NULL_INT);
            }

            if (null != additionalParams && additionalParams.ContainsKey("trackingParam"))
            {
                memberData.ViewProfileURL = BreadCrumbHelper.AppendParamToProfileLink(memberData.ViewProfileURL, additionalParams["trackingParam"].ToString());
            }

            if (ColorCodeHelper.IsColorCodeEnabled(brand) && ColorCodeHelper.HasMemberCompletedQuiz(member, brand) && !ColorCodeHelper.IsMemberColorCodeHidden(member, brand))
            {

                Color color = ColorCodeHelper.GetPrimaryColor(member, brand);
                memberData.ColorCode = color.ToString().ToLower();

                memberData.ColorCodeText = ColorCodeHelper.GetFormattedColorText(color).ToLower();
            }

            bool isMale = FrameworkGlobals.IsMaleGender(member, g);
            if (photo != null && photo.IsApproved)
            {
                if (MemberPhotoDisplayManager.Instance.IsPrivatePhoto(photo, brand) || !MemberPhotoDisplayManager.Instance.IsPhotosAvailableToGuests(g.Member, member, brand))
                {
                    memberData.ThumbPhotos.Add(MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, isMale));
                }
                else if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Thumbnail, g.Brand))
                {
                    string imagePath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, member, g.Brand,
                                                                                      photo, PhotoType.Thumbnail,
                                                                                      PrivatePhotoImageType.TinyThumb,
                                                                                      NoPhotoImageType.TinyThumbV2);

                    memberData.ThumbPhotos.Add(imagePath);
                }
            }
            else
            {
                memberData.ThumbPhotos.Add(MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, isMale));
            }

            if (MemberLikeHelper.Instance.IsMemberLikeEnabled(g.Brand))
            {
                int loggedInMemberId = (null != additionalParams && additionalParams.ContainsKey("loggedInMemberId")) ? Convert.ToInt32(additionalParams["loggedInMemberId"]) : 0;
                MemberLikeParams likeParams=new MemberLikeParams();
                likeParams.MemberId = loggedInMemberId;
                likeParams.SiteId = g.Brand.Site.SiteID;
                List<MemberLikeObject> memberLikes=MemberLikeHelper.Instance.GetLikesByObjectIdAndType(likeParams, answer.AnswerID, Convert.ToInt32(LikeObjectTypes.Answer));
                if (null != memberLikes)
                {
                    memberLikes = memberLikes.FindAll(delegate(MemberLikeObject mlo)
                    {
                        Matchnet.Member.ServiceAdapters.Member _member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(mlo.MemberId, MemberLoadFlags.None);
                        bool b = !MemberLikeHelper.Instance.IsMemberSuppressed(_member, g);
                        return b;
                    });
                    foreach (MemberLikeObject likeObj in memberLikes)
                    {
                        if (null == answerViewObject.MemberLikes) answerViewObject.MemberLikes = new List<MemberLikeViewObject>();
                        answerViewObject.MemberLikes.Add(MemberLikeViewObject.Parse(likeObj));
                    }
                    bool hasLike = false;
                    MemberLikeObject match = memberLikes.Find(delegate(MemberLikeObject obj)
                    {
                        bool b = (obj.MemberId == loggedInMemberId);
                        return b;
                    });
                    hasLike = (null != match);
                    answerViewObject.MemberLikeText = (hasLike) ? "unlike" : "like";
                }
            }

            answerViewObject.Member = memberData;

            return answerViewObject;
        }

        public static IComparer<Answer> GetAnswerComparer(string sortBy, bool reverse, ContextGlobal g)
        {
            if (string.IsNullOrEmpty(sortBy) || !IsQuestionAnswerSortEnabled(g.Brand)) return null;
            switch (sortBy)
            {
                case "gender":
                    return new GenderAnswerComparator(g, reverse);
                case "date":
                    return new UpdateDateAnswerComparer(reverse);
                case "matches":
                    return new YourMatchesAnswerComparer(g, reverse);
                default:
                    return null;
            }
        }

        public static string GetLastSortForMember(Brand brand, Matchnet.Member.ServiceAdapters.Member member)
        {
            string lastSort = string.Empty;
            if (null != member && member.MemberID > 0)
            {
                lastSort = member.GetAttributeText(brand, "LastQandASort");
            }
            return lastSort;
        }

        public static IComparer<Answer> GetAnswerComparerFromCriteria(string sortCriteria, string lastSort, ContextGlobal g)
        {
            if (string.IsNullOrEmpty(sortCriteria)) sortCriteria = lastSort;
            try
            {
                if (null != g.Member && g.Member.MemberID > 0 && !string.IsNullOrEmpty(sortCriteria) && !sortCriteria.Equals(lastSort))
                {
                    g.Member.SetAttributeText(g.Brand, "LastQandASort", sortCriteria, Matchnet.Member.ValueObjects.TextStatusType.None);
                    Member.ServiceAdapters.MemberSA.Instance.SaveMember(g.Member);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            if (string.IsNullOrEmpty(sortCriteria))
            {
                return null;
            }
            else
            {
                string[] strings = sortCriteria.Split(new char[] { '|' });
                string sortBy = (null != strings && strings.Length > 0) ? strings[0] : null;
                bool reverseSort = (null != strings && strings.Length > 1) ? Boolean.TrueString.ToLower().Equals(strings[1]) : false;
                return QuestionAnswerHelper.GetAnswerComparer(sortBy, reverseSort, g);
            }
        }

        public static Hashtable GetLoggedInMemberQuestions(ContextGlobal g)
        {
            Hashtable myQuestions = null;
            if (g.Member != null && g.Member.MemberID > 0)
            {
                //get question and answers for logged in member                
                myQuestions = g.Cache["myQuestions-" + g.Member.MemberID] as Hashtable;
                if (null == myQuestions)
                {
                    Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question[] memQuestions = QuestionAnswerHelper.GetMemberQuestions(g.Member.MemberID, g.Brand.Site.SiteID, false, null);
                    foreach (Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question q in memQuestions)
                    {
                        if (null == myQuestions) myQuestions = new Hashtable();
                        myQuestions[q.QuestionID] = q.Answers[0];
                    }
                    if (null != myQuestions)
                    {
                        g.Cache.Add("myQuestions-" + g.Member.MemberID, myQuestions, null, DateTime.Now.Add(new TimeSpan(0, 5, 0)), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                    }
                }
            }
            return myQuestions;
        }

        public static void ResetLoggedInMemberQuestions(ContextGlobal g)
        {
            if (g.Member != null && g.Member.MemberID > 0)
            {
                g.Cache.Remove("myQuestions-" + g.Member.MemberID);
            }
        }

        public static bool IsPrePopulateAnswersForSortFilterEnabled(ContextGlobal g)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_QA_PREPROPULATE_FOR_SORT_FILTER", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        public static int GetAnswerListDaysBack(int siteID)
        {
            int days = 10;
            try
            {
                days = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("QUESTION_ANSWER_RECENT_DAYS", Constants.NULL_INT, siteID, Constants.NULL_INT));
            }
            catch (Exception ex)
            {
                //setting missing
                days = 10;
            }

            return days;

        }
        #endregion
    }

    #region ViewObjects (data containers for json)
    public class QuestionViewObject
    {
        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public int TotalAnswerNum { get; set; }
        public int QuestionTypeId { get; set; }
        public List<AnswerViewObject> Answers { get; set; }
        public List<StockAnswerViewObject> StockAnswers { get; set; }
        public AnswerViewObject MemberAnswer { get; set; }
        public string HasYourMatchesAnswers { get; set; }

        public static QuestionViewObject Parse(Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question)
        {
            QuestionViewObject questionViewObject=null;
            if (question != null)
            {
                questionViewObject= new QuestionViewObject();
                questionViewObject.QuestionId = question.QuestionID;
                questionViewObject.QuestionText = question.Text;
                questionViewObject.QuestionTypeId = question.QuestionTypeID;
                if (null != question.StockAnswers && question.StockAnswers.Count > 0)
                {
                    if (null == questionViewObject.StockAnswers) questionViewObject.StockAnswers = new List<StockAnswerViewObject>();
                    foreach (StockAnswer stockAnswer in question.StockAnswers)
                    {
                        questionViewObject.StockAnswers.Add(StockAnswerViewObject.Parse(stockAnswer));
                    }
                }
            }
            return questionViewObject;
        }
    }

    public class AnswerViewObject
    {
        public int AnswerId { get; set; }
        public string Text { get; set; }
        public string TimeStamp { get; set; }
        public string Status { get; set;}
        public WebServices.Member.MemberData Member { get; set; }
        public List<MemberLikeViewObject> MemberLikes { get; set; }
        public string MemberLikeText { get; set; }


        public static AnswerViewObject Parse(Answer answer) 
        {
            AnswerViewObject answerViewObject=null;
            if(null != answer) {
                answerViewObject = new AnswerViewObject();
                answerViewObject.AnswerId=answer.AnswerID;
                answerViewObject.Status=answer.AnswerStatus.ToString();
                answerViewObject.Text = answer.AnswerValue.Replace(Environment.NewLine, "<br />");
            }
            return answerViewObject;
        }
    }

    public class MemberLikeViewObject
    {
        public int MemberId { get; set; }
        public int SiteId { get; set; }
        public DateTime InsertDate { get; set; }
        public int LikeObjectId { get; set; }
        public int LikeObjectTypeId { get; set; }

        public static MemberLikeViewObject Parse(MemberLikeObject likeObject)
        {
            MemberLikeViewObject likeViewObject = null;
            if (null != likeObject)
            {
                likeViewObject = new MemberLikeViewObject();
                likeViewObject.MemberId = likeObject.MemberId;
                likeViewObject.LikeObjectId = likeObject.LikeObjectId;
                likeViewObject.LikeObjectTypeId = likeObject.LikeObjectTypeId;
                likeViewObject.SiteId = likeObject.SiteId;
                likeViewObject.InsertDate = likeObject.InsertDate;
            }
            return likeViewObject;
        }
    }

    public class StockAnswerViewObject
    {
        public int StockAnswerId { get; set; }
        public int SortOrder { get; set; }
        public string Text { get; set; }
        public DateTime InsertDate { get; set; }

        public static StockAnswerViewObject Parse(StockAnswer stockAnswer)
        {
            StockAnswerViewObject stockAnswerViewObject = null;
            if (null != stockAnswer)
            {
                stockAnswerViewObject = new StockAnswerViewObject();
                stockAnswerViewObject.StockAnswerId = stockAnswer.StockAnswerID;
                stockAnswerViewObject.SortOrder= stockAnswer.SortOrder;
                stockAnswerViewObject.Text = stockAnswer.AnswerValue.Replace(Environment.NewLine, "<br />");
                stockAnswerViewObject.InsertDate = stockAnswer.InsertDate;
            }
            return stockAnswerViewObject;
        }
    }
    #endregion

    #region Comparers
    public class UpdateDateQuestionComparer : IComparer<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question>
    {
        private bool _reverse = false;

        public UpdateDateQuestionComparer(){}

        public UpdateDateQuestionComparer(bool reverse)
        {
            this._reverse = reverse;
        }
             
        #region IComparer<Question> Members
        public int Compare(Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question x, Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question y)
        {
            DateTime dt1 = (null != x && null != x.Answers && x.Answers.Count > 0) ? x.Answers[0].UpdateDate : DateTime.MinValue;
            DateTime dt2 = (null != y && null != y.Answers && y.Answers.Count > 0) ? y.Answers[0].UpdateDate : DateTime.MinValue;
            return (_reverse) ? dt1.CompareTo(dt2) : dt2.CompareTo(dt1);
        }
        #endregion
    }

    public class GenderQuestionComparator : IComparer<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question>
    {
        #region IComparer<Answer> Members
        private ContextGlobal _g = null;
        private bool _reverse = false;

        public GenderQuestionComparator(ContextGlobal g, bool reverse)
        {
            this._g = g;
            this._reverse = reverse;
        }

        //TODO: DETERMINE PERFORMACE IMPACT FOR LOOKING UP ALL OF THESE MEMBERS!!!!!
        public int Compare(Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question x, Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question y)
        {
            if (QuestionAnswerHelper.IsPrePopulateAnswersForSortFilterEnabled(_g))
            {
                //NOTE: GenderMask is populated in answer dynamically when question/answer is pulled in MT
                int genderMask1 = (null != x && null != x.Answers && x.Answers.Count > 0) ? x.Answers[0].GenderMask : Constants.NULL_INT;
                int genderMask2 = (null != y && null != y.Answers && y.Answers.Count > 0) ? y.Answers[0].GenderMask : Constants.NULL_INT;
                int sex1 = ((genderMask1 & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE) ? 0 : 1;
                int sex2 = ((genderMask2 & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE) ? 0 : 1;

                return (_reverse) ? sex1.CompareTo(sex2) : sex2.CompareTo(sex1);
            }
            else
            {
                int memberId1 = (null != x && null != x.Answers && x.Answers.Count > 0) ? x.Answers[0].MemberID : Constants.NULL_INT;
                int memberId2 = (null != y && null != y.Answers && y.Answers.Count > 0) ? y.Answers[0].MemberID : Constants.NULL_INT;
                Matchnet.Member.ServiceAdapters.Member member1 = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberId1, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                Matchnet.Member.ServiceAdapters.Member member2 = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberId2, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                int genderMask1 = member1.GetAttributeInt(_g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                int genderMask2 = member2.GetAttributeInt(_g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                int sex1 = ((genderMask1 & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE) ? 0 : 1;
                int sex2 = ((genderMask2 & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE) ? 0 : 1;
                return (_reverse) ? sex1.CompareTo(sex2) : sex2.CompareTo(sex1);
            }
        }
        #endregion
    }

    public class UpdateDateAnswerComparer : IComparer<Answer>
    {
        private bool _reverse = false;

        public UpdateDateAnswerComparer(){}

        public UpdateDateAnswerComparer(bool reverse)
        {
            this._reverse = reverse;
        }

        #region IComparer<Question> Members
        public int Compare(Answer x, Answer y)
        {
            DateTime dt1 = (null != x) ? x.UpdateDate : DateTime.MinValue;
            DateTime dt2 = (null != y) ? y.UpdateDate : DateTime.MinValue;
            int rc = dt2.CompareTo(dt1);
            return (_reverse) ? -rc : rc;
        }
        #endregion
    }

    public class GenderAnswerComparator : IComparer<Answer>
    {
        #region IComparer<Question> Members
        private ContextGlobal _g = null;
        private bool _reverse = false;

        public GenderAnswerComparator(ContextGlobal g, bool reverse)
        {
            this._g = g;
            this._reverse = reverse;
        }

        public int Compare(Answer x, Answer y)
        {
            if (QuestionAnswerHelper.IsPrePopulateAnswersForSortFilterEnabled(_g))
            {
                //NOTE: GenderMask is populated in answer dynamically when question/answer is pulled in MT
                int sex1 = (x != null && (x.GenderMask & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE) ? 0 : 1;
                int sex2 = (y != null && (y.GenderMask & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE) ? 0 : 1;
                int rc = sex2.CompareTo(sex1);
                return (_reverse) ? -rc : rc;
            }
            else
            {
                int memberId1 = (null != x) ? x.MemberID : Constants.NULL_INT;
                int memberId2 = (null != y) ? y.MemberID : Constants.NULL_INT;
                Matchnet.Member.ServiceAdapters.Member member1 = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberId1, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                Matchnet.Member.ServiceAdapters.Member member2 = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberId2, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                int genderMask1 = member1.GetAttributeInt(_g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                int genderMask2 = member2.GetAttributeInt(_g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                int sex1 = ((genderMask1 & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE) ? 0 : 1;
                int sex2 = ((genderMask2 & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE) ? 0 : 1;
                int rc = sex2.CompareTo(sex1);
                return (_reverse) ? -rc : rc;
            }
        }
        #endregion
    }

    public class YourMatchesAnswerComparer : IComparer<Answer>
    {
        private bool _reverse = false;
        private ContextGlobal _g = null;
        private ArrayList yourMatchesIds = null;
        private int seeking = Matchnet.Lib.ConstantsTemp.GENDERID_SEEKING_FEMALE;
        private bool _hasYourMatches=false;
        
        public bool HasYourMatches
        {
            get { return _hasYourMatches; }
            set { _hasYourMatches = value; }
        }
        
        public YourMatchesAnswerComparer() { }

        public YourMatchesAnswerComparer(ContextGlobal g, bool reverse)
        {
            this._reverse = reverse;
            this._g = g;
            int genderMask=g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
            this.seeking = ((genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_SEEKING_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_SEEKING_MALE) ? 0 : 1;
            string key = ResultListHandler.GetYourMatchesCacheKey(g);
            this.yourMatchesIds = g.Cache[key] as ArrayList;
            if (null == this.yourMatchesIds)
            {
                //saving orginal order by value
                string oldOrderBy = g.SearchPreferences["SearchOrderBy"];
                g.SearchPreferences["SearchOrderBy"] = ((Int32)Matchnet.Search.Interfaces.QuerySorting.Proximity).ToString();
                this.yourMatchesIds = ResultListHandler.GetYourMatchesIds(g, 360);
                //set back to old value
                g.SearchPreferences["SearchOrderBy"] = oldOrderBy;
            }
        }

        #region IComparer<Question> Members
        public int Compare(Answer x, Answer y)
        {
            int x1 = (null != this.yourMatchesIds && this.yourMatchesIds.Contains(x.MemberID)) ? 1 : -1;
            int y1 = (null != this.yourMatchesIds && this.yourMatchesIds.Contains(y.MemberID)) ? 1 : -1;

            //set flag if any member is in your matches
            if (x1 == 1 || y1 == 1) { HasYourMatches = true; }

            int rc = y1.CompareTo(x1);
            //if both are in your matches, sort by proximity
            if (x1 == 1 && y1 == 1)
            {
                int xIdx = this.yourMatchesIds.IndexOf(x.MemberID);
                int yIdx = this.yourMatchesIds.IndexOf(y.MemberID);
                //yourMatchesIds is sorted by proximity so a lower idx should sort first
                rc = xIdx.CompareTo(yIdx);
            }

            //if both are not in your matches, sort by gender and location
            if (x1 == -1 && y1 == -1)
            {
                if (QuestionAnswerHelper.IsPrePopulateAnswersForSortFilterEnabled(_g))
                {
                    int sex1 = (x != null && (x.GenderMask & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE) ? 0 : 1;
                    int sex2 = (y != null && (y.GenderMask & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE) ? 0 : 1;
                    int seekingX = (sex1 == this.seeking) ? 0 : 1;
                    int seekingY = (sex2 == this.seeking) ? 0 : 1;

                    rc = seekingX.CompareTo(seekingY);
                }
                else
                {
                    Matchnet.Member.ServiceAdapters.Member xMember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(x.MemberID, MemberLoadFlags.None);
                    Matchnet.Member.ServiceAdapters.Member yMember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(y.MemberID, MemberLoadFlags.None);
                    int genderMask1 = xMember.GetAttributeInt(_g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                    int genderMask2 = yMember.GetAttributeInt(_g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                    int sex1 = ((genderMask1 & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE) ? 0 : 1;
                    int sex2 = ((genderMask2 & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE) ? 0 : 1;
                    int seekingX = (sex1 == this.seeking) ? 0 : 1;
                    int seekingY = (sex2 == this.seeking) ? 0 : 1;

                    rc = seekingX.CompareTo(seekingY);
                }
                //TODO: how do we sort by location here??
            }
            return (_reverse) ? -rc : rc;
        }
        #endregion
    }
    #endregion
}
