﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Home.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Home" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="qa" TagName="Question" Src="Question.ascx" %>
<%@ Register TagPrefix="qa" TagName="QuestionList" Src="~/Applications/QuestionAnswer/Controls/QuestionsList.ascx" %>
<%@ Register TagPrefix="qa" TagName="AnswerModule" Src="~/Applications/QuestionAnswer/Controls/AnswerModule.ascx" %>

<script type="text/javascript">
$j(document).ready(function(){
	$j('#qnaHomeCont').bind('click',function(event){
			//let's close any opened dialogs
			$j(".question-dialog").dialog("close");
	
			$target = $j(event.target);
			$target.closest('input.btn').parents('.question-container').find('.question-dialog').dialog({
				width: 512,
				//height: 230,
				title: "<%=GetResourceForJS("TXT_ANSWER_HEADER")%>",
				open: function(event, ui){
					$j(this).find('textarea').focus();
                    $j(this).find('.tease.link-primary').bind(function(){
                        Spark_bindFlirtOverlay();
                    });
				},
				close: function(event, ui) {
					$j(this).dialog("destroy").appendTo($target.parents('.question-container'));
				}
            });
	});
});
</script>


<div class="page-container">
<h2 class="component-header" style="float:none"><mn:Txt runat="server" ResourceConstant="TXT_HOME_HEADER" ID="txtHomeHeader" /> &#160;<mn:Txt runat="server" ResourceConstant="TXT_HOME_HEADER_TOOLTIP" ID="txtHomeHeaderTooltip" /></h2>

<div id="qnaHomeCont" class="qna-home-cont tabs-hollow-cont clearfix">

<ul class="tabs-hollow">
<li class="<asp:Literal id=litClassQnATab1 runat=server />">
<asp:HyperLink id="lnkQnATab1" Runat="server" NavigateUrl="~/Applications/QuestionAnswer/Home.aspx?TabId=1" TabbedDisplay="true"><mn:Txt ID="txtQnATab1" runat="server" ResourceConstant="TXT_QNA_TAB_1"  /> </asp:HyperLink>
</li>
<li class="<asp:Literal id=litClassQnATab2 runat=server />">
<asp:HyperLink id="lnkQnATab2" Runat="server"  NavigateUrl="~/Applications/QuestionAnswer/Home.aspx?TabId=2"><mn:Txt ID="txtQnATab2" runat="server" ResourceConstant="TXT_QNA_TAB_2"/> </asp:HyperLink>
</li>
<li class="<asp:Literal id=litClassQnATab3 runat=server />">
<asp:HyperLink id="lnkQnATab3" Runat="server"  NavigateUrl="~/Applications/QuestionAnswer/Home.aspx?TabId=3"><mn:Txt ID="txt1" runat="server" ResourceConstant="TXT_QNA_TAB_3" NavigateUrl="~/Applications/QuestionAnswer/Home.aspx?Tab=3"/> </asp:HyperLink>
</li>
</ul>

<asp:PlaceHolder ID="phQnATab1" runat="server" Visible="false">
    <div id="qna-tab-question">
    <qa:Question ID="ucQuestion" runat="server" AutoLoad="false" TabbedDisplay="true" />
    </div>
</asp:PlaceHolder> 
<asp:PlaceHolder ID="phQnATab2" runat="server" Visible="false">
    <div id="qna-tab-recent">
    <qa:AnswerModule ID="ucAnswerModule" runat="server" PageSize="12"  DisplayMode="recent" HideSortOption="true" />
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phQnATab3" runat="server" Visible="false">
    <div id="qna-tab-list">
    <qa:QuestionList ID="ucQuestionList" runat="server" PageSize="24" ChapterSize="3" />
    </div>
</asp:PlaceHolder>
</div>
</div><!-- end #page-container -->