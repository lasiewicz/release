﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Member.ServiceAdapters;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.Web.Applications.MemberLike;

namespace Matchnet.Web.Applications.QuestionAnswer.Controls
{
    public partial class MiniAnswerProfile : FrameworkControl
    {
        private Answer _Answer;
        private Matchnet.Member.ServiceAdapters.Member _Member;
        private int _ordinal;
        private BreadCrumbHelper.EntryPoint _myEntryPoint = BreadCrumbHelper.EntryPoint.QuestionPage;
        private string _viewProfileUrl;
        private bool _isHighlighted = false;
        protected Color _Color = Color.none;
        protected string _ColorText = "";

        public bool IsCommentsEnabled
        {
            get { return QuestionAnswerHelper.IsQuestionAnswerCommentEnabled(g.Brand); }
        }

        public bool IsLoggedIn
        {
            get { return null != g.Member && g.Member.MemberID > 0; }
        }

        public bool IsMemberLikeEnabled
        {
            get { return MemberLikeHelper.Instance.IsMemberLikeEnabled(g.Brand); }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }


        public BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get { return (_myEntryPoint); }
            set { _myEntryPoint = value; }
        }

        public bool IsHighlighted
        {
            get { return (this._isHighlighted); }
            set { this._isHighlighted = value; }
        }

        public string TrackingParam { get; set; }

        public int AnswerId
        {
            get { return _Answer.AnswerID; }
        }

        public int QuestionId
        {
            get { return _Answer.QuestionID; }
        }

        public int MemberId
        {
            get
            {
                if (_Member != null && _Member.MemberID > 0)
                {
                    return _Member.MemberID;
                }
                return 0;
            }
        }

        public int MemberLikeId
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.MemberID;
                }
                return 0;
            }
        }

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {



        }

        #endregion

        #region Public methods
        public void LoadAnswerProfile(Answer answer)
        {
            try
            {
                _Answer = answer;
                _Member = MemberSA.Instance.GetMember(answer.MemberID, MemberLoadFlags.None);

                //member answer
                literalMemberAnswer.Text = _Answer.AnswerValue.Replace(Environment.NewLine, "<br />");

                //member profile info
                SearchResultProfile searchResult = new SearchResultProfile(_Member, Ordinal);
                lnkUserName1.Text = FrameworkGlobals.Ellipsis(_Member.GetUserName(_g.Brand), 12);
                this.literalSeeking.Text = Matchnet.Web.Applications.MemberProfile.ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(_Member, this.g);
                this.literalAge.Text = FrameworkGlobals.GetAge(_Member, g.Brand).ToString();
                this.literalLocation.Text = Matchnet.Web.Applications.MemberProfile.ProfileDisplayHelper.GetRegionDisplay(_Member, g);

                if (this.IsHighlighted)
                {
                    _viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, _Member.MemberID, this.Ordinal, null, Constants.NULL_INT, false, (int)BreadCrumbHelper.PremiumEntryPoint.Highlight);
                }
                else
                {
                    _viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, _Member.MemberID, this.Ordinal, null, Constants.NULL_INT, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
                }

                if (!String.IsNullOrEmpty(TrackingParam))
                {
                    _viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(_viewProfileUrl, TrackingParam);
                }

                lnkUserName1.NavigateUrl = _viewProfileUrl;
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, true);
                searchResult.SetThumb(imgThumb, noPhoto, _viewProfileUrl);

                //color code
                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(_Member, g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(_Member, g.Brand))
                {
                    phColorCode.Visible = true;
                    _Color = ColorCodeHelper.GetPrimaryColor(_Member, g.Brand);
                    _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);
                }

                lastTimeAction.LoadLastTime(_Answer.UpdateDate);

                if (IsMemberLikeEnabled)
                {
                    MemberLikeParams likeParams = new MemberLikeParams();
                    likeParams.SiteId = g.Brand.Site.SiteID;
                    likeParams.MemberId = g.Member.MemberID;
                    List<MemberLikeObject> likes = MemberLikeHelper.Instance.GetLikesByObjectIdAndType(likeParams, answer.AnswerID, Convert.ToInt32(LikeObjectTypes.Answer));
                    int likeCount = 0;
                    bool hasLike = false;
                    if (null != likes)
                    {
                        likes = likes.FindAll(delegate(MemberLikeObject likeObject)
                        {
                            Matchnet.Member.ServiceAdapters.Member aMember = MemberSA.Instance.GetMember(likeObject.MemberId, MemberLoadFlags.None);
                            return !MemberLikeHelper.Instance.IsMemberSuppressed(aMember, g);
                        });
                        MemberLikeObject match = likes.Find(delegate(MemberLikeObject obj)
                        {
                            bool b = (obj.MemberId == g.Member.MemberID);
                            return b;
                        });
                        hasLike = (null != match);
                        likeCount = likes.Count;
                    }

                    string likeNumber = string.Empty;
                    if (likeCount > 0)
                    {
                        likeNumber = string.Format(g.GetResource("TXT_LIKE_NUM", this), likeCount);
                    }

                    string likeResKey = (hasLike) ? "TXT_ANSWER_UNLIKE" : "TXT_ANSWER_LIKE";
                    likeTxt.Text = string.Format(g.GetResource(likeResKey, this), likeNumber);
                    likeTxt.Visible = true;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }


        #endregion
    }
}
