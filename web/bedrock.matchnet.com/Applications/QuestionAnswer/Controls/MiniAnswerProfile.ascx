﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniAnswerProfile.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.MiniAnswerProfile" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="~/Framework/Ui/PageElements/NoPhoto20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" Src="~/Framework/Ui/BasicElements/LastTime.ascx" TagName="LastTime" %>
<div class="answer-profile clearfix">
	<div class="member-pic">
	    <mn:Image runat="server" ID="imgThumb" Width="54" Height="70" class="profileImageHover" Border="0" ResourceConstant="ALT_PROFILE_PICTURE" TitleResourceConstant="TTL_VIEW_MY_PROFILE" />
        <uc1:NoPhoto runat="server" ID="noPhoto" class="no-photo" Visible="False" />
        <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
            <div class="cc-pic-tag cc-pic-tag-sm cc-pic-<%=_ColorText.ToLower() %>"><span><%=_ColorText %></span></div>
        </asp:PlaceHolder>
	</div>

    <div class="answer-container">
	    <div class="answer-body">
		    <div class="member-answer"><asp:Literal ID="literalMemberAnswer" runat="server"></asp:Literal></div>
	    </div>
	    <div class="member-info clearfix">
		    <span class="member-info-cont"><span class="member-name"><asp:HyperLink runat="server" ID="lnkUserName1" /></span><span class="member-values">,
		    <asp:Literal ID="literalAge" runat="server"></asp:Literal>,
		    <asp:Literal ID="literalSeeking" runat="server"></asp:Literal>,
		    <asp:Literal ID="literalLocation" runat="server"></asp:Literal></span>
		     &#9642; <span class="timestamp"> <uc2:LastTime ID="lastTimeAction" runat="server" /></span></span>
	        <span class="answer-info"><% if (IsCommentsEnabled && IsLoggedIn) { %><label class="link-label" for="comment-<%=AnswerId%>-<%=MemberId%>"><mn:txt id="mntxt5880" runat="server" resourceconstant="TXT_COMMENT_LABEL" expandimagetokens="true" />
	        </label><% } %>
            <% if (IsMemberLikeEnabled && IsLoggedIn) { %><span onclick="spark.qanda.LikeAnswer(this,<%=MemberLikeId%>,<%=AnswerId%>,spark.qanda.likeOmniParams)">&#9642; <asp:Literal ID="likeTxt" visible="false" runat="server"/></span><% } %>
	        </span>
	    </div>
	    <% if (IsCommentsEnabled && IsLoggedIn) { %>
	    <div class="member-comment hide">
	        <h2><mn:txt id="mntxt9377" runat="server" resourceconstant="TXT_ANSWER_COMMENT_HEADER" expandimagetokens="true" /></h2>
	        <span class="close"><mn:txt id="mntxt9198" runat="server" resourceconstant="TXT_COMMENT_CLOSE_BOX" expandimagetokens="true" /></span>
	        <textarea id="comment-<%=AnswerId%>-<%=MemberId%>" rows="2" cols="70" class="watermark"></textarea>
	        <input type="submit" id="<%=AnswerId%>-<%=MemberId%>" name="sendComment" class="btn secondary commentSubmit" value="<%=g.GetResource("TXT_COMMENT_LABEL",this)%>" />
	    </div>
	    <% } %>
	</div>
</div>
