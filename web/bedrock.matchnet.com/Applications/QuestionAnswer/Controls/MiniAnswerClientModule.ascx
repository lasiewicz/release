﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniAnswerClientModule.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.MiniAnswerClientModule" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<% if (IsCommentsEnabled && IsLoggedIn){ %>
<script type="text/javascript" src="/Applications/QuestionAnswer/Controls/qanda.js"></script>
<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {
            IsCommentsEnabled:true,
            RedirectUrl:'<%=GetRedirectUrl(888) %>',
            BrandId:<%=g.Brand.BrandID %>,
            txtCommentCloseBox: '<%=GetResourceForJS("TXT_COMMENT_CLOSE_BOX") %>',
            txtCommentLabel: '<%=GetResourceForJS("TXT_COMMENT_LABEL") %>',
            txtCommentSubmit:'<%=GetResourceForJS("TXT_COMMENT_SEND") %>',
            txtCommentAnswerHeading:'<%=GetResourceForJS("TXT_ANSWER_COMMENT_HEADER") %>',
            txtCommentWatermarkCopy:'<%=GetResourceForJS("TXT_WATERMARK_COMMENT") %>',
            txtCommentConfirmationCopy:'<%=GetResourceForJS("TXT_COMMENT_CONFIRM") %>',
            txtEmptyCommentErrorCopy:'<%=GetResourceForJS("TXT_EMPTY_COMMENT_ERROR") %>',
            txtCommentErrorCopy:'<%=GetResourceForJS("TXT_COMMENT_ERROR") %>',
            txtCommentMailSubject:'<%=GetResourceForJS("TXT_COMMENT_SUBJECT") %>',
            QuestionId:<%=QuestionId%>,
            __init__: function() {
            }        
       });
    })();

    $j(document).ready(function() {
        $j("input.commentSubmit").live("click",function(evt) {
            var elementId=$j(this).attr('id');
            var toMemberId = (elementId && elementId.indexOf('-') > -1) ? elementId.split('-')[1] : 0;
            var myFunc=function() {
                return spark.qanda.SendMail('comment-'+elementId,spark.qanda.QuestionId,<%=MemberId%>,'<%=MemberUserName%>',{Event:"event51",Overwrite:true,Props:[{Num:38,Value:"Comment-Home Page"},{Num:35,Value:toMemberId}]});
            }
            spark.qanda.ajaxCallWithBlock($j(this).parent(),{ 
                  message: '<%=GetResourceForJS("HTML_BLOCK_ELEMENT_MSG") %>',
                  centerY: false,
                  css: {backgroundColor: 'transparent', border: 'none', top:'10%'},
                  overlayCSS: {backgroundColor: '#fff', opacity: 0.8 }
            },myFunc,'/Applications/API/IMailService.asmx/SendComment');            
            evt.stopPropagation();            
            return false;
        });
        
        $j("#mini-a-module").bind('click', function(event) {
            var target=$j(event.target);
            if (target.hasClass("link-label") && spark.qanda.RedirectUrl && spark.qanda.RedirectUrl.length > 0) {
                document.location.href = spark.qanda.RedirectUrl;
            } else {
                target.closest('label').parents('.member-info').toggleClass('hide')
                .parents('.answer-profile').find('.member-comment').toggleClass('hide');
                target.closest('.close').parents('.member-comment').toggleClass('hide')
                .parents('.answer-profile').find('.member-info').toggleClass('hide');
            }
        });

        $j("#mini-a-module textarea").watermark(spark.qanda.txtCommentWatermarkCopy);
        
        $j("label.link-label").click(function(evt) {
            spark.tracking.addEvent("event50", true);
            spark.tracking.addProp(38, "Comment-Home Page", true);
            spark.tracking.track();
        });
        
    });   
    //]]>
</script>
<% } %>
<div class="header-options clearfix">
    <h2 class="component-header"><mn2:FrameworkLiteral ID="answersHeader" runat="server" ResourceConstant="TXT_MEMBER_ANSWERS" /> <span id="answer-count" class="answer-count"><asp:HyperLink ID="lnkQuestion" runat="server">(<span id="answerNum"><%=TotalAnswersNum.ToString()%></span> <span id="answerNumTxt"><mn:txt id="Txt2" runat="server" ResourceConstant="TXT_ANSWERS" /></span>)</asp:HyperLink></span></h2>
<% if (IsSortEnabled && IsLoggedIn){ %>
<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {
            RedirectUrl:'<%=GetRedirectUrl(888) %>',
            BrandId:<%=g.Brand.BrandID %>,
            txtViewMyProfile: '<%=GetResourceForJS("TXT_VIEW_MY_PROFILE") %>',
            EntryPoint:'<%=EntryPoint%>',
            TrackingParam:'<%=TrackingParam%>',
            IsSite20Enabled:<%=g.IsSite20Enabled.ToString().ToLower() %>,
            txtOneAnswerNumberText:'<%=GetResourceForJS("TXT_ONE_ANSWER") %>',
            txtAnswerNumberText:'<%=GetResourceForJS("TXT_ANSWERS") %>',
            QuestionId:<%=QuestionId%>,
            __init__: function() {
                if (!$j.render) {
                    $j("head").append("<script type=\"text/javascript\" src=\"/javascript20/library/jquery.tmpl.js\"><"+ "/script>");
                }
                if (!spark || !spark.qanda || !spark.qanda.GetQuestion) {
                    $j("head").append("<script type=\"text/javascript\" src=\"/Applications/QuestionAnswer/Controls/qanda.js\"><"+ "/script>");
                }
            }
       });
    })();

    $j(document).ready(function() {
        $j("#miniAnswerSort").change(function(evt) {
            var myFunc = function() {
                var sortVals=$j("#miniAnswerSort").val().split("|");
                spark.qanda.GetQuestion(evt,"<%=ElementId%>","mini-a-module","answerNum","answerNumTxt",spark.qanda.QuestionId,<%=MemberId%>,<%=PageSize%>,1,sortVals[0],sortVals[1]);
            };
            spark.qanda.ajaxCallWithBlock('answers-list',{ 
                  message: '<%=GetResourceForJS("HTML_BLOCK_ELEMENT_MSG") %>',
                  centerY: false,
                  css: {backgroundColor: 'transparent', border: 'none', top:'10%'},
                  overlayCSS: {backgroundColor: '#fff', opacity: 0.8 }
            },myFunc,'/Applications/API/QuestionAnswerService.asmx/GetQuestion');
        }).parents("label").removeClass("hide");
        
        $j("#miniAnswerSort option[value='<%=LastSort%>']").attr('selected','selected');
    });
    //]]>
</script>
    <label class="float-outside hide"><mn2:FrameworkLiteral ID="sortSelect" runat="server" ResourceConstant="TXT_SORT_SELECT" />
        <select name="miniAnswerSort" id="miniAnswerSort">
            <option value="date|false" selected="selected"><mn2:FrameworkLiteral ID="FrameworkLiteral1" runat="server" ResourceConstant="TXT_SORT_NEWEST_FIRST" /></option>
            <option value="date|true"><mn2:FrameworkLiteral ID="FrameworkLiteral2" runat="server" ResourceConstant="TXT_SORT_OLDEST_FIRST" /></option>
            <option value="matches|false"><mn2:FrameworkLiteral ID="FrameworkLiteral5" runat="server" ResourceConstant="TXT_SORT_YOUR_MATCHES_FIRST" /></option>
            <option value="gender|true"><mn2:FrameworkLiteral ID="FrameworkLiteral3" runat="server" ResourceConstant="TXT_SORT_MEN_FIRST" /></option>
            <option value="gender|false"><mn2:FrameworkLiteral ID="FrameworkLiteral4" runat="server" ResourceConstant="TXT_SORT_WOMEN_FIRST" /></option>
        </select>
    </label>
<% } %>
<% if (IsMemberLikeEnabled && IsLoggedIn){ %>
<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {
            BrandId:<%=g.Brand.BrandID %>,
            MemberId:<%=MemberId %>,
            txtLike: '<%=GetResourceForJS("TXT_ANSWER_LIKE") %>',
            txtUnLike: '<%=GetResourceForJS("TXT_ANSWER_UNLIKE") %>',
            txtLikeNum: '<%=GetResourceForJS("TXT_LIKE_NUM") %>',
            IsMemberLikeEnabled: true,
            likeOmniParams:{QuestionId:<%=QuestionId %>,Event:"event51,event52",Overwrite:true,Props:[{Num:38,Value:"Home Page"}]},
            __init__: function() { 
                if (!spark || !spark.qanda || !spark.qanda.LikeAnswer) {
                    $j("head").append("<script type=\"text/javascript\" src=\"/Applications/QuestionAnswer/Controls/qanda.js\"><"+ "/script>");
                }
            }        
       });
    })();
    //]]>
</script>
<% } %>    

</div>
