﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BothAnsweredProfile.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.BothAnsweredProfile" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="~/Framework/Ui/PageElements/NoPhoto20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" Src="~/Framework/Ui/BasicElements/LastTime.ascx" TagName="LastTime" %>
<div class="double-q-thiers">
     <div class="answer-profile clearfix">
	    <div class="member-pic">
	        <mn:Image runat="server" ID="imgThumb" Width="54" Height="70" class="profileImageHover" Border="0" ResourceConstant="ALT_PROFILE_PICTURE" TitleResourceConstant="TTL_VIEW_MY_PROFILE" />
            <uc1:NoPhoto runat="server" ID="noPhoto" class="no-photo" Visible="False" />
            <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
                <div class="cc-pic-tag cc-pic-tag-sm cc-pic-<%=_ColorText.ToLower() %>"><span><%=_ColorText %></span></div>
            </asp:PlaceHolder>
	    </div>

        <div class="answer-container">
	        <div class="answer-body">
		        <div class="member-answer"><span class="member-answer-expandor"><asp:Literal ID="literalMemberAnswer" runat="server"></asp:Literal></span><span class="timestamp"> <uc2:LastTime ID="lastTimeAction" runat="server" /></span></div>
	        </div>
            <div class="answer-info">
            <% if (IsCommentsEnabled && IsLoggedIn) { %><label class="link-label" 
                                for="comment-<%=AnswerId%>-<%=MemberId %>"><mn:Txt 
                                ID="mntxt2513" runat="server" expandimagetokens="true" 
                                resourceconstant="TXT_COMMENT_LABEL" />
                            </label><% } %> 
                <% if (IsMemberLikeEnabled && IsLoggedIn) { %> &#9642; <span onclick="spark.qanda.LikeAnswer(this,<%=MemberLikeId%>,<%=AnswerId%>,{QuestionId:<asp:Literal id='likeQuestionId' runat='server'/>,Event:'event51,event52',Overwrite:true,Props:[{Num:38,Value:'Another Member Profile'}]})"> <asp:Literal ID="likeTxt" visible="false" runat="server"/></span><% } %>
	        </div>
                <% if (IsCommentsEnabled && IsLoggedIn) { %>
                <div class="member-comment clearfix hide">
                    <span class="close">
                    <mn:Txt ID="mntxt5239" runat="server" expandimagetokens="true" 
                        resourceconstant="TXT_COMMENT_CLOSE_BOX" />
                    </span>
                    <h2><mn:Txt ID="TxtAnswerCommentHeader" runat="server" expandimagetokens="true" resourceconstant="TXT_ANSWER_COMMENT_HEADER" /></h2>
                    <textarea ID="comment-<%=AnswerId%>-<%=MemberId %>" 
                        class="watermark" cols="70" rows="2"></textarea>
                    <mn2:FrameworkButton ID="buttonAnswerId" runat="server" 
                        CssClass="btn secondary commentSubmit" ResourceConstant="TXT_COMMENT_LABEL" />
                </div>
                <% } %>
	    </div>
    </div>
</div>
<div class="double-q-yours">
     <div class="answer-profile clearfix">
	    <div class="member-pic">
	        <mn:Image runat="server" ID="imgThumb2" Width="54" Height="70" class="profileImageHover" Border="0" ResourceConstant="ALT_PROFILE_PICTURE" TitleResourceConstant="TTL_VIEW_MY_PROFILE" />
            <uc1:NoPhoto runat="server" ID="noPhoto2" class="no-photo" Visible="False" />
            <asp:PlaceHolder ID="phColorCode2" runat="server" Visible="false">
                <div class="cc-pic-tag cc-pic-tag-sm cc-pic-<%=_ColorText2.ToLower() %>"><span><%=_ColorText2 %></span></div>
            </asp:PlaceHolder>
	    </div>
        <div class="answer-container">
	        <div class="answer-body">
		        <div class="member-answer"><span class="member-answer-expandor"><asp:Literal ID="literalMemberAnswer2" runat="server"></asp:Literal></span><span class="timestamp"> <uc2:LastTime ID="lastTimeAction2" runat="server" /></span></div>
	        </div>
	        <div class="answer-info">
                <asp:PlaceHolder ID="phEditSection" runat="server">
                    <asp:HyperLink ID="lnkEditQuestion" runat="server"><mn2:FrameworkLiteral ID="literalEditQuestion" runat="server" ResourceConstant="TXT_REMOVE"></mn2:FrameworkLiteral></asp:HyperLink>
                </asp:PlaceHolder>
            </div>        
	    </div>
        <asp:PlaceHolder ID="phPendingAnswer" runat="server" Visible="false">
            <div class="answer-review-message notification">
                <mn2:FrameworkLiteral ID="literalQuestionAnsweredApproval" runat="server" 
                    ResourceConstant="TXT_QUESTIONANSWEREDAPPROVAL"></mn2:FrameworkLiteral>
            </div>
        </asp:PlaceHolder>	
    </div>
</div>
