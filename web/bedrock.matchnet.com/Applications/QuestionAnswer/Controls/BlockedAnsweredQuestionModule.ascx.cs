﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.Web.Framework.Ui.PageElements;

namespace Matchnet.Web.Applications.QuestionAnswer.Controls
{
    public partial class BlockedAnsweredQuestionModule : FrameworkControl
    {
        private Hashtable myQuestions;
        private List<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> _nonMatchingQuestions;
        protected IMemberDTO _MemberProfile;
        private BreadCrumbHelper.EntryPoint _entryPoint = BreadCrumbHelper.EntryPoint.MemberProfile;
        protected string _WatermarkText = "";
        protected Color _Color = Color.none;
        protected string _ColorText = "";
        private int _ordinal;
        private int _PageSize = 6;
        private int _ChapterSize = 3;

        public int PageSize
        {
            get { return _PageSize; }
            set { _PageSize = value; }
        }

        public int ChapterSize
        {
            get { return _ChapterSize; }
            set { _ChapterSize = value; }
        }
        
        public IMemberDTO MemberProfile
        {
            get { return _MemberProfile; }
            set { _MemberProfile = value; }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }

        public int ProfileMemberId
        {
            get { return (null != MemberProfile) ? MemberProfile.MemberID:0; }
        }

        public int LoggedInMemberId
        {
            get { return (null != g.Member) ?  g.Member.MemberID : 0; }
        }

        public List<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> NonMatchingQuestions
        {
            set { this._nonMatchingQuestions = value; }
            get { return this._nonMatchingQuestions; }
        }

        public bool HasMatchingQuestions { get; set; }

        public string TrackingParam { get; set; }
        public int TotalAnswersNum { get; set; }

        public string RunJSOnAnswerPostSuccess { get; set; }

        public BreadCrumbHelper.EntryPoint EntryPoint
        {
            get { return _entryPoint; }
            set { _entryPoint = value; }
        }

        public string GetResourceForJS(string resConst)
        {
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, this));
        }

        public string GetResourceForJS(string resConst, object resourceControl)
        {
            if (null == resourceControl)
            {
                return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst));
            }
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, resourceControl));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void LoadModule(IMemberDTO member, List<PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> questions)
        {
            if (null != questions && questions.Count > 0)
            {
                phHeader.Visible = true;
                phClientPagintation.Visible = QuestionAnswerHelper.IsQuestionAnswerClientPaginationEnabled(g.Brand);

                NonMatchingQuestions = questions;
                MemberProfile = member;
                //get question and answers for member                
                myQuestions = QuestionAnswerHelper.GetLoggedInMemberQuestions(g);

                repeaterDoubleQandAOneAnswered.DataSource = NonMatchingQuestions;
                repeaterDoubleQandAOneAnswered.DataBind();
                repeaterDoubleQandAOneAnswered.Visible = true;

                string hasOthersText = (HasMatchingQuestions) ? g.GetResource("TXT_OTHER", this) : string.Empty;
                string reskey = (NonMatchingQuestions.Count > 1) ? "TXT_NUM_MATCHED" : "TXT_NUM_MATCHED_ONE";
                numMatchedQuestions.Text = g.GetResource(reskey, this, new string[] { MemberProfile.GetUserName(g.Brand), NonMatchingQuestions.Count.ToString(), hasOthersText });
                int genderMask = MemberProfile.GetAttributeInt(g.Brand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                string reskey2 = ((genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) ? "TXT_INSTRUCTIONS_HER" : "TXT_INSTRUCTIONS_HIS";
                instructions.Text = g.GetResource(reskey2, this);
            }
            else
            {
                phHeader.Visible = false;
                phClientPagintation.Visible = false;
            }
        }

        protected void repeaterDoubleQandAOneAnswered_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question = e.Item.DataItem as Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question;
            if (null == myQuestions || !myQuestions.ContainsKey(question.QuestionID))
            {
                Literal literalQuestion = (Literal)e.Item.FindControl("literalQuestion2");
                HyperLink linkQuestion = (HyperLink)e.Item.FindControl("lnkQuestion2");
                Literal questionId = (Literal)e.Item.FindControl("questionId");
                FrameworkButton btnSubmitQuestion = (FrameworkButton)e.Item.FindControl("btnSubmitQuestion");
                Matchnet.Web.Framework.Image imgThumb = (Matchnet.Web.Framework.Image)e.Item.FindControl("imgThumb");
                NoPhoto20 noPhoto = (NoPhoto20)e.Item.FindControl("noPhoto");
                PlaceHolder phColorCode = (PlaceHolder)e.Item.FindControl("phColorCode");

                //member profile info
                SearchResultProfile searchResult = new SearchResultProfile(MemberProfile, Ordinal);
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, true);
                searchResult.SetThumb(imgThumb, noPhoto, string.Empty);

                //color code
                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(MemberProfile, g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(MemberProfile, g.Brand))
                {
                    phColorCode.Visible = true;
                    _Color = ColorCodeHelper.GetPrimaryColor(MemberProfile, g.Brand);
                    _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);
                }

                WatermarkTextbox textAnswer = (WatermarkTextbox)e.Item.FindControl("txtAnswer1");
                _WatermarkText = g.GetResource("TXT_QUESTIONANSWER_WATERMARK", textAnswer).Trim();
                textAnswer.WatermarkText = _WatermarkText;

                QuestionHandler _questionHandler = new QuestionHandler();
                Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question _Question = Matchnet.PremiumServiceSearch.ServiceAdapters.QuestionAnswerSA.Instance.GetQuestion(question.QuestionID);

                _questionHandler.Init(g, _Question, this, e.Item);
                _questionHandler.DisplayUnAnsweredQuestion();
                string validateJS = string.Format("return spark.qanda.ValidateQAnswer(function(){{ return {0};}},'divAnswerError{1}');", _questionHandler.AnswerElement, question.QuestionID);
                btnSubmitQuestion.OnClientClick = validateJS;

                literalQuestion.Text = question.Text;
                linkQuestion.NavigateUrl += "?" + WebConstants.URL_PARAMETER_NAME_QUESTION_ID + "=" + question.QuestionID;
                questionId.Text = question.QuestionID.ToString();
                
            }
        }
    }
}