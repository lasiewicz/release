﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BothAnsweredQuestionModule.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.BothAnsweredQuestionModule" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" Src="~/Framework/Ui/BasicElements/LastTime.ascx" TagName="LastTime" %>
<%@ Register TagPrefix="ml" TagName="MemberLikeAnswerClientModule" Src="~/Applications/MemberLike/MemberLikeAnswerClientModule.ascx" %>
<%@ Register TagPrefix="qa" TagName="BothAnsweredProfile" Src="BothAnsweredProfile.ascx" %>
<asp:PlaceHolder ID="phClientPagintation" runat="server" Visible="false">
    <script type="text/javascript">
        //<![CDATA[
        $j(document).ready(function() {        
            if($j(".double-q").size() > <%=PageSize %>) {
                __addToNamespace__('spark.paging', {
                    bothAnsweredPagingObj: new spark.paging.Pages($j(".double-q"), <%=PageSize %>, <%=ChapterSize %>, {next: '<%=GetResourceForJS("TXT_NEXT", null)%>', prev: '<%=GetResourceForJS("TXT_PREVIOUS", null)%>'}, { location: 'both answered' }),
                    __init__:function() {
                        spark.paging.bothAnsweredPagingObj.getPagingText({ name: 'spark.paging.bothAnsweredPagingObj', elemIds: ['topPaging'] });
                        $j("#bothAnsweredHeader").addClass("with-paging");
                    }
                });                
            } else {
                $j("#bothAnsweredHeader").removeClass("with-paging");
            }
        });            
        //]]>    
    </script>
</asp:PlaceHolder>
<div id="bothAnsweredHeader" class="double-q-header clearfix" style="<asp:Literal id='headerStyle' runat='server' />">
    <h2><asp:Literal ID="numMatchedQuestions" runat="server"/></h2><div id="topPaging" class="paging"></div>
</div>
<div id="both-answered-list">
<asp:Repeater ID="repeaterDoubleQandABothAnswered" runat="server" OnItemDataBound="repeaterDoubleQandABothAnswered_ItemDataBound" Visible="false">
    <ItemTemplate>
    <div id="qid-<asp:Literal id='questionId' runat='server'/>" class="double-q clearfix">
        <h2><mn2:FrameworkLiteral ID="literalQ" runat="server" ResourceConstant="TXT_Q"></mn2:FrameworkLiteral> <asp:HyperLink ID="lnkQuestion" NavigateUrl="/Applications/QuestionAnswer/Question.aspx" runat="server"><asp:Literal ID="literalQuestion" runat="server"></asp:Literal></asp:HyperLink></h2>
        <qa:BothAnsweredProfile ID="MiniAnswerProfile1" runat="server" />
    </div><!-- end double-q -->
    </ItemTemplate>
</asp:Repeater>        
</div>
<script type="text/javascript">
 $j("#both-answered-list .member-answer").expander({
    slicePoint: 200,
    expandPrefix: ' ',
    expandText: '[&hellip;]',
    userCollapseText: '[^]'
  });
</script>
