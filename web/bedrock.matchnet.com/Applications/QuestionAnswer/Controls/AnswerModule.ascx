﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnswerModule.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.AnswerModule" %>
<%@ Register TagPrefix="qa" TagName="AnswerProfile" Src="AnswerProfile.ascx" %>
<%@ Import Namespace="Matchnet.Web.Applications.MemberProfile" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>


<% if (IsCommentsEnabled && IsLoggedIn){ %>
<script type="text/javascript" src="/Applications/QuestionAnswer/Controls/qanda.js"></script>
<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {
            RedirectUrl:'<%=GetRedirectUrl(888) %>',
            BrandId:<%=g.Brand.BrandID %>,
            txtViewMyProfile: '<%=GetResourceForJS("TXT_VIEW_MY_PROFILE") %>',
            txtCommentCloseBox: '<%=GetResourceForJS("TXT_COMMENT_CLOSE_BOX") %>',
            txtCommentLabel: '<%=GetResourceForJS("TXT_COMMENT_LABEL") %>',
            txtCommentSubmit:'<%=GetResourceForJS("TXT_COMMENT_SEND") %>',
            txtCommentWatermarkCopy:'<%=GetResourceForJS("TXT_WATERMARK_COMMENT") %>',
            txtCommentConfirmationCopy:'<%=GetResourceForJS("TXT_COMMENT_CONFIRM") %>',
            txtEmptyCommentErrorCopy:'<%=GetResourceForJS("TXT_EMPTY_COMMENT_ERROR") %>',
            txtCommentErrorCopy:'<%=GetResourceForJS("TXT_COMMENT_ERROR") %>',
            txtCommentMailSubject:'<%=GetResourceForJS("TXT_COMMENT_SUBJECT") %>',
            __init__: function() { }        
       });
    })();
    
    jQuery(document).ready(function() {
        $j("#a-module").bind('click',function(event){
            var target=$j(event.target);
            if (target.hasClass("link-label") && spark.qanda.RedirectUrl && spark.qanda.RedirectUrl.length > 0) {
                document.location.href = spark.qanda.RedirectUrl;
            } else {
                target.closest('label').parents('.member-info').toggleClass('hide')
                    .parents('.answer-profile').find('.member-comment').toggleClass('hide');
                target.closest('.close').parents('.member-comment').toggleClass('hide')
                    .parents('.answer-profile').find('.member-info').toggleClass('hide');
            }
        });

        $j("#a-module textarea").watermark(spark.qanda.txtCommentWatermarkCopy);

        $j("input.commentSubmit").live("click",function(evt) {
            var elementId=$j(this).attr('id');
            var toMemberId = (elementId && elementId.indexOf('-') > -1) ? elementId.split('-')[1] : 0;
            var myFunc=function() {
                return spark.qanda.SendMail('comment-'+elementId,<%=QuestionId%>,<%=MemberId%>,'<%=MemberUserName%>',{Event:"event51",Overwrite:true,Props:[{Num:38,Value:"Comment-<%=Location%>"},{Num:35,Value:toMemberId}]});
            }
            spark.qanda.ajaxCallWithBlock($j(this).parent(),{ 
                  message: '<%=GetResourceForJS("HTML_BLOCK_ELEMENT_MSG") %>',
                  centerY: false,
                  css: {backgroundColor: 'transparent', border: 'none', top:'10%'},
                  overlayCSS: {backgroundColor: '#fff', opacity: 0.8 }
            },myFunc,'/Applications/API/IMailService.asmx/SendComment');            
            evt.stopPropagation();            
            return false;
        });        
        
        $j("label.link-label").click(function(evt){
            spark.tracking.addEvent("event50", true);
            spark.tracking.addProp(38, "Comment-<%=Location%>", true);
            spark.tracking.track();                            
        });        
    });
    //]]>
</script>
<% } %>    

<% if (IsMemberLikeEnabled && IsLoggedIn){ %>
<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {
            BrandId:<%=g.Brand.BrandID %>,
            txtLike: '<%=GetResourceForJS("TXT_ANSWER_LIKE") %>',
            txtUnLike: '<%=GetResourceForJS("TXT_ANSWER_UNLIKE") %>',
            txtLikeNum: '<%=GetResourceForJS("TXT_LIKE_NUM") %>',
            __init__: function() { 
                if (!spark || !spark.qanda || !spark.qanda.LikeAnswer) {
                    $j("head").append("<script type=\"text/javascript\" src=\"/Applications/QuestionAnswer/Controls/qanda.js\"><"+ "/script>");
                }
            }        
       });
    })();
    //]]>
</script>
<% } %>    

<script type="text/javascript">
    //<![CDATA[
    jQuery(document).ready(function() {
    <% if (IsSortEnabled) { %>        
        $j("#pageAnswerSort").change(function(evt) {
            var sortVals = $j(this).val().split("|");
            var url = '<%=QuestionUrlBase %>';
            url += "sort=" + sortVals[0] + "&sortd=" + sortVals[1];
            document.location.href = url;
        }).parents("label").removeClass("hide");
        $j("#pageAnswerSort option[value='<%=LastSort%>']").attr('selected','selected');
    <% } %>
        if (spark && spark.qanda && spark.qanda.QuestionInputElement) {
            $j("#"+spark.qanda.QuestionInputElement).click(function(evt) {
                if (!spark.tracking.clickOnce) {
                    spark.tracking.addEvent("event50", true);
                    spark.tracking.addProp(38,"Answer-<%=Location%>", true);
                    spark.tracking.track();
                    spark.tracking.clickOnce = true;
                }
            });
        }
        //tracking for answer question buttons
        $j(".btn.primary").click(function(evt) {
            spark.tracking.addEvent("event50", true);
            spark.tracking.addProp(38,"Answer-<%=Location%>", true);
            spark.tracking.track();
        });
    });
    //]]>
</script>

<script type="text/javascript">
    //<![CDATA[
    function AnswerPopupQ(questionid, memberid, answerMemberId, answerText, isSite20Enabled, btnelement, divsuccessmsg, divanswererror, literalanswertext) {
        var error = false;
        
      try{
       
          if (answerText == '' || answerText == '<%= WatermarkText%>') {
           error = true;
       }
       
        if (error == true) {

            $j('#' + divanswererror).toggleClass('hide');
            return false;
        }
       
       $j.ajax({
           type: "POST",
           url: "/Applications/API/QuestionAnswerService.asmx/AnswerBlockedQuestion",
           data: "{questionID:" + questionid + "," +
                "memberID:" + memberid + "," +
                "answerMemberID:" + answerMemberId + "," +
                "answerText:\"" + answerText + "\"," +
                "entryPoint:\"\",trackingParam:\"\"," +
                "isSite20Enabled:" + isSite20Enabled + "}",
           contentType: "application/json; charset=ISO-8859-1",
           dataType: "json",
           success: function(msg, status) {
              // alert('success');

               var answeredHtml = '<div class="member-answer clearfix">' + literalanswertext + '<a href="/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&<%=WebConstants.URL_PARAMETER_NAME_PROFILETAB%>=<%=ProfileTabEnum.Relationship.ToString("d")%>" class="edit-inline"><%=GetResourceForJS("TXT_ANSWER_EDIT") %></a></div>'
               
               //console.log('answerText: ' + answerText);
               //console.log('answeredHtml: ' + answeredHtml);
               
               $answerInputDiv = $j("#" + btnelement).parents('.answer-input');
               $answerInputDiv.find('textarea, ul').css({display: 'none'});
               $answerInputDiv.prepend(answeredHtml);

               $j("#" + btnelement).toggleClass('hide');
               $j("#" + divsuccessmsg).toggleClass('hide');
               spark.tracking.addEvent("event51", true);
               spark.tracking.addProp(38, "Answer-<%=Location%>", true);
               spark.tracking.track();
               return true;
           },

           error: function(msg) { $j('#' + divanswererror).toggleClass('hide'); return false; }

       });
    } catch (err) {
       
    }
    }
    //]]>
</script>

<asp:PlaceHolder ID="phAnswerProfile" runat="server" Visible="false">
    <div id="a-module" class="a-module clearfix">
        <div class="header-options clearfix">
    	    <h1 class="component-header"><asp:PlaceHolder ID="phAnswersFromMember" runat="server"> <mn:txt id="txtAnswersFromMember" runat="server" ResourceConstant="TXT_ANSWERS_FROM_MEMBERS" /> <span id="answer-count" class="answer-count">(<%=_TotalAnswers.ToString()%> <mn:txt id="Txt2" runat="server" ResourceConstant="TXT_ANSWERS" />)</span></asp:PlaceHolder></h1>
            <div class="paging"><asp:label id="lblListNavigationTop" Runat="server" /></div>
            <% if (IsSortEnabled && !HideSortOption) { %>
            <label class="sort hide"><mn2:FrameworkLiteral ID="sortSelect" runat="server" ResourceConstant="TXT_SORT_SELECT" />
                <select name="pageAnswerSort" id="pageAnswerSort">
                    <option value="date|false"><mn2:FrameworkLiteral ID="FrameworkLiteral1" runat="server" ResourceConstant="TXT_SORT_NEWEST_FIRST" /></option>
                    <option value="date|true"><mn2:FrameworkLiteral ID="FrameworkLiteral2" runat="server" ResourceConstant="TXT_SORT_OLDEST_FIRST" /></option>
                    <option value="matches|false"><mn2:FrameworkLiteral ID="FrameworkLiteral5" runat="server" ResourceConstant="TXT_SORT_YOUR_MATCHES_FIRST" /></option>
                    <option value="gender|true"><mn2:FrameworkLiteral ID="FrameworkLiteral3" runat="server" ResourceConstant="TXT_SORT_MEN_FIRST" /></option>
                    <option value="gender|false"><mn2:FrameworkLiteral ID="FrameworkLiteral4" runat="server" ResourceConstant="TXT_SORT_WOMEN_FIRST" /></option>
                </select>
            </label>
            <% } %>
        </div>
        
        <mn:Txt ID="txtNoYourMatchesAnswers" ResourceConstant="TXT_NO_YOUR_MATCHES_ANSWERS" runat="server" Visible="false" />

        <!--answer profiles-->
        <asp:Repeater ID="repeaterAnswers" runat="server" OnItemDataBound="repeaterAnswers_ItemDataBound">
            <ItemTemplate>
                <qa:AnswerProfile ID="AnswerProfile1" runat="server"></qa:AnswerProfile>
            </ItemTemplate>
        </asp:Repeater>
        
        <div class="pagination lower"><asp:label id="lblListNavigationBottom" Runat="server" /></div>

    </div>
    <script type="text/javascript">
     $j(".member-answer", $j('#a-module')[0]).expander({
        slicePoint: 174,
        expandPrefix: ' ',
        expandText: '[&hellip;]',
        userCollapseText: '[^]'
      });
    </script>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phNoAnswers" runat="server" Visible="false">
    <div id="qanda-no-answers">
        <mn2:FrameworkLiteral ID="literalNoAnswers" runat="server" ResourceConstant="TXT_NO_ANSWERS"></mn2:FrameworkLiteral>
    </div>
</asp:PlaceHolder>