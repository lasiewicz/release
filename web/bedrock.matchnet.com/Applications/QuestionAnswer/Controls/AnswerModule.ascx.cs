﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Applications.MemberLike;

namespace Matchnet.Web.Applications.QuestionAnswer.Controls
{
    public enum DisplayAnswersMode
    {
        question=1,
        recent=2
    }
    public partial class AnswerModule : FrameworkControl,iTab
    {
        private Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question _Question;
        private int _StartRow = 0;
        protected int _TotalAnswers = 0;
        private int _PageSize = 12;
        private int _ChapterSize = 3;
        private int _Counter = 0;
        private string _lastSort=null;
        private BreadCrumbHelper.EntryPoint _entryPoint = BreadCrumbHelper.EntryPoint.HomePage;
        private string _location = "Question Page";
        private bool _HideSortOption = false;

        #region Properties
        public bool IsCommentsEnabled
        {
            get { return QuestionAnswerHelper.IsQuestionAnswerCommentEnabled(g.Brand); }
        }

        public bool IsSortEnabled
        {
            get { return QuestionAnswerHelper.IsQuestionAnswerSortEnabled(g.Brand); }
        }

        public bool HideSortOption
        {
            get { return _HideSortOption; }
            set { _HideSortOption = value; }
        }

        public bool IsLoggedIn
        {
            get { return null != g.Member && g.Member.MemberID > 0; }
        }

        public bool IsMemberLikeEnabled
        {
            get { return MemberLikeHelper.Instance.IsMemberLikeEnabled(g.Brand); }
        }

        public int PageSize
        {
            get { return _PageSize; }
            set { _PageSize = value; }
        }
        public string WatermarkText
        {
            get
            {
                try
                {
                    Control w = LoadControl("../../../Framework/Ui/FormElements/WatermarkTextbox.ascx");
                    return g.GetResource("TXT_QUESTIONANSWER_WATERMARK", w).Trim();
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }
        public string TrackingParam { get; set; }
        public string QuestionUrlBase
        {
            get
            {
                if (DisplayMode == DisplayAnswersMode.recent)
                {
                   
                    return "/Applications/QuestionAnswer/Home.aspx?tabid=2&";
                }
                else
                    return "/Applications/QuestionAnswer/Question.aspx?" + WebConstants.URL_PARAMETER_NAME_QUESTION_ID + "=" + _Question.QuestionID.ToString() + "&";
            }
        }

        public string LastSort
        {
            get
            {
                if (string.IsNullOrEmpty(_lastSort) && IsSortEnabled)
                {
                    try
                    {
                        _lastSort = QuestionAnswerHelper.GetLastSortForMember(g.Brand, g.Member);
                    }
                    catch (Exception ex)
                    {
                        g.ProcessException(ex);
                    }
                }
                return _lastSort;
            }
        }

        public int QuestionId
        {
            get {
                if (_Question != null)
                    return _Question.QuestionID;
                else
                    return 0;
            }
        }

        public int MemberId
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.MemberID;
                }
                return 0;
            }
        }


        public DisplayAnswersMode DisplayMode { get; set; }
        public bool TabbedDisplay { get; set; }
        public string MemberUserName
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.GetUserName(g.Brand);
                }
                return string.Empty;
            }
        }

        public bool IsPayingMember
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.IsPayingMember(g.Brand.Site.SiteID);
                }
                return false;
            }
        }

        public BreadCrumbHelper.EntryPoint EntryPoint
        {
            get { return _entryPoint; }
            set { _entryPoint = value; }
        }

        public string Location { 
            get { return _location; }
            set { _location = value; }
        }
        #endregion

        #region Event Handlers
        public string GetResourceForJS(string resConst)
        {
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, this));
        }

        public string GetRedirectUrl(int prtId)
        {
            string redirectUrl = string.Empty;
            if (g.Member != null && g.Member.MemberID > 0)
            {
                if (!g.Member.IsPayingMember(g.Brand.Site.SiteID))
                {
                    Matchnet.Content.ServiceAdapters.Links.ParameterCollection parameterCollection = new Matchnet.Content.ServiceAdapters.Links.ParameterCollection();
                    if (_g.ImpersonateContext)
                    {
                        parameterCollection.Add(WebConstants.IMPERSONATE_MEMBERID, _g.TargetMemberID.ToString());
                        parameterCollection.Add(WebConstants.IMPERSONATE_BRANDID, _g.TargetBrandID.ToString());
                    }
                    redirectUrl = Framework.Util.Redirect.GetSubscriptionUrl(_g.Brand, prtId, g.Member.MemberID, false, HttpContext.Current.Server.UrlEncode(HttpContext.Current.Items["FullURL"].ToString()), parameterCollection);
                }
            }
            return redirectUrl;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void repeaterAnswers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Answer answer = e.Item.DataItem as Answer;
                AnswerProfile miniAnswerProfile = e.Item.FindControl("AnswerProfile1") as AnswerProfile;
                miniAnswerProfile.Ordinal = _StartRow + _Counter;
                miniAnswerProfile.TrackingParam = TrackingParam;
                miniAnswerProfile.DisplayMode = DisplayMode;
                miniAnswerProfile.Location = Location;
                miniAnswerProfile.LoadAnswerProfile(answer);
                _Counter++;
            }
        }

        #endregion

        #region Public methods
        public void LoadAnswers(Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question)
        {
            _Question = question;
            phAnswerProfile.Visible = false;
            IComparer<Answer> comparer = GetComparer(Request);
            Answer[] approvedAnswers = QuestionAnswerHelper.GetApprovedAnswers(question, g.Member.MemberID, g, comparer);
            load(approvedAnswers,comparer);
            if (approvedAnswers.Length <= 0)
            {
                Answer memberAnswer = QuestionAnswerHelper.GetMemberAnswer(g.Member.MemberID, question);
                if (memberAnswer == null)
                {
                    phNoAnswers.Visible = true;
                }
            }
            //if (TabbedDisplay)
            //{
            //    _location = "Home Question";
            //}
        }
        public void Load()
        {
            try
            {
                phAnswerProfile.Visible = false;
                IComparer<Answer> comparer = GetComparer(Request);
                List<Answer> list = Matchnet.PremiumServiceSearch.ServiceAdapters.QuestionAnswerSA.Instance.GetRecentAnswerList(g.Brand.Site.SiteID, QuestionAnswerHelper.GetAnswerListDaysBack(g.Brand.Site.SiteID), false);
                Answer[] approvedAnswers = QuestionAnswerHelper.GetApprovedAnswers(list, g.Member.MemberID, g, comparer);
                load(approvedAnswers, comparer);
                if (DisplayMode==DisplayAnswersMode.recent)
                    phAnswersFromMember.Visible = false;

               

            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }

        private void load(Answer[] approvedAnswers, IComparer<Answer> comparer)
        {
            try
            {
               
               
                if (comparer is YourMatchesAnswerComparer)
                {
                    Txt txtNoYourMatchesAnswers = this.FindControl("txtNoYourMatchesAnswers") as Txt;
                    txtNoYourMatchesAnswers.Visible = !((YourMatchesAnswerComparer)comparer).HasYourMatches;
                }

                _TotalAnswers = approvedAnswers.Length;
                if (1 == _TotalAnswers)
                {
                    Txt2.ResourceConstant = "TXT_ONE_ANSWER";
                }

                //Get StartRow (if user clicked on paging)
                if (!String.IsNullOrEmpty(Request.QueryString["StartRow"]))
                {
                    int.TryParse(Request.QueryString["StartRow"], out _StartRow);
                    if (_StartRow < 0)
                        _StartRow = 0;
                }

                // if the start row is greater than the totalrows, set the startrow to the nearest round page_size less than totalrows
                if (_StartRow > _TotalAnswers)
                {
                    _StartRow = (_TotalAnswers / _PageSize) * _PageSize + 1;
                }

                if (approvedAnswers.Length > 0)
                {
                    phAnswerProfile.Visible = true;

                    if (approvedAnswers.Length > _PageSize)
                    {
                        //load paging
                        PagingHelper.LoadPageNavigation(lblListNavigationTop, lblListNavigationBottom, QuestionUrlBase, approvedAnswers.Length, _ChapterSize, _PageSize, _StartRow, g.GetResource("TXT_NEXT"), g.GetResource("TXT_PREVIOUS"));
                    }

                    //load answer profiles
                    repeaterAnswers.DataSource = approvedAnswers.Skip(_StartRow - 1).Take(_PageSize);
                    repeaterAnswers.DataBind();
                }
               

            }
            catch (Exception ex)
            { throw(ex); }

        }

        #endregion

        private IComparer<Answer> GetComparer(HttpRequest request)
        {
            if (!IsSortEnabled || HideSortOption) return null;
            string sortBy = request["sort"];
            bool reverseSort = (!string.IsNullOrEmpty(request["sortd"]) && Boolean.TrueString.ToLower().Equals(request["sortd"].ToString().ToLower()));
            string sortCriteria = (string.IsNullOrEmpty(sortBy))?null:sortBy + "|" + reverseSort.ToString().ToLower();
            bool changeSort = (!string.IsNullOrEmpty(sortCriteria) && !sortCriteria.Equals(LastSort));
            IComparer<Answer> comparer = QuestionAnswerHelper.GetAnswerComparerFromCriteria(sortCriteria, LastSort, g);
            if (changeSort) _lastSort = null;
            return comparer;
        }

    }
}
