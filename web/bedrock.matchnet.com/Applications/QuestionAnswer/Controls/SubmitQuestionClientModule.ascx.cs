﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.QuestionAnswer.Controls
{
    public partial class SubmitQuestionClientModule : FrameworkControl
    {
        private string _currentUrl = string.Empty;
        private int _Ordinal = 0;

        public BreadCrumbHelper.EntryPoint MyEntryPoint { get; set; }
        public string TrackingParam { get; set; }

        public string CurrentUrl
        {
            get
            {
                return _currentUrl;
            }
        }

        public int Ordinal
        {
            get { return _Ordinal; }
            set { _Ordinal = value; }
        }

        public bool IsEnabled
        {
            get
            {
                return QuestionAnswerHelper.IsQuestionAnswerMemberQuestionsEnabled(g.Brand);
            }
        }

        public string GetResourceForJS(string resConst)
        {
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, this));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.literalSendQuestion.Text = string.Format(g.GetResource("TXT_SEND_QUESTION_LINK", this), Ordinal);
        }
    }
}