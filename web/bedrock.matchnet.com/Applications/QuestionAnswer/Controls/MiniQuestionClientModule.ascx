﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniQuestionClientModule.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.MiniQuestionClientModule" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="qa" TagName="WatermarkTextbox" Src="~/Framework/Ui/FormElements/WatermarkTextbox.ascx" %>
<%@ Register TagPrefix="ml" TagName="MemberLikeAnswerClientModule" Src="~/Applications/MemberLike/MemberLikeAnswerClientModule.ascx" %>

 <%-- <h2><mn:txt id="Txt1" runat="server" resourceconstant="TXT_ANSWER_HEADER" expandimagetokens="true" /></h2>
  <span class="answer-popup-close"><mn:txt id="Txt2" runat="server" resourceconstant="TXT_CLOSE_BOX" expandimagetokens="true" /></span>--%>

<dl class="q-module clearfix">

    <asp:PlaceHolder ID="phConfirmationForNewQuestion" runat="server" Visible="false">
        <div class="notification">
            <%--This feature is not available yet, but this will be a placeholder for it. ER-385. Also, while this copy should be in a resource, that link needs to be dynamic to link to the appropriate question page. Maybe break it up into two resources, though that takes away from flexibility in the copy. --%>
            Thanks for answering! While your post is being reviewed, <a href="#">see what other people are saying.</a>
        </div>
    </asp:PlaceHolder>

	<dt class="question">
		<mn2:FrameworkLiteral ID="literalA" runat="server" ResourceConstant="TXT_Q"></mn2:FrameworkLiteral> 
		<asp:HyperLink ID="lnkQuestion" runat="server"><asp:Literal ID="literalQuestionText" runat="server"></asp:Literal></asp:HyperLink>
	</dt>
	
	<asp:PlaceHolder ID="phNotAnswered" runat="server" visible="false">
	    <!--member did not answer-->



	    <dd class="answer">
		    <mn2:FrameworkLiteral ID="FrameworkLiteral1" runat="server" ResourceConstant="TXT_A"></mn2:FrameworkLiteral>
		    <div class="answer-input">
		        <qa:WatermarkTextbox ID="txtAnswer1" runat="server" TextMode="MultiLine" CssClass="filled" WatermarkCssClass="watermark" MaxLength="3000" WatermarkTextResourceConstant="TXT_QUESTIONANSWER_WATERMARK" Visible="false"></qa:WatermarkTextbox>
	            <ul id="rblStockAnswers" class="radio-list clearfix answer-body" runat="server" Visible="false" />
	            <div class="answer-links" runat="server" id="divPostAnswer"><mn2:FrameworkButton ID="btnSubmitQuestion" runat="server" CssClass="submit-answer-popup btn secondary wide" ResourceConstant="TXT_ANSWER_QUESTION"  /></div>
		        <div id="divAnswerError"  class="notification error hide" runat="server"><mn:image ID="imgAnswerError" filename="page_error.gif" alt="" runat="server" /> <mn2:FrameworkLiteral ID="literalAnswerError" runat="server" ResourceConstant="TXT_EMPTY_ANSWER_ERROR"></mn2:FrameworkLiteral></div>
		         <div class="notification hide" runat="server" id="divPostAnswerSuccess" >
                  <asp:PlaceHolder ID="phPostAnswerSuccess" runat="server">
                    <mn:Image ID="imgAnsweredNotification" runat="server" FileName="icon-page-message.gif" />
                    <mn2:FrameworkLiteral ID="literalQuestionAnsweredApproval" runat="server" ResourceConstant="TXT_QUESTIONANSWEREDAPPROVAL"></mn2:FrameworkLiteral>
                  </asp:PlaceHolder>
                </div>
			</div>
	    </dd>
	</asp:PlaceHolder>
	
	<asp:PlaceHolder ID="phAnswered" runat="server">
	    <!--member already answered-->
	    <dd class="answer">
			<span class="a answered"><mn2:FrameworkLiteral ID="FrameworkLiteral2" runat="server" ResourceConstant="TXT_A"></mn2:FrameworkLiteral></span>
			<div class="member-answer clearfix">
			    <asp:Literal ID="literalMemberAnswer" runat="server" Visible="false"></asp:Literal>
			    <ul id="rblMemberAnswers" class="radio-list disabled clearfix answer-body" runat="server" Visible="false" />
			</div>
            <asp:PlaceHolder ID="phApprovalNotification" runat="server" Visible="false">
            
            </asp:PlaceHolder>            
            <ml:MemberLikeAnswerClientModule ID="memberLikeClientModule" runat="server" Enabled="true"/>
		</dd>
<% if(!rblMemberAnswers.Visible) { %>
<script type="text/javascript">

</script>
<% } %>
	</asp:PlaceHolder>
	
	<asp:HiddenField ID="hidQuestionID" runat="server" />
</dl>