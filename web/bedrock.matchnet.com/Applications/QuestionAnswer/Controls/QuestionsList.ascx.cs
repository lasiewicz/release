﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using QAS=Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.Web.Applications.QuestionAnswer;

using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Ui;
namespace Matchnet.Web.Applications.QuestionAnswer.Controls
{
    public partial class QuestionsList : FrameworkControl, iTab
    {
        private string _qFormat = "";
        string _sort = "recent";
        private string _location = "QnA Home tab2";
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        
        public int PageSize { get; set; }
        public int ChapterSize { get; set; }
        int _Counter = 0;
        int _Total;
        int _StartRow = 0;

        string _urlBase = "/Applications/QuestionAnswer/Home.aspx?TabId=3&sort={0}&";
        public string URLBase{get{return _urlBase;} set{URLBase=value;}}
       
        protected void Page_Load(object sender, EventArgs e)
        {


        }

        public void Load()
        {
            try
            {
                if (!String.IsNullOrEmpty(Request["sort"]))
                    _sort = Request["sort"];

                QAS.QuestionListSort eSort=QAS.QuestionListSort.recent;
                try
                {
                   eSort=(QAS.QuestionListSort)Enum.Parse(typeof(QAS.QuestionListSort),_sort);
                }catch(Exception ex1)
                {}

                if (!String.IsNullOrEmpty(Request.QueryString["StartRow"]))
                {
                    int.TryParse(Request.QueryString["StartRow"], out _StartRow);
                    if (_StartRow < 0)
                        _StartRow = 0;
                }

                List<QAS.Question> lst = QuestionAnswerSA.Instance.GetActiveQuestionList(g.Brand.Site.SiteID,PageSize, _StartRow,eSort, out _Total);

                _urlBase = string.Format(_urlBase, _sort);
                if (_sort.ToLower() == "az")
                {   litClassSortAZ.Text = "selected";
                 litClassSortAZ1.Text = "selected";
                }
                else
                {   litClassSortRecent.Text = "selected";
                    litClassSortRecent1.Text = "selected";
                 }
                rptQuestions.DataSource = lst;
              
                rptQuestions.DataBind();
                
                // if the start row is greater than the totalrows, set the startrow to the nearest round page_size less than totalrows
                if (_StartRow > _Total)
                {
                    _StartRow = (_Total / PageSize) * PageSize + 1;
                }

                if (_Total > PageSize)
                {

                    //load paging
                    PagingHelper.LoadPageNavigation(lblListNavigationTop, lblListNavigationBottom, URLBase, _Total, ChapterSize, PageSize, _StartRow, g.GetResource("TXT_NEXT"), g.GetResource("TXT_PREVIOUS"));

                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }


        }
        private void rptQuestions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
           QAS.Question   q = (QAS.Question)e.Item.DataItem;

            HyperLink lnkQuestionTab = (HyperLink)e.Item.FindControl("lnkQuestionTab");

            lnkQuestionTab.NavigateUrl = String.Format("/Applications/QuestionAnswer/Home.aspx?TabId=1&questid={0}",q.QuestionID);
            Txt qtext = (Txt)e.Item.FindControl("txtQuestion");
            //qtext.Text = g.GetResource("TXT_Q", this, new string[] { q.Text });
            qtext.Text = q.Text;
        }



        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rptQuestions.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptQuestions_ItemDataBound);
            
        }
        #endregion
    }
}