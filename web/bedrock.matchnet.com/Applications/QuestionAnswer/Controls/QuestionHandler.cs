﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Applications.MemberLike;
using Matchnet.Web.Framework.Ui.FormElements;


namespace Matchnet.Web.Applications.QuestionAnswer.Controls
{
    public class QuestionHandler
    {
        private ContextGlobal _g;
        private Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question _question;
        private QuestionAnswerEnums.QuestionType _questionType;
        private FrameworkControl _resourceControl;
        private Control _pageControl;
        private string _answerElement;
        private string _answerElementId;
        private string _answerText;
        public string AnswerElement
        {
            get { return _answerElement; }
        }

        public string AnswerText
        {
            get { return _answerText; }
        }

        public string AnswerElementId
        {
            get { return _answerElementId; }
        }

        public void Init(ContextGlobal g, Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question, FrameworkControl resourceControl, Control pageControl)
        {
            this._g = g;
            this._question = question;
            this._resourceControl = resourceControl;
            this._pageControl = pageControl;
            int typeId = (_question != null && _question.QuestionTypeID > 0) ? _question.QuestionTypeID : 1;
            this._questionType = (QuestionAnswerEnums.QuestionType)Enum.Parse(typeof(QuestionAnswerEnums.QuestionType), typeId.ToString());
        }

        private void RegisterOmnitureClientJS(string inputClientId)
        {
            StringBuilder scriptBlock = new StringBuilder("<script type=\"text/javascript\">");
            scriptBlock.Append("(function() {\n");
            scriptBlock.Append("    __addToNamespace__('spark.qanda', {\n");
            scriptBlock.Append("        QuestionInputElement: '" + inputClientId + "',\n");
            scriptBlock.Append("        __init__: function() {}\n");
            scriptBlock.Append("    });\n");
            scriptBlock.Append("})();\n");
            scriptBlock.Append("</script>\n");
            this._pageControl.Page.RegisterClientScriptBlock("omniInputElementscript", scriptBlock.ToString());
        }

        public void DisplayUnAnsweredQuestion()
        {
            switch (_questionType)
            {
                case QuestionAnswerEnums.QuestionType.FreeText:
                    WatermarkTextbox txtAnswer1 = this._pageControl.FindControl("txtAnswer1") as WatermarkTextbox;
                    txtAnswer1.Visible = true;
                    _answerElement = string.Format("$j(\"#{0}\").val()", txtAnswer1.TextBox.ClientID.Replace(".", "\\\\."));
                    _answerText = _answerElement;
                    _answerElementId = txtAnswer1.TextBox.ClientID;
                    RegisterOmnitureClientJS(txtAnswer1.TextBox.ClientID);
                    break;
                case QuestionAnswerEnums.QuestionType.MultipleChoice:
                    HtmlGenericControl rblStockAnswers = _pageControl.FindControl("rblStockAnswers") as HtmlGenericControl;
                    if (null != rblStockAnswers)
                    {
                        rblStockAnswers.Controls.Clear();
                        HiddenField radioValue = new HiddenField();
                        HiddenField radioText = new HiddenField();
                        radioValue.ID = "rval";
                        radioText.ID = "rtext";
                        rblStockAnswers.Controls.Add(radioValue);
                        rblStockAnswers.Controls.Add(radioText);
                        foreach (StockAnswer stockAnswer in _question.StockAnswers)
                        {
                            RadioButton radioButton = new RadioButton();
                            radioButton.Text = stockAnswer.AnswerValue;
                            radioButton.GroupName = "rblStockAnswers";
                            string setRadioText = "$j(\"#" + radioText.ClientID.Replace(".", "\\\\.") + "\").val(\"" + stockAnswer.AnswerValue + "\");";
                            radioButton.InputAttributes.Add("onClick", "$j(\"#" + radioValue.ClientID.Replace(".", "\\\\.") + "\").val(" + stockAnswer.StockAnswerID + ");" + setRadioText );

                            HtmlGenericControl radioSpan = new HtmlGenericControl("li");
                            radioSpan.Attributes.Add("class", "radio-item");
                            radioSpan.Controls.Add(radioButton);
                            
                            rblStockAnswers.Controls.Add(radioSpan);                            
                        }
                        rblStockAnswers.Visible = true;
                        _answerElement = string.Format("$j(\"#{0}\").val()", radioValue.ClientID.Replace(".", "\\\\."));
                        _answerElementId = radioValue.ClientID;
                        _answerText=  string.Format("$j(\"#{0}\").val()", radioText.ClientID.Replace(".", "\\\\."));
                    }
                    RegisterOmnitureClientJS(rblStockAnswers.ClientID);
                    break;
            }
        }

        public void DisplayAnsweredQuestion(Answer memberAnswer)
        {
            if (null == memberAnswer) return;
            PlaceHolder phApprovalNotification = _pageControl.FindControl("phApprovalNotification") as PlaceHolder;
            HyperLink lnkEditQuestion = _pageControl.FindControl("lnkEditQuestion") as HyperLink;
            switch (_questionType)
            {
                case QuestionAnswerEnums.QuestionType.FreeText:
                    Literal literalMemberAnswer = _pageControl.FindControl("literalMemberAnswer") as Literal;
                    literalMemberAnswer.Visible = true;
                    if (memberAnswer.AnswerStatus == QuestionAnswerEnums.AnswerStatusType.Pending)
                    {
                        phApprovalNotification.Visible = true;
                        literalMemberAnswer.Text = memberAnswer.AnswerValuePending.Replace(Environment.NewLine, "<br />");
                    }
                    else
                    {
                        phApprovalNotification.Visible = false;
                        literalMemberAnswer.Text = memberAnswer.AnswerValue.Replace(Environment.NewLine, "<br />");
                    }
                    if (null != lnkEditQuestion)
                    {
                        lnkEditQuestion.CssClass = "edit-inline";
                    }
                    break;
                case QuestionAnswerEnums.QuestionType.MultipleChoice:
                    phApprovalNotification.Visible = false;
                    HtmlGenericControl rblMemberAnswers = _pageControl.FindControl("rblMemberAnswers") as HtmlGenericControl;
                    if (null != rblMemberAnswers)
                    {
                        rblMemberAnswers.Controls.Clear();
                        foreach (StockAnswer stockAnswer in _question.StockAnswers)
                        {
                            RadioButton radioButton = new RadioButton();
                            radioButton.Text = stockAnswer.AnswerValue;
                            radioButton.GroupName = "rblMemberAnswers";

                            if (memberAnswer.AnswerValue.Equals(stockAnswer.AnswerValue))
                            {
                                radioButton.Checked = true;
                            }
                            else
                            {
                                radioButton.InputAttributes.Add("disabled", "disabled");
                            }

                            HtmlGenericControl radioSpan = new HtmlGenericControl("li");
                            radioSpan.Attributes.Add("class", "radio-item");

                            radioSpan.Controls.Add(radioButton);
                            rblMemberAnswers.Controls.Add(radioSpan);
                        }
                        rblMemberAnswers.Visible = true;
                    }
                    if (null != lnkEditQuestion)
                    {
                        lnkEditQuestion.CssClass = "edit-multiple";
                    }
                    break;
            }
        }

        public string GetAnswerText(HttpRequest request)
        {
            string answerText = string.Empty;
            if (null != _question)
            {
                switch (_questionType)
                {
                    case QuestionAnswerEnums.QuestionType.FreeText:
                        WatermarkTextbox txtAnswer1 = this._pageControl.FindControl("txtAnswer1") as WatermarkTextbox;
                        answerText = request.Form[txtAnswer1.TextBox.UniqueID];
                        break;
                    case QuestionAnswerEnums.QuestionType.MultipleChoice:
                        HtmlGenericControl rblStockAnswers = _pageControl.FindControl("rblStockAnswers") as HtmlGenericControl;
                        int stockAnswerID = Conversion.CInt(request.Form[rblStockAnswers.Parent.NamingContainer.UniqueID+ ":rval"]);
                        foreach (StockAnswer stockAnswer in _question.StockAnswers)
                        {
                            if (stockAnswer.StockAnswerID == stockAnswerID)
                            {
                                answerText = stockAnswer.AnswerValue;
                                break;
                            }
                        }
                        break;
                }
            }
            return answerText;
        }

        /// <summary>
        /// This is a temporary method to retrieve question answers 
        /// based on UI templates (question.1.tmpl.html, question.2.tmpl.html) 
        /// that has hard coded ID/name for controls
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public string GetAnswerTextUITemplate(HttpRequest request)
        {
            string answerText = string.Empty;
            if (null != _question)
            {
                switch (_questionType)
                {
                    case QuestionAnswerEnums.QuestionType.FreeText:
                        answerText = request.Form["_ctl0:_ctl6:MiniQuestionModule1:txtAnswer1:WatermarkTextBox1"];
                        break;
                    case QuestionAnswerEnums.QuestionType.MultipleChoice:
                        int stockAnswerID = Conversion.CInt(request.Form["_ctl0:_ctl6:MiniQuestionModule1:rval"]);
                        foreach (StockAnswer stockAnswer in _question.StockAnswers)
                        {
                            if (stockAnswer.StockAnswerID == stockAnswerID)
                            {
                                answerText = stockAnswer.AnswerValue;
                                break;
                            }
                        }
                        break;
                }
            }
            return answerText;
        }
    }
}
