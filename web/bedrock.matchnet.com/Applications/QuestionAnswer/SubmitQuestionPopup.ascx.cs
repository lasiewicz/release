﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.PageElements;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Applications.QuestionAnswer
{
    public partial class SubmitQuestionPopup : FrameworkControl
    {
        private int likeObjectId = Constants.NULL_INT;
        private int likeObjectTypeId = Constants.NULL_INT;
        private string _currentUrl = string.Empty;

        public BreadCrumbHelper.EntryPoint MyEntryPoint { get; set; }
        public string TrackingParam { get; set; }
        public string CurrentUrl
        {
            get
            {
                return _currentUrl;
            }
        }

        public string ElementId { get; set; }

        public string GetResourceForJS(string resConst)
        {
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, this));
        }

        public int MemberId
        {
            get
            {
                if (null != g.Member && g.Member.MemberID > 0)
                {
                    return g.Member.MemberID;
                }
                return 0;
            }
        }

        public int BrandId
        {
            get
            {
                if (null != g.Brand)
                {
                    return g.Brand.BrandID;
                }
                return 0;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return QuestionAnswerHelper.IsQuestionAnswerMemberQuestionsEnabled(g.Brand);
            }
        }

        #region Event handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["loid"]))
            {
                likeObjectId = Convert.ToInt32(Request["loid"]);
            }
            if (!string.IsNullOrEmpty(Request["lotid"]))
            {
                likeObjectTypeId = Convert.ToInt32(Request["lotid"]);
            }
            if (!string.IsNullOrEmpty(Request["eid"]))
            {
                ElementId = Request["eid"];
            }

            if (!string.IsNullOrEmpty(Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT]))
            {
                try
                {
                    MyEntryPoint = (BreadCrumbHelper.EntryPoint)Enum.Parse(typeof(BreadCrumbHelper.EntryPoint), Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT]);
                }
                catch (Exception ignore){}
            }
        }
        #endregion
    }
}
