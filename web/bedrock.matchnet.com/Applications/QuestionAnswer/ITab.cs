﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Web.Applications.QuestionAnswer
{
    public interface iTab
    {
        void Load();
        string Location{get; set;}
    }
}
