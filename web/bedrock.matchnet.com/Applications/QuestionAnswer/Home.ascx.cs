﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
namespace Matchnet.Web.Applications.QuestionAnswer
{
    public partial class Home : FrameworkControl
    {
        private int _tabid;
        List<TabUI> _tabs;

        protected void Page_Load(object sender, EventArgs e)
        {
            _tabid =Conversion.CInt( Request["tabid"]);
            if (_tabid <= 0) _tabid = 1;
            InitTabs();
        }

        private void InitTabs()
        {
            try
            {
                _tabs = new List<TabUI>();
                TabUI tab1 = new TabUI(phQnATab1, litClassQnATab1, ucQuestion);
                tab1.AddOmnitureKeyValue("pageName", "QuestionAnswer – Home Question");
                tab1.AddOmnitureKeyValue("prop38", "QnA Home tab1");
                tab1.TabControl.Location = "QnA Home tab1";
                _tabs.Add(tab1);
                TabUI tab2 = new TabUI(phQnATab2, litClassQnATab2, ucAnswerModule);
                tab2.AddOmnitureKeyValue("pageName", "QuestionAnswer - Home Recent");
                tab2.AddOmnitureKeyValue("prop38", "QnA Home tab2");
                tab2.TabControl.Location = "QnA Home tab2";
                _tabs.Add(tab2);
                TabUI tab3 = new TabUI(phQnATab3, litClassQnATab3, ucQuestionList);
                tab3.AddOmnitureKeyValue("pageName", "QuestionAnswer - Home AllQuestions");
                tab3.AddOmnitureKeyValue("prop38", "QnA Home tab3");
                tab3.TabControl.Location = "QnA Home tab3";
                _tabs.Add(tab3);

                _tabs[_tabid - 1].SetActive();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
        
        protected override void OnPreRender(System.EventArgs e)
        {
            TabUI activeTab = _tabs.First<TabUI>();
            foreach (TabUI tab in _tabs)
            {
                if (tab.IsActive)
                {
                    activeTab = tab;
                    break;
                }
            }
            g.AnalyticsOmniture.Prop38 = activeTab.GetOmnitureValue("prop38");
            g.AnalyticsOmniture.PageName = activeTab.GetOmnitureValue("pageName");
        }

        public string GetResourceForJS(string resConst)
        {
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, this));
        }
       
    }


    public class TabUI
    {
        public PlaceHolder TabPlaceHolder { set; get; }
        public Literal TabClassLiteral { set; get; }
        public iTab TabControl { get; set; }
        public bool IsActive { get; set; }
        private Dictionary<string, string> OmnitureMap = new Dictionary<string, string>();

        public TabUI(PlaceHolder ph, Literal lit, iTab tab)
        {
            TabPlaceHolder = ph;
            TabClassLiteral = lit;
            TabControl = tab;

        }

        public void SetActive()
        {
            TabPlaceHolder.Visible = true;
            TabClassLiteral.Text = "selected";
            TabControl.Load();
            IsActive = true;
        }
        public void SetInActive()
        {
            TabPlaceHolder.Visible = false;
            TabClassLiteral.Text = "";
            IsActive = false;
        }

        public void AddOmnitureKeyValue(string key, string value)
        {
            OmnitureMap.Add(key, value);
        }

        public string GetOmnitureValue(string key)
        {
            string value = string.Empty;
            if (OmnitureMap.ContainsKey(key))
            {
                value = OmnitureMap[key];
            }
            return value;
        }
    }
}