﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.MembersOnline.ValueObjects;

using Matchnet.Session.ValueObjects;
using Matchnet.Web.Applications.MembersOnline.Controls;

namespace Matchnet.Web.Applications.MembersOnline
{
    public interface IMembersOnline
    {
        HtmlInputHidden HIDStartRow { get;}
        string ChangeVoteMessage { get; set; }
        Matchnet.Web.Framework.Ui.BasicElements.IResultList MOLResultList { get; }
        Matchnet.Web.Framework.Image IconViewType { get; }
        HyperLink LNKViewType { get; }
        Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BTNSearch { get; }
        DropDownList MemberSortDropDown { get; set; }
        IMembersOnlineFilter MOLFilter { get; }
        Matchnet.Web.Framework.Txt TXTViewBy { get; }
        Panel PNLReviseLinks { get; }
        Matchnet.Web.Framework.Title TTLOnlineNow { get; }
        Matchnet.Web.Framework.Txt TXTViewAsMOL { get; }
        PlaceHolder PhSortBy { get; }
      
        HtmlInputHidden HIDGalleryView { get; }
        HtmlInputHidden HIDResultViewChanged { get; }

        Label LBLListNavigationTop { get; }
        Label LBLListNavigationBottom { get; }
        FrameworkControl ThisControl { get; }

        Txt TXTListOff { get; }
        Txt TXTGalleryOff { get; }
        
    }
}
