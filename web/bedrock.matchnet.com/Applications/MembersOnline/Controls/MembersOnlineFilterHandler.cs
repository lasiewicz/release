﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
#endregion

#region Matchnet Service References
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Applications.Search;
using Spark.SAL;
using System.Text;
#endregion

namespace Matchnet.Web.Applications.MembersOnline.Controls
{
    public class MembersOnlineFilterHandler : FrameworkControl
    {
        IMembersOnlineFilter _filterControl = null;
        ContextGlobal g;
        public MembersOnlineFilterHandler(ContextGlobal context,IMembersOnlineFilter filter)
        {
            _filterControl = filter;
            g = context;

        }


        public int intRegionID
        {
            get
            {
                //down and dirty fix for TT#16325
                if (Conversion.CInt(_filterControl.SELRegionID.Value) <= 0)
                {
                    string molRegionID = g.Session["MembersOnlineRegion"].ToString();
                    if (molRegionID != null && molRegionID != String.Empty)
                    {
                        _filterControl.SELRegionID.Value = molRegionID;
                    }
                }
                return Matchnet.Conversion.CInt(_filterControl.SELRegionID.Value);
            }
        }

        public DataTable GetLanguageOptions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("Value", typeof(System.Int32));

            dt.Rows.Add(new object[] { g.GetResource("LT_ANY_LANGUAGE_GT", _filterControl.ThisControl), Constants.NULL_INT });

            DataTable dtOptions = Option.GetOptions("LanguageMask", g);
            foreach (DataRow row in dtOptions.Rows)
            {
                dt.Rows.Add(new object[] { row["Content"], row["Value"] });
            }

            return dt;
        }

        public DataTable GetRegionOptions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("Value", typeof(System.Int32));


            if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Cupid)
            {
                if (g.Member != null)
                {
                    string SessionRegionID = g.Session[WebConstants.ATTRIBUTE_NAME_MOLREGIONID].ToString();
                    if (SessionRegionID != String.Empty)
                    {
                        dt.Rows.Add(new object[] { FrameworkGlobals.GetUnicodeText(g.GetResource("YOUR_REGION", _filterControl.ThisControl)), SessionRegionID });
                    }
                    else
                    {
                        dt.Rows.Add(new object[] { FrameworkGlobals.GetUnicodeText(g.GetResource("YOUR_REGION", _filterControl.ThisControl)), g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID) });
                        g.Session.Add(WebConstants.ATTRIBUTE_NAME_MOLREGIONID, g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID).ToString(), SessionPropertyLifetime.Temporary);
                    }
                }
                dt.Rows.Add(new object[] { FrameworkGlobals.GetUnicodeText(g.GetResource("ALL_OF_ISRAEL", _filterControl.ThisControl)), ConstantsTemp.REGIONID_ISRAEL });
                dt.Rows.Add(new object[] { FrameworkGlobals.GetUnicodeText(g.GetResource("ANYWHERE_CUPID", _filterControl.ThisControl)), Constants.NULL_INT });
            }
            else
            {
                dt.Rows.Add(new object[] { g.GetResource("ANYWHERE", _filterControl.ThisControl), Constants.NULL_INT });
                if (g.Member != null)
                {
                    if (g.Session[WebConstants.ATTRIBUTE_NAME_MOLREGIONID] != String.Empty)
                    {
                        dt.Rows.Add(new object[] { g.GetResource("YOUR_REGION", _filterControl.ThisControl), g.Session[WebConstants.ATTRIBUTE_NAME_MOLREGIONID] });
                    }
                    else
                    {
                        dt.Rows.Add(new object[] { FrameworkGlobals.GetUnicodeText(g.GetResource("YOUR_REGION", _filterControl.ThisControl)), g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID) });
                        g.Session.Add(WebConstants.ATTRIBUTE_NAME_MOLREGIONID, g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID).ToString(), SessionPropertyLifetime.Temporary);
                    }
                }

                if (g.Brand.Site.Community.CommunityID != (Int32)WebConstants.COMMUNITY_ID.JDate)
                {
                    dt.Rows.Add(new object[] { g.GetResource("AUSTRALIA", _filterControl.ThisControl), ConstantsTemp.REGIONID_AUSTRALIA });
                }
                if (g.Brand.Site.SiteID == (Int32)WebConstants.SITE_ID.JDateFR)
                {
                    dt.Rows.Add(new object[] { g.GetResource("BELGIUM", _filterControl.ThisControl), ConstantsTemp.REGIONID_BELGIUM });
                }
                dt.Rows.Add(new object[] { g.GetResource("CANADA", _filterControl.ThisControl), ConstantsTemp.REGIONID_CANADA });
                if (g.Brand.Site.SiteID == (Int32)WebConstants.SITE_ID.JDateFR)
                {
                    dt.Rows.Add(new object[] { g.GetResource("FRANCE", _filterControl.ThisControl), ConstantsTemp.REGIONID_FRANCE });
                }
                if (g.Brand.Site.Community.CommunityID != (Int32)WebConstants.COMMUNITY_ID.JDate)
                {
                    dt.Rows.Add(new object[] { g.GetResource("GERMANY", _filterControl.ThisControl), ConstantsTemp.REGIONID_GERMANY });
                }
                dt.Rows.Add(new object[] { g.GetResource("ISRAEL", _filterControl.ThisControl), ConstantsTemp.REGIONID_ISRAEL });
                if (g.Brand.Site.SiteID == (Int32)WebConstants.SITE_ID.JDateFR)
                {
                    dt.Rows.Add(new object[] { g.GetResource("LUXEMBURG", _filterControl.ThisControl), ConstantsTemp.REGIONID_LUXEMBURG });
                }
                dt.Rows.Add(new object[] { g.GetResource("UNITED_KINGDOM", _filterControl.ThisControl), ConstantsTemp.REGIONID_UNITED_KINGDOM });
                dt.Rows.Add(new object[] { g.GetResource("UNITED_STATES", _filterControl.ThisControl), ConstantsTemp.REGIONID_USA });
                if (g.Brand.Site.SiteID == (Int32)WebConstants.SITE_ID.JDateFR)
                {
                    dt.Rows.Add(new object[] { g.GetResource("SWITZERLAND", _filterControl.ThisControl), ConstantsTemp.REGIONID_SWITZERLAND });
                }
            }

            return dt;
        }

        public DataTable GetGenderOptions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("Value", typeof(System.Int16));

            switch (g.Brand.Site.Community.CommunityID)
            {
                case (int)WebConstants.COMMUNITY_ID.JDate:
                    dt.Rows.Add(new object[] { g.GetResource("EVERYONE", _filterControl.ThisControl), -1 });
                    dt.Rows.Add(new object[] { g.GetResource("WOMEN", _filterControl.ThisControl), 0 });
                    dt.Rows.Add(new object[] { g.GetResource("MEN", _filterControl.ThisControl), 1 });
                    break;

                default:
                    dt.Rows.Add(new object[] { g.GetResource("EVERYONE", _filterControl.ThisControl), -1 });
                    dt.Rows.Add(new object[] { g.GetResource("WOMEN_SEEKING_MEN", _filterControl.ThisControl), 0 });
                    dt.Rows.Add(new object[] { g.GetResource("MEN_SEEKING_WOMEN", _filterControl.ThisControl), 1 });
                    dt.Rows.Add(new object[] { g.GetResource("WOMEN_SEEKING_WOMEN", _filterControl.ThisControl), 2 });
                    dt.Rows.Add(new object[] { g.GetResource("MEN_SEEKING_MEN", _filterControl.ThisControl), 3 });
                    break;
            }

            return dt;
        }


        public int GetGenderMask()
        {
            //fix for outstanding cupid.co.il bug #16325. Quick and dirty no need to spend 2 days finding
            //where value is lost for this site.  If this gets executed on any other site is will do no harm.
            //seems should jsut use this line of code without the if statment for all sites anyway.
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid && g.Brand.Site.Name == "Cupid.co.il")
            {
                string newValue = MembersOnlineManager.Instance.IsE2MOLEnabled(g.Brand) ? GetMOLGenderFromGenderMask(g.MembersOnlineSearchPreferences["gendermask"]).ToString() : g.Session["MembersOnlineGender"];
                if (newValue != null && newValue != string.Empty)
                {
                    _filterControl.SELGenderMask.Value = newValue;
                }
            }
            switch (Convert.ToInt32(_filterControl.SELGenderMask.Value))
            {
                case 0:
                    return (ConstantsTemp.GENDERID_FEMALE | ConstantsTemp.GENDERID_SEEKING_MALE);

                case 1:
                    return (ConstantsTemp.GENDERID_MALE | ConstantsTemp.GENDERID_SEEKING_FEMALE);

                case 2:
                    return (ConstantsTemp.GENDERID_FEMALE | ConstantsTemp.GENDERID_SEEKING_FEMALE);

                case 3:
                    return (ConstantsTemp.GENDERID_MALE | ConstantsTemp.GENDERID_SEEKING_MALE);

                default:
                    return Constants.NULL_INT;
            }
        }

        public int GetMOLGenderFromGenderMask(string genderMask)
        {
            //this takes the GenderMask (used by E2) and converts to gender used in dropdown
            if (!string.IsNullOrEmpty(genderMask))
            {

                if (genderMask == (ConstantsTemp.GENDERID_FEMALE | ConstantsTemp.GENDERID_SEEKING_MALE).ToString())
                {
                    return 0;
                }
                else if (genderMask == (ConstantsTemp.GENDERID_MALE | ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString())
                {
                    return 1;
                }
                else if (genderMask == (ConstantsTemp.GENDERID_FEMALE | ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString())
                {
                    return 2;
                }
                else if (genderMask == (ConstantsTemp.GENDERID_MALE | ConstantsTemp.GENDERID_SEEKING_MALE).ToString())
                {
                    return 3;
                }
            }

            return -1;
        }

        public void BindGender()
        {
            _filterControl.SELGenderMask.DataSource = GetGenderOptions();
            _filterControl.SELGenderMask.DataTextField = "Content";
            _filterControl.SELGenderMask.DataValueField = "Value";
            _filterControl.SELGenderMask.DataBind();

            string newValue = MembersOnlineManager.Instance.IsE2MOLEnabled(g.Brand) ? GetMOLGenderFromGenderMask(g.MembersOnlineSearchPreferences["gendermask"]).ToString() : g.Session["MembersOnlineGender"];
            if (newValue != null && newValue != string.Empty)
                _filterControl.SELGenderMask.Value = newValue;

           
        }

        public void BindDistance()
        {
            if (string.IsNullOrEmpty(g.MembersOnlineSearchPreferences["distance"]))
            {
                g.MembersOnlineSearchPreferences["distance"] = "1000";
            }
            DataTable distanceOptions = Option.GetOptions(SearchUtil.GetDistanceAttribute(g), g);

            _filterControl.DDLDistance.DataSource = distanceOptions;
            _filterControl.DDLDistance.DataTextField = "Content";
            _filterControl.DDLDistance.DataValueField = "Value";
            _filterControl.DDLDistance.DataBind();

            FrameworkGlobals.SelectItem(_filterControl.DDLDistance, g.MembersOnlineSearchPreferences["distance"], SettingsManager.GetSettingInt(SettingConstants.DEFAULT_DISTANCE_LIST_ORDER, g.Brand) - 1);
        }

        public void BindRegion()
        {

            _filterControl.SELRegionID.DataSource = GetRegionOptions();
            _filterControl.SELRegionID.DataTextField = "Content";
            _filterControl.SELRegionID.DataValueField = "Value";
            _filterControl.SELRegionID.DataBind();

            string molRegionID = g.Session["MembersOnlineRegion"].ToString();
            if (g.Member != null && molRegionID != String.Empty)
            {
                _filterControl.SELRegionID.SelectedIndex = _filterControl.SELRegionID.Items.IndexOf(_filterControl.SELRegionID.Items.FindByValue(molRegionID));
            }
            else if (g.Brand.Site.DefaultRegionID != Constants.NULL_INT)
            {

                if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Cupid)
                {//Cupid defaults to "Anywhere"
                    _filterControl.SELRegionID.SelectedIndex = _filterControl.SELRegionID.Items.IndexOf(_filterControl.SELRegionID.Items.FindByValue(Constants.NULL_INT.ToString()));
                }
                else
                {
                    _filterControl.SELRegionID.SelectedIndex = _filterControl.SELRegionID.Items.IndexOf(_filterControl.SELRegionID.Items.FindByValue(g.Brand.Site.DefaultRegionID.ToString()));
                }

            }
            if (molRegionID != null && molRegionID != string.Empty)
            {
                _filterControl.SELRegionID.Value = molRegionID;
            }
        }

        public void BindLocation(bool postBack)
        {
            SearchType searchType = (SearchType)Enum.Parse(typeof(SearchType), g.MembersOnlineSearchPreferences["SearchTypeID"].ToString());
            #region Bind PickRegion Control
            if (!postBack)
            {
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                {
                    // Initially select the Region tab when opening the Search
                    // Region popup in JDate.fr  
                    _filterControl.PrSearchRegion.SearchType = SearchType.Region;
                }
                else
                {
                    _filterControl.PrSearchRegion.SearchType = searchType;
                }
            }

            if (string.IsNullOrEmpty(g.MembersOnlineSearchPreferences["RegionID"]))
            {
                g.MembersOnlineSearchPreferences["RegionID"] = (g.Member != null && g.Member.MemberID > 0) ? g.Member.GetAttributeInt(g.Brand, "RegionID").ToString() : g.Brand.Site.DefaultRegionID.ToString();
            }

            _filterControl.PrSearchRegion.RegionID = Conversion.CInt(g.MembersOnlineSearchPreferences["RegionID"].ToString());
            _filterControl.PrSearchRegion.SchoolID = Conversion.CInt(g.MembersOnlineSearchPreferences["SchoolID"].ToString());

            Int32 countryRegionID = 0;

            StringBuilder sbAreaCodes = new StringBuilder();
            for (Int32 i = 1; i <= 6; i++)
            {
                string areaCode = g.MembersOnlineSearchPreferences["AreaCode" + i].ToString();
                if (!string.IsNullOrEmpty(areaCode) && Conversion.CInt(areaCode) > 0)
                {
                    countryRegionID = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveAreaCodes()[Conversion.CInt(areaCode)];
                    _filterControl.PrSearchRegion.AddAreaCode(areaCode, countryRegionID);
                    sbAreaCodes.Append(areaCode + ", ");
                }
            }
            if (sbAreaCodes.Length > 0)
            {
                sbAreaCodes.Remove(sbAreaCodes.Length - 2, 2);
            }

            _filterControl.PrSearchRegion.AreaCodeCountryRegionID = countryRegionID;
            #endregion

            _filterControl.LnkLocation.UpdateAfterCallBack = true;
            _filterControl.LnkLocation.Attributes.Add("onclick", "popUpDiv(\"searchPopupDiv\",\"3px\",\"-3px\");return false");
            _filterControl.LnkLocation.Attributes.Add("href", "#");

            _filterControl.PlcDistance.Visible = true;
            _filterControl.PlcDistance.UpdateAfterCallBack = true;
            _filterControl.PlcAreaCodes.Visible = false;
            _filterControl.PlcAreaCodes.UpdateAfterCallBack = true;
            _filterControl.PlcCollege.Visible = false;
            _filterControl.PlcCollege.UpdateAfterCallBack = true;

            switch (searchType)
            {
                case SearchType.PostalCode:
                case SearchType.Region:
                    _filterControl.LnkLocation.Text = FrameworkGlobals.GetRegionString(Conversion.CInt(g.MembersOnlineSearchPreferences["RegionID"]), g.Brand.Site.LanguageID);
                    break;
                case SearchType.AreaCode:
                    _filterControl.PlcDistance.Visible = false;
                    _filterControl.PlcAreaCodes.Visible = true;
                    _filterControl.LnkLocation.Text = sbAreaCodes.ToString();
                    break;
                case SearchType.College:
                    _filterControl.PlcDistance.Visible = false;
                    _filterControl.PlcCollege.Visible = true;
                    _filterControl.LnkLocation.Text = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveSchoolName(Conversion.CInt(g.MembersOnlineSearchPreferences["SchoolID"])).SchoolName;
                    break;
            }

            _filterControl.PrSearchRegion.RegionName = _filterControl.LnkLocation.Text;
            _filterControl.LnkLocation.Text += g.GetResource("EDIT", _filterControl.ThisControl);
        }

        public void BindLanguage()
        {
            if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Cupid)
            {
                _filterControl.PLCanguage.Visible = false;
            }
            else
            {
                _filterControl.PLCanguage.Visible = true;

                _filterControl.SELLanguageMask.DataSource = GetLanguageOptions();
                _filterControl.SELLanguageMask.DataTextField = "Content";
                _filterControl.SELLanguageMask.DataValueField = "Value";
                _filterControl.SELLanguageMask.DataBind();
            }


            string newValue = MembersOnlineManager.Instance.IsE2MOLEnabled(g.Brand) ? g.MembersOnlineSearchPreferences["languagemask"] : g.Session["MembersOnlineLanguage"];
            if (newValue != null && newValue != string.Empty)
            {
                _filterControl.SELLanguageMask.Value = newValue;
            }
        }

        public void SetAge()
        {
            // min age
            bool molConsolidated = MembersOnlineManager.Instance.IsE2MOLEnabled(g.Brand);
            string newValue;
            if (HttpContext.Current.Request["AgeMin"] != null)
            {
               _filterControl.TXTAgeMin.Value = HttpContext.Current.Request["AgeMin"];
            }
            else
            {
                newValue = molConsolidated ? g.MembersOnlineSearchPreferences["minage"] : g.Session["MembersOnlineAgeMin"];
                if (molConsolidated && string.IsNullOrEmpty(newValue))
                {
                    newValue = g.SearchPreferences["minage"];
                }

                if (newValue != null && newValue != string.Empty)
                {
                    _filterControl.TXTAgeMin.Value = newValue;
                }
                else
                {
                    _filterControl.TXTAgeMin.Value = g.Brand.DefaultAgeMin.ToString();
                }
            }

            if (HttpContext.Current.Request["AgeMax"] != null)
            {
                _filterControl.TXTAgeMax.Value = HttpContext.Current.Request["AgeMax"];
            }
            else
            {
                newValue = molConsolidated ? g.MembersOnlineSearchPreferences["maxage"] : g.Session["MembersOnlineAgeMax"];
                if (molConsolidated && string.IsNullOrEmpty(newValue))
                {
                    newValue = g.SearchPreferences["maxage"];
                }

                if (newValue != null && newValue != string.Empty)
                {
                    _filterControl.TXTAgeMax.Value = newValue;
                }
                else
                {
                    _filterControl.TXTAgeMax.Value = g.Brand.DefaultAgeMax.ToString();
                }
            }
			
					
        }
        public void DoValidation()
        {
            // Add min and max age validation
            string minAgeError = g.GetResource("MIN_AGE_NOTICE", _filterControl.ThisControl);
            string maxAgeError = g.GetResource("MAX_AGE_NOTICE", _filterControl.ThisControl);
            string greaterThanError = g.GetResource("GREATER_THAN_NOTICE", _filterControl.ThisControl);

            _filterControl.VALAgeMinRange.ErrorMessage = minAgeError;
            _filterControl.VALAgeMinRange.MinimumValue = ConstantsTemp.MIN_AGE_RANGE.ToString();
            _filterControl.VALAgeMinRange.MaximumValue = ConstantsTemp.MAX_AGE_RANGE.ToString();
            _filterControl.VALAgeMaxRange.ErrorMessage = maxAgeError;
            _filterControl.VALAgeMaxRange.MinimumValue = ConstantsTemp.MIN_AGE_RANGE.ToString();
            _filterControl.VALAgeMaxRange.MaximumValue = ConstantsTemp.MAX_AGE_RANGE.ToString();
            _filterControl.VALAgeMinReq.ErrorMessage = minAgeError;
            _filterControl.VALAgeMaxReq.ErrorMessage = maxAgeError;
            _filterControl.VALCompare.ErrorMessage = greaterThanError;

            _filterControl.VALAgeMinRange.Type = System.Web.UI.WebControls.ValidationDataType.Integer;
            _filterControl.VALAgeMaxRange.Type = System.Web.UI.WebControls.ValidationDataType.Integer;
            _filterControl.VALCompare.Type = System.Web.UI.WebControls.ValidationDataType.Integer;
        }

        public void SetSession()
        {
            try
            {
                if (MembersOnlineManager.Instance.IsE2MOLEnabled(g.Brand))
                {
                    g.MembersOnlineSearchPreferences["gendermask"] = _filterControl.GetGenderMask().ToString();
                    g.MembersOnlineSearchPreferences["minage"] = _filterControl.intAgeMin.ToString();
                    g.MembersOnlineSearchPreferences["maxage"] = _filterControl.intAgeMax.ToString();
                    g.MembersOnlineSearchPreferences["languagemask"] = _filterControl.intLanguageMask.ToString();
                    g.MembersOnlineSearchPreferences["distance"] = _filterControl.intDistance.ToString();

                    // if this is a photo required site, we want to set this search pref manually
                    if (Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                        g.Brand.Site.SiteID, g.Brand.BrandID)))
                    {
                        g.MembersOnlineSearchPreferences["hasphotoflag"] = "1";
                    }

                    g.Session.Add(WebConstants.SESSION_MOL_SEARCH, g.MembersOnlineSearchPreferences, SessionPropertyLifetime.Temporary);
                }
                else
                {
                    int expirationDays = Conversion.CInt(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MOL_CRITERIA_EXPIRATION", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID), 365);
                    g.Session.Add("MembersOnlineGender", _filterControl.SELGenderMask.Value, expirationDays, false);

                    g.Session.Add("MembersOnlineAgeMin", _filterControl.TXTAgeMin.Value, expirationDays, false);
                    g.Session.Add("MembersOnlineAgeMax", _filterControl.TXTAgeMax.Value, expirationDays, false);
                    g.Session.Add("MembersOnlineLanguage", _filterControl.SELLanguageMask.Value, expirationDays, false);
                    //g.Session.Add(WebConstants.ATTRIBUTE_NAME_MOLREGIONID, this.RegionID.Value, SessionPropertyLifetime.Temporary);

                    string regionSelect = _filterControl.SELRegionID.Value;
                    string regionSession = "";
                    if (g.Session.Get("MembersOnlineRegion") != null)
                        regionSession = g.Session.Get("MembersOnlineRegion").ToString();

                    g.Session.Add("MembersOnlineRegion", _filterControl.SELRegionID.Value, expirationDays, false);
                    if (regionSelect != regionSession)
                    {
                        g.Session.Add("MembersOnlineRegionChanged", "true", SessionPropertyLifetime.Temporary);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
