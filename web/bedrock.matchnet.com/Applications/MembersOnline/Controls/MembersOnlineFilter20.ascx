﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MembersOnlineFilter20.ascx.cs" Inherits="Matchnet.Web.Applications.MembersOnline.Controls.MembersOnlineFilter20" %>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnfe" TagName="PickRegionNew" Src="/Framework/Ui/FormElements/PickRegionNew.ascx" %>
<%@ Register TagPrefix="mnfe" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div class="filter">
<fieldset>
    <legend>Filter search results</legend>
	<label class="view">
		<mn:Txt runat="server" id="txtShow" resourceconstant="TXT_SHOW" />:<br />
		<select name="GenderMask" id="GenderMask" runat="server"></select>
	</label>

    <asp:placeholder runat="server" ID="plcRegion">
	<label class="from">
		<mn:Txt runat="server" id="txtFrom" resourceconstant="TXT_FROM" />:<br />
		<select name="RegionID" id="RegionID" runat="server"></select>
	</label>
    </asp:placeholder>

	<div class="age-container">
        <label class="age-from">
		    <mn:Txt runat="server" id="txtAges" resourceconstant="TXT_AGE" />:<br />
		    <input name="AgeMin" id="AgeMin" type="text" size="2" maxlength="2" runat="server" />
	    </label>
	    <label class="age-to">
		    &nbsp;<br /><mn:Txt runat="server" id="txtTo" resourceconstant="TO_" />
		    <input name="AgeMax" id="AgeMax" type="text" size="2" maxlength="2" runat="server" />
		</label>
	</div>
	
    <asp:placeholder runat="server" id="plcLanguage">
     <label class="speak">
			<mn:Txt id="txtWhoSpeak" resourceconstant="TXT_WHO_SPEAK" runat="server"></mn:Txt>:<br />
			<select id="LanguageMask" name="LanguageMask" runat="server"></select>
     </label>
    </asp:placeholder>

    <asp:PlaceHolder runat="server" ID="plcRegionE2" Visible="false">
        <label>
		    <anthem:PlaceHolder ID="plcDistance" Runat="server">
			    <mn:Txt id="txtWithin" ResourceConstant="TXT_LOCATED_WITHIN" runat="server"></mn:Txt><br />
                <asp:DropDownList id="ddlDistance" Runat="server"></asp:DropDownList>&nbsp; 
                <mn:Txt id="txtOf" ResourceConstant="TXT_OF" runat="server"></mn:Txt>
		    </anthem:PlaceHolder>
		    <anthem:PlaceHolder ID="plcAreaCodes" Runat="server" Visible="false">
			    <mn:Txt id="txtAreaCodes" ResourceConstant="TXT_IN_AREA_CODES" runat="server"></mn:Txt>
		    </anthem:PlaceHolder>
		    <anthem:PlaceHolder ID="plcCollege" Runat="server" Visible="false">
			    <mn:Txt id="txtCollege" ResourceConstant="TXT_AT_COLLEGE" runat="server"></mn:Txt>
		    </anthem:PlaceHolder>
	    </label>
	    <div class="prefSearchPRInfoContDynamic no-wrap-class text-inside">
		    <anthem:HyperLink ID="lnkLocation" Runat="server" />
		    <mnfe:PickRegionNew id="prSearchRegion" Runat="server" />
	    </div>
    </asp:PlaceHolder>
	
	<label class="button">&nbsp;<br />
		<cc1:FrameworkButton runat="server" id="btnSearch" ResourceConstant="SEARCH" class="btn secondary" />
	  </label>
	<asp:rangevalidator runat="server" id="AgeMinRange" controltovalidate="AgeMin" display="None"></asp:rangevalidator>
	<asp:rangevalidator runat="server" id="AgeMaxRange" controltovalidate="AgeMax" display="None"></asp:rangevalidator>
	<asp:requiredfieldvalidator runat="server" id="AgeMinReq" controltovalidate="AgeMin" display="None"></asp:requiredfieldvalidator>
	<asp:requiredfieldvalidator runat="server" id="AgeMaxReq" controltovalidate="AgeMax" display="None"></asp:requiredfieldvalidator>
	<asp:validationsummary runat="server" id="valSummary" displaymode="BulletList" CssClass="validation-error"></asp:validationsummary>
	<asp:comparevalidator runat="server" id="valCompare" controltovalidate="AgeMin" controltocompare="AgeMax" operator="LessThanEqual" display="None"></asp:comparevalidator>
	</fieldset>
</div>
