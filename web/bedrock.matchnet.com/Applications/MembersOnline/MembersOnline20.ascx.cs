﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.MembersOnline.ValueObjects;

using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Ui.PageAnnouncements;
using Matchnet.Web.Framework.Managers;
namespace Matchnet.Web.Applications.MembersOnline
{
    public partial class MembersOnline20 : FrameworkControl, IMembersOnline
    {
        string changeVoteMessage;
        public HtmlInputHidden HIDStartRow
        { get { return StartRow; } set { StartRow = value; } }

        public string ChangeVoteMessage
        { get { return changeVoteMessage; } set { changeVoteMessage = value; } }

        public Matchnet.Web.Framework.Ui.BasicElements.IResultList MOLResultList
        { get { return MembersOnlineList; } }

        public Matchnet.Web.Framework.Image IconViewType
        { get { return null; } }

        public HyperLink LNKViewType
        { get { return null; } }

        public Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BTNSearch
        { get { return btnSearch; } }

        public DropDownList MemberSortDropDown
        {
            get { return ddlMemberSort; }
            set { ddlMemberSort = value; }
        }

        public PlaceHolder PhSortBy
        {
            get { return phSortBy; }
        }

        public Matchnet.Web.Framework.Txt TXTViewBy
        { get { return txtViewby; } }

        public Panel PNLReviseLinks
        { get { return null; } }

        //this is only for a new page, null in old
        public Txt TXTListOff { get { return null; } }
        public Txt TXTGalleryOff { get { return null; } }

        public Matchnet.Web.Framework.Title TTLOnlineNow { get { return ttlOnlineNow; } }
        public Matchnet.Web.Framework.Txt TXTViewAsMOL { get { return null; } }
        public Matchnet.Web.Applications.MembersOnline.Controls.IMembersOnlineFilter MOLFilter { get { return mof; } }
        public HtmlInputHidden HIDGalleryView { get { return GalleryView; } }
        public HtmlInputHidden HIDResultViewChanged { get { return ResultViewChanged; } }

        public Label LBLListNavigationTop { get { return lblListNavigationTop; } }
        public Label LBLListNavigationBottom { get { return lblListNavigationBottom; } }

        public FrameworkControl ThisControl { get { return this; } }

        public MembersOnlineHandler _handler = null;

        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                _handler = new MembersOnlineHandler(g, this);

                ChangeVoteMessage = g.GetResource("ARE_YOU_SURE_YOU_WANT_TO_CHANGE", this);

                //Check to see if a search order has been specified.
                _handler.SetSortDropDown(IsPostBack);

                // Wire up the List Navigation.
                g.ListNavigationTop = lblListNavigationTop;
                g.ListNavigationBottom = lblListNavigationBottom;


                _handler.HandlerVisitorLimit();

                if (!string.IsNullOrEmpty(Request.QueryString["gender"]) && !IsPostBack && string.IsNullOrEmpty(Request.QueryString["postback"]))
                {
                    ListItem itemGender = mof.SELGenderMask.Items.FindByValue(Request.QueryString["gender"]);
                    if (itemGender != null)
                    {
                        mof.SELGenderMask.SelectedIndex = mof.SELGenderMask.Items.IndexOf(itemGender);
                    }
                }
                
                if (!string.IsNullOrEmpty(Request.QueryString["minage"]) && !IsPostBack && string.IsNullOrEmpty(Request.QueryString["postback"]))
                {
                    mof.TXTAgeMin.Value = Request.QueryString["minage"];
                }

                if (!string.IsNullOrEmpty(Request.QueryString["maxage"]) && !IsPostBack && string.IsNullOrEmpty(Request.QueryString["postback"]))
                {
                    mof.TXTAgeMax.Value = Request.QueryString["maxage"];
                }

                if (MembersOnlineManager.Instance.IsE2MOLEnabled(g.Brand))
                {
                    //for consolidation, we will use only one search button
                    btnSearch.Visible = false;
                    mof.BTNSearch.Visible = false;
                    btnSearchConsolidated.Visible = true;

                    if (IsPostBack || !string.IsNullOrEmpty(Request.QueryString["postback"]))
                    {
                        MembersOnlineList.IgnoreAllSearchResultsCache = true;
                        MembersOnlineList.IgnoreSAFilteredSearchResultsCache = true;
                        MembersOnlineList.IgnoreSASearchResultsCache = true;
                    }

                }
                else
                {
                    btnSearch.Visible = true;
                    mof.BTNSearch.Visible = true;
                    btnSearchConsolidated.Visible = false;
                }
              }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void RenderPage()
        {
            try
            {
                idResultsViewType.GetSearchResultViewMode();
                MOLResultList.GalleryView = idResultsViewType.GalleryViewFlag;
                _handler.BindResultList();

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            RenderPage();
            g.SetPageTitle(g.GetResource("TXT_PAGE_TITLE", this));

            if (g.BreadCrumbTrailHeader != null)
            {
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_ONLINE_NOW"),
                                                        g.AppPage.App.DefaultPagePath);

            }
            if (g.BreadCrumbTrailFooter != null)
            {
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_ONLINE_NOW"),
                                                       g.AppPage.App.DefaultPagePath);
            }

            //IM Announcement
            if (IMManager.Instance.IsImAnnouncementEnabled(g.Brand))
            {
                bool showIMAnnouncementOverride = false;
                if (!Page.IsPostBack && !String.IsNullOrEmpty(Request["showIMAnnouncement"]) &&
                    Request["showIMAnnouncement"] == "true")
                {
                    showIMAnnouncementOverride = true;
                }

                if (showIMAnnouncementOverride ||
                    !PageAnnouncementHelper.HasViewedPageAnnouncement(g.Member, g.Brand.Site.SiteID,
                                                                      PageAnnouncementHelper.PageAnnouncementMask
                                                                                            .IMEnhancements))
                {
                    //display Mutual Match Rating feature announcement
                    IMAnnouncement imAnnouncement =
                        Page.LoadControl("/Framework/Ui/PageAnnouncements/IMAnnouncement.ascx") as IMAnnouncement;
                    imAnnouncement.LoadAnnouncement();
                    g.LayoutTemplateBase.AddPageAccouncementControl(imAnnouncement);
                }
            }
        }

        private void btnSearchConsolidated_Click(object sender, System.EventArgs e)
        {
            try
            {
                mof.SetFilterSession();

                string url = Request.RawUrl;
                if (url.IndexOf("Postback") < 0)
                {
                    url += ((Request.RawUrl.IndexOf("?") >= 0) ? "&" : "?") + "Postback=1";
                }
                g.Transfer(url);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            // PreRender defaults for any html/controls on the page

            // TT #15790 - specifically, set hidden field value back to false by default in all cases
            ResultViewChanged.Value = "false";
        }

        // TODO: This needs to support MTF / FTM as well
        private int GetGenderMaskOption(int genderMask)
        {
            return MembersOnlineHandler.GetGenderMaskOption(genderMask);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearchConsolidated.Click += new System.EventHandler(this.btnSearchConsolidated_Click);
        }
        #endregion
    }
}