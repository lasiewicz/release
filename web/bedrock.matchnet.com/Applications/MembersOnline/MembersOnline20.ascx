﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MembersOnline20.ascx.cs" Inherits="Matchnet.Web.Applications.MembersOnline.MembersOnline20" %>
<%@ Register TagPrefix="mol" TagName="MembersOnlineFilter" Src="Controls/MembersOnlineFilter20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/BasicElements/ResultList20.ascx" %>
<%@ Register TagPrefix="mn1" TagName="ResultsViewType" Src="../../Framework/Ui/BasicElements/ResultsViewType.ascx" %>
<script type="text/javascript">
	var Page_BlockSubmit = false;
</script>

<div class="header-options clearfix">
	<mn:Title runat="server" id="ttlOnlineNow" ResourceConstant="TXT_MEMBERS_ONLINE"/>
	<div class="pagination"><asp:label id="lblListNavigationTop" Runat="server" /></div>
</div>
<mol:membersonlinefilter id="mof" runat="server"></mol:membersonlinefilter>
<input id="StartRow" type="hidden" name="StartRow" runat="server" />

<div class="sort-display">
	<div class="view-type image-text-pair">
	     <mn1:ResultsViewType id="idResultsViewType" runat="server" />   
	</div>

    
	<asp:PlaceHolder ID="phSortBy" runat="server">
	    <div class="view-by">
            <label>
                <mn:Txt id="txtViewby" runat="server" resourceconstant="TXT_VIEW_BY"></mn:Txt>
                <asp:dropdownlist id="ddlMemberSort" runat="server"></asp:dropdownlist>&nbsp;
	            <cc1:FrameworkButton id="btnSearch" runat="server" ResourceConstant="TXT_GO" class="btn secondary" />
	            <cc1:FrameworkButton id="btnSearchConsolidated" runat="server" ResourceConstant="SEARCH" class="btn secondary" />
            </label>
	    </div>
	</asp:PlaceHolder>

    <input type="hidden" name="GalleryView" runat="server" id="GalleryView" value="false" />
    <input type="hidden" name="ResultViewChanged" runat="server" id="ResultViewChanged" value="false" />
</div>		

<hr class="hr-thin" />

<div id="results-container" class="clearfix clear-both">
    <result:resultlist id="MembersOnlineList" runat="server" resultlistcontexttype="MembersOnline"></result:resultlist>
</div>

<div class="pagination"><asp:label id="lblListNavigationBottom" Runat="server" /></div>
<input type="hidden" value="<% = ChangeVoteMessage %>" name="changeVoteMessage" />
