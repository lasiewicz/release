﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupMicroProfile.ascx.cs" Inherits="Matchnet.Web.Applications.AffiliationGroups.Controls.GroupMicroProfile" %>
 <%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
  <div class="picture">
     
      <asp:Image ID="imgLogo" runat="server" />
  </div>
  <div class="liquid">
        
        
         <div class="overview">
			<asp:HyperLink runat="server" ID="lnkGroupName"><asp:Literal ID="litGroupName" runat="server" /></asp:HyperLink>
              
               <mn:Txt ID="lblLocation" runat="server" ResourceConstant="TXT_LOCATION" /><asp:Literal ID="litLocation" runat ="server" /><br />
               <mn:Txt ID="lblCategory" runat="server" ResourceConstant="TXT_CATEGORY" /><asp:HyperLink runat="server" ID="lnkCategory"><asp:Literal ID="litCategory" runat ="server" /></asp:HyperLink><br />
           </div>
		

    </div>	