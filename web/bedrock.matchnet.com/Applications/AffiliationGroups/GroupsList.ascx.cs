﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Specialized;
using System.Text;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.Lib;
using Matchnet.Lib.Util;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Privilege;

using Matchnet.Content.ValueObjects.BrandConfig;

using Matchnet.AffiliationGroups.ServiceAdapters;
using Matchnet.AffiliationGroups.ValueObjects;
namespace Matchnet.Web.Applications.AffiliationGroups
{
    public partial class GroupsList : FrameworkControl
    {


        int _categoryid = 1003;
        
        const string sort_new = "new";
        const string sort_top = "top";
        const string sort_new_all = "newall";
        const string sort_top_all = "topall";
        const string sort_all = "all";
        public const string CHAPTER_SPACER = "&nbsp;/&nbsp;";
        public const string QSPARAM_STARTROW = "StartRow";
        public const string QSPARAM_RESOURCECONSTANT = "rc";
        string _sort = sort_new;
        string _sortall = "";
        AffiliationGroup _featuredGroup = null;
        List<AffiliationGroup> _sortedgroups = null;
        #region paging
        int pageSize = 12;
        int chapterSize = 12;
        private int startRow;
        public Int32 PageSize
        {
            get { return pageSize; }
            set { pageSize = value; }
        }

        public Int32 ChapterSize
        {
            get { return chapterSize; }
            set { chapterSize = value; }
        }

        public Int32 TotalRows
        {
            get { return _TotalRows; }
            set { _TotalRows = value; }
        }

        private Int32 _TotalRows;

        public Int32 StartRow
        {
            get
            {
                return Conversion.CInt(hidStartRow.Value);
            }
            set
            {
                hidStartRow.Value = value.ToString();
            }
        }

        public int EndRow { get; set; }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty( Request["categoryid"]))
            {
                _categoryid=Conversion.CInt(Request["categoryid"],_categoryid);

            }
            lnkCreateGroup.NavigateUrl = "AddGroup.aspx";
            
            AffiliationCategorySite category = Matchnet.AffiliationGroups.ServiceAdapters.MetaDataSA.Instance.GetAffiliationCategory(_categoryid);

            if (!String.IsNullOrEmpty(Request["sort"]))
                _sort = Request["sort"];

            
            startRow = Conversion.CInt(Request["StartRow"]);
            if (startRow <= 0)
                startRow = 1;
            StartRow = startRow;

           
            AffiliationGroupsCollection groups = Matchnet.AffiliationGroups.ServiceAdapters.AffiliationGroupsSA.Instance.GetAffiliationGroups(_categoryid);
            if (groups == null || groups.Groups == null)
            {
                phTopFeature.Visible = false;
                tabBar.Visible = false;
                return;


            }
            if (_sort == sort_new)
                 _sortedgroups = groups.GetNewGroups(4);
            else if (_sort == sort_top)
                _sortedgroups = groups.GetTopGroups(4);
            else if (_sort == sort_new_all)
            {
                _sortedgroups = groups.GetNewGroups(startRow - 1, pageSize);
                _sortall = "all";
            }
            else if (_sort == sort_top_all)
            {
                _sortall = "all";
                _sortedgroups = groups.GetTopGroups(startRow - 1, pageSize);
            }
            else
            {
                _sortall = "all";
                _sortedgroups = groups.GetTopGroups(startRow - 1, pageSize);
            }
            _TotalRows = groups.Groups.Count;
            if (_sort == sort_all || _sort==sort_new_all || _sort==sort_top_all)
            {
                phTopFeature.Visible = false;
                phPromo.Visible = false;
                lnkSeeMore.Visible = false;
            }
            else
            {

                Random random = new Random(DateTime.Now.Second);
                int ind = random.Next(1, 4 < groups.Groups.Count ? 4 : groups.Groups.Count);
                _featuredGroup = groups.Groups[ind - 1];

                lnkFeaturedGroups.NavigateUrl = "group.aspx?groupid=" + _featuredGroup.AffiliationGroupID.ToString();
                if (!String.IsNullOrEmpty(_featuredGroup.Image))
                    imgFeature.ImageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL") + "/" + category.ImagePath + "/" + _featuredGroup.Image;
                else
                    imgFeature.ImageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL") + "/" + category.ImagePath + "/" + category.DefaultLogo;
                litGroupName.Text = _featuredGroup.Name;
                //litContact.Text = _featuredGroup.Head;
                //if (_featuredGroup.MembershipCount > 0)
                //    litMembership.Text = _featuredGroup.MembershipCount.ToString();
                //else
                //    litMembership.Text = "";

                //litCategory.Text = category.Name;
                //lnkCategory.NavigateUrl = "/Applications/AffiliationGroups/GroupsList.aspx?categoryid=" + _categoryid.ToString() + "&sort=" + _sort;
                litAbout.Text = FrameworkGlobals.Ellipsis( _featuredGroup.About,500);

                if(_sortedgroups.Contains(_featuredGroup))
                    _sortedgroups.Remove(_featuredGroup);

                lnkSeeMore.NavigateUrl = "/Applications/AffiliationGroups/GroupsList.aspx?sort=" + _sort + "all";
                lnkMore.NavigateUrl = "/Applications/AffiliationGroups/Group.aspx?groupid=" + _featuredGroup.AffiliationGroupID;
            }
            rptGroups.DataSource = _sortedgroups;
           
            rptGroups.DataBind();
            tabBar.HrefBase = "/Applications/AffiliationGroups/GroupsList.aspx?sorted";
            tabBar.HrefFormat = "{0}={1}";
            tabBar.TabFormat = "{0}";
            tabBar.MaxTextLen = 20;
            tabBar.ClassSelected = "selected";
            tabBar.ResourceControl = this;
            Tab tab = new Tab(1, 1, 0, _sort == "new" + _sortall, "/Applications/AffiliationGroups/GroupsList.aspx?sort=new" + _sortall, "TXT_SORT_NEW", "");
            tabBar.AddTab(tab);
            tab = new Tab(1, 1, 0, _sort == "top" + _sortall, "/Applications/AffiliationGroups/GroupsList.aspx?sort=top" + _sortall, "TXT_SORT_TOP", "");
            tabBar.AddTab(tab);

            tab = new Tab(1, 1, 0, _sort == "all", "/Applications/AffiliationGroups/GroupsList.aspx?sort=all", "TXT_SORT_ALL", "");
            tabBar.AddTab(tab);

            if (_sort == sort_all)
            {
                g.ListNavigationTop = lblListNavigationTop;
                g.ListNavigationBottom = lblListNavigationBottom;
                RenderPaging();
            }
        }


        public void BindGroups(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Footer || e.Item.ItemType == ListItemType.Header)
                { return; }
                AffiliationGroup group = (AffiliationGroup)e.Item.DataItem;

              
                AffiliationGroups.Controls.GroupMiniProfile groupProfile = (AffiliationGroups.Controls.GroupMiniProfile)e.Item.FindControl("groupProfile");
                groupProfile.Group = group;
                
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }



        # region Render Paging
        public void RenderPaging()
        {
            if (HttpContext.Current.Request["TotalRows"] != null)
            {
                TotalRows = Convert.ToInt32(HttpContext.Current.Request["TotalRows"]);
            }
            if (StartRow < 0)
                StartRow = 1;
            // if the start row is greater than the totalrows, set the startrow to the nearest round page_size less than totalrows
            if (StartRow > TotalRows)
            {
                StartRow = (TotalRows / PageSize) * PageSize + 1;
            }

            // define needed vars
            int pageCount = ((TotalRows - 1) / PageSize) + 1;
            int pageRequested = StartRow / PageSize + 1;

            int chapter;
            int pageCurrent = 0;
            int extraRows = TotalRows - (ChapterSize * PageSize);
            int extraPagesNeeded = Convert.ToInt32(Math.Ceiling((double)extraRows / (double)PageSize));

            // if the requested page is greater than the count, set the requested page to the count
            if (pageRequested > pageCount)
            {
                pageRequested = pageCount;
            }

            // set the chapter
            if (pageRequested > ChapterSize)
            {
                chapter = (StartRow / PageSize / (ChapterSize + extraPagesNeeded)) + 1;
            }
            else
            {
                chapter = (StartRow / PageSize / ChapterSize) + 1;
            }

            // set the start page depending on the startrow
            int startSize = 0;
            int maxChapter = ChapterSize;
            if (StartRow > PageSize * 2)
            {
                startSize = (StartRow - 1) / PageSize;

                // if the start size is within the last two page elements of all of the pages,
                // decrement the start size.  This means that always three page links
                // will be shown at the end of the list.
                if ((startSize > ChapterSize && pageCount - startSize < ChapterSize - 1) || pageCount == ChapterSize)
                {
                    startSize--;
                }

                maxChapter = ((StartRow - 1) / PageSize) + (ChapterSize - 1);
            }
            else
            {
                startSize = 1;
            }

           
            // there must be more rows than the page size to show paging
            if (TotalRows > PageSize)
            {
                //AddBreadCrumbPaging(pageRequested, pageCount);

                // Add links for the ListNavigation (see SearchResults.ascx, MembersOnline.ascx, and Hotlist.View.ascx) if necessary.
                AddListNavigation(pageRequested, pageCount, pageCurrent, startSize, chapter, maxChapter);
            }
        }

        public void AddListNavigation(int pageRequested, int pageCount, int pageCurrent, int startSize, int chapter, int maxChapter)
        {
            if (g.ListNavigationTop != null && g.ListNavigationBottom != null)
            {
                // show back to start pipe and arrow if necessary.
                if (pageCount > ChapterSize && StartRow >= PageSize * (ChapterSize - 1))
                {
                    g.ListNavigationTop.Controls.Add(CreateArrowLink(1, "breadCrumbPagesLink", "start"));
                    g.ListNavigationBottom.Controls.Add(CreateArrowLink(1, String.Empty, "start"));

                    g.ListNavigationTop.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                    g.ListNavigationBottom.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                }

                // Do not show a previous link if we are on the first page.
                if (pageRequested > 1)
                {
                    g.ListNavigationTop.Controls.Add(CreateListArrowLink(pageRequested, String.Empty, false));
                    g.ListNavigationTop.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                    g.ListNavigationBottom.Controls.Add(CreateListArrowLink(pageRequested, String.Empty, false));
                    g.ListNavigationBottom.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                }

                // display each page
                for (int page = startSize; (page <= maxChapter && page <= pageCount); page++)
                {
                    pageCurrent = page * chapter;

                    bool isCurrentPage = (page == pageRequested);

                    g.ListNavigationTop.Controls.Add(CreatePagingLink(pageCurrent, "breadCrumbPagesLink", isCurrentPage));
                    g.ListNavigationBottom.Controls.Add(CreatePagingLink(pageCurrent, String.Empty, isCurrentPage));

                    // display the character spacer in between page links (but not at the end)
                    if ((page < maxChapter) && (pageCurrent != pageCount))
                    {
                        g.ListNavigationTop.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                        g.ListNavigationBottom.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                    }
                }

                // show end arrows if necessary.
                if (pageCount > ChapterSize && ((TotalRows - StartRow) > (PageSize * 2)))
                {
                    // Do not show a next link if we are on the last page.
                    if (pageRequested < pageCount)
                    {
                        g.ListNavigationTop.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                        g.ListNavigationBottom.Controls.Add(new LiteralControl(CHAPTER_SPACER));

                        g.ListNavigationTop.Controls.Add(CreateListArrowLink(pageRequested, String.Empty, true));
                        g.ListNavigationBottom.Controls.Add(CreateListArrowLink(pageRequested, String.Empty, true));
                    }
                }
            }
        }

        public string GetPageText(int pageCurrent, bool showBold)
        {
            int start = (pageCurrent * PageSize) - PageSize + 1;
            int end = start + PageSize - 1;

            // use the total rows instead of the normal last number if it is the last number
            if (TotalRows < end && TotalRows >= start)
            {
                end = TotalRows;
            }

            if (showBold)
            {
                return "<strong>" + start + "-" + end + "</strong>";
            }

            return start + "-" + end;
        }

        public HyperLink CreateArrowLink(int nextPage, string cssClass, string direction)
        {
            int pageToLink;
            HyperLink link = new HyperLink();
            if (cssClass != String.Empty)
            {
                link.CssClass = cssClass;
            }
            if (direction == "next")
            {
                link.Text = "&gt;&gt;";
                pageToLink = (nextPage - 1) * PageSize + 1;
            }
            else if (direction == "end")
            {
                link.Text = "&gt;|";
                pageToLink = (nextPage - 1) * PageSize + 1;
            }
            else if (direction == "start")
            {
                link.Text = "|&lt;";
                pageToLink = (nextPage - 1) * PageSize + 1;
            }
            else
            {
                link.Text = "&lt;&lt;";
                pageToLink = nextPage;
            }

            link.NavigateUrl = BuildPagingURL(pageToLink);
            return link;
        }

        /// <summary>
        /// This is used to create the Next and Previous arrows.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="cssClass"></param>
        /// <param name="isNext"></param>
        /// <returns></returns>
        public HyperLink CreateListArrowLink(int page, string cssClass, bool isNext)
        {
            int pageToLink;
            HyperLink link = new HyperLink();
            if (cssClass != String.Empty)
            {
                link.CssClass = cssClass;
            }

            if (isNext)
            {
                link.Text = g.GetResource("TXT_NEXT");
                pageToLink = page * PageSize + 1;
            }
            else
            {
                link.Text = g.GetResource("TXT_PREVIOUS");
                pageToLink = (page - 2) * PageSize + 1;
            }

            link.NavigateUrl = BuildPagingURL(pageToLink);
            return link;
        }

        public HyperLink CreatePagingLink(int pageCurrent, string cssClass, bool showBold)
        {
            int pageToLink = (pageCurrent - 1) * PageSize + 1;
            HyperLink link = new HyperLink();
            if (cssClass != String.Empty)
            {
                link.CssClass = cssClass;
            }
            link.Text = GetPageText(pageCurrent, showBold);
            link.NavigateUrl = BuildPagingURL(pageToLink);
            return link;
        }

        public string BuildPagingURL(int startRow)
        {
            //start with the name of the current control
            StringBuilder url = new StringBuilder(g.AppPage.ControlName + ".aspx?");
            NameValueCollection qsParams = HttpContext.Current.Request.QueryString;
            if (qsParams.Keys.Count > 0)
            {
                //rebuild the querystring portion of the URL 
                foreach (string key in qsParams.Keys)
                {
                    //We're adding our own start row parameter, so ignore that part of the querystring. Also, 
                    //we don't want to pass the resource constant around since it's a one time message, 
                    // so strip it out as well. 
                    if (key != null && !key.Equals(QSPARAM_STARTROW) && !key.Equals(QSPARAM_RESOURCECONSTANT))
                    {
                        url.Append(key + "=" + qsParams[key] + "&");
                    }
                }
            }

            url.Append(QSPARAM_STARTROW + "=" + startRow);
            return url.ToString();
        }


        #endregion
    }
}
