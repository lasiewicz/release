﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddGroup.ascx.cs" Inherits="Matchnet.Web.Applications.AffiliationGroups.AddGroup" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<asp:PlaceHolder ID="phNotification" runat="server" Visible="false">
<asp:HyperLink ID="lnkCategory" runat="server"><asp:Literal ID="litCategory" runat="server" /></asp:HyperLink>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phCreateGroup" runat="server" >
<div id="group-create">
    <h1><mn:Txt runat="server" ResourceConstant="TXT_TITLE" /></h1>
    <p><mn:Txt ID="Txt12" runat="server" ResourceConstant="TXT_SUB_TITLE" /></p>
    <dl class="dl-form clearfix">
	    <dt><label for="groupName"><mn:Txt runat="server" ResourceConstant="VAL_GROUP_NAME" /></label></dt>
        <dd><asp:TextBox runat="server" size="50" id="groupName" />
       <mn:MultiValidator ID="valgroupName" TabIndex="-1" runat="server" Display="Dynamic"
                                                            RequiredType="AlphaNumType" ControlToValidate="groupName" IsRequired="true"
                                                            FieldNameResourceConstant="VAL_GROUP_NAME"></mn:MultiValidator>
        </dd>
	    <dt><label for="groupCreator"><mn:Txt ID="Txt1" runat="server" ResourceConstant="VAL_GROUP_CREATOR" /></label></dt>
        <dd><asp:TextBox runat="server"  size="50" id="groupCreator" />
        <mn:MultiValidator ID="MultiValidator1" TabIndex="-1" runat="server" Display="Dynamic"
            RequiredType="AlphaNumType" ControlToValidate="groupCreator" IsRequired="true"
            FieldNameResourceConstant="VAL_GROUP_CREATOR" ></mn:MultiValidator></dd>

	    <dt><label for="groupHead"><mn:Txt ID="Txt2" runat="server" ResourceConstant="VAL_GROUP_HEAD" /></label></dt>
        <dd><asp:TextBox runat="server"  size="50" id="groupHead" /></dd>
	    <dt><label for="totalMembership"><mn:Txt ID="Txt3" runat="server" ResourceConstant="VAL_GROUP_COUNT" /></label></dt>
        <dd><asp:TextBox runat="server"  size="20" id="totalMembership" />
          <mn:MultiValidator ID="MultiValidator3" TabIndex="-1" runat="server" Display="Dynamic"
            RequiredType="IntegerType" ControlToValidate="totalMembership" IsRequired="false"
            FieldNameResourceConstant="VAL_GROUP_COUNT" ></mn:MultiValidator>
        </dd>
        <asp:Repeater ID="rptOptions" runat="server" OnItemDataBound="BindOptions">
            <ItemTemplate>
            
                <dt><mn:Txt ID="txtOptionName" runat ="server" /></dt>
             <dd>
                <asp:DropDownList ID="ddlOptions" runat="server" />
            </dd>
            </ItemTemplate>
	    </asp:Repeater>
	    <dt><label for="address"><mn:Txt ID="Txt4" runat="server" ResourceConstant="VAL_GROUP_ADDRESS" /></label></dt>
        <dd><asp:TextBox runat="server"  size="50" id="address" /></dd>
    	
	    <dt><label for="city"><mn:Txt ID="Txt5" runat="server" ResourceConstant="VAL_GROUP_CITY" /></label></dt>
        <dd><asp:TextBox runat="server"  size="50" id="city" /></dd>
        <dt><label for="state"><mn:Txt ID="Txt6" runat="server" ResourceConstant="VAL_GROUP_STATE" /></label></dt>
        <dd>
    	     <asp:DropDownList ID="ddlState" runat="server" />
        </dd>
	    <dt><label for="zip"><mn:Txt ID="Txt7" runat="server" ResourceConstant="VAL_GROUP_ZIP" /></label></dt>
        <dd><asp:TextBox runat="server"  size="50" id="zip" /></dd>
	    <dt><label for="phone"><mn:Txt ID="Txt8" runat="server" ResourceConstant="VAL_GROUP_PHONE" /></label></dt>
        <dd><asp:TextBox runat="server"  size="50" id="phone" />
         <mn:MultiValidator ID="MultiValidator2" TabIndex="-1" runat="server" Display="Dynamic"
            RequiredType="PhoneNumberType" ControlToValidate="phone" IsRequired="false"
            FieldNameResourceConstant="VAL_GROUP_PHONE" ></mn:MultiValidator>
        
        </dd>
	    <dt><label for="email"><mn:Txt ID="Txt9" runat="server" ResourceConstant="VAL_GROUP_EMAIL" /></label></dt>
        <dd><asp:TextBox runat="server"  size="50" id="email" />
           <mn:MultiValidator ID="MultiValidator4" TabIndex="-1" runat="server" Display="Dynamic"
            RequiredType="EmailType" ControlToValidate="email" IsRequired="true"
            FieldNameResourceConstant="VAL_GROUP_EMAIL" ></mn:MultiValidator>
         </dd>
	    <dt><label for="web"><mn:Txt ID="Txt10" runat="server" ResourceConstant="VAL_GROUP_WEB" /></label></dt>
        <dd><asp:TextBox runat="server"  size="50" id="web" /></dd>
	    <dt><label for="about"><mn:Txt ID="Txt11" runat="server" ResourceConstant="VAL_GROUP_ABOUT" /></label></dt>
        <dd><asp:TextBox id="about" runat="server" TextMode="MultiLine"/></dd>
        <dt>&nbsp;</dt>
        <dd><asp:Button runat="server" ID="btnSubmit" Text="Save" OnClick="btnSave_OnClick" /></dd>
       
    </dl>

</div>
</asp:PlaceHolder>