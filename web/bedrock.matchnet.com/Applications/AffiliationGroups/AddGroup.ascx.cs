﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.Lib;
using Matchnet.Lib.Util;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Privilege;

using Matchnet.Content.ValueObjects.BrandConfig;

using Matchnet.AffiliationGroups.ServiceAdapters;
using Matchnet.AffiliationGroups.ValueObjects;
using Matchnet.Web.Applications.Home;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Content.ServiceAdapters;
namespace Matchnet.Web.Applications.AffiliationGroups
{
    public partial class AddGroup : FrameworkControl
    {
        const int REGION_ID_US=223;
        //no time to implement categories
        int _categoryid=1003;
        AffiliationCategorySite categorysite = null;
        Hashtable _options;
        protected void Page_Load(object sender, EventArgs e)
        {
            lnkCategory.NavigateUrl = "/Applications/AffiliationGroups/GroupsList.aspx";
            
            bindStates();
            categorysite = MetaDataSA.Instance.GetAffiliationCategory(_categoryid);
            if (categorysite.Options != null && categorysite.Options.Options.Count > 0)
             {
                _options = new Hashtable();
                 rptOptions.DataSource = categorysite.Options.Options;
                 rptOptions.DataBind();
              }
            litCategory.Text = g.GetResource("TXT_BACK", this, new string[] { categorysite.Description });
        }

        public void BindOptions(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Footer || e.Item.ItemType == ListItemType.Header)
                { return; }
                AffiliationCategoryOption option = (AffiliationCategoryOption)e.Item.DataItem;


                Txt txtOptionName = (Txt)e.Item.FindControl("txtOptionName");
                DropDownList ddlOptions = (DropDownList)e.Item.FindControl("ddlOptions");
                if (txtOptionName != null)
                {
                    txtOptionName.Text = option.Name;
                }
                if (ddlOptions != null)
                {
                    ddlOptions.DataSource = option.OptionValues;
                    ddlOptions.DataValueField = "AffiliationCategoryOptionValueID";
                    ddlOptions.DataTextField = "Description";

                    ddlOptions.DataBind();
                  
                }

                _options[option.AffiliationCategoryOptionID.ToString()] = ddlOptions.UniqueID;
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsValid)
                    return;
                AffiliationGroup group = new AffiliationGroup();

                if (group.Options == null)
                    group.Options = new List<AffiliationGroupOption>();

                StringDictionary tokens=new StringDictionary();

                tokens.Add("CATEGORY", categorysite.Description);
                tokens.Add("GROUPNAME", groupName.Text);
                tokens.Add("CREATOR",groupCreator.Text);

                tokens.Add("GROUPHEAD",groupHead.Text);
                tokens.Add("ADDRESS", address.Text);
                
                tokens.Add("CITY",city.Text);
                tokens.Add("STATE",ddlState.SelectedItem.Text);
                tokens.Add("ZIP",zip.Text);
                tokens.Add("EMAIL",email.Text);
                tokens.Add("WEB", web.Text);
                tokens.Add("ABOUT", about.Text);
            
                tokens.Add("PHONE",  phone.Text);

                string username = "";
                string useremail = "";
                if (g.Member != null)
                {
                    username = g.Member.GetUserName(g.Brand);
                    username = String.IsNullOrEmpty(username) ? g.Member.MemberID.ToString() : username;
                    useremail = g.Member.EmailAddress;
                }
                else
                {
                    username = "n/a";
                    useremail = "n/a";
                }

                tokens.Add("USERNAME",username);
                tokens.Add("USEREMAIL", useremail);
                tokens.Add("MEMBERSHIP",totalMembership.Text);
                tokens.Add("BRAND", g.Brand.Uri);
               System.Text.StringBuilder stropt=new System.Text.StringBuilder();
                foreach (string key in _options.Keys)
                {
                    int value = Conversion.CInt(Request.Form[_options[key].ToString()]);
                    int optionid = Conversion.CInt(key);
                    AffiliationCategoryOption option=categorysite.Options.GetOption(optionid);
                    AffiliationCategoryOptionValue optionvalue=null;
                    if(option != null)
                    {
                       optionvalue=option.GetOptionValue(value);
                    }
                    string optval="";
                    if(optionvalue != null)
                    {
                        optval=optionvalue.Description;
                    }

                    stropt.Append(option.Name +  ": " + optval + "\r\n");
                 
                }
                
                tokens.Add("OPTIONS",stropt.ToString());
                string emailbody = g.GetResource("TXT_EMAIL_BODY", this, tokens);

                string toemail = SettingsManager.GetSettingString(SettingConstants.AFFILIATIONGROUP_TO_EMAIL, g.Brand);
                string fromemail = SettingsManager.GetSettingString(SettingConstants.AFFILIATIONGROUP_FROM_EMAIL, g.Brand);


                Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendInternalCorpMail(g.Brand.BrandID,fromemail, toemail, emailbody, username, "Affiliation group request");
                g.Notification.AddMessageString(g.GetResource("TXT_SAVED",this));
                //g.Transfer("/Applications/AffiliationGroups/GroupsList.aspx?categoryid=" + categorysite.CategorySiteID.ToString());
                phCreateGroup.Visible = false;
                phNotification.Visible = true;

               
            }
            catch (Exception ex)
            {
                _g.Notification.AddErrorString("Error saving group.");
                _g.ProcessException(ex);
            }

        }

        private   void bindStates()
        {
            
                RegionCollection stateRegions = RegionSA.Instance.RetrieveChildRegions(REGION_ID_US, g.Brand.Site.LanguageID);
                ddlState.DataSource = stateRegions;
                ddlState.DataTextField = "Description";
                ddlState.DataValueField = "RegionID";
                ddlState.DataBind();

                //ddlState.Items.Insert(0, new ListItem("",Constants.NULL_INT.ToString()));
                ddlState.Items.Insert(0, GetEmptyListItem());

               
            }
        private ListItem GetEmptyListItem()
        {   ListItem l;
            string displayTxt="";
           
            l = new ListItem(displayTxt, Constants.NULL_INT.ToString());
            return l;

        }
        
    }
}