﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Specialized;
using System.Text;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.Lib;
using Matchnet.Lib.Util;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Privilege;

using Matchnet.Content.ValueObjects.BrandConfig;

using Matchnet.AffiliationGroups.ServiceAdapters;
using Matchnet.AffiliationGroups.ValueObjects;
using Matchnet.Web.Applications.Home;

namespace Matchnet.Web.Applications.AffiliationGroups
{
    public partial class GroupMembers : FrameworkControl
    {
        bool joined = true;
        string locationformat = "{0}, {1}";
        int _groupid = 0;
        AffiliationGroup _group = null;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            _groupid = Conversion.CInt(Request["groupid"]);

            _group = AffiliationGroupsSA.Instance.GetAffiliationGroup(_groupid);
            if (_group == null)
                return;
            if (g.Member != null)
            {
                AffiliationMemberGroupsList membergroups = Matchnet.AffiliationGroups.ServiceAdapters.AffiliationGroupMemberSA.Instance.GetMemberGroups(g.Brand.Site.SiteID, g.Member.MemberID);
                if (membergroups.IfGroupMember(_group.CategorySiteID, _groupid))
                {
                    btnJoin.Visible = false;
                    litGroupMember.Text = g.GetResource("TXT_GROUP_MEMBER", this, new string[] { _group.Name });
                }
            }
            else
            {
                btnJoin.Visible = false;
            }

            AffiliationCategorySite category = Matchnet.AffiliationGroups.ServiceAdapters.MetaDataSA.Instance.GetAffiliationCategory(_group.CategorySiteID);
            if (!String.IsNullOrEmpty(_group.Image))
                imgLogo.ImageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL") + "/" + category.ImagePath + "/" + _group.Image;
            else
                imgLogo.ImageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL") + "/" + category.ImagePath + "/" + category.DefaultLogo;
            litContact.Text = _group.Head;
           
            litLocation.Text = string.Format(locationformat, _group.City, _group.State);
           
            litCategory.Text = category.Name;
            if (_group.MembershipCount <= 0)
                litMembership.Text = "";
            else
                litMembership.Text = _group.MembershipCount.ToString("N0");
        
            g.ListNavigationTop = lblListNavigationTop;
            g.ListNavigationBottom = lblListNavigationBottom;
            idResultsViewType.GetSearchResultViewMode();
            litGroupName.Text = _group.Name;
            SearchResultList.GalleryView = idResultsViewType.GalleryViewFlag;
           
            SearchResultList.ResultListContextType = ResultContextType.AffiliationGroupMembers;
           


        }

        private void btnJoin_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (g.Member != null)
                    Matchnet.AffiliationGroups.ServiceAdapters.AffiliationGroupMemberSA.Instance.AddGroupMember(g.Brand.BrandID, _group.CategorySiteID, _groupid, g.Member.MemberID);

                g.Notification.AddMessageString(g.GetResource("TXT_JOIN_SUCCESS"));
                joined = true;
                if (g.Member.GetAttributeInt(g.Brand, "HasAffiliationGroup") != 1)
                {
                    g.Member.SetAttributeInt(g.Brand, "HasAffiliationGroup", 1);
                    Member.ServiceAdapters.MemberSA.Instance.SaveMember(g.Member);
                }
            }
            catch (Exception ex)
            {
                g.Notification.AddErrorString(g.GetResource("TXT_JOIN_ERR"));
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (joined)
                btnJoin.Visible = false;
         
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnJoin.Click += new System.EventHandler(this.btnJoin_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            // this.Init += new System.EventHandler(this.Page_Init);

        }
        #endregion
    }


}