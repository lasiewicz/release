﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Omnidate.Controls
{

    public partial class OmnidateAvatarButton : FrameworkControl
    {
        public IMemberDTO TargetMember { get; set; }

        protected string Gender
        {
            get
            {
                string result = string.Empty;
                if (g.Member != null)
                {
                    string genderMask = Convert.ToString(g.Member.GetAttributeInt(g.Brand, "Gendermask"));
                    switch (genderMask)
                    {
                        case "6":
                        case "10":
                            result = "female";
                            break;
                        case "5":
                        case "9":
                            result = "male";
                            break;
                    }
                }
                return result;
            }
        }

        //Omnidate Style
        protected string OmniStyle
        {
            get
            {
                if (g.Brand.Site.LanguageID == (int)Language.Hebrew)
                {
                    return "Spark02";
                }
                return "Spark01";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (g.Member != null && TargetMember != null && g.Member.MemberID == TargetMember.MemberID)
                {
                    if (RuntimeSettings.GetSetting("ENABLE_OMNIDATE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
                    {
                        plcAvatarButton.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}