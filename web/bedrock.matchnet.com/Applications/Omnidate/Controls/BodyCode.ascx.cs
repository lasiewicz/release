﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;


namespace Matchnet.Web.Applications.Omnidate.Controls
{


    public partial class BodyCode : FrameworkControl
    {
        protected string OmnidateAccountID
        {
            get
            {
                return RuntimeSettings.GetSetting("OMNIDATE_ACCOUNT_ID", g.Brand.Site.Community.CommunityID,
                                                  g.Brand.Site.SiteID);
            }
        }

        protected string MemberID
        {
            get
            {
                string result = string.Empty;
                if (g.Member != null)
                {
                    result = g.Member.MemberID.ToString();
                }
                return result;
            }
        }

        protected string Username
        {
            get
            {
                string result = string.Empty;
                if (g.Member != null)
                {
                    result = g.Member.GetUserName(g.Brand);
                }
                return result;
            }
        }

        protected string Gender
        {
            get
            {
                string result = string.Empty;
                if (g.Member != null)
                {
                    string genderMask = Convert.ToString(g.Member.GetAttributeInt(g.Brand, "Gendermask"));
                    switch (genderMask)
                    {
                        case "6":
                        case "10":
                            result = "female";
                            break;
                        case "5":
                        case "9":
                            result = "male";
                            break;
                    }
                }
                return result;
            }
        }

        protected string ViewProfileURL
        {
            get
            {
                return "http://" + Request.Url.Host + "/Applications/MemberProfile/ViewProfile.aspx?MemberID=";
            }
        }

        protected string UserXmlURL
        {
            get
            {
                return "http://" + Request.Url.Host + "/Applications/Omnidate/OmnidateProfileInfo.aspx?MemberID=";
            }
        }

        protected string NonSubMember
        {
            get
            {
                string result = string.Empty;
                if (g.Member != null)
                {

                    //if (!g.Member.IsPayingMember(g.Brand.Site.SiteID))
                    if (OmnidateHelper.GetOmnidateUserClass(g) == OmnidateUserClass.NonSubscriber)
                    {
                        result = " ,'premium', '";
                        result += FrameworkGlobals.GetSubscriptionLink(false, 746)
                               + "&DestinationURL=" + HttpContext.Current.Server.UrlEncode(HttpContext.Current.Items["FullURL"].ToString()); ;
                        result += "'";
                    }
                }
                return result;
            }
        }

        protected string OmnidateLanguage
        {
            get
            {
                string result = string.Empty;
                switch (g.Brand.Site.LanguageID)
                {
                    case (int)Language.French:
                        result = ",'language','fr'";
                        break;
                    case (int)Language.Hebrew:
                        result = ",'language','he'";
                        break;
                    default:
                        break;
                }

                return result;
            }
        }

        protected string UserClass
        {
            get
            {
                string userClass = string.Empty;
                if (g.Member != null)
                {
                    userClass = ((int)OmnidateHelper.GetOmnidateUserClass(g)).ToString();
                }

                return userClass;
            }
        }

        protected string RegisterUrl
        {
            get
            {
                return FrameworkGlobals.GetSubscriptionLink(false, 1026) + "&DestinationURL=" + HttpContext.Current.Server.UrlEncode("/Applications/MemberProfile/ViewProfile.aspx?MemberID=");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (g.Member != null)
                {
                    if (RuntimeSettings.GetSetting("ENABLE_OMNIDATE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
                    {
                        plcOmnidate.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}