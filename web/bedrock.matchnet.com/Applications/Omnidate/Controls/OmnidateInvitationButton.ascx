﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OmnidateInvitationButton.ascx.cs"
    Inherits="Matchnet.Web.Applications.Omnidate.Controls.OmnidateInvitationButton" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<asp:PlaceHolder runat="server" ID="plcOmnidateButtonObject" Visible="false">
    <script type="text/javascript" language="JavaScript">
        var MyButton<%=TargetMember.MemberID %> = new Object;
        MyButton<%=TargetMember.MemberID %>.online = "<%=OmnidateOnlineButtonURL %>";
        MyButton<%=TargetMember.MemberID %>.inactive = "<%=OmnidateButtonInactiveURL %>";
        MyButton<%=TargetMember.MemberID %>.dating = "<%=OmnidateDatingButtonURL %>";
        MyButton<%=TargetMember.MemberID %>.offline = "<%=OmnidateButtonOfflineURL %>";
        MyButton<%=TargetMember.MemberID %>.height = "<%=OmnidateButtonHeight %>";
        MyButton<%=TargetMember.MemberID %>.width = "<%=OmnidateButtonWidth %>";
        MyButton<%=TargetMember.MemberID %>.info = "";
        MyButton<%=TargetMember.MemberID %>.iheight = "0";
        MyButton<%=TargetMember.MemberID %>.iwidth = "0";
    </script>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plcOmnidateButton" Visible="false">
    <mn:Txt runat="server" ID="txtOpenOmniBtnWrap" Visible="false" />
    <div runat="server" id="divButton" class="item omnidate">
        <asp:PlaceHolder runat="server" ID="plcPayingMember" Visible="false">
            <script type="text/javascript" language="JavaScript">

                Omni_Button('userid', '<%=TargetMember.MemberID %>', 'hover', MyText, 'style', MyButton<%=TargetMember.MemberID %>);
            </script>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcNonPayingMember" Visible="false">
            <asp:HyperLink runat="server" ID="lnkInvitationButton">
                <mn:Image runat="server" ID="imgInvitationButton" />
            </asp:HyperLink>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcNonPayingMemberMatchesBeta" Visible="false">
            <asp:HyperLink runat="server" ID="lnkInvitationButtonMatchesBeta">
                <mn:Image runat="server" ID="imgInvitationButtonMatchesBeta" />
            </asp:HyperLink>
        </asp:PlaceHolder>
        <div id="onmnidateHover" runat="server" style="display: none;" visible="false" class="omnidate-hover clearfix">
            <h3>
                <asp:Label runat="server" ID="lblHoverTitle"></asp:Label></h3>
            <mn:Image ID="Image1" FileName="ui-omnidate-hover-tmb.jpg" runat="server" />
            <mn:Txt runat="server" ID="txtHoverContent" ResourceConstant="TXT_HOVER_CONTENT" />
        </div>
    </div>
    <mn:Txt runat="server" ID="txtCloseOmniBtnWrap" Visible="false" />
</asp:PlaceHolder>
