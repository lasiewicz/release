<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EventTopNav.ascx.cs" Inherits="Matchnet.Web.Applications.Events.EventTopNav" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<table border="0" cellspacing="4" cellpadding="1" dir="ltr" runat="server" ID="tblEventTopNav">
	<tr>
		<td id="tdEventsParties">&nbsp;&nbsp;<a href="events.aspx?EventTypeID=1"><mn:Txt id="txtEventsParties" runat="server" ResourceConstant="TXT_EVENTS_PARTIES" /></a>&nbsp;&nbsp;|</td>
		<td id="tdEventsPartiesOn" class="bold" visible="False">&nbsp;&nbsp;<span style="FONT-WEIGHT:bold"><mn:Txt id="txtEventsPartiesOn" runat="server" ResourceConstant="TXT_EVENTS_PARTIES" /></span>&nbsp;&nbsp;|</td>
		<td id="tdTravelAdventures">&nbsp;&nbsp;<a href="events.aspx?EventTypeID=2"><mn:Txt id="txtTravelAdventures" runat="server" ResourceConstant="TXT_EVENTS_TRAVEL_ADVENTURES" /></a>&nbsp;&nbsp;|</td>
		<td id="tdTravelAdventuresOn" class="bold" visible="False">&nbsp;&nbsp;<span style="FONT-WEIGHT:bold"><mn:Txt id="txtTravelAdventuresOn" runat="server" ResourceConstant="TXT_EVENTS_TRAVEL_ADVENTURES" /></span>&nbsp;&nbsp;|</td>
		<td id="tdEventsPhotoAlbums">&nbsp;&nbsp;<a href="events.aspx"><mn:Txt id="txtEventsPhotoAlbums" runat="server" ResourceConstant="TXT_EVENTS_PHOTO_ALBUMS" /></a>&nbsp;&nbsp;|</td>
		<td id="tdEventsPhotoAlbumsOn" class="bold" visible="False">&nbsp;&nbsp;<span style="font-weight:bold;"><mn:Txt id="txtEventsPhotoAlbumsOn" runat="server" ResourceConstant="TXT_EVENTS_PHOTO_ALBUMS" /></span>&nbsp;&nbsp;|</td>
		<td id="tdEventsFAQ">&nbsp;&nbsp;<a href="/Applications/Article/ArticleDefault.aspx?CategoryID=356&ArticleID=2444&HideLeftNav=1"><mn:Txt id="txtEventsFAQ" runat="server" ResourceConstant="TXT_EVENTS_FAQS" /></a>&nbsp;&nbsp;|</td>
		<td id="tdEventsFAQOn" class="bold" visible="False">&nbsp;&nbsp;<span style="font-weight:bold;"><mn:Txt id="txtEventsFAQOn" runat="server" ResourceConstant="TXT_EVENTS_FAQS" /></span>&nbsp;&nbsp;|</td>
		<td id="tdEventsQuestions">&nbsp;&nbsp;<a href="/Applications/ContactUs/ContactUs.aspx"><mn:Txt id="txtEventsQuestions" runat="server" ResourceConstant="TXT_EVENTS_QUESTIONS" /></a>&nbsp;&nbsp;</td>
		<td id="tdEventsQuestionsOn" class="bold" visible="False">&nbsp;&nbsp;<span style="FONT-WEIGHT:bold"><mn:Txt id="txtEventsQuestionsOn" runat="server" ResourceConstant="TXT_EVENTS_QUESTIONS" /></span>&nbsp;&nbsp;</td>
	</tr>	
</table>
