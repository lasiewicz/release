#region System References
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

#region Matchnet Web App References
using Matchnet.Web.Framework;
#endregion

#region Matchnet Service References
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Events;
#endregion

namespace Matchnet.Web.Applications.Events
{
	public class upEvent : FrameworkControl
	{
		protected Label lblEventContent;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (Page.IsPostBack != true) 
				{
					LoadEvent();
				}
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void LoadEvent()
		{
			try
			{
				int EventId = Constants.NULL_INT;
				if (Request.QueryString["EventId"] != null)
				{
					EventId = Convert.ToInt32(Request.QueryString["EventId"]);
					
					Matchnet.Content.ValueObjects.Events.Event oEvent = new Event();
					oEvent = EventSA.Instance.GetEvent(EventId);
					
					if (Request.QueryString["EventTypeId"] != null)
					{
						lblEventContent.Text = oEvent.Content;
					}
					else //Photo
					{
						lblEventContent.Text = oEvent.AlbumContent;
					}
				}
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
