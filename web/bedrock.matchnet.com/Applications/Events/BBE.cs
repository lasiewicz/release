﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Applications.Events
{
    [Serializable]
    public class BBE
    {
        [XmlElement()]
        public string ViewedPromoDate { get; set; }
        [XmlElement()]
        public string PasscodeRequestDate { get; set; }
        [XmlElement()]
        public string Passcode { get; set; }
    }
}