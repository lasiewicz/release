<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Offer.ascx.cs" Inherits="Matchnet.Web.Applications.Termination.Offer" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="rightNew">
	<h1><mn:txt id="txtTerminateMembership" runat="server" ResourceConstant="TXT_TERMINATE_MEMBERSHIP"></mn:txt></h1>
	<mn:txt id="txtOffer" runat="server" ExpandImageTokens="True"></mn:txt> 
	<p><cc1:frameworkbutton id="btnNo" runat="server" ResourceConstant="NO_THANKS__WANT_TO_CANCEL" CssClass="activityButton"></cc1:frameworkbutton> 
		<cc1:frameworkbutton id="btnYes" runat="server" ResourceConstant="YES_ACCEPT_OFFER" CssClass="activityButton"></cc1:frameworkbutton></p>
	<input type="hidden" id="Tier" runat="server" name="Tier">
	<input type="hidden" id="TransactionReasonID" runat="server" NAME="TransactionReasonID">
	<input type="hidden" id="SaveOfferID" runat="server" NAME="SaveOfferID">
</div><!-- end Right Div area -->
