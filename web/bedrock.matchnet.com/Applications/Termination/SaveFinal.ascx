<%@ Control Language="c#" AutoEventWireup="false" Codebehind="SaveFinal.ascx.cs" Inherits="Matchnet.Web.Applications.Termination.SaveFinal" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="rightNew">
	<h1><mn:txt id="txtTerminateMembership" runat="server" ResourceConstant="TXT_THANK_YOU"></mn:txt></h1>

	<table border="0" cellpadding="0" cellspacing="0" width="600">
		<tr>
			<td class="bold"><mn:txt id="txtSaveFinal1" runat="server" ResourceConstant="SAVE_FINAL_1" /></td>
		</tr>
		<tr>
			<td><br /></td>
		</tr>
		<tr>
			<td ><mn:txt id="txtSaveFinal2" runat="server" ResourceConstant="SAVE_FINAL_2" /></td>
		</tr>
		<tr>
			<td><br /></td>
		</tr>
		<tr>
			<td><br /></td>
		</tr>
	</table>
</div>
