﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="TerminateReason20.ascx.cs"
    Inherits="Matchnet.Web.Applications.Termination.TerminateReason20" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="page-container">
    <asp:PlaceHolder ID="plcDefault" runat="server">
        <mn:Title ID="ttlTerminateMembership" ResourceConstant="TXT_TERMINATE_MEMBERSHIP"
            runat="server"></mn:Title>
        <h3 class="border-btm-dotted">
            <mn:Txt ID="txtSelectTerminationReason" ResourceConstant="SELECT_TERMINATION_REASON"
                runat="server"></mn:Txt>
        </h3>
        <div>
            <mn:RequiredFieldValidator ID="valTerminationReason" runat="server" ControlToValidate="radTerminationReason"
                ErrorResourceConstant="TERMINATION_REASON_REQUIRED" Display="Dynamic"></mn:RequiredFieldValidator>
            <asp:RadioButtonList ID="radTerminationReason" runat="server" CellPadding="2" EnableViewState="False"
                AutoPostBack="True" RepeatLayout="Table" CellSpacing="0" Width="700" RepeatColumns="2">
            </asp:RadioButtonList>
        </div>
        <div runat="server" id="divTrialPay">
            <div id="terminateTrialPayBanner">
                <div id="terminateTrialPayButton">
                    <a href='<%= GetTrialPayLink() %>'>
                        <mn:Image ID="Image2" runat="server" NAME="Image2" Border="0" FileName="btn_trialPay_checkout.gif"
                            alt="TrialPay"></mn:Image></a>
                </div>
                <div id="terminateTrialPayLink">
                    <a href="/Applications/Subscription/TrialPay.aspx">
                        <mn:Txt ID="Txt2" ResourceConstant="TRIALPAY_LEARNMORE" runat="server"></mn:Txt>
                    </a>
                </div>
            </div>
        </div>
        <p class="border-top-dotted">
            <mn:Txt ID="Txt1" ResourceConstant="CANCEL_UNDERSTAND" runat="server" ExpandImageTokens="true">
            </mn:Txt>
        </p>
        <p class="text-center">
            <cc1:FrameworkButton ID="btnContinue" ResourceConstant="CONTINUE_WITH_CANCELLATION"
                runat="server" CssClass="btn primary"></cc1:FrameworkButton></p>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcNewFlow" Visible="false">

     <mn:Title ID="Title1" ResourceConstant="TXT_TERMINATE_MEMBERSHIP"
            runat="server"></mn:Title>
        <h3 class="border-btm-dotted">
            <mn:Txt ID="txt4" ResourceConstant="SELECT_TERMINATION_REASON"
                runat="server"></mn:Txt>
        </h3>

        <asp:RadioButtonList runat="server" ID="radTerminationReasonNew" CssClass="termination-container">
            <asp:ListItem Value="9"></asp:ListItem>
            <asp:ListItem Value="13"></asp:ListItem>
            <asp:ListItem Value="6"></asp:ListItem>
            <asp:ListItem Value="2"></asp:ListItem>
        </asp:RadioButtonList>
        <p class="border-top-dotted">
            <mn:Txt ID="Txt3" ResourceConstant="CANCEL_UNDERSTAND" runat="server" ExpandImageTokens="true">
            </mn:Txt>
        </p>
        <p class="text-center">
            <cc1:FrameworkButton ID="btnContinueNew" ResourceConstant="CONTINUE_WITH_CANCELLATION"
                runat="server" CssClass="btn primary"></cc1:FrameworkButton></p>
    </asp:PlaceHolder>
</div>
