﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="TerminateFinal20.ascx.cs" Inherits="Matchnet.Web.Applications.Termination.TerminateFinal20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<div id="page-container">
	<mn:txt id="txtTerminateMembership" runat="server" ResourceConstant="TXT_TERMINATE_MEMBERSHIP"></mn:txt>
	
	<div>
	    <mn:txt id="txtTerminateFinal1" runat="server" ResourceConstant="TERMINATE_FINAL_1" />
	    <p><mn:txt id="txtTerminateFinal2" runat="server" ResourceConstant="TERMINATE_FINAL_2" /></p>
	</div>
	<%--<mn:AdUnit id="AdUnitTerminationFinalTop" runat="server" expandImageTokens="true" GAMAdSlot="terminateconfirm_top_728x90" />--%>
	<mn:AdUnit id="AdUnitTerminationFinalMiddle" runat="server" expandImageTokens="true" GAMAdSlot="terminateconfirm_middle_640x192" />
</div>
