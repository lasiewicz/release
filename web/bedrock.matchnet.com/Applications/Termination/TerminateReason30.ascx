﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="TerminateReason30.ascx.cs"
    Inherits="Matchnet.Web.Applications.Termination.TerminateReason30" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="page-container">
    <style type="text/css">
        
        #divMatches {
            border: solid 2px black;
            width: 200px;
            height: 100px;
            margin-top: 5px;
            margin-bottom: 5px;
        }
        
        .divMatches span {
            margin-top: 15px;
            margin-bottom: 5px;
            margin-left: 5px;
            font-weight: bold;
            text-align: left;
            color: #000000;
        }
        
        .reasonsStyle {
             
        }
        
        .reasonsStyle label{
            width: 45%;
            display:inline-block;
        }
        
        .messageHolder {
            margin-top: 20px;
            
        }
        
    </style>
     <script type="text/javascript">
         $j(document).ready(function () {

             $j(":radio").attr('onclick', '');

             $j(":radio").live('change', function () {

                 switch ($j(this).val()) {

                     case '2':
                         $j('#divMessageHolder').html($j('#divHolder0').html());
                         break;
                     case '8':
                         $j('#divMessageHolder').html($j('#divHolder11').html());
                         break;
                     case '14':
                         $j('#divMessageHolder').html($j('#divHolder1').html());
                         break;
                     case '7':
                         $j('#divMessageHolder').html($j('#divHolder12').html());
                         break;
                     case '15':
                         $j('#divMessageHolder').html($j('#divHolder5').html());
                         break;
                     case '9':
                         $j('#divMessageHolder').html($j('#divHolder14').html());
                         break;
                     case '51':
                         $j('#divMessageHolder').html($j('#divHolder9').html());
                         break;
                     case '79':
                         $j('#divMessageHolder').html($j('#divHolder13').html());
                         break;
                     case '6':
                         $j('#divMessageHolder').html($j('#divHolder12').html());
                         break;
                     case '80':
                         $j('#divMessageHolder').html('');
                         break;
                     default:
                 }


             });

         });

         


       

     </script>
     
     <div id="divHolder11" style="display:none;">
         <div class="divMatches">
             <span>
             <p>
               <mn:Txt ID="Txt5" ResourceConstant="TXT_DONT_LIKE_MATCHES" runat="server"></mn:Txt> 
             </p>
             </span>
         </div>
     </div>
     
     <div id="divHolder0" style="display:none;">
         <div class="divMatches">
             <span>
             <p>
               <mn:Txt ID="Txt6" ResourceConstant="TXT_I_MET_SOMEONE_ON_JDATE" runat="server"></mn:Txt> 
             </p>
             </span>
         </div>
     </div>
     
     <div id="divHolder1" style="display:none;">
         <div class="divMatches">
             <span>
             <p>
               <mn:Txt ID="Txt10" ResourceConstant="TXT_TAKING_A_BREAK" runat="server"></mn:Txt> 
             </p>
             </span>
         </div>
     </div>
     
     <div id="divHolder5" style="display:none;">
         <div class="divMatches">
             <span>
             <p>
             <mn:Txt ID="Txt7" ResourceConstant="TXT_NOT_ENUFF_MEMBERS_IN_AREA" runat="server"></mn:Txt> 
             </p>
             </span>
         </div>
     </div>
     
     <div id="divHolder9" style="display:none;">
         <div class="divMatches">
             <span>
             <p>
                <mn:Txt ID="Txt8" ResourceConstant="TXT_RATHER_NOT_SAY" runat="server"></mn:Txt> 
             </p>
             </span>
         </div>
     </div>
     
     <div id="divHolder12" style="display:none;">
         <div class="divMatches">
             <span>
             <p>
               <mn:Txt ID="Txt9" ResourceConstant="TXT_NOT_ENOUGH_PEOPLE_REPLY" runat="server"></mn:Txt> 
             </p>
             </span>
         </div>
     </div>
     <div id="divHolder13" style="display:none;">
         <div class="divMatches">
             <span>
             <p>
               <mn:Txt ID="Txt11" ResourceConstant="TXT_WASNT_MEETING_RIGHT_PEOPLE" runat="server"></mn:Txt> 
             </p>
             </span>
         </div>
     </div>
     
     <div id="divHolder14" style="display:none;">
         <div id="Div1">
                <div id="Div2">
                    <a href='<%= GetTrialPayLink() %>'>
                        <mn:Image ID="Image1" runat="server" NAME="Image2" Border="0" FileName="btn_trialPay_checkout.gif"
                            alt="TrialPay"></mn:Image></a>
                </div>
                <div id="Div3">
                    <a href="/Applications/Subscription/TrialPay.aspx">
                        <mn:Txt ID="Txt12" ResourceConstant="TRIALPAY_LEARNMORE" runat="server"></mn:Txt>
                    </a>
                </div>
            </div>
     </div>
     
    

    <asp:PlaceHolder ID="plcDefault" runat="server">
        <mn:Title ID="ttlTerminateMembership" ResourceConstant="TXT_TERMINATE_MEMBERSHIP"
            runat="server"></mn:Title>
        <h3 class="border-btm-dotted">
            <mn:Txt ID="txtSelectTerminationReason" ResourceConstant="SELECT_TERMINATION_REASON"
                runat="server"></mn:Txt>
        </h3>
        <div>
            <mn:RequiredFieldValidator ID="valTerminationReason" runat="server" ControlToValidate="radTerminationReason"
                ErrorResourceConstant="TERMINATION_REASON_REQUIRED" Display="Dynamic"></mn:RequiredFieldValidator>
            <asp:RadioButtonList ID="radTerminationReason" runat="server" CellPadding="2" EnableViewState="False"
                AutoPostBack="True" RepeatLayout="Table" CellSpacing="0" Width="700" RepeatColumns="2">
            </asp:RadioButtonList>
        </div>
        <div runat="server" id="divTrialPay">
            <div id="terminateTrialPayBanner">
                <div id="terminateTrialPayButton">
                    <a href='<%= GetTrialPayLink() %>'>
                        <mn:Image ID="Image2" runat="server" NAME="Image2" Border="0" FileName="btn_trialPay_checkout.gif"
                            alt="TrialPay"></mn:Image></a>
                </div>
                <div id="terminateTrialPayLink">
                    <a href="/Applications/Subscription/TrialPay.aspx">
                        <mn:Txt ID="Txt2" ResourceConstant="TRIALPAY_LEARNMORE" runat="server"></mn:Txt>
                    </a>
                </div>
            </div>
        </div>
        <div id="divMessageHolder" class="messageHolder">
            

            
        </div>
        <p class="border-top-dotted">
            <mn:Txt ID="Txt1" ResourceConstant="CANCEL_UNDERSTAND" runat="server" ExpandImageTokens="true" />
        </p>
        <p class="text-center">
            <cc1:FrameworkButton ID="btnContinue" ResourceConstant="CONTINUE_WITH_CANCELLATION" runat="server" CssClass="btn primary"/>
            
            <span style="padding-left: 20px;"><cc1:FrameworkButton ID="btnCancelOut" ResourceConstant="BTN_NO_I_WANT_TO_KEEP" runat="server" CssClass="btn primary"/></span>
       </p>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcNewFlow" Visible="false">

     <mn:Title ID="Title1" ResourceConstant="TXT_TERMINATE_MEMBERSHIP"
            runat="server"></mn:Title>
        <h3 class="border-btm-dotted">
            <mn:Txt ID="txt4" ResourceConstant="SELECT_TERMINATION_REASON"
                runat="server"></mn:Txt>
        </h3>

        <asp:RadioButtonList runat="server" ID="radTerminationReasonNew" CssClass="termination-container">
            <asp:ListItem Value="9"></asp:ListItem>
            <asp:ListItem Value="13"></asp:ListItem>
            <asp:ListItem Value="6"></asp:ListItem>
            <asp:ListItem Value="2"></asp:ListItem>
        </asp:RadioButtonList>
        <p class="border-top-dotted">
            <mn:Txt ID="Txt3" ResourceConstant="CANCEL_UNDERSTAND" runat="server" ExpandImageTokens="true">
            </mn:Txt>
        </p>
        <p class="text-center">
            <cc1:FrameworkButton ID="btnContinueNew" ResourceConstant="CONTINUE_WITH_CANCELLATION"
                runat="server" CssClass="btn primary"></cc1:FrameworkButton></p>
    </asp:PlaceHolder>
</div>
