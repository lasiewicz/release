﻿using System;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Purchase.ServiceAdapters;
using Spark.Common.Adapter;
using Matchnet.Web.Framework.Managers;


namespace Matchnet.Web.Applications.Termination
{
    public partial class TerminateReason20 : FrameworkControl
    {
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (SettingsManager.GetSettingBool(SettingConstants.ENABLE_NEW_AUTO_RENEWAL_TERMINATION_FLOW, g.Brand))
                {
                    plcNewFlow.Visible = true;
                    plcDefault.Visible = false;
                    radTerminationReasonNew.Items[0].Text = g.GetResource("TXT_TOO_EXPENSIVE_NEW", this);
                    radTerminationReasonNew.Items[1].Text = g.GetResource("TXT_DIFFICULTY_USING_THE_SITE_NEW", this);
                    radTerminationReasonNew.Items[2].Text = g.GetResource("TXT_NOBODY_CONTACTS_ME_NEW", this);
                    radTerminationReasonNew.Items[3].Text = g.GetResource("TXT_FOUND_MY_SOUL_MATE_ON_THE_SITE_NEW", this);
                }
                else
                {
                    if (!MemberPrivilegeAttr.IsCureentSubscribedMember(g) && g.ImpersonateContext == false)
                    {
                        g.Transfer("/Applications/MemberServices/MemberServices.aspx?rc=NO_SUSCRIPTION");
                    }

                    Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(g.TargetMemberID, g.TargetBrand);
                    if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0)
                    {
                        if (!renewalSub.IsRenewalEnabled && g.ImpersonateContext == false)
                        {
                            g.Transfer("/Applications/MemberServices/MemberServices.aspx?rc=SUBSCRIPTION_ALREADY_TERMINATED");
                        }
                    }

                    radTerminationReason.DataSource = Termination.GetTransactionReasons(g.Brand.Site.LanguageID, g.TargetBrand.Site.SiteID);
                    radTerminationReason.DataTextField = "Description";
                    radTerminationReason.DataValueField = "TransactionReasonID";
                    radTerminationReason.DataBind();
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }


        private void btnContinue_Click(object sender, EventArgs e)
        {
            g.Transfer("/Applications/Termination/Offer.aspx?TransactionReasonID=" + radTerminationReason.SelectedValue);
        }

        private void btnContinueNew_Click(object sender, EventArgs e)
        {
            g.Transfer("/Applications/Termination/ContactCustomerSupport.aspx?TransactionReasonID=" + radTerminationReasonNew.SelectedValue);
        }

        private void Page_Load(object sender, EventArgs e)
        {
            HtmlGenericControl divBanner;
            divBanner = (HtmlGenericControl)this.FindControl("divTrialPay");
            divBanner.Style["display"] = "none";
        }
        protected string GetTrialPayLink()
        {
            string strResult = string.Empty;
            string strLink = string.Empty;
            //			strLink=_g.GetResource("TRIALPAY_LINK",this);
            strLink = _g.GetResource("TRIALPAY_LINK");
            if (!strLink.Equals(string.Empty))
            {
                strResult = string.Format(strLink, _g.Member.MemberID.ToString(), _g.Brand.Site.SiteID.ToString());
            }
            return strResult;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            this.radTerminationReason.SelectedIndexChanged += new System.EventHandler(this.radTerminationReason_SelectedIndexChanged);
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            this.btnContinueNew.Click += new System.EventHandler(this.btnContinueNew_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);

        }
        #endregion

        private void radTerminationReason_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (_g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles || _g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate
                || _g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.DateCA)
            {
                HtmlGenericControl divBanner;
                divBanner = (HtmlGenericControl)this.FindControl("divTrialPay");
                Int32 selectedValue = Int32.Parse(radTerminationReason.SelectedValue);
                //MPR-111 Show TrialPay on all auto-renewal options - removing conditions
                divBanner.Style["display"] = "block";

                // OI-125 On Jdate, only show Trial pay on "Too Expensive" and "Not interested in paying for online dating".
                if (_g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
                {
                    if (selectedValue == 9 || selectedValue == 50) // 9 and 50 are the value for (9,'Too expensive') and (50,'online dating is too expensive')
                    {
                        divBanner.Style["display"] = "block";
                    }
                    else
                    {
                        divBanner.Style["display"] = "none";
                    }
                }
            }
        }
    }
}