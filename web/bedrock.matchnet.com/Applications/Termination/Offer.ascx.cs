using System;

using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Member.ServiceAdapters;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Globalization;


namespace Matchnet.Web.Applications.Termination
{
	public class Offer : FrameworkControl
	{
		protected Matchnet.Web.Framework.Txt txtOffer;
		protected Matchnet.Web.Framework.Ui.FormElements.FrameworkButton btnYes;
		protected System.Web.UI.HtmlControls.HtmlInputHidden Tier;
		protected System.Web.UI.HtmlControls.HtmlInputHidden SaveOfferID;
		protected System.Web.UI.HtmlControls.HtmlInputHidden TransactionReasonID;
		protected Matchnet.Web.Framework.Ui.FormElements.FrameworkButton btnNo;

		private void Offer_PreRender(object sender, EventArgs e)
		{
			try
			{
				double transactionReasonID;

				if (Request.QueryString["TransactionReasonID"] != null)
				{
					if (Double.TryParse(Request.QueryString["TransactionReasonID"].ToString(), System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out transactionReasonID))
					{
						PopulateOffer((int)transactionReasonID, GetTier());
						return;
					}
				}

				g.Transfer("/Applications/Termination/TerminateReason.aspx");
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}


		private void btnYes_Click(object sender, EventArgs e)
		{
			double saveOfferID = 0;

			try 
			{
				if (SaveOfferID.Value.Length > 0)
				{
					if (!Double.TryParse(SaveOfferID.Value, System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out saveOfferID))
					{
						//this is bad
					}
				}
				
				int adminMemberID = (g.IsAdmin && g.Member != null) ? g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) : 0;
				SubscriptionResult sr = PurchaseSA.Instance.SaveSaveOffer( g.TargetMemberID, adminMemberID, g.Brand.Site.SiteID, (int)saveOfferID );

				if ( sr.ReturnValue == Constants.RETURN_OK )
				{
					g.Transfer("/Applications/Termination/SaveFinal.aspx");
				}
				else
				{
					g.Notification.AddError( sr.ResourceConstant );
				}
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}


		private int GetTier()
		{
			double tier = 1;

			if (Tier.Value.Length > 0)
			{
				if (Double.TryParse(Tier.Value, System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out tier))
				{
					tier = tier + 1;
				}
				else
				{
					tier = 1;
				}
			}

			return (int)tier;
		}


		private void PopulateOffer(int transactionReasonID, int tier)
		{
			try
			{
				Termination termination = new Termination();
				if (termination.PopulateSaveOffer(g.Brand.Site.SiteID, g.TargetMemberID, transactionReasonID, tier))
				{
					txtOffer.ResourceConstant = termination.SaveOfferResourceConstant;
					Tier.Value = termination.SaveOfferTier.ToString();
					SaveOfferID.Value = termination.SaveOfferID.ToString();
					TransactionReasonID.Value = transactionReasonID.ToString();
				}
				else
				{
					g.Transfer("/Applications/Termination/ProfileSetting.aspx?TransactionReasonID=" + transactionReasonID.ToString());
				}
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
			this.PreRender += new EventHandler(Offer_PreRender);

		}
		#endregion

	}
}

