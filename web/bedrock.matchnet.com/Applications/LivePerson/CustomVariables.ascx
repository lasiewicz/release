﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomVariables.ascx.cs" Inherits="Matchnet.Web.Applications.LivePerson.CustomVariables" %>
<script type="text/javascript">
    var arrLPvars = [
    <asp:PlaceHolder ID="plcLPAttributes" runat="server"></asp:PlaceHolder>
    ];
    lpTag.vars.push(arrLPvars);

    var gLivepersonUserStatus = '<%=GetUserStatus()%>';
    var gLivepersonV15= '<%=GetV15()%>';
    var gLivepersonC17='<%=GetC17()%>'; 
    var gLivepersonC18='<%=GetC18()%>'; 
    var gLivepersonC19='<%=GetC19()%>';
    var gLivepersonV36='<%=GetV36()%>';

</script>    