﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Ui;

using Matchnet.Member.ServiceAdapters;
using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.MatchTest.ValueObjects.MatchMeter;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Applications.CompatibilityMeter.Controls;

namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public partial class Matches30 : FrameworkControl
    {
        #region Methods (6)


        // Protected Methods (1) 

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    /*
                    if (!IsMemeberHasFeedback(g.Member.MemberID))
                    {
                        // Current member hasn't finished the test
                        g.Transfer("/Applications/CompatibilityMeter/Welcome.aspx");
                    }
                    */
                    CompatibilityMeterHandler cmh = new CompatibilityMeterHandler(g);
                    cmh.RedirectIfNotAllowed(CompatibilityMeterHandler.JmeterPage.Matches);
                }
                var bmresults = MatchMeterSA.Instance.GetBestMatches(g.Member.MemberID, g.Brand, mf.intRegionID, mf.intAgeMin, mf.intAgeMax, mf.GetGenderMask(), MemberPrivilegeAttr.IsCureentSubscribedMember(g), 0, 12, mf.PhotoRequired);

                if (bmresults != null) // Results are already cached
                {
                    ShowResults(); // Show the results list panel
                    RenderPage();
                }
                else // Results are not cached yet, show the 'Pending' panel
                {
                    object value = null;
                    Int32 RefreshCount = 0;
                    value = g.Session.Get("Matches_RefreshCount");
                    if (value != null)
                    {
                        RefreshCount = Int32.Parse(value.ToString());
                    }
                    if (RefreshCount > 35) // 5 seconds * 35 is approx 3 minutes
                    {
                        g.Transfer("/Applications/CompatibilityMeter/Error.aspx");
                    }
                    else
                    {
                        RefreshCount++;

                        string path = Request.Url.AbsoluteUri.ToLower();
                        g.Session.Add("Matches_RefreshCount", RefreshCount.ToString(), SessionPropertyLifetime.Temporary);
                        /*
                        g.HeadContent =
                            string.Format(
                            "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"5;" +
                            " URL=" + path) + "\">";
                        */

                        Response.AddHeader("Refresh", "5; url=" + path);

                        ShowPending();
                    }
                }

                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_MATCHES", this),
                                                            g.AppPage.App.DefaultPagePath);

                }
                if (g.BreadCrumbTrailFooter != null)
                {
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_MATCHES", this),
                                                           g.AppPage.App.DefaultPagePath);
                }

                if (bmresults != null && !string.IsNullOrEmpty(g.Page.Request["RedirectToOneOnOne"]))
                {
                    g.Transfer("/Applications//CompatibilityMeter/OneOnOne.aspx?MemberID=" + bmresults.MatchResultsDic.First().Key.MemberID.ToString() + "&Ordinal=1");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);

                if (!g.IsDevMode && !(ex is System.Threading.ThreadAbortException))
                {
                    g.Transfer("/Applications/CompatibilityMeter/Error.aspx");
                }
            }
        }



        // Private Methods (5) 

        private bool IsMemeberHasFeedback(int memberID)
        {
            MatchTestStatus mts = MatchMeterSA.Instance.GetMatchTestStatus(memberID, MemberPrivilegeAttr.IsCureentSubscribedMember(g), g.Brand);
            return (mts == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.MinimumCompleted || mts == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.Completed);
        }

        //protected override void OnPreRender(EventArgs e)
        //{
        //    try
        //    {
        //        _g.AnalyticsOmniture.PageName = "JMeter Matches";
        //    }
        //    catch (Exception ex)
        //    {
        //        g.ProcessException(ex);
        //    }
        //}
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                // Checking if a subscription is requiered to view this page.
                int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                int mmenumval = (Int32)WebConstants.MatchMeterFlags.PayForBestMatches;
                if ((mmsett & mmenumval) == mmenumval && !MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                {
                    // If a sub is needed, redirect to sub page.
                    g.Transfer("/Applications/Subscription/Subscribe.aspx");
                    //Redirect.Subscription(g.Brand, prtid, destinationMemberID, false, Server.UrlEncode(viewProfileLink));
                }
                else
                {
                    // Wire up the List Navigation.
                    g.ListNavigationTop = lblListNavigationTop;
                    g.ListNavigationBottom = lblListNavigationBottom;
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void RenderPage()
        {
            try
            {
                int genderMask;
                int regionID;
                int ageMin;
                int ageMax;

                genderMask = mf.GetGenderMask();
                regionID = mf.intRegionID;
                ageMin = mf.intAgeMin;
                ageMax = mf.intAgeMax;

                // HACK:  They would like the ability to choose from "18 to 18".  In order
                // to accomplish this without making changes to the middle tier, we add 1 to ageMax if ageMin == ageMax.

                //11/07/2006 no need for this Hack, on ResultList.ascx ageMax also incremented by one
                // when passed to MembersOnlineSA in LoadMembersOnlinePage
                //if (ageMin == ageMax)
                //	ageMax++;

                MatchesCollection collection = new MatchesCollection(genderMask, regionID, ageMin, ageMax, mf.PhotoRequired);
                MembersMatcesList.MyMatchesCollection = collection;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void ShowPending()
        {
            plcPending.Visible = true;
            plcGotResults.Visible = false;
            _g.AnalyticsOmniture.PageName = "JMeter Matches Loading";
        }

        private void ShowResults()
        {
            plcPending.Visible = false;
            plcGotResults.Visible = true;
            _g.AnalyticsOmniture.PageName = "JMeter Matches";
        }


        #endregion Methods
    }
}