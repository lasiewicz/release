﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.MatchTest.ValueObjects.MatchMeter;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

using Matchnet.ExternalMail.ServiceAdapters;




namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public partial class Completion30 : FrameworkControl
    {
        #region Methods (7)


        // Public Methods (1) 

        public void miniProfileDataBind(object sender, RepeaterItemEventArgs args)
        {
            MiniProfile20 miniProfile = (MiniProfile20)args.Item.FindControl("toMemberMiniProfile");

            if (miniProfile != null)
            {
                miniProfile.Transparency = true;
            }
        }



        // Protected Methods (3) 

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                _g.AnalyticsOmniture.PageName = "JMeter Completion";
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //lnkYourMatches1.NavigateUrl = "/Applications/CompatibilityMeter/Matches.aspx";
                //lnkYourMatches2.NavigateUrl = "/Applications/CompatibilityMeter/Matches.aspx";
                if (!string.IsNullOrEmpty(Request["fromreturning"]))
                {
                    //lnkYourMatches1.NavigateUrl += "?fromreturning=1";
                    //lnkYourMatches2.NavigateUrl += "?fromreturning=1";
                }

                if (!IsPostBack)
                {
                    SendEmail();
                }

                MatchMeterSA.Instance.SetMatchTestStatusToDefault(g.Member, g.Brand);

                var pfb = MatchMeterSA.Instance.GetPersonalFeedback(g.Member.MemberID, g.Brand, MemberPrivilegeAttr.IsCureentSubscribedMember(g));
               
                if (pfb.MemberStatus == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.MinimumCompleted || pfb.MemberStatus == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.Completed)
                {
                    int memberID = getMemberId();
                    if (memberID > 0)
                    {
                        Member.ServiceAdapters.Member aMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                        //var pfbTarget = MatchMeterSA.Instance.GetPersonalFeedback(memberID, g.Brand.Site.LanguageID, aMember.IsPayingMember(g.Brand.Site.SiteID));
                        MatchTestStatus mtsTarget = MatchMeterSA.Instance.GetMatchTestStatus(memberID, aMember.IsPayingMember(g.Brand.Site.SiteID), g.Brand);
                        if (mtsTarget == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.MinimumCompleted || mtsTarget == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.Completed)
                        {
                            //  If target, member ID exists and he finished the test, we want to show one on one match results
                            g.Transfer("/Applications/CompatibilityMeter/OneOnOne.aspx?MemberID=" + memberID.ToString());
                        }
                        else
                        {
                            // Show the carrot on top of the page
                            memberProfile.Visible = true;
                            bindMiniProfile(aMember);
                        }
                    }

       
                    rptFeedbackCategories.DataSource = pfb.FeedbackCategories;
                    rptFeedbackCategories.DataBind();

                    personalFeedbackTop.PFB = pfb;
                }
                else
                {
                    g.Transfer("/Applications/CompatibilityMeter/Welcome.aspx");
                }


                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TTL_HEADER", this),
                                                            g.AppPage.App.DefaultPagePath);

                }
                if (g.BreadCrumbTrailFooter != null)
                {
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TTL_HEADER", this),
                                                           g.AppPage.App.DefaultPagePath);
                }
            }

            catch (Exception ex)
            {
                g.ProcessException(ex);

                if (!g.IsDevMode && !(ex is System.Threading.ThreadAbortException))
                {
                    g.Transfer("/Applications/CompatibilityMeter/Error.aspx");
                }
            }
        }



        // Private Methods (3) 

        private void bindMiniProfile(Member.ServiceAdapters.Member aMember)
        {
            ArrayList toMember = new ArrayList();

            toMember.Add(aMember);
            memberProfile.DataSource = toMember;
            memberProfile.DataBind();
        }

        private int getMemberId()
        {
            try
            {
                if (Request.Params.Get("MemberID") != null)
                {
                    return int.Parse(Request.Params.Get("MemberID"));
                }
            }
            catch (FormatException)
            {
                return 0;
            }
            return 0;
        }

        private void SendEmail()
        {
            if (Request.Params.Get("SendMail") != null)
            {
                ExternalMailSA.Instance.SendMatchMeterCongratsMail(g.Brand.BrandID, g.Brand.Site.SiteID, g.Member.MemberID, g.Member.GetUserName(_g.Brand), g.Member.EmailAddress);
                _g.AnalyticsOmniture.AddEvent("event17");
            }
        }


        #endregion Methods
    }
}