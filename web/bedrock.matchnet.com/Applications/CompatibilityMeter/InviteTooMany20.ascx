﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InviteTooMany20.ascx.cs" Inherits="Matchnet.Web.Applications.CompatibilityMeter.InviteTooMany20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<mn:Txt runat="server" ID="txtInviteTooManyText" ResourceConstant="TXT_INVITE_TOO_MANY_TEXT" />
<asp:HyperLink runat="server" ID="hlBack">
   <mn:Txt runat="server" ID="txtBack" ResourceConstant="TXT_BACK" /> 
</asp:HyperLink>

