﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.MatchTest.ValueObjects.MatchMeter;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public class CompatibilityMeterHandler
    {
        ContextGlobal g;

        public enum JmeterPage
        {
            Matches,
            OneOnOne,
            HotList,
            Welcome,
            ReturningUser,
            PersonalityTest,
            Completion,
            Error
        }

        private const string WELCOME_PAGE_URL = "/Applications/CompatibilityMeter/Welcome.aspx";
        private const string RETURNING_USER_PAGE_URL = "/Applications/CompatibilityMeter/ReturningUser.aspx";

        public CompatibilityMeterHandler(ContextGlobal _g)
        {
            g = _g;
        }

        /// <summary>
        /// Checking if the member has exceeded the jmeter invites for the target member
        /// The J♥meter invitation can be sent once to a user in a period time of 30 days
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if [is at quota per member]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsAtQuotaPerMember(int memberID)
        {
            bool result = false;

            var MMSentInvite = ListSA.Instance.GetList(g.Member.MemberID);
            var listItemDetail = MMSentInvite.GetListItemDetail(HotListCategory.MemberYouSentJmeterInvitationTo, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, memberID);
            if (listItemDetail != null)
            {
                DateTime dateInviteSent = listItemDetail.ActionDate;
                if (DateTime.Now.Subtract(dateInviteSent) < new TimeSpan(30, 0, 0, 0))
                {
                    result = true;
                }
            }
            return result;

        }
        /// <summary>
        /// Checking if the member has exceeded the jmeter invites for today. 30 every 24 hrs.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if [is at quata per day]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsAtQuotaPerDay()
        {
            bool result = false;
            int sendTest;

            if (QuotaSA.Instance.IsAtQuota(g.Brand.Site.Community.CommunityID, g.Member.MemberID, QuotaType.SendJmeterInvitation, out sendTest))
            {
                result = true;
            }
            return result;
        }

        private bool IsMemberOfThisSite(IMemberDTO aMember)
        {
            bool result = true;

            if (aMember != null)
            {
                bool isMemberOfOtherSites = false; // That are in this community
                bool isMemberOfCurrentSite = false;
                var brands = BrandConfigSA.Instance.GetBrandsByCommunity(g.Brand.Site.Community.CommunityID);
                int[] siteIDList = aMember.GetSiteIDList();
                if (siteIDList != null)
                {
                    foreach (int siteID in siteIDList)
                    {
                        if (siteID != g.Brand.Site.SiteID)
                        {
                            bool otherInCommu = false;
                            foreach (Brand b in brands)
                            {
                                if (siteID == b.Site.SiteID)
                                {
                                    otherInCommu = true;
                                    break;
                                }
                            }
                            if (!isMemberOfOtherSites && otherInCommu)
                            {
                                isMemberOfOtherSites = true;
                            }
                        }
                        if (siteID == g.Brand.Site.SiteID)
                        {
                            if (!isMemberOfCurrentSite)
                            {
                                isMemberOfCurrentSite = true;
                            }
                        }
                    }
                    // aMember is a member of jdil
                    if (isMemberOfCurrentSite)
                    {
                        // aMember is a member of other sites as well
                        if (isMemberOfOtherSites)
                        {
                            // Get the lats login date to jdil
                            DateTime lastLoginDate = Convert.ToDateTime(aMember.GetAttributeDate(g.Brand, "BrandLastLogonDate", DateTime.MinValue));
                            // Checks if the last login date to jdil was in the last six month
                            if (lastLoginDate != DateTime.MinValue && (DateTime.Now.Subtract(lastLoginDate) > new TimeSpan(30 * 6, 0, 0, 0)))
                            {
                                result = false;
                            }

                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            return result;
        }

        public bool IsMemeberHasFeedback()
        {
            if (g.Member == null) return false;

            MatchTestStatus mts = MatchMeterSA.Instance.GetMatchTestStatus(g.Member.MemberID, MemberPrivilegeAttr.IsCureentSubscribedMember(g), g.Brand);
            return (mts == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.MinimumCompleted || mts == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.Completed);
        }

        public bool IsMemeberHasFeedback(Member.ServiceAdapters.Member member)
        {
            MatchTestStatus mts = MatchMeterSA.Instance.GetMatchTestStatus(member.MemberID, member.IsPayingMember(g.Brand.Site.SiteID), g.Brand);
            return (mts == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.MinimumCompleted || mts == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.Completed);

        }

        public bool IsPayingMember()
        {
            return MemberPrivilegeAttr.IsCureentSubscribedMember(g);
        }

        public bool IsJmeterPremium()
        {
            Matchnet.PremiumServices.ValueObjects.InstanceMember member = Matchnet.PremiumServices.ServiceAdapter.ServiceMemberSA.Instance.GetMemberService(g.Brand.BrandID, g.Member.MemberID, (int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.ServiceIDs.JMeter);
            return (member != null) ? true : false;
        }




        public string CheckSendInvitationNotification(IMemberDTO aMember)
        {
            string notification = string.Empty;
            if (IsMemberOfThisSite(aMember))
            {

                if (!IsAtQuotaPerDay())
                {
                    if (IsAtQuotaPerMember(aMember.MemberID))
                    {
                        notification = "JMETER_INVITATION_QUOTA_PER_MEMBER_EXCEEDED";
                    }
                }
                else
                {
                    notification = "JMETER_INVITATION_QUOTA_PER_DAY_EXCEEDED";
                }
            }
            else
            {
                notification = "JMETER_INVITATION_NOT_A_MEMBER_OF_THIS_SITE";
            }
            return notification;
        }

        internal void RedirectIfNotAllowed(JmeterPage page)
        {
            string redirectURL = string.Empty;
            if (!IsMemeberHasFeedback())
            {
                redirectURL = WELCOME_PAGE_URL;
            }
            else
            {
                redirectURL = GetNextPage(page);
            }
            if (!string.IsNullOrEmpty(redirectURL))
            {
                g.Transfer(redirectURL);
            }
        }

        public bool CheckJMeterFlag(WebConstants.MatchMeterFlags flags)
        {
            int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID,
                                                                 g.Brand.Site.SiteID, g.Brand.BrandID));

            int mmenumval = (Int32)flags;
            return ((mmsett & mmenumval) == mmenumval);
        }

        private string GetNextPage(JmeterPage page)
        {
            string nextPageURL = string.Empty;
            int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID,
                                                                  g.Brand.Site.SiteID, g.Brand.BrandID));
            int mmenumval = 0;

            switch (page)
            {
                case JmeterPage.Matches:
                    mmenumval = (Int32)WebConstants.MatchMeterFlags.PremiumForBestMatches;
                    if ((mmsett & mmenumval) == mmenumval)
                    {
                        if (!IsJmeterPremium())
                        {
                            if (!string.IsNullOrEmpty(g.Page.Request["fromTopNav"]))
                            {
                                int prtid = 1913;
                                nextPageURL = GetNextPageURL(prtid);
                            }
                            else if (!string.IsNullOrEmpty(g.Page.Request["fromJMeterMenu"]))
                            {
                                int prtid = 1914;
                                nextPageURL = GetNextPageURL(prtid);
                            }
                            else if (string.IsNullOrEmpty(g.Page.Request["fromreturning"]))
                            {
                                nextPageURL = RETURNING_USER_PAGE_URL;
                                if (!string.IsNullOrEmpty(g.Page.Request["RedirectToOneOnOne"]))
                                {
                                    int prtid = 1409;
                                    nextPageURL = GetNextPageURL(prtid);
                                }
                            }

                            else
                            {
                                int prtid = 665;
                                if (!string.IsNullOrEmpty(g.Page.Request["RedirectToOneOnOne"]))
                                {
                                    prtid = 1409;
                                }
                                nextPageURL = GetNextPageURL(prtid);
                            }
                        }
                    }
                    break;
                case JmeterPage.OneOnOne:
                    mmenumval = (Int32)WebConstants.MatchMeterFlags.PremiumFor1On1;
                    if ((mmsett & mmenumval) == mmenumval)
                    {
                        if (!IsJmeterPremium())
                        {
                            if (string.IsNullOrEmpty(g.Page.Request["fromlookup"]))
                            {
                                nextPageURL = GetNextPageURL(Constants.NULL_INT);
                            }
                            else
                            {
                                nextPageURL = GetNextPageURL(666);
                            }
                        }
                    }
                    break;
            }
            return nextPageURL;
        }

        public JmeterPage GetCurrentPage()
        {
            JmeterPage currentPage = JmeterPage.Welcome;
            switch (g.AppPage.PageName.ToLower())
            {
                case "welcome": currentPage = JmeterPage.Welcome; break;
                case "personalitytest": currentPage = JmeterPage.PersonalityTest; break;
                case "returninguser": currentPage = JmeterPage.ReturningUser; break;
                case "completion": currentPage = JmeterPage.Completion; break;
                case "matches": currentPage = JmeterPage.Matches; break;
                case "oneonone": currentPage = JmeterPage.OneOnOne; break;
                case "error": currentPage = JmeterPage.Error; break;
            }

            return currentPage;
        }

        public MatchTestStatus GetMatchTestStatus(IMemberDTO member)
        {
            MatchTestStatus mts = MatchMeterSA.Instance.GetMatchTestStatus(member.MemberID, member.IsPayingMember(g.Brand.Site.SiteID), g.Brand);
            return mts;
        }

        public static bool IsJMeterV3(Brand brand)
        {
            return MatchMeterSA.Instance.IsJMeterV3(brand);
        }

        public bool IsJMeterMatchesEnabled()
        {
            int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID,
                                                                  g.Brand.Site.SiteID, g.Brand.BrandID));
            int mmenumval = 0;
            mmenumval = (Int32)WebConstants.MatchMeterFlags.ShowBestMatches;
            return ((mmsett & mmenumval) == mmenumval);
        }

        public static bool IsJMeterEnabledAsPremium(ContextGlobal g)
        {
            int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID,
                                                                  g.Brand.Site.SiteID, g.Brand.BrandID));

            bool isEnabled = (mmsett & ((int)WebConstants.MatchMeterFlags.EnableApp)) == ((int)WebConstants.MatchMeterFlags.EnableApp)
                            &&
                            ((mmsett & ((int)WebConstants.MatchMeterFlags.PremiumForBestMatches)) == ((int)WebConstants.MatchMeterFlags.PremiumForBestMatches)
                                || (mmsett & ((int)WebConstants.MatchMeterFlags.PremiumFor1On1)) == ((int)WebConstants.MatchMeterFlags.PremiumFor1On1));

            return isEnabled;
        }

        private string GetNextPageURL(int prtid)
        {
            string url = string.Empty;
            if (!IsPayingMember())
            {
                url  = FrameworkGlobals.GetSubscriptionLink(false, prtid) + "&DestinationURL=" + HttpContext.Current.Server.UrlEncode(HttpContext.Current.Items["FullURL"].ToString());
            }
            else
            {
                url = "/Applications/Subscription/PremiumServices.aspx?PackagesToDisplay=JM&prtid=" + prtid + "&DestinationURL=" + HttpContext.Current.Server.UrlEncode(HttpContext.Current.Items["FullURL"].ToString()); ;

            }

            return url;
        }
    }
}
