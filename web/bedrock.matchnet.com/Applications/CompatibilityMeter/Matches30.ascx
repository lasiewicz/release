﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Matches30.ascx.cs" Inherits="Matchnet.Web.Applications.CompatibilityMeter.Matches30" %>
<%@ Register TagPrefix="uc1" TagName="MatchesFilter" Src="Controls/MatchesFilter30.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/BasicElements/ResultList20.ascx" %>
<script language="javascript">
    var Page_BlockSubmit = false;
</script>
<script type="text/javascript" src="../../javascript/swfobject.js"></script>
<script type="text/javascript">
    swfobject.registerObject("jmeterPreloader", "9.0.0");
</script>
<asp:PlaceHolder runat="server" ID="plcGotResults">
    <div class="jmeter-profile jmeter-matches">
        <mn:Title runat="server" ID="ttlMatches" ResourceConstant="TXT_MATCHES" />
        <uc1:MatchesFilter ID="mf" runat="server" />
        <input id="StartRow" type="hidden" name="StartRow" runat="server">
        <%--  <div id="reviseLinks">
        <asp:Label ID="lblListNavigationTop" runat="server" /></div>--%>
        <div id="jmeter-results-reviseLinks">
            <asp:Label ID="lblListNavigationTop" runat="server" />
        </div>
        <result:ResultList ID="MembersMatcesList" runat="server" ResultListContextType="MatchMeterMatches">
        </result:ResultList>
        <div id="jmeter-results-reviseLinks">
            <asp:Label ID="lblListNavigationBottom" runat="server" /></div>
        <%--   <div id="reviseLinks">
        <asp:Label ID="lblListNavigationBottom" runat="server" /></div>--%>
        <%--<table>
    </table>--%>
        <%-- <input type="hidden" value="<% = ChangeVoteMessage %>" name="ChangeVoteMessage">--%>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plcPending">
    <mn:Txt ID="jmeterPreloader" runat="server" ExpandImageTokens="true" ResourceConstant="JMETER_PRELOADER" />
</asp:PlaceHolder>
