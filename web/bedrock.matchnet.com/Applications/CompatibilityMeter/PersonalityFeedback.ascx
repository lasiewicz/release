﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonalityFeedback.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.PersonalityFeedback" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="FeedbackCategoryItem" Src="Controls/FeedbackCategoryItem.ascx" %>
<div id="div_print">
    <!--<asp:Repeater runat="server" ID="rptFeedbackCategories">
        <ItemTemplate>
            <uc1:FeedbackCategoryItem runat="server" FBCItem='<%# Container.DataItem%>' />
        </ItemTemplate>
    </asp:Repeater>-->
    <div class="one-on-one-titles">
        <mn:Title runat="server" ID="ttlOneOnOne" ResourceConstant="TTL_FEEDBACK" />
        <h2>
            <mn:Txt runat="server" ID="ttlSubOneOnOne" ResourceConstant="TTL_SUB_FEEDBACK" />
        </h2>
    </div>
</div>
