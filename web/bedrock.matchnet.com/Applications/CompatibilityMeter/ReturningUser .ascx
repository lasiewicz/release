﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReturningUser .ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.ReturningUser" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<asp:Button runat="server" ID="btnMatches" Text="התאמות JMETER" OnClick="btnMatches_Click" />
<asp:Button runat="server" ID="btnCompletion" Text="Your Results" 
    onclick="btnCompletion_Click" />
<div id="lookUpRight">
    <uc1:lookupbyusername id="LookupByUsername" runat="server" />
</div>
<div id="lookUpRight">
    <uc1:sendtoafriend id="SendToAFriend" runat="server"></uc1:sendtoafriend>
</div>
