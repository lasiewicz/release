﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SendToAFriend20.ascx.cs" Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.SendToAFriend20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<strong>
    <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_SEND_TO_FRIEND" ExpandImageTokens="True" />
</strong>
<br />
<asp:TextBox runat="server" ID="txtEMail"></asp:TextBox>
<mn2:FrameworkButton ID="btnSend" runat="server" CssClass="activityButton" ResourceConstant="BTN_SEND"
    CausesValidation="false" OnClick="btnSend_Click" />
