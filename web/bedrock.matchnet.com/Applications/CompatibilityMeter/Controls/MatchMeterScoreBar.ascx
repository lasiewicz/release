﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchMeterScoreBar.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.MatchMeterScoreBar" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<mn:Image runat="server" ID="imgScore7" FileName="heart-empty.png"  />
<mn:Image runat="server" ID="imgScore6" FileName="heart-empty.png"  />
<mn:Image runat="server" ID="imgScore5" FileName="heart-empty.png"  />
<mn:Image runat="server" ID="imgScore4" FileName="heart-empty.png" />
<mn:Image runat="server" ID="imgScore3" FileName="heart-empty.png" />
<mn:Image runat="server" ID="imgScore2" FileName="heart-empty.png" />
<mn:Image runat="server" ID="imgScore1" FileName="heart-empty.png" />
<mn:Image runat="server" ID="imgJmeter" FileName="jmeter_logo_galleryView_b.gif" class="jmeter-1on1-from-profile" Style="vertical-align: top;" />