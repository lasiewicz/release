using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Lib;
using Matchnet.Lib.Exceptions;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.CompatibilityMeter.Controls
{

    /// <summary>
    ///		Summary description for LookupByUsername.
    /// </summary>
    public class LookupByUsername : FrameworkControl
    {
        protected HtmlInputText LookupUserName;
        protected Matchnet.Web.Framework.Txt Txt1;
        protected Matchnet.Web.Framework.Txt Txt6;
        protected Matchnet.Web.Framework.Ui.FormElements.FrameworkButton btnLookupByUserName;
        protected MultiValidator LookupUserNameValidator;
        protected Panel pnlBetaMessage;
        protected HyperLink lnkCloseText;
        protected HyperLink lnkImage;

        public bool ButtonEnabled
        {
            get
            {
                return btnLookupByUserName.Enabled;
            }
            set
            {
                btnLookupByUserName.Enabled = value;
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    //string userName = this.g.Member.GetUserName(_g.Brand);

                    //if (userName != "")
                    //{
                    //    this.LookupUserName.Value = userName;
                    //}
                    this.LookupUserName.Value = g.GetResource("TEXTBOX_TEXT", this);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void btnLookupByUserName_Click(object sender, EventArgs e)
        {
            try
            {
                string userName = this.LookupUserName.Value.Trim();

                if (userName != String.Empty && userName.IndexOf(".") < 0 && userName.IndexOf("/") < 0)
                {
                    if (userName != g.Member.GetUserName(_g.Brand))
                    {
                        int memberID = MemberSA.Instance.GetMemberID(userName, g.Brand.Site.Community.CommunityID);
                        // Check if memberID could be found by userName
                        if (memberID == Constants.NULL_INT || memberID == 0 || memberID == int.MinValue)
                        {
                            // If not
                            if (int.TryParse(userName, out memberID))
                            {
                                // If the textbox contains member id, memberID will hold its value
                            }
                        }
                        if (memberID != Constants.NULL_INT && memberID != 0 && memberID != int.MinValue)
                        {
                            Matchnet.Member.ServiceAdapters.Member member = null;
                            member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                            if (member != null)
                            {
                                // TT#15006, Somehow we were getting back a valid member that contained a null Username
                                // adding this check keeps the null reference from occuring when doing member.GetUserName(_g.Brand).ToLower()
                                //if (member.GetUserName(_g.Brand) != null && member.GetUserName(_g.Brand).ToLower() == userName.ToLower())
                                if (member.GetUserName(_g.Brand) != null)
                                {
                                    bool isMemberOfCurrentCommunity = false;
                                    int[] communityIDList = member.GetCommunityIDList();

                                    // communityIDList is the only other possiblity for a null reference exception besides member.Usernamne
                                    // this checks it just in case.
                                    if (communityIDList != null)
                                    {
                                        foreach (int communityID in communityIDList)
                                        {
                                            if (communityID == g.Brand.Site.Community.CommunityID)
                                            {
                                                isMemberOfCurrentCommunity = true;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // Add the notification message and then throw a matchnet exception so we can log in sparkmon which
                                        // MemberID returned a null communityIDList
                                        g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                                        //  throw new MatchnetException("GetCommunityIDList() returned NULL for MemberID: " + memberID, "LookupByUsername.btnLookupByUserName_Click");
                                    }
                                    if (isMemberOfCurrentCommunity)
                                    {
                                        // Check if the member is from the current site.
                                        bool isMemberOfCurrentSite = false;
                                        int[] siteIDList = member.GetSiteIDList();
                                        if (siteIDList != null)
                                        {
                                            foreach (int siteID in siteIDList)
                                            {
                                                if (siteID == g.Brand.Site.SiteID)
                                                {
                                                    isMemberOfCurrentSite = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (isMemberOfCurrentSite)
                                        {
                                            //Tranfer to one one page
                                            g.Transfer("/Applications/CompatibilityMeter/OneOnOne.aspx?fromlookup=1&MemberID=" + memberID.ToString());
                                        }
                                        else
                                        {
                                            // member isn't a part of the current site
                                            g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                                        }
                                    }
                                    else
                                    {	// member isn't part of the current community
                                        g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                                    }
                                }
                                else
                                {
                                    // Add the notification message and then throw a matchnet exception so we can log in sparkmon which
                                    // MemberID was returning a member with a null username.
                                    g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                                    if (member.GetUserName(_g.Brand) == null)
                                    {
                                        // throw new MatchnetException("Username was NULL in the member object returned for MemberID: " + memberID, "LookupByUsername.btnLookupByUserName_Click");
                                    }
                                }
                            }
                            else
                            {	// member was null or username didn't match search string
                                g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                            }
                        }
                        else
                        {	// member id was null or zero, username doesn't exist
                            g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                        }
                    }
                    else
                    { // Member tries to check 1 on 1 with himself.
                        g.Notification.AddMessage("CANNOT_CHECK_ONE_ON_ONE_WITH_YOURSELF");
                    }

                }
                else
                {	// bad search string.  
                    g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.btnLookupByUserName.Click += new EventHandler(this.btnLookupByUserName_Click);

        }
        #endregion
    }
}
