﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Matchnet.Lib;
using Matchnet.Lib.Exceptions;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.ExternalMail.ServiceAdapters;

using System.Text.RegularExpressions;


namespace Matchnet.Web.Applications.CompatibilityMeter.Controls
{
    public partial class SendToAFriend : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtEMail.Text = g.GetResource("TXT_ENTER_EMAIL", this);
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate email
                if (!string.IsNullOrEmpty(txtEMail.Text))
                {
                    Match emailMatch = System.Text.RegularExpressions.Regex.Match(txtEMail.Text, @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                    if (emailMatch.Success)
                    {
                        // Send mail using ExternalMailSA
                        ExternalMailSA.Instance.SendMatchMeterInvitationMail(g.Brand.BrandID, g.Brand.Site.SiteID, g.Member.MemberID, g.Member.GetUserName(_g.Brand), g.Member.EmailAddress, txtEMail.Text);
                        g.Notification.AddMessage("SUCCESSFULLY_SENT");
                        txtEMail.Text = g.GetResource("TXT_ENTER_EMAIL", this);
                    }
                    else
                    {
                        g.Notification.AddMessage("ERR_YOU_HAVE_PROVIDED_AN_INVALID_EMAIL_ADDRESS");
                    }
                }
                else
                {
                    g.Notification.AddMessage("ERR_NEW_EMAIL_ADDRESS_IS_REQUIRED");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}