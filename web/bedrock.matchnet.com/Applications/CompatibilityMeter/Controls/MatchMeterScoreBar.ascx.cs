﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Applications.CompatibilityMeter.Controls
{
    public partial class MatchMeterScoreBar : FrameworkControl
    {
        private const string EMPTY_HEART_FILE_NAME = "heart-empty.png";
        private const string HALF_HEART_FILE_NAME = "heart-half.png";
        private const string FULL_HEART_FILE_NAME = "heart-full.png";
        private const double SCORE_INTERVAL = 24.0 / 15.0;

        private decimal _MatchScore; // a value from Zoozamen from 1 to 7 with 0.25 intervals
        public decimal MatchScore
        {
            set { _MatchScore = value; }
        }
        public bool IsMemberHighlighted { get; set; }
        public double MatchSteps { get; set; }
        public bool DisplayHearts { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // by default, all hearts will be empty
                if (DisplayHearts)
                {
                    double scoreCalc = ((double)_MatchScore - 1) * 6;
                    MatchSteps = 0;
                    for (int i = 0; i <= 6; i++)
                    {
                        Control ctlImg = FindControl("imgScore" + (i + 1).ToString());
                        if (ctlImg != null && ctlImg is Matchnet.Web.Framework.Image)
                        {
                            if (scoreCalc >= ((i * (SCORE_INTERVAL * 2)) + SCORE_INTERVAL))
                            {
                                ((Matchnet.Web.Framework.Image)ctlImg).FileName = HALF_HEART_FILE_NAME;
                                MatchSteps += 0.5;
                            }
                            else
                            {
                                break; // Empty heart - no point in calculating the next hearts
                            }
                            if (scoreCalc >= ((i * (SCORE_INTERVAL * 2)) + (SCORE_INTERVAL * 2)))
                            {
                                ((Matchnet.Web.Framework.Image)ctlImg).FileName = FULL_HEART_FILE_NAME;
                                MatchSteps += 0.5;
                            }
                            else
                            {
                                break; // Half heart - no point in calculating the next hearts
                            }
                        }
                    }
                }
                else
                {
                    imgScore1.Visible=false;
                    imgScore2.Visible = false;
                    imgScore3.Visible = false;
                    imgScore4.Visible = false;
                    imgScore5.Visible = false;
                    imgScore6.Visible = false; 
                    imgScore7.Visible = false;
                }
                if (IsMemberHighlighted)
                {
                    imgJmeter.FileName = "jmeter_logo_galleryView_y.gif";
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}