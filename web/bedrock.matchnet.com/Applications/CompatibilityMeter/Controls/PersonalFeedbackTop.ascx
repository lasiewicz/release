﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonalFeedbackTop.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.PersonalFeedbackTop" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="MatchMeterScoreBar" Src="~/Applications/CompatibilityMeter/Controls/MatchMeterScoreBar.ascx" %>
 <div class="jmeter-1on1-box pf">
    <div class="jmeter-1on1-box-left">
    </div>
    <div class="jmeter-1on1-box-main">
        <asp:HyperLink runat="server" ID="lnkViewProfileMember1">
            <div class="jmeter-1on1-box-profile">
                <asp:Image runat="server" ID="imgMember1Thumb" />
            
            </div>
        </asp:HyperLink>
        <div class="jmeter-pf-small-mads">
            <asp:Repeater ID="rptrCategories" runat="server">
                <ItemTemplate>
                    <div>
                        <div class="jmeter-mad-title">
                            <%# ((Matchnet.MatchTest.ValueObjects.MatchMeter.PersoanlFeedbackCatergoryItem)Container.DataItem).CategoryName%>
                        </div>
                        <div class="jmeter-mad small">
                            <div class="mad-right">
                            </div>
                            <div class="mad-main">
                            </div>
                            <div class="mad-left">
                            </div>
                            <input type="hidden" value="<%# ((Matchnet.MatchTest.ValueObjects.MatchMeter.PersoanlFeedbackCatergoryItem)Container.DataItem).Score%>" />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="jmeter-clock personal">
            <mn:Image class="title" ID="imgClockMatch" runat="server" FileName="clock/ready.png" />
            <mn:Image class="bar" ID="imgClockBar" runat="server" FileName="clock/5.png" />
            <mn:Image class="pan" ID="imgClockPan" runat="server" FileName="clock/pan.png" />
            <div class="loader">
                <mn:Image ID="clockLoader" runat="server" FileName="ajax-loader.gif" />
            </div>
            <asp:HiddenField runat="server" ID="hfOneMatchScore" />
        </div>
      
    </div>
    <div class="jmeter-1on1-box-right">
    </div>
</div>
<div class="jmeter-1on1-textbox">
    <div class="title">
        <mn:Txt runat="server" ID="txt1" ResourceConstant="PERSONAL_READY_TITLE" />
       
       <%-- <div class="jmeterMatchScore">
            <uc1:MatchMeterScoreBar ID="MatchMeterScoreBar" runat="server" DisplayHearts="true" />
        </div>--%>
    </div>
    <div class="text">
        
        <asp:Literal runat="server" ID="litOOODescription"></asp:Literal>
    </div>
</div>
<div class="jmeterMatchScore">

</div>
