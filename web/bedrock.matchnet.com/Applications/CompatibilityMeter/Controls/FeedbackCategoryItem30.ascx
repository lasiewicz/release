﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeedbackCategoryItem30.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.FeedbackCategoryItem30" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div class="jmeter-review-category">
    <div class="review-title">
        <div class="jmeter-mad">
            <div class="mad-left">
            </div>
            <div class="mad-main">
            </div>
            <div class="mad-right">
            </div>
            <div class="mad-text less">
                <mn:Txt runat="server" ID="txtJmeterGraphLess" ResourceConstant="JMTR_GRAPH_LESS" />
            </div>
            <div class="mad-text more">
                <mn:Txt runat="server" ID="txtJmeterGraphLot" ResourceConstant="JMTR_GRAPH_LOT" />
            </div>
            <input type="hidden" value="<%=FBCItem.Score%>" />
        </div>
        <div class="jmeter-category-title">
            <div class="category-name">
                <%=FBCItem.CategoryName%></div>
            <div class="category-button">
                <span class="cat-open">
                    <mn:Txt runat="server" ID="txtJmeterGraphMore" ResourceConstant="JMTR_GRAPH_MORE" />
                </span><span class="cat-close">
                    <mn:Txt runat="server" ID="txtJmeterGraphClose" ResourceConstant="JMTR_GRAPH_CLOSE" />
                </span>
            </div>
        </div>
    </div>
    <div class="review-details">
        <div class="details-left">
            <span class="cat rotate">
                <%=FBCItem.GraphYTitle%></span> <span class="less rotate">
                    <%=FBCItem.GraphYLow%></span> <span class="more rotate">
                        <%=FBCItem.GraphYHigh%></span> <span class="cat">
                            <%=FBCItem.GraphXTitle%></span> <span class="less">
                                <%=FBCItem.GraphXLow%></span> <span class="more">
                                    <%=FBCItem.GraphXRight%></span>
            <div class="details-boxes">
                <div>
                </div>
                <div>
                </div>
                <div>
                </div>
                <div>
                </div>
                <!--<img class="dot-user" src="/img/site/jdate-co-il/graph-dot-red.png" />
				<img class="dot-match" src="/img/site/jdate-co-il/graph-dot-blue.png" />-->
                <mn:Image class="dot-user" runat="server" ID="imgGraphDotRed" FileName="graph-dot-red.png" />
                <input type="hidden" class="user-score" value="<%=FBCItem.GraphUserScore%>" />
            </div>
        </div>
        <div class="details-text">
            <p><%=FBCItem.Comment%></p>
            <p><%=FBCItem.GraphDescription%></p>
        </div>
    </div>
</div>
<div style="display: none;">
    <%=FBCItem.LeftHeader%>
    <%=FBCItem.RightHeader%>
    <%=FBCItem.ShowRuler%>
    <%=FBCItem.RulerLeftText%>
    <%=FBCItem.RulerRightText%>
    <%=FBCItem.ShowGraph%>
</div>
<%--<div class="jmeterComplete">
    <h2>
        <asp:Label runat="server" ID="lblCategoryName"></asp:Label></h2>
    <div class="jmeterCompleteBar">
        <mn:Image runat="server" ID="imgSlider" FileName="jmeter_indicateBar.png" />
    </div>
    <div class="textLeft">
        <asp:Label runat="server" ID="lblLeftHeader"></asp:Label></div>
    <div class="textRight">
        <asp:Label runat="server" ID="lblRightHeader"></asp:Label></div>
    <p>
        <asp:Label runat="server" ID="lblComment"></asp:Label></p>
</div>
--%>
