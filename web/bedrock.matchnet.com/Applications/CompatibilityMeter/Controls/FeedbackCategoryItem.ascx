﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeedbackCategoryItem.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.FeedbackCategoryItem" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div class="jmeterComplete">
    <h2>
        <asp:Label runat="server" ID="lblCategoryName"></asp:Label></h2>
    <div class="jmeterCompleteBar">
        <mn:Image runat="server" ID="imgSlider" FileName="jmeter_indicateBar.png" />
    </div>
    <div class="textLeft">
        <asp:Label runat="server" ID="lblLeftHeader"></asp:Label></div>
    <div class="textRight">
        <asp:Label runat="server" ID="lblRightHeader"></asp:Label></div>
    <p>
        <asp:Label runat="server" ID="lblComment"></asp:Label></p>
</div>
