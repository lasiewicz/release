﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.MatchTest.ValueObjects.MatchMeter;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.CompatibilityMeter.Controls
{
    public partial class PersonalFeedbackTop : FrameworkControl
    {
        public PersonalFeedback PFB { get; set; }
        public bool ViewProfileMode { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (PFB != null)
            {
                hfOneMatchScore.Value = PFB.LevelOfMaturity.ToString();


                Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(_g.Member, _g.Member, _g.Brand);
                string thumbPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(_g.Member, _g.Member, _g.Brand, photo,
                                                                        PhotoType.Thumbnail,
                                                                        PrivatePhotoImageType.Thumb,
                                                                        NoPhotoImageType.Thumb);

                imgMember1Thumb.ImageUrl = thumbPath;
                //MatchMeterScoreBar.MatchScore = PFB.LevelOfMaturity;
                litOOODescription.Text = PFB.LevelOfMaturityTEXT;

                rptrCategories.DataSource = PFB.FeedbackCategories;
                rptrCategories.DataBind();

                if (ViewProfileMode)
                {
                    imgClockMatch.FileName = "clock/small/ready.png";
                    imgClockBar.FileName = "clock/small/5.png";
                    imgClockPan.FileName = "clock/small/pan.png";
                }
            }
        }
    }
}