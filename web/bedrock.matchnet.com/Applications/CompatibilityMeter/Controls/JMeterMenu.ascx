﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JMeterMenu.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.JMeterMenu" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div class="jmeter-nav <%=GetClassNoMatches() %>">
    <ul>
        <asp:PlaceHolder runat="server" ID="plcLIPersonalityTest">
            <li <%=GetClassActive("Welcome") %>><a href="/Applications/CompatibilityMeter/Welcome.aspx">
                <mn:Txt runat="server" ID="txtJmeterMenuMain" ResourceConstant="JMTR_MENU_MAIN" />
            </a></li>
            <li <%=GetClassActive("PersonalityTest") %>><a href="/Applications/CompatibilityMeter/PersonalityTest.aspx">
                <mn:Txt runat="server" ID="txtJmeterMenuTest" ResourceConstant="JMTR_MENU_TEST" />
            </a></li>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcLICompletion">
            <li <%=GetClassActive("ReturningUser") %>><a href="/Applications/CompatibilityMeter/ReturningUser.aspx">
                <mn:Txt runat="server" ID="txtJmeterMenuMain2" ResourceConstant="JMTR_MENU_MAIN" />
            </a></li>
            <li <%=GetClassActive("Completion") %>><a href="/Applications/CompatibilityMeter/Completion.aspx">
                <mn:Txt runat="server" ID="txtJmeterMenuFeedback" ResourceConstant="JMTR_MENU_FEEDBACK" />
            </a></li>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcLIMatches">
            <li <%=GetClassActive("Matches") %>><a href="/Applications/CompatibilityMeter/Matches.aspx?fromJMeterMenu=1">
                <mn:Txt runat="server" ID="txtJmeterMenuMatches" ResourceConstant="JMTR_MENU_MATCHES" />
            </a></li>
        </asp:PlaceHolder>
        <li <%=GetClassActive("OneOnOne") %>><a href="/Applications/CompatibilityMeter/Matches.aspx?RedirectToOneOnOne=1">
            <mn:Txt runat="server" ID="txtJmeterMenu1on1" ResourceConstant="JMTR_MENU_1ON1" />
        </a></li>
    </ul>
</div>
