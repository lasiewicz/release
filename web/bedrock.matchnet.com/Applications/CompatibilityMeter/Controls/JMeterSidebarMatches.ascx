﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JMeterSidebarMatches.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.JMeterSidebarMatches" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div class="jmeter-left-box matches">
    <div class="jmeter-box-matches-title box-style">
        <mn:Txt runat="server" ID="txtJmeterMatchesTitle" ResourceConstant="JMTR_MATCHES_TTL" />
    </div>
    <div class="jmeter-box-matches box-style">
        <div class="jmeter-box-arrow box-arrow-right">
        </div>
        <div class="jmeter-box-arrow box-arrow-left">
        </div>
        <div class="jmeter-box-matches-profiles">
            <div class="jmeter-box-matches-profiles-run">
            </div>
            <div class="jmeter-box-error">
                <mn:Txt runat="server" ID="txtJmeterNoMatches" ResourceConstant="JMTR_NO_MATCHES" />
            </div>
            <div class="jmeter-box-matches-loader">
            </div>
        </div>
        <div class="jmeter-box-matches-more">
            <asp:HyperLink runat="server" ID="lnkJMeterMatches" NavigateUrl="/Applications/CompatibilityMeter/Matches.aspx">
                <mn:Txt runat="server" ID="txtJmeterMoreMatches" ResourceConstant="JMTR_MORE_MATCHES" />
            </asp:HyperLink>
        </div>
    </div>
    <div class="jmeter-box-arrow-button" id="matches-right">
    </div>
    <div class="jmeter-box-arrow-button" id="matches-left">
    </div>
</div>
