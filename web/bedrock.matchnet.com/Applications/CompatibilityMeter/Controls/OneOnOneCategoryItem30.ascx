﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OneOnOneCategoryItem30.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.OneOnOneCategoryItem30" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%--<div class="jmeterComplete">
    <h2>
        <asp:Label runat="server" ID="lblCategoryName"></asp:Label></h2>
    <div class="jmeterCompleteBar">
        <mn:Image runat="server" ID="imgSlider" FileName="jmeter_indicateBar.png" />
    </div>
    <div class="textLeft">
        <asp:Label runat="server" ID="lblLeftHeader"></asp:Label></div>
    <div class="textRight">
        <asp:Label runat="server" ID="lblRightHeader"></asp:Label></div>
    <p>
        <asp:Label runat="server" ID="lblComment"></asp:Label></p>
    <div class="alignRTL">
        <a href="#top">
            <mn:Image runat="server" ID="imgTop" alt="Back to top" FileName="btn_top.gif" Border="0" /></a>
    </div>
</div>--%>
<div class="jmeter-review-category">
	<div class="review-title">
		<div class="jmeter-mad">
			<div class="mad-left"></div>
			<div class="mad-main"></div>
			<div class="mad-right"></div>
			<div class="mad-text less"><mn:Txt runat="server" ID="txtJmeterGraphLess" ResourceConstant="JMTR_GRAPH_LESS" /></div>
			<div class="mad-text more"><mn:Txt runat="server" ID="txtJmeterGraphLot" ResourceConstant="JMTR_GRAPH_LOT" /></div>
            <input type="hidden" value="<%=OOOCItem.Score%>" />
		</div>
		<div class="jmeter-category-title">
			<div class="category-name"><%=OOOCItem.CategoryName %></div>
			<div class="category-button">
                <span class="cat-open"><mn:Txt runat="server" ID="txtJmeterGraphMore" ResourceConstant="JMTR_GRAPH_MORE" /></span>
                <span class="cat-close"><mn:Txt runat="server" ID="txtJmeterGraphClose" ResourceConstant="JMTR_GRAPH_CLOSE" /></span>
            </div>
		</div>
	</div>
	<div class="review-details">
		<div class="details-left">
			<span class="cat rotate"><%=OOOCItem.GraphYTitle %></span>
			<span class="less rotate"><%=OOOCItem.GraphYLow %></span>
			<span class="more rotate"><%=OOOCItem.GraphYHigh %></span>
			<span class="cat"><%=OOOCItem.GraphXTitle %></span>
			<span class="less"><%=OOOCItem.GraphXLow %></span>
			<span class="more"><%=OOOCItem.GraphXRight %></span>
			<div class="details-boxes">
				<div></div>
				<div></div>
				<div></div>
				<div></div>
				<!--<img class="dot-user" src="/img/site/jdate-co-il/graph-dot-red.png" />
				<img class="dot-match" src="/img/site/jdate-co-il/graph-dot-blue.png" />-->
                <mn:Image class="dot-user" runat="server" ID="imgGraphDotRed" FileName="graph-dot-red.png" />
                <%--<mn:Image class="dot-match" runat="server" ID="imgGraphDotBlue" FileName="graph-dot-blue.png" />--%>
                <input type="hidden" class="user-score" value="<%=OOOCItem.GraphUserScore %>" />
                <%--<input type="hidden" class="match-score" value="<%=OOOCItem.GraphMatchToScore %>" />--%>
			</div>
		</div>
		<div class="details-text">
			<p><%=OOOCItem.Comment %></p>
            <p><%=OOOCItem.GraphDescription %></p>
		</div>
	</div>
</div>

<div style="display:none;">
<%=OOOCItem.ShowGraph %>
</div>

