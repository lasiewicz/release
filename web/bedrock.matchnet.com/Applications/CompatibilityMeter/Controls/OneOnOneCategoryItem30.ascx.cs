﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.MatchTest.ValueObjects.MatchMeter;

namespace Matchnet.Web.Applications.CompatibilityMeter.Controls
{
    public partial class OneOnOneCategoryItem30 : FrameworkControl
    {
        private Matchnet.MatchTest.ValueObjects.MatchMeter.OneOnOneCategoryItem _OOOCItem;
        public Matchnet.MatchTest.ValueObjects.MatchMeter.OneOnOneCategoryItem OOOCItem
        {
            get { return _OOOCItem; }
            set { _OOOCItem = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //lblCategoryName.Text = OOOCItem.CategoryName;
            //lblLeftHeader.Text = OOOCItem.LeftHeader;
            //lblRightHeader.Text = OOOCItem.RightHeader;
            //lblComment.Text = OOOCItem.Comment;
            //int pad = (int)Math.Ceiling((double)(7 - OOOCItem.Score) / 6 * 345);
            //imgSlider.Style.Add("right", pad.ToString() + "px");
        }
    }
}