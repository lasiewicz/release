﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OneOnOneTop.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.OneOnOneTop" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="MatchMeterScoreBar" Src="~/Applications/CompatibilityMeter/Controls/MatchMeterScoreBar.ascx" %>
<%@ Register TagPrefix="uc5" TagName="OmnidateInvitationButton" Src="~/Applications/Omnidate/Controls/OmnidateInvitationButton.ascx" %>
<div class="jmeter-1on1-box">
    <div class="jmeter-1on1-box-left">
    </div>
    <div class="jmeter-1on1-box-main">
        <asp:HyperLink runat="server" ID="lnkViewProfileMember1">
            <div class="jmeter-1on1-box-profile">
                <asp:Image runat="server" ID="imgMember1Thumb" />
                <div class="title">
                    <asp:Literal runat="server" ID="litMember1Age"></asp:Literal>
                    |
                    <asp:Literal runat="server" ID="litMember1City"></asp:Literal>
                </div>
            </div>
        </asp:HyperLink>
        <asp:HyperLink runat="server" ID="lnkClock">
            <div class="jmeter-clock">
                <mn:Image class="title" ID="clockMatch" runat="server" FileName="clock/match.png" />
                <mn:Image class="bar" ID="clockBar" runat="server" FileName="clock/5.png" />
                <mn:Image class="pan" ID="clockPan" runat="server" FileName="clock/pan.png" />
                <div class="loader">
                    <mn:Image ID="clockLoader" runat="server" FileName="ajax-loader.gif" />
                </div>
                <asp:HiddenField runat="server" ID="hfOneMatchScore" />
            </div>
        </asp:HyperLink>
        <asp:HyperLink runat="server" ID="lnkViewProfileMember2">
            <div class="jmeter-1on1-box-profile">
                <asp:Image runat="server" ID="imgMember2Thumb" />
                <div class="title">
                    <asp:Literal runat="server" ID="litMember2Age"></asp:Literal>
                    |
                    <asp:Literal runat="server" ID="litMember2City"></asp:Literal>
                </div>
            </div>
        </asp:HyperLink>
        <div class="jmeter-1on1-box-buttons">
            <div class="jmeter-1on1-send-message">
                <asp:HyperLink runat="server" ID="lnkSendAMessage">
                    <mn:Image ID="oneOnOneSendMessage" runat="server" FileName="jmeter-1on1-send-message.png" />
                </asp:HyperLink>
            </div>
            <div class="jmeter-1on1-indicator">
                <mn:Image ID="imgOnline" runat="server" FileName="icon_chat.gif" ResourceConstant="ONLINE"
                    CssClass="jmeterOneOnOneIconOnline" />
                <uc5:OmnidateInvitationButton runat="server" ID="btnOmnidate" DisplayContext="MiniProfile" />
            </div>
        </div>
    </div>
    <div class="jmeter-1on1-box-right">
    </div>
</div>
<div class="jmeter-1on1-textbox">
    <div class="title">
        <mn:Txt runat="server" ID="txt1" ResourceConstant="1ON1_READY_TITLE" />
        <%--    <div class="jmeterMatchScore">
            <uc1:MatchMeterScoreBar ID="MatchMeterScoreBar" runat="server" DisplayHearts="true" />
        </div>--%>
    </div>
    <div class="text">
        <asp:Literal runat="server" ID="litOOODescription"></asp:Literal>
    </div>
</div>
