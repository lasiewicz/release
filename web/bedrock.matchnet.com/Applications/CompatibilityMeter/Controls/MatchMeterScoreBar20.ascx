﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchMeterScoreBar20.ascx.cs" Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.MatchMeterScoreBar20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<mn:Image runat="server" ID="imgScore5" FileName="jmeter_indicator_empty.gif" Style="vertical-align: top;" />
<mn:Image runat="server" ID="imgScore4" FileName="jmeter_indicator_empty.gif" Style="vertical-align: top;" />
<mn:Image runat="server" ID="imgScore3" FileName="jmeter_indicator_empty.gif" Style="vertical-align: top;" />
<mn:Image runat="server" ID="imgScore2" FileName="jmeter_indicator_empty.gif" Style="vertical-align: top;" />
<mn:Image runat="server" ID="imgScore1" FileName="jmeter_indicator_empty.gif" Style="vertical-align: top;" />
<mn:Image runat="server" ID="imgJmeter" FileName="jmeter_logo_galleryView_b.gif" Style="vertical-align: top;" />
