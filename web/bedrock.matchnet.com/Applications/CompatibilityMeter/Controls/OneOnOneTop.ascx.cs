﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.MatchTest.ValueObjects.MatchMeter;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Applications.CompatibilityMeter.Controls
{
    public partial class OneOnOneTop : FrameworkControl
    {
        public MatchTest.ValueObjects.MatchMeter.OneOnOne OOOFB { get; set; }
        public IMemberDTO ViewdMember { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (OOOFB != null && ViewdMember != null)
            {
                //member1
                Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(_g.Member, _g.Member, _g.Brand);
                string thumbPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(_g.Member, _g.Member, _g.Brand, photo,
                                                                        PhotoType.Thumbnail,
                                                                        PrivatePhotoImageType.Thumb,
                                                                        NoPhotoImageType.Thumb);

                imgMember1Thumb.ImageUrl = thumbPath;
                DateTime birthDate = _g.Member.GetAttributeDate(_g.Brand, "Birthdate");
                //                   litMember1City.Text = ProfileDisplayHelper.GetRegionDisplay(_g.Member, _g);
                litMember1City.Text = FrameworkGlobals.GetCityString(
                    _g.Member.GetAttributeInt(g.Brand, "regionid"), g.Brand.Site.LanguageID);
                litMember1Age.Text = FrameworkGlobals.GetAge(birthDate).ToString();
                lnkViewProfileMember1.NavigateUrl = "/Applications/MemberProfile/ViewProfile.aspx?MemberID=" +
                                                    _g.Member.MemberID;

                //member2

                photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(_g.Member, ViewdMember, _g.Brand);
                thumbPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(_g.Member, ViewdMember, _g.Brand, photo,
                                                                        PhotoType.Thumbnail,
                                                                        PrivatePhotoImageType.Thumb,
                                                                        NoPhotoImageType.Thumb);

                imgMember2Thumb.ImageUrl = thumbPath;
                birthDate = ViewdMember.GetAttributeDate(_g.Brand, "Birthdate");
                litMember2City.Text = FrameworkGlobals.GetCityString(ViewdMember.GetAttributeInt(g.Brand, "regionid"), g.Brand.Site.LanguageID);
                litMember2Age.Text = FrameworkGlobals.GetAge(birthDate).ToString();

                lnkViewProfileMember2.NavigateUrl = "/Applications/MemberProfile/ViewProfile.aspx?MemberID=" +
                                                   ViewdMember.MemberID;

                if (OOOFB.MatchStatus != OneOnOneStatus.RequestUserNotCompleted && OOOFB.MatchStatus != OneOnOneStatus.RequestUserNotTaken)
                {
                    // Check if the targat member finished the test
                    if (OOOFB.MatchStatus != OneOnOneStatus.MatchMemberNotTaken && OOOFB.MatchStatus != OneOnOneStatus.MatchMemberNotCompleted)
                    {
                        hfOneMatchScore.Value = OOOFB.OneMatchScore.ToString();
                        //   ooofb.RelativeMatchScore

                        lnkSendAMessage.NavigateUrl =
                            FrameworkGlobals.LinkHref(
                                string.Format("/Applications/Email/Compose.aspx?MemberID={0}&LinkParent={1}",
                                              ViewdMember.MemberID.ToString(), (int)LinkParent.MatchMeterOneOnOne), false);
                        OnlineLinkHelper.SetOnlineLink(ResultContextType.MatchMeterMatches, ViewdMember, g.Member,
                                                       imgOnline, LinkParent.MatchMeterOneOnOne);

                        //MatchMeterScoreBar.MatchScore = OOOFB.RelativeMatchScore;
                        litOOODescription.Text = OOOFB.Description;
                    }
                }

                btnOmnidate.TargetMember = ViewdMember;
            }
        }
    }
}