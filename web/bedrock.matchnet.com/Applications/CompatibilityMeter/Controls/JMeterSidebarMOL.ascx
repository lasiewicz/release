﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JMeterSidebarMOL.ascx.cs" Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.JMeterSidebarMOL" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div class="jmeter-left-box mol">
    <div class="jmeter-box-matches-title box-style">
        <mn:Txt runat="server" ID="txtJmeterMolTitle" ResourceConstant="JMTR_MOL_TTL" />
    </div>
    <div class="jmeter-box-matches box-style">
        <div class="jmeter-box-arrow box-arrow-right">
        </div>
        <div class="jmeter-box-arrow box-arrow-left">
        </div>
        <div class="jmeter-box-matches-profiles">
            <mn:Txt runat="server" ID="txtJmeterMOL" ResourceConstant="JMTR_MOL" />
            <div class="jmeter-box-matches-profiles-run"></div>
            <div class="jmeter-box-error"><mn:Txt runat="server" ID="txtJmeterNoMatches" ResourceConstant="JMTR_NO_MATCHES" /></div>
            <div class="jmeter-box-matches-loader"></div>
        </div>
    </div>
    <div class="jmeter-box-arrow-button" id="matches-right">
    </div>
    <div class="jmeter-box-arrow-button" id="matches-left">
    </div>
</div>