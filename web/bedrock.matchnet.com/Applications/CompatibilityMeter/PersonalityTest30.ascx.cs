﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;


using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.MatchTest.ValueObjects;
using Matchnet.MatchTest.ValueObjects.MatchMeter;
using Matchnet.Configuration.ServiceAdapters;


namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public partial class PersonalityTest30 : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //var pfb = MatchMeterSA.Instance.GetPersonalFeedback(g.Member.MemberID, g.Brand.Site.LanguageID, MemberPrivilegeAttr.IsCureentSubscribedMember(g));
                MatchTestStatus mts = MatchMeterSA.Instance.GetMatchTestStatus(g.Member.MemberID, MemberPrivilegeAttr.IsCureentSubscribedMember(g), g.Brand);
                if (mts == MatchTestStatus.NotTaken && string.IsNullOrEmpty(Request.QueryString["fromWelcome"]))
                {
                    g.Transfer("/Applications/CompatibilityMeter/Welcome.aspx");
                }
                else if (mts == MatchTestStatus.NotCompleted || (mts == MatchTestStatus.NotTaken&& !string.IsNullOrEmpty(Request.QueryString["fromWelcome"])))
                {
                    try
                    {
                        string httpURL = "http://";
                        string doneURL = "/Applications/CompatibilityMeter/ParentRedirect.aspx?PageType=1";
                        string errorURL = "/Applications/CompatibilityMeter/ParentRedirect.aspx?PageType=2";
                        string HomeURL = "/Applications/Home/Default.aspx";
                        if (!string.IsNullOrEmpty(Request.ServerVariables.Get("HTTP_HOST")))
                        {
                            httpURL += Request.ServerVariables.Get("HTTP_HOST");
                            doneURL = httpURL + doneURL;
                            errorURL = httpURL + errorURL;
                            HomeURL = httpURL + HomeURL;
                        }

                        int memberID = getMemberId();
                        if (memberID > 0)
                        {
                            Member.ServiceAdapters.Member aMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                            memberProfile.Visible = true;
                            bindMiniProfile(aMember);
                            doneURL += "&MemberID=" + memberID.ToString();
                        }
                        string gender = "Female";
                        int genderMask = g.Member.GetAttributeInt(g.Brand, "gendermask");
                        if ((genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE)
                        {
                            gender = "Male";
                        }
                        string userStat = MemberPrivilegeAttr.IsCureentSubscribedMember(g) ? "S" : "M";
                        doneURL = Server.UrlEncode(doneURL);
                        string testURL = string.Format("?UserID={0}&gender={1}&UserStat={2}&DoneURL={3}&ErrorURL={4}", g.Member.MemberID, gender, userStat, doneURL, errorURL);
                        testURL = RuntimeSettings.GetSetting("MATCHTEST_TEST_PAGE_URL", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID) + testURL;
                      // testURL = "http://212.150.44.43/" + testURL;

                        ifrm.Attributes["src"] = testURL;

                        // Add an omniture event for first timers...
                        if (mts == MatchTestStatus.NotTaken)
                        {
                            _g.AnalyticsOmniture.AddEvent("event19");
                        }

                        g.Member.SetAttributeInt(g.Brand, "ZoozamenMemberStatus", (int)Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.NotCompleted);
                        MemberSA.Instance.SaveMember(g.Member);
                    }
                    catch (Exception ex)
                    {
                        g.ProcessException(ex);
                    }
                }
                else
                {
                    g.Transfer("/Applications/CompatibilityMeter/ReturningUser.aspx");
                }
            }
            catch (System.Threading.ThreadAbortException taex) // This was added in order not to show error page when a redirection to ReturningUser should happen
            {
                g.ProcessException(taex);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);

                if (!g.IsDevMode && !(ex is System.Threading.ThreadAbortException))
                {
                    g.Transfer("/Applications/CompatibilityMeter/Error.aspx");
                }
            }



        }
        private int getMemberId()
        {
            try
            {
                if (Request.Params.Get("MemberID") != null)
                {
                    return int.Parse(Request.Params.Get("MemberID"));
                }
            }
            catch (FormatException)
            {
                return 0;
            }
            return 0;
        }
        public void miniProfileDataBind(object sender, RepeaterItemEventArgs args)
        {
            MiniProfile miniProfile = (MiniProfile)args.Item.FindControl("toMemberMiniProfile");

            if (miniProfile != null)
            {
                miniProfile.Transparency = true;
            }
        }

        // binds the single member profile for mini profile control
        private void bindMiniProfile(Member.ServiceAdapters.Member aMember)
        {
            ArrayList toMember = new ArrayList();

            toMember.Add(aMember);
            memberProfile.DataSource = toMember;
            memberProfile.DataBind();
        }
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                _g.AnalyticsOmniture.PageName = "JMeter Personality Test";
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}