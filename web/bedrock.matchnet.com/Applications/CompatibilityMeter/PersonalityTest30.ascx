﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonalityTest30.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.PersonalityTest30" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI.HtmlControls" Assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="../../Framework/Ui/BasicElements/MiniProfile.ascx" %>
<div id="personality-test-container" class="clear-floats">    
    <asp:Repeater runat="server" ID="memberProfile" OnItemDataBound="miniProfileDataBind"
        Visible="False">
        <ItemTemplate>
            <mn:miniProfile runat="server" ID="toMemberMiniProfile" Member="<%# Container.DataItem %>" />
        </ItemTemplate>
    </asp:Repeater>
    <asp:HtmlIframe id="ifrm" runat="server" style="width: 994px; height: 490px; border-style: none;"
        frameborder="0" scrolling="no"></asp:HtmlIframe>
</div>
