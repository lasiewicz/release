﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public partial class BetaPopup : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lnkMatchMeterInfoCloseText.Text = g.GetResource("TXT_CLOSE", this);
            lnkMatchMeterCloseImage.Text = g.GetResource("IMAGE_CLOSE", this);
            lnkMatchMeterInfoCloseText.Attributes.Add("href", "javascript: self.close();");
            lnkMatchMeterCloseImage.Attributes.Add("href", "javascript: self.close();");
        }
    }
}