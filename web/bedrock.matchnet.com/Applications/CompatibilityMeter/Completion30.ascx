﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Completion30.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Completion30" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="../../Framework/Ui/BasicElements/MiniProfile20.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LookupByUsername" Src="Controls/LookupByUsername20.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SendToAFriend" Src="Controls/SendToAFriend20.ascx" %>
<%@ Register TagPrefix="uc1" TagName="FeedbackCategoryItem" Src="Controls/FeedbackCategoryItem30.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PersonalFeedbackTop" Src="~/Applications/CompatibilityMeter/Controls/PersonalFeedbackTop.ascx" %>
<script type="text/javascript">
    function printdiv(printpage) {
        var headstr = "<html><head><title></title></head><body><div style=\"width:400px; text-align:center\">";
        var footstr = "</div></body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<div id="div_print" class="jmeter-profile">
    <div class="one-on-one-titles">
        <mn:Title runat="server" ID="ttlOneOnOne" ResourceConstant="TTL_FEEDBACK" />
        <h2>
            <mn:Txt runat="server" ID="ttlSubOneOnOne" ResourceConstant="TTL_SUB_FEEDBACK" />
        </h2>
    </div>
    <asp:Repeater runat="server" ID="memberProfile" OnItemDataBound="miniProfileDataBind"
        Visible="False">
        <ItemTemplate>
            <mn:miniProfile runat="server" ID="toMemberMiniProfile" Member="<%# Container.DataItem %>"
                DisplayContext="MatchMeterMatches" />
        </ItemTemplate>
    </asp:Repeater>
    
    <uc1:PersonalFeedbackTop runat="server" ID="personalFeedbackTop" />

    <asp:Repeater runat="server" ID="rptFeedbackCategories">
        <ItemTemplate>
            <uc1:FeedbackCategoryItem ID="FeedbackCategoryItem1" runat="server" FBCItem='<%# Container.DataItem%>' />
        </ItemTemplate>
    </asp:Repeater>

</div>
