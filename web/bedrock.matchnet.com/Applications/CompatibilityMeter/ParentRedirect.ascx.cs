﻿using System
;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public partial class ParentRedirect : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string httpURL = "http://";
                string redirectURL = "/Applications/CompatibilityMeter/Error.aspx";

                if (!string.IsNullOrEmpty(Request.ServerVariables.Get("HTTP_HOST")))
                {
                    httpURL += Request.ServerVariables.Get("HTTP_HOST");
                }
                redirectURL = httpURL + redirectURL;

                if (!string.IsNullOrEmpty(Request.QueryString["PageType"]))
                {
                    if (Request.QueryString["PageType"] == "1") // Completion page
                    {
                        redirectURL = httpURL + "/Applications/CompatibilityMeter/Completion.aspx?SendMail=1";
                        if (!string.IsNullOrEmpty(Request.QueryString["MemberID"]))
                        {
                            redirectURL += "&MemberID=" + Request.QueryString["MemberID"];
                        }
                    }
                }

                string js = string.Format("<script>self.parent.location='{0}'</script>", redirectURL);
                Page.RegisterStartupScript("MyScript", js);

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }
    }
}