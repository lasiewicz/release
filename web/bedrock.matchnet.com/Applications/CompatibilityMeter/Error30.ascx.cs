﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Content.ValueObjects.PageConfig;

namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public partial class Error30 : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string lnkMOLURL = FrameworkGlobals.LinkHref("/Applications/MembersOnline/MembersOnline.aspx", true);
                string lnkSearchURL = FrameworkGlobals.LinkHref("/Applications/Search/SearchResults.aspx", true);
                lnkMOL.NavigateUrl = lnkMOLURL;
                lnkSearch.NavigateUrl = lnkSearchURL;

                if (g.LayoutTemplate == LayoutTemplate.Popup || g.LayoutTemplate == LayoutTemplate.PopupProfile)
                {
                    lnkMOL.NavigateUrl = string.Empty;
                    lnkMOL.Attributes.Add("onclick", string.Format("javascript:window.open('{0}', 'matches_window', 'height=664,width=780,status=yes,toolbar=yes,menubar=yes,location=yes, resizable=yes, scrollbars=1');", lnkMOLURL));

                    lnkSearch.NavigateUrl = string.Empty;
                    lnkSearch.Attributes.Add("onclick", string.Format("javascript:window.open('{0}', 'matches_window', 'height=664,width=780,status=yes,toolbar=yes,menubar=yes,location=yes, resizable=yes, scrollbars=1');", lnkSearchURL));
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                _g.AnalyticsOmniture.PageName = "JMeter Error";
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}