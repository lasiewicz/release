﻿#region System References
using System;
using System.Data;
using System.Web.UI.WebControls;
#endregion

#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;
#endregion

#region Matchnet Middle Tier References
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
#endregion

namespace Matchnet.Web.Applications.ContactUs
{
    public partial class ContactUs20 : FrameworkControl
    {
        private bool _videoComment = false;
        
        private void Page_Init(object sender, System.EventArgs e)
        {
      	    try
			{
                if (Request.QueryString["VC"] != null)
                {
                    _videoComment = true;
                    SetVideoCommentConditionalElements();
                }
                
                BindDropDown();
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
		    try
		    {
		        if (Request.QueryString["IPB"] != null)
		        {
		            txtBlockedIP.Visible = true;
		        }

		        if (!IsPostBack)
		        {
		            BuildAndBindPage();
		        }

		        if (g.BreadCrumbTrailHeader != null)
		        {
		            g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("NAV_CONTACT_US", this),
		                g.AppPage.App.DefaultPagePath);

		        }
		        if (g.BreadCrumbTrailFooter != null)
		        {
		            g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("NAV_CONTACT_US", this),
		                g.AppPage.App.DefaultPagePath);
		        }
		        SetUserClientInformationIfMember();

                if (LivePersonManager.Instance.IsLivePersonEnabled(g.Member, g.Brand))
                {
                    txtCSNumber.ResourceConstant = "TXT_CS_LIVE_PERSON";
                }
                
		    }
		    catch (Exception ex)
		    {
		        g.ProcessException(ex);
		    }
		}


        private void SetUserClientInformationIfMember()
        {
            if (g.Member != null && g.Member.MemberID > 0)
            {
                g.Member.SetAttributeText(g.Brand, "UserAgent", Request.UserAgent, TextStatusType.Auto);
                g.Member.SetAttributeDate(g.Brand, "UserCapabilitiesCaptured", DateTime.Now);  
                MemberSA.Instance.SaveMember(g.Member);
            }
        }

        private void SetVideoCommentConditionalElements()
        {
            txtEmailAddress.Visible = false;
            txtYourEmailAddress.Visible = false;
            rfvEmailAddressValidator.Visible = false;
            rfvEmailAddressValidator.Enabled = false;
            revEmailAddressValidator.Visible = false;
            revEmailAddressValidator.Enabled = false;

            txtPleaseSelectFollowing.Visible = false;
            ddlList.Visible = false;
            reasonValidator.Visible = false;
            reasonValidator.Enabled = false;

            pnlContact.Visible = false;
            dtIGiveVideoPermission.Visible = true;
            ddIGiveVideoPermission.Visible = true;

            txtYourCommentsQuestions.ResourceConstant = "TXT_YOUR_COMMENTSQUESTIONS_VIDEO_COMMENT";
        }

		private void BuildAndBindPage()
		{
			if (null != g.Member)
			{
				txtUserName.Text = g.Member.GetUserName(_g.Brand);
				txtEmailAddress.Text = g.Member.EmailAddress;
			}
			else
			{
				txtEmailAddress.Text = g.Session["EmailAddress"];
			}
		}

		private void BindDropDown()
		{	
			DataTable dt = Option.GetOptions("ContactUsList", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID, g);
		
			ddlList.DataSource = dt;
			ddlList.DataTextField = "Content";
			ddlList.DataValueField = "Value";
			ddlList.DataBind();

			ddlList.Items.Insert(0, new ListItem("", ""));

			rfvEmailAddressValidator.ErrorMessage = g.GetResource("EMAIL_REQUIRED", this);
			revEmailAddressValidator.ErrorMessage = g.GetResource("EMAIL_REQUIRED", this);
		}

		public void Go_Click(object sender, EventArgs e)
		{
			try 
			{
				int memberID = 0;
				if(g.Member != null)
				{
					memberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
				}

				Page.Validate();
				if (!Page.IsValid)
				{
					return;
				}

                if (!_videoComment)
                {
                    // 20 = Report a concern. When I have more time, I will search for/create an enum for it.
                    if(ddlList.SelectedItem.Value == "20")
                    {
                        int? memberId = g.Member == null ? (int?)null : g.Member.MemberID;
                        var emailAddress = g.Member == null ? txtEmailAddress.Text.Trim() : g.Member.EmailAddress;

                        Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendReportAbuseEmail(g.Brand.BrandID,
                            ReportMemberManager.Instance.ComposeMessage(emailAddress, g.Brand.Site.Name, txtReportUsername.Text.Trim(), txtComment.Text, GetEnvironmentType(), string.Empty, memberId));
                    }
                    else
                    {
                        ExternalMailSA.Instance.SendContactUs(g.Brand.BrandID,
                                                            txtUserName.Text,
                                                            txtEmailAddress.Text,
                                                            memberID.ToString(),
                                                            Request.ServerVariables["HTTP_USER_AGENT"],
                                                            Request.ServerVariables["REMOTE_ADDR"],
                                                            ddlList.SelectedItem.Text,
                                                            txtComment.Text);    
                    }
                    
                    g.Transfer("/Applications/ContactUs/ContactUsConfirmation.aspx");
                }
                else
                {
                    string subject = g.GetResource("VIDEO_COMMENT_SUBJECT_LINE", this);

                    string comment = txtComment.Text;

                    if (chkIGiveVideoPermission.Checked)
                    {
                        comment = comment + Environment.NewLine + "--------------" + Environment.NewLine + g.GetResource("TXT_I_GIVE_VIDEO_PERMISSION", this);
                    }
                    
                    ExternalMailSA.Instance.SendContactUs(g.Brand.BrandID,
                                        txtUserName.Text,
                                        g.Member.EmailAddress,
                                        memberID.ToString(),
                                        Request.ServerVariables["HTTP_USER_AGENT"],
                                        Request.ServerVariables["REMOTE_ADDR"],
                                        subject,
                                        comment);
                    g.Transfer("/Applications/ContactUs/ContactUsConfirmation.aspx?VC=1");
                }

				

			} 
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

        private string GetEnvironmentType()
        {
            string EnvironmentType = "Production";
            if (g.IsDevMode)
            {
                EnvironmentType = "Dev";
            }

            if (Request.RawUrl.IndexOf("stage") >= 0)
            {
                EnvironmentType = "Stage";
            }

            return EnvironmentType;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{

			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			if ( this.Go != null ) 
			{
				this.Go.Click += new System.EventHandler(this.Go_Click);
			}

			this.Init += new System.EventHandler(this.Page_Init);		
			this.Load += new System.EventHandler(this.Page_Load);	
		}
		#endregion
    }
}
