﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ContactUsConfirmation20.ascx.cs" Inherits="Matchnet.Web.Applications.ContactUs.ContactUsConfirmation20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ArticleContent" Src="~/Applications/Article/Controls/ArticleContent20.ascx" %>

<div id="page-container">
	<mn:Title runat="server" id="ttlContact" ResourceConstant="NAV_CONTACT_US"/>
	<div class="confirmation-message clearfix">
	    <mn:image runat="server" id="imgContact" FileName="icon-email-confirmation.gif" />
		<div class="confirmation-message-content">
		    <mn:txt id="txtConfirmation" resourceconstant="CONTACTUSCONFIRMATION" runat="server" />
		</div>
	</div>
    <uc1:ArticleContent id="ArticleContent1" ArticleID="6314" runat="server"></uc1:ArticleContent>
</div>
