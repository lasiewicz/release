﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MemberLikeAnswerClientModule.ascx.cs" Inherits="Matchnet.Web.Applications.MemberLike.MemberLikeAnswerClientModule" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<% if(IsMemberLikeEnabled && (MemberAnswerId > 0 || MemberLikeId != 0)) { %>
    <script type="text/javascript">
        /* <![CDATA[ */
        $j(document).ready(function() {
            var dialogId="membersWhoLikeYourAnswer<%=LikeType %><%=Ordinal %>";
            var dialogLinkId = "likeOthersLink<%=LikeType %><%=Ordinal %>";
            $j("#"+dialogId).data("paginationResponses",{});
            $j("#"+dialogLinkId).click(function() {                
                $j("#"+dialogLinkId).attr("disabled", "disabled");
                $j.get('/Applications/MemberLike/MemberLikePopup.aspx?eid='+dialogId+'&loid=<%=(MemberLikeId + MemberAnswerId)%>&lotid=<%=LikeObjectTypeID %>&EntryPoint=<%=MyEntryPoint %>', function(response) {
                    $j("#"+dialogId).html(response);
                });

                $j("#"+dialogId).dialog({
                    width: 454,
                    height: 416,
                    modal: true,
                    title: "<%=g.GetResource(ResourceConstant,this) %>",
                    open:function(){
                        Spark_bindFlirtOverlay();
                    },
                    dialogClass: 'ui-dialog-override',
                    close: function(type, data) {
                        $j("#"+dialogLinkId).removeAttr("disabled");
                    }
                });

                spark.tracking.addEvent("event2", true);
                spark.tracking.addPageName("Q&A overlay", true);
                spark.tracking.addProp(10, "<%=CurrentUrl %>", true);
                spark.tracking.track();
            });
        });

        /* ]]> */
    </script>
    <div class="like-your-answer">
        <asp:Literal ID="literalOthersLikeTxt" runat="server" Visible="false"/>
    </div>
    <div id="membersWhoLikeYourAnswer<%=LikeType %><%=Ordinal %>" class="hide">
        <mn:image id="mnimage5012" runat="server" CssClass="loading-gif" filename="ajax-loader.gif" titleresourceconstant="" resourceconstant="" />
    </div>
    <%--end membersWhoLikeYourAnswer--%>        
<% } %>            
