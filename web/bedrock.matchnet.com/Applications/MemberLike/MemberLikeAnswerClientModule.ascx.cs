﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.MemberLike;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.MemberLike
{
    public partial class MemberLikeAnswerClientModule : FrameworkControl
    {
        private Answer _memberAnswer=null;
        private int _memberAnswerId = 0;
        private int _memberLikeId = 0;
        private string _currentUrl=string.Empty;
        private int _Ordinal = 0;
        private string _likeType = "";
        private int _objectTypeId = 0;
        private string _resourceConstant = "";

        public bool IsMemberLikeEnabled
        {
            get
            {
                return MemberLikeHelper.Instance.IsMemberLikeEnabled(g.Brand);
            }

        }

        public bool IsMemberPhotoLikeEnabled
        {
            get
            {
                return MemberLikeHelper.Instance.IsMemberPhotoLikeEnabled(g.Brand);
            }
        }

        public int MemberAnswerId {
            get
            {
                return _memberAnswerId;
            }
        }

        public int MemberLikeId
        {
            get
            {
                return _memberLikeId;
            }
        }

        public string LikeType
        {
            get
            {
                return _likeType;
            }
            set
            {
                _likeType = value;
            }
        }

        public int LikeObjectTypeID
        {
            get
            {
                return _objectTypeId;
            }
            set
            {
                _objectTypeId = value;
            }
        }

        public String ResourceConstant
        {
            get
            {
                return _resourceConstant;
            }
            set
            {
                _resourceConstant = value;
            }
        }

        public BreadCrumbHelper.EntryPoint MyEntryPoint { get; set; }
        public string TrackingParam { get; set; }

        public string CurrentUrl
        {
            get
            {
                return _currentUrl;
            }
        }



        public int Ordinal
        {
            get { return _Ordinal; }
            set { _Ordinal = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadMemberLikeClentModule(Answer memberAnswer) {
            LikeType = "Answer";
            LikeObjectTypeID = Convert.ToInt32(LikeObjectTypes.Answer);

            _memberAnswer = memberAnswer;
            if (IsMemberLikeEnabled)
            {
                MemberLikeParams likeParams=MemberLikeParams.GetParamsObject(g.Member.MemberID, g.Brand.Site.SiteID);
                List<MemberLikeObject> memberLikes = MemberLikeHelper.Instance.GetLikesByObjectIdAndType(likeParams,_memberAnswer.AnswerID, Convert.ToInt32(LikeObjectTypes.Answer));
                if (null != memberLikes)
                {
                    memberLikes = memberLikes.FindAll(delegate(MemberLikeObject likeObject)
                    {
                        Matchnet.Member.ServiceAdapters.Member _Member = MemberSA.Instance.GetMember(likeObject.MemberId, MemberLoadFlags.None);
                        return !MemberLikeHelper.Instance.IsMemberSuppressed(_Member, g);
                    });
                    int likeCount = memberLikes.Count;
                    Literal literalOthersLikeTxt = this.FindControl("literalOthersLikeTxt") as Literal;
                    if (likeCount > 0)
                    {
                        ResourceConstant = "TXT_MEMBERS_WHO_LIKE";
                        _memberAnswerId = _memberAnswer.AnswerID;
                        if (likeCount > 2)
                        {
                            string[] memberLikeProfiles = ProcessMemberLikeProfileText(memberLikes, 1);
                            literalOthersLikeTxt.Text = string.Format(g.GetResource("TXT_1_MEMBER_LIKE_PROFILE", this), memberLikeProfiles[0]) + string.Format(g.GetResource("TXT_OTHERS_LIKE_LINK_PLURAL", this), "likeOthersLink" + LikeType + Ordinal, likeCount - 1) + g.GetResource("TXT_LIKE_YOUR_ANSWER_PLURAL", this);
                            _currentUrl = Request.Url.AbsoluteUri;
                        }
                        else
                        {
                            string[] memberLikeProfiles = ProcessMemberLikeProfileText(memberLikes, memberLikes.Count);
                            string likesYourAnswerText = (memberLikeProfiles.Length == 1) ? "TXT_LIKE_YOUR_ANSWER" : "TXT_LIKE_YOUR_ANSWER_PLURAL";
                            literalOthersLikeTxt.Text = string.Format(g.GetResource("TXT_" + memberLikeProfiles.Length + "_MEMBER_LIKE_PROFILE", this), memberLikeProfiles) + g.GetResource(likesYourAnswerText, this);
                        }
                        literalOthersLikeTxt.Visible = true;
                    }
                    else
                    {
                        literalOthersLikeTxt.Visible = false;
                    }
                }
            }
        }


        public void LoadMemberLikeClientModule(int likeObjectID)
        {
            LikeType = "FreeText" + likeObjectID;
            LikeType = LikeType.Replace('-', '_');

            LikeObjectTypeID = Convert.ToInt32(LikeObjectTypes.ProfileFreeText);

            if (IsMemberLikeEnabled)
            {
                MemberLikeParams likeParams = MemberLikeParams.GetParamsObject(g.Member.MemberID, g.Brand.Site.SiteID);
                List<MemberLikeObject> memberLikes = MemberLikeHelper.Instance.GetLikesByObjectIdAndType(likeParams, likeObjectID, Convert.ToInt32(LikeObjectTypes.ProfileFreeText));
                if (null != memberLikes)
                {
                    memberLikes = memberLikes.FindAll(delegate(MemberLikeObject likeObject)
                    {
                        Matchnet.Member.ServiceAdapters.Member _Member = MemberSA.Instance.GetMember(likeObject.MemberId, MemberLoadFlags.None);
                        return !MemberLikeHelper.Instance.IsMemberSuppressed(_Member, g);
                    });
                    int likeCount = memberLikes.Count;
                    Literal literalOthersLikeTxt = this.FindControl("literalOthersLikeTxt") as Literal;
                    if (likeCount > 0)
                    {
                        ResourceConstant = "TXT_MEMBERS_WHO_LIKE_ESSAY";
                        _memberLikeId = likeObjectID;

                        if (likeCount > 2)
                        {
                            string[] memberLikeProfiles = ProcessMemberLikeProfileText(memberLikes, 1);
                            literalOthersLikeTxt.Text = string.Format(g.GetResource("TXT_1_MEMBER_LIKE_PROFILE", this), memberLikeProfiles[0]) + string.Format(g.GetResource("TXT_OTHERS_LIKE_LINK_PLURAL", this), "likeOthersLink" + LikeType + Ordinal, likeCount - 1) + g.GetResource("TXT_LIKE_YOUR_ANSWER_PLURAL", this);
                            _currentUrl = Request.Url.AbsoluteUri;
                        }
                        else
                        {
                            string[] memberLikeProfiles = ProcessMemberLikeProfileText(memberLikes, memberLikes.Count);
                            string likesYourAnswerText = (memberLikeProfiles.Length == 1) ? "TXT_LIKE_YOUR_ANSWER" : "TXT_LIKE_YOUR_ANSWER_PLURAL";
                            literalOthersLikeTxt.Text = string.Format(g.GetResource("TXT_" + memberLikeProfiles.Length + "_MEMBER_LIKE_PROFILE", this), memberLikeProfiles) + g.GetResource(likesYourAnswerText, this);
                        }
                        literalOthersLikeTxt.Visible = true;
                    }
                    else
                    {
                        literalOthersLikeTxt.Visible = false;
                    }
                }
            }
        }

        public void LoadMemberPhotoLikeClientModule(int likeObjectID)
        {
            LikeType = "Photo" + likeObjectID;
            LikeType = LikeType.Replace('-', '_');

            LikeObjectTypeID = Convert.ToInt32(LikeObjectTypes.Photo);

            if (IsMemberPhotoLikeEnabled)
            {
                MemberLikeParams likeParams = MemberLikeParams.GetParamsObject(g.Member.MemberID, g.Brand.Site.SiteID);
                List<MemberLikeObject> memberLikes = MemberLikeHelper.Instance.GetLikesByObjectIdAndType(likeParams, likeObjectID, Convert.ToInt32(LikeObjectTypes.Photo));
                if (null != memberLikes)
                {
                    memberLikes = memberLikes.FindAll(delegate(MemberLikeObject likeObject)
                    {
                        Matchnet.Member.ServiceAdapters.Member _Member = MemberSA.Instance.GetMember(likeObject.MemberId, MemberLoadFlags.None);
                        return !MemberLikeHelper.Instance.IsMemberSuppressed(_Member, g);
                    });
                    int likeCount = memberLikes.Count;
                    Literal literalOthersLikeTxt = this.FindControl("literalOthersLikeTxt") as Literal;
                    if (likeCount > 0)
                    {
                        ResourceConstant = "TXT_MEMBERS_WHO_LIKE_PHOTO";
                        _memberLikeId = likeObjectID;

                        if (likeCount > 2)
                        {
                            string[] memberLikePhotos = ProcessMemberLikeProfileText(memberLikes, 1);
                            literalOthersLikeTxt.Text = string.Format(g.GetResource("TXT_1_MEMBER_LIKE_PROFILE", this), memberLikePhotos[0]) + string.Format(g.GetResource("TXT_OTHERS_LIKE_LINK_PLURAL", this), "likeOthersLink" + LikeType + Ordinal, likeCount - 1) + g.GetResource("TXT_LIKE_YOUR_PHOTO_PLURAL", this);
                            _currentUrl = Request.Url.AbsoluteUri;
                        }
                        else
                        {
                            string[] memberLikePhotos = ProcessMemberLikeProfileText(memberLikes, memberLikes.Count);
                            string likesYourPhotoText = (memberLikePhotos.Length == 1) ? "TXT_LIKE_YOUR_PHOTO" : "TXT_LIKE_YOUR_PHOTO_PLURAL";
                            literalOthersLikeTxt.Text = string.Format(g.GetResource("TXT_" + memberLikePhotos.Length + "_MEMBER_LIKE_PROFILE", this), memberLikePhotos) + g.GetResource(likesYourPhotoText, this);
                        }
                        literalOthersLikeTxt.Visible = true;
                    }
                    else
                    {
                        literalOthersLikeTxt.Visible = false;
                    }
                }
            }
        }

        private string [] ProcessMemberLikeProfileText(List<MemberLikeObject> memberLikes, int maxLength)
        {
            List<string> profiles = new List<string>();
            for (int i = 0; i < memberLikes.Count; i++)
            {
                MemberLikeObject likeObject = memberLikes[i];
                Matchnet.Member.ServiceAdapters.Member _Member = MemberSA.Instance.GetMember(likeObject.MemberId, MemberLoadFlags.None);
                string _viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, _Member.MemberID, i, null, Constants.NULL_INT, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoTracking);
                if (!String.IsNullOrEmpty(TrackingParam))
                {
                    _viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(_viewProfileUrl, TrackingParam);
                }
                profiles.Add((string.Format(g.GetResource("TXT_PROFILE_LINK", this), _viewProfileUrl, FrameworkGlobals.Ellipsis(_Member.GetUserName(_g.Brand), 12))));                    
                if (profiles.Count == maxLength) break; 
            }
            return profiles.ToArray();
        }
    }
}