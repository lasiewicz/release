namespace Matchnet.Web.Applications.FrameNav
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using Matchnet;

	using Matchnet.Web.Framework;

	/// <summary>
	///		Its job is to only render the left nav on request.
	///		To be used for integrating Mingle's Connect framework.
	///		Their pages will be using an iframe to retrieve our left nav section
	/// </summary>
	public class Left : FrameworkControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (Request["header"] != "1")
			{
				g.HeaderControl.Visible = false;
			}

			if (Request["footer"] != "1")
			{
				g.FooterControl20.Visible = false;
			}

			if (Request["left"] != "1")
			{
				//g.LeftNavControl.Visible = false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
