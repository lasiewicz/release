﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.FrameNav
{
    /// <summary>
    ///		Its job is to only render the right activity center on request.
    ///		To be used for integrating Mingle's Connect framework.
    ///		Their pages will be using an iframe to retrieve our right activity section
    /// </summary>
    public partial class Right : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if (Request["header"] != "1")
            {
                if (g.HeaderControl != null)
                    g.HeaderControl.Visible = false;
            }

            if (Request["footer"] != "1")
            {
                if (g.FooterControl20 != null)
                    g.FooterControl20.Visible = false;
            }

            if (Request["right"] != "1")
            {
                if (g.RightNavControl != null)
                    g.RightNavControl.IsVisible = false;
            }
            else
            {
                if (g.LayoutTemplateBase != null && g.LayoutTemplateBase.ImageLogo != null)
                {
                    g.LayoutTemplateBase.ImageLogo.Visible = false;
                }
            }

            // if we are using the IFrame layout template, the template itself has 0 ad slot so we must add it here
            if (g.LayoutTemplate == Matchnet.Content.ValueObjects.PageConfig.LayoutTemplate.IFrame)
            {
                phRightSquareAd.Visible = true;
            }

            g.LoadingRightOnly = true;
        }
    }
}