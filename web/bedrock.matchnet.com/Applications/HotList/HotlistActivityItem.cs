﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.List.ValueObjects;

namespace Matchnet.Web.Applications.HotList
{
    /// <summary>
    /// Class represents a hotlist item activity to encapsulate details
    /// </summary>
    public class HotlistActivityItem : IComparable
    {
        public HotListCategory HotlistCategory = HotListCategory.Default;
        public DateTime ActionDate = DateTime.MinValue;
        public int MemberID = Constants.NULL_INT;

        #region IComparable Members

        public Int32 CompareTo(object obj)
        {
            if (obj is HotlistActivityItem)
            {
                HotlistActivityItem listDetail = obj as HotlistActivityItem;
                Int32 result = listDetail.ActionDate.CompareTo(this.ActionDate);
                if (result == 0)
                {
                    return listDetail.MemberID.CompareTo(this.MemberID);
                }

                return result;
            }
            else
                throw new Exception("Compare object is not a HotlistActivityItem");
        }

        #endregion
    }
}
