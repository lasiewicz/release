﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="IconList20.ascx.cs"
    Inherits="Matchnet.Web.Applications.HotList.IconList20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="page-container" class="icon-list">
    <mn:Title runat="server" ID="ttlIconList" ResourceConstant="TXT_ICON_LIST" />
    <p class="sub">
        <mn:Txt runat="server" ID="txt4" ResourceConstant="TXT_ICON_LIST_DESCRIPTION" />
    </p>
    <asp:PlaceHolder runat="server" ID="plcYNM">
        <div class="settings-header icon-header">
            <span class="icon-title">
                <mn:Txt runat="server" ID="txtClick" ResourceConstant="TXT_CLICK_MATCH" />
            </span>
            <mn:Txt runat="server" ID="txtClickDescription" ResourceConstant="TXT_CLICK_HEADER_DESCRIPTION" />
        </div>
        <ul class="icon-description-container clearfix">
            <li class="icon-image"><span class="spr s-icon-click-y-on"><span></span></span></li>
            <li class="icon-description">
                <mn:Txt runat="server" ID="txtSecretly" ResourceConstant="TXT_CLICKMATCH_YES_TEXT" />
            </li>
        </ul>
        <ul class="icon-description-container clearfix">
            <li class="icon-image"><span class="spr s-icon-click-n-on"><span></span></span></li>
            <li class="icon-description">
                <mn:Txt runat="server" ID="txtNo" ResourceConstant="TXT_CLICKMATCH_NO_TEXT" />
            </li>
        </ul>
        <ul class="icon-description-container clearfix">
            <li class="icon-image"><span class="spr s-icon-click-m-on"><span></span></span></li>
            <li class="icon-description">
                <mn:Txt runat="server" ID="txtMaybe" ResourceConstant="TXT_CLICKMATCH_MAYBE_TEXT" />
            </li>
        </ul>
        <ul class="icon-description-container clearfix">
            <li class="icon-image"><span class="spr s-icon-click-yy"><span></span></span></li>
            <li class="icon-description">
                <mn:Txt runat="server" ID="txtYY" ResourceConstant="TXT_BOTHYES_TEXT" />
            </li>
        </ul>
    </asp:PlaceHolder>
    <div class="settings-header icon-header">
        <span class="icon-title">
            <mn:Txt runat="server" ID="txtOnlineStatus" ResourceConstant="TXT_ONLINE_STATUS" />
        </span>
        <mn:Txt runat="server" ID="txtOnlineStatusDescription" ResourceConstant="TXT_ONLINE_STATUS_HEADER_DESCRIPTION" />
    </div>
    <ul class="icon-description-container clearfix">
        <li class="icon-image">
            <mn:Image alt="" runat="server" FileName="icon-status-disconnected.gif" ID="Image2" />
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ResourceConstant="TXT_INSTANT_MESSAGE_STATUS_DISCONNECTED"
                ID="Txt5" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-status-hidden"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ResourceConstant="TXT_INSTANT_MESSAGE_STATUS_HIDDEN" ID="Txt6" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-status-offline"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ResourceConstant="TXT_THIS_MEMBER_IS_OFFLINE" ID="Txt7" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image">
            <mn:Image alt="" runat="server" FileName="icon-status-online.gif" ID="Image6" /><%-- no sprite: animated icon --%>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtMemberOnline" ResourceConstant="TXT_THIS_MEMBER_IS_ONLINE" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-IM-mem"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtIMmem" ResourceConstant="TXT_MEMBERS_WHO_INSTANT_MESSAGED_YOU" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-IM-you"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtIMYou" ResourceConstant="TTL_MEMBERS_YOUVE_IMD" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-IM-bth" title=""><span></span>
        </span></li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtYouBthIMd" ResourceConstant="TXT_YOU_BOTH_INSTANT_MESSAGED_EACH_OTHER" />
        </li>
    </ul>
    <asp:PlaceHolder runat="server" ID="plcOmnidate" Visible="false">
        <div class="settings-header icon-header">
            <span class="icon-title">
                <mn:Txt runat="server" ID="txtOmnidate" ResourceConstant="PRO_OMNIDATE" />
            </span>
            <mn:Txt runat="server" ID="txtOmnidateDescription" ResourceConstant="TXT_OMNIDATE_HEADER_DESCRIPTION" />
        </div>
        <ul class="icon-description-container clearfix">
            <li class="icon-image"><span class="spr s-icon-omnidate"><span></span></span></li>
            <li class="icon-description">
                <mn:Txt runat="server" ID="txtInviteMember" ResourceConstant="TXT_INVITE_MEMBER" />
            </li>
        </ul>
        <ul class="icon-description-container clearfix">
            <li class="icon-image"><span class="spr s-icon-omnidate-dating"><span></span></span>
            </li>
            <li class="icon-description">
                <mn:Txt runat="server" ID="txtDating" ResourceConstant="TXT_DATING" />
            </li>
        </ul>
        <ul class="icon-description-container clearfix">
            <li class="icon-image"><span class="spr s-icon-members-omnidate-mem"><span></span></span>
            </li>
            <li class="icon-description">
                <mn:Txt runat="server" ID="txtInvitedYou" ResourceConstant="TXT_INVITED_YOU" />
            </li>
        </ul>
        <ul class="icon-description-container clearfix">
            <li class="icon-image"><span class="spr s-icon-members-omnidate-you"><span></span></span>
            </li>
            <li class="icon-description">
                <mn:Txt runat="server" ID="txtYouveInvited" ResourceConstant="TXT_YOUVE_INVITED" />
            </li>
        </ul>
        <ul class="icon-description-container clearfix">
            <li class="icon-image"><span class="spr s-icon-members-omnidate-bth"><span></span></span>
            </li>
            <li class="icon-description">
                <mn:Txt runat="server" ID="txtBothInvited" ResourceConstant="TXT_BOTH_INVITED" />
            </li>
        </ul>
    </asp:PlaceHolder>
    <div class="settings-header icon-header">
        <span class="icon-title">
            <mn:Txt runat="server" ID="txtYouHotlist" ResourceConstant="PRO_HOT_LIST" />
        </span>
        <mn:Txt runat="server" ID="txt9" ResourceConstant="TXT_HOT_LIST_HEADER_DESCRIPTION" />
    </div>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-hotlist-add"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtAdd" ResourceConstant="TXT_ADD_HOTLIST" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-hotlist-remove"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txt3" ResourceConstant="TXT_HOTLIST_REMOVE" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-hotlisted-mem"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtHotYou" ResourceConstant="TTL_MEMBERS_WHO_HAVE_HOT_LISTED_YOU" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-hotlisted-you"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtHotlistedMem" ResourceConstant="TTL_MEMBERS_YOUVE_HOT_LISTED" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-hotlisted-bth"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtHotBth" ResourceConstant="TXT_YOU_BOTH_HOTLISTED_EACH_OTHER" />
        </li>
    </ul>
    <div class="settings-header icon-header">
        <span class="icon-title">
            <mn:Txt runat="server" ID="txtEmail" ResourceConstant="PRO_EMAIL" />
        </span>
        <mn:Txt runat="server" ID="txtEmailDescription" ResourceConstant="TXT_EMAIL_HEADER_DESCRIPTION" />
    </div>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-emailed-mem"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtEmailedYou" ResourceConstant="TXT_MEMBERS_WHO_EMAILED_YOU" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-emailed-you"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtYouEmailed" ResourceConstant="TXT_MEMBERS_YOU_EMAILED" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-emailed-bth"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtBthEmailed" ResourceConstant="TXT_YOU_BOTH_EMAILED_EACH_OTHER" />
        </li>
    </ul>
    <div class="settings-header icon-header">
        <span class="icon-title">
            <mn:Txt runat="server" ID="txtFlirtHeader" ResourceConstant="PRO_TEASE" />
        </span>
        <mn:Txt runat="server" ID="txt10" ResourceConstant="TXT_TEASE_HEADER_DESCRIPTION" />
    </div>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-flirted-mem"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtTeasedYou" ResourceConstant="TXT_MEMBERS_WHO_TEASED_YOU" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-flirted-you"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtYouTeased" ResourceConstant="TXT_MEMBERS_YOU_TEASED" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-flirted-bth"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtBthTeased" ResourceConstant="TXT_YOU_BOTH_TEASED_EACH_OTHER" />
        </li>
    </ul>
    <div class="settings-header icon-header">
        <span class="icon-title">
            <mn:Txt runat="server" ID="txt11" ResourceConstant="TXT_VIEWED_HEADER" />
        </span>
        <mn:Txt runat="server" ID="txt12" ResourceConstant="TXT_VIEWED_HEADER_DESCRIPTION" />
    </div>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-viewed-mem"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtMembersViewed" ResourceConstant="TXT_MEMBERS_WHO_VIEWED_YOUR_PROFILE" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-viewed-you"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtYouViewed" ResourceConstant="TXT_MEMBERS_WHOSE_PROFILES_YOU_VIEWED" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-members-viewed-bth"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtBothViewed" ResourceConstant="TXT_BOTH_VIEWED_EACHOTHERS_PROFILES" />
        </li>
    </ul>
    <div class="settings-header icon-header">
        <mn:Txt runat="server" ID="txt13" ResourceConstant="TXT_MISCELLANEOUS_HEADER" />
    </div>
    <asp:Panel ID="Panel1" Visible="False" runat="server" EnableViewState="False">
        <ul class="icon-description-container clearfix">
            <li class="icon-image"><span class="spr s-icon-ecard"><span></span></span></li>
            <li class="icon-description">
                <mn:Txt runat="server" ID="txtEcards" ResourceConstant="TXT_ECARDS" />
            </li>
        </ul>
     </asp:Panel>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-send-to-friend"><span></span></span>
        </li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtEmailToFriend" ResourceConstant="TXT_EMAIL_THIS_MEMBERS_PROFILE_TO_A_FRIEND" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix">
        <li class="icon-image"><span class="spr s-icon-new-member"><span></span></span></li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtNewMember" ResourceConstant="TXT_NEW_MEMBER" />
        </li>
    </ul>
    <ul class="icon-description-container clearfix icon-image-updated">
        <li class="icon-image"><span class="spr s-icon-updated"><span></span></span></li>
        <li class="icon-description">
            <mn:Txt runat="server" ID="txtBlocked" ResourceConstant="TXT_UPDATED_DESCRIPTION" />
        </li>
    </ul>
    <%-- Why is this here? --%>
    <asp:Panel ID="pnlEcards" Visible="False" runat="server" EnableViewState="False">
    </asp:Panel>
</div>
<!-- end page-container area -->
