﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotlistProfileStrip.ascx.cs" Inherits="Matchnet.Web.Applications.HotList.HotlistProfileStrip" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register Src="/Framework/Ui/BasicElements/MicroProfileStrip.ascx" TagName="MicroProfileStrip" TagPrefix="uc2" %>

<div class="hotlist-home clearfix micro-profile-strip">
    <div class="component-header clearfix">
        <h2 class="component-header"><mn2:FrameworkLiteral ID="litTitle" runat="server" ></mn2:FrameworkLiteral></h2>
        <div class="view-all-home">
            <div class="pagination-buttons image-text-pair">
            <a href="/Applications/HotList/View.aspx?CategoryID=-1" class="button btn link-secondary small"><span class="next"><mn2:FrameworkLiteral ID="FrameworkLiteral6" runat="server" ResourceConstant="TXT_VIEW_ALL_HOTLISTS"></mn2:FrameworkLiteral></span><span class="direction"><mn:Txt runat="server" ResourceConstant="DIRECTION_ARROW" /></span></a>
            </div>
        </div>
    </div>
    <table class="homepage-hotlists-links" cellspacing="2">
        <tr>
        <asp:Repeater ID="rptHostlistLinks" runat="server">
        <ItemTemplate>
         <td>
            <a href="/Applications/HotList/View.aspx?CategoryID=<%# DataBinder.Eval(Container.DataItem, "Category")%>">
                <mn2:FrameworkLiteral ID="litListCountText" runat="server" ResourceConstant=<%# DataBinder.Eval(Container.DataItem, "ResourceConstant")%>></mn2:FrameworkLiteral>
                &nbsp;<span><asp:Literal runat="server" ID="litListCount" Text=<%# DataBinder.Eval(Container.DataItem, "Count")%> /></span>
            </a>
          </td>
        </ItemTemplate>
        </asp:Repeater>
        </tr>
    </table>
   <uc2:MicroProfileStrip id="microProfileStrip" runat="server" />
</div>