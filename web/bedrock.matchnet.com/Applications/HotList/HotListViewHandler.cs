﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Session.ValueObjects;
using System.Collections;
using System.Collections.Generic;

namespace Matchnet.Web.Applications.HotList
{
    public class HotListViewHandler
    {

        ContextGlobal g;
        IHotListView _hotlistView = null;
        public HotListViewHandler(ContextGlobal context, IHotListView hotlistview)
        {
            g = context;
            _hotlistView = hotlistview;
        }
        public void HandleAction()
        {
            int memberID = Constants.NULL_INT;
            int toMemberID = Constants.NULL_INT;
            int fromMemberID = Constants.NULL_INT;
            string action = HttpContext.Current.Request.QueryString["a"];

            if (action != null && ValidateActionValues(ref memberID, ref toMemberID, ref fromMemberID))
            {

                if (action == "add")
                {
                    DeleteMember(fromMemberID);
                    ListSA.Instance.AddListMember(_hotlistView.ResultHotList.HotListCategory,
                        g.Brand.Site.Community.CommunityID,
                        g.Brand.Site.SiteID,
                        g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                        toMemberID,
                        null,
                        Constants.NULL_INT,
                        !((g.Member.GetAttributeInt(g.Brand, "HideMask", 0) & (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists),
                        false);
                }
                if (action == "delete")
                {
                    ListSA.Instance.RemoveListMember(_hotlistView.ResultHotList.HotListCategory,
                        g.Brand.Site.Community.CommunityID,
                        g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                        toMemberID);

                }

                // Reload the page so that the deleted/added item does not show/shows up in the list.
                g.Transfer("/Applications/Hotlist/View.aspx");
            }
        }

        # region Event Handlers for Page Action

        public void MoveMember(bool Checked, string CheckValue, string NoteValue, string moveType)
        {
            if (Checked)
            {
                int memberID = int.Parse(CheckValue);

                if (NoteValue == g.GetResource("HOT_MAKE_A_NOTE", this))
                {
                    NoteValue = null;
                }

                if (moveType == "delete")
                {
                    DeleteMember(memberID);
                }
                else if (moveType == "block")
                {
                    DeleteMember(memberID);
                    MoveMember(memberID,
                        HotListCategory.IgnoreList,
                        NoteValue);
                }
                else if (moveType == "favorite")
                {
                    DeleteMember(memberID);
                    MoveMember(memberID,
                        HotListCategory.Default,
                        NoteValue);
                }
                else
                {
                    DeleteMember(memberID);
                    MoveMember(memberID,
                        (HotListCategory)Enum.Parse(typeof(HotListCategory), moveType, true),
                        NoteValue);
                }

                // Add 1 to the Move Count.
                _hotlistView.MoveCount++;
            }
        }

        public void MoveMember(int memberID, HotListCategory category, string notes)
        {
            ListSA.Instance.AddListMember(category,
                g.Brand.Site.Community.CommunityID,
                g.Brand.Site.SiteID,
                g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                memberID,
                notes,
                Constants.NULL_INT,
                !((g.Member.GetAttributeInt(g.Brand, "HideMask", 0) & (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists),
                false);
        }

        public void DeleteMember(int fromMemberID)
        {
            ListSA.Instance.RemoveListMember(_hotlistView.ResultHotList.HotListCategory,
                g.Brand.Site.Community.CommunityID,
                g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                fromMemberID);
        }
        #endregion

        public bool ValidateActionValues(ref int memberID, ref int toMemberID, ref int fromMemberID)
        {
            double tempVal;

            if (double.TryParse(HttpContext.Current.Request.QueryString["MemberID"], System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out tempVal))
            {
                memberID = (int)tempVal;
            }

            if (double.TryParse(HttpContext.Current.Request.QueryString["ToMemberID"], System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out tempVal))
            {
                toMemberID = (int)tempVal;
            }

            if (double.TryParse(HttpContext.Current.Request.QueryString["FromMemberID"], System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out tempVal))
            {
                fromMemberID = (int)tempVal;
            }

            if (memberID != Constants.NULL_INT && toMemberID != Constants.NULL_INT && fromMemberID != Constants.NULL_INT)
            {
                return true;
            }

            return false;
        }

        public static HotListDirection GetHotListDirection(HotListCategory category)
        {
            HotListDirection listDirection=HotListDirection.None;
            HotListType listType=HotListType.ViewProfile;
            ListInternal.MapListTypeDirection(category, out listType, out listDirection);

            return listDirection;
        }

        public static bool IsNotificationsEnabled(ContextGlobal _g)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_HOTLIST_NOTIFICATIONS", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID, _g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        /// <summary>
        /// Gets a count of new notifications for a specific hotlist category
        /// </summary>
        /// <param name="category"></param>
        /// <param name="_g"></param>
        /// <returns></returns>
        public static int GetHotlistNewNotificationCount(HotListCategory category, ContextGlobal _g)
        {
             //get new notifications
             int brandid = _g.Brand.BrandID;
             int communityid = _g.Brand.Site.Community.CommunityID;
             int siteid = _g.Brand.Site.SiteID;
             string lastVisitAttributeId = "LastHotListPageVisit-" + category.ToString();
             bool attributeIsSet = true;
             DateTime today = DateTime.Now;
             DateTime lastVisit = today;
             int notificationCount = 0;
             int count = 0;
             
             try
             {
                 lastVisit = _g.Member.GetAttributeDate(communityid, siteid, brandid, lastVisitAttributeId, lastVisit);
             }
             catch (Exception ex)
             {
                 //exception is thrown if attribute not set for site/brand
                 attributeIsSet = false;
                 ex = null;
             }

             if (attributeIsSet)
             {
                 count = _g.List.GetCount(category, communityid, siteid);

                 //if no last visit yet then count == notifications. This is only true if the
                 //pages have never been visited for the life of the account (only really true 
                 //for new members)
                 if (lastVisit.Equals(today) || count <= 0)
                 {
                     notificationCount = count;
                 }
                 else
                 {
                     //get count based on last date of page visit
                     int rowcount;

                     ArrayList memberIDs = _g.List.GetListMembers(category,
                                         communityid,
                                         siteid,
                                         1,
                                         count,
                                         out rowcount);

                     if (memberIDs != null)
                     {
                         for (int i = 0; i < memberIDs.Count; i++)
                         {
                             ListItemDetail listDetail = _g.List.GetListItemDetail(category,
                                             communityid,
                                             siteid,
                                             (int)memberIDs[i]);

                             if (listDetail != null)
                             {
                                 if (listDetail.ActionDate > lastVisit)
                                 {
                                     notificationCount++;
                                 }
                                 else
                                     break; //list are returned sorted by action date so there's no need to continue
                             }
                         }
                     }
                 }
             }

             return notificationCount;
        }

        /// <summary>
        /// Gets a list of recent activities for a set of hotlists
        /// </summary>
        /// <param name="categoryToInclude"></param>
        /// <param name="numberofActivityFromEachCategory"></param>
        /// <param name="_g"></param>
        /// <returns>List of activities sorted by most recent action date</returns>
        public static List<HotlistActivityItem> GetLatestHotlistActivity(HotListCategory[] categoryToInclude, int numberofActivityFromEachCategory, ContextGlobal _g)
        {
            int brandid = _g.Brand.BrandID;
            int communityid = _g.Brand.Site.Community.CommunityID;
            int siteid = _g.Brand.Site.SiteID;
            List<HotlistActivityItem> listActivities = new List<HotlistActivityItem>();

            foreach (HotListCategory category in categoryToInclude)
            {
                int rowcount;
                ArrayList memberIDs = _g.List.GetListMembers(category,
                                    communityid,
                                    siteid,
                                    1,
                                    numberofActivityFromEachCategory,
                                    out rowcount);

                if (memberIDs != null)
                {
                    for (int i = 0; i < memberIDs.Count; i++)
                    {
                        ListItemDetail listDetail = _g.List.GetListItemDetail(category,
                                        communityid,
                                        siteid,
                                        (int)memberIDs[i]);

                        if (listDetail != null)
                        {
                            HotlistActivityItem hotlistDetail = new HotlistActivityItem();
                            hotlistDetail.ActionDate = listDetail.ActionDate;
                            hotlistDetail.HotlistCategory = category;
                            hotlistDetail.MemberID = (int)memberIDs[i];
                            listActivities.Add(hotlistDetail);
                        }
                    }
                }
            }

            listActivities.Sort();
            return listActivities;
        }

       
    }
}
