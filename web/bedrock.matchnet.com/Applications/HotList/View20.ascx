﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View20.ascx.cs" Inherits="Matchnet.Web.Applications.HotList.View20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/BasicElements/ResultList20.ascx" %>
<%@ Register TagPrefix="mn1" TagName="ResultsViewType" Src="../../Framework/Ui/BasicElements/ResultsViewType.ascx" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<%@ Register TagPrefix="uc1" TagName="TabBar"  Src="../../Framework/Ui/BasicElements/TabBar.ascx" %>

<div class="header-options clearfix">
    <h1><asp:literal runat="server" id="ltlPageTitle" /></h1>
    <div class="pagination"><asp:label id="lblListNavigationTop" Runat="server" /></div>
</div>
<uc1:TabBar id="hotListTabBar" runat="server" ClassSelected="selected" />
<div class="sort-display clearfix">
  <asp:PlaceHolder ID="phMoreMembers" runat="server"  Visible="false">
    <script type="text/javascript">
    jQuery(document).ready(function(){
		jQuery('li.head-link').hover(
			function() { jQuery('ul', this).css('display', 'block'); },
			function() { jQuery('ul', this).css('display', 'none'); });
	});
    </script>
    <div class="css-dropdown-wrapper">
        <ul id="css-dropdown">
            <li class="head-link">
                <a href="#"><mn:Txt runat="server" ID="txtMoreMembers" ResourceConstant="ADD_MORE_MEMBERS" /></a>
                <ul>
                    <li><a href="/Applications/Search/SearchResults.aspx"> <mn:Txt runat="server" ID="txt1" ResourceConstant="ADD_MORE_MATCHES" /></a></li>
                    <li><a href="/Applications/QuickSearch/QuickSearchResults.aspx"> <mn:Txt runat="server" ID="txt3" ResourceConstant="ADD_MORE_QUICKSEARCH" /></a></li>
                    <li><a href="/Applications/MembersOnline/MembersOnline.aspx"> <mn:Txt runat="server" ID="txt2" ResourceConstant="ADD_MORE_MOL" /></a></li>
                </ul>
            </li>
        </ul>
    </div>
     </asp:PlaceHolder>
	<div class="view-type image-text-pair">
	     <mn1:ResultsViewType id="idResultsViewType" runat="server" />   
	</div>
	<div class="cat-list">
	    <span class="spr s-icon-folder-create-manage"><span></span></span>&nbsp;<mn:txt id="txtEditCat" runat="server" resourceconstant="TXT_EDIT_CATEGORIES_ARROWS" href="/Applications/HotList/ManageListCategory.aspx" />
    </div>
	<%--<asp:PlaceHolder ID="phSearchLinks" runat="server" Visible="false">
		<mn:txt id="txtWithin" ResourceConstant="ADD_MORE_MEMBERS" runat="server"></mn:txt>
        <asp:DropDownList id="ddlSearchLinks" AutoPostBack="true" Runat="server"></asp:DropDownList> --%>
   
</div>

<div id="results-container" class="clearfix clear-both">
    <result:resultlist runat="server" id="resultHotList" resultlistcontexttype="HotList" />
</div>

<div class="pagination"><asp:label id="lblListNavigationBottom" Runat="server" /></div>
