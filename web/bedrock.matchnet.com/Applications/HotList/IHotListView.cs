﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Session.ValueObjects;

namespace Matchnet.Web.Applications.HotList
{
    public interface IHotListView
    {

        IResultList ResultHotList { get; }
        int MoveCount { get; set; }
        //string ChangeVoteMessage;
        //protected Matchnet.Web.Framework.Image icon_galleryView;
        //protected DropDownList ddlOptions;
        //protected System.Web.UI.WebControls.Literal ltlPageTitle;
        //protected System.Web.UI.WebControls.HyperLink lnkViewType;
        //protected Matchnet.Web.Framework.Image icon_ViewType;
        //protected Matchnet.Web.Framework.Ui.BasicElements.ResultList ResultHotList;
        //protected Matchnet.Web.Framework.Txt txtEditCat;
        //protected Matchnet.Web.Framework.Txt txtViewAsHL;
        //protected HtmlInputHidden GalleryView;
    }
}
