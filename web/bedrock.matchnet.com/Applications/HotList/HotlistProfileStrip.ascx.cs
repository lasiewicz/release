﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Applications.Home;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.HotList
{
    public enum HotlistDirectionType : int
    {
        WhoList = 1,
        YouList = 2
    }
    public class ListProfileHolder : ProfileHolder, IComparable
    {
        ListItemDetail _listItem;
        public ListItemDetail ListItem
        {
            get { return _listItem; }
            set { _listItem = value; }
        }

        public int CompareTo(object obj)
        {
            if (obj is ListProfileHolder)
            {
                ListProfileHolder l2 = (ListProfileHolder)obj;
                return _listItem.ActionDate.CompareTo(l2.ListItem.ActionDate) * -1;
            }
            else
                throw new ArgumentException("Object is not a ListProfileHolder.");
        }
    }

    public class HotlistResourceMapType
    {
        int category;
        public string resourceConstant;
        public int count;

        public HotlistResourceMapType(int cat, string res)
        {
            category = cat;
            resourceConstant = res;
            count = 0;
        }

        public int Category
        {
            get { return category; }
            set { category = value; }
        }

        public int Count
        {
            get { return count; }
            set { count = value; }
        }

        public string ResourceConstant
        {
            get { return resourceConstant; }
            set { resourceConstant = value; }
        }
    }
    public partial class HotlistProfileStrip : FrameworkControl
    {


        #region default lists of resource constants init can be overwritten by props
        List<HotlistResourceMapType> _whoListResourceList = new List<HotlistResourceMapType>{new HotlistResourceMapType(-1,"LISTCATEGORY_WHOS_VIEWED_YOUR_PROFILE"),new HotlistResourceMapType(-3,"LISTCATEGORY_WHOS_TEASED_YOU"),new HotlistResourceMapType(-4,"LISTCATEGORY_WHOS_EMAILED_YOU"),
                                                            new HotlistResourceMapType(-2,"LISTCATEGORY_WHOS_ADDED_YOU_TO_THEIR_HOT_LISTS"),new HotlistResourceMapType(-25,"LISTCATEGORY_WHOS_SENT_YOU_ECARDS"),new HotlistResourceMapType(-5,"LISTCATEGORY_WHOS_IMD_YOU")};

        List<HotlistResourceMapType> _youListResourceList = new List<HotlistResourceMapType>{new HotlistResourceMapType(-9,"LISTCATEGORY_MEMBERS_YOUVE_VIEWED"),new HotlistResourceMapType(-6,"LISTCATEGORY_MEMBERS_YOUVE_TEASED"),new HotlistResourceMapType(-7,"LISTCATEGORY_MEMBERS_YOUVE_SENT_EMAILS_TO"),
                                                            new HotlistResourceMapType(-0,"LISTCATEGORY_MEMBERS_YOUVE_HOT_LISTED"),new HotlistResourceMapType(-24,"LISTCATEGORY_MEMBERS_YOUVE_SENT_ECARDS"),new HotlistResourceMapType(-8,"LISTCATEGORY_MEMBERS_YOUVE_IMD")};

        List<HotListCategory> _categoryWhoListSubscriber = new List<HotListCategory>{HotListCategory.WhoEmailedYou, HotListCategory.WhoTeasedYou, HotListCategory.WhoIMedYou,
                                                                                   HotListCategory.WhoAddedYouToTheirFavorites, HotListCategory.WhoViewedYourProfile};


        List<HotListCategory> _categoryWhoListNonSubscriber = new List<HotListCategory> { HotListCategory.WhoViewedYourProfile, HotListCategory.WhoAddedYouToTheirFavorites, HotListCategory.WhoSentYouECards };


        List<HotListCategory> _categoryYouListSubscriber = new List<HotListCategory>{HotListCategory.MembersYouEmailed, HotListCategory.MembersYouTeased, HotListCategory.MembersYouIMed,
                                                                                   HotListCategory.Default, HotListCategory.MembersYouViewed};


        List<HotListCategory> _categoryYouListNonSubscriber = new List<HotListCategory> { HotListCategory.MembersYouViewed, HotListCategory.MembersYouIMed, HotListCategory.MembersYouSentECards };


        #endregion

        FrameworkControl _resourceControl = null;
        int _hotlistCount = 3;
        List<HotlistResourceMapType> _displayResourceList;
        List<HotListCategory> _displayCategoryList;
        HotlistDirectionType _hotlistDirection;
        string _title;
        string _noResultsResource;
        #region public props
        public FrameworkControl ResourceControl
        {
            set { _resourceControl = value; }
            get { return _resourceControl; }
        }

        public List<HotlistResourceMapType> DisplayResourceList
        {
            set { _displayResourceList = value; }
            get { return _displayResourceList; }
        }

        public List<HotListCategory> DisplayCategoryList
        {
            set { _displayCategoryList = value; }
            get { return _displayCategoryList; }
        }



        public HotlistDirectionType HotlistDirection
        {
            set { _hotlistDirection = value; }
            get { return _hotlistDirection; }
        }

        public HotlistResourceMapType TitleResourceMap
        {
            get
            {
                if (_displayResourceList != null && _displayResourceList.Count > 0)
                    return _displayResourceList[0];
                else
                    return _whoListResourceList[0];
            }
        }

        public string Title
        {
            get
            { return _title; }
            set { _title = value; }
        }


        public string NoResultsResource
        {
            get
            { return _noResultsResource; }
            set { _noResultsResource = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {


                if (_hotlistDirection == HotlistDirectionType.WhoList)
                {
                    if(_displayResourceList== null)
                        _displayResourceList = _whoListResourceList;
                   
                    if (String.IsNullOrEmpty(_title))
                    { _title = "TXT_WHO_CHECKING_YOU_OUT"; }

                }
                else
                {
                    if (_displayResourceList == null)
                        _displayResourceList = _youListResourceList;
                    
                    if (String.IsNullOrEmpty(_title))
                    { _title = "TXT_WHO_YOU_ARE_CHECKING_OUT"; }
                }

                if (!g.EcardsEnabled)
                {
                  //Remove ecards from the lists
                  _displayResourceList.RemoveAt(4);
                }

                PopulateCounts();
                if (_resourceControl == null)
                    _resourceControl = this;

                litTitle.Text = g.GetResource(_title, _resourceControl);
                
                rptHostlistLinks.DataSource = _displayResourceList;
                rptHostlistLinks.DataBind();
                DetermineCategories();
                PopulateProfiles();
                
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        #region private methods
        private void PopulateCounts()
        {
            try
            {

                int communityid = g.Brand.Site.Community.CommunityID;
                int siteid = g.Brand.Site.SiteID;
                for (int i = 0; i < _displayResourceList.Count; i++)
                {
                    _displayResourceList[i].Count = g.List.GetCount((HotListCategory)_displayResourceList[i].Category, communityid, siteid);
                }
            }
            catch (Exception ex)
            { throw (ex); }
        }

        private void PopulateProfiles()
        {
            List<ListProfileHolder> profileList = new List<ListProfileHolder>();
            foreach (HotListCategory cat in _displayCategoryList)
            {
                DetermineHotlistProfiles(profileList, cat);

            }
            profileList.Sort();
            List<ProfileHolder> profileHolderList = new List<ProfileHolder>();
            foreach (ListProfileHolder list in profileList)
            {
                profileHolderList.Add((ProfileHolder)list);
            }
            microProfileStrip.MemberList = profileHolderList;
            microProfileStrip.DisplayContext = DisplayContextType.HotList;
            microProfileStrip.NoResultContentResource = _noResultsResource;
            microProfileStrip.ResourceControl = this;
        }

        private void DetermineHotlistProfiles(List<ListProfileHolder> profileList, HotListCategory cat)
        {
            int count;

            ArrayList memberIDs = g.List.GetListMembers(cat,
                                g.Brand.Site.Community.CommunityID,
                                g.Brand.Site.SiteID,
                                1,
                                _hotlistCount,
                                out count);

            ArrayList MemberList = MemberDTOManager.Instance.GetIMemberDTOs(g, memberIDs, MemberType.Search, MemberLoadFlags.None);
                
            for (int i = 0; i < MemberList.Count; i++)
            {
                ListProfileHolder profileMWH = new ListProfileHolder();
                profileMWH.MyEntryPoint = BreadCrumbHelper.EntryPoint.HotLists;
                profileMWH.Ordinal = i + 1; //hotlist ordinals start at 1
                profileMWH.HotlistCategory = cat;
                profileMWH.Member = MemberList[i] as IMemberDTO;
                ListItemDetail listDetail = g.List.GetListItemDetail(cat,
                                g.Brand.Site.Community.CommunityID,
                                g.Brand.Site.SiteID,
                                (int)memberIDs[i]);

                profileMWH.ListItem = listDetail;

                IEnumerable<ListProfileHolder> coll = profileList.Where<ListProfileHolder>(delegate(ListProfileHolder l) { return l.Member.MemberID == profileMWH.Member.MemberID; });
                if (coll != null && coll.Count<ListProfileHolder>() > 0)
                {
                    ListProfileHolder existingProfile = coll.ToList<ListProfileHolder>()[0];
                    if (existingProfile.ListItem.ActionDate < profileMWH.ListItem.ActionDate)
                    {
                        profileList.Remove(existingProfile);
                        profileList.Add(profileMWH);

                    }

                }
                else
                { profileList.Add(profileMWH); }
            }

        }

        private void DetermineCategories()
        {
            if (_displayCategoryList != null && _displayCategoryList.Count > 0)
                return;
            if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
            {
                if (_hotlistDirection == HotlistDirectionType.WhoList)
                    _displayCategoryList = _categoryWhoListSubscriber;
                else
                    _displayCategoryList = _categoryYouListSubscriber;
            }
            else
            {
                if (_hotlistDirection == HotlistDirectionType.WhoList)
                    _displayCategoryList = _categoryWhoListNonSubscriber;
                else
                    _displayCategoryList = _categoryYouListNonSubscriber;
            }

        }
        #endregion
    }
}