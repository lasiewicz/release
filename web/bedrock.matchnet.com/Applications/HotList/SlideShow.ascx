﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SlideShow.ascx.cs" Inherits="Matchnet.Web.Applications.HotList.SlideShow" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register src="/Framework/Ui/BasicElements/SlideshowProfile.ascx" tagname="SlideshowProfile" tagprefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="MicroProfileList" Src="/Framework/Ui/BasicElements/MicroProfileList.ascx" %>
<script type="text/javascript" src="/javascript20/library/jquery.tmpl.js"></script>
<script type="text/javascript" src="/webservices/member/membersvc.js"></script>



<script type="text/javascript">

    var ageformat = "<%= GetAgeFormat()%>";
    var viewprofilelinkformat = "<% =ViewProfileLinkFormat%>";

    var viewmore = "test";

    var cache = {};
    function GetMember(memberid) {

        GetMemberByID(memberid, 1, AjaxSucceeded, AjaxFailed);
        return false;
    }
    function AjaxSucceeded(result) {
        if (result.d.MemberID >= 0) {
            var microPrf = $j("#MictoProfileTemplate").render(result.d);
            $j("#divMicroProfileInject").replaceWith(microPrf);
            cache[result.d.MemberID] = result.d;

        }
        else {
            //  alert("An error occurred and the update was not made.");
        }
    }

    function AjaxFailed(result) {
        //console.log(result);
        
    }
    $j(document).ready(function() {
        var $buckets = $j('#buckets');
        var $yesBucket = $j('#yesBucket');
        var $noBucket = $j('#noBucket');
        var $maybeBucket = $j('#maybeBucket');
	    var $placeholder = $j('#placeholder');
	    var profile = '.home-your-matches';
        /*$yesBucket.find('.carousel').jCarouselLite({
            btnNext: "#yesBucket>.next",
            btnPrev: "#yesBucket>.prev",
            circular: false,
            vertical: true,
            visible: 4
        });*/
        if($yesBucket.find(profile).length < 4) {
            $yesBucket.find('.your-matches-view-more-btm').hide();
        } else {
            $yesBucket.find('.your-matches-view-more-btm').appendTo($yesBucket).show();
        };
        /*$noBucket.find('.carousel').jCarouselLite({
            btnNext: "#noBucket>.next",
            btnPrev: "#noBucket>.prev",
            circular: false,
            vertical: true,
            visible: 4
        });*/
        if($noBucket.find(profile).length < 4) {
            $noBucket.find('.your-matches-view-more-btm').hide();
        } else {
            $noBucket.find('.your-matches-view-more-btm').appendTo($noBucket).show();
        };
        /*$maybeBucket.find('.carousel').jCarouselLite({
            btnNext: "#maybeBucket>.next",
            btnPrev: "#maybeBucket>.prev",
            circular: false,
            vertical: true,
            visible: 4
        });*/
        if($maybeBucket.find(profile).length < 4) {
            $maybeBucket.find('.your-matches-view-more-btm').hide();
        } else {
            $maybeBucket.find('.your-matches-view-more-btm').appendTo($maybeBucket).show();
        };
        /*$j('button', '#buckets').click(function(){
            return false;
        });*/
        function checkEmptyBuckets(){
            if ($buckets.hasClass('empty') != true) {
                var bucketLength = $buckets.find(profile).length;
                if (bucketLength == 0) {
                    $buckets.addClass('empty');
                    $placeholder.fadeIn();
                } else {};
            } else {};
        };
        checkEmptyBuckets(); 
        $j('#slideshow').delegate('.spr-btn', 'click', function(){
            $placeholder.fadeOut('fast');
        });
    });
</script>
<script id="MictoProfileTemplate" type="text/html">
  
<li class="home-your-matches clearfix">
{% 
    var viewprofilelink=viewprofilelinkformat.replace('{0}',MemberID);
    
   %}
            <!--microprofile20-->
      <div class="micro-profile clearfix">
	
    <!--MicroProfile20-->
    <div class="member-pic">
    {% if(ThumbPhotos.length > 0){ %}
        <a href="{%=viewprofilelink %}"><img border="0px" alt="profile picture" src="{%= ThumbPhotos[0]%}" class="profileImageHover" title="View my profile"></a>
    {% }; %}
    </div>
    <div class="member-info">
        <!--mutual yes-->
        
        <!--username-->
        <a href="{%=viewprofilelink %}" >{%=UserName%}</a>
        <!--highlight icon-->
        
        <!--other basic info-->
        <p> {%= ageformat.replace('{0}',Age) %}
            
            <br>
            {%= DisplayLocation %}
        </p>
       
    </div>
    

  </div>
 </li>

</script>
<div id="secretAdmirer" class="secret-admirer-cont">
    <div id="slideshow" class="slideshow block-on-load">
        <uc1:SlideshowProfile ID="ucSlideshowProfile" runat="server" SlideShowMode="SecretAdmirerGame"  />
    </div>
    <div id="buckets" class="clearfix">
        <div id="yesBucket" class="bucket clearfix">
            <div class="wrapper">
                <h3 class="bucket-header"><mn:Txt  runat="server" ID="txtYesTitle" ResourceConstant="TXT_YES_TITLE" ExpandImageTokens="true" /></h3>
                <div class="carousel">
                    <uc1:MicroProfileList ID="listYes" runat="server" YNMBucket="YesBucket" DisplayContext="YNMList"/>
                </div>
                <%--<button class="prev disabled">&#9650;</button>
                <button class="next disabled">&#9660;</button>--%>
            </div>
        </div>

        <div id="noBucket" class="bucket clearfix">
            <div class="wrapper">
                <h3 class="bucket-header"><mn:Txt  runat="server" ID="txtNoTtile" ResourceConstant="TXT_NO_TITLE" ExpandImageTokens="true" /></h3>
                <div class="carousel">
                    <uc1:MicroProfileList ID="listNo" runat="server" DisplayContext="YNMList" YNMBucket="NoBucket"/>
                </div>
                <%--<button class="prev disabled">&#9650;</button>
                <button class="next disabled">&#9660;</button>--%>
            </div>
        </div>

        <div id="maybeBucket" class="bucket clearfix">
            <div class="wrapper">
                <h3 class="bucket-header"><mn:Txt  runat="server" ID="txtMaybeTitle" ResourceConstant="TXT_MAYBE_TITLE" ExpandImageTokens="true" /></h3>
                <div class="carousel">
                    <uc1:MicroProfileList ID="listMaybe" runat="server" DisplayContext="YNMList" YNMBucket="MaybeBucket"/>
                </div>
                <%--<button class="prev disabled">&#9650;</button>
                <button class="next disabled">&#9660;</button>--%>
            </div>
        </div>
        <div id="placeholder"><mn:Txt runat="server" ID="mnTxtAfterYouVote" ResourceConstant="TXT_AFTER_YOU_VOTE" /></div>
    </div>
</div>
