﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Applications.Registration.Templates;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Enumerations;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration
{
    public class RegistrationBase : FrameworkControl
    {
        string _emailAddress;
        string _userName;
        string _password;

        protected List<AttributeControl> _controls;
        protected RegistrationTemplateBase _templateControl;
        protected bool __debugTrace;
        protected bool _ShowUserNameWarning = false;
        protected bool _setOmniture = true;
        protected string _attributesNames;
        protected Scenario _regScenario;
        protected FrameworkControl _resourceControl;
        protected RegistrationWizard _wizard;
        protected bool _isOverlayReg;
        protected string _lastContainerDivClientID;
        protected Dictionary<string, string> _clientIDErrorMessage = new Dictionary<string, string>();
        protected string _regSuccessURL;
        protected NameValueCollection nvcSearchPreferencesParamaters = new NameValueCollection();
        protected JQueryValidation jQueryValidation;
        protected string IOBlackBoxValue;
        protected const string OMNITURE_PAGE_NAME = "RegStep_{0}_Scenario_{1}_{2}";
        protected const int DEFAULT_PROMOTIONID = 55020;

        private RegistrationFlowType _flowType = RegistrationFlowType.FullRegistration;
        private const int NEWSLETTER_MASK_SPECIALOFFERS = 4;
        private const int DEFAULT_NEWSLETTER_MASK = 7;
        private const int REGIONID_INTERNATIONAL = -999;
        private const string SESSION_CACHED_REMOTE_REQ = "cached_registration_req";
        private const string SESSION_REMOTE_REG = "Session_remote_Reg";
        private const string SESSION_REG_SUBHEADER_RESX = "registration_sub_header";
        private string MID_USERNAME_WARNING = "rc=USER_NAME_WARNING,{0},{1}";
        private string MID_USERNAME_EMAIL_WARNING = "rc=USER_NAME_EMAIL_WARNING,{0}";


        public static string EVAR43_SESSION_KEY = "EVAR43_SESSION_KEY";

        public bool SetOmniture
        { set { _setOmniture = value; } }

        public FrameworkControl ResourceControl
        {
            set { _resourceControl = value; }
            get
            {
                if (_resourceControl == null)
                    return this;
                else
                    return _resourceControl;
            }
        }

        public RegistrationFlowType FlowType { get { return _flowType; } set { _flowType = value; } }

        public int PromotionID
        {
            get
            {
                try
                {
                    int id = DEFAULT_PROMOTIONID;
                    if (!String.IsNullOrEmpty(_wizard.Persistence["prm"]))
                        id = Conversion.CInt(_wizard.Persistence["prm"]);
                    else
                    {
                        id = Conversion.CInt(getRemoteRequestParam("prm"));

                    }
                    if (id < 0)
                        id = DEFAULT_PROMOTIONID;

                    return id;
                }
                catch (Exception ex)
                {
                    return DEFAULT_PROMOTIONID;
                }

            }
        }

        protected int RegisterNewUser(Wizard registrationWizard)
        {
            try
            {
                string ipAddressString = g.ClientIP;
                Int32 ipAddress = Constants.NULL_INT;
                if (ipAddressString.Length > 0)
                {
                    ipAddress = BitConverter.ToInt32(IPAddress.Parse(ipAddressString).GetAddressBytes(), 0);
                }
                _emailAddress = GetAttributeStringFromControls("EmailAddress");
                _userName = GetAttributeStringFromControls("UserName") ?? string.Empty;
                _password = GetAttributeStringFromControls("Password");

                if (string.IsNullOrEmpty(_emailAddress) || string.IsNullOrEmpty(_userName) || string.IsNullOrEmpty(_password))
                {
                    LogIncompleteRegistration(registrationWizard);
                }


                g.IsRemoteRegistration = false;

                MemberRegisterResult memberRegisterResult;

                if (rejectForBeingOnDNE(_emailAddress, g.Brand.Site.SiteID))
                {
                    memberRegisterResult = new MemberRegisterResult(RegisterStatusType.EmailAddressBlocked, 0, _userName);
                }
                else
                {
                    memberRegisterResult = MemberSA.Instance.Register(g.Brand,
                         _emailAddress, _userName, _password, ipAddress, g.Brand.Site.SiteID);
                }

                /*if (memberRegisterResult.RegisterStatus == RegisterStatusType.IPBlocked)
                {
                    if (_isOverlayReg || _wizard.RegistrationScenario.IsOnePageReg)
                    {
                        AddErrorMessage("IPBlocked", _lastContainerDivClientID);
                    }
                    else
                    {
                        FrameworkGlobals.RedirectToErrorPageForIPBlock();
                    }
                    
                    return 0;
                }*/
                if (memberRegisterResult.RegisterStatus != RegisterStatusType.Success)
                {

                    string s = g.GetResource(RegisterStatusTypeResources.GetResourceConstant(memberRegisterResult.RegisterStatus), null);

                    if (_lastContainerDivClientID != null)
                    {
                        AddErrorMessage(g.GetResource(RegisterStatusTypeResources.GetResourceConstant(memberRegisterResult.RegisterStatus), null), _lastContainerDivClientID);
                    }
                    if (_templateControl.ValidationMessageText != null)
                    {
                        _templateControl.ValidationMessageText.Text = g.GetResource(RegisterStatusTypeResources.GetResourceConstant(memberRegisterResult.RegisterStatus), null);
                        _templateControl.ValidationMessageText.Visible = true;

                    }
                    else
                    {
                        g.Notification.AddError(RegisterStatusTypeResources.GetResourceConstant(memberRegisterResult.RegisterStatus));
                    }
                    // g.Session.Remove(SESSION_CACHED_REMOTE_REQ);
                    FrameworkGlobals.Trace(__debugTrace, "Registration.RegisterNewUser", "Failed - " + memberRegisterResult.RegisterStatus.ToString(), _wizard.ToString());
                    return 0;
                }
                else // Success
                {
                    g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_REGISTRATIONDATE, DateTime.Now.ToString("u"), Matchnet.Session.ValueObjects.SessionPropertyLifetime.TemporaryDurable);
                    g.Session.Add("MemberID", memberRegisterResult.MemberID, SessionPropertyLifetime.Temporary);
                    g.Session.Add("EmailAddress", _emailAddress, SessionPropertyLifetime.Persistent);
                    //  ExternalMailSA.Instance.SendRegistrationVerification(g.Member.MemberID, g.Brand.BrandID, g.Brand.Site.SiteID);
                    // g.Session.Remove(SESSION_CACHED_REMOTE_REQ);
                    FrameworkGlobals.Trace(__debugTrace, "Registration.RegisterNewUser", "Success - " + memberRegisterResult.RegisterStatus.ToString(), _wizard.ToString());
                    return memberRegisterResult.MemberID;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                FrameworkGlobals.Trace(__debugTrace, "Registration.RegisterNewUser", "Exception - " + ex.ToString(), null);
                return 0;
            }

        }

        private void LogIncompleteRegistration(Wizard registrationWizard)
        {
            StringBuilder logText = new StringBuilder();

            string emailAddress = GetAttributeStringFromControls("EmailAddress") ?? string.Empty;
            string userName = GetAttributeStringFromControls("UserName") ?? string.Empty;
            string password = GetAttributeStringFromControls("Password") ?? string.Empty;

            logText.AppendLine("BEGIN PERSISTENCE OUTPUT");
            logText.AppendLine("Persistence: " + registrationWizard.Persistence.ToString());
            logText.AppendLine("END PERSISTENCE OUTPUT");
            logText.AppendLine("Scenario: " + registrationWizard.ScenarioName);
            logText.AppendLine("RegGuid: " + registrationWizard.RegistrationgUID);
            logText.AppendLine("CompletedSteps: " + registrationWizard.CompletedSteps.ToString());
            logText.AppendLine("CurrentStep: " + registrationWizard.StepID.ToString());
            logText.AppendLine("EmailAddress: " + emailAddress);
            logText.AppendLine("UserName: " + userName);
            logText.AppendLine("Password: " + password);
            Exception ex = new Exception("INCOMPLETE REGISTRATION ERROR: " + logText.ToString());

            g.LoggingManager.LogException(ex, null, g.Brand, "RegistrationBase");
        }

        protected bool SaveMember(Member.ServiceAdapters.Member _Member)
        {

            try
            {
                g.LoggingManager.LogInfoMessage("RegistrationBase", "MemberID: " + _Member.MemberID + " Number of controls: " + _controls.Count.ToString());

                for (int i = 0; i < _controls.Count; i++)
                {
                    string attributeValue = string.Empty;

                    if (_controls[i].AttributeType == AttrType.attrInt) attributeValue = _controls[i].AttributeIntValue.ToString();
                    if (_controls[i].AttributeType == AttrType.attrDate) attributeValue = _controls[i].AttributeDateValue.ToShortDateString();
                    if (_controls[i].AttributeType == AttrType.attrTxt) attributeValue = _controls[i].AttributeTextValue;

                    string loggingMessage = string.Format("MemberID: {0} Count: {1} AttributeName: {2} Value: {3} ConditionalShowFlag: {4} ", _Member.MemberID, i, _controls[i].AttributeName, attributeValue, _controls[i].ConditionalShowFlag);
                    g.LoggingManager.LogInfoMessage("RegistrationBase", loggingMessage);

                    if (!_controls[i].ConditionalShowFlag)
                        continue;
                    if (_controls[i].Type == ControlType.colorcode)
                    { continue; }
                    if (_controls[i].AttributeName.ToLower() == "newslettermask")
                    {
                        int newsletterMask = _controls[i].AttributeIntValue;
                        // FrameworkControl attribute = (FrameworkControl)_templateControl.FindControl("id" + _controls[i].Name);
                        if (newsletterMask <= 0)
                        {

                            newsletterMask = DEFAULT_NEWSLETTER_MASK - NEWSLETTER_MASK_SPECIALOFFERS;

                        }
                        else
                        {
                            newsletterMask = DEFAULT_NEWSLETTER_MASK;
                        }

                        _controls[i].AttributeIntValue = newsletterMask;
                    }
                    // Username already save in mnMember, mnLogon, mnAdmin at this point. W/o this code, it overwrites it especially
                    // when memberID is saved as username.
                    if (_controls[i].AttributeName.ToLower() == "username" || _controls[i].AttributeName.ToLower() == "infocontrol")
                    {
                        continue;
                    }
                    if (_controls[i].SearchPref == SearchPrefType.Yes)
                    {
                        continue;
                    }
                    _controls[i].SaveMemberAttribute(_Member, g);
                    FrameworkGlobals.Trace(__debugTrace, "Registration.SaveMember", "SaveMemberAttribute", _controls[i].ToString());
                }

                _Member.SetAttributeText(g.Brand, "RegistrationScenarioName", _wizard.RegistrationScenario.Name, TextStatusType.Auto);

                FrameworkGlobals.Trace(__debugTrace, "Registration.SaveMember", "Setting brand dates", "");
                Int32 brandLogonCount = _Member.GetAttributeInt(g.Brand, "BrandLogonCount", 0);
                if (brandLogonCount == 0)
                {
                    DateTime now = DateTime.Now;
                    _Member.SetAttributeDate(g.Brand, "BrandInsertDate", now);
                    //this attribute is immutable so it is safe to attempt updates all day
                    _Member.SetAttributeDate(g.Brand, "BrandLastLogonDate", now);
                    _Member.SetAttributeInt(g.Brand, "LastBrandID", g.Brand.BrandID);
                    _Member.SetAttributeInt(g.Brand, "BrandLogonCount", brandLogonCount + 1);
                    _Member.SetAttributeInt(g.Brand, "LanguageMask", g.Brand.Site.LanguageID);
                }



                FrameworkGlobals.Trace(__debugTrace, "Registration.SaveMember", "EmailVerify", "");
                if (g.EmailVerify.EnableEmailVerification)
                {
                    if (g.EmailVerify.IfMustBlock(WebConstants.EmailVerificationSteps.AfterRegistration) || g.EmailVerify.IfMustHide(WebConstants.EmailVerificationSteps.AfterRegistration))
                    {
                        g.EmailVerify.SetNotVerifiedMemberAttributes(_Member, WebConstants.EmailVerificationSteps.AfterRegistration);

                    }

                    g.Session.Add(EmailVerifyHelper.SESSION_EMAIL_VERIFY_JUST_REGISTERED, "true", SessionPropertyLifetime.Temporary);
                }

                RegistrationReason.SetMemberAttributes(_Member, g);
                _Member.SetAttributeDate(g.Brand, "LastUpdated", DateTime.Now);
                _Member.SetAttributeDate(g.Brand, "LastUpdated", DateTime.Now);
                FrameworkGlobals.Trace(__debugTrace, "Registration.SaveMember", "setPromotionInfo", "");
                setPromotionInfo(_Member);

                //desperate hack
                try
                {
                    FrameworkGlobals.Trace(__debugTrace, "Registration.SaveMember", "Checking RegionID", "");
                    int registrationRegionID = _Member.GetAttributeInt(g.Brand, "RegionID");
                    if (registrationRegionID <= 0)
                    {
                        FrameworkGlobals.Trace(__debugTrace, "Registration.SaveMember", "Checking RegionID - problem, will try to fix ", "");
                        //attempt to read from session
                        registrationRegionID = Conversion.CInt(g.Session.GetString("REGISTRATION_REGIONID", Constants.NULL_INT.ToString()), Constants.NULL_INT);
                        if (registrationRegionID > 0)
                        {
                            FrameworkGlobals.Trace(__debugTrace, "Registration.SaveMember", "Checking RegionID - problem, trying to fix from session - " + registrationRegionID.ToString(), "");
                            _Member.SetAttributeInt(_g.Brand, "RegionID", registrationRegionID);
                            _g.Session.Remove("REGISTRATION_REGIONID");
                        }
                        else
                        {
                            FrameworkGlobals.Trace(__debugTrace, "Registration.SaveMember", "Checking RegionID - problem, trying to fix from session - no luck ", "");
                        }
                    }

                }
                catch (Exception regionexc)
                {
                    FrameworkGlobals.Trace(__debugTrace, "Registration.SaveMember", "Checking RegionID - problem, exception " + regionexc.ToString(), "");
                }

                MemberSaveResult saveResult = MemberSA.Instance.SaveMember(_Member, g.Brand, true);

                if (_Member.GetAttributeInt(g.Brand, "RegionID", 0) == 0)
                {
                    g.LoggingManager.LogInfoMessage("RegistrationBase", "MemberID: " + _Member.MemberID + " HAS NO REGIONID");
                }

                FrameworkGlobals.Trace(__debugTrace, "Registration.SaveMember", "saveResult=" + saveResult.ToString(), "");
                if (saveResult.SaveStatus == MemberSaveStatusType.Success)
                {
                    if (RegistrationCapture.IsRegistrationCaptureEnabled(g))
                        RegistrationCapture.DeleteRegistrationEvent(_wizard.RegistrationEvent, g);
                    _wizard.ClearPersistence();
                    FrameworkGlobals.SetConnectCookie(g);
                    ExternalMailSA.Instance.SendRegistrationVerification(g.Member.MemberID, g.Brand.BrandID, g.Brand.Site.SiteID);
                    // Omniture Analytics - Event and property variable tracking
                    if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                    {
                        // Tracking Omniture Traffic Variables on successful login.
                        g.Session.Add("OmnitureSavedRegStep", "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                    }

                    //g.Session.Remove(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID);
                    g.Session.Remove(SESSION_REG_SUBHEADER_RESX);
                    g.Session.Remove(SESSION_REMOTE_REG);

                    SavePhotos();
                    SaveColorCode();
                    g.MemberPhotoRequire.ApplyPhotoRequirementToMember(g.Member);
                    g.MemberPhotoRequire.UpdateRebrandLoginDate(g.Member);

                    bool banishForROLF = Convert.ToBoolean(RuntimeSettings.GetSetting("MINGLE_ROLF_ENABLE_BANISH", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    bool passedFraud = PassedFraud(_Member);

                    g.Session.Add("RegCompletePixelNotFired", "true", SessionPropertyLifetime.TemporaryDurable);

                    if (passedFraud || !banishForROLF)
                    {
                        if (_isOverlayReg)
                        {
                            _regSuccessURL = getNextRegistrationStepURL();
                        }
                        else
                        {
                            g.Transfer(getNextRegistrationStepURL());
                        }
                    }
                    else
                    {
                        //suspend user
                        GlobalStatusMask globalStatusMask = (GlobalStatusMask)_Member.GetAttributeInt(_g.Brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK);
                        globalStatusMask = globalStatusMask | GlobalStatusMask.AdminSuspended;
                        _Member.SetAttributeInt(_g.Brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK, (Int32)globalStatusMask);
                        _Member.SetAttributeInt(_g.Brand, "LastSuspendedAdminReasonID", (Int32)AdminActionReasonID.ROLF);


                        MemberSA.Instance.SaveMember(_Member);

                        //log the action in the admin log
                        AdminSA.Instance.AdminActionLogInsert(_Member.MemberID,
                                    Constants.GROUP_PERSONALS
                                    , (Int32)AdminAction.AdminSuspendMember
                                    , -1
                                    , AdminMemberIDType.AdminProfileMemberID
                                    , AdminActionReasonID.ROLF);

                        //kill all traces of user session
                        MembersOnlineSA.Instance.Remove(g.Brand.Site.Community.CommunityID, _Member.MemberID);

                        // Members are now deleted from cache if they log off to allow for synchronization
                        // with outside partners such as Mingle that use the SparkWS.
                        MemberSA.Instance.ExpireCachedMember(_Member.MemberID);

                        g.Session.Add("OverrideTypeID", "", SessionPropertyLifetime.TemporaryDurable);
                        g.Session.Clear();

                        // AutoLogin
                        if (AutoLoginHelper.IsSettingEnabled(g))
                        {
                            g.AutoLogin.ExpireAutoLogin();
                        }


                        if (_isOverlayReg)
                        {
                            _regSuccessURL = "/Default.aspx";
                        }
                        else
                        {
                            g.Transfer("/Default.aspx");
                        }
                    }

                }
                else
                {
                    // Problems with registration/save, refresh page with error message.
                    if (_isOverlayReg)
                    {
                        _regSuccessURL = "/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit&rc=ERR_ERROR_SAVING_REGISTRATION";
                    }
                    else
                    {
                        g.Transfer("/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit&rc=ERR_ERROR_SAVING_REGISTRATION");

                    }
                }
                return true;
            }
            catch (System.Threading.ThreadAbortException thEx)
            {
                thEx = null;
                return true;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                throw ex;
            }

        }


        private bool PassedFraud(Member.ServiceAdapters.Member member)
        {
            MingleROLFClient rolfClient = new MingleROLFClient(g.Brand, member.MemberID, member.EmailAddress);
            rolfClient.RegIP = member.GetAttributeInt(g.Brand, "RegistrationIP");
            rolfClient.GenderMask = member.GetAttributeInt(g.Brand, "GenderMask");
            rolfClient.FirstName = member.GetAttributeText(g.Brand, "SiteFirstName");
            rolfClient.LastName = member.GetAttributeText(g.Brand, "SiteLastName");
            rolfClient.MaritalStatus = GetMaritalStatusDescription(member.GetAttributeInt(g.Brand, "MaritalStatus"));

            if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
            {
                rolfClient.Ethnicity = Option.GetDescriptions("JDateEthnicity", member.GetAttributeInt(g.Brand, "JDateEthnicity"), g);
                rolfClient.Religion = Option.GetDescription("JDateReligion", member.GetAttributeInt(g.Brand, "JDateReligion"), g);
            }
            else
            {
                rolfClient.Ethnicity = Option.GetDescriptions("Ethnicity", member.GetAttributeInt(g.Brand, "Ethnicity"), g);
                rolfClient.Religion = Option.GetDescription("Religion", member.GetAttributeInt(g.Brand, "Religion"), g);
            }

            if (Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_COLORCODE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)) == true)
            {
                string memberColorCodeVal = member.GetAttributeText(g.Brand, "ColorCodeQuizAnswers");
                rolfClient.ColorCode = memberColorCodeVal == "0" ? string.Empty : memberColorCodeVal;
            }

            RegionLanguage region = RegionSA.Instance.RetrievePopulatedHierarchy(member.GetAttributeInt(g.Brand, "RegionID"), g.Brand.Site.LanguageID);
            Region postalRegion = RegionSA.Instance.RetrieveRegionByID(region.PostalCodeRegionID, g.Brand.Site.LanguageID);
            Region countryRegion = RegionSA.Instance.RetrieveRegionByID(region.CountryRegionID, g.Brand.Site.LanguageID);

            rolfClient.PostalCode = postalRegion.Description;
            rolfClient.Country = countryRegion.Description;

            if (g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE, null) != null)
            {
                rolfClient.LGID = g.Session[WebConstants.SESSION_PROPERTY_NAME_LUGGAGE];
            }

            if (g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, null) != null)
            {
                rolfClient.PRM = g.Session[WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID];
            }
            //Registration overlay - hidden field value is empty
            //Get the blackbox value from cookie
            if (string.IsNullOrEmpty(IOBlackBoxValue))
            {
                //Get blackbox value from cookie
                HttpCookie bbCookie = HttpContext.Current.Request.Cookies["regiobb"];
                if (bbCookie != null)
                {
                    //jQuery.Cookie plugin by default encodes the value. Decode the value to avoid double encoding while passing to ROLF.
                    IOBlackBoxValue = Uri.UnescapeDataString(bbCookie.Value);
                    //Expire cookie
                    bbCookie.Expires = DateTime.Now.AddDays(-2);
                }
            }
            //Send blackbox value with ROLF request
            rolfClient.IOBlackBoxValue = IOBlackBoxValue;

            ROLFResponse response = rolfClient.GetROLFResponse();

            member.SetAttributeInt(g.Brand, "ROLFScore", response.Code);
            member.SetAttributeText(g.Brand, "ROLFScoreDescription", response.Description, TextStatusType.Auto);
            MemberSaveResult saveResult = MemberSA.Instance.SaveMember(member);

            if (response.Error)
            {
                g.ProcessException(new Exception(string.Format("Error from ROLF for memberid {0}: {1}", member.MemberID.ToString(), response.ErrorDescription)));
            }

            return response.Passed;
        }


        private string GetMaritalStatusDescription(int optionID)
        {
            if (optionID == Constants.NULL_INT) return string.Empty;

            AttributeOptionCollection options = AttributeOptionSA.Instance.GetAttributeOptionCollection("MaritalStatus", 8383);

            if ((from AttributeOption o in options where o.AttributeOptionID == optionID select o).ToList().Count == 0)
                return string.Empty;

            string description = (from AttributeOption o in options where o.AttributeOptionID == optionID select o).First().Description;

            return description;
        }



        private void SaveColorCode()
        {
            try
            {

                string value = _wizard.Persistence[ControlType.colorcode.ToString()];
                Dictionary<int, int> answers = new Dictionary<int, int>();
                if (String.IsNullOrEmpty(value))
                    return;
                MemberQuiz memberQuiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);

                Quiz quiz = ColorCodeHelper.GetQuiz(g);

                string[] values = value.Split(new char[] { '|' });
                foreach (string v in values)
                {
                    string[] valuepair = v.Split(new char[] { '=' });
                    if (!String.IsNullOrEmpty(valuepair[0]))
                    {
                        int qid = Int32.Parse(valuepair[0]);
                        int q = Int32.Parse(valuepair[1]);

                        memberQuiz.AddQuestionAnswer(qid, q, quiz);
                    }
                }
                ColorCodeHelper.SaveMemberCompletedColorCodeQuiz(g.Member, g.Brand, memberQuiz, quiz);
            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }



        private void SavePhotos()
        {
            try
            {
                if (g.Member == null)
                    return;
                PhotoUpdate photoUpdate;
                byte[] newFileBytes = (byte[])g.Session.Get("REG_PHOTO");

                if (newFileBytes == null || newFileBytes.Length <= 0)
                    return;
                photoUpdate = new PhotoUpdate(newFileBytes,
                    false,
                    Constants.NULL_INT,
                    Constants.NULL_INT,
                    Constants.NULL_STRING,
                    Constants.NULL_INT,
                    Constants.NULL_STRING,
                    1,
                    false,
                    false,
                    Constants.NULL_INT,
                    Constants.NULL_INT);

                if (photoUpdate != null)
                {

                    MemberSA.Instance.SavePhotos(g.Brand.BrandID, g.Brand.Site.SiteID,
                            g.Brand.Site.Community.CommunityID, g.Member.MemberID, new PhotoUpdate[] { photoUpdate });


                    //for omniture tracking
                    g.Session.Add("REG_PHOTO_UPLOADED", "true", SessionPropertyLifetime.Temporary);
                }
                g.Session.Remove("REG_PHOTO");


            }
            catch (Exception ex)
            { g.Session.Remove("REG_PHOTO"); }
        }


        private void setPromotionInfo(Member.ServiceAdapters.Member _Member)
        {
            Int32 trackingVal = Constants.NULL_INT;

            trackingVal = g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDPROMOTIONID);
            if (trackingVal <= 1 || trackingVal == Constants.NULL_INT)
            {
                if (g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, null) != null)
                {
                    _Member.SetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDPROMOTIONID,
                        Convert.ToInt32(g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID)));
                }
                else
                {
                    if (PromotionID > 0)
                    {
                        _Member.SetAttributeInt(g.Brand, WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID,
                            Convert.ToInt32(PromotionID));
                    }
                    else
                    {
                        // No members should ever get here (a DEFAULT_PROMOTIONID) unless someone screwed up and created
                        // a registration page that didn't send a PRM and didn't have a PromotionID dropdown.
                        _Member.SetAttributeInt(g.Brand, WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, DEFAULT_PROMOTIONID);
                    }
                }
            }

            trackingVal = Constants.NULL_INT;
            trackingVal = g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDLANDINGPAGEID);
            if ((trackingVal <= 1 || trackingVal == Constants.NULL_INT)
                && g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_LANDINGPAGEID, null) != null)
            {
                _Member.SetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDLANDINGPAGEID,
                    Convert.ToInt32(g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_LANDINGPAGEID)));
            }

            trackingVal = Constants.NULL_INT;
            trackingVal = g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDBANNERID);
            if ((trackingVal <= 1 || trackingVal == Constants.NULL_INT)
                && g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_BANNERID, null) != null)
            {
                _Member.SetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDBANNERID,
                    Convert.ToInt32(g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_BANNERID)));
            }

            if (g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE, null) != null)
            {
                _Member.SetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE,
                    g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE), TextStatusType.Auto);
            }

            if (g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_REFCD, null) != null)
            {
                _Member.SetAttributeText(g.Brand, WebConstants.SESSION_PROPERTY_NAME_REFCD,
                    g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_REFCD), TextStatusType.Auto);
            }
        }




        private bool rejectForBeingOnDNE(string emailAddress, int siteID)
        {
            bool reject = false;

            DoNotEmailEntry entry = DoNotEmailSA.Instance.GetEntryBySiteAndEmailAddress(siteID, emailAddress);
            if (entry != null)
            {
                reject = true;
            }

            return reject;
        }

        protected string GetAttributeStringFromControls(string attrname)
        {
            try
            {
                if (_controls == null)
                    return null;

                IEnumerable<AttributeControl> query = from c in _controls
                                                      where c.AttributeName.ToLower() == attrname.ToLower()
                                                      select c;

                AttributeControl ctr = null;
                if (query.ToList<AttributeControl>().Count > 0)
                    ctr = query.ToList<AttributeControl>()[0];
                if (ctr == null)
                {
                    query = from c in _controls
                            where c.Name.ToLower() == attrname.ToLower()
                            select c;
                    ctr = query.ToList<AttributeControl>()[0];
                }
                return ctr.AttributeTextValue;

            }
            catch (Exception ex)
            { return null; }

        }

        private int GetAttributeIntFromControls(string attrname)
        {
            try
            {
                if (_controls == null)
                    return Constants.NULL_INT;

                IEnumerable<AttributeControl> query = from c in _controls
                                                      where c.AttributeName.ToLower() == attrname.ToLower()
                                                      select c;

                AttributeControl ctr = null;
                if (query.ToList<AttributeControl>().Count > 0)
                    ctr = query.ToList<AttributeControl>()[0];
                if (ctr == null)
                {
                    query = from c in _controls
                            where c.Name.ToLower() == attrname.ToLower()
                            select c;
                    ctr = query.ToList<AttributeControl>()[0];
                }
                return ctr.AttributeIntValue;

            }
            catch (Exception ex)
            { return Constants.NULL_INT; }

        }

        private string getRemoteRequestParam(string key)
        {
            string pwd = null;
            //if (g.IsRemoteRegistration)
            pwd = Request[key];
            //else
            //    if (_remoteRequest != null)
            //        pwd = _remoteRequest[key];

            return pwd;
        }



        private string getNextRegistrationStepURL()
        {
            string mid = string.Empty;
            string memberIdParam = string.Empty;
            if (_ShowUserNameWarning)
            {
                // Desired Username, Actual Username
                mid = getUsernameWarningParams();
            }

            if (Request.Params.Get("memberID") != null)
            {
                memberIdParam = "&memberId=" + Request.Params.Get("memberId");
            }

            string fiveDFT;
            if (Conversion.CInt(Request[WebConstants.URL_PARAMETER_NAME_FIVEDFT]) == 1)
            {
                fiveDFT = "&" + WebConstants.URL_PARAMETER_NAME_FIVEDFT + "=1";
            }
            else
            {
                fiveDFT = string.Empty;
            }

            string sparkWS = string.Empty;
            if (Request[WebConstants.URL_PARAMETER_NAME_SPARKWS_CONTEXT] == "true")
            {
                sparkWS = "&" + WebConstants.URL_PARAMETER_NAME_SPARKWS_CONTEXT + "=true";
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION]) || _wizard.RegistrationScenario.IsFacebookRegistration)
            {
                return g.AppendEncryptedSessionID("/Applications/MemberProfile/FacebookWelcome.aspx?new=1&" + WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION + "=1");
            }

            if (_wizard.RegistrationScenario.IsOnePageReg)
            {
                return "/Applications/MemberProfile/RegistrationWelcomeOnePage.aspx";
            }

            return
                string.Format(
                "/Applications/MemberProfile/RegistrationWelcome.aspx?registrationMode=1&{0}&DestinationURL={1}{2}{3}{4}"
                , mid
                , HttpUtility.UrlEncode(g.GetDestinationURL())
                , memberIdParam
                , fiveDFT
                , sparkWS);
        }

        private string getUsernameWarningParams()
        {
            string username = GetAttributeStringFromControls("UserName");
            if (g.Member.EmailAddress.ToLower().IndexOf(username.ToLower()) > -1)
            {
                return string.Format(MID_USERNAME_EMAIL_WARNING, g.Member.GetUserName(_g.Brand));
            }
            else
            {
                return string.Format(MID_USERNAME_WARNING, username, g.Member.GetUserName(_g.Brand));
            }
        }

        public string GetOmnitureRegistrationPageName(string format)
        {
            if (!string.IsNullOrEmpty(_wizard.RegistrationScenario.PageName))
            {
                return _wizard.RegistrationScenario.PageName;
            }

            string pageName = _wizard.RegistrationScenario.Name;

            if (!_wizard.RegistrationScenario.IsOnePageReg)
            {
                for (int i = 0; i < _wizard.RegistrationStep.Controls.Count; i++)
                {
                    if (!String.IsNullOrEmpty(_wizard.RegistrationStep.Controls[i].AttributeName))
                        _attributesNames += _wizard.RegistrationStep.Controls[i].AttributeName;
                    else
                        _attributesNames += _wizard.RegistrationStep.Controls[i].Name;
                    if (i < _wizard.RegistrationStep.Controls.Count - 1)
                    {
                        _attributesNames += ",";
                    }



                }

                pageName = String.Format(format, _wizard.RegistrationScenario.Name, _wizard.StepID, _attributesNames);
            }

            return pageName;
        }
        public string GetOmnitureRegistrationScenario()
        {
            string name = _wizard.RegistrationScenario.Name;

            if (_wizard.RegistrationScenario.Blank)
            {
                name += " Optimized for Performance";
            }

            return name;
        }

        protected void SetTemplate()
        {
            try
            {

                _templateControl = (RegistrationTemplateBase)LoadControl(_regScenario.Template);
                _templateControl.ContentPlaceholder = ((IRegistrationTemplate)_templateControl).ContentPlaceholder;
                _templateControl.ResourceControl = ResourceControl;
                _templateControl.TitleResource = _wizard.RegistrationStep.ResourceConstant;
                _templateControl.TipResource = _wizard.RegistrationStep.TipResourceConstant;
                _templateControl.OnePageRegContentDivCSSClass = _wizard.RegistrationStep.OnePageRegContentDivCSSClass;
                _templateControl.SubmitButtonImageFile = _wizard.RegistrationStep.SubmitButtonImageFile;
                if (_templateControl.LitmboxEVar != null)
                {
                    _templateControl.LitmboxEVar.Text = _wizard.RegistrationStep.MBoxEVar;
                }
            }
            catch (Exception ex)
            { }
        }

        protected void AddErrorMessage(string errorText, string containerDivClientID)
        {
            if (_isOverlayReg)
            {
                if (!_clientIDErrorMessage.ContainsKey(containerDivClientID))
                {
                    _clientIDErrorMessage.Add(containerDivClientID, errorText);
                }
            }
            else
            {
                if (_templateControl.ValidationMessageText != null)
                {
                    _templateControl.ValidationMessageText.Text = errorText;
                    _templateControl.ValidationMessageText.Visible = true;

                }
            }
        }

        protected string AddSearchPreferencesParamatersToURL(string url)
        {
            if (nvcSearchPreferencesParamaters.Count > 0)
            {
                url += (url.Contains('?')) ? '&' : '?';

                url += string.Join("&", Array.ConvertAll(nvcSearchPreferencesParamaters.AllKeys, key => string.Format("{0}={1}", HttpUtility.UrlEncode(key),
                    HttpUtility.UrlEncode(FlipGenderMaskValue(key, nvcSearchPreferencesParamaters[key])))));
            }
            return url;
        }

        private string FlipGenderMaskValue(string key, string value)
        {
            if (key.ToLower() != "gendermask")
                return value;

            int genderMask = Convert.ToInt32(value);
            int searchGenderMask = 0;

            // let the flipping begin
            if ((genderMask & (int)GenderMask.Male) == (int)GenderMask.Male)
            {
                searchGenderMask |= (int)GenderMask.SeekingMale;
            }
            else
            {
                searchGenderMask |= (int)GenderMask.SeekingFemale;
            }

            if ((genderMask & (int)GenderMask.SeekingMale) == (int)GenderMask.SeekingMale)
            {
                searchGenderMask |= (int)GenderMask.Male;
            }
            else
            {
                searchGenderMask |= (int)GenderMask.Female;
            }

            return searchGenderMask.ToString();
        }

        protected void ReturnJSONResponse()
        {
            ReturnJSONResponse(false);
        }

        protected void ReturnJSONResponse(bool refreshSearchResults)
        {
            string status = "ok";
            string errors = string.Empty;
            Dictionary<string, object> dicResponse = new Dictionary<string, object>();

            if (_clientIDErrorMessage.Count > 0)
            {
                status = "errors";

                InputError[] errorsArray = new InputError[_clientIDErrorMessage.Count];


                int i = 0;
                foreach (var error in _clientIDErrorMessage)
                {
                    errorsArray[i++] = new InputError() { id = error.Key, msg = error.Value };
                }

                dicResponse.Add("errors", errorsArray);
            }
            else
            {
                if (!string.IsNullOrEmpty(_regSuccessURL))
                {
                    status = "complete";
                    dicResponse.Add("url", _regSuccessURL);
                    dicResponse.Add("IsFacebookRegistration",
                                    _wizard.RegistrationScenario.IsFacebookRegistration.ToString());
                }
            }

            dicResponse.Add("status", status);
            dicResponse.Add("refreshSearchResults", refreshSearchResults.ToString().ToLower());

            Response.Clear();
            Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse));
            Response.End();
        }

        /// <summary>
        /// If media load balancer is enabled, traffic from media to reg pages will be distributed evenly between several versions of registration
        /// </summary>
        protected void MediaTrafficLoadBalancer()
        {
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_MEDIA_TRAFFIC_LOAD_BALANCER", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {

                if (!string.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_NAME_PRM]))
                {
                    //JDIL PRM 24/7 girls
                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL &&
                        (Request.QueryString[WebConstants.URL_PARAMETER_NAME_PRM] == "80056" ||
                        Request.QueryString[WebConstants.URL_PARAMETER_NAME_PRM] == "80057" ||
                        Request.QueryString[WebConstants.URL_PARAMETER_NAME_PRM] == "80192"))
                    {
                        return;
                    }

                    Random rnd = new Random();
                    int numberOfVersions = 2;

                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                    {
                        numberOfVersions = 3;
                    }

                    int regVersion = rnd.Next(numberOfVersions);
                    string host = HttpContext.Current.Request.Url.Host;
                    string targetURL = "http://" + host;

                    switch (regVersion)
                    {
                        // Media - OPR  
                        case 0:
                            targetURL += "/?regabtest=opr"; break;
                        // Media – Default registration  
                        case 1:
                            targetURL += "/Applications/Registration/Registration.aspx?media=1"; break;
                        // Media – Overlay
                        case 2:
                            targetURL += "/Applications/Membersonline/membersonline.aspx?regoverlay=true&media=1"; break;
                        default:
                            targetURL = string.Empty;
                            break;
                    }

                    if (targetURL != string.Empty)
                    {
                        g.Transfer(targetURL);
                    }
                }
            }
        }

        protected void RedirectToNewRegSiteBasedOnSetting()
        {
            //only redirect if enabled AND this is the reg pag, otherwise this will pick up splash page redirection as well

            if (g.AppPage.PageName.ToLower().Contains("registration") &&
                SettingsManager.GetSettingBool(SettingConstants.REGISTRATION_USE_NEW_SITE, g.Brand))
            {
                bool qsAppended = false;
                StringBuilder regURL = new StringBuilder();

                regURL.Append(SettingsManager.GetSettingString(SettingConstants.NEW_REG_SITE_URL_WITH_REGISTRATION_PATH, g.Brand));

                if (Request.UrlReferrer != null && !Request.UrlReferrer.ToString().ToLower().Contains(g.Brand.Uri.ToLower()))
                {
                    regURL.Append(regURL.ToString().Contains("?") ? "&" : "?");
                    regURL.Append(WebConstants.URL_REFERRER_TO_PASS + "=" + Server.UrlEncode(Request.UrlReferrer.AbsoluteUri) + "&");
                    qsAppended = true;
                }

                if (Request.QueryString.Count > 0)
                {
                    if (!qsAppended)
                    {
                        regURL.Append(regURL.ToString().Contains("?") ? "&" : "?");
                    }
                    
                    foreach (var key in (Request.QueryString.AllKeys))
                    {
                        regURL.Append(key + "=" + Request.QueryString[key] + "&");
                    }
                }

                Response.Redirect(regURL.ToString());
            }
        }


        /// <summary>
        /// A validator error message that should be sent back to the client for display in JSON format
        /// </summary>
        private class InputError
        {
            /// <summary>
            /// Gets or sets the id.
            /// </summary>
            /// <value>The id of the div element surrounding the input field</value>
            public string id { get; set; }
            /// <summary>
            /// Gets or sets the MSG.
            /// </summary>
            /// <value>The error message to be displayed</value>
            public string msg { get; set; }
        }
    }
}
