﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Applications.Registration.Controls;
using Matchnet.Web.Applications.Registration.Templates;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Enumerations;
using Matchnet.Web.Framework.HTTPContextWrappers;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.Registration
{
    public partial class RegistrationOverlay : RegistrationBase
    {

        //public const string COOKIE_NAME = "REG091202";

        private Step _regStep;
        private bool _loadNewWizard;
        private const string OMNITURE_SCRIPT = "var s2 = s_gi(\"{0}\"); s2.events = \"{1}\"; s2.eVar48=\"{2}\";  ";
        private int _clientStepID = 0;
        private string _attributesNamesStep1 = string.Empty;
        private PlaceHolder ContentPanel
        {
            get
            {

                return phContent;

            }
        }

        public bool IsSplash { get; set; }

        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                RedirectToNewRegSiteBasedOnSetting();

                MediaTrafficLoadBalancer();

                jQueryValidation = new JQueryValidation();

                _isOverlayReg = true;

                g.Session.Add("GalleryView", "true", SessionPropertyLifetime.Temporary);
                try
                {
                    __debugTrace = Conversion.CBool(System.Configuration.ConfigurationSettings.AppSettings.Get("DebugTrace"));
                }
                catch (Exception exc) { }
                LoadStep();

                RegistrationReason.AddDirectLinkToSession(g);

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {


                if (!IsPostBack)
                {
                    // Set the timeout for the overlay layout
                    string timeoutInSeconds =
                        Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("REG_OVERLAY_POPUP_SECONDS",
                                                                                          _g.Brand.Site.Community.
                                                                                              CommunityID,
                                                                                          _g.Brand.Site.SiteID).Trim();
                    string overlayTimout = string.Format("var overlayTimeout={0}000;", timeoutInSeconds);
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "overlayTimout", overlayTimout, true);
                }

                if (!string.IsNullOrEmpty(Request["stepID"]))
                {
                    int.TryParse(Request["stepID"], out _clientStepID);
                }

                _wizard.StepID = _clientStepID + 1;

                if (!string.IsNullOrEmpty(Request.QueryString["checkstep"]))
                {
                    SubmitStep();
                    Step currentStep = _wizard.RegistrationScenario.Steps[_clientStepID];
                    ReturnJSONResponse(currentStep != null ? currentStep.OnCompleteRefreshSearchResults : false);
                }

                if (_wizard.RegistrationStep.MBox)
                {
                    PlaceHolderOmnitureMboxRegBegin.Visible = true;
                    litmboxPageName.Text = _wizard.RegistrationStep.MBoxPageName;
                    litmboxEVar.Text = _wizard.RegistrationStep.MBoxEVar;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            try
            {
                string pageName = !string.IsNullOrEmpty(_wizard.RegistrationScenario.PageName) ? _wizard.RegistrationScenario.PageName : String.Format(OMNITURE_PAGE_NAME, _wizard.RegistrationScenario.Name, _wizard.StepID, _attributesNamesStep1, _wizard.RegistrationScenario.Name);
                string registrationEvent = "";

                //omniture can be set on splash page
                if (_setOmniture)
                {
                    if (String.IsNullOrEmpty(g.Session.GetString("OMNITURE_REGISTRATION_BEGIN_EVENT")))
                    {
                       // registrationEvent = "event7";
                        registrationEvent = "";
                        g.Session.Add("OMNITURE_REGISTRATION_BEGIN_EVENT", "true", SessionPropertyLifetime.Temporary);
                        _wizard.StartStepID = 0;

                    }

                    _g.AnalyticsOmniture.PageName = pageName;
                    if (string.IsNullOrEmpty(Request.QueryString["media"]))
                    {
                        _g.AnalyticsOmniture.AddEvent(registrationEvent);
                        if (!string.IsNullOrEmpty(_g.Session[EVAR43_SESSION_KEY]))
                        {
                            _g.AnalyticsOmniture.Evar43 = _g.Session[EVAR43_SESSION_KEY];
                            _g.Session.Remove(EVAR43_SESSION_KEY);
                        }
                    }
                    else
                    {
                        // evar43 was not set in splash, set it here
                        _g.AnalyticsOmniture.Evar43 = _wizard.RegistrationScenario.EVar43;
                    }

                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "OmnitureRegStartEvent", "var omnitureRegStartEvent='" + registrationEvent + "';", true);
                    _g.AnalyticsOmniture.Evar48 = _wizard.RegistrationScenario.Name;
                    //if (g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.JDate)11
                    //    _g.AnalyticsOmniture.Evar47 = "";

                    //Registration link from Q/A Gate page
                    if (!String.IsNullOrEmpty(Request.QueryString["PRM"]))
                    {
                        if (Request.QueryString["PRM"] == "66799")
                        {
                            _g.AnalyticsOmniture.Evar20 = "Q&A Gate Page:Registration";
                        }
                    }
                    else
                    {

                        if (!String.IsNullOrEmpty(Request["RegistrationGUID"]))
                        {
                            _g.AnalyticsOmniture.PRM = "Registration Recapture";
                        }
                    }
                }

                string omniScript = String.Format(OMNITURE_SCRIPT, g.AnalyticsOmniture.S_AccountName, registrationEvent, _wizard.RegistrationScenario.Name);
                this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), "Omniture", omniScript, true);

                Page.ClientScript.RegisterStartupScript(this.GetType(), "jqueryvalidation",
                                                        jQueryValidation.GetValidationJSCode(), true);

                for (int k = 0; k < _regScenario.Steps.Count; k++)
                {
                    for (int i = 0; i < _regScenario.Steps[k].Controls.Count; i++)
                    {
                        IAttributeControl attribute =
                            (IAttributeControl)
                            _templateControl.FindControl("id" + _regScenario.Steps[k].Controls[i].Name);
                        if (attribute != null)
                        {
                            if (k + 1 != _wizard.StepID)
                            {
                                //attribute.SetVisible(false);
                            }
                            else
                            {
                                if (attribute is IJQValidation)
                                {
                                    jQueryValidation.AddValidationRules(((IJQValidation)attribute).GetValidationRules);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write(ex.ToString());
                System.Diagnostics.EventLog.WriteEntry("WWW", "Err:\r\n" + ex.ToString() + "\r\nStack Trace" + ex.StackTrace);
                g.ProcessException(ex);
            }
        }





        private void LoadStep()
        {
            try
            {

                //not really
                _controls = new List<AttributeControl>();
                // Added "091202" to the name of the cookie to ignore the existing values.

                _wizard = new RegistrationWizard(g.Brand, RegistrationFlowType.Overlay);

                //_wizard = new RegistrationWizard(g, COOKIE_NAME, _templatePath, _templatePath + "." + g.Brand.Site.SiteID.ToString());
                if (RegistrationCapture.IsRegistrationCaptureEnabled(g) && !Conversion.CBool(g.Session.Get("REGISTRATION_POPULATED")))
                {
                    FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.LoadStep", "RegistrationCaptureEnabled", null);
                    if (!String.IsNullOrEmpty(Request["RegistrationGUID"]))
                    {
                        FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.LoadStep", "PopulateWizardFromEvent:" + Request["RegistrationGUID"], null);
                        RegistrationCapture.PopulateWizardFromEvent(Request["RegistrationGUID"], _wizard.Name, g);
                        g.Session.Add("REGISTRATION_POPULATED", "true", SessionPropertyLifetime.Temporary);
                    }


                }

                _wizard.DebugTrace = __debugTrace;
                FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.LoadStep", "Create wizard", null);
                _wizard.Load(_loadNewWizard);
                FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.LoadStep", "Load wizard", null);
                if (!Page.IsPostBack)
                {
                    _wizard.CacheRequest(new CurrentRequest());
                }

                FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.LoadStep", "PersistToCookie", "PromotionID=" + PromotionID.ToString());

                _wizard.Persistence["prm"] = PromotionID.ToString();
                if (!String.IsNullOrEmpty(Request["ScenarioName"]))
                {
                    _wizard.ScenarioName = Server.UrlDecode(Request["ScenarioName"]);
                }
                else
                {
                    if (!String.IsNullOrEmpty(Request["ScenarioID"]))
                    {
                        _wizard.ScenarioName = "Scenario " + Server.UrlDecode(Request["ScenarioID"]);
                    }
                }

                _wizard.LoadRegistrationSteps(true);

                _wizard.Persist();

                _regScenario = _wizard.RegistrationScenario;

                // MPR-1734
                if (g.LayoutTemplate == Matchnet.Content.ValueObjects.PageConfig.LayoutTemplate.WideReset && _regScenario.Blank == true)
                {
                    g.Head20.IsBlank = true;
                    ((Matchnet.Web.LayoutTemplates.WideReset)g.LayoutTemplateBase).IsBlank = true;
                    // Footer20 depends on g.Head20.IsBlank 's value.
                }

                FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.LoadStep", "Wizard and template loaded", _wizard.ToString());

                SetTemplate();
                for (int k = 0; k < _regScenario.Steps.Count; k++)
                {
                    int controlsCreated = 0;

                    FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.LoadStep", "Loading Step", _wizard.ToString());

                    //txtTitle.Text = GetResourceString(_wizard.RegistrationStep.ResourceConstant, ResourceControl);
                    _templateControl.StartStep("step_" + k, "step", _regScenario.Steps[k].Name, _regScenario.Steps[k].ResourceConstant, _regScenario.Steps[k].DontSkip);
                    _attributesNames = string.Empty;

                    for (int i = 0; i < _regScenario.Steps[k].Controls.Count; i++)
                    {

                        FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.LoadStep", "Loading Controls for Step", "StepID=" + k.ToString() + ", ControlID=" + i.ToString());
                        bool show = true;
                        AttributeControl ctrl = (AttributeControl)_regScenario.Steps[k].Controls[i];

                        //ctrl.LoadFromCookie(_wizard.PersistCookie);
                        ctrl.LoadFromPersistence(_wizard.Persistence);
                        if (k + 1 != _wizard.RegistrationStep.ID)
                            show = false;
                        else
                        {
                            show = true;

                        }
                        FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.LoadStep", "Loading Controls for Step", "StepID=" + k.ToString() + "ControlID=" + i.ToString() + ", " + ctrl.ToString());

                        IAttributeControl attrcontrol = _templateControl.Add(ctrl, show, true);
                        if (ExecuteDisplayCondition(ctrl))
                        {
                            //_templateControl.ResourceControl = ResourceControl;

                            attrcontrol.ResourceControl = ResourceControl;
                            // litScript.Text = litScript.Text + "\r\n" + attrcontrol.ScriptBlock();

                            ctrl.OnePageReg = _regScenario.IsOnePageReg;

                            _controls.Add(ctrl);
                            controlsCreated += 1;
                        }
                        else
                        {
                            attrcontrol.ResourceControl = ResourceControl;
                            // litScript.Text = litScript.Text + "\r\n" + attrcontrol.ScriptBlock();

                            _controls.Add(ctrl);

                        }

                        if (!String.IsNullOrEmpty(_regScenario.Steps[k].Controls[i].AttributeName))
                            _attributesNames += _regScenario.Steps[k].Controls[i].AttributeName;
                        else
                            _attributesNames += _regScenario.Steps[k].Controls[i].Name;
                        if (i < _regScenario.Steps[k].Controls.Count - 1)
                        {
                            _attributesNames += ",";
                        }
                    }
                    if (k == 0)
                    {
                        _attributesNamesStep1 = _attributesNames;
                    }

                    string pageName = !string.IsNullOrEmpty(_wizard.RegistrationScenario.PageName) ? _wizard.RegistrationScenario.PageName : String.Format(OMNITURE_PAGE_NAME, _wizard.RegistrationScenario.Name, k + 1, _attributesNames, _wizard.RegistrationScenario.Name);

                    _templateControl.EndStep(_regScenario.Steps[k].TipResourceConstant, k, pageName);

                    if (controlsCreated == 0 && _wizard.ConditionalSkipSteps != null && String.IsNullOrEmpty(_regScenario.Steps[k].NextStepURL))
                    {
                        _wizard.ConditionalSkipSteps.Add(k + 1);
                    }

                }


                _templateControl.Wizard = _wizard;

                ContentPanel.Controls.Add(_templateControl);

                if (_wizard.SkipStep(_wizard.StepID))
                {
                    if (_wizard.StepID + 1 < _wizard.RegistrationScenario.Steps.Count)
                    {
                        _wizard.StepID += 1;
                        //_wizard.PersistToCookie();
                        _wizard.Persist();
                    }

                }
                //SetTemplateNavigation();

            }
            catch (Exception ex)
            {
                string x = ex.ToString();
                g.ProcessException(ex);
            }
        }

        public bool ExecuteDisplayCondition(AttributeControl ctrl)
        {

            if (String.IsNullOrEmpty(ctrl.DisplayConditionName))
            { return true; }

            string val = _wizard.GetProperty(ctrl.DisplayConditionName.ToLower());


            if (val.ToLower() == ctrl.DisplayConditionValue.ToLower())
            { return true; }
            else { return false; }

        }

        private void SubmitStep()
        {
            bool skipStep = false;
            Step currentStep = _wizard.RegistrationScenario.Steps[_clientStepID];
            try
            {
                bool allValid = true;

                allValid = Valid();
                bool completed = false;
                if (allValid && string.IsNullOrEmpty(Request.QueryString["ValidateOnly"]))
                {
                    FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.SubmitStep", "All controls are valid", _wizard.ToString());
                    if (String.IsNullOrEmpty(_wizard.RegistrationStep.NextStepURL))
                    {
                        if (_wizard.CompletedSteps < _wizard.StepID)
                            _wizard.CompletedSteps += 1;
                        if (_wizard.StepID < _regScenario.Steps.Count)
                        {
                            _wizard.StepID += 1;

                        }
                        else
                            completed = true;

                        if (_wizard.SkipStep(_wizard.StepID))
                        {
                            skipStep = true;
                            if (_wizard.StepID + 1 > _wizard.RegistrationScenario.Steps.Count)
                                completed = true;
                            else
                            {
                                _wizard.StepID += 1;
                            }

                        }
                        FrameworkGlobals.Trace(__debugTrace, "RegistrationWizardOverlay.LoadRegistrationSteps ", "About to persist to cookie", _wizard.ToString());

                        _wizard.GetRegistrationPersistenceValue("emailaddress");
                        string email = _wizard.GetRegistrationPersistenceValue("emailaddress");
                        if (!String.IsNullOrEmpty(email) && RegistrationCapture.IsRegistrationCaptureEnabled(g))
                        {
                            FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.SubmitStep", "UpdateRegistrationEventFromCookie, email - " + email, null);
                            RegistrationCapture.UpdateRegistrationEventFromPersistence(_wizard.RegistrationEvent, _wizard.ExcludedFields, _wizard.Persistence);
                            FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.SubmitStep", "SaveRegistrationEvent, email - " + email, null);
                            RegistrationCapture.SaveRegistrationEvent(_wizard.RegistrationEvent, _wizard.Persistence);

                        }
                        else
                        {
                            FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.SubmitStep", "Either email is empty or regcapture is not enabled, email - " + email, null);
                        }
                        //_wizard.PersistToCookie();
                        _wizard.Persist();
                    }
                    else
                    {
                        string url = _wizard.RegistrationStep.NextStepURL;
                        if (_controls.Count > 0)
                        {
                            _wizard.CompletedSteps = _wizard.StepID;
                            _wizard.StepID = _wizard.StepID + 1;
                            _wizard.StartStepID = _wizard.StepID;
                        }
                        _wizard.ScenarioName = "";

                        //_wizard.ScenarioFile = "";
                        FrameworkGlobals.Trace(__debugTrace, "RegistrationWizardOverlay.LoadRegistrationSteps", "NextStepURL empty", _wizard.ToString());
                        _wizard.Persist();
                        g.Transfer(url);

                    }
                    if (completed)
                    {
                        //need to update controls for the last step
                        FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.SubmitStep", "Registration last step", _wizard.ToString());
                        int totalControls = _controls.Count;
                        int laststepControls = currentStep.Controls.Count;

                        int indLastStepControls = totalControls - laststepControls - 1;
                        if (indLastStepControls < 0)
                        {
                            indLastStepControls = 0;
                        }

                        if (indLastStepControls > 0 || _wizard.RegistrationScenario.Steps.Count == 1)
                        {
                            for (int i = indLastStepControls; i < totalControls; i++)
                            {
                                //_controls[i].LoadFromCookie(_wizard.PersistCookie);
                                _controls[i].LoadFromPersistence(_wizard.Persistence);
                            }
                        }
                        FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.SubmitStep", "Registration last step - RegisterNewUser", _wizard.ToString());

                        int mid = RegisterNewUser(_wizard);
                        if (mid > 0)
                        {
                            FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.SubmitStep", "Registration last step - RegisterNewUser - success", _wizard.ToString());
                            Member.ServiceAdapters.Member member = g.Member;
                            FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.SubmitStep", "Registration last step - SaveMember", _wizard.ToString());
                            SaveMember(member);
                            // Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);


                        }
                        else
                        {
                            /*    _templateControl.ValidationMessageText.Text = g.GetResource("ERR_SYSTEM", this); ;
                                _templateControl.ValidationMessageText.Visible = true;
                             * */
                            AddErrorMessage(g.GetResource("ERR_SYSTEM", this), _lastContainerDivClientID);
                            if (skipStep)
                                _wizard.StepID -= 1;

                        }

                    }
                    else
                    {
                        // _templateControl.ButtonNext.Visible = false;
                    }
                }

            }
            catch (System.Threading.ThreadAbortException ex)
            {
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.SubmitStep", "Exception: " + ex.ToString(), null);
                /*_templateControl.ValidationMessageText.Text = g.GetResource("ERR_SYSTEM", this); ;
                _templateControl.ValidationMessageText.Visible = true;*/

                AddErrorMessage(g.GetResource("ERR_SYSTEM", this), _lastContainerDivClientID);
                if (skipStep)
                    _wizard.StepID -= 1;

                _clientIDErrorMessage.Add("_ctl0__ctl5__ctl0_idTermsAndConditions_divControl", g.GetResource("ERR_SYSTEM", this));
            }

        }

        private bool Valid()
        {
            bool allValid = true;
            Step currentStep = _wizard.RegistrationScenario.Steps[_clientStepID];

            for (int i = 0; i < currentStep.Controls.Count; i++)
            {

                FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.Valid", "Validating Controls for Saving Step", "StepID=" + _wizard.RegistrationStep.ToString() + ", ControlID=" + i.ToString());

                AttributeControl ctrl = currentStep.Controls[i];


                IAttributeControl attr = (IAttributeControl)_templateControl.ContentPlaceholder.FindControl("id" + currentStep.Controls[i].Name);

                if (attr != null)
                {
                    string val = attr.GetValue();
                    bool valid = attr.Validate();
                    FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.Valid", "Validating Control, valid=" + valid.ToString(), ctrl.ToString());
                    //attr.PersistToCookie(_wizard.PersistCookie);
                    attr.Persist(_wizard.Persistence);
                    if (valid)
                    {

                        if (!String.IsNullOrEmpty(ctrl.OnSubmit))
                            valid = ExecuteOnSubmit(ctrl, attr);
                    }
                    else
                    {
                        _clientIDErrorMessage.Add(attr.ContainerDivClientID, attr.ValidationMessage.Text);
                    }

                    _lastContainerDivClientID = attr.ContainerDivClientID;
                    allValid = allValid & valid;
                }
            }

            return allValid;
        }

        public bool ExecuteOnSubmit(AttributeControl ctrl, IAttributeControl control)
        {
            bool validated = true;
            if (ctrl.OnSubmit == "ValidateEmail")
            {
                validated = ValidateEmail(control.GetValue(), control);

            }
            else if (ctrl.OnSubmit == "ValidateDOB")
            {
                validated = ValidateBirthDate(control.GetValue(), control);
            }
            else if (ctrl.OnSubmit == "ValidateUsername")
            {
                validated = ValidateUsername(control.GetValue(), control);
            }
            return validated;


        }

        private bool ValidateEmail(string email, IAttributeControl control)
        {
            try
            {
                int id = MemberSA.Instance.GetMemberIDByEmail(email);


                if (id > 0)
                {
                    string errEmailExistsResourceConstant = "ERR_EMAIL_EXISTS";

                    //if (!string.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION]))
                    //if (_isFacebookReg)
                    //{
                    //    errEmailExistsResourceConstant += "_FB";
                    //}
                    /*
                    if (_templateControl.ValidationMessageText == null)
                        regValidation.Text = g.GetResource(errEmailExistsResourceConstant, this);
                    else
                    {
                        _templateControl.ValidationMessageText.Text = g.GetResource(errEmailExistsResourceConstant, this);
                        _templateControl.ValidationMessageText.Visible = true;
                    }
                     * */

                    _clientIDErrorMessage.Add(control.ContainerDivClientID, g.GetResource(errEmailExistsResourceConstant, this));
                    return false;
                }

                DoNotEmailEntry entry = DoNotEmailSA.Instance.GetEntryBySiteAndEmailAddress(g.Brand.Site.SiteID, email);
                if (entry != null)
                {
                    /*
                    if (_templateControl.ValidationMessageText == null)
                        regValidation.Text = g.GetResource("ERR_DNE", this);
                    else
                    {
                        _templateControl.ValidationMessageText.Text = g.GetResource("ERR_DNE", this);
                        _templateControl.ValidationMessageText.Visible = true;
                    }
                     * */

                    _clientIDErrorMessage.Add(control.ContainerDivClientID, g.GetResource("ERR_DNE", this));

                    return false;
                }
                else
                    return true;
            }
            catch (Exception ex)
            {
                FrameworkGlobals.Trace(__debugTrace, "RegistrationOverlay.ValidateEmail", "Exception:" + ex.ToString(), null);
                /*_templateControl.ValidationMessageText.Text = g.GetResource("ERR_SYSTEM", this); ;
                _templateControl.ValidationMessageText.Visible = true;*/

                _clientIDErrorMessage.Add(control.ContainerDivClientID, g.GetResource("ERR_DNE", this));

                g.ProcessException(ex);
                return false;
            }
        }

        private bool ValidateBirthDate(string dob, IAttributeControl ctrl)
        {
            DateTime dateofbirth = Conversion.CDateTime(dob);

            if (FrameworkGlobals.GetAge(dateofbirth) < 18)
            {
                /*    ctrl.ValidationMessage.Text = g.GetResource("ERR_SHOULD_BE_18", this);
                    ctrl.ValidationMessage.Visible = true;*/

                _clientIDErrorMessage.Add(ctrl.ContainerDivClientID, g.GetResource("ERR_SHOULD_BE_18", this));


                return false;
            }
            else
            {
                return true;
            }

        }


        private bool ValidateUsername(string username, IAttributeControl control)
        {
            try
            {
                ArrayList results = null;
                int totalRows = 0;
                const int pageSize = 10;
                int startRow = 0;
                bool isFound = false;

                while (!isFound && (startRow == 0 || (startRow * pageSize < totalRows)))
                {
                    results = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMembersByUserName(username, startRow, pageSize, ref totalRows);

                    if (null != results && results.Count > 0)
                    {
                        foreach (Matchnet.Member.ValueObjects.CachedMember member in results)
                        {
                            Member.ServiceAdapters.Member mem = MemberSA.Instance.GetMember(member.MemberID, MemberLoadFlags.None);
                            if (mem != null)
                            {
                                if (mem.GetUserName(g.Brand) == username)
                                {
                                    isFound = true;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        break;
                    }

                    startRow++;
                }

                if (isFound)
                {
                    string errEmailExistsResourceConstant = "USERNAME_IN_USE";
                    _clientIDErrorMessage.Add(control.ContainerDivClientID, string.Format(g.GetResource(errEmailExistsResourceConstant, this), username));
                    return false;
                }
                else
                {
                    return true;
                }

            }
            catch (Exception ex)
            {

                _clientIDErrorMessage.Add(control.ContainerDivClientID, g.GetResource("ERR_DNE", this));

                g.ProcessException(ex);
                return false;
            }
        }

        private void RedirectToNewRegSiteBasedOnSetting()
        {
            //only redirect if enabled AND this is the reg pag, otherwise this will pick up splash page redirection as well

            if ((g.AppPage.PageName.ToLower().Contains("registration") || SettingsManager.GetSettingBool(SettingConstants.REGISTRATION_USE_NEW_SITE_FOR_ALL_PAGES, g.Brand)) &&
                SettingsManager.GetSettingBool(SettingConstants.REGISTRATION_USE_NEW_SITE, g.Brand))
            {
                bool qsAppended = false;
                StringBuilder regURL = new StringBuilder();

                regURL.Append(SettingsManager.GetSettingString(SettingConstants.NEW_REG_SITE_OVERLAY_URL_WITH_REGISTRATION_PATH, g.Brand));

                if (Request.UrlReferrer != null && !Request.UrlReferrer.ToString().ToLower().Contains(g.Brand.Uri.ToLower()))
                {
                    regURL.Append(regURL.ToString().Contains("?") ? "&" : "?");
                    regURL.Append(WebConstants.URL_REFERRER_TO_PASS + "=" + Server.UrlEncode(Request.UrlReferrer.AbsoluteUri) + "&");
                    qsAppended = true;
                }

                if (Request.QueryString.Count > 0)
                {
                    if (!qsAppended)
                    {
                        regURL.Append(regURL.ToString().Contains("?") ? "&" : "?");
                    }

                    foreach (var key in (Request.QueryString.AllKeys))
                    {
                        regURL.Append(key + "=" + Request.QueryString[key] + "&");
                    }
                }

                Response.Redirect(regURL.ToString());
            }
        }

    }
}
