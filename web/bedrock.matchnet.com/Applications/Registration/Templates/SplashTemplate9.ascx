﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashTemplate9.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Templates.SplashTemplate9" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript">window.jQuery || document.write('<script type="text/javascript" src="/javascript20/library/jquery-1.4.2.min.js">\x3C/script>')</script>
<div id="min-max-container">
	<div id="header" class="clearfix">
		<div id="header-logo">
			<p class="header-message"><mn:image id="imgLogo" runat="server" titleResourceConstant="TTL_SITE_LOGO" ResourceConstant="ALT_SITE_LOGO" FileName="trans.gif" NavigateURLClass="logo" />Welcome to JDate, the premier Jewish singles community online. Connect with thousands of members in your area and around the world - photos, email, chat, IM and more.</p>
		</div>
		<ul id="header-nav">
			<li><a href="http://www.jdate.co.il/">אתר הכרויות</a></li>
			<li><a href="http://www.jdate.fr/">Rencontres Juives</a></li>
		</ul>
		<div id="header-login"><a href="/Applications/Logon/Logon.aspx">Member Login</a></div>
	</div>
	<div id="content-container">
		<div id="content-main" class="clearfix">
			<div id="rotator">
					<uc1:ImageRotator id="imageRotator" runat="server"  ResourceConstant="IMG_DATA_FILE"/>
			</div>
			<div id="marketing-wrap">
				<big>Meet Jewish Singles</big>
				<ul id="splash-bullets">
					<li>Initiate contact for FREE</li>
					<li>View photos of singles near you</li>
					<li>Connect with <a href="/Applications/MembersOnline/MembersOnline.aspx" class="mol-count"><mn:Txt ID="txtMOLCount" runat="server" /></a> members online now!</li>
				</ul>
				<div id="cta">
					<a href="/Applications/Registration/Registration.aspx" class="link-primary">BROWSE FOR FREE</a>
				</div>
			</div>
		</div>
		<div id="vis-info" class="clear-both editorial">
			<h1>Welcome to JDate, the Premier Jewish Community Online for Dating Jewish Singles</h1>
			<p>JDate has been building the Jewish community for more than a decade, so it's not surprising that it seems like everyone knows someone who fell in love on JDate and it's no wonder why countless marriages begin right here!  Each week, hundreds of JDaters meet their soul mates. To see why JDate is the undisputed leader in Jewish dating and the modern alternative to traditional Jewish matchmaking, just take a look at our <a href="http://www.jdate.com/jmag/category/successstories/">Success Stories</a> section.</p>
			<h2>JDate is Where Tens of Thousands of Jewish Singles Interact Every Day!</h2>
			<p>JDate is an ideal destination for Jewish men and Jewish women to make connections, and find friends, dates and soul mates, all within the faith. With hundreds of thousands of members, fun and easy online features, fantastic offline activity options (including, travel and events), JDate is the number one place for Jewish romance in the&nbsp;world!</p>
			<p>Create your profile now and, in minutes, you'll be ready to start communicating with the JDaters who live near you. After you join, check out our <a href="/Applications/Video/VideoCenter.aspx">JDate Videos</a>, our <a href="http://static.jdate.com/microsites/jewishholidays/">Jewish Holiday Calendar</a>, our <a href="http://static.jdate.com/microsites/synagoguedirectory/default.asp">Synagogue Directory</a>, our online magazine, <a href="http://www.jdate.com/jmag/">Jewish Magazine</a> and <a href="http://www.jdate.com/blog/">blog</a>, where you can follow real-life stories of JDaters in <a href="http://search.jdate.com/California/LosAngeles/">Los Angeles</a>, <a href="http://search.jdate.com/NewYork/NewYork/">New York City</a> and much, much more.</p>
			<p>Register and post your FREE JDate profile today. On JDate, your beshert is just a click away.</p>
			<h2>JDate's Mission - Building the Jewish Community for Over a Decade </h2>
			<p>JDate's mission is to strengthen the Jewish community and ensure that Jewish traditions are sustained for generations to come. To accomplish this mission, we provide a global network where Jewish singles can meet to find friendship, romance and life-long partners within the Jewish faith.</p>
			<p>Here at JDate we are proud of our Jewish traditions and values and are therefore not only deeply committed to our support for Israel and Jewish cultural programs throughout the world, but also supportive of charitable non-profit organizations of all faiths.</p>
			<p>Since 1997, JDate has been growing the Jewish community one success story at a time, forming countless relationships and ultimately, creating Jewish families. The most gratifying part of our jobs here at JDate are the thousands of phone calls, emails and letters we receive every year from JDate success stories thanking us for connecting them with their beshert. We never tire of hearing stories and testimonies from Jews all over the world about how JDate has influenced their lives and, in the process, helped build the Jewish community.</p>
		</div>
	</div>
</div>

<div id="footer">
	<div id="footer-content">
		<mn1:Footer20 ID="Footer20" Runat="server" />
	</div>
</div>




