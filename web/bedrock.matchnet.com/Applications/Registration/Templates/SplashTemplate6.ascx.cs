﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Applications.Registration.Controls;

namespace Matchnet.Web.Applications.Registration.Templates
{
    public partial class SplashTemplate6 : RegistrationTemplateBase, IRegistrationTemplate
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            txtTitle.Text = GetResourceString(TitleResource, ResourceControl);
            btnContinue.ResourceControl = ResourceControl;

            //imageRotator.ResourceControl = this;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("AMADESA_SITE_ENABLE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)) ||
               Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("DISABLE_SPLASH_LIVE_EXPERIENCE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
            {
                pnlJSCta.Visible = false;
                pnlJSHeadline.Visible = false;
            }
        }

         

        #region IRegistrationTemplate Members

        public override PlaceHolder ContentPlaceholder { get { return phContent; } }

        #endregion
    }
}