﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Template1.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Templates.Template1" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="rc" Namespace="Matchnet.Web.Applications.Registration.Controls" Assembly="Matchnet.Web" %>
<%@ Register src="/Framework/Ui/BasicElements/GalleryMiniProfile20.ascx" tagname="GalleryMiniProfile20" tagprefix="uc2" %>
<%@ Register TagPrefix="uc2" TagName="ProgressBar" Src="/Framework/Ui/BasicElements/ProgressBar.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="fb" TagName="Registration" Src="/Applications/Registration/Controls/FacebookRegistration.ascx" %>

<asp:PlaceHolder runat="server" ID="templateStylesheet" Visible="true">
    <link rel="stylesheet" type="text/css" href="" runat="server" id="structStyleSheet" />
    <link rel="stylesheet" type="text/css" href="" runat="server" id="styleStyleSheet" />
</asp:PlaceHolder>

<script type="text/javascript">
//iOS detection
var spark_iOS = false;
if (navigator.userAgent.match(/iP(ad|hone|od)/i)) {
    spark_iOS = true;
}

if(spark_iOS){
    $j(function(){
        spark_fixIOSSelectBug($j('select'));
    });
    function Anthem_PostCallBack() {
        spark_fixIOSSelectBug($j('select'));
    }
}

function listboxAutoClick(sender) {
//    if (navigator.userAgent.match(/iP(ad|hone|od)/i)) {
//        return false;
//    }
//    else{
//        document.getElementById('<%= btnContinue.ClientID %>').click();
//    }
}
</script>

    <div id="min-max-container">
   
        <div id="header" class="clearfix" >
            <asp:PlaceHolder runat="server" ID="NavTopHeader" Visible="true">

                <div id="header-contentainer" class="clearfix">
                    <div id="header-logo">
		                <p class="header-message"><mn:image id="imgLogo" runat="server" CssClass="logo" DefaultTitleResourceConstant="ALT_SITE_LOGO" DefaultResourceConstant="ALT_SITE_LOGO" FileName="trans.gif" /></p>
                    </div>
                    <div class="header-login" id="headerLogin" runat="server"><a href="/Applications/Logon/Logon.aspx"><mn:Txt ID="txtLogon" runat="server" ResourceConstant="TXT_LOGON" /></a></div>
                    <ul class="header-nav" id="ulJdateSiteLinks" runat="server">
                        <li id="liAlreadyAMember" runat="server" class="header-nav-filler"><mn:Txt ID="txt3" runat="server" ResourceConstant="TXT_ALREADY_MEMBER" /></li>
                    </ul>
                </div>

            </asp:PlaceHolder>
        </div>
        
        <div id="content-container" class="one-column your-profile clearfix">

                                
<div id="content-main" <asp:Literal id="litCntMain" runat="server" />>

   <div class="content-main clearfix">
    <h1><mn:Txt ID="txtTitle" runat="server" /></h1>
    <mn:Txt ID="txtAdditionalHtml" runat="server" />
    <fieldset class="primary-content <asp:Literal id='litCSSClass' runat=server/>" >
        <asp:PlaceHolder ID="phContent" runat="server">

        <fb:Registration runat="server" ID="fbReg"/>


            <uc1:ValidationMessage runat="server" ID="txtValidation" Visible=false />
        </asp:PlaceHolder>
        <div class="text-center margin-medium cta">
            <mn2:FrameworkButton runat="server" ID="btnContinue" ResourceConstant="TXT_CONTINUE" CssClass="btn primary" />
        </div>
    </fieldset>
    <%--<asp:Button runat="server" ID="btnContinue" Text="Continue" CssClass="btn primary" />--%>
    
    <div class="secondary-content <asp:Literal id='litCSSSecondaryClass' runat=server/>">
       
        <div id="divMiniProfile" runat="server" class="profile-wrapper"><div class="deactivate"></div></div>
        <div class="border-gen tips" runat="server" id="divTip">
            <mn:Txt ID="txtTip" runat="server" />
        </div>
    </div>
    <!-- close secondary content -->
    <mn:txt id="txtDecorationTitle" runat="server" resourceconstant="TXT_DECORATION_TITLE" expandimagetokens="true" />
    <%--<p class="decoration-content" title="Trudy & Seth">Trudy & Seth</p>--%>
</div>
<!-- close content-main -->
<div class="vis-links">
    <uc2:ProgressBar runat="server" id="idProgress" />
    <mn2:FrameworkButton runat="server" ID="btnPrevious" ResourceConstant="TXT_PREVIOUS" Visible="false" CssClass="btn secondary prev-arrow" />
    <mn2:FrameworkButton runat="server" ID="btnNext" ResourceConstant="TXT_NEXT" Visible="false" CssClass="btn secondary next-arrow" />
</div>

                        </div>
        	        


 </div>

    </div>
    
    <div id="footer-narrow">
        <div class="footer-container">
            <mn:Copyright id="copyright" runat="server"></mn:Copyright>
            <mn:txt id="txtPrivacyStat" runat="server" resourceconstant="TXT_PRIVACY_STAT"/>    
        </div>
    </div> 