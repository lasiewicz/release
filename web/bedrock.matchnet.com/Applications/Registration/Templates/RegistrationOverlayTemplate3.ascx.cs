﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.Registration.Templates
{
    public partial class RegistrationOverlayTemplate3 : RegistrationTemplateBase, IRegistrationTemplate
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region IRegistrationTemplate Members

        PlaceHolder IRegistrationTemplate.ContentPlaceholder
        {
            get { return phContent; }
        }

        #endregion
    }
}