﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashTemplate14a.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Templates.SplashTemplate14a" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript">window.jQuery || document.write('<script type="text/javascript" src="/javascript20/library/jquery-1.4.2.min.js">\x3C/script>')</script>

<div id="min-max-container">
   <mn:Txt ID="txtMinMaxContainerContent" runat="server" ResourceConstant="TXT_MIN_MAX_CONTAINER_CONTENT" ExpandImageTokens="false" />
    <%--<div id="vis-info" class="clear-both editorial">
        <mn:Txt ID="mntxt6945" runat="server" ResourceConstant="TXT_SPLASH_SEO" ExpandImageTokens="true" />
    </div>--%>
</div>
<div id="footer">
    <div id="footer-content">
        <mn1:Footer20 ID="Footer20" Runat="server" />
    </div>
</div>
