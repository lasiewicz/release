﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SparkSplashTemplate1.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Templates.SparkSplashTemplate1" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>

<div id="min-max-container">
	<div id="header" class="clearfix">
		<div id="header-logo">
		    <mn:image id="imgLogo" runat="server" titleResourceConstant="TTL_SITE_LOGO" ResourceConstant="ALT_SITE_LOGO" FileName="trans.gif" NavigateURLClass="logo" />
		</div>
		<ul id="header-nav">
			<li><a href="/Applications/Registration/Registration.aspx">Join Free</a></li>
			<li><a href="/Applications/Logon/Logon.aspx">Member Login</a></li>
		</ul>
		<div id="gamSplashTop">
		    <mn:AdUnit ID="adSplashTop" Size="SplashTop" runat="server" GAMAdSlot="splash_top_165x86" />
		</div>
		<div class="mboxDefault">
		<p id="header-message">Online dating made easy and fun! Serious about meeting someone special? Problem solved&hellip;</p>
		</div>
		<script type="text/javascript">mboxCreate("spark_tagline");</script>
	</div>
	
	
	<div id="content-container">
		<div id="content-main" class="clearfix">
			<div id="slideshow" class="mboxDefault">
			    <mn:image id="mnimage9434" runat="server" filename="ui-splash-rotator-photo-01.jpg" titleresourceconstant="TTL_ROTATOR_PHOTO" resourceconstant="ALT_ROTATOR_PHOTO" />			    	
			</div>
			<script type="text/javascript">mboxCreate("spark_image");</script>
			<div id="marketing-wrap">
			  <div class="mboxDefault">
				<big>Communicate for Free</big>                <p>Free to flirt, read and respond to all emails!</p>
				<big>Just Be Yourself</big>                <p>Highlight your values, passions and even quirks using our Q&amp;A and Color Code personality tools.</p>
				<big>Search, Match or Both</big>                <p>You control how you'd like to search &mdash; there are no limits on Spark.com!</p>
        </div>
        <script type="text/javascript">mboxCreate("spark_content");</script>
                
        
				<div id="cta" class="mboxDefault">
					<a href="/Applications/Registration/Registration.aspx">
					<mn:image id="mnimage2944" runat="server" filename="btn-splash-reg.png" titleresourceconstant="TTL_BTN_REG" resourceconstant="ALT_BTN_REG" />
					</a>
				</div>
				<script type="text/javascript">mboxCreate("spark_button");</script>
			</div>
		</div>
		
        <div id="footer" class="clearfix">
            <div class="three-column">
	            <ul>
	                <li><big>Spark.com</big>
	                    <ul>
		                    <li><a href="/Default.aspx">Home</a></li>
		                    <li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1998&amp;HideNav=True" class="footerLink">About Spark.com</a></li>
		                    <li><a class="footerlink" href="/Applications/Logon/Logon.aspx">Login</a></li>
                            <li><a href="http://www.spark.com/tv" class="footerLink">SparkTV</a></li>
		                    <li><a href="/Applications/ContactUs/ContactUs.aspx" class="footerlink">Contact Us</a></li>
		                    <li><a href="/safety" target="_blank" class="footerLink">Safety</a></li>
		                    <li><a href="/Applications/MemberServices/MemberServices.aspx" class="footerlink">Member Services</a></li>
		                    <li><a href="/Applications/Article/FAQMain.aspx" class="footerLink">Help &amp; Advice</a></li>
		                    <li><a href="/Applications/MemberServices/VerifyEmail.aspx" class="footerlink">Verify Email</a></li>
		                    <li><a href="/Applications/Sitemap/Sitemap.aspx" class="footerLink">Site Map</a></li>
		                    <li>&nbsp;</li>
		                    <li><a href="/Applications/Subscription/Subscribe.aspx?prtid=16">Subscribe</a></li>
	                    </ul>
	                </li>
	            </ul>
	        </div>
            <div class="three-column">
	            <ul>
	                <li><big>Spark Networks&reg;</big>
	                    <ul>
		                    <li><a href="http://www.spark.net/about.htm" class="footerLink" target="_blank">About Spark Networks</a></li>
		                    <li><a href="http://www.spark.net/our-portfolio/portfolio-overview/" target="_blank">Spark Networks' Sites</a></li>
		                    <li><a href="http://www.spark.net/advertise.asp" target="_blank">Advertise With Us</a></li>
		                    <li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#privacy">Privacy</a></li>
		                    <li><a href="http://www.spark.net/investor.htm" class="footerLink" target="_blank">Investor Relations</a></li>
                            <li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#service">Terms of Service</a></li>
		                    <li><a href="http://affiliates.jdate.com/" class="footerLink" target="_blank">Affiliate Program</a></li>
                            <li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#service">Terms of Service</a></li>
		                    <li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#privacy">Privacy</a></li>
                            <li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#cookie">Cookie Policy</a></li>
		                    <li><a href="http://www.spark.net/intellectual.htm" class="footerLink">Our Intellectual Property</a></li>
		                    <li><a href="http://search.Spark.com/">Local Online Dating</a></li>
        		        </ul>
	                </li>
	            </ul>
	        </div>
        	
        </div>
        
		<div id="vis-info" class="clear-both clearfix">
		    <div class="three-column">
			    <h1>Online Dating at Spark.com</h1>
                
                <p>Dating and finding love shouldn't be difficult. That's why at the NEW Spark.com, we focus on providing the highest quality online dating experience from day one by requiring that all of our members take a short, but detailed personality test called the Color Code. We also require all members to have a photo and allow all of our members to respond to emails.</p>
                <p>At Spark.com, anyone can respond to emails. At most dating sites, non-paying members are limited to sending canned smiles or flirts but at Spark.com, singles can respond to anyone who sends them an email, making your search for love even easier.</p>
                <p>We have millions of members from all walks of life, backgrounds, professions and ages, who are looking for others to share their experiences. New singles are joining all the time and tons of connections are being made every day.</p>
                <p>Spark.com, ignite your possibilities.</p>
                
			</div>

		    <div class="three-column">
			    <h2>What Sets Us Apart as an Online Dating Service?</h2>
                <p>How is the Color Code different from other dating site personality tests? Unlike sites such as eHarmony that don't give you control over your own dating experience, the Color Code helps you learn how to build stronger and deeper relationships with the people YOU choose to meet. We call it our guided matching process and it's designed to guide you to dating success. We also make dating affordable by not charging an arm and a leg like those other guys, so you can meet thousands of quality singles on your journey for love.</p>                    <p>How does requiring members to have photos improve your dating experience? Ask any of the singles who have used other online dating sites and they'll tell you the site was filled with blank profiles, and even half of those with any information are without a photo. Studies have shown that singles with a photo on their dating profile get at least 10 times more profile views. People want to see the smiling face behind the bio and personality test.</p>                    </div>            
            <div class="three-column" id="comparison-chart">
	            <h2>Compare Spark.com against: <a href="http://www.spark.com/eHarmony/" title="eHarmony" target="_blank">eHarmony&reg;</a> | <a href="http://www.Spark.com/match/" title="Match.com" target="_blank">Match.com&reg;</a></h2>
		        <mn:image id="mnimage5661" runat="server" filename="ui-splash-comparison.gif" titleresourceconstant="" resourceconstant="" />
		        <p>When you compare Spark.com to services such as <a href="http://www.Spark.com/match/" title="Match.com" target="_blank">Match.com</a> and <a href="http://www.Spark.com/eHarmony/" title="eHarmony" target="_blank">eHarmony</a> you might ask ‘What makes us different?' or ‘Why should I join this dating site?' The answers are numerous. Lower prices: Buying from <a href="http://www.Spark.com/match/" title="Match.com" target="_blank">Match</a> or <a href="http://www.Spark.com/eHarmony/" title="eHarmony" target="_blank">eHarmony</a> will likely cost you at least $40 while Spark is significantly less. Plus, we offer more features, it's free to respond to emails, we have the Color Code Personality Test, more members with photos, and more!</p>
		        <p>Join today by creating your free profile, post up to 12 photos and soon you'll be in the thick of online dating. Once you've joined, check out our <a href="http://www.Spark.com/tipscenter/" title="Dating Advice Center">Dating Advice Center</a> to read about <a href="http://www.Spark.com/tipscenter/17.htm" title="Online Dating Safety">Online Dating Safety</a> and <a href="http://www.Spark.com/tipscenter/03.htm" title="Creating the Best Free Dating Profile">Creating the Best Free Dating Profile</a>.</p>
		        <p><a href="Applications/Registration/Registration.aspx" title="Register and post your FREE dating profile today">Register and post your FREE dating profile today</a>. </p>
		        <p class="twitter">Follow us on: <a href="http://www.twitter.com/sparkdotcom" title="twitter" target="_blank">Twitter</a></p>
            </div>
			
		</div>
	</div>
	
	
<div id="copyright">
    <p>Copyright &copy; 2012 Spark Networks&reg; USA, LLC. All rights reserved. Spark Networks USA, LLC is a wholly-owned subsidiary of Spark Networks, Inc., a NYSE MKT Company (<a target="_blank" href="http://www.nyse.com/about/listed/lcddata.html?ticker=LOV">LOV</a>)</p>
    <p>*NOTE ABOUT THE COMPARISON CHART: The information provided in this comparison is believed to be accurate as of 3/18/2010. If you believe any of this information to be inaccurate, please let us know and we will correct the inaccuracy the next time this page is updated. eHarmony® is a registered trademark of eHarmony, Inc. Match.com&reg; is a registered trademark of Match.com, L.L.C.</p>    <p>**While eHarmony's subscribers are able to communicate with the people with whom eHarmony has matched them, Spark.com's subscribers are free to communicate with anyone on the site.</p>    <p class="background-check">SPARK NETWORKS USA, LLC DOES NOT CONDUCT BACKGROUND CHECKS ON THE MEMBERS OR SUBSCRIBERS OF THIS WEBSITE.</p>
</div>
</div>

<!-- Google Code for Spark.com - Hompage Remarketing List --> 
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1029757028;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "666666";
    var google_conversion_label = "PW6GCO6txgEQ5LCD6wM";
    var google_conversion_value = 0;
    /* ]]> */ 
</script> 
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js"> 
</script> 
<noscript> 
<div style="display:inline;"> 
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1029757028/?label=PW6GCO6txgEQ5LCD6wM&amp;guid=ON&amp;script=0"/> 
</div> 
</noscript>