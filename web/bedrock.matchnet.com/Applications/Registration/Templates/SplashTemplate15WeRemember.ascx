﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashTemplate15WeRemember.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Templates.SplashTemplate15WeRemember" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>

<style type="text/css">
body{background-image:none;background-color:white;}
#container {
	border-radius:6px;-moz-border-radius:6px;-webkit-border-radius:6px;
	box-shadow:0 0 9px #333333;-moz-box-shadow:0 0 9px #333333;-webkit-box-shadow:0 0 9px #333333;
	border:2px solid white;
	background-color:#96b1cc;
	display: block;
	width:995px;
	margin:0 auto;
	padding:8px;
	background:#96b1cc url(../img/Community/JDate/ajax-loader-96B1CC.gif) no-repeat 80px 80px;
}

#subcontainer {
	width:995px;
    height:483px;
	background:#96b1cc url(../img/Community/JDate/Promo/promo-we-remember.jpg) no-repeat 0px 0px;
}


#content-container{
    background-color:#f7f7f7;
    border:1px solid #eaeaea;
    border-radius:6px;-moz-border-radius:6px;-webkit-border-radius:6px;
    margin-top:1em;
}
#footer-content{width:976px;min-height:134px;}
#mediaLogosHalfWidth{float:left;width:470px;}
#splash-ourstories {
    position: relative;
    top: 287px;
    left: 8px;
    float: left;
    z-index: 1;
}
#jdvideo{
    float:left;
    width:520px;
    background:transparent url(../img/Community/JDate/splash-video-bg.jpg) no-repeat 0 -10px;
    min-height:121px;
    padding-left: 20px;
}
#jdvideo > a > .video-icon {
    width: 101px;
    height: 82px;
    margin-right: 20px;
    float: left;
}
#jdvideo > .text-title {
    font: normal normal bold 11px/13px arial,'helvetica neue',helvetica,sans-serif;
    color: #333;
}
#jdvideo > .text-body {
    margin-top: 33px;
    font: normal normal normal 11px/21px arial,'helvetica neue',helvetica,sans-serif;
}
#footernew{border-bottom:4px solid #e2e2e2;margin-bottom:18px;}
#footernew big{
    font-size:16px;
    color:#25426b;
    font-family:Georgia, Serif;
    margin-bottom:10px;
    display:block;
}
#jd-links{
    width:536px;
    float:left;
    background-color:#f7f7f7;
    margin-bottom:1em;
    margin-right:40px;
}
.linklist{width:148px;margin-right:22px;float:left;}
.linklist > li{display:block;border-bottom:1px dotted #ccc;}
.linklist > li > a:link,.linklist > li > a:active,.linklist > li > a:visited{
    color:#333;
    font-weight:normal;
    display:block;
    padding:3px;
}
.linklist > li > a:hover{background-color:#ededed;}
#sn-links{
    width:394px;
    float:left;
    background-color:#f7f9f7;
    margin-bottom:1em;    
}
#footernewfooter{clear:both;margin-bottom:12px;}
#footernewfooter .legal-copyright{
    color:#999;
    font-size:11px;
}
#footernewfooter .legal-copyright a{color:#999;font-size:11px;}

#marketing-wrap  {
    background-color: #96B1CC;
    border: medium none;
    left: 466px;
    top: 77px;
    width: 420px;
}
#marketing-wrap-inner  {
	border:1px solid white;
    margin:3px;
	background-color:#96b1cc;
    padding:3px 12px 8px;
}
#marketing-wrap big{
    text-align:left;
    margin:12px 0;
}
ul#splash-bullets{
    margin:0;
}
#marketing-wrap #cta .link-primary, #marketing-wrap #cta .primary{
    margin:8px 0;
}
</style>

<div id="min-max-container">
	<div id="header" class="clearfix">
		<div id="header-logo">
			<p class="header-message"><mn:image id="imgLogo" runat="server" titleResourceConstant="TTL_SITE_LOGO" ResourceConstant="ALT_SITE_LOGO" FileName="trans.gif" NavigateURLClass="logo" /><mn:txt id="mntxt5508" runat="server" resourceconstant="TXT_HEADER_MESSAGE" expandimagetokens="true" />
			</p>
		</div>
		<ul id="header-nav">
			<li><a href="http://www.jdate.co.il/"><mn:txt id="mntxt1627" runat="server" resourceconstant="TXT_HEBREW" expandimagetokens="true" /></a>
			</li>
			<li><a href="http://www.jdate.fr/"><mn:txt id="mntxt3959" runat="server" resourceconstant="TXT_FRENCH" expandimagetokens="true" />
			</a></li>
		</ul>
		<div id="header-login"><a href="/Applications/Logon/Logon.aspx"><mn:txt id="mntxt4348" runat="server" resourceconstant="TXT_LOGON" expandimagetokens="true" />
		</a></div>
	</div>
</div>
	
<div id="container" class="clearfix">
    <div id="subcontainer"><%--
			<div id="rotator">
				<mn:txt id="mntxt2864" runat="server" resourceconstant="HTML_CONTENT_PHOTO" expandimagetokens="true" />
			</div>--%>
			<div id="marketing-wrap">
            <div id="marketing-wrap-inner">
				<mn:txt id="mntxt1638" runat="server" resourceconstant="HTML_MARKETING_CONTENT" expandimagetokens="true" />
                <ul id="splash-bullets">
                    <li><mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_MARKETING_COPY_ONE" expandImageTokens="True"/></li>
                    <li><mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_MARKETING_COPY_TWO" expandImageTokens="True"/></li>
                    <li><mn:Txt ID="Txt6" runat="server" ResourceConstant="TXT_MARKETING_COPY_THREE" expandImageTokens="True"/> <a href="/Applications/MembersOnline/MembersOnline.aspx" class="mol-count"><mn:Txt ID="txtMOLCount" runat="server" /></a> <mn:Txt ID="Txt7" runat="server" ResourceConstant="TXT_MARKETING_COPY_FOUR"  expandImageTokens="True"/></li>
                </ul>
				<div id="cta">
					<a href="/Applications/Registration/Registration.aspx" class="link-primary"><mn:txt id="mntxt8138" runat="server" resourceconstant="TXT_REG_BUTTON" expandimagetokens="true" />
					</a>
				</div>
			</div>
            </div>
    </div>
</div>

<div style="margin:1.5em auto 1em;width:1014px;">
		    <div class="clearfix">
		        <div id="mediaLogosHalfWidth">
		            <p class="text-center"><strong>Recent JDate Success Stories have been featured in:</strong></p> 
		            <mn:image id="mnimage3087" runat="server" filename="splash-mediahalf-logos.jpg" titleresourceconstant="" resourceconstant="" />
                </div>
		        <div id="jdvideo">
		            <a href="/Applications/Video/VideoCommercialCenter.aspx?NavPoint=top" title="JDate Video"><img src="../img/Community/JDate/splash-video-icon.jpg" alt="JDate Video" width="106" height="87" border="0" class="video-icon" /></a>
		            <p class="text-title">Watch the Full Versions of “Our Story Began on JDate”</p>
		            <p class="text-body">You’ve seen the previews on TV, but only here can you watch the full stories of real couples who met on JDate. <a href="/Applications/Video/VideoCommercialCenter.aspx?NavPoint=top" title="Watch Now">Watch now &raquo;</a></p>
		        </div>
            </div>            
	<div id="content-container"><%--
		<div id="content-main" class="clearfix" style="display:none;">
			<div id="rotator">
				<div id="img_contents">
				    <mn:txt id="mntxt2864" runat="server" resourceconstant="HTML_CONTENT_PHOTO" expandimagetokens="true" />
				</div>
			</div>
			<div id="marketing-wrap">
				<mn:txt id="mntxt1638" runat="server" resourceconstant="HTML_MARKETING_CONTENT" expandimagetokens="true" />
                <ul id="splash-bullets">
                    <li><mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_MARKETING_COPY_ONE" expandImageTokens="True"/></li>
                    <li><mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_MARKETING_COPY_TWO" expandImageTokens="True"/></li>
                    <li><mn:Txt ID="Txt6" runat="server" ResourceConstant="TXT_MARKETING_COPY_THREE" expandImageTokens="True"/> <a href="/Applications/MembersOnline/MembersOnline.aspx" class="mol-count"><mn:Txt ID="txtMOLCount" runat="server" /></a> <mn:Txt ID="Txt7" runat="server" ResourceConstant="TXT_MARKETING_COPY_FOUR"  expandImageTokens="True"/></li>
                </ul>
				<div id="cta">
					<a href="/Applications/Registration/Registration.aspx" class="link-primary"><mn:txt id="mntxt8138" runat="server" resourceconstant="TXT_REG_BUTTON" expandimagetokens="true" />
					</a>
				</div>
			</div>
		</div>--%>
		
<div id="vis-info" class="clear-both">

<div id="footernew" class="clear-both clearfix">
<div id="jd-links">
    <big>JDate.com</big>
<ul class="linklist clearfix">
    <li><a href="http://www.jdate.com/Applications/Home/Default.aspx">Home</a></li>
    <li><a href="/Applications/Logon/Logon.aspx">Login</a></li>
    <li><a href="/Applications/ContactUs/ContactUs.aspx">Contact Us</a></li>
    <li><a href="/Applications/MemberServices/MemberServices.aspx">Your Account</a></li>
    <li><a href="/Applications/MemberServices/VerifyEmail.aspx">Verify Email</a></li>
    <li><a href="https://www.jdate.com/Applications/Subscription/Subscribe.aspx?prtid=16">Subscribe</a></li>
	<li><a href="http://static.JDate.com/Campaigns/MK07-0110_Gift_of_JDate/">Gift of JDate</a></li>
</ul>
<ul class="linklist clearfix">
	<li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1998&amp;HideNav=True">About JDate</a></li>
	<li><a href="/Applications/Article/ArticleView.aspx?CategoryID=2060&amp;HideNav=true">Our Mission</a></li>
	<li><a href="/safety" target="_blank">Safety</a></li>
	<li><a href="/Applications/Video/VideoCenter.aspx">Video</a></li>
	<li><a href="http://www.JDate.com/jmag">JMag</a></li>
	<li><a href="http://www.JDate.com/blog/">JBlog</a></li>
	<li><a href="http://www.jdatemobile.com">JDate Mobile</a></li>
</ul>
<ul class="linklist clearfix">
	<li><a href="/Applications/Events/Events.aspx">Travel &amp; Events</a></li>
	<li><a href="http://www.JDate.com/synagoguesdirectory">Synagogue Directory</a></li>
    <li><a href="http://www.JDate.com/jewish-holidays/">Jewish Holiday Calendar</a></li>
	<li><a href="/Applications/Article/FAQMain.aspx">Help &amp; Advice</a></li>
	<li><a href="/Applications/SiteMap/SiteMap.aspx">Site Map</a></li>
	<li><a href="http://www.JDate.com/blog/2011-09/jdate-infographic/">JDate Success</a></li>
</ul>
</div>
<div id="sn-links">
    <big>Spark Networks</big>
    <ul class="linklist clearfix">
	    <li><a href="http://www.spark.net/about.htm">About Spark Networks</a></li>
	    <li><a href="http://www.spark.net/our-portfolio/portfolio-overview/">Spark Networks' Sites</a></li>
	    <li><a href="http://www.spark.net/advertise.asp" >Advertise With Us</a></li>
	    <li><a href="http://affiliates.spark.net/">Affiliate Program</a></li>
	    <li><a href="http://www.spark.net/investor.htm">Investor Relations</a></li>
	    <li><a href="http://www.spark.net/careers.htm">Jobs</a></li>
	    <li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#privacy">Privacy</a></li>
	</ul>
	<ul class="linklist clearfix">
	    <li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#service">Terms of Service</a>  </li>
         <li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#cookie">Cookie Policy</a>  </li>
	    <li><a href="http://search.JDate.com/">Local Online Dating</a></li>
	    <li><a target="_blank" href="http://www.JDate.com/singles/">Jewish Singles</a></li>
	    <li><a href="http://www.spark.net/intellectual.htm">Our Intellectual Property </a></li>
    </ul>
</div>
    <div id="footernewfooter" class="clearfix">
        <p class="legal-copyright editorial">Copyright &copy; 2012 Spark Networks&reg; USA, LLC. All rights reserved. Spark Networks USA, LLC is a wholly-owned subsidiary of Spark Networks, Inc., a NYSE MKT Company (<a target="_blank" href="http://www.nyse.com/about/listed/lcddata.html?ticker=LOV">LOV</a>)</p>
<%--        <div id="socialbuttons">
            <a id="buttonTwitter" href="http://www.Twitter.com/JDate" target="_blank"></a>
            <a id="buttonFacebook" href="http://www.Facebook.com/JDate" target="_blank"></a>
        </div>--%>
    </div>
</div>
		<div class="editorial">
            <mn:txt id="mntxt6945" runat="server" resourceconstant="TXT_SPLASH_SEO" expandimagetokens="true" /></div>
		</div>
	</div>
</div>
<div id="footer">
	<div id="footer-content">
	    <p class="legal-background-checks"><b>Spark Networks USA, LLC does not conduct background checks on the members or subscribers of this website.</b></p>
	</div>
</div>

<%--<div id="footer">
	<div id="footer-content">
		<mn1:Footer20 ID="Footer20" Runat="server" />
	</div>
</div>--%>

<!-- Google Code for Spark.com - Test D Remarketing List --> 
<script type="text/javascript"> 
/* <![CDATA[ */ 
var google_conversion_id = 1029757028; 
var google_conversion_language = "en"; 
var google_conversion_format = "3"; 
var google_conversion_color = "666666"; 
var google_conversion_label = "PiMQCK6O2AEQ5LCD6wM"; 
var google_conversion_value = 0; 
/* ]]> */ 
</script> 
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js"> 
</script> 
<noscript> 
<div style="display:inline;"> 
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1029757028/?label=PiMQCK6O2AEQ5LCD6wM&amp;guid=ON&amp;script=0"/> 
</div> 
</noscript>