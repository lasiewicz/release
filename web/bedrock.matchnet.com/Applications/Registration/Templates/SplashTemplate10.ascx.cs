﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.Registration.Controls;

namespace Matchnet.Web.Applications.Registration.Templates
{
    public partial class SplashTemplate10 : RegistrationTemplateBase, IRegistrationTemplate
    {

        protected void Page_Load(object sender, EventArgs e)
        {
          
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
            {
                g.SetPageTitle(g.GetResource("TXT_PAGE_TITLE", this));
                //g.SetMetaDescriptionWithTag(g.GetResource("TXT_META_DESCRIPTION", this));
            }
        }
    }
}

