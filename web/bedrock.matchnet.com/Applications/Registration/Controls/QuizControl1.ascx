﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuizControl1.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Controls.QuizControl1" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="divControl"  runat="server" >
    <uc2:ValidationMessage runat="server" ID="txtValidation" Visible=true />
    <mn:txt runat="server" id="txtTitle" />
    <asp:Repeater ID="repeaterQuestions" runat="server" OnItemDataBound="RepeaterSections_ItemDataBound">
        <HeaderTemplate><ul class="cc-questions clearfix"></HeaderTemplate>
        <ItemTemplate>
            <li><h2><asp:Literal ID="literalQuestionText" runat="server"></asp:Literal></h2>
            <ul>
                <li><asp:Literal ID="radioChoice1" runat="server"></asp:Literal></li>
                <li><asp:Literal ID="radioChoice2" runat="server"></asp:Literal></li>
                <li><asp:Literal ID="radioChoice3" runat="server"></asp:Literal></li>
             <li><asp:Literal ID="radioChoice4" runat="server"></asp:Literal></li>
            </ul></li>
        </ItemTemplate>
        <FooterTemplate></ul><%-- <p><strong>NOTE:</strong> You may encounter a question that does not have an answer which accurately describes you. That is ok! Just pick the word that comes closest.</p> --%></FooterTemplate>
    </asp:Repeater>       
</div>