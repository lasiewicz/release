﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Security;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class QuizControl1 : FrameworkControl, IAttributeControl
    {
        AttributeControl _attrControl;
        string _selectedValue;

        private Quiz _quiz = null;
        private MemberQuiz _memberQuiz = null;
        List<Question> _questions;

        Dictionary<int, int> _answers;
        FrameworkControl _resourceControl;
        System.Web.UI.WebControls.Literal litCss;

        public string ContainerDivClientID { get { return divControl.ClientID; } }

        public bool IsOverlayReg { get; set; }

        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "block");

        }
        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }
        public ValidationMessage ValidationMessage { get { return txtValidation; } }
        public void Save()
        {

        }
        public void SetValue(string value)
        {
            _answers = new Dictionary<int, int>();
            if (String.IsNullOrEmpty(value))
                return;

            string[] values = value.Split(new char[] { '|' });
            foreach (string v in values)
            {
                string[] valuepair = v.Split(new char[] { '=' });
                if (!String.IsNullOrEmpty(valuepair[0]))
                {
                    int qid = Int32.Parse(valuepair[0]);
                    int q = Int32.Parse(valuepair[1]);

                    _answers.Add(qid, q);
                }
            }

        }
        public string GetValue()
        {
            _selectedValue = "";

            return _selectedValue.ToString();
            //_selectedValue.ToString();

        }
        public string ScriptBlock()
        {
            return "";
        }
        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return "";
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }
        public void PersistToCookie(HttpCookie cookie)
        {
            foreach (Question q in _questions)
            {
                bool answered = false;
              
                string cookiestring="";
                foreach(int key in _answers.Keys)
                {
                    cookiestring+=key.ToString() + "=" + _answers[key].ToString() + "|";
                }
                cookie[_attrControl.AttributeName] = cookiestring;
            }
        }

        public void Persist(ITemporaryPersistence persistence)
        {
            foreach (Question q in _questions)
            {
                bool answered = false;

                string cookiestring = "";
                foreach (int key in _answers.Keys)
                {
                    cookiestring += key.ToString() + "=" + _answers[key].ToString() + "|";
                }
                persistence[_attrControl.AttributeName] = cookiestring;
            }
        }

        public bool Validate()
        {
            bool valid = true;
            try{
                foreach (Question q in _questions)
                {
                   
                        int choiceID = _answers[q.ID];
                     
                         if (choiceID <= 0)
                        {
                            valid = false;
                            txtValidation.Text = _attrControl.GetValidationMessage("CCQUIZ_ANSWERS_REQUIRED", g, _resourceControl);
                            break;
                        }

                    
                }
                return valid;
            }catch(Exception ex)
            {
                txtValidation.Text = _attrControl.GetValidationMessage("CCQUIZ_ANSWERS_REQUIRED", g, _resourceControl);
                return false;}
        }




        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }

        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                _quiz = ColorCodeHelper.GetQuiz(g);

                //if (!String.IsNullOrEmpty(_attrControl.AttributeName))
                //     cookie[_attrControl.AttributeName] = GetValue();

                _questions = _quiz.GetQuestions(_attrControl.QuizQuestionID, _attrControl.QuizQuestionCount);
                SetValue(_attrControl.AttributeTextValue);

                txtTitle.Text = g.GetResource(_attrControl.ResourceConstant, _resourceControl);
                if (!String.IsNullOrEmpty(_attrControl.CSSClass))
                {
                    divControl.Attributes.Add("class", _attrControl.CSSClass);
                }

            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (_answers == null)
                    _answers = new Dictionary<int, int>();
                foreach (Question q1 in _questions)
                {
                    if (!String.IsNullOrEmpty(Request.Form["question" + q1.ID.ToString()]))
                    {
                        int choiceID = Convert.ToInt32(Request.Form["question" + q1.ID.ToString()]);
                        _answers[q1.ID] = choiceID;
                    }
                    
                }
                repeaterQuestions.DataSource = _questions;
                repeaterQuestions.DataBind();

                divControl.Attributes["class"] = _attrControl.Name;
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
           
        }



        private void Page_PreRender(object sender, System.EventArgs e)
        {
            //AttributeTextControl.Text = _attrControl.AttributeTextValue;


            // txtCaptchaTextbox.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);

        }

        protected void RepeaterSections_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //get data object
                Question dataItem = e.Item.DataItem as Question;

                //Question text
                Literal literalQuestionText = (Literal)e.Item.FindControl("literalQuestionText");
                literalQuestionText.Text = dataItem.Text;

                //Choices
                int indexCount = 1;
                foreach (Choice c in dataItem.Choices)
                {
                    Literal radioChoice = e.Item.FindControl("radioChoice" + indexCount.ToString()) as Literal;
                    if (radioChoice != null)
                    {
                        int memberChoiceID = 0;
                        if (_answers != null )
                        {
                            try
                            {
                                memberChoiceID = _answers[dataItem.ID];
                            }
                            catch (Exception ex) { }
                        }
                            //_memberQuiz.GetSelectedChoiceID(dataItem.ID);
                        if (c.ID == memberChoiceID)
                            radioChoice.Text = "<input id=\"choice" + dataItem.ID.ToString() + "_" + c.ID.ToString() + "\" type=\"radio\" name=\"question" + dataItem.ID.ToString() + "\" value=\"" + c.ID.ToString() + "\" checked=\"checked\" />";
                        else
                            radioChoice.Text = "<input id=\"choice" + dataItem.ID.ToString() + "_" + c.ID.ToString() + "\" type=\"radio\" name=\"question" + dataItem.ID.ToString() + "\" value=\"" + c.ID.ToString() + "\" />";

                        radioChoice.Text += "<label for=\"choice" + dataItem.ID.ToString() + "_" + c.ID.ToString() + "\">" + c.Text + "</label>";
                    }
                    indexCount++;
                }

            }

        }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            return new NameValueCollection();
        }



    }
}