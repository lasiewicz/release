﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeCaptcha.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeCaptcha" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<div id="divControl" runat="server">
    <%=GetTitleWrapperOpen %><mn:Txt ID="txtCaptchaTextbox" runat="server" ResourceConstant="TXT_CAPTCHA" />
    <%=GetTitleWrapperClose %>
    <uc2:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
    <mn:Image runat="server" ID="imgCaptcha" Width="200" Height="50" /><%=GetBR(1)%>
    <%=GetFieldWrapperOpen %>
    <asp:TextBox ID="AttributeCaptchaControl" Style="width: 180px;" runat="server" CssClass="formDefault" />
    <mn:Image runat="server" ID="imgCaptchaOnePageReg" Visible="false" CssClass="capcha-image"/>
    <asp:PlaceHolder runat="server" ID="plcRefreshCaptcha" Visible="false">
        <div id="refresh-captcha">
            <mn:Image runat="server" ID="imgRefreshCapcha" FileName="refresh-capcha-btn.png"
                CssClass="refresh-capcha" TitleResourceConstant="IMG_REFRESH_CAPTCHA_TITLE" />
        </div>
    </asp:PlaceHolder>
    <%=GetFieldWrapperClose %>
    <%=GetBR(2)%>
</div>
