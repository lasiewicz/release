﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeDOBControl : FrameworkControl, IAttributeControl, IJQValidation
    {
        protected DropDownList ddlBirthDateMonth;
        protected DropDownList ddlBirthDateDay;
        AttributeControl _attrControl;
        DateTime _selectedValue;

        FrameworkControl _resourceControl;

        public string ContainerDivClientID { get { return divControl.ClientID; } }

        public bool IsOverlayReg { get; set; }

        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "block");

        }
        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }

        public void Save()
        {

        }
        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return ddlBirthDateMonth.ClientID;
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }
        public void SetValue(string value)
        {
            _selectedValue = Conversion.CDateTime(value);
        }
        public string GetValue()
        {
            setBirthDate();
            return _selectedValue.ToString();

        }

        public void PersistToCookie(HttpCookie cookie)
        {
            cookie[_attrControl.AttributeName] = GetValue();
        }

        public void Persist(ITemporaryPersistence persistence)
        {
            persistence[_attrControl.AttributeName] = GetValue();
        }

        public bool Validate()
        {
            bool valid = true;
            DateTime date = Conversion.CDateTime(GetValue());
            if (date == DateTime.MinValue)
            {
                valid = false;
                txtValidation.Visible = true;
                txtValidation.Text = _attrControl.GetValidationMessage(_attrControl.ErrorMessageRequired, g, _resourceControl);
            }

            return valid;
        }
        public string ScriptBlock()
        {
            return "";
        }

        public ValidationMessage ValidationMessage { get { return txtValidation; } }

        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {

            string dateFormat = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SITE_DATE_FORMAT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

            if (dateFormat == "UK")
            {
                ddlBirthDateMonth = ddlBirthDateMonthBrit;
                ddlBirthDateDay = ddlBirthDateDayBrit;
                plcBritDOB.Visible = true;
                plcUSDOB.Visible = false;
                plcHebrewDOB.Visible = false;
            }
            else if (dateFormat == "US")
            {
                ddlBirthDateMonth = ddlBirthDateMonthUS;
                ddlBirthDateDay = ddlBirthDateDayUS;
                plcUSDOB.Visible = true;
                plcBritDOB.Visible = false;
                plcHebrewDOB.Visible = false;
            }

            if (_attrControl.OnePageReg)
            {
                ddlBirthDateDay = ddlBirthDateDayOnePage;
                ddlBirthDateDayOnePage.Visible = true;
                ddlBirthDateDayUS.Visible = false;
            }



            bindBirthDate();

            if (_attrControl.RequiredFlag)
            {
                tbBirthDateYearHebrew.CssClass += " mandatory";
                ddlBirthDateDayBrit.CssClass += " mandatory";
                ddlBirthDateMonthBrit.CssClass += " mandatory";
                ddlBirthDateMonthUS.CssClass += " mandatory";
                ddlBirthDateDayUS.CssClass += " mandatory";
                ddlBirthDateYearUS.CssClass += " mandatory";
                ddlBirthDateDayOnePage.CssClass += " mandatory";
            }
            else
            {
                tbBirthDateYearHebrew.CssClass += " optional";
                ddlBirthDateDayBrit.CssClass += " optional";
                ddlBirthDateMonthBrit.CssClass += " optional";
                ddlBirthDateMonthUS.CssClass += " optional";
                ddlBirthDateDayUS.CssClass += " optional";
                ddlBirthDateYearUS.CssClass += " optional";
                ddlBirthDateDayOnePage.CssClass += " optional";
            }

            divControl.Attributes["class"] = _attrControl.Name;
        }


        private void Page_PreRender(object sender, System.EventArgs e)
        {
            txtCaption.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);
            setBirthControls();
        }
        private void bindBirthYear()
        {
            ddlBirthDateYearUS.Items.Add(new ListItem(g.GetResource("Year", _resourceControl), null)); // Empty item at top of list
            int year = DateTime.Now.Year - _attrControl.MinLen;
            for (int i = year; i >= year - (99 - 18); i--)
            {
                ddlBirthDateYearUS.Items.Add(
                    new ListItem(i.ToString(), i.ToString())
                    );
            }

            if (_selectedValue > DateTime.MinValue)
            {
                ddlBirthDateYearUS.SelectedValue = _selectedValue.Year.ToString();
            }
        }


        private void bindBirthMonth()
        {
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTH", _resourceControl), null)); // Empty item at top of list

            // these are called out explicitly rather than via a loop for
            // the benefit of automated Resouce Management tools. Please don't
            // change unless you know this restriction to no longer be relevant.
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR1", null), "1"));
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR2", null), "2"));
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR3", null), "3"));
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR4", null), "4"));
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR5", null), "5"));
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR6", null), "6"));
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR7", null), "7"));
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR8", null), "8"));
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR9", null), "9"));
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR10", null), "10"));
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR11", null), "11"));
            ddlBirthDateMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR12", null), "12"));
            if (_selectedValue > DateTime.MinValue)
            {
                ddlBirthDateMonth.SelectedValue = _selectedValue.Month.ToString();
            }
        }

        private void bindBirthDay()
        {
            ddlBirthDateDay.Items.Add(new ListItem(g.GetResource("DAY", _resourceControl), null)); // Empty item at top of list
            for (int i = 1; i <= 31; i++)
            {
                ddlBirthDateDay.Items.Add(
                    new ListItem(i.ToString(), i.ToString())
                    );
            }

            if (_selectedValue > DateTime.MinValue)
            {
                ddlBirthDateDay.SelectedValue = _selectedValue.Day.ToString();
            }
        }

        private void bindBirthDate()
        {
            bindBirthMonth();
            bindBirthDay();
            bindBirthYear();
        }

        private void setBirthDate()
        {
            int birthMonth = 0;
            int birthDay = 0;
            int birthYear = 0;
            try
            {
                if (IsOverlayReg)
                {
                    string dateFormat = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SITE_DATE_FORMAT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

                    if (dateFormat == "UK")
                    {
                        ddlBirthDateMonth = ddlBirthDateMonthBrit;
                        ddlBirthDateDay = ddlBirthDateDayBrit;
                        plcBritDOB.Visible = true;
                        plcUSDOB.Visible = false;
                        plcHebrewDOB.Visible = false;
                    }
                    else if (dateFormat == "US")
                    {
                        ddlBirthDateMonth = ddlBirthDateMonthUS;
                        ddlBirthDateDay = ddlBirthDateDayUS;
                        plcUSDOB.Visible = true;
                        plcBritDOB.Visible = false;
                        plcHebrewDOB.Visible = false;
                    }

                    if (_attrControl.OnePageReg)
                    {
                        ddlBirthDateDay = ddlBirthDateDayOnePage;
                    }


                    int.TryParse(Request[ddlBirthDateMonth.UniqueID], out birthMonth);
                    int.TryParse(Request[ddlBirthDateDay.UniqueID], out birthDay);
                    int.TryParse(Request[ddlBirthDateYearUS.UniqueID], out birthYear);
                    _selectedValue = new DateTime(birthYear, birthMonth, birthDay);
                }
                else
                {
                    birthMonth = Convert.ToInt32(ddlBirthDateMonth.SelectedValue);
                    birthDay = Convert.ToInt32(ddlBirthDateDay.SelectedValue);
                    //birthYear	= Convert.ToInt32(this.ddlBirthDateYear.SelectedValue);
                    birthYear = Convert.ToInt32(ddlBirthDateYearUS.Text.Trim());
                    _selectedValue = new DateTime(birthYear, birthMonth, birthDay);
                }
            }
            catch (Exception)
            {
                //Member has entered an invalid combination of MM/DD/YYYY, reset birth date causing validation to fail.
                _selectedValue = DateTime.MinValue;
            }
        }

        private void setBirthControls()
        {
            if (_selectedValue > DateTime.MinValue)
            {
                ddlBirthDateMonth.SelectedValue = _selectedValue.Month.ToString();
                ddlBirthDateDay.SelectedValue = _selectedValue.Day.ToString();
                ddlBirthDateYearUS.SelectedValue = _selectedValue.Year.ToString();
            }
        }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            return new NameValueCollection();
        }

        protected string GetTitleWrapperOpen
        {
            get
            {
                return (_attrControl.OnePageReg) ? "<div class=\"label\">" : "<h2>";
            }
        }
        protected string GetTitleWrapperClose
        {
            get
            {
                return (_attrControl.OnePageReg) ? "</div>" : "</h2>";
            }
        }
        protected string GetFieldWrapperOpen
        {
            get { return (_attrControl.OnePageReg) ? "<div class=\"field\">" : string.Empty; }
        }
        protected string GetFieldWrapperClose
        {
            get { return (_attrControl.OnePageReg) ? "</div>" : string.Empty; }
        }

        #region IJQValidation Members

        public string InputFieldID
        {
            get
            {
                return ddlBirthDateDayUS.UniqueID;
            }
        }

        public List<ValidationDictionaryEntry> GetValidationRules
        {
            get
            {
                List<ValidationDictionaryEntry> result = new List<ValidationDictionaryEntry>();
                ValidationDictionaryEntry rules = new ValidationDictionaryEntry();
                rules.UniqueID = ddlBirthDateDayOnePage.UniqueID;
                rules.ValidationEntryList.Add(new ValidationEntry()
                                                  {
                                                      errorKey = "wrongBirthDate",
                                                      message = _attrControl.GetValidationMessage(_attrControl.ErrorMessageRequired, g, _resourceControl),
                                                      value = "true"
                                                  });

                result.Add(rules);
                return result;
            }
        }

        #endregion
    }
}