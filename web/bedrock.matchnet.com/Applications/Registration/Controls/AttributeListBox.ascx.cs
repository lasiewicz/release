﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Interfaces;


namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeListBox : FrameworkControl, IAttributeControl
    {
        AttributeControl _attrControl;
        bool _useAttributeDescription;
        bool _includeBlanks;
        int _selectedValue;

        FrameworkControl _resourceControl;
        public int ScrollSelectedTop { get; set; }
        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }

        public void Save()
        {

        }
        public ValidationMessage ValidationMessage { get { return txtValidation; } }
        public void SetValue(string value)
        {
            _selectedValue = Conversion.CInt(value);
            //AttributeOptions.SelectedValue = _selectedValue.ToString();
        }
        public string GetValue()
        {
            if (IsOverlayReg)
            {
                if (string.IsNullOrEmpty(Request[AttributeOptions.UniqueID]) && !string.IsNullOrEmpty(_attrControl.DefaultSelection))
                {
                    if (AttributeOptions.Items.FindByValue(_attrControl.DefaultSelection) != null)
                    {
                        AttributeOptions.SelectedIndex =
                            AttributeOptions.Items.IndexOf(AttributeOptions.Items.FindByValue(_attrControl.DefaultSelection));
                    }
                    return _attrControl.DefaultSelection;
                }
                else
                {
                    return Request[AttributeOptions.UniqueID];
                }

            }
            else
            {
                return AttributeOptions.SelectedValue;
            }
        }
        public string ScriptBlock()
        {
            return "";
        }
        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return AttributeOptions.ClientID;
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }
        public void PersistToCookie(HttpCookie cookie)
        {
            cookie[_attrControl.AttributeName] = GetValue();
        }

        public void Persist(ITemporaryPersistence persistence)
        {
            persistence[_attrControl.AttributeName] = GetValue();
        }

        public bool Validate()
        {
            bool valid = false;
            if (!_attrControl.RequiredFlag)
                return true;

            if (IsOverlayReg)
            {
                _selectedValue = Conversion.CInt(GetValue());
            }
            else
            {
                _selectedValue = Conversion.CInt(AttributeOptions.SelectedValue);
            }

            if (_selectedValue <= 0 && _attrControl.AttributeName != "ChildrenCount") // Children Count can be  0 (None)
            {
                txtValidation.Visible = true;
                txtValidation.Text = _attrControl.GetValidationMessage(_attrControl.ErrorMessageRequired, g, _resourceControl);
            }
            else
            {
                txtValidation.Visible = false;
                txtValidation.Text = "";
                valid = true;
            }

            return valid;
        }
        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "block");

        }


        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsOverlayReg)
            {
                // For auto submitting
                AttributeOptions.Attributes.Add("onclick", "listboxAutoClick(this);");
            }

            txtCaption.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);

            ListItemCollection collection;

            if (_attrControl.AttributeName.ToLower() == "height")
            {
                collection = FrameworkGlobals.CreateHeightListItems(g, false);
            }
            else
            {
                if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
                {
                    collection = _attrControl.GetAttributeOptionCollectionAsListItems(g, _resourceControl, false);
                }
                else
                {
                    collection = _attrControl.GetAttributeOptionCollectionAsListItems(g, null, false);
                }
            }

            if ((AttributeOptions is ListBox) && (_attrControl.NoScroll))
            {
                int count = collection.Count - 1;

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BBW || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BlackSingles)
                {
                    // per James Green.. ... .... ........ This needs to be turned into meta data but it's midnight.
                    switch (_attrControl.AttributeName.ToLower())
                    {
                        case "height":
                        case "religion":
                            ((ListBox)AttributeOptions).SelectedValue = _attrControl.SelectedValue;

                            count = _attrControl.ListVisibleElementsCount;

                            break;
                        case "eyecolor":
                        case "haircolor":
                        case "educationlevel":
                        case "ethnicity":
                            count = 8;
                            break;
                    }
                }

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles)
                {
                    switch (_attrControl.AttributeName.ToLower())
                    {
                        case "religion":
                        case "jdatereligion":
                        case "industrytype":
                            count = 8;
                            break;
                    }
                }

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
                {
                    switch (_attrControl.AttributeName.ToLower())
                    {
                        case "industrytype":
                            count = 8;
                            break;
                    }
                }

                ((ListBox)AttributeOptions).Rows = count;


            }

            //Remove the blank option for this type of control (Copying provo's)
            collection.RemoveAt(0);

            AttributeOptions.DataSource = collection;

            AttributeOptions.DataValueField = "Value";
            AttributeOptions.DataTextField = "Text";

            AttributeOptions.DataBind();

            // Value "0" should not be selected in children count
            if (_attrControl.AttributeName.ToLower() != "childrencount" || _selectedValue > 0)
            {
                AttributeOptions.SelectedValue = _selectedValue.ToString();
            }
            
            MoveSelectedToTop();

            divControl.Attributes["class"] = _attrControl.Name;

            if (_attrControl.RequiredFlag)
            {
                AttributeOptions.CssClass += " mandatory";
            }
            else
            {
                AttributeOptions.CssClass += " optional";
            }
        }


        private void MoveSelectedToTop()
        {
            string jqueryformat = "<script type=\"text/javascript\">$j(document).ready(function(){ $j('#{0}').scrollTop({1});});</script>";
            try
            {
                if (!_attrControl.ListMoveFirstElementTop)
                    return;
                else
                {
                    // phSelectedFirst.Visible = true;
                    ScrollSelectedTop = AttributeOptions.SelectedIndex * _attrControl.ListElementHeight;
                    System.Text.StringBuilder bld = new System.Text.StringBuilder();
                    bld.Append("$j(document).ready(function(){ $j('#");
                    bld.Append(AttributeOptions.ClientID);
                    bld.Append("').scrollTop(");
                    bld.Append(ScrollSelectedTop.ToString());
                    bld.Append(");});");
                    //string script=string.Format(jqueryformat,AttributeOptions.ClientID,ScrollSelectedTop);
                    if(IsOverlayReg)
                        this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), "FirstElementToTop", bld.ToString());
                    else
                        Anthem.Manager.RegisterStartupScript(this.Page.GetType(), "FirstElementToTop", bld.ToString(), true);
                    bld = null;
                   
                }





            }
            catch (Exception ex)
            {

            }


        }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            return new NameValueCollection();
        }


        #region IAttributeControl Members


        public string ContainerDivClientID
        {
            get { return divControl.ClientID; }
        }

        public bool IsOverlayReg { get; set; }


        #endregion
    }
}