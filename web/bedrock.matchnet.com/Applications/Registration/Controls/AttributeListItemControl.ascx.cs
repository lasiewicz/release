﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeListItemControl : FrameworkControl, IAttributeControl
    {

        AttributeControl _attrControl;
        
        int _selectedValue;
        FrameworkControl _resourceControl;
        ListControl AttributeOptions;

        public string ContainerDivClientID { get { return divControl.ClientID; } }

        public bool IsOverlayReg { get; set; }
        public bool DisableAutoAdvance { get; set; }

        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }
        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "block");

            //  this.Visible = visible;

        }
        public ValidationMessage ValidationMessage { get { return txtValidation; } }
        public void Save()
        {

        }
        public void SetValue(string value)
        {
            _selectedValue = Conversion.CInt(value);
            //if (_attrControl.AttributeName == "GenderMask" && AttributeOptions != null)
            //{
            //    AttributeOptions.SelectedValue = _selectedValue.ToString();
            //}
            if (AttributeOptions != null)
            {
                foreach (ListItem listItem in AttributeOptions.Items)
                {
                    int itemValue = Conversion.CInt(listItem.Value);

                    if ((itemValue & _selectedValue) == itemValue)
                    {
                        if (!IsOverlayReg || (IsOverlayReg && !string.IsNullOrEmpty(_attrControl.SelectedValue)) || (IsOverlayReg && _attrControl.AttributeName == "GenderMask"))
                        {
                            listItem.Selected = true;
                        }

                        if (_attrControl.ListControlType != Framework.TemplateControls.ListType.checkbox)
                        {
                            break;
                        }
                    }
                    else
                    {
                        listItem.Selected = false;
                    }
                }
            }
        }
        public string GetValue()
        {
            int value = 0;

            if (IsOverlayReg)
            {
                if (_attrControl.ListControlType == Framework.TemplateControls.ListType.ddl || _attrControl.ListControlType == Framework.TemplateControls.ListType.listbox || _attrControl.ListControlType == Framework.TemplateControls.ListType.ddlcustom)
                {
                    int.TryParse(Request[AttributeOptions.UniqueID], out value);
                }
                else
                {
                    if (AttributeOptions != null)
                    {
                        int inputIndex = 0;
                        foreach (ListItem listItem in AttributeOptions.Items)
                        {
                            if (!string.IsNullOrEmpty(Request[AttributeOptions.UniqueID + ":" + inputIndex++]))
                            {
                                value += Conversion.CInt(listItem.Value);
                            }
                        }
                    }
                }

            }
            else
            {
                if (AttributeOptions != null)
                {
                    foreach (ListItem listItem in AttributeOptions.Items)
                    {
                        if (listItem.Selected)
                        {
                            value += Conversion.CInt(listItem.Value);
                        }
                    }
                }
            }

            return value.ToString();
        }

        public void PersistToCookie(HttpCookie cookie)
        {
            string currCookieVal = cookie[_attrControl.AttributeName];
            int val, newval;
            newval = 0;
            if (!String.IsNullOrEmpty(currCookieVal))
            {
                val = Conversion.CInt(currCookieVal, 0);
                int thisval = Conversion.CInt(GetValue(), 0);
                for (int i = 0; i < AttributeOptions.Items.Count; i++)
                {
                    ListItem item = AttributeOptions.Items[i];
                    int itemval = Conversion.CInt(item.Value);
                    if ((val & itemval) == itemval)
                    {
                        val = val - itemval;
                    }
                }
                if (val > 0 && thisval > 0)
                {
                    if ((val & thisval) != val)
                        newval = val + thisval;
                    else
                        newval = val;
                }
                else
                {
                    newval = thisval;
                }

            }
            else
            {
                newval = Conversion.CInt(GetValue(), 0);
            }
            cookie[_attrControl.AttributeName] = newval.ToString();
        }

        public void Persist(ITemporaryPersistence persistence)
        {
            string currCookieVal = persistence[_attrControl.AttributeName];
            int val, newval;
            newval = 0;
            if (!String.IsNullOrEmpty(currCookieVal))
            {
                val = Conversion.CInt(currCookieVal, 0);
                int thisval = Conversion.CInt(GetValue(), 0);
                for (int i = 0; i < AttributeOptions.Items.Count; i++)
                {
                    ListItem item = AttributeOptions.Items[i];
                    int itemval = Conversion.CInt(item.Value);
                    if ((val & itemval) == itemval)
                    {
                        val = val - itemval;
                    }
                }
                if (val > 0 && thisval > 0)
                {
                    if ((val & thisval) != val)
                        newval = val + thisval;
                    else
                        newval = val;
                }
                else
                {
                    newval = thisval;
                }

            }
            else
            {
                newval = Conversion.CInt(GetValue(), 0);
            }
            persistence[_attrControl.AttributeName] = newval.ToString();
        }

        public bool Validate()
        {
            bool valid = false;
            if (!_attrControl.RequiredFlag)
                return true;

            if (IsOverlayReg)
            {
                _selectedValue = Conversion.CInt(GetValue());
            }
            else
            {
                _selectedValue = Conversion.CInt(AttributeOptions.SelectedValue);
            }

            if (_selectedValue <= 0)
            {
                txtValidation.Visible = true;

                txtValidation.Text = _attrControl.GetValidationMessage(_attrControl.ErrorMessageRequired, g, _resourceControl);
            }
            else
            {
                txtValidation.Visible = false;
                txtValidation.Text = "";
                valid = true;
            }

            return valid;
        }

        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return AttributeOptions.ClientID;
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }



        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            switch (_attrControl.ListControlType)
            {
                case Framework.TemplateControls.ListType.checkbox:
                    AttributeOptions = new CheckBoxList();
                    ((CheckBoxList)AttributeOptions).RepeatColumns = 1;
                    ((CheckBoxList)AttributeOptions).RepeatLayout = RepeatLayout.Flow;
                    break;
                case Framework.TemplateControls.ListType.ddl:
                case Framework.TemplateControls.ListType.ddlcustom:
                    AttributeOptions = new DropDownList();
                    if (_attrControl.Multiple != Framework.TemplateControls.MultipleType.none)
                    {
                        AttributeOptions.Attributes.Add("multiple", "");
                        string cssClass = "multiple";
                        if (_attrControl.Multiple == Framework.TemplateControls.MultipleType.single)
                        {
                            cssClass = "single";
                        }

                        AttributeOptions.CssClass = cssClass;
                    }

                    if (_attrControl.RequiredFlag)
                    {
                        AttributeOptions.CssClass += " mandatory";
                    }
                    else
                    {
                        AttributeOptions.CssClass += " optional";
                    }

                    break;
                case Framework.TemplateControls.ListType.listbox:
                    AttributeOptions = new ListBox();
                    if (_attrControl.RequiredFlag)
                    {
                        AttributeOptions.CssClass += " mandatory";
                    }
                    else
                    {
                        AttributeOptions.CssClass += " optional";
                    }
                    break;
                case Framework.TemplateControls.ListType.radio:
                    AttributeOptions = new RadioButtonList();
                    ((RadioButtonList)AttributeOptions).RepeatColumns = 1;
                    ((RadioButtonList)AttributeOptions).RepeatLayout = RepeatLayout.Flow;
                    break;


            }

            bool skipblanks = (_attrControl.ListControlType != Framework.TemplateControls.ListType.ddl);

            ListItemCollection items = null;
            if ((_attrControl.ListControlType != Framework.TemplateControls.ListType.ddlcustom) &&
                (_attrControl.ListControlType != Framework.TemplateControls.ListType.listbox))
                items = _attrControl.GetAttributeOptionCollectionAsListItems(g, _resourceControl, skipblanks);
            else
                items = _attrControl.GetListItems(g, _resourceControl, skipblanks);

            if (_attrControl.ListControlType == Framework.TemplateControls.ListType.listbox)
            {
                if (!IsOverlayReg && !_attrControl.DisableAutoAdvance)
                {
                    // For auto submitting
                    AttributeOptions.Attributes.Add("onclick", "listboxAutoClick(this);");
                }

                if (_attrControl.AttributeName.ToLower() == "gendermask" && g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.BlackSingles && g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.JDate)
                {
                    items.Insert(0, new ListItem(g.GetResource("SELECT_GENDER", this), "0"));
                }
            }

            if ((AttributeOptions is ListBox) && (_attrControl.NoScroll))
            {
                int count = items.Count - 1;

                if (_attrControl.AttributeName.ToLower() == "gendermask")
                    count++;

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BBW)
                {
                    // per James Green.. ... .... ........ This needs to be turned into meta data but it's midnight.
                    switch (_attrControl.AttributeName.ToLower())
                    {
                        case "height":
                            ((ListBox)AttributeOptions).SelectedValue = "150";
                            count = 10;
                            break;
                        case "eyecolor":
                        case "haircolor":
                        case "educationlevel":
                        case "ethnicity":
                            count = 8;
                            break;
                    }
                }

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles)
                {
                    switch (_attrControl.AttributeName.ToLower())
                    {
                        case "religion":
                        case "jdatereligion":
                        case "industrytype":
                            count = 8;
                            break;
                    }
                }

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
                {
                    switch (_attrControl.AttributeName.ToLower())
                    {
                        case "industrytype":
                            count = 8;
                            break;
                    }
                }


                ((ListBox)AttributeOptions).Rows = count;
            }

            if (_attrControl.ListControlType == Framework.TemplateControls.ListType.checkbox &&
                _attrControl.ListSelection == ListSelectionType.single)
            {
                bindListItems(items);
            }
            else
            {
                AttributeOptions.DataSource = items;
                AttributeOptions.DataValueField = "Value";
                AttributeOptions.DataTextField = "Text";

                AttributeOptions.DataBind();

                if (_attrControl.ListControlType == Framework.TemplateControls.ListType.ddl && IsOverlayReg)
                {
                    AttributeOptions.Attributes.Add("Size", items.Count.ToString());
                }

                /*
                foreach (ListItem item in AttributeOptions.Items)
                {
                    if (_attrControl.RequiredFlag)
                    {
                        item.Attributes["class"] += " mandatory";
            }
                    else
                    {
                        item.Attributes["class"] += " optional";
                    }
                }
                 * */
            }

            //AttributeOptions.SelectedValue = _selectedValue.ToString();

            if (!String.IsNullOrEmpty(_attrControl.ResourceConstant))
                txtCaption.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);

            if (_attrControl.ShowLabelInsideInput == true)
            {
                AttributeOptions.Items.Insert(0, new ListItem(txtCaption.Text, "-2"));
            }

            SetValue(_selectedValue.ToString());
            AttributeOptions.ID = "AttributeOptions";
            plcCtrl.Controls.Add(AttributeOptions);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            divControl.Attributes["class"] = _attrControl.Name;
        }

        private void bindListItems(ListItemCollection items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                items[i].Attributes.Add("onclick", "javascript:checkSingleSelection(" + i.ToString() + "," + items.Count + " )");
                items[i].Attributes.Add("name", "name_" + _attrControl.AttributeName);

                AttributeOptions.Items.Add(items[i]);
            }
        }


        public string ScriptBlock()
        {
            StringBuilder bld = new StringBuilder();
            if (_attrControl.ListControlType == Framework.TemplateControls.ListType.checkbox && _attrControl.ListSelection == ListSelectionType.single)
            {

                bld.Append("<script type=\"text/javascript\">\r\n");
                bld.Append("function checkSingleSelection(index, count) \r\n ");
                bld.Append("{\r\n var containerID=\"" + AttributeOptions.ClientID + "\"\r\n");
                bld.Append("   var clickedItem = document.getElementById(containerID + \"_\" + index);" + "\r\n");

                bld.Append("  if (clickedItem == null)\r\n");
                bld.Append("        return;\r\n");
                bld.Append("  if(clickedItem.checked)\r\n");
                bld.Append("    {\r\n");
                bld.Append("    for (i = 0; i < count; i++) \r\n{");
                bld.Append("        var itmid =  containerID + \"_\" +  i;\r\n");
                bld.Append("        var chkitm=document.getElementById(itmid);\r\n");

                bld.Append("        if (chkitm != null) {\r\n");
                bld.Append("           if (index != i) {\r\n");
                bld.Append("                 if (chkitm.checked )  chkitm.checked = false;\r\n");
                bld.Append("  } } } } }\r\n");
                bld.Append("</script>\r\n");
            }
            return bld.ToString();
        }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            NameValueCollection result = new NameValueCollection();
            string parameterName = _attrControl.AttributeName.ToLower();
            result.Add(parameterName, GetValue());
            return result;
        }

        protected string GetTitleWrapperOpen
        {
            get
            {
                return (_attrControl.OnePageReg) ? "<div class=\"label\">" : "<label class=\"h2\">";
            }
        }
        protected string GetTitleWrapperClose
        {
            get
            {
                return (_attrControl.OnePageReg) ? "</div>" : "</label>";
            }
        }
        protected string GetFieldWrapperOpen
        {
            get { return (_attrControl.OnePageReg) ? "<div class=\"field\">" : string.Empty; }
        }
        protected string GetFieldWrapperClose
        {
            get { return (_attrControl.OnePageReg) ? "</div>" : string.Empty; }
        }
    }
}
