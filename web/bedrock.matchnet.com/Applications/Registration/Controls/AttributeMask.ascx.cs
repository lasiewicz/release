﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeMask : FrameworkControl, IAttributeControl
    {
        AttributeControl _attrControl;
        bool _useAttributeDescription;
        bool _includeBlanks;
        int _selectedValue;

        FrameworkControl _resourceControl;

        public string ContainerDivClientID { get { return divControl.ClientID; } }

        public bool IsOverlayReg { get; set; }

        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }

        public void Save()
        {

        }
        public ValidationMessage ValidationMessage { get { return txtValidation; } }
        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "inline");

        }
        public void SetValue(string value)
        {
            _selectedValue=Conversion.CInt(value);
            AttributeMaskControl.SetSelectedMasks(_selectedValue);
        }
        public string GetValue()
        {
            if(IsOverlayReg)
            {
                return AttributeMaskControl.ComputeMaskAttributeValueFromPost().ToString();
            }
            else
            {
                return AttributeMaskControl.ComputeMaskAttributeValue().ToString();
            }


        }
        public string ScriptBlock()
        {
            return "";
        }
        public  void PersistToCookie(HttpCookie cookie)
        {
            cookie[_attrControl.AttributeName] = GetValue();
        }

        public void Persist(ITemporaryPersistence persistence)
        {
            persistence[_attrControl.AttributeName] = GetValue();
        }

        public bool Validate()
        {
            bool valid=false;
            if (!_attrControl.RequiredFlag)
                return true;

            int mask = AttributeMaskControl.ComputeMaskAttributeValue();
            
            if (mask <= 0)
            {
                txtValidation.Visible = true;
              
                txtValidation.Text = _attrControl.GetValidationMessage(_attrControl.ErrorMessageRequired, g, _resourceControl);
            }
            else
            {
                txtValidation.Visible = false;
                txtValidation.Text = "";
                valid = true;
            }

            return valid;
        }

        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return AttributeMaskControl.ClientID;
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        } 

        public bool UseAttributeDescription { get { return _useAttributeDescription; } set { _useAttributeDescription = value; } }

        public bool IncludeBlanks { get { return _includeBlanks; } set { _includeBlanks = value; } }

        

        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }
        protected void Page_Load(object sender, EventArgs e)
        {

            txtCaption.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g,_resourceControl);
            //AttributeMaskControlValidator.Enabled = _attrControl.RequiredFlag;
            //AttributeMaskControlValidator.IsRequired=_attrControl.RequiredFlag;
            //AttributeMaskControlValidator.ErrorMessage = _attrControl.GetResourceString(_attrControl.ErrorMessageRequired, g);
            //AttributeMaskControlValidator.Enabled = _attrControl.RequiredFlag;
            //if (_attrControl.RequiredFlag)
            //    AttributeMaskControlValidator.ErrorMessage = _attrControl.GetResourceString(_attrControl.ErrorMessageRequired, g);
            AttributeMaskControl.ULClassName = "";
            AttributeMaskControl.DataSource = _attrControl.GetAttributeOptionAsDataTable(_useAttributeDescription, _includeBlanks, g, null);
            AttributeMaskControl.DataBind();
            AttributeMaskControl.SetSelectedMasks(_selectedValue);

            if (_attrControl.RequiredFlag)
            {
                AttributeMaskControl.CssClass += " mandatory";
            }
            else
            {
                AttributeMaskControl.CssClass += " optional";
            }

            divControl.Attributes["class"] = _attrControl.Name;
            
        }


        #region Page Life Cycle
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

         

        }
      
        #endregion

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            return new NameValueCollection();
        }

        
    }
}