﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeListBox.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeListBox" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>

<asp:Literal ID="litSelectFirst" runat="server" />


<div id="divControl" runat="server">


</asp:PlaceHolder>
<h2><mn:txt runat="server" id="txtCaption"  /></h2>
<uc2:ValidationMessage runat="server" ID="txtValidation" Visible=false />
<asp:ListBox ID="AttributeOptions" runat="server"></asp:ListBox>
</div>