﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeCheckBox.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeCheckBox" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<div id="divControl" runat="server"><%--This is all on one line to get rid of that pesky white space--%>
<%=GetOpenDiv("TNCInput")%><asp:CheckBox ID="AttributeCheckBoxControl" runat="server"></asp:CheckBox><%=GetCloseDiv() %><%=GetOpenDiv("TNCText")%><mn:Txt runat="server" ID="txtCaption" /><%=GetCloseDiv()%>
<uc2:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
</div>
