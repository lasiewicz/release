﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Matchnet.Web;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;

#region Matchnet Web App References
using Matchnet.Lib;
using Matchnet.Lib.Exceptions;
#endregion

#region Matchnet Service References
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using System.Collections;
using Matchnet.Web.Interfaces;

#endregion

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeRegionAjaxControl : FrameworkControl, IAttributeControl, IJQValidation
    {
        private Matchnet.Content.ValueObjects.Region.RegionID _RegionID = null;
        private int _CountryRegionID = Constants.NULL_INT;
        private int _StateRegionID = Constants.NULL_INT;
        private int _CityRegionID = Constants.NULL_INT;
        private int _LanguageID = Constants.NULL_INT;
        private int _PostalCodeRegionID = Constants.NULL_INT;
        private string _PostalCode = Matchnet.Constants.NULL_STRING;
        private int _DefaultRegionID = Constants.NULL_INT;
        private string _CityName = Matchnet.Constants.NULL_STRING;
        private int _ChildrenDepth = Constants.NULL_INT;
        private bool blnCityNotSelected = false;
        private const string SESSION_REMOTE_REG = "Session_remote_Reg";
        List<Region> _countries;
        string strDefaultItem;

        private const int REGION_ID_ISRAEL = 105;



        #region public properties
        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return country.ClientID;
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }
        public int RegionID
        {
            get
            {
                return GetRegionID();
            }
            set
            {
                if (_RegionID == null)
                {
                    _RegionID = new RegionID(value);
                }
                _RegionID.ID = value;
                PopulateHierarchy();
                updateControls();
            }
        }
        public int CountryRegionID
        {
            get
            {
                return _CountryRegionID;
            }
            set
            {
                _CountryRegionID = value;
                country.SelectedValue = _CountryRegionID.ToString();
            }
        }

        public int StateRegionID
        {
            get
            {
                return _StateRegionID;
            }
            set
            {
                _StateRegionID = value;
            }
        }
        public int CityRegionID
        {
            get
            {
                return _CityRegionID;
            }
            set
            {
                _CityRegionID = value;
            }
        }
        public int PostalCodeRegionID
        {
            get
            {
                return _PostalCodeRegionID;
            }
            set
            {
                _PostalCodeRegionID = value;
            }
        }
        public int LanguageID
        {
            get
            {
                return _LanguageID;
            }
            set
            {
                _LanguageID = value;
            }
        }

        public string CityName
        {
            get
            {
                return _CityName;
            }
            set
            {
                _CityName = value;
            }
        }

        public string PostalCode
        {
            get
            {
                return _PostalCode;
            }
            set
            {
                _PostalCode = value;
                postalCode.Text = _PostalCode;
            }
        }

        public int DefaultRegionID
        {
            get
            {
                return _DefaultRegionID;
            }
            set
            {
                _DefaultRegionID = value;
            }
        }
        public int ChildrenDepth
        {
            get
            {
                return _ChildrenDepth;
            }
            set
            {
                _ChildrenDepth = value;
            }
        }
        #endregion

        AttributeControl _attrControl;

        int _selectedValue;

        FrameworkControl _resourceControl;
        private bool _errorInZipCode;

        public string ContainerDivClientID { get { return divControl.ClientID; } }

        public bool IsOverlayReg { get; set; }

        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }
        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }
        public void Save()
        {

        }
        public ValidationMessage ValidationMessage { get { return txtValidation; } }
        public void SetValue(string value)
        {
            _selectedValue = Conversion.CInt(value);
            //AttributeMaskControl.SetSelectedMasks(_selectedValue);
            RegionID = _selectedValue;
        }
        public string GetValue()
        {
            try
            {
                return RegionID.ToString();
                //  return "3484027";
            }
            catch (Exception ex)
            { return "0"; }
        }
        public string ScriptBlock()
        {
            StringBuilder bld = new StringBuilder();


            bld.Append("<script type=\"text/javascript\">\r\n");

            bld.Append("function postalCode_PreCallBack() {\r\n ");
            bld.Append("obj = document.getElementById(\"lblCallBackMsg\");");
            bld.Append(" if (obj != null) {\r\n");
            bld.Append("   obj.style.display = \"inline\";\r\n}\r\n}\r\n");

            bld.Append("function postalCode_PostCallBack(list) {\r\n");
            bld.Append("obj = document.getElementById(\"lblCallBackMsg\");\r\n");
            bld.Append(" if (obj != null) {\r\n");
            bld.Append("  obj.style.display = \"none\";\r\n}\r\n}\r\n");

            bld.Append(" function ShowHideControl(strControl, strVisibility) {\r\n");
            bld.Append("  obj = document.getElementById(strControl.id);\r\n");
            bld.Append(" if (obj != null) {\r\n");
            bld.Append("  if (strVisibility == \"1\") {\r\n");
            bld.Append("     obj.style.visibility = \"visible\";\r\n}\r\n");
            bld.Append("  else {\r\n");
            bld.Append("    obj.style.visibility = \"hidden\";\r\n}\r\n}\r\n}\r\n");


            bld.Append("function popCityList()\r\n ");
            bld.Append("{\r\n");
            bld.Append("var parentRegionID;\r\n");
            bld.Append("var cityID;\r\n");
            bld.Append("var brandID=" + g.Brand.BrandID.ToString().TrimEnd() + ";\r\n");
            bld.Append(" if (document.forms[0].elements[\"StateDropDownID\"] != null){\r\n");
            bld.Append(" parentRegionID = document.forms[0].elements[\"StateDropDownID\"].value;\r\n}\r\n");
            bld.Append(" parentRegionID = document.forms[0].elements[\"StateDropDownID\"].value;\r\n");
            bld.Append(" if (parentRegionID != null && document.forms[0].elements[parentRegionID] != null) \r\n{\r\n");
            bld.Append(" parentRegionID = document.forms[0].elements[parentRegionID].value;\r\n}\r\n");
            bld.Append(" else {\r\n");
            bld.Append(" if (document.forms[0].elements[\"CountryDropDownID\"] != null) \r\n{\r\n");
            bld.Append("parentRegionID = document.forms[0].elements[\"CountryDropDownID\"].value;\r\n}\r\n");
            bld.Append("parentRegionID = document.forms[0].elements[parentRegionID].value;\r\n}\r\n");
            bld.Append(" cityID = document.forms[0].elements[\"CityID\"].value\r\n");
            bld.Append("window.open(\"/Applications/MemberProfile/CityList.aspx?plid=\" + brandID + \" &CityControlName=\"+ cityID + \"&ParentRegionID=\" + parentRegionID, \"\", \"height=415px,width=500px,scrollbars=yes\");\r\n}\r\n");


            bld.Append("</script>\r\n");

            return bld.ToString();
        }
        public void PersistToCookie(HttpCookie cookie)
        {
            cookie[_attrControl.AttributeName] = GetValue();
            if (IsOverlayReg)
            {
                saveControlValue(city);
            }
        }

        public void Persist(ITemporaryPersistence persistence)
        {
            persistence[_attrControl.AttributeName] = GetValue();
            if (IsOverlayReg)
            {
                saveControlValue(city);
            }
        }

        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "block");

        }
        public bool Validate()
        {
            bool valid = false;
            try
            {
                if (_RegionID != null && _RegionID.ID > 0)
                {
                    valid = true;
                }
                else
                {
                    string msgText = _attrControl.GetValidationMessage(_attrControl.ErrorMessageRequired, g, _resourceControl);
                    if (_errorInZipCode)
                    {
                        msgText = g.GetResource("ERROR_IN_ZIP", _resourceControl);
                    }
                    txtValidation.Text = msgText;
                    txtValidation.Visible = true;

                }
                return valid;
            }
            catch (Exception ex)
            {
                txtValidation.Text = _attrControl.GetValidationMessage(_attrControl.ErrorMessageValidation, g, _resourceControl);
                txtValidation.Visible = true;

                return valid;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.country.SelectedIndexChanged += new System.EventHandler(this.country_SelectedIndexChanged);
            this.state.SelectedIndexChanged += new EventHandler(state_SelectedIndexChanged);
            this.postalCode.TextChanged += new EventHandler(postalCode_TextChanged);
            this.frCityRegion.SelectedIndexChanged += new System.EventHandler(this.frCityRegion_SelectedIndexChanged);

            string CityPopup = Matchnet.Constants.NULL_STRING;
            Page.RegisterHiddenField("StateDropDownID", state.UniqueID);
            Page.RegisterHiddenField("CountryDropDownID", country.UniqueID);
            Page.RegisterHiddenField("CityID", city.UniqueID);
            //  CityPopup = FrameworkGlobals.MakePopCityListJavaScript(_g.Brand.BrandID, country.ClientID, city.ClientID);
            // litPopCityCountry.Text = country.ClientID;
            // CityPopup = FrameworkGlobals.MakePopCityListJavaScript(_g.Brand.BrandID, state.ClientID, city.ClientID);
            // litPopCityState.Text = state.ClientID;

            //RegionID = getRegionIDForControl();
            //PopulateCountry();
            //PopulateState();
            //PopulatePostalCode();
            //PopulateCity();
            //updateControls();
            _errorInZipCode = false;
        }

        private void ResetAutoCompleteControls()
        {
            state.CssClass = state.CssClass.Replace("hide regionid", string.Empty);
            city.CssClass = city.CssClass.Replace("autocomplete", string.Empty);
            ddlCities.CssClass = ddlCities.CssClass.Replace("hide", string.Empty);
            txtState.Visible = true;
            lblLookUpCity.Visible = true;
            if (!string.IsNullOrEmpty(divState.Attributes["class"]))
            {
                divState.Attributes["class"] = divState.Attributes["class"].Replace("hide", string.Empty);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetAutoCompleteControls();


            // if (!(Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].IndexOf("country") < 0 && Request.Form["__EVENTTARGET"].IndexOf("state") < 0))
            if (!PostBack())
            {
                clearControlValue(postalCode);
            }

            loadControlValue(postalCode);
            if (_RegionID == null || _RegionID.ID < 0)
            {
                RegionID = ConstantsTemp.REGION_US;
            }
            updateControls();
            if (_attrControl == null)
            {
                _attrControl = new AttributeControl();

            }

            if (_attrControl.RequiredFlag)
            {
                country.CssClass += " mandatory";
                state.CssClass += " mandatory";
                frCityRegion.CssClass += " mandatory";
                city.CssClass += " mandatory";
                postalCode.CssClass += " mandatory";
                ddlCities.CssClass += " optional";
            }
            else
            {
                country.CssClass += " optional";
                state.CssClass += " optional";
                frCityRegion.CssClass += " optional";
                city.CssClass += " optional";
                postalCode.CssClass += " optional";
                ddlCities.CssClass += " optional";
            }

            divControl.Attributes["class"] = _attrControl.Name;


            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR &&
                ((!string.IsNullOrEmpty(Request[country.UniqueID]) && Request[country.UniqueID] == ConstantsTemp.REGIONID_FRANCE.ToString())
                ||
                (string.IsNullOrEmpty(Request[country.UniqueID]) && country.SelectedValue == ConstantsTemp.REGIONID_FRANCE.ToString())
                ))
            {
                // frCityRegion.SelectedValue = "-1";
                frCityRegion.Visible = false;
                state.Visible = true;
                txtState.Visible = true;
                txtFRCityRegion.Visible = false;
                if (_attrControl.OnePageReg)
                {
                    txtFRCityRegionSeperator.Visible = true;
                    txtFRCityRegionSeperator.ResourceConstant = "REGION_CITY_COLON";
                }
                divCity.Style.Add("visibility", "visible");
                divCity.Attributes.Remove("class");
                _CityName = string.Empty;

                if (!(Request[frCityRegion.UniqueID] == "-1" || !string.IsNullOrEmpty(Request[state.UniqueID])))
                {
                    state.SelectedValue = Request[frCityRegion.UniqueID] != null ?
                                                                                   Request[frCityRegion.UniqueID].Remove(0, 1) :
                                                                                   string.Empty;

                    frCityRegion.Visible = true;
                    if (_selectedValue == 9795409)
                    {
                        frCityRegion.SelectedIndex = 8;
                    }
                    state.Visible = false;
                    _CityName = frCityRegion.SelectedItem.Text;
                    txtState.Visible = true;

                    txtFRCityRegion.Visible = true;

                    //show region city seperator in one page reg
                    if (_attrControl.OnePageReg)
                    {
                        txtFRCityRegionSeperator.ResourceConstant = "REGION_CITY_SEPERATOR";
                        txtFRCityRegionSeperator.Visible = true;
                    }
                    divCity.Style.Add("visibility", "hidden");
                    divCity.Attributes.Add("class", "hide");
                }

            }
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            saveControlValues(postalCode);
            // Only at page load 
            #region Select city
            lblCountry.Text = _attrControl.GetResourceString(lblCountry.ResourceConstant, g, _resourceControl);
            lblLookUpCity.Text = _attrControl.GetResourceString(lblLookUpCity.ResourceConstant, g, _resourceControl);
            if (_CountryRegionID == ConstantsTemp.REGION_US)
            {
                lblPostal.Text = _attrControl.GetResourceString("DDL_ZIPCODE", g, _resourceControl);
            }
            else
            {
                lblPostal.Text = _attrControl.GetResourceString("DDL_POSTAL", g, _resourceControl);
            }
            // GetRegionID();
            //if (Request.Form["__EVENTTARGET"]!= null &&  Request.Form["__EVENTTARGET"].IndexOf("postalCode") > 0)
            //{
            if (blnCityNotSelected || (postalCode.Text != string.Empty && _CountryRegionID == ConstantsTemp.REGION_US))
            {
                RegionCollection colCities = RegionSA.Instance.RetrieveCitiesByPostalCode(postalCode.Text);
                if (colCities.Count > 1)
                {
                    ddlCities.DataSource = colCities;
                    ddlCities.DataTextField = "Description";
                    ddlCities.DataValueField = "RegionID";
                    ddlCities.DataBind();
                    ddlCities.Items.Insert(0, strDefaultItem);
                    if (Request[ddlCities.UniqueID] != null)
                    {
                        FrameworkGlobals.SelectItem(ddlCities, Request[ddlCities.UniqueID]);
                    }
                    else
                    {
                        FrameworkGlobals.SelectItem(ddlCities, this._PostalCodeRegionID.ToString());
                    }
                    divCities.Style.Add("visibility", "visible");
                    //TrCities.Style.Add("Display","inline");
                }
                else
                {
                    divCities.Style.Add("visibility", "hidden");
                    //TrCities.Style.Add("Display","none");
                    ddlCities.Items.Clear();
                }
                blnCityNotSelected = false;

            }
            else
            {
                divCities.Style.Add("visibility", "hidden");
                //TrCities.Style.Add("Display","none");
                ddlCities.Items.Clear();
            }
            // }
            #endregion Select City

            // clear city field when dropdowns are changed
            country.Attributes.Add("onChange", @"try{document." + WebConstants.HTML_FORM_ID + ".elements['" + city.ClientID + @"'].value='';}catch(e){}");

            //when state dropdown is changed, clear city field and show Look up City link if applicable
            string stateOnChangeJS = @"try{ document." + WebConstants.HTML_FORM_ID + ".elements['" + city.ClientID + @"'].value=''; } catch(e){} ShowCityLink();";
            state.Attributes.Add("onChange", stateOnChangeJS);

            if (IsOverlayReg || _attrControl.IsSplash)
            {
                anthemPnl1.PostCallBackFunction = "autocompleteCities";
                if (_attrControl.OnePageReg)
                {
                    anthemPnl1.PreCallBackFunction = "BeforeRegionCallback";
                }
                if (_attrControl.OnePageReg)
                {
                    postalCode.AutoCallBack = false;
                }

                litScript.Text = ScriptBlock();

                if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew && _CountryRegionID == REGION_ID_ISRAEL)
                {
                    SortedList<string, string> citiesList = new SortedList<string, string>();


                    RegionCollection myRegions;
                    if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
                    {
                        myRegions = RegionSA.Instance.RetrieveChildRegions(_CountryRegionID, g.Brand.Site.LanguageID, WebConstants.TEMP_TRANSLATION_ID, true);
                    }
                    else
                    {
                        myRegions = RegionSA.Instance.RetrieveChildRegions(_CountryRegionID, g.Brand.Site.LanguageID, WebConstants.TEMP_TRANSLATION_ID);
                    }


                    foreach (Region regionState in myRegions)
                    {
                        int parentRegionID = regionState.RegionID;

                        RegionCollection citiesCollection = RegionSA.Instance.RetrieveChildRegions(parentRegionID, g.Brand.Site.LanguageID);

                        foreach (Region cityReg in citiesCollection)
                        {
                            citiesList.Add(cityReg.Description.Replace("\"", "\\\""), parentRegionID.ToString());

                        }
                    }

                    string citiesArray = "var citiesArr = {0};";

                    if (_attrControl.IsMobileReg)
                    {
                        ddlFullCityList.Visible = true;
                        ddlFullCityList.Items.Add(new ListItem(g.GetResource("DDL_CITY", this), "0"));

                        foreach (KeyValuePair<string, string> cityItem in citiesList)
                        {
                            ddlFullCityList.Items.Add((new ListItem(cityItem.Key, cityItem.Value)));
                        }
                    }

                    citiesArray = string.Format("var citiesArr = {0};",
                       Newtonsoft.Json.JsonConvert.SerializeObject(citiesList));

                    StringBuilder bld = new StringBuilder();
                    bld.Append("\n<script type=\"text/javascript\">\r\n");

                    bld.Append(citiesArray);

                    bld.Append("</script>\r\n");

                    litScript.Text += bld.ToString();

                    state.CssClass += " hide regionid";
                    divState.Attributes.Add("class", "hide");
                    if (_attrControl.IsMobileReg)
                    {
                        city.CssClass += " city-mobile";
                    }
                    else
                    {
                        city.CssClass += " autocomplete";
                    }

                    ddlCities.CssClass += " hide";
                    txtState.Visible = false;
                    lblLookUpCity.Visible = false;

                    loadControlValue(city);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["BadZip"]) && !IsPostBack)
                {
                    postalCode.Text = Request.QueryString["BadZip"];
                    if (!string.IsNullOrEmpty(Request.QueryString["PopCities"]))
                    {
                        postalCode_TextChanged(this, null);
                    }

                }

                if (divCities.Style["visibility"] == "hidden")
                {
                    divCities.Style.Add("display", "none");
                }


            }

            if (!string.IsNullOrEmpty(Request["iframeopr"]) && (Request["iframeopr"] == "academic" || Request["iframeopr"] == "walla" || Request["iframeopr"] == "ynet1" || Request["iframeopr"] == "ynet2"))
            {
                city.Text = g.GetResource("DDL_CITY", this);
                state.Items.Insert(0, new ListItem(g.GetResource("DDL_REGION", this), "-2"));
            }
        }
        private void postalCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (CountryRegionID == Matchnet.Lib.ConstantsTemp.REGION_US)
                {
                    if (Anthem.Manager.IsCallBack || !string.IsNullOrEmpty(Request.QueryString["PopCities"]))
                    {
                        RegionCollection colCities = RegionSA.Instance.RetrieveCitiesByPostalCode(postalCode.Text);
                        if (colCities.Count > 1)
                        {
                            ddlCities.DataSource = colCities;
                            ddlCities.DataTextField = "Description";
                            ddlCities.DataValueField = "RegionID";
                            ddlCities.DataBind();
                            ddlCities.Items.Insert(0, strDefaultItem);
                            ddlCities.UpdateAfterCallBack = true;
                            divCities.Style.Add("visibility", "visible");
                            // Anthem.Manager.AddScriptForClientSideEval("ShowHideControl(" + TrCities.ClientID + ",1); if ( window.PopulateCities ){ PopulateCities('true'); } ");
                        }
                        else
                        {	// Hiding the cities dropdown
                            //							Anthem.Manager.AddScriptForClientSideEval("ShowHideControl(" + TrCities.ClientID + ",0);");	
                            //Anthem.Manager.AddScriptForClientSideEval("ShowHideControl(" + TrCities.ClientID + ",0); if ( window.PopulateCities ){ PopulateCities('false'); } ");
                            ddlCities.Items.Clear();
                            ddlCities.UpdateAfterCallBack = true;
                        }
                    }
                }
                else
                {
                    //Anthem.Manager.AddScriptForClientSideEval("ShowHideControl(" + TrCities.ClientID + ",0);");	
                    //Anthem.Manager.AddScriptForClientSideEval("ShowHideControl(" + TrCities.ClientID + ",0); if ( window.PopulateCities ){ PopulateCities('false'); } ");
                    ddlCities.Items.Clear();
                    ddlCities.UpdateAfterCallBack = true;
                }
                saveControlValues(postalCode);

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
        private void country_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            _CountryRegionID = Matchnet.Conversion.CInt(country.SelectedValue);
            Matchnet.Content.ValueObjects.Region.Region regionInfo = RegionSA.Instance.RetrieveRegionByID(_CountryRegionID, _LanguageID);
            _ChildrenDepth = regionInfo.ChildrenDepth;

            //Hack warning: Because the PickRegion control always initially populates with the US region selected,
            //this event fires even when it shouldn't (for example, when RegStep1 is submitted).  Unfortunately, 
            //this was wiping out the selected value in the state dropdown.  The code below gets around this problem.
            //When time permits, this control should be gutted and re-written properly.
            bool countrySelectedIndexChanged = (country.SelectedValue != Request[country.UniqueID]);
            if (!countrySelectedIndexChanged)
            {
                _StateRegionID = Constants.NULL_INT;

            }
            else
            {
                _StateRegionID = Conversion.CInt(Request[state.UniqueID]);
                postalCode.Text = "";
                ddlCities.Items.Clear();
                // clearControlValues(postalCode);
            }

            _PostalCode = Matchnet.Constants.NULL_STRING;
            _CityName = Matchnet.Constants.NULL_STRING;
            SetZipVsPostalCode();
            PopulateCountry();
            PopulateState();
            updateControls();
            //  UpdatePanels();
        }
        private void state_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Request[state.UniqueID] != null)
            {
                if (CountryContainsState(_CountryRegionID, Conversion.CInt(Request[state.UniqueID])))
                {
                    state.SelectedValue = Request[state.UniqueID];
                    _StateRegionID = Conversion.CInt(state.SelectedValue);
                }
                // litPopCityState.Text = state.ClientID;
            }
        }
        private void setupRegionControl()
        {
            if (phPickRegion != null)
            {

                if (_RegionID == null)
                    RegionID = getRegionIDForControl();
                PopulateHierarchy();
                PopulateCountry();
                PopulateState();
                PopulateCity();
                updateControls();
            }
        }
        private void updateControls()
        {

            //divCountry.Style.Add("visibility", "visible");
            //divCities.Style.Add("visibility", "hidden");
            //divCity.Style.Add("visibility", "hidden");
            //divState.Style.Add("visibility", "hidden");


            if (_CountryRegionID == ConstantsTemp.REGION_US || _CountryRegionID == ConstantsTemp.REGIONID_CANADA)
            {
                divCity.Visible = false;
                divState.Visible = false;
                //litPopCityState.Visible = false;
                divPostalCode.Visible = true;
                divCities.Style.Add("visibility", "hidden");
                if (_CountryRegionID == ConstantsTemp.REGION_US)
                {
                    postalCode.MaxLength = 5;
                }
                else
                {
                    postalCode.MaxLength = 7;
                    divCities.Style.Add("visibility", "hidden");
                    //TrCities.Style.Add("Display","none");
                }
                litShowCityLinkJS.Visible = false; //US and Canada use postal code instead of city so no need to run the JavaScript to hide the "Look up City" link
            }
            else if (_ChildrenDepth == 3)
            {
                divCities.Style.Add("visibility", "hidden");
                //TrCities.Style.Add("Display","none");
                divState.Visible = false;
                // litPopCityState.Visible = false;
                //litPopCityCountry.Visible = true;
                divCity.Visible = true;
                divPostalCode.Visible = false;
                litShowCityLinkJS.Visible = true;
                litShowCityLinkJS.Text = "LookupCityLink.style.visibility = 'visible'";
                //cityLinkVisibility = "visible";
            }
            else
            {
                divCities.Style.Add("visibility", "hidden");
                //TrCities.Style.Add("Display","none");
                divCity.Visible = true;
                divState.Visible = true;
                // litPopCityState.Visible = true;
                // litPopCityCountry.Visible = false;
                divPostalCode.Visible = false;
                litShowCityLinkJS.Visible = true;
                // we want ShowCityLink to be called just in case this function was called
                // during a postback because the city was spelled wrong, in this case a state has been selected 
                // and so the citylink should be visible. -mroberts TT #12489
                litShowCityLinkJS.Text = "ShowCityLink()";
                //cityLinkVisibility = "hidden";
            }



        }

        private int getRegionIDForControl()
        {
            int regionID = 0;


            return g.Brand.Site.DefaultRegionID;

        }

        #region methods
        private void PopulateCountry()
        {
            if ((_CountryRegionID == Constants.NULL_INT)
                || (_CountryRegionID == 0))
            {
                return;
            }
            // determine if state dropdown should be shown or not

            RegionCollection countries = RegionSA.Instance.RetrieveCountries(g.Brand.Site.LanguageID);
            populateRegionList(countries);

            country.DataSource = _countries;
            country.DataTextField = "Description";
            country.DataValueField = "RegionID";
            country.DataBind();
            //try
            //{
            //    country.DataBind();
            //}
            //catch (ArgumentOutOfRangeException ex)
            //{
            //}

            country.SelectedValue = _CountryRegionID.ToString();
        }

        private void PopulateState()
        {
            if ((_ChildrenDepth == 3))
            {
                state.Items.Clear();
                return;
            }
            RegionCollection myRegions;
            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                myRegions = RegionSA.Instance.RetrieveChildRegions(_CountryRegionID, g.Brand.Site.LanguageID, WebConstants.TEMP_TRANSLATION_ID, true);
            }
            else
            {
                myRegions = RegionSA.Instance.RetrieveChildRegions(_CountryRegionID, g.Brand.Site.LanguageID, WebConstants.TEMP_TRANSLATION_ID);
            }

            try
            {	// Determine how the State/Region label should render.
                int regionMask = myRegions[0].Mask;

                if (regionMask != 8)
                {
                    regionMask -= 2;
                }
                this.txtState.RegionMask = regionMask;
            }
            catch { }

            state.DataSource = myRegions;
            state.DataTextField = "Description";
            state.DataValueField = "RegionID";
            try
            {
                state.DataBind();
            }
            catch { }

            // Insert a blank record
            if (_attrControl.ShowLabelInsideInput && _attrControl.IsMobileReg)
            {
                state.Items.Insert(0, new ListItem(g.GetResource("DDL_REGION", this), string.Empty));
            }
            else
            {
                state.Items.Insert(0, new ListItem(string.Empty, string.Empty));
            }




            if ((_StateRegionID != Constants.NULL_INT) && (_StateRegionID != 0))
            {
                state.SelectedValue = _StateRegionID.ToString();
            }


        }

        private void PopulateCity()
        {
            city.Text = _CityName;
            //saveControlValue(city);
        }

        protected void frCityRegion_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (frCityRegion.SelectedValue == "-1")
            {
            }
            PopulateState();
            PopulateCity();
        }

        private void PopulatePostalCode()
        {
            if (_PostalCode != Matchnet.Constants.NULL_STRING)
            {
                postalCode.Text = _PostalCode.ToString();
            }
        }

        private void populateRegionList(RegionCollection coll)
        {
            try
            {
                if (_countries == null)
                    _countries = new List<Region>();
                else
                    _countries.Clear();
                for (int i = 0; i < coll.Count; i++)
                {
                    if (coll[i].RegionID == ConstantsTemp.REGION_US)
                    {
                        _countries.Insert(0, coll[i]);
                    }
                    else if (coll[i].RegionID == ConstantsTemp.REGIONID_CANADA)
                    { _countries.Insert(0, coll[i]); }
                    else
                    {
                        if (coll[i].RegionID > -1) // Removing "undefined" from the options list
                        {
                            _countries.Add(coll[i]);
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }
        private void PopulateHierarchy()
        {
            // get data from resultset

            RegionLanguage region = RegionSA.Instance.RetrievePopulatedHierarchy(_RegionID.ID, _LanguageID);

            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew && Matchnet.Conversion.CInt(region.CountryRegionID) == REGION_ID_ISRAEL)
            {
                _LanguageID = g.Brand.Site.LanguageID;
                region = RegionSA.Instance.RetrievePopulatedHierarchy(_RegionID.ID, _LanguageID);

            }

            // fill data
            _CountryRegionID = Matchnet.Conversion.CInt(region.CountryRegionID);
            _StateRegionID = Matchnet.Conversion.CInt(region.StateRegionID);
            _CityRegionID = Matchnet.Conversion.CInt(region.CityRegionID);
            if (_CityRegionID > 0)
            {
                blnCityNotSelected = false;
            }
            _CityName = region.CityName;
            _PostalCodeRegionID = Matchnet.Conversion.CInt(region.PostalCodeRegionID);
            _PostalCode = region.PostalCode;
            if (_PostalCode != Matchnet.Constants.NULL_STRING)
                _CityName = "";
            // necessary to determine showing of state dropdown or not
            Matchnet.Content.ValueObjects.Region.Region regionInfo = RegionSA.Instance.RetrieveRegionByID(_CountryRegionID, _LanguageID);
            _ChildrenDepth = regionInfo.ChildrenDepth;

            // populate drop downs and select values
            PopulateCountry();
            PopulateState();
            PopulateCity();
            PopulatePostalCode();
        }
        private void SetZipVsPostalCode()
        {
            // memberprofile.pickregion inherits from this control and this parent control is referencing a control in the child control..
            try
            {
                if (_CountryRegionID == ConstantsTemp.REGION_US)
                {
                    lblPostal.Text = _attrControl.GetResourceString("DDL_ZIPCODE", g, _resourceControl);
                }
                else
                {
                    lblPostal.Text = _attrControl.GetResourceString("DDL_POSTAL", g, _resourceControl);
                }

            }
            catch
            {
            }
        }
        private bool CountryContainsState(int pCountryID, int pStateID)
        {
            RegionCollection rc = RegionSA.Instance.RetrieveChildRegions(pCountryID, g.Brand.Site.LanguageID);
            foreach (Region r in rc)
            {
                if (r.RegionID == pStateID)
                    return true;
            }
            return false;
        }
        #endregion
        private int GetRegionID()
        {
            _CountryRegionID = Matchnet.Conversion.CInt(Request[country.UniqueID]);
            if (state.SelectedValue != string.Empty)
            {
                _StateRegionID = Matchnet.Conversion.CInt(Request[state.UniqueID]);
            }
            else
            {
                // Handles situations where changing from countries with states to country with states
                //where the state change event handler will will get trigger because there were no states
                // previously. TT17150
                if (CountryContainsState(_CountryRegionID, Conversion.CInt(Request[state.UniqueID])))
                {
                    state.SelectedValue = Request[state.UniqueID];
                    _StateRegionID = Conversion.CInt(state.SelectedValue);
                }
            }

            /*
                        _PostalCode = postalCode.Text.Trim();
                        _CityName = city.Text.Trim();
             */

            _PostalCode = (string.IsNullOrEmpty(Request[postalCode.UniqueID]))
                              ? string.Empty
                              : Request[postalCode.UniqueID].Trim();
            _CityName = (string.IsNullOrEmpty(Request[city.UniqueID]))
                              ? string.Empty
                              : Request[city.UniqueID].Trim();

            _RegionID = null;

            // If U.S. then save postal code
            if (_CountryRegionID == ConstantsTemp.REGION_US || _CountryRegionID == ConstantsTemp.REGIONID_CANADA)
            {
                try
                {
                    //#region Select city 
                    RegionCollection colCities = RegionSA.Instance.RetrieveCitiesByPostalCode(_PostalCode);
                    if (_CountryRegionID == ConstantsTemp.REGION_US && colCities.Count > 1 && !(g.IsRemoteRegistration || g.Session.Get(SESSION_REMOTE_REG) != null))
                    {

                        try
                        {
                            if (Request[ddlCities.UniqueID] != null)
                            {
                                RegionID rgnID = new RegionID(Int32.Parse(Request[ddlCities.UniqueID]));
                                int i;
                                for (i = 0; i < colCities.Count; i++)
                                {
                                    if (colCities[i].RegionID == rgnID.ID)
                                    {
                                        _RegionID = rgnID;
                                        break;
                                    }
                                }
                                if (i >= colCities.Count) // no selected item found
                                {
                                    divCities.Style.Add("visibility", "visible");
                                    //TrCities.Style.Add("Display","inline");
                                    blnCityNotSelected = true;
                                }

                            }
                            else
                            {
                                _RegionID = new RegionID(_CityRegionID);
                            }
                            //else
                            //{
                            //    divCities.Style.Add("visibility", "visible");
                            //    //TrCities.Style.Add("Display","inline");
                            //    blnCityNotSelected = true;
                            //}
                        }
                        catch (Exception e)
                        {
                            divCities.Style.Add("visibility", "visible");
                            //TrCities.Style.Add("Display","inline");
                            blnCityNotSelected = true;
                            throw new MatchnetException("No city is selected. Please select a city.", "PickRegion.GetRegionID", e);
                        }
                    }
                    else
                    //#endregion
                    {
                        _RegionID = RegionSA.Instance.FindRegionIdByPostalCode(_CountryRegionID, _PostalCode);
                        if (_RegionID == null)
                        {
                            _errorInZipCode = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new MatchnetException("Invalid PostalCode [PostalCode: " + _PostalCode.ToString() +
                        " CountryRegionID: " + _CountryRegionID.ToString() + "]",
                        "PickRegion.GetRegionID", ex);
                    _errorInZipCode = true;
                }
            }
            // Other Countries
            else
            {
                try
                {
                    // Countries without states
                    if (_ChildrenDepth == 3)
                    {
                        _RegionID = RegionSA.Instance.FindRegionIdByCity(_CountryRegionID, _CityName, _g.Brand.Site.LanguageID);
                    }
                    // Countries with states
                    else
                    {
                        if (!string.IsNullOrEmpty(Request[frCityRegion.UniqueID]))
                        {
                            _StateRegionID = int.Parse(Request[frCityRegion.UniqueID].Remove(0, 1));
                            _CityName = frCityRegion.Items.FindByValue(Request[frCityRegion.UniqueID]).Text;
                        }
                        else if (_StateRegionID <= 0 && !string.IsNullOrEmpty(Request[state.UniqueID]))
                        {
                            _StateRegionID = int.Parse(Request[state.UniqueID]);
                        }
                        _RegionID = RegionSA.Instance.FindRegionIdByCity(_StateRegionID, _CityName, _g.Brand.Site.LanguageID);
                    }
                }
                catch (Exception ex)
                {
                    throw new MatchnetException("Invalid City [City:" + city.ToString() + " CountryRegionID: " +
                        _CountryRegionID.ToString() + "]", "PickRegion.GetRegionID", ex); ;
                }
            }

            //if (_RegionID == null)
            //{
            //    throw new MatchnetException("Region not found. [Country: " + _CountryRegionID.ToString() +
            //        " State: " + _StateRegionID.ToString() +
            //        " City: " + _CityName + " PostalCode: " + _PostalCode + "]",
            //        "PickRegion.GetRegionID");
            //}

            //if (_RegionID.ID <= 0)
            //{
            //    throw new MatchnetException("Region not found. [Country: " + _CountryRegionID.ToString() +
            //        " State: " + _StateRegionID.ToString() +
            //        " City: " + _CityName + " PostalCode: " + _PostalCode + "]",
            //        "PickRegion.GetRegionID");
            //}

            if (_RegionID != null)
            {
                return _RegionID.ID;
            }
            else
            {
                return Constants.NULL_INT;
            }
        }




        private bool PostBack()
        {
            bool postback = false;
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].IndexOf("country") > 0 && Request.Form["__EVENTTARGET"].IndexOf("state") > 0 && Request.Form["__EVENTTARGET"].IndexOf("city") > 0 && Request.Form["__EVENTTARGET"].IndexOf("Continue") > 0 && Request.Form["__EVENTTARGET"].IndexOf("Previous") < 0 && Request.Form["__EVENTTARGET"].IndexOf("Next") > 0)
            {
                postback = true;

            }
            return postback;
        }

        #region Handling Sessions
        protected string getSessionValue(System.Web.UI.Control control)
        {
            return g.Session[control.ID + "_Value"];
        }
        private void loadControlValues()
        {
            loadControlValues(Controls);
        }


        private void loadControlValues(ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                loadControlValues(control.Controls);
                loadControlValue(control);
            }
        }

        private void loadControlValue(Control control)
        {
            string controlValue = getSessionValue(control);

            if (controlValue != null)
            {
                switch (control.GetType().Name)
                {
                    case "TextBox":
                        TextBox textBox = control as TextBox;
                        if (controlValue != string.Empty)
                        {
                            textBox.Text = controlValue.Trim();
                        }
                        break;
                    case "DropDownList":
                        DropDownList dropDownList = control as DropDownList;
                        FrameworkGlobals.SelectItem(dropDownList, controlValue);
                        break;
                }
            }
        }
        private void loadControlValues(Control mControl)
        {
            loadControlValue(mControl);
            foreach (Control control in mControl.Controls)
            {
                loadControlValues(control.Controls);
                loadControlValue(control);
            }

        }
        //Have to do this because viewstate is off -- see http://weblogs.asp.net/despos/archive/2005/03/16/394834.aspx
        private void saveControlValues()
        {
            saveControlValues(Controls);
        }
        private void saveControlValues(ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                saveControlValues(control.Controls);
                saveControlValue(control);
            }
        }
        protected void saveControlValues(Control mControl)
        {
            saveControlValue(mControl);
            foreach (Control control in mControl.Controls)
            {
                saveControlValues(control.Controls);
                saveControlValue(control);
            }
        }
        protected void saveControlValue(Control control)
        {
            string controlValue = null;

            switch (control.GetType().Name)
            {
                case "TextBox":
                    TextBox textBox = control as TextBox;
                    controlValue = textBox.Text;
                    if (string.IsNullOrEmpty(controlValue))
                    {
                        controlValue = (string.IsNullOrEmpty(Request[textBox.UniqueID]))
                                           ? string.Empty
                                           : Request[textBox.UniqueID].Trim();
                    }

                    //HACK: When we load the control value from the session, if the value is not present,
                    //the Session service returns string.empty.  Therefore, we store a single space here
                    //to distinguish an explicit empty string value from the absence of a value.
                    if (controlValue == string.Empty)
                    {
                        controlValue = " ";
                    }
                    break;
                case "DropDownList":
                    DropDownList dropDownList = control as DropDownList;
                    controlValue = dropDownList.SelectedValue;
                    break;
            }

            if (controlValue != null)
            {
                g.Session.Add(control.ID + "_Value", controlValue, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                //attention hack!!!
                try
                {
                    int _CountryRegionIDTemp = _CountryRegionID;
                    int regionid = GetRegionID();
                    _CountryRegionID = _CountryRegionIDTemp;
                    if (regionid > 0)
                    {
                        g.Session.Add("REGISTRATION_REGIONID", regionid, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                    }
                }
                catch (Exception exception)
                {

                }

            }
        }

        private void clearControlValues()
        {
            clearControlValues(Controls);
        }
        private void clearControlValues(Control mControl)
        {
            clearControlValues(mControl.Controls);
            clearControlValue(mControl);
        }

        private void clearControlValues(ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                clearControlValues(control.Controls);
                clearControlValue(control);
            }
        }
        private void clearControlValue(Control control)
        {
            switch (control.GetType().Name)
            {
                case "TextBox":
                case "DropDownList":
                    g.Session.Remove(control.ID + "_Value");
                    break;
            }
        }
        #endregion

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            NameValueCollection result = new NameValueCollection();
            result.Add("CountryRegionID", _CountryRegionID.ToString());
            result.Add("RegionID", GetValue());
            return result;
        }

        protected string GetTitleWrapperOpen
        {
            get
            {
                return (_attrControl.OnePageReg) ? "<div class=\"label\">" : "<h2>";
            }
        }
        protected string GetTitleWrapperClose
        {
            get
            {
                return (_attrControl.OnePageReg) ? "</div>" : "</h2>";
            }
        }
        protected string GetFieldWrapperOpen
        {
            get { return (_attrControl.OnePageReg) ? "<div class=\"field\">" : string.Empty; }
        }
        protected string GetFieldWrapperClose
        {
            get { return (_attrControl.OnePageReg) ? "</div>" : string.Empty; }
        }


        #region IJQValidation Members

        public string InputFieldID
        {
            get { return city.UniqueID; }
        }

        public List<ValidationDictionaryEntry> GetValidationRules
        {
            get
            {
                List<ValidationDictionaryEntry> result = new List<ValidationDictionaryEntry>();

                if (frCityRegion.Visible)
                {
                    ValidationDictionaryEntry rulesfr = new ValidationDictionaryEntry();
                    rulesfr.UniqueID = frCityRegion.UniqueID;
                    rulesfr.ValidationEntryList.Add(new ValidationEntry()
                    {
                        errorKey = "required",
                        message =
                            _attrControl.GetValidationMessage(
                            _attrControl.ErrorMessageRequired, g, _resourceControl),
                        value = "true"
                    });

                    result.Add(rulesfr);
                }


                ValidationDictionaryEntry rules = new ValidationDictionaryEntry();
                rules.UniqueID = city.UniqueID;
                rules.ValidationEntryList.Add(new ValidationEntry()
                                                  {
                                                      errorKey = "required",
                                                      message =
                                                          _attrControl.GetValidationMessage(
                                                          _attrControl.ErrorMessageRequired, g, _resourceControl),
                                                      value = "true"
                                                  });

                string resourceKey = "WRONG_CITY";
                if (_attrControl.IsMobileReg)
                {
                    resourceKey = "WRONG_CITY_MOBILE";
                }

                rules.ValidationEntryList.Add(new ValidationEntry()
                {


                    errorKey = "wrongCity",
                    message =
                        _attrControl.GetValidationMessage(
                       resourceKey, g, _resourceControl),
                    value = "true"
                });


                result.Add(rules);
                return result;
            }
        }

        #endregion
    }
}
