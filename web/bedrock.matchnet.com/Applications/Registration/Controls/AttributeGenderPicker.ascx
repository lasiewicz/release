﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeGenderPicker.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeGenderPicker" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="PickGenderMask" Src="~/Framework/Ui/FormElements/PickGenderMask.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<div id="divControl" runat="server">
<h2><mn:txt runat="server" id="txtCaption"  /></h2>
<uc2:ValidationMessage runat="server" ID="txtValidation" Visible=false />
<uc1:PickGenderMask ID="AttributeGenderPickerControl" runat="server" SeekingGender="false"/></div>
