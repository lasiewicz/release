﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacebookRegistration.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Controls.FacebookRegistration" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<%--Move these styles into external stylesheet if/when FB reg goes to other sites/passes 50/50 split testing--%>
<style type="text/css">
/* .facebook class adds facebook typography to an element */
.facebook {font-family: "lucida grande",tahoma,verdana,arial,sans-serif; font-size: 11px; font-weight: normal;}
.facebook a:link, .facebok a:active, .facebok a:visited, .facebok a:hover {color: #3B5998; font-size: 11px;}
.facebook input[disabled], .facebook select[disabled] {border: 1px solid #b9c4d5; padding: 2px; background-color: #eeeeee; color: #999; font-size: 11px !important;}
.fb-button {background: #5B74A8 url(http://static.ak.fbcdn.net/rsrc.php/v1/y7/r/wnW-cz2rWzB.png) no-repeat 0 -98px; border: 1px solid #999; border-bottom-color: #888; -webkit-box-shadow: 0 1px 0 rgba(0, 0, 0, .1); -moz-box-shadow: 0 1px 0 rgba(0, 0, 0, .1); box-shadow: 0 1px 0 rgba(0, 0, 0, .1); cursor: pointer; display: inline-block; border-color: #29447E #29447E #1A356E; padding: 2px 20px;}
.fb-button input, .fb-button input:hover, .fb-button input:active, .fb-button input:visited {background: none; font-family: 'Lucida Grande', Tahoma, Verdana, Arial, sans-serif; font-size: 11px !important; padding: 1px 0 2px;border:none !important;color:White !important;}
/* facebook reg form */
.fb-reg-form {background-color: white; border: 1px solid #ccc; margin: .5em 0;}
.fb-reg-form > header {border-bottom: 1px solid #ccc; padding: 1em 1em 1em 36px; font-size: 11px; color: gray; background: #f0f0f0 url(../../img/ui-ico-facebook.png) no-repeat 1em 50%;}
.fb-reg-form > footer {background-color: #fefefe; border-top: 1px solid #ccc; padding: 1em; font-size: 9px; color: gray;}
.fb-reg-form > footer a {font-size: 9px !important;}
.fb-reg-form .form-item .form-label {color: #666}
.fb-reg-form #fbreg-tagline, .fb-reg-form .legal {color: #999; font-size: 10px;}
.fb-reg-form #fbreg-name-cont {border: 1px solid #b9c4d5; padding: 3px; background-color: #eeeeee;}
.fb-reg-form #fbreg-name-cont > img {float: left; margin: 0 4px 0 0; width: 32px; height: 32px;}
.fb-reg-form #fbreg-name-cont #fbreg-name {font-weight: bold; font-size: 11px; color: #333; margin-top: 2px;}
#fbregrequest {padding: 1em 1em 1em 56px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; position: relative; margin: 5em 0 18px; background: #fff url(../../img/ui-icon-facebook-glossy-34x34.png) no-repeat 1em 50%;}
#fbregrequest:after {color: #fff; content: "\25BC"; display: block; font-size: 18px; position: absolute; left: 48.5%; bottom: -16px;}
#fbregrequest > h2 {color: #936; font-size: 16px; font-family: Georgia,Palatino,"Palatino Linotype",Times,"Times New Roman", serif; margin: 0;}
#fbregRequestCont .btn {font-size: 11px}
#btnNoThanksCont a{color:#999}
</style>

<div id="fbregRequestCont">
    <div id="fbregrequest">
        <h2><mn:txt runat="server" id="txtFBChoiceHeader" resourceconstant="TXT_FB_CHOICE_HEADER" expandimagetokens="true" /></h2>
        <mn:txt runat="server" id="txtFBChoicePrivacy" resourceconstant="TXT_FB_CHOICE_PRIVACY" expandimagetokens="true" />
    </div>
    <div class="text-center">
        <input id="getRegData" class="btn primary" type="button" value="<%=g.GetResource("TXT_FB_CHOICE_BTN",this) %>" />
    </div>
</div>

<div id="fbRegForm" class="facebook fb-reg-form hide">
    <header><mn:txt runat="server" id="txtFBFormHeader" resourceconstant="TXT_FB_FORM_HEADER" expandimagetokens="true" /></header>
    <fieldset class="form-set form-label-outside margin-medium">
        <div class="form-item">
            <div class="form-label">
                <mn:txt runat="server" id="txtLabelName" resourceconstant="TXT_FB_FORM_LABEL_NAME" expandimagetokens="true" />
            </div>    
            <div class="form-input">
                <div id="fbreg-name-cont" class="clearfix">
                    <img id="fbreg-userPhoto" src="" alt="" />
                    <div id="fbreg-name"></div>
                    <div id="fbreg-tagline"></div>
                </div>
                <p class="legal"><mn:txt runat="server" id="txtFBFormPrivacyName" resourceconstant="TXT_FB_FORM_PRIVACY_NAME" expandimagetokens="true" /></p>
            </div>
        </div>
        <div class="form-item">
            <div class="form-label">
                <mn:txt runat="server" id="txtLabelEmail" resourceconstant="TXT_FB_FORM_LABEL_EMAIL" expandimagetokens="true" />
            </div>    
            <div class="form-input">
                <input type="text" id="fbreg-email" disabled="disabled" value="" />
            </div>
        </div>
        <div class="form-item">
            <div class="form-label">
                <mn:txt runat="server" id="txtLabelBirthday" resourceconstant="TXT_FB_FORM_LABEL_BIRTHDAY" expandimagetokens="true" />
            </div>    
            <div class="form-input">
                <select id="fbreg-bmonth" disabled="disabled"><option></option></select>
                <select id="fbreg-bday" disabled="disabled"><option></option></select>
                <select id="fbreg-byear" disabled="disabled"><option></option></select>
            </div>
        </div><%--
        <div class="form-item">
            <div class="form-label">
                <mn:txt runat="server" id="txtLabelLocation" resourceconstant="TXT_FB_FORM_LABEL_LOCATION" expandimagetokens="true" />
            </div>    
            <div class="form-input">
                <input type="text" id="fbregLocation" disabled="disabled" value="" />
            </div>
        </div>--%>
        <div class="form-item">
            <div class="form-label">&nbsp;</div>    
            <div class="form-input">
                <label class="fb-button"><mn2:FrameworkButton runat="server" ID="btnFBRegister" ResourceConstant="TXT_REGISTER" Visible="true" /></label>
            </div>
        </div>
    </fieldset>
    <footer>
        <mn:txt runat="server" id="htmlFBFormFooter" resourceconstant="HTML_FB_FORM_FOOTER" expandimagetokens="true" />
    </footer>

    <asp:HiddenField runat="server" id="hdnFBName" />
    <asp:HiddenField runat="server" id="hdnFBBirthday" />
    <asp:HiddenField runat="server" id="hdnFBLocation" />
    <asp:HiddenField runat="server" id="hdnFBEmail" />
</div>

<div id="btnNoThanksCont" class="text-outside margin-medium">
    <mn2:FrameworkLinkButton runat="server" ID="btnNoThanks" ResourceConstant="TXT_FB_CHOICE_NO_THANKS" Visible="true" />
</div>

<script>
__addToNamespace__('spark.facebook', {
    fbRegPopulator: function(){
        //activate loader
        $j('#fbregRequestCont').before(spark.util.ajaxLoader);
        document.getElementById('fbregRequestCont').style.display = "none";
        document.getElementById('btnNoThanksCont').style.display = "none";
        
        FB.api('/me/friends', function(friends){

            FB.api('/me', function(me){
            
                //check FB api error/availability
                if (!me || me.error || !friends || friends.error) {
                    document.getElementById('fbregRequestCont').style.display = "block";
                    document.getElementById('btnNoThanksCont').style.display = "block";
                    $j('.loading.spinner-only').remove();
                    alert(spark.facebook.connectionError);
                } else {
                
                    var birthdaySplit = me.birthday.split("/"),
                        tagline = "";

                    // populate html form fields
                    // required. these will not be empty
                    document.getElementById('fbreg-email').value = me.email;
                    document.getElementById('fbreg-name').innerHTML = me.name;
                    document.getElementById('fbreg-bmonth').options[0].innerHTML = birthdaySplit[0];
                    document.getElementById('fbreg-bday').options[0].innerHTML = birthdaySplit[1];
                    document.getElementById('fbreg-byear').options[0].innerHTML = birthdaySplit[2];

                    if(!me.username){
                        $j('#fbreg-userPhoto').remove();
                    } else {
                        document.getElementById('fbreg-userPhoto').src = 'https://graph.facebook.com/' + me.username + '/picture?type=square';
                    }

                    // build tagline
                    if(me.work){
                        tagline += me.work[0].employer.name + ', ';
                    }
                    if(me.education){
                        tagline += me.education[0].school.name + ', ';
                    }
                    tagline += friends.data.length + ' friends';
                    document.getElementById('fbreg-tagline').innerHTML = tagline;

                    //check location
                    //if(!me.location){
                    //    $j('#fbregLocation').parents('.form-item').remove();
                    //} else {
                    //    document.getElementById('fbregLocation').value = me.location.name;
                    //}

                    //show form
                    $j('.loading.spinner-only').remove();
                    document.getElementById('fbRegForm').style.display = "block";
                    document.getElementById('btnNoThanksCont').style.display = "block";
            
                    //populate hidden inputs with data
                    $j('#<%=hdnFBName.ClientID %>').val(me.name);
                    $j('#<%=hdnFBBirthday.ClientID %>').val(me.birthday);
                    //$j('#<%=hdnFBLocation.ClientID %>').val(me.location.name);
                    $j('#<%=hdnFBEmail.ClientID %>').val(me.email);
                }
            });
        });
    }
});

//log in to Facebook
$j('#getRegData').live('click', function (event) {
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            spark.facebook.fbRegPopulator();
        } else {
            spark.facebook.fbAuth(function(){spark.facebook.fbRegPopulator()}, null, 'user_birthday,user_photos,email');
        }
    }, true);
});
</script>
