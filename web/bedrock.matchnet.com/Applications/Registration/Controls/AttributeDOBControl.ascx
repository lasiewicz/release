﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeDOBControl.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeDOBControl" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<div id="divControl" runat="server">
    <%=GetTitleWrapperOpen %><mn:Txt runat="server" ID="txtCaption" />
    <%=GetTitleWrapperClose %>
    <uc2:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
    <%=GetFieldWrapperOpen %>
    <asp:PlaceHolder ID="plcHebrewDOB" runat="server">
        <asp:TextBox ID="tbBirthDateYearHebrew" runat="server" MaxLength="4" Width="54"></asp:TextBox>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcBritDOB" runat="server">
        <asp:DropDownList ID="ddlBirthDateDayBrit" runat="server" Style="width: 74px;" CssClass="select-birthdate select-day">
        </asp:DropDownList>
        <asp:DropDownList ID="ddlBirthDateMonthBrit" runat="server" Style="width: 80px;"
            CssClass="select-birthdate select-month">
        </asp:DropDownList>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcUSDOBMonthDay" runat="server">
        <asp:DropDownList ID="ddlBirthDateDayOnePage" runat="server" Style="width: 74px;"
            CssClass="select-birthdate select-day" Visible="false">
        </asp:DropDownList>
        <asp:DropDownList ID="ddlBirthDateMonthUS" runat="server" Style="width: 80px;" CssClass="select-birthdate select-month">
        </asp:DropDownList>
        <asp:DropDownList ID="ddlBirthDateDayUS" runat="server" Style="width: 74px;" CssClass="select-birthdate select-day">
        </asp:DropDownList>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcUSDOB" runat="server">
        <asp:DropDownList ID="ddlBirthDateYearUS" runat="server" Style="width: 80px;" CssClass="select-birthdate select-year">
        </asp:DropDownList>
    </asp:PlaceHolder>
    <%=GetFieldWrapperClose %>
</div>
