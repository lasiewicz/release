﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using Matchnet.Web;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using System.Text.RegularExpressions;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeText : FrameworkControl, IAttributeControl, IJQValidation
    {

        AttributeControl _attrControl;
        bool _useAttributeDescription;
        bool _includeBlanks;
        int _selectedValue;

        FrameworkControl _resourceControl;

        public string ContainerDivClientID { get { return divControl.ClientID; } }

        public bool IsOverlayReg { get; set; }

        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }

        public void Save()
        {

        }
        public ValidationMessage ValidationMessage { get { return txtValidation; } }
        public string InputFieldID { get { return AttributeTextControl.UniqueID; } }

        public void SetValue(string value)
        {
            AttributeTextControl.Text = HttpUtility.UrlDecode(value);
            AttributeTextControl.Attributes["value"] = _attrControl.AttributeTextValue;
            //AttributeMaskControl.SetSelectedMasks(_selectedValue);
            // atttributetextcon
        }
        public string GetValue()
        {
            if (IsOverlayReg)
            {
                return Request[AttributeTextControl.UniqueID];
            }
            else
            {
                return AttributeTextControl.Text;
            }
        }
        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "block");

        }
        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return AttributeTextControl.ClientID;
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }
        public void DisplayError(string err)
        {

            txtValidation.Text = err;
        }

        public string ScriptBlock()
        {
            //the good way to do it is to give a function unique name per control
            //may be next version
            StringBuilder bld = new StringBuilder();

            if (_attrControl.Type == ControlType.textarea)
            {
                string validationTxt = _attrControl.GetResourceString("MIN_TEXT_REQUIRE", g, ResourceControl);
                bld.Append("<script type=\"text/javascript\">\r\n");
                bld.Append("function showCharCount()\r\n ");
                bld.Append("{\r\n");
                bld.Append("var txtID=\"" + AttributeTextControl.ClientID + "\"\r\n");
                //bld.Append("var lblID=\"" + lblCharCount.ID + "\"\r\n");
                bld.Append("var lblClientID=\"" + lblCharCount.ClientID + "\"\r\n");
                bld.Append("var minLen=\"" + _attrControl.MinLen.ToString() + "\"\r\n");
                bld.Append("var minLen=\"" + _attrControl.MinLen.ToString() + "\"\r\n");
                bld.Append("var texObj = document.forms[0].elements[txtID];\r\n");
                bld.Append("if( texObj != null)\r\n{");
                bld.Append("   var tex = document.forms[0].elements[txtID].value;\r\n");
                bld.Append("   var lbl=document.getElementById(lblClientID);\r\n");

                bld.Append("   var len = tex.length;\r\n");
                bld.Append("  if(len <  minLen)\r\n");
                bld.Append("   lbl.innerHTML=\"[<font color=red>\" + len + \"</font>/\" + minLen + \"]\";\r\n");
                bld.Append("  else\r\n");
                bld.Append("   lbl.innerHTML=\"[<font color=green>\" + len + \"</font>/\" + minLen + \"]\";\r\n}\r\n}\r\n");
                bld.Append("</script>\r\n");
            }
            return bld.ToString();
        }

        public void PersistToCookie(HttpCookie cookie)
        {
            string cookiekey = _attrControl.AttributeName;
            if (String.IsNullOrEmpty(cookiekey))
            {
                cookiekey = _attrControl.Name;
            }
            cookie[cookiekey] = HttpUtility.UrlEncode(GetValue());
        }

        public void Persist(ITemporaryPersistence persistence)
        {
            string cookiekey = _attrControl.AttributeName;
            if (String.IsNullOrEmpty(cookiekey))
            {
                cookiekey = _attrControl.Name;
            }
            persistence[cookiekey] = HttpUtility.UrlEncode(GetValue());
        }

        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }
        public bool Validate()
        {
            bool valid = true;
            string match = "";

            string text = GetValue();

            // Text might be optional, meaning empty input is valid.
            if (!_attrControl.RequiredFlag && String.IsNullOrEmpty(text))
            {
                return valid;
            }

            if (_attrControl.RequiredFlag && String.IsNullOrEmpty(text))
            {
                string errMsg;
                if (String.IsNullOrEmpty(_attrControl.ErrorMessageRequired))
                {
                    errMsg = "ERR_REQUIRED";
                }
                else
                {
                    errMsg = _attrControl.ErrorMessageRequired;
                }
                txtValidation.Text = _attrControl.GetValidationMessage(errMsg, g, _resourceControl);
                valid = false;
                txtValidation.Visible = true;
                return valid;
            }

            if (text.Length < _attrControl.MinLen && _attrControl.MinLen > 0)
            {
                string key = "ERR_MIN_LENGTH";
                if (!string.IsNullOrEmpty(_attrControl.ErrorMessageMinLength))
                {
                    key = _attrControl.ErrorMessageMinLength;
                }
                txtValidation.Text = _attrControl.GetValidationMessage(key, g, _resourceControl);
                valid = false;
                txtValidation.Visible = true;
                return valid;
            }

            if (text.Length > _attrControl.MaxLen && _attrControl.MaxLen > 0)
            {
                txtValidation.Text = _attrControl.GetValidationMessage("ERR_MAX_LENGTH", g, _resourceControl);
                valid = false;
                txtValidation.Visible = true;
                return valid;
            }

            if (!String.IsNullOrEmpty(_attrControl.RegExString))
                match = _attrControl.RegExString;
            else
                if (_attrControl.Validation == ValidationRule.alpha)
                    match = "[a-zA-Zא-ת ]*";

                else if (_attrControl.Validation == ValidationRule.alphanum)
                {
                    match = "^[a-zA-Z0-9א-ת]*$";


                }
                else if (_attrControl.Validation == ValidationRule.email)
                {//match = @"\w+([-+.]\w+)*\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    match = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                }

            if (!string.IsNullOrEmpty(match))
            {
                //valid = Regex.IsMatch(text, match);
                Regex regValidation = new Regex(match);

                valid = regValidation.IsMatch(text);
            }
            if (!valid)
            {
                string validationErrorResourceConstant = string.IsNullOrEmpty(_attrControl.ErrorMessageValidation)
                                                             ? "ERR_INVALID"
                                                             : _attrControl.ErrorMessageValidation;


                txtValidation.Text = _attrControl.GetValidationMessage(validationErrorResourceConstant, g, _resourceControl);
            }
            if (!valid)
                txtValidation.Visible = true;
            return valid;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (_attrControl.Type == ControlType.textarea)
                AttributeTextControl.TextMode = TextBoxMode.MultiLine;
            else if (_attrControl.Type == ControlType.password)
            {
                AttributeTextControl.TextMode = TextBoxMode.Password;
                AttributeTextControl.Attributes.Add("autocomplete", "off");

            }
            if (_attrControl.MaxLen > 0)
                AttributeTextControl.MaxLength = _attrControl.MaxLen;

            if (_attrControl.RequiredFlag)
            {
                AttributeTextControl.CssClass += " mandatory";
            }
            else
            {
                AttributeTextControl.CssClass += " optional";
            }

            divControl.Attributes["class"] = _attrControl.Name;

            if (!string.IsNullOrEmpty(_attrControl.AttributeTip))
            {
                txtTip.ResourceConstant = _attrControl.AttributeTip;
            }
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            //AttributeTextControl.Text = _attrControl.AttributeTextValue;
            // AttributeTextControl.Attributes.Add("name", _attrControl.AttributeName);
            if (AttributeTextControl.TextMode == TextBoxMode.MultiLine)
            {
                AttributeTextControl.Attributes["onKeyUp"] = "javascript:showCharCount()";

                if (_attrControl.Rows > 0)
                {
                    AttributeTextControl.Rows = _attrControl.Rows;
                }
                if (AttributeTextControl.Text.Length < _attrControl.MinLen)
                    litCharCount.Text = "[<font color=red>" + AttributeTextControl.Text.Length.ToString() + "</font>/" + _attrControl.MinLen.ToString() + "]";
                else
                    litCharCount.Text = "[<font color=green>" + AttributeTextControl.Text.Length.ToString() + "</font>/" + _attrControl.MinLen.ToString() + "]";

            }
            txtCaption.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);
            if (_attrControl.Cols > 0)
            {
                AttributeTextControl.Columns = _attrControl.Cols;
            }

            if (AttrControl.ShowLabelInsideInput)
            {
                AttributeTextControl.Text = txtCaption.Text;
            }
        }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            return new NameValueCollection();
        }


        string GetRequiredErrorMessage()
        {
            string key;
            if (String.IsNullOrEmpty(_attrControl.ErrorMessageRequired))
            {
                key = "ERR_REQUIRED";
            }
            else
            {
                key = _attrControl.ErrorMessageRequired;
            }

            return _attrControl.GetValidationMessage(key, g, _resourceControl);
        }
        string GetMinLengthErrorMessage()
        {
            string key = "ERR_MIN_LENGTH";
            if (!string.IsNullOrEmpty(_attrControl.ErrorMessageMinLength))
            {
                key = _attrControl.ErrorMessageMinLength;
            }
            return _attrControl.GetValidationMessage(key, g, _resourceControl);
        }
        string GetMaxLengthErrorMessage()
        {

            string key = (string.IsNullOrEmpty(_attrControl.ErrorMessageMinLength)
                              ?
                                  "ERR_MIN_LENGTH"
                              : _attrControl.ErrorMessageMinLength);

            return _attrControl.GetValidationMessage(key, g, _resourceControl);
        }

        protected string GetTitleWrapperOpen
        {
            get
            {
                return (_attrControl.OnePageReg) ? "<div class=\"label\">" : "<label class=\"h2\">";
            }
        }
        protected string GetTitleWrapperClose
        {
            get
            {
                return (_attrControl.OnePageReg) ? "</div>" : "</label>";
            }
        }
        protected string GetFieldWrapperOpen
        {
            get { return (_attrControl.OnePageReg) ? "<div class=\"field\">" : string.Empty; }
        }
        protected string GetFieldWrapperClose
        {
            get { return (_attrControl.OnePageReg) ? "</div>" : string.Empty; }
        }





        #region IJQValidation Members


        public List<ValidationDictionaryEntry> GetValidationRules
        {
            get
            {
                List<ValidationDictionaryEntry> result = new List<ValidationDictionaryEntry>();
                ValidationDictionaryEntry rules = new ValidationDictionaryEntry();

                rules.UniqueID = AttributeTextControl.UniqueID;

                if (_attrControl.RequiredFlag)
                {
                    rules.ValidationEntryList.Add(new ValidationEntry()
                    {
                        errorKey = "required",
                        message = GetRequiredErrorMessage(),
                        value = "true"
                    });
                }
                if (_attrControl.MinLen > 0)
                {
                    rules.ValidationEntryList.Add(new ValidationEntry()
                    {
                        errorKey = "minlength",
                        message = GetMinLengthErrorMessage(),
                        value = _attrControl.MinLen.ToString()
                    });
                }
                if (_attrControl.MaxLen > 0)
                {
                    rules.ValidationEntryList.Add(new ValidationEntry()
                    {
                        errorKey = "maxlength",
                        message = GetMaxLengthErrorMessage(),
                        value = _attrControl.MaxLen.ToString()
                    });
                }

                string validationErrorResourceConstant = string.IsNullOrEmpty(_attrControl.ErrorMessageValidation)
                                                     ? "ERR_INVALID"
                                                     : _attrControl.ErrorMessageValidation;

                if (_attrControl.Validation == ValidationRule.email)
                {
                    rules.ValidationEntryList.Add(new ValidationEntry()
                    {
                        errorKey = "email",
                        message = _attrControl.GetValidationMessage(validationErrorResourceConstant, g, _resourceControl),
                        value = "true"
                    });
                }
                else if (_attrControl.Name == "Password")
                {
                    rules.ValidationEntryList.Add(new ValidationEntry()
                    {
                        errorKey = "password",
                        message = _attrControl.GetValidationMessage(validationErrorResourceConstant, g, _resourceControl),
                        value = "true"
                    });
                }

                result.Add(rules);
                return result;
            }
        }

        #endregion
    }
}