﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using Matchnet.Web;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using System.Text.RegularExpressions;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class ValidationMessage : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public String Text
        {
            get { return txtValidation.Text; }
            set { txtValidation.Text = value; }
        }
    }
}