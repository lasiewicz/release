﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuizIntroControl.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Controls.QuizIntroControl" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
  <div id="divControl" runat="server">
  <uc2:ValidationMessage runat="server" ID="txtValidation" Visible=false />
  <div id="page-container" class="cc-content-main cc-content-bars">

    <h2 class="tagline">The Color Code <small class="sans-serif">Understanding yourself and your potential perfect match.</small></h2>

    <div id="cc-content" class="editorial">
        <h1>About The Color Code Personality Test</h1>

        <p>The Color Code Personality Test will determine your core color and provide you with in-depth understanding of why you do the things you do and how you can best interact with the other personality "Colors."</p>
        <p>The Color Code is based on the following four personality "Colors," or driving Core Motives:</p>

        <ul class="ui-arrows">
	        <li><span class="cc-red">RED</span> (Motive: POWER)These are the power wielders.</li>
	        <li><span class="cc-blue">BLUE</span> (Motive: INTIMACY)These are the do-gooders.</li>
	        <li><span class="cc-white">WHITE</span> (Motive: PEACE)These are the peacekeepers.</li>

	        <li><span class="cc-yellow">YELLOW</span> (Motive: Fun)These are the fun lovers.</li>
        </ul>

        <p>The Color Code is different than ANY of the other personality profiles on the market today. It is the ONLY assessment that identifies driving Core Motives. Most other popular tests and assessments strictly identify your behavior, and leave it at that. In other words, the Color Code helps you to understand why you do what you do versus simply what you do. You can't possibly begin to understand yourself, if you don't know what motivates you! </p>
        <p>Once you have embraced the Color Code, you will understand why so many people claim that they can never see themselves or others the same again after having learned this system. This is your code to successful relationships. We guarantee it 100%. There isn't a better place to start than right now, with YOU! </p>
        <p>What "Color" are you? Find out by taking the Color Code Personality&nbsp;Test.</p>
        		
      <%--  <p class="text-center editorial cc-button-primary">	
	        <a href="Quiz1.aspx" class="link-primary">Take the Color Code Test Now</a>
        </p>--%>

    </div><!-- end .cc-content-->	
</div>
</div>