﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeGenderPicker : FrameworkControl, IAttributeControl
    {

        AttributeControl _attrControl;
        int _genderMask;
        FrameworkControl _resourceControl;

        public string ContainerDivClientID { get { return divControl.ClientID; } }

        public bool IsOverlayReg { get; set; }

        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }
        public void SetVisible(bool visible)
        {
            this.Visible = visible;
            //if (!visible)
            //{
            //    //divControl.Style.Add("display", "none");


            //}
            //else
            //    divControl.Style.Add("display", "inline");

        }
        public void Save()
        {

        }
        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return AttributeGenderPickerControl.ClientID;
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }

        public ValidationMessage ValidationMessage { get { return txtValidation; } }
        public void SetValue(string value)
        {
            _genderMask = Conversion.CInt(value);
            AttributeGenderPickerControl.GenderMask = _genderMask;
        }
        public string GetValue()
        {
            if (IsOverlayReg)
            {
                return AttributeGenderPickerControl.GetGenderMaskFromPost().ToString();
            }
            else
            {
                return AttributeGenderPickerControl.GenderMask.ToString();
            }
        }
        public string ScriptBlock()
        {
            return "";
        }
        public void PersistToCookie(HttpCookie cookie)
        {
            cookie[_attrControl.AttributeName] = GetValue();
        }

        public void Persist(ITemporaryPersistence persistence)
        {
            persistence[_attrControl.AttributeName] = GetValue();
        }

        public bool Validate()
        {
            bool valid = false;
            if (!_attrControl.RequiredFlag)
                return true;

            int mask = AttributeGenderPickerControl.GenderMask;
            if (mask <= 0)
            {
                txtValidation.Visible = true;
                txtValidation.Text = _attrControl.GetResourceString(_attrControl.ErrorMessageRequired, g, _resourceControl);
            }
            else
            {
                txtValidation.Visible = false;
                txtValidation.Text = "";
                valid = true;
            }

            return valid;
        }

        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }

        public int GenderMask
        { get { return _genderMask; } set { _genderMask = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (_attrControl.RequiredFlag)
            {
                AttributeGenderPickerControl.CssClass += " mandatory";
            }
            else
            {
                AttributeGenderPickerControl.CssClass += " optional";
            }

            divControl.Attributes["class"] = _attrControl.Name;
        }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            return new NameValueCollection();
        }
    }
}