﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeMask.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeMask" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="GenericMaskControl" Src="~/Framework/Ui/FormElements/GenericMaskControl.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<div id="divControl" runat="server">
<h2><mn:txt runat="server" id="txtCaption" /></h2>
<uc2:ValidationMessage runat="server" ID="txtValidation" Visible=false />
<uc1:GenericMaskControl ID="AttributeMaskControl" runat="server" />

<%--<asp:CheckBoxList ID="AttributeMaskControl" runat="server" />--%>
</div>
