﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using Matchnet.Web;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using System.Text.RegularExpressions;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeTextConfirm : FrameworkControl, IAttributeControl, IJQValidation
    {

        AttributeControl _attrControl;
        bool _useAttributeDescription;
        bool _includeBlanks;
        int _selectedValue;

        FrameworkControl _resourceControl;

        public string ContainerDivClientID { get { return divControl.ClientID; } }

        public bool IsOverlayReg { get; set; }

        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }

        public void Save()
        {

        }
        public ValidationMessage ValidationMessage { get { return txtValidation; } }
        public string InputFieldID { get { return AttributeTextControl.UniqueID; } }

        public void SetValue(string value)
        {
            AttributeTextControl.Text = HttpUtility.UrlDecode(value);
            AttributeTextControl.Attributes["value"] = _attrControl.AttributeTextValue;
            //AttributeMaskControl.SetSelectedMasks(_selectedValue);
            // atttributetextcon
        }

        public string GetValue()
        {
            if (IsOverlayReg)
            {
                return Request[AttributeTextControl.UniqueID];
            }
            else
            {
                return AttributeTextControl.Text;
            }
        }
        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "block");

        }
        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return AttributeTextControl.ClientID;
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }
        public void DisplayError(string err)
        {

            txtValidation.Text = err;
        }

        public string ScriptBlock()
        {
            return "";
        }

        public void PersistToCookie(HttpCookie cookie)
        {
            string cookiekey = _attrControl.AttributeName;
            if (String.IsNullOrEmpty(cookiekey))
            {
                cookiekey = _attrControl.Name;
            }
            cookie[cookiekey] = HttpUtility.UrlEncode(GetValue());
        }

        public void Persist(ITemporaryPersistence persistence)
        {
            string cookiekey = _attrControl.AttributeName;
            if (String.IsNullOrEmpty(cookiekey))
            {
                cookiekey = _attrControl.Name;
            }
            persistence[cookiekey] = HttpUtility.UrlEncode(GetValue());
        }

        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }
        public bool Validate()
        {
            bool valid = true;
            string match = "";
            string errMsg;
            string text = Request[AttributeTextControl.UniqueID];

            string textConfirm = Request[AttributeTextControlConfirm.UniqueID];

            if (_attrControl.RequiredFlag && (String.IsNullOrEmpty(text) || String.IsNullOrEmpty(textConfirm)))
            {

                if (String.IsNullOrEmpty(_attrControl.ErrorMessageRequired))
                {
                    errMsg = "ERR_REQUIRED";
                }
                else
                {
                    errMsg = _attrControl.ErrorMessageRequired;
                }
                txtValidation.Text = _attrControl.GetValidationMessage(errMsg, g, _resourceControl);
                valid = false;
                txtValidation.Visible = true;
                return valid;
            }

            if (text.Length < _attrControl.MinLen && _attrControl.MinLen > 0)
            {
                txtValidation.Text = _attrControl.GetValidationMessage("ERR_MIN_LENGTH", g, _resourceControl);
                valid = false;
                txtValidation.Visible = true;
                return valid;
            }

            if (text.Length > _attrControl.MaxLen && _attrControl.MaxLen > 0)
            {
                txtValidation.Text = _attrControl.GetValidationMessage("ERR_MAX_LENGTH", g, _resourceControl);
                valid = false;
                txtValidation.Visible = true;
                return valid;
            }

            if (!String.IsNullOrEmpty(_attrControl.RegExString))
                match = _attrControl.RegExString;
            else
                if (_attrControl.Validation == ValidationRule.alpha)
                    match = "[a-zA-Zא-ת ]*";

                else if (_attrControl.Validation == ValidationRule.alphanum)
                {
                    match = "^[a-zA-Z0-9א-ת]*$";


                }
                else if (_attrControl.Validation == ValidationRule.email)
                {//match = @"\w+([-+.]\w+)*\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    match = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                }
            if (!string.IsNullOrEmpty(match))
            {
                //valid = Regex.IsMatch(text, match);
                Regex regValidation = new Regex(match);
                valid = regValidation.IsMatch(text);


            }

            if (!valid)
            {
                txtValidation.Text = _attrControl.GetValidationMessage("ERR_INVALID", g, _resourceControl);

                txtValidation.Visible = true;
            }
            else
            {
                if (text != textConfirm)
                {
                    errMsg = "ERR_NOT_SAME";
                    valid = false;
                    txtValidationConfirm.Text = _attrControl.GetValidationMessage(errMsg, g, _resourceControl);
                    txtValidationConfirm.Visible = true;
                    if (IsOverlayReg)
                    {
                        txtValidation.Text = txtValidationConfirm.Text;
                    }
                    return valid;
                }
            }
            return valid;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (_attrControl.Type == ControlType.password)
            {
                AttributeTextControl.TextMode = TextBoxMode.Password;
                AttributeTextControl.Attributes.Add("autocomplete", "off");

            }
            if (_attrControl.MaxLen > 0)
                AttributeTextControl.MaxLength = _attrControl.MaxLen;

            if (_attrControl.RequiredFlag)
            {
                AttributeTextControl.CssClass += " mandatory";
                AttributeTextControlConfirm.CssClass += " mandatory";
            }
            else
            {
                AttributeTextControl.CssClass += " optional";
                AttributeTextControlConfirm.CssClass += " optional";
            }

            divControl.Attributes["class"] = _attrControl.Name;
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            txtCaption.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);
            txtCaptionConfirm.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant1, g, _resourceControl);
        }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            return new NameValueCollection();
        }


        #region IJQValidation Members


        public List<ValidationDictionaryEntry> GetValidationRules
        {
            get
            {
                List<ValidationDictionaryEntry> result = new List<ValidationDictionaryEntry>();
                ValidationDictionaryEntry rules = new ValidationDictionaryEntry();

                rules.UniqueID = AttributeTextControlConfirm.UniqueID;

                if (_attrControl.RequiredFlag)
                {
                    rules.ValidationEntryList.Add(new ValidationEntry()
                      {
                          errorKey = "required",
                          message = "input is required",
                          value = "true"
                      });
                }
                result.Add(rules);
                return result;
            }
        }

        #endregion
    }
}