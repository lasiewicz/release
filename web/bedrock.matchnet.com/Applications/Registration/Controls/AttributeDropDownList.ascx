﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeDropDownList.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeDropDownList" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<div id="divControl" runat="server">
    <%=GetTitleWrapperOpen %><mn:Txt runat="server" ID="txtCaption" />
    <%=GetTitleWrapperClose %>
    <uc2:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
    <%=GetFieldWrapperOpen %>
    <asp:DropDownList ID="AttributeOptions" runat="server">
    </asp:DropDownList>
    <%=GetFieldWrapperClose %>
</div>
