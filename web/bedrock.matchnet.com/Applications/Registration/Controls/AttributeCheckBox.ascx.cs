﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeCheckBox : FrameworkControl, IAttributeControl, IJQValidation
    {

        AttributeControl _attrControl;
        int _selectedValue = 0;
        PhotoUpdate _photoUpdate;
        FrameworkControl _resourceControl;


        public string ContainerDivClientID { get { return divControl.ClientID; } }

        public bool IsOverlayReg { get; set; }

        public string InputFieldID { get { return AttributeCheckBoxControl.UniqueID; } }

        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "block");

        }
        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }
        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return AttributeCheckBoxControl.ClientID;
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }
        public void Save()
        {

        }
        public ValidationMessage ValidationMessage { get { return txtValidation; } }
        public void SetValue(string value)
        {
            _selectedValue = Conversion.CInt(value);

        }
        public string GetValue()
        {
            if (IsOverlayReg)
            {
                if (!string.IsNullOrEmpty(Request[AttributeCheckBoxControl.UniqueID]))
                {
                    _selectedValue = _attrControl.AttributeIntValue;
                }
            }
            else
            {
                if (AttributeCheckBoxControl.Checked)
                    _selectedValue = _attrControl.AttributeIntValue;
            }


            return _selectedValue.ToString();
            //_selectedValue.ToString();

        }
        public string ScriptBlock()
        {
            return "";
        }
        public void PersistToCookie(HttpCookie cookie)
        {
            cookie[_attrControl.AttributeName] = GetValue();

        }

        public void Persist(ITemporaryPersistence persistence)
        {
            persistence[_attrControl.AttributeName] = GetValue();
        }

        public bool Validate()
        {
            bool valid = true;
            if (!string.IsNullOrEmpty(Request[AttributeCheckBoxControl.UniqueID]))
            {
                AttributeCheckBoxControl.Checked = true;
            }
            if (_attrControl.RequiredFlag && (!AttributeCheckBoxControl.Checked))
            {
                valid = false;

                txtValidation.Text = GetValidationMessage();
                _attrControl.GetResourceString(_attrControl.ErrorMessageRequired, g, _resourceControl);
                txtValidation.Visible = true;
            }
            return valid;
        }

        private string GetValidationMessage()
        {
            if (_attrControl.ClientValidationEnabled)
            {
                return _attrControl.GetResourceString(_attrControl.ErrorMessageRequired, g, _resourceControl);
            }
            else
            {
                return _attrControl.GetResourceString(_attrControl.ErrorMessageRequired, g, _resourceControl);
            }
        }




        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            txtCaption.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);
            if (_selectedValue > 0)
                AttributeCheckBoxControl.Checked = true;
            if (!String.IsNullOrEmpty(_attrControl.CSSClass))
            {
                divControl.Attributes.Add("class", _attrControl.CSSClass);
            }

            if (_attrControl.RequiredFlag)
            {
                AttributeCheckBoxControl.InputAttributes["class"] = AttributeCheckBoxControl.InputAttributes["class"] += " mandatory";
            }
            else
            {
                AttributeCheckBoxControl.InputAttributes["class"] = AttributeCheckBoxControl.InputAttributes["class"] += " optional";
            }

            divControl.Attributes["class"] = _attrControl.Name;
        }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            return new NameValueCollection();
        }

        #region IJQValidation Members

        public List<ValidationDictionaryEntry> GetValidationRules
        {
            get
            {
                List<ValidationDictionaryEntry> result = new List<ValidationDictionaryEntry>();
                ValidationDictionaryEntry rules = new ValidationDictionaryEntry();

                rules.UniqueID = AttributeCheckBoxControl.UniqueID;

                if (_attrControl.RequiredFlag)
                {
                    rules.ValidationEntryList.Add(new ValidationEntry()
                       {
                           errorKey = "required",
                           message = GetValidationMessage(),
                           value = "true"
                       });
                }

                result.Add(rules);
                return result;
            }
        }

        #endregion

        protected string GetOpenDiv(string id)
        {
            return (_attrControl.OnePageReg) ?
                string.Format("<div id=\"{0}\" >", id) :
            string.Empty;
        }
        protected string GetCloseDiv()
        {
            return (_attrControl.OnePageReg) ?
                string.Format("</div>") :
            string.Empty;
        }
    }
}