﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeText.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeText" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<div id="divControl" runat="server">

<%--    <script type="text/javascript">

        //function showCharCount() 
        //{
        ////alert("here");
        //var txtID='<asp:Literal ID="txtClientID" Runat="server" Visible="True" />'
        //var minLen=<asp:Literal ID="txtMinLen" Runat="server" Visible="True" />
        //var tex = document.forms[0].elements[txtID].value;
        //var lbl=document.getElementById("lblCharCount");
        ////alert(lbl);
        //var len = tex.length;

        //        
        // lbl.innerHTML="[" + len + "/" + minLen + "]";
        //}

    </script>--%>

    <%--<mn:Txt runat="server" ID="txtValidation" Visible=false style="color:Red"/><br />--%>
    <%=GetTitleWrapperOpen %>
    <mn:Txt runat="server" ID="txtCaption" />
    <%=GetTitleWrapperClose %>
    <uc2:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
    <%=GetFieldWrapperOpen %>
    <asp:TextBox ID="AttributeTextControl" runat="server"></asp:TextBox>
    <%=GetFieldWrapperClose %>
    <mn:Txt runat="server" ID="txtTip" />
    <br />
    <span id="lblCharCount" class="charcount" runat="server">
        <asp:Literal ID="litCharCount" runat="server" /></span>
</div>
