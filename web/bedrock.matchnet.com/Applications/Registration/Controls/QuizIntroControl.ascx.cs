﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Security;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class QuizIntroControl : FrameworkControl, IAttributeControl
    {
        AttributeControl _attrControl;
        string _selectedValue;

        FrameworkControl _resourceControl;

        public string ContainerDivClientID { get { return divControl.ClientID; } }

        public bool IsOverlayReg { get; set; }

        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "block");

        }
        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }
        public ValidationMessage ValidationMessage { get { return txtValidation; } }
        public void Save()
        {

        }
        public void SetValue(string value)
        {
            _selectedValue = value;

        }
        public string GetValue()
        {
            _selectedValue = "";

            return _selectedValue.ToString();
            //_selectedValue.ToString();

        }
        public string ScriptBlock()
        {
            return "";
        }
        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return "";
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }
        public void PersistToCookie(HttpCookie cookie)
        {
            //if (!String.IsNullOrEmpty(_attrControl.AttributeName))
            //    cookie[_attrControl.AttributeName] = GetValue();
            //else
            //    cookie[_attrControl.Name] = GetValue();

        }

        public void Persist(ITemporaryPersistence persistence)
        {
            
        }

        public bool Validate()
        {
            bool valid = true;

            return valid;
        }




        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            divControl.Attributes["class"] = _attrControl.Name;
            //   txtCaptchaTextbox.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);
        }



        private void Page_PreRender(object sender, System.EventArgs e)
        {
            //AttributeTextControl.Text = _attrControl.AttributeTextValue;


            // txtCaptchaTextbox.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);

        }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            return new NameValueCollection();
        }
    }
}