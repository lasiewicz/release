﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeListItemControl.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeListItemControl" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<asp:PlaceHolder runat="server"></asp:PlaceHolder>
<div id="divControl" runat="server">
    <%=GetTitleWrapperOpen %><mn:Txt runat="server" ID="txtCaption" />
    <%=GetTitleWrapperClose %>
    <%=GetFieldWrapperOpen %>
    <asp:PlaceHolder runat="server" ID="plcCtrl"></asp:PlaceHolder>
    <%=GetFieldWrapperClose %>
    <uc2:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
</div>
