﻿using System.Collections.Generic;
using System.Text;

namespace Matchnet.Web.Applications.Registration
{
    public class JQueryValidation
    {
        public Dictionary<string, List<ValidationEntry>> ValidationRules;

        public JQueryValidation()
        {
            ValidationRules = new Dictionary<string, List<ValidationEntry>>();
        }

        public void AddValidationRules(List <ValidationDictionaryEntry> newRulesList)
        {
            foreach (var newRule in newRulesList)
            {
                ValidationRules.Add(newRule.UniqueID, newRule.ValidationEntryList);
            }
        }

        public string GetValidationJSCode()
        {

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("var validator =");
            sb.AppendLine(Newtonsoft.Json.JsonConvert.SerializeObject(ValidationRules));
            sb.AppendLine(";");
            return sb.ToString();
        }
    }

    public class ValidationEntry
    {
        public string errorKey { get; set; }
        public string message { get; set; }
        public string value { get; set; }
    }

    public class ValidationDictionaryEntry
    {
        public string UniqueID { get; set; }
        public List<ValidationEntry> ValidationEntryList { get; set; }

        public ValidationDictionaryEntry()
        {
            ValidationEntryList = new List<ValidationEntry>();
        }
    }
}