﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;
using Matchnet.Lib;
using Matchnet.Lib.Util;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Applications.Registration;

namespace Matchnet.Web.Applications.Registration
{
    public partial class Landing1 : FrameworkControl
    {
        private const string OMNITURE_PAGE_NAME = "Landing Page {0} - {2}";
        Registration idRegStep;
        protected override void OnInit(EventArgs e)
        {
            try
            {
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BlackSingles)
                {
                    if (Request["prm"] != null)
                    {
                        g.Transfer(FrameworkGlobals.LinkHref("/?" + Request.QueryString, false));
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            try
            {
                base.OnInit(e);
                idRegStep = (Registration)LoadControl("/Applications/Registration/Registration.ascx");
                idRegStep.TemplatePath = "/Applications/Registration/XML/LandingPage.xml";
                idRegStep.AjaxEnabled = false;
                idRegStep.IsSplash = true;
                if (!Page.IsPostBack)
                    idRegStep.LoadNewWizard = true;

                idRegStep.SetOmniture = false;
                phContent.Controls.Add(idRegStep);
            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            g.AnalyticsOmniture.PageName = idRegStep.GetOmnitureRegistrationPageName(OMNITURE_PAGE_NAME);

            g.AnalyticsOmniture.Evar16 = idRegStep.GetOmnitureRegistrationScenario();
        }
    }
}