﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.SearchElements;

namespace Matchnet.Web.Applications.Article
{
    public partial class Bachelor : FrameworkControl
    {
        private static string BACHELOR_MAIN_PAGE_URL = "http://static.jdate.co.il/Microsites/Bachelor/index.html?userstat=";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _g.Transfer("/Applications/Home/Default.aspx", "301");
                Response.End();
                //char userStat = 'V';
                //if (g.Member != null)
                //{
                //    userStat = g.Member.IsPayingMember(g.Brand.Site.SiteID) ? 'S' : 'M';
                //}

                //string bachelorURL = BACHELOR_MAIN_PAGE_URL + userStat;

                //ifrmBachelorPage.Attributes.Add("src", bachelorURL);

                //g.BreadCrumbTrailFooter.Visible = false;
                //g.BreadCrumbTrailHeader.Visible = false;

                //g.AnalyticsOmniture.PageName = "Bachelor Bedrock";

                ////Determine whether to display mini-search
                //MiniSearchBar miniSearchBar = null;
                //if (g.HeaderControl != null)
                //{
                //    miniSearchBar = (g.HeaderControl as Header20).MiniSearchBar;
                //    var isPhotoGallery30Enabled = PhotoGalleryManager.Instance.IsPhotoGallery30Enabled(_g.Brand);

                //    if (miniSearchBar != null && isPhotoGallery30Enabled)
                //    {
                //        //load mini-search
                //        miniSearchBar.Visible = true;
                //        miniSearchBar.LoadMiniSearch();
                //    }
                //}
            }
            catch (Exception ex)
            {

                g.ProcessException(ex);
            }
        }
    }
}