﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Article
{
    public partial class JExperts : FrameworkControl
    {
        private static string JEXPERTS_MAIN_PAGE_URL = "http://static.jdate.co.il/Microsites/Jexperts/index.html?userstat=";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                char userStat = 'V';
                if (g.Member != null)
                {
                    userStat = g.Member.IsPayingMember(g.Brand.Site.SiteID) ? 'S' : 'M';
                }

                string jExpertsURL = JEXPERTS_MAIN_PAGE_URL + userStat;

                if (!string.IsNullOrEmpty(Request.QueryString["jexpertspageid"]))
                {
                    jExpertsURL += "&jexpertspageid=" + Request.QueryString["jexpertspageid"];
                }

                ifrmExpertsPage.Attributes.Add("src", jExpertsURL);

                g.BreadCrumbTrailFooter.Visible = false;
                g.BreadCrumbTrailHeader.Visible = false;

                g.AnalyticsOmniture.PageName = "Jexperts Bedrock";
            }
            catch (Exception ex)
            {

                g.ProcessException(ex);
            }
        }
    }
}