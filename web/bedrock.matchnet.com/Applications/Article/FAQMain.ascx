<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ArticleContent" Src="Controls/ArticleContent20.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FAQMain.ascx.cs" Inherits="Matchnet.Web.Applications.Article.FAQMain" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div id="rightNew" class="page-container">
	<mn:Title runat="server" id="ttlFAQ" ResourceConstant="NAV_FREQUENTLY_ASKED_QUESTIONS" ImageName="hdr_faqmain.gif" CommunitiesForImage="CI;" />
	<!--<h1><mn:txt runat="server" id="txtFAQ" ResourceConstant="NAV_FREQUENTLY_ASKED_QUESTIONS" /></h1>-->
	<div id="article-classic">	
		<uc1:ArticleContent id="ArticleContent1" ArticleID="6314" runat="server"></uc1:ArticleContent>
    </div>
</div>
