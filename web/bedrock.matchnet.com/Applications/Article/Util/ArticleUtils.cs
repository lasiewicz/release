using System;
using System.Web;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Article;

namespace Matchnet.Web.Applications.Article.Util
{
	/// <summary>
	/// </summary>
	public class ArticleUtils
	{
		public static int DetermineArticleIDFromRequest(HttpRequest Request, int SiteID)
		{
			int retVal = Constants.NULL_INT;

			int categoryID = Conversion.CInt(Request["CategoryID"]);
			int articleID = Conversion.CInt(Request["ArticleID"]);

			if(articleID != Constants.NULL_INT)
			{
				//	If they gave us a specific ArticleID then use it
				retVal = articleID;
			}
			else if(categoryID != Constants.NULL_INT)
			{
				// If we didn't get a specific ArticleID then pull
				//	back the ArticleInfo and grab the first referenced
				//	article.

				retVal = DetermineArticleIDFromCategoryID(categoryID, SiteID);
			}

			return(retVal);
		}

		public static int DetermineArticleIDFromCategoryID(int CategoryID, int SiteID)
		{
			int retVal = Constants.NULL_INT;

			SiteArticleCollection articles = ArticleSA.Instance.RetrieveCategorySiteArticles(CategoryID, SiteID);
			if(articles.Count > 0)
			{
				retVal = articles[0].ArticleID;
			}

			return(retVal);
		}

		public static int DetermineCategoryIDFromRequest(HttpRequest Request)
		{
			int categoryID = Conversion.CInt(Request["CategoryID"]);
			return(categoryID);
		}
	}
}
