﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Web.Applications.Article.Controls;
using Matchnet.Web.Applications.Article.Util;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Article
{
    public partial class ArticleDefault20 : FrameworkControl
    {
        private int _categoryID;
        public int CategoryID
        {
            get { return (_categoryID); }
            set { _categoryID = value; }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                CategoryID = ArticleUtils.DetermineCategoryIDFromRequest(Request);
                //SiteCategoryCollection siteCategories = ArticleSA.Instance.RetrieveSiteCategoryChildren(g.Brand.Site.SiteID, CategoryID);
                //Need to retrieve the current category to display its title in the BreadCrumb:
                SiteCategory siteCategory = ArticleSA.Instance.RetrieveSiteCategory(g.Brand.Site.SiteID, CategoryID);

                // Set the title if the Request says to.
                if (1 == Conversion.CInt(Request[WebConstants.URL_PARAMETER_NAME_SHOWTITLE], Constants.NULL_INT))
                {
                    SetArticleTitle();
                }

                //foreach(SiteCategory mySiteCategory in siteCategories)
                //{
                int siteCategoryID = siteCategory.SiteCategoryID;
                string content = FrameworkGlobals.GetUnicodeText(siteCategory.Content);

                //	Kind of a hack to keep these out of actual display
                string badPrefix = "(YNM4) ";
                if (content.StartsWith(badPrefix))
                {
                    content = content.Substring(badPrefix.Length);
                }

                if(CategoryID==2010 && g.Brand.Site.SiteID== (int)WebConstants.SITE_ID.JDateCoIL)
                {
                    content = g.GetResource("CATEGORY_2010_LABEL_FOR_SEO", this);
                }

                //if(siteCategoryID == CategoryID)
                //{
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(content, null);
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(content, null);
                //}
                //}
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void SetArticleTitle()
        {
            int articleID = ArticleUtils.DetermineArticleIDFromRequest(Request, g.Brand.Site.SiteID);

            SiteArticle siteArticle = ArticleSA.Instance.RetrieveSiteArticle(articleID, g.Brand.Site.SiteID);
            if (siteArticle != null)
            {
                lblTitle.Text = siteArticle.Title;
                plcTitle.Visible = true;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}