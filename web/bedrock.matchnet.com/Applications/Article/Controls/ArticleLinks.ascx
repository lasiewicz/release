<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ArticleLinks.ascx.cs" Inherits="Matchnet.Web.Applications.Article.Controls.ArticleLinks" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<asp:Repeater id="linkRepeater" runat="server">
	<ItemTemplate>
		<div>
			<asp:HyperLink ID="linkArticleCategory" Runat="server" />
			<asp:Label ID="labelArticleCategory" Runat="server" CssClass="copyright" />
		</div>
	</ItemTemplate>
</asp:Repeater>

<asp:Label id="labelTitle" runat="server" />