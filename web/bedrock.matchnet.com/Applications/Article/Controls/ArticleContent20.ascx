﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArticleContent20.ascx.cs" Inherits="Matchnet.Web.Applications.Article.Controls.ArticleContent20" %>
<%@ Register TagPrefix="uc1" TagName="ArticleNav" Src="ArticleNav20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>


<input type="hidden" id="NavStartIndex" runat="server" />

	<asp:PlaceHolder ID=plcTopArticleNav Runat=server Visible=False>
        <uc1:ArticleNav id="topArticleNav" runat="server" />
	</asp:PlaceHolder>

<%--show man's third party article--%>
	<asp:PlaceHolder id=plcThirdPartyArticleMan runat=server visible=false>
			<iframe src="http://catchhimandkeephim.com/m/AmericanSingles/" width="430px" height="800px" scrolling="no" frameborder="0">
			[Your user agent does not support frames or is currently configured
				not to display frames. However, you may visit
				<a href="http://catchhimandkeephim.com/m/AmericanSingles/">the site here.</A>]
			</iframe>

			<mn:AdUnit id="adWideskyscraperArticle2" Size="WideSkyscraperArticle" runat="server" visible="false" expandImageTokens="true" />
	</asp:PlaceHolder>

<%--show the woman's articles--%>	
	<asp:PlaceHolder id=plcThirdPartyArticleWoman runat=server visible=false>
			<iframe src="http://doubleyourdating.com/m/AmericanSingles/" width="430px" height="800px" scrolling="no" frameborder="0">
			[Your user agent does not support frames or is currently configured
				not to display frames. However, you may visit
				<a href="http://doubleyourdating.com/m/AmericanSingles/">the site here.</A>]
			</iframe>

			<mn:AdUnit id="adWideskyscraperArticle1" Size="WideSkyscraperArticle" runat="server" visible="false" expandImageTokens="true" />
	</asp:PlaceHolder>

<%--Show the Total Beauty article--%>
	<asp:PlaceHolder id=phlTotalBeauty runat=server visible=false>
			<iframe src="http://static.jdate.com/Components/MPR-0626_TotalBeauty/index.html" width="590px" height="1024px" scrolling="no" frameborder="0">
			[Your user agent does not support frames or is currently configured
				not to display frames. However, you may visit
				<a href="http://static.jdate.com/Components/MPR-0626_TotalBeauty/index.html">the site here.</A>]
			</iframe>
	</asp:PlaceHolder>

<%--old crappy code that is indescipherable (lol, spell that :)--%>
	<asp:PlaceHolder id=plcArticleContent runat=server visible=true>
        <asp:Literal id="articleContent" runat="server"  />
		<mn:AdUnit id="adWideskyscraperArticle" Size="WideSkyscraperArticle" runat="server" visible="false" expandImageTokens="true" />
	</asp:PlaceHolder>
	
	<asp:PlaceHolder ID=plcBottomArticleNav Runat=server Visible=False>
        <uc1:ArticleNav id="bottomArticleNav" runat="server" />
	</asp:PlaceHolder>
	
