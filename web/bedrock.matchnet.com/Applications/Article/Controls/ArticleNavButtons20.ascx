﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArticleNavButtons20.ascx.cs" Inherits="Matchnet.Web.Applications.Article.Controls.ArticleNavButtons20" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div class="pagination-buttons image-text-pair float-outside">
<a href="" runat="server" class="btn link-secondary small" id="lnkPrevious">
 <span class="direction">&#9668</span>
 <span class="prev">
        <mn2:Txt runat="server" ID="btnPrevious" ResourceConstant="TXT_PREVIOUS" Visible="true"/>
 </span>
</a>&nbsp;<a href="" class="btn link-secondary" runat="server" id="lnkNext">
<span class="next">
<mn2:Txt runat="server" ID="btnNext" ResourceConstant="TXT_NEXT" Visible="true" />
</span>
 <span class="direction"><mn2:Txt runat="server" ID="directionArrow" ResourceConstant="DIRECTION_ARROW" Visible="true" /></span>
</a>
</div>