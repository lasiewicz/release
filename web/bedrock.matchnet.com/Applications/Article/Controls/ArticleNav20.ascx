﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ArticleNav20.ascx.cs"
    Inherits="Matchnet.Web.Applications.Article.Controls.ArticleNav20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ArticleNavButtons" Src="ArticleNavButtons20.ascx" %>
<div class="text-outside article-marketing">
    <mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_FOUND_LOVE_ON_BRAND" />
    <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_TELL_US_ABOUT_IT" />
</div>
<div class="wrapper">
    <div id="article-pagination-bottom" class="article-navigation clearfix">
        <asp:HyperLink runat="server" ID="lnkPreviousTen" Visible="False">
            <mn:Txt ID="txtPreviousTen" runat="server" ResourceConstant="TXT_PREVIOUS_TEN" />
        </asp:HyperLink>
        <asp:HyperLink runat="server" ID="lnkPreviousArticle" Visible="False">
            <mn:Txt ID="txtPrevious" runat="server" ResourceConstant="TXT_PREVIOUS" />
        </asp:HyperLink>
        <asp:Repeater runat="server" ID="rptIndexLinks">
            <ItemTemplate>
                <asp:HyperLink runat="server" ID="lnkIndexLink" />
                <mn:Txt runat="server" ID="txtCurrentLinkText" Visible="false" />
            </ItemTemplate>
            <SeparatorTemplate>
                <mn:Txt ID="txtSpacer" runat="server" ResourceConstant="TXT_SPACER" />
            </SeparatorTemplate>
        </asp:Repeater>
        <asp:HyperLink runat="server" ID="lnkNextArticle" Visible="False">
            <mn:Txt ID="txtNext" runat="server" ResourceConstant="TXT_NEXT" />
        </asp:HyperLink>
        <asp:HyperLink runat="server" ID="lnkNextTen" Visible="False">
            <mn:Txt ID="txtNextTen" runat="server" ResourceConstant="TXT_NEXT_TEN" />
        </asp:HyperLink>
    </div>
</div>
<div id="article-pagination-buttons-bottom">
    <uc1:ArticleNavButtons ID="idArticleNavButtons" runat="server"></uc1:ArticleNavButtons>
</div>
