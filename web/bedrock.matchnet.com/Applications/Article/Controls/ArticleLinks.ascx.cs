using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.Article.Util;

namespace Matchnet.Web.Applications.Article.Controls
{
	/// <summary>
	/// </summary>
	public class ArticleLinks : FrameworkControl
	{
		protected System.Web.UI.WebControls.Repeater linkRepeater;
		protected System.Web.UI.WebControls.Label labelTitle;
		private string _qPrefix;

		private int _CategoryID;
		public int CategoryID
		{
			get { return(_CategoryID); }
			set { _CategoryID = value; }
		}

		private int _ArticleID;
		public int ArticleID
		{
			get { return(_ArticleID); }
			set { _ArticleID = value; }
		}

		private string _DocumentType;
		protected string DocumentType
		{
			get { return(_DocumentType); }
			set { _DocumentType = value; }
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				//	Do this once rather than pulling it for every list item.
				_qPrefix = g.GetResource("TXT_Q", this) + " ";

				CategoryID = ArticleUtils.DetermineCategoryIDFromRequest(Request);
				ArticleID = ArticleUtils.DetermineArticleIDFromRequest(Request, g.Brand.Site.SiteID);

				SiteArticleCollection siteArticles = ArticleSA.Instance.RetrieveCategorySiteArticles(CategoryID, g.Brand.Site.SiteID);
				if(siteArticles.Count > 1)
				{
					linkRepeater.ItemDataBound += new RepeaterItemEventHandler(linkRepeater_ItemDataBound_Article);
					linkRepeater.DataSource = siteArticles;
					linkRepeater.DataBind();
				}
				else
				{
					string title = String.Empty;
					if(siteArticles.Count > 0)
					{
						title = (string)siteArticles[0].Title;
						labelTitle.Text = FrameworkGlobals.GetUnicodeText(title);
					}

					linkRepeater.ItemDataBound += new RepeaterItemEventHandler(linkRepeater_ItemDataBound_Category);
					// DataTable siteCategories = categories.RetrieveChildInfo(WebConstants.TEMP_TRANSLATION_ID, CategoryID);
					SiteCategoryCollection siteCategories = ArticleSA.Instance.RetrieveSiteCategoryChildren(g.Brand.Site.SiteID, CategoryID);
					linkRepeater.DataSource = siteCategories;
					linkRepeater.DataBind();
				}
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		protected void linkRepeater_ItemDataBound_Category(Object Sender, RepeaterItemEventArgs e)
		{
			//	Get our controls.
			HyperLink linkArticleCategory = (HyperLink)e.Item.FindControl("linkArticleCategory");
			Label labelArticleCategory = (Label)e.Item.FindControl("labelArticleCategory");

			// DataRowView myRow = (DataRowView)e.Item.DataItem;
			SiteCategory siteCategory = (SiteCategory)e.Item.DataItem;

			int categoryID = siteCategory.CategoryData.CategoryID;
			string content = FrameworkGlobals.GetUnicodeText(siteCategory.Content);

			if(categoryID == CategoryID)
			{
				//	No link.  We are dealing with the current category.
				linkArticleCategory.Visible = false;
				labelArticleCategory.Visible = true;
				labelArticleCategory.Text = content;
			}
			else
			{
				//	Make the link.
				linkArticleCategory.Visible = true;
				labelArticleCategory.Visible = false;
				linkArticleCategory.Text = content;
				linkArticleCategory.NavigateUrl = "/Applications/Article/ArticleView.aspx?CategoryID=" + Server.UrlEncode(categoryID.ToString())
														+ "&ParentCategoryID=" + Server.UrlEncode(CategoryID.ToString());
			}
		}

		protected void linkRepeater_ItemDataBound_Article(Object Sender, RepeaterItemEventArgs e)
		{
			//	Get our controls.
			HyperLink linkArticleCategory = (HyperLink)e.Item.FindControl("linkArticleCategory");
			Label labelArticleCategory = (Label)e.Item.FindControl("labelArticleCategory");

			// DataRowView myRow = (DataRowView)e.Item.DataItem;
			SiteArticle siteArticle = (SiteArticle)e.Item.DataItem;

			// int articleID = (int)myRow["ArticleExID"];
			int articleID = siteArticle.ArticleID;
			// string content = (string)myRow["Title"];
			string content = siteArticle.Title;
			content = FrameworkGlobals.GetUnicodeText(_qPrefix + content);

			if(articleID == ArticleID)
			{
				//	No link.  We are dealing with the current category.
				linkArticleCategory.Visible = false;
				labelArticleCategory.Visible = true;
				labelArticleCategory.Text = content;
			}
			else
			{
				//	Make the link.
				linkArticleCategory.Visible = true;
				labelArticleCategory.Visible = false;
				linkArticleCategory.Text = content;
				linkArticleCategory.NavigateUrl = "/Applications/Article/ArticleView.aspx?CategoryID=" + Server.UrlEncode(CategoryID.ToString()) + "&ArticleID=" + Server.UrlEncode(articleID.ToString()) + "&RowNumber=1";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
