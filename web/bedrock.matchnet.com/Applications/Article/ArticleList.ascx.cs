namespace Matchnet.Web.Applications.Article
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using Matchnet.Content.ServiceAdapters;
	using Matchnet.Content.ValueObjects.Article;
	using Matchnet.Web.Framework;
	using Matchnet.Web.Applications.Article.Util;
	using Matchnet.Web.Applications.Article.Controls;

	/// <summary>
	///		Summary description for ArticleList.
	/// </summary>
	public class ArticleList : FrameworkControl
	{
		protected System.Web.UI.WebControls.Repeater LinkRepeater;
		protected System.Web.UI.WebControls.Label CategoryLabel;

		private int _CategoryID;
		public int CategoryID
		{
			get { return(_CategoryID); }
			set { _CategoryID = value; }
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				CategoryID = ArticleUtils.DetermineCategoryIDFromRequest(Request);

				SiteCategory siteCategory = ArticleSA.Instance.RetrieveSiteCategory(g.Brand.Site.SiteID, CategoryID);
				string myTitle = siteCategory.Content;
				CategoryLabel.Text = myTitle;

				SiteArticleCollection siteArticles = ArticleSA.Instance.RetrieveCategorySiteArticles(CategoryID, g.Brand.Site.SiteID);
				LinkRepeater.DataSource = siteArticles;
				LinkRepeater.DataBind();
			} 
			catch (Exception ex) { g.ProcessException(ex); }
		}

		protected void LinkRepeater_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
		{
			//	Get our controls.
			HyperLink myLink = (HyperLink)e.Item.FindControl("MyLink");
			// DataRowView myRow = (DataRowView)e.Item.DataItem;
			SiteArticle siteArtcle = (SiteArticle)e.Item.DataItem;

			// int articleID = (int)myRow["ArticleExID"];
			int articleID = siteArtcle.SiteArticleID;
			// string content = (string)myRow["Title"];
			string content = siteArtcle.Title;

			myLink.Text = content;
			myLink.NavigateUrl = "/Applications/Article/ArticleView.aspx?CategoryID=" + Server.UrlEncode(CategoryID.ToString()) + "&ArticleID=" + Server.UrlEncode(articleID.ToString());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			this.LinkRepeater.ItemDataBound += new RepeaterItemEventHandler(LinkRepeater_ItemDataBound);

		}
		#endregion
	}
}
