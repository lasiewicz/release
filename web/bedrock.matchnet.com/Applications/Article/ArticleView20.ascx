﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ArticleView20.ascx.cs" Inherits="Matchnet.Web.Applications.Article.ArticleView20" %>
<%@ Register TagPrefix="uc1" TagName="ArticleLinks" Src="Controls/ArticleLinks.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ArticleContent" Src="Controls/ArticleContent20.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ArticleNavButtons" Src="Controls/ArticleNavButtons20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="page-container" class="editorial">
	<asp:PlaceHolder runat="server" Visible="false" ID="phSuccessStoryTitle">
	<div class="clearfix">
	    <h1 class="float-inside"><mn:txt id="successStory" runat="server" ResourceConstant="TXT_SUCCESS_STORY" /></h1>
	    <div class="float-outside"><mn:txt id="txtArticleCount" runat="server"  visible="false"/></div>
	</div>
	<div class="text-outside article-marketing">
		<mn:txt id="Txt4" runat="server" ResourceConstant="TXT_FOUND_LOVE_ON_BRAND" />
		<mn:txt id="Txt1" runat="server" ResourceConstant="TXT_TELL_US_ABOUT_IT" />
	</div>
		</asp:PlaceHolder>
    <div class="article-navigation clearfix">
        <asp:PlaceHolder runat="server" Visible="true" ID="phArticleTitle">
        <div class="float-inside">	
	        <h2><asp:Label id="lblTitle" runat="server"></asp:Label></h2>  
	    </div>
	    </asp:PlaceHolder>
	    <div>
	        <uc1:ArticleNavButtons id="idArticleNavButtons" runat="server" Visible=false></uc1:ArticleNavButtons>
	    </div>
	</div>
	

		<mn:txt id="backLink" runat="server" ResourceConstant="TXT_BACK" />
			<mn:txt id="moreTopics" runat="server" ResourceConstant="TXT_MORE_TOPICS_ON_THIS_SUBJECT" href="#moreQ" />
			<uc1:ArticleContent id="ArticleContent1" runat="server"></uc1:ArticleContent>
	        <asp:Panel id="panelEndNav" runat="server">
			<hr />
			<a name="moreQ"></a>
			<h2><mn:txt runat="server" id="txtMoreTopics" resourceConstant="TXT_MORE_TOPICS_ON_SUBJECT" /></h2>
			<uc1:ArticleLinks id="ArticleLinks1" runat="server"></uc1:ArticleLinks>
		</asp:Panel>
</div>
