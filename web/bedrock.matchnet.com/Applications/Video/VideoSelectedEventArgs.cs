﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Matchnet.Web.Applications.Video
{
    /// <summary>
    /// Encapsulates the necessary elements when a new video is selected either from 
    /// the playlist or the 'more videos' section. 
    /// </summary>
    public class VideoSelectedEventArgs : System.EventArgs
    {
        public string VideoID { get; set; }
        public VideoViewingType ViewingType { get; set; }

        public VideoSelectedEventArgs(string videoID) : this(videoID, VideoViewingType.Playlist) { }
        public VideoSelectedEventArgs(string videoID, VideoViewingType viewingType)
        {
            VideoID = videoID;
            viewingType = ViewingType;
        }
    }

    public delegate void VideoSelectedHandler(object sender, VideoSelectedEventArgs e);
}
