﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Matchnet.Web.Applications.Video.Controls
{
    public partial class VideoEpisodeTabHeader : System.Web.UI.UserControl
    {
        public List<EpisodeTab> Tabs = new List<EpisodeTab>();
        public int SelectedTabIndex { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            rptTabs.DataSource = Tabs;
            rptTabs.DataBind();
        }

        protected void rptTabs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            EpisodeTab tab = (EpisodeTab)e.Item.DataItem;
            if (SelectedTabIndex > 0 && Tabs[SelectedTabIndex - 1] != null && Tabs[SelectedTabIndex - 1] == tab)
            {
                HtmlGenericControl li = (HtmlGenericControl)e.Item.FindControl("liTab");
                li.Attributes["class"] = "tab selected";
            }
        }
    }
}