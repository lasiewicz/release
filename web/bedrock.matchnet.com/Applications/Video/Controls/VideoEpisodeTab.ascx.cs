﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Video.Controls
{
    public partial class VideoEpisodeTab : FrameworkControl
    {
        private string _titleResourceConstant;
        private string _subTitleResourceConstant;
        private string _descriptionResourceConstant;
        private string _link;
        
        public string TitleResourceConstant
        {
            get { return _titleResourceConstant; }
            set { _titleResourceConstant = value; }
        }

        public string SubTitleResourceConstant
        {
            get { return _subTitleResourceConstant; }
            set { _subTitleResourceConstant = value; }
        }

        public string DescriptionResourceConstant
        {
            get { return _descriptionResourceConstant; }
            set { _descriptionResourceConstant = value; }
        }

        public string Link
        {
            get { return _link; }
            set { _link = value; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            literalTitle.ResourceConstant = _titleResourceConstant;
            literalSubTitle.ResourceConstant = _subTitleResourceConstant;
            literalDescription.ResourceConstant = _descriptionResourceConstant;
            lnkTab.NavigateUrl = _link;
        }
    }
}