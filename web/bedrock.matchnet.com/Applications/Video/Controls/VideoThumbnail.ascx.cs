﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.Video;

namespace Matchnet.Web.Applications.Video.Controls
{
    /// <summary>
    /// User control used in the playlist that visually and functionally represents a single video. 
    /// </summary>
    public partial class VideoThumbnail : FrameworkControl
    {
        #region Public Properties
        public string TitleResourceConstant { get; set; }
        public string DescriptionResourceConstant { get; set; }
        public string ImagePath { get; set; }
        public string VideoID { get; set; }
        public bool Selected { get; set; }
        public bool Disabled { get; set; }
        #endregion

        #region Events
        public event VideoSelectedHandler VideoSelected;
        #endregion

        #region Event Handlers
        /// <summary>
        /// Sets all the initial values for the controls on the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                literalDescription.ResourceConstant = DescriptionResourceConstant;
                imgThumbnail.ImageUrl = ImagePath;
                imgThumbnail.CommandArgument = lnkTitle.CommandArgument = VideoID;
                //The javascript function 'setClickedVideo' is located in YouTubeMain.ascx
                imgThumbnail.OnClientClick = lnkTitle.OnClientClick = string.Format("setClickedVideo('{0}');", VideoID);
                imgThumbnail.AlternateText = imgThumbnail.ToolTip = g.GetResource(TitleResourceConstant, this);
                lnkTitle.ResourceConstant = TitleResourceConstant;
                hdnVideoID.Value = VideoID;

                if (Selected)
                {
                    //ToDo: Eventually I think we'll want to change out the style if this is the 
                    //selected thumbnail
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }

        /// <summary>
        /// Raises the VideoSelected event when the user clicks on the imagebutton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgThumbnail_Command(object sender, CommandEventArgs e)
        {
            VideoSelected(this, new VideoSelectedEventArgs(e.CommandArgument.ToString()));
        }

        /// <summary>
        /// Raises the VideoSelected event when the user clicks on the linkbutton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkTitle_Command(object sender, CommandEventArgs e)
        {
            VideoSelected(this, new VideoSelectedEventArgs(e.CommandArgument.ToString()));
        }

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
            base.OnInit(e);
        }
        #endregion
    }
}