﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Video.Controls
{
    public partial class OOyalaPlayer : FrameworkControl
    {  
        private const string SETTING_VIDEO_MODE = "VIDEO_MODE";
        private const string SETTING_VIDEO_ID = "VIDEO_CHANNEL_ID";
        private const string SETTING_VIDEO_URL_PARAMS = "VIDEO_CHANNEL_PARAMS";
        #region Public properties
        public string VideoID { get; set; }
        public string VideoParams { get; set; }
        public string PlayerMode { get; set; }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Registers the include file needed to get the YouTube player to work correctly. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Uses a VideoSummary to set the necessary properties
        /// </summary>
        /// <param name="summary"></param>
        public void SetPlayerProperties(VideoSummary summary)
        {
            VideoID = summary.ID;
            //literalTitle.ResourceConstant = summary.TitleResourceConstant;
            //literalDescription.ResourceConstant = summary.DescriptionResourceConstant;
        }

        //no summary - channel type
         public void SetPlayerProperties()
        {
           
            //literalTitle.Visible=false;
            //literalDescription.Visible=false;
            getSettings();

        }

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
            base.OnInit(e);
        }
        #endregion

          private void getSettings()
        {
            try
            {
                //PlayerMode = Configuration.ServiceAdapters.RuntimeSettings.GetSetting(SETTING_VIDEO_MODE, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                //VideoID = Configuration.ServiceAdapters.RuntimeSettings.GetSetting(SETTING_VIDEO_ID, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
               // VideoParams = Configuration.ServiceAdapters.RuntimeSettings.GetSetting(SETTING_VIDEO_URL_PARAMS, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                
               
            }
            catch (Exception ex)
            { }
        }
    }
    }
