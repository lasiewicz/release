﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MoreVideos.ascx.cs" Inherits="Matchnet.Web.Applications.Video.Controls.MoreVideos" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div class="videoList">
    <div class="categoryHeader">more videos</div>
    <div class="videoListContainer">
        <asp:DataList ID="dlstMoreVideos" runat="server" RepeatColumns="2" RepeatDirection="Vertical" OnItemDataBound="dlstMoreVideos_ItemDataBound">
            <ItemTemplate>
	            <div class="videoListTeaser"><span class="videoListTeaserPhoto"><asp:ImageButton ID="imgBtnMoreVideo" runat="server" AlternateText='Photo' width="100" height="63"  ImageUrl='<%# DataBinder.Eval(Container.DataItem, "ImagePath") %>' CommandName="ChangeVideo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>' OnCommand="imgBtnMoreVideo_Command" /> </span>
	                <span class="videoListTeaserContent">
		                <h2><mn2:FrameworkLinkButton ID="lnkBtnMoreVideo" runat="server" CommandName="ChangeVideo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>' OnCommand="lnkBtnMoreVideo_Command" ResourceConstant='<%# DataBinder.Eval(Container.DataItem, "TitleResourceConstant") %>' /></h2>
		                <mn2:FrameworkLiteral runat="server" id="literalDescription" ResourceConstant='<%# DataBinder.Eval(Container.DataItem, "DescriptionResourceConstant") %>' />
		            </span>
		        </div>			
            </ItemTemplate>
        </asp:DataList>
    </div>
</div>