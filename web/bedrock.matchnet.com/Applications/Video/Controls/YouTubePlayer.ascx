﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="YouTubePlayer.ascx.cs" Inherits="Matchnet.Web.Applications.Video.Controls.YouTubePlayer" EnableViewState="false"%>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<script type="text/javascript">
function onYouTubePlayerReady(playerId) {
  ytplayer = document.getElementById("myytplayer");
  ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
}

function onytplayerStateChange(newState) {
    //if video is over, play next video
    if(newState == 0)
    {
        //this function is in YouTubeMain.ascx
        loadNextPlaylistVideo();
    }
    if(newState == 5)
    {
        ytplayer.playVideo();
    }
}

function isPlayerFinished()
{
    if(ytplayer.getPlayerState() == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function getPlayerState()
{
    return ytplayer.getPlayerState();
}    

</script>
<div id="ytapiplayer">
  You need Flash player 8+ and JavaScript enabled to view this video.
</div>
<script type="text/javascript">

      // allowScriptAccess must be set to allow the Javascript from one domain to access the swf on the youtube domain
    var params = { allowScriptAccess: "always" };
    // this sets the id of the object or embed tag to 'myytplayer'. You then use this id to access the swf and make calls to the player's API
    var atts = { id: "myytplayer"  };
    swfobject.embedSWF("http://www.youtube.com/v/<%=VideoID %>&amp;border=0&amp;enablejsapi=1&amp;playerapiid=ytplayer&rel=0", 
                    "ytapiplayer", "482", "387", "8",null, null, params, atts);
  

</script>
<br />
<div class="videoPlayerInfo">
    <H2><mn2:FrameworkLiteral runat="server" id="literalTitle"></mn2:FrameworkLiteral></H2>
    <mn2:FrameworkLiteral runat="server" id="literalDescription"></mn2:FrameworkLiteral>
    
</div>
