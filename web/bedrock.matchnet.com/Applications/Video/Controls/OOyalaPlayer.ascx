﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OOyalaPlayer.ascx.cs" Inherits="Matchnet.Web.Applications.Video.Controls.OOyalaPlayer" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<script type="text/javascript">
var ytplayer;

function receiveOoyalaEvent(playerId, eventName, eventParams)
{
    if(!ytplayer)
    {
        ytplayer = document.getElementById("ytplayer");
    }
    
    switch(eventName)
    {
        case 'stateChanged':
            onytplayerStateChange();
            return;
    }
}

function onytplayerStateChange() {
    //if video is over, play next video
    if(isPlayerFinished())
    {
        //this function is in YouTubeMain.ascx
        loadNextPlaylistVideo();
    }
}

function isPlayerFinished()
{
    var state = ytplayer.getState();
    var currentTime = ytplayer.getPlayheadTime();
    var totalTime = ytplayer.getTotalTime();
    var returnvalue = false;
    
    if(state == "paused" && !isNaN(currentTime))
    {
        var timeFromEnd = Number(totalTime) - Number(currentTime);
        if(timeFromEnd < 1.5)
        {
            returnvalue = true;
        }
    }
 
    return returnvalue;
}


</script>

<!--<script src="http://www.ooyala.com/player.js?callback=receiveOoyalaEvent&autoplay=1&playerId=ytplayer&embedCode=<%=VideoID %>" type="text/javascript"></script>-->
<script src="http://www.ooyala.com/player.js?callback=receiveOoyalaEvent&autoplay=1&playerId=ytplayer&embedCode=<%=VideoID %><%=VideoParams%>&wmode=transparent" type="text/javascript"></script>