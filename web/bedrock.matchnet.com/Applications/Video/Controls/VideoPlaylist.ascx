﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="VideoPlaylist.ascx.cs" Inherits="Matchnet.Web.Applications.Video.Controls.VideoPlaylist" EnableViewState="false"%>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="vid" Tagname="VideoThumbnail" Src="VideoThumbnail.ascx" %>
<mn:txt CssClass="categoryHeader" runat="server" id="txtPlaylist" ResourceConstant="TXT_PLAYLIST" />
<div class="playListContainer">
   <asp:Repeater ID="rptPlaylist" runat="server" 
        onitemdatabound="rptPlaylist_ItemDataBound">
    <ItemTemplate>
        <vid:VideoThumbnail runat="server" ID="vid" DescriptionResourceConstant='<%# DataBinder.Eval(Container.DataItem, "DescriptionResourceConstant") %>' TitleResourceConstant='<%# DataBinder.Eval(Container.DataItem, "TitleResourceConstant") %>' ImagePath='<%# DataBinder.Eval(Container.DataItem, "ImagePath") %>' VideoID='<%# DataBinder.Eval(Container.DataItem, "ID") %>' />
    </ItemTemplate>
    </asp:Repeater>
</div>

<asp:HiddenField ID="hdnCurrentPlaylistPosition" runat="server" Value="1"/>	