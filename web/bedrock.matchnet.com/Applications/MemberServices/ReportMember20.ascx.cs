﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class ReportMember20 : FrameworkControl
    {
        int TargetMemberID = Constants.NULL_INT;
        int MemberMailID = Constants.NULL_INT;
        bool isAlreadyReported = false;
        int ReportAbuseCount = 0;

        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
                // jQuery makes the profile unclickable so this is not needed
                MemberMiniProfile.Transparency = false;

                // Retrieve the TargetMemberID and MemberMailID for later processing, using ReportAbuse static methods
                TargetMemberID = ReportAbuse.GetMemberID();
                MemberMailID = ReportAbuse.GetMemberMailID();

                // Set isAlreadyReported equal to isHotListed value for new hot list type to determine if this member has already reported the target member
                isAlreadyReported = g.List.IsHotListed(List.ValueObjects.HotListType.ReportAbuse,
                    List.ValueObjects.HotListDirection.OnYourList,
                    g.Brand.Site.Community.CommunityID,
                    TargetMemberID);

                // this member has already attempted to report the target member
                if (isAlreadyReported)
                {
                    g.Transfer("/Applications/MemberServices/ReportTooMany.aspx?MemberID=" + TargetMemberID);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                // Set breadcrumb trail accordingly
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_REPORT_A_MEMBER", this), String.Empty);
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_REPORT_A_MEMBER", this), String.Empty);

                // Set the two messages for the validators equal to resources
                // ValidReason.ErrorMessage = g.GetResource("VALIDATION_TEXT", this);
                //ValidComplaint.ErrorMessage = g.GetResource("TEXTBOX_VALIDATION_TEXT", this);

                // Load the miniprofile with the TargetMemberID
                ReportAbuse.LoadMiniProfile(MemberMiniProfile, TargetMemberID);

                // Setup the list of reasons for the user to select
                BindReportReason();

                //default abuse option
                if (!Page.IsPostBack)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_REPORT_ABUSE]))
                    {
                        int val = 0;
                        int.TryParse(Request.QueryString[WebConstants.URL_PARAMETER_REPORT_ABUSE], out val);
                        if (val > 0)
                        {
                            hidSelectedReason.Value = val.ToString();
                            hidReasonTxtVisible.Value = IsReportAbuseOptionCommentEnabled(val).ToString().ToLower();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        /// <summary>
        /// Bind the reasons for the user to select for why they are reporting the target member.
        /// </summary>
        private void BindReportReason()
        {
            DataTable dt = Framework.Util.Option.GetOptions("ReportAbuseReason", g);

            rptReportReason.DataSource = dt;

            rptReportReason.DataBind();
        }

        /// <summary>
        /// Adds a single item to the radiobuttonlist rblReportReason using Value to retrieve a Resource for the text
        /// </summary>
        /// <param name="Value">Both the resource name and Value to be used as the item being added.</param>
        //private void AddItem(string Value)
        //{
        //    rptReportReason.Items.Add(new ListItem(g.GetResource(Value, this), Value));
        //}

        /// <summary>
        /// Process the report of the member
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSend_Click(object sender, System.EventArgs e)
        {
            try
            {
                // Add the target member to the member's blocked list if they marked the checkbox
                int val = Conversion.CInt(hidSelectedReason.Value, -1);
                if (val < 0)
                {
                    g.Notification.AddError("VALIDATION_TEXT_NO_REASON");
                    return;
                }

                if (!IsComplaintCommentValid(val))
                    return;

                if (chkBlock.Checked)
                {
                    ListSA.Instance.AddListMember(
                        HotListCategory.IgnoreList
                        , g.Brand.Site.Community.CommunityID
                        , g.Brand.Site.SiteID
                        , g.Member.MemberID
                        , TargetMemberID
                        , string.Empty
                        , Constants.NULL_INT
                        , false
                        , false);
                }

                // Add the target member to the member's ReportAbuse list
                List.ValueObjects.ListSaveResult reportResult = ListSA.Instance.ReportAbuser(
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID,
                    g.Member.MemberID,
                    TargetMemberID,
                    Matchnet.Conversion.CInt(hidSelectedReason.Value));

                // Transfer the user to the confirmation page if success				
                if (reportResult.Status == ListActionStatus.Success)
                {
                    Member.ServiceAdapters.Member targetMember = MemberSA.Instance.GetMember(TargetMemberID, MemberLoadFlags.None);
                    ReportAbuseCount = targetMember.GetAttributeInt(g.Brand, "ReportAbuseCount", 0);
                    ReportAbuseCount++;
                    targetMember.SetAttributeInt(g.Brand, "ReportAbuseCount", ReportAbuseCount);
                    MemberSA.Instance.SaveMember(targetMember);

                    // Send an email of the content to our system for CS to process.  
                    // The delimiter is empty because email picks up line breaks.
                    ComposeAndSendEmailReport(ComposeMessage(String.Empty));

                    // Add the content to the admin notes of the target member.
                    // The delimiter is a break because this is displayed in HTML in the admin notes section
                    // AppendAdminNote(ComposeMessage("<br />"));

                    g.Transfer("/Applications/MemberServices/ReportConfirmation.aspx?MemberID=" + TargetMemberID);
                }
                else
                {
                    g.Transfer("/Applications/MemberServices/ReportTooMany.aspx?MemberID=" + TargetMemberID);
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        /// <summary>
        /// Build the message that the CS reps will see to determine if action should be taken.
        /// </summary>
        /// <param name="EndLineDelimiter">The string added at the end of each line that is written.  This was created
        ///		so that line breaks can be used in the admin notes and line returns can be used in the email.</param>
        /// <returns>A string of the content regarding this report.</returns>
        private string ComposeMessage(string EndLineDelimiter)
        {
            Matchnet.Member.ServiceAdapters.Member targetMember = MemberSA.Instance.GetMember(TargetMemberID, MemberLoadFlags.None);

            System.IO.StringWriter Message = new System.IO.StringWriter();
            Message.WriteLine("MemberID: " + g.Member.MemberID + " (" + g.Member.EmailAddress + ")" + EndLineDelimiter);
            Message.WriteLine("Reported MemberID: " + TargetMemberID + EndLineDelimiter);
            Message.WriteLine("Site: " + g.Brand.Site.Name + EndLineDelimiter);
            Message.WriteLine("Time: " + DateTime.Now.ToString() + EndLineDelimiter);
            Message.WriteLine("Times Reported: " + ReportAbuseCount.ToString() + EndLineDelimiter);
            Message.WriteLine("Reason: " + Framework.Util.Option.GetContent("ReportAbuseReason", Conversion.CInt(hidSelectedReason.Value), g) + EndLineDelimiter);
            Message.WriteLine("Environment: " + GetEnvironmentType());

            // If the user filled in information about the report, include it in the report.
            if (txtComplaint.Text.Trim() != String.Empty)
            {
                Message.WriteLine(EndLineDelimiter);
                Message.WriteLine("Explanation: " + txtComplaint.Text.Trim().Replace("\n", EndLineDelimiter) + EndLineDelimiter);
            }

            // If the MemberMailID is present, include information about the email in question with the report
            if (MemberMailID != Constants.NULL_INT)
            {
                EmailMessage Email = EmailMessageSA.Instance.RetrieveMessage(
                    g.Member.MemberID,
                    g.Brand.Site.Community.CommunityID,
                    MemberMailID);

                Message.WriteLine(EndLineDelimiter);
                Message.WriteLine("Email Subject: " + Email.Subject + EndLineDelimiter);
                Message.WriteLine("Email Contents: " + Email.Content + EndLineDelimiter);
            }

            return Message.ToString();
        }

        private string GetEnvironmentType()
        {
            string EnvironmentType = "Production";
            if (g.IsDevMode)
            {
                EnvironmentType = "Dev";
            }

            if (Request.RawUrl.IndexOf("stage") >= 0 || Request.RawUrl.IndexOf("stg") >= 0)
            {
                EnvironmentType = "Stage";
            }

            return EnvironmentType;
        }

        private void ComposeAndSendEmailReport(string Message)
        {
            // this is commented out until externalmail svc is deployed to production so that stage deployments wont get ruined.
            Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendReportAbuseEmail(g.Brand.BrandID, Message);
        }

        /*
        /// <summary>
        /// Simple helper method to add the report contents to both member's admin notes.
        /// </summary>
        /// <param name="Message">The content to be added to the notes.</param>
        private void AppendAdminNote(string Message)
        {
            // Add the note to the target member
            Content.ServiceAdapters.AdminSA.Instance.UpdateAdminNote(
                Message
                , TargetMemberID
                , g.Member.MemberID
                , g.Brand.Site.Community.CommunityID
                , WebConstants.ADMIN_NOTE_MAX_LENGTH);

            // Add the note to the reporting member
            Content.ServiceAdapters.AdminSA.Instance.UpdateAdminNote(
                Message
                , g.Member.MemberID
                , g.Member.MemberID
                , g.Brand.Site.Community.CommunityID
                , WebConstants.ADMIN_NOTE_MAX_LENGTH);
        }
        */

        private bool IsComplaintCommentValid(int val)
        {
            if (IsReportAbuseOptionCommentEnabled(val))
            {
                if (txtComplaint.Text.Trim() != String.Empty && txtComplaint.Text.Trim().Length <= 700)
                {
                    return true;
                }
                else
                {
                    g.Notification.AddError("TXT_REPORT_MEMBER_COMMENT");
                    return false;
                }
            }
            else
            {
                // no need to validate
                return true;
            }
        }

        private bool IsReportAbuseOptionCommentEnabled(int val)
        {
            if (val == 3 || val == 4 || val == 6 || val == 8)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void rptReportReason_DataBind(object sender, RepeaterItemEventArgs e)
        {
            DataRowView rw = e.Item.DataItem as DataRowView;
            int value = Conversion.CInt(rw["Value"].ToString());
            Literal litReason = (Literal)e.Item.FindControl("litReportReason");

            string onClick = "";
            if (IsReportAbuseOptionCommentEnabled(value))
                onClick = "SetSelectedValue(" + value.ToString() + ", true)";
            else
                onClick = "SetSelectedValue(" + value.ToString() + ", false)";


            litReason.Text = onClick;

        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
        }
        #endregion
    }
}