﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.EmailNotifier.ValueObjects;
using Matchnet.EmailNotifier.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Search.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.Adapter;
using Spark.Common.DiscountService;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.Subscription;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.ExternalMail.ServiceAdapters;

namespace Matchnet.Web.Applications.MemberServices
{
    public class GiftNotificationSender
    {
        public const string SCHEDULE_NAME = "BOGO";

        public static GiftNotificationStatus SendNotification(Gift gift, string emailAddress, Matchnet.Member.ServiceAdapters.Member member, Brand brand)
        {
            if(gift.BeginValidDate >= DateTime.Now)
            {
                SendScheduledNotification(gift, emailAddress, member, brand);
                return GiftNotificationStatus.Pending;
            }
            else
            {
                SendRegularNotification(gift, emailAddress, member, brand);
                return GiftNotificationStatus.Sent;
            }
        }

        private static void SendRegularNotification(Gift gift, string emailAddress, Matchnet.Member.ServiceAdapters.Member member, Brand brand)
        {
            GiftResponse response = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().AddGiftNotification(gift.GiftID,
                                                                GiftNotificationType.SentByCustomer,
                                                                emailAddress, GiftNotificationStatus.Sent,
                                                                Constants.NULL_INT);
            if(response.InternalResponse.responsecode =="0")
            {
                string encryptedPromo = getEncryptedBogoPromo(brand);

                ExternalMailSA.Instance.SendBOGONotificationEmail(member.MemberID, brand.BrandID,
                                                                  emailAddress, encryptedPromo, gift.Code);
            }
            else
            {
                //notification wasn't sucessful, throw error
                throw new ApplicationException("Gift Notification not successfully sent.");
            }
        }

        private static void SendScheduledNotification(Gift gift, string emailAddress, Matchnet.Member.ServiceAdapters.Member member, Brand brand)
        {

            GiftResponse response = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().AddGiftNotification(gift.GiftID,
                                                                            GiftNotificationType.SentByCustomer,
                                                                            emailAddress, GiftNotificationStatus.Pending,
                                                                            Constants.NULL_INT);

            //First, see if there's another pending notification for this member
            ScheduledEvent scheduledBogoNotification = WCFEmailNotifierSA.Instance.GetScheduledEventByMember(brand.BrandID,
                                                                                                     member.MemberID,
                                                                                                     SCHEDULE_NAME);

            if (scheduledBogoNotification != null)
            {
                //if so, delete it
                WCFEmailNotifierSA.Instance.DeleteScheduledEvent(scheduledBogoNotification.ScheduleGUID);
            }
            
            if(response.InternalResponse.responsecode =="0")
            {
                int giftNotificationID = response.ReturnID;
                string encryptedPromo = getEncryptedBogoPromo(brand);
            
                ScheduledEvent notificationEvent = new ScheduledEvent();
                notificationEvent.EmailAddress = emailAddress;
                notificationEvent.ScheduleName = SCHEDULE_NAME;
                notificationEvent.GroupID = brand.BrandID;
                notificationEvent.NextAttemptDate = gift.BeginValidDate;
                notificationEvent.MemberID = member.MemberID;
                notificationEvent.ScheduleDetails = new ScheduleDetail();
                notificationEvent.ScheduleDetails.Add("GiftCode", gift.Code);
                notificationEvent.ScheduleDetails.Add("GiftPromo", encryptedPromo);
                notificationEvent.ScheduleDetails.Add("GiftNotificationID", giftNotificationID.ToString());
                WCFEmailNotifierSA.Instance.SaveScheduledEvent(notificationEvent);
            }
            else
            {
                //notification wasn't sucessful, throw error
                throw new ApplicationException("Gift Notification not successfully sent.");
            }
        }

        private static string getEncryptedBogoPromo(Brand brand)
        {
            int bogoPromoID = Convert.ToInt32(RuntimeSettings.GetSetting("BOGO_PROMOID", brand.Site.Community.CommunityID,
                                                                brand.Site.SiteID, brand.BrandID));
            return new SubscribeCommon().EncryptAndEncodeNumber(bogoPromoID);
        }
    }
}