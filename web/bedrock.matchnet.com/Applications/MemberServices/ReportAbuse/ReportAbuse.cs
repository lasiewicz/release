using System;
using System.Web;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.MemberServices
{
	/// <summary>
	/// Summary description for ReportAbuse.
	/// </summary>
	public class ReportAbuse
	{
		/// <summary>
		/// Retrieve the MemberID from the querystring.  If not present or not a number, return Constants.NULL_INT
		/// </summary>
		/// <returns>The MemberID or Constants.NULL_INT</returns>
		public static int GetMemberID()
		{
			if(HttpContext.Current.Request.QueryString["MemberID"] != null)
			{
				return Matchnet.Conversion.CInt(HttpContext.Current.Request.QueryString["MemberID"]);
			}
			else
			{
				return Constants.NULL_INT;
			}
		}

		/// <summary>
		/// Retrieve the MemberMailID from the querystring.  If not present or not a number, return Constants.NULL_INT
		/// </summary>
		/// <returns>The MemberMailID or Constants.NULL_INT</returns>
		public static int GetMemberMailID()
		{
			if(HttpContext.Current.Request.QueryString["MemberMailID"] != null)
			{
				return Matchnet.Conversion.CInt(HttpContext.Current.Request.QueryString["MemberMailID"]);
			}
			else
			{
				return Constants.NULL_INT;
			}
		}

		/// <summary>
		/// Use the preset MemberID to load a Member into the Miniprofile.  Set transparency depending on the existance of this member
		/// </summary>
		public static void LoadMiniProfile(IMiniProfile MemberMiniProfile, int MemberID)
		{
			//MemberMiniProfile.Visible = false;

			if (MemberID > 0)
			{
				MemberMiniProfile.Member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);

				if(MemberMiniProfile.Member != null)
				{
					//MemberMiniProfile.Visible = true;
				}
			}
		}

	}
}
