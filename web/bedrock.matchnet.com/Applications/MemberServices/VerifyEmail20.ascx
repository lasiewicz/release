﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="VerifyEmail20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.VerifyEmail20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<script type="text/javascript">
<!--
function postVerifyEmail(verifyCode)
{
	document.forms[0].action = '/Applications/MemberServices/VerifyEmail.aspx?a=Verify';
	document.forms[0].submit();
}
function editEmail()
{document.forms[0].action = '/Applications/MemberServices/VerifyEmail.aspx';
    document.getElementById("dvEdit").style.display="block";
    document.getElementById("div2").style.display="none";
}
function stopEditEmail()
{   //debugger;
    document.forms[0].action = '/Applications/MemberServices/VerifyEmail.aspx';
    document.getElementById("dvEdit").style.display="none";   
    document.getElementById("div2").style.display="block";
   var allElements = document.getElementsByTagName("input");

    for (var i=0; i < allElements.length; i++) 
    { if(allElements[i].id.indexOf("txtNewEmail") > 0)
        allElements[i].value="";
       }

}
//-->
</script>
<input type="hidden" name="a" value="18550" />

	<asp:Label ID="lblError" Runat="server" CssClass="error" >
		<mn:image ID="Image1" runat="server" Filename="page-error.gif" />
		<asp:Literal ID="litErrorMessage" Runat="server" />
	</asp:Label>

    <mn:Title runat="server" id="ttlVerifyEmail" ResourceConstant="NAV_SUB_VERIFY_EMAIL"/>

<div id="verify-email">
    <mn:txt runat="server" id="xttInstruction1" ResourceConstant="TXT_INSTRUCTION_1" />
        <asp:Panel runat="server" ID="PanelEdit" DefaultButton="btnSave">    
    <div id="current-email">
        <span class="title"><mn:txt runat="server" id="txtCurrentEmail" ResourceConstant="TXT_CURRENT_EMAIL_ADDRESS" /></span>
        <span class="email"><mn:txt runat="server" id="txtEmailAddress" ResourceConstant="TXT_EMAIL_ADDRESS" />	<a href="#" id="lnkEdit" onclick="javascript:editEmail();"><mn:txt runat="server" id="txt3" ResourceConstant="LNK_EDIT" /></a></span>

        <div id="dvEdit" style="display:none">
            <span class="title"><mn:txt runat="server" id="txt4" ResourceConstant="TXT_NEW_EMAIL_ADDRESS" /></span>
             <span class="email">
                <asp:TextBox runat="server" ID="txtNewEmail" name="NewEmail"></asp:TextBox>
                <asp:LinkButton Runat="server" ID="btnSave" class="link-secondary" ><mn:txt runat="server" id="txt6" ResourceConstant="BTN_SAVE" /></asp:LinkButton>
                &nbsp;<a href="#" id="a1" onclick="javascript:stopEditEmail();"/><mn:txt runat="server" id="txt5" ResourceConstant="LNK_CANCEL" /></a> 
            </span>
 
        </div>
        
    </div>
               <asp:RegularExpressionValidator id="RegularExpressionValidatorNewEmailAddress" runat="server" 
                ControlToValidate = "txtNewEmail"
                EnableClientScript = "true"
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                <mn:txt runat="server" id="txtNewEmailAddressIsNotValid" name="NewEmailAddressIsNotValid"  cssclass="error" resourceconstant="ERR_NEW_EMAIL_ADDRESS_IS_NOT_VALID" />
            </asp:RegularExpressionValidator>
    </asp:Panel>
    
	    <div id="div2">
	        <div class="buttons">
	            <asp:LinkButton Runat="server" ID="lbutClickHere" class="link-secondary" ><mn:txt runat="server" id="txtClickHere" ResourceConstant="BTN_RESEND" /></asp:LinkButton>
            </div>

        	
	        <mn:txt runat="server" id="Txt2" ResourceConstant="TXT_INSTRUCTION_2" />
	        <p><strong><mn:txt runat="server" id="Txt1" ResourceConstant="TXT_VERIFICATION_CODE" /></strong>&nbsp;<input type="text" name="VerifyCode" size="42" /> 
	        <a href="#" class="link-primary" onclick="javascript:return postVerifyEmail();">
		        <mn:txt runat="server" id="txtSubmit" ResourceConstant="BTN_ENTER" /></a></p>
	        <p><mn:txt runat="server" id="txt7" ResourceConstant="TXT_HAVING_PROBLEM" /></p>
	    </div>	
    </div>
