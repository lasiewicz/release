﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Spark.Common.Adapter;
using Spark.Common.DiscountService;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class ManageGifts : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Gift gift = null;

            try
            {
                List<Gift> gifts = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().GetGiftsForPurchaser(g.Member.MemberID);

                if(gifts != null && gifts.Count > 0)
                {
                    gift = gifts[0];
                    int pendingNotifications = 0;
                    int sentNotifications = 0;
                    
                    if(gift.Notifications != null && gift.Notifications.Count > 0)
                    {
                        sentNotifications = (from GiftNotification sent in gift.Notifications
                                             where sent.NotificationStatus == GiftNotificationStatus.Sent
                                             select sent).Count();

                        pendingNotifications = (from GiftNotification sent in gift.Notifications
                                             where sent.NotificationStatus == GiftNotificationStatus.Pending
                                             select sent).Count();
                    }

                    if(gift.Status == GiftStatus.Redeemed && gift.GiftRedemption != null)
                    {
                        string redeemedText = g.GetResource("TXT_REDEEMED", this);

                        Matchnet.Member.ServiceAdapters.Member member = Member.ServiceAdapters.MemberSA.Instance.GetMember(gift.GiftRedemption.CustomerID, MemberLoadFlags.None);

                        lblRedeemed.Text = string.Format(redeemedText, member.EmailAddress,
                                                           gift.InsertDate);
                        
                        pnlRedeemed.Visible = true;
                    }
                    else if(pendingNotifications >0)
                    {
                        GiftNotification pendingNotification = (from GiftNotification sent in gift.Notifications
                                                                where sent.NotificationStatus == GiftNotificationStatus.Pending
                                                                select sent).First();

                        ShowPendingNotification(pendingNotification.SentTo, pendingNotification.CreateDate);

                    }
                    else if (sentNotifications > 0)
                    {
                        GiftNotification sentNotification = (from GiftNotification sent in gift.Notifications
                                                                where sent.NotificationStatus == GiftNotificationStatus.Sent
                                                                select sent).First();

                        ShowSentNotificaiton(sentNotification.SentTo, sentNotification.UpdateDate);
                        
                    }
                    else
                    {
                        pnlSend.Visible = true;
                        lblInstructions2.Text = string.Format(g.GetResource("TXT_INSTRUCTIONS2", this), gift.ExpirationDate.ToShortDateString());
                    }
                }
                else
                {
                    pnlNoGift.Visible = true;
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            string emailAddress = txtEmailAddress.Text;
            bool emailAddressIsCurrentSub = false;

            try
            {
                int existingMemberID = Member.ServiceAdapters.MemberSA.Instance.GetMemberIDByEmail(emailAddress);
                if (existingMemberID != Constants.NULL_INT)
                {
                    Matchnet.Member.ServiceAdapters.Member member = Member.ServiceAdapters.MemberSA.Instance.GetMember(existingMemberID, MemberLoadFlags.None);
                    if (member.IsPayingMember(g.Brand.Site.SiteID))
                    {
                        //the selected email address is alreadt a sub.
                        emailAddressIsCurrentSub = true;
                    }
                }
                if (emailAddressIsCurrentSub)
                {
                    pnlCantBeCurrentSub.Visible = true;
                }
                else
                {
                    Gift gift = null;
                    List<Gift> gifts = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().GetGiftsForPurchaser(g.Member.MemberID);

                    if (gifts != null)
                    {
                        gift = gifts[0];

                        GiftNotificationStatus notificationStatus = GiftNotificationSender.SendNotification(gift, emailAddress, g.Member, g.Brand);

                        if (notificationStatus == GiftNotificationStatus.Pending)
                        {
                            ShowPendingNotification(emailAddress, gift.BeginValidDate);
                        }
                        else if (notificationStatus == GiftNotificationStatus.Sent)
                        {
                            ShowSentNotificaiton(emailAddress, DateTime.Now);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        private void ShowPendingNotification(string emailAddress, DateTime sendDate)
        {
            pnlWillBeSent.Visible = true;
            string willBeSentText = g.GetResource("TXT_WILL_BE_SENT", this);
            lblWillBeSent.Text = string.Format(willBeSentText, emailAddress,
                                               sendDate.ToShortDateString());

            pnlRedeemed.Visible = false;
            pnlWasSent.Visible = false;
            pnlSend.Visible = false;
            pnlCantBeCurrentSub.Visible = false;
        }

        private void ShowSentNotificaiton(string emailAddress, DateTime sendDate)
        {
            pnlWasSent.Visible = true;
            string wasSentText = g.GetResource("TXT_WAS_SENT", this);
            lblWasSent.Text = string.Format(wasSentText, emailAddress,
                                               sendDate.ToShortDateString());
            pnlRedeemed.Visible = false;
            pnlWillBeSent.Visible = false;
            pnlSend.Visible = false;
            pnlCantBeCurrentSub.Visible = false;
            
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}