﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="VerifyEmailConfirmation20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.VerifyEmailConfirmation20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<h1><mn:Txt ID="Txt2" runat="server" ResourceConstant="TXT_CONFIRMATION" /></h1>
<div id="verify-email-confirmation">
     <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_GETTING_STARTED_LIST" expandImageTokens="True" />
</div>