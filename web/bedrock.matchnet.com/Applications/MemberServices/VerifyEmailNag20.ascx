﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="VerifyEmailNag20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.VerifyEmailNag20" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="page-container">
<script type="text/javascript">
<!--
function postVerifyEmailNag()
{
	document.forms[0].action = '/Applications/MemberServices/VerifyEmail.aspx?a=SendEmail';
	document.forms[0].submit();
	return (true);
}
//-->
</script>

<mn:Title runat="server" id="ttlHeader" ResourceConstant="TXT_VERIFY_YOUR_EMAIL_ADDRESS"/>

<div class="editorial">
    <p><mn:txt runat="server" id="txtVerifySo" ResourceConstant="TXT_VERIFY_EMAIL_SO_YOU_DONT" /></p>
    <hr />
    <p><mn:txt runat="server" id="txtWeNoticed" resourceConstant="TXT_VERIFY_EMAIL_WE_NOTICED" /></p>
    <p><mn:txt runat="server" id="txtHeresHowToVerify" resourceConstant="TXT_HERES_HOW_TO_VERIFY_YOUR_EMAIL_ADDRESS" /></p>
    <ol class="editorial">
        <mn:txt runat="server" id="txtHeresHowToVerifyList" resourceConstant="TXT_HERES_HOW_LIST" />
    </ol>
    <p><mn2:FrameworkHtmlButton runat="server" id="btnSubmit" ResourceConstant="BTN_SEND_ME_A_VERIFICATION_EMAIL" class="btn primary" onclick="javascript:return postVerifyEmailNag();" /></p>
</div>

</div>