﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.HeaderNavigation20;
using Matchnet.Content.ServiceAdapters.Links;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class VerifyEmailConfirmation20 : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string subURL = LinkFactory.Instance.GetLink(HeaderLinks.Subscribe, g.Brand, Request.Url.ToString());

            g.AddExpansionToken("SUB_URL_PRT_140", subURL);
        }


        private void Page_Init(object sender, System.EventArgs e)
        {


            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
            {

                Matchnet.Web.Analytics.Omniture omniture = (Matchnet.Web.Analytics.Omniture)Page.LoadControl("/Analytics/Omniture.ascx");

                PlaceHolder placeHolderAnalytics = new PlaceHolder();

                placeHolderAnalytics.Controls.Add(omniture);

                this.Controls.Add(placeHolderAnalytics);

                _g.AnalyticsOmniture = omniture;

                if (MemberProfile.EmailVerifyHelper.IsMemberVerified(_g.Member, _g.Brand))
                {

                    omniture.Evar15 = WebConstants.OMNITURE_EVAR15_VERIFIED;
                    omniture.Prop4 = WebConstants.OMNITURE_SPROP4_VERIFIED;

                }


            }


        }
    }
}