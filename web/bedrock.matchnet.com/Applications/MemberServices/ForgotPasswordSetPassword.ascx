﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgotPasswordSetPassword.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.ForgotPasswordSetPassword" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div id="page-container">
<asp:Panel ID="pnlReset" runat="server">
 <style>
        .pwdError 
        {
            display: none;
            color: #ff0000;
            font-size: 12px;
            
        }
    </style>

    <script type="text/javascript">
        $j(document).ready(function () {
            $j('input[type="submit"]').unbind('click').click(function (event) {

                var isValid = false;
                var thePassword = $j('input[type="password"]').val();

                if (thePassword != "") {
                    //password has to be between 8 to 16 charaters long 
                    if (thePassword.length >= 8 && thePassword.length <= 16) {

                        if (/\d/.test(thePassword) && /[אבגדהוזחטיכלמנסעפצקרשתץףןםa-zA-Z\_]/.test(thePassword)) {
                            isValid = true;
                        }
                    }
                }

                //[אבגדהוזחטיכלמנסעפצקרשתץףןםa-zA-Z0-9\_]

                if (isValid)
                    $j('#Form1').submit();
                else {

                    $j("#pwdError").show();

                    return false;
                }

            });

            function isNumeric(n) {
                return !isNaN(parseFloat(n)) && isFinite(n);
            }


        });
    </script>

<h1><mn:Txt runat="server" id="txtResetPassword" ResourceConstant="RESET_PASSWORD_HEADER" /></h1>
<span id="pwdError" class="pwdError"><mn:Txt runat="server" id="txt5" ResourceConstant="TXT_PASSWORD_ERROR" /></span>
<%--<h3 class="tag-line"><mn:Txt runat="server" id="txt1" ResourceConstant="RESET_PASSWORD_DESC" /></h3>--%>
<div class="rbox-wrapper-member" style="margin-top:10px;">
<fieldset id="loginBox" class="rbox-style-high login-box member">
<dl class="dl-form reset-password clearfix" style="padding:20px">
    <dt><strong><mn:Txt runat="server" id="txtPasswordLabel" ResourceConstant="PASSWORD_LABEL" /></strong></dt>
    <dd><asp:TextBox runat="server" ID="tbxPassword" TextMode="Password"  ></asp:TextBox> <span class="instruction"><mn:Txt runat="server" ID="txtPasswordInstruction" ResourceConstant="TXT_PASSWORD_INSTRUCTION" /></span></dd>
    <dt><strong><mn:Txt runat="server" id="txtConfirmPwdlabel" ResourceConstant="CONFIRM_PASSWORD_LABEL" /></strong></dt>
    <dd><asp:TextBox runat="server" ID="tbxConfirmPassword" TextMode="Password"  ></asp:TextBox></dd>
	<dt>&nbsp;</dt>
	<dd class="cta" style="padding-top:10px;">
         <mn1:frameworkbutton runat="server" id="btnChangePassword" ResourceConstant="BTN_CHANGE_PASSWORD"></mn1:frameworkbutton>
    </dd>
</dl>
</fieldset>
</div>
</asp:Panel>
<asp:Panel  ID="pnlResetMsg" runat="server" visible="false" CssClass="dl-form">
	<h1><mn:Txt runat="server" id="txt4" ResourceConstant="FORGOT_PASSWORD_LINK_EXPIRED" /></h1>
    <div class="rbox-wrapper-member" style="margin-top:10px;">
        <fieldset id="Fieldset1" class="rbox-style-high login-box member">
            <dl class="dl-form" style="padding:20px 20px 20px 20px">
	                 <mn:Txt runat="server" id="txt2" ResourceConstant="FORGOT_PASSWORD_EXPIRED_DESC" />
                      <a href="/Applications/Memberservices/ForgotPassword.aspx"><mn:Txt runat="server" id="txt3" ResourceConstant="FORGOT_PASSWORD_CLICK_TO_TRY_AGAIN" /></a>.
            </dl>
        </fieldset>
    </div>
</asp:Panel>


</div>
