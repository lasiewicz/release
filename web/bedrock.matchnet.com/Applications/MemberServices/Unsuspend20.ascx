﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Unsuspend20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.Unsuspend20" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="rightNew" class="page-container">
	<mn:Title runat="server" id="ttlUnsuspendMembership" ResourceConstant="TXT_UNSUSPEND_MEMBERSHIP"/>
	<p><mn:txt id="txtUnsuspend" runat="server" Resourceconstant="UNSUSPEND_INTRO" /></p>
	<p><mn2:FrameworkButton runat="server" CssClass="activityButton" id="btnUnsuspend" ResourceConstant="BTN_UNSUSPEND" /></p>
	<%--<p><asp:Button id="btnUnsuspend" runat="server" Text="[Unsuspend]" CssClass="activityButton"></asp:Button></p>--%>
</div> <!-- end Right Div area -->
