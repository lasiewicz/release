﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Exceptions
{
    [Serializable]
    public class ImageTypeNotSupportedException: ApplicationException 
    {
        public ImageTypeNotSupportedException(): base("Image type not supported for upload. Must be JPG, GIF, or BMP.")
        {
            
        }

        public ImageTypeNotSupportedException(string message): base(message)
        {

        }

         public ImageTypeNotSupportedException(string message, Exception innerException): base(message, innerException)
         {
             
         }
    }
}