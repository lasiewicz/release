using System;
using Matchnet.Member.ValueObjects;

namespace Matchnet.Web.Framework.Globalization
{
	/// <summary>
	/// Summary description for MemberRegisterResultResources.
	/// </summary>
	public sealed class RegisterStatusTypeResources
	{
		/// <summary>
		///		Prevent Instantiation
		/// </summary>
		private RegisterStatusTypeResources() {}

		public static string GetResourceConstant(RegisterStatusType statusType)
		{
			string errorResourceConstant;
/*
		/// <summary>
		/// Indicates that the user's password exceeded the maximum length.
		/// </summary>
		PasswordLengthExceeded = 401005,
		
		/// <summary>
		/// Indicates that the user's e-mail address is not allowed because the domain has been blocked.
		/// </summary>
		EmailAddressNotAllowed = 519238,
		
		/// <summary>
		/// Indicates that the user is already registered in the system.
		/// </summary>
		AlreadyRegistered = 401002,
		
		/// <summary>
		/// Indicates that the user's e-mail address has been blocked.
		/// </summary>
		EmailAddressBocked = 519238,
		
		/// <summary>
		/// Indicates that the user's e-mail address is invalid.
		/// </summary>
		InvalidEmail = 519385
 */
			switch (statusType)
			{
				case RegisterStatusType.AlreadyRegistered:
					errorResourceConstant = "ALREADY_REGISTERED";
					break;

				case RegisterStatusType.EmailAddressBlocked:
					errorResourceConstant = "EMAIL_ADDRESS_ON_DNE";
					break;

				case RegisterStatusType.EmailAddressNotAllowed:
					errorResourceConstant = "EMAIL_ADDRESS_NOT_ALLOWED";
					break;

				case RegisterStatusType.InvalidEmail:
					errorResourceConstant = "INVALIDEMAIL";
					break;

                case RegisterStatusType.IPBlocked:
                    errorResourceConstant = "IPADDRESS_BLOCKED";
                    break;

				case RegisterStatusType.PasswordLengthExceeded:
					errorResourceConstant = "";
					break;

				default:
					throw new ArgumentOutOfRangeException("statusType", "Unknown RegisterStatusType value: " + statusType);
			}

			return errorResourceConstant;
		}
	}
}
