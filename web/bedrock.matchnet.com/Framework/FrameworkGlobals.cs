#region System References
using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Managers;
using Microsoft.Security.Application;
using System.Collections.Specialized;
#endregion

#region Matchnet Web App References

using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Member.ServiceAdapters.Interfaces;
#endregion


#region Matchnet Service References
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Web.Framework.Diagnostics;
using Matchnet.Web.Applications.PremiumServices;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.PremiumServices.ServiceAdapter;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using System.Text;
using Matchnet.Purchase;
using System.Data;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Security;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Matchnet.Web.Framework.Enumerations;
using Spark.Common.Chat;
using Matchnet.Web.Applications.Home;
using Matchnet.Web.Framework.Util;

#endregion

namespace Matchnet.Web.Framework
{
    /// <summary>
    /// Summary description for FrameworkGlobals.
    /// </summary>
    public class FrameworkGlobals
    {
        private static SettingsManager _settingsManager = new SettingsManager();

        /// <summary>
        /// Returns true if current request is on https (SSL) site. This is important for image paths among other things.
        /// It is important to note that the built in .Net property: HttpContext.Current.Request.IsSecureConnection will
        /// apparently not work in our environment. Garry (Spark Networks) says: it will not work. Our load balancer does SSL
        /// so the web servers never see real ssl requests.
        /// </summary>
        /// <returns></returns>
        public static bool IsSecureRequest()
        {
            const string SECURE_PORT = "443";
            if (HttpContext.Current == null)
            {
                //this is for when running through a non http client, like in unit tests
                return false;
            }
            else
            {
                return HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == SECURE_PORT;
            }
        }

        public static string GetShortcutIconURL()
        {
            Matchnet.Web.Framework.Image I = new Image();
            I.FileName = "Shortcut.ico";
            I.App = WebConstants.APP.Home;
            return I.ImageUrl;
        }

        public static void RedirectToSamePage(List<string> removeParamNames)
        {
            //Return a location change for PRG implementation to avoid caching posted page for "back button issue" resulting in expired page
            string redirectURL = HttpContext.Current.Request.RawUrl;
            if (removeParamNames != null && removeParamNames.Count > 0)
            {
                foreach (string s in removeParamNames)
                {
                    redirectURL = RemoveParamFromURL(redirectURL, s);
                }
            }
            redirectURL = FrameworkGlobals.AppendParamToURL(redirectURL, "pgRefresh", DateTime.Now.TimeOfDay.Ticks.ToString());
            HttpContext.Current.Response.Redirect(redirectURL, true);
        }

        public static void RedirectToErrorPageForIPBlock()
        {
            try
            {
                HttpContext.Current.Response.Redirect("/Applications/Home/Error.aspx?" + WebConstants.URL_PARAMETER_REDIRECT_FOR_IPBLOCK + "=1");
            }
            catch (System.Threading.ThreadAbortException ex)
            { ex = null; }
            catch (Exception ex)
            {
                string inner = "";
                string exc = "";

                if (ex.InnerException != null)
                    inner = ex.InnerException.ToString();
                exc = "Tripple failure on Redirect to Err Page \r\n";
                exc += ex.ToString() + "\r\n";
                exc += "Inner exception: \r\n" + inner;
                exc += "\r\n" + GetHttpInformation();

                System.Diagnostics.EventLog.WriteEntry("WWW", exc, System.Diagnostics.EventLogEntryType.Warning);
                HttpContext.Current.Response.Redirect("/Error.htm");
            }
        }

        public static string SerializeToString(object obj)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(obj.GetType());

                using (StringWriter writer = new StringWriter())
                {
                    serializer.Serialize(writer, obj);

                    return writer.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error serializing xml", ex);
            }
        }

        public static T SerializeFromString<T>(string xml)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));

                xml = xml.Replace(@"%0d%0a", "");
                xml = xml.Replace("\n", "");

                using (XmlReader reader = XmlReader.Create(new StringReader(xml)))
                {
                    return (T)serializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error deserializing xml", ex);
            }
        }

        public static string JsonSerializeToString(object obj)
        {
            try
            {
                Newtonsoft.Json.JsonSerializer serializer = new Newtonsoft.Json.JsonSerializer();

                serializer.TypeNameHandling = Newtonsoft.Json.TypeNameHandling.All;

                string stringPromo;

                using (System.IO.StringWriter writer = new System.IO.StringWriter())
                {
                    serializer.Serialize(writer, obj);

                    stringPromo = writer.ToString();
                }

                return stringPromo;
            }
            catch (Exception ex)
            {
                throw new Exception("Error serializing json", ex);
            }
        }

        public static T JsonDeserializeFromString<T>(string deserializedString, Type type)
        {
            try
            {
                var serializer = new Newtonsoft.Json.JsonSerializer();

                T obj;

                using (System.IO.TextReader reader = new StringReader(deserializedString))
                {
                    obj = (T)serializer.Deserialize(reader, type);
                }

                System.Diagnostics.Debug.WriteLine("Deserialized");

                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("Error deserializing json", ex);
            }
        }

        /// <summary>
        /// Returns the timestamp of the last modified date of the given file (such as a javascript file) in 
        /// MMddyyyyHHmmss format (e.g., 11142005201925 ). Caches the result for one hour.
        /// This way, we can replace the spark.js file (for example) with newer version and defeat browser caching 
        /// of old javascript file.
        /// </summary>
        /// <param name="path">The relative to site root path to the file in question (a Server.MapPath(path) determines 
        /// the absolute file system path)</param>
        public static string GetLastWriteTimeString(string path)
        {
            object lastModified = HttpContext.Current.Cache[path + "_Cache"];
            if (lastModified == null)
            {
                lastModified = System.IO.File.GetLastWriteTime(HttpContext.Current.Server.MapPath(path)).ToString("MMddyyyyHHmmss");
                HttpContext.Current.Cache.Insert(path + "_Cache", lastModified, null, DateTime.Now.AddMinutes(60), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.Normal, null);
            }
            return lastModified.ToString();
        }

        public static string GetWebAppPath(string requestPath)
        {
            // Remove page name
            int slashIndex = requestPath.LastIndexOf("/");
            if (slashIndex < requestPath.Length)
                requestPath = requestPath.Remove(slashIndex + 1, requestPath.Length - (slashIndex + 1));

            return requestPath;
        }

        public static string GetGenderString(int genderMask, ContextGlobal g, FrameworkControl resourceControl)
        {

            string genderStr = string.Empty;

            if (genderMask < 0)
                return genderStr;

            if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                return g.GetResource("MALE", resourceControl);

            if ((genderMask & ConstantsTemp.GENDERID_FEMALE) == ConstantsTemp.GENDERID_FEMALE)
                return g.GetResource("FEMALE", resourceControl);

            if ((genderMask & ConstantsTemp.GENDERID_MTF) == ConstantsTemp.GENDERID_MTF)
                return g.GetResource("MTF", resourceControl);

            if ((genderMask & ConstantsTemp.GENDERID_FTM) == ConstantsTemp.GENDERID_FTM)
                return g.GetResource("FTM", resourceControl);

            // Else
            return genderStr;
        }

        public static bool IsMaleGender(int memberID, Brand b)
        {
            if (memberID > 0)
            {
                IMemberDTO pMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                return IsMaleGender(pMember, b);
            }

            return true;
        }

        public static bool IsMaleGender(int memberID, ContextGlobal g)
        {
            if (memberID > 0)
            {
                IMemberDTO pMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                return IsMaleGender(pMember, g);
            }

            return true;
        }

        public static bool IsMaleGender(IMemberDTO pMember, ContextGlobal g)
        {
            if (pMember != null && g != null)
            {
                return IsMaleGender(pMember, g.Brand);
            }

            return true;
        }

        public static bool IsMaleGender(Member.ServiceAdapters.Member pMember, Brand b)
        {
            bool isMale = true;
            if (pMember != null && b != null)
            {
                int genderMask = pMember.GetAttributeInt(b, "gendermask");
                if (genderMask > 0)
                {
                    if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                        isMale = true;
                    else if ((genderMask & ConstantsTemp.GENDERID_FEMALE) == ConstantsTemp.GENDERID_FEMALE)
                        isMale = false;
                }
            }

            return isMale;
        }

        public static bool IsMaleGender(IMemberDTO pMember, Brand b)
        {
            bool isMale = true;
            if (pMember != null && b != null)
            {
                int genderMask = pMember.GetAttributeInt(b, "gendermask");
                if (genderMask > 0)
                {
                    if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                        isMale = true;
                    else if ((genderMask & ConstantsTemp.GENDERID_FEMALE) == ConstantsTemp.GENDERID_FEMALE)
                        isMale = false;
                }
            }

            return isMale;
        }

        public static bool IsSeekingMaleGender(IMember pMember, Brand b)
        {
            return IsSeekingMaleGender(pMember.GetIMemberDTO(), b);
        }

        public static bool IsSeekingMaleGender(IMemberDTO pMember, Brand b)
        {
            bool isSeekingMale = true;
            if (pMember != null && b != null)
            {
                int genderMask = pMember.GetAttributeInt(b, "gendermask");
                if (genderMask > 0)
                {
                    if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MALE) == ConstantsTemp.GENDERID_SEEKING_MALE)
                        isSeekingMale = true;
                    else if ((genderMask & ConstantsTemp.GENDERID_SEEKING_FEMALE) == ConstantsTemp.GENDERID_SEEKING_FEMALE)
                        isSeekingMale = false;
                }
            }

            return isSeekingMale;
        }

        private static ListItemCollection PopulateHeightListItemsInHebrew(ContextGlobal g)
        {
            string displayString = "{0}" + g.GetResource("CM");
            ListItemCollection items = new ListItemCollection();
            // Add the minimum height option.
            //items.Add(new ListItem(string.Format(displayString, "< 137"), Matchnet.Conversion.CInt(Constants.HEIGHT_MIN).ToString()));
            items.Add(new ListItem(string.Format(displayString, "< 137"), Convert.ToString(Convert.ToInt32(Constants.HEIGHT_MIN - 2))));

            for (int i = 137; i <= 244; i++)
            {
                items.Add(new ListItem(string.Format(displayString, i), i.ToString()));
            }
            //items.Add(new ListItem(string.Format(displayString, "> 244"), Matchnet.Conversion.CInt(Constants.HEIGHT_MAX).ToString()));
            items.Add(new ListItem(string.Format(displayString, "> 244"), Convert.ToString(Convert.ToInt32(Constants.HEIGHT_MAX + 2))));

            return items;
        }
        public static ListItemCollection CreateHeightListItems(ContextGlobal g, bool withAnyAttribute)
        {
            double height;
            int heightUS;
            int inches;
            int feet;
            string display = "";
            ListItem listItem;
            bool siteUsesUSHeight = false;

            if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USE_US_HEIGHT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
            {
                siteUsesUSHeight = true;
            }

            ListItemCollection items = new ListItemCollection();



            if (withAnyAttribute)
            {
                ListItem li = new ListItem(g.GetResource("ANY_"), Constants.NULL_INT.ToString());

                items.Add(li);
            }
            else
            {
                items.Add(new ListItem("", ""));
            }

            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew
                || g.Brand.Site.LanguageID == (int)Matchnet.Language.French)
            {
                return PopulateHeightListItemsInHebrew(g);
            }
            else
            {
                #region less than min height
                // Yep per Green.
                if (g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.BBW)
                {

                    height = Constants.HEIGHT_MIN - 2.54;
                    if (siteUsesUSHeight || g.Brand.Site.CultureInfo.LCID == Constants.LOCALE_US_ENGLISH)
                    {
                        heightUS = Convert.ToInt32(Constants.HEIGHT_MIN / 2.54);
                        inches = heightUS % 12;
                        feet = (heightUS - inches) / 12;

                        display = String.Format("< {0}\'{1}\"({2:F0} cm)", feet, inches, Constants.HEIGHT_MIN);
                    }
                    else
                    {
                        display = String.Format("< {0:F0} cm", Constants.HEIGHT_MIN.ToString());
                    }
                    listItem = new ListItem();
                    listItem.Text = display;
                    listItem.Value = Convert.ToString(Convert.ToInt32(height));
                    items.Add(listItem);
                }
                #endregion

                for (height = Constants.HEIGHT_MIN; height <= Constants.HEIGHT_MAX; height += 2.54)
                {
                    if (siteUsesUSHeight || g.Brand.Site.CultureInfo.LCID == Constants.LOCALE_US_ENGLISH)
                    {
                        heightUS = Convert.ToInt32(height / 2.54);
                        inches = heightUS % 12;
                        feet = (heightUS - inches) / 12;
                        display = String.Format("{0}\'{1}\"({2:F0} cm)", feet, inches, height);
                    }
                    else
                    {
                        display = String.Format("{0:F0} cm", height.ToString());
                    }
                    listItem = new ListItem();
                    listItem.Text = display;
                    listItem.Value = Convert.ToString(Convert.ToInt32(height));
                    items.Add(listItem);
                }

                #region more than max height
                // Yep per Green.
                if (g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.BBW)
                {
                    height = Constants.HEIGHT_MAX + 2.54;
                    if (siteUsesUSHeight || g.Brand.Site.CultureInfo.LCID == Constants.LOCALE_US_ENGLISH)
                    {
                        heightUS = Convert.ToInt32(Constants.HEIGHT_MAX / 2.54);
                        inches = heightUS % 12;
                        feet = (heightUS - inches) / 12;

                        display = String.Format("> {0}\'{1}\"({2:F0} cm)", feet, inches, Constants.HEIGHT_MAX);
                    }
                    else
                    {
                        display = String.Format("> {0:F0} cm", Constants.HEIGHT_MAX.ToString());
                    }
                    listItem = new ListItem();
                    listItem.Text = display;
                    listItem.Value = Convert.ToString(Convert.ToInt32(height));
                    items.Add(listItem);
                }
                #endregion
            }


            return items;
        }

        public static ListItemCollection CreateAgeRangeListItems(ContextGlobal g, bool withYearsOldText)
        {
            ListItemCollection items = new ListItemCollection();
            string yearsOldText = string.Empty;
            if (withYearsOldText)
            {
                yearsOldText = (g.GetResource("YEARS_OLD_TEXT"));
            }

            items.Add(new ListItem("18-24 " + yearsOldText, "0"));
            items.Add(new ListItem("25-34 " + yearsOldText, "1"));
            items.Add(new ListItem("35-44 " + yearsOldText, "2"));
            items.Add(new ListItem("45-54 " + yearsOldText, "3"));
            items.Add(new ListItem("55-99 " + yearsOldText, "4"));

            return items;
        }

        public static List<System.Web.UI.Control> GetAllControls(List<System.Web.UI.Control> controls, Type t, System.Web.UI.Control parent)  /* can be Page */
        {
            foreach (System.Web.UI.Control c in parent.Controls)
            {
                if (c.GetType() == t)
                    controls.Add(c);
                if (c.HasControls())
                    controls = GetAllControls(controls, t, c);
            }
            return controls;
        }

        public static string GetRegionResourceConstant(int regionMask)
        {
            switch (regionMask)
            {
                case 4:
                    return "REGIONMASK_4";
                case 8:
                    return "REGIONMASK_8";
                case 16:
                    return "REGIONMASK_16";
                case 32:
                    return "REGIONMASK_32";
                case 64:
                    return "REGIONMASK_64";
                case 128:
                    return "REGIONMASK_128";
                case 256:
                    return "REGIONMASK_256";
                case 512:
                    return "REGIONMASK_512";
                case 1024:
                case 1025:
                    return "REGIONMASK_1024";
                case 2048:
                    return "REGIONMASK_2048";
                case 4096:
                    return "REGIONMASK_4096";
                case 8192:
                    return "REGIONMASK_8192";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Isn't hard coding exciting? 
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public static bool IsMingleSite(int communityID)
        {
            int[] communityIDList = new int[] { 20, 21, 22, 23, 24, 25, 50 };

            foreach (int ID in communityIDList)
            {
                if (ID.Equals(communityID))
                {
                    return true;
                }
            }

            return false;
        }

        public static string RemovePathInfo(string requestPath)
        {
            string _pathInfo = HttpContext.Current.Request.PathInfo;
            if (_pathInfo.Length == 0)
                return requestPath;

            return requestPath.Substring(0, requestPath.Length - _pathInfo.Length);
        }

        public static void RedirectToErrorPage(bool SafeHtmlFile)
        {
            string rawURL = "";
            bool toolBarURL = false;

            try
            {

                try
                {
                    rawURL = HttpContext.Current.Request.RawUrl;
                    if (rawURL.ToLower().IndexOf("toolbarnotifications") > 0)
                        toolBarURL = true;
                }
                catch
                { }
                if (toolBarURL)
                    return;
                if (!Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE")))
                {
                    if (HttpContext.Current.Request.RawUrl.IndexOf("/Applications/Home/Error.aspx") < 0)
                    {
                        Exception ex = new Exception("Redirecting to error.aspx\r\n");

                        ContextExceptions _ContextExceptions = new ContextExceptions();
                        _ContextExceptions.LogException(ex, true);

                        HttpContext.Current.Response.Redirect("/Applications/Home/Error.aspx");



                    }
                    else if (SafeHtmlFile)
                    {
                        Exception ex = new Exception("Redirecting to error.htm\r\n");

                        ContextExceptions _ContextExceptions = new ContextExceptions();
                        _ContextExceptions.LogException(ex, true);

                        HttpContext.Current.Response.Redirect("/Error.htm");
                    }
                }
            }
            catch (System.Threading.ThreadAbortException ex)
            { ex = null; }
            catch (Exception ex)
            {
                string inner = "";
                string exc = "";

                if (ex.InnerException != null)
                    inner = ex.InnerException.ToString();
                exc = "Tripple failure on Redirect to Err Page \r\n";
                exc += ex.ToString() + "\r\n";
                exc += "Inner exception: \r\n" + inner;
                exc += "\r\n" + GetHttpInformation();
                if (!toolBarURL)
                {
                    System.Diagnostics.EventLog.WriteEntry("WWW", exc, System.Diagnostics.EventLogEntryType.Warning);
                    HttpContext.Current.Response.Redirect("/Error.htm");
                }
            }
        }

        public static void RedirectToErrorPageNoLog(bool SafeHtmlFile)
        {
            string rawURL = "";
            bool toolBarURL = false;

            try
            {
                try
                {
                    rawURL = HttpContext.Current.Request.RawUrl;
                    if (rawURL.ToLower().IndexOf("toolbarnotifications") > 0)
                        toolBarURL = true;
                }
                catch
                { }
                if (toolBarURL)
                    return;
                if (!Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE")))
                {
                    if (HttpContext.Current.Request.RawUrl.IndexOf("/Applications/Home/Error.aspx") < 0)
                    {
                        HttpContext.Current.Response.Redirect("/Applications/Home/Error.aspx");
                    }
                    else if (SafeHtmlFile)
                    {
                        HttpContext.Current.Response.Redirect("/Error.htm");
                    }
                }
            }
            catch (System.Threading.ThreadAbortException ex)
            { ex = null; }
            catch (Exception ex)
            {
                string inner = "";
                string exc = "";

                if (ex.InnerException != null)
                    inner = ex.InnerException.ToString();
                exc = "Tripple failure on Redirect to Err Page \r\n";
                exc += ex.ToString() + "\r\n";
                exc += "Inner exception: \r\n" + inner;
                exc += "\r\n" + GetHttpInformation();
                if (!toolBarURL)
                {
                    System.Diagnostics.EventLog.WriteEntry("WWW", exc, System.Diagnostics.EventLogEntryType.Warning);
                    HttpContext.Current.Response.Redirect("/Error.htm");
                }
            }
        }

        /// <summary>
        /// Check to see if a member is valid (the member exists, having a positive memberid that correlates to a member in the database)
        /// </summary>
        /// <param name="member">The member to test</param>
        /// <returns>True if the member exists, false otherwise</returns>
        public static bool IsValidMember(IMemberDTO member, Brand brand)
        {
            bool retVal = false;

            if (member != null && member.MemberID > 0 && member.GetUserName(brand) != null && member.EmailAddress != null)
            {
                retVal = true;
            }

            return retVal;
        }

        /// <summary>
        /// Returns a string representation of the age of a person given the birthdate.
        /// If the age computed is not valid, returns a string given a resource key.
        /// </summary>
        /// <param name="birthDate"></param>
        /// <param name="g">ContextGlobal instance from the caller</param>
        /// <param name="caller">calling control</param>
        /// <param name="ageNotValidResourceKey">Resource key to use in case age makes no sense.</param>
        /// <returns></returns>
        public static string GetAgeCaption(DateTime birthDate, ContextGlobal g, object caller, string ageNotValidResourceKey)
        {
            int age = GetAge(birthDate);

            if (age > 0)
            {
                return age.ToString();
            }
            else
            {
                return g.GetResource(ageNotValidResourceKey, caller);
            }
        }

        public static int GetAge(DateTime birthDate)
        {
            if (birthDate == DateTime.MinValue) return 0;

            int age = DateTime.Today.Year - birthDate.Year;
            if (birthDate > DateTime.Today.AddYears(-age))
                age--;
            return age;
        }

        public static int GetAge(IMemberDTO member, Brand brand)
        {
            if (member != null && brand != null)
                return GetAge(member.GetAttributeDate(brand, "BirthDate", DateTime.MinValue));
            else
                return 0;
        }

        /// <summary>
        /// Originally for JDate.co.il
        /// Returns a boolean value of whether this member currently has a valid 
        /// premium plan subscription  
        /// </summary>
        /// <param name="memberID">Member ID being checked</param>
        /// <param name="brand">Current brand</param>
        /// <returns>True if the member currently has a valid premium plan subscription or false if the member has not subscribed to the premium plan or has expired</returns>
        public static bool IsMemberPremiumAuthenticated(int memberID, Brand brand)
        {
            //return true;

            try
            {
                IMemberDTO objMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                DateTime dtePremiumAuthenticatedExpirationDate = objMember.GetAttributeDate(brand, "PremiumAuthenticatedExpirationDate", DateTime.MinValue);

                if (dtePremiumAuthenticatedExpirationDate == DateTime.MinValue)
                {
                    // Occurs when the member has no PremiumAuthenticatedExpirationDate attribute
                    // because they never subscribed to the Premium plan
                    return false;
                }
                else
                {
                    if (DateTime.Now < dtePremiumAuthenticatedExpirationDate)
                    {
                        // Member has subscribed to the premium plan and the time has
                        // not yet expired
                        return true;
                    }
                    else
                    {
                        // Expired premium plan subscription
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Error in FrameworkGlobals.IsMemberPremiumAuthenticated(), " + ex.ToString());

                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

        /// <summary>
        /// Originally for JDate.co.il
        /// Returns a boolean value of whether this member should have his profile 
        /// and mini profile highlighted
        /// </summary>
        /// <param name="memberID">Member ID being checked</param>
        /// <param name="brand">Current brand</param>
        /// <returns>True if the member should be highlighted or false if the member should not be highlighted</returns>
        public static bool memberHighlighted(int memberID, Brand brand)
        {
            try
            {
                IMemberDTO objMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                DateTime dteHighlightedExpirationDate = DateTime.MinValue;// objMember.GetAttributeDate(brand, "HighlightedExpirationDate", DateTime.MinValue);
                Spark.Common.AccessService.AccessPrivilege ap = objMember.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.HighlightedProfile, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (ap != null)
                {
                    dteHighlightedExpirationDate = ap.EndDatePST;
                }

                /*
                Returns a boolean value of whether this member currently has the highlighted profile
                premium service and whether it is turned on.  This is independent of whether this
                member has an active subscription.				
                True if the member currently has the highlighted profile and is turned on or false if the member does not have the highlighted profile or has it but turned it off.  
                */
                if (dteHighlightedExpirationDate == DateTime.MinValue)
                {
                    // Member does not have the highlighted profile
                    return false;
                }
                else
                {
                    if (DateTime.Now < dteHighlightedExpirationDate)
                    {
                        // Highlighted profile purchase has not yet expired
                        // Check whether the highlighted profile is turned on
                        if ((objMember.GetAttributeInt(brand, "HighlightedFlag", 0) == 1))
                        {
                            // Highlighted profile is turned on
                            return true;
                        }
                        else
                        {
                            // Highlighted profile is turned off
                            return false;
                        }
                    }
                    else
                    {
                        // Highlighted profile purchase has expired  
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Error in FrameworkGlobals.memberHighlighted(), " + ex.ToString());

                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

        public static bool memberHighlighted(IMemberDTO objMember, Brand brand)
        {
            try
            {
                DateTime dteHighlightedExpirationDate = DateTime.MinValue;// objMember.GetAttributeDate(brand, "HighlightedExpirationDate", DateTime.MinValue);
                Spark.Common.AccessService.AccessPrivilege ap = objMember.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.HighlightedProfile, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (ap != null)
                {
                    dteHighlightedExpirationDate = ap.EndDatePST;
                }

                /*
                Returns a boolean value of whether this member currently has the highlighted profile
                premium service and whether it is turned on.  This is independent of whether this
                member has an active subscription.				
                True if the member currently has the highlighted profile and is turned on or false if the member does not have the highlighted profile or has it but turned it off.  
                */
                if (dteHighlightedExpirationDate == DateTime.MinValue)
                {
                    // Member does not have the highlighted profile
                    return false;
                }
                else
                {
                    if (DateTime.Now < dteHighlightedExpirationDate)
                    {
                        // Highlighted profile purchase has not yet expired
                        // Check whether the highlighted profile is turned on
                        if ((objMember.GetAttributeInt(brand, "HighlightedFlag", 0) == 1))
                        {
                            // Highlighted profile is turned on
                            return true;
                        }
                        else
                        {
                            // Highlighted profile is turned off
                            return false;
                        }
                    }
                    else
                    {
                        // Highlighted profile purchase has expired  
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Error in FrameworkGlobals.memberHighlighted(), " + ex.ToString());

                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

        /// <summary>
        /// Verifies that a premium subscriber still has at least one active premium service    
        /// </summary>
        /// <example>
        /// For example, a member can purchase a 3 month premium service and then 2 months into
        /// it, purchase a standard plan.  When the member purchases the standard plan, it will
        /// set his subscription status to a premium subscriber since he still has 1 month of 
        /// premium service left.  However, after the 3 months ends, he should be a standard subscriber. 
        /// Also, a premium subscriber may turn into a premium member if an admin removes subscription privilege.
        /// For example, a member can purchase a 3 month premium service and then 1 month into it,
        /// an admin removes 1 month of subscription privilege.  At this moment, the member still has both 
        /// subscription and premium privilege so he is still a premium subscriber.  But after the second month,
        /// this member will now just have premium privilege but not subscription privilege.  His subscription
        /// status should be a premium member. 
        /// Also, a premium subscriber may turn into a non subscriber if an admin removes both subscription
        /// and premium privilege.  For example, a member can purchase a 3 month premium service and then 1 
        /// month into it, the admin selects a premium plan and removes 1 month.  At this moment, the member still
        /// has both subscription and premium privilege so he is still a premium subscriber.  But after the 
        /// second month, this member will not have either subscription nor premium privilege.  His subscription
        /// status should be a non subscriber.  
        /// Also need to check that a subscribed member still has a valid subscription expiration date since
        /// his current subscription may be expire but nothing triggers a check for this.
        /// </example>
        /// <param name="memberID">Member ID being checked</param>
        /// <param name="brand">Current brand</param>
        /// <returns>Member subscription status</returns>
        public static int GetMemberSubcriptionStatus(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            int memberSubscriptionStatus = Constants.NULL_INT;

            try
            {
                bool evaluateSubscriptionStatusAgain = false;

                Matchnet.Member.ServiceAdapters.Member objMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                //get sub status
                memberSubscriptionStatus = objMember.GetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONSTATUS, 0);
                Matchnet.Purchase.ValueObjects.SubscriptionStatus enumSubscriptionStatus = (Matchnet.Purchase.ValueObjects.SubscriptionStatus)memberSubscriptionStatus;

                //get basic sub expiration date
                DateTime dtSubendDate = DateTime.MinValue;// objMember.GetAttributeDate(brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE, DateTime.MinValue);
                Spark.Common.AccessService.AccessPrivilege ap = objMember.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.BasicSubscription, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (ap != null)
                {
                    dtSubendDate = ap.EndDatePST;
                }

                if (enumSubscriptionStatus == Matchnet.Purchase.ValueObjects.SubscriptionStatus.PremiumSubscriber)
                {
                    if (dtSubendDate != DateTime.MinValue)
                    {
                        // Member is currently identified as a premium subscriber
                        //if ((PremiumServiceSA.Instance.GetMemberActivePremiumServices(memberID, brand).Count < 1)
                        //    || dtSubendDate < DateTime.Now)
                        if ((PremiumServiceSA.Instance.GetMemberActivePremiumServices(objMember, brand).Count < 1)
                           || dtSubendDate < DateTime.Now)
                        {
                            // Member does not have an active premium service or the member does not have
                            // subscription privilege.  
                            // Reevaluate the subscription status of this member
                            evaluateSubscriptionStatusAgain = true;
                        }
                    }
                }
                else if (enumSubscriptionStatus != Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber
                         && enumSubscriptionStatus != Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberFreeTrial
                         && enumSubscriptionStatus != Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed)
                {
                    if (dtSubendDate != DateTime.MinValue)
                    {
                        // Member is currently identified as a subscriber
                        if (dtSubendDate < DateTime.Now)
                        {
                            // Member does not have subscription privilege.  
                            // Reevaluate the subscription status of this member
                            evaluateSubscriptionStatusAgain = true;
                        }
                    }
                }

                if (evaluateSubscriptionStatusAgain)
                {
                    MemberSaveResult result = null;

                    memberSubscriptionStatus = Convert.ToInt16(Purchase.ServiceAdapters.PurchaseSA.Instance.GetMemberSubStatus(memberID, brand.Site.SiteID, dtSubendDate, false).Status);
                    objMember.SetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONSTATUS, memberSubscriptionStatus);
                    result = MemberSA.Instance.SaveMember(objMember);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Error in FrameworkGlobals.GetMemberSubcriptionStatus(), " + ex.ToString());
            }

            return memberSubscriptionStatus;
        }

        public static int GetMemberSubcriptionStatus(Matchnet.Member.ServiceAdapters.Member member, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            int memberSubscriptionStatus = Constants.NULL_INT;

            try
            {
                bool evaluateSubscriptionStatusAgain = false;

                //Matchnet.Member.ServiceAdapters.Member objMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                //get sub status
                memberSubscriptionStatus = member.GetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONSTATUS, 0);
                Matchnet.Purchase.ValueObjects.SubscriptionStatus enumSubscriptionStatus = (Matchnet.Purchase.ValueObjects.SubscriptionStatus)memberSubscriptionStatus;

                //get basic sub expiration date
                DateTime dtSubendDate = DateTime.MinValue;// objMember.GetAttributeDate(brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE, DateTime.MinValue);
                Spark.Common.AccessService.AccessPrivilege ap = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.BasicSubscription, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (ap != null)
                {
                    dtSubendDate = ap.EndDatePST;
                }

                if (enumSubscriptionStatus == Matchnet.Purchase.ValueObjects.SubscriptionStatus.PremiumSubscriber)
                {
                    if (dtSubendDate != DateTime.MinValue)
                    {
                        // Member is currently identified as a premium subscriber
                        //if ((PremiumServiceSA.Instance.GetMemberActivePremiumServices(memberID, brand).Count < 1)
                        //    || dtSubendDate < DateTime.Now)
                        if ((PremiumServiceSA.Instance.GetMemberActivePremiumServices(member, brand).Count < 1)
                           || dtSubendDate < DateTime.Now)
                        {
                            // Member does not have an active premium service or the member does not have
                            // subscription privilege.  
                            // Reevaluate the subscription status of this member
                            evaluateSubscriptionStatusAgain = true;
                        }
                    }
                }
                else if (enumSubscriptionStatus != Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber
                         && enumSubscriptionStatus != Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberFreeTrial
                         && enumSubscriptionStatus != Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed)
                {
                    if (dtSubendDate != DateTime.MinValue)
                    {
                        // Member is currently identified as a subscriber
                        if (dtSubendDate < DateTime.Now)
                        {
                            // Member does not have subscription privilege.  
                            // Reevaluate the subscription status of this member
                            evaluateSubscriptionStatusAgain = true;
                        }
                    }
                }

                if (evaluateSubscriptionStatusAgain)
                {
                    MemberSaveResult result = null;

                    memberSubscriptionStatus = Convert.ToInt16(Purchase.ServiceAdapters.PurchaseSA.Instance.GetMemberSubStatus(member.MemberID, brand.Site.SiteID, dtSubendDate, false).Status);
                    member.SetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONSTATUS, memberSubscriptionStatus);
                    result = MemberSA.Instance.SaveMember(member);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Error in FrameworkGlobals.GetMemberSubcriptionStatus(), " + ex.ToString());
            }

            return memberSubscriptionStatus;
        }

        public static string GetAbbreviatedMemberSubscriptionStatusForOmniture(Matchnet.Purchase.ValueObjects.SubscriptionStatus subscriptionStatus)
        {
            switch (subscriptionStatus)
            {
                case Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed:
                    return "NeverSub";
                case Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber:
                    return "ExSub";
                case Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscribedFTS:
                    return "FTS";
                case Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscribedWinBack:
                    return "WB";
                case Matchnet.Purchase.ValueObjects.SubscriptionStatus.RenewalSubscribed:
                    return "RenewingFTS";
                case Matchnet.Purchase.ValueObjects.SubscriptionStatus.RenewalWinBack:
                    return "RenewingWB";
                case Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscriberFreeTrial:
                    return "SubFT";
                case Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberFreeTrial:
                    return "FT";
                case Matchnet.Purchase.ValueObjects.SubscriptionStatus.PremiumSubscriber:
                    return "Premium";
                case Matchnet.Purchase.ValueObjects.SubscriptionStatus.PausedSubscriber:
                    return "Paused";
                default:
                    return "Unknown";
            }
        }

        /// <summary>
        /// Returns a boolean value of whether the sender of the message speaks the site
        /// default language
        /// </summary>
        /// <param name="memberID">Member ID being checked</param>
        /// <param name="brand">Current brand</param>
        /// <returns>True if the sender speaks the site default language or false if the sender does not speak the site default language</returns>
        public static bool senderSpeaksSiteDefaultLanguage(int senderMemberID, Brand brand)
        {
            bool blnSenderSpeaksSiteDefaultLanguage = false;

            try
            {
                IMemberDTO sender = MemberSA.Instance.GetMember(senderMemberID, MemberLoadFlags.None);

                int languageMask = sender.GetAttributeInt(brand, "LanguageMask");
                int siteDefaultLanguage = brand.Site.LanguageID;

                if (languageMask != Constants.NULL_INT)
                {
                    if ((languageMask & siteDefaultLanguage) == siteDefaultLanguage)
                    {
                        blnSenderSpeaksSiteDefaultLanguage = true;
                    }
                }
            }
            catch (Exception ex)
            {
                // Member may not exists or attribute may not exists for this member
                blnSenderSpeaksSiteDefaultLanguage = false;
            }

            return blnSenderSpeaksSiteDefaultLanguage;
        }

        /// <summary>
        /// Returns a boolean value of whether the recipient of the message speaks the site
        /// default language
        /// </summary>
        /// <param name="memberID">Member ID being checked</param>
        /// <param name="brand">Current brand</param>
        /// <returns>True if the recipient speaks the site default language or false if the recipient does not speak the site default language</returns>
        public static bool recipientSpeaksSiteDefaultLanguage(int senderMemberID, Brand brand)
        {
            return senderSpeaksSiteDefaultLanguage(senderMemberID, brand);
        }

        public static string GetOffsetAdjustedDateDisplay(DateTime pstDate, Brand brand)
        {
            var cookieHelper = new CookieHelper();
            var offset = cookieHelper.GetCookie("tzo", string.Empty, false);
            int offsetValue = 0;
            string timeFormat = brand.Site.CultureInfo.LCID == Matchnet.Lib.ConstantsTemp.LOCALE_US_ENGLISH
                                    ? "MM/dd/yyyy hh:mm tt"
                                    : "dd/MM/yyyy hh:mm tt";

            if (offset != string.Empty && int.TryParse(offset, out offsetValue))
            {
                return pstDate.ToUniversalTime().AddHours(offsetValue).ToString(timeFormat);
            }

            return pstDate.ToString(timeFormat);
        }


        public static string getDateDisplay(DateTime pDate, Brand pBrand)
        {
            if (pBrand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                //set to Israel local time.
                //TODO:  this won't work around dailight savings time.
                // consider using GMT and setting local to Israel to remedy. 
                // -NH
                pDate = pDate.AddHours(10);

                // The FullDateTimePattern, "F", is overriden in Default.aspx.cs when the culture info is set initially.
                return pDate.ToString("F");
            }
            else if (pBrand.Site.LanguageID == (int)Matchnet.Language.French)
            {
                pDate = pDate.AddHours(9);

                //return pDate.ToString("dd/MM/yyyy hh:mm tt");
                //return string.Format("{0:dd/MM/yyyy h:mm tt}", pDate);

                string strDate = string.Format("{0:dd/MM/yyyy hh:mm tt}", pDate);
                int intHourAdjustment = pDate.Hour - 12;

                if (intHourAdjustment <= 0)
                {
                    strDate += " AM";
                }
                else if (intHourAdjustment > 0)
                {
                    strDate += " PM";
                }

                return strDate;
            }
            else if (pBrand.Site.SiteID == (int)WebConstants.SITE_ID.JDateUK)
            {
                // In the JDateUK case, we can't depend on the LanguageID since it's English just like JDate.com
                pDate = pDate.AddHours(8 - pBrand.Site.GMTOffset);

                // The FullDateTimePattern, "F", is overriden in Default.aspx.cs when the culture info is set initially.
                return pDate.ToString("F");
            }
            else
            {
                // The FullDateTimePattern, "F", is overriden in Default.aspx.cs when the culture info is set initially.
                return pDate.ToString("F");
            }
        }

        /// <summary>
        /// This method is necessary because doing a Convert.ToDateTime("04/08/2005") for Hebrew sites will assume
        /// that the culture is already "Hebrew" and thus the 04 will be assumed to be the Day.  We must do a convert
        /// using the AS Culture (US) while performing the conversion.  Search to see where this method is called to
        /// see why it is necessary.
        /// </summary>
        /// <param name="dateStr">Assumes that this Date string will be formatted in AS (US) Culture. mm/dd/yyyy.</param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public static string getDateDisplay(string dateStr, Brand brand)
        {
            DateTime date = Convert.ToDateTime(dateStr, new CultureInfo("en-US"));

            return getDateDisplay(date, brand);
        }

        public static string GetGenderMaskString(int genderMask)
        {
            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];
            string gender = GetGenderString(genderMask);
            string seeking = GetSeekingGenderString(genderMask);

            if (seeking == string.Empty || gender == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                return gender + " " + g.GetResource("SEEKING") + " " + seeking;
            }
        }

        public static string GetGenderString(int genderMask)
        {
            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];
            string genderStr = string.Empty;

            if (genderMask < 0)
                return genderStr;

            if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                return g.GetResource("MALE");

            if ((genderMask & ConstantsTemp.GENDERID_FEMALE) == ConstantsTemp.GENDERID_FEMALE)
                return g.GetResource("FEMALE");

            if ((genderMask & ConstantsTemp.GENDERID_MTF) == ConstantsTemp.GENDERID_MTF)
                return g.GetResource("MTF");

            if ((genderMask & ConstantsTemp.GENDERID_FTM) == ConstantsTemp.GENDERID_FTM)
                return g.GetResource("FTM");

            // Else
            return genderStr;
        }

        public static string GetGenderString(int genderMask, Brand brand)
        {
            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];
            string genderStr = string.Empty;

            if (genderMask < 0)
                return genderStr;

            if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                return g.GetResource(brand, "MALE");

            if ((genderMask & ConstantsTemp.GENDERID_FEMALE) == ConstantsTemp.GENDERID_FEMALE)
                return g.GetResource(brand, "FEMALE");

            if ((genderMask & ConstantsTemp.GENDERID_MTF) == ConstantsTemp.GENDERID_MTF)
                return g.GetResource(brand, "MTF");

            if ((genderMask & ConstantsTemp.GENDERID_FTM) == ConstantsTemp.GENDERID_FTM)
                return g.GetResource(brand, "FTM");

            // Else
            return genderStr;
        }

        public static string GetSeekingGenderString(int genderMask)
        {
            string seeking = string.Empty;

            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];

            if ((genderMask & ConstantsTemp.GENDERID_SEEKING_FEMALE) == ConstantsTemp.GENDERID_SEEKING_FEMALE)
                seeking = g.GetResource("SEEKING_FEMALE");

            if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MALE) == ConstantsTemp.GENDERID_SEEKING_MALE)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + g.GetResource("SEEKING_MALE");
            }

            if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MTF) == ConstantsTemp.GENDERID_SEEKING_MTF)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + g.GetResource("SEEKING_MTF");
            }

            if ((genderMask & ConstantsTemp.GENDERID_SEEKING_FTM) == ConstantsTemp.GENDERID_SEEKING_FTM)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + g.GetResource("SEEKING_FTM");
            }

            return seeking;
        }

        public static string GetSeekingGenderString(int genderMask, Brand brand)
        {
            string seeking = string.Empty;

            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];

            if ((genderMask & ConstantsTemp.GENDERID_SEEKING_FEMALE) == ConstantsTemp.GENDERID_SEEKING_FEMALE)
                seeking = g.GetResource(brand, "SEEKING_FEMALE");

            if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MALE) == ConstantsTemp.GENDERID_SEEKING_MALE)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + g.GetResource(brand, "SEEKING_MALE");
            }

            if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MTF) == ConstantsTemp.GENDERID_SEEKING_MTF)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + g.GetResource(brand, "SEEKING_MTF");
            }

            if ((genderMask & ConstantsTemp.GENDERID_SEEKING_FTM) == ConstantsTemp.GENDERID_SEEKING_FTM)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + g.GetResource(brand, "SEEKING_FTM");
            }

            return seeking;
        }

        public static string GetRegionString(int regionID, int languageID)
        {
            return GetRegionString(regionID, languageID, false, false, false);
        }

        public static string GetRegionString(int regionID, int languageID, bool showPostalCode)
        {
            return GetRegionString(regionID, languageID, showPostalCode, false, false);
        }

        public static string GetCityString(int regionID, int languageID)
        {
            string city = string.Empty;

            if (regionID > 0)
            {
                // regionLanguage = new RegionLanguage();
                // regionLanguage.PopulateHierarchy(regionAttribute, g.Brand.Site.LanguageID);
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);

                // Incrementally build up the regionString with commas where necessary
                // based on the contents being empty or not before and after each comma
                // This made for CI/CP and tested on CL/GL 
                // start WEL
                city = regionLanguage.CityName;
            }

            return city;
        }

        public static string GetRegionString(int regionID, int languageID, bool showPostalCode, bool showAbbreviatedState, bool showAbbreviatedCountry)
        {
            return GetRegionString(regionID, languageID, showPostalCode, showAbbreviatedState, showAbbreviatedCountry, true);
        }

        public static string GetRegionString(int regionID, int languageID, bool showPostalCode, bool showAbbreviatedState, bool showAbbreviatedCountry, bool showFullState)
        {
            string regionString = string.Empty;

            if (regionID > 0)
            {
                // regionLanguage = new RegionLanguage();
                // regionLanguage.PopulateHierarchy(regionAttribute, g.Brand.Site.LanguageID);
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);

                // Incrementally build up the regionString with commas where necessary
                // based on the contents being empty or not before and after each comma
                // This made for CI/CP and tested on CL/GL 
                // start WEL
                regionString = regionLanguage.CityName;

                string stateString = string.Empty;

                // TT #13622, hide the state name if the region is not in the same country as the Site's default region.
                if (isDefaultRegion(regionLanguage) && (showFullState || showAbbreviatedState))
                {
                    if (showAbbreviatedState && regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                    {
                        stateString = regionLanguage.StateAbbreviation;
                    }
                    else
                    {
                        stateString = regionLanguage.StateDescription;
                    }
                }

                if (isNotBlankString(regionString) && isNotBlankString(stateString))
                {
                    regionString += ", ";
                }
                regionString += stateString;

                if (!isDefaultRegion(regionLanguage))
                {
                    string countryString = string.Empty;
                    if (showAbbreviatedCountry)
                    {
                        countryString = regionLanguage.CountryAbbreviation;
                    }
                    else
                    {
                        countryString = regionLanguage.CountryName;
                    }
                    if (isNotBlankString(regionString) && isNotBlankString(countryString))
                    {
                        regionString += ", ";
                    }
                    regionString += countryString;
                }

                // end WEL

                if (regionString != null && regionString.Length > ConstantsTemp.MAX_REGION_STRING_LENGTH)
                {
                    regionString = regionString.Substring(0, ConstantsTemp.MAX_REGION_STRING_LENGTH - 3) + "...";
                }
            }
            else
            {
                regionString = "N/A";
            }
            return regionString;
        }


        public static string GetRegionString(int regionID, int languageID, bool showAbbreviatedState, bool showAbbreviatedCountry)
        {
            string regionString = string.Empty;

            if (regionID > 0)
            {
                // regionLanguage = new RegionLanguage();
                // regionLanguage.PopulateHierarchy(regionAttribute, g.Brand.Site.LanguageID);
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);

                // Incrementally build up the regionString with commas where necessary
                // based on the contents being empty or not before and after each comma
                // This made for CI/CP and tested on CL/GL 
                // start WEL
                regionString = regionLanguage.CityName;

                string stateString = string.Empty;


                if (showAbbreviatedState && regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                {
                    stateString = regionLanguage.StateAbbreviation;
                }
                else
                {
                    stateString = regionLanguage.StateDescription;
                }


                if (isNotBlankString(regionString) && isNotBlankString(stateString))
                {
                    regionString += ", ";
                }
                regionString += stateString;


                string countryString = string.Empty;
                if (showAbbreviatedCountry)
                {
                    countryString = regionLanguage.CountryAbbreviation;
                }
                else
                {
                    countryString = regionLanguage.CountryName;
                }
                if (isNotBlankString(regionString) && isNotBlankString(countryString))
                {
                    regionString += ", ";
                }
                regionString += countryString;


                // end WEL

                if (regionString != null && regionString.Length > ConstantsTemp.MAX_REGION_STRING_LENGTH)
                {
                    regionString = regionString.Substring(0, ConstantsTemp.MAX_REGION_STRING_LENGTH - 3) + "...";
                }
            }
            else
            {
                regionString = "N/A";
            }
            return regionString;
        }

        private static bool isDefaultRegion(RegionLanguage regionLanguage)
        {
            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];
            if (g == null || regionLanguage.CountryRegionID == g.Brand.Site.DefaultRegionID)
            {
                return true;
            }
            return false;
        }

        private static bool isNotBlankString(string s)
        {
            if (s != String.Empty && s != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if pBrand is one of the Hebrew websites; Cupid, JDIL, or anything where languageID is Hebrew.
        /// </summary>
        /// <param name="pBrand"></param>
        /// <returns></returns>
        public static bool isHebrewSite(Brand pBrand)
        {
            if (pBrand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Cupid ||
                pBrand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL ||
                pBrand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public static string Ellipsis(string currentValue, int maxLength)
        {
            if (currentValue != null && currentValue.Length > maxLength)
            {
                currentValue = currentValue.Substring(0, maxLength - 3) + "...";
            }

            return currentValue;
        }

        public static string Ellipsis(string currentValue, int maxLength, string trailing)
        {
            if (currentValue != null && currentValue.Length > maxLength)
            {
                currentValue = currentValue.Substring(0, maxLength - 1) + trailing;
            }

            return currentValue;
        }


        public static string GetControlHTML(System.Web.UI.WebControls.WebControl control)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter sw = new System.IO.StringWriter(sb);
            System.Web.UI.HtmlTextWriter htmlWriter = new System.Web.UI.HtmlTextWriter(sw);
            control.RenderControl(htmlWriter);
            return htmlWriter.InnerWriter.ToString();
        }


        public static string Link(string caption, string href, string attributes)
        {
            return LinkExternal(caption, LinkHref(href, true), attributes);
        }

        public static string LinkExternal(string caption, string href, string attributes)
        {

            if (attributes.IndexOf("class") == -1)
            {
                attributes = attributes + " class=\"default\"";
            }
            return "<a href=\"" + href + "\" " + attributes + ">" + caption + "</a>";
        }

        public static DropDownList CreateHeightList(ContextGlobal g, string attributeID, int attributeValue)
        {
            return CreateHeightList(g, attributeID, attributeValue, false);
        }

        private static void PopulateHeightListInHebrew(ContextGlobal g, DropDownList pDropDownList)
        {
            string displayString = "{0}" + g.GetResource("CM");

            // Add the minimum height option.
            pDropDownList.Items.Add(new ListItem(string.Format(displayString, "< 137"), Convert.ToString(Convert.ToInt32(Constants.HEIGHT_MIN - 2))));

            for (int i = 137; i <= 244; i++)
            {
                pDropDownList.Items.Add(new ListItem(string.Format(displayString, i), i.ToString()));
            }
            pDropDownList.Items.Add(new ListItem(string.Format(displayString, "> 244"), Convert.ToString(Convert.ToInt32(Constants.HEIGHT_MAX + 2))));
        }

        public static DropDownList CreateHeightList(ContextGlobal g, string attributeID, int attributeValue, bool withAnyAttribute)
        {
            double height;
            int heightUS;
            int inches;
            int feet;
            string display = "";
            ListItem listItem;
            bool siteUsesUSHeight = false;

            if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USE_US_HEIGHT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
            {
                siteUsesUSHeight = true;
            }

            DropDownList dropDownList = new DropDownList();
            dropDownList.ID = attributeID;

            if (withAnyAttribute)
            {
                ListItem li = new ListItem(g.GetResource("ANY_"), Constants.NULL_INT.ToString());

                dropDownList.Items.Add(li);
            }
            else
            {
                dropDownList.Items.Add(new ListItem("", ""));
            }

            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew
                || g.Brand.Site.LanguageID == (int)Matchnet.Language.French)
            {
                PopulateHeightListInHebrew(g, dropDownList);
            }
            else
            {
                #region less than min height
                height = Constants.HEIGHT_MIN - 2.54;
                if (siteUsesUSHeight || g.Brand.Site.CultureInfo.LCID == Constants.LOCALE_US_ENGLISH)
                {
                    heightUS = Convert.ToInt32(Constants.HEIGHT_MIN / 2.54);
                    inches = heightUS % 12;
                    feet = (heightUS - inches) / 12;
                    display = String.Format("< {0}\'{1}\"({2:F0} cm)", feet, inches, Constants.HEIGHT_MIN);
                }
                else
                {
                    display = String.Format("< {0:F0} cm", Constants.HEIGHT_MIN.ToString());
                }
                listItem = new ListItem();
                listItem.Text = display;
                listItem.Value = Convert.ToString(Convert.ToInt32(height));
                dropDownList.Items.Add(listItem);

                #endregion

                for (height = Constants.HEIGHT_MIN; height <= Constants.HEIGHT_MAX; height += 2.54)
                {
                    if (siteUsesUSHeight || g.Brand.Site.CultureInfo.LCID == Constants.LOCALE_US_ENGLISH)
                    {
                        heightUS = Convert.ToInt32(height / 2.54);
                        inches = heightUS % 12;
                        feet = (heightUS - inches) / 12;
                        display = String.Format("{0}\'{1}\"({2:F0} cm)", feet, inches, height);
                    }
                    else
                    {
                        display = String.Format("{0:F0} cm", height.ToString());
                    }
                    listItem = new ListItem();
                    listItem.Text = display;
                    listItem.Value = Convert.ToString(Convert.ToInt32(height));
                    dropDownList.Items.Add(listItem);
                }

                #region more than max height
                height = Constants.HEIGHT_MAX + 2.54;
                if (siteUsesUSHeight || g.Brand.Site.CultureInfo.LCID == Constants.LOCALE_US_ENGLISH)
                {
                    heightUS = Convert.ToInt32(Constants.HEIGHT_MAX / 2.54);
                    inches = heightUS % 12;
                    feet = (heightUS - inches) / 12;
                    display = String.Format("> {0}\'{1}\"({2:F0} cm)", feet, inches, Constants.HEIGHT_MAX);
                }
                else
                {
                    display = String.Format("> {0:F0} cm", Constants.HEIGHT_MAX.ToString());
                }
                listItem = new ListItem();
                listItem.Text = display;
                listItem.Value = Convert.ToString(Convert.ToInt32(height));
                dropDownList.Items.Add(listItem);
                #endregion
            }

            try
            {
                ListItem foundValue = dropDownList.Items.FindByValue(attributeValue.ToString());
                if (foundValue != null)
                {
                    dropDownList.SelectedValue = attributeValue.ToString();
                }
                else
                {
                    // if the value wasnt found, add one and try it again
                    attributeValue = attributeValue + 1;
                    foundValue = dropDownList.Items.FindByValue(attributeValue.ToString());

                    if (foundValue != null)
                    {
                        dropDownList.SelectedValue = attributeValue.ToString();
                    }
                }
            }
            catch (Exception)
            {
                //_g.ProcessException(ex); ->may try to select values with deleted attribute values. let it skip.
            }

            dropDownList.CssClass = "RegDropDown";

            return dropDownList;
        }

        public static DropDownList CreateWeightList(ContextGlobal g, string attributeName, int attributeValue)
        {
            return CreateWeightList(g, attributeName, attributeValue, false);
        }

        public static DropDownList CreateWeightList(ContextGlobal g, string attributeName, int attributeValue, bool withAnyAttribute)
        {
            double grams;
            int pounds;
            double kg;
            string display = "";
            string hebrewDisplay = "{0} " + g.GetResource("KG");
            bool siteUsesPounds = false;
            ListItem listItem;

            DropDownList dropDownList = new DropDownList();
            dropDownList.ID = attributeName;

            // Empty item at top of list
            if (withAnyAttribute)
            {
                ListItem li = new ListItem("<" + g.GetResource("ANY_") + ">", Constants.NULL_INT.ToString());

                dropDownList.Items.Add(li);
            }
            else
            {
                dropDownList.Items.Add(new ListItem("", null));
            }

            if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USE_US_WEIGHT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
            {
                siteUsesPounds = true;
            }

            if (siteUsesPounds || g.Brand.Site.CultureInfo.LCID == Constants.LOCALE_US_ENGLISH)
            {
                for (grams = Constants.WEIGHT_MIN; grams <= Constants.WEIGHT_MAX; grams += 907.2)
                {
                    pounds = Convert.ToInt32(grams / 453.6);
                    kg = Convert.ToDouble(RoundWeight(Convert.ToInt32(grams)));
                    display = String.Format("{0} lbs ({1:F2} kg)", pounds.ToString(), kg.ToString());
                    listItem = new ListItem();
                    listItem.Text = display;
                    listItem.Value = Convert.ToString(Convert.ToInt32(grams));
                    dropDownList.Items.Add(listItem);
                }
            }
            else
            {
                // default the starting weight to +500kg to offset WEIGHT_MIN at .5 kg
                for (grams = Constants.WEIGHT_MIN + 500; grams <= Constants.WEIGHT_MAX; grams += 1000)
                {
                    kg = Convert.ToDouble(RoundWeight(Convert.ToInt32(grams)));
                    if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
                    {
                        display = String.Format(hebrewDisplay, kg.ToString());
                    }
                    else
                    {
                        display = String.Format("{0} kg", kg.ToString());
                    }
                    listItem = new ListItem();
                    listItem.Text = display;
                    listItem.Value = Convert.ToString(Convert.ToInt32(grams));
                    dropDownList.Items.Add(listItem);
                }
            }

            /*			for (grams = Constants.WEIGHT_MIN; grams <= Constants.WEIGHT_MAX; grams+=907.2)
                        {
                            pounds = Convert.ToInt32(grams / 453.6);
                            kg = Convert.ToDouble(RoundWeight(Convert.ToInt32(grams)));

                            if( siteUsesPounds || (g.Brand.Site.CultureInfo.LCID == Constants.LOCALE_US_ENGLISH) )
                            {
                                display =  String.Format("{0} lbs ({1:F2} kg)",pounds.ToString(), kg.ToString());
                            }
                            else
                            {
                                if(g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
                                {
                                    display = String.Format(hebrewDisplay, kg.ToString());
                                }
                                else 
                                {
                                    display = String.Format("{0} kg", kg.ToString());
                                }
                            }
                            listItem = new ListItem();
                            listItem.Text = display;
                            listItem.Value = Convert.ToString(Convert.ToInt32(grams));
                            dropDownList.Items.Add(listItem);
                        }
            */
            try
            {
                dropDownList.SelectedValue = attributeValue.ToString();
            }
            catch (Exception)
            {
                //_g.ProcessException(ex); ->may try to select values with deleted attribute values. let it skip.
            }

            dropDownList.CssClass = "RegDropDown";

            return dropDownList;
        }

        public static double RoundWeight(int grams)
        {
            double kg;
            double offset;

            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];

            offset = (grams % 1000);
            kg = ((grams - offset) / 1000);
            if (offset < 250)
            {

            }
            else if (offset < 750)
            {
                kg += 0.5;
            }
            else
            {
                kg += 1;
            }
            return kg;
        }

        /// <summary>
        /// This method is unorganized and has wavering logic.  It is a good candidate for scrapping or rewriting
        /// </summary>
        /// <param name="href"></param>
        /// <param name="checkForHTTPS"></param>
        /// <returns></returns>
        public static string LinkHref(string href, bool checkForHTTPS)
        {
            if (StringIsEmpty(href)) return String.Empty;

            if (HttpContext.Current == null) return href;

            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];

            string returnValue = href;

            if (checkForHTTPS && g.IsSecureRequest() && href.ToLower().IndexOf("javascript:") == -1)
            {
                string host = HttpContext.Current.Request.Url.Host;

                if (g.Brand.Uri.ToLower().StartsWith(host.ToLower()))
                {
                    returnValue = "http://" + g.Brand.Uri + returnValue;
                }
                else
                {
                    returnValue = "http://" + host + returnValue;
                }
            }

            if (g.PersistLayoutTemplate == true && g.LayoutTemplate != LayoutTemplate.Standard && g.LayoutTemplate != LayoutTemplate.PopupOnce)
            {
                string delim;

                if (href.IndexOf("?") != -1)
                {
                    delim = "&";
                }
                else
                {
                    delim = "?";
                }
                returnValue += delim + "LayoutTemplateID=" + ((int)g.LayoutTemplate).ToString();
            }

            return returnValue;
        }

        /// <summary>
        /// This method is unorganized and has wavering logic.  It is a good candidate for scrapping or rewriting
        /// </summary>
        /// <param name="href"></param>
        /// <returns></returns>
        public static string LinkHrefSSL(string href)
        {
            string delim;
            string args = string.Empty;
            string host = string.Empty;
            string sourceID =
                (HttpContext.Current.Request["srid"] != null) ?
                HttpContext.Current.Request["srid"] : string.Empty;
            string purchaseReasonTypeID =
                (HttpContext.Current.Request["prtid"] != null) ?
                HttpContext.Current.Request["prtid"] : string.Empty;
            string planID =
                (HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_PLANID] != null) ?
                HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_PLANID] : string.Empty;

            href = LinkHref(href, false);

            if (href.IndexOf("?") != -1)
                delim = "&";
            else
                delim = "?";

            if (sourceID.Length > 0)
                args += delim + "srid=" + sourceID;

            if (purchaseReasonTypeID.Length > 0)
                args += (args.Length > 0) ? "&prtid=" + purchaseReasonTypeID : delim + "prtid=" + purchaseReasonTypeID;

            if (planID.Length > 0)
                args += (args.Length > 0) ? "&" + WebConstants.URL_PARAMETER_NAME_PLANID + "=" + planID : delim + WebConstants.URL_PARAMETER_NAME_PLANID + "=" + planID;

            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];

            if ((Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("LOCAL_SECURE_MODE"))))
            {
                host = HttpContext.Current.Request.Url.Host;
            }
            else
            {
                host = g.Brand.Site.DefaultHost + "." + g.Brand.Uri;
            }

            if ((g.Brand.StatusMask & ConstantsTemp.PRIVATELABEL_STATUS_DISABLE_SSLURL) !=
                ConstantsTemp.PRIVATELABEL_STATUS_DISABLE_SSLURL)
            {
                delim = (args.Length > 0) ? "&" : delim;
            }

            // Bedrock pages aren't hosted by https servers anymore
            return "http://" + host + href + args;

            //if ((Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("LOCAL_SECURE_MODE"))))
            //{
            //    // in development mode, the ssl url should be "https://<machinename>.<brandUrl>"
            //    return "https://" + host + href + args;
            //}
            //else
            //{
            //    // in production, the ssl url should be the Site SSLUrl
            //    return g.Brand.Site.SSLUrl + href + args;
            //}
        }

        public static string GetArticleLink(string resourceConstant, LayoutTemplate pLayoutTemplate, string title, bool ssl)
        {
            string link = "/Applications/Article/Article.aspx?LayoutTemplateID=" + ((int)pLayoutTemplate).ToString() +
                    "&ArticleRC=" + resourceConstant +
                    "&title=" + System.Web.HttpContext.Current.Server.UrlEncode(title);

            if (ssl)
                link = LinkHrefSSL(link);
            else
                link = LinkHref(link, true);

            return link;
        }

        public static string GetLaunchWindowLink(string url, string text, string title, int width, int height, string properties)
        {
            string link = string.Format(
                "<a href=\"javascript:launchWindow('{0}', '{1}', '{2}', '{3}', '{4}')\">{5}</a>",
                url,
                title,
                width.ToString(),
                height.ToString(),
                properties,
                text);

            return link;
        }

        public static string GetSubscriptionLink()
        {
            return GetSubscriptionLink(true);
        }

        public static string GetSubscriptionLink(int PRTID)
        {
            return GetSubscriptionLink(true, PRTID);
        }

        public static string GetSubscriptionLink(bool javaScript)
        {
            return GetSubscriptionLink(true, Constants.NULL_INT);
        }

        public static string GetSubscriptionLink(bool javaScript, int PRTID)
        {
            string returnURL = "/applications/subscription/subscribe.aspx";
            if (PRTID != Constants.NULL_INT)
            {
                returnURL += "?prtid=" + PRTID.ToString();
            }
            returnURL = LinkHrefSSL(returnURL);

            if (javaScript)
            {
                returnURL = "javascript:if(window.opener != null && window.opener.location!=null){window.opener.location.href='"
                    + returnURL + "';window.close();}else{window.location.href='"
                    + returnURL + "';}";
            }

            return returnURL;
        }

        public static string GetEmailLink(int ViewingMemberID, bool returnToMember)
        {
            string emailLink = "/Applications/Email/Compose.aspx?MemberId=" + ViewingMemberID;

            //tt # 15984
            if (returnToMember)
            {
                emailLink += "&ReturnToMember=True";
            }

            return emailLink;
        }

        public static string GetEmailLinkTemplate(bool returnToMember)
        {
            string emailLink = "/Applications/Email/Compose.aspx?MemberId={0}";

            //tt # 15984
            if (returnToMember)
            {
                emailLink += "&ReturnToMember=True";
            }

            return emailLink;
        }

        public static string GlobalHeight(int localeID, int cm, Brand brand)
        {
            string height;
            int feet = 1;
            int inches = 1;
            bool siteUsesFeet = false;

            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];

            if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USE_US_HEIGHT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
            {
                siteUsesFeet = true;
            }

            if (cm > 0)
            {
                if (siteUsesFeet || localeID == Constants.LOCALE_US_ENGLISH)
                {

                    CentimetersToInches(cm, out feet, out inches);
                    height = feet.ToString() + "' " + inches.ToString() + "\" (" + cm.ToString() + "&nbsp;" + g.GetResource("CM", null) + ")";
                }
                else
                {
                    height = cm.ToString() + "&nbsp;" + g.GetResource("CM", null);
                }
            }
            else
            {
                height = Localizer.GetStringResource("N_A", null, brand, new StringDictionary());
            }

            return height;
        }

        /// <summary>
        /// This method should be used when ContextGlobal is not available. API/GetResource.aspx is using this method.
        /// </summary>
        /// <param name="localeID"></param>
        /// <param name="cm"></param>
        /// <param name="brand"></param>
        /// <param name="useContextGlobal"></param>
        /// <returns></returns>
        public static string GlobalHeight(int localeID, int cm, Brand brand, bool useContextGlobal)
        {
            if (useContextGlobal)
                return GlobalHeight(localeID, cm, brand);

            string height;
            int feet = 1;
            int inches = 1;
            bool siteUsesFeet = false;


            if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USE_US_HEIGHT", brand.Site.Community.CommunityID, brand.Site.SiteID) == "true")
            {
                siteUsesFeet = true;
            }

            if (cm > 0)
            {
                if (siteUsesFeet || localeID == Constants.LOCALE_US_ENGLISH)
                {

                    CentimetersToInches(cm, out feet, out inches);
                    height = feet.ToString() + "' " + inches.ToString() + "\" (" + cm.ToString() + " " + Localizer.GetStringResourceWithNoTokeMap("CM", null, brand) + ")";
                }
                else
                {
                    height = cm.ToString() + " " + Localizer.GetStringResourceWithNoTokeMap("CM", null, brand);
                }
            }
            else
            {
                height = Localizer.GetStringResource("N_A", null, brand, null);
            }

            return height;
        }

        public static void CentimetersToInches(int cm, out int feet, out int inches)
        {
            inches = Matchnet.Conversion.CInt(cm * 0.3937);
            feet = (inches / 12);
            inches = inches % 12;
        }

        public static bool DetermineReportLinkVisibility(int MemberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                string EnableReportAbuse = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_REPORT_ABUSE_DIGIT", brand.Site.Community.CommunityID, brand.Site.SiteID);

                if ("NOTAPPLIED" != EnableReportAbuse)
                {
                    string[] arrEnableReportID = EnableReportAbuse.Split(',');

                    //get the last digit of the MemberID
                    string digit = MemberID.ToString().Substring(MemberID.ToString().Length - 1);

                    foreach (string s in arrEnableReportID)
                    {
                        if (Convert.ToInt32(s) == Convert.ToInt32(digit))
                        {
                            return true;
                        }
                    }
                }

                /*
                if(Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE")))
                {
                    return true;
                }*/

                return false;
            }
            catch (Exception ex)
            {
                throw new Exception("FrameworkGlobals.DetermineReportLinkVisibility() - " + ex.Message);
            }

        }

        /// <summary>
        /// Helps to shield calls to ListControl.SetSelectedValue() so that they don't
        /// throw exceptions.
        /// </summary>
        /// <param name="control">control to set</param>
        /// <param name="theValue">value to set the selected value of the control to</param>
        /// <param name="defaultIndex">
        /// if an exception has to be handled, what should the new selected index
        /// for that control be
        /// </param>
        /// <returns>
        /// true if the list control was set up without incident, false if
        /// an error had to be handled
        /// </returns>
        public static bool SafeSetListControl(ListControl control, string theValue, int defaultIndex)
        {
            bool retVal = true;

            try
            {
                control.SelectedValue = theValue;
            }
            catch (ArgumentOutOfRangeException)
            {
                retVal = false;
                control.SelectedIndex = defaultIndex;
            }

            return (retVal);
        }

        /// <summary>
        /// Overload of above method that just un-sets the selection for the
        /// list control if there is an error.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="theValue"></param>
        /// <returns></returns>
        public static bool SafeSetListControl(ListControl control, string theValue)
        {
            return (SafeSetListControl(control, theValue, -1));
        }

        /// <summary>
        /// Determines whether or not the given string is empty
        /// </summary>
        /// <param name="val">The string to evaluate</param>
        /// <returns>Whether or not the given string is empty</returns>
        public static bool StringIsEmpty(string val)
        {
            if (val == null || val.Trim().Length == 0 || val == String.Empty)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// The Connect Framework is a web app controlled by the Provo office that exists as a subdomain.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="g"></param>
        /// <param name="addEncryptedSessionIDFlag">The Connect Framework sometimes makes calls to the SparkWS that
        /// require an encrypted SessionID.</param>
        /// <returns></returns>
        public static string BuildConnectFrameworkLink(String page, ContextGlobal g, bool addEncryptedSessionIDFlag)
        {
            bool enableEncryptedSessionIDInURL = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_SPWSCYPHER_IN_CONNECT_URL", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));

            string link = String.Format("http://connect.{0}{1}/{2}", GetDevSubdomain(g), g.Brand.Uri, page);
            if (addEncryptedSessionIDFlag && enableEncryptedSessionIDInURL)
                return g.AppendEncryptedSessionID(link);
            else
                return link;
        }

        public static void SetConnectCookie(ContextGlobal g)
        {
            bool setCookie = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_SPWSCYPHER_COOKIE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));

            if (setCookie)
            {
                string encryptedSessionID = HttpUtility.UrlEncode(Crypto.Encrypt(WebConstants.SPARK_WS_CRYPT_KEY, g.Session.Key.ToString()));
                HttpContext.Current.Response.Cookies[WebConstants.CONNECT_COOKIE_NAME].Value = encryptedSessionID;
                HttpContext.Current.Response.Cookies[WebConstants.CONNECT_COOKIE_NAME].Expires = DateTime.Now.AddMonths(1);
                //HttpContext.Current.Response.Cookies[WebConstants.CONNECT_COOKIE_NAME].Domain = "." + GetDevSubdomain(g) + g.Brand.Uri;
                HttpContext.Current.Response.Cookies[WebConstants.CONNECT_COOKIE_NAME].Domain = "." + g.Brand.Uri;
            }
        }

        public static void ExpireConnectCookie()
        {
            HttpContext.Current.Response.Cookies.Remove(WebConstants.CONNECT_COOKIE_NAME);
            HttpContext.Current.Response.Cookies[WebConstants.CONNECT_COOKIE_NAME].Expires = DateTime.Now.AddDays(-1D);
        }

        public static string StripQuerystringFromURL(string url)
        {
            return url.Substring(0, url.IndexOf('?') > 0 ? url.IndexOf('?') : url.Length);
        }

        public static string RemoveParamFromURL(string url, string paramName)
        {
            int questionMarkIndex = url.IndexOf("?");
            if (questionMarkIndex >= 0 && url.ToLower().Contains(paramName.ToLower() + "="))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(url.Substring(0, questionMarkIndex + 1));
                string[] paramArray = url.Substring(questionMarkIndex + 1).Split(new char[] { '&' });
                int count = 1;
                foreach (string param in paramArray)
                {
                    int equalSign = param.IndexOf("=");
                    string paramN = param.Substring(0, equalSign);
                    if (paramN.ToLower() != paramName.ToLower())
                    {
                        if (count > 1)
                        {
                            sb.Append("&");
                        }
                        sb.Append(param);
                        count++;
                    }
                }

                url = sb.ToString();
            }

            return url;
        }

        public static string AppendParamToURL(string url, string paramName, string paramValue)
        {
            string revisedParam = paramName + "=" + paramValue;

            int questionMarkIndex = url.IndexOf("?");
            if (questionMarkIndex <= 0)
            {
                url = url + "?" + revisedParam;
            }
            else if (url.ToLower().Contains(paramName.ToLower() + "="))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(url.Substring(0, questionMarkIndex + 1));
                string[] paramArray = url.Substring(questionMarkIndex + 1).Split(new char[] { '&' });
                int count = 1;
                foreach (string param in paramArray)
                {
                    int equalSign = param.IndexOf("=");
                    string paramN = param.Substring(0, equalSign);
                    if (paramN.ToLower() != paramName.ToLower())
                    {
                        if (count > 1)
                        {
                            sb.Append("&");
                        }
                        sb.Append(param);
                        count++;
                    }
                }

                if (count > 1)
                {
                    sb.Append("&");
                }
                sb.Append(revisedParam);
                url = sb.ToString();
            }
            else
            {
                url += "&" + revisedParam;
            }

            return url;
        }

        public static string AppendParamToURL(string url, string param)
        {
            string revisedParam = param;
            if (param.StartsWith("&"))
                revisedParam = param.Substring(1);

            if (IsJavascriptLink(url))
            {
                int questionMarkIndex = url.IndexOf("?");
                url = url.Insert(questionMarkIndex + 1, revisedParam + "&");
            }
            else if (url.IndexOf("?") > 0)
            {
                int questionMarkIndex = url.IndexOf("?");
                url = url.Insert(questionMarkIndex + 1, revisedParam + "&");
            }
            else
            {
                url += "?" + revisedParam;
            }

            return url;
        }

        public static bool IsJavascriptLink(string url)
        {
            if (!string.IsNullOrEmpty(url) && url.StartsWith("javascript:"))
                return true;
            else
                return false;
        }

        private static string GetDevSubdomain(ContextGlobal g)
        {
            string devSubdomain = String.Empty;
            if (g.Environment == String.Empty || g.Environment == WebConstants.ENV_PROD)
                devSubdomain = "";
            else
                devSubdomain = g.Environment + ".";
            return devSubdomain;
        }

        public static string GetEnvironmentString()
        {
            string env = "";
            try
            {
                env = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENVIRONMENT_TYPE");
                if (env != null)
                    env = env.Trim().ToLower();

                return env;
            }
            catch (Exception ex)
            { return env; }
        }


        /// <summary>
        /// Assemble the JavaScript code necessary to pop up a city chooser window that is bound
        /// to the parent page.
        /// </summary>
        /// <param name="PrivateLabelID"></param>
        /// <param name="RegionControlName"></param>
        /// <param name="cityControlName"></param>
        /// <returns></returns>
        public static string MakePopCityListJavaScript(int PrivateLabelID, string RegionControlName, string cityControlName)
        {
            return MakePopCityListJavaScript(PrivateLabelID, RegionControlName, cityControlName, "popCityList");
        }

        public static string MakePopCityListJavaScript(int PrivateLabelID, string RegionControlName, string cityControlName, string functionName)
        {
            System.Text.StringBuilder script = new System.Text.StringBuilder();
            script.Append("<script type=\"text/javascript\">\n");
            script.Append("function " + functionName + "(){\n");
            script.Append(@"var parentRegionID = document.forms[0].elements['" + RegionControlName + "'].value;");
            script.Append(@"window.open('/Applications/MemberProfile/CityList.aspx?" +
                "plid=" + PrivateLabelID +
                "&CityControlName=" + cityControlName + "&ParentRegionID=' + parentRegionID, '','height=415px,width=500px,scrollbars=yes');\n");

            script.Append("}\n</script>\n");

            return (script.ToString());
        }


        public static void SelectItem(ListControl list, string selectedValue)
        {
            ListItem li = list.Items.FindByValue(selectedValue);

            if (li != null)
            {
                list.SelectedItem.Selected = false;
                li.Selected = true;
            }
        }

        public static void SelectItem(ListControl list, string selectedValue, int defaultSelectedListOrder)
        {
            ListItem li = list.Items.FindByValue(selectedValue);

            if (null == li && defaultSelectedListOrder < list.Items.Count)
            {
                li = list.Items[defaultSelectedListOrder];
            }

            if (li != null)
            {
                list.SelectedItem.Selected = false;
                li.Selected = true;
            }
        }


        #region Unicode Conversion Utilities

        public static string GetUnicodeText(string pTextToDecode, int pCharacterSet)
        {
            /*
            if (pTextToDecode.Equals(string.Empty))
            {
                return string.Empty;
            }

            mnTranslator.IUnicodeEncoder encoder = null;

            encoder = new mnTranslator.UnicodeEncoderClass();

            //Get the Unicode encoding of the text
            encoder.SetMbText(pTextToDecode, pCharacterSet);
            string encodedText = encoder.GetUnicodeText();

            //Now decode the result back to Mb text
            encoder.SetUnicodeText(encodedText);
            string decodedText = encoder.GetMbText(pCharacterSet);

            //If the decoding doesn't match the original string, the original was already in Unicode 
            if (decodedText == pTextToDecode)
            {
                return encodedText;
            }
            else
            {
                return pTextToDecode;
            }
            */
            return pTextToDecode;
        }

        public static string GetUnicodeText(string pTextToDecode)
        {
            return GetUnicodeText(pTextToDecode, 1255);
        }

        public static string StripHtmlChars(string source)
        {
            try
            {
                return Regex.Replace(source, "<[^>]*>", "");
            }
            catch (Exception ex)
            { return source; }


        }
        /*
        public static string GetMbText(string pTextToDecode, int characterSet)
        {
            mnTranslator.IUnicodeEncoder encoder = null;
            try
            {
                encoder = new mnTranslator.UnicodeEncoderClass();
                encoder.SetUnicodeText(pTextToDecode);
                return encoder.GetMbText(characterSet);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        */

        /*
        public static string GetMbText(string pTextToDecode)
        {
            return GetMbText(pTextToDecode, 1255);
        }
        */

        #endregion


        public static string GetHttpInformation()
        {
            string info = "HttpInfo";
            try
            {
                info += ContextExceptions.getAdditionalExInfo(null);

                return info;
            }
            catch (Exception ex)
            {
                info += "\r\n Unable to get HttpInfo, because exception:\r\n" + ex.ToString();
                return info;
            }

        }

        public static string MatchAttributeOptions(ContextGlobal g, IMemberDTO targetMember, List<string> attrNames)
        {
            System.Text.StringBuilder matches = new System.Text.StringBuilder();
            try
            {
                if (g.Member == null || targetMember == null)
                    return null;


                if (attrNames == null || attrNames.Count <= 0)
                    return null;


                for (int i = 0; i < attrNames.Count; i++)
                {

                    string attr = attrNames[i];

                    int a1 = g.Member.GetAttributeInt(g.Brand, attr);
                    int a2 = targetMember.GetAttributeInt(g.Brand, attr);

                    if (a1 < 0 || a2 < 0)
                        continue;
                    int match = (a1 & a2);

                    string matchstring = Matchnet.Web.Framework.Util.Option.GetMaskContent(attr, match, g);
                    if (!String.IsNullOrEmpty(matchstring))
                    {
                        if (matches.Length > 0)
                            matchstring = ", " + matchstring;

                        matches.Append(matchstring);
                    }

                }

                return matches.ToString();

            }
            catch (Exception ex)
            { return matches.ToString(); }


        }

        public static IMemberDTO GetSpotlightProfile(ContextGlobal g, System.Collections.Generic.List<int> searchIDs, out bool promoMemberFlag)
        {
            IMemberDTO m = null;
            promoMemberFlag = false;
            int memberid = 0;
            try
            {
                string controlname = g.AppPage.ControlName;
                string uri = HttpContext.Current.Request.RawUrl;
                if (controlname.ToLower() == "default" && uri.ToLower().IndexOf("siteid") >= 0)
                    return null;

                Matchnet.PremiumServices.ValueObjects.PremiumServiceCollections services = Matchnet.PremiumServices.ServiceAdapter.ServiceSA.Instance.GetPremiumServiceCollection();

                //get last spotlight profile
                if (HttpContext.Current.Request.Form["__EVENTTARGET"] != null)
                {
                    string eventtarget = HttpContext.Current.Request.Form["__EVENTTARGET"];
                    if (!String.IsNullOrEmpty(eventtarget))
                    {
                        if (eventtarget.ToLower().IndexOf("lnktakeaction") >= 0)
                        {
                            memberid = PremiumServiceSearch.ServiceAdapters.SpotlightMemberSA.Instance.GetLastDisplayedSpotlight(g.Brand, g.Member, g.AppPage.ID);
                            if (memberid > 0)
                            {
                                m = MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                                if (m != null)
                                {
                                    g.Session.Add("SpotlightHotlistEvent", "true", Session.ValueObjects.SessionPropertyLifetime.TemporaryDurable);
                                    return m;
                                }
                            }
                        }
                        else
                        { g.Session.Remove("SpotlightHotlistEvent"); }
                    }

                }
                else if (g.Session.Get("SpotlightHotlistEvent") != null)
                {
                    memberid = PremiumServiceSearch.ServiceAdapters.SpotlightMemberSA.Instance.GetLastDisplayedSpotlight(g.Brand, g.Member, g.AppPage.ID);
                    if (memberid > 0)
                    {
                        m = MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                        if (m != null)
                        {
                            g.Session.Remove("SpotlightHotlistEvent");
                            return m;
                        }
                    }
                    
                }

                //get self spotlight profile
                if (g.Session.Get("SelfSpotlightedCheck") == null)
                {
                    g.Session.Add("SelfSpotlightedCheck", "true", SessionPropertyLifetime.Temporary);
                    memberid = PremiumServiceSearch.ServiceAdapters.SpotlightMemberSA.Instance.GetSelfProfileSpotlight(g.Brand, g.Member, false);
                    if (memberid > 0)
                    {
                        m = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                        return m;
                    }
                }

                //get next spotlight profile
                bool resetSpotlightClientMetadataCache = HttpContext.Current.Request.QueryString["resetSpotlightClientCache"] != null ? true : false;
                memberid = PremiumServiceSearch.ServiceAdapters.SpotlightMemberSA.Instance.GetNextSpotlight(g.Brand, g.Member, searchIDs, services, g.AppPage.ID, resetSpotlightClientMetadataCache, out promoMemberFlag);
                if (memberid > 0)
                {
                    m = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                }

                return m;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// Saves the URL to be used for Back to Results on view profile page
        /// </summary>
        public static void SaveBackToResultsURL(ContextGlobal g)
        {
            SaveBackToResultsURL(g, "");
        }

        /// <summary>
        /// Saves the URL to be used for Back to Results on view profile page
        /// </summary>
        /// <param name="g"></param>
        /// <param name="url">Explicit URL to use instead of request's raw url, leave empty if you want to use request's raw url</param>
        public static void SaveBackToResultsURL(ContextGlobal g, string url)
        {
            if (g != null && g.AppPage != null)
            {
                string controlname = g.AppPage.ControlName;

                string uri = HttpContext.Current.Request.RawUrl;

                if (controlname.ToLower() == "default" && (uri.ToLower().IndexOf("siteid") >= 0 || uri.Trim().EndsWith("&") || HttpContext.Current.Request["v"] != null))
                    return;


                //remove navpoint and navvalue, and actioncall related params which have already been submitted to omniture.
                string rawurl = url;

                if (String.IsNullOrEmpty(rawurl))
                {
                    rawurl = HttpContext.Current.Request.RawUrl;
                    int questionMarkIndex = rawurl.IndexOf("?");
                    if (questionMarkIndex > 0)
                    {
                        string[] paramArray = rawurl.Substring(questionMarkIndex + 1).Split(new char[] { '&' });
                        StringBuilder param = new StringBuilder();
                        if (paramArray.Length > 0)
                        {
                            foreach (string s in paramArray)
                            {
                                if (!s.ToLower().StartsWith(WebConstants.URL_PARAMETER_NAME_OMNITURE_NAVPOINT.ToLower())
                                    && !s.ToLower().StartsWith(WebConstants.URL_PARAMETER_NAME_OMNITURE_NAVVALUE.ToLower())
                                    && !s.ToLower().StartsWith(WebConstants.ACTION_CALL.ToLower())
                                    && !s.ToLower().StartsWith(WebConstants.ACTION_CALL_PAGE.ToLower())
                                    && !s.ToLower().StartsWith(WebConstants.ACTION_CALL_PAGE_DETAIL.ToLower())
                                    && !s.ToLower().StartsWith(WebConstants.ACTION_LINK.ToLower()))
                                {
                                    param.Append("&" + s);
                                }
                            }

                            rawurl = rawurl.Substring(0, questionMarkIndex);
                            if (param.ToString().Length > 0)
                                rawurl += "?" + param.ToString().Substring(1);
                        }
                    }
                }

                //add back to result url to session
                // g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_BACKTORESULT_URL, rawurl, SessionPropertyLifetime.Temporary);
                AddCookie(WebConstants.SESSION_PROPERTY_NAME_BACKTORESULT_URL, rawurl, 24, true);
            }
        }

        public static string GetOmnitureActionTrackingLink(string url, WebConstants.PageIDs actionPage, WebConstants.Action action)
        {

            if (url.IndexOf("?") >= 0)
            { url = url + "&" + WebConstants.ACTION_CALL + "=" + actionPage; }
            else
            { url = url + "?" + WebConstants.ACTION_CALL + "=" + actionPage; }

            if (action != WebConstants.Action.None)
            {
                url = url + "&" + WebConstants.ACTION_LINK + "=" + action;
            }

            return url;
        }


        /// <summary>
        /// Gets the homepage absolute URL.
        /// </summary>
        /// <param name="g">ContextGlobal g.</param>
        /// <param name="httpHost">The HTTP host. The result of Request.ServerVariables.Get("HTTP_HOST")</param>
        /// <returns>Website's absolute url</returns>
        public static string GetHomepageAbsURL(ContextGlobal g, string httpHost)
        {
            string homepageURL = "http://www." + g.Brand.Uri.ToLower();
            httpHost = httpHost.ToLower();
            if (g.IsDevMode || httpHost.Contains("dev") || httpHost.Contains("stg") || httpHost.Contains("preprod"))
            {
                homepageURL = "http://" + httpHost;
            }

            if (g.Member != null && g.AppPage.ControlName.ToLower().IndexOf("logoff") == -1)
            {
                homepageURL += "/Applications/Home/Default.aspx";
            }

            return homepageURL;
        }

        public static void Trace(bool trace, string source, string msg, Object traceObj)
        {
            string format = "Source: {0}, Message: {1}, ObjectTrace: {2}";
            try
            {
                if (trace)
                {
                    string obj = (traceObj != null) ? traceObj.ToString() : "";
                    System.Diagnostics.Trace.Write(String.Format(format, source, msg, obj));
                }
            }
            catch (Exception ex)
            { ex = null; }
        }
        public static string GetRegistrationPageURL(ContextGlobal g)
        {
            string url = "/Applications/Registration/Registration.aspx";
            try
            {
                string destUrl = g.GetDestinationURL();
                if (!String.IsNullOrEmpty(destUrl) && destUrl.ToLower().IndexOf("destinationurl") < 0)
                {
                    destUrl = "?DestinationURL=" + HttpUtility.UrlEncode(destUrl);
                }

                url += destUrl;

                // JDIL on view profile page should load up the reg overlay registration wizard
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                {
                    if (HttpContext.Current.Request.UrlReferrer != null && HttpContext.Current.Request.UrlReferrer.AbsolutePath.ToLower() == "/applications/memberprofile/viewprofile.aspx"
                        && !HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Contains("regoverlay"))
                    {
                        url = HttpContext.Current.Request.UrlReferrer.PathAndQuery + "&regoverlay=true";
                    }
                    else
                    {
                        url = "/Applications/MembersOnline/MembersOnline.aspx?regoverlay=true&overlayreg=visitiorslimit" + g.GetDestinationURL();
                    }
                }

                return url;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return url;
            }
        }

        public static string GetRegistrationPageURL(ContextGlobal g, string urlparam)
        {
            string url = "/Applications/Registration/Registration.aspx";
            string delimiter = "?";
            try
            {
                if (!String.IsNullOrEmpty(urlparam))
                {
                    url += delimiter + urlparam;
                    delimiter = "&";
                }

                string destUrl = g.GetDestinationURL();
                if (!String.IsNullOrEmpty(destUrl))
                {

                    if (!String.IsNullOrEmpty(destUrl) && destUrl.ToLower().IndexOf("destinationurl") < 0)
                    {
                        destUrl = "DestinationURL=" + HttpUtility.UrlEncode(destUrl);
                    }
                    url += delimiter + destUrl;
                }


                return url;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return url;
            }
        }

        public static bool IfHadSubscription(ContextGlobal g)
        {
            try
            {
                Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(g.TargetMemberID, g.TargetBrand);
                if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0)
                {
                    return true;
                }
                else
                {
                    int memberSubscriptionStatus = (g.Member != null && g.Member.MemberID == g.TargetMemberID) ?
                                                        GetMemberSubcriptionStatus(g.Member, g.TargetBrand) :
                                                        GetMemberSubcriptionStatus(g.TargetMemberID, g.TargetBrand);
                    Matchnet.Purchase.ValueObjects.SubscriptionStatus enumSubscriptionStatus = (Matchnet.Purchase.ValueObjects.SubscriptionStatus)memberSubscriptionStatus;

                    if (enumSubscriptionStatus == Matchnet.Purchase.ValueObjects.SubscriptionStatus.PausedSubscriber)
                    {
                        // A member that has a frozen subscription did have a renewal 
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        public static bool IfPaidWithCC(ContextGlobal g)
        {
            try
            {
                bool returnValue = true;
                Spark.Common.PaymentProfileService.PaymentProfileInfo paymentProfileInfo = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(g.TargetMemberID, g.TargetBrand.Site.SiteID);
                if (paymentProfileInfo != null && !String.IsNullOrEmpty(paymentProfileInfo.PaymentProfileID))
                {
                    if (Convert.ToInt32(paymentProfileInfo.PaymentProfileType) != (int)Matchnet.Purchase.ValueObjects.PaymentType.CreditCard)
                    {
                        returnValue = false;
                    }
                }
                return returnValue;
            }

            catch (Exception ex)
            {
                return true;
            }
        }

        public static bool IfMemberHasSubscribed(ContextGlobal g)
        {
            if (g.Member == null)
                return false;
            else
                return g.TargetMemberID == g.Member.MemberID ? IfMemberHasSubscribed(g.Member, g.TargetBrand) : IfMemberHasSubscribed(g.TargetMemberID, g.TargetBrand);
        }

        public static bool IfMemberHasSubscribed(int targetMemberID, Brand targetBrand)
        {
            try
            {
                IMemberDTO objMember = MemberSA.Instance.GetMember(targetMemberID, MemberLoadFlags.None);
                DateTime dtSubendDate = objMember.GetAttributeDate(targetBrand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE, DateTime.MinValue);

                if (dtSubendDate == DateTime.MinValue)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool IfMemberHasSubscribed(IMemberDTO targetMember, Brand targetBrand)
        {
            try
            {
                DateTime dtSubendDate = targetMember.GetAttributeDate(targetBrand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE, DateTime.MinValue);

                if (dtSubendDate == DateTime.MinValue)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool IfHasPremiumService(ContextGlobal g)
        {
            try
            {
                ArrayList arrPremiumServices = PremiumServiceSA.Instance.GetMemberActivePremiumServices(g.Member, g.Brand);
                if (arrPremiumServices.Count > 1)
                {
                    return true;
                }
                else if (arrPremiumServices.Count == 1 && arrPremiumServices[0] != Spark.Common.AccessService.PrivilegeType.AllAccessEmails.ToString())
                {
                    //all access email by itself should not be considered having premium, since you can't use it without all access privilege
                    return true; 
                }
                
                return false;
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        public static DataTable GetAttributeOptionAsDataTable(string attributeName, bool useDescription, bool includeBlankValue, ContextGlobal g, FrameworkControl resourceControl)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Value", typeof(System.String));
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("ListOrder", typeof(System.Int32));


            AttributeOptionCollection attOptions = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

            foreach (AttributeOption attributeOption in attOptions)
            {
                DataRow row = dt.NewRow();

                if (attributeOption.Value != Constants.NULL_INT)
                {
                    row["Value"] = attributeOption.Value;
                }
                else
                {
                    // This means we got a blank value from the DB
                    if (includeBlankValue)
                    {
                        row["Value"] = string.Empty;
                    }
                    else
                    {
                        // Skip this one
                        row = null;
                        continue;
                    }
                }

                if (useDescription)
                {
                    row["Content"] = attributeOption.Description;
                }
                else
                {
                    row["Content"] = g.GetResource(attributeOption.ResourceKey, resourceControl);
                    //row["Content"] = g.GetResource(attributeOption.ResourceKey);
                }

                row["ListOrder"] = attributeOption.ListOrder;

                dt.Rows.Add(row);
            }

            return dt;
        }



        public static ListItemCollection GetAttributeOptionCollectionAsListItems(string attributeName, ContextGlobal g, FrameworkControl resourceControl, bool skipBlanks)
        {
            ListItemCollection items = new ListItemCollection();

            if (attributeName != Constants.NULL_STRING)
            {
                AttributeOptionCollection attOptions = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);



                foreach (AttributeOption option in attOptions)
                {
                    if (option.Value <= Constants.NULL_INT && skipBlanks)
                    {
                        continue;
                    }

                    ListItem item = new ListItem();
                    item.Value = option.Value.ToString();

                    if (resourceControl == null)
                        item.Text = g.GetTargetResource(option.ResourceKey, BrandConfigSA.Instance.GetBrandByID(g.Brand.BrandID));
                    //item.Text = g.GetResource(option.ResourceKey);
                    else
                        item.Text = g.GetResource(option.ResourceKey, resourceControl);
                    items.Add(item);
                    item = null;


                }
            }

            return items;
        }

        public static void PopulateSiteDDL(DropDownList ddl, ContextGlobal g)
        {
            try
            {

                Sites sites = BrandConfigSA.Instance.GetSites();
                ddl.Items.Clear();
                List<Site> slist = new List<Site>();

                foreach (Site s in sites)
                {

                    int siteid = s.SiteID;
                    int communityid = s.Community.CommunityID;
                    Brand brand = GetBrandForSite(siteid);

                    bool active = _settingsManager.GetSettingBool(SettingConstants.SITE_ACTIVE_FLAG, brand);
                    if (!active)
                        continue;
                    slist.Add(s);


                }
                slist.Sort(delegate(Site s1, Site s2)
                {
                    return s1.Name.CompareTo(s2.Name);
                });
                ddl.DataSource = slist;
                ddl.DataTextField = "Name";
                ddl.DataValueField = "SiteID";
                ddl.DataBind();
                //ddl.Items.Add(new ListItem(s.Name, s.SiteID.ToString()));
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public static void PopulateBrandDDL(DropDownList ddl, ContextGlobal g)
        {
            try
            {

                Sites sites = BrandConfigSA.Instance.GetSites();
                ddl.Items.Clear();
                List<Brand> blist = new List<Brand>();

                foreach (Site s in sites)
                {
                    Brand brand = GetBrandForSite(s.SiteID);

                    bool active = _settingsManager.GetSettingBool(SettingConstants.SITE_ACTIVE_FLAG, brand);
                    if (!active)
                        continue;

                    Brands brands = BrandConfigSA.Instance.GetBrandsBySite(s.SiteID);
                    foreach (Brand b in brands)
                    {
                        if (b.Uri.ToLower() == s.Name.ToLower())
                        {
                            blist.Add(b);
                            break;
                        }
                    }


                }
                blist.Sort(delegate(Brand b1, Brand b2)
                {
                    return b1.Site.Name.CompareTo(b2.Site.Name);
                });

                ddl.DataSource = blist;
                ddl.DataTextField = "Uri";
                ddl.DataValueField = "BrandID";
                ddl.DataBind();

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public static void PopulateBrandDDL(DropDownList ddl, ContextGlobal g, string blankText, string blankValue)
        {
            try
            {

                Sites sites = BrandConfigSA.Instance.GetSites();
                ddl.Items.Clear();
                List<Brand> blist = new List<Brand>();

                foreach (Site s in sites)
                {
                    Brand brand = GetBrandForSite(s.SiteID);

                    bool active = _settingsManager.GetSettingBool(SettingConstants.SITE_ACTIVE_FLAG, brand);
                    if (!active)
                        continue;

                    Brands brands = BrandConfigSA.Instance.GetBrandsBySite(s.SiteID);
                    foreach (Brand b in brands)
                    {
                        if (b.Uri.ToLower() == s.Name.ToLower())
                        {
                            blist.Add(b);
                            break;
                        }
                    }


                }

                blist.Sort(delegate(Brand b1, Brand b2)
                {
                    return b1.Site.Name.CompareTo(b2.Site.Name);
                });

                ddl.DataSource = blist;
                ddl.DataTextField = "Uri";
                ddl.DataValueField = "BrandID";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem(blankText, blankValue));

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public static bool MailAccessException(WebConstants.MailAccessArea mailArea, ContextGlobal g, IMemberDTO fromMember)
        {
            try
            {
                string setting = "";
                switch (mailArea)
                {
                    case WebConstants.MailAccessArea.mailBox:
                        setting = SettingConstants.MAILBOX_ACCESS_MASK;
                        break;
                    case WebConstants.MailAccessArea.viewMessage:
                        setting = SettingConstants.VIEWMSG_ACCESS_MASK;
                        break;

                    case WebConstants.MailAccessArea.compose:
                        setting = SettingConstants.COMPOSEMSG_ACCESS_MASK;
                        break;
                    case WebConstants.MailAccessArea.IMHistory:
                        return false;
                        break;
                }

                int exceptionMask = _settingsManager.GetSettingInt(setting, g.Brand);
                if (exceptionMask == (int)WebConstants.MailAccessExceptionRule.defaultRule)
                    return false;

                if (exceptionMask == (int)WebConstants.MailAccessExceptionRule.noRestriction)
                    return true;

                if ((exceptionMask & (int)WebConstants.MailAccessExceptionRule.defaultCountry) == (int)WebConstants.MailAccessExceptionRule.defaultCountry)
                {
                    IMemberDTO member =
                       Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                       MemberLoadFlags.None);

                    RegionLanguage region = RegionSA.Instance.RetrievePopulatedHierarchy(member.GetAttributeInt(g.Brand, "RegionID"), g.Brand.Site.LanguageID);

                    if (region.CountryRegionID == g.Brand.Site.DefaultRegionID)
                    {
                        if (mailArea == WebConstants.MailAccessArea.mailBox)
                            return true;
                        else
                        {
                            RegionLanguage fromRegion = RegionSA.Instance.RetrievePopulatedHierarchy(fromMember.GetAttributeInt(g.Brand, "RegionID"), g.Brand.Site.LanguageID);

                            if (!MemberPrivilegeAttr.IsPermitMember(g) && (fromRegion.CountryRegionID == g.Brand.Site.DefaultRegionID))
                            {
                                return true;
                            }
                        }
                    }




                }

                if ((exceptionMask & (int)WebConstants.MailAccessExceptionRule.defaultLanguage) == (int)WebConstants.MailAccessExceptionRule.defaultLanguage)
                {
                    if (mailArea == WebConstants.MailAccessArea.mailBox)
                        return FrameworkGlobals.recipientSpeaksSiteDefaultLanguage(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), g.Brand);
                    else
                    {
                        return FrameworkGlobals.senderSpeaksSiteDefaultLanguage(fromMember.MemberID, g.Brand);
                    }
                }
                return false;

            }
            catch (Exception ex)
            { g.ProcessException(ex); return false; }

        }


        public static int GetMaxPhotoCount(ContextGlobal g)
        {
            return _settingsManager.GetSettingInt(SettingConstants.MAX_PHOTO_COUNT, g.Brand);
        }

        public static string GetMorePhotoLink(int photoCount, ContextGlobal g, object resourceControl)
        {
            string resx = "TXT_VIEW_MEMBER_ARROWS";
            string linkText = "";
            try
            {
                linkText = g.GetResource(resx, resourceControl);
                switch (photoCount)
                {
                    case 0:
                    case 1:
                        return linkText;
                    default:
                        int morePhotos = photoCount - 1;

                        resx = morePhotos > 1 ? "TXT_VIEW_MEMBER_PHOTOS" : "TXT_VIEW_MEMBER_PHOTO";

                        linkText = g.GetResource(resx, resourceControl, new string[] { morePhotos.ToString() });
                        return linkText;

                }
            }
            catch (Exception ex)
            {
                return linkText;
            }
        }


        public static string GetMorePhotoLink(int photoCount, ContextGlobal g, object resourceControl, string resx)
        {

            string linkText = "";
            try
            {
                linkText = g.GetResource(resx, resourceControl);
                switch (photoCount)
                {
                    case 0:

                        return linkText;
                    default:
                        int morePhotos = photoCount - 1;

                        resx = morePhotos > 1 ? "TXT_VIEW_MEMBER_PHOTOS" : "TXT_VIEW_MEMBER_PHOTO";

                        linkText = g.GetResource(resx, resourceControl, new string[] { morePhotos.ToString() });
                        return linkText;

                }
            }
            catch (Exception ex)
            {
                return linkText;
            }
        }

        public static string GetMorePhotoLink(int photoCount, object resourceControl, string resx, ResourceManager resourceManager)
        {

            string linkText = "";
            try
            {
                linkText = resourceManager.GetResource(resx, resourceControl);
                switch (photoCount)
                {
                    case 0:
                    case 1:
                        return linkText;
                    default:
                        int morePhotos = photoCount - 1;

                        resx = morePhotos > 1 ? "TXT_VIEW_MEMBER_PHOTOS" : "TXT_VIEW_MEMBER_PHOTO";

                        linkText = resourceManager.GetResource(resx, resourceControl, new string[] { morePhotos.ToString() });
                        return linkText;

                }
            }
            catch (Exception ex)
            {
                return linkText;
            }
        }

        //public static List<Photo> GetApprovedPhotos(IMember _member, ContextGlobal g)
        //{
        //    List<Photo> approvedPhotos = new List<Photo>();
        //    try
        //    {
        //        PhotoCommunity photos = _member.GetPhotos(g.Brand.Site.Community.CommunityID);

        //        if (photos != null && photos.Count > 0)
        //        {

        //            for (byte i = 0; i < photos.Count; i++)
        //            {
        //                if (photos[i].IsApproved)
        //                    approvedPhotos.Add(photos[i]);
        //            }
        //        }
        //        return approvedPhotos;
        //    }
        //    catch (Exception ex)
        //    { return approvedPhotos; }

        //}

        //public static Photo GetRandomApprovedPhoto(IMember _member, ContextGlobal g)
        //{
        //    Photo photo = null;

        //    List<Photo> approvedPhotos = GetApprovedPhotos(_member, g);

        //    if (approvedPhotos.Count > 0)
        //    {
        //        Random random = new Random();
        //        photo = approvedPhotos[random.Next(0, approvedPhotos.Count - 1)];
        //    }

        //    return photo;
        //}

        public static int GetJewishReligionOnly(ContextGlobal g)
        {
            int jewishonly = 0;
            try
            {
                DataTable options = Matchnet.Web.Framework.Util.Option.GetOptions("jdatereligion", g);

                DataTable jewishOptions = options.Clone();
                foreach (DataRow row in options.Select(@"Value not in (2048, 4096, 16384) and Content not like '' ", "ListOrder asc"))
                {
                    int option = Conversion.CInt(row["Value"]);
                    jewishonly = jewishonly + option;
                }
                return jewishonly;

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return jewishonly;
            }



        }

        public static List<Site> GetSitesList(ContextGlobal g)
        {
            try
            {

                Sites sites = BrandConfigSA.Instance.GetSites();

                List<Site> slist = new List<Site>();

                foreach (Site s in sites)
                {
                    Brand brand = GetBrandForSite(s.SiteID);

                    bool active = _settingsManager.GetSettingBool(SettingConstants.SITE_ACTIVE_FLAG, brand);
                    if (!active)
                        continue;
                    slist.Add(s);


                }
                slist.Sort(delegate(Site s1, Site s2)
                {
                    return s1.Name.CompareTo(s2.Name);
                });

                return slist;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return null;
            }
        }

        public static Brand GetBrandForSite(int siteID)
        {
            Brands brands = BrandConfigSA.Instance.GetBrandsBySite(siteID);

            //default brand should have the smallest brandID
            Brand defaultBrand = null;
            foreach (Brand b in brands)
            {
                if (defaultBrand == null)
                {
                    defaultBrand = b;
                }
                else if (b.BrandID < defaultBrand.BrandID)
                {
                    defaultBrand = b;
                }
            }

            return defaultBrand;
        }


        public static string JavaScriptEncode(string jsArg)
        {
            return AntiXss.JavaScriptEncode(jsArg, false);
        }

        public static bool IsAutorenewOn(IMemberDTO member, ContextGlobal g)
        {
            bool returnValue = false;

            Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(member.MemberID, g.Brand);
            if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0)
            {
                returnValue = renewalSub.IsRenewalEnabled;
            }

            return returnValue;
        }

        /// <summary>
        /// This determines whether Unified Access is enabled for the site, used to determine whether we 
        /// read privileges from attributes or from Access svc.
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public static bool IsUPSAccessEnabled(int brandID, int siteID, int communityID)
        {
            return true;

            /*
             * Note: We're always using Access now.
             * 
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_ACCESS_ENABLED", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
             * */

        }

        /// <summary>
        /// This is a kill switch so Bedrock systems will stop reading Access data
        /// </summary>
        /// <returns></returns>
        public static bool IsUPSAccessMasterReadKillSwitchEnabled()
        {
            return false;

            /*
             * Note: We're always using Access now.
             * 
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_ACCESS_MASTER_READ_KILLSWITCH_ENABLED");
            }
            catch (Exception ex)
            {

                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
             * */

        }

        /// <summary>
        /// This setting determines whether Bedrock systems will update Access Service with changes to privileges (e.g. Admin Adjust)
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public static bool IsUPSAccessUpdateEnabled(int brandID, int siteID, int communityID)
        {
            return true;

            /*
             * Note: We're always using Access now.
             * 
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_ACCESS_UPDATE_PRIVILEGE_ENABLED", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {

                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
            **/
        }

        public static bool IsUPSRenewalEnabled(int brandID, int siteID, int communityID)
        {
            return true;

            /*
             * Note: We're always using UPS now.
             * 
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_RENEWAL_ENABLED", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
             * */

        }

        public static bool DisplayTopLargeSubscribeBanner(ContextGlobal g)
        {
            int enableSubMask = 1;
            bool isEnabled = false;

            if (g != null && g.Member != null && DisplayBannerArea(g))
            {
                try
                {
                    //Enables Subscribe Now Banner: 0 (off), 1 (show small banner), 2 (show large top banner)
                    enableSubMask = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SUBSCRIBE_NOW_BANNER_MASK", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                }
                catch (Exception ex)
                {
                    //setting missing
                    enableSubMask = 1;
                }

                if ((enableSubMask & 2) == 2)
                {
                    if (!g.Member.IsPayingMember(g.Brand.Site.SiteID))
                    {
                        isEnabled = true;

                        //showing new top banner on photo gallery
                        if (g.AppPage.PageName == "photogallery"
                            &&
                            Matchnet.Conversion.CBool(RuntimeSettings.GetSetting("USE_PHOTOGALLERY_30",
                                                                                 g.Brand.Site.Community.CommunityID,
                                                                                 g.Brand.Site.SiteID)))
                        {
                            return true;
                        }

                        //showing new top fat banner on homepage
                        if (g.AppPage.App.ID == (int)WebConstants.APP.Home
                            && Convert.ToBoolean((RuntimeSettings.GetSetting("ENABLE_SUBNOW_AD",
                                                                          g.Brand.Site.Community.CommunityID,
                                                                          g.Brand.Site.SiteID,
                                                                          g.Brand.BrandID))))
                        {
                            return true;
                        }

                        //do not show sub banner when member is viewing their own profile
                        if (Matchnet.Web.Applications.MemberProfile.ProfileTabs30.ProfileUtility.IsDisplayingProfile30(g))
                        {
                            isEnabled = (g.Member.MemberID != Matchnet.Web.Applications.MemberProfile.ViewProfileTabUtility.GetMemberIdForProfileView(g, true));
                        }

                        //Remove this after new top sub banner is used sitewide
                        if (isEnabled && !(Matchnet.Web.Applications.MemberProfile.ProfileTabs30.ProfileUtility.IsDisplayingProfile30(g))
                            && !(Matchnet.Web.Framework.BetaHelper.IsDisplayingSearchRedesign30(g))
                            && !(Matchnet.Web.Applications.Search.SearchUtil.IsDisplayingReverseSearchRedesign20(g)))
                        {
                            isEnabled = false;
                        }

                    }
                }
            }

            return isEnabled;
        }

        public static bool DisplaySmallSubscribeBanner(ContextGlobal g)
        {
            int enableSubMask = 1;
            bool isEnabled = false;

            if (g != null && g.Member != null && DisplayBannerArea(g))
            {
                try
                {
                    //Enables Subscribe Now Banner: 0 (off), 1 (show small banner), 2 (show large top banner)
                    enableSubMask = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SUBSCRIBE_NOW_BANNER_MASK", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                }
                catch (Exception ex)
                {
                    //setting missing
                    enableSubMask = 1;
                }

                if ((enableSubMask & 1) == 1)
                {
                    if (!g.Member.IsPayingMember(g.Brand.Site.SiteID))
                    {
                        isEnabled = true;


                        //do not show sub banner when member is viewing their own profile
                        if (Matchnet.Web.Applications.MemberProfile.ProfileTabs30.ProfileUtility.IsDisplayingProfile30(g))
                        {
                            isEnabled = (g.Member.MemberID != Matchnet.Web.Applications.MemberProfile.ViewProfileTabUtility.GetMemberIdForProfileView(g, true));
                        }
                        else if (HomeUtil.IsDisplayingHome40(g.Member, g.AppPage, g.Brand))
                        {
                            //do not show on new homepage 4.0
                            isEnabled = false;
                        }

                        //Remove this after new top sub banner is used sitewide
                        //Redesign - temp for now, don't show this on new any page that shows the new top sub now banner
                        if (isEnabled && DisplayTopLargeSubscribeBanner(g))
                        {
                            isEnabled = false;
                        }
                    }
                }
            }

            return isEnabled;

        }

        public static bool DisplayBannerArea(ContextGlobal g)
        {
            //This logic was taken from LayoutTemplateBase.cs in LoadBanner()
            //and contains various conditions on whether to display the subscribe now banner section

            if (g == null || g.Member == null)
                return false;

            // if the user is not subscribed and the page is not in the subscription area
            // TODO: Need to implement securitymask for apps..
            string defaultPagePath = g.AppPage.App.DefaultPagePath;
            if (defaultPagePath == null)
                defaultPagePath = string.Empty;

            //Issue #10963 -- subscribe now banner should not show up on reg pages
            string url = HttpContext.Current.Request.RawUrl;

            bool isRegistration = ((url.StartsWith("/Applications/MemberProfile/RegistrationStep")
                                        || url.StartsWith("/Applications/MemberProfile/RegistrationWelcome.aspx")
                                        || url.StartsWith("/Applications/MemberProfile/MemberPhotoUpload.aspx")));

            if (!(g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles && g.AppPage.App.ID == (int)WebConstants.APP.Home) && g.AppPage.App.ID != 3 //subscription
                    && g.AppPage.App.ID != 4 //PaymentProfile
                    && !(defaultPagePath.ToLower().IndexOf("/admin/") > 0)
                    && !isRegistration
                    && g.Brand.Site.Community.CommunityID != (int)WebConstants.COMMUNITY_ID.College
                    && !(url.ToLower().IndexOf("/framenav/") > 0)
                    && g.AppPage.App.ID != (int)WebConstants.APP.Logon
                    && !g.IsSiteMingle)
                return true;
            else
                return false;
        }

        //this method assumes that hex entities are separated by semicolon's
        public static string DecodeHexEntities(string hexStr)
        {
            if (string.IsNullOrEmpty(hexStr)) return hexStr;
            StringBuilder builder = new StringBuilder();
            string[] hexValuesSplit = HttpUtility.UrlDecode(hexStr).Split(';');
            int hexValuesSplitLen = hexValuesSplit.Length;
            if (hexValuesSplitLen > 1)
            {
                foreach (String hex in hexValuesSplit)
                {
                    try
                    {
                        // Convert the number expressed in base-16 to an integer.
                        int value = Convert.ToInt32(hex, 16);
                        // Get the character corresponding to the integral value.
                        builder.Append(Char.ConvertFromUtf32(value));
                    }
                    catch (Exception ex)
                    {
                        //ignore the exception for now and just append the string to the buffer
                        builder.Append(hex);
                    }
                }
                return builder.ToString();
            }
            else
            {
                return HttpUtility.UrlDecode(hexStr);
            }

        }

        public static DateTime ConvertUTCToPST(DateTime transactionDate)
        {
            TimeZoneInfo timeZoneInfoInPST = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            DateTime transactionInsertDateInPST = TimeZoneInfo.ConvertTimeFromUtc(transactionDate, timeZoneInfoInPST);

            return transactionInsertDateInPST;
        }

        public static bool IsRegOverlay()
        {
            if ((!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["regoverlay"]) && HttpContext.Current.Request.QueryString["regoverlay"] == "true")
            || HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Contains("regoverlay=true"))
            {
                return true;
            }
            return false;
        }

        public static bool IsAreaCodePreferenceEnabled(int brandID, int siteID, int communityID)
        {
            string isEnabled = "true";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_AREA_CODE_FOR_LOCATION_PREF", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "true";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        public static string CheckForFriendlyURL()
        {
            HttpContext Context = HttpContext.Current;
            string fullURL = HttpUtility.UrlDecode(Context.Request.Url.AbsoluteUri);
            string checkForFriendlyUrl = string.Empty;
            if (fullURL.Contains("?404;"))
                checkForFriendlyUrl = fullURL.Split(new string[] { "?404;" }, StringSplitOptions.None)[1];
            return checkForFriendlyUrl;
        }

        /// <summary>
        /// Use this method to get the current chat provider.
        /// CONNECT_IM_ENABLE - use of this setting is deprecated with the new setting.
        /// CONNECT_CHAT_ENABLE - this one stays intact for now as it is for chatrooms.
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        public static ChatProvider GetChatProvider(Brand brand)
        {
            return (ChatProvider)Enum.Parse(typeof(ChatProvider),
                RuntimeSettings.GetSetting("CHAT_PROVIDER", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
        }

        public static void AddCookie(string cookiename, string cookieval, int expirehours, bool encode)
        {
            AddCookie(cookiename, cookieval, expirehours, encode, null, null);
        }

        public static void AddCookie(string cookiename, string cookieval, int expirehours, bool encode, string domain, string path)
        {
            try
            {
                string val;
                if (encode)
                {
                    val = HttpUtility.UrlEncode(cookieval);
                }
                else
                {
                    val = cookieval;
                }

                HttpCookie cookie = new HttpCookie(cookiename, val);

                if (expirehours != Constants.NULL_INT)
                {
                    // If expirehours is set to Constants.NULL_INT, then the cookie will expired
                    // with the browser session  
                    cookie.Expires = DateTime.Now.AddHours(expirehours);
                }

                if (null != domain)
                {
                    cookie.Domain = domain;
                }

                if (null != path)
                {
                    cookie.Path = path;
                }

                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            catch (Exception ex)
            {
                Trace(true, "AddCookie", String.Format("Cannot add cookie - name:{0}, value:{1}, domain:{2}, path:{3}", cookiename, cookieval, domain, path), ex);
            }
        }

        public static void RemoveCookie(string cookiename)
        {
            RemoveCookie(cookiename, null, null);
        }

        public static void RemoveCookie(string cookiename, string domain, string path)
        {
            try
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(cookiename);

                if (cookie == null)
                    return;

                cookie.Expires = DateTime.Now.AddDays(-1);
                if (null != domain) cookie.Domain = domain;
                if (null != path) cookie.Path = path;
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            catch (Exception ex)
            {
                Trace(true, "RemoveCookie", String.Format("Cannot remove cookie - name:{0}, domain:{1}, path:{2}", cookiename, domain, path), ex);
            }
        }

        public static string GetTopLevelDomain(ContextGlobal g)
        {
            try
            {
                string topLevelDomain = "." + g.Brand.Uri.ToLower();
                return topLevelDomain;
            }
            catch (Exception e)
            {
                g.ProcessException(e);
                return HttpContext.Current.Request.Url.Host;
            }
        }

        public static void RemoveMultiPartCookie(string cookieName)
        {
            for (int i = 1; i < (WebConstants.COOKIE_MULTI_PART + 1); i++)
            {
                FrameworkGlobals.RemoveCookie(cookieName + i.ToString());
            }
        }

        /// <summary>
        /// Method added to support storing Promo VO for admin. Shouldn't be storing large values for normal users..
        /// </summary>
        /// <param name="cookieName"></param>
        /// <param name="cookieVal"></param>
        /// <param name="expireHours"></param>
        public static void AddMultiPartCookie(string cookieName, string cookieVal, int expireHours, bool encode)
        {
            int chunk = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cookieVal.Length) / Convert.ToDouble(WebConstants.COOKIE_MULTI_PART)));

            for (int i = 0; i < WebConstants.COOKIE_MULTI_PART; i++)
            {
                string value = "";

                if (i == (WebConstants.COOKIE_MULTI_PART - 1))
                {
                    value = cookieVal.Substring(chunk * i);
                }
                else
                {
                    value = cookieVal.Substring(chunk * i, chunk);
                }

                FrameworkGlobals.AddCookie(cookieName + (i + 1).ToString(), value, expireHours, encode);
            }
        }

        public static string GetMultiPartCookie(string cookieName, string defaultValue, bool encode)
        {
            string value = "";

            for (int i = 1; i < (WebConstants.COOKIE_MULTI_PART + 1); i++)
            {
                value += FrameworkGlobals.GetCookie(cookieName + i.ToString(), defaultValue, encode);
            }

            return value;
        }

        public static string GetCookie(string cookiename, string defaultVal, bool decode)
        {
            string cookieVal = defaultVal;
            try
            {

                HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(cookiename);
                if (cookie != null)
                {
                    if (decode)
                        cookieVal = HttpUtility.UrlDecode(cookie.Value);
                    else
                        cookieVal = cookie.Value;

                }
                return cookieVal;


            }
            catch (Exception ex)
            {
                return cookieVal;
            }
        }

        public static bool SearchResultsM2MUpgradeEnabled(ContextGlobal g)
        {
            if (g.Member == null)
                return false;
            FeatureThrottler ft = new FeatureThrottler(ThrottleMode.LastDigit, "ENABLE_SEARCH_RESULTS_M2M_UPGRADE",
                                                            g.Brand, g.Member);
            return ft.IsFeatureEnabled();
        }
        
    }
}
