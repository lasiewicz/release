using System;
using System.Configuration;
using System.Web;
using Matchnet.Caching.CacheBuster;
using Matchnet.Caching.CacheBuster.HTTPContextWrappers;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Lib.Encryption;
using Matchnet.Web.Framework.Managers;
using Spark.Authentication.OAuth;
using Spark.Authentication.OAuth.V2;

namespace Matchnet.Web.Framework
{
	/// <summary>
	/// This class is initiated in ContextGlobal. This is a site based helper to enable auto logging.
	/// 
	/// Use a cookie viewing/editing tool (FireFix/Edit Cookies add on) for testing.
	/// TraceView will print events.
	/// </summary>
	public sealed class AutoLoginHelper : FrameworkControl 
	{
		#region Private Constants

		/// TODO: Probably not the best place to store the key..
		private const string DES_KEY = "w0nTw033";

		// Cookie that's currently being saved on logon
		private const string COOKIE_KEY_EMAIL_ADDRESS = "al-juso";
		private const string COOKIE_KEY_PASSWORD= "al-amho";

		// Database, mnSystem..Setting name
		private const string SETTING_VALUE = "AutomaticLogin";

		#endregion

		#region Private Variables

		// This is used for analytics only for now.
		private AutoLoginStatus _autoLoginStatus;

        private static string _tokenCookieEnvironment;

        
        #endregion

		#region Public Properties

		public AutoLoginStatus Status
		{
			get
			{
				return _autoLoginStatus; 
			}
			set
			{
				_autoLoginStatus = value;
				g.Session.Add("AutoLoginStatus", _autoLoginStatus.ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
			}
		}

		#endregion

		#region Public Enums

		public enum AutoLoginStatus
		{
			NormalLoggedIn,
			AutoLoggedIn,
			NewAutoLoggedIn
		}

		#endregion

		#region Public Constructors

		public AutoLoginHelper()
		{
		}


		#endregion

		#region Public Methods

		/// <summary>
		/// Autologins the user with stored cookie(emailaddress & password) values.
		/// </summary>
		/// <returns>True if auto logged in.</returns>
		public bool AutoLogin()
		{
            try
            {// No need to authenticate w/o these values.	
               
                if ((HttpContext.Current.Request.Cookies.Get(COOKIE_KEY_PASSWORD) == null) ||
                    (HttpContext.Current.Request.Cookies.Get(COOKIE_KEY_EMAIL_ADDRESS) == null))
                {
                    ExpireAutoLogin();
                    return false;
                }

                if (RequiresHardLogin())
                {
                    ExpireAutoLogin();
                    return false;
                }

                // Retrieve the email address stored in user's cookie.
                var emailAddress = Decrypt(HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies.Get(COOKIE_KEY_EMAIL_ADDRESS).Value));

                // Retrieve the encrypted password from the user's cookie.
                var newEncryptedPassword = HttpUtility.UrlDecode((HttpContext.Current.Request.Cookies.Get(COOKIE_KEY_PASSWORD).Value));
                CurrentRequest currentRequest = new CurrentRequest();
                CurrentServer currentServer = new CurrentServer();
                var ipConverter = new IPConverter();
                string ipAddress = currentRequest.ClientIP;
                g.LoggingManager.LogInfoMessage("AutoLoginHelper", "Email : " + emailAddress + " IPAddress : " + ipAddress);
                var authResult = MemberSA.Instance.Authenticate(g.Brand, emailAddress, Constants.NULL_STRING, newEncryptedPassword);

                switch (authResult.Status)
                {
                    case AuthenticationStatus.Authenticated:
                        g.StartLogonSession(authResult.MemberID, g.Brand.Site.Community.CommunityID, g.Brand.BrandID,
                            emailAddress, authResult.UserName, false, Constants.NULL_INT);
                        Status = AutoLoginStatus.AutoLoggedIn;

                        if (Convert.ToBoolean(RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                        {
                            // Tracking Omniture Traffic Variables on successful login.
                            g.Session.Add("OmnitureLoggedIn", "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                            g.Session.Add("OmnitureAutoLoggedIn", "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                        }

                        FrameworkGlobals.SetConnectCookie(g);
                        var IMOAuthEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("SET_IM_OAUTH_COOKIES",
                                                                        g.Brand.Site.Community.CommunityID,
                                                                        g.Brand.Site.SiteID, g.Brand.BrandID));
                        if (IMOAuthEnabled)
                        {
                            if (_tokenCookieEnvironment == null)
                            {
                                _tokenCookieEnvironment = SettingsManager.GetTokenEnvironmentForCookie();
                            }
                            LoginManager.Instance.FetchTokensAndSaveToCookies(emailAddress, newEncryptedPassword,
                                                                                true, true, _tokenCookieEnvironment,
                                                                                g.Brand.Site.Name.ToLower(), g.Brand.BrandID);
                        }

                        //System.Diagnostics.Trace.WriteLine("__AutoLogin: Logged in sucessfully. [EmailAddress=" + emailAddress + "]");
                        return true;
                    default:
                        // Remove the values to prevent unnecessary authentication next time.
                        ExpireAutoLogin();
                        return false;
                }
            }
            catch (Exception ex)
            {
                ExpireAutoLogin();
                return false;
            }
		}


		/// <summary>
		/// Deletes the password in the user's cookie thus invalidating auto login the next time.
		/// This is called by Logout function.
		/// 
		/// http://msdn2.microsoft.com/en-us/library/ms178195.aspx
		/// </summary>
		public void ExpireAutoLogin()
        {
            try
            {
                HttpContext.Current.Response.Cookies.Remove(COOKIE_KEY_PASSWORD);
                HttpContext.Current.Response.Cookies[COOKIE_KEY_PASSWORD].Expires = DateTime.Now.AddDays(-1D);
                HttpContext.Current.Response.Cookies.Remove(COOKIE_KEY_EMAIL_ADDRESS);
                HttpContext.Current.Response.Cookies[COOKIE_KEY_EMAIL_ADDRESS].Expires = DateTime.Now.AddDays(-1D);
            }
            catch
            { }
			//System.Diagnostics.Trace.WriteLine("__AutoLogin: Expired cookies.");
		}


	    /// <summary>
	    /// Create the user's password and emailaddress cookies and saves. Note that it is saving to a different cookie than g.Session is using.
	    /// </summary>
	    /// <param name="emailAddress"></param>
	    /// <param name="password"></param>
	    public string CreateAutoLogin(string emailAddress, string password)
		{
            var encryptedEmailAddress = Encrypt(emailAddress);
            var encryptedPassword = MemberSA.Instance.GetPasswordForAuth(emailAddress, g.Brand.Site.Community.CommunityID, password);

		    // Session service does not have support for indivisual cookie expiration.
			HttpCookie cookiePassword = new HttpCookie(COOKIE_KEY_PASSWORD, HttpUtility.UrlEncode(encryptedPassword));
			HttpCookie cookieEmailAddress = new HttpCookie(COOKIE_KEY_EMAIL_ADDRESS, HttpUtility.UrlEncode(encryptedEmailAddress));

			cookiePassword.Expires = DateTime.Now.AddDays((Matchnet.Conversion.CInt(RuntimeSettings.GetSetting(AutoLoginHelper.SETTING_VALUE, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID))));
			cookieEmailAddress.Expires = DateTime.Now.AddDays((Matchnet.Conversion.CInt(RuntimeSettings.GetSetting(AutoLoginHelper.SETTING_VALUE, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID))));

			HttpContext.Current.Response.Cookies.Add(cookiePassword);
			HttpContext.Current.Response.Cookies.Add(cookieEmailAddress);

			Status = AutoLoginStatus.NewAutoLoggedIn;
		    return encryptedPassword;

		    //System.Diagnostics.Trace.WriteLine("__AutoLogin: Created new cookies.  [EmailAddress=" + cookieEmailAddress.Value + " Password=" + cookiePassword.Value + " Expires=" + cookiePassword.Expires.ToString("g") + "]");
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// When only session is missing then it will redirect to the log on page.
		/// When both session & email address are missing in g.Session (mnc5 in cookie) then the framework will redirect to reg.
		/// </summary>
		/// <returns>Returns true if the current page requires a hard/manual login based on the page's security settings.</returns>
		private bool RequiresHardLogin()
		{
			if ((g.AppPage.SecurityMask & Matchnet.Content.ValueObjects.PageConfig.SecurityMask.RequiresHardLogin)
				== Matchnet.Content.ValueObjects.PageConfig.SecurityMask.RequiresHardLogin)
			{
				g.Session.Add("HardLoginRequired",  "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
				//System.Diagnostics.Trace.WriteLine("__AutoLogin: Site page requires hard login.  Redirecting. [Control="  + g.AppPage.ControlName + "]");

				return true;
			}

			//System.Diagnostics.Trace.WriteLine("__AutoLogin: Site page does not require hard login. [Control=" + g.AppPage.ControlName + "]");

			return false;
		}


		private string Encrypt(string unEncryptedvalue)
		{
			SymmetricalEncryption encryption = new SymmetricalEncryption(SymmetricalEncryption.Provider.DES);

			return encryption.Encrypt(unEncryptedvalue, DES_KEY);
		}


		private string Decrypt(string encryptedValue)
		{
			SymmetricalEncryption encryption = new SymmetricalEncryption(SymmetricalEncryption.Provider.DES);

			return encryption.Decrypt(encryptedValue, DES_KEY);
		}


		#endregion

		#region Public Static Methods

		/// <summary>
		/// Use this static method as a shorcut to determine if this feature is turned on.
		/// </summary>
		/// <returns></returns>
		public static bool IsSettingEnabled(ContextGlobal g)
		{
			if (Matchnet.Conversion.CInt(RuntimeSettings.GetSetting(AutoLoginHelper.SETTING_VALUE, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		#endregion
	}
}
