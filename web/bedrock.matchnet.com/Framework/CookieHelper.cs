﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework
{
    public class CookieHelper
    {
        private const string DONT_GO_COOKIE_NAME = "dontgo";
        
        public void AddDontGoCookie()
        {
            AddCookie(DONT_GO_COOKIE_NAME, "1", 48, true);
        }

        public bool DoesDontGoCookieExist()
        {
            return DoesCookieExist(DONT_GO_COOKIE_NAME);
        }

        public void RemoveDontGoCookie()
        {
            RemoveCookie(DONT_GO_COOKIE_NAME);
        }
        
        public bool DoesCookieExist(string cookiename)
        {
            bool exists = false;
            HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(DONT_GO_COOKIE_NAME);
            if (cookie != null) exists = true;
            return exists;
        }

        public void AddCookie(string cookiename, string cookieval, int expirehours, bool encode)
        {
            AddCookie(cookiename, cookieval, expirehours, encode, null, null);
        }
        
        public void AddCookie(string cookiename, string cookieval, int expirehours, bool encode, string domain, string path)
        {
            try
            {
                string val;
                if (encode)
                {
                    val = HttpUtility.UrlEncode(cookieval);
                }
                else
                {
                    val = cookieval;
                }

                HttpCookie cookie = new HttpCookie(cookiename, val);

                if (expirehours != Constants.NULL_INT)
                {
                    // If expirehours is set to Constants.NULL_INT, then the cookie will expired
                    // with the browser session  
                    cookie.Expires = DateTime.Now.AddHours(expirehours);
                }

                if (null != domain)
                {
                    cookie.Domain = domain;
                }

                if (null != path)
                {
                    cookie.Path = path;
                }

                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            catch (Exception ex)
            {
                FrameworkGlobals.Trace(true, "CookieHelper.AddCookie", String.Format("Cannot add cookie - name:{0}, value:{1}, domain:{2}, path:{3}", cookiename, cookieval, domain, path), ex);
            }
        }

        public void RemoveCookie(string cookiename)
        {
            try
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(cookiename);

                if (cookie == null)
                    return;

                cookie.Expires = DateTime.Now.AddDays(-1);

                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            catch (Exception ex)
            {
            }
        }

        public void RemoveMultiPartCookie(string cookieName)
        {
            for (int i = 1; i < (WebConstants.COOKIE_MULTI_PART + 1); i++)
            {
                FrameworkGlobals.RemoveCookie(cookieName + i.ToString());
            }
        }

        /// <summary>
        /// Method added to support storing Promo VO for admin. Shouldn't be storing large values for normal users..
        /// </summary>
        /// <param name="cookieName"></param>
        /// <param name="cookieVal"></param>
        /// <param name="expireHours"></param>
        public void AddMultiPartCookie(string cookieName, string cookieVal, int expireHours, bool encode)
        {
            int chunk = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cookieVal.Length) / Convert.ToDouble(WebConstants.COOKIE_MULTI_PART)));

            for (int i = 0; i < WebConstants.COOKIE_MULTI_PART; i++)
            {
                string value = "";

                if (i == (WebConstants.COOKIE_MULTI_PART - 1))
                {
                    value = cookieVal.Substring(chunk * i);
                }
                else
                {
                    value = cookieVal.Substring(chunk * i, chunk);
                }

                FrameworkGlobals.AddCookie(cookieName + (i + 1).ToString(), value, expireHours, encode);
            }
        }

        public string GetMultiPartCookie(string cookieName, string defaultValue, bool encode)
        {
            string value = "";

            for (int i = 1; i < (WebConstants.COOKIE_MULTI_PART + 1); i++)
            {
                value += FrameworkGlobals.GetCookie(cookieName + i.ToString(), defaultValue, encode);
            }

            return value;
        }

        public string GetCookie(string cookiename, string defaultVal, bool decode)
        {
            string cookieVal = defaultVal;
            try
            {

                HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(cookiename);
                if (cookie != null)
                {
                    if (decode)
                        cookieVal = HttpUtility.UrlDecode(cookie.Value);
                    else
                        cookieVal = cookie.Value;

                }
                return cookieVal;


            }
            catch (Exception ex)
            {
                return cookieVal;
            }
        }

    }
}