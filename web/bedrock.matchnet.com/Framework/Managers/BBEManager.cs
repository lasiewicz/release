﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Interfaces;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Applications.Events;
using Matchnet.Member.ServiceAdapters.Interfaces;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ExternalMail.ServiceAdapters;

namespace Matchnet.Web.Framework.Managers
{
    /// <summary>
    /// Manages Black Board Eats processes
    /// </summary>
    public class BBEManager : AbstractManagerBaseClass
    {
        private ISaveMember _memberSaveService;
        private IExternalMailSA _externalMail;

        #region Singleton manager
        private static readonly BBEManager _Instance = new BBEManager();
        private BBEManager()
        {
        }
        public static BBEManager Instance
        {
            get { return _Instance; }
        }

        #endregion

        #region Properties
        public ISaveMember MemberSaveService
        {
            get { return _memberSaveService ?? MemberSA.Instance; }
            set { _memberSaveService = value; }
        }

        public IExternalMailSA ExternalMail
        {
            get { return _externalMail ?? (_externalMail = ExternalMailSA.Instance); }
            set { _externalMail = value; }
        }
        #endregion

        #region Public Methods
        public bool IsBlackBoardEatsPromotionEnabled(Brand brand)
        {
            //DB Setting
            bool returnValue = false;
            try
            {
                returnValue = SettingsManager.GetSettingBool("ENABLE_BLACK_BOARD_EATS_PROMOTION", brand);
            }
            catch (Exception ex)
            {
                //setting missing
            }

            return returnValue;
        }

        public BBE GetMemberDataBBE(IMember member, Brand brand)
        {
            BBE bbe = null;
            if (member != null)
            {
                string bbeXML = member.GetAttributeText(brand, "BlackBoardEatsPromo1");
                if (!string.IsNullOrEmpty(bbeXML))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(BBE));
                    StringReader sr = new StringReader(bbeXML);
                    bbe = serializer.Deserialize(sr) as BBE;
                }
            }

            return bbe;
        }

        public DateTime GetBBEPromotionExpirationDate(Brand brand)
        {
            DateTime expires = DateTime.Now;
            try
            {
                expires = Convert.ToDateTime(SettingsManager.GetSettingString("BBE_PROMOTION_EXPIRATION", brand));
            }
            catch (Exception ex)
            {
                //setting missing
            }

            return expires;
        }

        public BBE ProcessBBEPasscodeRequest(IMember member, Brand brand, string emailAddress)
        {
            return ProcessBBEPasscodeRequest(member, brand, emailAddress, null);

        }

        public BBE ProcessBBEPasscodeRequest(IMember member, Brand brand, string emailAddress, BBE bbe)
        {
            bool requiresSave = false;
            if (member != null)
            {
                if (bbe == null)
                {
                    bbe = GetMemberDataBBE(member, brand);
                }

                DateTime nowDate = DateTime.Now;
                if (bbe == null)
                {
                    bbe = new BBE();
                    bbe.ViewedPromoDate = nowDate.ToString();
                    requiresSave = true;
                }

                if (string.IsNullOrEmpty(bbe.ViewedPromoDate))
                {
                    bbe.ViewedPromoDate = nowDate.ToString();
                    requiresSave = true;
                }

                if (string.IsNullOrEmpty(bbe.PasscodeRequestDate))
                {
                    bbe.PasscodeRequestDate = nowDate.ToString();
                    requiresSave = true;
                }

                if (string.IsNullOrEmpty(bbe.Passcode))
                {
                    if (GetBBEPromotionExpirationDate(brand) > nowDate)
                    {
                        //generate passcode
                        string passcodePrefix = "AGO";// Test 1: "GUSTO", Test 2: "AGO";
                        int passcodeSequenceID = KeySA.Instance.GetKey("BBEPasscodeID");
                        bbe.Passcode = passcodePrefix + passcodeSequenceID.ToString();

                        //send email confirmation
                        try
                        {
                            ExternalMail.SendBBEPasscodeEmail(member.MemberID, brand.BrandID, emailAddress, bbe.Passcode);
                        }
                        catch (Exception e)
                        {
                            //do nothing, email issues should not prevent page from showing passcode
                            this.LoggingManager.LogException(e, member, brand, "BBEManager.ProcessBBEPasscodeRequest()");
                        }

                        requiresSave = true;
                    }
                }

                if (requiresSave)
                {
                    SaveMemberDataBBE(member, brand, bbe);
                }
            }

            return bbe;
        }

        public void SaveMemberDataBBE(IMember member, Brand brand, BBE bbe)
        {
            if (member != null && bbe != null)
            {
                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                XmlSerializer serializer = new XmlSerializer(typeof(BBE));
                serializer.Serialize(sw, bbe, ns);
                member.SetAttributeText(brand, "BlackBoardEatsPromo1", sb.ToString(), Member.ValueObjects.TextStatusType.Auto);
                MemberSaveService.SaveMember(member);
            }
        }

        #endregion
    }
}