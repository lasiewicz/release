﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Web.Framework.Managers
{
    public class SubscriberManager : AbstractManagerBaseClass
    {
        #region Singleton manager
        private static readonly SubscriberManager _Instance = new SubscriberManager();
        private SubscriberManager()
        {
        }
        public static SubscriberManager Instance
        {
            get { return _Instance; }
        }

        #endregion

        /// <summary>
        /// Determines if member is a current IAP subscriber
        /// </summary>
        /// <param name="member"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public bool IsIAPSubscriber(IMember member, Brand brand)
        {
            //return HttpContext.Current.Request.QueryString["hasIAP"] != null ? true : false;

            bool isIAPSubscriber = false;
            try
            {
                if (member.IsPayingMember(brand.Site.SiteID))
                {
                    if (!string.IsNullOrEmpty(member.GetAttributeText(brand, "IOS_IAP_Receipt_Original_TransactionID")))
                    {
                        isIAPSubscriber = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //attribute likely doesn't exist for this site/community
                LoggingManager.LogInfoMessage("SubscriberManager: ", "IsIAPSubscriber() issue: " + ex.Message);
            }

            return isIAPSubscriber;
        }

        /// <summary>
        /// returns the proper IAP redirect url based on the setting. 
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        public string GetJumpPageRedirect(Brand brand)
        {
            return SettingsManager.GetSettingString("IAP_SUBSCRIPTION_JUMPPAGE_REDIRECT_URL", brand, "");
        }

        public string GetKountMerchantID(Brand brand)
        {
            string merchantID = "";
            try
            {
                merchantID = SettingsManager.GetSettingString("KOUNT_MERCHANT_ID", brand);
            }
            catch (Exception ex)
            {
                //setting probably doesn't exist
            }

            return merchantID;
        }
        /// <summary>
        /// returns the gender string
        /// </summary>
        /// <param name="genderMask"></param>
        /// <returns></returns>
        public string GetGender(int genderMask)
        {
            genderMask = genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE;
            string gender = (genderMask == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) ? "Female" : "Male";
            return gender;
        }

    }
}