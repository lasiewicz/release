﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Interfaces;
using Spark.FacebookLike.ValueObjects;
using Spark.FacebookLike.ValueObjects.ServiceDefinitions;

namespace Matchnet.Web.Framework.Managers
{
    /// <summary>
    /// Manages facebook related processes
    /// </summary>
    public class FacebookManager : AbstractManagerBaseClass
    {
        public enum FacebookConnectStatus : int
        {
            None = 0,
            NonConnected = 1,
            ConnectedNoFBData = 2,
            ConnectedNoSavedData = 3,
            ConnectedShowData = 4,
            ConnectedHideData = 5
        }

        public const int MAX_BADGE_LIKES = 4;

        private ISaveMember _memberSaveService;
        private IFacebookLikeSA _facebookLikeService = null;

        #region Singleton manager
        private static readonly FacebookManager _Instance = new FacebookManager();
        private FacebookManager()
        {
        }
        public static FacebookManager Instance
        {
            get { return _Instance; }
        }

        #endregion

        #region Properties
        public ISaveMember MemberSaveService
        {
            get { return _memberSaveService ?? MemberSA.Instance; }
            set { _memberSaveService = value; }
        }

        public IFacebookLikeSA FacebookLikeService
        {
            get { return _facebookLikeService ?? Spark.FacebookLike.ServiceAdapters.FacebookLikeServiceSA.Instance; }
            set { _facebookLikeService = value; }
        }
        #endregion

        #region Public Methods
        public bool HasMemberSeenFacebookPrompt(IMember member, Brand brand)
        {
            //prompt should only be shown once and only for members who has never connected to facebook
            if (member != null)
            {
                DateTime promptDate = member.GetAttributeDate(brand, "FacebookFeaturePromptDate", DateTime.MinValue);

                if (promptDate != DateTime.MinValue)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public bool IsFacebookLikesInterestsDataEnabled(Brand brand)
        {
            bool isEnabled = false;
            try
            {
                isEnabled = SettingsManager.GetSettingBool("ENABLE_FACEBOOK_PROFILE_LIKES_INTERESTS_DATA", brand);
            }
            catch (Exception ex)
            {
                //missing setting
            }

            return isEnabled;
        }

        public int GetRandomFacebookLikeCount(Brand b)
        {
            int maxFBLikesForBadge = MAX_BADGE_LIKES;
            try
            {
                int settingInt = SettingsManager.GetSettingInt("MAX_FB_BADGE_LIKES", b);
                if (settingInt > Constants.NULL_INT)
                {
                    maxFBLikesForBadge = settingInt;
                }
            }
            catch (Exception noSetting)
            {
                this.LoggingManager.LogException(noSetting, null, b, "FacebookManager.GetRandomFacebookLikeCount()");
                maxFBLikesForBadge = MAX_BADGE_LIKES;
            }
            return maxFBLikesForBadge;
        }

        public FacebookConnectStatus GetFacebookConnectStatus(IMemberDTO member, Brand brand)
        {
            FacebookConnectStatus fbConnectStatus = FacebookConnectStatus.NonConnected;
            if (member != null)
            {
                //change later to grab from attribute
                //if (HasMemberSeenFacebookPrompt(member, brand))
                //{
                //    fbConnectStatus = FacebookConnectStatus.ConnectedNoSavedData;
                //}

                int connectStatusID = member.GetAttributeInt(brand, "FacebookConnectStatusID", 0);
                if (connectStatusID > 0)
                {
                    try
                    {
                        fbConnectStatus = (FacebookConnectStatus) Enum.Parse(typeof(FacebookConnectStatus), connectStatusID.ToString());
                    }
                    catch (Exception ex)
                    {
                        //invalid connectStatusID
                        System.Diagnostics.Trace.WriteLine("GetFacebookConnectStatus(" + member.MemberID.ToString() + ", " + brand.Site.SiteID.ToString() + "), ConnectStatusID: " + connectStatusID.ToString() + ", Error: " + ex.Message);
                    }
                }

            }

            return fbConnectStatus;
        }

        public void UpdateFacebookConnect(IMember member, Brand brand, string fbUserID, string accessToken, int expiresInSeconds)
        {
            if (member != null && brand != null)
            {
                //calculate token expiration date
                DateTime tokenExpirationDate = DateTime.Now.AddSeconds(expiresInSeconds);
                if (member.GetAttributeDate(brand, "FacebookConnectDate", DateTime.MinValue) <= DateTime.MinValue)
                {
                    member.SetAttributeDate(brand, "FacebookConnectDate", tokenExpirationDate);
                }
                member.SetAttributeDate(brand, "FacebookLastConnectDate", DateTime.Now);
                member.SetAttributeDate(brand, "FacebookUserAccessTokenExpirationDate", tokenExpirationDate);
                member.SetAttributeText(brand, "FacebookUserAccessToken", accessToken, Member.ValueObjects.TextStatusType.Auto);
                member.SetAttributeText(brand, "FacebookUserID", fbUserID, Member.ValueObjects.TextStatusType.Auto);
                FacebookConnectStatus fbConnectStatus = GetFacebookConnectStatus(member.GetIMemberDTO(), brand);
                //TODO - check if member has saved data
                if (fbConnectStatus == FacebookConnectStatus.None || fbConnectStatus == FacebookConnectStatus.NonConnected)
                {
                    fbConnectStatus = FacebookConnectStatus.ConnectedNoSavedData;
                }
                member.SetAttributeInt(brand, "FacebookConnectStatusID", (int)fbConnectStatus);
                
                MemberSaveService.SaveMember(member);
            }
        }

        public FacebookLikeCategoryGroupList GetFacebookLikesCategoryGroupList(Brand brand)
        {
            return FacebookLikeService.GetFacebookLikeCategoryGroupList();
        }

        public FacebookLikeSaveResult SaveFacebookLikes(List<FacebookLikeParams> facebookLikeParamList, IMember member, Brand brand)
        {
            FacebookLikeSaveResult fbResult = FacebookLikeService.ProcessFacebookLikes(facebookLikeParamList);

            if (fbResult.SaveStatus == FacebookLikeSaveStatusType.Success || fbResult.SaveStatus == FacebookLikeSaveStatusType.PartialSuccess)
            {
                FacebookConnectStatus fbConnectStatus = GetFacebookConnectStatus(member.GetIMemberDTO(), brand);
                //TODO - check if member has saved data
                if (fbConnectStatus != FacebookConnectStatus.ConnectedShowData)
                {
                    fbConnectStatus = FacebookConnectStatus.ConnectedHideData;
                    member.SetAttributeInt(brand, "FacebookConnectStatusID", (int)fbConnectStatus);
                    MemberSaveService.SaveMember(member);
                }
            }

            return fbResult;
        }

        public FacebookLikeSaveResult ClearFacebookLikes(IMember member, Brand brand)
        {
            FacebookLikeParams fbLikeParams = new FacebookLikeParams();
            fbLikeParams.MemberId = member.MemberID;
            fbLikeParams.SiteId = brand.Site.SiteID;
            FacebookLikeSaveResult fbResult = FacebookLikeService.RemoveAllFacebookLikes(fbLikeParams);

            if (fbResult.SaveStatus == FacebookLikeSaveStatusType.Success)
            {
                FacebookConnectStatus fbConnectStatus = GetFacebookConnectStatus(member.GetIMemberDTO(), brand);
                if (fbConnectStatus != FacebookConnectStatus.ConnectedNoSavedData)
                {
                    member.SetAttributeInt(brand, "FacebookConnectStatusID", (int)FacebookConnectStatus.ConnectedNoSavedData);
                    MemberSaveService.SaveMember(member);
                }
            }

            return fbResult;
        }

        public FacebookLikeList GetFacebookSavedLikes(IMemberDTO member, Brand brand)
        {
            FacebookLikeParams fbLikeParams = new FacebookLikeParams();
            fbLikeParams.MemberId = member.MemberID;
            fbLikeParams.SiteId = brand.Site.SiteID;

            FacebookLikeList likesList = FacebookLikeService.GetFacebookLikeListByMember(fbLikeParams);
            return likesList;
        }

        public List<FacebookLike> GetRandomFacebookSavedLikes(IMemberDTO member, Brand brand, int count, bool approvedOnly)
        {
            FacebookLikeParams fbLikeParams = new FacebookLikeParams();
            fbLikeParams.MemberId = member.MemberID;
            fbLikeParams.SiteId = brand.Site.SiteID;

            List<FacebookLike> randomFacebookLikesListByMember = FacebookLikeService.GetRandomFacebookLikesListByMember(fbLikeParams, count, approvedOnly);
            return randomFacebookLikesListByMember;
        }

        public List<FacebookLike> GetFacebookLikesByCategoryGroup(int memberID, Brand brand, int chunkStartIndex, int categoryGroupID, bool approvedOnly)
        {
            List<FacebookLike> facebookLikes = new List<FacebookLike>();
            FacebookLikeParams fbLikeParams = new FacebookLikeParams();
            fbLikeParams.MemberId = memberID;
            fbLikeParams.SiteId = brand.Site.SiteID;

            FacebookLikeList likesList = FacebookLikeService.GetFacebookLikeListByMember(fbLikeParams);
            if (likesList != null && likesList.FacebookLikesGroupDictionary.ContainsKey(categoryGroupID))
            {
                List<FacebookLike> facebookLikesByCategoryGroup = likesList.FacebookLikesGroupDictionary[categoryGroupID];
                if (facebookLikesByCategoryGroup.Count > chunkStartIndex)
                {
                    for (int i = chunkStartIndex; i < facebookLikesByCategoryGroup.Count; i++)
                    {
                        FacebookLike fbLike = facebookLikesByCategoryGroup[i];
                        if (approvedOnly)
                        {
                            if (fbLike.FacebookLikeStatus == FacebookLikeStatus.Approved)
                            {
                                facebookLikes.Add(fbLike);
                            }
                        }
                        else
                        {
                            facebookLikes.Add(fbLike);
                        }
                    }
                }
            }

            return facebookLikes;
        }
        #endregion

    }
}
