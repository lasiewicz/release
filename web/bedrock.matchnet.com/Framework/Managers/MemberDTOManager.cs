﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Search.ValueObjects;

namespace Matchnet.Web.Framework.Managers
{
    public class MemberDTOManager : AbstractManagerBaseClass
    {
        private const string STR_FORMAT_DTO_DISPOSE_NOT_COMPLETE = "{0} DTOs not completely disposed for {1} request for {2} runs in {3} millis.";
        private const string STR_FORMAT_DTO_DISPOSE_COMPLETE = "{0} DTOs completely disposed for {1} request for {2} runs in {3} millis.";
        private const string CLASS_NAME = "MemberDTOManager";
        public const string ENABLE_MEMBER_DTO_CONSTANT = "ENABLE_MEMBER_DTO";
        private static readonly MemberDTOManager _instance = new MemberDTOManager();
        private IGetMember _getMemberService = null;
        //TODO: Add lock for afdd and remove
        private static Dictionary<string, ArrayList> dtoListRegistry = new Dictionary<string, ArrayList>();
        private static object _lockObject = new object();
        private MemberDTOManager() { }
        
        private void AddDTOToList(string key, IMemberDTO dto)
        {
            lock (_lockObject)
            {
                ArrayList dtoList = null;
                if (null != dtoListRegistry)
                {
                    if(!dtoListRegistry.ContainsKey(key))
                    {
                        dtoList = new ArrayList();
                        dtoListRegistry[key] = dtoList;
                    }
                    dtoListRegistry[key].Add(dto);
                } 
            }
        }

        private void AddDTOListToList(string key, ArrayList dtos)
        {
            lock (_lockObject)
            {
                ArrayList dtoList = null;
                if (null != dtoListRegistry)
                {
                    if (!dtoListRegistry.ContainsKey(key))
                    {
                        dtoList = new ArrayList();
                        dtoListRegistry[key] = dtoList;
                    }
                    dtoListRegistry[key].AddRange(dtos);
                }
            }
        }

        public static MemberDTOManager Instance
        {
            get { return _instance; }
        }

        public IGetMember GetMemberService
        {
            get { return _getMemberService ?? (_getMemberService = MemberSA.Instance); }
            set { _getMemberService = value; }
        }

        public IMemberDTO GetIMemberDTO(int memberID, ContextGlobal context, MemberType type, MemberLoadFlags flags)
        {
            return GetIMemberDTO(memberID, context.Brand, type, flags);
        }

        public IMemberDTO GetIMemberDTO(int memberID, Brand b, MemberType type, MemberLoadFlags flags)
        {
            if (SettingsManager.GetSettingBool(ENABLE_MEMBER_DTO_CONSTANT, b))
            {
                IMemberDTO iMemberDto = GetMemberService.GetMemberDTO(memberID, type);
                string key = (string)HttpContext.Current.Items[WebConstants.REQUEST_SCOPED_DTO_KEY];

                AddDTOToList(key,iMemberDto);
                return iMemberDto;
            }
            else
            {
                return GetMemberService.GetMember(memberID, flags);
            }
        }

        public ArrayList GetIMemberDTOsFromResults(MatchnetQueryResults searchResults, ContextGlobal context, MemberType type, MemberLoadFlags flags)
        {
            return GetIMemberDTOsFromResults(searchResults, context.Brand, type, flags);
        }

        public ArrayList GetIMemberDTOsFromResults(MatchnetQueryResults searchResults, Brand b, MemberType type, MemberLoadFlags flags)
        {
            ArrayList memberIDs = searchResults.ToArrayList();            
            return GetIMemberDTOs(b, memberIDs, type, flags);
        }

        public ArrayList GetIMemberDTOs(ContextGlobal context, ArrayList memberIDs, MemberType type, MemberLoadFlags flags)
        {
            return GetIMemberDTOs(context.Brand, memberIDs, type, flags);
        }

        public ArrayList GetIMemberDTOs(Brand b, ArrayList memberIDs, MemberType type, MemberLoadFlags flags)
        {
            if (SettingsManager.GetSettingBool(ENABLE_MEMBER_DTO_CONSTANT, b))
            {
                ArrayList memberDtOs = GetMemberService.GetMemberDTOs(memberIDs, type);
                string key = (string)HttpContext.Current.Items[WebConstants.REQUEST_SCOPED_DTO_KEY];
                AddDTOListToList(key, memberDtOs);
                return memberDtOs;
            }
            else
            {
                return GetMemberService.GetMembers(memberIDs, flags);
            }
        }

        public void DisposeDTOs(string key)
        {
            if (string.IsNullOrEmpty(key)) return;
            ArrayList dtos = null;

            lock (_lockObject)
            {
                if (null != dtoListRegistry && dtoListRegistry.ContainsKey(key))
                {
                    dtos = dtoListRegistry[key];
                }
            }

            if (null != dtos)
            {
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                int dtoLength = dtos.Count;
                DisposeDTO[] disposeDtos = new DisposeDTO[dtoLength];
                for (int i = 0; i < dtoLength; i++)
                {
                    DisposeDTO disposeDto = new DisposeDTO(dtos[i]);
                    disposeDtos[i] = disposeDto;
                    System.Threading.Thread t = new System.Threading.Thread(disposeDto.Run);
                    t.Start();
                    t.Join();
                }

                bool allDone = false;
                int numOfDisposeRuns = SettingsManager.GetSettingInt("NUM_DTO_DISPOSAL_ATTEMPTS",0,0,0);
                if (numOfDisposeRuns < 1) numOfDisposeRuns = 25; //default to 25 if setting is not present

                int idx=0;
                for (idx=0; !allDone && idx < numOfDisposeRuns; idx++)
                {
                    foreach (DisposeDTO disposeDto in disposeDtos)
                    {
                        allDone = disposeDto.IsDone;
                        if(!allDone) break;
                    }
                }
                stopwatch.Stop();
                string logStringFormat = (!allDone) ? STR_FORMAT_DTO_DISPOSE_NOT_COMPLETE : STR_FORMAT_DTO_DISPOSE_COMPLETE;                                                    
                LoggingManager.LogInfoMessage(CLASS_NAME, string.Format(logStringFormat, dtoLength, key, idx, stopwatch.ElapsedMilliseconds));
            }

            lock (_lockObject)
            {
                if (null != dtoListRegistry && dtoListRegistry.ContainsKey(key))
                {
                    dtoListRegistry.Remove(key);
                }
            }
        }
    }

//        private void DisposeDTO(object dto)
//        {
//            if (null != dto && dto is MemberDTO) ((MemberDTO)dto).Dispose();
//        }

    public class DisposeDTO
    {
        public object Obj { get; set; }
        public bool IsDone { get; set; }

        public DisposeDTO(object obj)
        {
            this.Obj = obj;
            this.IsDone = false;
        }

        public void Run()
        {
            try
            {
                if (null != Obj && Obj is MemberDTO) ((MemberDTO)Obj).Dispose();
            }
            finally
            {
                IsDone = true;
                Obj = null;
            }
        }
    }
}