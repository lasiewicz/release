﻿using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Web.Framework.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Managers
{
    /// <summary>
    /// Manages all LivePerson related processes
    /// </summary>
    public class LivePersonManager : AbstractManagerBaseClass
    {
        #region Singleton manager
        private static readonly LivePersonManager _Instance = new LivePersonManager();
        private LivePersonManager()
        {
        }
        public static LivePersonManager Instance
        {
            get { return _Instance; }
        }

        #endregion

        public bool IsLivePersonEnabled(IMember member, Brand brand)
        {
            // LP should be displyed to visitors 
            if (member == null && !string.IsNullOrEmpty(SettingsManager.GetSettingString("ENABLE_LIVE_PERSON", brand, string.Empty)))
                return true;

            if (member != null)
            {
                if (!member.IsPayingMember(brand.Site.SiteID))
                {
                    FeatureThrottler ft = new FeatureThrottler(ThrottleMode.LastDigit, "ENABLE_LIVE_PERSON",
                        brand, member);

                    return ft.IsFeatureEnabled();
                }
            }

            return false;
        }
    }
}