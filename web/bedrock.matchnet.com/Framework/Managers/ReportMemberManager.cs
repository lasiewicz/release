﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Admin;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.Managers
{
    public class ReportMemberManager : AbstractManagerBaseClass, IReportMemberManager
    {
        #region Singleton implementation
        private static readonly ReportMemberManager _instance = new ReportMemberManager();
        private ReportMemberManager()
        {}
        public static ReportMemberManager Instance
        {
            get { return _instance; }
        }
        #endregion

        /// <summary>
        /// This method is used by ContactUs page.
        /// </summary>
        /// <param name="EmailAddress"></param>
        /// <param name="SiteName"></param>
        /// <param name="Explanation"></param>
        /// <param name="EnvironmentType"></param>
        /// <param name="EndLineDelimiter"></param>
        /// <param name="MemberId"></param>
        /// <returns></returns>
        public string ComposeMessage(string EmailAddress, string SiteName, string ReportedUsername, string Explanation, string EnvironmentType, string EndLineDelimiter, int? MemberId)
        {
            var Message = new System.IO.StringWriter();
            var memberIdString = MemberId == null ? "n/a" : MemberId.ToString();

            Message.WriteLine("MemberID: {0} ({1}){2}", memberIdString, EmailAddress, EndLineDelimiter);
            Message.WriteLine("Site: " + SiteName + EndLineDelimiter);
            Message.WriteLine("Time: " + DateTime.Now.ToString() + EndLineDelimiter);
            Message.WriteLine("Environment: " + EnvironmentType + EndLineDelimiter);
            Message.WriteLine("Reported Username: " + ReportedUsername + EndLineDelimiter);
            Message.WriteLine("Explanation: " + Explanation.Trim().Replace("\n", EndLineDelimiter) + EndLineDelimiter);

            return Message.ToString();
        }
    }
}