﻿using System;
using System.Data;
using System.Web;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Lib;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Web.Applications.PhotoGallery;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Interfaces;
using Matchnet.PhotoSearch.ServiceAdapters.Interfaces;
using Matchnet.PhotoSearch.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using System.Collections;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Content.ServiceAdapters.Links;

namespace Matchnet.Web.Framework.Managers
{
    /// <summary>
    /// Manages photo gallery related processes
    /// </summary>
    public class PhotoGalleryManager : AbstractManagerBaseClass
    {
        private IGetMember _getMember;
        private IPhotoGallery _photoGalleryService;
        
        #region Singleton manager
        private static readonly PhotoGalleryManager _Instance = new PhotoGalleryManager();
        private PhotoGalleryManager()
        {
            
        }
        public static PhotoGalleryManager Instance
        {
            get { return _Instance; }
        }

        #endregion

        #region Properties
        public IPhotoGallery PhotoGalleryService
        {
            get { return _photoGalleryService ?? PhotoGallerySA.Instance; }
            set { _photoGalleryService = value; }
        }

        public IGetMember GetMemberService
        {
            get { return _getMember ?? (_getMember = MemberSA.Instance); }
            set { _getMember = value; }
        }
        #endregion

        #region Public methods
        public PhotoGalleryResultList SearchPhotoGallery(Brand brand, int memberID, int startRow, int pageSize, PhotoSearch.ValueObjects.PhotoGalleryQuery query, FrameworkControl resourceControl)
        {
            PhotoGalleryResultList resultsList = null;

            if (query == null)
                return resultsList;

            //perform search
            int page = startRow / pageSize + 1;
            resultsList = PhotoGalleryService.Search(query, memberID, brand.Site.Community.CommunityID, page, pageSize, query.SortBy == SortByType.random);

            //populate member info for results
            GetPhotoGalleryResultMembers(resultsList.PhotoGalleryResults, brand, memberID, startRow, resourceControl);

            return resultsList;

        }

        public void GetPhotoGalleryResultMembers(ArrayList resultlist, Brand brand, int memberID, int startRow, FrameworkControl resourceControl)
        {
            if (resultlist == null)
                return;

            IMemberDTO ViewingMember = null;
            List.ServiceAdapters.List ViewingMemberList = null;

            if (memberID > 0)
            {
                ViewingMember = GetMemberService.GetMember(memberID, MemberLoadFlags.None);
                ViewingMemberList = ListSA.Instance.GetList(memberID);
            }
            ArrayList memberIDs = new ArrayList();
            for (int i = 0; i < resultlist.Count; i++)
            {
                memberIDs.Add(((PhotoResultItem)resultlist[i]).MemberID);
            }

            ArrayList members = MemberDTOManager.Instance.GetIMemberDTOs(brand, memberIDs, MemberType.Search, MemberLoadFlags.None);
            if (members == null)
                return;

            //Resources
            var resourceManager = new ResourceManager(brand);


            string AgeResourceText1 = "";
            string AgeResourceText2 = "";
            string PostedResourceText1 = "";
            string PostedResourceText2 = "";
            string PostedResourceToday = "";
            string PostedResourceYesterday = "";
            if (resourceControl != null)
            {
                AgeResourceText1 = FrameworkGlobals.GetUnicodeText(resourceManager.GetResource("AGE_STR_1", resourceControl));
                AgeResourceText2 = FrameworkGlobals.GetUnicodeText(resourceManager.GetResource("AGE_STR", resourceControl));
                //PostedResourceText1 = FrameworkGlobals.GetUnicodeText(resourceManager.GetResource("POSTED_1", resourceControl));
                PostedResourceText2 = FrameworkGlobals.GetUnicodeText(resourceManager.GetResource("POSTED_2", resourceControl));
                PostedResourceToday = FrameworkGlobals.GetUnicodeText(resourceManager.GetResource("POSTED_TODAY", resourceControl));
                PostedResourceYesterday = FrameworkGlobals.GetUnicodeText(resourceManager.GetResource("POSTED_YESTERDAY", resourceControl));
            }

            for (int i = 0; i < members.Count; i++)
            {
                IMemberDTO member = (IMemberDTO)members[i];
                if (member == null)
                    continue;
                PhotoGalleryMember gmember = new PhotoGalleryMember();
                gmember.MemberID = member.MemberID;
                gmember.BirthDate = member.GetAttributeDate(brand, "Birthdate", DateTime.MinValue);
                gmember.UserName = member.GetUserName(brand);
                gmember.Age = FrameworkGlobals.GetAge(gmember.BirthDate);
                gmember.AgeDisplayText = String.Format("{0} {1} {2}", AgeResourceText1, gmember.Age.ToString(), AgeResourceText2);
                int ordinal = startRow + i;
                gmember.ProfileURL = BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.PhotoGallery, member.MemberID, ordinal, null, 0, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
                if (resourceControl != null)
                {
                    gmember.IsNew = member.IsNewMember(brand.Site.Community.CommunityID);
                    gmember.IsUpdated = member.IsUpdatedMember(brand.Site.Community.CommunityID);
                    bool isOnline = false;
                    OnlineLinkHelper onlineLinkHelper = OnlineLinkHelper.getOnlineLinkHelper(Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.None, member, ViewingMember, LinkParent.GalleryMiniProfile, (int)PurchaseReasonType.AttemptToIMGalleryMiniProfile, out isOnline);
                    gmember.IsOnline = isOnline;
                    gmember.IMURL = onlineLinkHelper.OnlineLink;
                    string emailURL = "/Applications/Email/Compose.aspx?MemberId=" + gmember.MemberID + "&EntryPoint=" + (int)BreadCrumbHelper.EntryPoint.PhotoGallery;
                    gmember.EmailURL = emailURL;

                    gmember.IsHighlighted = FrameworkGlobals.memberHighlighted(member, brand);                    
                    gmember.AgeLocationDisplayText = ProfileDisplayHelper.GetFormattedAgeLocation(member, resourceControl, brand, resourceManager);
                    gmember.MaritalSeekingDisplayText = ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(member, brand, resourceManager);
                    gmember.Location = FrameworkGlobals.GetRegionString(member.GetAttributeInt(brand, "regionid"),
                                                                        brand.Site.LanguageID, false, false, false,
                                                                        false);
                    gmember.LocationShort =FrameworkGlobals.Ellipsis(gmember.Location, 15);
                    

                    if(ViewingMemberList != null)
                    {
                       gmember.IsFavorited = ViewingMemberList.IsHotListed(HotListCategory.Default, brand.Site.Community.CommunityID, member.MemberID);
                    }
                }
                else
                {
                    gmember.IsOnline = false;
                    gmember.IsNew = false;
                    gmember.IsUpdated = false;
                    gmember.AgeLocationDisplayText = "";
                    gmember.MaritalSeekingDisplayText = "";
                }
                var photos = MemberPhotoDisplayManager.Instance.GetAllPhotos(member, brand.Site.Community.CommunityID);
                if (photos != null)
                {
                    gmember.PhotoCount = MemberPhotoDisplayManager.Instance.GetApprovedPhotosCount(ViewingMember, member, brand);
                    if (gmember.PhotoCount < 0)
                    {
                        gmember.PhotoCount = 0;
                    }

                    if (resourceControl != null)
                    {
                        gmember.PhotoMoreDisplayText = HttpContext.Current.Server.HtmlDecode(FrameworkGlobals.GetMorePhotoLink((gmember.PhotoCount == 1) ? 0 : gmember.PhotoCount, resourceControl, "TXT_VIEW_MEMBER_ARROWS", resourceManager));
                    }
                    for (int l = 0; l < photos.Count; l++)
                    {
                        if (photos[l].MemberPhotoID == ((PhotoResultItem)resultlist[i]).MemberPhotoID)
                        {
                            gmember.Caption = photos[l].IsCaptionApproved ? photos[l].Caption : "";
                            gmember.ThumbWebPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(ViewingMember,
                                                                                                            member,
                                                                                                            brand,
                                                                                                            photos[l],
                                                                                                            PhotoType.
                                                                                                                Thumbnail,
                                                                                                            PrivatePhotoImageType
                                                                                                                .Thumb,
                                                                                                            NoPhotoImageType
                                                                                                                .Thumb,
                                                                                                            false, true);
                            gmember.FileWebPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(ViewingMember,
                                                                                                        member,
                                                                                                        brand,
                                                                                                        photos[l],
                                                                                                        PhotoType.
                                                                                                            Thumbnail,
                                                                                                        PrivatePhotoImageType
                                                                                                            .Thumb,
                                                                                                        NoPhotoImageType
                                                                                                            .Thumb,
                                                                                                        false, true);
                            gmember.LargeWebPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(ViewingMember,
                                                                                                        member,
                                                                                                        brand,
                                                                                                        photos[l],
                                                                                                        PhotoType.Full,
                                                                                                        PrivatePhotoImageType
                                                                                                            .Full,
                                                                                                        NoPhotoImageType
                                                                                                            .Thumb,
                                                                                                        false, true);

                            break;
                        }
                    }
                }
                gmember.PostedDate = ((PhotoResultItem)resultlist[i]).PhotoInsertDate;
                double hrs = DateTime.Now.Subtract(gmember.PostedDate).TotalHours;
                int hrs24 = (int)hrs / 24;
                int remhrs = (int)hrs % 24;

                int days = hrs24 + (remhrs > 0 ? 1 : 0);
                if (hrs24 == 0 && (gmember.PostedDate.Day == DateTime.Today.Day && gmember.PostedDate.Month == DateTime.Today.Month && gmember.PostedDate.Year == DateTime.Today.Year))
                {
                    //leave like it - it's today
                }
                else
                {
                    hrs24 += 1;
                }

                if (hrs24 != 0)
                {
                    if (days == 1)
                        gmember.PostedDateDisplayText = String.Format("{0} {1} {2}", PostedResourceYesterday, "", "");
                    else
                        gmember.PostedDateDisplayText = String.Format("{0} {1}", days, PostedResourceText2);
                }
                else
                    gmember.PostedDateDisplayText = String.Format("{0} {1} {2}", PostedResourceToday, "", "");

                ((PhotoResultItem)resultlist[i]).GalleryMember = gmember;                
            }
        }

        public int GetPageSize(Brand brand)
        {
            //DB Setting
            int returnValue = int.MinValue;
            try
            {
                returnValue = SettingsManager.GetSettingInt("PHOTOGALLERY_PAGE_SIZE", brand);
            }
            catch (Exception ex)
            {
                //setting missing
            }

            if (returnValue <= 0)
            {
                //hard coded default
                returnValue = 24;
            }

            return returnValue;
        }

        public bool IsPhotoGalleryAsHomePageEnabled(IMember member, Brand brand)
        {
            if (member != null)
            {
                FeatureThrottler ft = new FeatureThrottler(ThrottleMode.LastDigit, "PHOTO_GALLERY_AS_HOMEPAGE",
                    brand, member);

                return ft.IsFeatureEnabled();
            }

            return false;
        }

        #endregion

        #region FilterRelatedMethods

        public bool IsPhotoGallery30Enabled(Brand brand)
        {
            return SettingsManager.GetSettingBool(Matchnet.Configuration.ValueObjects.SettingConstants.USE_PHOTOGALLERY_30, brand);
        }

        public int GetFilterPersistenceExpirationDays(Brand brand)
        {
            return SettingsManager.GetSettingInt(Matchnet.Configuration.ValueObjects.SettingConstants.PHOTOGALLERY_PERSISTENCE_DAYS, brand);
        }

        public DataTable GetGenderOptions(FrameworkControl resourceControl, Brand brand)
        {
            var resourceManager = new ResourceManager(brand);
            
            var dt = new DataTable();
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("Value", typeof(System.Int16));

            dt.Rows.Add(new object[] { resourceManager.GetResource("EVERYONE", resourceControl), 0 });
            dt.Rows.Add(new object[] { resourceManager.GetResource("WOMEN", resourceControl), ConstantsTemp.GENDERID_FEMALE });
            dt.Rows.Add(new object[] { resourceManager.GetResource("MEN", resourceControl), ConstantsTemp.GENDERID_MALE });


            return dt;
        }

        public DataTable GetPostedDaysOptions(QueryOptionCollection coll, FrameworkControl resourceControl, Brand brand)
        {
            var resourceManager = new ResourceManager(brand);
            
            var dt = new DataTable();
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("Value", typeof(System.Int16));

            foreach (QueryOption opt in coll)
            {
                string content = ""; 
                if (opt.OptionValue > 1)
                    content = opt.OptionValue.ToString() + " " + resourceManager.GetResource("TXT_DAYS", resourceControl);
                else
                    content = opt.OptionValue.ToString() + " " + resourceManager.GetResource("TXT_DAY", resourceControl);
                dt.Rows.Add(new object[] { content, opt.OptionValue });

            }

            return dt;
        }

        public DataTable GetRegionOptions(FrameworkControl resourceControl, Brand brand, IMemberDTO member)
        {
            var resourceManager = new ResourceManager(brand);
            
            var dt = new DataTable();
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("Value", typeof(System.Int32));

            var coll = PhotoGalleryService.GetOptionCollection(brand.Site.SiteID, QueryOptionType.region);

            dt.Rows.Add(new object[] { resourceManager.GetResource("ANYWHERE", resourceControl), Constants.NULL_INT });
            if (member != null)
            {
                dt.Rows.Add(new object[] { FrameworkGlobals.GetUnicodeText(resourceManager.GetResource("YOUR_REGION", resourceControl)), member.GetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_REGIONID) });
            }
            if (coll == null)
                return dt;
            
            for (int i = 0; i < coll.Count; i++)
            {
                int regionid = coll[i].OptionValue;
                Content.ValueObjects.Region.Region region = Content.ServiceAdapters.RegionSA.Instance.RetrieveRegionByID(regionid, brand.Site.LanguageID);
                dt.Rows.Add(new object[] { region.Description, regionid });
            }
            
            return dt;
        }

        public DataTable GetSortOptions(FrameworkControl resourceControl, Brand brand)
        {
            var resourceManager = new ResourceManager(brand);

            var dt = new DataTable();
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("Value", typeof(System.Int16));

            dt.Rows.Add(new object[] { resourceManager.GetResource("TXT_SORT_RANDOM", resourceControl), (int)SortByType.random });
            dt.Rows.Add(new object[] { resourceManager.GetResource("TXT_SORT_NEWEST", resourceControl), (int)SortByType.newest });


            return dt;
        }

        public QueryOptionCollection GetPeriodOptions(Brand brand)
        {
            return PhotoGalleryService.GetOptionCollection(brand.Site.SiteID, QueryOptionType.insertdays);
        }

        public PhotoGalleryFilterSettings GetFilterSettings(Brand brand, IMemberDTO member)
        {
            var filterExpirationDays = GetFilterPersistenceExpirationDays(brand);
            var filterSettings = new PhotoGalleryFilterSettings();

            var photoGalleryQuery = PhotoGalleryUtils.GetQueryFromSession(brand, member != null ? member.MemberID : int.MinValue, filterExpirationDays);
            filterSettings.Gender = photoGalleryQuery.Gender;
            filterSettings.MinAge = photoGalleryQuery.AgeMin;
            filterSettings.MaxAge = photoGalleryQuery.AgeMax;

            if(photoGalleryQuery.InsertDays > 0)
            {
                filterSettings.PostedDays = photoGalleryQuery.InsertDays;
            }
            else
            {
                filterSettings.PostedDays = 7;
            }

            var memberRegionID = member != null ? member.GetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_REGIONID, int.MinValue) : Constants.NULL_INT;

            if (photoGalleryQuery.RegionType == SearchRegionType.anywhere)
            {
                filterSettings.RegionID = photoGalleryQuery.RegionID;
                filterSettings.CountryRegionID = Constants.NULL_INT;
            }
            else if(photoGalleryQuery.RegionType == SearchRegionType.myregion && member != null)
            {
                filterSettings.RegionID = memberRegionID;
                filterSettings.CountryRegionID = RegionSA.Instance.RetrievePopulatedHierarchy(filterSettings.RegionID, brand.Site.LanguageID).CountryRegionID;
            }
            else if(photoGalleryQuery.RegionType == SearchRegionType.country)
            {
                filterSettings.RegionID = photoGalleryQuery.RegionID;
                filterSettings.CountryRegionID = photoGalleryQuery.RegionID;
            }

            filterSettings.RegionType = photoGalleryQuery.RegionType;

            if (photoGalleryQuery.SortBy >= 0)
            {
                filterSettings.SortBy = (int)photoGalleryQuery.SortBy;
            }
            else
            {
                filterSettings.SortBy = (int)SortByType.random;
            }

            return filterSettings;
        }

        public void SetFilterOptionsAndSaveToSession(Brand brand, IMemberDTO member, int gender, int postedDayRange, int regionId, int minAge, int maxAge, int sortBy)
        {
            var filterExpirationDays = GetFilterPersistenceExpirationDays(brand);

            var query = PhotoGalleryUtils.GetQueryFromSession(brand, member == null ? Constants.NULL_INT : member.MemberID, filterExpirationDays);
            query.Gender = gender;
            query.InsertDays = postedDayRange;
            query.RegionID = regionId;
            query.AgeMin = minAge;
            query.AgeMax = maxAge;
            if (sortBy < 0)
                sortBy = 0;
            query.SortBy = (SortByType)Enum.Parse(typeof(SortByType), sortBy.ToString());

            var memberRegionID = member != null ? member.GetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_REGIONID, int.MinValue) : Constants.NULL_INT;

            if (regionId == Constants.NULL_INT)
            {
                query.RegionType = SearchRegionType.anywhere;
            }
            else if (memberRegionID != int.MinValue && regionId == memberRegionID)
            {
                query.RegionType = SearchRegionType.myregion;
                query.Radius = brand.DefaultSearchRadius;
                query.CountryRegionID = RegionSA.Instance.RetrievePopulatedHierarchy(regionId, brand.Site.LanguageID).CountryRegionID;
            }
            else
            {
                query.RegionType = SearchRegionType.country;
                query.CountryRegionID = regionId;
            }

            PhotoGalleryUtils.SaveQueryToSession(query, brand, member == null ? Constants.NULL_INT : member.MemberID, filterExpirationDays);
        }

        public bool ValidateFilterOptions(string gender, string postedDayRange, string regionId, string minAge, string maxAge)
        {
            int genderAsInt;
            int postedDayRangeAsInt;
            int regionIdAsInt;
            int minAgeAsInt;
            int maxAgeAsInt;
            
            if(!int.TryParse(gender, out genderAsInt))
            {
                return false;
            }
            if (!int.TryParse(postedDayRange, out postedDayRangeAsInt))
            {
                return false;
            }
            if (!int.TryParse(regionId, out regionIdAsInt))
            {
                return false;
            }
            if (!int.TryParse(minAge, out minAgeAsInt))
            {
                return false;
            }
            if (!int.TryParse(maxAge, out maxAgeAsInt))
            {
                return false;
            }
            if (minAgeAsInt > maxAgeAsInt)
            {
                return false;
            }
            if (minAgeAsInt < Constants.MIN_AGE_RANGE)
            {
                return false;
            }
            if (maxAgeAsInt > Constants.MAX_AGE_RANGE)
            {
                return false;
            }

            return true;
        }

        #endregion

    }
}
