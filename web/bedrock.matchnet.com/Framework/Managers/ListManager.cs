﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.List.ValueObjects;
using Matchnet.List.ValueObjects.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ServiceAdapters.Interfaces;
using Matchnet.List.ServiceAdapters;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;

namespace Matchnet.Web.Framework.Managers
{
    public class ListManager: AbstractManagerBaseClass
    {
        private IListSA _ListService;
        public IListSA ListService
        {
            get { return _ListService ?? ListSA.Instance; }
            set { _ListService = value; }
        }

        private ISaveMember _MemberSaveService;
        public ISaveMember MemberSaveService
        {
            get { return _MemberSaveService ?? MemberSA.Instance; }
            set { _MemberSaveService = value; }
        }

        public void YNMVote(int voteType, int fromMemberID, int toMemberID, Brand brand, ContextGlobal g)
        {
            ListService.AddClick(brand.Site.Community.CommunityID,
                    brand.Site.SiteID,
                    brand.Site.LanguageID,
                    fromMemberID,
                    toMemberID,
                    (ClickListType)voteType);

            if (g != null && UserNotificationFactory.IsUserNotificationsEnabled(g))
            {
                UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new MutualYesUserNotification(), toMemberID, fromMemberID, g);
                if (notification.IsActivated())
                {
                    UserNotificationParams unParams = UserNotificationParams.GetParamsObject(toMemberID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                    UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                    //add to both users' notifications
                    UserNotification notification2 = UserNotificationFactory.Instance.GetUserNotification(new MutualYesUserNotification(), fromMemberID, toMemberID, g);
                    UserNotificationParams unParams2 = UserNotificationParams.GetParamsObject(fromMemberID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                    UserNotificationsServiceSA.Instance.AddUserNotification(unParams2, notification2.CreateViewObject());
                }
            } 
        }

        public void MarkHotListLastViewed(IMember member, Brand brand, List<HotListCategory> hotListCategoryList,
                                          DateTime lastViewedDate)
        {
            foreach (HotListCategory hotListCategory in hotListCategoryList)
            {
                var lastVisitAttributeId = "LastHotListPageVisit-" + hotListCategory.ToString();
                try
                {
                    member.SetAttributeDate(brand, lastVisitAttributeId, lastViewedDate);
                }
                catch (Exception)
                {
                    //attribute doesn't exist
                }
                
            }
            MemberSaveService.SaveMember(member);

        }

        public int GetUnseenNotificationCount(IHotList hotList, IMember member, Brand brand, HotListCategory hotListCategory)
        {
            if(hotList == null) { throw new ArgumentNullException("HotList can not be null"); }
            if(member == null) { throw new ArgumentNullException("Member can not be null"); }
            if (brand == null) { throw new ArgumentNullException("Brand can not be null"); }
            
            var notificationCount = 0;
            int rowcount;
            var lastVisitAttributeId = "LastHotListPageVisit-" + hotListCategory.ToString();

            var lastVisit = DateTime.Today;
            try
            {
                lastVisit = member.GetAttributeDate(brand, lastVisitAttributeId, lastVisit);
            }
            catch 
            {
                //exception is thrown if attribute not set for site/brand
            }


            var memberIDs = hotList.GetListMembers(hotListCategory,
                                             brand.Site.Community.CommunityID,
                                             brand.Site.SiteID,
                                             1,
                                             100,
                                             out rowcount);

            for (int i = 0; i < memberIDs.Count; i++)
            {
                var listDetail = hotList.GetListItemDetail(hotListCategory,
                                brand.Site.Community.CommunityID,
                                brand.Site.SiteID,
                                (int)memberIDs[i]);

                if (listDetail.ActionDate > lastVisit)
                {
                    notificationCount++;
                }
            }

            return notificationCount;
        }

        public bool IsViewedYourProfileListASubscriberFeature(Brand brand)
        {
            if (brand != null)
            {
                return SettingsManager.GetSettingBool("ENABLE_VIEWED_YOUR_PROFILE_AS_SUBSCRIBER_FEATURE", brand, false);
            }
            else
            {
                return false;
            }
        }

        public bool IsViewedYourProfileListAvailableToMember(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
        {
            bool avail = true;

            if (IsViewedYourProfileListASubscriberFeature(brand))
            {
                if (member == null || !member.IsPayingMember(brand.Site.SiteID))
                {
                    avail = false;
                }
            }

            return avail;
        }
    }
}