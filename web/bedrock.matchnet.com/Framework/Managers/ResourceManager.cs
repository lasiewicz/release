﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Web.Framework.ConstantClasses;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.Managers
{
    public class ResourceManager: AbstractManagerBaseClass
    {
        private IMember _Member = null;
        private Brand _brand = null;
        private StringDictionary _expansionTokens = new StringDictionary();
        private ILocalizer _localizerService = null;

        public ILocalizer LocalizerService
        {
            get { return _localizerService ?? LocalizerSingleton.Instance; }
            set { _localizerService = value; }
        }
        
        public ResourceManager(Brand brand)
        {
            if(brand == null || brand.Site == null || brand.Site.Community == null)
            {
                throw new ArgumentNullException("brand");
            }
            
            _brand = brand;
        }

        public ResourceManager(IMember member, Brand brand): this(brand)
        {
            _Member = member;
        }

        private void InitializeExpansionTokens()
        {
            //string subscribeLink = LinkFactory.Instance.GetLink("/Applications/Subscription/Subscribe.aspx?", _brand, HttpContext.Current.Request.Url.ToString());
            string subscribeLink = "http://" + _brand.Site.DefaultHost + "." + _brand.Site.Name + "/Applications/Subscription/Subscribe.aspx";
            string hurryDateUrl = SettingsManager.GetSettingString(SettingConstants.HD_LANDINGPAGE_URL, _brand);
            string siteAlias = SettingsManager.GetSettingString(SettingConstants.SITE_NAME_ALIAS, _brand);
            string memberid = _Member != null ? _Member.MemberID.ToString() : string.Empty;

            _expansionTokens.Add(ExpansionTokens.SiteURL, "http://" + _brand.Site.DefaultHost + "." + _brand.Site.Name);
            _expansionTokens.Add(ExpansionTokens.SiteURLSSL, "http://" + _brand.Site.DefaultHost + "." + _brand.Site.Name);
            _expansionTokens.Add(ExpansionTokens.PLDomain, _brand.Site.Name);
            _expansionTokens.Add(ExpansionTokens.PLServerName, _brand.Site.DefaultHost);
            _expansionTokens.Add(ExpansionTokens.Year, System.DateTime.Now.Year.ToString());
            _expansionTokens.Add(ExpansionTokens.PLPhoneNumber, _brand.PhoneNumber);
            _expansionTokens.Add(ExpansionTokens.PLSecureLink, subscribeLink);
            _expansionTokens.Add(ExpansionTokens.PLSecureCreditLink, subscribeLink);
            _expansionTokens.Add(ExpansionTokens.PLSecureCheckLink, subscribeLink);
            _expansionTokens.Add(ExpansionTokens.CSSupportPhoneNumber, this.GetResource("CS_SUPPORT_PHONE_NUMBER"));
            _expansionTokens.Add(ExpansionTokens.HurryDateLandingPageURL, hurryDateUrl);
            
            //adding encrypted memberurlid for HurryDate, we already have 5 pages using this token so it makes sense to initialize it in 1 place instead of 5
            _expansionTokens.Add(ExpansionTokens.MemeberIDURLParam, memberid);
            
            if (String.IsNullOrEmpty(siteAlias))
            {
                siteAlias = _brand.Site.Name;

            }
            _expansionTokens.Add(ExpansionTokens.BrandName, siteAlias);
        }
        
        internal string GetResource(string resourceConstant)
        {
            return GetResource(resourceConstant, null);
        }

        public string GetResource(string resourceConstant, object caller)
        {
            return GetResource(resourceConstant, caller, _expansionTokens);
        }

        public string GetResource(string resourceConstant, object caller, StringDictionary expansionTokens)
        {
            return GetResource(resourceConstant, caller, expansionTokens, null);
        }
        
        public string GetResource(string resourceConstant, object caller, string[] args)
        {
            return GetResource(resourceConstant, caller, _expansionTokens, args);
        }
        
        public string GetResource(string resourceConstant, string[] args, bool replaceImageTokens, object caller)
        {
            string resourceString = GetResource(resourceConstant, caller, args);
            if (replaceImageTokens)
            {
                resourceString = Localizer.ExpandImageTokens(resourceString, _brand);
            }

            return resourceString;
        }

        public string GetResource(string resourceConstant, object caller, StringDictionary expansionTokens, string[] args)
        {
            return GetResource(resourceConstant, caller, _brand, expansionTokens, args);
        }

        public string GetTargetResource(string resourceConstant, Brand targetBrand)
        {
            return GetResource(resourceConstant, null, targetBrand, _expansionTokens, null);
        }

        public string GetResource(Brand brand, string resourceConstant)
        {
            return GetResource(resourceConstant, null, brand, _expansionTokens, null);
        }

        private string GetResource(string resourceConstant, object caller, Brand brand, StringDictionary expansionTokens, string[] args)
        {
            if (_expansionTokens.Count == 0) InitializeExpansionTokens();
            if(SettingsManager.GetSettingBool(SettingConstants.SHOW_RESOURCE_HINTS, brand))
            {
                return LocalizerService.GetFormattedStringResource(resourceConstant, caller, brand, expansionTokens, args, true);
            }
            else
            {
                return LocalizerService.GetFormattedStringResource(resourceConstant, caller, brand, expansionTokens, args, false);
            }
        }

        public void AddExpansionToken(string key, string val)
        {
            if (!_expansionTokens.ContainsKey(key))
            {
                _expansionTokens.Add(key, val);
            }
             _expansionTokens[key] = val;
        }
    }
}