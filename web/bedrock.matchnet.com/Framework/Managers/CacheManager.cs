﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Web.Framework.Managers
{
    public class CacheManager: AbstractManagerBaseClass
    {
        #region Singleton manager
        private static readonly CacheManager _Instance = new CacheManager();
        private CacheManager()
        {
        }
        public static CacheManager Instance
        {
            get { return _Instance; }
        }

        #endregion

        public ICaching GetCacheInstance(Brand brand)
        {
            bool useMembase = SettingsManager.GetSettingBool(SettingConstants.USE_MEMBASE_CACHE, brand);
            bool useMembaseForWeb = SettingsManager.GetSettingBool(SettingConstants.USE_MEMBASE_CACHE_FOR_WEB, brand);

            if (useMembase && useMembaseForWeb)
            {
                MembaseConfig membaseConfig =
                    RuntimeSettings.GetMembaseConfigByBucket(WebConstants.MEMBASE_WEB_BUCKET_NAME);
                return Spark.Caching.MembaseCaching.GetSingleton(membaseConfig);
            }
            else
            {
                return CachingTemp.Cache.GetInstance();
            }
        }
    }
}