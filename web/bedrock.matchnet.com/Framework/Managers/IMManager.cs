﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Interfaces;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Web.Framework.Ui;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Framework.Managers
{
    /// <summary>
    /// Manages IM related processes
    /// </summary>
    public class IMManager : AbstractManagerBaseClass
    {
        #region Singleton manager
        private static readonly IMManager _Instance = new IMManager();
        private IMManager()
        {
        }
        public static IMManager Instance
        {
            get { return _Instance; }
        }

        #endregion

        public bool IsImAnnouncementEnabled(Brand brand)
        {
            if (brand != null)
            {
                return SettingsManager.GetSettingBool("ENABLE_IM_PAGE_ANNOUNCEMENT", brand, false);
            }
            else
            {
                return false;
            }
        }

        public bool IsImHistoryEnabled(Brand brand)
        {
            if (brand != null)
            {
                return SettingsManager.GetSettingBool("ENABLE_IM_HISTORY_UI", brand, false);
            }
            else
            {
                return false;
            }
        }

        public bool IsImHistoryPageAvailableToNonSubs(Brand brand)
        {
            if (brand != null)
            {
                return SettingsManager.GetSettingBool("ENABLE_IM_HISTORY_FOR_NON_SUBS", brand, false);
            }
            else
            {
                return false;
            }
        }

        public IMHistoryMemberListResult GetIMHistoryMembersDummyDataForJSON(Brand brand, int memberID, int startRow, int pageSize, FrameworkControl resourceControl)
        {
            int totalTempDataCount = 100;
            IMHistoryMemberListResult response = new IMHistoryMemberListResult();

            response.IMHistoryMemberList = new List<IMHistoryMember>();
            response.MemberID = memberID;
            response.TotalResultCount = totalTempDataCount;
            response.BatchResultCount = 0;

            if (startRow <= totalTempDataCount)
            {
                int endIndex = startRow + pageSize - 1;
                if (endIndex > totalTempDataCount)
                {
                    endIndex = totalTempDataCount;
                }

                for (int i = startRow; i <= endIndex; i++) {
                    IMHistoryMember member = new IMHistoryMember();
                    member.MemberID = i;

                    int age = 17 + i;
                    if ((i % 2) > 0)
                    {
                        member.ThumbWebPath = "http://s3.amazonaws.com/sparkmemberphotos/2009/10/12/11/151029673.jpg";
                        member.UserName = "ChampOne" + i;
                        member.AgeLocationDisplayText =  age.ToString() + ", New York, NY";
                        member.LastMessageDateString = "03/06/2013 05:28 PM";
                    }
                    else
                    {
                        member.ThumbWebPath = "http://s3.amazonaws.com/sparkmemberphotos/2009/03/20/22/148361413.jpg";
                        member.UserName = "ChampTwo" + i;
                        member.AgeLocationDisplayText = age.ToString() + ", Los Angeles, CA";
                        member.LastMessageDateString = "03/06/2013 05:28 PM";
                        
                    }
                    response.IMHistoryMemberList.Add(member);
                    response.BatchResultCount++;
                }
            }

            return response;
        }

        public IMHistoryMessageListResult GetIMHistoryMessagesDummyDataForJSON(Brand brand, int memberID, int targetMemberID, int startRow, int pageSize, FrameworkControl resourceControl)
        {
            int totalTempDataCount = 10000;
            IMHistoryMessageListResult response = new IMHistoryMessageListResult();

            response.IMHistoryMessageList = new List<IMHistoryMessage>();
            response.MemberID = memberID;
            response.TotalResultCount = totalTempDataCount;
            response.BatchResultCount = 0;
            response.AgeLocationDisplayText = "";
            response.EmailURL = "/Applications/Email/Compose.aspx?MemberId=" + targetMemberID.ToString() + "&EntryPoint=" + (int)BreadCrumbHelper.EntryPoint.Messages;
            response.IMURL = "";
            response.IsOnline = true;
            response.MaritalSeekingDisplayText = "Single, Woman seeking a Man";
            response.ProfileURL = BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.Messages, targetMemberID, 1, null, 0, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
            response.TargetMemberID = targetMemberID;
            response.ThumbWebPath = "";
            response.UserName = "ChampOne" + targetMemberID.ToString();

            int age = 17 + targetMemberID;
            if ((targetMemberID % 2) > 0)
            {
                response.IsOnline = true;
                response.ThumbWebPath = "http://s3.amazonaws.com/sparkmemberphotos/2009/10/12/11/151029673.jpg";
                response.AgeLocationDisplayText = age.ToString() + ", New York, NY";
                response.IMURL = "javascript:alert('IM link here');";
            }
            else
            {
                response.IsOnline = false;
                response.ThumbWebPath = "http://s3.amazonaws.com/sparkmemberphotos/2009/03/20/22/148361413.jpg";
                response.AgeLocationDisplayText = age.ToString() + ", Los Angeles, CA";
                response.IMURL = "javascript:alert('Subscribe link here');";
            }

            if (startRow <= totalTempDataCount)
            {
                int endIndex = startRow + pageSize - 1;
                if (endIndex > totalTempDataCount)
                {
                    endIndex = totalTempDataCount;
                }

                for (int i = startRow; i <= endIndex; i++)
                {
                    IMHistoryMessage message = new IMHistoryMessage();
                    message.MemberID = targetMemberID;
                    message.MessageID = i;
                    message.MessageDateString = "03/06/2013 05:28 PM";
                    
                    int remainder = (i % 10);
                    if (i == 5)
                    {
                        message.MessageText = @"here you go with icons too <sparktag type=""emoticon"" class=""spr icons-smiley-a-0008""/> <sparktag type=""emoticon"" class=""spr icons-smiley-a-0012""/> <sparktag type=""photo"" alt=""myfilename"" src=""/attachments/136834b9f8d3479e21b07f3aba487b1d/preview/173373392.jpg"" id=""2137"" data-originalExt=""jpg""/>";
                    }
                    else
                    {
                        switch (remainder)
                        {
                            case 0:
                                message.MessageText = @"<sparktag type=""emoticon"" class=""spr icons-smiley-a-0008""/> <sparktag type=""emoticon"" class=""spr icons-smiley-a-0012""/> <sparktag type=""emoticon"" class=""spr icons-smiley-a-0013""/> hello";
                                message.MemberID = memberID;
                                break;
                            case 9:
                                message.MessageText = "hey, how are you?";
                                break;
                            case 8:
                                message.MessageText = "did you just finish dinner?";
                                break;
                            case 7:
                                message.MessageText = "yep, all finished, had some beans";
                                message.MemberID = memberID;
                                break;
                            case 6:
                                message.MessageText = "very good, here check out this picture";
                                break;
                            case 5:
                                message.MessageText = @"here you go <sparktag type=""photo"" alt=""myfilename"" src=""/attachments/136834b9f8d3479e21b07f3aba487b1d/preview/173373392.jpg"" id=""2137"" data-originalExt=""jpg""/>";
                                break;
                            case 4:
                                message.MessageText = "what do you think of that, took it myself";
                                break;
                            case 3:
                                message.MessageText = "not sure what to think";
                                message.MemberID = memberID;
                                break;
                            case 2:
                                message.MessageText = "ok";
                                break;
                            case 1:
                                message.MessageText = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, cum, incidunt atque deserunt pariatur voluptate obcaecati recusandae eaque error ut possimus doloremque vel quo dolore ducimus porro harum culpa unde!";
                                break;
                            default:
                                message.MessageText = "Can't be happening";
                                break;
                        }
                    }

                    message.MessageText = i.ToString() + ": " + message.MessageText;
                    response.IMHistoryMessageList.Add(message);
                    response.BatchResultCount++;
                }
            }

            return response;
        }
    }
}