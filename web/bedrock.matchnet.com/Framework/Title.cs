using System;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Framework
{
	/// <summary>
	/// Summary description for Txt.
	/// </summary>
	[DefaultProperty("Resource")]
	public class Title : WebControl
	{	
		private ContextGlobal _g; 
		private string		_titleResourceConstant = string.Empty;
		private string		_resourceConstant = string.Empty;
		private string[]	_args;
		private string		_imagename;
		private string		_altResourceConstant;
		private string		_CommunitiesForImage;
		private string[]	_CommunitiesForImageArray;
		private string		_width;
		private string		_height;
		private string		_href;
		private string		_border;
		private bool		_bold;
		private bool		_italic;
	
		public Title()
		{
			if ( Context != null )
			{
				if ( Context.Items["g"] != null )
				{
					_g = (ContextGlobal) Context.Items["g"];
				}
			}
			else
			{
				_g = new ContextGlobal( null );
			}
		}


		[Bindable(true), Category("Appearance"), DefaultValue("")]
		public string ResourceConstant 
		{
			get { return _resourceConstant; }
			set { _resourceConstant = value; }
		}


		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public string TitleResourceConstant
		{
			get { return _titleResourceConstant; }
			set	{ _titleResourceConstant = value; }
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public string AltResourceConstant
		{
			get { return _altResourceConstant; }
			set	{ _altResourceConstant = value; }
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public string ImageName
		{
			get { return _imagename; }
			set	{ _imagename = value; }
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public string[] Args 
		{
			get	{ return _args; }
			set	{ _args = value; }
		}
		
		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public string ImageHeight
		{
			get { return _height; }
			set	{ _height = value; }
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public string ImageWidth 
		{
			get	{ return _width; }
			set	{ _width = value; }
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")]
		public string CommunitiesForImage
		{
			get { return _CommunitiesForImage; }
			set { _CommunitiesForImage = value; }
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public string Href 
		{
			get	{ return _href; }
			set	{ _href = value; }
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public bool Bold 
		{
			get	{ return _bold; }
			set	{ _bold = value; }
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public bool Italic 
		{
			get	{ return _italic; }
			set	{ _italic = value; }
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public string Border 
		{
			get	{ return _border; }
			set	{ _border = value; }
		}

		/// <summary> 
		/// Render this control to the output parameter specified.
		/// </summary>
		/// <param name="output"> The HTML writer to write out to </param>
		protected override void Render(HtmlTextWriter output)
		{
			bool IsImage = DetermineTitleType();

			if(Href != null)
			{
				output.Write("<a");
				RenderAttribute(output, Href, HtmlTextWriterAttribute.Href.ToString(), false);
				output.Write(">");
			}
			if(IsImage)
			{
				output.WriteBeginTag(HtmlTextWriterTag.Img.ToString());
				RenderAttribute(output, Image.GetURLFromFilename(ImageName), HtmlTextWriterAttribute.Src.ToString(), false);

				RenderAttribute(output, ImageWidth, HtmlTextWriterAttribute.Width.ToString(), false);
				RenderAttribute(output, ImageHeight, HtmlTextWriterAttribute.Height.ToString(), false);
				RenderAttribute(output, Border, HtmlTextWriterAttribute.Border.ToString(), false);
			}
			else
			{
				output.WriteBeginTag(HtmlTextWriterTag.H1.ToString().ToLower());
			}

			RenderAttribute(output, CssClass, HtmlTextWriterAttribute.Class.ToString(), false);
			RenderAttribute(output, TitleResourceConstant, HtmlTextWriterAttribute.Title.ToString(), true);
			RenderAttribute(output, AltResourceConstant, HtmlTextWriterAttribute.Alt.ToString(), true);

			output.Write(HtmlTextWriter.TagRightChar);

			if(!IsImage){
				if(Bold)
					output.Write("<strong>");
				if(Italic)
					output.Write("<em>");
			}
				
			if(IsImage)
			{
				output.WriteEndTag(HtmlTextWriterTag.Img.ToString());
			}
			else
			{
				RenderResource(output);
				if(!IsImage)
				{
					if(Italic)
						output.Write("</em>");
					if(Bold)
						output.Write("</strong>");
				}
				output.WriteEndTag(HtmlTextWriterTag.H1.ToString().ToLower());
			}
			if(Href != null)
			{
				output.Write("</a>");
			}
		}

		/// <summary>
		/// Returns true if: The domains array is not null, the ImageName is not null, the ImageName is not String.Empty, and the current Domain is in the DomainsList.
		/// </summary>
		private bool DetermineTitleType()
		{
			if(_CommunitiesForImage != null)
			{
				_CommunitiesForImageArray = _CommunitiesForImage.Split(';');

				if (_CommunitiesForImageArray != null && ImageName != null && ImageName != string.Empty)
				{
					string domainAbbreviation = Matchnet.Web.Framework.Util.BrandUtils.GetDomainAbbreviation(_g.Brand.Site.Community.CommunityID);
					foreach (string domain in _CommunitiesForImageArray)
					{
                        if (!String.IsNullOrEmpty(domainAbbreviation.ToLower()) && domain.ToLower().Equals(domainAbbreviation.ToLower()))
						{
							return true;
						}
					}
				}
			}
			
			return false;
		}

		/// <summary>
		/// Reusable method for writing a specified attribute and value into an htmltextwriter
		/// </summary>
		/// <param name="output">The htmltextwriter that the attribute should be written to</param>
		/// <param name="AttributeValue">The resource to be written for the attribute's value</param>
		/// <param name="AttributeTag">The type attribute to be written out</param>
		private void RenderAttribute(HtmlTextWriter output, string AttributeValue, string AttributeTag, bool IsResource)
		{
			if(AttributeValue != null && AttributeValue != String.Empty)
			{
				if(IsResource)
				{
					AttributeValue = _g.GetResource(AttributeValue, this);
				}

				output.WriteAttribute(AttributeTag, AttributeValue, true);
			}
		}

		/// <summary>
		/// Helper method to write the resource to the provided htmltextwriter
		/// </summary>
		/// <param name="output">The writer that the resource should be written to</param>
		private void RenderResource(HtmlTextWriter output)
		{
			if (ResourceConstant != null && ResourceConstant != string.Empty)
			{
				output.Write(_g.GetResource(ResourceConstant, this));
			}
		}
	}
}
