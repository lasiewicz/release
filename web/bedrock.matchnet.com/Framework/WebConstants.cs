using System;

namespace Matchnet.Web.Framework
{
	/// <summary>
	/// Summary description for WebConstants.
	/// </summary>
	public class WebConstants
	{
		#region ENVIRONMENTS
		public const string ENV_DEV = "dev";
		public const string ENV_STAGE = "stage";
		public const string ENV_PROD = "prod";

		#endregion

		#region Member Attribute Name Constants
		public const string ATTRIBUTE_NAME_BIRTHDATE = "Birthdate";
		public const string ATTRIBUTE_NAME_GENDERMASK = "GenderMask";
		public const string ATTRIBUTE_NAME_REGIONID = "RegionID";
		public const string ATTRIBUTE_NAME_MOLREGIONID = "MOLRegionID";
		public const string ATTRIBUTE_NAME_GLOBALSTATUSMASK = "GlobalStatusMask";
		public const string ATTRIBUTE_NAME_DESIREDMINAGE = "DesiredMinAge";
		public const string ATTRIBUTE_NAME_DESIREDMAXAGE = "DesiredMaxAge";
		public const string ATTRIBUTE_NAME_BIRTHCOUNTRYREGIONID = "BirthCountryRegionID";
		public const string ATTRIBUTE_NAME_DISPLAYPHOTOSTOGUESTS = "DisplayPhotosToGuests";
		public const string ATTRIBUTE_NAME_HASNEWMAIL = "HasNewMail";
		public const string ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE = "SubscriptionExpirationDate";
		public const string ATTRIBUTE_NAME_SUBSCRIPTIONSTATUS = "SubscriptionStatus";
		public const string ATTRIBUTE_NAME_MAILBOXPREFERENCE = "MailboxPreference";
		public const string ATTRIBUTE_NAME_HASNEWNOTIFICATION = "HasNewNotificationMask";


		///TODO: Rolled back this change until we change it to site level. Instead of rolling back all changes, just changing back to old attribute names.
		//TT#21001 - Created new brand level marketing attributes.
		//		public const string ATTRIBUTE_NAME_BRANDPROMOTIONID = "BrandPromotionID";
		//		public const string ATTRIBUTE_NAME_BRANDLUGGAGE = "BrandLuggage";
		//		public const string ATTRIBUTE_NAME_BRANDLANDINGPAGEID = "BrandLandingPageID";
		//		public const string ATTRIBUTE_NAME_BRANDBANNERID = "BrandBannerID"; 

		public const string ATTRIBUTE_NAME_BRANDPROMOTIONID = "PromotionID";
		public const string ATTRIBUTE_NAME_BRANDLUGGAGE = "Luggage";
		public const string ATTRIBUTE_NAME_BRANDLANDINGPAGEID = "LandingPageID";
		public const string ATTRIBUTE_NAME_BRANDBANNERID = "BannerID";

        public const string ATTRIBUTE_NAME_COLORCODEQUIZANSWERS = "ColorCodeQuizAnswers";
        public const string ATTRIBUTE_NAME_COLORCODEPRIMARYCOLOR = "ColorCodePrimaryColor";
        public const string ATTRIBUTE_NAME_COLORCODESECONDARYCOLOR = "ColorCodeSecondaryColor";
        public const string ATTRIBUTE_NAME_COLORCODEBLUESCORE = "ColorCodeBlueScore";
        public const string ATTRIBUTE_NAME_COLORCODEYELLOWSCORE = "ColorCodeYellowScore";
        public const string ATTRIBUTE_NAME_COLORCODEWHITESCORE = "ColorCodeWhiteScore";
        public const string ATTRIBUTE_NAME_COLORCODEREDSCORE = "ColorCodeRedScore";
        public const string ATTRIBUTE_NAME_COLORCODEHIDDEN = "ColorCodeHidden";
        public const string ATTRIBUTE_NAME_COLORCODEQUIZCOMPLETEDATE = "ColorCodeQuizCompleteDate";

		#endregion

		#region Legacy Constants

		// todo: these are all legacy references and can and should be removed
		public const string HTTPIM_ENCRYPT_KEY = "8ffez102";
		public const int PAGE_MEMBER_PROFILE_VIEW_PROFILE_MAIN = 7070;
		public const int PT_PROFILE = 4;


		public const int PT_POPUP_ONCE = 2; // this makes it a popup once, all links on this page don't won't keep state (e.g. home page)
		public const int EMAIL_VERIFIED = 4;	// some form of global status mask
		public const int APP_EMAIL = 27;	// application specific ?
		public const string USERNAME_VERIFICATION_ACCOUNT = "VerifyEmail";		// verification email account 
		public const string USERNAME_ACTIVATION_ACCOUNT = "Activations";		// activiation account 
		public const string USERNAME_SUPPORT_ACCOUNT = "comments";		// support account
		public const int PAGE_MEMBER_SERVICES_SUSPEND = 18020;
		public const string USERNAME_RETURN_ACCOUNT = "communications";
		public const string IMPERSONATE_MEMBERID = "impmid";
		public const string IMPERSONATE_BRANDID = "imppid";
		public const int ADMIN_NOTE_MAX_LENGTH = 3900;
		#endregion

        public const string COOKIE_PROPERTY_LANDINGPAGE_TEST_ID = "omnivar";

		#region Session Related Constants
		public const string SESSION_PROPERTY_NAME_MEMBERID = "MemberID";
		public const string SESSIONKEY_RESXHINT_KEY_TOGGLE = "ResxKeyHint"; // hints on off for individual resource keys
		public const string SESSIONKEY_RESXHINT_CTRL_TOGGLE = "ResxCtrlHint"; // hints on off for the whole control
		public const string SESSIONKEY_RESOURCE_HINTS_TOGGLE = "ResourceHintsToggle";
		public const string SESSION_PROPERTY_NAME_BRANDID = "BrandID";
		public const string SESSION_PROPERTY_NAME_BRANDALIASID = "BrandAliasID";
		public const string SESSION_PROPERTY_NAME_LUGGAGE = "Luggage";
		public const string SESSION_PROPERTY_NAME_REFCD = "Refcd";
		public const string SESSION_PROPERTY_NAME_LANDINGPAGEID = "LandingPageID";
        public const string SESSION_PROPERTY_NAME_LANDINGPAGETESTID = "LandingPageTestID";
		public const string SESSION_PROPERTY_NAME_BANNERID = "BannerID_Tracking";	// BannerID is used elsewhere by another session variable.
		public const string SESSION_PROPERTY_NAME_PROMOTIONID = "PromotionID";
	    public const string SESSION_PROPERTY_NAME_EID = "EID";


		public const string SESSION_PROPERTY_NAME_CHECKPROMOTIONFLAG = "CheckPromotionFlag";
		public const string SESSION_PROPERTY_NAME_DOMAINALIASID = "DomainAliasID";
		public const string SESSION_PROPERTY_NAME_MOL_SORT = "MOLSort";
		public const string SESSION_PROPERTY_NAME_EMAILADDRESS = "EmailAddress";
		public const string SESSION_PROPERTY_NAME_CCNO = "ccno";
		public const string SESSION_PROPERTY_NAME_CVCNO = "cvcno";
		public const string SESSION_PROPERTY_NAME_CHECK_ACCTNO = "checkacctno";
		public const string SESSION_PROPERTY_NAME_CHECK_ROUTINGNO = "checkrtgno";
		public const string SESSION_PROPERTY_NAME_CHECK_DLNO = "checkdlno";
		public const string SESSION_PROPERTY_NAME_VIEWED_SUBSCRIPTION = "viewedsubscription";	// for enforcing viewing of 5dft from popup blockers
		public const string SESSION_PROPERTY_NAME_IsraeliID = "IsraeliID";
		public const string SESSION_PROPERTY_NAME_SEARCH_TYPE = "SearchType";

		public const string SESSION_PROPERTY_NAME_ENC_MEMBERID = "EncryptedMemberId";
		public const string SESSION_PROPERTY_NAME_VIDEO_CURRENT_ORDER = "VideoPageCurrentVideoOrder";
		public const string SESSION_PROPERTY_NAME_VIDEO_ISPLAYLIST = "VideoPageIsPlayListMode";


		public const string SESSION_PROPERTY_NAME_BACKTORESULT_URL = "BackToResultURL"; //Back to Results URL, used by ViewProfile and all pages that lin
		public const string SESSION_PROPERTY_NAME_HOTLIST_ACTION = "HotlistAction";
		public const string SESSION_PROPERTY_NAME_MATCHLIST_ORDINAL_TRACKER = "MatchlistOrdinalTracker"; //Keeps track of last ordinal displayed on homepage for matches

		public const string SESSION_PROPERTY_NAME_HAS_VALID_CC_ON_FILE = "HasValidCCOnFile";
        public const string SESSION_PROPERTY_NAME_HIDE_ANNOUNCEMENT_MESSAGE = "HideAnnouncement"; //Hides announcement message for life of a user's session

        public const string SESSION_MOBILE_SETUP_START = "MobileSetupStart";
        public const string SESSION_MOBILE_SETUP_COMPLETE = "MobileSetupComplete";
        public const string SESSION_MOBILE_SETUP_SOURCE = "MobileSetupSource";

        public const string SESSION_PHOTO_REQUIRE_TEMP_EXEMPT = "PhotoReqTempExempt";
        public const string SESSION_NOTIFICATION_MESSAGE = "NotificationMsg";

        public const string SESSION_ADV_SEARCH = "AdvSearchPref";
        public const string SESSION_MOL_SEARCH = "MolSearchPref";
		
        public const string LAST_SAVED_VISITOR_SESSIONDETAILS_SESSION_KEY = "LastSavedVisitorSessionDetailsSessionKey";
        public const string LAST_SAVED_MEMBER_SESSIONDETAILS_SESSION_KEY = "LastSavedMemberSessionDetailsSessionKey";
        public const string SESSION_TRACKING_ID = "SessionTrackingID";
        public const string SESSION_PROPERTY_NAME_CLIENTIP = "ClientIP";
        public const string SESSION_PROPERTY_NAME_REFERRALURL = "ReferralURL";
        public const string SESSION_PROPERTY_NAME_REGISTRATIONDATE = "RegistrationDate";

	    public const string SESSION_PROPERTY_PUSH_FLIRTS_OVERLAY_SHOWN_COUNT = "PushFlirtsShowCount";
        public const string SESSION_PROPERTY_SECRET_ADMIRER_MODAL_SHOW_COUNT = "SecretAdmirerModalShowCount";

	    public const string SESSION_PROPERTY_LAST_SUB_PROMOID = "LastSubPromoID";
	    public const string SESSION_PROPERTY_LAST_SUB_PRTID = "LastSubPRTID";
        public const string SESSION_PROPERTY_ORIGINAL_SUB_PRTID = "OriginalSubPRTID";

	    public const string SESSION_PROPERTY_NUDGE_ABOUTME_LATER = "updateAboutMeLater";
        public const string SESSION_PROPERTY_NUDGE_PHOTO_LATER = "updatePhotoLater";
        public const string SESSION_PROPERTY_PLEDGE_FRAUD_LATER = "updatePledgeToCounterFroudLater";

        

	    public const string SESSION_PROPERTY_REG_SCENARIO_NAME = "RegScenarioName";

	    public const string SESSION_STEALTH_LOGON_ADMIN_MEMBER_ID = "StealthLogonAdminMemberID";
        public const string SESSION_GOOGLE_PIXEL_FIRED = "GooglePixelFired";
        public const string SESSION_FACEBOOK_PIXEL_FIRED = "FacebookPixelFired";

		#region Subscription Admin related session keys
        public const int COOKIE_MULTI_PART = 6; // This should hold even a fat ass promo.
		public const string SESSION_TEMP_PROMO_OBJECT = "TempPromo"; // Now stored in cookies.
		public const string SESSION_TARGET_BRAND_ID = "SubAdminTargetBrandID";
		public const string SESSION_PLAN_FILTERS = "PlanFilters";
		public const string SESSION_PLAN_TO_INSERT = "PlanToInsert";
		public const string SESSION_PROMO_ORDER_PROMO_LIST = "PromoOrderPromoList";
		public const string SESSION_PROMO_ORDER_PAYMENTTYPE = "PromoOrderPaymentType";
        public const string SESSION_PROMO_ORDER_PROMOTYPE = "PromoOrderPromoType";
		public const string SESSION_TEMP_PROMO_SEARCH_CRITERIA = "TempPromoSearchCriteria";
		public const string SESSION_PROMO_SEARCH_CRITERIA_BRANDID = "SrchCritBrandID";
		public const string SESSION_PROMO_SEARCH_CRITERIA_PROMOID = "SrchCritPromoID";
		public const string SESSION_PROMO_SEARCH_CRITERIA_PROMONAME = "SrchCritPromoName";
		public const string SESSION_PROMO_SEARCH_CRITERIA_PAYMENTTYPE = "SrchCritPaymentType";
		public const string SESSION_PROMO_SEARCH_CRITERIA_PROMOTYPEID = "SrchCritPromoTypeID";
		public const string SESSION_RESOURCE_TEMPLATE_ID = "ResourceTemplateID";
		public const string SESSION_RESOURCE_TEMPLATE_TYPE = "ResourceTemplateType";
		public const string SESSION_CREATE_PROMO_CURRENT_STEP = "CreatePromoCurrentStep";
		public const string SESSION_SUB_ADMIN_PREVIOUS_PAGE = "SubscriptionAdminPreviousPage";
		public const string SESSION_SUB_ADMIN_MESSAGE = "SubAdminMsg";
		public const string SESSION_MIXED_COLUMN_MODE = "IsMixedColMode";
		#endregion
		#endregion

		#region HTML Related Constants
		public const string HTML_FORM_ID = "aspnetForm";
		#endregion



		#region Temporary
		//todo: temporary
		public const int TEMP_TRANSLATION_ID = 2;
		#endregion

		#region JRewards status enum
		public enum JRewardsMembershipStatus
		{
			None,
			NeverMember,
			PendingMember,
			ActiveMember,
			RecentExMember,
			OldExMember,
			FTSFailedMembership,
			RecentExFailedMembership,
			OldExFailedMembership
		};
		#endregion

		#region JDate redesign

		public const string SESSION_LAYOUT_OVERRIDE = "LayoutOverride";
		public const string LAYOUT_OVERRIDE_PAGES = "pages";
		public const string LAYOUT_OVERRIDE_NONE = "none";
		public const string LAYOUT_OVERRIDE_ALL = "all";

		public const string OMNITURE_EVAR16_A = "UI Control";
		public const string OMNITURE_EVAR16_B = "UI Redesign";
		public const string ACTION_CALL_PAGE = "ActionCallPage";
		public const string ACTION_CALL_PAGE_DETAIL = "ActionCallPageDetail";
		public const string ACTION_CALL = "Action";
		public const string ACTION_LINK = "ActionLink";


		public enum Action : int
		{
			None = 0,
			Email,
			HotList,
			ClickY,
			ClickM,
			ClickN,
			IM,
			Flirt,
			ViewProfile,
			ProfilePhoto,
			ProfileName,
			ECard,
			MsgDeleteTop,
			MsgBlockTop,
			MsgMarkTop,
			ReplyTop,
			ViewMemberButton,
			EmailMeNowButton,
            ViewMemberLink,
            ViewMorePhotos,
            ViewMoreLink,
            ViewMoreButton,
            ViewMoreImageLink,
            ViewPageLink,
            ViewPageButton,
            ViewPageTitle,
            ViewPageImageLink,
            EditProfile,
            ManagePhotos,
            HotlistButton,
            EmailMe
		}
		#endregion
		#region Forced Email
		public const string OMNITURE_EVAR22 = "Registered";
		public const string OMNITURE_EVAR15_UNVERIFIED = "Unverified";
		public const string OMNITURE_EVAR15_VERIFIED = "Verified";
		public const string OMNITURE_SPROP4_UNVERIFIED = "Unverified";
		public const string OMNITURE_SPROP4_VERIFIED = "Verified";
		#endregion
		#region Community ID enum
		// todo: remove or clean up if possible
		public enum COMMUNITY_ID : int
		{
			AmericanSingles = 1,
			Glimpse = 2,
			JDate = 3,
			Corp = 8,
			Cupid = 10,
			College = 12,
			Spark = 17,
			Mingle = 20,
            ItalianSinglesConnection=21,
            InterRacialSingles=22,
            BBWPersonalsPlus = 23,
            BlackSingles=24,
            ChristianMingle=25

		}
		#endregion

		#region App ID

		public enum APP
		{
			None = 0,
			Home = 1,
			Logon = 2,
			Subscription = 3,
			BillingInformation = 4,
			MemberProfile = 7,
			Chat = 12,
			Article = 13,
			ContactUs = 14,
			SendToFriend = 16,
			MemberServices = 18,
			IM = 19,
			Search = 22,
			MembersOnline = 23,
			HotList = 26,
			Email = 27,
			Events = 28,
			Termination = 32,
			Captcha = 50,
			Tease = 51,
			MemberPhotos = 52,
			Error = 53,
			Offer = 62,
			Telephony = 63,
			Resource = 507,
			MemberSearch = 22,
			Report = 523,
			FreeTextApproval = 525,
			MemberList = 26,
			EditProfile = 539,
			LookupProfile = 544,
			PixelAdministrator = 545,
			Options = 549,
			PhotoGallery = 564,
			Fun = 561,
			QuickSearch = 565,
			Toolbar = 566,
			Video = 568,
			MatchMeter = 569,
			FrameNav = 550,
			SubAdmin = 570,
			DNE = 572,
            Mobile = 575,
            ColorCode = 577,
            QuestionAnswer = 578,
            AdvancedSearch = 582,
            UserNotifications=583,
            MemberLike=590
		}
		#endregion

		#region PageIDs

		public enum PageIDs
		{
			SubscribeByDirectDebit = 9,
			WhatIsCVC = 10,
			Renewal = 20,
			OtherWaysToPay = 21,
			MemberPhotoFile = 23,
			FreeTrialPhotoUpload = 27,
			FreeTrial = 28,
			FreeTrialSubscribe = 31,
			FreeTrialRedirect = 32,
			SubscribeRedirect = 36,
			RegistrationSubscribe = 37,
			StaticResults = 100,
			Default = 1000,
			Splash = 1030,
			CityVerify = 1031,
			Error = 1040,
			MessageBoardRedirect = 1050,
			JpegImage = 1060,
			JpegImageTest = 1070,
			Subscribe = 3000,
			SubscribeByCheck = 3020,
			History = 3060,
			Terminate = 3090,
			AdminAdjust = 3120,
			AdminRemovePaymentInfo = 3140,
			GenPromoPopup = 3200,
			PaymentProfile = 3300,
			MemberPhotoUpload = 4000,
			AttractionBar = 7070,
			AttractionBarSave = 7071,
			ClickPopupHelp = 7072,
			ViewProfile = 7080,
			ProfileUnavailable = 7081,
			RegistrationStep1 = 7090,
			RegistrationStep2 = 7100,
			RegistrationStep3 = 7110,
			RegistrationStep4 = 7120,
			RegistrationStep5 = 7130,
			ChangeEmail = 7180,
			RegistrationWelcome = 7200,
			RegistrationCompletion = 7300,
			LandingPageHolder = 7400,
			LandingPageHolderStatic = 7401,
			AstroView = 7500,
			Details = 10000,
			ChatRoom = 12000,
			ChatRoomDefault = 12010,
			upChat = 12030,
			FAQMain = 13020,
			AlbumSetting = 15010,
			EditAlbum = 15020,
			MemberServices = 18000,
			LookupProfile = 18010,
			Plan = 18130,
			DisplaySettings = 18150,
			VerifyEmailNag = 18170,
			Settings = 19040,
			IMNotify = 19041,
			EMailNotify = 19042,
			PromoNotify = 19043,
			MiniProfile = 19090,
			Header = 19100,
			Left = 19110,
			Input = 19120,
			HttpInstantMessenger = 19130,
			Close = 19140,
			CheckIM = 19170,
			MeetMeOnline = 19200,
			Navigation = 19210,
			Main = 19220,
			BuddyList = 19230,
			Send = 19310,
			Recv = 19320,
			InstantCommunicator = 19400,
			ICXML = 19410,
			SearchResults = 22000,
			SearchPreferencesNew = 22010,
			SearchPreferences = 22040,
			SearchTest = 22050,
			GalleryExitPopup = 22060,
			SavedSearches = 22070,
			JSSearchResults = 22080,
			SearchResultsExplain = 22090,
			MembersOnline = 23000,
			MapSearch = 23010,
			View = 26000,
			CategoryEdit = 26010,
			ManageListCategory = 26020,
			Edit = 26050,
			IconList = 26061,
			CategoryDelete = 26070,
			MailBox = 27000,
			TeaseSentAlready = 27001,
			TeaseSent = 27002,
			ViewMessage = 27020,
			FolderSettings = 27030,
			Compose = 27060,
			MailToList = 27070,
			Tease = 27090,
			TeaseTooMany = 27110,
			MessageSent = 27130,
			MessageSettings = 27140,
			Unavailable = 27150,
			TeaseBlocked = 27160,
			Events = 28000,
			CityList = 29000,
			CupidQuest = 549030,
			PhotoGallery = 549061,
			TrialPay = 549060,
			QuickSearch = 549062,
			QuickSearchResults = 549063,
            AdvanceSearch = 549249,
            AdvanceSearchResults = 549250,
			ToolbarDownload = 549101,
			ToolbarThankYou = 549102,
			PremiumServicesSettings = 549103,
			YouTubeMain = 549120,
			AutoRenewalSettings = 549122,
			MatchMeterMatches = 549126,
			VideoCenter = 549139,
			VideoEpisodeCenter = 549141,
			MiniSearch = 70010,
			SMSAlert = 18180,
			SMSAlertsSettings = 549144,
			KeywordSearch = 549159,
			KeywordSearchResults = 549158,
			Splash20 = 549151,
			ArticleView = 13010,
			ReverseSearch = 549167,
            MemberPhotoEdit = 549200,
            JDateMobile = 549169,
            ColorCodeLanding = 549220,
            ColorCodeQuizIntro = 549221,
            ColorCodeQuiz1 = 549222,
            ColorCodeQuiz2 = 549223,
            ColorCodeQuizConfirmation = 549224,
            ColorCodeAnalysis = 549225,
            ColorCodeComprehensiveAnalysis = 549226,
            ChemistrySettings = 549227,
            ColorCodeSendToFriend = 549229,
            Question = 549234,
            QuestionAnswerHome = 549267,
            Registration = 549150,
            DontGo = 549277,
            SlideShow = 549256,
            Notifications = 549252,
            ECard = -1000 //Non-site page
		}

		#endregion


		#region Site ID enum
		// todo: clean this up/remove if possible
		public enum SITE_ID : int
		{
			JDateCoIL = 4,
			MatchnetUK = 6,
			DateCA = 13,
			Cupid = 15,
			Spark = 100,
			AmericanSingles = 101,
			Glimpse = 102,
			JDate = 103,
			JDateFR = 105,
			JDateUK = 107,
			Matchnet = 108,
			CollegeLuv = 112,
            JewishMingle = 9171,
            NRGDating = 19,
            ItalianSinglesConnection=9161,
            BBW=9041,
            BlackSingles = 9051
		}
		#endregion

		#region BrandID enum
		// todo: clean this up/remove if possible
		public enum BRAND_ID : int
		{
			AmericanSingles = 1001,
			Glimpse = 1002,
			JDate = 1003,
			JDateFR = 1105,
			JDateCoIL = 1004,
			JDateUK = 1107,
			MatchnetUK = 1006,
			DateCA = 1013,
			Cupid = 1015,
			Matchnet = 10008,
			CollegeLuv = 10012,
			Spark = 1017,
			Nana = 1933,
			JewishMingle = 91710,
            NRGDating = 1019
		}
		#endregion

		#region attribute option enums
		[Flags]
		public enum AttributeOptionHideMask : int
		{
			HideSearch = 1,
			HideHotLists = 2,
			HideMembersOnline = 4,
			HidePhotos = 8
		}
		public enum ForcedBlockingMask
		{
			NotBlocked = 0,
			BlockedAfterReg = 1,
			BlockedAfterEmailChange = 2,
			HideFromSearch = 8,
			HideFromMOL = 16
		}

		public enum EmailVerificationSteps
		{
			None = 0,
			AfterRegistration = 1,
			AfterEmailChange = 2

		}

        public enum Custody
        {
            Blank= 0,
            NoKids = 2,
            FarAway = 4,
            Sometimes = 8,
            DontLiveWithMe = 16,
            SomeLiveWithMe = 32,
            WeekendsOnly = 64,
            LiveWithMe = 128
        }

		#region CUPID NEW BILLING SYSTEM - Promotional WOMEN FREE flags
		[Flags]
		public enum CupidWomenFreeFlags : int
		{
			DEFAULT_NoChange = 0,
			GenderLock = 1,
		}
		#endregion

		#region MatchMeter Application Settings
		[Flags]
		public enum MatchMeterFlags : int
		{
			DEFAULT_NoChange = 0,
			EnableApp = 1,
			ShowBestMatches = 2,
			PayFor1On1 = 4,
			PayForBestMatches = 8,
            PremiumFor1On1 = 16,
            PremiumForBestMatches = 32
		}
		#endregion

		[Flags]
		[Obsolete("Please use Matchnet.Member.ValueObjects.AttributeOptionMailboxPreference")]
		public enum AttributeOptionMailboxPreference
		{
			IncludeOriginalMessagesInReplies = 1,
			SaveCopiesOfSentMessages = 2
		}
		#endregion

		#region URL Parameter Names
		#region MPR-4,5 NEW LANDING PAGES
		public const string URL_PARAMETER_NAME_REGLANDINGPAGEID = "reglpid";
		#endregion
        public const string URL_PARAMETER_NAME_VIP_STANDARD_MAIL_MASKED = "vsmm";
		public const string URL_PARAMETER_NAME_LANDINGPAGEID = "lpid";
		public const string URL_PARAMETER_NAME_BANNERID = "bid";
		public const string URL_PARAMETER_NAME_BRANDALIASID = "plid";
		public const string URL_PARAMETER_NAME_AID = "aid";
		public const string URL_PARAMETER_NAME_PID = "pid";
		public const string URL_PARAMETER_NAME_SID = "sid";
		public const string URL_PARAMETER_NAME_LUGGAGE = "Luggage";
		public const string URL_PARAMETER_NAME_PLANID = "PlanID";
		public const string URL_PARAMETER_NAME_DOMAINALIASID = "DomainAliasID";
		public const string URL_PARAMETER_NAME_PROMOTIONID = "PromotionID";
		public const string URL_PARAMETER_NAME_LGID = "LGID";
		public const string URL_PARAMETER_NAME_REFCD = "Refcd";
		public const string URL_PARAMETER_NAME_PRM = "PRM";
		public const string URL_PARAMETER_NAME_LAYOUTTEMPLATEID = "LayoutTemplateID";
		public const string URL_PARAMETER_NAME_ENTRYPOINT = "EntryPoint";
		public const string URL_PARAMETER_NAME_PREMIUM_ENTRYPOINT = "PremEntPoint";
		public const string URL_PARAMETER_NAME_SHOWTITLE = "ShowTitle";
		public const string URL_PARAMETER_NAME_HIDELEFTNAV = "HideLeftNav";
		public const string URL_PARAMETER_NAME_SEARCHINITTAB = "SearchInitTab";
		public const string URL_PARAMETER_NAME_SEARCHORDERBY = "SearchOrderBy";
		public const string URL_PARAMETER_NAME_SEARCHTYPEID = "SearchTypeID";
		public const string URL_PARAMETER_NAME_COUNTRYREGIONID = "CountryRegionID";
		public const string URL_PARAMETER_NAME_FIVEDFT = "FiveDFT";
		public const string URL_PARAMETER_NAME_LOGON_PAGE_FLAG = "lgn";
        public const string URL_PARAMETER_NAME_SITEID = "siteID"; // formerly QSPARAM_SITEID = "?siteID=" used to show site specific temporarily unavailable images
        public const string URL_PARAMETER_NAME_TRANID = "tranID";
        public const string URL_PARAMETER_NAME_ORDERID = "orderID";
		public const string URL_PARAMETER_NAME_MEMBERID = "MemberID"; //for viewprofile
		public const string URL_PARAMETER_NAME_SPARKWS_CONTEXT = "SparkWS"; // = true if it is SparkWS context - for API / SparkWS project.
		public const string URL_PARAMETER_NAME_SPARKWS_CYPHER_OUT = "SPWSCYPHER"; // carries a cypher on a redirect from login in web service API context.
		public const string URL_PARAMETER_NAME_DELETEKEY = "DeleteKey";
		public const string URL_PARAMETER_NAME_RESOURCEFILENAME = "ResourceFileName";
		public const string URL_PARAMETER_NAME_RESOURCEFILEFULLPATH = "ResourceFileFullPath";
		public const string URL_PARAMETER_NAME_IMAGEFILENAME = "ImageFileName";
		public const string URL_PARAMETER_NAME_CATEGORYID = "CategoryID";
		public const string URL_PARAMETER_NAME_MEMBERMAILID = "MemberMailID";
		public const string URL_PARAMETER_NAME_ORDINAL = "Ordinal";
		public const string URL_PARAMETER_NAME_PROFILETAB = "ProfileTab"; //represents the active profile tab to load on member profile page
		public const string URL_PARAMETER_NAME_ADMINADJUSTTAB = "AdminAdjustTab"; //represents the active admin adjust tab to load
		public const string URL_PARAMETER_NAME_OMNITURE_NAVPOINT = "navpoint"; //used by our omniture code to track navigation in site 20
        public const string URL_PARAMETER_NAME_ACCOUNTHISTORYTAB = "AccountHistoryTab"; //represents the active admin adjust tab to load
		public const string URL_PARAMETER_NAME_OMNITURE_NAVVALUE = "navvalue"; //used by our omniture code to provide value for nav tracking in site 20
		public const string URL_PARAMETER_NAME_USE_BACKTORESULT = "btr"; //specifies true/false to indicate whether results pages should load results based on URL parameters
		public const string URL_PARAMETER_NAME_FROM_PROFILE_WITH_BTR_PREV = "dbtr"; //specifies true/false to indicate whether to page navigated from new profile page with back to results and previous links
		public const string URL_PARAMETER_NAME_AJAX = "ajxmode"; //specifies true/false to indicate whether page is requested via manual ajax
		public const string URL_PARAMETER_NAME_HERO_PROFILE = "hero"; //used in hero profile for clickthrough tracking to view profile page
		public const string URL_PARAMETER_NAME_FORCE_ENTRY_FORM = "FDEF";
		public const string URL_PARAMETER_NAME_SELECTED_SUBSCRIPTION_PAYMENT_INFO = "SPIS";
		public const string URL_PARAMETER_NAME_ADMIN_ONLY_PROMO = "aop"; //indicates if SubscribeNew should display admin only promo
        public const string URL_PARAMETER_NAME_QUESTION_ID = "questid";
	    public const string URL_PARAMETER_FACEBOOK_REGISTRATION_SITE_ID = "fbregsid";
        public const string URL_PARAMETER_FACEBOOK_REGISTRATION = "fbreg";
        public const string URL_PARAMETER_MINISLIDESHOW_MINAGE = "msmia";
        public const string URL_PARAMETER_MINISLIDESHOW_MAXAGE = "msmaa";
        public const string URL_PARAMETER_MINISLIDESHOW_POSTAL = "mspos";
        public const string URL_PARAMETER_MINISLIDESHOW_REGIONID = "msreg";
        public const string URL_PARAMETER_MINISLIDESHOW_SEARCHTYPE = "msst";
        public const string URL_PARAMETER_MINISLIDESHOW_AREACODE_LIST = "msacl";
        public const string URL_PARAMETER_SLIDESHOW_DISPLAY_TYPE = "sdt";
        public const string URL_PARAMETER_SLIDESHOW_OUTER_CONTAINER_DIV = "socd";
        public const string URL_PARAMETER_SLIDESHOW_OMNITURE_PAGE_NAME = "sopn";
        public const string URL_PARAMETER_SLIDESHOW_MODE = "slideshowmode";
	    public const string URL_PARAMETER_SLIDESHOW_SHOW_UPDATED_WIDE_STYLE = "slideshowsuws"; 
        public const string URL_PARAMETER_PURCHASEREASONTYPEID = "prtid";
        public const string URL_PARAMETER_REDIRECT_FOR_IPBLOCK = "rbl";
        public const string URL_PARAMETER_BETA_TEST_ENABLE = "EnableBeta";
        public const string URL_PARAMETER_BETA_TEST_DISABLE = "DisableBeta";
        public const string URL_PARAMETER_STATIC_REGISTRATION_FORM = "staticregform";
        public const string URL_PARAMETER_EID = "eid";
	    public const string URL_MESSAGE_RECIPIENT_MEMBER_ID = "mrmid";
	    public const string URL_REFERRER_TO_PASS = "referrer";
        public const string URL_PARAMETER_MATCH_RATING = "mmrating";
	    public const string URL_PARAMETER_REPORT_ABUSE = "reportabuse";
        public const string URL_PARAMETER_MAILTYPE = "mailtype";

		#endregion

		#region External URL
		public const string HTTP_ASTROLOGY_URL = "http://www.zdki.us/bin/taReportsw.dll/MakeReport?";
		#endregion

		#region Ad Regions
		public const string ADREGION_NONE = "none";
		public const string ADREGION_S1 = "SI";
		public const string ADREGION_S2 = "S2";
		public const string ADREGION_L1 = "L1";
		public const string ADREGION_M1 = "M1";
		public const string ADREGION_M2 = "M2";
		public const string ADREGION_M3 = "M3";
		public const string ADREGION_M4 = "M4";
		public const string ADREGION_A1 = "A1";
		public const string ADREGION_A2 = "A2";
		public const string ADREGION_A3 = "A3";
		public const string ADREGION_A4 = "A4";
		public const string ADREGION_V1 = "V1";
		public const string ADREGION_C1 = "C1";
		#endregion

		#region EncKeys
		public const string XOR_SALT_ADD_VALUE = "42";
		public const string SPARK_WS_CRYPT_KEY = "A877C90D";// used to encrypt session id for tranfer over to SparkWS.
		public const string OUTBOUND_EMAIL_CRYPT_KEY = "A3G6K2S9"; // This key must be synchonized to the one on the SQL UDF MPR-675

		#endregion

		#region SETTINGS
		public const string SETTING_ENABLE_POPUP_VIEW_PROFILE_MASK = "ENABLE_POPUP_VIEW_PROFILE_MASK";
		public const string SETTING_ENABLE_TABBED_VIEW_PROFILE = "ENABLE_TABBED_VIEW_PROFILE";
		public const string SETTING_TABBED_PROFILE_PHOTO_NAVIGATION_MODE = "TABBED_PROFILE_NAVIGATION_MODE";
		public const string SETTING_TABBED_PROFILE_PHOTO_DISPLAY_MODE = "TABBED_PROFILE_DISPLAY_MODE";
		public const string SETTING_ENABLE_RENEWAL_TO_SUBSCRIBE_REDIRECT = "ENABLE_RENEWAL_TO_SUBSCRIBE_REDIRECT";
		public const string SETTING_ABTESTING_JDATEREDESIGN = "JDATEREDESIGN";
		#endregion

		#region Omniture Constants
		public const string OMNITURE_EVAR29_YES = "Y";
		public const string OMINTURE_EVAR29_NO = "N";
		public const string OMNITURE_EVAR44_MEMBERID = "MemberID{0}";
		#endregion

		public enum LayoutVersions
		{
			versionNarrow = 1,
			versionWide = 2

		}
		#region MOLPreferences
		public string MOL_REGIONID = "MOLRegionID";
		public string MOL_MIN_AGE = "MembersOnlineAgeMin";
		public string MOL_MAX_AGE = "MembersOnlineAgeMax";
		public string MOL_LANGUAGE = "MembersOnlineLanguage";
		public string MOL_GENDER = "MembersOnlineGender";


		#endregion


        #region misc
        public string JSCRIPT_SLIDESHOW_VOTE_NO_SUBMIT = "javascript:recordYNMVoteAndGetNewSlideshowProfile('{0}','{1}', '{2}', '{3}', 'true', '{4}');return false;";
        public string JSCRIPT_SLIDESHOW_VOTE = "javascript:recordYNMVoteAndGetNewSlideshowProfile('{0}','{1}', '{2}', 'false', 'false','{3}');";

        public string JSCRIPT_MINISLIDESHOW_VOTE_NO_SUBMIT = "javascript:recordYNMVoteAndGetNewMiniSlideshowProfile('{0}','{1}', '{2}', '{3}', 'true');return false;";
        public string JSCRIPT_MINISLIDESHOW_VOTE = "javascript:recordYNMVoteAndGetNewMiniSlideshowProfile('{0}','{1}', '{2}', 'false', 'false');";


        public string JSCRIPT_SLIDESHOW_VOTE_NO_SUBMIT_EX = "javascript:recordYNMVoteAndGetNewSlideshowProfileEx('{0}','{1}', '{2}', '{3}', 'true','{4}');return false;";
        public string JSCRIPT_SLIDESHOW_VOTE_EX = "javascript:recordYNMVoteAndGetNewSlideshowProfileEx('{0}','{1}', '{2}', 'false', 'false','{3}');";

        #endregion

        #region Membase
        public const string MEMBASE_SETTING_CONSTANT = "USE_MEMBASE_CACHE";
        public const string MEMBASE_WEB_SETTING_CONSTANT = "USE_MEMBASE_CACHE_FOR_WEB";
        public const string MEMBASE_WEB_BUCKET_NAME = "webcache";

        #endregion

        public enum MailAccessExceptionRule : int
		{
			defaultRule = 0,
			defaultCountry = 1,
			defaultLanguage = 2,
            noRestriction = 3
		}
		public enum RegistrationModeType : int
		{
			defaultMode = 1,
			wizard
		}
		public enum MailAccessArea : int
		{
			mailBox = 0,
			viewMessage = 1,
			compose = 2,
            IMHistory = 4
		}

		public enum AmadesaScriptArea : int
		{
			amadesaHome = 1,
			amadesaEVerify = 2,
			amadesaReg = 4,
			amadesaRegConfirm = 8,
			amadesaSub = 16,
			amadesaSubConfirm = 32,
			amadesaViewProfile = 64,
			amadesaDefault = 128,
			amadesaTermination = 256,
			amadesaTerminationFinal = 512,
			amadesaRegWelcome = 1024

		}

        public const int ColorCodeSearch = 5;
        public const string CONNECT_COOKIE_NAME = "SPWSCYPHER";

	    public const string REQUEST_SCOPED_DTO_KEY = "__REQUEST_SCOPED_DTO_KEY__";
	}
}
