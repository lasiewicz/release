using System;
using System.Web;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Framework
{
    /// <summary>
    /// Summary description for VisitorLimitHelper.
    /// </summary>
    public class VisitorLimitHelper
    {
        public static string SEARCHRESULTS_VIEW_CHANGED_SESSION_KEY = "SearchResultsViewFormatChanged";
        public const string SESSIONKEY_VISITORLIMIT_DONOTOVERWRITESEARCHPREFS = "VLKeepSearchPrefs";
        private const string SESSIONKEY_VISITORLIMIT_FULLPROFILE = "VLFullProfile";
        private const string QSPARAM_MAILTYPEID = "MailTypeID";

        public enum VisitorLimitTypeEnum
        {
            Search,
            MOL,
            FullProfile
        }

        public static void ResetVisitorLimits(ref ContextGlobal g)
        {
            g.Session.Remove(SESSIONKEY_VISITORLIMIT_FULLPROFILE);
        }

        /// <summary>
        /// This will translate the ResultContextType to a VisitorLimitType.
        /// </summary>
        /// <param name="g"></param>
        /// <param name="ResultContextType"></param>
        public static void CheckVisitorLimit(ref ContextGlobal g, ResultContextType ResultContextType, int Ordinal, int StartRow)
        {
            bool resetFlag = false;

            if (Ordinal == 1)
                resetFlag = true;

            switch (ResultContextType)
            {
                case Ui.BasicElements.ResultContextType.SearchResult:
                    CheckVisitorLimit(ref g, VisitorLimitTypeEnum.Search, resetFlag, Ordinal, StartRow);
                    break;
                case Ui.BasicElements.ResultContextType.MembersOnline:
                    CheckVisitorLimit(ref g, VisitorLimitTypeEnum.MOL, resetFlag, Ordinal, StartRow);
                    break;
                case Ui.BasicElements.ResultContextType.QuickSearchResult:
                    CheckVisitorLimit(ref g, VisitorLimitTypeEnum.Search, resetFlag, Ordinal, StartRow);
                    break;
                case Ui.BasicElements.ResultContextType.PhotoGallery:
                    CheckVisitorLimit(ref g, VisitorLimitTypeEnum.Search, resetFlag, Ordinal, StartRow);
                    break;
            }
        }

        public static void CheckVisitorLimit(ref ContextGlobal g, VisitorLimitTypeEnum VisitorLimitType)
        {
            CheckVisitorLimit(ref g, VisitorLimitType, false, int.MinValue, int.MinValue);
        }

        /// <summary>
        /// NOTE:  Only the Ordinal value for FullProfile is saved in a cookie.  The others are derived from the
        /// Ordinal of the Search Results page.  The reason for this is to limit the Search Results to the FIRST X pages
        /// instead of any X pages.  If we limit it to any X pages, then the user can go to page 1 then page 2 and then
        /// back to page 1 to reset the counter then page 3 then back to page 1, etc.  This would be a loophole for them
        /// to view every page.  There is still a loophole.  If a user views X full profiles on page 1 and then refreshes
        /// the page (thus, reseting the FullProfile cookie value), he will then be able to view X full profiles on page 2.
        /// </summary>
        /// <param name="g"></param>
        /// <param name="VisitorLimitType"></param>
        /// <param name="ResetFlag"></param>
        /// <param name="Ordinal">The current ResultList page.</param>
        /// <param name="PageSize">The number of records per ResultList.</param>
        private static void CheckVisitorLimit(ref ContextGlobal g, VisitorLimitTypeEnum VisitorLimitType, bool ResetFlag, int Ordinal, int StartRow)
        {
            // Only applies to Visitors.
            // NOTE:  For now, we just check for any value for MailTypeID.  In the future, we may want to be more selective
            // about which values we want to filter by.
            if (g.Member == null && (System.Web.HttpContext.Current.Request.QueryString[QSPARAM_MAILTYPEID] == null || System.Web.HttpContext.Current.Request.QueryString[QSPARAM_MAILTYPEID] == string.Empty))
            {
                // If the Ordinal is the first page, then reset all the counters.
                if (ResetFlag)
                {
                    // TT #15790 - check session flag to see whether the list view changed, so we don't reset the VisitorLimit inappropriately
                    if (g.Session[SEARCHRESULTS_VIEW_CHANGED_SESSION_KEY] != null && !g.Session[SEARCHRESULTS_VIEW_CHANGED_SESSION_KEY].Equals(string.Empty))
                    {
                        if (g.Session[SEARCHRESULTS_VIEW_CHANGED_SESSION_KEY] != "ViewChanged")
                        {
                            ResetVisitorLimits(ref g);
                        }

                        // remove the session flag so it does not carry thru and prevent VisitorLimit reset entirely
                        g.Session.Remove(SEARCHRESULTS_VIEW_CHANGED_SESSION_KEY);
                    }
                }

                // Determine what Session key to use.
                string sessionKey = string.Empty;
                string settingKey = string.Empty;
                string startRow = string.Empty;
                switch (VisitorLimitType)
                {
                    case VisitorLimitTypeEnum.Search:
                        settingKey = "VISITORLIMIT_SEARCH";

                        startRow = StartRow.ToString();

                        break;

                    case VisitorLimitTypeEnum.MOL:
                        // Disregard the mol view limit when regoverlay should be displayed
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["regoverlay"]))
                            return;
                        settingKey = "VISITORLIMIT_MOL";
                        break;

                    case VisitorLimitTypeEnum.FullProfile:
                        // Disregard the full profile view limit when regoverlay should be displayed
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["regoverlay"]))
                            return;

                        Ordinal = g.Session.GetInt(SESSIONKEY_VISITORLIMIT_FULLPROFILE, 1);

                        // Add 1 to the Limit cookie.
                        g.Session.Add(SESSIONKEY_VISITORLIMIT_FULLPROFILE, Convert.ToString(Ordinal + 1), SessionPropertyLifetime.Temporary);

                        settingKey = "VISITORLIMIT_FULLPROFILE";
                        break;
                }

                // Determine what the limit value is.
                int limit = Convert.ToInt32(RuntimeSettings.GetSetting(settingKey, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                if (limit >= 0 && Ordinal > limit)
                {
                    string pagePath = GetVisitorPagePath(startRow);

                    g.LogonRedirect(pagePath, "TXT_LOGIN_OR_REGISTER_TO_CONTINUE_SEARCHING");
                }
            }
        }

        public static string GetVisitorPagePath(string startRow)
        {
            string pagePath = System.Web.HttpContext.Current.Items["PagePath"].ToString();

            // Add other params.
            if ((System.Web.HttpContext.Current.Request.QueryString.HasKeys() == true))
            {
                pagePath += "?" + System.Web.HttpContext.Current.Request.QueryString.ToString();

                if (startRow != string.Empty)
                    pagePath += "&" + ResultListHandler.QSPARAM_STARTROW + "=" + startRow;
            }
            else
            {
                if (startRow != string.Empty)
                    pagePath += "?" + ResultListHandler.QSPARAM_STARTROW + "=" + startRow;
            }

            return pagePath;
        }
    }
}
