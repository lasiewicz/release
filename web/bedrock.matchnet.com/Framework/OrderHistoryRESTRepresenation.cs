﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework
{
    public class OrderHistory
    {
        public int AdminUserID { get; set; }
        public string AdminUserName { get; set; }
        public int CallingSystemID { get; set; }
        public int CallingSystemTypeID { get; set; }
        public object ChargebackCaseID { get; set; }
        public int CurrencyID { get; set; }
        public int CustomerID { get; set; }
        public string CustomerIP { get; set; }
        public int Duration { get; set; }
        public int DurationTypeID { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime InsertDateInPST { get; set; }
        public int InternalResponseStatusID { get; set; }
        public string InternalStatusMessage { get; set; }
        public string LastFourAccountNumber { get; set; }
        public List<OrderDetail> OrderDetail { get; set; }
        public int OrderID { get; set; }
        public int OrderReasonID { get; set; }
        public int OrderStatusID { get; set; }
        public List<OrderUserPayment> OrderUserPayment { get; set; }
        public string OriginalSubscriptionStatusBeforeUpsale { get; set; }
        public int OriginalTemplateIDBeforeUpsale { get; set; }
        public int PaymentType { get; set; }
        public int PmotionID { get; set; }
        public int PrimaryPackageID { get; set; }
        public int PromoID { get; set; }
        public int PurchaseReasonTypeID { get; set; }
        public int ReferenceOrderID { get; set; }
        public int RegionID { get; set; }
        public string SecondaryPurchaseReasonTypeIDs { get; set; }
        public double TotalAmount { get; set; }
        public int UPSLegacyDataID { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UserPaymentGuid { get; set; }
    }

    public class OrderDetail
    {
        public double Amount { get; set; }
        public int CurrencyID { get; set; }
        public int DisburseCount { get; set; }
        public int DisburseDuration { get; set; }
        public int DisburseDurationTypeID { get; set; }
        public double DiscountAmount { get; set; }
        public string DiscountCode { get; set; }
        public int DiscountID { get; set; }
        public int DiscountType { get; set; }
        public int Duration { get; set; }
        public int DurationTypeID { get; set; }
        public int GiftTypeID { get; set; }
        public DateTime InsertDate { get; set; }
        public string ItemDescription { get; set; }
        public int ItemID { get; set; }
        public int OrderDetailID { get; set; }
        public int OrderId { get; set; }
        public List<object> OrderTax { get; set; }
        public int OrderTypeID { get; set; }
        public int PackageID { get; set; }
        public int PackageType { get; set; }
        public int PromoID { get; set; }
        public int ReferenceOrderDetailID { get; set; }
        public DateTime UpdateDate { get; set; }
    }

    public class OrderUserPayment
    {
        public int CallingSystemID { get; set; }
        public int CallingSystemTypeID { get; set; }
        public int ChargeID { get; set; }
        public int ChargeStatusID { get; set; }
        public int ChargeTypeID { get; set; }
        public DateTime InsertDate { get; set; }
        public int OrderID { get; set; }
        public DateTime UpdateDate { get; set; }
    }

    public class DiscountTransaction
    {
        public int OrderId { get; set; }
        public double DiscountAmount { get; set; }
        public string DiscountCode { get; set; }
        public int DiscountType { get; set; }
    }
}