﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;

namespace Matchnet.Web.Framework
{
	[
	ToolboxData("<{0}:MultiValidator runat=\"server\" " +
		"ErrorMessage=\"MultiValidator\" "+
		"Text=\"MV{1}\"></{0}:MultiValidator>")
	]
	public class MultiValidator : BaseValidator 
	{
		public enum BasicTypes 
		{
            IntegerType, LetterType, AlphaNumType, PhoneNumberType, StringType, FreeTextType, CreditCardType, PhotoType, CheckBoxType, CheckBoxListType, PostalCode, DecimalType, EmailType, BirthDateType, AgeRangeType, RegionType
        }


		public MultiValidator()
		{
			// A little extra protection to ensure compatiblity with .NET designer...
			if ( Context != null )
			{
				if ( Context.Items["g"] != null )
					_g = (ContextGlobal) Context.Items["g"];
			}		

		}
		
		private bool _ResourcesHaveBeenResolved=false;
		protected ContextGlobal _g;
		private bool _IsRequired=false;
		private string _IsRequiredErrorMessage="*Is required";
		private int _MaximumLength=0;
		private string _MaximumLengthErrorMessage="*Exceeds maximum length";
		private int _MinimumLength=0;
		private string _MinimumLengthErrorMessage="*Does not satisfy minimumn length";
		private string _RequiredTypeRegEx="";
		private string _RequiredTypeErrorMessage="*Wrong type";
		private BasicTypes _RequiredType;
		private bool _ShowPopup = false;
        private bool _CustomizeErrorMessage = true;

		private int _MaxVal = 0;
		private int _MinVal = 0;
		private string _MaxValErrorMessage="*Exceeds maximum value";
		private string _MinValErrorMessage="*Does not satisfy minimum value";
		private string _appendToErrorMessage = "";

		private string _RequiredResourceConstant = "RESOURCE_NOT_FOUND";
		private string _RequiredTypeResourceConstant = "RESOURCE_NOT_FOUND";
		private string _MaximumLengthResourceConstant = "RESOURCE_NOT_FOUND";
		private string _MinimumLengthResourceConstant = "RESOURCE_NOT_FOUND";
		private string _FieldNameResourceConstant = "RESOURCE_NOT_FOUND";
		private string _MaxValResourceConstant = "RESOURCE_NOT_FOUND";
		private string _MinValResourceConstant = "RESOURCE_NOT_FOUND";

        private Control _ResourceControl = null;
        private int _RegionId = Constants.NULL_INT;

        [
        Category("Behavior"),
        DefaultValue(true),
        Description("Setting this to false will not apply any customization of error messages.")
        ]
        public virtual bool CustomizeErrorMessage
        {
            get
            {
                return _CustomizeErrorMessage;
            }
            set
            {
                _CustomizeErrorMessage = value;
            }
        }

		[
		Category("Behavior"),
		DefaultValue(false),
		Description("Whether a client side javascript alert is shown when a validation error occurs.")
		]
		public virtual bool ShowPopup
		{
			get
			{
				return _ShowPopup;
			}
			set
			{
				_ShowPopup=value;
			}
		}

		[
		Category("Behavior"),
		DefaultValue(true),
		Description("Whether a value is required.")
		]
		public virtual bool IsRequired
		{
			get
			{
				return _IsRequired;
			}
			set
			{
				_IsRequired=value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")]
		public string RequiredResourceConstant
		{
			get
			{
				return _RequiredResourceConstant;
			}

			set
			{
				_RequiredResourceConstant = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")]
		public string RequiredTypeResourceConstant
		{
			get
			{
				return _RequiredTypeResourceConstant;
			}

			set
			{
				_RequiredTypeResourceConstant = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")]
		public string MaximumLengthResource 
		{
			get
			{
				return _MaximumLengthResourceConstant;
			}

			set
			{
				_MaximumLengthResourceConstant = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")]
		public string MinimumLengthResourceConstant 
		{
			get
			{
				return _MinimumLengthResourceConstant;
			}

			set
			{
				_MinimumLengthResourceConstant = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")]
		public string MinValResourceConstant 
		{
			get
			{
				return _MinValResourceConstant;
			}

			set
			{
				_MinValResourceConstant = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")]
		public string MaxValResourceConstant 
		{
			get
			{
				return _MaxValResourceConstant;
			}

			set
			{
				_MaxValResourceConstant = value;
			}
		}



		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")]
		public string FieldNameResourceConstant
		{
			get
			{
				return _FieldNameResourceConstant;
			}

			set
			{
				_FieldNameResourceConstant = value;
			}
		}

		[
		Category("Display"),
		DefaultValue(true),
		Description("ErrorMessage for required type")
		]
		public virtual string RequiredTypeErrorMessage
		{
			get
			{
				return _RequiredTypeErrorMessage;
			}
			set
			{
				_RequiredTypeErrorMessage=value;
			}
		}

		[
		Category("Display"),
		DefaultValue(""),
		Description("Append to error message")
		]
		public virtual string AppendToErrorMessage
		{
			get
			{
				return _appendToErrorMessage;
			}
			set
			{
				_appendToErrorMessage = value;
			}
		}


		[
		Category("Behavior"),
		DefaultValue(true),
		Description("Whether a value is required.")
		]
		public virtual BasicTypes RequiredType
		{
			get
			{
				return _RequiredType;
			}
			set
			{
				_RequiredType=value;

				switch (value)
				{
					case BasicTypes.IntegerType:
						_RequiredTypeRegEx = "\\d*";
						break;

					case BasicTypes.CheckBoxListType:
						_RequiredTypeRegEx = "\\d*";
						break;

					case BasicTypes.CheckBoxType:
						_RequiredTypeRegEx = "\\d*";
						break;

					case BasicTypes.LetterType:
						_RequiredTypeRegEx = "[a-zA-Zא-ת ]*";
						break;

					case BasicTypes.AlphaNumType:
						_RequiredTypeRegEx = "([a-zA-Zא-ת ]|\\d)*";
						break;

					case BasicTypes.PhoneNumberType:
						_RequiredTypeRegEx = "(\\d|\\-|\\(|\\)| )*";
						break;

					case BasicTypes.StringType:
						_RequiredTypeRegEx = "^[^<|^>]+$";
						break;

                    case BasicTypes.FreeTextType:
                        _RequiredTypeRegEx = String.Empty;
                        break;

					case BasicTypes.CreditCardType:
						_RequiredTypeRegEx = "(\\d| |\\-)+";
						break;

					case BasicTypes.PhotoType:
						// jpeg, jpg, gif, bmp ( TT15456 - and directory path ) 
						//reverting back to original regex. Validate the extension only, not the path, for FireFox compatibility. 18582
						_RequiredTypeRegEx = @"(.*\.[j/J][p/P][e/E][g/G]$)|(.*\.[j/J][p/P][g/G]$)|(.*\.[g/G][i/I][f/F]$)|(.*\.[b/B][m/M][p/P]$)";
						//_RequiredTypeRegEx = @"^(([a-zA-Z]:)|(\\{2}\w+))(\\(\w[\w .]*\w))*((.*\.[j/J][p/P][e/E][g/G]$)|(.*\.[j/J][p/P][g/G]$)|(.*\.[g/G][i/I][f/F]$)|(.*\.[b/B][m/M][p/P]$))";
						break;

					case BasicTypes.PostalCode:
						_RequiredTypeRegEx = "[0-9]{5}|[a-zA-Z0-9]{3} ?[a-zA-Z0-9]{3}";
						break;

                    case BasicTypes.DecimalType:
                        _RequiredTypeRegEx = "\\d*\\.{0,1}\\d{0,2}";
                        break;
                    case BasicTypes.EmailType:
                        _RequiredTypeRegEx = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
                        break;
                    case BasicTypes.BirthDateType:
                        _RequiredTypeRegEx = String.Empty;
                        break;
                    case BasicTypes.AgeRangeType:
                        _RequiredTypeRegEx = String.Empty;
                        break;
                    case BasicTypes.RegionType:
                        _RequiredTypeRegEx = String.Empty;
                        break;
                    default:
						throw new ArgumentException("Unknown type " + value, "RequiredType");
				}
			}
		}


		[
		Category("Display"),
		DefaultValue(true),
		Description("ErrorMessage for IsRequired")
		]
		public virtual string IsRequiredErrorMessage
		{
			get
			{
				return _IsRequiredErrorMessage;
			}
			set
			{
				_IsRequiredErrorMessage=value;
			}
		}


		[
		Bindable(true),
		Category("Behavior"),
		DefaultValue(0),
		Description("The Maximum length of a valid value.")
		]
		public virtual int MaximumLength
		{
			get
			{
				return _MaximumLength;
			}
			set
			{
				_MaximumLength=value;
			}
		}

		[
		Category("Display"),
		DefaultValue(true),
		Description("ErrorMessage for MaximumLength")
		]
		public virtual string MaximumLengthErrorMessage
		{
			get
			{
				return _MaximumLengthErrorMessage;
			}
			set
			{
				_MaximumLengthErrorMessage=value;
			}
		}


		[
		Bindable(true),
		Category("Behavior"),
		DefaultValue(0),
		Description("The Minimum length of a valid value.")
		]
		public virtual int MinimumLength
		{
			get
			{
				return _MinimumLength;
			}
			set
			{
				_MinimumLength=value;
			}
		}

		[
		Bindable(true),
		Category("Behavior"),
		DefaultValue(0),
		Description("The Minimum value of a valid value.")
		]
		public virtual int MinVal
		{
			get
			{
				return _MinVal;
			}
			set
			{
				_MinVal=value;
			}
		}


		[
		Bindable(true),
		Category("Behavior"),
		DefaultValue(0),
		Description("The Maximum value of a valid value.")
		]
		public virtual int MaxVal
		{
			get
			{
				return _MaxVal;
			}
			set
			{
				_MaxVal=value;
			}
		}

		[
		Category("Display"),
		DefaultValue(true),
		Description("ErrorMessage for MinVal")
		]
		public virtual string MinValErrorMessage
		{
			get
			{
				return _MinValErrorMessage;
			}
			set
			{
				_MinValErrorMessage=value;
			}
		}

		[
		Category("Display"),
		DefaultValue(true),
		Description("ErrorMessage for MaxVal")
		]
		public virtual string MaxValErrorMessage
		{
			get
			{
				return _MaxValErrorMessage;
			}
			set
			{
				_MaxValErrorMessage=value;
			}
		}


		[
		Category("Display"),
		DefaultValue(true),
		Description("ErrorMessage for MinimumLength")
		]
		public virtual string MinimumLengthErrorMessage
		{
			get
			{
				return _MinimumLengthErrorMessage;
			}
			set
			{
				_MinimumLengthErrorMessage=value;
			}
		}

        public Control ResourceControl 
        {        
            get{
                if (this._ResourceControl == null)
                {
                    this._ResourceControl = this;
                }
                return this._ResourceControl;
            }

            set
            {
                this._ResourceControl = value;
            }
        }

        public int RegionId { get { return _RegionId; } set { _RegionId = value; } }

		protected override bool ControlPropertiesValid()
		{
			return (ControlToValidate != null);
		}

		// Main validation entry point
		protected override bool EvaluateIsValid()
		{
			ResolveResources();
			string controlValue = GetControlValidationValue(ControlToValidate);

			if (_RequiredType == BasicTypes.CheckBoxListType)
			{
				return IsValidCheckBoxList();
			}

			if (_RequiredType == BasicTypes.CheckBoxType)
			{
				return IsValidCheckBox();
			}

            if (_RequiredType == BasicTypes.BirthDateType)
            {
                return IsValidBirthDate();
            }

            if (_RequiredType == BasicTypes.AgeRangeType)
            {
                return IsValidAgeRange();
            }

            if (_RequiredType == BasicTypes.RegionType)
            {
                return IsValidRegion();
            }

            if (controlValue == null)
			{
				if (IsRequired)
				{
					Text = IsRequiredErrorMessage;
					return false;
				}
				return true;
			}

			controlValue = controlValue.Trim();
			
			if ( !IsRequired  && controlValue.Length == 0 )
			{
				return true;
			}
			
			if ( IsRequired && controlValue.Length == 0 ) 
			{
				Text = IsRequiredErrorMessage;
				return false;
			}

			int maxLength = MaximumLength;

			if ( (maxLength != 0) && (controlValue.Length > maxLength))
			{
				Text = MaximumLengthErrorMessage;
				return false;
			}

			int minLength = MinimumLength;

			if ( (minLength != 0) && (controlValue.Length < minLength))
			{
				Text = MinimumLengthErrorMessage;
				return false;
			}

			// MaxVal ------------------------------
			int maxVal = MaxVal;

			if ( (maxVal != 0) && (Matchnet.Conversion.CInt(controlValue, MaxVal+1) > maxVal))
			{
				Text = MaxValErrorMessage;
				return false;
			}

			int minVal = MinVal;

			if ( (minVal != 0) && (Matchnet.Conversion.CInt(controlValue, MinVal-1) < minVal))
			{
				Text = MinValErrorMessage;
				return false;
			}
			//-------------------------------------



			if ( _RequiredType == BasicTypes.CreditCardType )
			{
				// Do some more advanced validation for credit card types, else apply the normal logic
				if ( ! IsValidCreditCard(ref controlValue) )
				{
					Text = _RequiredTypeErrorMessage;
					return false;
				}
				else
				{
					// Reset the contents of the target control...
					System.Web.UI.WebControls.TextBox txtBox = (System.Web.UI.WebControls.TextBox)FindControl(ControlToValidate);

					if ( txtBox != null )
					{
						txtBox.Text = controlValue;
						return true;
					}
				}
			}
			else if ( _RequiredTypeRegEx.Length > 0 )
			{
				Regex regex = new Regex(_RequiredTypeRegEx);

				if ( regex.Match(controlValue).Length != controlValue.Length )
				{
					Text = _RequiredTypeErrorMessage;
					return false;
				}
			}

			return true;
		}

        protected bool IsValidAgeRange()
        {
            try
            {
                string[] ids = this.ControlToValidate.Split(',');
                TextBox minAge = (TextBox)this.FindControl(ids[0]);
                TextBox maxAge = (TextBox)this.FindControl(ids[1]);

                //IsValid is true if no values and not required
                if (!IsRequired && string.IsNullOrEmpty(minAge.Text) && string.IsNullOrEmpty(maxAge.Text))
                {
                    return true;
                }

                if (!string.IsNullOrEmpty(minAge.Text) && !string.IsNullOrEmpty(maxAge.Text))
                {
                    int minAgeInt = Conversion.CInt(minAge.Text);
                    int maxAgeInt = Conversion.CInt(maxAge.Text);
                    if ((minAgeInt >= 18 && minAgeInt < 99)
                        && (maxAgeInt >= 18 && maxAgeInt < 99)
                        && (maxAgeInt > minAgeInt))
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex){}
            Text = _g.GetResource("TXT_INVALID_AGE_RANGE", null);
            return false;
        }

        protected bool IsValidRegion()
        {
            try
            {
                if (Constants.NULL_INT != RegionId && RegionId > 0)
                {
                   return true;
                }
            }
            catch{}
            Text = _g.GetResource("PRO_REGION_IS_REQUIRED", null);
            return false;
        }

        protected bool IsValidBirthDate()
        {
            try
            {
                string[] ids = this.ControlToValidate.Split(',');
                DropDownList months = (DropDownList)this.FindControl(ids[0]);
                DropDownList days = (DropDownList)this.FindControl(ids[1]);
                TextBox year = (TextBox)this.FindControl(ids[2]);

                DateTime birthdate = DateTime.ParseExact(months.SelectedValue + "/" + days.SelectedValue + "/" + year.Text, "M/d/yyyy", new System.Globalization.DateTimeFormatInfo());

                string validationMessage = "";
                bool isValid = ValidationHelper.IsValidBirthDate(_g, birthdate, null, out validationMessage);
                if (!string.IsNullOrEmpty(validationMessage))
                    Text = validationMessage;

                return isValid;
            }
            catch (Exception ex){}
            Text = _g.GetResource("TEXT_INVALID_BIRTH_DATE", null);
            return false;
        }

		protected bool IsValidCheckBox()
		{
			CheckBox cb;
			try
			{
				cb = (CheckBox)this.FindControl(ControlToValidate);
			}
			catch (Exception)
			{
				Text = IsRequiredErrorMessage;
				return false;
			}
			if (cb != null && _IsRequired)
			{
				if (cb.Checked)
				{
					return true;
				}
			}
			Text = IsRequiredErrorMessage;
			return false;
		}

		protected bool IsValidCheckBoxList()
		{
			CheckBoxList cbl;
			try
			{
				cbl = (CheckBoxList)this.FindControl(ControlToValidate);
			}
			catch (Exception)
			{
				Text = IsRequiredErrorMessage;
				return false;
			}
			if (cbl != null)
			{
				foreach (ListItem li in cbl.Items)
				{
					if (li.Selected)
					{
						return true;
					}
				}
			}
			Text = IsRequiredErrorMessage;
			return false;
		}

		//TT15456 - check for valid img path
		protected bool IsValidPhotoPath()
		{
			TextBox photoTb;
			try
			{
				photoTb = (TextBox)this.FindControl(ControlToValidate);
			}
			catch (Exception)
 			{
				Text = IsRequiredErrorMessage;
				return false;
			}
			if (photoTb != null)
			{
				Regex chkPhotoPath = new Regex("^[A-Z]:(\\[A-Z0-9_]+)+$");
				if (chkPhotoPath.IsMatch(photoTb.Text))
					return true;
				else
					return false;
			}
			return false;
		}


		protected bool IsValidCreditCard(ref string cardNumber)
		{
			// Clean out any blanks or spaces...
			cardNumber = cardNumber.Trim();
			cardNumber = cardNumber.Replace("-","");
			cardNumber = cardNumber.Replace(" ","");

			//Regex regex = new Regex("^3[4,7]\\d{13}|3[0,6,8]\\d{12}|((4\\d{3})|(5[1-5]\\d{2})|(6011)|(35\\d{2}))\\d{12}$");
			//if ( regex.Match(cardNumber).Length != cardNumber.Length )
			//return false;
	
			try
			{
				ArrayList CheckNumbers = new ArrayList();
				int CardLength = cardNumber.Length;

				if ( CardLength > 19 || CardLength < 8 )
					return false;

				if (CardLength == 8 && 
					(_g.Brand.BrandID == (int) WebConstants.BRAND_ID.Cupid ||
					_g.Brand.BrandID == (int) WebConstants.BRAND_ID.JDateCoIL))
				{
					//the eight digit Israeli credit card doesn't comply to the Mod 10 rule.
					//so we don't check it.
					return true;
				}

    
				// Double the value of alternate digits, starting with the second digit
				// from the right, i.e. back to front.
				// Loop through starting at the end
				for (int i = CardLength-2; i >= 0; i = i - 2)
				{
					// Double the number returned
					CheckNumbers.Add( Int32.Parse(cardNumber[i].ToString())*2 );
				}

				int CheckSum = 0;    // Will hold the total sum of all checksum digits
            
				// Second stage, add separate digits of all products
				for (int iCount = 0; iCount <= CheckNumbers.Count-1; iCount++)
				{
					int _count = 0;    // will hold the sum of the digits

					// determine if current number has more than one digit
					if ((int)CheckNumbers[iCount] > 9)
					{
						int _numLength = ((int)CheckNumbers[iCount]).ToString().Length;
						// add count to each digit
						for (int x = 0; x < _numLength; x++)
						{
							_count = _count + Int32.Parse( 
								((int)CheckNumbers[iCount]).ToString()[x].ToString() );
						}
					}
					else
					{
						// single digit, just add it by itself
						_count = (int)CheckNumbers[iCount];   
					}
					CheckSum = CheckSum + _count;    // add sum to the total sum
				}
				// Stage 3, add the unaffected digits
				// Add all the digits that we didn't double still starting from the
				// right but this time we'll start from the rightmost number with 
				// alternating digits
				int OriginalSum = 0;
				for (int y = CardLength-1; y >= 0; y = y - 2)
				{
					OriginalSum = OriginalSum + Int32.Parse(cardNumber[y].ToString());
				}

				// Perform the final calculation, if the sum Mod 10 results in 0 then
				// it's valid, otherwise its false.
				return (((OriginalSum+CheckSum)%10)==0);
			}
			catch
			{
				return false;
			}

		}


		protected override void OnPreRender(EventArgs e)
		{
			if ( CssClass.Length < 1 )
				CssClass="error";

			// TT15787 - render checkbox/radiobutton list client side validator (because there isn't one incl. in the .NET validation controls)
			if ( this.EnableClientScript)
				if (_RequiredType == BasicTypes.CheckBoxListType)
				{
					this.CBLClientScript(this.ID);
				}
					// we specify the id and not the control type because the radiobuttonlists is passed in as an IntegerType
				else if (this.ID == "SeekingGenderIDValidator")
				{
					this.CBLClientScript("SeekingGenderIDValidator");
				}
				else if (this.ID == "GenderIDValidator")
				{
					this.CBLClientScript("GenderIDValidator");
				}
			

			base.OnPreRender(e);

			ResolveResources();
		}

		// TT15787 - appends client side js validation for checkboxlist
		protected void CBLClientScript(string controlName) 
		{
			this.Attributes["evaluationfunction"] = controlName + "_verify";

			StringBuilder sbScript = new StringBuilder();
			sbScript.Append( "<script language=\"javascript\">" );
			sbScript.Append( "\r" );
			sbScript.Append( "\r" );
			sbScript.Append( "function " + controlName + "_verify(val) {" );
			sbScript.Append( "\r" );

			sbScript.Append( "var val = document.all[document.all[\"" );
			sbScript.Append( this.ClientID );
			sbScript.Append( "\"].controltovalidate];" );
			sbScript.Append( "\r" );
			sbScript.Append( "var valctrl = document.all[\"");
			sbScript.Append( this.ClientID );
			sbScript.Append( "\"];" );
			sbScript.Append( "\r" );
			sbScript.Append( "var col = val.all;" );
			sbScript.Append( "\r" );

			sbScript.Append( "if ( col != null ) {" );
			sbScript.Append( "\r" );
			sbScript.Append( "for ( i = 0; i < col.length; i++ ) {" );
			sbScript.Append( "\r" );
			sbScript.Append( "if (col.item(i).tagName == \"INPUT\") {" );
			sbScript.Append( "\r" );
			sbScript.Append( "if ( col.item(i).checked ) {" );
			sbScript.Append( "\r" );
			sbScript.Append( "\r" );
			sbScript.Append( "return true;" );
			sbScript.Append( "\r" );
			sbScript.Append( "}" );
			sbScript.Append( "\r" );
			sbScript.Append( "}" );
			sbScript.Append( "\r" );
			sbScript.Append( "}" );
			sbScript.Append( "\r" );
			sbScript.Append( "\r" );
			sbScript.Append( "valctrl.innerText = valctrl.requiredErrorMessage;" );
			sbScript.Append( "\r" );
			sbScript.Append( "valctrl.innerHTML = valctrl.requiredErrorMessage;" );
			sbScript.Append( "\r" );
			//sbScript.Append( "MultiValitorSetControlFocus(\"" + controlName + "\");");
			sbScript.Append( "\r" );
			sbScript.Append( "return false;" );
			sbScript.Append( "\r" );
			sbScript.Append( "}" );
			sbScript.Append( "\r" );
			sbScript.Append( "}" );
			sbScript.Append( "\r" );
			sbScript.Append( "</script>" );
			this.Page.RegisterClientScriptBlock( controlName + "_Script", sbScript.ToString() );
   
		}


		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);

			if ( RenderUplevel )
			{
				// TT15787 - skip if the control is not a checkboxlist b/c we add our own custom client side js for validation.
				if (_RequiredType != BasicTypes.CheckBoxListType)
				{
					writer.AddAttribute("evaluationfunction",
						"MultiValidatorEvaluateIsValid");
				}


				// Is Required
				writer.AddAttribute("isRequired",
					IsRequired.ToString(CultureInfo.InvariantCulture));
				writer.AddAttribute("requiredErrorMessage",
					IsRequiredErrorMessage);

				// Max length
				writer.AddAttribute("maxLength",
					MaximumLength.ToString(CultureInfo.InvariantCulture));
				writer.AddAttribute("maximumLengthErrorMessage",
					MaximumLengthErrorMessage);

				// Min length
				writer.AddAttribute("minLength",
					MinimumLength.ToString(CultureInfo.InvariantCulture));
				writer.AddAttribute("minimumLengthErrorMessage",
					MinimumLengthErrorMessage);

				// Max Val ----------------------------------------
				writer.AddAttribute("maxVal",
					MaxVal.ToString(CultureInfo.InvariantCulture));
				writer.AddAttribute("maxValErrorMessage",
					MaxValErrorMessage);

				// Min Val
				writer.AddAttribute("minVal",
					MinVal.ToString(CultureInfo.InvariantCulture));
				writer.AddAttribute("minValErrorMessage",
					MinValErrorMessage);
				// ------------------------------------------------

				// Type Expression 
				writer.AddAttribute("requiredTypeRegEx",
					_RequiredTypeRegEx.ToString(CultureInfo.InvariantCulture));
				writer.AddAttribute("requiredTypeErrorMessage",
					_RequiredTypeErrorMessage);
				
				// Show popups
				writer.AddAttribute("showPopup",
					ShowPopup.ToString(CultureInfo.InvariantCulture));

				writer.AddAttribute("appendToErrorMessage", AppendToErrorMessage);
			}
		}

		protected void ResolveResources()
		{
			if ( _g == null || _ResourcesHaveBeenResolved )
				return;

			// Let's resolve the error messages...


			// IsRequired
			if ( _RequiredResourceConstant == "RESOURCE_NOT_FOUND" )
			{
                if (_CustomizeErrorMessage)
                    _IsRequiredErrorMessage = _g.GetResource("FRMVALIDATION1", null, new string[1] { _g.GetResource(_FieldNameResourceConstant, ResourceControl) });
                else
                    _IsRequiredErrorMessage = _g.GetResource(_FieldNameResourceConstant, ResourceControl);
			}
			else
			{
                _IsRequiredErrorMessage = _g.GetResource(_RequiredResourceConstant, ResourceControl, new string[1] { _g.GetResource(_FieldNameResourceConstant, ResourceControl) });
			}

			// RequiredType
			if ( _RequiredTypeResourceConstant == "RESOURCE_NOT_FOUND" )
			{
				switch (_RequiredType)
				{
					case BasicTypes.IntegerType:
						_RequiredTypeResourceConstant = "FRMVALIDATION2";
						break;
					case BasicTypes.CheckBoxListType:
						_RequiredTypeResourceConstant = "FRMVALIDATION2";
						break;

					case BasicTypes.LetterType:
						_RequiredTypeResourceConstant = "FRMVALIDATION5";
						break;
					case BasicTypes.AlphaNumType:
						_RequiredTypeResourceConstant = "FRMVALIDATION4";
						break;
					case BasicTypes.PhoneNumberType:
						_RequiredTypeResourceConstant = "FRMVALIDATION19";
						break;
					case BasicTypes.StringType:
						_RequiredTypeResourceConstant = "FRMVALIDATION20";
						break;
					case BasicTypes.CreditCardType:
						_RequiredTypeResourceConstant = "INVALID_CREDIT_CARD_NUMBER";
						break;
					case BasicTypes.PhotoType:
						_RequiredTypeResourceConstant = "INVALID_PHOTO_TYPE";
						break;
					case BasicTypes.PostalCode:
						_RequiredTypeResourceConstant = "INVALID_POSTAL_CODE";
						break;
                    case BasicTypes.DecimalType:
                        _RequiredTypeResourceConstant = "INVALID_DECIMAL_TYPE";
                        break;
                    case BasicTypes.EmailType:
                        _RequiredTypeResourceConstant = "INVALID_EMAIL_TYPE";
                        break;
				}

                _RequiredTypeErrorMessage = _g.GetResource(_RequiredTypeResourceConstant, null, new string[1] { _g.GetResource(_FieldNameResourceConstant, ResourceControl) });
			}
			else
			{
                _RequiredTypeErrorMessage = _g.GetResource(_RequiredTypeResourceConstant, ResourceControl, new string[1] { _g.GetResource(_FieldNameResourceConstant, ResourceControl) });
			}

			// MaximumLength
			if ( _MaximumLengthResourceConstant == "RESOURCE_NOT_FOUND" )
			{
                _MaximumLengthErrorMessage = _g.GetResource("FRMVALIDATION10", null, new string[2] { _g.GetResource(_FieldNameResourceConstant, ResourceControl), _MaximumLength.ToString() });
			}
            else if (_MaximumLengthResourceConstant == "FREE_TEXT_MAXIMUM_LENGTH")
            {
                _MaximumLengthErrorMessage = _g.GetResource("FREE_TEXT_MAXIMUM_LENGTH", null, new string[1] {MaximumLength.ToString()});
            }
            else
			{
                _MaximumLengthErrorMessage = _g.GetResource(_MaximumLengthResourceConstant, ResourceControl, new string[2] { _g.GetResource(_FieldNameResourceConstant, ResourceControl), _MaximumLength.ToString() });
			}

			// MinimumLength
			if ( _MinimumLengthResourceConstant == "RESOURCE_NOT_FOUND" )
			{
                _MinimumLengthErrorMessage = _g.GetResource("FRMVALIDATION9", null, new string[2] { _g.GetResource(_FieldNameResourceConstant, ResourceControl), _MinimumLength.ToString() });
			}
			else
			{
                _MinimumLengthErrorMessage = _g.GetResource(_MinimumLengthResourceConstant, ResourceControl, new string[2] { _g.GetResource(_FieldNameResourceConstant, ResourceControl), _MinimumLength.ToString() });
			}
			
			
			// MaxVal
			if ( _MaxValResourceConstant == "RESOURCE_NOT_FOUND" )
			{
                _MaxValErrorMessage = _g.GetResource("FRMVALIDATION14", null, new string[3] { _g.GetResource(_FieldNameResourceConstant, ResourceControl), _MinVal.ToString(), _MaxVal.ToString() });
			}
			else
			{
                _MaxValErrorMessage = _g.GetResource(_MaxValResourceConstant, ResourceControl, new string[3] { _g.GetResource(_FieldNameResourceConstant, ResourceControl), _MinVal.ToString(), _MaxVal.ToString() });
			}

			// MinVal
			if ( _MinValResourceConstant == "RESOURCE_NOT_FOUND" )
			{
                _MinValErrorMessage = _g.GetResource("FRMVALIDATION14", null, new string[3] { _g.GetResource(_FieldNameResourceConstant, ResourceControl), _MinVal.ToString(), _MaxVal.ToString() });
			}
			else
			{
                _MinValErrorMessage = _g.GetResource(_MinValResourceConstant, ResourceControl, new string[3] { _g.GetResource(_FieldNameResourceConstant, ResourceControl), _MinVal.ToString(), _MaxVal.ToString() });
			}

			_ResourcesHaveBeenResolved = true;
		}
	}
}
