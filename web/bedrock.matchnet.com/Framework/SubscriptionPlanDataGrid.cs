using System;
using System.Data;
using System.Web;
using System.Web.Caching;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Lib.Util;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;

namespace Matchnet.Web.Framework
{
	/// <summary>
	/// Summary description for SubscriptionPlanDataGrid.
	/// </summary>
	public class SubscriptionPlanDataGrid : System.Web.UI.WebControls.WebControl
	{
		private ICaching _Cache;
		private const string SUBSCRIPTION_PLAN_CACHEKEY_PREFIX = "Subscription:Plan:";
		private ContextGlobal _g; 
		private const int CACHE_TTL = 3600;

		public SubscriptionPlanDataGrid()
		{
            bool useMembase = Convert.ToBoolean(RuntimeSettings.GetSetting(WebConstants.MEMBASE_SETTING_CONSTANT));
            bool useMembaseForWeb = Convert.ToBoolean(RuntimeSettings.GetSetting(WebConstants.MEMBASE_WEB_SETTING_CONSTANT));
            if (useMembase && useMembaseForWeb)
            {
                MembaseConfig membaseConfig =
                    RuntimeSettings.GetMembaseConfigByBucket(WebConstants.MEMBASE_WEB_BUCKET_NAME);
                _Cache = Spark.Caching.MembaseCaching.GetSingleton(membaseConfig);
            }
            else
            {
                _Cache = CachingTemp.Cache.GetInstance();
            }

		    if ( Context != null )
			{
				if ( Context.Items["g"] != null )
					_g = (ContextGlobal) Context.Items["g"];
			}
			else
			{
				_g = new ContextGlobal( null );
			}
		}

		public DataGrid createGrid()
		{
			string key = SUBSCRIPTION_PLAN_CACHEKEY_PREFIX + "createGrid:PrivateLabelID" + _g.TargetBrand.BrandID.ToString();
			DataGrid grid = (DataGrid)_Cache[key];
			if (grid == null)
			{
				grid = new DataGrid();
				grid.CssClass = "grid";
				grid.AutoGenerateColumns = false;
				grid.CellPadding = 1;
				grid.CellSpacing = 2;
				grid.Width=592;
				grid.BorderWidth=0;
				grid.CssClass = "white";
				grid.ItemStyle.CssClass="medium";
				grid.HeaderStyle.CssClass="light";
				grid.HeaderStyle.Font.Bold = true;
				BoundColumn column = new BoundColumn();
				column.HeaderText = _g.GetTargetResource("INITIAL_SUBSCRIPTION_TERM", this);
				column.DataField = "InitialDurationMonths";
				//column.DataFormatString = "{0:G}" + " month";
				//column.DataFormatString = "{0:G}" + " " + _g.GetTargetResource("MONTH_S", this);

				grid.Columns.Add(column);

				column = new BoundColumn();
				column.HeaderText =_g.GetTargetResource("INITIAL_COST", this);

				column.DataField = "InitialCost";
				//column.DataFormatString = "{0:C}";
				grid.Columns.Add(column);
				column = new BoundColumn();
				column.HeaderText = _g.GetTargetResource("GUARANTEED_MONTHLY_RENEWAL_RATE__519664", this);
				column.DataField = "RenewCost";
				//	column.DataFormatString = "{0:C}";
				grid.Columns.Add(column);
				populatePlan(ref grid);
			}
			if (grid != null)
			{
				_Cache.Insert(key,grid,null,DateTime.Now.AddSeconds(CACHE_TTL),TimeSpan.Zero,CacheItemPriority.Normal,null);
			}
			return grid;
		}

		private void populatePlan(ref DataGrid  grid)
		{
			PlanCollection pc = PlanSA.Instance.GetPlans( _g.TargetBrand.BrandID, PlanType.Regular, PaymentType.CreditCard );
			
			DataTable formatTable = new DataTable();
			DataRow tempRow;
			string[] temp = new string[1];

			DataColumn dc = new DataColumn("InitialDurationMonths");
			//dc.DataType = System.Type.GetType("System.Int32");
			formatTable.Columns.Add(dc);
			dc = new DataColumn("InitialCost");
			formatTable.Columns.Add(dc);
			dc = new DataColumn("RenewCost");
			formatTable.Columns.Add(dc);
			if ( pc != null )
			{
				foreach(Plan plan in pc)
				{
					
					if(408 != plan.PlanID && ! (((int) plan.PlanTypeMask & (int) Matchnet.Purchase.ValueObjects.PlanType.PromotionalPlan) >0) )
					{
						tempRow = formatTable.NewRow();
						if(plan.InitialDurationType != Matchnet.DurationType.Day)
						{
							if(plan.InitialDuration > 1)
							{
								tempRow["InitialDurationMonths"] = plan.InitialDuration + " " + _g.GetTargetResource("MONTH_S", this);
							}
							else
							{
								tempRow["InitialDurationMonths"] = plan.InitialDuration + " " + _g.GetTargetResource("MONTHABBR", this);
							}
						}
						else
						{
							if(plan.InitialDuration > 1)
							{
								tempRow["InitialDurationMonths"] = plan.InitialDuration + " " + _g.GetTargetResource("DAY_S", this);
							}
							else
							{
								tempRow["InitialDurationMonths"] = plan.InitialDuration + " " + _g.GetTargetResource("DAYABBR", this);
							}
						}
									
						temp[0] = Decimal.Round(Matchnet.Lib.Util.Util.CDecimal(plan.InitialCost), 2).ToString();
						tempRow["InitialCost"] =  _g.GetTargetResource("FORMAT_CURRENCY", this, temp);
						temp[0] = Decimal.Round(Matchnet.Lib.Util.Util.CDecimal(plan.RenewCost), 2).ToString();
						tempRow["RenewCost"] = _g.GetTargetResource("FORMAT_CURRENCY", this, temp);
						formatTable.Rows.Add(tempRow);
					}
				}
			}
			grid.DataSource = formatTable;
			grid.DataBind();
		}
	}
}
