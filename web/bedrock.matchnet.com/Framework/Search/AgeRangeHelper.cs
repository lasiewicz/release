﻿using System;
using System.Collections.Generic;

namespace Matchnet.Web.Framework.Search
{
	public static class AgeRangeHelper
	{
		public static readonly Dictionary<Int32, Int32> AgeRanges = new Dictionary<Int32, Int32>
		{
			{20,9},
			{30,10},
			{40,11},
			{50,12},
			{60,13},
			{70,14},
		};

		public static Int32 GetAgeRangeFromPreferences(ContextGlobal g)
		{
			try
			{
				Int32 minAge, maxAge;

				Int32.TryParse(g.SearchPreferences[Constants.Keys.MinAge], out minAge);

				Int32.TryParse(g.SearchPreferences[Constants.Keys.MaxAge], out maxAge);

				var average = (maxAge + minAge) / 2.0;

				return average > 0 ? (Int32)average : 0;
			}
			catch (NullReferenceException)
			{
				return 0;
			}
		}

		public static Int32 GetAgeRange( ContextGlobal g )
		{
			var ageRangeFromPreferences = GetAgeRangeFromPreferences(g);

			return
				((ageRangeFromPreferences > 0 ? ageRangeFromPreferences : 1) /
				Constants.Literals.Ten) * Constants.Literals.Ten;
		}
	}
}