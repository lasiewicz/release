﻿using System;

namespace Matchnet.Web.Framework.Search
{
	/// <summary>
	/// A class of constants used in Facelink keyword search.
	/// </summary>
	public static class Constants
	{
		public static class Text
		{
			public static Char Comma = ',';

			public static Char Space = ' ';

			public static String SpaceString = " ";
			
			public static String Apostrophe = "'";

			public static String SecondApostrophe = "’";

			public static Char Underscore = '_';

			public static String LeftParentheses = "(";

			public static String RightParentheses = ")";

			public static Char Dash = '-';

			public static String CulturallyJewish = "culturally jewish but not practicing";

			public static String Secular = "secular";

			public static String Regularly = "regularly";

			public static String SmokesRegularly = "smokes regularly";

			public static String OnOcassion = "on occasion";

			public static String Occasionally = "occasionally";

			public static String AnotherStream = "another stream";

			public static String AnotherStreamOfJudaism = "another stream of judaism";

			public const String any = "any";
		}

		/// <summary>
		/// Paging constants used for paging through results.
		/// </summary>
		public static class Paging
		{
			public const Int32 DefaultPage = 1;

			public const Int32 DefaultPageSize = 12;

			public const Int32 DefaultChapterSize = 3;

			public const Int32 NumberOfTrackedPages = 6;

			public const String ChapterSpacer = "&nbsp;/&nbsp;";

			public const String QsparamStartrow = "StartRow";
		}

        public static class ReverseSearch
        {
            public static class Parameter
            {
                public const String Page = "pg";

                public const String SortType = "sl";

                public const String OnlyPhotos = "op";
            }
        }

		public const String WillTellYouLater = "Will tell you later";

		public const String Any = "Any";

		public const String On_occassion = "On occasion";

		public const String Occasionally = "Occasionally";

		public const String NonSmoker = "NonSmoker";

		public const String Non_Smoker = "Non Smoker";

        public const int QS_JewishOnly_Unchecked = 50000;

		/// <summary>
		/// Url query parameters.
		/// </summary>
		public static class Parameters
		{
			public const String SearchTerms = "st";

			public const String Seeker = "s";

			public const String Seeking = "sk";

			public const String Age = "a";

			public const String Location = "l";

			public const String Paging = "p";

			public const String AlternatePage = "pp";

			public const String StartRow = "StartRow";

			public const String ResultType = "result";

			public const String SearchTermsKey = "st=";

			public const String ResultKey = "&result=";

			public const String SeekerKey = "&s=";

			public const String SeekingKey = "&sk=";

			public const String AgeKey = "&a=";

			public const String LocationKey = "&l=";

			public const String PageKey = "&p=";

			public const String ResetPaging = "resetPaging";

			public const String GalleryKeyValuePair = "&result=gallery";

			public const String KeywordNavigationRequest = "isnavrequest";

			public const String ReverseSearchNavigationRequest = "isrevnavreq";

            public const String SlideshowNavigationRequest = "isslidenavreq";

            public const String SlideShowFilmstripNavigationRequest = "isslidefilmstripnavreq";
            public const String SlideShowFilmstripMOLNavigationRequest = "isslidefilmstripmolnavreq";

			public const String AlternatePageKey = "&pp=";

			public const String DestinationUrl = "DestinationUrl";
		}

		public static class Numbers
		{
			public const Double DaysPerYear = 365.25;
		}

		public static class Attributes
		{
			public const String RegionId = "regionid";

			public const String GenderMask = "gendermask";

			public const String SynagogueAttendance = "synagogueattendance";

			public const String SmokingHabits = "smokinghabits";

			public const String KeepKosher = "keepkosher";

			public const String JDateReligion = "jdatereligion";

			public const String EducationLevel = "educationlevel";

			public const String ActivityLevel = "activitylevel";

			public const String DrinkingHabits = "drinkinghabits";

			public const String HaveChildren = "morechildrenflag";

			public const String Height = "height";

			public const String Weight = "weight";

			public const String MaritalStatus = "maritalstatus";

			public const String Ethnicity = "jdateethnicity";

			public const String LanguageMask = "languagemask";

			public const String BirthDate = "birthdate";

		    public const String WillingToRelocate = "relocateflag";
		}

		/// <summary>
		/// Keys for accessing .net resource key value pairs in satellite assemblies.
		/// </summary>
		public static class ResourceKeys
		{
			public const String ShowProfilesWithPhotosOnly = "showProfilesWithPhotosOnly";

			public const String Choose = "choose";

			public const String Man = "man";

			public const String Woman = "woman";

			public const String Men = "men";

			public const String Women = "women";

			public const String ManSeekingWomen = "ManSeekingWomen";

			public const String WomanSeekingMen = "WomanSeekingMen";

			public const String ManSeekingMen = "ManSeekingMen";

			public const String WomanSeekingWomen = "WomanSeekingWomen";

			public const String Of = "of";

			public const String Profiles = "profiles";

			public const String NoKeywordError = "noKeywordError";

			public const String NoGenderError = "noGenderError";

			public const String NoSeekingError = "noSeekingError";

			public const String NoAgeRangeError = "noAgeRangeError";

			public const String NoAgeError = "noAgeError";

			public const String NoLocationError = "noLocationError";

			public const String NoPreferenceError = "noPreferenceError";

			public const String NoQueryError = "noQueryError";

			public const String SearchResource = "search";

			public const String NoSeekerError = "noSeekerError";

			public const String SearchTermsTooLong = "searchTermsTooLong";

			public const String LocationTooLongError = "locationTooLong";

			public const String MissingPreferences = "missingPreferences";

			public const String UnidentifiedPreferences = "unidentifiedPreferences";

			public const String MissingAgeRange = "missingAgeRange";

			public const String InvalidAgeRange = "invalidAgeRange";

			public const String InvalidPreferenceError = "invalidPreferenceError";
		}

		/// <summary>
		/// Keys used in dictionaries and the like.
		/// </summary>
		public static class Keys
		{
			public const String MinAge = "MinAge";

			public const String MaxAge = "MaxAge";

			public const String RegionID = "RegionID";
		}

		/// <summary>
		/// Holds min/max values used in search.
		/// </summary>
		public static class Thresholds
		{
			public const Int32 MaximumSearchTermLength = 100;

			public const Int32 MaximumNumberOfKeywords = 40;

			public const Int32 MaximumLocationLength = 128;

			public const Int32 MinimumAgeRangeValue = 0;

			public const Int32 MaximumAgeRangeValue = 14;
		}

		/// <summary>
		/// Pieces of html used in code-behind. (BLECH!)
		/// </summary>
		public static class HtmlPieces
		{
			public const String TotalResultsSpanTagStart = "<span class='totalResults'>";
			public const String SpanTagEnd = "</span>";
		}

		/// <summary>
		/// Literal values, such as Seven, which represents the string value 7.
		/// </summary>
		public static class Literals
		{
			public const String Display = "display";

			public const String Block = "block";

			public const String Two = "2";

			public const String JDate = "jdate";

			public const String FromJDate = "+from+jdate";

			public const String JDateCode = "+a28e4b4a";

            public const String SparkCom = "sparkcom";

            public const String FromSparkCom = "+from+sparkcom";

            public const String SparkComCode = "+345df294";

            public const String FromBBW = "+from+bbw";

            public const String BBWCode = "+823ef2e6";

            public const String FromBlack = "+from+black";

            public const String BlackCode = "+1ffd9e75";

			public const Char Space = ' ';

			public const String Dash = "-";

			public const String SpaceString = " ";

			public const Char Plus = '+';

			public const Char Seven = '7';

			public const Char Comma = ',';

			public const String CommaPlusSpace = ", ";

			public const String Gallery = "gallery";

			public const String MaleToMale = "m2m";

			public const String MaleToFemale = "m2f";

			public const String FemaleToFemale = "f2f";

			public const String FemaleToMale = "f2m";

			public const Char WCharacter = 'w';

			public const Char FCharacter = 'f';

			public const Char MCharacter = 'm';

			public const String ChooseOption = "--Choose--";

			public const Int32 Ten = 10;

			public const String ManSymbol = "m";

			public const String WomanSymbol = "f";

			public const String UnitedStatesLocationModifier = ", USA";

			public const String KeywordSearchResultsUrl = "/Applications/Search/KeywordSearchResults.aspx?";

			public const String GenderMaskAttribute = "gendermask";

			public const String InitialLocationText = "City, State/Region, Country";

			public const String US = "US";

			public const String USA = "USA";

			public const String NoneSelected = "None Selected";

			public const String KeywordSearchResults = "keywordsearchresults";
		}

		/// <summary>
		/// Control names.
		/// </summary>
		public static class Controls
		{
			public const String SearchTextboxName = "searchTermsTextBox";

			public const String AgeDropDownListName = "ageDropDownList";

			public const String LocationName = "location";

			public const String PreferenceName = "preference";

			public const String TestSearchTerm = "asdfasdf";

			public const String SeekerControlName = "seekerDropDownList";

			public const String SeekingControlName = "seekingDropDownList";

			public const String AgeControlName = "ageDropDownList";

			public const String LocationControlName = "locationTextBox";
		}

		/// <summary>
		/// This class stores session constants, like cookie key names.
		/// </summary>
		public static class Session
		{
			public const String CookieKeyName = "kwdschprfs";

			public const Int32 CookieExpiration = 30;

			public const String SearchPreferencesKey = "sp";
		}
	}
}
