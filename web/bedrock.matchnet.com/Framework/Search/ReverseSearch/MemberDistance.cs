﻿using System;

namespace Matchnet.Web.Framework.Search.ReverseSearch
{
	public class MemberDistance
	{
		public Int32 MemberId { get; set; }

		public Coordinate Location { get; set; }

		public Double Distance { get; set; }

		public void CalculateDistance(Coordinate otherLocation)
		{
			if (otherLocation == null) throw new ArgumentNullException("otherLocation");

			if (Location == null) throw new InvalidOperationException("Location must be set.");

			Distance = Coordinate.GetDistance(Location, otherLocation);
		}
	}
}