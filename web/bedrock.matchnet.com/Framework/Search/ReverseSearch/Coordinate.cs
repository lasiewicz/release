﻿using System;

namespace Matchnet.Web.Framework.Search.ReverseSearch
{
    [Serializable]
	public class Coordinate
	{
		private const Double EarthRadius = 6371;

		public Double Latitude { get; set; }
		public Double Longitude { get; set; }

		public static Double GetDistance(Coordinate coordinate1, Coordinate coordinate2)
		{
			if (coordinate1 == null) throw new ArgumentNullException("coordinate1");

			if (coordinate2 == null) throw new ArgumentNullException("coordinate2");

			var dLat = (coordinate2.Latitude - coordinate1.Latitude).ToRadians();
			var dLon = (coordinate2.Longitude - coordinate1.Longitude).ToRadians();
			var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
			        Math.Cos(coordinate1.Latitude.ToRadians()) *
			        Math.Cos(coordinate2.Latitude.ToRadians()) *
			        Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
			var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
			return EarthRadius * c;
		}
	}

	static class Extension
	{
		public static Double ToRadians(this Double value)
		{
			return value * Math.PI / 180;
		}
	}
}