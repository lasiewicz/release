﻿using System;

namespace Matchnet.Web.Framework.Search.ReverseSearch
{
	public class ReverseSearchConfiguration
	{
		public Int32 MinimumResults { get; set; }

		public Int32 MaximumResults { get; set; }
	}
}