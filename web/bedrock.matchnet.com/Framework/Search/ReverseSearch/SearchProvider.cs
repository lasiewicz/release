﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.DistributedCaching.ServiceAdapters;

namespace Matchnet.Web.Framework.Search.ReverseSearch
{
	public class SearchProvider
	{
		private static readonly Adapter adapter =
			new Adapter { Host = ConfigurationManager.AppSettings["ReverseSearchServerUrl"] };

		private const SortType DefaultSortType = SortType.Newest;

		private static readonly Int32 minimumResults =
			((ReverseSearchConfiguration) ConfigurationSettings.GetConfig(
				"ReverseSearchConfiguration")).MinimumResults;

		private static readonly Int32 maximumResults =
			((ReverseSearchConfiguration)ConfigurationSettings.GetConfig(
				"ReverseSearchConfiguration")).MaximumResults;

		private static readonly Dictionary<String, SortType> sortTypes = new Dictionary<String, SortType>
		{
			{"n", SortType.Newest},
			{"m", SortType.MostActive},
			{"c", SortType.ClosestToYou},
			{"p", SortType.BestMatch},
			{String.Empty, SortType.Newest},
		};

        private const string SEARCH_RESULTS_NAMED_CACHE = "searchresults";

        private static int maximumUnfilteredResults = Matchnet.Constants.NULL_INT;

        private static int MaximumUnfilteredResults
        {
            get
            {
                //if (maximumUnfilteredResults == Matchnet.Constants.NULL_INT)
                //{
                //    maximumUnfilteredResults = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MAXIMUM_UNFILTERED_REVERSE_SEARCH"));   
                //}

                return Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MAXIMUM_UNFILTERED_REVERSE_SEARCH"));  
            }
        }

		public SearchResponse GetResults(
			ContextGlobal g, HttpRequest request, Int32 pageNumber, Boolean onlyPhotos)
		{
            return GetResults(g, request, pageNumber, onlyPhotos, Constants.Paging.DefaultPageSize);
		}

        public SearchResponse GetResults(
            ContextGlobal g, HttpRequest request, Int32 pageNumber, Boolean onlyPhotos, int pageSize)
        {
            var startingRow = ((pageNumber - 1) * pageSize);

            var sortType = GetSortType(request[Constants.ReverseSearch.Parameter.SortType]);

            if (sortType == SortType.BestMatch
                    && !Matchnet.Web.Applications.Search.SearchUtil.IsSearchSortByPopularityEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
            {
                sortType = SortType.Newest;
            }

            return sortType == SortType.ClosestToYou ?
                GetClosestResults(g, startingRow, onlyPhotos, pageSize) :
                GetNormalResults(g, startingRow, sortType, onlyPhotos, pageSize);
        }

        public SearchResponse GetResults(
            ContextGlobal g, HttpRequest request, Int32 startingRow, Boolean onlyPhotos, int pageSize, int searchOrderBy)
        {
            var sortType = DefaultSortType;
            switch (searchOrderBy)
            {
                case (int)Matchnet.Search.Interfaces.QuerySorting.LastLogonDate:
                    sortType = SortType.MostActive;
                    break;
                case (int)Matchnet.Search.Interfaces.QuerySorting.Proximity:
                    sortType = SortType.ClosestToYou;
                    break;
            }
            

            if (sortType == SortType.BestMatch
                    && !Matchnet.Web.Applications.Search.SearchUtil.IsSearchSortByPopularityEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
            {
                sortType = SortType.Newest;
            }

            return sortType == SortType.ClosestToYou ?
                GetClosestResults(g, startingRow, onlyPhotos, pageSize) :
                GetNormalResults(g, startingRow, sortType, onlyPhotos, pageSize);
        }

		private static Boolean IsValidResults(CachedSearchResults results, Boolean onlyPhotos)
		{
			if (results == null) return false;

			return results.OnlyPhotos == onlyPhotos;
		}

		private SearchResponse GetClosestResults(ContextGlobal g, Int32 startingRow, Boolean onlyPhotos, int pageSize)
		{
            bool isProfileBlockingEnabled = (g.Member != null &&
                Convert.ToBoolean(RuntimeSettings.GetSetting("PROFILE_BLOCK_ENABLED", g.Brand.Site.Community.CommunityID)));

            // The new distributed cache used for search results that have been filtered by "ExcludeFrom" of the member
            string cacheKey = string.Empty;
            if (isProfileBlockingEnabled)
            {
                cacheKey = GetMemberSpecificCacheKey(g, SortType.ClosestToYou, onlyPhotos);
                //List<int> cachedMemberIds = DistributedCachingDirect.DistributedCachingWeb.Instance.Get(cacheKey, SEARCH_RESULTS_NAMED_CACHE) as List<int>;
                List<int> cachedMemberIds = DistributedCachingSA.Instance.GetIntList(cacheKey, SEARCH_RESULTS_NAMED_CACHE);

                if (cachedMemberIds != null)
                    return CreateResponse(cachedMemberIds, startingRow, pageSize);
            }

            // The old web cache usage
            CachedSearchResults cachedResults = null;
            var key = CachedSearchResults.GetCacheKey(g);
            if (IsValidResults(g.Cache[key] as CachedSearchResults, onlyPhotos))
            {
                cachedResults = g.Cache[key] as CachedSearchResults;
            }
            else
            {

                var results = adapter.GetResults(
                    SearchRequest.Create(g),
                    0,
                    MaximumUnfilteredResults,
                    SortType.ClosestToYou,
                    onlyPhotos
                );

                if (results.TotalResults < minimumResults)
                {
                    results = adapter.GetSecondaryResults(
                        SearchRequest.Create(g),
                        0,
                        MaximumUnfilteredResults,
                        SortType.ClosestToYou,
                        onlyPhotos
                    );
                }

                // this does more than just caching. it actually calculates the distance for all the members and sorts by the distance.
                cachedResults = CachedSearchResults.SortAndCacheToWeb(g, results, onlyPhotos);
            }

            /// filter the cache results if we have profile blocking enabled
            if (isProfileBlockingEnabled)
            {
                List<int> filteredMemberIds = RemoveExcludedMembers(cachedResults, g);
                //DistributedCachingDirect.DistributedCachingWeb.Instance.Put(cacheKey, filteredMemberIds, SEARCH_RESULTS_NAMED_CACHE);
                DistributedCachingSA.Instance.Put(cacheKey, filteredMemberIds, SEARCH_RESULTS_NAMED_CACHE);

                return CreateResponse(filteredMemberIds, startingRow, pageSize);
            }

            return CachedSearchResults.CreateResponse(cachedResults, startingRow);            
		}

		private SearchResponse GetNormalResults(ContextGlobal g, Int32 startingRow, 
			SortType sortType, Boolean onlyPhotos, int pageSize)
		{
            bool isProfileBlockingEnabled = (g.Member != null &&
                Convert.ToBoolean(RuntimeSettings.GetSetting("PROFILE_BLOCK_ENABLED", g.Brand.Site.Community.CommunityID)));

            string cacheKey = string.Empty;
            if (isProfileBlockingEnabled)
            {
                cacheKey = GetMemberSpecificCacheKey(g, sortType, onlyPhotos);
                //List<int> cachedMemberIds = DistributedCachingDirect.DistributedCachingWeb.Instance.Get(cacheKey, SEARCH_RESULTS_NAMED_CACHE) as List<int>;
                List<int> cachedMemberIds = DistributedCachingSA.Instance.GetIntList(cacheKey, SEARCH_RESULTS_NAMED_CACHE);

                if (cachedMemberIds != null)
                    return CreateResponse(cachedMemberIds, startingRow, pageSize);
            }

            int realStartingRow = startingRow;
            int realPageSize = pageSize;

            if (isProfileBlockingEnabled)
            {
                realStartingRow = 0;
                realPageSize = MaximumUnfilteredResults;
            }

			var results = adapter.GetResults(
                SearchRequest.Create(g),
				realStartingRow,
                realPageSize,
				sortType,
				onlyPhotos
			);

			if (results.TotalResults < minimumResults)
			{
				results = adapter.GetSecondaryResults(
                    SearchRequest.Create(g),
                    realStartingRow,
                    realPageSize,
					sortType,
					onlyPhotos
				);
			}

            if (isProfileBlockingEnabled)
            {
                List<int> filteredMemberIds = RemoveExcludedMembers(results, g);
                //DistributedCachingDirect.DistributedCachingWeb.Instance.Put(cacheKey, filteredMemberIds, SEARCH_RESULTS_NAMED_CACHE);
                DistributedCachingSA.Instance.Put(cacheKey, filteredMemberIds, SEARCH_RESULTS_NAMED_CACHE);

                return CreateResponse(filteredMemberIds, startingRow, pageSize);
            }

			return results;
		}

        private static SearchResponse CreateResponse(List<int> memberIds, int startingRow, int pageSize)
        {
            return new SearchResponse
            {
                MemberIds = memberIds.GetRange(startingRow, GetEndRow(startingRow, memberIds.Count, pageSize)),
                TotalResults = memberIds.Count
            };
        }

        private static Int32 GetEndRow(Int32 startingRow, Int32 total, int pageSize)
        {
            return (startingRow + pageSize) > total ?
                total - startingRow : pageSize;
        }

        private static List<int> RemoveExcludedMembers(CachedSearchResults cachedResults, ContextGlobal g)
        {
            Matchnet.List.ServiceAdapters.List list = Matchnet.List.ServiceAdapters.ListSA.Instance.GetList(g.Member.MemberID);

            if (list == null)
                return cachedResults.Response.MemberIds.ToList<int>();

            // since this is a reverse search (people who are looking for you), we are compared against our "Exclude From" list rather than the typical ExcludeListInternal
            var memberIds = cachedResults.Response.MemberIds.Where(mId => !list.IsHotListed(Matchnet.List.ValueObjects.HotListCategory.ExcludeFromList,
                g.Brand.Site.Community.CommunityID,
                mId)
                );

            return memberIds.ToList<int>();
        }

        private static List<int> RemoveExcludedMembers(SearchResponse searchResponse, ContextGlobal g)
        {
            Matchnet.List.ServiceAdapters.List list = Matchnet.List.ServiceAdapters.ListSA.Instance.GetList(g.Member.MemberID);

            if (list == null)
                return searchResponse.MemberIds.ToList<int>();

            // since this is a reverse search (people who are looking for you), we are compared against our "Exclude From" list rather than the typical ExcludeListInternal
            var memberIds = searchResponse.MemberIds.Where(mId => !list.IsHotListed(Matchnet.List.ValueObjects.HotListCategory.ExcludeFromList,
                g.Brand.Site.Community.CommunityID,
                mId)
                );

            return memberIds.ToList<int>();
        }

        private static string GetMemberSpecificCacheKey(ContextGlobal g, SortType sortType, bool onlyPhotos)
        {
            return string.Join("_", new string[]
            {
                "EXCL_RS",
                g.Brand.Site.Community.CommunityID.ToString(),
                g.Member.MemberID.ToString(),
                Enum.GetName(typeof(SortType), sortType),
                onlyPhotos.ToString()
            });
        }

		private static SortType GetSortType(String sortType)
		{
			return String.IsNullOrEmpty(sortType) ?
				DefaultSortType :
				sortTypes.ContainsKey(sortType) ?
					sortTypes[sortType] :
					DefaultSortType;
		}
	}
}