﻿using System;
using System.Collections.Generic;

namespace Matchnet.Web.Framework.Search.ReverseSearch
{
    [Serializable]
	public class SearchResponse
	{
		public IEnumerable<Int32> MemberIds { get; set; }

		public Int32 TotalResults { get; set; }
	}
}