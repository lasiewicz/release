﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Linq;

namespace Matchnet.Web.Framework.Search.ReverseSearch
{
	public class Adapter
	{

		#region Constants

		private const String Method = "/select?q=";

		private const String AndConnector = "+AND+";

		#endregion Constants

		public String Host { get; set; }

		#region Attribute Lookup Dictionaries

		private readonly Dictionary<String, String> smokingStatus = new Dictionary<String, String>
   	{
   		{"Regularly", "Smokes Regularly"},
   		{"Non-Smoker", "Non Smoker"},
   		{"Occasionally", "Occasional Smoker"},
   		{"Trying to quit", "Trying To Quit"},
   	};

		private readonly Dictionary<String, String> gender = new Dictionary<String, String>
     	{
     		{"Man", "male"},
     		{"man", "male"},      
     		{"Men", "male"},
     		{"men", "male"},   
     		{"Woman", "female"},  
     		{"woman", "female"},
     		{"Women", "female"},  
     		{"women", "female"},  	
     	};

		private readonly Dictionary<String, String> educationLevel = new Dictionary<String, String>
    	{
    		{"Bachelor's Degree", "bachelors degree"},
    		{"Bachelor'sDegree", "bachelors Degree"},
			{"bachelor's degree", "bachelors degree"},
    		{"bachelor'sdegree", "bachelors Degree"},
    		{"Master's Degree", "masters degree"},
			{"master's degree", "masters degree"},
    		{"Ph.D./Postdoctoral", "doctorate"},
			{"jd/ph.d/postdoctoral", "doctorate"},
    		{"Associate Degree", "associates degree"},
			{"associate degree", "associates degree"},
    	};

		private readonly Dictionary<String, String> jDateReligion = new Dictionary<String, String>
   	{
   		{"Culturally Jewish but not practicing", "secular"},
			{"culturally jewish but not practicing", "secular"},
			{"orthodox frum", "orthodox frum from birth"},
   	};

		#endregion Attribute Lookup Dictionaries

		public SearchResponse GetSecondaryResults(
			SearchRequest request, Int32 startingRow, Int32 totalRecords, SortType sortType, Boolean onlyPhotos)
		{
			var results = Get(CreateSecondaryUrl(request, startingRow, totalRecords, sortType, onlyPhotos));

			return new SearchResponse
			{
				MemberIds = GetMemberIds(results),
				TotalResults = GetTotal(results),
			};
		}

		public SearchResponse GetResults(
			SearchRequest request, Int32 startingRow, Int32 totalRecords, SortType sortType, Boolean onlyPhotos)
		{
			var results = Get(CreateUrl(request, startingRow, sortType, onlyPhotos, totalRecords));

			return new SearchResponse
			{
				MemberIds = GetMemberIds(results),
				TotalResults = GetTotal(results),
			};
		}

		private static Int32 GetTotal(XContainer results)
		{
			var total = from r in results.Descendants("result")
							select r.Attribute("numFound").Value;

			return Int32.Parse(total.First());
		}

		private String CreateUrl(
			SearchRequest request,
			Int32 startingRow,
			SortType sortType,
			Boolean onlyPhotos,
			Int32 totalRecords
		)
		{
			return String.Join(
						AndConnector,
						new[]
			       		{
			       			Host + Method + GetGender(request),
                            GetSite(request),
			       			GetAge(request),
			       			GetLocation(request),
			       			GetSmokingStatus(request),
			       			GetHaveChildren(request),
			       			GetDrinkingStatus(request),
			       			GetJDateReligion(request),
			       			GetMaritalStatus(request),
			       			GetEducation(request),
								GetKeepKosherStatus(request),
								GetLanguages(request),
								GetHeight(request),
								GetTempleStatus(request),
								GetActivityLevel(request),
								GetEthnicity(request),
								GetRelocateStatus(request),
			       			GetOnlyPhotos(onlyPhotos)
                            
								
			       		}.Where(
							value => String.IsNullOrEmpty(value) == false
							).ToArray()
						) + "&rows=" + totalRecords + "&start=" + startingRow + GetSortType(sortType);
		}

		private String CreateSecondaryUrl(
			SearchRequest request, Int32 startingRow, Int32 totalRows, SortType sortType, Boolean onlyPhotos)
		{
			return String.Join(
						AndConnector,
						new[]
			       		{
			       			Host + Method + GetGender(request),
                            GetSite(request),
			       			GetAge(request),
			       			AddLocation160Miles(request),
			       			GetOnlyPhotos(onlyPhotos),
			       		}.Where(
							value => String.IsNullOrEmpty(value) == false
							).ToArray()
						) + "&rows=" + totalRows + "&start=" + startingRow + GetSortType(sortType);
		}

		private static IEnumerable<Int32> GetMemberIds(XContainer results)
		{
			return from memberId in results.Descendants("int")
					 where memberId.Attribute("name").Value == "memberid"
					 select Int32.Parse(memberId.Value);
		}

		private static XElement Get(String url)
		{
			var response = WebRequest.Create(url).GetResponse();

			using (var reader = XmlReader.Create(response.GetResponseStream()))
			{
				var result = XElement.Load(reader);

				return result;
			}
		}

		private static String GetSortType(SortType sortType)
		{
			switch (sortType)
			{
				case SortType.ClosestToYou:
					return String.Empty;
				case SortType.MostActive:
					return "&sort=lastlogin desc";
				case SortType.BestMatch:
					return "&sort=specificity desc";
				case SortType.Newest:
					return "&sort=signup desc";
				default:
					return String.Empty;
			}
		}

		protected String GetGender(SearchRequest request)
		{
			return String.Format(
				"seekinggender%3A\"{0}\"+AND+gender%3A{1}",
				gender.ContainsKey(request.Gender) ?
					gender[request.Gender] : request.Gender,
				gender.ContainsKey(request.SeekingGender) ?
					gender[request.SeekingGender] : request.SeekingGender
				);
		}

		protected String GetAge(SearchRequest request)
		{
			return String.Format("preferredagemin%3A[18+TO+{0}]+AND+preferredagemax%3A[{0}+TO+99]", request.Age);
		}

		protected String GetHeight(SearchRequest request)
		{
			if (request.Height < 100 || request.Height > 246) return String.Empty;

			return String.Format("preferredheightmin%3A[100+TO+{0}]+AND+preferredheightmax%3A[{0}+TO+300]", request.Height);
		}

		protected String AddAgePlusMinus5(SearchRequest request)
		{
			return String.Format(
				"preferredagemin%3A[18+TO+{0}]+AND+preferredagemax%3A[{1}+TO+99]",
				request.Age + 5,
				request.Age - 5
			);
		}

		protected String GetLocation(SearchRequest request)
		{
			return String.Format(
				"preferredlatitudemin%3A[-90+TO+{0}]+AND+preferredlatitudemax%3A[{0}+TO+90]+AND+preferredlongitudemin%3A[-180+TO+{1}]+AND+preferredlongitudemax%3A[{1}+TO+180]",
				request.Latitude,
				request.Longitude
			);
		}

		protected String AddLocation160Miles(SearchRequest request)
		{
			return String.Format(
				"preferredlatitudemin160%3A[-90+TO+{0}]+AND+preferredlatitudemax160%3A[{0}+TO+90]+AND+preferredlongitudemin160%3A[-180+TO+{1}]+AND+preferredlongitudemax160%3A[{1}+TO+180]",
				request.Latitude,
				request.Longitude
				);
		}

		protected String GetLanguages(SearchRequest request)
		{
			if (request.Languages == null || request.Languages.Count() < 1) return String.Empty;

			return String.Format(
				"preferredlanguage%3A({0})",
				String.Join(
					" OR ",
					request.Languages.Select(language => String.Format("\"{0}\"", language)).ToArray()
				)
			);
		}

		protected String GetTempleStatus(SearchRequest request)
		{
			if (IsValidValue(request.TempleAttendance) == false) return String.Empty;

			return String.Format("preferredtempleattendance%3A\"{0}\"", request.TempleAttendance).
				Replace("on some shabbats", "on some shabbat");
		}

		protected String GetPreferredRelocateStatus(SearchRequest request)
		{
			if (IsValidValue(request.Relocate) == false) return String.Empty;

			return String.Format("preferredrelocatestatus%3A\"{0}\"", request.Relocate);
		}

		protected String GetSmokingStatus(SearchRequest request)
		{
			if (IsValidValue(request.SmokingHabits) == false) return String.Empty;

			return String.Format(
				"preferredsmokingstatus%3A\"{0}\"",
				smokingStatus.ContainsKey(request.SmokingHabits) ?
					smokingStatus[request.SmokingHabits] : request.SmokingHabits
					).Replace("occasionally", "occasional smoker");
		}

		protected String GetJDateEthnicity(SearchRequest request)
		{
			if (IsValidValue(request.Ethnicity) == false) return String.Empty;

			return String.Format("preferredethnicity%3A\"{0}\"", request.Ethnicity);
		}


		protected String GetDrinkingStatus(SearchRequest request)
		{
			if (IsValidValue(request.DrinkingHabits) == false) return String.Empty;

			return String.Format("preferreddrinking%3A\"{0}\"", request.DrinkingHabits);
		}

		protected String GetKeepKosherStatus(SearchRequest request)
		{
			if (IsValidValue(request.KeepKosher) == false) return String.Empty;

			return String.Format("preferredkeepkosher%3A\"{0}\"", request.KeepKosher);
		}

		protected String GetJDateReligion(SearchRequest request)
		{
			if (IsValidValue(request.JDateReligion) == false) return String.Empty;

			return String.Format(
				"preferredreligion%3A\"{0}\"",
				jDateReligion.ContainsKey(request.JDateReligion) ?
					jDateReligion[request.JDateReligion] : request.JDateReligion
				);
		}

		protected String GetOnlyPhotos(Boolean onlyPhotos)
		{
			return onlyPhotos == false ? String.Empty : "hasphoto:true";
		}

		protected String GetEducation(SearchRequest request)
		{
			if (IsValidValue(request.EducationLevel) == false) return String.Empty;

			return String.Format("preferrededucation%3A\"{0}\"",
				educationLevel.ContainsKey(request.EducationLevel) ?
					educationLevel[request.EducationLevel] : request.EducationLevel
				);
		}

        protected string GetSite(SearchRequest request)
        {
            return String.Format("site%3A{0}", request.SiteID);
        }
        

		protected String GetActivityLevel(SearchRequest request)
		{
			if (IsValidValue(request.ActivityLevel) == false) return String.Empty;

			return String.Format("preferredactivitylevel%3A\"{0}\"", request.ActivityLevel);
		}

		protected String AddDrinkingStatus(SearchRequest request)
		{
			if (IsValidValue(request.DrinkingHabits) == false) return String.Empty;

			return String.Format("preferreddrinking%3A\"{0}\"", request.DrinkingHabits);
		}

		protected String GetMaritalStatus(SearchRequest request)
		{
			if (IsValidValue(request.MaritalStatus) == false) return String.Empty;

			return String.Format("preferredmaritalstatus%3A\"{0}\"", request.MaritalStatus);
		}

		protected String GetRelocateStatus(SearchRequest request)
		{
			if (IsValidValue(request.Relocate) == false) return String.Empty;

			return String.Format("preferredrelocatestatus%3A\"{0}\"", request.Relocate);
		}

		protected String GetEthnicity(SearchRequest request)
		{
			if (IsValidValue(request.Ethnicity) == false) return String.Empty;

			return String.Format("preferredethnicity%3A\"{0}\"", request.Ethnicity);
		}

		protected String GetHaveChildren(SearchRequest request)
		{
			if (IsValidValue(request.WantsChildren) == false ||
				request.WantsChildren.ToLower() == Constants.Text.any) return String.Empty;

			return String.Format("preferredwantschildren%3A\"{0}\"", request.WantsChildren);
		}

		protected Boolean IsValidValue(String value)
		{
			return String.IsNullOrEmpty(value) == false && value != Constants.WillTellYouLater;
		}
	}
}