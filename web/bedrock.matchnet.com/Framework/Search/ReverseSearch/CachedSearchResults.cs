﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Caching;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Framework.Search.ReverseSearch
{
	[Serializable]
	public class CachedSearchResults
	{
		public Boolean OnlyPhotos { get; set; }

		public Int32 TotalRecords { get; set; }

		public SearchResponse Response { get; set; }

		public static SearchResponse CreateResponse(CachedSearchResults results, Int32 startingRow)
		{
			var memberIds = new List<Int32>(results.Response.MemberIds);

			return new SearchResponse
			{
				MemberIds = memberIds.GetRange(startingRow, GetEndRow(startingRow, memberIds.Count)),
				TotalResults = memberIds.Count
			};
		}

		private static Int32 GetEndRow(Int32 startingRow, Int32 total)
		{
			return (startingRow + Constants.Paging.DefaultPageSize) > total ?
				total - startingRow : Constants.Paging.DefaultPageSize;
		}

		public static String GetCacheKey(ContextGlobal g)
		{
			return "rs-closest-" + g.Member.MemberID;
		}

		private static Coordinate GetMemberCoordinate(ContextGlobal g, Int32 memberId)
		{
			var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);

			var region = RegionSA.Instance.RetrievePopulatedHierarchy(
				member.GetAttributeInt(g.Brand, Constants.Attributes.RegionId), g.Brand.Site.LanguageID, 3);

			return new Coordinate { Latitude = (Double)region.Latitude, Longitude = (Double)region.Longitude };
		}

		private static Coordinate GetMemberCoordinate(ContextGlobal g)
		{
			var region = RegionSA.Instance.RetrievePopulatedHierarchy(
				g.Member.GetAttributeInt(g.Brand, Constants.Attributes.RegionId), g.Brand.Site.LanguageID, 3);

			return new Coordinate { Latitude = (Double)region.Latitude, Longitude = (Double)region.Longitude };
		}

		private static SearchResponse Sort(ContextGlobal g, SearchResponse response)
		{
			var memberCoordinate = GetMemberCoordinate(g);

			var memberDistances = new List<MemberDistance>();

			Array.ForEach(response.MemberIds.ToArray(), 
				memberId =>
				{
					var coordinate = GetMemberCoordinate(g, memberId);

					memberDistances.Add(
						new MemberDistance
						{
							MemberId = memberId,
							Location = coordinate,
							Distance = Coordinate.GetDistance(memberCoordinate, coordinate),
						}
					);
				}
			);

			return new SearchResponse
			{
				MemberIds = from m in memberDistances orderby m.Distance select m.MemberId,
				TotalResults = response.TotalResults,
			};
		}

		public static SearchResponse CacheAndCreate(
			ContextGlobal g,
			SearchResponse results,
			Int32 startingRow,
			Boolean onlyPhotos
		)
		{
            return CreateResponse(SortAndCacheToWeb(g, results, onlyPhotos), startingRow);
		}

        public static CachedSearchResults SortAndCacheToWeb(
            ContextGlobal g,
            SearchResponse results,            
            Boolean onlyPhotos
        )
        {
            var cachedResults = new CachedSearchResults
            {
                OnlyPhotos = onlyPhotos,
                Response = Sort(g, results),
                TotalRecords = results.TotalResults
            };

            g.Cache.Add(
                GetCacheKey(g),
                cachedResults,
                null,
                DateTime.Now.AddMinutes(10),
                TimeSpan.Zero,
                CacheItemPriority.Normal,
                null
            );

            return cachedResults;
        }
	}
}