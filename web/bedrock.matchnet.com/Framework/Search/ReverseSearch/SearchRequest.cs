﻿using System;
using System.Collections.Generic;
using System.Linq;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Applications.MemberProfile.Controls;

namespace Matchnet.Web.Framework.Search.ReverseSearch
{
	[Serializable]
	public class SearchRequest
	{
		private const Double RadiansToDegrees = 180 / Math.PI;

		private const Int32 DefaultHaveChildren = 4;

		private static readonly Dictionary<Int32, String> moreChildren = new Dictionary<Int32, String>
     	{
     		{1, "yes"},
     		{0, "no"},
     		{2, "not sure"},
     		{4, "any"},
     	};

		#region Properties

		public Int32 Age { get; set; }

		public Double Latitude { get; set; }

		public Double Longitude { get; set; }

		public String Gender { get; set; }

		public String SeekingGender { get; set; }

		public String TempleAttendance { get; set; }

		public String SmokingHabits { get; set; }

		public String KeepKosher { get; set; }

		public String JDateReligion { get; set; }

		public String EducationLevel { get; set; }

		public String ActivityLevel { get; set; }

		public String DrinkingHabits { get; set; }

		public String WantsChildren { get; set; }

		public String Relocate { get; set; }

		public IEnumerable<String> Languages { get; set; }

		public Int32 Height { get; set; }

		public Int32 Weight { get; set; }

		public String MaritalStatus { get; set; }

		public String Ethnicity { get; set; }

        public Int32 SiteID { get; set; }

		#endregion Properties

		private static String CleanValue(String value)
		{
			if (String.IsNullOrEmpty(value)) return String.Empty;

			return value.ToLower().
				Replace(Constants.Text.Underscore, Constants.Text.Space).
				Replace(Constants.Text.Apostrophe, String.Empty).
				Replace(Constants.Text.SecondApostrophe, String.Empty).
				Replace(Constants.Text.LeftParentheses, String.Empty).
				Replace(Constants.Text.RightParentheses, String.Empty).
				Replace(Constants.Text.Dash, Constants.Text.Space).
				Trim();
		}

		private static String GetWillingToRelocateDisplay(ContextGlobal g)
		{
			switch (g.Member.GetAttributeInt(g.Brand, "relocateflag"))
			{
				case 1:
					return "yes";
				case 0:
				case 2:
					return "no";
				case 4:
					return "not sure";
				default:
					return String.Empty;
			}
		}

		public static SearchRequest Create(ContextGlobal g)
		{
			if (g == null) throw new ArgumentNullException("g");

            Brand brand = g.Brand;

            if (brand.BrandID == (int)WebConstants.BRAND_ID.JDateCoIL || brand.BrandID == (int)WebConstants.BRAND_ID.JDateFR)
            {
                brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID((int)WebConstants.BRAND_ID.JDate);
            }

			var region = RegionSA.Instance.RetrievePopulatedHierarchy(
				g.Member.GetAttributeInt(g.Brand, Constants.Attributes.RegionId), g.Brand.Site.LanguageID, 3);

            var genderMask = g.Member.GetAttributeInt(brand, Constants.Attributes.GenderMask);

			return new SearchRequest
			{
				Age = GetAge(g),

				Latitude = FormatLocationComponent(region.Latitude),

				Longitude = FormatLocationComponent(region.Longitude),

				Gender = FrameworkGlobals.GetGenderString(genderMask, brand).ToLower(),

				SeekingGender = FrameworkGlobals.GetSeekingGenderString(genderMask, brand).ToLower(),

				TempleAttendance = CleanValue(ProfileDisplayHelper.GetOptionValue(
					g.Member, brand, Constants.Attributes.SynagogueAttendance, Constants.Attributes.SynagogueAttendance)),

				SmokingHabits = CleanValue(ProfileDisplayHelper.GetOptionValue(
                    g.Member, brand, Constants.Attributes.SmokingHabits, Constants.Attributes.SmokingHabits)).
					Replace(Constants.Text.Regularly, Constants.Text.SmokesRegularly),

				KeepKosher = CleanValue(ProfileDisplayHelper.GetOptionValue(
                    g.Member, brand, Constants.Attributes.KeepKosher, Constants.Attributes.KeepKosher)),

				JDateReligion = CleanValue(ProfileDisplayHelper.GetOptionValue(
                    g.Member, brand, Constants.Attributes.JDateReligion, Constants.Attributes.JDateReligion)).
					Replace(Constants.Text.CulturallyJewish, Constants.Text.Secular).
					Replace(Constants.Text.AnotherStreamOfJudaism, Constants.Text.AnotherStream),

				EducationLevel = CleanValue(ProfileDisplayHelper.GetOptionValue(
                    g.Member, brand, Constants.Attributes.EducationLevel, Constants.Attributes.EducationLevel)),

				ActivityLevel = CleanValue(ProfileDisplayHelper.GetOptionValue(
                    g.Member, brand, Constants.Attributes.ActivityLevel, Constants.Attributes.ActivityLevel)),

				DrinkingHabits = CleanValue(ProfileDisplayHelper.GetOptionValue(
                    g.Member, brand, Constants.Attributes.DrinkingHabits, Constants.Attributes.DrinkingHabits)),

				WantsChildren =
                    CleanValue(moreChildren.ContainsKey(g.Member.GetAttributeInt(brand, Constants.Attributes.HaveChildren)) ?
                    moreChildren[g.Member.GetAttributeInt(brand, Constants.Attributes.HaveChildren)] :
					moreChildren[DefaultHaveChildren]),

                Height = g.Member.GetAttributeInt(brand, Constants.Attributes.Height),

                Weight = g.Member.GetAttributeInt(brand, Constants.Attributes.Weight),

				MaritalStatus = CleanValue(ProfileDisplayHelper.GetOptionValue(
                    g.Member, brand, Constants.Attributes.MaritalStatus, Constants.Attributes.MaritalStatus)),

				Ethnicity = CleanValue(ProfileDisplayHelper.GetOptionValue(
                    g.Member, brand, Constants.Attributes.Ethnicity, Constants.Attributes.Ethnicity)),

				Languages = GetLanguages(g, brand),

				Relocate = GetWillingToRelocateDisplay(g),

                SiteID = g.Brand.Site.SiteID
			};
		}

		private static IEnumerable<String> GetLanguages(ContextGlobal g, Brand brand)
		{
            var value = ProfileDisplayHelper.GetMaskContent(g.Member, brand, Constants.Attributes.LanguageMask);

			if (String.IsNullOrEmpty(value)) return new String[] {};

			return value.Replace(Constants.Text.SpaceString, String.Empty).
				Split(Constants.Text.Comma).Select(
					language => language.Trim().ToLower()
				).Where(
					language => !String.IsNullOrEmpty(language)
			);
		}

		private static Double FormatLocationComponent(Decimal locationComponent)
		{
			return (Double)locationComponent * RadiansToDegrees;
		}

		private static Int32 GetAge(ContextGlobal g)
		{
			return (Int32)(DateTime.Now.Subtract(g.Member.GetAttributeDate(g.Brand, Constants.Attributes.BirthDate)).TotalDays /
								Constants.Numbers.DaysPerYear);
		}
	}
}