﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Matchnet.Web.Facelink.Search;
using Matchnet.DistributedCaching.ServiceAdapters;
using System.Diagnostics;

namespace Matchnet.Web.Framework.Search
{
	/// <summary>
	/// KeywordSearcher is a static helper class that offers a method
	/// to get Facelink keyword search results.
	/// </summary>
	public static class KeywordSearcher
	{
        private const string SEARCH_RESULTS_NAMED_CACHE = "searchresults";
        private static int maximumUnfilteredResults = Matchnet.Constants.NULL_INT;

        private static int MaximumUnfilteredResults
        {
            get
            {
                //if (maximumUnfilteredResults == Matchnet.Constants.NULL_INT)
                //{
                //    maximumUnfilteredResults = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MAXIMUM_UNFILTERED_KEYWORD_SEARCH"));  
                //}

                return Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MAXIMUM_UNFILTERED_KEYWORD_SEARCH"));
            }
        }

		/// <summary>
		/// Retrieves a list of keyword search results based on
		/// age, location, and preference.
		/// </summary>
		/// <param name="ageId">The age id represents an age range.</param>
		/// <param name="location">The location for the search.</param>
		/// <param name="preference">The search preference, such as m2f, which represents a man
		/// seeking women.</param>
		/// <param name="query">The query.</param>
		/// <param name="page">The page.</param>
		/// <param name="pageSize">The page size.</param>
		/// <returns>
		/// A response containing the total number of results, along
		/// with a list of matches.
		/// </returns>
		public static searchResultResponse GetKeywordSearchResults(
            Int32 memberId,
			Int32 ageId,
			String location,
			String preference,
			String query,
			Int32 page,
			Int32 pageSize, 
            Int32 communityID)
		{
            bool isProfileBlockingEnabled = false;
            isProfileBlockingEnabled = (memberId != Matchnet.Constants.NULL_INT &&
                Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROFILE_BLOCK_ENABLED", communityID)));

            string cacheKey = string.Empty;

            // if profile blocking enabled, we have a separate cache that stores the filtered results
            if(isProfileBlockingEnabled)
            {
                Int32[] memberIDs = null;
                try
                {
                cacheKey = GetMemberSpecificCacheKey(memberId, ageId, location, preference, query, communityID);
                    memberIDs = DistributedCachingSA.Instance.GetArray(cacheKey, SEARCH_RESULTS_NAMED_CACHE) as Int32[];
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("KeywordSearcher.cs", "Error retrieving keyword results from cache! " + ex.ToString(), EventLogEntryType.Error);
                }

                if (memberIDs != null)
                {
                    searchResultItem[] searchItems = new searchResultItem[memberIDs.Length];

                    for (int i = 0; i < memberIDs.Length; i++)
                    {
                        searchItems[i] = new searchResultItem { memberId = memberIDs[i].ToString() };
                    }

                    searchResultResponse searchResp = new searchResultResponse();
                    searchResp.getSearchResult = searchItems;
                    searchResp.total = searchItems.Length;

                    return GetPageWorthOfMembers(searchResp, page, pageSize);
                }
            }

            // either no cache hit or this profile blocking is not even enabled. continue with contacting facelink for the search results
			var client = new SearchPortTypeClient();

			var cleanLocation = location.StripMultipleSpaces();

			if (String.IsNullOrEmpty(cleanLocation) || cleanLocation == Constants.Literals.NoneSelected)
			{
				cleanLocation = Constants.Literals.SpaceString;
			}
            query = FixSiteCode(query.StripMultipleSpaces().ReplaceSpacesWithPluses(), communityID);

            // filter and cache if profile blocking feature is enabled
            if (isProfileBlockingEnabled)
            {
                searchResultResponse searchResponse = client.getSearchResults(
                new searchRequest
                {
                    age = ageId,
                    location = cleanLocation,
                    preference = preference,
                    query = query,
                    page = "0",
                    pageSize = MaximumUnfilteredResults.ToString()
                }
                );

                searchResultResponse filteredSearchResults = RemovedExcludedMembers(memberId, communityID, searchResponse);

                try
                {
                    DistributedCachingSA.Instance.Put(cacheKey, filteredSearchResults.getSearchResult.Select(item => Int32.Parse(item.memberId)).ToArray<Int32>(), SEARCH_RESULTS_NAMED_CACHE);
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("KeywordSearcher.cs", "Error setting keyword results in cache! " + ex.ToString(), EventLogEntryType.Error);
                }

                return GetPageWorthOfMembers(filteredSearchResults, page, pageSize);
            }
            else
            {
                return client.getSearchResults(
                new searchRequest
                {
                    age = ageId,
                    location = cleanLocation,
                    preference = preference,
                    query = query,
                    page = page.ToString(),
                    pageSize = pageSize.ToString()
                }
                );
            }
		}

        private static searchResultResponse GetPageWorthOfMembers(searchResultResponse searchResults, int page, int pageSize)
        {
            searchResultItem[] pageWorth = null;
            int arraySize = 0;

            // page variable is actually the index of the item. make sure the requested index is within range
            if (page < searchResults.getSearchResult.Length)
            {
                arraySize = (searchResults.getSearchResult.Length - page) > pageSize ? pageSize : (searchResults.getSearchResult.Length - page);
            }

            pageWorth = new searchResultItem[arraySize];

            if (arraySize > 0)
            {
                Array.Copy(searchResults.getSearchResult, page, pageWorth, 0, arraySize);
            }

            searchResultResponse pageWorthResults = new searchResultResponse();
            pageWorthResults.getSearchResult = pageWorth;
            pageWorthResults.total = searchResults.total;

            return pageWorthResults;
        }

        private static searchResultResponse RemovedExcludedMembers(int memberId, int communityId, searchResultResponse searchResults)
        {
            Matchnet.List.ServiceAdapters.List list = Matchnet.List.ServiceAdapters.ListSA.Instance.GetList(memberId);

            // if list is null, don't bother with filtering
            if (list == null)
                return searchResults;

            // Filter against our ExcludeListInternal
            var filtered = from sr in searchResults.getSearchResult
                           where !list.IsHotListed(Matchnet.List.ValueObjects.HotListCategory.ExcludeListInternal, communityId, Convert.ToInt32(sr.memberId))
                           select sr;

            searchResultResponse filteredResults = new searchResultResponse();
            filteredResults.getSearchResult = filtered.ToArray<searchResultItem>();
            filteredResults.total = filtered.Count();

            return filteredResults;
        }

        private static string GetMemberSpecificCacheKey(int memberId, int ageId, string location, string preference, string query, int communityId)
        {
            string hash = HashString(string.Join("_", new string[] {
                "EXCL_KEYWORD",
                memberId.ToString(),
                ageId.ToString(),
                location,
                preference,
                query,
                communityId.ToString()}));
            return hash;
        }

        private static string HashString(string Value)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] data = System.Text.Encoding.ASCII.GetBytes(Value);
            data = x.ComputeHash(data);
            string ret = "";
            for (int i = 0; i < data.Length; i++)
                ret += data[i].ToString("x2").ToLower();
            return ret;
        }

		/// <summary>
		/// Retrieves a list of keyword search results based on a search parameter object.
		/// </summary>
		/// <param name="searchParameters">The search parameter object.</param>
		/// <returns>
		/// A response containing the total number of results, along
		/// with a list of matches.
		/// </returns>
		public static searchResultResponse GetKeywordSearchResults(Int32 memberId, SearchParameters searchParameters)
		{
			if (searchParameters == null) throw new ArgumentNullException("searchParameters");

			return GetKeywordSearchResults(
                memberId,
				Int32.Parse(searchParameters.AgeRange),
				searchParameters.Location,
				String.Join( String.Empty, new[]{ searchParameters.Seeker, 2.ToString(), searchParameters.Seeking } ),
				searchParameters.Query,
				Int32.Parse(searchParameters.Page),
				Constants.Paging.DefaultPageSize, 
                searchParameters.CommunityID
			);
		}

		/// <summary>
		/// Removes jdate+from+us from queries.
		/// </summary>
		/// <param name="s">The string to remove jdate+from+us from.</param>
		/// <returns>A string without jdate+from+us, if it had it.</returns>
		public static String RemoveSiteCodeUS(this String s)
		{
			if (String.IsNullOrEmpty(s)) return s;

			var cleanString = s.Replace(Constants.Literals.Space, Constants.Literals.Plus);

			var result = cleanString.Contains(Constants.Literals.FromJDate) ?
				cleanString.Replace(Constants.Literals.FromJDate, String.Empty) : cleanString;

            result = cleanString.Contains(Constants.Literals.FromSparkCom) ?
            cleanString.Replace(Constants.Literals.FromSparkCom, String.Empty) : cleanString;

            result = cleanString.Contains(Constants.Literals.FromBBW) ?
            cleanString.Replace(Constants.Literals.FromBBW, String.Empty) : cleanString;

            result = cleanString.Contains(Constants.Literals.FromBlack) ?
            cleanString.Replace(Constants.Literals.FromBlack, String.Empty) : cleanString;

			return result.Replace(Constants.Literals.Plus, Constants.Literals.Space);
		}

		private static String ReplaceSpacesWithPluses(this String s)
		{
			return s.Replace(Constants.Literals.Space, Constants.Literals.Plus);
		}

		private static String StripMultipleSpaces(this String s)
		{
			return Regex.Replace(s, @"\s+", @" ");
		}

		private static String JDateize(this String s)
		{
			return s.Contains(Constants.Literals.JDate) ? s : s + Constants.Literals.FromJDate;
		}

        private static String FixSiteCode(String s, Int32 communityID)
        {
            String newString = String.Empty;
            switch (communityID)
            {
                case (int)WebConstants.COMMUNITY_ID.JDate:
                    newString = (s.Contains(Constants.Literals.JDate) ? s : s + Constants.Literals.FromJDate).Replace(Constants.Literals.FromJDate, Constants.Literals.JDateCode);
                    break;
                case (int)WebConstants.COMMUNITY_ID.AmericanSingles:
                    newString = (s.Contains(Constants.Literals.SparkCom) ? s : s + Constants.Literals.FromJDate).Replace(Constants.Literals.FromSparkCom, Constants.Literals.SparkComCode);
                    break;
                case (int)WebConstants.COMMUNITY_ID.BBWPersonalsPlus:
                    newString = (s.Contains(Constants.Literals.BBWCode) ? s : s + Constants.Literals.FromBBW).Replace(Constants.Literals.FromBBW, Constants.Literals.BBWCode);
                    break;
                case (int)WebConstants.COMMUNITY_ID.BlackSingles:
                    newString = (s.Contains(Constants.Literals.BlackCode) ? s : s + Constants.Literals.FromBlack).Replace(Constants.Literals.FromBlack, Constants.Literals.BlackCode);
                    break;
            }

            return newString;
        }
	}

}
