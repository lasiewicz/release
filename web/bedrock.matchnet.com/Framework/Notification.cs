using Matchnet.Content;

namespace Matchnet.Web.Framework
{
	public enum NotificationType: int
	{
		Normal,
		Error
	}

	public class Notification
	{
		private NotificationType	_notifyType;
		private string				_resourceConstant;
		private string[]			_args;
		private string				_message= "";

		#region Constructors
        public Notification(NotificationType notifyType)
        {
        	_notifyType = notifyType;
        	_message = "undefined";
        }

        // Constructors that use ResourceConstants
		public Notification(NotificationType notifyType, string resourceConstant)
		{
			_notifyType = notifyType;
			_resourceConstant = resourceConstant;
        }

		public Notification(NotificationType notifyType, string resourceConstant, string[] args) : this(notifyType, resourceConstant)
		{
			_args = args;
		}
		#endregion

		public override bool Equals(object obj)
		{
			if (obj == null) return false;

			if (this.GetType() != obj.GetType()) return false;

			// safe because of the GetType check
			Notification notification = (Notification)obj;

			// use this pattern to compare value members
			if (!Message.Equals(notification.Message)) return false;
			
			// need to do something for args.
			if (Args != null) 
			{
				if (notification.Args == null) return false;
				if (Args.Length != notification.Args.Length) return false;

				for (int i = 0; i < Args.Length; i++) 
				{
					if (Args[i] != notification.Args[i]) return false;
				}
			} 
			else 
			{
				if (notification.Args != null) return false;
			}
			return true;
		}

		public override int GetHashCode()
		{
			return _resourceConstant.GetHashCode();
		}

		#region Public Properties
		public NotificationType NotifyType
		{
			get { return _notifyType; }
		}

		public string ResourceConstant
		{
			get { return _resourceConstant; }
		}

		public string[] Args
		{
			get { return _args; }
		}

		public string Message
		{
			get { return _message; }
            set { _message = value; }
		}
		#endregion
	}
}
