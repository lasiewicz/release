using System;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework
{
	/// <summary>
	/// Summary description for MemberPrivilegeAttr.
	/// </summary>
	public class MemberPrivilegeAttr
	{
		public const int NonPermitMember = 0;
		public const int PermitMember = 1; // 1 bit
		public const int SubscribedMember = 1023; //10 bits 111111111, subscribe member should have privilege to everything

		/// <summary>
		/// This class describes all the methods to deterimine members permimisions
		/// </summary>
		public MemberPrivilegeAttr()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		/// <summary>
		/// method returns bitmask that describe a member privilege
		/// </summary>
		/// <param name="g"></param>
		/// <returns></returns>
		public static int GetMemberPrivilegeAtt(ContextGlobal g)
		{
			int memberPrivilege = NonPermitMember;

			//grab configuration is photo approved access is for this site
			string strConfigSetting = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTO_FEATURE_ACCESS",g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
			if("TRUE" == strConfigSetting.Trim().ToUpper())
			{
				//check if a member has an approved photo
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                if (null != MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, g.Member, g.Brand))
				{
					memberPrivilege = memberPrivilege | PermitMember;
				}
			}

			//grab configuration to see if paid features is accessable for promotion
			strConfigSetting = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FTPlay_FEATURE_ACCESS",g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
			if("TRUE" == strConfigSetting.Trim().ToUpper())
			{
				memberPrivilege = memberPrivilege | PermitMember;
			}

			if(g.Member != null)
			{
				//check if a member currently has a subscription 
				if(g.Member.IsPayingMember(g.Brand.Site.SiteID))
				{
					memberPrivilege = memberPrivilege | SubscribedMember;
				}
			}
			
			return memberPrivilege;
		}
		
		/// <summary>
		/// check if a member has the current promotional access
		/// </summary>
		/// <param name="g"></param>
		/// <returns></returns>
		public static bool IsPermitMember(ContextGlobal g)
		{
			int memberPrivilege = GetMemberPrivilegeAtt(g);

			if(PermitMember ==  (PermitMember & memberPrivilege))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// check if the member is currently a subscriber
		/// </summary>
		/// <param name="g"></param>
		/// <returns></returns>
		public static bool IsCureentSubscribedMember(ContextGlobal g)
		{
			int memberPrivilege = GetMemberPrivilegeAtt(g);
			if(SubscribedMember == (SubscribedMember & memberPrivilege) )
			{
				return true;
			}
			else
			{
				return false;
			}
		}


        public static int GetMemberPrivilegeAtt(Member.ServiceAdapters.Member member, ContextGlobal g)
        {
            int memberPrivilege = NonPermitMember;

            //grab configuration is photo approved access is for this site
            string strConfigSetting = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTO_FEATURE_ACCESS", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
            if ("TRUE" == strConfigSetting.Trim().ToUpper())
            {
                //check if a member has an approved photo
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                if (null != MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, member, g.Brand))
                {
                    memberPrivilege = memberPrivilege | PermitMember;
                }
            }

            //grab configuration to see if paid features is accessable for promotion
            strConfigSetting = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FTPlay_FEATURE_ACCESS", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
            if ("TRUE" == strConfigSetting.Trim().ToUpper())
            {
                memberPrivilege = memberPrivilege | PermitMember;
            }

            if (member != null)
            {
                //check if a member currently has a subscription 
                if (member.IsPayingMember(g.Brand.Site.SiteID))
                {
                    memberPrivilege = memberPrivilege | SubscribedMember;
                }
            }

            return memberPrivilege;
        }

        /// <summary>
        /// check if a member has the current promotional access
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public static bool IsPermitMember(Member.ServiceAdapters.Member member, ContextGlobal g)
        {
            int memberPrivilege = GetMemberPrivilegeAtt(member,g);

            if (PermitMember == (PermitMember & memberPrivilege))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// check if the member is currently a subscriber
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public static bool IsCureentSubscribedMember(Member.ServiceAdapters.Member member, ContextGlobal g)
        {
            int memberPrivilege = GetMemberPrivilegeAtt(member,g);
            if (SubscribedMember == (SubscribedMember & memberPrivilege))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
	}
}
