using System;
using System.Web.UI.WebControls;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Security;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Content.ServiceAdapters.Links;
using Spark.Common.Chat;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework
{

    /// <summary>
    /// Summary description for OnlineLinkHelper.
    /// </summary>
    public class OnlineLinkHelper : FrameworkControl
    {
        private bool isOnline = false;
        private string imageName = "icon-status-offline.gif";
        private string offlineImageName = "icon-status-offline.gif";
        private string onlineImageName;
        private string onlineLink = string.Empty;
        private int prtId = int.MinValue;
        public bool IsOnline
        {
            get { return isOnline; }
            set { isOnline = value; }
        }

        public string ImageName
        {
            get { return imageName; }
            set { imageName = value; }
        }

        public string OfflineImageName
        {
            get { return offlineImageName; }
            set { offlineImageName = value; }
        }

        public string OnlineImageName
        {
            get { return onlineImageName; }
            set { onlineImageName = value; }
        }

        public string OnlineLink
        {
            get { return onlineLink; }
            set { onlineLink = value; }
        }

        public int PRTID
        {
            get { return prtId; }
            set { prtId = value; }
        }

        /// <summary>
        /// Constructor for online link helper
        /// </summary>
        /// <param name="displayContext"></param>
        /// <param name="receivingMember">intended recipient of IM request</param>
        /// <param name="sendingMember">Member who initiated IM request</param>
        /// <param name="linkParent">The source of the original click on IM</param>
        public OnlineLinkHelper(ResultContextType displayContext, IMemberDTO receivingMember, IMemberDTO sendingMember, LinkParent linkParent)
        {
            SetIsOnline(displayContext, receivingMember);

            bool sendVal = false;

            if (sendingMember != null)
            {
                sendVal = g.ValidateMemberContact(sendingMember.MemberID);
            }

            Populate(receivingMember, sendVal, linkParent);
        }

        /// <summary>
        /// Constructor for online link helper
        /// </summary>
        /// <param name="displayContext"></param>
        /// <param name="receivingMember">intended recipient of IM request</param>
        /// <param name="sendingMember">Member who initiated IM request</param>
        /// <param name="linkParent">The source of the original click on IM</param>
        public OnlineLinkHelper(ResultContextType displayContext, IMemberDTO receivingMember, IMemberDTO sendingMember, LinkParent linkParent, string evar27ForIM)
        {
            SetIsOnline(displayContext, receivingMember);

            bool sendVal = false;

            if (sendingMember != null)
            {
                sendVal = g.ValidateMemberContact(sendingMember.MemberID);
            }

            Populate(receivingMember, sendVal, linkParent, evar27ForIM);
        }



        public OnlineLinkHelper(ResultContextType displayContext, IMemberDTO receivingMember, IMemberDTO sendingMember, LinkParent linkParent, string imgonline, string imgoffline)
        {
            SetIsOnline(displayContext, receivingMember);

            bool sendVal = false;

            if (sendingMember != null)
            {
                sendVal = g.ValidateMemberContact(sendingMember.MemberID);
            }

            imageName = imgonline;
            onlineImageName = imgonline;
            offlineImageName = imgoffline;
            Populate(receivingMember, sendVal, linkParent);
        }
        
        /// <summary>
        ///		Renders the appropriate image, alt text and URL for the IM link
        /// </summary>
        /// <param name="displayContext">The context in which the profile is being displayed.</param>
        /// <param name="receivingMember">The intended recipient of IM initiation request</param>
        /// <param name="sendingMember">The initiator of IM request</param>
        /// <param name="imgChat">the image or icon that shows online presence</param>
        /// <param name="lnkOnline"></param>
        /// <param name="linkParent">The source of the original IM request (e.g., mini-profile etc.)</param>
        /// <param name="isOnline"></param>
        public static void SetOnlineLink(ResultContextType displayContext, IMemberDTO receivingMember, IMemberDTO sendingMember, Image imgChat, HyperLink lnkOnline, LinkParent linkParent, out bool isOnline)
        {
            OnlineLinkHelper linkHelper = new OnlineLinkHelper(displayContext, receivingMember, sendingMember, linkParent);

            imgChat.FileName = linkHelper.ImageName;

            isOnline = linkHelper.IsOnline;

            if (isOnline)
            {
                if (lnkOnline != null)
                {
                    lnkOnline.NavigateUrl = linkHelper.OnlineLink;
                }
                else
                {
                    imgChat.NavigateUrl = linkHelper.OnlineLink;
                }
            }
            else
            {
                ContextGlobal g = (ContextGlobal)System.Web.HttpContext.Current.Items["g"];
                imgChat.TitleResourceConstant = "ALT_IM_OFFLINE";
            }
        }

        public static void SetOnlineLink(ResultContextType displayContext, IMemberDTO receivingMember, IMemberDTO sendingMember, HyperLink lnkOnline, LinkParent linkParent, out bool isOnline)
        {
            OnlineLinkHelper linkHelper = new OnlineLinkHelper(displayContext, receivingMember, sendingMember, linkParent);

            isOnline = linkHelper.IsOnline;

            if (isOnline)
            {
                if (lnkOnline != null)
                {
                    lnkOnline.NavigateUrl = linkHelper.OnlineLink;
                }
              
            }
            else
            {
                ContextGlobal g = (ContextGlobal)System.Web.HttpContext.Current.Items["g"];
               
            }
        }

        public static void SetOnlineLink(ResultContextType displayContext, IMemberDTO receivingMember, IMemberDTO sendingMember, HyperLink lnkOnline, LinkParent linkParent, out bool isOnline, string evar27ForIM)
        {
            OnlineLinkHelper linkHelper = new OnlineLinkHelper(displayContext, receivingMember, sendingMember, linkParent, evar27ForIM);

            isOnline = linkHelper.IsOnline;

            if (isOnline)
            {
                if (lnkOnline != null)
                {
                    lnkOnline.NavigateUrl = linkHelper.OnlineLink;
                }

            }
            else
            {
                ContextGlobal g = (ContextGlobal)System.Web.HttpContext.Current.Items["g"];

            }
        }

        public OnlineLinkHelper(ResultContextType displayContext,
            IMemberDTO receivingMember, IMemberDTO sendingMember,
            LinkParent linkParent, int prtID, string evar27ForIM)
        {
            this.prtId = prtID;
            SetIsOnline(displayContext, receivingMember);

            bool sendVal = false;

            if (sendingMember != null)
            {
                sendVal = g.ValidateMemberContact(sendingMember.MemberID);
            }

            Populate(receivingMember, sendVal, linkParent, evar27ForIM);
        }

        public OnlineLinkHelper(ResultContextType displayContext,
            IMemberDTO receivingMember, IMemberDTO sendingMember,
            LinkParent linkParent, int prtID)
        {
            this.prtId = prtID;
            SetIsOnline(displayContext, receivingMember);

            bool sendVal = false;

            if (sendingMember != null)
            {
                sendVal = g.ValidateMemberContact(sendingMember.MemberID);
            }

            Populate(receivingMember, sendVal, linkParent);
        }

        public static OnlineLinkHelper getOnlineLinkHelper(ResultContextType displayContext, IMemberDTO receivingMember, IMemberDTO sendingMember, LinkParent linkParent, int prtID, out bool isOnline)
        {
            return getOnlineLinkHelper(displayContext, receivingMember, sendingMember, linkParent, prtID, out isOnline,
                string.Empty);
        }

        public static OnlineLinkHelper getOnlineLinkHelper(ResultContextType displayContext, IMemberDTO receivingMember, IMemberDTO sendingMember, LinkParent linkParent, int prtID, out bool isOnline, string evar27ForIM)
        {
            OnlineLinkHelper linkHelper = new OnlineLinkHelper(displayContext, receivingMember, sendingMember,
                linkParent, prtID, evar27ForIM);
            isOnline = linkHelper.IsOnline;
            return linkHelper;
        }

        public static void SetOnlineLink(ResultContextType displayContext, IMemberDTO receivingMember, IMemberDTO sendingMember, Image imgChat, HyperLink lnkOnline, LinkParent linkParent, string imgonline, string imgoffline, out bool isOnline)
        {
            OnlineLinkHelper linkHelper = new OnlineLinkHelper(displayContext, receivingMember, sendingMember, linkParent, imgonline, imgoffline);

            imgChat.FileName = linkHelper.ImageName;

            isOnline = linkHelper.IsOnline;

            if (isOnline)
            {
                if (lnkOnline != null)
                {
                    lnkOnline.NavigateUrl = linkHelper.OnlineLink;
                }
                else
                {
                    imgChat.NavigateUrl = linkHelper.OnlineLink;
                }
            }
            else
            {
                ContextGlobal g = (ContextGlobal)System.Web.HttpContext.Current.Items["g"];
                imgChat.TitleResourceConstant = "ALT_IM_OFFLINE";
            }
        }

        public static void SetOnlineLink(ResultContextType displayContext,
            IMemberDTO receivingMember, IMemberDTO sendingMember,
            Matchnet.Web.Framework.Image imgChat,
            LinkParent linkParent)
        {
            bool isOnline;	// ignore in this context
            SetOnlineLink(displayContext, receivingMember, sendingMember, imgChat, null, linkParent, out isOnline);
        }


        public static void SetOnlineLink(ResultContextType displayContext,
        IMemberDTO receivingMember, IMemberDTO sendingMember,
        Matchnet.Web.Framework.Image imgChat,
        LinkParent linkParent, string imgonline, string imgoffline)
        {
            bool isOnline;	// ignore in this context
            SetOnlineLink(displayContext, receivingMember, sendingMember, imgChat, null, linkParent, imgonline, imgoffline, out isOnline);
        }

        public static void SetOnlineLink(IMemberDTO receivingMember, IMemberDTO sendingMember, Image imgChat, HyperLink lnkOnline, LinkParent linkParent, out bool isOnline)
        {
            //If the ResultContextType is MembersOnline, we don't bother to check whether the user is online when generating the link.
            //Since the result context type is unspecified in this overload, we'll set it to SearchResult to ensure that the check is performed.

            Matchnet.Web.Framework.Ui.BasicElements.ResultContextType rct = Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.SearchResult;
            SetOnlineLink(rct, receivingMember, sendingMember, imgChat, lnkOnline, linkParent, out isOnline);
        }

        public static void SetOnlineLink(IMemberDTO receivingMember, IMemberDTO sendingMember,
            Matchnet.Web.Framework.Image imgChat,
            LinkParent linkParent)
        {
            bool isOnline;	// ignore in this context
            SetOnlineLink(receivingMember, sendingMember, imgChat, null, linkParent, out isOnline);
        }

        public void Populate(IMemberDTO receivingMember, bool validMemberAccess, LinkParent linkParent)
        {
            if (IsOnline)
            {

                if (!String.IsNullOrEmpty(onlineImageName))
                {
                    ImageName = onlineImageName;

                }
                else
                {
                    ImageName = "icon-status-online.gif";
                }

                if (validMemberAccess)
                {
                    PopulateIMLinkPath(receivingMember, string.Empty);
                }
                else
                {
                    PopulateSubscriptionLinkPath(receivingMember, linkParent);
                }
            }
            else
            {
                ImageName = offlineImageName;
            }

        }

        public void Populate(IMemberDTO receivingMember, bool validMemberAccess, LinkParent linkParent, string evar27ForIM)
        {
            if (IsOnline)
            {

                if (!String.IsNullOrEmpty(onlineImageName))
                {
                    ImageName = onlineImageName;

                }
                else
                {
                    ImageName = "icon-status-online.gif";
                }

                if (validMemberAccess)
                {
                    PopulateIMLinkPath(receivingMember, evar27ForIM);
                }
                else
                {
                    PopulateSubscriptionLinkPath(receivingMember, linkParent);
                }
            }
            else
            {
                ImageName = offlineImageName;
            }

        }

        private string EncryptMemberID(IMemberDTO member)
        {
            return Crypto.Encrypt(WebConstants.HTTPIM_ENCRYPT_KEY, member.MemberID.ToString() + ",1," + DateTime.Now);
        }

        /// <summary>
        /// Inserts the javascript code that pops open new Instant Messenger window
        /// </summary>
        /// <param name="receivingMember">The member that the user wants to initiate a conversation with</param>
        /// /// <param name="evar27ForIM">Evar27 for tracking</param>
        public void PopulateIMLinkPath(IMemberDTO receivingMember, string evar27ForIM)
        {
            OnlineLink = GetIMLinkPath(g, receivingMember, evar27ForIM);
        }

        public static string GetIMLinkPath(ContextGlobal g, IMemberDTO receivingMember, string evar27ForIM)
        {
            string IMLinkPath = "";
            if (MembersOnlineManager.Instance.IsConsolidatedWebChatEnabled(g.Brand))
            {
                //Consolidated Ejabberd chat (maintained by Utah team) - Newest
                IMLinkPath = string.Format("javascript:LaunchConsolidatedIMChat('{0}', '{1}')", receivingMember.MemberID.ToString(), Convert.ToString(receivingMember.GetUserName(g.Brand)));
            }
            else if (FrameworkGlobals.GetChatProvider(g.Brand) == ChatProvider.Ejabberd)
            {
                //LA specific Ejabberd chat
                if (string.IsNullOrEmpty(evar27ForIM))
                {
                    IMLinkPath = string.Format("javascript:LaunchNewEjabIMConversation('{0}', '{1}')",
                                           Convert.ToString(receivingMember.MemberID),
                                           RuntimeSettings.GetSetting("WEBCHAT_URL",
                                                                      g.Brand.Site.Community.CommunityID,
                                                                      g.Brand.Site.SiteID, g.Brand.BrandID));
                }
                else
                {
                    IMLinkPath = string.Format("javascript:LaunchNewEjabIMConversation('{0}', '{1}', '{2}')",
                                           Convert.ToString(receivingMember.MemberID), RuntimeSettings.GetSetting("WEBCHAT_URL", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID), evar27ForIM);
                }

            }
            else
            {
                //LA specific Ejabberd chat
                IMLinkPath = string.Format("javascript:LaunchNewIMConversation('{0}')", Convert.ToString(receivingMember.MemberID));
            }

            return IMLinkPath;
        }

        public static string GetSubscriptionLinkPath(IMemberDTO receivingMember, int prtID, LinkParent linkParent)
        {
            string subLinkPath = "/Applications/InstantMessenger/upInstantCommunicator.aspx?DestinationMemberID=" + Convert.ToString(receivingMember.MemberID);
            //15941 add source of original click to instant messenger
            subLinkPath += "&LinkParent=" + (int)linkParent;
            //omniture prt id for favorites online 
            if (prtID != int.MinValue && prtID != Constants.NULL_INT)
            {
                subLinkPath += "&prtid=" + prtID;
            }

            return subLinkPath;
        }

        /// <summary>
        /// Inserts a link to the Instant Messenger application for users who are NOT subscribers
        /// (they will be redirected to the subscription page)
        /// </summary>
        /// <param name="receivingMember">The member that the user wants to initiate a conversation with</param>
        /// <param name="linkParent">The source of the original click to Instant Messenger</param>
        public void PopulateSubscriptionLinkPath(IMemberDTO receivingMember, LinkParent linkParent)
        {
            OnlineLink = GetSubscriptionLinkPath(receivingMember, this.prtId, linkParent);

        }


        // Sets IsOnline to true iff the member being viewed is online.  
        private void SetIsOnline(ResultContextType displayContext, IMemberDTO receivingMember)
        {
            // Is this for memberonline? Don't bother to do lookup
            if (displayContext == Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.MembersOnline)
            {
                IsOnline = true;
            }
            else
            {
                IsOnline = Matchnet.MembersOnline.ServiceAdapters.MembersOnlineSA.Instance.IsMemberOnline(g.Brand.Site.Community.CommunityID, receivingMember.MemberID);
            }
        }
    }
}
