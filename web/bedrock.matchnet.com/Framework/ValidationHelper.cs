﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework
{
    public static class ValidationHelper
    {
        public static bool isOverEighteen(DateTime birthdate)
        {
            int yearsLived = DateTime.Now.Year - birthdate.Year;
            if (yearsLived < 18)
            {
                return false;
            }
            else if (yearsLived == 18)
            {
                // if birth month is later in the year than now, then not 18 yet
                if (birthdate.Month > DateTime.Now.Month)
                {
                    return false;
                }
                else if (birthdate.Month == DateTime.Now.Month)
                {
                    // if birth day is later in the month than now, then not 18 yet
                    if (birthdate.Day > DateTime.Now.Day)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool isUnderNinetyNine(DateTime birthdate)
        {
            int yearsLived = DateTime.Now.Year - birthdate.Year;
            if (yearsLived > 99)
            {
                return false;
            }
            else if (yearsLived == 99)
            {
                // if birth month is earlier in the year than now, then already 99
                if (birthdate.Month < DateTime.Now.Month)
                {
                    return false;
                }
                else if (birthdate.Month == DateTime.Now.Month)
                {
                    // if birth day is earlier in the month than now, then already 99
                    if (birthdate.Day <= DateTime.Now.Day)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool IsValidBirthDate(ContextGlobal _g, DateTime birthdate, System.Web.UI.Control ResourceControl, out string ValidationMessage)
        {
            try
            {
                if (!isOverEighteen(birthdate))
                {
                    ValidationMessage = _g.GetResource("YOU_MUST_BE_AT_LEAST_18_YEARS_OLD_TO_REGISTER", ResourceControl);
                    return false;
                }
                else if (!isUnderNinetyNine(birthdate))
                {
                    ValidationMessage = _g.GetResource("YOU_MUST_BE_YOUNGER_THAN_99_TO_REGISTER", ResourceControl);
                    return false;
                }
                else
                {
                    ValidationMessage = "";
                    return true;
                }
            }
            catch (Exception ex) { }
            ValidationMessage = _g.GetResource("TEXT_INVALID_BIRTH_DATE", ResourceControl);
            return false;
        }

        public static bool IsValidAgeRange(ContextGlobal _g, string minAge, string maxAge, System.Web.UI.Control ResourceControl, out string ValidationMessage)
        {
            try
            {
                if (!string.IsNullOrEmpty(minAge) && !string.IsNullOrEmpty(maxAge))
                {
                    int minAgeInt = Conversion.CInt(minAge);
                    int maxAgeInt = Conversion.CInt(maxAge);
                    if ((minAgeInt >= 18 && minAgeInt < 99)
                        && (maxAgeInt >= 18 && maxAgeInt < 99)
                        && (maxAgeInt > minAgeInt))
                    {
                        ValidationMessage = "";
                        return true;
                    }
                }
            }
            catch (Exception ex) { }
            ValidationMessage = _g.GetResource("TEXT_INVALID_BIRTH_DATE", ResourceControl);
            return false;
        }
        
    }
}