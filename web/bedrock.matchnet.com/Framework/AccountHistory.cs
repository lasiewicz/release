﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Common.OrderHistoryService;
using Matchnet.Purchase.ValueObjects;
using Matchnet;
using System.Text;
using Spark.Common.AuthorizationService;
using Spark.Common.PaymentProfileService;
using Spark.Common.Adapter;
using Matchnet.Member.ServiceAdapters;
using Spark.Common.DiscountService;

namespace Matchnet.Web.Framework.UPS
{
    public class AccountHistory
    {
        # region Private Members

        private DateTime _insertDateInPST = DateTime.MinValue;
        private int _orderID = Constants.NULL_INT;
        private TransactionType _transactionType = TransactionType.None;
        private decimal _totalAmount = Constants.NULL_DECIMAL;
        private string _lastFourAccountNumber = Constants.NULL_STRING;
        private int _callingSystemID = Constants.NULL_INT;
        private PaymentType _paymentType = PaymentType.None;
        private OrderStatus _orderStatus = OrderStatus.None;
        private int _adminID = Constants.NULL_INT;
        private int _promoID = Constants.NULL_INT;
        private int _primaryPackageID = Constants.NULL_INT;
        private int _brandID = Constants.NULL_INT;
        private string _packageIDList = Constants.NULL_STRING;
        private int _duration = Constants.NULL_INT;
        private DurationType _durationType = DurationType.None;
        private bool _isOneOffPurchase = false;
        private bool _isFreeTrialSubscription = false;
        private List<Spark.Common.AccessService.PrivilegeType> _privilegeTypeList = new List<Spark.Common.AccessService.PrivilegeType>();
        private int _adjustEmailCount = 0;
        private AccountHistoryType _accountHistoryType = AccountHistoryType.None;
        private decimal _deferredAmount = Constants.NULL_DECIMAL;
        public string _discountCode = Constants.NULL_STRING;
         
         
        
        #endregion

        #region Constructors

        public AccountHistory()
        {

        }

        public AccountHistory(OrderInfo orderInfo, int brandID, AccountHistoryType accountHistoryType)
        {
            this._insertDateInPST = ConvertUTCToPST(orderInfo.InsertDate);
            this._orderID = orderInfo.OrderID;
            // The transaction type is the first transaction type found  
            this._transactionType = GetAllUPSTransactionTypes(orderInfo)[0];
            this._totalAmount = orderInfo.TotalAmount;
            this._lastFourAccountNumber = orderInfo.LastFourAccountNumber;
            this._callingSystemID = orderInfo.CallingSystemID;
            this._paymentType = (PaymentType) Enum.Parse(typeof(PaymentType), Enum.GetName(typeof(Spark.Common.OrderHistoryService.PaymentType), orderInfo.PaymentType));
            this._orderStatus = (OrderStatus) orderInfo.OrderStatusID;
            this._adminID = orderInfo.AdminUserID;
            this._promoID = orderInfo.PromoID;
            this._primaryPackageID = orderInfo.PrimaryPackageID;
            this._brandID = brandID;
            this._packageIDList = GetPackageIDList(orderInfo);
            this._duration = orderInfo.Duration;
            this._durationType = (DurationType)orderInfo.DurationTypeID;
            this._accountHistoryType = accountHistoryType;
			
            foreach (OrderDetailInfo orderDetailInfo in orderInfo.OrderDetail)
            {
                if (orderDetailInfo.PackageType == PackageType.OneOff)
                {
                    this._isOneOffPurchase = true;
                }
            }

        }

        public AccountHistory(Spark.Common.RenewalService.RenewalTransaction renewalTran, int brandID, AccountHistoryType accountHistoryType)
        {
            this._insertDateInPST = ConvertUTCToPST(renewalTran.InsertDateUTC);
            this._orderID = renewalTran.RenewalTransactionID;
            this._transactionType = (TransactionType)renewalTran.TransactionTypeID;
            this._totalAmount = 0.00m;
            this._lastFourAccountNumber = "";
            this._callingSystemID = renewalTran.CallingSystemID;
            this._paymentType = PaymentType.None;
            this._orderStatus = (OrderStatus)renewalTran.RenewalStatusID;
            this._adminID = renewalTran.AdminID;
            this._promoID = Constants.NULL_INT;
            this._primaryPackageID = Constants.NULL_INT;
            this._packageIDList = "";
            if (this._transactionType == TransactionType.AutoRenewalReopen || this.TransactionType == TransactionType.AutoRenewalTerminate)
            {
                //primary package will be the first one
                if (renewalTran.RenewalTransactionDetails != null && renewalTran.RenewalTransactionDetails.Length > 0)
                {
                    this._primaryPackageID = renewalTran.RenewalTransactionDetails[0].PackageID;

                    //a la carte packages
                    for (int i = 1; i < renewalTran.RenewalTransactionDetails.Length; i++)
                    {
                        if (String.IsNullOrEmpty(this.PackageIDList))
                            this._packageIDList += renewalTran.RenewalTransactionDetails[i].PackageID;
                        else
                            this._packageIDList += (";" + renewalTran.RenewalTransactionDetails[i].PackageID);
                    }
                }                
            }
            else if (this._transactionType == TransactionType.AutoRenewalReopenALaCarte || this.TransactionType == TransactionType.AutoRenewalTerminateALaCarte)
            {
                if (renewalTran.RenewalTransactionDetails != null && renewalTran.RenewalTransactionDetails.Length > 0)
                {
                    //a la carte packages
                    for (int i = 0; i < renewalTran.RenewalTransactionDetails.Length; i++)
                    {
                        if (String.IsNullOrEmpty(this.PackageIDList))
                            this._packageIDList += renewalTran.RenewalTransactionDetails[i].PackageID;
                        else
                            this._packageIDList += (";" + renewalTran.RenewalTransactionDetails[i].PackageID);
                    }
                }
            }
            else if (this._transactionType == TransactionType.FreezeSubscriptionAccount)
            {
                this._totalAmount = -Math.Abs(renewalTran.DeferredAmount);
                if (renewalTran.PrimaryPackageID > 0)
                {
                    this._primaryPackageID = renewalTran.PrimaryPackageID;
                }

                // Frozen time is saved in minutes  
                decimal days = renewalTran.Duration / 1440;
                this._duration = -Math.Abs(Convert.ToInt32(Math.Ceiling(days)));
                this._durationType = DurationType.Day;
            }
            else if (this._transactionType == TransactionType.UnfreezeSubscriptionAccount)
            {
                this._totalAmount = Math.Abs(renewalTran.DeferredAmount);
                if (renewalTran.PrimaryPackageID > 0)
                {
                    this._primaryPackageID = renewalTran.PrimaryPackageID;
                }

                // Frozen time is saved in minutes  
                decimal days = renewalTran.Duration / 1440;
                this._duration = Math.Abs(Convert.ToInt32(Math.Ceiling(days)));
                this._durationType = DurationType.Day;
            }

            if (this._transactionType != TransactionType.FreezeSubscriptionAccount
                && this._transactionType != TransactionType.UnfreezeSubscriptionAccount)
            {
                this._duration = 0;
                this._durationType = DurationType.Month;
            }
            this._brandID = brandID;
            //this._duration = 0;
            //this._durationType = DurationType.Month;
            this._accountHistoryType = accountHistoryType;
        }

        public AccountHistory(Spark.Common.AccessService.AccessTransaction accessTran, int brandID, AccountHistoryType accountHistoryType)
        {
            this._insertDateInPST = ConvertUTCToPST(accessTran.InsertDateUTC);
            this._orderID = accessTran.AccessTransactionID;
            this._transactionType = (TransactionType)accessTran.UnifiedTransactionTypeID;
            this._totalAmount = 0.00m;
            this._lastFourAccountNumber = "";
            this._callingSystemID = accessTran.CallingSystemID;
            this._paymentType = PaymentType.None;
            this._orderStatus = OrderStatus.Successful;
            this._adminID = accessTran.AdminUserID;
            this._promoID = Constants.NULL_INT;
            this._primaryPackageID = Constants.NULL_INT;
            this._duration = 0;
            this._durationType = DurationType.None;
            if (accessTran.AccessTransactionDetails != null)
            {
                foreach (Spark.Common.AccessService.AccessTransactionDetail atd in accessTran.AccessTransactionDetails)
                {
                    if (this._durationType == DurationType.None && atd.Duration != Constants.NULL_INT)
                    {
                        this._duration = atd.Duration;
                        this._durationType = (DurationType)atd.DurationTypeID;
                    }

                    this._privilegeTypeList.Add((Spark.Common.AccessService.PrivilegeType)atd.UnifiedPrivilegeTypeID);
                    if (((Spark.Common.AccessService.PrivilegeType)atd.UnifiedPrivilegeTypeID) == Spark.Common.AccessService.PrivilegeType.AllAccessEmails)
                        _adjustEmailCount = atd.Count;
                }
            }

            if (this._duration < -20000000)
            {
                this._duration = 0;
                this._durationType = DurationType.Month;
            }

            this._brandID = brandID;
            this._packageIDList = "";
            
            this._accountHistoryType = accountHistoryType;

            if (this._transactionType == TransactionType.PrivilegeAuthorization
                || this._transactionType == TransactionType.CancelFreeTrial)
            {
                this._isFreeTrialSubscription = true;

                // This is a free trial subscription so get the authorization subscription details
                try
                {
                    AuthorizationSubscription freeTrialSubscription = null;
                    freeTrialSubscription = AuthorizationServiceWebAdapter.GetProxyInstance().GetAuthorizationSubscriptionByAccessTransactionID(accessTran.AccessTransactionID);

                    if (freeTrialSubscription != null)
                    {
                        this._orderID = freeTrialSubscription.AuthorizationSubscriptionID;

                        ObsfucatedPaymentProfileResponse paymentProfileResponse = PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByUserPaymentID(freeTrialSubscription.PaymentProfileID, freeTrialSubscription.CallingSystemID);

                        if (paymentProfileResponse.Code == "0")
                        {
                            ObsfucatedCreditCardPaymentProfile creditCardPaymentProfile = paymentProfileResponse.PaymentProfile as ObsfucatedCreditCardPaymentProfile;
                            ObfuscatedCreditCardShortFormFullWebSitePaymentProfile creditCardShortFormFullWebSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormFullWebSitePaymentProfile;
                            ObfuscatedCreditCardShortFormMobileSitePaymentProfile creditCardShortFormMobileSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormMobileSitePaymentProfile;

                            if (creditCardPaymentProfile != null)
                            {
                                this._paymentType = PaymentType.CreditCard;
                                this._lastFourAccountNumber = creditCardPaymentProfile.LastFourDigitsCreditCardNumber;
                            }
                            else if (creditCardShortFormFullWebSitePaymentProfile != null)
                            {
                                this._paymentType = PaymentType.CreditCardShortFormFullWebSite;
                                this._lastFourAccountNumber = creditCardShortFormFullWebSitePaymentProfile.LastFourDigitsCreditCardNumber;

                            }
                            else if (creditCardShortFormMobileSitePaymentProfile != null)
                            {
                                this._paymentType = PaymentType.CreditCardShortFormMobileSite;
                                this._lastFourAccountNumber = creditCardShortFormMobileSitePaymentProfile.LastFourDigitsCreditCardNumber;

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("Error in getting the free trial subscription details, Error message: " + ex.Message);
                }
                finally
                {
                    PaymentProfileServiceWebAdapter.CloseProxyInstance();
                    AuthorizationServiceWebAdapter.CloseProxyInstance();
                }
            }
        }

        public AccountHistory(Spark.Common.AccessService.AccessTransaction accessTran, int brandID, Gift gift)
        {
            this._accountHistoryType = AccountHistoryType.GiftRedemption;
            this._insertDateInPST = accessTran.InsertDateUTC;
            this._orderID = accessTran.AccessTransactionID;
            this._transactionType = (TransactionType)accessTran.UnifiedTransactionTypeID;
            this._totalAmount = 0.00m;
            this._lastFourAccountNumber = gift.GiftRedemption.LastFourAccountNumber;
            this._callingSystemID = gift.GiftRedemption.CallingSystemID;
            this._paymentType = (Matchnet.Web.Framework.UPS.PaymentType)gift.GiftRedemption.PaymentType; 
            this._orderStatus = OrderStatus.Successful;
            this._adminID = Constants.NULL_INT;
            this._promoID = gift.GiftRedemption.PromoID;
            this._primaryPackageID = gift.GiftRedemption.PackageID;
            if (accessTran.AccessTransactionDetails != null)
            {
                foreach (Spark.Common.AccessService.AccessTransactionDetail atd in accessTran.AccessTransactionDetails)
                {
                    if (this._durationType == DurationType.None && atd.Duration != Constants.NULL_INT)
                    {
                        this._duration = atd.Duration;
                        this._durationType = (DurationType)atd.DurationTypeID;
                    }

                    this._privilegeTypeList.Add((Spark.Common.AccessService.PrivilegeType)atd.UnifiedPrivilegeTypeID);
                    if (((Spark.Common.AccessService.PrivilegeType)atd.UnifiedPrivilegeTypeID) == Spark.Common.AccessService.PrivilegeType.AllAccessEmails)
                        _adjustEmailCount = atd.Count;
                }
            }

            this._packageIDList = gift.GiftRedemption.PackageID.ToString();
        }

        public AccountHistory(MemberTran memberTran, int brandID, AccountHistoryType accountHistoryType)
        {
            this._insertDateInPST = memberTran.InsertDate;
            this._orderID = memberTran.MemberTranID;
            // The transaction type is the first transaction type found  
            this._transactionType = ConvertLegacyTransactionTypeToUPSTransactionType(memberTran.TranType, memberTran.Amount);
            this._totalAmount = memberTran.Amount;
            this._lastFourAccountNumber = GetLegacyLastFourAccountNumber(memberTran);
            this._callingSystemID = memberTran.SiteID;
            this._paymentType = ConvertLegacyPaymentTypeToUPSPaymentType(memberTran);
            this._orderStatus = ConvertLegacyTransactionStatusToUPSTransactionStatus(memberTran.MemberTranStatus);
            this._adminID = memberTran.AdminMemberID;
            this._promoID = memberTran.PromoID;
            this._primaryPackageID = memberTran.PlanID;
            this._brandID = brandID;
            this._packageIDList = memberTran.PlanIDList;
            this._duration = memberTran.Duration;
            this._durationType = ConvertLegacyDurationTypeToUPSDurationType(memberTran.DurationType);
            this._accountHistoryType = accountHistoryType;

        }

        
        #endregion

        #region Properties
        public string DiscountCode
        {
            get { return this._discountCode; }
            set { this._discountCode = value; }
        }

        public string DiscountAmountForDisplay
        {
            get; set;
        }

        public int DiscountType
        {
            get;set;
        }

        public bool HasDiscount
        {
            get;set;
        }

        public DateTime InsertDateInPST
        {
            get { return this._insertDateInPST; }
            set { this._insertDateInPST = value; }
        }

        public int OrderID
        {
            get { return this._orderID; }
            set { this._orderID = value; }
        }

        public TransactionType TransactionType
        {
            get { return this._transactionType; }
            set { this._transactionType = value; }
        }

        public string TransactionTypeResourceConstant
        {
            get
            {
                string resourceConstant = Constants.NULL_STRING;

                switch (this._transactionType)
                {
                    case TransactionType.InitialSubscriptionPurchase:
                        resourceConstant = "INITIAL_BUY";
                        break;
                    case TransactionType.Renewal:
                        resourceConstant = "RENEWAL";
                        break;
                    case TransactionType.Credit:
                        resourceConstant = "CREDIT";
                        break;
                    case TransactionType.Void:
                        resourceConstant = "VOID";
                        break;
                    case TransactionType.AutoRenewalTerminate:
                        resourceConstant = "TERMINATION";
                        break;
                    case TransactionType.AutoRenewalTerminateALaCarte:
                        resourceConstant = "TERMINATION_PARTIAL";
                        break;
                    case TransactionType.Adjustment:
                        resourceConstant = "ADMINISTRATIVE_ADJUSTMENT";
                        break;
                    case TransactionType.PrivilegeAuthorization:
                        resourceConstant = "AUTHORIZATION_ONLY";
                        break;
                    case TransactionType.PaymentProfileAuthorization:
                        resourceConstant = "PAYMENT_PROFILE_AUTHORIZATION";
                        break;
                    case TransactionType.TrialPayAdjustment:
                        resourceConstant = "TRIALPAY_ADJUSTMENT";
                        break;
                    case TransactionType.AutoRenewalReopen:
                        resourceConstant = "REOPEN_AUTO_RENEWAL";
                        break;
                    case TransactionType.AutoRenewalReopenALaCarte:
                        resourceConstant = "REOPEN_AUTO_RENEWAL_PARTIAL";
                        break;
                    case TransactionType.AdditionalSubscriptionPurchase:
                        resourceConstant = "ADDITIONAL_SUBSCRIPTION_PURCHASE";
                        break;
                    case TransactionType.AdditionalNonSubscriptionPurchase:
                        resourceConstant = "ADDITIONAL_NONSUBSCRIPTION_PURCHASE";
                        break;
                    case TransactionType.CancelFreeTrial:
                        resourceConstant = "CANCEL_FREE_TRIAL";
                        break;
					case TransactionType.TrialTakenInitialSubscription:
                        resourceConstant = "TRIAL_TAKEN_INITIAL_SUBSCRIPTION";			
                        break;
					case TransactionType.GiftRedeem:
                        resourceConstant = "GIFT_REDEMPTION";
						break;
                    case TransactionType.FreezeSubscriptionAccount:
                        resourceConstant = "SUBSCRIPTION_PAUSE";
                        break;
                    case TransactionType.UnfreezeSubscriptionAccount:
                        resourceConstant = "SUBSCRIPTION_RESUME";
                        break;
                    case TransactionType.ManualPayment:
                        resourceConstant = "MANUAL_PAYMENT";
                        break;
					case TransactionType.RenewalRecyclingTimeAdjustment:
                        resourceConstant = "RENEWAL_RECYCLING_TIME_ADJUSTMENT";
                        break;					
                    default:
                        resourceConstant = "MISSING";
                        break;
                }

                //return Enum.GetName(typeof(TransactionType), this._transactionType) + RESOURCE_CONSTANT;

                return resourceConstant;
            }
        }

        public decimal TotalAmount
        {
            get { return this._totalAmount; }
            set { this._totalAmount = value; }
        }

        public string LastFourAccountNumber
        {
            get { return this._lastFourAccountNumber; }
            set { this._lastFourAccountNumber = value; }
        }

        public int CallingSystemID
        {
            get { return this._callingSystemID; }
            set { this._callingSystemID = value; }
        }

        public int BrandID
        {
            get { return this._brandID; }
            set { this._brandID = value; }
        }

        public PaymentType PaymentType
        {
            get { return this._paymentType; }
            set { this._paymentType = value; }
        }

        public string PaymentTypeResourceConstant
        {
            get
            {
                string resourceConstant = Constants.NULL_STRING;

                switch (this._paymentType)
                {
                    case PaymentType.Check:
                        resourceConstant = "CHECK";
                        break;
                    case PaymentType.CreditCard:
                    case PaymentType.CreditCardShortFormMobileSite:
                    case PaymentType.CreditCardShortFormFullWebSite:
                        resourceConstant = "CREDIT_CARD";
                        break;
                    case PaymentType.SMS:
                        resourceConstant = "SMS";
                        break;
                    case PaymentType.PayPalLitle:
                        resourceConstant = "PAYPALLITLE";
                        break;
                    case PaymentType.PaypalDirect:
                        resourceConstant = "PAYPALDIRECT";
                        break;
                    case PaymentType.Manual:
                        resourceConstant = "MANUAL";
                        break;
                    case PaymentType.ElectronicFundsTransfer:
                        resourceConstant = "ELECTRONICFUNDSTRANSFER";
                        break;
                    case PaymentType.PaymentReceived:
                        resourceConstant = "PAYMENTRECEIVED";
                        break;
                    case PaymentType.DebitCard:
                        resourceConstant = "DEBIT_CARD";
                        break;
                    case PaymentType.InApplicationPurchase:
                        resourceConstant = "IAP_PAYMENT";
                        break;
                    default:
                        resourceConstant = "MISSING";
                        break;
                }

                return resourceConstant;
            }
        }

        public OrderStatus OrderStatus
        {
            get { return this._orderStatus; }
            set { this._orderStatus = value; }
        }

        public string OrderStatusResourceConstant
        {
            get
            {
                string resourceConstant = Constants.NULL_STRING;

                switch (this._orderStatus)
                {
                    case OrderStatus.Pending:
                        resourceConstant = "PENDING";
                        break;
                    case OrderStatus.Successful:
                        resourceConstant = "SUCCESS";
                        break;
                    case OrderStatus.Failed:
                        resourceConstant = "TRANSACTION_FAILURE";
                        break;
                    default:
                        resourceConstant = "MISSING";
                        break;
                }

                return resourceConstant;
            }
        }

        public int AdminID
        {
            get { return this._adminID; }
            set { this._adminID = value; }
        }

        public int PromoID
        {
            get { return this._promoID; }
            set { this._promoID = value; }
        }

        public int PrimaryPackageID
        {
            get { return this._primaryPackageID; }
            set { this._primaryPackageID = value; }
        }

        public string PackageResourceConstant
        {
            get
            {
                try
                {
                    string resourceConstant = Constants.NULL_STRING;

                    if (this.IsOneOffPurchase)
                    {
                        resourceConstant =  "ONEOFF_PACKAGE";
                    }
                    else
                    {
                        Matchnet.Purchase.ValueObjects.Plan plan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(this._primaryPackageID, this._brandID);
                        resourceConstant = plan.PlanResourceConstant;
                    }

                    return resourceConstant;
                }
                catch (Exception ex)
                {
                    return "MISSING_PACKAGEID_" + Convert.ToString(this._primaryPackageID);
                }
            }
        }

        public string PackageIDList
        {
            get { return this._packageIDList; }
            set { this._packageIDList = value; }
        }

        public int Duration
        {
            get { return this._duration; }
            set { this._duration = value; }
        }

        public DurationType DurationType
        {
            get { return this._durationType; }
            set { this._durationType = value; }
        }

        public bool IsOneOffPurchase
        {
            get { return this._isOneOffPurchase; }
            set { this._isOneOffPurchase = value; }
        }

        public bool IsFreeTrialSubscription
        {
            get { return this._isFreeTrialSubscription; }
            set { this._isFreeTrialSubscription = value; }
        }		

        public string DurationTypeResourceConstant
        {
            get
            {
                string resourceConstant = Constants.NULL_STRING;

                switch (this._durationType)
                {
                    case DurationType.Minutes:
                        resourceConstant = "MINUTE_S";
                        break;
                    case DurationType.Hour:
                        resourceConstant = "HOUR_S";
                        break;
                    case DurationType.Day:
                        resourceConstant = "DAY_S";
                        break;
                    case DurationType.Week:
                        resourceConstant = "WEEK_S";
                        break;
                    case DurationType.Month:
                        resourceConstant = "MONTH_S";
                        break;
                    case DurationType.Year:
                        resourceConstant = "YEAR_S";
                        break;
                    default:
                        resourceConstant = "MISSING";
                        break;
                }

                return resourceConstant;
            }
        }

        public AccountHistoryType AccountHistoryType
        {
          get { return this._accountHistoryType; }
          set { this._accountHistoryType = value; }
        }

        public List<Spark.Common.AccessService.PrivilegeType> PrivilegeTypeList
        {
            get { return this._privilegeTypeList; }
            set { this._privilegeTypeList = value; }
        }

        public int AdjustEmailCount
        {
            get { return this._adjustEmailCount; }
            set { this._adjustEmailCount = value; }
        }

        public decimal DeferredAmount
        {
            get { return this._deferredAmount; }
            set { this._deferredAmount = value; }
        }

        #endregion

        #region Methods
        private string GetPackageIDList(OrderHistory orderInfo)
        {
            List<int> arrPackageID = new List<int>();
            foreach (OrderDetail orderDetailInfo in orderInfo.OrderDetail)
            {
                if (!arrPackageID.Contains(orderDetailInfo.PackageID))
                {
                    arrPackageID.Add(orderDetailInfo.PackageID);
                }
            }
            StringBuilder sb = new StringBuilder();
            foreach (int packageID in arrPackageID)
            {
                if (sb.ToString().Length > 0)
                {
                    sb.Append(";" + Convert.ToString(packageID));
                }
                else
                {
                    sb.Append(Convert.ToString(packageID));
                }
            }

            return sb.ToString();
        }
        private List<TransactionType> GetAllUPSTransactionTypes(OrderInfo orderInfo)
        {
            List<TransactionType> arrOrderTransactionTypes = new List<TransactionType>();
            foreach (OrderDetailInfo orderDetailInfo in orderInfo.OrderDetail)
            {
                if (orderDetailInfo.OrderTypeID != Constants.NULL_INT)
                {
                    TransactionType orderDetailTransactionType = (TransactionType)orderDetailInfo.OrderTypeID;
                    if (!arrOrderTransactionTypes.Contains(orderDetailTransactionType))
                    {
                        arrOrderTransactionTypes.Add(orderDetailTransactionType);
                    }

                }
            }

            return arrOrderTransactionTypes;
        }

        private TransactionType ConvertLegacyTransactionTypeToUPSTransactionType(TranType legacyTranType, decimal amount)
        {
            TransactionType upsTransactionType = TransactionType.None;
            switch (legacyTranType)
            {
                case TranType.AdministrativeAdjustment:
                    if (Math.Abs(amount) > 0)
                    {
                        upsTransactionType = TransactionType.Credit;
                    }
                    else
                    {
                        upsTransactionType = TransactionType.Adjustment;
                    }
                    break;
                case TranType.AuthorizationNoTran:
                    upsTransactionType = TransactionType.PrivilegeAuthorization;
                    break;
                case TranType.AuthorizationOnly:
                    upsTransactionType = TransactionType.PrivilegeAuthorization;
                    break;
                case TranType.AuthPmtProfile:
                    upsTransactionType = TransactionType.PaymentProfileAuthorization;
                    break;
                case TranType.Discount:
                    upsTransactionType = TransactionType.None;
                    break;
                case TranType.InitialBuy:
                    upsTransactionType = TransactionType.InitialSubscriptionPurchase;
                    break;
                case TranType.ReceivedCheck:
                    upsTransactionType = TransactionType.None;
                    break;
                case TranType.Renewal:
                    upsTransactionType = TransactionType.Renewal;
                    break;
                case TranType.ReopenAutoRenewal:
                    upsTransactionType = TransactionType.AutoRenewalReopen;
                    break;
                case TranType.Termination:
                    upsTransactionType = TransactionType.AutoRenewalTerminate;
                    break;
                case TranType.TrialPayAdjustment:
                    upsTransactionType = TransactionType.TrialPayAdjustment;
                    break;
                case TranType.Void:
                    upsTransactionType = TransactionType.Void;
                    break;
                default:
                    break;
            }

            return upsTransactionType;
        }

        private string GetLegacyLastFourAccountNumber(MemberTran memberTran)
        {
            string legacyLastFourAccountNumber = Constants.NULL_STRING;

            if (memberTran.AccountNumber != null && memberTran.AccountNumber.Length > 0)
            {
                legacyLastFourAccountNumber = memberTran.AccountNumber;
            }
            else if (memberTran.LegacyLastFourAccountNumber != null && memberTran.LegacyLastFourAccountNumber.Length > 0)
            {
                legacyLastFourAccountNumber = memberTran.LegacyLastFourAccountNumber;
            }

            return legacyLastFourAccountNumber;
        }

        private PaymentType ConvertLegacyPaymentTypeToUPSPaymentType(MemberTran memberTran)
        {
            PaymentType upsPaymentType = PaymentType.None;

            switch (memberTran.PaymentType)
            {
                case Matchnet.Purchase.ValueObjects.PaymentType.None:
                    switch (memberTran.PaymentTypeResourceConstant)
                    {
                        case "CHECK":
                            upsPaymentType = PaymentType.Check;
                            break;
                        case "CREDIT_CARD":
                            upsPaymentType = PaymentType.CreditCard;
                            break;
                        case "SMS":
                            upsPaymentType = PaymentType.SMS;
                            break;
                        default:
                            upsPaymentType = PaymentType.None;
                            break;
                    }
                    break;
                case Matchnet.Purchase.ValueObjects.PaymentType.Check:
                    upsPaymentType = PaymentType.Check;
                    break;
                case Matchnet.Purchase.ValueObjects.PaymentType.CreditCard:
                    upsPaymentType = PaymentType.CreditCard;
                    break;
                case Matchnet.Purchase.ValueObjects.PaymentType.PaypalLitle:
                    upsPaymentType = PaymentType.PayPalLitle;
                    break;
                case Matchnet.Purchase.ValueObjects.PaymentType.Manual:
                    upsPaymentType = PaymentType.Manual;
                    break;
                case Matchnet.Purchase.ValueObjects.PaymentType.ElectronicFundsTransfer:
                    upsPaymentType = PaymentType.ElectronicFundsTransfer;
                    break;
                case Matchnet.Purchase.ValueObjects.PaymentType.PaymentReceived:
                    upsPaymentType = PaymentType.PaymentReceived;
                    break;
                default:
                    upsPaymentType = PaymentType.None;
                    break;
            }

            return upsPaymentType;
        }

        private OrderStatus ConvertLegacyTransactionStatusToUPSTransactionStatus(MemberTranStatus legacyTransactionStatus)
        {
            OrderStatus upsTransactionStatus = OrderStatus.None;

            switch (legacyTransactionStatus)
            {
                case MemberTranStatus.None:
                    upsTransactionStatus = OrderStatus.None;
                    break;
                case MemberTranStatus.Pending:
                    upsTransactionStatus = OrderStatus.Pending;
                    break;
                case MemberTranStatus.Success:
                    upsTransactionStatus = OrderStatus.Successful;
                    break;
                case MemberTranStatus.Failure:
                    upsTransactionStatus = OrderStatus.Failed;
                    break;
                default:
                    upsTransactionStatus = OrderStatus.None;
                    break;
            }

            return upsTransactionStatus;
        }

        private string GetPackageIDList(OrderInfo orderInfo)
        {
            List<int> arrPackageID = new List<int>();
            foreach (OrderDetailInfo orderDetailInfo in orderInfo.OrderDetail)
            {
                if (!arrPackageID.Contains(orderDetailInfo.PackageID))
                {
                    arrPackageID.Add(orderDetailInfo.PackageID);
                }
            }
            StringBuilder sb = new StringBuilder();
            foreach (int packageID in arrPackageID)
            {
                if (sb.ToString().IndexOf(";") > 0)
                {
                    sb.Append(";" + Convert.ToString(packageID));
                }
                else
                {
                    sb.Append(Convert.ToString(packageID)); 
                }
            }

            return sb.ToString();
        }

        public string GetPackageIDList(AuthorizationSubscription authorizationSubscription)
        {
            List<int> arrPackageID = new List<int>();
            arrPackageID.Add(authorizationSubscription.PrimaryPackageID);
            foreach (int packageID in authorizationSubscription.AllSecondaryPackageID)
            {
                if (!arrPackageID.Contains(packageID))
                {
                    arrPackageID.Add(packageID);
                }
            }
            StringBuilder sb = new StringBuilder();
            foreach (int packageID in arrPackageID)
            {
                if (sb.ToString().IndexOf(";") > 0)
                {
                    sb.Append(";" + Convert.ToString(packageID));
                }
                else
                {
                    sb.Append(Convert.ToString(packageID));
                }
            }

            return sb.ToString();
        }

        private DurationType ConvertLegacyDurationTypeToUPSDurationType(Matchnet.DurationType legacyDurationType)
        {
            DurationType upsDurationType = DurationType.None;

            switch (legacyDurationType)
            {
                case Matchnet.DurationType.Minute: 
                    upsDurationType = DurationType.Minutes;
                    break;
                case Matchnet.DurationType.Hour:
                    upsDurationType = DurationType.Hour;
                    break;
                case Matchnet.DurationType.Day:
                    upsDurationType = DurationType.Day;
                    break;
                case Matchnet.DurationType.Week:
                    upsDurationType = DurationType.Week;
                    break;
                case Matchnet.DurationType.Month:
                    upsDurationType = DurationType.Month;
                    break;
                case Matchnet.DurationType.Year:
                    upsDurationType = DurationType.Year;
                    break;
                default:
                    upsDurationType = DurationType.None;
                    break;
            }

            return upsDurationType;
        }

        public DateTime ConvertUTCToPST(DateTime transactionDate)
        {
            TimeZoneInfo timeZoneInfoInPST = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            DateTime transactionInsertDateInPST = TimeZoneInfo.ConvertTimeFromUtc(transactionDate, timeZoneInfoInPST);

            return transactionInsertDateInPST;
        }

        private void GetDiscountInfo(OrderHistory oh, out double _discountAmount, out string _discountCode)
        {
            _discountAmount = Constants.NULL_DOUBLE;
            _discountCode = Constants.NULL_STRING;
            foreach (OrderDetail od in oh.OrderDetail)
            {
                if (od.DiscountAmount > 0.0 && !String.IsNullOrEmpty(od.DiscountCode))
                {
                    _discountCode = od.DiscountCode;
                    _discountAmount = od.DiscountAmount;
                    this.HasDiscount = true;
                    break;
                }
            }
        }
        
         private PaymentType ConvertPaymentTypeToUPSPaymentType(int type)
        {
              PaymentType upsPaymentType = PaymentType.None;

              switch (type)
              {
                  case (int)Matchnet.Purchase.ValueObjects.PaymentType.Check:
                      upsPaymentType = PaymentType.Check;
                      break;
                  case (int)Matchnet.Purchase.ValueObjects.PaymentType.CreditCard:
                      upsPaymentType = PaymentType.CreditCard;
                      break;
                  case (int)Matchnet.Purchase.ValueObjects.PaymentType.PaypalLitle:
                      upsPaymentType = PaymentType.PayPalLitle;
                      break;
                  case (int)Matchnet.Purchase.ValueObjects.PaymentType.Manual:
                      upsPaymentType = PaymentType.Manual;
                      break;
                  case (int)Matchnet.Purchase.ValueObjects.PaymentType.ElectronicFundsTransfer:
                      upsPaymentType = PaymentType.ElectronicFundsTransfer;
                      break;
                  case (int)Matchnet.Purchase.ValueObjects.PaymentType.PaymentReceived:
                      upsPaymentType = PaymentType.PaymentReceived;
                      break;
                  default:
                      upsPaymentType = PaymentType.None;
                      break;
              }

              return upsPaymentType;
        }
        
        #endregion


    }

    public enum AccountHistoryType
    {
        None = 0,
        Financial = 1,
        NonFinancial = 2, 
        GiftRedemption = 3
    }

    /// <summary>
    /// Order transaction types
    /// </summary>
    public enum TransactionType
    {
        None = 0,
        InitialSubscriptionPurchase = 1,
        Renewal = 2,
        Credit = 3,
        Void = 4,
        AutoRenewalTerminate = 5,
        PrivilegeAuthorization = 6,
        PaymentProfileAuthorization = 10,
        PlanChange = 11,
        TrialPayAdjustment = 12,
        AutoRenewalReopen = 18,
        AdditionalSubscriptionPurchase = 23, //upgrade before
        AdditionalNonSubscriptionPurchase = 24, //downgrade before
        VirtualTerminalPurchase = 34,
        VirtualTerminalCredit = 35,
        VirtualTerminalVoid = 36,
        BatchRenewal = 37,
        LifetimeMemberAdjustment = 38,
        AutoRenewalTerminateALaCarte = 39,
        AutoRenewalReopenALaCarte = 40,
        BatchCaptureAuthorization = 41,
        ReverseAuthorization = 42,
        CancelFreeTrial = 43,
        ReverseAuthorizationAndCancelFreeTrial = 44,
        AutoDelayedCaptureTerminate = 45,
        FreezeSubscriptionAccount = 46,
        UnfreezeSubscriptionAccount = 47,
        ManualPayment = 48,		
        TrialTakenInitialSubscription = 1001,
        ChargeBack = 1004,
        CheckReversal = 1005,
        Adjustment = 1007,
        GiftPurchase = 1013,
        GiftRedeem = 1014,
        RenewalRecyclingTimeAdjustment = 1015
    }

    /// <summary>
    /// The status of the order    
    /// </summary>
    public enum OrderStatus : int
    {
        None = 0,
        Pending = 1,
        Successful = 2,
        Failed = 3
    }

    /// <summary>
    /// The payment type of the payment profile
    /// </summary>
    public enum PaymentType : int
    {
        None = 0,
        CreditCard = 1,
        Check = 2,
        DirectDebit = 4,
        PayPalLitle = 8,
        SMS = 16,
        PaypalDirect = 32,
        Manual = 64,
        ElectronicFundsTransfer = 128,
        PaymentReceived = 256,
        DebitCard = 512,
        InApplicationPurchase = 1024,
        CreditCardShortFormMobileSite = 2048,
        CreditCardShortFormFullWebSite = 4096
    }

    public enum DurationType : int
    {
        None = 0,
        Minutes = 1,
        Hour = 2,
        Day = 3,
        Week = 4,
        Month = 5,
        Year = 6
    }

}
