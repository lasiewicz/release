using System;
using System.Web;
using Matchnet.Web.Framework.Diagnostics;

namespace Matchnet.Web.Framework
{
    public class Dispatch : IHttpModule
    {
        public Dispatch()
        {
        }

        public void Init(HttpApplication application)
        {
            application.BeginRequest += (new EventHandler(this.Application_BeginRequest));
            application.EndRequest += new EventHandler(application_EndRequest);
            application.Error += new EventHandler(this.Application_OnError);
        }

        private void Application_OnError(Object source, EventArgs e)
        {

            Exception ex = HttpContext.Current.Server.GetLastError();
            if (ex != null)
            {
                if (ex is HttpException)
                {
                    if (((HttpException)ex).GetHttpCode() == 404)
                    {
                        return;
                    }
                }

                ContextExceptions _ContextExceptions = new ContextExceptions();
                _ContextExceptions.LogException(ex, true);
            }

            HttpContext Context = HttpContext.Current;

            FrameworkGlobals.RedirectToErrorPage(true);

            Context.ApplicationInstance.CompleteRequest();
        }

        private void Application_BeginRequest(Object source, EventArgs e)
        {
            // Set up local variables
            HttpContext Context = HttpContext.Current;
            string requestPath = Context.Request.Path.ToLower();
            string fullURL = HttpUtility.UrlDecode(Context.Request.Url.AbsoluteUri);


            string checkForFriendlyUrl = string.Empty;


            checkForFriendlyUrl = FrameworkGlobals.CheckForFriendlyURL();

            bool isFriendlyUrl = false;

            if (!string.IsNullOrEmpty(checkForFriendlyUrl) && (
                checkForFriendlyUrl.Contains("religious-dating") || checkForFriendlyUrl.Contains("academic-dating") ||
                checkForFriendlyUrl.Contains("religious-dating-profiles") || checkForFriendlyUrl.Contains("academic-dating-profiles") ||
                checkForFriendlyUrl.Contains("Femmes-juives") || checkForFriendlyUrl.Contains("Les-hommes-juifs") ||
                checkForFriendlyUrl.Contains("student-dating-signup") || checkForFriendlyUrl.Contains("religious-dating-signup") ||
                checkForFriendlyUrl.Contains("Divorced-dating") || checkForFriendlyUrl.Contains("Senior-dating") ||
                checkForFriendlyUrl.Contains("dating-for-divorced-signup") || checkForFriendlyUrl.Contains("silver-dating-signup")))
            {
                isFriendlyUrl = true;
            }

            //check to see if it was a mobile landing page

            if(!string.IsNullOrEmpty(requestPath))
            {
                if (requestPath.ToLower().StartsWith("/mobile") && !requestPath.EndsWith(".aspx"))
                {
                    requestPath = "/mobile";
                    isFriendlyUrl = true;
                }
                
            }


            // Return if this is a special page
            if (!isFriendlyUrl)
            {
                if (requestPath.StartsWith("/applications/api") ||
                    requestPath.StartsWith("/fbchannel") ||
               !requestPath.EndsWith(".aspx") ||
               requestPath.EndsWith("404form.aspx") ||
               requestPath.EndsWith("cachemanager.aspx") ||
               requestPath.EndsWith("tag-744.aspx") ||
               requestPath.EndsWith("nophoto.aspx") ||
               requestPath.EndsWith("mmimagedisplay.aspx") ||
               requestPath.EndsWith("searchstoreview.aspx") ||
                   requestPath.EndsWith("get_aspx_ver.aspx"))  // This is a special page used by dev studio
                {
                    return;
                }
            }



            // Add pagepath/fullurl to context
            Context.Items["PagePath"] = requestPath;
            Context.Items["FullURL"] = fullURL;

            // Rewrite to Default
            Context.RewritePath("/Default.aspx");
        }

        public void Dispose()
        {
        }

        private void application_EndRequest(object sender, EventArgs e)
        {
            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];
            if (g != null)
            {
                g.Context_EndRequest();
            }
        }
    }
}
