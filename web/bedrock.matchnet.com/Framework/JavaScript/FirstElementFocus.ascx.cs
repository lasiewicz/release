namespace Matchnet.Web.Framework.JavaScript
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for FirstElementFocus.
	/// </summary>
	public class FirstElementFocus : System.Web.UI.UserControl
	{
		private string elementClientId;
		protected Literal elementId;

		public string ElementClientId
		{
			set
			{
				this.elementClientId = value;
			}
			get
			{
				return this.elementClientId;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			this.elementId.Text = this.ElementClientId;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
