﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ValueObjects;
using Matchnet.Lib;

namespace Matchnet.Web.Framework
{
    public static class PromotionHelper
    {
        public const string BeautyBeastPromotionOmniture = "BB displayed";

        public static bool IsBeautyAndBeastPromotionEnabled(ContextGlobal g)
        {
            //DB Setting
            string isEnabled = "false";
            bool returnValue = false;
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_BEAUTY_BEAST_SLIDESHOW_PROMOTION", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                returnValue = true;
            else
                returnValue = false;

            //Additional criteria checks
            if (returnValue)
            {
                returnValue = false;
                if (g.Member != null)
                {
                    //member needs to be female
                    int genderMask = g.Member.GetAttributeInt(g.Brand, "gendermask");
                    if ((genderMask & ConstantsTemp.GENDERID_FEMALE) == ConstantsTemp.GENDERID_FEMALE)
                    {
                        returnValue = true;
                    }
                }
            }

            return returnValue;
        }

    }
}