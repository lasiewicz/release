using System;

namespace Matchnet.Web.Framework
{
	/// <summary>
	/// Summary description for RegionLabel.
	/// </summary>
	public class RegionLabel : Txt
	{
		private int _regionMask;
		private int _regionID = Constants.NULL_INT;

		public int RegionMask
		{
			get { return _regionMask; }
			set
			{
				_regionMask = value;
				SetResourceConstant();
			}
		}

		public int RegionID
		{
			get { return _regionID; }
			set
			{
				_regionID = value;
			}
		}
		
		private void SetResourceConstant()
		{
            string resourceConstant = FrameworkGlobals.GetRegionResourceConstant(_regionMask);
            if(string.IsNullOrEmpty(resourceConstant)) {
                throw new ArgumentException("Unknown Region mask: " + _regionMask + ", RegionID: " + Convert.ToString(_regionID), "RegionMask");
            } else {
                base.DefaultResourceConstant=resourceConstant;
            }
		}
	}
}
