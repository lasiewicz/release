using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Applications.Subscription;
using Matchnet.Web.Framework.Ui.Advertising;

namespace Matchnet.Web.Framework.Banner
{
	/// <summary>
	///		Summary description for BannerControl.
	/// </summary>
	public class BannerControl : FrameworkControl
	{
		protected HyperLink lnkBanner;
		protected PlaceHolder plcBanner;
		protected PlaceHolder plcAdUnit;
        protected AdUnit AdUnitSmallLeaderBoard;
        const string RESOURCE_SUBSCRIBER="BANNER_2";
		const string RESOURCE_REGISTRANT="BANNER_1";

		private void Page_Load(object sender, System.EventArgs e)
		{
			string resourceConstant = String.Empty;
            if (g.EmailVerify.IsMemberBlocked(g.Member))
                return;
		
			if(SubscriptionHelper.IsPaymentExpiring(g))
			{
				lnkBanner.Text = _g.GetResource("PAYMENT_PROFILE_EXPIRATION_BANNER", this);
				lnkBanner.NavigateUrl = FrameworkGlobals.LinkHrefSSL("/applications/BillingInformation/PaymentProfile.aspx");
			}
			// if the member is logged in and is not a subscriber and we're not on logon/logoff pages
			else if (_g.Member != null && !MemberPrivilegeAttr.IsCureentSubscribedMember(g))
			{
				if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMailPermissions", g.Brand.Site.Community.CommunityID,
					g.Brand.Site.SiteID, g.Brand.BrandID)) && g.AppPage.ControlName.ToLower() == "mailbox")
				{
					#region Exception in case of IMailPermissionsSetting is turned on MailBox.aspx

					lnkBanner.Text = _g.GetResource("IMAIL_PERMISSIONS_MAILBOX_BANNER", this);

					#endregion
				}
				else
				{
					plcBanner.Visible=false;

                    if (FrameworkGlobals.DisplaySmallSubscribeBanner(g))
                    {
                        if (!_g.AppPage.ControlName.ToLower().Equals("attributeedit")  )
                        {
                            plcAdUnit.Visible = true;
                            AdUnitSmallLeaderBoard.Disabled = false;
                        }
                        if (_g.AppPage.ControlName.ToLower().Equals("bachelor"))
                        {
                            plcAdUnit.Visible = false;
                        }
                    }
				}
			}			
		}
	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
