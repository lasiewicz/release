﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Web.Framework.Targeting
{
    public abstract class MemberTargetingCriteria
    {
        protected Brand _brand;

        public abstract bool PassesCriteria();

        public MemberTargetingCriteria(Brand brand)
        {
            _brand = brand;
        }
    }
}