﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Targeting
{
    public class BooleanSettingCriteria: MemberTargetingCriteria
    {
        private string _settingConstant = string.Empty;
        private SettingsManager _settingsManager = null;

        public BooleanSettingCriteria(Brand brand, string settingConstant, SettingsManager settingsManager)
            : base(brand)
        {
            _settingConstant = settingConstant;
            _settingsManager = settingsManager;
        }

        public override string ToString()
        {
            return "BooleanSettingCriteria: " + _settingConstant + " = true";
        }

        public override bool PassesCriteria()
        {
            return _settingsManager.GetSettingBool(_settingConstant, _brand);
        }
    }
}