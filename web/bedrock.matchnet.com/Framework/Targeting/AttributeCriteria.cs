﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;

namespace Matchnet.Web.Framework.Targeting
{
    public abstract class AttributeCriteria : MemberTargetingCriteria
    {
        protected IMember _member;
        protected string _attributeName;

        public AttributeCriteria(IMember member, Brand brand, string attributeName)
            : base(brand)
        {
            _member = member;
            _attributeName = attributeName;
        }
    }
}