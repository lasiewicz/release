﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Web.Framework.Targeting
{
    public class DateAttributeEqualityCriteria : AttributeCriteria
    {
        private DateTime _comparativeValue = DateTime.MinValue;


        public DateAttributeEqualityCriteria(IMember member, Brand brand, string attributeName, DateTime comparativeValue)
            : base(member, brand, attributeName)
        {
            _comparativeValue = comparativeValue;
        }

        public override string ToString()
        {
            return "DateAttributeEqualityCriteria: " + _attributeName + " = " +  _comparativeValue.ToShortDateString();
        }

        public override bool PassesCriteria()
        {
            DateTime attributeValue = _member.GetAttributeDate(_brand, _attributeName, DateTime.MinValue);
            if (attributeValue == DateTime.MinValue) return false;

            if (attributeValue.Date == _comparativeValue.Date)
                return true;
            else
                return false;
        }
    }
}