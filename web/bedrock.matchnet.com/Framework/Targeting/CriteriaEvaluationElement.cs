﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Targeting
{
    public class CriteriaEvaluationElement : CriteriaEvaluationUnit
    {
        private MemberTargetingCriteria _criteria;
        
        public CriteriaEvaluationElement(MemberTargetingCriteria criteria)
        {
            _criteria = criteria;
        }
        
        public override void AddEvaluationUnit(CriteriaEvaluationUnit unit, BooleanEvaluationOperator evaluationOperator)
        {
            
        }

        public override string ToString()
        {
            if(_criteria != null)
            {
                return _criteria.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public override bool PassesEvaluationCriteria()
        {
            return _criteria.PassesCriteria();
        }
    }
}