﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Web.Framework.Targeting
{
    public class MemberIDCriteria: MemberTargetingCriteria
    {
        protected IMember _member;
        private MemberIDCriteriaOperator _criteriaOperator;

        public MemberIDCriteria(IMember member, Brand brand, MemberIDCriteriaOperator criteriaOperator)
            : base(brand)
        {
            _member = member;
            _criteriaOperator = criteriaOperator;
        }

        public override bool PassesCriteria()
        {
            bool passes = false;

            if (_member.MemberID % 2 == 0 && _criteriaOperator == MemberIDCriteriaOperator.Even) passes = true;
            if (_member.MemberID % 2 == 1 && _criteriaOperator == MemberIDCriteriaOperator.Odd) passes = true;
            
            return passes;
        }

        public override string ToString()
        {
            return "MemberIDCriteria: " + _criteriaOperator.ToString();
        }
    }
}