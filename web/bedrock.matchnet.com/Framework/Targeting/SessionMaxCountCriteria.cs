﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Targeting
{
    public class SessionMaxCountCriteria: MemberTargetingCriteria
    {
        private string _sessionVariableName;
        private int _count;
        public SessionManager _sessionManager;

        public SessionMaxCountCriteria(Brand brand, SessionManager sessionManager, string sessionVariableName, int count ): base(brand)
        {
            _sessionVariableName = sessionVariableName;
            _count = count;
            _sessionManager = sessionManager;
        }

        public override bool PassesCriteria()
        {
            bool passes = false;

            UserSession session = _sessionManager.GetCurrentSession(_brand);
            string sessiontimes = session.GetString(_sessionVariableName);

            if (!String.IsNullOrEmpty(sessiontimes))
            {
                int timesalreadyshown = Conversion.CInt(sessiontimes, 0);
                if (timesalreadyshown < _count) passes = true;
            }
            else
            {
                passes = true;
            }

            return passes;
        }

        public override string ToString()
        {
            return "SessionMaxCountCriteria: " + _sessionVariableName + " maximum of " + _count.ToString() + " times";
        }
    }
}