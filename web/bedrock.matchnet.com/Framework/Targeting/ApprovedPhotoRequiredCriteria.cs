﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Photos;

namespace Matchnet.Web.Framework.Targeting
{
    public class ApprovedPhotoRequiredCriteria: MemberTargetingCriteria
    {
        protected IMember _member;
        public ApprovedPhotoRequiredCriteria(IMember member, Brand brand): base(brand)
        {
            _member = member;
        }

        public override bool PassesCriteria()
        {
            bool passes = false;
            
            List<Photo> approvedPhotos = _member.GetApprovedPhotos(_brand.Site.Community.CommunityID, false);
            if(approvedPhotos != null && approvedPhotos.Count > 0)
            {
                passes = true;
            }

            return passes;
        }

        public override string ToString()
        {
            return "ApprovedPhotoRequiredCriteria: Must have photo";
        }

    }
}