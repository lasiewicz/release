﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Web.Framework.Targeting
{
    public class IntAttributeListCriteria : AttributeCriteria
    {
        private List<int> _comparativeValueList = null;
        private IntAttributeCriteriaOperator _criteriaOperator;

        public IntAttributeListCriteria(IMember member, Brand brand, string attributeName, IntAttributeCriteriaOperator criteriaOperator, List<int> comparativeValueList)
            : base(member, brand, attributeName)
        {
            if (comparativeValueList == null || comparativeValueList.Count == 0)
            {
                throw new ApplicationException("The comparative value list must not be null and must contain at least one value");
            }
            
            _comparativeValueList = comparativeValueList;
            _criteriaOperator = criteriaOperator;
        }

        public override bool PassesCriteria()
        {
            bool passes = false;

            int attributeValue = _member.GetAttributeInt(_brand, _attributeName, Constants.NULL_INT);
            if (attributeValue == Constants.NULL_INT) return false;

            foreach (int value in _comparativeValueList)
            {
                switch (_criteriaOperator)
                {
                    case IntAttributeCriteriaOperator.Equals:
                        if (attributeValue.Equals(value)) passes = true;
                        break;
                    case IntAttributeCriteriaOperator.ContainsMaskValue:
                        if ((attributeValue & value) == value) passes = true;
                        break;
                }
                if (passes) break;
            }

            return passes;
        }

        public override string ToString()
        {
            return "IntAttributeCriteria: " + _attributeName + "  " + _criteriaOperator.ToString() + " " + _comparativeValueList.ToString();
        }
    }
}