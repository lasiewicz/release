﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Spark.Common.Adapter;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Targeting
{
    public class SubscriptionLapseDateCriteria : MemberTargetingCriteria
    {
        protected IMember _member;
        private DateAttributeRangeCriteriaOperator _criteriaOperator;
        private int _factor = 0;

        public SubscriptionLapseDateCriteria(IMember member, Brand brand, DateAttributeRangeCriteriaOperator criteriaOperator, int factor)
            : base(brand)
        {
            _member = member;
            _criteriaOperator = criteriaOperator;
            _factor = factor;
        }

        public override bool PassesCriteria()
        {
            bool passes = false;
            DateTime subscriptionLapseDate = DateTime.MinValue;

            Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(_member.MemberID, _brand);
            if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0)
            {
                Spark.Common.RenewalService.RenewalTransaction terminateTransaction = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetRenewalTransactionMostRecent(_member.MemberID, _brand.BrandID, 5);
                if (terminateTransaction != null && terminateTransaction.RenewalTransactionID > 0)
                {
                    subscriptionLapseDate = FrameworkGlobals.ConvertUTCToPST(terminateTransaction.InsertDateUTC);
                }
            }

            if (subscriptionLapseDate != DateTime.MinValue)
            {
                switch (_criteriaOperator)
                {
                    case DateAttributeRangeCriteriaOperator.WithinNumberOfDaysInFuture:
                        if (subscriptionLapseDate >= DateTime.Now.Date && subscriptionLapseDate < DateTime.Now.Date.AddDays(_factor)) passes = true;
                        break;
                    case DateAttributeRangeCriteriaOperator.WithinNumberOfDaysInFutureInclusive:
                        if (subscriptionLapseDate >= DateTime.Now.Date && subscriptionLapseDate <= DateTime.Now.Date.AddDays(_factor)) passes = true;
                        break;
                    case DateAttributeRangeCriteriaOperator.WithinNumberOfDaysInPast:
                        if (subscriptionLapseDate <= DateTime.Now.Date && subscriptionLapseDate > DateTime.Now.Date.AddDays(-_factor)) passes = true;
                        break;
                    case DateAttributeRangeCriteriaOperator.WithinNumberOfDaysInPastInclusive:
                        if (subscriptionLapseDate <= DateTime.Now.Date && subscriptionLapseDate >= DateTime.Now.Date.AddDays(-_factor)) passes = true;
                        break;
                    case DateAttributeRangeCriteriaOperator.GreaterThanNumberOfDaysInFuture:
                        if (subscriptionLapseDate > DateTime.Now.Date && subscriptionLapseDate > DateTime.Now.Date.AddDays(_factor)) passes = true;
                        break;
                    case DateAttributeRangeCriteriaOperator.GreaterThanNumberOfDaysInFutureInclusive:
                        if (subscriptionLapseDate > DateTime.Now.Date && subscriptionLapseDate >= DateTime.Now.Date.AddDays(_factor)) passes = true;
                        break;
                    case DateAttributeRangeCriteriaOperator.GreaterThanNumberOfDaysInPast:
                        if (subscriptionLapseDate < DateTime.Now.Date && subscriptionLapseDate < DateTime.Now.Date.AddDays(_factor)) passes = true;
                        break;
                    case DateAttributeRangeCriteriaOperator.GreaterThanNumberOfDaysInPastInclusive:
                        if (subscriptionLapseDate < DateTime.Now.Date && subscriptionLapseDate <= DateTime.Now.Date.AddDays(_factor)) passes = true;
                        break;
                }
            }


            return passes;
        }

        public override string ToString()
        {
            return "SubscriptionLapseDateCriteria: " + _factor.ToString() + " " + _criteriaOperator.ToString();
        }

    }
}