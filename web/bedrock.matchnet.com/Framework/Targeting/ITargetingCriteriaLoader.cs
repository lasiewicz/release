﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Web.Framework.Targeting
{
    public interface ITargetingCriteriaLoader
    {
        //List<MemberTargetingCriteria> GetCriteria(string feature, IMember member, Brand brand);
        CriteriaEvaluationUnit GetCriteria(string feature, IMember member, Brand brand);
    }
}