﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Member.ValueObjects;

namespace Matchnet.Web.Framework
{
    //[Flags]
    //public enum PhotoStatusMask
    //{
    //    // this indicates member purchased subscription after AS was converted to spark.com
    //    // in which case at least 1 approved photo is required.
    //    PhotoRequired = 1 
    //}

    public class MemberPhotoReqHelper
    {
        private const string REBRAND_FIRST_LOGON_DATE = "RebrandFirstLogonDate";
        private const string PHOTO_STATUS_MASK = "PhotoStatusMask";
        
        Brand _brand;
        bool _photoRequirementChecked = false;
        bool _isPhotoRequiredSite = false;
        int _photoRequireGracePeriod = Constants.NULL_INT;

        #region Properties
        public bool IsPhotoRequiredSite
        {
            get
            {
                if (_photoRequirementChecked)
                    return _isPhotoRequiredSite;

                _isPhotoRequiredSite = Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", _brand.Site.Community.CommunityID, _brand.Site.SiteID));
                _photoRequirementChecked = true;

                return _isPhotoRequiredSite;                 
            }
        }

        public int PhotoRequireGracePeriodInHours
        {
            get
            {
                if (_photoRequireGracePeriod != Constants.NULL_INT)
                    return _photoRequireGracePeriod;

                _photoRequireGracePeriod = Conversion.CInt(RuntimeSettings.GetSetting("PHOTO_REQUIRE_GRACE_PERIOD", _brand.Site.Community.CommunityID, _brand.Site.SiteID));

                return _photoRequireGracePeriod;
            }
        }
        #endregion

        public MemberPhotoReqHelper(Brand brand)
        {
            _brand = brand;
        }

        public void UpdateRebrandLoginDate(Member.ServiceAdapters.Member member)
        {
            if (!IsPhotoRequiredSite)
                return;

            // RebrandLastLogonDate attribute needs to be inserted/updates only once because it will be used to
            // determine the grace period window. this is somewhat redundant because this attribute is immutable.
            DateTime rebrandLogonDate = member.GetAttributeDate(_brand, REBRAND_FIRST_LOGON_DATE, DateTime.MinValue);
            if (rebrandLogonDate == DateTime.MinValue)
            {
                member.SetAttributeDate(_brand, REBRAND_FIRST_LOGON_DATE, DateTime.Now);
                MemberSA.Instance.SaveMember(member);
            }
        }

        public void ApplyPhotoRequirementToMember(Member.ServiceAdapters.Member member)
        {
            if (!IsPhotoRequiredSite)
                return;

            int photoStatusMask = member.GetAttributeInt(_brand, PHOTO_STATUS_MASK, 0);
            photoStatusMask = photoStatusMask | (int)PhotoStatusMask.PhotoRequired;

            member.SetAttributeInt(_brand, PHOTO_STATUS_MASK, photoStatusMask);
            MemberSA.Instance.SaveMember(member);
        }

        public bool IsMemberBlocked(Member.ServiceAdapters.Member member, UserSession usrSession)
        {

            if (!IsPhotoRequiredSite)
            {
                // if this site does not require a photo, we don't block any member so return false
                return false;
            }

            // if this is not set yet, we cannot do anything so just return false. this should be called after this is set.
            DateTime rebrandLogonDate = member.GetAttributeDate(_brand, REBRAND_FIRST_LOGON_DATE, DateTime.MinValue);
            if(rebrandLogonDate == DateTime.MinValue)
                return false;

            // check for temporary exemption in Session
            if (usrSession.Get(WebConstants.SESSION_PHOTO_REQUIRE_TEMP_EXEMPT) != null)
                return false;

            bool ret = false;

            int photoStatusMask = member.GetAttributeInt(_brand, PHOTO_STATUS_MASK, 0);

            if ((photoStatusMask & (int)PhotoStatusMask.PhotoRequired) == (int)PhotoStatusMask.PhotoRequired)
            { 
                // this means this member either registered or subscribed after AS was converted to spark.com so
                // we require a photo from them.
                if (DateTime.Now > rebrandLogonDate.AddHours(PhotoRequireGracePeriodInHours))
                    ret = true;
            }
            else
            {
                // this means this member was a member before AS was converted to spark.com. only require a photo from
                // a non-sub member.
                if (!member.IsPayingMember(_brand.Site.SiteID))
                {
                    if (DateTime.Now > rebrandLogonDate.AddHours(PhotoRequireGracePeriodInHours))
                        ret = true;
                }
            }

            if (ret)
            {
                ret = !member.HasApprovedPhoto(_brand.Site.Community.CommunityID);
            }

            return ret;
        }
    }
}
