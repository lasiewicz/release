﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Framework.Menus
{
    [Serializable]
    public class MenuItem
    {
        string _desc;
        string _titleresource;
        string _resource;
        string _link;
        string _showconditionproc;
        string _linkproc;
        string _renderproc;
        string _liclass;
        string _mBoxName;

        List<SelectCondition> _selectconditions;
        List<MenuItem> _items;
        string _configcondition;


        public string Desc
        { get { return _desc; } set { _desc = value; } }

        public string TitleResourceConstant
        { get { return _titleresource; } set { _titleresource = value; } }


        public string ResourceConstant
        { get { return _resource; } set { _resource = value; } }

        public string Link
        { get { return _link; } set { _link = value; } }

        public string LinkFunction
        { get { return _linkproc; } set { _linkproc = value; } }

        public string ConfigurationCondition
        { get { return _configcondition; } set { _configcondition = value; } }

        public string ShowConditionFunction
        { get { return _showconditionproc; } set { _showconditionproc = value; } }

        public string RenderFunction
        { get; set; }

        public string Target
        { get; set; }

        public string LIClass
        {
            get { return _liclass; }
            set { _liclass = value; }
        }

        public string MBoxName
        {
            get { return _mBoxName; }
            set { _mBoxName = value; }
        }

        public List<MenuItem> MenuItems
        { get { return _items; } set { _items = value; } }

        public List<SelectCondition> SelectConditions
        { get { return _selectconditions; } set { _selectconditions = value; } }

        public bool HtmlEncodeLinks
        { get; set; }

        public string GAMAdSlotName { get; set; } 

        public string GetExpandedLink(ContextGlobal g)
        {
            try
            {
                string link = _link;


                return Framework.Globalization.Localizer.ExpandTokens(link, g.ExpansionTokens);
            }
            catch (Exception ex)
            { return _link; }
        }

    }

    [Serializable]
    public class SelectCondition
    {
        string _controlname;
        string _app;
        string _function;
        string _requestparam;
        string _requestparamvalue;

        public string ControlName
        {
            get { return _controlname; }
            set { _controlname = value; }
        }

        public string App
        {
            get { return _app; }
            set { _app = value; }
        }

        public string SelectFunction
        {
            get { return _function; }
            set { _function = value; }
        }

        public string RequestParameter
        {
            get { return _requestparam; }
            set { _requestparam = value; }
        }

        public string RequestParameterValue
        {
            get { return _requestparamvalue; }
            set { _requestparamvalue = value; }
        }


    }
    [Serializable]
    public class Menu
    {
        public List<MenuItem> MenuItems;

        public string FacebookLikeCode;

        public string GooglePlus1Code;

        public string TwitterFollowCode;
    }

}

