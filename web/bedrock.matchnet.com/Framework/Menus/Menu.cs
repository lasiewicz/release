﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Menus
{
    [Serializable]
    public class Menu
    {
        public List<MenuItem> MenuItems;
    }
}
