﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.TemplateControls
{
    public enum ControlType : int
    {
        ddlist,
        checkbox,
        captcha,
        textbox,
        textarea,
        password,
        mask,
        radiolist,
        text,
        region,
        date,
        dateofbirth,
        list,
        file,
        colorcode,
        textboxconfirm,
        listbox,
        regionajax,
        info,
        hidden
    }

    public enum ValidationRule : int
    {
        none,
        alpha,
        alphanum,
        email,
        regex

    }
    public enum AttrType : int
    {
        attrInt,
        attrTxt,
        attrDate,
        attrColorCode
    }

    public enum MultipleType : int
    {
        none,
        single,
        multiple
    }

    [Serializable]
    public class FrameworkTemplateControl
    {
        string _name;
        string _resourceConstant;
        string _resourceConstant1;
        bool _resourceGlobal;
        ControlType _type;
        bool _requiredFlag;
        ValidationRule _validationRule;
        int _minLen;
        int _maxLen;
        string _conditionName;
        string _conditionValue;
        string _regExString;
        string _onSubmit;
        MultipleType _multipleType;
        //[NonSerialized]
        //public FrameworkControl ResourceControl;

        [XmlAttribute("Name")]
        public string Name { get { return _name; } set { _name = value; } }
        [XmlAttribute("ResourceConstant")]
        public string ResourceConstant { get { return _resourceConstant; } set { _resourceConstant = value; } }
        [XmlAttribute("ResourceConstant1")]
        public string ResourceConstant1 { get { return _resourceConstant1; } set { _resourceConstant1 = value; } }
        [XmlAttribute("ResourceGlobal")]
        public bool ResourceGlobal { get { return _resourceGlobal; } set { _resourceGlobal = value; } }
        [XmlAttribute("Type")]
        public ControlType Type { get { return _type; } set { _type = value; } }
        [XmlAttribute("RequiredFlag")]
        public bool RequiredFlag { get { return _requiredFlag; } set { _requiredFlag = value; } }
        [XmlAttribute("Validation")]
        public ValidationRule Validation { get { return _validationRule; } set { _validationRule = value; } }
        [XmlAttribute("MinLen")]
        public int MinLen { get { return _minLen; } set { _minLen = value; } }
        [XmlAttribute("MaxLen")]
        public int MaxLen { get { return _maxLen; } set { _maxLen = value; } }
        [XmlAttribute("DisplayConditionName")]
        public string DisplayConditionName { get { return _conditionName; } set { _conditionName = value; } }
        [XmlAttribute("DisplayConditionValue")]
        public string DisplayConditionValue { get { return _conditionValue; } set { _conditionValue = value; } }
        [XmlAttribute("RegExString")]
        public string RegExString { get { return _regExString; } set { _regExString = value; } }
        [XmlAttribute("OnSubmit")]
        public string OnSubmit { get { return _onSubmit; } set { _onSubmit = value; } }
        [XmlAttribute("Multiple")]
        public MultipleType Multiple { get { return _multipleType; } set { _multipleType = value; } }
        [XmlAttribute("ClientValidationEnabled")]
        public bool ClientValidationEnabled { get; set; }

       

        // public FrameworkControl ResourceControl { get { return _resourceControl; } set { _resourceControl = value; } }



        public virtual void PersistToCookie(HttpCookie cookie)
        {


        }
        public virtual void LoadFromCookie(HttpCookie cookie)
        {

        }

        public virtual void Persist(ITemporaryPersistence persistence)
        {


        }
        public virtual void LoadFromPersistence(ITemporaryPersistence persistence)
        {

        }


    }
}
