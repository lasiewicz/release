﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;


namespace Matchnet.Web.Framework.TemplateControls
{
    [Serializable]
    public class Step
    {
        int _id;
        string _resourceConstant;
        bool _resourceGlobal;
        string _tipResourceConstant;
        List<AttributeControl> _controls;
        bool _completed;
        string _cssClass = "";
        string _name = "";
        string _nextStepURL = "";
        int _nextStepID = -1;


        [XmlAttribute("ID")]
        public int ID { get { return _id; } set { _id = value; } }
        [XmlAttribute("Name")]
        public string Name { get { return _name; } set { _name = value; } }
        [XmlAttribute("ResourceConstant")]
        public string ResourceConstant { get { return _resourceConstant; } set { _resourceConstant = value; } }
        [XmlAttribute("TipResourceConstant")]
        public string TipResourceConstant { get { return _tipResourceConstant; } set { _tipResourceConstant = value; } }
        [XmlAttribute("CssClass")]
        public string CSSClass { get { return _cssClass; } set { _cssClass = value; } }
        [XmlAttribute("ResourceGlobal")]
        public bool ResourceGlobal { get { return _resourceGlobal; } set { _resourceGlobal = value; } }
        [XmlAttribute("Completed")]
        public bool Completed { get { return _completed; } set { _completed = value; } }

        [XmlArray("Controls")]
        [XmlArrayItem("Control")]
        public List<AttributeControl> Controls { get { return _controls; } set { _controls = value; } }

        [XmlAttribute("NextStepURL")]
        public string NextStepURL { get { return _nextStepURL; } set { _nextStepURL = value; } }

        [XmlAttribute("NextStepID")]
        public int NextStepID { get { return _nextStepID; } set { _nextStepID = value; } }

        [XmlAttribute("CssMainContainer")]
        public string CssMainContainer { get; set; }
        [XmlAttribute("CssSecondaryContainer")]
        public string CssSecondaryContainer { get; set; }

        [XmlAttribute("StepButtonResource")]
        public string StepButtonResource { get; set; }

        [XmlAttribute("AdditionalHtmlResource")]
        public string AdditionalHtmlResource { get; set; }
        [XmlAttribute("MBox")]
        public bool MBox { get; set; }
        [XmlAttribute("MultiRegionMBox")]
        public bool MultiRegionMBox { get; set; }
        [XmlAttribute("MBoxEVar")]
        public string MBoxEVar { get; set; }
        [XmlAttribute("MBoxPageName")]
        public string MBoxPageName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a step in overlay reg should not be auto skipped even though values already exist in all of the attributes.
        /// </summary>
        /// <value><c>true</c> if [dont skip]; otherwise, <c>false</c>.</value>
        [XmlAttribute("DontSkip")]
        public bool DontSkip { get; set; }

        [XmlAttribute("OnePageRegContentDivCSSClass")]
        public string OnePageRegContentDivCSSClass { get; set; }

        [XmlAttribute("SubmitButtonImageFile")]
        public string SubmitButtonImageFile { get; set; }

        [XmlAttribute("OnCompleteRefreshSearchResults")]
        public bool OnCompleteRefreshSearchResults { get; set; }

        [XmlAttribute("IsFacebookReg")]
        public bool IsFacebookReg { get; set; }

        public AttributeControl GetAttributeControlByName(string attributeName)
        {
            AttributeControl control = null;

            if(_controls != null)
            {
                control = (from a in _controls where a.AttributeName == attributeName select a).FirstOrDefault();
            }

            return control;
        }
    }
}
