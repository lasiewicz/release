﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Matchnet.Content.ValueObjects.Promotion;
using Matchnet.Web;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using System.Data;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.TemplateControls
{
    public enum ListType
    {
        radio,
        ddl,
        checkbox,
        ddlcustom,
        listbox
    }

    public enum ListSelectionType
    {

        single,
        multi

    }

    /// <summary>
    /// Is the attribute is also used as a search preference that can be used in the target URL (ie mol or quick search results) to customize search results
    /// </summary>
    public enum SearchPrefType : int
    {
        /// <summary>
        /// The attribute is only a profile attribute (default)only
        /// </summary>
        No,
        /// <summary>
        /// The attribute is a search preference only
        /// </summary>
        Yes,
        /// <summary>
        /// The attrbiute is both profile attribute and search preference
        /// </summary>
        Both
    }

    [Serializable]
    public class AttributeControl : FrameworkTemplateControl
    {

        string _attrName;
        AttrType _attrType;
        bool _visible = true;
        int _attrIntVal;
        string _attrTextVal;
        DateTime _attrDateVal;
        string _errMessageRequired = "ERR_REQUIRED";
        string _errMessageValidation;
        string _errMessageMinLength;
        string _list;
        ListType _listtype;
        int _cols = 0;
        int _rows = 0;
        bool _noscroll = false;
        string _displayName;
        string _selectedValue;
        string _cssClass = "";
        SearchPrefType _searchPrefType;
        string _defaultSelection;
        string _attrTip;
        bool _clientValidationEnabled;
        bool _showLabelInsideInput = false;
        bool _disableAutoAdvance = false;
        private bool _allowZeroValue = false;


        ListSelectionType _listSelection = ListSelectionType.multi;
        //[NonSerialized]
        //public FrameworkControl ResourceControl;
        [XmlAttribute("AttributeName")]
        public string AttributeName { get { return _attrName; } set { _attrName = value; } }
        [XmlAttribute("AttributeType")]
        public AttrType AttributeType { get { return _attrType; } set { _attrType = value; } }

        [XmlAttribute("ErrorMessageRequired")]
        public string ErrorMessageRequired { get { return _errMessageRequired; } set { _errMessageRequired = value; } }
        [XmlAttribute("ErrorMessageValidation")]
        public string ErrorMessageValidation { get { return _errMessageValidation; } set { _errMessageValidation = value; } }
        [XmlAttribute("ErrorMessageMinLength")]
        public string ErrorMessageMinLength { get { return _errMessageMinLength; } set { _errMessageMinLength = value; } }


        [XmlAttribute("ListItems")]
        public string List { get { return _list; } set { _list = value; } }
        [XmlAttribute("ListItemsResource")]
        public string ListItemsResource { get; set; }
        [XmlAttribute("ListControlType")]
        public ListType ListControlType { get { return _listtype; } set { _listtype = value; } }
        [XmlAttribute("ListSelection")]
        public ListSelectionType ListSelection { get { return _listSelection; } set { _listSelection = value; } }
        [XmlAttribute("Cols")]
        public int Cols { get { return _cols; } set { _cols = value; } }
        [XmlAttribute("Rows")]
        public int Rows { get { return _rows; } set { _rows = value; } }
        [XmlAttribute("NoScroll")]
        public bool NoScroll { get { return _noscroll; } set { _noscroll = value; } }
        [XmlAttribute("DisplayName")]
        public string DisplayName { get { return _displayName; } set { _displayName = value; } }

        [XmlAttribute("CssClass")]
        public string CSSClass { get { return _cssClass; } set { _cssClass = value; } }


        [XmlAttribute("SelectedValue")]
        public string SelectedValue { get { return _selectedValue; } set { _selectedValue = value; } }

        [XmlAttribute("QuizStartQuestionID")]
        public int QuizQuestionID { get; set; }

        [XmlAttribute("QuizQuestionCount")]
        public int QuizQuestionCount { get; set; }

        [XmlAttribute("ListVisibleElementsCount")]
        public int ListVisibleElementsCount { get; set; }

        [XmlAttribute("ListElementHeight")]
        public int ListElementHeight { get; set; }

        [XmlAttribute("ListMoveFirstElementTop")]
        public bool ListMoveFirstElementTop { get; set; }

        [XmlAttribute("SearchPref")]
        public SearchPrefType SearchPref { get { return _searchPrefType; } set { _searchPrefType = value; } }

        [XmlAttribute("DisableAutoAdvance")]
        public bool DisableAutoAdvance
        {
            get { return _disableAutoAdvance; }
            set { _disableAutoAdvance = value; }
        }

        [XmlAttribute("AllowZeroValue")]
        public bool AllowZeroValue
        {
            get { return _allowZeroValue; }
            set { _allowZeroValue = value; }
        }


        /// <summary>
        /// This is the value that will be set when no value is selected by the user.
        /// </summary>
        /// <value>The default selection. </value>
        [XmlAttribute("DefaultSelection")]
        public string DefaultSelection { get { return _defaultSelection; } set { _defaultSelection = value; } }

        /// <summary>
        /// Gets or sets the attribute tip resource constant.
        /// </summary>
        /// <value>The attribute tip.</value>
        [XmlAttribute("AttributeTip")]
        public string AttributeTip { get { return _attrTip; } set { _attrTip = value; } }

        public string AttributeTextValue { get { return _attrTextVal; } set { _attrTextVal = value; } }

        [XmlAttribute("AttributeIntValue")]
        public int AttributeIntValue { get { return _attrIntVal; } set { _attrIntVal = value; } }

        public DateTime AttributeDateValue { get { return _attrDateVal; } set { _attrDateVal = value; } }

        public bool ConditionalShowFlag { get { return _visible; } set { _visible = value; } }

        [XmlAttribute("IsSplash")]
        public bool IsSplash { get; set; }



        [XmlAttribute("ClientValidationEnabled")]
        public bool ClientValidationEnabled { get; set; }

        [XmlAttribute("ShowLabelInsideInput")]
        public bool ShowLabelInsideInput { get { return _showLabelInsideInput; } set { _showLabelInsideInput = value; } }

        [XmlAttribute("IsMobileReg")]
        public bool IsMobileReg { get { return _showLabelInsideInput; } set { _showLabelInsideInput = value; } }

        public bool OnePageReg { get; set; }



        //cookie
        public override void PersistToCookie(HttpCookie cookie)
        {
            string cookiekey = _attrName.ToUpper();
            if (String.IsNullOrEmpty(cookiekey))
                cookiekey = Name;
            switch (_attrType)
            {
                case AttrType.attrInt:
                    cookie[cookiekey] = _attrIntVal.ToString();
                    break;
                case AttrType.attrTxt:
                    cookie[cookiekey] = _attrTextVal.ToString();
                    break;
                case AttrType.attrDate:
                    cookie[cookiekey] = _attrDateVal.ToString();
                    break;
            }
        }

        public override void Persist(ITemporaryPersistence persistence)
        {
            string key = _attrName.ToUpper();
            if (String.IsNullOrEmpty(key))
                key = Name;
            switch (_attrType)
            {
                case AttrType.attrInt:
                    persistence[key] = _attrIntVal.ToString();
                    break;
                case AttrType.attrTxt:
                    persistence[key] = _attrTextVal.ToString();
                    break;
                case AttrType.attrDate:
                    persistence[key] = _attrDateVal.ToString();
                    break;
            }
        }

        public override void LoadFromPersistence(ITemporaryPersistence persistence)
        {
            LoggingManager loggingManager = new LoggingManager();
            string sessionID = string.IsNullOrEmpty(persistence["SESSION_ID"]) ? "NoSession" : persistence["SESSION_ID"];

            if (!persistence.HasValues)
            {
                loggingManager.LogInfoMessage("AttributeControl", "SessionID: " + sessionID + " has no values");
                return;
            }
            string key = _attrName.ToUpper();
            loggingManager.LogInfoMessage("AttributeControl", "SessionID: " + sessionID + " AttributeName: " + key);
            if (String.IsNullOrEmpty(key))
            {
                key = Name;
                loggingManager.LogInfoMessage("AttributeControl", "SessionID: " + sessionID + " AttributeName is Name: " + key);
            }

            switch (_attrType)
            {
                case AttrType.attrInt:
                    _attrIntVal = Conversion.CInt(persistence[key]);
                    loggingManager.LogInfoMessage("AttributeControl", "SessionID: " + sessionID + " AttributeIntVal: " + _attrIntVal.ToString());
                    break;
                case AttrType.attrTxt:
                    _attrTextVal = HttpUtility.UrlDecode(persistence[key]);
                    loggingManager.LogInfoMessage("AttributeControl", "SessionID: " + sessionID + " AttributeIntVal: " + _attrTextVal);
                    break;
                case AttrType.attrDate:
                    _attrDateVal = Conversion.CDateTime(persistence[key]);
                    loggingManager.LogInfoMessage("AttributeControl", "SessionID: " + sessionID + " AttributeIntVal: " + _attrDateVal.ToString());
                    break;
                case AttrType.attrColorCode:
                    _attrTextVal = HttpUtility.UrlDecode(persistence[key]);
                    break;
            }
        }

        //cookie
        public override void LoadFromCookie(HttpCookie cookie)
        {
            if (cookie == null)
                return;
            string cookiekey = _attrName.ToUpper();
            if (String.IsNullOrEmpty(cookiekey))
                cookiekey = Name;

            switch (_attrType)
            {
                case AttrType.attrInt:
                    _attrIntVal = Conversion.CInt(cookie[cookiekey]);
                    break;
                case AttrType.attrTxt:
                    _attrTextVal = HttpUtility.UrlDecode(cookie[cookiekey]);
                    break;
                case AttrType.attrDate:
                    _attrDateVal = Conversion.CDateTime(cookie[cookiekey]);
                    break;
                case AttrType.attrColorCode:
                    _attrTextVal = HttpUtility.UrlDecode(cookie[cookiekey]);
                    break;
            }
        }

        public virtual void SaveMemberAttribute(Member.ServiceAdapters.Member member, ContextGlobal g)
        {
            if (String.IsNullOrEmpty(_attrName) || member == null)
                return;

            switch (_attrType)
            {
                case AttrType.attrInt:
                    member.SetAttributeInt(g.Brand, _attrName, _attrIntVal);
                    break;
                case AttrType.attrTxt:
                    switch (_attrName.ToLower())
                    {
                        case "emailaddress":
                        case "sitefirstname":
                        case "sitelastname":
                            member.SetAttributeText(g.Brand, _attrName, _attrTextVal, TextStatusType.Auto);
                            break;
                        case "username":
                            member.SetUsername(_attrTextVal, g.Brand);
                            break;
                        default:
                            member.SetAttributeText(g.Brand, _attrName, _attrTextVal, TextStatusType.None);
                            break;
                    }

                    break;
                case AttrType.attrDate:
                    member.SetAttributeDate(g.Brand, _attrName, _attrDateVal);
                    break;
            }
        }

        public string GetResourceString(string key, ContextGlobal g, FrameworkControl resourceControl)
        {
            string resString = "";
            if (ResourceGlobal || resourceControl == null)
                resString = g.GetResource(key);
            else
                resString = g.GetResource(key, resourceControl);
            return resString;
        }

        public virtual string GetValidationMessage(string key, ContextGlobal g, FrameworkControl resourceControl)
        {
            string displayName = String.IsNullOrEmpty(_displayName) ? _attrName : _displayName;
            string[] args = new string[] { displayName, MinLen.ToString(), MaxLen.ToString() };
            string msg = GetResourceString(key, g, resourceControl, args);
            return msg;
        }
        public string GetResourceString(string key, ContextGlobal g, FrameworkControl resourceControl, string[] args)
        {
            string resString = "";
            if (ResourceGlobal || resourceControl == null)
                resString = g.GetResource(key);
            else
                resString = g.GetResource(key, resourceControl);


            resString = String.Format(resString, args);
            return resString;
        }


        //databinding routins



        public ListItemCollection GetAttributeOptionCollectionAsListItems(ContextGlobal g, FrameworkControl resourceControl, bool skipBlanks)
        {
            ListItemCollection items = new ListItemCollection();

            bool useList = false;

            if (AttributeName == "PromotionID")
            {
                RegistrationPromotionCollection collection = PromotionSA.Instance.RetrieveRegistrationPromotions(g.Brand);

                if (collection.Count > 0)
                {
                    // dt.Columns.Add("Description");
                    foreach (RegistrationPromotion promotion in collection)
                    {
                        promotion.Description = g.GetResource(promotion.ResourceConstant, null);
                    }
                }
                
                items.Add(new ListItem("", "0"));

                foreach (Matchnet.Content.ValueObjects.Promotion.RegistrationPromotion promotion
                    in collection)
                {
                    items.Add(new ListItem(promotion.Description, promotion.PromotionID.ToString()));
                }
            }
            else if (AttributeName == "MoreChildrenFlag")
            {
                string[] listItems = null;
                if (!String.IsNullOrEmpty(_list))
                {
                    listItems = _list.Split(',');
                    if (listItems.Length > 0)
                        useList = true;
                }
                
                DataTable dt = Util.Option.GetMoreChildrenOptions(g, ResourceGlobal ? null : resourceControl);
                
                foreach(DataRow dr in dt.Rows)
                {
                    //rowBlank["Value"] = "";
                    //rowBlank["Content"] = "";
                    //rowBlank["ListOrder"] = 0;
                    if(Conversion.CInt(dr["Value"]) <= Constants.NULL_INT && skipBlanks)
                    {
                        continue;
                    }

                    if (useList && !listItems.Contains<string>(dr["Value"].ToString()))
                    { continue; }

                    ListItem item = new ListItem();
                    item.Value = dr["Value"].ToString();

                    if (ResourceGlobal)
                    {
                        item.Text = dr["Content"].ToString();
                    } 
                    items.Add(item);
                    item = null;
                }

            }

            else if (AttributeName != Constants.NULL_STRING)
            {
                AttributeOptionCollection attOptions = AttributeOptionSA.Instance.GetAttributeOptionCollection(AttributeName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

                string[] listItems = null;
                if (!String.IsNullOrEmpty(_list))
                {
                    listItems = _list.Split(',');
                    if (listItems.Length > 0)
                        useList = true;
                }
                foreach (AttributeOption option in attOptions)
                {
                    if (option.Value <= Constants.NULL_INT && skipBlanks)
                    {
                        continue;
                    }
                    if (useList && !listItems.Contains<string>(option.Value.ToString()))
                    { continue; }
                    ListItem item = new ListItem();
                    item.Value = option.Value.ToString();

                    if (ResourceGlobal)
                    {
                        item.Text = g.GetTargetResource(option.ResourceKey, BrandConfigSA.Instance.GetBrandByID(g.Brand.BrandID));
                    }
                    //item.Text = g.GetResource(option.ResourceKey);
                    else
                    {
                        string resourceKey = option.ResourceKey;
                        if (this.OnePageReg && this.Name == "PartnersOffers2")
                        {
                            resourceKey = this.ResourceConstant;
                        }
                        item.Text = g.GetResource(resourceKey, resourceControl);
                    }

                    items.Add(item);
                    item = null;


                }
            }

            return items;
        }

        public ListItemCollection GetListItems(ContextGlobal g, FrameworkControl resourceControl, bool skipBlanks)
        {
            ListItemCollection items = new ListItemCollection();

            try
            {
                string[] listItems = null;
                if (!String.IsNullOrEmpty(_list))
                {
                    listItems = _list.Split(',');


                }
                string[] listItemsResx = null;
                if (!String.IsNullOrEmpty(ListItemsResource))
                {
                    listItemsResx = ListItemsResource.Split(',');

                }

                for (int i = 0; i < listItems.Length; i++)
                {
                    ListItem item = new ListItem();
                    item.Value = listItems[i];

                    if (ResourceGlobal)
                        item.Text = g.GetTargetResource(listItemsResx[i], BrandConfigSA.Instance.GetBrandByID(g.Brand.BrandID));
                    //item.Text = g.GetResource(option.ResourceKey);
                    else
                        item.Text = g.GetResource(listItemsResx[i], resourceControl);
                    items.Add(item);
                    item = null;


                }

                return items;
            }
            catch (Exception ex)
            { g.ProcessException(ex); return items; }
        }

        public DataTable GetAttributeOptionAsDataTable(bool useDescription, bool includeBlankValue, ContextGlobal g, FrameworkControl resourceControl)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Value", typeof(System.String));
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("ListOrder", typeof(System.Int32));


            AttributeOptionCollection attOptions = AttributeOptionSA.Instance.GetAttributeOptionCollection(AttributeName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

            foreach (AttributeOption attributeOption in attOptions)
            {
                DataRow row = dt.NewRow();

                if (attributeOption.Value != Constants.NULL_INT)
                {
                    row["Value"] = attributeOption.Value;
                }
                else
                {
                    // This means we got a blank value from the DB
                    if (includeBlankValue)
                    {
                        row["Value"] = string.Empty;
                    }
                    else
                    {
                        // Skip this one
                        row = null;
                        continue;
                    }
                }

                if (useDescription)
                {
                    row["Content"] = attributeOption.Description;
                }
                else
                {
                    row["Content"] = GetResourceString(attributeOption.ResourceKey, g, resourceControl);
                    //row["Content"] = g.GetResource(attributeOption.ResourceKey);
                }

                row["ListOrder"] = attributeOption.ListOrder;

                dt.Rows.Add(row);
            }

            return dt;
        }
        public override string ToString()
        {
            string retStr = "";
            System.Text.StringBuilder bld = new System.Text.StringBuilder();
            try
            {
                bld.Append("AttributeName=" + ((AttributeName != null) ? AttributeName : "") + ", ");
                bld.Append("AttributeTextValue=" + ((AttributeTextValue != null) ? AttributeTextValue : "") + ", ");
                bld.Append("AttributeIntValue=" + AttributeIntValue.ToString() + ", ");
                bld.Append("AttributeDateValue=" + AttributeDateValue.ToString() + ", ");
                bld.Append("DisplayName=" + ((DisplayName != null) ? DisplayName : "") + ", ");
                retStr = bld.ToString();
                bld = null;
                return retStr;
            }
            catch (Exception ex) { bld = null; return retStr; }

        }

        public virtual string CreateValidationRule()
        {

            string rule_format = "\t\t{0}: {1}{2}\r\n";
            string rules = "";
            try
            {
                rules += String.Format(rule_format, "required", RequiredFlag.ToString().ToLower(), ",");
                rules += String.Format(rule_format, "minlength", MinLen.ToString().ToLower(), ",");
                rules += String.Format(rule_format, "maxlength", MaxLen.ToString().ToLower(), ",");
                if (Validation == Framework.TemplateControls.ValidationRule.alphanum)
                    rules += String.Format(rule_format, "alphanum", "true", "");
                if (Validation == Framework.TemplateControls.ValidationRule.alpha)
                    rules += String.Format(rule_format, "lettersonly", "true", "");
                if (Validation == Framework.TemplateControls.ValidationRule.email)
                    rules += String.Format(rule_format, "email", "true", "");


                return rules;
            }
            catch (Exception ex)
            { return rules; }
        }

        public virtual string CreateValidationMessage(FrameworkControl resourceControl, ContextGlobal g)
        {

            string rule_format = "{0}: \"{1}\"{2}";
            string rules = "";
            try
            {
                // rules += String.Format(rule_format, "required", GetValidationMessage(ErrorMessageRequired,g,resourceControl), ",");
                rules += String.Format(rule_format, "minlength", GetValidationMessage("ERR_MIN_LENGTH", g, resourceControl), "");
                /*   rules += String.Format(rule_format, "maxlength", GetValidationMessage("ERR_MAX_LENGTH", g, resourceControl), ",");
                   if (Validation == Framework.TemplateControls.ValidationRule.alphanum)
                       rules += String.Format(rule_format, "alphanum", GetValidationMessage("ERR_INVALID", g, resourceControl), "");
                   if (Validation == Framework.TemplateControls.ValidationRule.alpha)
                       rules += String.Format(rule_format, "lettersonly", GetValidationMessage("ERR_INVALID", g, resourceControl), "");
                   if (Validation == Framework.TemplateControls.ValidationRule.email)
                       rules += String.Format(rule_format, "email", GetValidationMessage("ERR_INVALID", g, resourceControl), "");

                   */
                return rules;
            }
            catch (Exception ex)
            { return rules; }
        }
    }


}
