﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Framework.TemplateControls
{
    public enum ScenarioCondition : int
    {
        mod,
        random,
        condition
    }

    [Serializable]
    public class Scenario
    {
        int _id;
        string _name;
        string _template;
        List<Step> _steps;

        [XmlAttribute("ID")]
        public int ID { get { return _id; } set { _id = value; } }
        [XmlAttribute("Name")]
        public string Name { get { return _name; } set { _name = value; } }
        [XmlAttribute("Template")]
        public string Template { get { return _template; } set { _template = value; } }
        [XmlArray("Steps")]
        [XmlArrayItem("Step")]
        public List<Step> Steps { get { return _steps; } set { _steps = value; } }
        [XmlAttribute("Blank")]
        public bool Blank { get; set; }
        [XmlAttribute("GAMEnableOnly")]
        public bool GAMEnableOnly { get; set; }
        [XmlAttribute("IsRegOverlay")]
        public bool IsRegOverlay { get; set; }
        [XmlAttribute("IsOnePageReg")]
        public bool IsOnePageReg { get; set; }
        [XmlAttribute("EVar43")]
        public string EVar43 { get; set; }
        [XmlAttribute("EVar47")]
        public string EVar47 { get; set; }
        [XmlAttribute("EVar48")]
        public string EVar48 { get; set; }
        [XmlAttribute("PageName")]
        public string PageName { get; set; }
        [XmlAttribute("IsFacebookRegistration")]
        public bool IsFacebookRegistration { get; set; }
        [XmlAttribute("FirstStepContinueButtonResourceConstant")]
        public String FirstStepContinueButtonResourceConstant { get; set; }
        [XmlAttribute("RegCompleteDivID")]
        public string RegCompleteDivID { get; set; }
        [XmlAttribute("IsIFrame")]
        public bool IsIFrame { get; set; }

        public Step GetStep(int id)
        {
            try
            {
                if (_steps == null)
                    return null;
                IEnumerable<Step> query = from s in _steps
                                          where s.ID == id
                                          select s;

                return query.ToList<Step>()[0];

            }
            catch (Exception ex)
            { return null; }

        }

        public Step GetStepContainingAttributeControl(string attributeName)
        {
            Step step = null;

            if (_steps != null)
            {
                step = (from s in _steps 
                        where (s.Controls.Count(a => a.AttributeName == attributeName) == 1) 
                        select s).
                        FirstOrDefault();
            }

            return step;
        }

        [XmlArray("StaticConditions")]
        [XmlArrayItem("StaticCondition")]
        public List<StaticCondition> StaticConditions { get; set; }




    }

    public enum ConditionTypeEnum
    {
        BrandID,
        URLParam,
        FullURLContains,
        DBForcedScenarioName
    }

    [Serializable]
    public class StaticCondition
    {
        [XmlAttribute("ConditionType")]
        public ConditionTypeEnum ConditionType { get; set; }
        [XmlAttribute("ConditionValue")]
        public string ConditionValue { get; set; }
    }


    [Serializable]
    public class Scenarios
    {
        List<Scenario> _scenarios;
        ScenarioCondition _condition;

        [XmlAttribute("ScenarioCondition")]
        public ScenarioCondition Condition { get { return _condition; } set { _condition = value; } }

        [XmlArray("ScenarioList")]
        [XmlArrayItem("Scenario")]
        public List<Scenario> ScenarioList { get { return _scenarios; } set { _scenarios = value; } }


        public Scenario GetScenario(string name)
        {
            try
            {
                if (_scenarios == null)
                    return null;
                IEnumerable<Scenario> query = from s in _scenarios
                                              where s.Name == name
                                              select s;

                return query.ToList<Scenario>()[0];

            }
            catch (Exception ex)
            { return null; }

        }
    }
}
