#region System references
using System;
using System.Collections.Specialized;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Applications.QuickSearch;
#endregion

#region Matchnet Web App references

using Matchnet.CachingTemp;
using Matchnet.Web.Framework.Diagnostics;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Web.Framework.Ui.PageElements;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Applications.Subscription;
using Matchnet.Web.Framework.Ui.HeaderNavigation20;
#endregion

#region Matchnet Service References
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.List.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Security;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.Content.ValueObjects.Promotion;
using Matchnet.Web.Applications.QuickSearch;
using Matchnet.CachingTemp;
using Spark.Common.Chat;
using Matchnet.Web.Interfaces;
using Matchnet.Web.Framework.Managers;

#endregion

namespace Matchnet.Web.Framework
{
    /// <summary>
    /// Summary description for ContextGlobal.
    /// </summary>
    public class ContextGlobal
    {
        private Brand _Brand;
        private Brand _ImpersonateBrand;
        private BrandAlias _BrandAlias;

        private UserSession _Session;

        private StringDictionary _expansionTokens = new StringDictionary();
        private Member.ServiceAdapters.Member _member;
        private Member.ServiceAdapters.Member _targetMember;
        private List.ServiceAdapters.List _list;
        private Notifications _Notification;
        private bool _LeftNavigationVisible = false;

        private bool _ImpersonateContext;
        private System.Web.UI.Page _Page;
        private ContextExceptions _ContextExceptions = new ContextExceptions();
        private string _DesignTimeURL = "http://www.jdate.com/default.aspx";
        private IRight _rightNavControl;
        private BreadCrumbTrail _breadCrumbTrailHeader;
        private BreadCrumbTrail _breadCrumbTrailFooter;
        private IHeader _Header;
        private Footer20 _Footer20;
        public Head20 Head20 { set; get; }
        private bool _saveSession = true;
        private SearchPreferenceCollection _searchPreferences;
        private Spark.SAL.MemberSearchCollection _memberSearchCollection;
        private SearchPreferenceCollection _quickSearchPreferences;
        private SearchPreferenceCollection _membersOnlineSearchPrerences;
        private bool _hasNewMessage;
        private bool _newMessageCheckDone;
        private LayoutTemplate _layoutTemplate;
        private EmailFolderCollection _memberEmailFolders;
        private bool _UseBrandAliasURI;
        private bool _IsAdmin;
        private int _RandomNumber = int.MinValue;

        private string _encryptedMemberID = String.Empty;
        private string _environment = String.Empty;
        // for impersonation.
        private int _ImpersonateMemberID = Constants.NULL_INT;

        private string _ToolbarFlag;

        private InstanceMemberCollection premiumServices;

        private bool _hasValidCCOnFile = false;
        private Hashtable _currencyMap = null;
        private bool? _isGamesEnabled = null;
        private bool _isAPI = false;
        private bool _isApiSession = false;
        private Hashtable _EditControls = new Hashtable();
        private bool _ResetSearchStartRow = false;
        private HTTPContextWrappers.CurrentRequest _CurrentRequestWrapper = new HTTPContextWrappers.CurrentRequest();
        private ILoggingManager _loggingManager = new LoggingManager();

        /// <summary>
        /// There are so many references to "impersonate" and "admin" already; they are very confusing, but I believe
        /// all those properties combined only support a small number of pages, and they are only good for one request only.
        /// e.g. coming from AdminTool to view a member's profile, etc.
        /// Due to this, I decided to name my property with "stealth" to avoid confusion.  When stealth logon is initiated from AdminTool,
        /// we log the admin in as the actual member, and this property is the only thing that can tell apart a real member log on versus an
        /// admin logging in as a member.
        /// </summary>
        private Member.ServiceAdapters.Member _stealthLogonAdmin = null;

        public Member.ServiceAdapters.Member StealthLogonAdmin
        {
            get
            {
                if (_stealthLogonAdmin == null)
                {
                    var adminId = Session.GetInt(WebConstants.SESSION_STEALTH_LOGON_ADMIN_MEMBER_ID, Constants.NULL_INT);
                    if (adminId != Constants.NULL_INT)
                    {
                        _stealthLogonAdmin = MemberSA.Instance.GetMember(adminId, MemberLoadFlags.None);
                    }
                }

                return _stealthLogonAdmin;
            }
        }

        public bool IsSiteMingle
        {
            get
            {
                bool isSiteMingle = false;
                if (this.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BBW || this.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BlackSingles)
                {
                    //we would expand this to include black and christain when we migrate those sites
                    isSiteMingle = true;
                }
                return isSiteMingle;
            }
        }

        public bool HasValidCCOnFile
        {
            get
            {
                if (Session.Get(WebConstants.SESSION_PROPERTY_NAME_HAS_VALID_CC_ON_FILE) != null)
                {
                    _hasValidCCOnFile = Convert.ToBoolean(Session.Get(WebConstants.SESSION_PROPERTY_NAME_HAS_VALID_CC_ON_FILE));
                }
                else
                {
                    //TODO - Update this later to pull from UPS, but this isn't needed at this time
                    _hasValidCCOnFile = false;// Purchase.ServiceAdapters.PurchaseSA.Instance.HasValidCCOnFile(Member.MemberID, Brand.BrandID);
                    Session.Add(WebConstants.SESSION_PROPERTY_NAME_HAS_VALID_CC_ON_FILE, _hasValidCCOnFile, SessionPropertyLifetime.Temporary);
                }
                return _hasValidCCOnFile;
            }
            set
            {
                _hasValidCCOnFile = value;
                Session.Add(WebConstants.SESSION_PROPERTY_NAME_HAS_VALID_CC_ON_FILE, _hasValidCCOnFile, SessionPropertyLifetime.Temporary);
            }
        }

        //Is yes no maybe feature enabled
        public bool IsYNMEnabled { get; set; }
        // Connect IM transition
        private string _isConnectIMEnabled;

        public PromoPlansHelper PromoPlan { get; set; }

        /// <summary>
        /// Determines whether the site20 is enabled.  This is currently set in the Default page.
        /// </summary>
        public bool IsSite20Enabled { get; set; }

        /// <summary>
        /// Determines whether Wide2Column template should only render right control
        /// </summary>
        public bool LoadingRightOnly { get; set; }

        /// <summary>
        /// Google Ad Manager
        /// 
        /// Register Google Ad Slots using this property.
        /// </summary>
        public Ui.Advertising.GAM GAM { get; set; }

        public Analytics.Omniture AnalyticsOmniture { get; set; }

        public AutoLoginHelper AutoLogin { get; private set; }

        public EmailVerifyHelper EmailVerify { get; private set; }

        public MemberPhotoReqHelper MemberPhotoRequire { get; private set; }

        public StringDictionary ExpansionTokens
        {
            get { return _expansionTokens; }
        }

        public string Environment
        {
            get { return _environment.ToLower(); }
        }

        public bool EcardsEnabled { get; private set; }
        public bool EventsEnabled { get; private set; }

        public bool IsToolbarEnabled
        {
            get
            {
                bool flag = false;
                try
                {
                    if (_ToolbarFlag == null)
                    {
                        _ToolbarFlag = RuntimeSettings.GetSetting("TOOLBAR_FLAG", Brand.Site.Community.CommunityID, Brand.Site.SiteID, Brand.BrandID);

                    }
                    flag = Convert.ToBoolean(_ToolbarFlag);
                    return flag;

                }
                catch (Exception ex)
                {
                    return flag;
                }
            }
        }

        public bool IsConnectIMEnabled
        {
            get
            {
                bool flag;

                if (_isConnectIMEnabled == null)
                {
                    // No idea why it's doing try parse on a setting value. Isn't there a global default value?
                    // CONNECT_IM_ENABLE - replaced by GetChatProvider()
                    // TODO update this method to just do a simple ?: check.
                    if (FrameworkGlobals.GetChatProvider(Brand) == ChatProvider.Connect)
                        _isConnectIMEnabled = "true";

                }

                if (!bool.TryParse(_isConnectIMEnabled, out flag))
                    flag = false;

                return flag;
            }
        }

        public WebConstants.LayoutVersions LayoutVersion { get; set; }
        public bool HideSubNowBanner { get; set; }
        public ILoggingManager LoggingManager
        {
            get { return _loggingManager; }
            set { _loggingManager = value; }
        }

        public ContextGlobal(HttpContext context)
        {
            RightNavigationVisible = true;

            bool useMembase = Convert.ToBoolean(RuntimeSettings.GetSetting(WebConstants.MEMBASE_SETTING_CONSTANT));
            bool useMembaseForWeb = Convert.ToBoolean(RuntimeSettings.GetSetting(WebConstants.MEMBASE_WEB_SETTING_CONSTANT));
            if (useMembase && useMembaseForWeb)
            {
                MembaseConfig membaseConfig =
                    RuntimeSettings.GetMembaseConfigByBucket(WebConstants.MEMBASE_WEB_BUCKET_NAME);
                Cache = Spark.Caching.MembaseCaching.GetSingleton(membaseConfig);
            }
            else
            {
                Cache = CachingTemp.Cache.GetInstance();
            }
            LayoutVersion = WebConstants.LayoutVersions.versionNarrow;
            SaveSessionAsync = true;

            if (context != null)
            {
                IsDevMode = Boolean.Parse(RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE"));

                ParseDestinationURL();
                InitializeSession();
                InitializeBrand();
                InitializeBrandAlias();
                InitializeIsAdmin();
                AutoLogin = new AutoLoginHelper();
                //SetPromoPlan(context);

                // TODO: This doesn't belong in g, this is page
                //       behavior specific.
                if (context.Request[WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID] != null)
                {
                    PersistLayoutTemplate =
                        Conversion.CInt(context.Request[WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID]) ==
                        (int)LayoutTemplate.PopupOnce;
                }

                HttpContext.Current.Items["g"] = this;
                if (Convert.ToBoolean(RuntimeSettings.GetSetting("PROMOENGINE_ENABLE", _Brand.Site.Community.CommunityID, _Brand.Site.SiteID)))
                {
                    PromoPlan = new PromoPlansHelper(SubscribeCommon.SUBSCRIPTION_ENCRYPT_KEY, _Brand);
                }
                InitializeExpansionTokens(); //this is the only place ever calling this. So move this internally to ContextGlobal constructor.
                _environment = FrameworkGlobals.GetEnvironmentString();
                // TODO: This doesn't belong in g, this is layout
                //       specific
                LeftPanel = new Panel { Width = 80, Height = 300 };

                //Expire cache access to help avoid synchronization delay
                if (HttpContext.Current.Request.RawUrl.ToLower().IndexOf("subscriptionconfirmation.aspx") > 0)
                {
                    if (Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) > 0)
                    {
                        MemberSA.Instance.ExpireCachedMemberAccess(Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), true);
                        //System.Diagnostics.EventLog.WriteEntry("WWW", "ContextGlobal: ExiredCachedMemberAccess" + Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID).ToString() + " for SubscriptionConfirmation page");
                    }
                }
                else if (Member != null)
                {
                    try
                    {
                        if (Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) == TargetMemberID &&
                            TargetMemberID == Member.MemberID)
                        {
                            bool accessCacheRefresh = Conversion.CBool(Member.GetAttributeInt(Brand, "AccessCacheRefresh", 0));
                            if (accessCacheRefresh)
                            {
                                Member.SetAttributeInt(Brand, "AccessCacheRefresh", 0);
                                MemberSA.Instance.SaveMember(Member);
                                MemberSA.Instance.ExpireCachedMemberAccess(Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), true);
                                System.Diagnostics.EventLog.WriteEntry("WWW", "ContextGlobal: AccessCacheRefresh attribute indicates to expire cached access for " + Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID).ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ProcessException(ex);
                    }
                }
            }
            else
            {
                System.Diagnostics.Trace.WriteLine("ContextGlobal(HttpContext ctx) created with null context");
            }

            try
            {
                EcardsEnabled = bool.Parse(RuntimeSettings.GetSetting("ENABLE_ECARDS",
                    Brand.Site.Community.CommunityID, Brand.Site.SiteID));

                EventsEnabled = bool.Parse(RuntimeSettings.GetSetting("ENABLE_EVENTS",
                    Brand.Site.Community.CommunityID, Brand.Site.SiteID));


                EmailVerify = new EmailVerifyHelper(_Brand);
                MemberPhotoRequire = new MemberPhotoReqHelper(_Brand);
                IsYNMEnabled = bool.Parse(RuntimeSettings.GetSetting("ENABLE_YES_NO_MAYBE", this.Brand.Site.Community.CommunityID, this.Brand.Site.SiteID));
            }
            catch (Exception ex)
            {
                ProcessException(ex);
            }
        }

        public bool SaveSession
        {
            set { _saveSession = value; }
        }

        public bool ValidateMemberContact()
        {
            return ValidateMemberContact(Constants.NULL_INT);
        }


        /// <summary>
        /// Refactored this to point to identical static method in FrameworkGlobals.
        /// </summary>
        /// <returns></returns>
        public bool IsSecureRequest()
        {
            return FrameworkGlobals.IsSecureRequest();
        }

        public void SetPageTitle(string title)
        {
            if (!string.IsNullOrEmpty(title))
            {
                if (this.Head20 != null)
                {
                    this.Head20.PageTitle = title;
                }
            }
        }

        public void SetMetaDescription(string metaDescription)
        {
            if (!string.IsNullOrEmpty(metaDescription))
            {
                if (this.Head20 != null)
                {
                    this.Head20.MetaDescription = metaDescription;
                }
            }
        }

        public void SetMetaDescriptionWithTag(string metaDescription)
        {
            if (!string.IsNullOrEmpty(metaDescription))
            {
                if (this.Head20 != null)
                {
                    metaDescription = "<meta name=\"description\" content=\"" +
                                        metaDescription +
                                        "\" />";

                    this.Head20.MetaDescription = metaDescription;
                }
            }
        }

        public void SetMeta(string meta)
        {
            if (!string.IsNullOrEmpty(meta))
            {
                if (this.Head20 != null)
                {
                    this.Head20.Meta = meta;
                }
            }
        }

        public bool ValidateMemberContact(int toMemberID)
        {
            if (Member != null)
            {
                // 1) If the member is paying, they can contact
                // this method is mainly for the IM, which falls under promotional access or subscriber access
                if (MemberPrivilegeAttr.IsPermitMember(this))
                    return true;

                // 2) If the member is an admin, they can contact
                if (IsAdmin)
                    return true;

                // 3) If this is a freesite, they can contact
                if (!Brand.IsPaySite)
                    return true;

                // If this is Jdate.co.il, there are some special rules:
                if (Brand.BrandID == (int)WebConstants.BRAND_ID.JDateCoIL)
                {
                    // RegionLanguage memberRegion = new RegionLanguage();
                    // memberRegion.Populate(Member.GetAttributeInt(WebConstants.ATTRIBUTE_NAME_REGIONID), PrivateLabel.LanguageID);
                    //	TOFIX - This is strange.  Typically the Populate method translates directly to the new RetrieveRegionByID method
                    //	however here the CountryRegionID field is used later on, and this is only found in RegionLanguages.  For now
                    //	we'll try to use a RegionLanguage and the RetrievePopulatedHierarchy method to construct it and see how
                    //	that works for us - dcornell
                    RegionLanguage memberRegion = RegionSA.Instance.RetrievePopulatedHierarchy(Member.GetAttributeInt(this.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID), Brand.Site.LanguageID);
                    // Region memberRegion = RegionSA.Instance.RetrieveRegionByID(Member.GetAttributeInt(WebConstants.ATTRIBUTE_NAME_REGIONID), PrivateLabel.LanguageID);

                    // 4) If we aren't in the privatelabel's default country and this is jdate.co.il and we are trying to IM, stop it...
                    if (DestinationPage == Pages.PAGE_INSTANT_MESSENGER_MAIN)
                        return false;

                    // 5) If we are in the same country as the privatelabel's default
                    if (memberRegion.CountryRegionID == Brand.Site.DefaultRegionID)
                    {
                        //If we aren't trying to contact a specific member (e.g., for Folder Settings), it's ok
                        if (toMemberID == Constants.NULL_INT)
                            return true;

                        // RegionLanguage toMemberRegion = new RegionLanguage();
                        //	TOFIX - This is strange.  Typically the Populate method translates directly to the new RetrieveRegionByID method
                        //	however here the CountryRegionID field is used later on, and this is only found in RegionLanguages.  For now
                        //	we'll try to use a RegionLanguage and the RetrievePopulatedHierarchy method to construct it and see how
                        //	that works for us - dcornell
                        Member.ServiceAdapters.Member toMember = MemberSA.Instance.GetMember(toMemberID, MemberLoadFlags.None);
                        RegionLanguage toMemberRegion = RegionSA.Instance.RetrievePopulatedHierarchy(toMember.GetAttributeInt(this.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID), Brand.Site.LanguageID);
                        //Member.Member toMember = new Member.Member();
                        //toMember.Populate(toMemberID, PrivateLabel.PrivateLabelID);
                        // toMemberRegion.Populate(toMember.Attributes.ValueInt(PrivateLabel.PrivateLabelID, "RegionID"), PrivateLabel.LanguageID);

                        // 6) If the member is in the same country as the toMember, it's ok
                        //if (memberRegion.CountryRegionID == toMemberRegion.CountryRegionID)
                        //	return true;
                    }
                }
            }
            return false;
        }

        public string YesNo(bool blnVal)
        {
            if (blnVal)
            {
                return GetResource("YES");
            }

            return GetResource("NO");
        }

        public int TargetMemberID
        {
            get
            {
                if (Member != null && (IsAdmin || AdminImpersonate))
                {
                    if (_ImpersonateContext)
                    {
                        return _ImpersonateMemberID;
                    }
                }
                return Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
            }
        }

        public Member.ServiceAdapters.Member TargetMember
        {
            get
            {

                if (_targetMember == null)
                {
                    if (IsAdmin || AdminImpersonate)
                        _targetMember = MemberSA.Instance.GetMember(TargetMemberID, MemberLoadFlags.IngoreSACache); //09022008 TL - Updated to ensure admin always has the latest member object.
                    else
                        _targetMember = MemberSA.Instance.GetMember(TargetMemberID, MemberLoadFlags.None);
                }

                return _targetMember;
            }
        }


        public int TargetBrandID
        {
            get
            {
                if ((IsAdmin || AdminImpersonate) && Member != null && _ImpersonateContext)
                {
                    return TargetBrand.BrandID;
                }
                return Brand.BrandID;
            }
        }


        public int TargetCommunityID
        {
            get
            {
                if ((IsAdmin || AdminImpersonate) && Member != null && _ImpersonateContext)
                {
                    return TargetBrand.Site.Community.CommunityID;
                }
                return Brand.Site.Community.CommunityID;
            }
        }

        public int TargetLanguageID
        {
            get
            {
                if ((IsAdmin || AdminImpersonate) && Member != null)
                {
                    if (_ImpersonateContext)
                    {
                        return TargetBrand.Site.LanguageID;
                    }
                }
                return Brand.Site.LanguageID;
            }
        }

        public Brand TargetBrand
        {
            get
            {
                if ((IsAdmin || AdminImpersonate) && Member != null && _ImpersonateContext)
                {
                    return _ImpersonateBrand;
                }
                return Brand;
            }
        }

        public bool ImpersonateContext
        {
            get { return _ImpersonateContext; }
        }


        public Panel LeftPanel { get; private set; }

        public bool SaveSessionAsync { get; set; }

        public void Context_EndRequest()
        {
            Notification.Clear();

            if (_Session != null && _saveSession && _Session.Properties != null)
            {
                string domainName = String.Empty;

                if ((_Brand.StatusMask & (int)StatusType.CoBrand) == (int)StatusType.CoBrand)
                {
                    domainName = _Brand.Site.Name;
                }
                else if (!_UseBrandAliasURI || _BrandAlias == null)
                {
                    domainName = Brand.Uri;
                }
                else
                {
                    domainName = _BrandAlias.Uri;
                }

                Matchnet.Session.ServiceAdapters.SessionSA.Instance.SaveSession(_Session, domainName, IsDevMode, SaveSessionAsync);
            }
        }

        public bool HasPrivilege(Int32 memberID, Int32 privilegeID)
        {
            return MemberPrivilegeSA.Instance.HasPrivilege(memberID, privilegeID, HttpContext.Current);
        }

        public bool IsAdmin
        {
            get
            {
                return _IsAdmin;
            }
        }

        public bool AdminImpersonate
        {
            get
            {
                //Admin coming from admintool for viewing member profile
                int memberId = Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
                bool fromAdmintool = false;
                bool.TryParse(HttpContext.Current.Request["admin"], out fromAdmintool);
                if (fromAdmintool && MemberPrivilegeSA.Instance.IsAdminByPrivilegesOnly(memberId))
                {
                    return true;
                }
                return false;
            }
        }

        public int RandomNumber
        {
            get
            {
                if (_RandomNumber == int.MinValue)
                {
                    Random number = new Random();
                    _RandomNumber = number.Next();
                }
                return _RandomNumber;
            }
        }


        private void InitializeIsAdmin()
        {
            int memberID = Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
            if (memberID > 0)
            {
                _IsAdmin = MemberPrivilegeSA.Instance.IsAdmin(memberID, HttpContext.Current);
            }
        }

        /////////////////////////////////////////
        // constant accessors resources

        internal string GetResource(string resourceConstant)
        {
            return GetResource(resourceConstant, null);
        }

        public string GetResource(string resourceConstant, object caller)
        {
            return GetResource(resourceConstant, caller, _expansionTokens);
        }

        public string GetTargetResource(string resourceConstant, object caller)
        {
            return GetTargetResource(resourceConstant, caller, _expansionTokens);
        }

        public string GetResource(string resourceConstant, object caller, StringDictionary expansionTokens)
        {
            return GetResource(resourceConstant, caller, expansionTokens, null);
        }

        public string GetTargetResource(string resourceConstant, object caller, StringDictionary expansionTokens)
        {
            return GetTargetResource(resourceConstant, caller, expansionTokens, null);
        }

        public string GetResource(string resourceConstant, object caller, string[] args)
        {
            return GetResource(resourceConstant, caller, _expansionTokens, args);
        }

        public string GetTargetResource(string resourceConstant, object caller, string[] args)
        {
            return GetTargetResource(resourceConstant, caller, _expansionTokens, args);
        }

        public string GetResource(string resourceConstant, string[] args, bool replaceImageTokens, object caller)
        {
            string resourceString = GetResource(resourceConstant, caller, args);
            if (replaceImageTokens)
            {
                resourceString = Localizer.ExpandImageTokens(resourceString, Brand);
            }

            return resourceString;
        }

        public string GetResource(string resourceConstant, object caller, StringDictionary expansionTokens, string[] args)
        {
            return GetResource(resourceConstant, caller, Brand, expansionTokens, args);
        }

        public string GetTargetResource(string resourceConstant, object caller, StringDictionary expansionTokens, string[] args)
        {
            return GetResource(resourceConstant, caller, TargetBrand, expansionTokens, args);
        }

        public string GetTargetResource(string resourceConstant, Brand targetBrand)
        {
            return GetResource(resourceConstant, null, targetBrand, _expansionTokens, null);
        }

        public string GetResource(Brand brand, string resourceConstant)
        {
            return GetResource(resourceConstant, null, brand, _expansionTokens, null);
        }

        private string GetResource(string resourceConstant, object caller, Brand brand, StringDictionary expansionTokens, string[] args)
        {
            if (Matchnet.Conversion.CBool(
                RuntimeSettings.GetSetting("SHOW_RESOURCE_HINTS", brand.Site.Community.CommunityID, brand.Site.SiteID)))
            {
                return Localizer.GetFormattedStringResource(resourceConstant, caller, brand, expansionTokens, args, true);
            }
            else
            {
                return Localizer.GetFormattedStringResource(resourceConstant, caller, brand, expansionTokens, args, false);
            }
        }


        /// <summary>
        /// Returns ampersand &[ParameterName]=[UrlEncoded string] in this case 
        /// &DestinationURL=[UrlEncoded string]. DO NOT USE THIS METHOD IF DESTINATION URL
        /// IS THE ONLY PARAMETER IN YOUR DESIRED URL BECAUSE AS OF 9/28/05
        /// this returns string starting with ampersand only (not question mark (?).
        /// </summary>
        /// <returns></returns>
        public string AppendDestinationURL()
        {
            string destinationURL = String.Empty;
            string result = String.Empty;

            destinationURL = GetDestinationURL();

            if (destinationURL.Length > 0)
            {
                result = "&DestinationURL=" + destinationURL; //System.Web.HttpUtility.UrlEncode(destinationURL);
                //Adam: 9/28/05 - in attempt to fix / Hack issued 16191
                // This code below is horribly incorrect. Cannot determine whether to
                //append destination url with a ? or a & based on knowing JUST the
                //Destination url! You must have input of the rest of the URL,
                //which is NOT possible to know in this context unless the COMPLETE url is 
                // passed in (which it isn't)
                /*
                 * result = 
                    (destinationURL.IndexOf("?") > 0) ? 
                    "&DestinationURL=" + System.Web.HttpUtility.UrlEncode(destinationURL) : 
                    "?DestinationURL=" + System.Web.HttpUtility.UrlEncode(destinationURL); 
                */
            }

            return result;
        }

        /// <summary>
        /// Read the "DestinationURL" param from the URL QueryString and if the parameter exists, redirect the browser to it.
        /// </summary>
        public void DestinationURLRedirect()
        {
            // Using StripQuerystringFromURL will lose URL QueryString params if any. 
            // We want to keep these params because they may include prtid's.

            bool appendEncryptedSessionID = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_SPWSCYPHER_IN_CONNECT_URL", this.Brand.Site.Community.CommunityID, Brand.Site.SiteID, Brand.BrandID));

            string destinationUrl = GetDestinationURL(appendEncryptedSessionID);

            if (destinationUrl.Length == 0) // no need to do further processing if destinationUrl is empty.
            {
                return;
            }

            bool isAllowedRedirect = true;

            if (!destinationUrl.StartsWith("/")) // a relative URL does not need extra checking
            {
                bool isWebServiceContext = (HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_SPARKWS_CONTEXT] == "true");
                isAllowedRedirect = DestinationUrlSA.Instance.IsAllowedRedirectFromList(FrameworkGlobals.StripQuerystringFromURL(destinationUrl), isWebServiceContext);
            }

            if (isAllowedRedirect)
            {
                Transfer(destinationUrl);
            }
        }

        public string AppendEncryptedSessionID(string url)
        {
            if (url.IndexOf(WebConstants.URL_PARAMETER_NAME_SPARKWS_CYPHER_OUT) > -1)
            {
                return url;
            }

            //append encrypted session.
            if (_Session != null)
            {
                string encryptedSessionID = Crypto.Encrypt(WebConstants.SPARK_WS_CRYPT_KEY, _Session.Key.ToString());
                if (url.IndexOf("?") == url.Length - 1)
                    url += WebConstants.URL_PARAMETER_NAME_SPARKWS_CYPHER_OUT + "=" + HttpUtility.UrlEncode(encryptedSessionID);
                else if (url.IndexOf("?") > 1)
                    url += "&" + WebConstants.URL_PARAMETER_NAME_SPARKWS_CYPHER_OUT + "=" + HttpUtility.UrlEncode(encryptedSessionID);
                else
                    url += "?" + WebConstants.URL_PARAMETER_NAME_SPARKWS_CYPHER_OUT + "=" + HttpUtility.UrlEncode(encryptedSessionID);

            }

            return url;
        }

        /// <summary>
        /// Returns the encrypted session ID.  Primary purpose is for Connect framwork interaction.
        /// </summary>
        /// <param name="urlEncode"></param>
        /// <returns></returns>
        public string GetEncryptedSessionID(bool urlEncode)
        {
            string ret = string.Empty;

            if (_Session != null)
                ret = Crypto.Encrypt(WebConstants.SPARK_WS_CRYPT_KEY, _Session.Key.ToString());

            if (urlEncode)
                ret = HttpUtility.UrlEncode(ret);

            return ret;
        }

        public string RetrieveQSValueFromDestinationURL(string queryStringParamNam)
        {
            string retVal = string.Empty;
            string destinationURL = GetDestinationURL().ToLower();

            if (destinationURL != string.Empty && destinationURL.IndexOf(queryStringParamNam) != -1)
            {
                // Retrieve everything from the queryStringParamNam on.
                retVal = destinationURL.Substring(destinationURL.IndexOf(queryStringParamNam) + queryStringParamNam.Length + 1);

                // Remove everything after the queryStringParamNam.
                int ampIndex = retVal.IndexOf('&');
                if (ampIndex != -1)
                    retVal = retVal.Substring(0, ampIndex - 1);
            }

            return retVal;
        }

        public string GetDestinationURL()
        {
            return GetDestinationURL(false);
        }

        private Boolean IsKeywordSearchUrl(String url)
        {
            if (String.IsNullOrEmpty(url)) return false;

            return url.ToLower().Contains(Search.Constants.Literals.KeywordSearchResults);
        }

        private String GetKeywordSearchUrl(HttpRequest request)
        {
            return String.Join(
                String.Empty,
                new[]
				{
					request.Params[Search.Constants.Parameters.DestinationUrl],
					Search.Constants.Parameters.SeekerKey,
					request.Params[Search.Constants.Parameters.Seeker],
					Search.Constants.Parameters.SeekingKey,
					request.Params[Search.Constants.Parameters.Seeking],
					Search.Constants.Parameters.AgeKey, 
					request.Params[Search.Constants.Parameters.Age],
					Search.Constants.Parameters.LocationKey,
					request.Params[Search.Constants.Parameters.Location],
					Search.Constants.Parameters.ResultKey,
					request.Params[Search.Constants.Parameters.ResultType],
				}
            );
        }

        public string GetDestinationURL(Boolean appendEncryptedSessionIDFlag)
        {
            string retVal = GetValueByName("DestinationURL");

            if (appendEncryptedSessionIDFlag)
            {
                retVal = AppendEncryptedSessionID(retVal);
            }

            return retVal;
        }

        private string GetValueByName(string name)
        {
            if (IsKeywordSearchUrl(HttpContext.Current.Request.Params[Search.Constants.Parameters.DestinationUrl]))
            {
                return GetKeywordSearchUrl(HttpContext.Current.Request);
            }

            var s = String.Empty;

            if (HttpContext.Current.Request.QueryString[name] != null)
                s = HttpContext.Current.Request.QueryString[name];
            else if (HttpContext.Current.Request.Form[name] != null)
                s = HttpContext.Current.Request.Form[name];

            return s;
        }

        private void ParseDestinationURL()
        {
            string destinationURL = String.Empty;
            string[] nameValuePairs;
            string[] pair;
            int pairNum;
            string pairName;
            string pairValue;
            double pairValueNum;
            int pos;
            string query;

            destinationURL = GetDestinationURL();

            try
            {
                if (destinationURL.Length > 0)
                {
                    Uri uri = new Uri("http://foo" + destinationURL);

                    if (uri.AbsolutePath.ToLower() != "/default.aspx")
                    {
                        //
                        // current framework page

                        // Did we find the IM page?
                        if (uri.AbsolutePath.IndexOf("InstantMessenger") != -1)
                        {
                            DestinationPage = Pages.PAGE_INSTANT_MESSENGER_MAIN;
                        }
                    }

                    query = uri.Query;
                    pos = query.IndexOf("?");
                    if (pos > -1)
                    {
                        query = query.Substring(pos + 1);
                    }

                    nameValuePairs = query.Split('&');

                    for (pairNum = 0; pairNum < nameValuePairs.Length; pairNum++)
                    {
                        pair = nameValuePairs[pairNum].Split('=');

                        if (pair.Length == 2)
                        {
                            pairName = pair[0];
                            pairValue = pair[1];

                            // specific logic for decoding recipientmemberid request parameter for IMs
                            if (pairName.ToLower().Equals("recipientmemberid"))
                            {
                                pairValue = decodeIMRecipientMemberID(pairValue);
                            }

                            if (double.TryParse(pairValue, System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out pairValueNum))
                            {
                                try
                                {
                                    switch (pairName.ToLower())
                                    {
                                        // TODO: having a p and an a is perfectly valid. this seems
                                        //       to pick just one of them for the destination page.
                                        //       Should there be a DestinationAction as well?
                                        case "p":
                                        case "a":
                                            DestinationPage = (Pages)pairValueNum;
                                            break;
                                        case "memberid":
                                        case "recipientmemberid":
                                        case "destinationmemberid":
                                        case "tomemberid":
                                            RecipientMemberID = (int)pairValueNum;
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Trace.WriteLine("ContextGlobal.ParseDestinationURL " + ex);
                                }
                            }
                        }
                    }
                }
            }
            catch { }
        }

        private string decodeIMRecipientMemberID(string encodedMemberID)
        {
            string decoded = Crypto.Decrypt(WebConstants.HTTPIM_ENCRYPT_KEY, encodedMemberID);
            string[] values = decoded.Split(",".ToCharArray(), 3);

            return values[0];
        }

        public string UTFToHebrew(string strUTF)
        {
            Encoding encHebrew = Encoding.GetEncoding(1255); //hebrew encoding
            Encoding encUnicode = Encoding.GetEncoding("utf-16");

            byte[] bufferUTF = encUnicode.GetBytes(strUTF);
            byte[] bufferHebrew = Encoding.Convert(encUnicode, encHebrew, bufferUTF);
            return Encoding.UTF7.GetString(bufferHebrew);
        }


        #region VerifyEmail stuff

        static char[] hexDigits = {
									  '0', '1', '2', '3', '4', '5', '6', '7',
									  '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

        /// <summary>
        /// Hash a memberID for email verification purposes
        /// 
        /// TODO - Perhaps put it in Matchnet.SharedLib? -dcornell
        /// </summary>
        /// <param name="memberID">memberID to hash</param>
        /// <returns>hashed memberID</returns>
        private static string HashMemberID(int memberID)
        {
            string mENCRYPT_KEY = "kdfajdsf";

            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(Encoding.Unicode.GetBytes(mENCRYPT_KEY + memberID.ToString()));
            string retVal = ToHexString(result) + memberID;

            return (retVal);
        }


        public static string HashString(string sIn, int memberID)
        {
            string mENCRYPT_KEY = "kdfajdsf";

            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(System.Text.Encoding.Unicode.GetBytes(mENCRYPT_KEY + sIn));
            string retVal = ToHexString(result) + memberID.ToString();

            return (retVal);
        }

        private static string ToHexString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length * 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                int b = bytes[i];
                chars[i * 2] = hexDigits[b >> 4];
                chars[i * 2 + 1] = hexDigits[b & 0xF];
            }
            return new string(chars);
        }

        public bool VerifyMemberIDHash(string hashedData, int memberID)
        {
            bool retVal = false;

            string newHash = HashMemberID(memberID);
            if (newHash.Equals(hashedData))
            {
                retVal = true;
            }

            return (retVal);
        }


        public bool VerifyMemberHashCode(string hashedData, int memberID, string emailAddress)
        {
            bool retVal = false;
            if (EmailVerify.HashCodeGenerationMask == EmailVerifyHelper.HashCodeMask.fromEmailAddress)
            {
                string newHash = HashString(emailAddress, memberID);
                if (newHash.Equals(hashedData))
                {
                    retVal = true;
                }
            }
            else
            { retVal = VerifyMemberIDHash(hashedData, memberID); }

            return (retVal);
        }
        #endregion

        public System.Web.UI.Page Page
        {
            get
            {
                return _Page;
            }
            set
            {
                _Page = value;
            }
        }


        public int ProcessException(Exception ex)
        {
            return _ContextExceptions.LogException(ex);
        }

        public int ProcessException(Exception ex, System.Diagnostics.EventLogEntryType eventLogEntryType)
        {
            return _ContextExceptions.LogException(ex, eventLogEntryType);
        }

        public void Transfer(string fullUrl)
        {
            //Applications.PremiumServices.VelocityHandler.DebugTrace("COntextGlobal Transfer", "URL=" + fullUrl, this);
            Transfer(fullUrl, String.Empty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullUrl"></param>
        /// <param name="statusCode">Empty status code defaults to using 302 which is used by Response.Redirect by default.</param>
        public void Transfer(string fullUrl, string statusCode)
        {
            try
            {
                string link;

                if ((Brand.Site.SiteID == (int)WebConstants.SITE_ID.BlackSingles)
                    && (fullUrl.IndexOf("Registration.aspx") > -1))
                {
                    link = LinkFactory.Instance.GetLink(fullUrl, new Matchnet.Content.ServiceAdapters.Links.ParameterCollection(), Brand, HttpContext.Current.Request.Url.ToString(), false,
                        false, LayoutTemplate.WideSimple);
                }
                else
                {
                    link = LinkFactory.Instance.GetLink(fullUrl, Brand, HttpContext.Current.Request.Url.ToString());
                }

                switch (statusCode)
                {
                    case "301":
                        HttpContext.Current.Response.Status = "301 Moved Permanently";
                        HttpContext.Current.Response.AddHeader("Location", link);
                        break;

                    default:
                        HttpContext.Current.Response.Redirect(link);
                        break;
                }
            }
            catch (ThreadAbortException)
            {
                // do nothing. what a beautiful use of exceptions!
                // but reviewing all the calls that expected this to be the last line of code running
                // is to lengthy
            }
        }


        public void SetBodyTagAttribute(string attribute, string[] attributeValues, ContextGlobal.Pages[] pages)
        {
            int i = 0;

            foreach (Pages p in pages)
            {
                if (DestinationPage == p)
                {
                    HtmlGenericControl BodyTag = (HtmlGenericControl)Page.FindControl("BodyTag");
                    if (BodyTag != null)
                    {
                        BodyTag.Attributes.Remove(attribute);
                        BodyTag.Attributes.Add(attribute, attributeValues[i]);
                    }
                    break;
                }
                i++;
            }
        }


        public bool ValidatePageView()
        {

            if (Brand.IsPayToReplySite)
            {
                // since this method is only used by the the hot list in the ResultList.ascx.cs, it is feature base permission.
                if (MemberPrivilegeAttr.IsPermitMember(this) || IsAdmin)
                {
                    return true;
                }

                if (!Brand.IsPaySite || Brand.BrandID == (int)WebConstants.BRAND_ID.JDateCoIL)
                {
                    // RegionLanguage region = new RegionLanguage();
                    // region.Populate(this.Member.GetAttributeInt(WebConstants.ATTRIBUTE_NAME_REGIONID), this.PrivateLabel.LanguageID);
                    RegionLanguage region = RegionSA.Instance.RetrievePopulatedHierarchy(this.Member.GetAttributeInt(this.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID), Brand.Site.LanguageID);
                    if (region.CountryRegionID != Brand.Site.DefaultRegionID)
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }

            return false;
        }


        /// <summary>
        /// Helper method to insert spaces into a given string.  
        /// This will prevent the page from displaying strangely because of user input that does not contain any spaces.
        /// </summary>
        /// <param name="inputString">The string to add spaces to.</param>
        /// <param name="MAX_CHARACTERS">The maximum number of characters that can occur between spaces.</param>
        /// <returns>The newly formatted string with spaces.</returns>
        public string InsertSpaces(string inputString, int MAX_CHARACTERS)
        {
            string retVal = string.Empty;

            int startIndex = 0; //Index of the first character of the current word in the input string
            int endIndex = inputString.IndexOf(" "); //Index of the space following the current word in the input string
            int wordLength = endIndex;
            int lineLength = 0;

            while (endIndex != -1)
            {
                int excess = 0;
                if (lineLength + wordLength > MAX_CHARACTERS)
                {
                    excess = lineLength + wordLength - MAX_CHARACTERS;
                    endIndex -= excess;
                    wordLength = endIndex - startIndex;
                }
                retVal += inputString.Substring(startIndex, endIndex - startIndex) + " ";
                lineLength = (lineLength + wordLength) % MAX_CHARACTERS;

                if (excess > 0)
                {
                    startIndex = endIndex;
                }
                else
                {
                    startIndex = endIndex + 1;
                }
                endIndex = inputString.IndexOf(" ", startIndex); //Index of the space following the current word in the input string
                wordLength = endIndex - startIndex;
            }

            endIndex = inputString.Length;
            wordLength = endIndex - startIndex;

            while (lineLength + wordLength > MAX_CHARACTERS)
            {
                int excess = lineLength + wordLength - MAX_CHARACTERS;
                endIndex -= excess;

                retVal += inputString.Substring(startIndex, endIndex - startIndex) + " ";
                lineLength = 0;
                startIndex = endIndex;
                endIndex = inputString.Length;
                wordLength = endIndex - startIndex;
            }

            if (startIndex < inputString.Length)
            {
                retVal += inputString.Substring(startIndex);
            }

            return retVal;
        }


        public void LogonRedirect(string pagePath)
        {
            LogonRedirect(pagePath, Constants.NULL_STRING);
        }


        public void LogonRedirect(string pagePath, string resourceConstant)
        {
            this.Transfer(GetLogonRedirect(pagePath, resourceConstant));
        }

        public string GetLogonRedirect(string pagePath, string resourceConstant)
        {
            string url;

            // Can and should the destination Url ever be the logon page?
            // Without this check, it can result a loop
            if (pagePath.ToLower().Contains("/Applications/Logon/Logon.aspx"))
            {
                pagePath = String.Empty;
            }

            if (Session["EmailAddress"].Length > 0)
            {
                url = "/Applications/Logon/Logon.aspx";
            }
            else if (HasLogonPageFlag(WebConstants.URL_PARAMETER_NAME_LOGON_PAGE_FLAG))
            {
                // TT16535 - detect ext mail communications for proper redirect to logon page
                url = "/Applications/Logon/Logon.aspx";
            }
            else if (pagePath.ToLower().Contains("home/default.aspx") && Convert.ToBoolean(RuntimeSettings.GetSetting("VISITORS_GET_LOGON_PAGE_WHEN_REQUESTING_HOMEPAGE", this.Brand.Site.Community.CommunityID, this.Brand.Site.SiteID, this.Brand.BrandID)))
            {
                url = "/Applications/Logon/Logon.aspx";
            }
            else
            { 
                // Capture these urls for Registration Reason ID.  These will be saved to the member's attributes if registration actually occurs
                RegistrationReason.AddToSession(this, RegistrationReason.KeysToAdd.Both);
                url = FrameworkGlobals.GetRegistrationPageURL(this);
            }

            if (resourceConstant != Constants.NULL_STRING)
                resourceConstant = "&rc=" + resourceConstant;
            else
                resourceConstant = string.Empty;

            return FrameworkGlobals.LinkHref(url +
                (url.Contains("?") ? "&" : "?")
                +
                "DestinationURL=" + System.Web.HttpUtility.UrlEncode(pagePath) + resourceConstant, true);
        }


        /// <summary>
        /// Called by any ext. mail destination pages that must check whether to redirect to logon.aspx or not
        /// </summary>
        /// <param name="flagParm">Typically WebConstants.URL_PARAMETER_NAME_LOGON_PAGE_FLAG</param>
        /// <returns></returns>
        public bool HasLogonPageFlag(string flagParm)
        {
            bool flagPresent = false;

            if (GetValueByName(flagParm) != string.Empty)
                flagPresent = true;

            return flagPresent;
        }


        /// <summary>
        /// Removes a specified parameter from the URL being passed in as a string, with the option of returning a substring of the
        /// original URL minus the named paramter and all that follows it.
        /// </summary>
        /// <param name="currURL">URL to be cleaned</param>
        /// <param name="paramToRemove">Parameter you wish to remove. Pass in string.Empty if you're planning on passing in true as the next parameter (remove all else).</param>
        /// <param name="removeAllElse">Do you wish to remove everything else from the URL, including the parameter passed in?</param>
        /// <returns>Returns the resulting URL as a string, a.k.a. the "clean URL".</returns>
        public string RemoveParamFromURL(string currURL, string paramToRemove, bool removeAllElse)
        {
            try
            {
                if (currURL.IndexOf("?") != -1)
                {
                    string baseURL = currURL.Substring(0, currURL.IndexOf("?") + 1);
                    string paramsStr = currURL.Remove(0, currURL.LastIndexOf("?") + 1);

                    // Create a NameValueCollection from the params stripped out of the URL
                    NameValueCollection paramsNVC = NameValueCollection(paramsStr);

                    // Remove all occurrences of the paramToRemove
                    for (int i = 0; i < paramsNVC.Count; i++)
                    {
                        if (Convert.ToString(paramsNVC.GetKey(i)) == paramToRemove)
                        {
                            paramsNVC.Remove(Convert.ToString(paramsNVC.GetKey(i)));
                        }
                    }

                    // convert whats left of the NameValueCollection back to a string
                    paramsStr = ConvertNVCToString(paramsNVC);

                    // reassemble url without the param requested to be removed (and it's value)
                    currURL = baseURL + paramsStr;

                    // Check if we need to remove all params, including the '?'
                    if (removeAllElse || paramsStr == null || paramsStr == string.Empty || paramsStr == "")
                    {
                        currURL = currURL.Substring(0, currURL.IndexOf("?"));
                    }
                }
            }
            catch (Exception ex)
            {
                // Trivial error handling, which is more than can be said of all other catch statements in ContextGlobal
                System.Diagnostics.Trace.WriteLine("RemoveParamFromURL threw an error: " + ex.Message);

                // The usual process exception so we can see it in sparkmon.
                ProcessException(ex);
            }
            return currURL;
        }


        /// <summary>
        /// Utility method: convert passed in string (ideally a query string) to NameValueCollection. Used by RemoveParamFromURL
        /// </summary>
        private static NameValueCollection NameValueCollection(string qs)
        {
            NameValueCollection nvc = new NameValueCollection();

            //strip string data before the question mark
            qs = qs.IndexOf('?') > 0 ? qs.Remove(0, qs.IndexOf('?')) : qs;
            Array sqarr = qs.Split("&".ToCharArray());
            for (int i = 0; i < sqarr.Length; i++)
            {
                if (sqarr.GetValue(i).ToString().IndexOf("=") != -1)
                {
                    string[] pairs = sqarr.GetValue(i).ToString().Split("=".ToCharArray());
                    nvc.Add(pairs[0], pairs[1]);
                }
            }
            return nvc;
        }


        public string getEncryptedMemberID()
        {
            string encMemberId = "";
            string key = "";
            try
            {
                if (Member != null && Member.MemberID != Constants.NULL_INT)
                {
                    key = RuntimeSettings.GetSetting("KEY");
                    encMemberId = Crypto.Encrypt(key, Member.MemberID.ToString());
                    encMemberId = HttpUtility.UrlEncode(encMemberId);
                }
                return encMemberId;
            }
            catch (Exception ex)
            {
                ProcessException(ex);
                return encMemberId;
            }
        }
        /// <summary>
        /// Utility method: convert passed in NameValueCollection to a QueryString string. Used by RemoveParamFromURL
        /// </summary>
        private static string ConvertNVCToString(NameValueCollection myCol)
        {
            StringBuilder sb = new StringBuilder();
            string nvcString = string.Empty;

            for (int i = 0; i < myCol.Count; i++)
            {
                sb.Append(Convert.ToString(myCol.GetKey(i)));
                sb.Append("=");
                sb.Append(Convert.ToString(myCol.Get(i)));

                if (i != myCol.Count - 1)
                {
                    sb.Append("&");
                }
            }

            nvcString = sb.ToString();
            return nvcString;
        }


        public string GetReferrerUrl()
        {
            Uri referrer = HttpContext.Current.Request.UrlReferrer;
            if (referrer != null)
            {
                return referrer.ToString();
            }

            return string.Empty;
        }

        //Created by Michael Conway in order to accomidate Toms request for a public GetEntryPoint method in ContextGlobal
        //during E-cards implimentation
        public BreadCrumbHelper.EntryPoint GetEntryPoint(int pEntryPoint)
        {
            return BreadCrumbHelper.GetEntryPoint(pEntryPoint);
        }

        public BreadCrumbHelper.EntryPoint GetEntryPoint(HttpRequest request)
        {
            return BreadCrumbHelper.GetEntryPoint(Conversion.CInt(request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT]));
        }



        public bool PromotionalProfileEnabled()
        {
            try
            {
                bool flag = false;
                //check site config
                string promotionFlag = RuntimeSettings.GetSetting("PROMOTION_MEMBER_FLAG", Brand.Site.Community.CommunityID, Brand.Site.SiteID);
                string genderPref = RuntimeSettings.GetSetting("PROMOTION_MEMBER_GENDER_MASK_LIST", Brand.Site.Community.CommunityID, Brand.Site.SiteID);

                if (genderPref == null || genderPref == String.Empty)
                    return false;
                string[] sGenderPref = genderPref.Split(',');

                bool bFlag = Conversion.CBool(promotionFlag, false);

                int genderMask = Conversion.CInt(SearchPreferences["GenderMask"], Constants.NULL_INT);
                //int gender=GenderUtils.GetGenderFromGenderSeeking(genderMask);

                bool genderFlag = false;
                for (int i = 0; i < sGenderPref.Length; i++)
                {
                    int mask = Conversion.CInt(sGenderPref[i], Constants.NULL_INT);
                    if (mask == genderMask)
                    { genderFlag = true; break; }
                }
                if (genderFlag)
                {
                    string promotionalMemberId = GetResource("PROMOTION_MEMBER_ID");
                    int iMemberID = Conversion.CInt(promotionalMemberId, Constants.NULL_INT);
                    if (iMemberID != Constants.NULL_INT)
                    {
                        flag = true;
                        flag = flag && bFlag;
                    }
                }
                return flag;

            }
            catch (Exception ex)
            { return false; }

        }

        public int PromotionalProfileMember()
        {
            try
            {
                string promotionalMemberId = GetResource("PROMOTION_MEMBER_ID");
                int iMemberID = Conversion.CInt(promotionalMemberId, Constants.NULL_INT);
                return iMemberID;

            }
            catch (Exception ex)
            { return Constants.NULL_INT; }

        }
        #region public properties

        public bool NextProfileUrlGenerated { get; set; }


        public string NextProfileUrl { get; set; }

        public bool PreviousProfileUrlGenerated { get; set; }


        public string PreviousProfileUrl { get; set; }

        public bool SilentCheckIM { get; set; }


        public Brand Brand
        {
            get
            {
                // It makes more sense to use just one Brand instead of having Brand and TargetBrand
                // since the latter already defaulted to Brand.
                if ((IsAdmin || AdminImpersonate) && Member != null && _ImpersonateContext)
                {
                    if (_ImpersonateBrand != null)
                    {
                        // If changing to a brand that has a different culture, use the different culture.
                        if (Thread.CurrentThread.CurrentCulture.Name != _ImpersonateBrand.Site.CultureInfo.Name)
                        {
                            Thread.CurrentThread.CurrentCulture = _ImpersonateBrand.Site.CultureInfo;
                        }

                        return _ImpersonateBrand;
                    }
                }

                // Switch back to original culture if necessary
                if (Thread.CurrentThread.CurrentCulture.Name != _Brand.Site.CultureInfo.Name)
                {
                    Thread.CurrentThread.CurrentCulture = _Brand.Site.CultureInfo;
                }

                return _Brand;
            }
            set
            {	// Certain circumstances require forcing the current brand, not based on URL
                // but on last logon date of member (required for JD to JDIL Instant Messenger communication)
                // see 16402, 16392, 16298, 15972, 14572
                _Brand = value;
                Thread.CurrentThread.CurrentCulture = _Brand.Site.CultureInfo;
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentUICulture;
            }
        }


        public BrandAlias BrandAlias
        {
            get
            {
                return _BrandAlias;
            }
        }


        public String EncryptedMemberID
        {
            get
            {
                try
                {
                    if (Session.GetString(WebConstants.SESSION_PROPERTY_NAME_ENC_MEMBERID, "") == String.Empty)
                    {
                        if (Member != null)
                        {
                            _encryptedMemberID = Crypto.Encrypt(WebConstants.SPARK_WS_CRYPT_KEY,
                                Member.MemberID.ToString());
                            Session.Add(WebConstants.SESSION_PROPERTY_NAME_ENC_MEMBERID,
                                _encryptedMemberID, SessionPropertyLifetime.Temporary);
                        }
                    }
                    else
                    {
                        _encryptedMemberID = Session.GetString(WebConstants.SESSION_PROPERTY_NAME_ENC_MEMBERID);
                    }

                    return _encryptedMemberID;
                }
                catch (Exception ex)
                { ProcessException(ex); return _encryptedMemberID; }
            }

        }

        /// <summary>
        /// Order of how brand is loaded
        /// 
        /// 1. Load by BrandID (checks for "brandid" in querystring)
        /// 2. Load by PLID to support legacy referrals (checks for "plid" in querystring)
        /// 3. Load by URI 
        /// </summary>
        private void InitializeBrand()
        {
            _Brand = null;
            _BrandAlias = null;
            int sessionBrandID = Constants.NULL_INT; // BrandID stored in session
            HttpContext context = HttpContext.Current;
            bool shouldRedirectFromBrandAlias = false;

            // Loading from querystring key
            if (context != null)
            {
                #region 1. Load by Session
                sessionBrandID = _Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_BRANDID, Constants.NULL_INT);

                if (sessionBrandID != Constants.NULL_INT)
                {
                    _Brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrands().GetBrand(sessionBrandID);
                }

                if (_Brand != null)
                {
                    _UseBrandAliasURI = true;
                }
                #endregion

                #region 2. Load by BrandID or PLID or DomainAliasID
                int plid = Constants.NULL_INT;

                plid = Conversion.CInt(context.Request["BrandID"], Constants.NULL_INT);

                if (plid == Constants.NULL_INT)
                {
                    plid = Conversion.CInt(context.Request[WebConstants.URL_PARAMETER_NAME_DOMAINALIASID], Constants.NULL_INT);
                }

                if ((plid == Constants.NULL_INT) && (HttpContext.Current.Request[WebConstants.IMPERSONATE_BRANDID] == null))
                {
                    plid = Conversion.CInt(context.Request[WebConstants.URL_PARAMETER_NAME_BRANDALIASID], Constants.NULL_INT);
                }

                if (plid != Constants.NULL_INT)
                {
                    _Brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrands().GetBrand(plid);

                    if (_Brand == null)
                    {
                        _BrandAlias = BrandConfigSA.Instance.GetBrands().GetBrandAlias(plid);

                        if (_BrandAlias != null)
                        {
                            _Brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrands().GetBrand(_BrandAlias.BrandID);
                        }
                    }
                }
                #endregion
            }

            #region 3. Load by URI
            if (_Brand == null)
            {
                string uri = String.Empty;

                if (context != null)
                {
                    uri = context.Request.Url.Host;
                }
                else
                {
                    uri = new Uri(_DesignTimeURL).Host;
                }

                _Brand = BrandConfigSA.Instance.GetBrands().GetBrand(uri);

                // BrandAlias URI
                if (_Brand == null)
                {
                    _BrandAlias = BrandConfigSA.Instance.GetBrands().GetBrandAlias(uri);

                    if (_BrandAlias != null)
                    {
                        _Brand = BrandConfigSA.Instance.GetBrands().GetBrand(_BrandAlias.BrandID);

                        _UseBrandAliasURI = true;
                        shouldRedirectFromBrandAlias = true;
                    }
                }
            }
            #endregion

            // If no brand is found use AS brand as default(ported from content svc)
            if (_Brand == null)
            {
                _Brand = BrandConfigSA.Instance.GetBrands().GetBrand(1001);
            }

            // Add new brandID to session
            if (sessionBrandID == Constants.NULL_INT || sessionBrandID != _Brand.BrandID)
            {
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_BRANDID, _Brand.BrandID, SessionPropertyLifetime.TemporaryDurable);
            }


            // This is to prevent redirecting to a brand's URI we don't own
            if ((_Brand.StatusMask & (int)StatusType.CoBrand) ==
                (int)StatusType.CoBrand)
            {
                return;
            }

            double dbl;

            // Ignore same uri, ignore IP, ignore healthcheck
            if (shouldRedirectFromBrandAlias || (!context.Request.Url.Host.ToLower().EndsWith(_Brand.Uri.ToLower()) &&
                !double.TryParse(context.Request.Url.Host.Replace(".", ""), System.Globalization.NumberStyles.Number, null, out dbl) &&
                !HttpContext.Current.Request.RawUrl.ToLower().Equals("/applications/health/healthcheck.aspx")))
            {
                StringBuilder sbUri = new StringBuilder("http://" + _Brand.Site.DefaultHost + "." + _Brand.Uri + context.Request.RawUrl);

                //If we are viewing a BrandAlias, include the BrandAliasID in the uri so the BrandAlias doesn't get lost in the redirect:
                if (_BrandAlias != null)
                {
                    sbUri.Append(context.Request.RawUrl.IndexOf("?") < 0 ? "?" : "&");
                    sbUri.Append(WebConstants.URL_PARAMETER_NAME_BRANDALIASID + "=" + _BrandAlias.BrandAliasID);
                    if (!string.IsNullOrEmpty(BrandAlias.RedirectParameters))
                    {
                        sbUri.Append("&" + BrandAlias.RedirectParameters);
                    }
                }

                Transfer(sbUri.ToString());
            }
        }


        private void InitializeBrandAlias()
        {
            // This could already be set in InitializeBrand
            if (_BrandAlias != null)
            {
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_BRANDALIASID, _BrandAlias.BrandAliasID.ToString(),
                    SessionPropertyLifetime.TemporaryDurable);

                return;
            }

            //First check "plid" parameter
            int brandAliasID = Conversion.CInt(HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_BRANDALIASID]);

            //If that doesn't work, check DomainAliasID
            if (brandAliasID == Constants.NULL_INT)
            {
                brandAliasID = Conversion.CInt(HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_DOMAINALIASID]);
            }

            //If no BrandAliasID was specified in the URL, retrieve existing BrandAliasID from the session.
            if (brandAliasID == Constants.NULL_INT)
            {
                brandAliasID = Conversion.CInt(Session[WebConstants.SESSION_PROPERTY_NAME_BRANDALIASID]);
            }

            if (brandAliasID != Constants.NULL_INT)
            {
                _BrandAlias = BrandConfigSA.Instance.GetBrands().GetBrandAlias(brandAliasID);

                if (_BrandAlias != null)
                {
                    Session.Add(WebConstants.SESSION_PROPERTY_NAME_BRANDALIASID, brandAliasID.ToString(), SessionPropertyLifetime.TemporaryDurable);
                }
            }
        }


        private void InitializeExpansionTokens()
        {
            try
            {


                _expansionTokens.Add("SITEURL", "http://" + HttpContext.Current.Request.Url.Host);
                // bedrock pages should no longer be served by https
                _expansionTokens.Add("SITEURLSSL", "http://" + HttpContext.Current.Request.Url.Host);
                _expansionTokens.Add("PLDOMAIN", Brand.Site.Name);
                _expansionTokens.Add("PLSERVERNAME", Brand.Site.DefaultHost);
                _expansionTokens.Add("YEAR", System.DateTime.Now.Year.ToString());
                _expansionTokens.Add("PLPHONENUMBER", Brand.PhoneNumber);
                string subscribeLink = LinkFactory.Instance.GetLink("/Applications/Subscription/Subscribe.aspx?", Brand, HttpContext.Current.Request.Url.ToString());
                _expansionTokens.Add("PLSECURELINK", subscribeLink);
                _expansionTokens.Add("PLSECURECREDITLINK", subscribeLink);
                _expansionTokens.Add("PLSECURECHECKLINK", subscribeLink);
                _expansionTokens.Add("CSSUPPORTPHONENUMBER", this.GetResource("CS_SUPPORT_PHONE_NUMBER"));

                string hurryDateUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("HD_LANDINGPAGE_URL", Brand.Site.Community.CommunityID, Brand.Site.SiteID);
                _expansionTokens.Add("HURRYDATELANDINGPAGEURL", hurryDateUrl);
                //adding encrypted memberurlid for HurryDate, we already have 5 pages using this token so it makes sense to initialize it in 1 place instead of 5
                string memberid = "";
                if (Member != null)
                { memberid = Member.MemberID.ToString(); }
                _expansionTokens.Add("MEMBERIDURLPARAM", memberid);

                string siteAlias = RuntimeSettings.GetSetting("SITE_NAME_ALIAS", Brand.Site.Community.CommunityID, Brand.Site.SiteID, Brand.BrandID);
                if (String.IsNullOrEmpty(siteAlias))
                {
                    siteAlias = Brand.Site.Name;

                }
                _expansionTokens.Add("BRANDNAME", siteAlias);

            }
            catch (Exception ex)
            {
                ProcessException(ex);
            }
        }


        public Matchnet.Session.ValueObjects.UserSession Session
        {
            get
            {
                return _Session;
            }
        }


        public void StartLogonSession(int memberID, int domainID, int privateLabelID, string emailAddress, string userName, bool IsNewRegistration, int adminMemberID)
        {
            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID,
                MemberLoadFlags.None);

            this.Session.Add("MemberID", memberID.ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
            this.Session.Add("EmailAddress", emailAddress, SessionPropertyLifetime.Persistent);
            this.Session.Add("GenderMask", Convert.ToString(member.GetAttributeInt(this.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK)), SessionPropertyLifetime.Temporary);
            this.Session.Add("HideMask", member.GetAttributeInt(this.Brand, "HideMask"), SessionPropertyLifetime.Temporary);
            this.Session.Add("SelfSuspendedFlag", member.GetAttributeBool(this.Brand, "SelfSuspendedFlag") ? "1" : "0", SessionPropertyLifetime.Temporary);
            this.Session.Add("IsPayingMemberFlag", MemberPrivilegeAttr.IsCureentSubscribedMember(this) ? "1" : "0", SessionPropertyLifetime.Temporary);
            this.Session.Add(WebConstants.ATTRIBUTE_NAME_MOLREGIONID, String.Empty, SessionPropertyLifetime.Temporary);

            // if this paramter is passed in, that means this is a stealth logon
            if (adminMemberID != Constants.NULL_INT)
                this.Session.Add(WebConstants.SESSION_STEALTH_LOGON_ADMIN_MEMBER_ID, adminMemberID.ToString(), SessionPropertyLifetime.Temporary);
        }


        private void InitializeSession()
        {

            if (!IsApiSession())
                _Session = SessionSA.Instance.GetSession(IsDevMode);
            else
            {
                _Session = GetSessionFromID(HttpContext.Current);
            }



            HttpContext context = HttpContext.Current;

            SetPromotionSession();
        }


        /// <summary>
        /// Saves the search preferences in the g.SearchPreferences collection.
        /// For members this will utilize the SearchPreferencesSA
        /// For visitors, search preferences will be stored in the Session.
        /// </summary>
        public void SaveSearchPreferences()
        {
            if ((Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) > 0) && (_Session.GetString(VisitorLimitHelper.SESSIONKEY_VISITORLIMIT_DONOTOVERWRITESEARCHPREFS, String.Empty) != "true"))
            {
                // Persist a member's search preferences via the Search service.
                SearchPreferencesSA.Instance.Save(
                    Member.MemberID,
                    Brand.Site.Community.CommunityID,
                    SearchPreferences
                );

                RemoveCachedMatchResults();
            }
            else
            {
                // Store a non-members search preferences in session.
                Session.Add(
                    SearchPreferencesKey.GetCacheKey(Session.Key.ToString()),
                    SearchPreferences,
                    Matchnet.Session.ValueObjects.SessionPropertyLifetime.TemporaryDurable
                );
            }
        }

        public void RemoveCachedMatchResults()
        {
            //remove your matches from cache if prefs are changed
            try
            {
                Cache.Remove(Matchnet.Web.Framework.Ui.BasicElements.ResultListHandler.GetYourMatchesCacheKey(this));
            }
            catch (Exception ignore) { }
        }

        /// <summary>
        /// Saves the search preferences in the g.SearchPreferences collection.
        /// For members this will utilize the SearchPreferencesSA
        /// For visitors, search preferences will be stored in the Session.
        /// </summary>
        //        public void SaveQuickSearchPreferences()
        //        {
        //            // Store a non-members search preferences in session.
        //            this.Session.Add(SearchPreferencesKey.GetCacheKey(this.Session.Key.ToString())
        //                , this.QuickSearchPreferences
        //                , Matchnet.Session.ValueObjects.SessionPropertyLifetime.TemporaryDurable);
        //        }

        private void SetPromotionSession()
        {
            HttpContext current = HttpContext.Current;

            HttpCookie landingPageTransferCookie = current.Request.Cookies["landingPageTransfer"];
            HttpCookie registrationTransferCookie = current.Request.Cookies["registrationTransfer"];

            bool newPRM = false;

            if (current == null) return;

            // Check for BrandAliasID(Former PLID)
            int val = Conversion.CInt(current.Request[WebConstants.URL_PARAMETER_NAME_DOMAINALIASID], Constants.NULL_INT);

            if (val != Constants.NULL_INT)
            {
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_DOMAINALIASID, Convert.ToString(val), SessionPropertyLifetime.TemporaryDurable);
            }

            // Check for PromotionID
            val = Conversion.CInt(current.Request[WebConstants.URL_PARAMETER_NAME_PROMOTIONID], Constants.NULL_INT);
            if (val != Constants.NULL_INT)
            {
                // If there's a new PRM, then BID, LPID, LGID(Luggage) session values must be overwritten accordingly. TT #20961.
                newPRM = true;

                //_Session.Add(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, Convert.ToString(val), SessionPropertyLifetime.Persistent);
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, Convert.ToString(val), 3, false);
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_CHECKPROMOTIONFLAG, "true", SessionPropertyLifetime.Temporary);
            }

            // Same as PromotionID
            val = Conversion.CInt(current.Request[WebConstants.URL_PARAMETER_NAME_PRM], Constants.NULL_INT);
            if (val == Constants.NULL_INT && landingPageTransferCookie != null)
            {
                //need to check the cookie coming from the landing page ending
                val = Conversion.CInt(landingPageTransferCookie.Values[WebConstants.URL_PARAMETER_NAME_PRM], Constants.NULL_INT);
            }

            if (val == Constants.NULL_INT && registrationTransferCookie != null)
            {
                //need to check the cookie coming from the new registration site
                val = Conversion.CInt(registrationTransferCookie.Values[WebConstants.URL_PARAMETER_NAME_PRM], Constants.NULL_INT);
            }

            // For cases in which we have prm after a friendly URL
            if (val == Constants.NULL_INT)
            {
                if (current.Request.Url.AbsoluteUri.ToUpper().Contains(WebConstants.URL_PARAMETER_NAME_PRM))
                {
                    string[] arr =
                        current.Request.Url.AbsoluteUri.ToUpper().Split(
                            new string[] { WebConstants.URL_PARAMETER_NAME_PRM + "=" }, StringSplitOptions.None);

                    if (arr.Length > 1)
                        val = Conversion.CInt(arr[1]);
                }
            }

            if (val != Constants.NULL_INT)
            {
                newPRM = true;

                //_Session.Add(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, Convert.ToString(val), SessionPropertyLifetime.Persistent);
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, Convert.ToString(val), 3, false);
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_CHECKPROMOTIONFLAG, "true", SessionPropertyLifetime.Temporary);
            }

            // Check for LandingPageID
            val = Conversion.CInt(current.Request[WebConstants.URL_PARAMETER_NAME_LANDINGPAGEID], Constants.NULL_INT);
            if (val == Constants.NULL_INT && landingPageTransferCookie != null)
            {
                //need to check the cookie coming from the landing page ending
                val = Conversion.CInt(landingPageTransferCookie.Values[WebConstants.URL_PARAMETER_NAME_LANDINGPAGEID], Constants.NULL_INT);
            }

            if (val == Constants.NULL_INT && registrationTransferCookie != null)
            {
                //need to check the cookie coming from the landing page ending
                val = Conversion.CInt(registrationTransferCookie.Values[WebConstants.URL_PARAMETER_NAME_LANDINGPAGEID], Constants.NULL_INT);
            }


            if (newPRM)
            {
                _Session.Remove(WebConstants.SESSION_PROPERTY_NAME_LANDINGPAGEID);
            }

            if (val != Constants.NULL_INT)
            {
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_LANDINGPAGEID, Convert.ToString(val), SessionPropertyLifetime.Persistent);
            }

            // Check for BannerID(BID)
            val = Matchnet.Conversion.CInt(current.Request[WebConstants.URL_PARAMETER_NAME_BANNERID], Constants.NULL_INT);
            if (val == Constants.NULL_INT && landingPageTransferCookie != null)
            {
                //need to check the cookie coming from the landing page ending
                val = Conversion.CInt(landingPageTransferCookie.Values[WebConstants.URL_PARAMETER_NAME_BANNERID], Constants.NULL_INT);
            }

            if (val == Constants.NULL_INT && registrationTransferCookie != null)
            {
                //need to check the cookie coming from the landing page ending
                val = Conversion.CInt(registrationTransferCookie.Values[WebConstants.URL_PARAMETER_NAME_BANNERID], Constants.NULL_INT);
            }

            if (newPRM)
            {
                _Session.Remove(WebConstants.SESSION_PROPERTY_NAME_BANNERID);
            }

            if (val != Constants.NULL_INT)
            {
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_BANNERID, Convert.ToString(val), SessionPropertyLifetime.Persistent);
            }

            // Check for Luggage
            string luggage = current.Request[WebConstants.URL_PARAMETER_NAME_LUGGAGE];

            if (newPRM)
            {
                _Session.Remove(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE);
            }

            if (!string.IsNullOrEmpty(luggage))
            {
                //_Session.Add(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE, luggage, SessionPropertyLifetime.Persistent);
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE, luggage, 3, false);
            }

            // Check for Luggage(LGID)
            luggage = current.Request[WebConstants.URL_PARAMETER_NAME_LGID];
            if (string.IsNullOrEmpty(luggage) && landingPageTransferCookie != null)
            {
                //need to check the cookie coming from the landing page ending
                luggage = landingPageTransferCookie.Values[WebConstants.URL_PARAMETER_NAME_LGID];
            }

            if (string.IsNullOrEmpty(luggage) && registrationTransferCookie != null)
            {
                luggage = registrationTransferCookie.Values[WebConstants.URL_PARAMETER_NAME_LGID];
            }

            if (newPRM)
            {
                _Session.Remove(WebConstants.URL_PARAMETER_NAME_LGID);
            }

            if (!string.IsNullOrEmpty(luggage))
            {
                //_Session.Add(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE, luggage, SessionPropertyLifetime.Persistent);
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE, luggage, 3, false);
            }

            // Commission junction luggage
            luggage = current.Request[WebConstants.URL_PARAMETER_NAME_AID] + " " + current.Request[WebConstants.URL_PARAMETER_NAME_PID] + " " + current.Request[WebConstants.URL_PARAMETER_NAME_SID];
            if (luggage.Length > 2)
            {
                //_Session.Add(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE, luggage, SessionPropertyLifetime.Persistent);
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE, luggage, 3, false);
            }

            // Check for Refcd
            string refcd = String.Empty;
            refcd = current.Request[WebConstants.URL_PARAMETER_NAME_REFCD];
            if (string.IsNullOrEmpty(refcd) && landingPageTransferCookie != null)
            {
                //need to check the cookie coming from the landing page ending
                refcd = landingPageTransferCookie.Values[WebConstants.URL_PARAMETER_NAME_REFCD];
            }

            if (string.IsNullOrEmpty(refcd) && registrationTransferCookie != null)
            {
                //need to check the cookie coming from the landing page ending
                refcd = registrationTransferCookie.Values[WebConstants.URL_PARAMETER_NAME_REFCD];
            }

            if (newPRM)
            {
                _Session.Remove(WebConstants.SESSION_PROPERTY_NAME_REFCD);
            }

            if (!string.IsNullOrEmpty(refcd))
            {
                //_Session.Add(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE, luggage, SessionPropertyLifetime.Persistent);
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_REFCD, refcd, 3, false);
            }

            // Check for client IP address 
            string clientIPAddress = String.Empty;
            clientIPAddress = this.ClientIP;

            if (newPRM)
            {
                _Session.Remove(WebConstants.SESSION_PROPERTY_NAME_CLIENTIP);
            }

            if (!string.IsNullOrEmpty(clientIPAddress))
            {
                _Session.Add(WebConstants.SESSION_PROPERTY_NAME_CLIENTIP, clientIPAddress, SessionPropertyLifetime.TemporaryDurable);
            }

            string eid = current.Request[WebConstants.URL_PARAMETER_EID];

            if (string.IsNullOrEmpty(eid) && registrationTransferCookie != null)
            {
                eid = registrationTransferCookie.Values[WebConstants.URL_PARAMETER_EID];
            }

            if (!string.IsNullOrEmpty(eid))
            {
                string currentSessionEid = _Session[WebConstants.SESSION_PROPERTY_NAME_EID];
                if (String.IsNullOrEmpty(currentSessionEid) || currentSessionEid != eid)
                {
                    _Session.Add(WebConstants.SESSION_PROPERTY_NAME_EID, eid, SessionPropertyLifetime.TemporaryDurable);
                }
            }

            // Check for referral URL 
            if (HttpContext.Current.Request.UrlReferrer != null)
            {
                string referralURL = String.Empty;
                referralURL = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;

                if (newPRM)
                {
                    _Session.Remove(WebConstants.SESSION_PROPERTY_NAME_REFERRALURL);
                }

                if (!string.IsNullOrEmpty(referralURL))
                {
                    _Session.Add(WebConstants.SESSION_PROPERTY_NAME_REFERRALURL, referralURL, SessionPropertyLifetime.TemporaryDurable);
                }
            }

            //MPR-1234 TL: Get PRM and Luggage based on Referral URL if no PRM exists
            if (_Session.GetString(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, null) == null)
            {
                string referralURL = "";
                string referralHost = "";
                string referralURLQueryString = "";
                Uri referralURi = HttpContext.Current.Request.UrlReferrer;
                if (referralURi != null)
                {
                    referralHost = referralURi.Host;
                    referralURL = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    if (!String.IsNullOrEmpty(referralURi.Query))
                        referralURLQueryString = referralURi.Query;
                }

                PRMURLMapperList prmList = PromotionSA.Instance.GetPRMURLMapperList();

                if (prmList != null)
                {
                    PRMURLMapper prmURLMapper = prmList.GetPRMByReferralURL(referralHost);
                    if (prmURLMapper != null)
                    {
                        if (String.IsNullOrEmpty(prmURLMapper.SourceURL))
                        {
                            if (referralURL.Length > 150)
                            {
                                referralURL = referralURL.Substring(0, 150);
                            }
                            //default PRM
                            _Session.Add(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, prmURLMapper.PromotionID.ToString(), 3, false);
                            _Session.Add(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE, referralURL, 3, false);
                        }
                        else
                        {
                            if (referralURLQueryString.Length > 150)
                            {
                                referralURLQueryString = referralURLQueryString.Substring(0, 150);
                            }
                            //matching PRM
                            _Session.Add(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, prmURLMapper.PromotionID.ToString(), 3, false);
                            _Session.Add(WebConstants.SESSION_PROPERTY_NAME_LUGGAGE, referralURLQueryString, 3, false);
                        }
                    }
                }
            }

            if (landingPageTransferCookie != null)
            {
                var removeMe = new HttpCookie("landingPageTransfer") { Expires = DateTime.Now.AddDays(-1) };
                current.Response.AppendCookie(removeMe);
            }

            if (registrationTransferCookie != null)
            {
                var removeMeReg = new HttpCookie("registrationTransferCookie") { Expires = DateTime.Now.AddDays(-1) };
                current.Response.AppendCookie(removeMeReg);
            }

        }

        public void AddExpansionToken(string key, string val)
        {
            try
            {
                if (!_expansionTokens.ContainsKey(key))
                { _expansionTokens.Add(key, val); }

                _expansionTokens[key] = val;
            }
            catch (Exception)
            { }
        }



        public Member.ServiceAdapters.Member Member
        {
            get
            {
                if (_member == null && Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) > 0)
                {
                    _member = MemberSA.Instance.GetMember(Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                        MemberLoadFlags.None);
                    //removed crap fix for registration problem
                    //bool validMember = IsMemberValid(_member);

                    //if (!validMember )
                    //{
                    //    Thread.Sleep(100);
                    //     _member = MemberSA.Instance.GetMember(Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                    //     MemberLoadFlags.IngoreSACache);

                    //     validMember = IsMemberValid(_member);
                    //     System.Diagnostics.EventLog.WriteEntry("WWW", "Validate member second attempt - valid=" + validMember.ToString() + ", memberid=" + _member.MemberID, System.Diagnostics.EventLogEntryType.Warning);
                    //}


                    //Get the impersonate info
                    if (IsAdmin || AdminImpersonate)
                    {
                        HttpContext current = HttpContext.Current;
                        if (current.Request[WebConstants.IMPERSONATE_MEMBERID] != null)
                        {
                            _ImpersonateMemberID = Conversion.CInt(current.Request[WebConstants.IMPERSONATE_MEMBERID]);
                            int intTargetBrandID = Conversion.CInt(current.Request[WebConstants.IMPERSONATE_BRANDID]);
                            _ImpersonateContext = true;

                            if (intTargetBrandID == Constants.NULL_INT)
                            {
                                //use the current member's BrandID if no impersonate Brand found in the query string.
                                intTargetBrandID = Brand.BrandID;
                            }
                            _ImpersonateBrand = BrandConfigSA.Instance.GetBrands().GetBrand(intTargetBrandID);
                        }
                    }
                }

                return _member;
            }
            set
            {
                _member = value;
            }
        }


        public List.ServiceAdapters.List List
        {
            get
            {
                if (_list == null && Member != null)
                {
                    _list = ListSA.Instance.GetList(Member.MemberID);
                }

                return _list;
            }
        }


        // checks to see if the current member has new messages in their message center
        public bool hasNewMessage
        {
            get
            {
                if (Member != null)
                {
                    if (!_newMessageCheckDone)
                    {
                        // Check for new messages.
                        _hasNewMessage = Member.GetAttributeBool(Brand, WebConstants.ATTRIBUTE_NAME_HASNEWMAIL);
                        _newMessageCheckDone = true;

                        #region  turn off e-card new messages for jdate.co.il

                        if (!EcardsEnabled)
                        {
                            System.Collections.ICollection foundMessages =
                                EmailMessageSA.Instance.RetrieveMessages(
                                    TargetMemberID, TargetCommunityID, (int)SystemFolders.Inbox);
                            //, 1, 1000,SortFieldType. );			

                            _hasNewMessage = false;
                            foreach (EmailMessage message in foundMessages)
                            {
                                if (message.MailTypeID == MailType.ECard)
                                {
                                }
                                else
                                {
                                    if ((int)message.StatusMask == 12)
                                    {
                                        _hasNewMessage = true;
                                    }
                                }
                            }
                        }
                        #endregion

                    }


                    return _hasNewMessage;
                }

                return false;
            }
        }




        public Notifications Notification
        {
            get
            {
                if (_Notification == null)
                {
                    _Notification = new Notifications();
                }
                return _Notification;
            }
        }


        public ICaching Cache { get; private set; }


        public LayoutTemplate LayoutTemplate
        {
            get
            {
                return _layoutTemplate;
            }
            set
            {
                _layoutTemplate = value;
            }
        }

        public Matchnet.Web.Lib.LayoutTemplateBase LayoutTemplateBase
        {
            get;
            set;
        }


        public bool LeftNavigationVisible
        {
            get
            {
                return _LeftNavigationVisible;
            }
            set
            {
                _LeftNavigationVisible = value;
            }
        }

        //new right nav for SiteRedesign
        public bool RightNavigationVisible { get; set; }


        
        public Content.ServiceAdapters.Page AppPage { get; set; }


        public string HeadContent { get; set; }


        public Pages DestinationPage { get; private set; }


        public int RecipientMemberID { get; private set; }


        public bool PersistLayoutTemplate { get; set; }


        public IAttributeAccessor Analytics { get; set; }
        
        //new right nav for SiteRedesign
        public IRight RightNavControl
        {
            get
            {
                if (_rightNavControl == null && Page != null)
                {
                    if (IsSiteMingle && !Matchnet.Web.Applications.MemberProfile.ProfileTabs30.ProfileUtility.IsDisplayingProfile30(this))
                    {
                        _rightNavControl = Page.LoadControl("/Framework/Ui/MingleRight.ascx") as IRight;
                    }
                    else
                    {
                        bool showSubNonSubRightRail = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_SUB_NON_SUB_RIGHT_RAIL", this.Brand.Site.Community.CommunityID, this.Brand.Site.SiteID, this.Brand.BrandID));
                        if (this.Member != null && this.AppPage.PageName.ToLower() == "default" && showSubNonSubRightRail)
                        {
                            if (this.Member.IsPayingMember(this.Brand.Site.SiteID)) // if subscriber
                            {
                                _rightNavControl = Page.LoadControl("/Framework/Ui/RightSubscriber.ascx") as IRight;
                            }
                            else
                            {
                                _rightNavControl = Page.LoadControl("/Framework/Ui/RightNonSubscriber.ascx") as IRight;
                            }
                        }
                        else
                        {
                            _rightNavControl = Page.LoadControl("/Framework/Ui/Right.ascx") as IRight;
                        }
                    }
                }
                return _rightNavControl;
            }
            set
            {
                _rightNavControl = value;
            }
        }


       public Footer20 FooterControl20
        {
            get
            {
                if (_Footer20 == null && Page != null)
                {
                    _Footer20 = Page.LoadControl("/Framework/Ui/Footer20.ascx") as Footer20;
                }

                return _Footer20;
            }
            set
            {
                _Footer20 = value;
            }
        }


        public IHeader HeaderControl
        {
            get
            {
                if (Page != null && _Header == null)
                {
                    _Header = Page.LoadControl("/Framework/Ui/Header20.ascx") as Header20;
                }
                return _Header;
            }
            set
            {
                _Header = value;
            }
        }


        public TopAuxNav TopAuxilaryMenu { get; set; }

        public BreadCrumbTrail BreadCrumbTrailHeader
        {
            get
            {
                if (_breadCrumbTrailHeader == null)
                {
                    if (Page != null)
                        _breadCrumbTrailHeader = Page.LoadControl("/Framework/Ui/PageElements/BreadCrumbTrail.ascx") as BreadCrumbTrail;
                }

                return _breadCrumbTrailHeader;
            }
        }


        public BreadCrumbTrail BreadCrumbTrailFooter
        {
            get
            {
                if (_breadCrumbTrailFooter == null)
                {
                    if (Page != null)
                    {
                        _breadCrumbTrailFooter = Page.LoadControl("/Framework/Ui/PageElements/BreadCrumbTrail.ascx") as BreadCrumbTrail;
                        _breadCrumbTrailFooter.ContextType = BreadCrumbTrail.ContextTypeEnum.Footer;
                    }
                }

                return _breadCrumbTrailFooter;
            }
        }


        public Label ListNavigationTop { get; set; }

        public Label ListNavigationBottom { get; set; }

        /// <summary>
        /// A collection of MemberSearch, each of which includes a set of search preferences.
        /// This allows a member to have multiple saved sets of search preferences.
        /// </summary>
        public Spark.SAL.MemberSearchCollection MemberSearches
        {
            get
            {
                if (_memberSearchCollection == null)
                {
                    if ((Member != null) && (Member.MemberID > 0) && (_Session.GetString(VisitorLimitHelper.SESSIONKEY_VISITORLIMIT_DONOTOVERWRITESEARCHPREFS, String.Empty) != "true"))
                    {
                        _memberSearchCollection = Spark.SAL.MemberSearchCollection.Load(Member.MemberID, Brand);
                    }
                    else
                    {
                        _memberSearchCollection = Spark.SAL.MemberSearchCollection.Load(Constants.NULL_INT, Brand);
                    }
                }

                return _memberSearchCollection;
            }
        }

        public SearchPreferenceCollection SearchPreferences
        {
            get
            {
                if (_searchPreferences == null)
                {
                    if ((Member != null) && (Member.MemberID > 0) && (_Session.GetString(VisitorLimitHelper.SESSIONKEY_VISITORLIMIT_DONOTOVERWRITESEARCHPREFS, String.Empty) != "true"))
                    {
                        _searchPreferences = SearchPreferencesSA.Instance.GetSearchPreferences(Member.MemberID
                            , Brand.Site.Community.CommunityID);
                    }
                    else
                    {
                        _searchPreferences = Session.Get(SearchPreferencesKey.GetCacheKey(Session.Key.ToString())) as SearchPreferenceCollection;
                    }
                }

                if (_searchPreferences == null)
                {
                    _searchPreferences = new SearchPreferenceCollection();
                }

                //ensure required pref exist
                if (string.IsNullOrEmpty(_searchPreferences["SearchTypeID"]))
                {
                    _searchPreferences["SearchTypeID"] = "2";
                }

                return _searchPreferences;
            }
        }


        public SearchPreferenceCollection QuickSearchPreferences
        {
            get
            {
                if (_quickSearchPreferences == null)
                {
                    QuickSearchHandler handler = new QuickSearchHandler(null, this);
                    Spark.SAL.MemberSearch membersearch = handler.getMemberSearch(0);
                    _quickSearchPreferences = QuickSearchHandler.GetQuickSearchPreferenceCollection(membersearch);
                }

                //ensure required pref exist
                if (string.IsNullOrEmpty(_quickSearchPreferences["SearchTypeID"]))
                {
                    _quickSearchPreferences["SearchTypeID"] = "2";
                }

                return _quickSearchPreferences;
            }
        }

        public SearchPreferenceCollection AdvancedSearchPreferences
        {
            get
            {
                if (_searchPreferences == null)
                {
                    _searchPreferences = Session.Get(WebConstants.SESSION_ADV_SEARCH) as SearchPreferenceCollection;
                }

                if (_searchPreferences == null)
                {
                    _searchPreferences = new SearchPreferenceCollection();
                }

                //ensure required pref exist
                if (string.IsNullOrEmpty(_searchPreferences["SearchTypeID"]))
                {
                    _searchPreferences["SearchTypeID"] = "2";
                }

                return _searchPreferences;
            }
        }

        public SearchPreferenceCollection MembersOnlineSearchPreferences
        {
            get
            {
                if (_membersOnlineSearchPrerences == null)
                {
                    _membersOnlineSearchPrerences = Session.Get(WebConstants.SESSION_MOL_SEARCH) as SearchPreferenceCollection;
                }

                if (_membersOnlineSearchPrerences == null)
                {
                    _membersOnlineSearchPrerences = new SearchPreferenceCollection();
                }

                //ensure required pref exist
                _membersOnlineSearchPrerences["online"] = "1";
                if (string.IsNullOrEmpty(_membersOnlineSearchPrerences["SearchTypeID"]))
                {
                    _membersOnlineSearchPrerences["SearchTypeID"] = "4"; 
                }

                return _membersOnlineSearchPrerences;
            }
        }

        public void ResetSearchPreferences()
        {
            _searchPreferences = null;
        }

        public bool IsRemoteRegistration { get; set; }

        public LandingPage LandingPage { get; set; }

        public EmailFolderCollection MemberEmailFolders
        {
            get
            {
                if (_memberEmailFolders == null)
                {
                    _memberEmailFolders = EmailFolderSA.Instance.RetrieveFolders(Member.MemberID, Brand.Site.Community.CommunityID, true);
                }

                return _memberEmailFolders;
            }
        }


        public string ClientIP
        {
            get
            {
                return _CurrentRequestWrapper.ClientIP;
            }
        }

        public string GetUserAgent()
        {
            return _CurrentRequestWrapper.GetUserAgent();
        }

        public string GetHeaders()
        {
            return _CurrentRequestWrapper.GetHeaders();
        }

        public bool IsDevMode { get; private set; }


        public InstanceMemberCollection PremiumServices
        {
            get
            {
                if (premiumServices == null)
                {
                    if (_Brand != null && _member != null)
                        premiumServices = Matchnet.PremiumServices.ServiceAdapter.ServiceMemberSA.Instance.GetMemberServices(_Brand, _member);

                }
                return premiumServices;

            }


            set
            {
                if (value != null)
                    premiumServices = value;

            }
        }

        public Scenario GetABScenario(string scenName)
        {
            Scenario scen = Scenario.A;
            try
            {

                if (Member != null)
                {
                    AnaliticsScenarioBase scenario = new AnaliticsScenarioBase(scenName, Brand.Site.Community.CommunityID, Brand.Site.SiteID, Member.MemberID);
                    scen = RuntimeSettings.GetAnaliticsScenario(scenario);
                }

                return scen;
            }
            catch (Exception)
            {
                return scen;
            }
        }

        public bool ResetSearchStartRow
        {
            get
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["ResetSearchStartRow"]))
                    return true;
                else
                    return _ResetSearchStartRow;
            }
            set
            {
                _ResetSearchStartRow = value;
            }
        }


        #endregion

        public void SetTopAuxMenuVisibility(bool visible, bool logoVisible)
        {
            if (TopAuxilaryMenu != null)
            {
                TopAuxilaryMenu.SetTopNavVisibility(visible, logoVisible);
            }
        }

        public void SetTopNoNavTitle(string title)
        {
            if (HeaderControl != null)
            {
                HeaderControl.SetTopNavTitle(title);
            }
        }

        /// <summary>
        /// Determines the JRewards status for a member
        /// </summary>
        /// <returns></returns>
        public WebConstants.JRewardsMembershipStatus DetermineJRewardsStatus()
        {
            WebConstants.JRewardsMembershipStatus ret = WebConstants.JRewardsMembershipStatus.None;

            int failureThreshold = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENCORE_FAILURE_AGE"));
            DateTime matchbackDate = this.Member.GetAttributeDate(this.Brand, "EncoreMatchbackDate", DateTime.MinValue);
            DateTime startDate = this.Member.GetAttributeDate(this.Brand, "JRewardStartDate", DateTime.MinValue);
            DateTime endDate = this.Member.GetAttributeDate(this.Brand, "JRewardEndDate", DateTime.MinValue);

            if (matchbackDate == DateTime.MinValue)
            {
                ret = WebConstants.JRewardsMembershipStatus.NeverMember;
            }
            else
            {
                // can assume matchback date exists                
                if (startDate == DateTime.MinValue && endDate == DateTime.MinValue)
                {
                    // pending versus failed check
                    if (matchbackDate > DateTime.Now.AddHours(-failureThreshold))
                    {
                        ret = WebConstants.JRewardsMembershipStatus.PendingMember;
                    }
                    else
                    {
                        ret = WebConstants.JRewardsMembershipStatus.FTSFailedMembership;
                    }
                }
                else
                {
                    // can assume matchbackdate exists
                    // No end date means this member is an active member
                    if (startDate != DateTime.MinValue && endDate == DateTime.MinValue)
                    {
                        ret = WebConstants.JRewardsMembershipStatus.ActiveMember;
                    }
                    else
                    {
                        // From this point on, we need endDate to process anything
                        if (endDate != DateTime.MinValue)
                        {
                            // can assume both dates are filled out along with matchbackdate
                            if (endDate > DateTime.Now)
                            {
                                ret = WebConstants.JRewardsMembershipStatus.ActiveMember;
                            }
                            else
                            {
                                // this means the user is one of four exmember types; recent, old, recent failed, old failed
                                // **matchbackDate records date and time, and endDate only records date.  if matchbackDate and
                                // endDate end up having the same date, then there is no way to know which event came first.
                                // this is a limitation that we are willing to accept, and we will assume that endDate always took
                                // place after matchbackDate if they end up having the same date.
                                DateTime modifiedEndDate = new DateTime(endDate.Year, endDate.Month, endDate.Day);
                                modifiedEndDate = modifiedEndDate.AddDays(1);
                                if (matchbackDate > modifiedEndDate)
                                {
                                    // one of the failed types or could be pending
                                    if (matchbackDate > DateTime.Now.AddHours(-failureThreshold))
                                    {
                                        ret = WebConstants.JRewardsMembershipStatus.PendingMember;
                                    }
                                    else
                                    {
                                        if (endDate > DateTime.Now.AddMonths(-6))
                                        {
                                            ret = WebConstants.JRewardsMembershipStatus.RecentExFailedMembership;
                                        }
                                        else
                                        {
                                            ret = WebConstants.JRewardsMembershipStatus.OldExFailedMembership;
                                        }
                                    }
                                }
                                else
                                {
                                    if (endDate > DateTime.Now.AddMonths(-6))
                                    {
                                        ret = WebConstants.JRewardsMembershipStatus.RecentExMember;
                                    }
                                    else
                                    {
                                        ret = WebConstants.JRewardsMembershipStatus.OldExMember;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// Determines if the member is eligible for Encore's rebate. Does not check for valid CC or remaining credit amount.
        /// </summary>
        /// <returns></returns>
        public bool IsEligibleForEncoreRebateCredit()
        {
            return IsEligibleForEncoreRebateCredit(Member.MemberID, Brand.BrandID, DateTime.Now, false);
        }
        /// <summary>
        /// Determines if the member is eligible for Encore's rebate. Does not check for valid CC or remaining credit amount.
        /// </summary>
        /// <param name="ignoreActiveSub">Active sub check should be ignored or not</param>
        /// <returns></returns>
        public bool IsEligibleForEncoreRebateCredit(bool ignoreActiveSub)
        {
            return IsEligibleForEncoreRebateCredit(Member.MemberID, Brand.BrandID, DateTime.Now, ignoreActiveSub);
        }
        /// <summary>
        /// Determines if the member is eligible for Encore's rebate. Does not check for valid CC or remaining credit amount.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brandID"></param>
        /// <param name="membershipStartDate"></param>
        /// <param name="ignoreActiveSub"></param>
        /// <returns></returns>
        public bool IsEligibleForEncoreRebateCredit(int memberID, int brandID, DateTime membershipStartDate, bool ignoreActiveSub)
        {
            bool isActiveSub = false; ;
            bool outsideMaryland = false;
            bool firstTimeBuyer = false;

            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            if (member != null)
            {
                // active subscriber?
                if (ignoreActiveSub)
                {
                    isActiveSub = true;
                }
                else
                {
                    isActiveSub = member.IsPayingMember(brand.Site.SiteID);
                }

                // Outside Maryland check & within USA
                if (isActiveSub)
                {
                    int regionID = member.GetAttributeInt(brand, "RegionID", Constants.NULL_INT);
                    if (regionID != Constants.NULL_INT)
                    {
                        Matchnet.Content.ValueObjects.Region.RegionLanguage region = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, brand.Site.LanguageID);

                        outsideMaryland = (region.StateRegionID != 1554) && (region.CountryRegionID == 223);
                    }
                }

                // Successful purchase and first time buyer check
                if (outsideMaryland)
                {
                    DateTime memberAttrStartDate = member.GetAttributeDate(brand, "JRewardStartDate", DateTime.MinValue);

                    firstTimeBuyer = (memberAttrStartDate == DateTime.MinValue) && (membershipStartDate > DateTime.MinValue);
                }

            }

            // DO A FINAL BOOL EXPRESSION HERE
            return isActiveSub && outsideMaryland && firstTimeBuyer;
        }

        public string GetCurrencyString(int currencyid)
        {
            try
            {
                if (_currencyMap == null)
                {
                    _currencyMap = new Hashtable();
                    _currencyMap.Add("1", "USD");
                    _currencyMap.Add("2", "EU");
                    _currencyMap.Add("3", "CAD");
                    _currencyMap.Add("4", "GBP");
                    _currencyMap.Add("5", "AUD");
                    _currencyMap.Add("6", "ILS");


                }
                return _currencyMap[currencyid.ToString()].ToString();
            }
            catch (Exception ex)
            { return "USD"; }

        }

        // PM-218 This method is not in use.
        //public bool IsMemberValid(Matchnet.Member.ServiceAdapters.Member m)
        //{
        //    string format = "memberid={0};gender={1};emailaddress={2};username={3};brandlogoncount={4};insertdate={5};url={6}";
        //    try
        //    {
        //        string page = HttpContext.Current.Request.RawUrl; ;

        //        if (page.ToLower().IndexOf("registration") < 0)
        //            return true;

        //        if (m == null)
        //        {
        //            System.Diagnostics.EventLog.WriteEntry("WWW", "Member is null in GlobalContext.", System.Diagnostics.EventLogEntryType.Warning);
        //            return false;
        //        }

        //        int memberid = m.MemberID;
        //        int genderMask = m.GetAttributeInt(Brand, "GenderMask");
        //        string emailAddress = m.EmailAddress;
        //        string userName = m.GetUserName(G.Brand);
        //        int brandLogonCount = m.GetAttributeInt(Brand, "BrandLogonCount", 0);
        //        DateTime brandInsertDate = m.GetAttributeDate(Brand, "BrandInsertDate", DateTime.MinValue);
        //        if (genderMask < 0 || userName == null || userName == string.Empty || brandLogonCount == 0 || brandInsertDate == DateTime.MinValue)
        //        {
        //            string data = string.Format(format, m.MemberID, genderMask, emailAddress, userName, brandLogonCount, brandInsertDate.ToString(), page);
        //            System.Diagnostics.EventLog.WriteEntry("WWW", "Member invalid in GlobalContext, member data - " + data, System.Diagnostics.EventLogEntryType.Warning);
        //            return false;
        //        }
        //        else
        //            return true;

        //    }
        //    catch (Exception ex)
        //    { ProcessException(ex); return false; }


        //}

        public bool IsAPI
        {
            get { return _isAPI; }
            set { _isAPI = value; }
        }

        public StringBuilder ProfileSectionItemJS { get; set; }
        public StringBuilder ProfileEditItemJS { get; set; }

        public UserSession GetSessionFromID(HttpContext context)
        {

            try
            {
                UserSession session = null;
                if (context.Request["SPWSCYPHER"] != null)
                {
                    string key = HttpUtility.UrlDecode(context.Request["SPWSCYPHER"]).Replace(" ", "+");
                    if (!String.IsNullOrEmpty(key))
                    {
                        try
                        {
                            string keyret = Matchnet.Security.Crypto.Decrypt(WebConstants.SPARK_WS_CRYPT_KEY, key);

                            session = SessionSA.Instance.GetSession(keyret);
                        }
                        catch (Exception ex)
                        { //this exception can take down app because g will not be initialized
                            Exception newex = new Exception("Failed to decrypt session guid while getting session by id:" + key, ex);
                            ProcessException(newex);
                        }
                    }

                }
                if (session == null)
                    session = SessionSA.Instance.GetSession(IsDevMode);


                return session;

            }
            catch (Exception ex)
            { ProcessException(ex); return null; }
        }

        public bool IsApiSession()
        {
            try
            {
                string session = HttpContext.Current.Request["SPWSCYPHER"];
                return !String.IsNullOrEmpty(session) ? true : false;
            }
            catch (Exception ex)
            { ProcessException(ex); return false; }
        }

        public Hashtable EditControls
        {
            get { return _EditControls; }
            set { _EditControls = value; }
        }

        public bool UpdateSessionInt(string key, int value, int lifetime)
        {
            try
            {
                SessionPropertyLifetime sessionlifetime = SessionPropertyLifetime.Temporary;
                try
                {
                    sessionlifetime = (SessionPropertyLifetime)lifetime;
                }
                catch (Exception e)
                {
                }
                string val = this.Session.GetString(key);
                int ival = Conversion.CInt(val, 0);
                ival += value;
                Session.Add(key, ival.ToString(), sessionlifetime);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }


        }

        #region Pages enum
        public enum Pages
        {
            PAGE_TERMINATION_TERMINATEREASON = 1,
            PAGE_TERMINATION_PROFILESETTING = 2,
            PAGE_TERMINATION_TERMINATEFINAL = 3,
            PAGE_CAPTCHA_IMAGE = 4,
            PAGE_TERMINATION_SAVEFINAL = 5,
            PAGE_TERMINATION_OFFER = 6,
            PAGE_SUBSCRIPTION_RENEWAL = 20,
            PAGE_SUBSCRIPTION_OTHERWAYSTOPAY = 21,
            PAGE_MEMBERSEARCH_PASSWORDLOOKUP = 22,
            PAGE_MEMBERPROFILE_MEMBERPHOTOFILE = 23,
            PAGE_ERROR_ERROR = 24,
            PAGE_MEMBERSEARCH_REQUEUEMEMBER = 25,
            PAGE_SUBSCRIPTION_FREETRIALPHOTOUPLOAD = 27,
            PAGE_SUBSCRIPTION_FREETRIAL = 28,
            PAGE_ARTICLE_ARTICLE = 30,
            PAGE_SUBSCRIPTION_FREETRIALSUBSCRIBE = 31,
            PAGE_HOME_HOME = 1000,
            PAGE_HOME_NOT_FOUND = 1010,
            ACTION_HOME_LOGON = 1500,
            ACTION_HOME_CLEAR_CACHE = 1510,
            PAGE_LOGON_DEFAULT = 2000,
            PAGE_LOGON_REDIRECT = 2011,
            PAGE_LOGON_RETRIEVE_PASSWORD = 2021,
            PAGE_LOGON_CONTACT_LOGON = 2031,
            PAGE_LOGON_LOGON_ACTION = 2041,
            ACTION_LOGON_AUTHENTICATE = 2500,
            ACTION_LOGON_RETRIEVEPASSWORD = 2510,
            ACTION_LOGON_LOGOFF = 2520,
            PAGE_SUBSCRIPTIONS_CREDITCARDFORM = 3000,
            PAGE_SUBSCRIPTIONS_CONFIRM = 3010,
            PAGE_SUBSCRIPTIONS_CHECKFORM = 3020,
            PAGE_SUBSCRIPTIONS_CONFIRM_COMPLETE = 3030,
            PAGE_SUBSCRIPTIONS_ONE_CLICK = 3040,
            PAGE_SUBSCRIPTIONS_DEFAULT = 3050,
            PAGE_SUBSCRIPTIONS_TRANSACTIONS = 3060,
            PAGE_SUBSCRIPTIONS_END = 3090,
            PAGE_SUBSCRIPTIONS_END_RESULT = 3100,
            PAGE_SUBSCRIPTIONS_ADMIN_INSERT = 3110,
            PAGE_SUBSCRIPTIONS_ADMIN_ADJUST = 3120,
            PAGE_SUBSCRIPTIONS_CREDIT_CARD_UPDATE = 3130,
            PAGE_SUBSCRIPTION_ADMINREMOVEPAYMENTINFO = 3140,
            ACTION_SUBSCRIPTIONS_INSERT = 3500,
            ACTION_SUBSCRIPTIONS_ONE_CLICK_BUY = 3510,
            ACTION_SUBSCRIPTIONS_DEFAULT = 3520,
            ACTION_SUBSCRIPTIONS_END = 3530,
            ACTION_SUBSCRIPTIONS_ADMIN_INSERT = 3540,
            ACTION_SUBSCRIPTIONS_ADMIN_ADJUST = 3550,
            ACTION_SUBSCRIPTIONS_GET_END_PAGE = 3560,
            ACTION_SUBSCRIPTIONS_CREDIT_CARD_SAVE = 3570,
            PAGE_MEMBER_PHOTOS_DEFAULT = 4000,
            ACTION_MEMBER_PHOTOS_MEMBER_PHOTO_MEMBER_ATTRIBUTE = 4001,
            ACTION_MEMBER_PHOTOS_MNAPPROVE = 4003,
            ACTION_MEMBER_PHOTOS_SAVE = 4500,
            ACTION_MEMBER_PHOTOS_DELETE = 4510,
            ACTION_MEMBER_PHOTOS_SHOW_FULLSIZE = 4520,
            ACTION_MEMBER_PHOTOS_REQUEUE = 4530,
            PAGE_MEMBER_PROFILE_DONE = 7000,
            PAGE_MEMBER_PROFILE_VIEW_PROFILE_MAIN = 7070,
            PAGE_MEMBER_PROFILE_EDIT_PROFILE = 7080,
            PAGE_MEMBER_PROFILE_STEP_1 = 7090,
            PAGE_MEMBER_PROFILE_STEP_2 = 7100,
            PAGE_MEMBER_PROFILE_STEP_3 = 7110,
            PAGE_MEMBER_PROFILE_STEP_4 = 7120,
            PAGE_MEMBER_PROFILE_STEP_5 = 7130,
            PAGE_MEMBER_PROFILE_VIEW_PROFILE_BASICS = 7150,
            PAGE_MEMBER_PROFILE_VIEW_PROFILE_PERSONALITY = 7160,
            PAGE_MEMBER_PROFILE_VIEW_PROFILE_INTERESTS = 7170,
            PAGE_MEMBER_PROFILE_CHANGE_EMAIL_PASSWORD = 7180,
            PAGE_MEMBER_PROFILE_WELCOME = 7200,
            PAGE_MEMBER_PROFILE_CAPTCHA = 7210,
            ACTION_MEMBER_PROFILE_SAVE_STEP_1 = 7500,
            ACTION_MEMBER_PROFILE_SAVE_STEP_2 = 7510,
            ACTION_MEMBER_PROFILE_SAVE_STEP_3 = 7520,
            ACTION_MEMBER_PROFILE_SAVE_STEP_4 = 7530,
            ACTION_MEMBER_PROFILE_SAVE_STEP_5 = 7540,
            ACTION_MEMBER_PROFILE_SAVE_STEP_6 = 7550,
            ACTION_MEMBER_PROFILE_DELETE_PHOTO = 7560,
            ACTION_MEMBER_PROFILE_SEND_VERIFY_EMAIL_LETTER = 7580,
            ACTION_MEMBER_PROFILE_SAVE_STEP_NO_FORM_HANDLING = 7590,
            ACTION_MEMBER_PROFILE_SAVE_CHANGE_EMAIL_PASSWORD = 7600,
            ACTION_MEMBER_PROFILE_CHANGE_REGION = 7610,
            ACTION_MEMBER_PROFILE_SAVE_WELCOME_PAGE = 7620,
            PAGE_SUBSCRIPTION_DETAILS = 10000,
            PAGE_CONTACT_MEMBER_CONFIRMATION_MESSAGE = 10010,
            PAGE_CONTACT_MEMBER_DECLINE_MESSAGE = 10020,
            PAGE_CONTACT_MEMBER_DECLINE_CONFIRMATION_MESSAGE = 10030,
            PAGE_CONTACT_MEMBER_CONTACT_MEMBER = 10040,
            PAGE_CONTACT_MEMBER_TEASE_MEMBER = 10050,
            PAGE_CONTACT_MEMBER_TEASE_CONFIRMATION_MESSAGE = 10060,
            PAGE_CONTACT_MEMBER_TEASE_ERROR = 10070,
            ACTION_CONTACT_MEMBER_SEND_MESSAGE = 10500,
            ACTION_CONTACT_MEMBER_DECLINE_MESSAGE = 10510,
            ACTION_CONTACT_MEMBER_SEND_TEASE = 10520,
            ACTION_CONTACT_MEMBER_SEND_MESSAGE_MINI_PROFILE = 10530,
            ACTION_CONTACT_MEMBER_SEND_EMAIL = 10540,
            PAGE_HEALTHCHECK_DEFAULT = 11000,
            PAGE_CHAT_LAUNCH = 12000,
            PAGE_CHAT_DEFAULT = 12010,
            PAGE_ARTICLES_DEFAULT = 13000,
            PAGE_ARTICLES_VIEW_ARTICLE = 13010,
            ACTION_ARTICLES_DEFAULT = 13500,
            PAGE_CONTACTUS_CONFIRMATION = 14000,
            PAGE_CONTACTUS_DEFAULT = 14010,
            ACTION_CONTACTUS_SEND = 14500,
            PAGE_PHOTO_ALBUMS_EDIT_ALBUM = 15010,
            PAGE_PHOTO_ALBUMS_VIEW_PHOTOS = 15020,
            PAGE_PHOTO_ALBUMS_VIEW_PHOTO = 15030,
            PAGE_PHOTO_ALBUMS_LIST_MEMBERS = 15040,
            ACTION_PHOTO_ALBUMS_SAVE_ALBUM = 15500,
            ACTION_PHOTO_ALBUMS_DELETE_ALBUM = 15510,
            ACTION_PHOTO_ALBUMS_ADD_PHOTO = 15520,
            ACTION_PHOTO_ALBUMS_MOVE_PHOTOS = 15530,
            ACTION_PHOTO_ALBUMS_DELETE_PHOTOS = 15540,
            ACTION_PHOTO_ALBUMS_EDIT_PHOTO = 15550,
            ACTION_PHOTO_ALBUMS_CHANGE_ALBUM = 15560,
            ACTION_PHOTO_ALBUMS_EDIT_MEMBER = 15570,
            ACTION_PHOTO_ALBUMS_REQUEST_ACCESS = 15580,
            ACTION_PHOTO_ALBUMS_CHANGE_STATUS = 15590,
            PAGE_SEND_TO_FRIEND_DEFAULT = 16000,
            PAGE_SEND_TO_FRIEND_CONFIRMATION = 16010,
            PAGE_SEND_TO_FRIEND_ECARD_DEFAULT = 16020,
            PAGE_SEND_TO_FRIEND_ECARD_PREVIEW = 16030,
            PAGE_SEND_TO_FRIEND_ECARD_CONFIRMATION = 16040,
            PAGE_SEND_TO_FRIEND_ECARD_READ = 16050,
            ACTION_SEND_TO_FRIEND_SEND = 16500,
            ACTION_SEND_TO_FRIEND_MINI_PROFILE_SEND = 16510,
            ACTION_SEND_TO_FRIEND_ECARD_SEND = 16520,
            ACTION_SEND_TO_FRIEND_ECARD_READ = 16530,
            PAGE_MEMBER_SERVICES_DEFAULT = 18000,
            PAGE_MEMBER_SERVICES_LOOKUP_PROFILE = 18010,
            PAGE_MEMBER_SERVICES_SUSPEND = 18020,
            PAGE_MEMBER_SERVICES_UNSUSPEND = 18030,
            PAGE_MEMBER_SERVICES_SHOW_PROFILE = 18060,
            PAGE_MEMBER_SERVICES_LOGON_ACTIVITY = 18080,
            PAGE_MEMBER_SERVICES_EMAIL_SETTINGS = 18090,
            PAGE_MEMBER_SERVICES_VERIFY_EMAIL = 18100,
            PAGE_MEMBER_SERVICES_EMAIL_SETTINGS_CONFIRMATION = 18110,
            PAGE_MEMBER_SERVICES_VERIFY_EMAIL_CONFIRMATION = 18120,
            PAGE_MEMBER_SERVICES_COSTS = 18130,
            PAGE_MEMBER_SERVICES_TERMS_PURCHASE = 18140,
            PAGE_MEMBER_SERVICES_PROFILE_DISPLAY_SETTINGS = 18150,
            PAGE_MEMBER_SERVICES_CONFIRM_EMAIL = 18160,
            ACTION_MEMBER_SERVICES_LOOKUP_BY_MEMBERID = 18500,
            ACTION_MEMBER_SERVICES_SUSPEND = 18510,
            ACTION_MEMBER_SERVICES_UNSUSPEND = 18520,
            ACTION_MEMBER_SERVICES_SAVE_EMAIL_SETTINGS = 18530,
            ACTION_MEMBER_SERVICES_VERIFY_EMAIL = 18540,
            ACTION_MEMBER_SERVICES_VERIFY_EMAIL_FORM = 18550,
            ACTION_MEMBER_SERVICES_SAVE_DISPLAY_SETTINGS = 18560,
            ACTION_MEMBER_SERVICES_SAVE_EMAIL = 18570,
            ACTION_MEMBER_SERVICES_LOOKUP_BY_USERNAME = 18580,
            ACTION_MEMBER_SERVICES_LOOKUP_BY_ACCOUNT = 18590,
            ACTION_MEMBER_SERVICES_SAVE_VIEW_STATE = 18600,
            PAGE_INSTANT_MESSENGER_INITIATE_CONVERSATION = 19010,
            PAGE_INSTANT_MESSENGER_SHOW_MESSAGES = 19020,
            PAGE_INSTANT_MESSENGER_IGNORE_LIST = 19030,
            PAGE_INSTANTMESSENGER_AWAY = 19040,
            PAGE_INSTANT_MESSENGER_ERROR = 19050,
            PAGE_INSTANT_MESSENGER_ESTABLISH_CONNECTION = 19060,
            PAGE_INSTANT_MESSENGER_AWAY = 19070,
            PAGE_INSTANT_MESSENGER_MINI_PROFILE = 19090,
            PAGE_INSTANT_MESSENGER_HEADER = 19100,
            PAGE_INSTANT_MESSENGER_LEFT = 19110,
            PAGE_INSTANT_MESSENGER_INPUT = 19120,
            PAGE_INSTANT_MESSENGER_MAIN = 19130,
            PAGE_INSTANT_MESSENGER_CLOSE = 19140,
            PAGE_INSTANT_MESSENGER_HOTLIST = 19150,
            PAGE_INSTANT_MESSENGER_INSTALL = 19160,
            ACTION_INSTANT_MESSENGER_DELETE_IGNORED_USER = 19500,
            ACTION_INSTANT_MESSENGER_ADD_IGNORED_USER = 19510,
            ACTION_INSTANT_MESSENGER_UPDATE_AWAY_STATUS = 19520,
            ACTION_INSTANT_MESSENGER_START_CONVERSATION = 19540,
            ACTION_INSTANT_MESSENGER_ESTABLISH_CONNECTION = 19550,
            PAGE_APPLET_ACTION_PERFORM_ACTION = 20000,
            PAGE_SEARCH_RESULTS = 22000,
            PAGE_SEARCH_LARGE_FORM = 22010,
            PAGE_SEARCH_MEMBER_MATCHES = 22020,
            PAGE_SEARCH_RESULTS_GRID = 22030,
            PAGE_SEARCH_PREFERENCES = 22040,
            PAGE_SEARCH_EXIT_RESULTS = 22050,
            ACTION_SEARCH_LARGE_FORM_SUBMIT = 22500,
            ACTION_SEARCH_MENU = 22510,
            ACTION_SEARCH_MINI_FORM_SUBMIT = 22520,
            ACTION_SEARCH_AREA_CODE_SUBMIT = 22530,
            ACTION_SEARCH_POSTAL_CODE_SUBMIT = 22540,
            ACTION_SEARCH_SAVE_VIEW_STATE = 22550,
            ACTION_SEARCH_QUICK_SORT = 22560,
            PAGE_MEMBERS_ONLINE_DEFAULT = 23000,
            ACTION_MEMBERS_ONLINE_VALIDATE_FORM = 23510,
            PAGE_MATCHNET_CORP_DEFAULT = 24000,
            PAGE_MATCHNET_CORP_ABOUT_US = 24010,
            PAGE_MATCHNET_CORP_INVESTOR_RELATIONS = 24020,
            PAGE_MATCHNET_CORP_BUSINESS_OPPORTUNITY = 24030,
            PAGE_MATCHNET_CORP_NEWSROOM = 24040,
            PAGE_MATCHNET_CORP_CAREER_OPPORTUNITY = 24050,
            PAGE_MATCHNET_CORP_CONTACT_US = 24060,
            PAGE_MATCHNET_CORP_SITE_MAP = 24070,
            PAGE_POLLS_LIST = 25000,
            ACTION_POLLS_ANSWER = 25500,
            PAGE_MEMBER_LIST_DEFAULT = 26000,
            PAGE_MEMBER_LIST_SAVE_CATEGORY = 26010,
            PAGE_MEMBER_LIST_MANAGE_CATEGORIES = 26020,
            PAGE_MEMBER_LIST_SAVE_FRIEND_MINI_PROFILE = 26040,
            PAGE_MEMBER_LIST_SAVE_FRIEND = 26050,
            PAGE_MEMBER_LIST_LEGEND = 26060,
            ACTION_MEMBER_LIST_SAVE_CATEGORY = 26500,
            ACTION_MEMBER_LIST_DELETE_CATEGORY = 26510,
            ACTION_MEMBER_LIST_DELETE_MULTIPLE_CATEGORIES = 26520,
            ACTION_MEMBER_LIST_DELETE_FRIEND = 26530,
            ACTION_MEMBER_LIST_SAVE_FRIEND = 26540,
            ACTION_MEMBER_LIST_SAVE_FRIEND_MINI_PROFILE = 26550,
            ACTION_MEMBER_LIST_DELETE_FRIEND_MINI_PROFILE = 26560,
            PAGE_EMAIL_DEFAULT = 27000,
            PAGE_EMAIL_VIEW_MESSAGES = 27010,
            PAGE_EMAIL_VIEW_MESSAGE = 27020,
            PAGE_EMAIL_VIEW_FOREIGN_MESSAGE = 27021,
            PAGE_EMAIL_VIEW_FOLDERS = 27030,
            PAGE_EMAIL_SAVE_FOLDER = 27040,
            PAGE_EMAIL_SETTINGS = 27050,
            PAGE_EMAIL_COMPOSE = 27060,
            PAGE_EMAIL_SELECT_MEMBER_TO_EMAIL = 27070,
            PAGE_EMAIL_LOOKUP_MEMBER_TO_EMAIL = 27080,
            PAGE_EMAIL_TEASE = 27090,
            PAGE_EMAIL_TEASE_CONFIRMATION = 27100,
            PAGE_EMAIL_TEASE_ERROR = 27110,
            ACTION_EMAIL_ADD_FOLDER = 27500,
            ACTION_EMAIL_DELETE_FOLDER = 27510,
            ACTION_EMAIL_SAVE_FOLDER = 27520,
            ACTION_EMAIL_MARK_AS_UNREAD_MULTIPLE = 27530,
            ACTION_EMAIL_MOVE_TO_FOLDER = 27540,
            ACTION_EMAIL_DELETE_MESSAGES = 27550,
            ACTION_EMAIL_SAVE_SETTINGS = 27560,
            ACTION_EMAIL_SEND = 27570,
            ACTION_EMAIL_LOOKUP_BY_MEMBERID = 27580,
            ACTION_EMAIL_LOOKUP_BY_USERNAME = 27590,
            ACTION_EMAIL_LOOKUP_BY_ACCOUNT = 27600,
            ACTION_EMAIL_SAVE_AS_A_DRAFT = 27610,
            ACTION_EMAIL_MARK_AS_UNREAD = 27620,
            ACTION_EMAIL_IGNORE = 27630,
            ACTION_EMAIL_VIEW_MESSAGE = 27640,
            ACTION_EMAIL_SEND_DECLINE = 27650,
            ACTION_EMAIL_SEND_TEASE = 27660,
            ACTION_EMAIL_CLEAR_FOLDER = 27670,
            ACTION_EMAIL_RELOAD_COMPOSE = 27680,
            ACTION_EMAIL_IGNORE_MULTIPLE = 27690,
            ACTION_EMAIL_DELETE = 27700,
            PAGE_EVENTS_DEFAULT = 28000,
            PAGE_EVENTS_PHOTO_ALBUMN = 28010,
            PAGE_EVENTS_FAQ = 28020,
            PAGE_EVENTS_CONTACT_US = 28030,
            PAGE_EVENTS_VIEW_EVENT = 28040,
            PAGE_EVENTS_VIEW_PHOTO_ALBUM = 28050,
            ACTION_EVENTS_CONTACT_US = 28500,
            ACTION_EVENTS_EVENT_SELECT = 28510,
            PAGE_REGION_SELECT_CHILD_REGION = 29000,
            ACTION_REGION_SELECT_CITY = 29500,
            PAGE_SPELL_CHECK_INIT = 30000,
            PAGE_SPELL_CHECK_CHECKING = 30010,
            PAGE_SPELL_CHECK_FINISHED = 30020,
            ACTION_SPELL_CHECK_IGNORE = 30500,
            ACTION_SPELL_CHECK_IGNORE_ALL = 30510,
            ACTION_SPELL_CHECK_CHANGE = 30520,
            ACTION_SPELL_CHECK_CHANGE_ALL = 30530,
            ACTION_SPELL_CHECK_ADD_TO_DICTIONARY = 30540,
            ACTION_SPELL_CHECK_FINISH = 30550,
            ACTION_SPELL_CHECK_CHECK_SPELLING = 30560,
            PAGE_KAZAA_SEARCH = 31000,
            PAGE_KAZAA_SPLASH = 31010,
            PAGE_ADMIN_HOME_DEFAULT = 501000,
            PAGE_ADMIN_HOME_UNAUTHORIZED = 501010,
            PAGE_ADMIN_LOCALES_DEFAULT = 503000,
            PAGE_ADMIN_LOCALES_SAVE = 503010,
            ACTION_ADMINLOCALES_SAVE = 503500,
            ACTION_ADMINLOCALES_DELETE = 503510,
            PAGE_ADMIN_ACTIONPAGES_DEFAULT = 504000,
            PAGE_ADMIN_ACTIONPAGES_SAVE = 504010,
            PAGE_ADMIN_ACTIONPAGES_LIST_PRIVILEGES = 504040,
            ACTION_ADMINACTIONPAGES_SAVE = 504500,
            ACTION_ADMINACTIONPAGES_DELETE = 504510,
            ACTION_ADMINACTIONPAGES_SAVE_PRIVILEGES = 504550,
            PAGE_ADMIN_APPLICATIONS_DEFAULT = 505000,
            PAGE_ADMIN_APPLICATIONS_SAVE = 505010,
            ACTION_ADMINAPPLICATIONS_SAVE = 505500,
            ACTION_ADMINAPPLICATIONS_DELETE = 505510,
            PAGE_ADMIN_BUGTRACK_DEFAULT = 506000,
            PAGE_ADMIN_BUGTRACK_ISSUE_INSERT = 506010,
            PAGE_ADMIN_BUGTRACK_COMMENT_INSERT = 506020,
            PAGE_ADMIN_BUGTRACK_ISSUE_LIST = 506030,
            PAGE_ADMIN_BUGTRACK_COMMENT_LIST = 506040,
            PAGE_ADMIN_BUGTRACK_ISSUE_STAT = 506050,
            PAGE_ADMIN_BUGTRACK_ISSUE_UPDATE = 506060,
            ACTION_ADMINBUGTRACK_ISSUE_INSERT = 506500,
            ACTION_ADMINBUGTRACK_COMMENT_INSERT = 506510,
            ACTION_ADMINBUGTRACK_ISSUE_LIST = 506520,
            ACTION_ADMINBUGTRACK_ISSUE_STAT_LIST = 506530,
            ACTION_ADMINBUGTRACK_ISSUE_STAT_OPEN = 506540,
            ACTION_ADMINBUGTRACK_ISSUE_STAT_CATEGORY = 506550,
            ACTION_ADMINBUGTRACK_ISSUE_UPDATE = 506560,
            PAGE_RESOURCE_LIST = 507000,
            PAGE_RESOURCE_EDIT = 507010,
            PAGE_ADMIN_RESOURCES_DOMAIN_DEFAULT = 507020,
            PAGE_ADMIN_RESOURCES_CONTENT_SAVE = 507030,
            PAGE_ADMIN_RESOURCES_ACTION_PAGE_RESOURCE_DEFAULT = 507040,
            PAGE_ADMIN_RESOURCES_ACTION_RESOURCE_LINK = 507050,
            ACTION_ADMINRESOURCES_EDIT = 507500,
            ACTION_ADMINRESOURCES_DELETE = 507510,
            ACTION_ADMINRESOURCES_SAVE = 507520,
            ACTION_ADMINRESOURCES_CONTENT_DELETE = 507530,
            ACTION_ADMINRESOURCES_CONTENT_SAVE = 507540,
            ACTION_ADMINRESOURCES_ACTION_RESOURCE_LINK = 507550,
            PAGE_ADMIN_PRIVILEGES_UPDATE_MEMBER = 508010,
            PAGE_ADMIN_PRIVILEGES_LIST_MEMBER_TOTALS = 508030,
            PAGE_ADMIN_PRIVILEGES_DEFAULT = 508040,
            ACTION_ADMINPRIVILEGES_UPDATE_MEMBER = 508520,
            PAGE_ADMIN_HTTPHEADERS_DEFAULT = 509000,
            PAGE_ADMIN_GROUPS_DEFAULT = 510000,
            PAGE_ADMIN_GROUPS_SAVE_MEMBERS = 510030,
            ACTION_ADMINGROUPS_ADD_MEMBER = 510530,
            ACTION_ADMINGROUPS_REMOVE_MEMBER = 510550,
            PAGE_ADMIN_PACKAGES_DEFAULT = 511000,
            PAGE_ADMIN_PACKAGES_SAVE_PACKAGE = 511010,
            PAGE_ADMIN_PACKAGES_LIST_PACKAGE_PRODUCTS = 511020,
            PAGE_ADMIN_PACKAGES_SAVE_PACKAGE_PRODUCTS = 511030,
            PAGE_ADMIN_PACKAGES_LIST_PRODUCTS = 511040,
            PAGE_ADMIN_PACKAGES_SAVE_PRODUCT = 511050,
            ACTION_ADMINPACKAGES_SAVE_PACKAGE = 511500,
            ACTION_ADMINPACKAGES_DELETE_PACKAGE = 511510,
            ACTION_ADMINPACKAGES_SAVE_PRODUCT = 511520,
            ACTION_ADMINPACKAGES_DELETE_PRODUCT = 511530,
            ACTION_ADMINPACKAGES_ASSIGN_PACKAGE_PRODUCTS = 511540,
            ACTION_ADMINPACKAGES_SAVE_PACKAGE_PRODUCTS = 511550,
            ACTION_ADMINPACKAGES_DELETE_PRODUCT_PACKAGE = 511560,
            PAGE_ADMIN_DOMAINS_DEFAULT = 512000,
            PAGE_ADMIN_DOMAINS_SAVE = 512010,
            ACTION_ADMINDOMAINS_SAVE = 512500,
            ACTION_ADMINDOMAINS_DELETE = 512510,
            PAGE_ADMIN_CONFIGURATION_DEFAULT = 513000,
            ACTION_ADMINCONFIGURATION_SAVE_ASP_CONSTANTS = 513500,
            ACTION_ADMINCONFIGURATION_TOGGLE_DEBUG_MODE = 513520,
            PAGE_ADMIN_PROMOTIONS_DEFAULT = 514000,
            PAGE_ADMIN_PROMOTIONS_SAVE = 514010,
            PAGE_ADMIN_PROMOTIONS_LIST_PROMOTERS = 514020,
            PAGE_ADMIN_PROMOTIONS_SAVE_PROMOTER = 514030,
            PAGE_ADMIN_PROMOTIONS_REGISTRATION = 514040,
            PAGE_ADMIN_PROMOTIONS_STATS = 514050,
            PAGE_ADMIN_PROMOTIONS_TEXT = 514060,
            ACTION_ADMINPROMOTIONS_SAVE = 514500,
            ACTION_ADMINPROMOTIONS_DELETE = 514510,
            ACTION_ADMINPROMOTIONS_SAVE_PROMOTER = 514520,
            ACTION_ADMINPROMOTIONS_DELETE_PROMOTER = 514530,
            ACTION_ADMINPROMOTIONS_SAVE_REGISTRATION = 514540,
            ACTION_ADMINPROMOTIONS_STATS_RUN = 514550,
            ACTION_ADMINPROMOTIONS_TEXT_INSERT = 514560,
            ACTION_ADMINPROMOTIONS_TEXT_UPDATE = 514570,
            ACTION_ADMINPROMOTIONS_TEXT_SELECT = 514580,
            ACTION_ADMINPROMOTIONS_TEXT_LIST = 514590,
            ACTION_ADMINPROMOTIONS_TEXT_DELETE = 514600,
            PAGE_ADMIN_APPROVAL_QUEUE_DEFAULT = 515000,
            ACTION_ADMINAPPROVAL_QUEUE_DEFAULT = 515500,
            PAGE_ADMIN_CACHE_UPDATE = 516000,
            PAGE_ADMIN_CACHE_INSERT = 516010,
            PAGE_ADMIN_CACHE_BROWSE = 516020,
            PAGE_ADMIN_CACHE_DEFAULT = 516030,
            ACTION_ADMINCACHE_INSERT = 516500,
            ACTION_ADMINCACHE_UPDATE = 516510,
            ACTION_ADMINCACHE_DELETE = 516520,
            ACTION_ADMINCACHE_DELETE_ALL = 516530,
            PAGE_ADMIN_ARTICLES_DEFAULT = 517000,
            PAGE_ADMIN_ARTICLES_SAVE = 517010,
            PAGE_ADMIN_ARTICLES_UPDATE = 517020,
            ACTION_ADMINARTICLES_DEFAULT = 517500,
            ACTION_ADMINARTICLES_SAVE = 517510,
            ACTION_ADMINARTICLES_DELETE = 517520,
            ACTION_ADMINARTICLES_UPLOAD = 517530,
            ACTION_ADMINARTICLES_UPDATE = 517540,
            PAGE_ADMIN_CATEGORIES_DEFAULT = 518000,
            PAGE_ADMIN_CATEGORIES_SAVE = 518010,
            PAGE_ADMIN_CATEGORIES_UPDATE = 518020,
            ACTION_ADMINCATEGORIES_DEFAULT = 518500,
            ACTION_ADMINCATEGORIES_SAVE = 518510,
            ACTION_ADMINCATEGORIES_DELETE = 518520,
            ACTION_ADMINCATEGORIES_UPDATE = 518530,
            PAGE_ADMIN_ATTRIBUTES_DEFAULT = 519000,
            PAGE_ADMIN_ATTRIBUTES_SAVE = 519010,
            PAGE_ADMIN_ATTRIBUTES_LIST_OPTIONS = 519020,
            PAGE_ADMIN_ATTRIBUTES_SAVE_OPTIONS = 519030,
            ACTION_ADMINATTRIBUTES_SAVE = 519500,
            ACTION_ADMINATTRIBUTES_DELETE = 519510,
            ACTION_ADMINATTRIBUTES_SAVE_OPTIONS = 519520,
            ACTION_ADMINATTRIBUTES_SAVE_OPTION_RESOURCES = 519530,
            PAGE_ADMIN_EXECUTE_SQL_RUN = 520000,
            PAGE_MEMBERSEARCH_SEARCH = 521000,
            PAGE_ADMIN_MEMBER_SEARCH_RESULTS = 521010,
            PAGE_MEMBERSEARCH_VIEWADMINNOTES = 521020,
            PAGE_ADMIN_MEMBER_SEARCH_VIEW_ACTION_LOG = 521021,
            PAGE_ADMIN_MEMBER_SEARCH_INSERT_NOTE = 521030,
            PAGE_ADMIN_MEMBER_SEARCH_CLOSE_WINDOW = 521040,
            PAGE_MEMBERSEARCH_CHANGEGLOBALSTATUSMASK = 521050,
            PAGE_ADMIN_MEMBER_SEARCH_EMAIL_LOG = 521060,
            PAGE_MEMBERSEARCH_CHANGEEMAIL = 521070,
            PAGE_ADMIN_MEMBER_SEARCH_ATTRIBUTE_EDIT = 521080,
            PAGE_ADMIN_MEMBER_SEARCH_PASSWORD_LOOKUP = 521090,
            ACTION_ADMINMEMBER_SEARCH_SEND_WELCOME_EMAIL = 521500,
            ACTION_ADMINMEMBER_SEARCH_SEND_PASSWORD = 521510,
            ACTION_ADMINMEMBER_SEARCH_SAVE_GLOBALSTATUSMASK = 521520,
            ACTION_ADMINMEMBER_SEARCH_VALIDATE_SEARCH = 521540,
            ACTION_ADMINMEMBER_SEARCH_INSERT_NOTE = 521550,
            ACTION_ADMINMEMBER_SEARCH_SEND_VERIFY_EMAIL_LETTER = 521560,
            ACTION_ADMINMEMBER_SEARCH_CHANGE_EMAIL = 521570,
            PAGE_ADMIN_MEMBER_SEARCH_EX_DEFAULT_ = 522000,
            PAGE_ADMIN_MEMBER_SEARCH_EX_ADMIN_NOTES = 522010,
            PAGE_ADMIN_MEMBER_SEARCH_EX_CHANGE_EMAIL = 522020,
            PAGE_MEMBERSEARCH_VIEWEMAILACTIVITY = 522030,
            PAGE_ADMIN_MEMBER_SEARCH_EX_INSERT_NOTE = 522040,
            PAGE_ADMIN_MEMBER_SEARCH_EX_PASSWORD_LOOKUP = 522050,
            PAGE_ADMIN_MEMBER_SEARCH_EX_SAVE_GLOBALSTATUSMASK = 522060,
            PAGE_MEMBERSEARCH_VIEWADMINACTIONLOG = 522070,
            PAGE_ADMIN_MEMBER_SEARCH_EX_RESULTS = 522080,
            PAGE_ADMIN_MEMBER_SEARCH_EX_CLOSE_WINDOW = 522090,
            ACTION_ADMINMEMBER_SEARCH_EX_VALIDATE_SEARCH = 522500,
            ACTION_ADMINMEMBER_SEARCH_EX_INSERT_NOTE = 522510,
            PAGE_MEMBERSEARCH_SENDPASSWORD = 522520,
            ACTION_ADMINMEMBER_SEARCH_EX_SEND_WELCOME_EMAIL = 522530,
            PAGE_MEMBERSEARCH_SENDVERIFICATIONLETTER = 522540,
            ACTION_ADMINMEMBER_SEARCH_EX_SAVE_GLOBALSTATUSMASK = 522550,
            ACTION_ADMINMEMBER_SEARCH_EX_CHANGE_EMAIL = 522560,
            PAGE_ADMIN_REPORTS_HOME = 523010,
            PAGE_ADMIN_REPORTS_PROMOTION_SALES = 523050,
            PAGE_ADMIN_REPORTS_SALES_RESULTS = 523060,
            PAGE_REPORT_PROMOTIONSALES = 523070,
            PAGE_REPORT_SALES = 523080,
            PAGE_ADMIN_REPORTS_PROMOTION_REGISTRATIONS = 523090,
            PAGE_ADMIN_REPORTS_PROMOTION_REGISTRATIONS_RESULTS = 523100,
            PAGE_REPORT_PROMOTERSALES = 523110,
            PAGE_ADMIN_REPORTS_PROMOTER_SALES_RESULTS = 523120,
            PAGE_ADMIN_REPORTS_PROMOTER_REGISTRATIONS = 523130,
            PAGE_ADMIN_REPORTS_PROMOTER_REGISTRATIONS_RESULTS = 523140,
            PAGE_ADMIN_REPORTS_FREE_TEXT = 523150,
            PAGE_ADMIN_REPORTS_FREE_TEXT_RESULTS = 523160,
            PAGE_ADMIN_REPORTS_ADMIN_ACTION_LOG = 523170,
            PAGE_ADMIN_REPORTS_ADMIN_ACTION_LOG_RESULTS = 523180,
            PAGE_REPORT_APPROVEQUEUESTATS = 523190,
            PAGE_ADMIN_REPORTS_APPROVE_QUEUE_STATS_RESULTS = 523200,
            PAGE_ADMIN_REPORTS_SITE_ACTIVITY = 523210,
            PAGE_ADMIN_REPORTS_MEMBER_ONLINE_COUNTS = 523220,
            PAGE_ADMIN_REPORTS_EMAIL_COUNTS = 523230,
            ACTION_ADMINREPORTS_SALES = 523510,
            ACTION_ADMINREPORTS_PROMOTION_SALES = 523530,
            ACTION_ADMINREPORTS_PROMOTION_REGISTRATIONS = 523540,
            ACTION_ADMINREPORTS_PROMOTER_SALES = 523550,
            ACTION_ADMINREPORTS_PROMOTER_REGISTRATIONS = 523560,
            ACTION_ADMINREPORTS_FREE_TEXT = 523570,
            ACTION_ADMINREPORTS_ADMIN_LOG = 523580,
            ACTION_ADMINREPORTS_APPROVE_QUEUE_STATS = 523590,
            ACTION_ADMINREPORTS_EMAIL_COUNTS = 523600,
            ACTION_ADMINREPORTS_SALES_PL = 523610,
            PAGE_ADMIN_NON_ACTIONPAGE_PHOTO_EDITOR = 524000,
            PAGE_ADMIN_NON_ACTIONPAGE_MINI_PROFILE = 524010,
            PAGE_ADMIN_NON_ACTIONPAGE_MEMBER_SEARCH_FORM = 524020,
            PAGE_ADMIN_NON_ACTIONPAGE_GALLERY = 524030,
            PAGE_ADMIN_NON_ACTIONPAGE_MEMBER_PROFILE_EDIT = 524040,
            PAGE_ADMIN_NON_ACTIONPAGE_MEMBER_STATUS = 524050,
            PAGE_ADMIN_NON_ACTIONPAGE_MEMBER_ONLINE_PROFILE = 524060,
            PAGE_ADMIN_NON_ACTIONPAGE_ADMIN_PROFILE = 524070,
            PAGE_FREETEXTAPPROVAL_APPROVE = 525000,
            PAGE_FREETEXTAPPROVAL_EDIT = 525010,
            PAGE_ADMIN_FREE_TEXT_APPROVAL_SAVE_CONFIRMATION = 525020,
            PAGE_ADMIN_FREE_TEXT_APPROVAL_CONFIRM_PAGE = 525030,
            ACTION_ADMINFREE_TEXT_APPROVAL_SAVE = 525500,
            ACTION_ADMINFREE_TEXT_APPROVAL_SUSPEND = 525510,
            ACTION_ADMINFREE_TEXT_APPROVAL_CHECK_IN = 525520,
            ACTION_ADMINFREE_TEXT_APPROVAL_SAVE_ATTRIBUTE = 525530,
            PAGE_ADMIN_REGION_TEST_PROFILE = 526000,
            ACTION_ADMINREGION_TEST_PROFILE = 526500,
            PAGE_ADMIN_ATTRIBUTE_GROUPS_DEFAULT = 527000,
            PAGE_ADMIN_ATTRIBUTE_GROUPS_LIST_ATTRIBUTES = 527010,
            PAGE_ADMIN_ATTRIBUTE_GROUPS_SAVE = 527020,
            ACTION_ADMINATTRIBUTE_GROUPS_SAVE = 527500,
            ACTION_ADMINATTRIBUTE_GROUPS_DELETE = 527510,
            ACTION_ADMINATTRIBUTE_GROUPS_SAVE_ATTRIBUTES = 527520,
            PAGE_ADMIN_PRIVILEGES_CONFIG_DEFAULT = 528000,
            PAGE_ADMIN_PRIVILEGES_CONFIG_SAVE_PRIVILEGE = 528010,
            ACTION_ADMINPRIVILEGES_CONFIG_SAVE = 528500,
            ACTION_ADMINPRIVILEGES_CONFIG_DELETE = 528510,
            PAGE_ADMIN_GROUPS_CONFIG_DEFAULT = 529000,
            PAGE_ADMIN_GROUPS_CONFIG_SAVE = 529010,
            PAGE_ADMIN_GROUPS_CONFIG_SAVE_PRIVILEGES = 529020,
            ACTION_ADMINGROUPS_CONFIG_SAVE_PRIVILEGES = 529520,
            ACTION_ADMINGROUPS_CONFIG_DELETE = 529530,
            ACTION_ADMINGROUPS_CONFIG_SAVE = 529540,
            PAGE_ADMIN_SITE_STATUS_DEFAULT = 530000,
            PAGE_ADMIN_SITE_STATUS_REPORTS = 530010,
            PAGE_ADMIN_SITE_STATUS_LIST = 530020,
            ACTION_ADMINSITE_STATUS_RUN_REPORT = 530500,
            ACTION_ADMINSITE_STATUS_RUN_LIST = 530510,
            PAGE_ADMIN_DOMAIN_ALIASES_DEFAULT = 531000,
            PAGE_ADMIN_DOMAIN_ALIASES_SAVE = 531010,
            ACTION_ADMINDOMAIN_ALIASES_SAVE = 531500,
            ACTION_ADMINDOMAIN_ALIASES_DELETE = 531510,
            ACTION_ADMINDOMAIN_ALIASES_DISABLE = 531520,
            PAGE_ADMIN_POLLS_DEFAULT = 532000,
            PAGE_ADMIN_POLLS_SAVE = 532010,
            ACTION_ADMINPOLLS_DELETE = 532500,
            ACTION_ADMINPOLLS_ADD_ANSWERS = 532510,
            ACTION_ADMINPOLLS_SAVE = 532520,
            PAGE_ADMIN_EVENTS_DEFAULT = 533000,
            PAGE_ADMIN_EVENTS_EVENT = 533010,
            PAGE_ADMIN_EVENTS_PHOTOALBUM = 533020,
            PAGE_ADMIN_EVENTS_EVENT_INSERT = 533030,
            PAGE_ADMIN_EVENTS_EVENT_UPDATE = 533040,
            PAGE_ADMIN_EVENTS_PHOTOALBUM_INSERT = 533050,
            PAGE_ADMIN_EVENTS_PHOTOALBUM_UPDATE = 533060,
            ACTION_ADMINEVENTS_SAVE_EVENT = 533500,
            ACTION_ADMINEVENTS_SAVE_PHOTOALBUM = 533510,
            ACTION_ADMINEVENTS_DELETE_EVENT = 533520,
            ACTION_ADMINEVENTS_DELETE_PHOTOALBUM = 533530,
            ACTION_ADMINEVENTS_EDIT_EVENT = 533540,
            ACTION_ADMINEVENTS_EDIT_PHOTOALBUM = 533550,
            PAGE_ADMIN_PAGETEXT_DEFAULT = 534000,
            PAGE_ADMIN_PAGETEXT_SAVE = 534010,
            PAGE_ADMIN_PAGETEXT_VIEW_GROUPS = 534020,
            PAGE_ADMIN_PAGETEXT_SAVE_GROUP = 534030,
            ACTION_ADMINPAGETEXT_SAVE = 534500,
            ACTION_ADMINPAGETEXT_DELETE = 534510,
            ACTION_ADMINPAGETEXT_SAVE_GROUP = 534520,
            ACTION_ADMINPAGETEXT_DELETE_GROUP = 534530,
            PAGE_ADMIN_SAMPLES_FORM_TEST = 535000,
            ACTION_ADMINSAMPLES_FORM_SUBMIT = 535500,
            PAGE_ADMIN_PRIVATE_LABELS_DEFAULT = 537000,
            PAGE_ADMIN_PRIVATE_LABELS_SAVE = 537010,
            ACTION_ADMINPRIVATE_LABELS_SAVE = 537500,
            ACTION_ADMINPRIVATE_LABELS_DELETE = 537510,
            PAGE_ADMIN_FREE_TEXT_APPROVAL_EX_DEFAULT = 538000,
            PAGE_ADMIN_FREE_TEXT_APPROVAL_EX_SAVE_CONFIRMATION = 538010,
            ACTION_ADMINFREE_TEXT_APPROVAL_EX_SAVE = 538500,
            PAGE_EDITPROFILE_EDITPROFILE = 539000,
            PAGE_ADMINHOME_HOME = 540000,
            PAGE_MISC_PRIVILEGES = 541010,
        }


        #endregion


    }
}
