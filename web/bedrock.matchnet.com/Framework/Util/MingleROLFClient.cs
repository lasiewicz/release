﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using System.Web;

namespace Matchnet.Web.Framework.Util
{
    public class MingleROLFClient
    {
        private const string STATUS = "active";
        private const string RETURN_STATUS_PASSED = "OK";
        private const string RETURN_STATUS_FAILED = "BAD";
        private const string RETURN_STATUS_ERROR = "ERROR";
        private string _ROLFURL;
        private string _ROLFKey;
        private int _memberID;
        private Brand _brand;

        public int RegIP { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int GenderMask { get; set; }
        public string MaritalStatus { get; set; }
        public string PRM { get; set; }
        public string LGID { get; set; }
        public string PID { get; set; }
        public string EmailAddress { get; set; }
        public List<string> Ethnicity {get;set;}
        public string Religion { get; set; }
        public string ColorCode { get; set; }
        public string IOBlackBoxValue { get; set; }

        public MingleROLFClient(Brand brand, int memberID, string emailAddress)
        {
            _ROLFURL = RuntimeSettings.GetSetting("MINGLE_ROLF_URL");
            _ROLFKey = RuntimeSettings.GetSetting("MINGLE_ROLF_KEY");
            _brand = brand;
            _memberID = memberID;
            EmailAddress = emailAddress;
        }

        public ROLFResponse GetROLFResponse()
        {
            string webResponse = string.Empty;
            ROLFResponse response = null;

            bool makeROLFCall = Convert.ToBoolean(RuntimeSettings.GetSetting("MINGLE_ROLF_ENABLE_CALL", _brand.Site.Community.CommunityID, _brand.Site.SiteID, _brand.BrandID));
            if (!makeROLFCall)
            {
                response = new ROLFResponse();
                response.Code = 9999;
                response.Description = "CALL NOT ENABLED";
                response.Passed = true;
                response.Error = false;
                return response;
            }

            WebPostRequest request = new WebPostRequest(_ROLFURL, "POST", 2000);
            request.Add("key", _ROLFKey);
            request.Add("memberid", _memberID.ToString());
            request.Add("bhreportingsiteid", _brand.Site.SiteID.ToString());
            request.Add("profile_status", STATUS);
            request.Add("reg_ip_int", RegIP.ToString());
            request.Add("reg_location_country", Country);
            request.Add("reg_location_postalcode", PostalCode);
            request.Add("reg_firstname", FirstName);
            request.Add("reg_lastname", LastName);
            request.Add("reg_gender", GetGender(GenderMask));
            request.Add("reg_marital_status", MaritalStatus);
            request.Add("campaign_category", string.Empty);
            request.Add("campaign_source", PRM);
            request.Add("campaign_adid", string.Empty);
            request.Add("campaign_optional", LGID);
            request.Add("campaign_pid", PID);
            request.Add("reg_emailaddress", EmailAddress);
            request.Add("reg_religion", Religion);
            request.Add("reg_ethnicity", GetDelimitedEthnicityValue(Ethnicity));
            request.Add("reg_colorcode", ColorCode);
            request.Add("reg_iovation_bb", IOBlackBoxValue); 
             
            
            try
            {
                webResponse = request.GetResponse();
                response = ParseWebResponseIntoROLFResponse(webResponse);
            }
            catch (Exception ex)
            {
                if (HttpContext.Current != null && HttpContext.Current.Items["g"] != null)
                {
                    ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];
                    g.ProcessException(ex);
                }
                response = new ROLFResponse();
                response.Error = true;
                response.Passed = true;
                response.Code = Constants.NULL_INT;
                response.Description = "Error calling ROLF";
            }

            return response;

        }

        private string GetGender(int genderMask)
        {
            if ((genderMask & 1) == 1)
                return "M";
            else
                return "F";
        }

        private string GetDelimitedEthnicityValue(List<string> ethnicites)
        {
            if (ethnicites.Count == 1)
            {
                return ethnicites[0];
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                foreach (string ethnicity in ethnicites)
                {
                    if (ethnicity.ToLower().Trim().Equals("_blank")) continue;
                    if (sb.Length == 0)
                    {
                        sb.Append(ethnicity);
                    }
                    else
                    {
                        sb.Append("|" + ethnicity);
                    }
                }

                return sb.ToString();
            }
        }

        private ROLFResponse ParseWebResponseIntoROLFResponse(string webResponse)
        {
            ROLFResponse response = new ROLFResponse();
            string[] tokens = webResponse.Split(':');

            switch (tokens[0])
            {
                case RETURN_STATUS_PASSED:
                    response.Error = false;
                    response.Passed = true;
                    response.Code = int.Parse(tokens[1]);
                    response.Description = tokens[2];
                    break;
                case RETURN_STATUS_FAILED:
                    response.Error = false;
                    response.Passed = false;
                    response.Code = int.Parse(tokens[1]);
                    response.Description = tokens[2];
                    break;
                case RETURN_STATUS_ERROR:
                    response.Error = true;
                    response.Passed = false;
                    response.ErrorDescription = tokens[1];
                    break;
            }

            return response;
        }

    }
}
