using System.IO;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Image;
using Matchnet.Content.ValueObjects.Region;

namespace Matchnet.Web.Framework.Util
{
	/// <summary>
	/// Summary description for BrandUtils.
	/// </summary>
	public class BrandUtils
	{
		private BrandUtils() {}

		public static string RetrieveRegionString(int regionID, int languageID)
		{
			string retVal = "N/A";

			RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);
			retVal = regionLanguage.CityName + ", " + regionLanguage.StateDescription;

			return(retVal);
		}

		public static string GetDomainAbbreviation(int domainID)
		{
			string domainAbbreviation = "";

			switch (domainID)
			{
				case (int)WebConstants.COMMUNITY_ID.AmericanSingles:
					domainAbbreviation = "as";
					break;
				case (int)WebConstants.COMMUNITY_ID.Glimpse:
					domainAbbreviation = "gy";
					break;
				case (int)WebConstants.COMMUNITY_ID.JDate:
					domainAbbreviation = "jd";
					break;
				case (int)WebConstants.COMMUNITY_ID.College:
					domainAbbreviation = "cl";
					break;
				case (int)WebConstants.COMMUNITY_ID.Cupid:
					domainAbbreviation = "ci";
					break;
                case (int)WebConstants.COMMUNITY_ID.Mingle:
                    domainAbbreviation = "mn";
                    break;
			}

			return domainAbbreviation;
		}

		public static string GetSiteAbbreviation(int siteID)
		{
			string siteAbbreviation = "";

			switch (siteID)
			{
				case (int)WebConstants.SITE_ID.JDate:
					siteAbbreviation = "jd";
					break;
				case (int)WebConstants.SITE_ID.JDateCoIL:
					siteAbbreviation = "il";
					break;
                case (int)WebConstants.SITE_ID.JDateFR:
                    siteAbbreviation = "fr";
                    break;
				case (int)WebConstants.SITE_ID.JDateUK:
					siteAbbreviation = "jduk";
					break;
                case (int)WebConstants.SITE_ID.AmericanSingles:
					siteAbbreviation = "as";
					break;
				case (int)WebConstants.SITE_ID.DateCA:
					siteAbbreviation = "ca";
					break;
				case (int)WebConstants.SITE_ID.MatchnetUK:
					siteAbbreviation = "uk";
					break;
                case (int)WebConstants.SITE_ID.JewishMingle:
                    siteAbbreviation = "jm";
                    break;
			}

			return siteAbbreviation;
		}
	}
}
