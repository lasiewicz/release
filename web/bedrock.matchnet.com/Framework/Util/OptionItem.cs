﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Util
{
    [Serializable]
    public class OptionItem
    {
        public string Value { get; set; }
        public string Content { get; set; }
        public int ListOrder { get; set; }

    }
}
