﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Util
{
    public class DTODisposalModule : IHttpModule
    {
        private const string SEPARATOR = "^";
        private HttpApplication httpApp;
        private SessionManager _sessionManager = null;

        public SessionManager SessionManager
        {
            get
            {
                if (_sessionManager == null)
                {
                    _sessionManager = new SessionManager();
                }
                return _sessionManager;
            }
            set { _sessionManager = value; }
        }

        public void Init(HttpApplication httpApp)
        {
            this.httpApp = httpApp;
            httpApp.BeginRequest += new EventHandler(this.Setup);
            httpApp.EndRequest += new EventHandler(this.DisposeDTOs);
            httpApp.Error += new EventHandler(this.DisposeDTOs);
        }

        void Setup(object sender, EventArgs a)
        {
            HttpContext Context = HttpContext.Current;
            HttpRequest Request = Context.Request;
            if (Request["p"] != null || (Request[WebConstants.URL_PARAMETER_NAME_SEARCHTYPEID] != null && Request.RawUrl.IndexOf("/Applications/Search/SearchResults.aspx") < 0))
            {
                return;
            }

            if (ProceedWithRequest(Context))
            {
                ContextGlobal g = (null != Context && null != Context.Items["g"]) ? Context.Items["g"] as ContextGlobal : new ContextGlobal(Context);
                UserSession session = (null != g) ? SessionManager.GetCurrentSession(g.Brand) : SessionManager.GetSessionFromID();
                if (null != session)
                {
                    Context.Items[WebConstants.REQUEST_SCOPED_DTO_KEY] = session.Key.ToString() + SEPARATOR + g.ClientIP + SEPARATOR + g.RandomNumber;
                }
            }
        }

        void DisposeDTOs(object sender, EventArgs a)
        {
            HttpContext Context = HttpContext.Current;
            if (ProceedWithRequest(Context))
            {
                if (null != Context)
                {
                    MemberDTOManager.Instance.DisposeDTOs((string) Context.Items[WebConstants.REQUEST_SCOPED_DTO_KEY]);
                }
            }
        }

        private bool ProceedWithRequest(HttpContext Context)
        {
            bool shouldProceed = true;
            HttpRequest Request = Context.Request;
            string requestPath = Request.Path.ToLower();
            string checkForFriendlyUrl = string.Empty;


            checkForFriendlyUrl = FrameworkGlobals.CheckForFriendlyURL();

            bool isFriendlyUrl = false;

            if (!string.IsNullOrEmpty(checkForFriendlyUrl) && (
                                                                  checkForFriendlyUrl.Contains("religious-dating") ||
                                                                  checkForFriendlyUrl.Contains("academic-dating") ||
                                                                  checkForFriendlyUrl.Contains("religious-dating-profiles") ||
                                                                  checkForFriendlyUrl.Contains("academic-dating-profiles") ||
                                                                  checkForFriendlyUrl.Contains("Femmes-juives") ||
                                                                  checkForFriendlyUrl.Contains("Les-hommes-juifs") ||
                                                                  checkForFriendlyUrl.Contains("student-dating-signup") ||
                                                                  checkForFriendlyUrl.Contains("religious-dating-signup") ||
                                                                  checkForFriendlyUrl.Contains("Divorced-dating") ||
                                                                  checkForFriendlyUrl.Contains("Senior-dating") ||
                                                                  checkForFriendlyUrl.Contains("dating-for-divorced-signup") ||
                                                                  checkForFriendlyUrl.Contains("silver-dating-signup")))
            {
                isFriendlyUrl = true;
            }

            // Return if this is a special page
            if (!isFriendlyUrl)
            {
                shouldProceed = ValidPath(requestPath);
                if (shouldProceed)
                {
                    try
                    {
                        string rawRequestPath = Request.RawUrl.Split('?')[0];
                        shouldProceed = ValidPath(rawRequestPath.ToLower());
                    }
                    catch (Exception e)
                    {
                        //swallow?
                        shouldProceed = false;
                    }
                }
            }
            return shouldProceed;
        }

        private bool ValidPath(string requestPath)
        {
            bool shouldProceed = true;
            if (!requestPath.EndsWith(".aspx") ||
                requestPath.StartsWith("/fbchannel") ||
                requestPath.EndsWith("404form.aspx") ||
                requestPath.EndsWith("cachemanager.aspx") ||
                requestPath.EndsWith("tag-744.aspx") ||
                requestPath.EndsWith("nophoto.aspx") ||
                requestPath.EndsWith("mmimagedisplay.aspx") ||
                requestPath.EndsWith("searchstoreview.aspx") ||
                requestPath.EndsWith("checkim.aspx") ||
                requestPath.EndsWith("updatemembersonline.aspx") ||
                requestPath.EndsWith("get_aspx_ver.aspx")) // This is a special page used by dev studio
            {
                if (requestPath.StartsWith("/applications/api/favorites.asmx") ||
                    requestPath.StartsWith("/applications/api/searchresults.asmx"))
                {
                    shouldProceed = true;
                }
                else
                {
                    shouldProceed = false;
                }
            }
            return shouldProceed;
        }

        public void Dispose()
        {
            this.SessionManager = null;
            this.httpApp = null;
        }
    }
}