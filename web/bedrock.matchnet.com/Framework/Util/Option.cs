using System.Data;
using System.Text;
using System.Collections.Generic;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Globalization;
using System;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Util
{
    /// <summary>
    /// Summary description for Option.
    /// </summary>
    public class Option
    {
        public Option()
        {

        }

        public static DataTable GetOptions(string pAttributeName, int pCommunityID, int pSiteID, int pBrandID, ContextGlobal g)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(pAttributeName, pCommunityID, pSiteID, pBrandID);

            DataTable dt = new DataTable();
            dt.Columns.Add("Value", typeof(System.String));
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("ListOrder", typeof(System.Int32));

            if (attributeOptionCollection == null)
                return dt;

            foreach (AttributeOption attributeOption in attributeOptionCollection)
            {
                DataRow row = dt.NewRow();

                if (attributeOption.Value != Constants.NULL_INT)
                {
                    row["Value"] = attributeOption.Value;
                }
                else
                {
                    row["Value"] = string.Empty;
                }

                row["Content"] = g.GetResource(attributeOption.ResourceKey);
                row["ListOrder"] = attributeOption.ListOrder;

                dt.Rows.Add(row);
            }

            return dt;
        }


        public static DataTable GetOptions(string pAttributeName, ContextGlobal g)
        {
            return GetOptions(pAttributeName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID, g);
        }

        public static List<OptionItem> GetOptionItems(string pAttributeName, ContextGlobal g)
        {
            DataTable dtOptions = GetOptions(pAttributeName, g);
            List<OptionItem> optionItems = new List<OptionItem>();
            if (dtOptions != null && dtOptions.Rows != null)
            {
                foreach (DataRow dr in dtOptions.Rows)
                {
                    OptionItem oi = new OptionItem();
                    oi.Value = dr["Value"].ToString();
                    oi.Content = dr["Content"].ToString();
                    oi.ListOrder = Convert.ToInt32(dr["ListOrder"]);
                    optionItems.Add(oi);
                }
            }
            return optionItems;
        }

        public static DataTable GetMoreChildrenOptions(ContextGlobal g, System.Web.UI.Control ResourceControl)
        {
            DataTable dtDesiredMoreChildren = new DataTable();
            dtDesiredMoreChildren.Columns.Add("Value", typeof(System.String));
            dtDesiredMoreChildren.Columns.Add("Content", typeof(System.String));
            dtDesiredMoreChildren.Columns.Add("ListOrder", typeof(System.Int32));

            DataRow rowBlank = dtDesiredMoreChildren.NewRow();
            rowBlank["Value"] = "";
            rowBlank["Content"] = "";
            rowBlank["ListOrder"] = 0;
            dtDesiredMoreChildren.Rows.Add(rowBlank);

            DataRow rowNo = dtDesiredMoreChildren.NewRow();
            rowNo["Value"] = 0;
            rowNo["Content"] = g.GetResource("MORECHILDREN_NO", ResourceControl);
            rowNo["ListOrder"] = 1;
            dtDesiredMoreChildren.Rows.Add(rowNo);

            DataRow rowYes = dtDesiredMoreChildren.NewRow();
            rowYes["Value"] = 1;
            rowYes["Content"] = g.GetResource("MORECHILDREN_YES", ResourceControl);
            rowYes["ListOrder"] = 2;
            dtDesiredMoreChildren.Rows.Add(rowYes);

            DataRow rowNotsure = dtDesiredMoreChildren.NewRow();
            rowNotsure["Value"] = 2;
            rowNotsure["Content"] = g.GetResource("MORECHILDREN_UNSURE", ResourceControl);
            rowNotsure["ListOrder"] = 3;
            dtDesiredMoreChildren.Rows.Add(rowNotsure);

            return dtDesiredMoreChildren;
        }

        public static DataTable GetDesiredMoreChildrenOptions(ContextGlobal g, System.Web.UI.Control ResourceControl)
        {
            DataTable dtDesiredMoreChildren = new DataTable();
            dtDesiredMoreChildren.Columns.Add("Value", typeof(System.String));
            dtDesiredMoreChildren.Columns.Add("Content", typeof(System.String));
            dtDesiredMoreChildren.Columns.Add("ListOrder", typeof(System.Int32));

            DataRow rowBlank = dtDesiredMoreChildren.NewRow();
            rowBlank["Value"] = "";
            rowBlank["Content"] = "";
            rowBlank["ListOrder"] = 0;
            dtDesiredMoreChildren.Rows.Add(rowBlank);

            DataRow rowYes = dtDesiredMoreChildren.NewRow();
            rowYes["Value"] = 1;
            rowYes["Content"] = g.GetResource("MORECHILDREN_YES", ResourceControl);
            rowYes["ListOrder"] = 1;
            dtDesiredMoreChildren.Rows.Add(rowYes);

            DataRow rowNo = dtDesiredMoreChildren.NewRow();
            rowNo["Value"] = 0;
            rowNo["Content"] = g.GetResource("MORECHILDREN_NO", ResourceControl);
            rowNo["ListOrder"] = 2;
            dtDesiredMoreChildren.Rows.Add(rowNo);

            DataRow rowNotsure = dtDesiredMoreChildren.NewRow();
            rowNotsure["Value"] = 2;
            rowNotsure["Content"] = g.GetResource("MORECHILDREN_UNSURE", ResourceControl);
            rowNotsure["ListOrder"] = 3;
            dtDesiredMoreChildren.Rows.Add(rowNotsure);

            DataRow rowDoesNotMatter = dtDesiredMoreChildren.NewRow();
            rowDoesNotMatter["Value"] = 4;
            rowDoesNotMatter["Content"] = g.GetResource("MORECHILDREN_DOES_NOT_MATTER", ResourceControl);
            rowDoesNotMatter["ListOrder"] = 4;
            dtDesiredMoreChildren.Rows.Add(rowDoesNotMatter);

            return dtDesiredMoreChildren;
        }

        public static DataTable GetChildrenCountOptions(ContextGlobal g)
        {
            //return GetOptions(pAttributeName, g.Brand.Site.Community.CommunityID,  g.Brand.Site.SiteID, g.Brand.BrandID,  g);

            DataTable dt = new DataTable();
            dt.Columns.Add("Value", typeof(System.String));
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("ListOrder", typeof(System.Int32));

            DataRow row = dt.NewRow();

            row["Value"] = Constants.NULL_INT.ToString();
            row["Content"] = g.GetResource("ANY_");
            row["ListOrder"] = 0;
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["Value"] = "1";
            row["Content"] = g.GetResource("ATTOPT_370_0");
            row["ListOrder"] = 1;

            dt.Rows.Add(row);

            row = dt.NewRow();
            row["Value"] = "2";
            row["Content"] = g.GetResource("ATTOPT_370_1");
            row["ListOrder"] = 2;
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["Value"] = "4";
            row["Content"] = g.GetResource("ATTOPT_370_2");
            row["ListOrder"] = 3;
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["Value"] = "8";
            row["Content"] = g.GetResource("ATTOPT_370_3_OR_MORE");
            row["ListOrder"] = 4;
            dt.Rows.Add(row);

            return dt;

        }
        public static string GetContent(string pAttributeName, int pAttributeValue, ContextGlobal g)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(pAttributeName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            AttributeOption attributeOption = attributeOptionCollection[pAttributeValue];

            if (attributeOption != null)
                return g.GetResource(attributeOption.ResourceKey);

            return string.Empty;
        }

        public static string GetContent(string pAttributeName, int pAttributeValue, ResourceManager resourceManager, Brand brand)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(pAttributeName, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            AttributeOption attributeOption = attributeOptionCollection[pAttributeValue];

            if (attributeOption != null)
                return resourceManager.GetResource(attributeOption.ResourceKey);

            return string.Empty;
        }

        public static string GetContent(string pAttributeName, int pAttributeValue, int pDefaultListOrder, ContextGlobal g)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(pAttributeName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            AttributeOption attributeOption = attributeOptionCollection[pAttributeValue];

            if (attributeOption != null)
                return g.GetResource(attributeOption.ResourceKey);

            attributeOption = attributeOptionCollection.FindByListOrder(pDefaultListOrder);
            if (null != attributeOption)
            {
                return g.GetResource(attributeOption.ResourceKey);
            }

            return string.Empty;
        }

        /// <summary>
        /// This method should be used when ContextGlobal is not available. This is written for GetResource API page which is being
        /// used for the Messmo integration.
        /// </summary>
        /// <param name="pAttributeName"></param>
        /// <param name="pAttributeValue"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public static string GetContent(string pAttributeName, int pAttributeValue, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(pAttributeName, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            AttributeOption attributeOption = attributeOptionCollection[pAttributeValue];

            if (attributeOption != null)
                return Localizer.GetStringResourceWithNoTokeMap(attributeOption.ResourceKey, null, brand);

            return string.Empty;
        }

        public static string GetDescription(string attributeName, int attributeValue, ContextGlobal g)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            AttributeOption attributeOption = attributeOptionCollection[attributeValue];

            if (attributeOption != null)
                return attributeOption.Description.ToString();

            return string.Empty;
        }

        public static List<string> GetDescriptions(string attributeName, int attributeMaskValue, ContextGlobal g)
        {
            List<string> descriptions = new List<string>();

            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

            if (attributeOptionCollection == null)
                return descriptions;

            foreach (AttributeOption attributeOption in attributeOptionCollection)
            {
                if (attributeOption.MaskContains(attributeMaskValue))
                {
                    descriptions.Add(attributeOption.Description.ToString());
                }
            }

            return descriptions;
        }

        public static string GetChildrenCountMaskContent(int pMaskValue, ContextGlobal g)
        {
            StringBuilder sb = new StringBuilder();
            foreach (DataRow row in GetChildrenCountOptions(g).Rows)
            {
                bool addResource = false;
                int Value = int.Parse(row["Value"].ToString());
                if (pMaskValue == int.MinValue || pMaskValue == Constants.NULL_INT)
                {
                    if (Value == int.MinValue || Value == Constants.NULL_INT)
                        addResource = true;
                }
                else
                {
                    addResource = ((pMaskValue & Value) == Value);
                }

                if (!addResource) continue;
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }
                sb.Append((string)row["Content"]);
            }

            return sb.ToString();
        }

        public static string GetMaskContent(string pAttributeName, int pMaskValue, ContextGlobal g)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(pAttributeName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            StringBuilder sb = new StringBuilder();

            foreach (AttributeOption attributeOption in attributeOptionCollection)
            {
                if (attributeOption.MaskContains(pMaskValue))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(", ");
                    }
                    sb.Append(g.GetResource(attributeOption.ResourceKey));
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// This method should be used when ContextGlobal is not available. This is written for GetResource API page which is being
        /// used for the Messmo integration.
        /// </summary>
        /// <param name="pAttributeName"></param>
        /// <param name="pMaskValue"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public static string GetMaskContent(string pAttributeName, int pMaskValue, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(pAttributeName, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            StringBuilder sb = new StringBuilder();

            foreach (AttributeOption attributeOption in attributeOptionCollection)
            {
                if (attributeOption.MaskContains(pMaskValue))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(", ");
                    }
                    sb.Append(Localizer.GetStringResourceWithNoTokeMap(attributeOption.ResourceKey, null, brand));
                }
            }

            return sb.ToString();
        }
    }
}
