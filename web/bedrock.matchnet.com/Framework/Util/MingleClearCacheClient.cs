﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using System.Web;

namespace Matchnet.Web.Framework.Util
{
    public class MingleClearCacheClient
    {
        private string _ClearCacheURL;
        private string _ClearCacheKey;
        private int _memberID;
        private Brand _brand;

        public MingleClearCacheClient(Brand brand, int memberID)
        {
            _ClearCacheURL = RuntimeSettings.GetSetting("MINGLE_CLEAR_CACHE_URL");
            _ClearCacheKey = RuntimeSettings.GetSetting("MINGLE_CLEAR_CACHE_KEY");
            _brand = brand;
            _memberID = memberID;
        }

        public string GetClearCacheResponse()
        {
            string response = null;

            string siteIDs = RuntimeSettings.GetSetting("MINGLE_CLEAR_CACHE_SITE_IDS");
            if (string.IsNullOrEmpty(siteIDs.Trim()))
            {
                return string.Empty;
            }

            string[] sitesArray = siteIDs.Split(new char[] { ',' });

            foreach (string siteID in sitesArray)
            {
                string requestURL = _ClearCacheURL + string.Format("?key={0}&siteid={1}&memberid={2}", _ClearCacheKey, siteID, _memberID);

                WebPostRequest request = new WebPostRequest(requestURL, "GET", 2000);
                request.Add("key", _ClearCacheKey);
                request.Add("memberid", _memberID.ToString());
                request.Add("siteid", siteID);

                try
                {
                    string webResponse = request.GetGETResponse();
                    response += GetTextFromResponse(webResponse) + " siteID=" + siteID + "\n";

                }
                catch (Exception ex)
                {
                    if (HttpContext.Current != null && HttpContext.Current.Items["g"] != null)
                    {
                        ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];
                        g.ProcessException(ex);
                    }

                    response += "An error has occurred while trying to clear Mingle's cache." + " siteID=" + siteID + "\n";
                }
            }

            return response;
        }

        private string GetTextFromResponse(string webResponse)
        {
            string result = string.Empty;

            switch (webResponse)
            {
                case "-1": result = "You didn't pass in all of the GET parameters or the key didn't match."; break;
                case "0": result = "An error occurred on Mingle's end."; break;
                case "1": result = "The cache was cleared for this member."; break;
                case "An application error has occurred": result = "A system failure has occured on Mingle."; break;
                default:
                    result = "Unknown response: " + webResponse; break;
            }

            return result;
        }
    }
}
