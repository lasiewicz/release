using System;
using System.Web;

using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Web.Framework.Util
{
    /// <summary>
    /// Summary description for Redirect.
    /// </summary>
    public class Redirect
    {
        public const string SUBSCRIBE_URL = "/Applications/Subscription/Subscribe.aspx";
        public const string VARIABLE_PRICING_UPSALE_URL = "/Applications/Subscription/Upsale.aspx";
        public const string FIXED_PRICING_UPSALE_URL = "/Applications/Subscription/FixedPricingUpsale.aspx";

        public Redirect()
        {
        }

        public static void Subscription(Brand pBrand)
        {
            Subscription(pBrand, Constants.NULL_INT, Constants.NULL_INT);
        }

        public static void Subscription(Brand pBrand, int pPurchaseReasonTypeID, int pSourceID)
        {
            Subscription(pBrand, pPurchaseReasonTypeID, pSourceID, false);
        }

        public static void Subscription(Brand pBrand, int pPurchaseReasonTypeID, int pSourceID, bool pOverrideTransactionHistory)
        {
            Subscription(pBrand, pPurchaseReasonTypeID, pSourceID, pOverrideTransactionHistory, null);
        }

        public static void Subscription(Brand pBrand, int pPurchaseReasonTypeID, int pSourceID, bool pOverrideTransactionHistory, string pDestinationURL)
        {
            ParameterCollection parameterCollection = new ParameterCollection();

            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];

            if (g.ImpersonateContext)
            {
                parameterCollection.Add(WebConstants.IMPERSONATE_MEMBERID, g.TargetMemberID.ToString());
                parameterCollection.Add(WebConstants.IMPERSONATE_BRANDID, g.TargetBrandID.ToString());
            }

            Subscription(pBrand, pPurchaseReasonTypeID, pSourceID, pOverrideTransactionHistory, pDestinationURL, parameterCollection);
        }


        //for compose project
        public static void Subscription(Brand pBrand, int pPurchaseReasonTypeID, int pSourceID, bool pOverrideTransactionHistory, string pDestinationURL, int pMemberMailID)
        {
            ParameterCollection parameterCollection = new ParameterCollection();

            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];

            if (g.ImpersonateContext)
            {
                parameterCollection.Add(WebConstants.IMPERSONATE_MEMBERID, g.TargetMemberID.ToString());
                parameterCollection.Add(WebConstants.IMPERSONATE_BRANDID, g.TargetBrandID.ToString());
            }
            parameterCollection.Add(WebConstants.URL_PARAMETER_NAME_MEMBERMAILID, pMemberMailID.ToString());

            Subscription(pBrand, pPurchaseReasonTypeID, pSourceID, pOverrideTransactionHistory, pDestinationURL, parameterCollection);
        }

        public static void Subscription(Brand pBrand, int pPurchaseReasonTypeID, int pSourceID, bool pOverrideTransactionHistory, string pDestinationURL, ParameterCollection parameterCollection)
        {
            string subURL = GetSubscriptionUrl(pBrand, pPurchaseReasonTypeID, pSourceID, pOverrideTransactionHistory, pDestinationURL, parameterCollection);

            HttpContext.Current.Response.Redirect(subURL);
        }

        public static string GetSubscriptionUrl(Brand pBrand, int pPurchaseReasonTypeID, int pSourceID, bool pOverrideTransactionHistory, string pDestinationURL, ParameterCollection parameterCollection)
        {
            if (pPurchaseReasonTypeID != Constants.NULL_INT)
            {
                parameterCollection.Add("prtid", pPurchaseReasonTypeID.ToString());
            }
            if (pSourceID != Constants.NULL_INT)
            {
                parameterCollection.Add("srid", pSourceID.ToString());
            }
            if (pOverrideTransactionHistory)
            {
                parameterCollection.Add("OverrideTransactionHistory", "1");
            }
            if (pDestinationURL != null)
            {
                parameterCollection.Add("DestinationURL", pDestinationURL);
            }

            string subURL = LinkFactory.Instance.GetLink(SUBSCRIBE_URL, parameterCollection, pBrand, HttpContext.Current.Request.Url.ToString());
            return subURL;
        }

        public static void Upsale(Brand pBrand, int pPurchaseReasonTypeID, int pSourceID, bool pOverrideTransactionHistory, string pDestinationURL, int pMemberMailID, bool isVariablePricing)
        {
            ParameterCollection parameterCollection = new ParameterCollection();

            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];

            if (g.ImpersonateContext)
            {
                parameterCollection.Add(WebConstants.IMPERSONATE_MEMBERID, g.TargetMemberID.ToString());
                parameterCollection.Add(WebConstants.IMPERSONATE_BRANDID, g.TargetBrandID.ToString());
            }
            parameterCollection.Add(WebConstants.URL_PARAMETER_NAME_MEMBERMAILID, pMemberMailID.ToString());

            Upsale(pBrand, pPurchaseReasonTypeID, pSourceID, pOverrideTransactionHistory, pDestinationURL, parameterCollection, isVariablePricing);
        }

        public static void Upsale(Brand pBrand, int pPurchaseReasonTypeID, int pSourceID, bool pOverrideTransactionHistory, string pDestinationURL, ParameterCollection parameterCollection, bool isVariablePricing)
        {
            string subURL = GetUpsaleUrl(pBrand, pPurchaseReasonTypeID, pSourceID, pOverrideTransactionHistory, pDestinationURL, parameterCollection, isVariablePricing);

            if (string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["composeoverlay"]))
            {
                HttpContext.Current.Response.Redirect(subURL);
            }
            else
            {
                HttpContext.Current.Response.Write(
                    "<html><head></head><body><script  type='text/javascript'> top.location = \"" + subURL + "\"; </script></body><html>");
                HttpContext.Current.Response.End();
            }
        }

        public static string GetUpsaleUrl(Brand pBrand, int pPurchaseReasonTypeID, int pSourceID, bool pOverrideTransactionHistory, string pDestinationURL, ParameterCollection parameterCollection, bool isVariablePricing)
        {
            if (pPurchaseReasonTypeID != Constants.NULL_INT)
            {
                parameterCollection.Add("prtid", pPurchaseReasonTypeID.ToString());
            }
            if (pSourceID != Constants.NULL_INT)
            {
                parameterCollection.Add("srid", pSourceID.ToString());
            }
            if (pOverrideTransactionHistory)
            {
                parameterCollection.Add("OverrideTransactionHistory", "1");
            }
            if (pDestinationURL != null)
            {
                parameterCollection.Add("DestinationURL", pDestinationURL);
            }

            string url = String.Empty;
            if (isVariablePricing)
            {
                url = VARIABLE_PRICING_UPSALE_URL;
            }
            else
            {
                url = FIXED_PRICING_UPSALE_URL;
            }

            string subURL = LinkFactory.Instance.GetLink(url, parameterCollection, pBrand, HttpContext.Current.Request.Url.ToString());
            return subURL;
        }




    }
}
