﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.SAL;

namespace Matchnet.Web.Framework.Util
{
    public class MemberSlideshowSettings
    {
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int RegionID { get; set; }
        public SearchType SearchType { get; set; }
        public int AreaCode1 { get; set; }
        public int AreaCode2 { get; set; }
        public int AreaCode3 { get; set; }
        public int AreaCode4 { get; set; }
        public int AreaCode5 { get; set; }
        public int AreaCode6 { get; set; }

        public MemberSlideshowSettings()
        {
            SearchType = SearchType.PostalCode;
        }

        public MemberSlideshowSettings(int minAge, int maxAge)
        {
            MinAge = minAge;
            MaxAge = maxAge;
        }

        public MemberSlideshowSettings(int minAge, int maxAge, int regionID, SearchType searchType,
            int areaCode1, int areaCode2, int areaCode3, int areaCode4, int areaCode5, int areaCode6)
        {
            MinAge = minAge;
            MaxAge = maxAge;
            RegionID = regionID;
            SearchType = searchType;
            AreaCode1 = areaCode1;
            AreaCode2 = areaCode2;
            AreaCode3 = areaCode3;
            AreaCode4 = areaCode4;
            AreaCode5 = areaCode5;
            AreaCode6 = areaCode6;
        }

    }
}
