﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Interfaces;
using System.Collections.Specialized;
using System.Text;

namespace Matchnet.Web.Framework.HTTPContextWrappers
{
    public class CurrentRequest:ICurrentRequest
    {
        #region ICurrentContext Members

        private string _clientIP = string.Empty;

        public NameValueCollection QueryString
        {
            get { return HttpContext.Current.Request.QueryString; }
        }

        public NameValueCollection Form
        {
            get { return HttpContext.Current.Request.Form; }
        }

        public Uri Url
        {
            get { return HttpContext.Current.Request.Url; }
        }

        public string RawUrl
        {
            get { return HttpContext.Current.Request.RawUrl; }
        }

        public string UrlReferrer
        {
            get { return HttpContext.Current.Request.UrlReferrer == null ? string.Empty : HttpContext.Current.Request.UrlReferrer.ToString(); }
        }

        public string this[string index]
        {
            get { return HttpContext.Current.Request[index]; }
        }

        public string ClientIP
        {
            get
            {
                if (string.IsNullOrEmpty(_clientIP))
                {
                    _clientIP = HttpContext.Current.Request.Headers["client-ip"];
                    if (_clientIP == null)
                    {
                        _clientIP = HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
                    }

                    Int32 commaPos = _clientIP.IndexOf(",");
                    if (commaPos > -1)
                    {
                        _clientIP = _clientIP.Substring(0, commaPos);
                    }
                }

                return _clientIP;
            }
        }

        #endregion

        public string GetUserAgent()
        {
            return HttpContext.Current.Request.UserAgent;
        }

        public string GetHeaders()
        {
            var sb = new StringBuilder();

            foreach (var key in HttpContext.Current.Request.Headers.AllKeys)
            {
                var separtor = sb.Length > 0 ? ";" : "";
                sb.Append(string.Format("{0}{1}:{2}", separtor, key, HttpContext.Current.Request.Headers[key]));
            }

            return sb.ToString();
        }
    }



}