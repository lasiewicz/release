﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JQValidator.ascx.cs" Inherits="Matchnet.Web.Framework.Validation.JQValidator" %>
	<script type="text/javascript" src="/Framework/Validation/validation-plugin.js"></script>	
	
<script type="text/javascript">
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Please enter letters only.");

    jQuery.validator.addMethod("usstates", function(value, element) {
        return this.optional(element) || /^(?:(A[KLRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|P[AR]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY]))$/i.test(value);
    }, "Please enter a valid US state abbreviation.");

    jQuery.validator.addMethod("phone", function(phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
		phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
    }, "Please enter a valid phone number");

    jQuery.validator.addMethod("selectNone", function(value, element) {
        if (element.value == "none") { return false; } else return true;
    }, "Please select an option.");
    
      jQuery.validator.addMethod("alphanum", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
    }, "Please enter letters only.");
        jQuery.validator.addMethod("email", function(value, element) {
        return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-].)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)+$/i.test(value);
    }, "Please enter email address.");


    jQuery(document).ready(function() {
        jQuery("#aspnetForm").validate({
        rules:
            {
            <asp:Literal id="litRules" runat="server" />
            },
            messages: {
            <asp:Literal id="litMessages" runat="server" />
            },
            errorElement: "em",
            success: function(label) {
                label.addClass("success");
            }
        });
    });
    
    
    jQuery(document).ready(function() {
	jQuery('.dl-form input, .dl-form select, .dl-form textarea').focus(function(event) {
		jQuery(this).addClass('current-focus');
		jQuery(this).parent().find('.tip').clone().appendTo('#move-tips-here');
	});
	jQuery('.dl-form input, .dl-form select, .dl-form textarea').blur(function(event) {
		jQuery(this).removeClass('current-focus');
		jQuery('#move-tips-here').find('.tip').remove();
	});
	
	//jQuery('#nameFirst').focus();
});
</script>