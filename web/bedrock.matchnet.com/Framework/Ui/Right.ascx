﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Right.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.Right" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" TagName="ActivityCenter" Src="/Framework/Ui/ActivityCenter.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="mn" TagName="MiniSearch" Src="/Framework/UI/BasicElements/MiniSearch.ascx" %>
<%@ Register TagPrefix="mn" TagName="QuickSearchSplash" Src="/Framework/UI/BasicElements/QuickSearchBox.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SlideshowProfile" Src="/Framework/Ui/BasicElements/SlideshowProfile.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MiniSlideshowProfile" Src="/Framework/Ui/BasicElements/MiniSlideshowProfile.ascx" %>
<%@ Register TagPrefix="ma" TagName="HeaderAnnouncement" Src="/Framework/Ui/HeaderNavigation20/HeaderAnnouncement.ascx" %>
<%@ Register TagPrefix="mn" TagName="ProfileToolBar" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileToolBar.ascx" %>
<%@ Register TagPrefix="mn" TagName="ProfileDetails" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDetails.ascx" %>
<%@ Register TagPrefix="mn" TagName="HotlistProfileList" Src="/Framework/Ui/HotlistElements/HotlistProfileList.ascx" %>

<!-- Registration component -->
<asp:PlaceHolder runat="server" ID="plcRegistration" Visible="false">
  <iframe width="100%" scrolling="no" runat="server" ID="ifrmReg" height="630" frameborder="0" src="/?iframeopr=academic&ShowSplashPageForPostLoginUsers=1" name="academicreg" class="jmag-iframe-container"></iframe>
</asp:PlaceHolder>

<!--ActivityCenter-->
<mn1:ActivityCenter runat="server" id="ActivityCenter1" />

<!--Profile Toolbar-->
<mn:ProfileToolBar ID="profileToolBar1" runat="server" visible="false"></mn:ProfileToolBar>

<!--MiniSearch-->
<mn:MiniSearch runat="server" id="MiniSearch1"></mn:MiniSearch>
<mn:QuickSearchSplash runat="server" id="QuickSearch1"></mn:QuickSearchSplash>
<!--MiniSlideshow-->
<asp:PlaceHolder ID="phMiniMemberSlideshow" runat="server" Visible="false">
    <div id="miniSlideshow">
        <uc1:MiniSlideshowProfile id="ucMiniSlideshowProfile" runat="server"></uc1:MiniSlideshowProfile>
    </div>
</asp:PlaceHolder>
<!--Slideshow-->
<asp:PlaceHolder ID="phMemberSlideshow" runat="server" Visible="false">
    <div id="slideshow">
        <uc1:SlideshowProfile ID="ucSlideshowProfile" runat="server" />
    </div>
</asp:PlaceHolder>
 <asp:PlaceHolder ID="phHeaderAnnouncement" runat="server" Visible="false">
        <ma:HeaderAnnouncement ID="HeaderAnnouncement1" runat="server" />
   </asp:PlaceHolder>


<%--<mn:TargetedContent id="AdServerTest" runat="server" baseslotid="1" visible="false"></mn:TargetedContent>--%>
<mn:AdUnit id="adSquareRight" runat="server" expandImageTokens="true" Size="Square"
    GAMAdSlot="right_300x250" GAMPageMode="Auto" />

<asp:PlaceHolder ID="phMiniMemberSlideshowBelowAd" runat="server" Visible="false">
    <div id="slideshowOuter">
        <div id="slideshow" class="slideshow block-on-load">
            <uc1:MiniSlideshowProfile id="ucMiniSlideshowProfileBelowAd" runat="server"></uc1:MiniSlideshowProfile>
        </div>
    </div>
</asp:PlaceHolder>

<!--profile details-->
<mn:ProfileDetails ID="profileDetails1" runat="server" visible="false"></mn:ProfileDetails>

<!--Hotlist Viewed Your Profile-->
<mn:HotlistProfileList ID="hotlistProfileList1" runat="server" Visible="false" />

<!--channel ads-->
<asp:PlaceHolder ID="PlaceHolderGAMChannels" runat="server" Visible="false">
    <mn:AdUnit id="AdUnitGAMChannel1" runat="server" expandImageTokens="true" Size="Square"
        GAMAdSlot="right_channel1_300x250" GAMPageMode="Auto" Disabled="true" />
    <mn:AdUnit id="AdUnitGAMChannel2" runat="server" expandImageTokens="true" Size="Square"
        GAMAdSlot="right_channel2_300x250" GAMPageMode="Auto" Disabled="true"/>
    <mn:AdUnit id="AdUnitGAMChannel3" runat="server" expandImageTokens="true" Size="Square"
        GAMAdSlot="right_channel3_300x250" GAMPageMode="Auto" Disabled="true"/>
    <mn:AdUnit id="AdUnitGAMChannel4" runat="server" expandImageTokens="true" Size="Square"
        GAMAdSlot="right_channel4_300x250" GAMPageMode="Auto" Disabled="true"/>
    <mn:AdUnit id="AdUnitGAMChannel5" runat="server" expandImageTokens="true" Size="Square"
        GAMAdSlot="right_channel5_300x250" GAMPageMode="Auto" Disabled="true"/>
    <mn:AdUnit id="AdUnitGAMChannel6" runat="server" expandImageTokens="true" Size="Square"
        GAMAdSlot="right_channel6_300x250" GAMPageMode="Auto" Disabled="true"/>
    <mn:AdUnit id="AdUnitGAMChannel7" runat="server" expandImageTokens="true" Size="Square"
        GAMAdSlot="right_channel7_300x250" GAMPageMode="Auto" Disabled="true"/>
    <mn:AdUnit id="AdUnitGAMChannel8" runat="server" expandImageTokens="true" Size="Square"
        GAMAdSlot="right_channel8_300x250" GAMPageMode="Auto" Disabled="true"/>
</asp:PlaceHolder>
<mn:Txt ID="rightContent" runat="server" ResourceConstant="RIGHT_NAV_CONTENT" ExpandImageTokens="true" />
<asp:PlaceHolder runat="server" ID="plcJMessage" Visible="False">
    <div style="position: absolute">
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="demoSWF" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0"
            width="430" height="300" viewastext>
            <param name="movie" value="<%=MovieUrl%>" />
            <param name="quality" value="high" />
            <param name="wmode" value="transparent" />
            <embed src="jmessage.swf" width="430" height="300" wmode="transparent" quality="high"
                pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"
                wmode="transparent"></embed>
        </object>
    </div>

    <script type="text/javascript">
        function IM(strUserId, strCity) {
            //alert(strUserId);
            document.demoSWF.SetVariable('from_ID', strUserId);
            document.demoSWF.SetVariable('location', strCity);
            document.demoSWF.TGotoLabel("_root", "IM_you");
        }

        function Open(strUserId) {
            document.demoSWF.SetVariable('Glabel', strUserId);
            document.demoSWF.TGotoLabel("_root", "Opened");
        }
    </script>

</asp:PlaceHolder>
<!-- BOM links-->
<asp:HyperLink CssClass="homePageAddTo" runat="server" ID="AddToFavorites" Visible="False">
    <mn:Txt ID="txtAddToFavorites" runat="server" />
</asp:HyperLink>
<asp:HyperLink CssClass="homePageAddTo" runat="server" ID="SetHomePage" Visible="False">
    <mn:Txt ID="txtSetHomePage" runat="server" />
</asp:HyperLink>
<!-- <asp:PlaceHolder runat="server" ID="plcNavigationContents" />-->
<!--Color Code-->
<asp:PlaceHolder ID="phColorCodeMemberFeedback" runat="server" Visible="false">
    <div id="cc-promo-feedback" class="editorial cc-promo">
        <h2 class="cc-promo-top-headline">
            Color Code Feedback</h2>
        <div class="cc-promo-item">
            <p>
                I was amazed how right on it was! Take the test and you'll see.. I'm definitely
                a BLUE followed by WHITE.. so good to know...!!!</p>
        </div>
        <div class="cc-promo-item">
            <p>
                I found that the color profile was 'right on' with me. Couldn't have been closer
                to the truth!</p>
        </div>
        <div class="cc-promo-item">
            <p>
                I'm 41 something percent BLUE. They nailed me right on, to the letter. It was a
                fascinating test too.</p>
        </div>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phColorCodeProfileExamples" runat="server" Visible="false">
    <div id="cc-promo-examples" class="editorial cc-promo">
        <h2 class="cc-promo-top-headline">
            Color Coded Profile Examples</h2>
        <p>
            Click each thumbnail to see the example.</p>
        <div id="cc-promo-examples-thumbs" class="clearfix">
            <div class="cc-promo-pic cc-spr-red">
                <div class="cc-pic-tag-sm cc-pic-red">
                    <span>Red</span></div>
            </div>
            <div class="cc-promo-pic cc-spr-yellow">
                <div class="cc-pic-tag-sm cc-pic-yellow">
                    <span>Yellow</span></div>
            </div>
            <div class="cc-promo-pic cc-spr-blue">
                <div class="cc-pic-tag-sm cc-pic-blue">
                    <span>Blue</span></div>
            </div>
            <div class="cc-promo-pic cc-spr-white">
                <div class="cc-pic-tag-sm cc-pic-white">
                    <span>White</span></div>
            </div>
        </div>
        <div id="cc-fp-example-red" class="cc-fp-example">
            <div class="close">
                <span title="Close" class="spr s-icon-close"><span></span></span> <a href="#">Close</a></div>
            <mn:Image ID="mnimage1462" runat="server" FileName="ui-cc-fp-example-red.jpg" TitleResourceConstant=""
                ResourceConstant="" />
        </div>
        <div id="cc-fp-example-yellow" class="cc-fp-example">
            <div class="close">
                <span title="Close" class="spr s-icon-close"><span></span></span> <a href="#">Close</a></div>
            <mn:Image ID="mnimage1163" runat="server" FileName="ui-cc-fp-example-yellow.jpg"
                TitleResourceConstant="" ResourceConstant="" />
        </div>
        <div id="cc-fp-example-blue" class="cc-fp-example">
            <div class="close">
                <span title="Close" class="spr s-icon-close"><span></span></span> <a href="#">Close</a></div>
            <mn:Image ID="mnimage2038" runat="server" FileName="ui-cc-fp-example-blue.jpg" TitleResourceConstant=""
                ResourceConstant="" />
        </div>
        <div id="cc-fp-example-white" class="cc-fp-example">
            <div class="close">
                <span title="Close" class="spr s-icon-close"><span></span></span> <a href="#">Close</a></div>
            <mn:Image ID="mnimage4082" runat="server" FileName="ui-cc-fp-example-white.jpg" TitleResourceConstant=""
                ResourceConstant="" />
        </div>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phColorCodeTestHelp" runat="server" Visible="false">
    <div id="cc-promo-choose" class="editorial cc-promo">
        <h2 class="cc-promo-top-headline">
            Don't Know What To Choose?</h2>
        <div class="cc-promo-item">
            <p>
                You may encounter a question that does not have an answer which accurately describes
                you. That is ok! Just pick the word that is the closest.</p>
        </div>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phColorCodeAbout" runat="server" Visible="false">
    <div id="cc-promo-about" class="editorial cc-promo">
        <h2 class="cc-promo-top-headline">
            About The Test</h2>
        <div class="cc-promo-item">
            <a href="/Applications/ColorCode/AboutColorCode.aspx">About the Color Code</a>
        </div>
        <div class="cc-promo-item">
            <a href="/Applications/ColorCode/MeetDrHartman.aspx">Meet Dr. Hartman</a>
        </div>
        <div class="cc-promo-item">
            <a href="mailto:colorcode@jdate.com" id="lnkColorCodeMailTo" runat="server">Share Your
                Color Code Story &raquo;</a>
        </div>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phColorCodePurchase" runat="server">
<mn:Txt ID="txtColorCodePurchase" runat="server" ResourceConstant="TXT_CC_PURCHASE" ExpandImageTokens="true" />
</asp:PlaceHolder>
<mn:Txt runat="server" ID="txtTips" ResourceConstant="TXT_TIPS" Visible="false" />
<mn:Txt runat="server" ID="txtJExperts" ResourceConstant="TXT_JEXPERTS" Visible="false" />
<mn:AdUnit ID="AdUnitGAMBottom2" runat="server" expandImageTokens="true" GAMAdSlot="bottom2_300X250" GAMPageMode="Auto" />
