﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Framework.Ui
{
    /// <summary>
    /// This control will be used to contain supporting features of ActivyCenter, particularly non-UI features
    /// such as IFrames and Divs for IM and Session so they continue to be available even if the ActivityCenter is no
    /// longer being displayed on the page.
    /// </summary>
    public partial class ActivitySupport : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            // check for online status onprerender (no page_load) so that it fires AFTER saving of displaysettings 16766 to get most recent hidemask
            // hide Instant Messenger checkIM if this page does not support checking IM
            // Don't check IM on page where we don't want interruptions
            // such as during subscription, initial registration and admin pages (related bug 15463)
            // Don't check IM if user has chosen to be hidden.
            // Also, if the page is Unsuspend, don't load Checkim because FrameBuster will hijack the page
            // (this might not be the best place to do this, however time is of the essence - this note borrowed from elsewhere)
            if (!g.SilentCheckIM
                && g.Member != null
                && g.AppPage.PageName != "Unsuspend"
                && g.Member.GetAttributeInt(g.Brand, "SelfSuspendedFlag") != 1
                && !Matchnet.Web.Lib.LayoutTemplateBase.IsAdminPage(g)
                )
            {
                //show online status as hidden if user wants to be hidden (should not check IM's or add to MOL. That is handled elsewhere by javascript)
                bool hidden = (g.Member.GetAttributeInt(g.Brand, "HideMask") & (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline) == (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline;
                plcIMActive.Visible = !hidden;
            }

            //hide Active IM section if member is blocked
            if (_g.EmailVerify.IsMemberBlocked(_g.Member))
            {
                plcIMActive.Visible = false;
            }

            base.OnPreRender(e);
        }
    }
}