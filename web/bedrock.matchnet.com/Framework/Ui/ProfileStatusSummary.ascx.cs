﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Ui
{
    public partial class ProfileStatusSummary : FrameworkControl
    {
        private Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint _EntryPoint = BreadCrumbHelper.EntryPoint.Unknown;

        public Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint EntryPoint
        {
            get { return _EntryPoint; }
            set { _EntryPoint = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializePage();
        }

        private void InitializePage()
        {
            lnkUserName.Text = FrameworkGlobals.Ellipsis(g.Member.GetUserName(_g.Brand), 20);
            //FrameworkGlobals.Ellipsis(g.Member.GetUserName(_g.Brand), 9);
            lnkUserName.ToolTip = g.Member.GetUserName(_g.Brand);
            string viewProfileUrl =
                FrameworkGlobals.LinkHref("/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=" + ((int)EntryPoint).ToString(), true);
            lnkUserName.NavigateUrl = viewProfileUrl;
            lnkUserName.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, lnkUserName.NavigateUrl, "Profile Status");
            txtEditProfile.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile,
                                                                   WebConstants.Action.ViewPageLink, txtEditProfile.Href,
                                                                   "Profile Status");

            string memberPhotoEditURL = "/Applications/MemberProfile/MemberPhotoEdit.aspx";
            memberPhotoEditURL = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MemberPhotoEdit, WebConstants.Action.ProfilePhoto, memberPhotoEditURL, "Profile Status");
            txtManagePhotos.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MemberPhotoEdit,
                                                                   WebConstants.Action.ViewPageLink, txtManagePhotos.Href,
                                                                   "Profile Status");

            noPhoto2.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, true);
            noPhoto2.MemberID = g.Member.MemberID;
            noPhoto2.Mode = PageElements.NoPhoto20.PhotoMode.UploadPhotoNow;
            noPhoto2.DestURL = memberPhotoEditURL;
            imgThumb2.NavigateUrl = memberPhotoEditURL;
            Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, g.Member, g.Brand);
            if (photo != null && photo.IsApproved)
            {
                if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Thumbnail, g.Brand))
                {
                    imgThumb2.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, g.Member, g.Brand,
                                                               photo, PhotoType.Thumbnail,
                                                               PrivatePhotoImageType.Thumb,
                                                                NoPhotoImageType.Thumb);

                    noPhoto2.Visible = false;
                    imgThumb2.Visible = true;
                }
            }
        }
    }
}