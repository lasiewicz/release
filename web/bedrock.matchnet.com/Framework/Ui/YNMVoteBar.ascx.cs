using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;


namespace Matchnet.Web.Framework.Ui
{
	/// <summary>
	///		Summary description for YNMVoteBar.
	/// </summary>
	public class YNMVoteBar : FrameworkControl
	{
		protected HtmlInputHidden YNMVoteStatus;
		protected Matchnet.Web.Framework.Image ImageYes;
		protected Matchnet.Web.Framework.Txt TxtYes;
		protected Matchnet.Web.Framework.Image ImageMaybe;
		protected Matchnet.Web.Framework.Txt TxtMaybe;
		protected Matchnet.Web.Framework.Image ImageNo;
		protected Matchnet.Web.Framework.Txt TxtNo;
		protected Matchnet.Web.Framework.Txt Txt1;
		protected Matchnet.Web.Framework.Txt TxtBothSaidYes;
		protected Matchnet.Web.Framework.Image imgBothSaidYes;
		private int _viewingMemberID;
		protected System.Web.UI.WebControls.PlaceHolder plcStatic;
		protected System.Web.UI.WebControls.PlaceHolder plcAnimated;
		protected Matchnet.Web.Framework.Txt txtNextLink;
		protected System.Web.UI.WebControls.Literal litBothSaidYesVisible;
		protected System.Web.UI.WebControls.Literal litImageOver;
		protected System.Web.UI.WebControls.Literal litImageOut;

		private void Page_Load(object sender, System.EventArgs e)
		{
			litImageOver.Text = Matchnet.Web.Framework.Image.GetURLFromFilename("bknd_vote_large_anim.gif");
			litImageOut.Text = Matchnet.Web.Framework.Image.GetURLFromFilename("bknd_vote_large.gif");
		}
		
		#region Public Properties.
		public string YNMVoteStatusValue
		{
			set
			{
				YNMVoteStatus.Value = value;
			}
		}

		public int ViewingMemberID
		{
			set
			{
				_viewingMemberID = value;
			}
		}

		#endregion

		#region Public and Protected Methods.
		public void ShowStaticBar()
		{
			plcStatic.Visible = true;
			plcAnimated.Visible = false;

			this.TxtBothSaidYes.Visible = false;
			this.imgBothSaidYes.Visible = false;
		}
		#region TPJ-2
		public void SetYNMControlsNextText( string NextText )
		{
			txtNextLink.Href = NextText;
		}
		#endregion

		public void SetYNMControls()
		{
			ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, _viewingMemberID);
			
			YNMVoteStatus.Value = ((int)clickMask).ToString();

			if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)
			{
				ImageYes.FileName = "icon-click-y-on.gif";
			}
			else if ((clickMask & ClickMask.MemberNo) == ClickMask.MemberNo)
			{
				ImageNo.FileName = "icon-click-n-on.gif";
			} 
			else if ((clickMask & ClickMask.MemberMaybe) == ClickMask.MemberMaybe)
			{
				ImageMaybe.FileName = "icon-click-m-on.gif";
			}

			//Show the "You both said yes" icon if applicable
			if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes && (clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes)
			{
				this.litBothSaidYesVisible.Text = "visible";
			}
			else
			{
				this.litBothSaidYesVisible.Text = "hidden";
			}

			// When a member is paging through a member collection, set the Next Profile link.
			#region TPJ-2
			txtNextLink.ResourceConstant = 
				(Request.QueryString["mmvid"] == null) ? "TXT_MORE_PROFILES" : "TXT_MORE_MATCHES" ;
			#endregion

			txtNextLink.Href = BreadCrumbHelper.StaticGetNextProfileLink(_viewingMemberID, Request, g);
			if (FrameworkGlobals.StringIsEmpty(txtNextLink.Href))//string.Empty.Equals(txtNextLink.Href))
			{	// Hide this link if there isn't a next profile.
				txtNextLink.Visible = false;
			}
			
			YNMVoteParameters parameters = new YNMVoteParameters();

			parameters.SiteID = g.Brand.Site.SiteID;
			parameters.CommunityID = g.Brand.Site.Community.CommunityID;
			parameters.FromMemberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
			parameters.ToMemberID = _viewingMemberID;
                    
			string encodedParameters = parameters.EncodeParams();

			ImageYes.NavigateUrl = string.Format("javascript:ProfileYNMVote('1','{0}');", encodedParameters);
			ImageYes.Attributes.Add("onMouseOver", string.Format("YNMMouseOver(this, '{0}', '1')", YNMVoteStatus.Parent.ClientID));
			ImageYes.Attributes.Add("onMouseOut", string.Format("YNMMouseOut(this, '{0}', '1')", YNMVoteStatus.Parent.ClientID));
				
			ImageNo.NavigateUrl = string.Format("javascript:ProfileYNMVote('2','{0}');", encodedParameters);
			ImageNo.Attributes.Add("onMouseOver", string.Format("YNMMouseOver(this, '{0}', '2')", YNMVoteStatus.Parent.ClientID));
			ImageNo.Attributes.Add("onMouseOut", string.Format("YNMMouseOut(this, '{0}', '2')", YNMVoteStatus.Parent.ClientID));

			ImageMaybe.NavigateUrl = string.Format("javascript:ProfileYNMVote('3','{0}');", encodedParameters);
			ImageMaybe.Attributes.Add("onMouseOver", string.Format("YNMMouseOver(this, '{0}', '4')", YNMVoteStatus.Parent.ClientID));
			ImageMaybe.Attributes.Add("onMouseOut", string.Format("YNMMouseOut(this, '{0}', '4')", YNMVoteStatus.Parent.ClientID));


			TxtYes.Href = ImageYes.NavigateUrl;
			TxtYes.Attributes.Add("onmouseover", string.Format("document.{0}.{1}.onmouseover();", WebConstants.HTML_FORM_ID, ImageYes.ClientID)); 
			TxtYes.Attributes.Add("onmouseout", string.Format("document.{0}.{1}.onmouseout();", WebConstants.HTML_FORM_ID, ImageYes.ClientID)); 

			TxtNo.Href = ImageNo.NavigateUrl;
			TxtNo.Attributes.Add("onmouseover", string.Format("document.{0}.{1}.onmouseover();", WebConstants.HTML_FORM_ID, ImageNo.ClientID)); 
			TxtNo.Attributes.Add("onmouseout", string.Format("document.{0}.{1}.onmouseout();", WebConstants.HTML_FORM_ID, ImageNo.ClientID)); 

			TxtMaybe.Href = ImageMaybe.NavigateUrl;
			TxtMaybe.Attributes.Add("onmouseover", string.Format("document.{0}.{1}.onmouseover();", WebConstants.HTML_FORM_ID, ImageMaybe.ClientID)); 
			TxtMaybe.Attributes.Add("onmouseout", string.Format("document.{0}.{1}.onmouseout();", WebConstants.HTML_FORM_ID, ImageMaybe.ClientID)); 
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
