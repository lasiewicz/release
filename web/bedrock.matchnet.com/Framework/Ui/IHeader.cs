﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Web.Framework.Ui
{
    public enum HeaderImageType
    {
        Default, Splash, Login, Registration
    }
    public interface IHeader
    {

        void ShowBlankHeader();
        void ShowBlankHeader(HeaderImageType headerType);
        void SwitchHeaderImage(HeaderImageType headerType);
        bool Visible { set; }
        void SetTopNavTitle(string title);
    }
}
