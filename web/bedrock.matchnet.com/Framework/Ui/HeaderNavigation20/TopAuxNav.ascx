﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopAuxNav.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.HeaderNavigation20.TopAuxNav" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<a name="top"></a>
<div id="headerCobrand" class="header-container clearfix" runat="server">
    <asp:PlaceHolder runat="server" ID="NoNavTopHeader" Visible="false">
        <div id="header-content" class="clearfix">
            <div id="header-logo">
                <mn:Image CssClass="logo" ID="imgLogoNo" runat="server" TitleResourceConstant="SITE_LOGO_ALT"
                    ResourceConstant="SITE_LOGO_ALT" FileName="trans.gif" />
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="NavTopHeader" Visible="true">
        <%--Original version--%>
        <asp:PlaceHolder ID="phNavTopHeaderOriginal" runat="server" Visible="false">
            <div id="header-content" class="clearfix">
                <div id="header-logo">
                    <mn:Image CssClass="logo" ID="imgLogo" runat="server" TitleResourceConstant="SITE_LOGO_ALT"
                        ResourceConstant="SITE_LOGO_ALT" FileName="trans.gif" />
                    <ul id="nav-auxiliary">
                        <%--Aux Nav Links--%>
                        <asp:Repeater runat="server" ID="rptMenu" OnItemDataBound="BindMenuItem">
                            <ItemTemplate>
                                <asp:PlaceHolder ID="plcMenuItem" runat="server">
                                    <li runat="server" id="liMenu">
                                        <mn:Txt ID="txtMenuItem" runat="server" EnableViewState="false" Visible="true" />
                                    </li>
                                </asp:PlaceHolder>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>
        </asp:PlaceHolder>
        <%--Social Nav version--%>
        <asp:PlaceHolder ID="phNavTopHeaderSocialNav" runat="server" Visible="false">
            <div id="header-content">
                <div id="header-logo">
                    <mn:Image CssClass="logo" ID="imgLogoSocialNav" runat="server" TitleResourceConstant="SITE_LOGO_ALT"
                        ResourceConstant="SITE_LOGO_ALT" FileName="trans.gif" />
                </div>
                <asp:PlaceHolder ID="phFacebookLike" runat="server" Visible="false">
                    <div class="header-fb-like-btn">
                        <asp:Literal runat="server" ID="litFacebookLikeCode"></asp:Literal>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phGooglePlus1" runat="server" Visible="false">
                    <div class="header-googleplus-btn">
                        <asp:Literal runat="server" ID="litGooglePlus1Code"></asp:Literal>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="phTwitterFollow">
                    <asp:Literal runat="server" ID="litTwitterFollowCode"></asp:Literal>
                </asp:PlaceHolder>
                <div id="header-nav">
                    <ul id="nav-auxiliary">
                        <%--Logged in version--%>
                        <asp:PlaceHolder ID="phLoggedInSocialNav" runat="server" Visible="false">
                            <li class="im-status">
                                <mn:Txt ID="txtWelcome" runat="server" EnableViewState="false" ResourceConstant="TXT_WELCOME" />
                                <asp:HyperLink runat="server" ID="hplUsername" NavigateUrl="" CssClass="user-name emphasized" /><span>:</span>
                                <!--Hidden Profile status-->
                                <asp:PlaceHolder ID="phProfileStatus" runat="server" Visible="false"><span>[</span><a
                                    href="<%=AbsoluteHostHref%>/Applications/MemberServices/DisplaySettings.aspx"
                                    class="emphasized"><mn:Txt ID="txtHiddenProfileStatus" runat="server" EnableViewState="false"
                                        ResourceConstant="TXT_PROFILE_HIDDEN" />
                                </a><span>]</span> </asp:PlaceHolder>
                                <!-- Not Hidden IM Status-->
                                <asp:PlaceHolder ID="plcIMActive" Visible="false" runat="server">
                                    <%--This is an AppDev debugging feature, add ?showimstatus=true to url to see it--%>
                                    <asp:PlaceHolder ID="plcIMOnlineDebug" runat="server" Visible="false">
                                        <div id="divIMOnlineDebug" class="activity-item">
                                            <a href="javascript:initOnlineChecker(false);" title="<mn:Txt runat='server' resourceconstant='TTL_ONLINE_STATUS_ONLINE' />"
                                                onmouseover="toggle('divCheckingInvitationsCounter')" onmouseout="toggle('divCheckingInvitationsCounter')">
                                                <mn:Image App="Home" runat="server" ID="imgOnlineStatus" FileName="icon-chat.gif"
                                                    TitleResourceConstant="TTL_ONLINE_STATUS_ONLINE" />
                                                <mn:Txt ID="Txt3" runat="server" ResourceConstant="TXT_ONLINE_STATUS_ONLINE" />
                                            </a><span id="lblCheckingInvitations" style="display: none">...</span>
                                            <div id="divCheckingInvitationsCounter" style="display: none">
                                                <mn:Txt runat="server" ResourceConstant="TXT_ONLINE_STATUS_NEXT_CHECK_IN" ID="Txt4" />
                                                <span id="lblCheckingInvitationsCounter">0</span>
                                                <mn:Txt runat="server" ResourceConstant="TXT_ONLINE_STATUS_TIME_UNIT" ID="Txt5" />
                                            </div>
                                        </div>
                                    </asp:PlaceHolder>
                                    <!--Active Status-->
                                    <div id="divIMOnline" class="activity-item">
                                        <span>[</span><a href="<%=AbsoluteHostHref%>/Applications/MemberServices/DisplaySettings.aspx"
                                            class="emphasized"><mn:Txt runat="server" ResourceConstant="TXT_ONLINE_STATUS_ONLINE"
                                                ID="Txt2" />
                                        </a><span>]</span>
                                    </div>
                                    <!--Disconnected status-->
                                    <div id="divIMOffline" style="display: none" class="activity-item">
                                        <a href="javascript:window.location.href=window.location.href;" title="<mn:Txt runat='server' resourceconstant='TTL_ONLINE_STATUS_OFFLINE' />">
                                            <span class="activity-icon">
                                                <mn:Image App="Home" runat="server" ID="Image3" FileName="icon-status-disconnected.gif"
                                                    TitleResourceConstant="TTL_ONLINE_STATUS_OFFLINE" alt="" /></span> <strong>
                                                        <mn:Txt runat="server" ResourceConstant="TXT_ONLINE_STATUS_OFFLINE" ID="Txt6" />
                                                    </strong></a>
                                    </div>
                                </asp:PlaceHolder>
                                <!--Hidden IM Status-->
                                <asp:PlaceHolder ID="plcIMHidden" Visible="false" runat="server">
                                    <!-- Hidden -->
                                    <div id="divIMHidden" class="activity-item">
                                        <span>[</span><a href="/Applications/MemberServices/DisplaySettings.aspx" class="emphasized"><mn:Txt
                                            runat="server" ID="lblOnlineStatusHidden" ResourceConstant="TXT_ONLINE_STATUS_HIDDEN" />
                                        </a><span>]</span>
                                    </div>
                                </asp:PlaceHolder>
                            </li>
                            <%--Aux Nav Links--%>
                            <asp:Repeater runat="server" ID="rptMenuSocialNav" OnItemDataBound="BindMenuItem">
                                <ItemTemplate>
                                    <asp:PlaceHolder ID="plcMenuItem" runat="server">
                                        <li runat="server" id="liMenu">
                                            <asp:Literal ID="literalSeparator" runat="server" Text="|"></asp:Literal>
                                            <mn:Txt ID="txtMenuItem" runat="server" EnableViewState="false" Visible="true" />
                                        </li>
                                    </asp:PlaceHolder>
                                </ItemTemplate>
                            </asp:Repeater>
                            <%--TODO: Should we place this logout link in xml file?--%>
                            <%--
                            <li runat="server" id="liMenuLogout">
                                <asp:Literal ID="literalSeparator" runat="server" Text="|"></asp:Literal>
                                <mn:Txt ID="txtMenuItemLogout" runat="server" EnableViewState="false" Visible="true"
                                    ResourceConstant="LEFT_LOGOUT" TitleResourceConstant="NAV_LOGOUT" />
                            </li>
                            --%>
                        </asp:PlaceHolder>
                        <%--Logged out version--%>
                        <asp:PlaceHolder ID="phLoggedOutSocialNav" runat="server" Visible="false"><span class="greeting">
                            <mn:Txt ID="txtWelcomeLoggedout" runat="server" EnableViewState="false" ResourceConstant="TXT_WELCOME_LOGGEDOUT" />
                        </span><a href="javascript:window.location.href='/Applications/Logon/Logon.aspx?DestinationURL='+ encodeURIComponent(window.location.href.replace('http://'+window.location.host,''))"
                            class="sign-in link-secondary">
                            <mn:Txt ID="txtLogin" runat="server" ResourceConstant="TXT_LOGIN" EnableViewState="false" />
                        </a>
                            <mn:Txt runat="server" ID="txtOr" EnableViewState="false" ResourceConstant="TXT_OR" />
                            <mn:Txt runat="server" ID="txtJoinNow" EnableViewState="false" ResourceConstant="TXT_JOIN_NOW" />
                        </asp:PlaceHolder>
                    </ul>
                </div>
            </div>
        </asp:PlaceHolder>
    </asp:PlaceHolder>

    <script type="text/javascript">
        function showTravelEventsWindow()
        {
            window.open('http://static.jdate.com/microsites/travel2014/', '_blank');
        }
    </script>



</div>
