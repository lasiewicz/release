﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Ui.HeaderNavigation20
{
    public class HeaderLinks
    {
        public const String ReverseSearch = "/Applications/Search/ReverseSearch.aspx?op=true&pg=1";
        public const String KeywordSearch = "/Applications/Search/KeywordSearch.aspx";
        public const string Home = "/Applications/Home/default.aspx";
        public const string MessageSettings = "/Applications/Email/MessageSettings.aspx";
        public const string AdminHome = "/Applications/Admin/MemberSearch/Search.aspx";
        public const string MemberServices = "/Applications/MemberServices/MemberServices.aspx";
        public const string Events = "<a href='http://static.jdate.com/microsites/travel2014/' target='_blank'>Fawad</a>"; 
        public const string EventsJDIL = "http://premium.jdate.co.il";
        public const string FAQ = "/Applications/Article/FAQMain.aspx";
        public const string ContactUs = "/Applications/ContactUs/ContactUs.aspx";
        public const string MembersOnline = "/Applications/MembersOnline/MembersOnline.aspx";
        public const string Chat = "/Applications/Chat/Default.aspx";
        public const string SubscriptionHistory = "/Applications/Subscription/History.aspx";
        public const string Mailbox = "/Applications/Email/MailBox.aspx";
        public const string VerifyEmail = "/Applications/MemberServices/VerifyEmail.aspx";
        public const string QuickSearch = "/Applications/QuickSearch/QuickSearch.aspx";
        public const string SearchPreferences = "/Applications/Search/SearchPreferences.aspx";
        public const string DisplaySettings = "/Applications/MemberServices/DisplaySettings.aspx";
        public const string JDateMobile = "/Applications/Mobile/MobileSettings.aspx";
        public const string ColorCode = "/Applications/ColorCode/Landing.aspx";
        public const string ViewProfile = "/Applications/MemberProfile/ViewProfile.aspx";
        public const string EditProfile = "/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit";
        public const string SearchResults = "/Applications/Search/SearchResults.aspx";
        public const string MemberPhotoUpload = "/Applications/MemberProfile/MemberPhotoEdit.aspx";
        public const string Subscribe = "/Applications/Subscription/Subscribe.aspx";
        public const string LookupProfile = "/Applications/LookupProfile/LookupProfile.aspx";
        public const string Default = "/Default.aspx";
        public const string HotListView = "/Applications/HotList/View.aspx";
        public const string PhotoGallery = "/Applications/PhotoGallery/PhotoGallery.aspx";
        public const string ToolbarDownload = "/Applications/Toolbar/ToolbarDownload.aspx";
        public const string CupidQuest = "javascript: popupGame('/Applications/Fun/CupidQuest.aspx?LayoutTemplateID=2');";
        //public const string Video = "/Applications/Video/VideoCenter.aspx";
        public const string Video = "/Applications/Video/VideoEpisodeCenter.aspx";
        public const string MatchMeter = "/Applications/CompatibilityMeter/Welcome.aspx";
        public const string MWHListHome = "/Applications/HotList/View.aspx?CategoryID=-1";
        public const string MYHListHome = "/Applications/HotList/View.aspx?CategoryID=-9";
        public const string ClickYNM = "/Applications/HotList/View.aspx?CategoryID=-15";
        public const string MatchMailList = "/Applications/HotList/View.aspx?CategoryID=-10";
        public const string CustomLists = "/Applications/HotList/ManageListCategory.aspx";
        public const string CustomFolders = "/Applications/Email/FolderSettings.aspx";
        public const string VideoCenter = "/Applications/Video/VideoCenter.aspx";
        public const string VideoEpisodeCenter = "/Applications/Video/VideoEpisodeCenter.aspx";
        public const string AutoRenewal = "/Applications/MemberServices/AutoRenewalSettings.aspx";
        public const string AccountHistory = "/Applications/Subscription/History.aspx";
        public const string BillingInfo = "/Applications/BillingInformation/PaymentProfile.aspx";
        public const string PremiumServicesSettings = "/Applications/MemberServices/PremiumServiceSettings.aspx";
        public const string Registration = "/Applications/Registration/Registration.aspx";



    }
}