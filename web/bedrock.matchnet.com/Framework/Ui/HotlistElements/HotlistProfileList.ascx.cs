﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.List.ValueObjects;
using System.Collections;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Ui.HotlistElements
{
    public partial class HotlistProfileList : FrameworkControl
    {
        protected HotListCategory _HotListCategory = HotListCategory.WhoViewedYourProfile;
        protected BreadCrumbHelper.EntryPoint _EntryPoint = BreadCrumbHelper.EntryPoint.HotLists;
        protected int _PageSize = 4;
        protected int _StartRow = 1;
        private int _Ordinal = 1;
        private string _OmnitureWidgetName = "";
        private ArrayList members = null;

        #region Properties
        public BreadCrumbHelper.EntryPoint EntryPoint
        {
            get { return _EntryPoint; }
            set { _EntryPoint = value; }
        }

        public HotListCategory HotlistCategory
        {
            get { return _HotListCategory; }
            set { _HotListCategory = value; }
        }

        public int PageSize
        {
            get { return _PageSize; }
            set { _PageSize = value; }
        }

        public int StartRow
        {
            get { return _StartRow; }
            set { _StartRow = value; }
        }

        #endregion

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void rpProfiles_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                IMemberDTO member = e.Item.DataItem as IMemberDTO;

                string profileURL = BreadCrumbHelper.MakeViewProfileLink(_EntryPoint, member.MemberID, _Ordinal, null, (int)_HotListCategory);

                //new or updated
                bool isNewProfile = member.IsNewMember(g.Brand.Site.Community.CommunityID);
                bool isUpdatedProfile = member.IsUpdatedMember(g.Brand.Site.Community.CommunityID);

                PlaceHolder phIsNewProfile = e.Item.FindControl("phIsNewProfile") as PlaceHolder;
                phIsNewProfile.Visible = isNewProfile;

                PlaceHolder phIsUpdatedProfile = e.Item.FindControl("phIsUpdatedProfile") as PlaceHolder;
                phIsUpdatedProfile.Visible = !isNewProfile && isUpdatedProfile;

                //username
                HyperLink lnkUserName = e.Item.FindControl("lnkUserName") as HyperLink;
                string userName = member.GetUserName(g.Brand);
                lnkUserName.Text = FrameworkGlobals.Ellipsis(userName, 15, "&hellip;");
                lnkUserName.ToolTip = userName;
                lnkUserName.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, profileURL, _OmnitureWidgetName);

                //age
                DateTime birthDate = member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue);
                Label txtAge = e.Item.FindControl("txtAge") as Label;
                txtAge.Text = FrameworkGlobals.GetAge(birthDate).ToString();
                if (txtAge.Text.Trim() == "")
                {
                    txtAge.Text = "&nbsp;";
                }
                else
                {
                    if (FrameworkGlobals.isHebrewSite(g.Brand))
                    {
                        txtAge.Text = g.GetResource("TXT_YEARS_OLD", this) + " " + txtAge.Text;
                    }
                    else
                    {
                        txtAge.Text += " " + g.GetResource("TXT_YEARS_OLD", this);
                    }
                }

                string location = ProfileDisplayHelper.GetRegionDisplay(member, g);
                Label txtLocation = e.Item.FindControl("txtLocation") as Label; 
                txtLocation.Text = FrameworkGlobals.Ellipsis(location, 22, "&hellip;");
                txtLocation.ToolTip = location;

                Literal literalMaritalAndGender = e.Item.FindControl("literalMaritalAndGender") as Literal;
                literalMaritalAndGender.Text = ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(member, g);

                //online
                bool isOnline = false;
                OnlineLinkHelper onlineLinkHelper = OnlineLinkHelper.getOnlineLinkHelper(Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.None, member, g.Member, LinkParent.HomePageMicroProfile, (int)PurchaseReasonType.AttemptToIMMiniProfile, out isOnline);

                if (!isOnline)
                {
                    PlaceHolder phOffline = e.Item.FindControl("phOffline") as PlaceHolder;
                    phOffline.Visible = true;

                    DateTime loginDate = member.GetLastLogonDate(g.Brand.Site.Community.CommunityID);
                    LastTime lastTimeLogon = e.Item.FindControl("lastTimeLogon") as LastTime;
                    lastTimeLogon.LoadLastTime(loginDate);
                    string lastLoginText = String.IsNullOrEmpty(lastTimeLogon.LiteralTimeControl.Text) ? g.GetResource("N_A", this) : lastTimeLogon.LiteralTimeControl.Text;

                    Sprite sprOffline = e.Item.FindControl("sprOffline") as Sprite;
                    sprOffline.ResourceControl = this;
                    sprOffline.Title = g.GetResource("LAST_ONLINE", this) + lastLoginText;
                    
                }
                else
                {
                    PlaceHolder phOnline = e.Item.FindControl("phOnline") as PlaceHolder;
                    phOnline.Visible = true;
                    Sprite sprOnline = e.Item.FindControl("sprOnline") as Sprite;
                    sprOnline.ResourceControl = this;
                    HyperLink lnkOnline = e.Item.FindControl("lnkOnline") as HyperLink;
                    lnkOnline.NavigateUrl = onlineLinkHelper.OnlineLink;
                }

                //hotlist date
                ListItemDetail listItemDetail = g.List.GetListItemDetail(_HotListCategory,
                        g.Brand.Site.Community.CommunityID,
                        g.Brand.Site.SiteID,
                        member.MemberID);

                DateTime hotlistDate = (listItemDetail != null) ? listItemDetail.ActionDate : DateTime.Now;
                LastTime lastTimeHotlistDate = e.Item.FindControl("lastTimeHotlistDate") as LastTime;
                lastTimeHotlistDate.LoadLastTime(hotlistDate);

                //photo
                Matchnet.Web.Framework.Image imgThumb = e.Item.FindControl("imgThumb") as Matchnet.Web.Framework.Image;
                imgThumb.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfilePhoto, profileURL, _OmnitureWidgetName); ;

                bool isMale = FrameworkGlobals.IsMaleGender(member, g.Brand);
                imgThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Thumb, isMale);

                var photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, member, g.Brand);
                if (photo == null)
                {
                    imgThumb.Visible = false;

                    Matchnet.Web.Framework.Ui.PageElements.NoPhoto20 noPhoto = e.Item.FindControl("noPhoto") as Matchnet.Web.Framework.Ui.PageElements.NoPhoto20;
                    noPhoto.Visible = true;
                    noPhoto.MemberID = member.MemberID;
                    noPhoto.Member = member;
                    noPhoto.DestURL = imgThumb.NavigateUrl;

                    noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, true);
                    noPhoto.PrivatePhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, true);
                }
                else
                {
                    imgThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, member, g.Brand,
                                                                           photo, PhotoType.Thumbnail,
                                                                           PrivatePhotoImageType.Thumb,
                                                                            NoPhotoImageType.Thumb);
                }

                _Ordinal++;
            }
        }

        #endregion

        #region Public Methods
        public void LoadHotlistProfileList()
        {
            //set hotlist specific info
            switch (_HotListCategory)
            {
                case HotListCategory.WhoViewedYourProfile:
                    _OmnitureWidgetName = "Viewed Your Profile List";
                    break;
                default:
                    _OmnitureWidgetName = "Hotlist Profile List";
                    break;
            }

            int communityID = g.Brand.Site.Community.CommunityID;
            int siteID = g.Brand.Site.SiteID;

            //get hotlist profiles
            int totalrows = 0;
            ArrayList targetMemberIDs = g.List.GetListMembers(_HotListCategory, communityID, siteID,
                                                                _StartRow, _PageSize, out totalrows);

            if (targetMemberIDs != null && targetMemberIDs.Count > 0)
            {
                //load profiles
                members = MemberDTOManager.Instance.GetIMemberDTOs(g, targetMemberIDs, MemberType.Search, MemberLoadFlags.None);
            }

            if (members != null && members.Count > 0)
            {
                bool memberCanViewList = true;
                //check for subscriber only list
                if (_HotListCategory == HotListCategory.WhoViewedYourProfile)
                {
                    ListManager listManager = new ListManager();
                    memberCanViewList = listManager.IsViewedYourProfileListAvailableToMember(g.Member, g.Brand);
                }

                if (memberCanViewList)
                {
                    //has profiles to display
                    phHasProfiles.Visible = true;
                    phNoProfiles.Visible = false;

                    //load profiles
                    _Ordinal = _StartRow;
                    rpProfiles.DataSource = members;
                    rpProfiles.DataBind();

                    lnkViewMore.NavigateUrl = "/Applications/HotList/View.aspx?CategoryID=" + ((int)_HotListCategory).ToString();
                    lnkViewMore.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.View, WebConstants.Action.ViewMoreLink, lnkViewMore.NavigateUrl, _OmnitureWidgetName);
                }
                else
                {
                    phHasProfiles.Visible = false;
                    phNoProfiles.Visible = false;
                    phNonSubViewForSubscriberOnlyLists.Visible = true;

                    //viewed your profile as subscriber only feature, show default copy to non-sub
                    int count = g.List.GetCount(HotListCategory.WhoViewedYourProfile, communityID, siteID);
                    string subscribeLink = FrameworkGlobals.GetSubscriptionLink(false, (int)PurchaseReasonType.HotlistViewedYourProfile);
                    litViewedYourProfileForSubscribersOnly.Text = string.Format(g.GetResource(litViewedYourProfileForSubscribersOnly.ResourceConstant, this), subscribeLink, count);
                }
                    
            }
            else
            {
                //no profiles to display
                phHasProfiles.Visible = false;
                phNoProfiles.Visible = true;

            }

        }
        #endregion

        #region Private Methods

        #endregion
    }
}
