﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Matchnet.Web.Framework.Ui.HotlistElements {
    
    
    public partial class HotlistProfileList {
        
        /// <summary>
        /// txtTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Txt txtTitle;
        
        /// <summary>
        /// phHasProfiles control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder phHasProfiles;
        
        /// <summary>
        /// rpProfiles control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater rpProfiles;
        
        /// <summary>
        /// lnkViewMore control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink lnkViewMore;
        
        /// <summary>
        /// txtViewMore control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Txt txtViewMore;
        
        /// <summary>
        /// phNoProfiles control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder phNoProfiles;
        
        /// <summary>
        /// txtNoProfile control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Txt txtNoProfile;
        
        /// <summary>
        /// phNonSubViewForSubscriberOnlyLists control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder phNonSubViewForSubscriberOnlyLists;
        
        /// <summary>
        /// litViewedYourProfileForSubscribersOnly control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Ui.FormElements.FrameworkLiteral litViewedYourProfileForSubscribersOnly;
    }
}
