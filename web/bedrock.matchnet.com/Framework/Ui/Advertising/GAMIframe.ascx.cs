﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Matchnet.Web.Framework.Ui.Advertising
{
    public partial class GAMIframe : FrameworkControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                g.SaveSession = false;
                string gamAdSlot = "";

                if (HttpContext.Current.Request["GAMAdSlot"] != null)
                    gamAdSlot = HttpContext.Current.Request["GAMAdSlot"];
                else
                {//hack

                    gamAdSlot = HttpContext.Current.Request["amp;GAMAdSlot"];

                }

                AdUnitDisplay.GAMAdSlot = gamAdSlot;
                
            }
            catch (Exception ex)
            {// bad luck
            }
        }
    }
}