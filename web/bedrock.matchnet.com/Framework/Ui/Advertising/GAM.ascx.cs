﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Collections.Generic;
using System.Xml.Serialization;
using Matchnet.Configuration.ValueObjects;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.MatchTest.ValueObjects.MatchMeter;
using Matchnet.Web.Applications.Email;
using Matchnet.Web.Applications.Omnidate;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Enumerations;
using Matchnet.PremiumServices.ValueObjects;
using Spark.Common.AccessService;
using Matchnet.ExternalMail.ValueObjects;

namespace Matchnet.Web.Framework.Ui.Advertising
{


    // Google Ad Manager
    // This control is added to Head at runtime, and renders GAM related Javascripts and tags.
    public partial class GAM : FrameworkControl
    {
        #region Private Variables

        private const string WHITELIST_FILE_PATH = "/Framework/Ui/Advertising/XML/AdSlotsWhitelist.{0}.xml";
        private const string WHITELIST_CACHE_KEY = "GAM_ADSLOTS_WHITELIST_CACHE_KEY_";
        private const string IAP_GAM_VALUE = "100";

        /// <summary>
        /// Currently there's only one account name, but new ones may be added for other sites.
        /// </summary>
        public readonly string GAM_ACCOUNT_NAME;

        /// <summary>
        /// For GPT, we need this instead of GAM_ACCOUNT_NAME
        /// </summary>
        public readonly string GAM_NETWORK_ID;

        /// <summary>
        /// Keeps track of new ad slot calls through out the page's life cycle.
        /// </summary>
        private List<string> adSlots = new List<string>();

        /// <summary>
        /// Caching is implemented for GAM mapping data.
        /// </summary>
        private ICaching _Cache;

        /// <summary>
        /// Render GAM or not show any based on this flag.
        /// </summary>
        public bool Render { get; set; }

        #endregion

        #region Constructors

        public GAM()
        {
            bool useMembase = Convert.ToBoolean(RuntimeSettings.GetSetting(WebConstants.MEMBASE_SETTING_CONSTANT));
            bool useMembaseForWeb = Convert.ToBoolean(RuntimeSettings.GetSetting(WebConstants.MEMBASE_WEB_SETTING_CONSTANT));
            if (useMembase && useMembaseForWeb)
            {
                MembaseConfig membaseConfig =
                    RuntimeSettings.GetMembaseConfigByBucket(WebConstants.MEMBASE_WEB_BUCKET_NAME);
                _Cache = Spark.Caching.MembaseCaching.GetSingleton(membaseConfig);
            }
            else
                _Cache = Matchnet.CachingTemp.Cache.GetInstance();

            GAM_ACCOUNT_NAME = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("GAM_ACCOUNT_NAME", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
            GAM_NETWORK_ID = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("GAM_NETWORK_ID", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
   
            // Set the default value to true.
            Render = true;
        }

        #endregion

        #region Events

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                populateAdAttributes();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                // Don't render any GAM stuff if there's no ad to serve. Only pre-render event can know if Render property was changed
                // from any other controls.
                //block GAM for ssl pages because it doesn't have https
                bool ssl = (Request.ServerVariables["SERVER_PORT"] == "443");
                if (adSlots.Count == 0 || !Render || ssl)
                {
                    PlaceHolderGAM.Visible = false;
                    return;
                }
                else
                {
                    populateAdSlots();
                    RegisterPlaceCastScript();
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion

        #region Private Methods
        // Returns a Hashtable the contains only the ad slots that should be rendered in the page
        private static Hashtable GetAdSlotsWhiteList(ContextGlobal g)
        {
            string cacheKey = WHITELIST_CACHE_KEY + g.Brand.Site.SiteID;
            Hashtable adSlotsWhiteList = g.Cache[cacheKey] as Hashtable;

            try
            {
                if (adSlotsWhiteList == null)
                {
                    adSlotsWhiteList = new Hashtable();

                    string xmlFile = HttpContext.Current.Server.MapPath(string.Format(WHITELIST_FILE_PATH, g.Brand.Site.SiteID.ToString()));

                    XMLObjectSerializer objectSerializer = new XMLObjectSerializer();
                    AdSlots adSlots = objectSerializer.FromXmlFile<AdSlots>(xmlFile);

                    // Converts the list into a Hashtable for fast access
                    foreach (AdSlots.AdSlot adSlot in adSlots.AdSlotsList)
                    {
                        string adSlotName = adSlot.AdSlotName.ToLower();
                        if (!adSlotsWhiteList.ContainsKey(adSlotName))
                        {
                            adSlotsWhiteList.Add(adSlotName, null);
                        }
                    }

                    //System.Web.Caching.CacheDependency cacheFileDependencies = new System.Web.Caching.CacheDependency(new string[] { xmlFile });
                    g.Cache.Insert(cacheKey, adSlotsWhiteList);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return adSlotsWhiteList;
        }

        // Checks if adSlotName should be rendered in the page 
        private static bool EnableRenderAdSlotCode(string adSlotName, ContextGlobal g)
        {
            bool enable = true;

            try
            {
                if (Convert.ToBoolean(RuntimeSettings.GetSetting("RENDER_WHITELISTED_GAM_ADS_ONLY",
                                g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
                {
                    if (!GetAdSlotsWhiteList(g).ContainsKey(adSlotName.ToLower()))
                    {
                        enable = false;
                    }
                    else
                    {
                        if (adSlotName.Contains("editprofile_overlay_1x1"))
                        {
                            if (!Convert.ToBoolean(RuntimeSettings.GetSetting("RENDER_EDITPROFILE_OVERLAY_1X1",
                                                                             g.Brand.Site.Community.CommunityID,
                                                                             g.Brand.Site.SiteID)))
                            {
                                enable = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return enable;
        }

        /// <summary>
        /// Populate member specific information used for custom targeting.
        /// </summary>
        private void populateAdAttributes()
        {
            // IL sites are using different set of attributes
            bool isILGAM = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_IL_GAM", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));

            List<AdAttribute> attributes = new List<AdAttribute>();

            // Site
            attributes.Add(new AdAttribute("Site", g.Brand.Site.Name));

            // Most attribute values are only available when the user is logged in.
            if (g.Member != null)
            {
                DateTime subscriptionExpirationDate = DateTime.MinValue;
                Spark.Common.AccessService.AccessPrivilege basicSubscriptionPrivilege = g.Member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.BasicSubscription, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (basicSubscriptionPrivilege != null)
                {
                    subscriptionExpirationDate = basicSubscriptionPrivilege.EndDatePST;
                }

                // Age
                attributes.Add(new AdAttribute("age", Matchnet.Web.Framework.FrameworkGlobals.GetAge(_g.Member.GetAttributeDate(_g.Brand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString(), true));

                #region Gender
                string genderMask = Convert.ToString(g.Member.GetAttributeInt(g.Brand, "Gendermask"));
                switch (genderMask)
                {
                    case "6":
                        attributes.Add(new AdAttribute("gender", "fm", true));
                        attributes.Add(new AdAttribute("gender", "f", true));
                        break;
                    case "10":
                        attributes.Add(new AdAttribute("gender", "ff", true));
                        attributes.Add(new AdAttribute("gender", "f", true));
                        break;
                    case "5":
                        attributes.Add(new AdAttribute("gender", "mm", true));
                        attributes.Add(new AdAttribute("gender", "m", true));
                        break;
                    case "9":
                        attributes.Add(new AdAttribute("gender", "mf", true));
                        attributes.Add(new AdAttribute("gender", "m", true));
                        break;
                }
                #endregion

                // Education
                attributes.Add(new AdAttribute("EducationLevel", Option.GetDescription("EducationLevel", g.Member.GetAttributeInt(g.Brand, "EducationLevel"), g)));

                // IndustryType(Profession)
                attributes.Add(new AdAttribute("IndustryType", Option.GetDescription("IndustryType", g.Member.GetAttributeInt(g.Brand, "IndustryType"), g)));



                // SubscriptionStatus
                //string subStatus = Convert.ToString(FrameworkGlobals.GetMemberSubcriptionStatus(g.Member.MemberID, g.Brand));
                if(SubscriberManager.Instance.IsIAPSubscriber(g.Member,g.Brand))
                    attributes.Add(new AdAttribute("SubscriptionStatus", IAP_GAM_VALUE)); //We dont have a particular sub scription status for IAP Subs. We put in the 100 value which corresponds to gam_value of "sub_iap" in GAM.xml
                else
                {
                    string subStatus = Convert.ToString(FrameworkGlobals.GetMemberSubcriptionStatus(g.Member, g.Brand));
                    attributes.Add(new AdAttribute("SubscriptionStatus", subStatus));
                }
                    

                #region # of days since reg

                DateTime regDate = Convert.ToDateTime(g.Member.GetAttributeDate(g.Brand, "BrandInsertDate"));
                int days = (regDate != DateTime.MinValue) ? DateTime.Now.Subtract(regDate).Days : 0;
                attributes.Add(new AdAttribute("since_reg", days.ToString(), true));

                if (days >= 0 && days <= 30)
                    attributes.Add(new AdAttribute("since_reg", "0-30", true));
                else if (days >= 31 && days <= 60)
                    attributes.Add(new AdAttribute("since_reg", "31-60", true));
                else if (days >= 61 && days <= 90)
                    attributes.Add(new AdAttribute("since_reg", "61-90", true));
                else if (days >= 91 && days <= 180)
                    attributes.Add(new AdAttribute("since_reg", "91-180", true));
                else if (days >= 181)
                    attributes.Add(new AdAttribute("since_reg", "181_plus", true));

                #endregion

                #region # of days til renew
                //only for current subscribers
                if (subscriptionExpirationDate != DateTime.MinValue && subscriptionExpirationDate > DateTime.Now)
                {
                    int daysTilRenew = (subscriptionExpirationDate - DateTime.Now.Date).Days;

                    if (daysTilRenew <= 30)
                    {
                        attributes.Add(new AdAttribute("til_renew", daysTilRenew.ToString(), true));
                    }
                    else if (daysTilRenew >= 31 && daysTilRenew <= 45)
                    {
                        attributes.Add(new AdAttribute("til_renew", "31-45", true));
                    }
                    else if (daysTilRenew >= 46 && daysTilRenew <= 60)
                    {
                        attributes.Add(new AdAttribute("til_renew", "46-60", true));
                    }
                    else if (daysTilRenew >= 61 && daysTilRenew <= 90)
                    {
                        attributes.Add(new AdAttribute("til_renew", "61-90", true));
                    }
                    else if (daysTilRenew >= 91 && daysTilRenew <= 120)
                    {
                        attributes.Add(new AdAttribute("til_renew", "91-120", true));
                    }
                    else if (daysTilRenew >= 121 && daysTilRenew <= 180)
                    {
                        attributes.Add(new AdAttribute("til_renew", "121-180", true));
                    }
                    else if (daysTilRenew >= 181)
                    {
                        attributes.Add(new AdAttribute("til_renew", "181+", true));
                    }
                }
                else
                {
                    attributes.Add(new AdAttribute("til_renew", string.Empty, true));
                }

                #endregion
                #region autorenew

                string autorenew = g.Session.GetString("AUTO_RENEW");
                bool autorenewflag = false;
                switch (autorenew)
                {
                    case null:
                    case "":
                        if (subscriptionExpirationDate != DateTime.MinValue)
                        {
                            autorenewflag = FrameworkGlobals.IsAutorenewOn(g.Member, g);
                        }

                        break;
                    case "autorenoff":

                        if (subscriptionExpirationDate != DateTime.MinValue && String.IsNullOrEmpty(g.Session.GetString("AUTO_RENEW_CHECKED")))
                        {//what if they subscribed in this session?
                            autorenewflag = FrameworkGlobals.IsAutorenewOn(g.Member, g);
                            g.Session.Add("AUTO_RENEW_CHECKED", "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

                        }
                        break;
                    case "autorenon":
                        autorenewflag = true;
                        break;
                }

                autorenew = autorenewflag ? "autorenon" : "autorenoff";
                g.Session.Add("AUTO_RENEW", autorenew, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);


                attributes.Add(new AdAttribute("features", autorenew, true));



                #endregion
                // Member ID
                attributes.Add(new AdAttribute("lastmemid", (g.Member.MemberID % 10).ToString(), true));

                #region featueres - Forced Page
                // Days since sub end (ex-subscribers)
                if (subscriptionExpirationDate != DateTime.MinValue && subscriptionExpirationDate < DateTime.Now)
                {
                    int daysSinceSubEnd = (DateTime.Now - subscriptionExpirationDate).Days;

                    attributes.Add(new AdAttribute("since_subx", daysSinceSubEnd.ToString(), true));
                    if (daysSinceSubEnd >= 0 && daysSinceSubEnd <= 30)
                        attributes.Add(new AdAttribute("since_subx", "0-30", true));
                    else if (daysSinceSubEnd >= 31 && daysSinceSubEnd <= 60)
                        attributes.Add(new AdAttribute("since_subx", "31-60", true));
                    else if (daysSinceSubEnd >= 61 && daysSinceSubEnd <= 90)
                        attributes.Add(new AdAttribute("since_subx", "61-90", true));
                    else if (daysSinceSubEnd >= 91 && daysSinceSubEnd <= 180)
                        attributes.Add(new AdAttribute("since_subx", "91-180", true));
                    else if (daysSinceSubEnd >= 181)
                        attributes.Add(new AdAttribute("since_subx", "181_plus", true));
                }
                else
                {
                    attributes.Add(new AdAttribute("since_subx", string.Empty, true));
                }

                // Marital Status
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_GAM_MARITAL_STATUS", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    attributes.Add(new AdAttribute("MaritalStatus", Option.GetDescription("MaritalStatus", g.Member.GetAttributeInt(g.Brand, "MaritalStatus"), g)));
                }
                #endregion

                #region features - for GAM forced overaly

                try
                {
                    // Jmeter status
                    int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID,
                                                                      g.Brand.Site.SiteID, g.Brand.BrandID));
                    int mmenumval = (Int32)WebConstants.MatchMeterFlags.EnableApp;
                    if ((mmsett & mmenumval) == mmenumval)
                    {
                        MatchTestStatus mts = MatchMeterSA.Instance.GetMatchTestStatus(_g.Member.MemberID, MemberPrivilegeAttr.IsCureentSubscribedMember(_g), _g.Brand);
                        if (mts == MatchTestStatus.Completed || mts == MatchTestStatus.MinimumCompleted)
                        {
                            attributes.Add(new AdAttribute("features", "jm_taker", true));
                        }
                        else
                        {
                            attributes.Add(new AdAttribute("features", "jm_notaker", true));
                        }
                    }
                }
                catch (Exception)
                {
                    // do nothing
                }


                // Photos
                if (MemberPhotoDisplayManager.Instance.GetAllPhotosCount(g.Member, g.Brand.Site.Community.CommunityID) > 0)
                {
                    attributes.Add(new AdAttribute("features", "photos", true));
                }
                else
                {
                    attributes.Add(new AdAttribute("features", "no_photos", true));
                }

                // Mobile
                if (RuntimeSettings.GetSetting("MESSMO_ENABLED", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToLower() == "true")
                {
                    int messmoCapable = _g.Member.GetAttributeInt(_g.Brand, "MessmoCapable", 0);

                    if (messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Capable ||
                        messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Pending)
                    {
                        attributes.Add(new AdAttribute("features", "mobile_yes", true));
                    }
                    else
                    {
                        attributes.Add(new AdAttribute("features", "mobile_no", true));
                    }
                }

                // Color Code
                attributes.Add(new AdAttribute("features", ColorCodeHelper.HasMemberCompletedQuiz(g.Member, g.Brand) ? "cc_taker" : "cc_notaker", true));

                // Highlight and Spotlight
                var apHighlightedProfile = g.Member.GetUnifiedAccessPrivilege(PrivilegeType.HighlightedProfile, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (apHighlightedProfile != null && apHighlightedProfile.EndDatePST > DateTime.Now)
                {
                    attributes.Add(new AdAttribute("features", "highlight_taker", true));
                }

                var apSpotlightMember = g.Member.GetUnifiedAccessPrivilege(PrivilegeType.SpotlightMember, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (apSpotlightMember != null && apSpotlightMember.EndDatePST > DateTime.Now)
                {
                    attributes.Add(new AdAttribute("features", "spotlight_taker", true));
                }

                // Read Receipts
                var apReadReceipts = g.Member.GetUnifiedAccessPrivilege(PrivilegeType.ReadReceipt, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (apReadReceipts != null && apReadReceipts.EndDatePST > DateTime.Now)
                {
                    attributes.Add(new AdAttribute("features", "read_yes", true));
                }
                else
                {
                    attributes.Add(new AdAttribute("features", "read_no", true));
                }

                #endregion

                #region Registration Confirmation Redirect

                if (Request.QueryString["tnt"] != null)
                {
                    if (Request.QueryString["tnt"] == "regconfirmationoverlay")
                    {
                        attributes.Add(new AdAttribute("features", "test_reg", true));
                    }
                }

                //Matchnet.Member.ServiceAdapters.Member objMember = MemberSA.Instance.GetMember(g.Member.MemberID, MemberLoadFlags.None);
                Matchnet.Member.ServiceAdapters.Member objMember = g.Member;

                string strColorAnalysis = objMember.GetAttributeText(Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID((int)WebConstants.BRAND_ID.AmericanSingles), "ColorCodePrimaryColor", string.Empty);
                switch (strColorAnalysis)
                {
                    case "red":
                        attributes.Add(new AdAttribute("features", "cc_red", true));
                        break;
                    case "blue":
                        attributes.Add(new AdAttribute("features", "cc_blue", true));
                        break;
                    case "yellow":
                        attributes.Add(new AdAttribute("features", "cc_yellow", true));
                        break;
                    case "white":
                        attributes.Add(new AdAttribute("features", "cc_white", true));
                        break;
                    default:
                        break;
                }

                int custodySituation = objMember.GetAttributeInt(_g.Brand, "Custody");
                if (custodySituation == Constants.NULL_INT)
                {
                    attributes.Add(new AdAttribute("features", "kids_blank", true));
                }
                else
                {
                    switch ((WebConstants.Custody)custodySituation)
                    {
                        case WebConstants.Custody.Blank:
                            attributes.Add(new AdAttribute("features", "kids_blank", true));
                            break;
                        case WebConstants.Custody.NoKids:
                            attributes.Add(new AdAttribute("features", "kids_no", true));
                            break;
                        default:
                            attributes.Add(new AdAttribute("features", "kids_yes", true));
                            break;
                    }
                }

                if (g.Member != null)
                {
                    var vipMember = VIPMailUtils.GetVIPMember(g, g.Member);
                    if (vipMember != null && vipMember.AllAccessExpireDate >= DateTime.Now)
                    {
                        attributes.Add(new AdAttribute("features", "allacc_yes", true));
                    }
                    else
                    {
                        attributes.Add(new AdAttribute("features", "allacc_no", true));
                    }
                }
                else
                {
                    attributes.Add(new AdAttribute("features", "allacc_no", true));
                }

                int NewsletterMask = g.Member.GetAttributeInt(g.Brand, "NewsletterMask", 0);
                if ((NewsletterMask & (int) NewsEventOfferMask.Events) == (int) NewsEventOfferMask.Events)
                {
                    attributes.Add(new AdAttribute("features", "invite_yes", true)); 
                }
                else
                {
                    attributes.Add(new AdAttribute("features", "invite_no", true)); 
                }

                //combining news and offers
                if ((NewsletterMask & (int)NewsEventOfferMask.News) == (int)NewsEventOfferMask.News
                    || (NewsletterMask & (int)NewsEventOfferMask.Offers) == (int)NewsEventOfferMask.Offers)
                {
                    attributes.Add(new AdAttribute("features", "newslet_y", true));
                }
                else
                {
                    attributes.Add(new AdAttribute("features", "newslet_n", true));
                }

                #endregion

                #region Reglion including JDateReligion

                if (g.Brand.Site.Community.CommunityID != (int)WebConstants.COMMUNITY_ID.JDate)
                {

                    attributes.Add(new AdAttribute("Religion", Option.GetDescription("Religion", g.Member.GetAttributeInt(g.Brand, "Religion"), g)));
                }
                else
                {
                    attributes.Add(new AdAttribute("Religion", Option.GetDescription("JDateReligion", g.Member.GetAttributeInt(g.Brand, "JDateReligion"), g)));
                }

                #endregion

                #region 



                #endregion

                if (isILGAM)
                {
                    // IL sites attributes

                    // PRM
                    attributes.Add(new AdAttribute("prm",
                                                   g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID,
                                                                       string.Empty), true));

                    // User State
                    if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                    {
                        attributes.Add(new AdAttribute("user_state", "subscriber", true));
                    }
                    else
                    {
                        attributes.Add(new AdAttribute("user_state", "registrant", true));
                    }

                    // Promo ID
                    int promoID = g.PromoPlan.PromoID;
                    if (promoID <= 0)
                    {
                        if (string.IsNullOrEmpty(g.Session["PROMO_ID_FOR_IL_GAM"]))
                        {

                            Matchnet.PromoEngine.ValueObjects.Promo promo =
                                Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetMemberPromo(
                                    g.Member.MemberID, g.Brand);
                            if (promo != null)
                            {
                                g.Session.Add("PROMO_ID_FOR_IL_GAM", promo.PromoID.ToString(), 1, false);
                            }
                            else
                            {
                                g.Session.Add("PROMO_ID_FOR_IL_GAM", "0", 1, false);
                            }
                        }
                        promoID = int.Parse(g.Session["PROMO_ID_FOR_IL_GAM"]);
                    }

                    attributes.Add(new AdAttribute("promo_id", (promoID > 0 ? promoID.ToString() : string.Empty), true));

                    //// Username
                    //attributes.Add(new AdAttribute("username", g.Member.GetUserName(_g.Brand), true));

                    // Auto-Renewal
                    if (subscriptionExpirationDate == DateTime.MinValue)
                    {
                        attributes.Add(new AdAttribute("auto_renew", "on", true));
                    }
                    else
                    {
                        attributes.Add(new AdAttribute("auto_renew", "off", true));
                    }

                    // Days since last login
                    int daysSinceLastLogin =
                        (DateTime.Now - g.Member.GetAttributeDate(g.Brand, "BrandLastLogonDateOldValue", DateTime.Now)).Days;
                    attributes.Add(new AdAttribute("since_logi", daysSinceLastLogin.ToString(), true));

                    if (daysSinceLastLogin >= 0 && daysSinceLastLogin <= 30)
                        attributes.Add(new AdAttribute("since_logi", "0-30", true));
                    else if (daysSinceLastLogin >= 31 && daysSinceLastLogin <= 60)
                        attributes.Add(new AdAttribute("since_logi", "31-60", true));
                    else if (daysSinceLastLogin >= 61 && daysSinceLastLogin <= 90)
                        attributes.Add(new AdAttribute("since_logi", "61-90", true));
                    else if (daysSinceLastLogin >= 91 && daysSinceLastLogin <= 180)
                        attributes.Add(new AdAttribute("since_logi", "91-180", true));
                    else if (daysSinceLastLogin >= 181)
                        attributes.Add(new AdAttribute("since_logi", "181_plus", true));

                    // Days Since Subscription Last Initial Purchase Date
                    DateTime subscriptionLastInitialPurchaseDate = g.Member.GetAttributeDate(g.Brand,
                                                                                             "SubscriptionLastInitialPurchaseDate", DateTime.MinValue);
                    int daysSinceLastInitialPurchase = (DateTime.Now - subscriptionLastInitialPurchaseDate).Days;
                    if (subscriptionLastInitialPurchaseDate == DateTime.MinValue)
                    {
                        daysSinceLastInitialPurchase = -1;
                    }

                    attributes.Add(new AdAttribute("since_purc", daysSinceLastInitialPurchase.ToString(), true));

                    if (daysSinceLastInitialPurchase >= 0 && daysSinceLastInitialPurchase <= 30)
                        attributes.Add(new AdAttribute("since_purc", "0-30", true));
                    else if (daysSinceLastInitialPurchase >= 31 && daysSinceLastInitialPurchase <= 60)
                        attributes.Add(new AdAttribute("since_purc", "31-60", true));
                    else if (daysSinceLastInitialPurchase >= 61 && daysSinceLastInitialPurchase <= 90)
                        attributes.Add(new AdAttribute("since_purc", "61-90", true));
                    else if (daysSinceLastInitialPurchase >= 91 && daysSinceLastInitialPurchase <= 180)
                        attributes.Add(new AdAttribute("since_purc", "91-180", true));
                    else if (daysSinceLastInitialPurchase >= 181)
                        attributes.Add(new AdAttribute("since_purc", "181_plus", true));
                }
                else
                {
                    #region Birthday - Days before and after	-355 to 365+

                    DateTime birthDate = g.Member.GetAttributeDate(_g.Brand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE);

                    DateTime currentBirthDate;

                    if (System.DateTime.IsLeapYear(birthDate.Year) && !System.DateTime.IsLeapYear(DateTime.Now.Year) &&
                        (birthDate.Month == 2) && (birthDate.Day == 29))
                    {
                        currentBirthDate = new DateTime(DateTime.Now.Year, 3, 1);
                    }
                    else
                    {
                        currentBirthDate = new DateTime(DateTime.Now.Year, birthDate.Month, birthDate.Day);
                    }

                    // Before if bdate hasn't passed.
                    // After if bdate has passed.
                    attributes.Add(new AdAttribute("birthday", (DateTime.Now.Date - currentBirthDate).Days.ToString(), true));

                    // Before if bdate hasn't passed.
                    if (currentBirthDate > DateTime.Now.Date)
                    {
                        attributes.Add(new AdAttribute("birthday", (DateTime.Now.Date - currentBirthDate.AddYears(-1)).Days.ToString(), true));
                    }
                    // After if bdate has passed.
                    else
                    {
                        attributes.Add(new AdAttribute("birthday", "-" + (currentBirthDate.AddYears(1) - DateTime.Now.Date).Days.ToString(), true));
                    }
                    #endregion

                    #region Ethnicity including JDateEthnicity

                    if (g.Brand.Site.Community.CommunityID != (int)WebConstants.COMMUNITY_ID.JDate)
                    {
                        attributes.Add(new AdAttribute("Ethnicity", Option.GetDescription("Ethnicity", g.Member.GetAttributeInt(g.Brand, "Ethnicity"), g)));
                    }
                    else
                    {
                        attributes.Add(new AdAttribute("Ethnicity", Option.GetDescription("JDateEthnicity", g.Member.GetAttributeInt(g.Brand, "JDateEthnicity"), g)));
                    }

                    #endregion

                   

                    // Income
                    attributes.Add(new AdAttribute("IncomeLevel", Option.GetDescription("IncomeLevel", g.Member.GetAttributeInt(g.Brand, "IncomeLevel"), g)));

                    // LeisureActivity
                    List<string> leisureActivies = Option.GetDescriptions("LeisureActivity", g.Member.GetAttributeInt(g.Brand, "LeisureActivity"), g);
                    foreach (string item in leisureActivies)
                    {
                        attributes.Add(new AdAttribute("LeisureActivity", item));
                    }

                    // Cuisine
                    List<string> cuisines = Option.GetDescriptions("Cuisine", g.Member.GetAttributeInt(g.Brand, "Cuisine"), g);
                    foreach (string item in cuisines)
                    {
                        attributes.Add(new AdAttribute("Cuisine", item));
                    }
                     
                    #region JRewards Membership Status
                    if (g.Brand.Site.SiteID == Convert.ToInt32(WebConstants.SITE_ID.JDate))
                    {
                        WebConstants.JRewardsMembershipStatus jrewardsStatus = g.DetermineJRewardsStatus();

                        //Write the GAM attribute with the appropriate name & value string
                        attributes.Add(new AdAttribute("encore", Enum.GetName(typeof(WebConstants.JRewardsMembershipStatus), jrewardsStatus)));

                        // eligibility_status  targeting key
                        if (g.HasValidCCOnFile)
                        {
                            attributes.Add(new AdAttribute("eligible", "cc_valid", true));

                            if (g.IsEligibleForEncoreRebateCredit() && g.HasValidCCOnFile)
                            {
                                attributes.Add(new AdAttribute("eligible", "jr_ph1_eli", true));
                            }
                        }
                        else
                        {
                            attributes.Add(new AdAttribute("eligible", "cc_invalid", true));
                        }
                    }
                    #endregion
                }

                //Registered Device OS
                string regDevice = GetGAMRegisteredDevice();
                if (!string.IsNullOrEmpty(regDevice))
                {
                    attributes.Add(new AdAttribute("reg_device", regDevice, true));
                }
            }
            else
            {
                if (isILGAM)
                {
                    attributes.Add(new AdAttribute("user_state", "visitor", true));
                }
            }

            //Generate GAM JS
            System.Text.StringBuilder addAttributesOutput = new System.Text.StringBuilder();
            var adTarWriter = new AdTargetingWriter();
            foreach (AdAttribute attribute in attributes)
            {
                // No need to do a look up
                if (attribute.UseLiteralValues)
                {
                    adTarWriter.AddTargeting(attribute.AttributeName, attribute.AttributeValue);
                    
                    //addAttributesOutput.Append(string.Format("googletag.pubads().setTargeting('{0}', '{1}');{2}",
                    //                                         attribute.AttributeName, attribute.AttributeValue,
                    //                                         Environment.NewLine));

                    //addAttributesOutput.Append("GA_googleAddAttr(\"" + attribute.AttributeName + "\",\"" + attribute.AttributeValue + "\");" + System.Environment.NewLine);
                }
                else
                {
                    DictionaryEntry entry = GetAddAttribute(attribute.AttributeName, attribute.AttributeValue);
                    if (entry.Key.ToString() != string.Empty)
                    {
                        adTarWriter.AddTargeting(entry.Key.ToString(), entry.Value.ToString());
                        
                        //addAttributesOutput.Append(string.Format("googletag.pubads().setTargeting('{0}', '{1}');{2}",
                        //                                         entry.Key, entry.Value, Environment.NewLine));

                        //addAttributesOutput.Append("GA_googleAddAttr(\"" + entry.Key + "\",\"" + entry.Value + "\");" + System.Environment.NewLine);
                    }
                }
            }

            addAttributesOutput.Append(adTarWriter.OutputTargetingJs());

            Literal literal = new Literal();
            literal.Text = addAttributesOutput.ToString();
            PlaceHolderAdAttributes.Controls.Add(literal);
        }

        /// <summary>
        /// Render ad slots based on registered slots from RegisterSlot method.
        /// </summary>
        private void populateAdSlots()
        {
            System.Text.StringBuilder adSlotsOutput = new System.Text.StringBuilder();

            foreach (string adSlot in adSlots)
            {
                if (EnableRenderAdSlotCode(adSlot, _g))
                {
                    var adSlotUi = AdHelper.GetAdSlotDimensions(adSlot);
                    adSlotsOutput.Append(string.Format("googletag.defineSlot('/{0}/{1}', [{2}, {3}], '{4}').addService(googletag.pubads());{5}",
                        GAM_NETWORK_ID, adSlot, adSlotUi.Width, adSlotUi.Height, adSlot, System.Environment.NewLine));

                    //adSlotsOutput.Append("GA_googleAddSlot(\"" + GAM_ACCOUNT_NAME + "\",\"" + adSlot + "\");" + System.Environment.NewLine);
                }
            }

            Literal literal = new Literal();
            literal.Text = adSlotsOutput.ToString();
            PlaceHolderAdSlots.Controls.Add(literal);
        }

        /// <summary>
        /// Extracts the mapped value between BH attribute option and GAM attribute option using LINQ.
        /// Implemented caching for each attribute as it would hit IO heavily w/o it.
        /// </summary>
        /// <param name="attributeName"></param>
        /// <param name="attributeOptionValue"></param>
        /// <returns></returns>
        public DictionaryEntry GetAddAttribute(string attributeName, string attributeOptionValue)
        {
            try
            {
                Attribute attribute;

                attribute = (Attribute)_Cache["GAMAttribute:" + attributeName];

                if (attribute == null)
                {
                    XDocument xdoc = XDocument.Load(Request.MapPath("/Framework/Ui/Advertising/GAM.xml"));

                    List<Attribute> attributes = (from x in xdoc.Elements("attributes").Elements("attribute")
                                                  where x.Element("name").Value == attributeName
                                                  select new Attribute
                                                  {
                                                      Name = x.Element("name").Value,
                                                      GAMName = x.Element("gamname").Value,
                                                      AttributeOptions = (from y in x.Element("attributeoptions").Elements("attributeoption")
                                                                          select new AttributeOption
                                                                          {
                                                                              Value = y.Element("value").Value,
                                                                              GAMValue = y.Element("gamvalue").Value
                                                                          }).ToList()
                                                  }).ToList();

                    attribute = attributes[0];
                    _Cache.Insert("GAMAttribute:" + attributeName, attribute);
                }

                string gamName = attribute.GAMName;

                // This try/catch is to ignore missing mapping data such as _blank option value in income.
                AttributeOption attributeOption;
                try
                {
                    attributeOption = (from x in attribute.AttributeOptions
                                       where x.Value.ToLower() == attributeOptionValue.ToLower()
                                       select x).ToList()[0];
                }
                catch (ArgumentOutOfRangeException)
                {
                    System.Diagnostics.Trace.WriteLine("Missing mapping attribute option: " + attributeName + ", " + attributeOptionValue);
                    return new DictionaryEntry(string.Empty, string.Empty);
                }

                string gamValue = attributeOption.GAMValue;

                return new DictionaryEntry(gamName.ToString(), gamValue);
            }
            catch (Exception ex)
            {
                throw new Exception("AttributeName:" + attributeName + ", AttributeOptionValue:" + attributeOptionValue, ex);
            }
        }


        private void RegisterPlaceCastScript()
        {
            try
            {

                if (_g.Member == null)
                    return;
                if (_g.AppPage.ControlName.ToLower() == "checkim")
                    return;
                if (!Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_PLACECAST_SCRIPT", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                    return;

                Content.ValueObjects.Region.RegionLanguage region = null;

                int regionid = g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID);
                region = Content.ServiceAdapters.RegionSA.Instance.RetrievePopulatedHierarchy(regionid, _g.Brand.Site.LanguageID);

                if (String.IsNullOrEmpty(region.PostalCode))
                    return;

                string script = "var  placeCast_PostalCode =\"" + region.PostalCode + "\";";

                Literal literal = new Literal();
                literal.Text = script;
                PlaceHolderPlaceCast.Controls.Add(literal);

            }
            catch (Exception ex)
            { g.ProcessException(ex); }


        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Allow other web applications from registering new ad slots. There should be no clash of same ad slots.
        /// </summary>
        /// <param name="gamAdSlotName">Name of the ad slot as defined in GAM.</param>
        /// <returns>Returns the full GAM Ad Slot Name</returns>
        public string RegisterAdSlot(string gamAdSlotName)
        {
            DictionaryEntry entry = GetAddAttribute("Site", g.Brand.Site.Name);
            if (entry.Key.ToString() == string.Empty)
                throw new Exception("GAM Attribute Site mapping not found.");

            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_IL_GAM", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                gamAdSlotName = "il_" + entry.Value + "_" + gamAdSlotName;
                gamAdSlotName = gamAdSlotName.ToLower();
            }
            else
            {
                gamAdSlotName = "bh_" + entry.Value + "_" + gamAdSlotName;
            }

            adSlots.Add(gamAdSlotName);

            return gamAdSlotName;
        }

        /// <summary>
        /// Overloaded method fo RegisterAdSlot when page mode is set to Auto. 
        /// </summary>
        /// <param name="gamAdSlotName"></param>
        /// <param name="autoGeneratePageName">prepend page name defined in GAM automatically</param>
        /// <returns>Returns the full GAM Ad Slot Name</returns>
        public string RegisterAdSlot(string gamAdSlotName, bool autoGeneratePageName)
        {
            string gamPageName = getGAMPageName(gamAdSlotName);

            gamAdSlotName = gamPageName.ToLower() + "_" + gamAdSlotName.ToLower();

            return RegisterAdSlot(gamAdSlotName);
        }

        public void UnregisterAdSlot(string gamAdSlotName)
        {
            string gamPageName = getGAMPageName(gamAdSlotName);
            string fullGamAdSlotName = gamPageName.ToLower() + "_" + gamAdSlotName.ToLower();

            DictionaryEntry entry = GetAddAttribute("Site", g.Brand.Site.Name);
            if (entry.Key.ToString() == string.Empty)
                throw new Exception("GAM Attribute Site mapping not found.");

            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_IL_GAM", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                fullGamAdSlotName = "il_" + entry.Value + "_" + fullGamAdSlotName;
                fullGamAdSlotName = fullGamAdSlotName.ToLower();
            }
            else
            {
                fullGamAdSlotName = "bh_" + entry.Value + "_" + fullGamAdSlotName;
            }

            adSlots.Remove(fullGamAdSlotName);
        }


        private string getGAMPageName(string gamAdSlotName)
        {
            string gamPageName = String.Empty;

            switch ((WebConstants.APP)g.AppPage.App.ID)
            {
                case WebConstants.APP.MembersOnline:
                    gamPageName = "mol";
                    break;
                case WebConstants.APP.Search:
                    gamPageName = "search";
                    if (g.AppPage.ControlName.ToLower() == "searchpreferences")
                        gamPageName = "searchpreferences";
                    break;
                case WebConstants.APP.HotList:
                    gamPageName = "hotlist";
                    break;
                case WebConstants.APP.Article:
                    gamPageName = "articles";
                    string category = Request["CategoryID"];
                    if (category == "2006")
                    {
                        gamPageName = "successstories";
                    }
                    else if (g.AppPage.ControlName.ToLower() == "jexperts")
                    {
                        gamPageName = "jexperts";
                    }
                    else if (g.AppPage.ControlName.ToLower() == "bachelor")
                    {
                        gamPageName = "bachelor";
                    }
                    else if (SectorsManager.GetSector() != Sector.Default && SectorsManager.GetSector() != Sector.HommesJuifs && SectorsManager.GetSector() != Sector.FemmesJuives)
                    {
                        gamPageName = "lobby_" + SectorsManager.GetSector().ToString().ToLower();
                    }
                    break;
                case WebConstants.APP.PhotoGallery:
                    gamPageName = "photogallery";
                    break;
                case WebConstants.APP.QuickSearch:
                    gamPageName = "quicksearch";
                    if (g.AppPage.ControlName.ToLower() == "quicksearchresults")
                        gamPageName = "quicksearchresults";
                    break;
                case WebConstants.APP.Home:
                    gamPageName = "home";
                    break;
                case WebConstants.APP.Email:
                    gamPageName = "email";
                    if (g.AppPage.ControlName.ToLower() == "mailbox" || g.AppPage.ControlName.Contains("messagesettings") || g.AppPage.ControlName.ToLower() == "viewmessage")
                        gamPageName = "inbox";
                    else if (g.AppPage.ControlName.ToLower() == "foldersettings")
                        gamPageName = "customfolder";
                    else if (g.AppPage.ControlName.ToLower() == "messagesent")
                    {
                        if (gamAdSlotName.Contains("overlay"))
                        {
                            gamPageName = "emailconf";
                        }
                    }
                    else if (g.AppPage.ControlName.ToLower() == "teasesent")
                    {
                        if (gamAdSlotName.Contains("overlay"))
                        {
                            gamPageName = "flirtsent";
                        }
                    }
                    else if (g.AppPage.ControlName.ToLower() == "tease")
                    {
                        gamPageName = "flirt";
                    }

                    break;
                case WebConstants.APP.Video:
                    gamPageName = "video";
                    break;
                case WebConstants.APP.MemberServices:
                    gamPageName = "MemberServices";
                    break;
                case WebConstants.APP.Logon:
                    gamPageName = "LoginPage";
                    if (g.AppPage.ControlName.ToLower() == "logoff")
                    {
                        if (gamAdSlotName.Contains("overlay"))
                        {
                            gamPageName = "logout";
                        }
                    }
                    break;
                case WebConstants.APP.MemberProfile:
                    gamPageName = "profile_basics";
                    if (g.AppPage.ControlName.ToLower().Contains("registration"))
                        gamPageName = "EditProfile";
                    else if (g.AppPage.ControlName.ToLower().Contains("memberphotoedit"))
                        gamPageName = "editprofile_photoedit";
                    else if (g.AppPage.ControlName.ToLower().Contains("attributeedit"))
                        gamPageName = "editprofile_attributeedit";
                    break;
                case WebConstants.APP.LookupProfile:
                    gamPageName = "lookupmember";
                    break;
                case WebConstants.APP.FrameNav:
                    gamPageName = "connect";
                    break;
                case WebConstants.APP.ColorCode:
                    gamPageName = "colorcoderesults";
                    break;
                case WebConstants.APP.Subscription:
                    if (g.AppPage.ControlName.ToLower() == "subscriptionconfirmation")
                    {
                        if (gamAdSlotName.Contains("overlay"))
                        {
                            gamPageName = "subconfirm";
                        }
                    }
                    break;
                case WebConstants.APP.Termination:
                    if (g.AppPage.ControlName.ToLower() == "terminatefinal")
                    {
                        if (gamAdSlotName.Contains("overlay"))
                        {
                            gamPageName = "terminateconfim";
                        }
                    }
                    break;
                case WebConstants.APP.Events:
                    if (g.AppPage.ControlName.ToLower() == "events")
                    {
                        if (gamAdSlotName.Contains("overlay"))
                        {
                            gamPageName = "travelevents";
                        }
                    }
                    break;
                case WebConstants.APP.QuestionAnswer:
                    gamPageName = "question";
                    break;
                case WebConstants.APP.AdvancedSearch:
                    gamPageName = "advancedsearch";

                    if (g.AppPage.ControlName.ToLower() == "advancedsearchresults")
                        gamPageName = "advancedsearchresults";

                    break;
                case WebConstants.APP.UserNotifications:
                    gamPageName = "newsfeed";
                    break;
                case WebConstants.APP.ContactUs:
                    gamPageName = "contactus";
                    break;
                default:
                    gamPageName = "not_available"; // This should throw an exception?
                    break;
            }

            return gamPageName;
        }

        private string GetGAMRegisteredDevice()
        {
            string regDevice = "";
            if (g.Member != null)
            {
                //GAM requires specific device naming convention
                DeviceOS deviceOS = MemberSA.Instance.GetRegisteredDevice(g.Member, g.Brand);
                switch (deviceOS)
                {
                    case DeviceOS.Android_Phone:
                        regDevice = "android_ph";
                        break;
                    case DeviceOS.Android_Tablet:
                        regDevice = "android_tb";
                        break;
                    case DeviceOS.Apple_Desktop:
                        regDevice = "apple_dt";
                        break;
                    case DeviceOS.Blackberry:
                        regDevice = "blackb";
                        break;
                    case DeviceOS.Blackberry_Tablet:
                        regDevice = "blackb_tb";
                        break;
                    case DeviceOS.IPad:
                        regDevice = "ipad";
                        break;
                    case DeviceOS.IPhone:
                        regDevice = "iphone";
                        break;
                    case DeviceOS.Linux_Desktop:
                        regDevice = "linux_dt";
                        break;
                    case DeviceOS.Linux_Phone:
                        regDevice = "linux_ph";
                        break;
                    case DeviceOS.Palm_Phone:
                        regDevice = "palm_ph";
                        break;
                    case DeviceOS.Palm_Tablet:
                        regDevice = "palm_tb";
                        break;
                    case DeviceOS.Symbian_Phone:
                        regDevice = "symbian_ph";
                        break;
                    case DeviceOS.Windows_Desktop:
                        regDevice = "windows_dt";
                        break;
                    case DeviceOS.Windows_Phone:
                        regDevice = "windows_ph";
                        break;
                    case DeviceOS.Windows_Tablet:
                        regDevice = "windows_tb";
                        break;
                }
            }

            return regDevice;
        }

        /// <summary>
        /// Generate FillSlot method call content and return to the caller.
        /// </summary>
        /// <param name="ID">ID of the control</param>
        /// <param name="gamAdSlotName">GAM slot name</param>
        /// <returns></returns>
        public static string GetFillSlot(string ID, string gamAdSlotName, ContextGlobal g)
        {
            if (EnableRenderAdSlotCode(gamAdSlotName, g))
            {
                AdSlotUI adSlotUi = AdHelper.GetAdSlotDimensions(gamAdSlotName);
                //string slotHtml =
                //    string.Format(
                //        "<div id='{0}' style='width:{1}px; height:{2}px;'><script type='text/javascript'>googletag.cmd.push(function() {{ googletag.display('{3}'); }});</script></div>",
                //        gamAdSlotName, adSlotUi.Width, adSlotUi.Height, gamAdSlotName);

                var className = string.Empty;
                
                if (gamAdSlotName.ToLower().Contains("overlay"))
                {
                    className = "gpt-overlay-container";
                }
                else if (gamAdSlotName.ToLower().Contains("wallpaper_1x1"))
                {
                    className = "gpt-wallpaper-container";
                }

                var slotHtml = string.Format("<div id=\"{0}\"{1}><script type=\"text/javascript\">googletag.cmd.push(function() {{ googletag.display('{0}'); }});</script></div>",
                    gamAdSlotName, string.IsNullOrEmpty(className) ? string.Empty : "  class=\"" + className + "\"");


                //string slotHtml = gamAdSlotName.ToLower().Contains("overlay")
                //                      ? string.Format(
                //                          "<div id=\"{0}\" class=\"gpt-overlay-container\"><script type=\"text/javascript\">googletag.cmd.push(function() {{ googletag.display('{1}'); }});</script></div>",
                //                          gamAdSlotName, gamAdSlotName)
                //                      : string.Format(
                //                          "<div id=\"{0}\"><script type=\"text/javascript\">googletag.cmd.push(function() {{ googletag.display('{1}'); }});</script></div>",
                //                          gamAdSlotName, gamAdSlotName);

                if (HttpContext.Current.Request["GAMDetail"] != null)
                {
                    slotHtml = ID + "," + gamAdSlotName + slotHtml;
                }

                return slotHtml;

                //// For troubleshooting purposes.
                //if (HttpContext.Current.Request["GAMDetail"] == null)
                //{
                //    return "<script type=\"text/javascript\">GA_googleFillSlot(\"" + gamAdSlotName + "\");</script>";
                //}
                //else
                //{
                //    return ID + "," + gamAdSlotName + "<script type=\"text/javascript\">GA_googleFillSlot(\"" + gamAdSlotName + "\");</script>";
                //}
            }

            return string.Empty;
        }

        // This method looks like it's not being used, and it won't work with GPT so I am commenting it out for now.
        /// <summary>
        /// Same as GetFillSlot but strips out Javascript tags. To be used inside Javascript.
        /// </summary>
        /// <param name="gamAdSlotName"></param>
        /// <returns></returns>
        //public static string GetStrippedFillSlot(string gamAdSlotName, ContextGlobal g)
        //{
        //    if (EnableRenderAdSlotCode(gamAdSlotName, g))
        //    {
        //        return "GA_googleFillSlot(\'" + gamAdSlotName + "\');";
        //    }
            

        //    return string.Empty;
        //}

        #endregion

        #region Private Classes

        /// <summary>
        /// To be used for GAM only.
        /// </summary>
        [Serializable]
        private class Attribute
        {
            public string Name { get; set; }
            public string GAMName { get; set; }
            public List<AttributeOption> AttributeOptions { get; set; }
        }

        /// <summary>
        /// To be used for GAM only.
        /// </summary>
        [Serializable]
        private class AttributeOption
        {
            public string Value { get; set; }
            public string GAMValue { get; set; }
        }

        /// <summary>
        /// For storing key/value items for rendering GAM Ad Attributes. Can't use Dictionary since there could be multiple keys with same value.
        /// </summary>
        private class AdAttribute
        {
            public string AttributeName { get; set; }
            public string AttributeValue { get; set; }
            public bool UseLiteralValues { get; set; }

            public AdAttribute(string attributeName, string attributeValue, bool useLiteralValues)
            {
                this.AttributeName = attributeName;
                this.AttributeValue = attributeValue;
                this.UseLiteralValues = useLiteralValues;
            }

            public AdAttribute(string attributeName, string attributeValue)
                : this(attributeName, attributeValue, false)
            {
            }
        }

        /// <summary>
        /// GPT does not support name, value pair if there are multiple values for a single key. We need to
        /// output all the values in an array format for this to work now. Use this class to automate that.
        /// </summary>
        private class AdTargetingWriter
        {
            private Dictionary<string, string> _targetingDictionary = new Dictionary<string, string>();

            public void AddTargeting(string key, string targetValue)
            {
                string currentValue;

                if (_targetingDictionary.TryGetValue(key, out currentValue))
                {
                    var newValue = currentValue.Substring(0, currentValue.Length - 1) + ", \"" + targetValue + "\"]";
                    _targetingDictionary[key] = newValue;
                }
                else
                {
                    // first time adding it
                    _targetingDictionary.Add(key, "[\"" + targetValue + "\"]");
                }
            }

            public string OutputTargetingJs()
            {
                var sb = new StringBuilder();

                foreach (var tar in _targetingDictionary)
                {
                    sb.Append(string.Format("googletag.pubads().setTargeting('{0}', {1});{2}",
                        tar.Key, tar.Value,
                        Environment.NewLine));
                }

                return sb.ToString();
            }
        }

        #endregion

        /// <summary>
        /// This value object is used for reading adslots names from the whitelist xml files
        /// </summary>
        public class AdSlots
        {
            public class AdSlot
            {
                [XmlAttribute("AdSlotName")]
                public string AdSlotName { get; set; }
            }

            public List<AdSlot> AdSlotsList { get; set; }

            public AdSlots()
            {
                AdSlotsList = new List<AdSlot>();
            }
        }
    }
}
