﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Matchnet.Web.Framework.Ui.Advertising
{
    public class AdSlotUI
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public class AdHelper
    {
        /// <summary>
        /// If there is a problem retrieving either the width or the height from the ad slot name, width and or height will be returned
        /// with the value of 1 to prevent the entire page from failing.
        /// </summary>
        /// <param name="AdSlotName"></param>
        /// <returns></returns>
        public static AdSlotUI GetAdSlotDimensions(string AdSlotName)
        {
            var width = 1;
            var height = 1;

            const string pattern = @"(\d+)x(\d+)$";
            var rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            var matches = rgx.Matches(AdSlotName);

            if (matches.Count == 1)
            {
                // first group is always the pattern match string
                if (matches[0].Groups.Count == 3)
                {
                    int.TryParse(matches[0].Groups[1].Value, out width);
                    int.TryParse(matches[0].Groups[2].Value, out height);
                }
            }

            return new AdSlotUI {Width = width, Height = height};
        }
    }
}