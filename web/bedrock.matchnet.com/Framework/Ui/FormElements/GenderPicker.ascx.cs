using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Framework.Ui.FormElements
{
	/// <summary>
	///		Summary description for GenderPicker.
	/// </summary>
	public class GenderPicker : FrameworkControl
	{
		protected ListType _listType = ListType.RadioButtonList;
		protected int _genderMask;
		protected ListControl _genderList = null;

		#region Properties
		public ListType ListType
		{
			get
			{
				return _listType;
			}
			set
			{
				_listType = value;
			}
		}

		public int GenderMask
		{
			get
			{
				int genderMask = 0;

				if (_genderList != null)
				{
					foreach (ListItem listItem in _genderList.Items)
					{
						if (listItem.Selected)
						{
							genderMask += Conversion.CInt(listItem.Value);
						}
					}
				}

				return genderMask;
			}
			set
			{
				_genderMask = value;
			}
		}

		public bool SeekingGender
		{
			get
			{
				return true; //TODO: Eventually this control could be used for both gender and seeking gender.  Right now it's just for seeking gender.
			}
		}
		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			switch (ListType)
			{
				case ListType.CheckBoxList:
					_genderList = new CheckBoxList();
					((CheckBoxList) _genderList).RepeatColumns = 2;
					break;
				case ListType.DropDownList:
					_genderList = new DropDownList();
					break;
				case ListType.RadioButtonList:
					_genderList = new RadioButtonList();
					((RadioButtonList) _genderList).RepeatColumns = 4;
					break;
			}

			DataTable dt = Option.GetOptions(WebConstants.ATTRIBUTE_NAME_GENDERMASK, g);
			_genderList.DataSource = dt;
			_genderList.DataTextField = "Content";
			_genderList.DataValueField = "Value";
			_genderList.DataBind();

			if (SeekingGender)
			{
				_genderList.Items.Remove(_genderList.Items.FindByValue(""));
				_genderList.Items.Remove(_genderList.Items.FindByValue("1"));
				_genderList.Items.Remove(_genderList.Items.FindByValue("2"));
				_genderList.Items.Remove(_genderList.Items.FindByValue("16"));
				_genderList.Items.Remove(_genderList.Items.FindByValue("32"));
			}

			SetGenderMask();

			Controls.Add(_genderList);
		}

		private void SetGenderMask()
		{
			if (_genderList != null)
			{
				foreach (ListItem listItem in _genderList.Items)
				{
					int itemValue = Conversion.CInt(listItem.Value);

					if ((itemValue & _genderMask) == itemValue)
					{
						listItem.Selected = true;
						if (_listType != ListType.CheckBoxList)
						{
							break;
						}
					}
					else
					{
						listItem.Selected = false;
					}
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}

	public enum ListType
	{
		DropDownList,
		CheckBoxList,
		RadioButtonList
	}
}
