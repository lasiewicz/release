using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework.Util;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;

using Spark.SAL;

namespace Matchnet.Web.Framework.Ui.FormElements
{
	/// <summary>
	///		Summary description for MaskControl.
	/// </summary>
	public class PickRegionNew : FrameworkControl
	{
		public delegate void RegionSavedEventHandler(EventArgs e);
		public event RegionSavedEventHandler RegionSaved;

		public delegate void ClosedEventHandler(EventArgs e);
		public event ClosedEventHandler Closed;

		public delegate void RegionResetEventHandler(EventArgs e);
		public event RegionResetEventHandler RegionReset;

		//Postal code tab
		protected DropDownList ddlPostalCodeCountry;
		protected TextBox tbPostalCode;

		//City tab
		protected Anthem.DropDownList ddlCountry;
		protected Anthem.PlaceHolder plcState;
		protected RegionLabel txtState;
		protected Anthem.DropDownList ddlState;
		protected Anthem.TextBox tbCity;
		protected Anthem.HyperLink lnkCityPopup;
		protected Anthem.LinkButton lnkResetRegion;

		//Area code tab
		protected Anthem.DropDownList ddlAreaCodeCountry;
		protected Anthem.TextBox tbAreaCode1;
		protected Anthem.TextBox tbAreaCode2;
		protected Anthem.TextBox tbAreaCode3;
		protected Anthem.TextBox tbAreaCode4;
		protected Anthem.TextBox tbAreaCode5;
		protected Anthem.TextBox tbAreaCode6;

		//College tab
		protected Anthem.DropDownList ddlCollegeCountry;
		protected Anthem.DropDownList ddlCollegeState;
		protected Anthem.DropDownList ddlCollege;

		//Repeater for tab strip
		protected Anthem.Repeater rptTabs;

		//Tab contents
		protected Anthem.PlaceHolder plcPostalCode;
		protected Anthem.PlaceHolder plcCity;
		protected Anthem.PlaceHolder plcAreaCode;
		protected Anthem.PlaceHolder plcCollege;

		protected Anthem.PlaceHolder plcError;
		protected Txt txtErrorText;
		//protected Anthem.PlaceHolder plcRegionName;
		protected Anthem.LinkButton lnkRegionName;
		protected Literal litRegionName;
		protected Anthem.Button btnSave;
		protected Anthem.LinkButton lnkClose;

		/*protected Anthem.PlaceHolder plcSuggestions;
		protected Repeater rptSuggestions;*/

        private bool showAreaCodesTab = true;
        private bool showZipCodeTab = true;
        private bool allowAnyRegion = false;

        enum ddlRegionType
        {
            country,
            state,
            city
        }
		private ArrayList _areaCodes;

		protected ArrayList AreaCodes
		{
			get
			{
				if (_areaCodes == null)
				{
					_areaCodes = new ArrayList();
				}

				return _areaCodes;
			}
		}

		public bool AddAreaCode(string areaCode, Int32 countryRegionID)
		{
			Int32 intAreaCode = Conversion.CInt(areaCode);
			if (intAreaCode > 0 && RegionSA.Instance.IsValidAreaCode(intAreaCode, countryRegionID))
			{
				AreaCodes.Add(areaCode);
				return true;
			}

			return false;
		}

		public Int32 AreaCodeCountryRegionID;

		public IEnumerator GetAreaCodes()
		{
			return AreaCodes.GetEnumerator();
		}

		public string RegionName
		{
            get
            {
                return litRegionName.Text;
            }
			set
			{
				litRegionName.Text = value;
				lnkRegionName.UpdateAfterCallBack = true;
			}
		}

		public Int32 RegionID;
		public Int32 SchoolID;

		protected RegionLanguage Region
		{
			get
			{
				return RegionSA.Instance.RetrievePopulatedHierarchy(RegionID, g.Brand.Site.LanguageID);
			}
		}

		private void PickRegionNew_Init(object sender, EventArgs e)
		{
			try
			{
                lnkClose.Attributes.Add("onclick", "unPopDiv(\"searchPopupDiv\")");
                lnkRegionName.Attributes.Add("onclick", "unPopDiv(\"searchPopupDiv\")");
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				btnSave.Text = g.GetResource("BTN_SAVE", this);

				// Reset region only applies for members only  
				if (g.Member == null)
				{
					lnkResetRegion.Visible = false;
				}
				else
				{
					lnkResetRegion.Visible = true;
					lnkResetRegion.Text = g.GetResource("TXT_RESET_REGION", this);
				}

				if (!IsPostBack)
				{
					clearControlValues();
				}

				bindTabs();
				bindData();

				loadControlValues();
                if(!showAreaCodesTab)
                    plcAreaCode.Visible = false;
                if(!showZipCodeTab)
                    plcPostalCode.Visible = false;
                //if (!showAreaCodesTab)
                //{
                //    ddlAreaCodeCountry.Visible = false;
                //    TextBox[] areaCodeBoxes = new TextBox[] { tbAreaCode1, tbAreaCode2, tbAreaCode3, tbAreaCode4, tbAreaCode5, tbAreaCode6 };
                //    for (Int32 i = 0; i < areaCodeBoxes.GetUpperBound(0) + 1; i++)
                //    {
                       
                //            areaCodeBoxes[i].Visible=false;
                       
                //    }
                //}
               
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}


		private string getSessionValue(Control control)
		{
			return g.Session[control.ID + "_Value"];
		}

		private void loadControlValues()
		{
			loadControlValues(Controls);
		}

		private void loadControlValues(ControlCollection controls)
		{
			foreach (Control control in controls)
			{
				loadControlValues(control.Controls);

				string controlValue = getSessionValue(control);

				if (controlValue != null)
				{
					switch (control.GetType().Name)
					{
						case "TextBox":
							TextBox textBox = control as TextBox;
							if (controlValue != string.Empty)
							{
								textBox.Text = controlValue.Trim();
							}
							break;
						case "DropDownList":
							DropDownList dropDownList = control as DropDownList;
							FrameworkGlobals.SelectItem(dropDownList, controlValue);
							break;
					}
				}
			}
		}
		//Have to do this because viewstate is off -- see http://weblogs.asp.net/despos/archive/2005/03/16/394834.aspx
		private void saveControlValues()
		{
			saveControlValues(Controls);
		}

		private void saveControlValues(ControlCollection controls)
		{
			foreach (Control control in controls)
			{
				saveControlValues(control.Controls);

				string controlValue = null;

				switch (control.GetType().Name)
				{
					case "TextBox":
						TextBox textBox = control as TextBox;
						controlValue = textBox.Text;

						//HACK: When we load the control value from the session, if the value is not present,
						//the Session service returns string.empty.  Therefore, we store a single space here
						//to distinguish an explicit empty string value from the absence of a value.
						if (controlValue == string.Empty)
						{
							controlValue = " ";
						}
						break;
					case "DropDownList":
						DropDownList dropDownList = control as DropDownList;
						controlValue = dropDownList.SelectedValue;
						break;
				}

				if (controlValue != null)
				{
					g.Session.Add(control.ID + "_Value", controlValue, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
				}
			}
		}

		private void clearControlValues()
		{
			clearControlValues(Controls);
		}

		private void clearControlValues(ControlCollection controls)
		{
			foreach (Control control in controls)
			{
				clearControlValues(control.Controls);

				switch (control.GetType().Name)
				{
					case "TextBox":
					case "DropDownList":
						g.Session.Remove(control.ID + "_Value");
						break;
				}
			}
		}

		private DataTable _tabData;

		private DataTable TabData
		{
			get
			{
				if (_tabData == null)
				{
					_tabData = new DataTable();
					_tabData.Columns.Add("SearchType", typeof(SearchType));
					_tabData.Columns.Add("Title", typeof(string));
					_tabData.Columns.Add("TabPlaceHolder", typeof(Anthem.PlaceHolder));
                    DataRow row = _tabData.NewRow();
                    if (showZipCodeTab)
                    {
                       
                        row["SearchType"] = SearchType.PostalCode;
                        row["Title"] = g.GetResource("TXT_POSTAL_CODE", this);
                        row["TabPlaceHolder"] = plcPostalCode;
                        _tabData.Rows.Add(row);
                    }

                    if (showAreaCodesTab)
                    {
                        row = _tabData.NewRow();
                        row["SearchType"] = SearchType.AreaCode;
                        row["Title"] = g.GetResource("TXT_AREA_CODE", this);
                        row["TabPlaceHolder"] = plcAreaCode;
                        _tabData.Rows.Add(row);
                    }

					row = _tabData.NewRow();
					row["SearchType"] = SearchType.Region;
					row["Title"] = g.GetResource("TXT_REGION_CITY", this);
					row["TabPlaceHolder"] = plcCity;
					_tabData.Rows.Add(row);

					if (g.Brand.Site.Community.CommunityID == (Int32) WebConstants.COMMUNITY_ID.College)
					{
						row = _tabData.NewRow();
						row["SearchType"] = SearchType.College;
						row["Title"] = g.GetResource("TXT_COLLEGE", this);
						row["TabPlaceHolder"] = plcCollege;
						_tabData.Rows.Add(row);
					}
				}

				return _tabData;
			}
		}

		private void bindTabs()
		{
			rptTabs.DataSource = TabData;
			rptTabs.DataBind();
		}

		private Int32 getDropDownValue(DropDownList list)
		{
			Int32 val = Conversion.CInt(getSessionValue(list));

			if (val == Constants.NULL_INT)
			{
				val = Conversion.CInt(list.SelectedValue);
			}

			return val;
		}

		private void bindData()
		{
            if (showZipCodeTab)
            {
                bindPostalCodeCountries();
                bindPostalCode();
            }
			bindCountries();
			bindStates(getDropDownValue(ddlCountry));
			bindCity();
            if (showAreaCodesTab)
            {
                bindAreaCodeCountries();
                bindAreaCodes();
            }
			bindCollegeCountries();
			bindCollegeStates(getDropDownValue(ddlCollegeCountry));
			bindCollege(getDropDownValue(ddlCollegeState));
		}

		public SearchType SearchType
		{
			get
			{
				return (SearchType) Conversion.CInt(g.Session[WebConstants.SESSION_PROPERTY_NAME_SEARCH_TYPE]);
			}
			set
			{
				g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_SEARCH_TYPE, ((Int32) value).ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
			}
		}

        public bool ShowAreaCodesTab
        {
            get { return showAreaCodesTab; }
            set { showAreaCodesTab = value; }
        }

        public bool ShowZipCodesTab
        {
            get { return showZipCodeTab; }
            set { showZipCodeTab = value; }
        }

        public bool AllowAnyRegion
        {
            get { return allowAnyRegion; }
            set { allowAnyRegion = value; }
        }

		private void rptTabs_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					Anthem.LinkButton lnkTab = (Anthem.LinkButton) e.Item.FindControl("lnkTab");

					DataRowView row = (DataRowView) e.Item.DataItem;
					SearchType searchType = (SearchType) row["SearchType"];
					Anthem.PlaceHolder plcTab = (Anthem.PlaceHolder) row["TabPlaceHolder"];

					if (searchType == SearchType) //If this is the active tab
					{
						lnkTab.CssClass = "prefTabItemActive";
						plcTab.Visible = true;
					}
					else
					{
						lnkTab.CssClass = "prefTabItem";
						plcTab.Visible = false;
					}
					plcTab.UpdateAfterCallBack = true;

					lnkTab.Text = row["Title"].ToString();
					lnkTab.CommandArgument = ((Int32) searchType).ToString();
					lnkTab.Click += new EventHandler(lnkTab_Click);
					lnkTab.UpdateAfterCallBack = true;
				}
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void lnkTab_Click(object sender, EventArgs e)
		{
			try
			{
				plcError.Visible = false;
				plcError.UpdateAfterCallBack = true;

				Anthem.LinkButton lnkTab = (Anthem.LinkButton) sender;
				SearchType = (SearchType) Conversion.CInt(lnkTab.CommandArgument);
				bindTabs();
				rptTabs.UpdateAfterCallBack = true;
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		#region City Tab Binding
		private void bindCountries()
		{
			ddlCountry.DataSource = RegionSA.Instance.RetrieveCountries(g.Brand.Site.LanguageID);
			ddlCountry.DataTextField = "Description";
			ddlCountry.DataValueField = "RegionID";
			ddlCountry.DataBind();
         
			if (RegionID > 0)
			{
				FrameworkGlobals.SelectItem(ddlCountry, Region.CountryRegionID.ToString());
			}
		}

		private void bindStates(Int32 countryRegionID)
		{
			if ((countryRegionID <= 0 || !countryHasStates(countryRegionID)))
			{
				plcState.Visible = false;
			}
			else
			{
				plcState.Visible = true;
				plcState.UpdateAfterCallBack = true;

				RegionCollection stateRegions = RegionSA.Instance.RetrieveChildRegions(countryRegionID, g.Brand.Site.LanguageID);
				ddlState.DataSource = stateRegions;
				ddlState.DataTextField = "Description";
				ddlState.DataValueField = "RegionID";
				ddlState.DataBind();
				
				//ddlState.Items.Insert(0, new ListItem("",Constants.NULL_INT.ToString()));
                ddlState.Items.Insert(0, GetEmptyListItem(ddlRegionType.state));

				try
				{	// Determine how the State/Region label should render.
					int regionMask = stateRegions[0].Mask;

					if (regionMask != 8)
					{
						regionMask -= 2;
					}
					txtState.RegionID = stateRegions[0].RegionID;
					txtState.RegionMask = regionMask;
				}
				catch (Exception ex)
				{
					g.ProcessException(ex);
				}

				if (Region.StateRegionID > 0)
				{
					FrameworkGlobals.SelectItem(ddlState, Region.StateRegionID.ToString());
				}
			}
		}

		private void bindCity()
		{
			tbCity.Text = Region.CityName;
		}

		private bool countryHasStates(Int32 countryRegionID)
		{
			return RegionSA.Instance.RetrieveRegionByID(countryRegionID, g.Brand.Site.LanguageID).ChildrenDepth == 2;
		}
		#endregion

		#region Postal Code Tab Binding
		private void bindPostalCodeCountries()
		{	
			ListItem li = null;

			li = new ListItem(g.GetResource("USA", this), Matchnet.Lib.ConstantsTemp.REGIONID_USA.ToString());
			ddlPostalCodeCountry.Items.Add(li);

			li = new ListItem(g.GetResource("CANADA", this), Matchnet.Lib.ConstantsTemp.REGIONID_CANADA.ToString());
			ddlPostalCodeCountry.Items.Add(li);

			FrameworkGlobals.SelectItem(ddlPostalCodeCountry, Region.CountryRegionID.ToString());
		}

		private void bindPostalCode()
		{
			tbPostalCode.Text = Region.PostalCode;
		}

		#endregion

		#region Area Code Tab Binding
		
		private void bindAreaCodeCountries()
		{
			ddlAreaCodeCountry.Items.Add(new ListItem(g.GetResource("USA", this), Matchnet.Lib.ConstantsTemp.REGIONID_USA.ToString()));
			ddlAreaCodeCountry.Items.Add(new ListItem(g.GetResource("CANADA", this), Matchnet.Lib.ConstantsTemp.REGIONID_CANADA.ToString()));

			if (g.Brand.Site.Community.CommunityID == (int) WebConstants.COMMUNITY_ID.Cupid || 
				g.Brand.Site.SiteID == (int) WebConstants.SITE_ID.JDateCoIL )
			{
				ddlAreaCodeCountry.Items.Add(new ListItem(g.GetResource("ISRAEL", this), Matchnet.Lib.ConstantsTemp.REGIONID_ISRAEL.ToString()));
				//AreaCodeCountryRegionID=Matchnet.Lib.ConstantsTemp.REGIONID_ISRAEL;
			}

			// prior to this code, AreaCodeCountryRegionID is correctly set
			int regionid=Region.CountryRegionID;
			if ( AreaCodeCountryRegionID == 0 ) 
			{
				// TPJ-98
				// assumes validity of a preset, and the preset not being overridden from the public property
				if (
					regionid == Matchnet.Lib.ConstantsTemp.REGIONID_CANADA	|| 
					regionid == Matchnet.Lib.ConstantsTemp.REGIONID_USA		|| 
					regionid == Matchnet.Lib.ConstantsTemp.REGIONID_ISRAEL
					) 
					AreaCodeCountryRegionID=regionid;
			}
/*
			int regionid=Region.CountryRegionID;
			if(regionid ==Matchnet.Lib.ConstantsTemp.REGIONID_CANADA || regionid ==Matchnet.Lib.ConstantsTemp.REGIONID_USA || regionid ==Matchnet.Lib.ConstantsTemp.REGIONID_ISRAEL) 
				AreaCodeCountryRegionID=regionid;
*/

			if (AreaCodeCountryRegionID > 0)
			{
				FrameworkGlobals.SelectItem(ddlAreaCodeCountry, AreaCodeCountryRegionID.ToString());
			}
		}

		private void bindAreaCodes()
		{
			TextBox[] areaCodeBoxes = new TextBox[] {tbAreaCode1, tbAreaCode2, tbAreaCode3, tbAreaCode4, tbAreaCode5, tbAreaCode6};

			for (Int32 i = 0; i < areaCodeBoxes.GetUpperBound(0) + 1; i++)
			{
				if (i < AreaCodes.Count && Conversion.CInt(AreaCodes[i]) > 0)
				{
					areaCodeBoxes[i].Text = AreaCodes[i].ToString();
				}
				else
				{
					areaCodeBoxes[i].Text = string.Empty;
				}
			}
		}

		#endregion

		#region College Tab Binding

		private void bindCollegeCountries()
		{
			ddlCollegeCountry.Items.Clear();
			ddlCollegeCountry.Items.Add(new ListItem(g.GetResource("USA", this), Matchnet.Lib.ConstantsTemp.REGIONID_USA.ToString()));
			ddlCollegeCountry.Items.Add(new ListItem(g.GetResource("CANADA", this), Matchnet.Lib.ConstantsTemp.REGIONID_CANADA.ToString()));
			
			SchoolRegionCollection regions = RegionSA.Instance.RetrieveSchoolParents(SchoolID);
			if (regions != null && regions.Count > 0)
			{
				FrameworkGlobals.SelectItem(ddlCollegeCountry, regions[0].CountryRegionID.ToString());
			}
		}

		private void bindCollegeStates(Int32 countryRegionID)
		{
			ddlCollegeState.DataSource = RegionSA.Instance.RetrieveChildRegions(countryRegionID, g.Brand.Site.LanguageID);
			ddlCollegeState.DataTextField = "Description";
			ddlCollegeState.DataValueField = "RegionID";
			ddlCollegeState.DataBind();

			SchoolRegionCollection regions = RegionSA.Instance.RetrieveSchoolParents(SchoolID);
			if (regions != null && regions.Count > 0)
			{
				FrameworkGlobals.SelectItem(ddlCollegeState, regions[0].StateProvinceRegionID.ToString());
			}
		}

		private void bindCollege(Int32 stateRegionID)
		{
			ddlCollege.DataSource = RegionSA.Instance.RetrieveSchoolListByStateRegion(stateRegionID);
			ddlCollege.DataTextField = "Name";
			ddlCollege.DataValueField = "SchoolRegionID";
			ddlCollege.DataBind();

			FrameworkGlobals.SelectItem(ddlCollege, SchoolID.ToString());
		}

		#endregion

		private void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				bindStates(Conversion.CInt(ddlCountry.SelectedValue));
				ddlState.UpdateAfterCallBack = true;
				tbCity.Text = string.Empty;
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}
	
		private void ddlState_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				//Sometimes this event fires when it shouldn't
				if (Request[ddlState.UniqueID] != getSessionValue(ddlState))
				{
					tbCity.Text = string.Empty;
					tbCity.UpdateAfterCallBack = true;
				}
				//TODO: REMOVE THIS BLOCK
				else
				{
					g.ProcessException(new Exception("SelectedIndexChanged event fired unnecessarily."));
				}
				/////////
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void ddlCollegeCountry_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				bindCollegeStates(Conversion.CInt(ddlCollegeCountry.SelectedValue));
				ddlCollegeState.UpdateAfterCallBack = true;

				bindCollege(Conversion.CInt(ddlCollegeState.SelectedValue));
				ddlCollege.UpdateAfterCallBack = true;
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}
	
		private void ddlCollegeState_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				bindCollege(Conversion.CInt(ddlCollegeState.SelectedValue));
				ddlCollege.UpdateAfterCallBack = true;
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init += new EventHandler(PickRegionNew_Init);
			this.Load += new System.EventHandler(this.Page_Load);
			this.PreRender += new EventHandler(PickRegionNew_PreRender);
			ddlCountry.SelectedIndexChanged += new EventHandler(ddlCountry_SelectedIndexChanged);
			ddlState.SelectedIndexChanged += new EventHandler(ddlState_SelectedIndexChanged);
			ddlCollegeCountry.SelectedIndexChanged += new EventHandler(ddlCollegeCountry_SelectedIndexChanged);
			ddlCollegeState.SelectedIndexChanged += new EventHandler(ddlCollegeState_SelectedIndexChanged);
			btnSave.Click += new EventHandler(btnSave_Click);
			lnkClose.Click += new EventHandler(lnkClose_Click);
			lnkRegionName.Click += new EventHandler(lnkClose_Click);
			rptTabs.ItemDataBound += new RepeaterItemEventHandler(rptTabs_ItemDataBound);
			lnkResetRegion.Click += new EventHandler(lnkResetRegion_Click);
			//tbCity.TextChanged += new EventHandler(tbCity_TextChanged);
		}
		#endregion

		private const Int32 MAX_SUGGESTIONS = 8;

		/*private void tbCity_TextChanged(object sender, EventArgs e)
		{
			Int32 parentRegionID = Conversion.CInt(ddlState.SelectedValue);
			if (parentRegionID != Constants.NULL_INT)
			{
				Matchnet.Content.ValueObjects.Region.RegionCollection regionMatches = RegionSA.Instance.RetrieveChildRegions(parentRegionID, g.Brand.Site.LanguageID, tbCity.Text[0] + "%");
				ArrayList suggestions = new ArrayList();

				for (Int32 i = 0; i < MAX_SUGGESTIONS && i < regionMatches.Count; i++)
				{
					suggestions.Add(regionMatches[i].Description);
				}

				rptSuggestions.ItemDataBound += new RepeaterItemEventHandler(rptSuggestions_ItemDataBound);
				rptSuggestions.DataSource = suggestions;
				rptSuggestions.DataBind();
			}
		}*/

		private void rptSuggestions_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			Literal litCityName = (Literal) e.Item.FindControl("litCityName");
			litCityName.Text = e.Item.DataItem.ToString();
		}

		private void PickRegionNew_PreRender(object sender, EventArgs e)
		{
			try
			{
				saveControlValues();

				Page.RegisterClientScriptBlock("CountryCityPopup", FrameworkGlobals.MakePopCityListJavaScript(_g.Brand.BrandID, ddlCountry.ClientID, tbCity.ClientID, "popCityListCountry"));
				Page.RegisterClientScriptBlock("StateCityPopup", FrameworkGlobals.MakePopCityListJavaScript(_g.Brand.BrandID, ddlState.ClientID, tbCity.ClientID, "popCityListState"));
				lnkCityPopup.NavigateUrl = countryHasStates(Conversion.CInt(ddlCountry.SelectedValue)) ? "javascript:popCityListState();" : "javascript:popCityListCountry()";
				lnkCityPopup.UpdateAfterCallBack = true;
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				bool success = false;

				switch (SearchType)
				{
					case SearchType.PostalCode:
						Int32 countryRegionID = Conversion.CInt(ddlPostalCodeCountry.SelectedValue);
						RegionID postalCodeRegionID = RegionSA.Instance.FindRegionIdByPostalCode(countryRegionID, tbPostalCode.Text);
						if (postalCodeRegionID != null && postalCodeRegionID.ID > 0)
						{
							RegionID = postalCodeRegionID.ID;
							success = true;
						}
						txtErrorText.ResourceConstant = "TXT_INVALID_POSTAL_CODE";
						break;
					case SearchType.AreaCode:
						AreaCodes.Clear();
						countryRegionID = Conversion.CInt(ddlAreaCodeCountry.SelectedValue);
						success |= AddAreaCode(tbAreaCode1.Text, countryRegionID);
						success |= AddAreaCode(tbAreaCode2.Text, countryRegionID);
						success |= AddAreaCode(tbAreaCode3.Text, countryRegionID);
						success |= AddAreaCode(tbAreaCode4.Text, countryRegionID);
						success |= AddAreaCode(tbAreaCode5.Text, countryRegionID);
						success |= AddAreaCode(tbAreaCode6.Text, countryRegionID);
						txtErrorText.ResourceConstant = "TXT_INVALID_AREA_CODE";
						break;
					case SearchType.Region:
						countryRegionID = Conversion.CInt(ddlCountry.SelectedValue);

                        Int32 parentRegionID;
                        RegionID cityRegionID;

                        if (countryHasStates(countryRegionID))
                        {
                            // Selected country that has states 
                            parentRegionID = Conversion.CInt(ddlState.SelectedValue);
                            if (parentRegionID < 0 )
                            {
                                // Required state was not selected 
                                if (!allowAnyRegion)
                                {
                                    txtErrorText.ResourceConstant = "TXT_INVALID_STATE";
                                    break;
                                }
                                else
                                {
                                    parentRegionID = countryRegionID;  
                                }
                            }
                           
                        }
                        else
                        {
                            parentRegionID = countryRegionID;    
                        }

                        // Either country that has states was selected along with 
                        // a required state or a country that has no states was 
                        // selected 
                        cityRegionID = RegionSA.Instance.FindRegionIdByCity(parentRegionID, tbCity.Text, g.Brand.Site.LanguageID);                                        
                        if (cityRegionID != null && cityRegionID.ID > 0)
                        {
                            RegionID = cityRegionID.ID;
                            success = true;
                        }
                        else
                        {
                            if (allowAnyRegion)
                            {
                                RegionID = parentRegionID;
                                success = true;
                            }
                            else
                            {
                                txtErrorText.ResourceConstant = "TXT_INVALID_CITY";
                            }
                        }                                                                       
						break;
					case SearchType.College:
						SchoolID = Conversion.CInt(ddlCollege.SelectedValue);
						success = true;
						break;
				}

				if (success)
				{
					RegionSaved(new EventArgs());
					plcError.Visible = false;
                    Anthem.Manager.AddScriptForClientSideEval("unPopDiv(\"searchPopupDiv\")");
				}
				else
				{
					plcError.Visible = true;
				}

				plcError.UpdateAfterCallBack = true;
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void lnkClose_Click(object sender, EventArgs e)
		{
			try
			{
				clearControlValues();

				if (Closed != null)
				{
					Closed(new EventArgs());
				}

				bindTabs();
				bindData();
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void lnkResetRegion_Click(object sender, EventArgs e)
		{
			RegionReset(new EventArgs());

			bindCountries();
			string selectedCountry=ddlCountry.SelectedValue;
			bindStates(Conversion.CInt(selectedCountry));
			bindCity();
		}

        private ListItem GetEmptyListItem(ddlRegionType regionType)
        {   ListItem l;
            string displayTxt="";
            if (allowAnyRegion)
            {
                displayTxt = g.GetResource("DDL_ANY_" + regionType.ToString().ToUpper(), this);
            }

            l = new ListItem(displayTxt, Constants.NULL_INT.ToString());
            return l;

        }
	}
}
