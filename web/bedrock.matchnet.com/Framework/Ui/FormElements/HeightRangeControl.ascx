<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="HeightRangeControl.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.FormElements.HeightRangeControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<div class="search-pref" id="preferenceControl" runat="server"><!-- begin heightRangeControl div -->
	<div class="search-pref-header <%if (IsCollapsed){%>search-pref-closed<%}else{ %>search-pref-open<%} %>">
		<span class="spr <%if (IsCollapsed){%>s-icon-arrow-right<%}else{ %>s-icon-arrow-down<%} %>"></span>
        <a href="#" class="title"><mn:txt id="txtPreferenceName" Runat="server" /></a>
		<span id="divSelectedItems" class="search-pref-selected-option" runat="server">
		    <asp:Literal ID="litCurrentRange" Runat="server" />
	    </span>
        <%if (IsSliderWeightEnabled)
          { %>
        <em class="value-wrapper importance"><span class="value"></span></em>
        <%} %>
    </div>
	
	<div <% if (IsCollapsed){ %> style="display:none;"<% }%> class="search-pref-container">	
        <%if (IsSliderWeightEnabled)
          { %>
        <div class="importance clearfix">
            <h4 class="title">Importance</h4>
            <div class="slider"></div>
            <em class="value-wrapper"><span class="value"></span></em>
        </div>
        <%} %>
        <span class="preferencesContainerHeight">
			<p>
				<mn:txt id="txtFrom" Runat="server" ResourceConstant="FROM" /> 
				<asp:PlaceHolder ID="phMinHeight" Runat="server" />
			</p>
			<p>
				<mn:txt id="txtTo" Runat="server" ResourceConstant="TO" />
				<asp:PlaceHolder ID="phMaxHeight" Runat="server" />
			</p>
		</span>
	</div>
    <asp:HiddenField ID="sliderValue" runat="server" />
</div><!-- end heightRangeControl div -->

<script type="text/javascript">
    if (<%=IsSliderWeightEnabled.ToString().ToLower()%>){
        preferenceSliderControl($j('#<%=preferenceControl.ClientID %>'), '<%=sliderValue.ClientID %>',<%=SearchWeightValue.ToString()%>);
    }
    preferenceToggler($j('#<%=preferenceControl.ClientID %> .search-pref-header'), $j('#<%=preferenceControl.ClientID %> .search-pref-container'), 'search-pref-open', 'search-pref-closed', 'spr spr s-icon-arrow-right', 'spr spr s-icon-arrow-down');
</script>
