using System;
using System.Web;
using System.Web.UI;
using System.ComponentModel;

namespace Matchnet.Web.Framework.Ui.FormElements
{
	/// <summary>
	/// Summary description for FrameworkButton.
	/// </summary>
	[DefaultProperty("Text"), 
		ToolboxData("<{0}:FrameworkHtmlButton runat=server></{0}:FrameworkHtmlButton>")]
	public class FrameworkHtmlButton : System.Web.UI.HtmlControls.HtmlInputButton
	{
		// private Translator.Resources _Resource;
		ContextGlobal g;
		string _ResourceConstant;

		[System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name="FullTrust")] 
		protected override void OnInit(EventArgs e)
		{
			g = (ContextGlobal)HttpContext.Current.Items["g"];

			base.OnInit(e);
			
		}

		protected override void RenderAttributes(HtmlTextWriter writer)
		{
			base.Attributes.Add("value", g.GetResource(ResourceConstant, this));

			base.RenderAttributes (writer);
		}

		public string ResourceConstant
		{
			get
			{
				return _ResourceConstant;
			}
			set
			{
				_ResourceConstant = value;
			}
		}

	}
}
