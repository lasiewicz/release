<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PickRegionNew.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.FormElements.PickRegionNew" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<script type="text/javascript">

function popUpDiv( obj, H, T ) {
	var bodyDirection = document.body.getAttribute("dir");
    var divE = document.getElementById(obj);
    
	if ( bodyDirection == "rtl" ) {
		with( divE.style ) {
		display = "block";
		position = "absolute";
		right = H;
		top = T;
		}
	} else {
		with( divE.style ) {
		display = "block";
		position = "absolute";
		left = H;
		top = T;
		}
	}
}

function unPopDiv( obj ) {
    var divE = document.getElementById(obj);
    divE.style.display = "none";
}

</script>


<div id="searchPopupDiv">
<div class="shadowBox" id="shadowBoxContainer">
<div class="shadowBox" id="shadowBoxContent">

<div class="prefRegionPickerContainer">

 <div class="prefRegionPickerHeader">
 	<anthem:LinkButton ID="lnkRegionName" Runat="Server"><strong><mn:image runat="server" id="prefRegionPickerHeaderArrow" Filename="icon-pref-on.gif" />&nbsp;
 	    <asp:Literal ID="litRegionName" Runat="server" /></strong>
 	</anthem:LinkButton>
 </div>

 <div class="prefRegionPickerPopupClose">
	<anthem:LinkButton id="lnkClose" Runat="server">
		<mn:txt id="Txt6" runat="server" ResourceConstant="TXT_CLOSE" />
	</anthem:LinkButton>
 </div>

<anthem:PlaceHolder ID="plcError" Runat="server" Visible="false">
	<div class="error" style="width:auto"><mn:txt ID="txtErrorText" Runat="server"/></div>
</anthem:PlaceHolder>

 <div class="prefRegionPickerTabsContainer">
	<anthem:Repeater ID="rptTabs" Runat="server">
		<HeaderTemplate>
			<div id="prefTab">
		</HeaderTemplate>
		<ItemTemplate>
			<anthem:LinkButton ID="lnkTab" Runat="server" />
		</ItemTemplate>
		<FooterTemplate>
			</div>
		</FooterTemplate>
	</anthem:Repeater>
</div><!-- end prefSearchSearchDiv -->

<anthem:PlaceHolder ID="plcPostalCode" Runat="server">
		<div class="prefRegionPickerItems">
			<div class="prefRegionPickerFormTitle"><strong><mn:txt id="txtCountry" runat="server" ResourceConstant="TXT_COUNTRY" />: </strong></div>
			<div class="prefRegionPickerFormInput"><asp:DropDownList ID="ddlPostalCodeCountry" Runat="server" /></div>
		</div>
		<div class="prefRegionPickerItems">
			<div class="prefRegionPickerFormTitle"><strong><mn:txt id="txtPostalCode" runat="server" ResourceConstant="TXT_POSTAL_CODE" />: </strong></div>
			<div class="prefRegionPickerFormInput"><asp:TextBox ID="tbPostalCode" Runat="server" MaxLength="7" /></div>
		</div>
</anthem:PlaceHolder>

<anthem:PlaceHolder ID="plcCity" Runat="server">

		<div class="prefRegionPickerItems">
			<div class="prefRegionPickerFormTitle"><strong><mn:txt id="txtCountry2" runat="server" ResourceConstant="TXT_COUNTRY" />: </strong></div>
			<div class="prefRegionPickerFormInput"><anthem:DropDownList ID="ddlCountry" Runat="server" AutoCallBack="True" /></div>
		</div>

		<anthem:PlaceHolder ID="plcState" Runat="server">
			<div class="prefRegionPickerItems">
				<div class="prefRegionPickerFormTitle"><strong><mn:RegionLabel Runat="server" ID="txtState" />: </strong></div>
				<div class="prefRegionPickerFormInput"><anthem:DropDownList ID="ddlState" Runat="server" AutoCallBack="True" /></div>
			</div>
		</anthem:PlaceHolder>
		
		<div class="prefRegionPickerItems">
			<div class="prefRegionPickerFormTitle"><strong><mn:txt id="txtCity" runat="server" ResourceConstant="TXT_CITY" />: </strong></div>
			<div class="prefRegionPickerFormInput"><anthem:TextBox ID="tbCity" Runat="server" AutoCallBack="False" MaxLength="100" />
			<%--anthem:PlaceHolder ID="plcCitySuggestions" Runat="server">
				<asp:Repeater ID="rptSuggestions" Runat="server">
					<ItemTemplate>
						<asp:Literal ID="litCityName" Runat="server" /><br />
					</ItemTemplate>
				</asp:Repeater>
			</anthem:PlaceHolder--%>

				<div class="prefRegionPickerCityLinks">
					<anthem:HyperLink id="lnkCityPopup" Runat="server" class="default">
						<span id="lookuplink" name="lookuplink"> <mn:txt id="txtLookupCity" runat="server" ResourceConstant="TXT_LOOK_UP_CITY" /></span></anthem:HyperLink><br />
					<anthem:LinkButton ID="lnkResetRegion" Runat="server" />
				</div>
			</div>
		</div>

</anthem:PlaceHolder>

<anthem:PlaceHolder ID="plcAreaCode" Runat="server">
	<div class="prefRegionPickerItems">
		<div class="prefRegionPickerFormTitle">
			<anthem:DropDownList ID="ddlAreaCodeCountry" Runat="server" />
		</div>
		<div class="prefRegionPickerFormInput">
			<anthem:TextBox style="width:30px;margin:2px;margin-top:0;" ID="tbAreaCode1" Runat="server" MaxLength="3" />
			<anthem:TextBox style="width:30px;margin:2px;margin-top:0;" ID="tbAreaCode2" Runat="server" MaxLength="3" />
			<anthem:TextBox style="width:30px;margin:2px;margin-top:0;" ID="tbAreaCode3" Runat="server" MaxLength="3" />
			<anthem:TextBox style="width:30px;margin:2px;margin-top:0;" ID="tbAreaCode4" Runat="server" MaxLength="3" />
			<anthem:TextBox style="width:30px;margin:2px;margin-top:0;" ID="tbAreaCode5" Runat="server" MaxLength="3" />
			<anthem:TextBox style="width:30px;margin:2px;margin-top:0;" ID="tbAreaCode6" Runat="server" MaxLength="3" />
		</div>
	</div>
</anthem:PlaceHolder>

<anthem:PlaceHolder ID="plcCollege" Runat="server" Visible="false">
	<div style="padding:4px;">
		<anthem:DropDownList style="margin:4px;" ID="ddlCollegeCountry" AutoCallBack="True" Runat="server" />
		<anthem:DropDownList style="margin:4px;" ID="ddlCollegeState" AutoCallBack="True" Runat="server" />
		<anthem:DropDownList style="margin:4px;" ID="ddlCollege" Runat="server" />
	</div>
</anthem:PlaceHolder>

<div class="prefRegionPickerItems">
	<div align="center" style="margin: 8px;">
		<anthem:Button ID="btnSave" Runat="server" class="activityButton" />
   </div>
</div>

</div><!-- end style div -->

</div></div><!-- end shadow divs -->
</div><!-- end hide div -->
