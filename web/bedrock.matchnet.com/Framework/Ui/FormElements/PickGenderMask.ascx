<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PickGenderMask.ascx.cs" Inherits="Matchnet.Web.Applications.Admin.EditProfile.Controls.PickGenderMask" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table cellpadding="1" cellspacing="0">
	<tr>
		<td class="defaultSmall">Member Gender</td>
		<td class="defaultSmall"><asp:PlaceHolder id="placeMemberGender" runat="server"></asp:PlaceHolder></td>
	</tr>
	<tr>
		<td class="defaultSmall">
			Seeking Gender</td>
		<td class="defaultSmall">
			<asp:PlaceHolder id="placeSeekingGender" runat="server"></asp:PlaceHolder></td>
	</tr>
</table>
