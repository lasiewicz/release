using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Lib;
using Matchnet.PhotoSearch.ServiceAdapters;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.Web.Applications.MembersOnline.Controls;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Search;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Framework.Ui
{
    /// <summary>
    /// Contains methods and types to assist in maintaining
    /// a breadcrumb trail.
    /// </summary>
    public class BreadCrumbHelper
    {
        /// <summary>
        /// EntryPoint defines various types of entry points
        /// into the application.
        /// </summary>
        public enum EntryPoint
        {
            /// <summary>
            /// Search Results.
            /// </summary>
            SearchResults = 1,

            /// <summary>
            /// Hot Lists.
            /// </summary>
            HotLists = 2,

            /// <summary>
            /// Members Online.
            /// </summary>
            MembersOnline = 3,

            /// <summary>
            /// Home Page.
            /// </summary>
            HomePage = 4,

            /// <summary>
            /// Thumb Trail.
            /// </summary>
            ThumbTrail = 5,

            /// <summary>
            /// Messages.
            /// </summary>
            Messages = 6,

            /// <summary>
            /// Look Up Member.
            /// </summary>
            LookUpMember = 7,

            /// <summary>
            /// ???
            /// </summary>
            AstroScope = 9,

            /// <summary>
            /// Admin.
            /// </summary>
            AdminView = 8,

            /// <summary>
            /// Photo Gallery.
            /// </summary>
            PhotoGallery = 10,

            /// <summary>
            /// Quick Search Results.
            /// </summary>
            QuickSearchResults = 11,  //11062008 TL - Added entry point to represent quick search

            /// <summary>
            /// Keyword Search Results.
            /// </summary>
            KeywordSearchResults = 12,

            /// <summary>
            /// Reverse Search Results.
            /// </summary>
            ReverseSearchResults = 13,
            /// <summary>
            /// Question Answer page
            /// </summary>
            QuestionPage = 14,

            /// <summary>
            /// MemberSlideshow
            /// </summary>
            MemberSlideshow = 15,

            /// <summary>
            /// Advanced Search Results.
            /// </summary>
            AdvancedSearchResults = 16,
            /// <summary>
            /// HomeHeroProfileMatches
            /// </summary>
            HomeHeroProfileMatches = 17,
            /// HomeHeroProfileMOL
            /// </summary>
            HomeHeroProfileMOL = 18,
            /// Secret Admirer Page
            /// </summary>
            SlideShowPage = 19,
            /// <summary>
            /// HomeHeroProfileMatches
            /// </summary>
            HomeHeroProfileFilmStripMatches = 20,
            /// HomeHeroProfileMOL
            /// </summary>
            HomeHeroProfileFilmStripMOL = 21,
            /// HomeHeroProfileMOL
            /// </summary>
            HomeHeroProfileFilmStripYourProfile = 22,
            /// <summary>
            /// Clicked from MemberLike profile link.
            /// </summary>
            MemberLikeLink = 23,
            /// <summary>
            /// Clicked from Member Profile.
            /// </summary>
            MemberProfile = 24,
            /// Home page viewed you widget
            HomeViewedYouWidget = 25,
            /// <summary>
            /// Home filmstrip with voting (merged between your matches and secret admirer)
            /// </summary>
            HomeProfileFilmstripWithVoting = 26,
            HomeProfileFilmstripWithVotingMOL = 27,
            /// <summary>
            /// Displaying MOL results for JDIL Bachelor page
            /// </summary>
            Bachelor = 99997,
            /// <summary>
            /// This is from UserNotificationFactory, special case where memberID and g.Member are the same!
            /// </summary>
            NotificationCreate = 99998,
            /// Nada.
            /// </summary>
            NoArgs = 99999,

            /// <summary>
            /// None provided.
            /// </summary>
            Unknown = 100000
        }

        [Flags]
        public enum PremiumEntryPoint
        {
            NoTracking = 0,
            NoPremium = 1,
            Highlight = 2,
            Spotlight = 4
        }

        #region Constants

        private static class QuickSearchConstants
        {
            public static class Params
            {
                public const string OrderBy = "MOOB";
                public const string GenderMask = "MOGM";
                public const string RegionId = "MOR";
                public const string AgeMinimum = "MOAM";
                public const string AgeMaximum = "MOAX";
                public const string LanguageMask = "MOLM";
            }

        }

        public const string DefaultNextProfileText = "&#187;";
        public const string ViewProfileBase = "/Applications/MemberProfile/ViewProfile.aspx";
        public const string JavascriptBackLink = "javascript:history.back();";



        #endregion Constants

        #region Properties

        public int DomainId { get; private set; }

        public EntryPoint MyEntryPoint { get; private set; }

        public int MemberId { get; private set; }

        public ContextGlobal G { get; private set; }

        public MOCollection MyMOCollection { get; private set; }

        public int HotListCategoryID { get; private set; }

        #endregion

        #region Constructors

        public BreadCrumbHelper(EntryPoint myEntryPoint, ContextGlobal g, int memberID, MOCollection myMOCollection, int hotListCategoryID)
        {
            DomainId = g.Brand.Site.Community.CommunityID;
            G = g;
            MemberId = memberID;
            MyMOCollection = myMOCollection;
            HotListCategoryID = hotListCategoryID;
            MyEntryPoint = myEntryPoint;
        }

        #endregion

        private static string GetViewProfileLinkBase(int memberID, ParameterCollection parameters, bool forceSameWindow)
        {
            return GetViewProfileLinkBase(memberID, parameters, forceSameWindow, string.Empty, EntryPoint.Unknown, Constants.NULL_INT);
        }

        private static string GetViewProfileLinkBase(int memberID, ParameterCollection parameters, bool forceSameWindow, string url, EntryPoint entryPoint, int ownerMemberID)
        {
            string viewProfileBase = ViewProfileBase;

            var g = (ContextGlobal)HttpContext.Current.Items["g"];

            if (HttpContext.Current.Items["g"] != null && g.IsSecureRequest())
            {
                viewProfileBase = FrameworkGlobals.LinkHref(ViewProfileBase, true);
            }

            //04082008 TL - MPR-102 - Set default to true; Following logic will override it appropriately
            bool newWindow = true;

            LayoutTemplate layoutTemplate = LayoutTemplate.Wide2Columns;

            if (forceSameWindow)
            {
                newWindow = false;
            }

            //04082008 TL - MPR-102 - Check settings to disable profile popups
            //12042008 TL - Site Redesign Phase 2, disable profile popup for site2.0 members.
            if (IsProfilePopupDisabled(memberID) || g.IsSite20Enabled)
            {
                newWindow = false;
                layoutTemplate = LayoutTemplate.Wide2Columns;
            }

            //11182010 TL - Re-enable profile popup for Mingle
            if (IsProfilePopupEnabledForSite(g.Brand))
            {
                if (!IsProfilePopupDisabled(memberID, entryPoint, ownerMemberID))
                {
                    newWindow = true;
                    layoutTemplate = LayoutTemplate.WidePopup;
                }
            }


            string thisUrl = (string.IsNullOrEmpty(url)) ? HttpContext.Current.Request.Url.ToString() : url;

            return LinkFactory.Instance.GetLink(
                viewProfileBase
                , parameters
                , g.Brand
                , thisUrl
                , false
                , newWindow
                , g.Brand.Site.SiteID.ToString() + "MemberProfile" //memberID.ToString() -- TL: Keep one popup window per site
                , 760 //width
                , 640 //height
                , "scrollbars=yes,resizable=yes"
                , false
                , layoutTemplate);
        }

        public static bool IsProfilePopupEnabledForSite(Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            return Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_POPUP_VIEW_PROFILE", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
        }

        /// <summary>
        /// Determines whether popup is disabled for current logged in member
        /// </summary>
        /// <param name="memberID">Member ID of current profile</param>
        /// <returns></returns>
        public static bool IsProfilePopupDisabled(int memberID)
        {
            return IsProfilePopupDisabled(memberID, EntryPoint.Unknown, Constants.NULL_INT);
        }

        public static bool IsProfilePopupDisabled(int memberID, EntryPoint entryPoint, int ownerMemberID)
        {
            ContextGlobal g;

            try
            {
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Items["g"] != null)
                    {
                        g = (ContextGlobal)HttpContext.Current.Items["g"];

                        if (g.Member != null)
                        {
                            //for certain situations like User Notification, determination of popup links
                            //are not based on logged in user, but the "owner" which is the recipient of the notification since the URL
                            //is pre-generated and saved as part of notifications
                            int popupOwnerMemberID = g.Member.MemberID;
                            if (ownerMemberID > 0)
                            {
                                popupOwnerMemberID = ownerMemberID;
                            }

                            if (memberID != popupOwnerMemberID || entryPoint == EntryPoint.NotificationCreate)
                            {
                                bool popEnabled = IsProfilePopupEnabledForSite(g.Brand);

                                if (popEnabled && ProfileUtility.IsProfile30Enabled(g))
                                {
                                    //check A/B
                                    bool popupABEnabled = false;
                                    try { popupABEnabled = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROFILE30_ENABLE_POPUP_AB", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)); }
                                    catch { popupABEnabled = false; }
                                    if (popupABEnabled && g.Member != null)
                                    {
                                        int memberIDMod = (popupOwnerMemberID % 2);
                                        if (memberIDMod > 0) //odd
                                            popEnabled = true;
                                        else
                                            popEnabled = false;
                                    }
                                }

                                return !popEnabled;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                        {
                            //visitors - no popup
                            return true;
                        }
                    }
                }

                //default
                return true;
            }
            catch
            {
                return true;
            }

        }

        public static string AppendParamToProfileLink(string profileURL, string param)
        {
            string revisedParam = param;
            if (param.StartsWith("&"))
                revisedParam = param.Substring(1);

            if (IsJavascriptProfileLink(profileURL))
            {
                int questionMarkIndex = profileURL.IndexOf("?");
                profileURL = profileURL.Insert(questionMarkIndex + 1, revisedParam + "&");
            }
            else
            {
                profileURL += "&" + revisedParam;
            }

            return profileURL;
        }

        public static bool IsJavascriptProfileLink(string profileURL)
        {
            if (!string.IsNullOrEmpty(profileURL) && profileURL.StartsWith("javascript:"))
                return true;
            else
                return false;
        }

        public static string MakeViewProfileLink(int pMemberID)
        {
            return MakeViewProfileLink(EntryPoint.NoArgs, pMemberID);
        }

        public static string MakeViewProfileLink(EntryPoint pEntryPoint, int pMemberID)
        {
            return MakeViewProfileLink(pEntryPoint, pMemberID, 1, null, Constants.NULL_INT);
        }

        public static string MakeViewProfileLink(EntryPoint entryPoint, int memberID, int ordinal, MOCollection moCollection, int hotListCategoryID)
        {
            return MakeViewProfileLink(entryPoint, memberID, ordinal, moCollection, hotListCategoryID, false);
        }

        public static string MakeViewProfileLink(EntryPoint entryPoint, int memberID, int ordinal, MOCollection moCollection, int hotListCategoryID, bool forceSameWindow)
        {
            return MakeViewProfileLink(entryPoint, memberID, ordinal, moCollection, hotListCategoryID, forceSameWindow, (int)PremiumEntryPoint.NoTracking);
        }

        public static string MakeViewProfileLink(EntryPoint entryPoint, int memberID, int ordinal, MOCollection moCollection, int hotListCategoryID, bool forceSameWindow, int premiumEntryPointVal)
        {
            return MakeViewProfileLink(entryPoint, memberID, ordinal, moCollection, hotListCategoryID, forceSameWindow, premiumEntryPointVal, string.Empty, Constants.NULL_INT);
        }

        public static string MakeViewProfileLink(EntryPoint entryPoint, int memberID, int ordinal, MOCollection moCollection, int hotListCategoryID, bool forceSameWindow, int premiumEntryPointVal, string url, int ownerMemberID)
        {
            var parameters = new ParameterCollection();

            parameters.Add("MemberID", memberID.ToString());

            if (ordinal >= 0)
            {
                parameters.Add("Ordinal", ordinal.ToString());
            }

            switch (entryPoint)
            {
                case EntryPoint.ReverseSearchResults:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.ReverseSearchResults).ToString());
                    break;

                case EntryPoint.KeywordSearchResults:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.KeywordSearchResults).ToString());
                    break;

                case EntryPoint.SearchResults:

                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.SearchResults).ToString());
                    break;

                case EntryPoint.HotLists:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.HotLists).ToString());
                    parameters.Add("HotListCategoryID", hotListCategoryID.ToString());
                    break;

                case EntryPoint.MembersOnline:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.MembersOnline).ToString());
                    parameters = AddMOLParameters(parameters, moCollection);
                    break;

                case EntryPoint.HomePage:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.HomePage).ToString());
                    break;

                case EntryPoint.Messages:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.Messages).ToString());
                    break;
                case EntryPoint.PhotoGallery:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.PhotoGallery).ToString());
                    break;
                case EntryPoint.QuickSearchResults:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.QuickSearchResults).ToString());
                    break;
                case EntryPoint.AdvancedSearchResults:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.AdvancedSearchResults).ToString());
                    break;
                case EntryPoint.QuestionPage:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.QuestionPage).ToString());
                    break;
                case EntryPoint.MemberSlideshow:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.MemberSlideshow).ToString());
                    break;
                case EntryPoint.HomeHeroProfileMatches:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.HomeHeroProfileMatches).ToString());
                    break;
                case EntryPoint.HomeHeroProfileMOL:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.HomeHeroProfileMOL).ToString());
                    parameters = AddMOLParameters(parameters, moCollection);
                    break;
                case EntryPoint.HomeHeroProfileFilmStripMatches:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.HomeHeroProfileFilmStripMatches).ToString());
                    break;
                case EntryPoint.HomeProfileFilmstripWithVoting:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.HomeProfileFilmstripWithVoting).ToString());
                    break;
                case EntryPoint.HomeHeroProfileFilmStripMOL:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.HomeHeroProfileFilmStripMOL).ToString());
                    parameters = AddMOLParameters(parameters, moCollection);
                    break;
                case EntryPoint.HomeProfileFilmstripWithVotingMOL:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.HomeProfileFilmstripWithVotingMOL).ToString());
                    parameters = AddMOLParameters(parameters, moCollection);
                    break;
                case EntryPoint.SlideShowPage:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.SlideShowPage).ToString());
                    break;
                case EntryPoint.NotificationCreate:
                    parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, ((int)EntryPoint.NotificationCreate).ToString());
                    break;
                default:
                    try
                    {
                        parameters.Add(WebConstants.URL_PARAMETER_NAME_ENTRYPOINT, (Convert.ToInt32(entryPoint).ToString()));
                    }
                    catch (Exception ignore) { }
                    break;
            }

            if (premiumEntryPointVal != (int)PremiumEntryPoint.NoTracking)
            {
                // TO TRACK THE PREMIUM SERVICE THAT THE TARGET MEMBER OWNS
                // ONLY TRACK WHEN AN EXPLICIT PREMIUM ENTRY POINT WAS PROVIDED 
                // IN THE TARGET MEMBER PROFILE  
                parameters.Add(WebConstants.URL_PARAMETER_NAME_PREMIUM_ENTRYPOINT, (premiumEntryPointVal).ToString());
                //parameters.Add(WebConstants.URL_PARAMETER_NAME_PREMIUM_ENTRYPOINT, ((int)premiumEntryPoint).ToString());
            }

            return GetViewProfileLinkBase(memberID, parameters, forceSameWindow, url, entryPoint, ownerMemberID);
        }

        #region Link Creation Routines

        public static ParameterCollection AddMOLParameters(ParameterCollection parameterCollection, MOCollection coll)
        {
            // In other parts of the code (i.e. VisitorLimits) the paths are too long.  If a value is a NULL number, then
            // it will not be added to the QueryString.
            parameterCollection.Add(QuickSearchConstants.Params.OrderBy, coll.OrderBy.ToString());
            if (coll.GenderMask != Constants.NULL_INT)
                parameterCollection.Add(QuickSearchConstants.Params.GenderMask, coll.GenderMask.ToString());
            if (coll.RegionID != Constants.NULL_INT)
                parameterCollection.Add(QuickSearchConstants.Params.RegionId, coll.RegionID.ToString());
            if (coll.AgeMin != Constants.NULL_INT)
                parameterCollection.Add(QuickSearchConstants.Params.AgeMinimum, coll.AgeMin.ToString());
            if (coll.AgeMax != Constants.NULL_INT)
                parameterCollection.Add(QuickSearchConstants.Params.AgeMaximum, coll.AgeMax.ToString());
            if (coll.LanguageMask != Constants.NULL_INT)
                parameterCollection.Add(QuickSearchConstants.Params.LanguageMask, coll.LanguageMask.ToString());

            return parameterCollection;
        }

        #endregion

        #region Main SetBreadCrumbs routine

        /// <summary>
        /// This method is provided for setting the URL for the 'Next Profile' link displayed on the YNM Vote Bar when
        /// a member is viewing a profile from a list (search results, hot lists, members online, etc.)
        /// It has common functionality with that of the bread crumb trail which is why it is implemented here.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="request"></param>
        /// <param name="g"></param>
        public static string StaticGetNextProfileLink(int memberID, HttpRequest request, ContextGlobal g)
        {
            MOCollection moCollection = AssembleMOCollection(request);
            int hotListCategoryID = Conversion.CInt(request["HotListCategoryID"]);
            string sMyEntryPoint = request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT];
            int iMyEntryPoint = Conversion.CInt(sMyEntryPoint);
            int ordinal = Conversion.CInt(request["Ordinal"]);
            EntryPoint myEntryPoint = GetEntryPoint(memberID, iMyEntryPoint);

            BreadCrumbHelper helper = new BreadCrumbHelper(myEntryPoint, g, memberID, moCollection, hotListCategoryID);
            return helper.GetNextProfileLink(ordinal);
        }

        /// <summary>
        /// This method is provided for setting the URL for the 'Previous Profile' Link, based off of the StaticGetNextProfileLink
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="request"></param>
        /// <param name="g"></param>
        public static string StaticGetPrevProfileLink(int memberID, HttpRequest request, ContextGlobal g)
        {
            MOCollection moCollection = AssembleMOCollection(request);
            int hotListCategoryID = Conversion.CInt(request["HotListCategoryID"]);
            string sMyEntryPoint = request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT];
            int iMyEntryPoint = Conversion.CInt(sMyEntryPoint);
            int ordinal = Conversion.CInt(request["Ordinal"]);
            EntryPoint myEntryPoint = GetEntryPoint(memberID, iMyEntryPoint);

            BreadCrumbHelper helper = new BreadCrumbHelper(myEntryPoint, g, memberID, moCollection, hotListCategoryID);
            return helper.GetPrevProfileLink(ordinal);
        }

        public static void StaticSetBreadCrumbs(int memberID, HttpRequest request, ContextGlobal g)
        {
            MOCollection mOCollection = AssembleMOCollection(request);
            int hotListCategoryID = Conversion.CInt(request["HotListCategoryID"]);
            string sMyEntryPoint = request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT];
            int iMyEntryPoint = Conversion.CInt(sMyEntryPoint);
            int ordinal = Conversion.CInt(request["Ordinal"]);
            EntryPoint myEntryPoint = GetEntryPoint(memberID, iMyEntryPoint);

            if (memberID == 0)
            {
                memberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
            }
            BreadCrumbHelper helper = new BreadCrumbHelper(myEntryPoint, g, memberID, mOCollection, hotListCategoryID);
            helper.SetBreadCrumbs(ordinal);
        }

        /// <summary>
        /// This method retrieves the URL for the 'Next Profile' links on the new tabbed profile pages, which includes a query parameter
        /// to indicate the active tab.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="request"></param>
        /// <param name="g"></param>
        /// <returns></returns>
        public static string StaticGetNextProfileLinkWithActiveTab(int memberID, HttpRequest request, ContextGlobal g)
        {
            string nextURL = StaticGetNextProfileLink(memberID, request, g);

            //add active tab query parameter
            if (nextURL != null && nextURL.Trim() != "")
            {
                if (nextURL.ToLower().IndexOf("javascript") >= 0)
                {
                    //tabProfileObject.currentTab is a javascript object that contains name of current active tab

                    string jsActiveTab = "' + '" + WebConstants.URL_PARAMETER_NAME_PROFILETAB + "=' + tabProfileObject.currentTab + '&";
                    int indexQuestionMark = nextURL.IndexOf("?");
                    string urlPart = nextURL.Substring(0, indexQuestionMark + 1);
                    string paramPart = nextURL.Substring(indexQuestionMark + 1);

                    nextURL = urlPart + jsActiveTab + paramPart;
                }
            }

            return nextURL;

        }

        /// <summary>
        /// This method retrieves the URL for the 'Prev Profile' links on the new tabbed profile pages, which includes a query parameter
        /// to indicate the active tab.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="request"></param>
        /// <param name="g"></param>
        /// <returns></returns>
        public static string StaticGetPreviousProfileLinkWithActiveTab(int memberID, System.Web.HttpRequest request, ContextGlobal g)
        {
            string prevURL = StaticGetPrevProfileLink(memberID, request, g);

            //add active tab query parameter
            if (prevURL != null && prevURL.Trim() != "")
            {
                if (prevURL.ToLower().IndexOf("javascript") >= 0)
                {
                    //tabProfileObject.currentTab is a javascript object that contains name of current active tab

                    string jsActiveTab = "' + '" + WebConstants.URL_PARAMETER_NAME_PROFILETAB + "=' + tabProfileObject.currentTab + '&";
                    int indexQuestionMark = prevURL.IndexOf("?");
                    string urlPart = prevURL.Substring(0, indexQuestionMark + 1);
                    string paramPart = prevURL.Substring(indexQuestionMark + 1);

                    prevURL = urlPart + jsActiveTab + paramPart;
                }
            }

            return prevURL;

        }


        public static EntryPoint GetEntryPoint(int pMemberId, int pEntryPoint)
        {
            EntryPoint entryPoint;
            //	If you have a valid memberId but no entry point then you
            //	are looking at someone else from a non-set link.  If
            //	you don't have a valid memberID and you don't have an
            //	entry point, then you are looking at yourself.

            if (pMemberId != 0 && pEntryPoint == Constants.NULL_INT)
            {
                entryPoint = EntryPoint.Unknown;
            }
            else if (pMemberId == 0 && pEntryPoint == Constants.NULL_INT)
            {
                entryPoint = EntryPoint.NoArgs;
            }
            else
            {
                entryPoint = (EntryPoint)pEntryPoint;
            }
            return entryPoint;
        }

        public static EntryPoint GetEntryPoint(int pEntryPoint)
        {
            EntryPoint entryPoint;

            //copied and modified from namespace Matchnet.Web.Framework.Ui.BreadCrumbHelper
            //by Michael Conway in order to accomidate Toms request for a public GetEntryPoint method in ContextGlobal
            //during E-cards implimentation

            if (pEntryPoint == Constants.NULL_INT)
            {
                entryPoint = EntryPoint.NoArgs;
            }
            else
            {
                entryPoint = (EntryPoint)pEntryPoint;
            }
            return entryPoint;
        }

        public static MOCollection AssembleMOCollection(HttpRequest request)
        {
            return AssembleMOCollection(request, EntryPoint.Unknown);
        }


        public static MOCollection AssembleMOCollection(HttpRequest request, EntryPoint entryPoint)
        {
            int genderMask;
            int regionID;
            int ageMin;
            int ageMax;
            int languageMask;

            MOCollection moCollection;

            if (entryPoint == EntryPoint.Bachelor)
            {
                System.Web.UI.Page page = new System.Web.UI.Page();
                MembersOnlineFilter20 molf = (MembersOnlineFilter20)page.LoadControl("/Applications/MembersOnline/Controls/MembersOnlineFilter20.ascx");
                System.Web.UI.HtmlControls.HtmlForm Form1 = new System.Web.UI.HtmlControls.HtmlForm();
                Form1.Controls.Add(molf);
                page.Controls.Add(Form1);
                StringWriter sw = new StringWriter();
                HttpContext.Current.Server.Execute(page, sw, false);


                genderMask = molf.GetGenderMask();
                regionID = molf.intRegionID;
                ageMin = molf.intAgeMin;
                ageMax = molf.intAgeMax;
                languageMask = molf.intLanguageMask;

                moCollection = new MOCollection(SortFieldType.HasPhoto.ToString(), genderMask, regionID, ageMin, ageMax, languageMask);
            }
            else
            {
                string orderBy = request[QuickSearchConstants.Params.OrderBy];

                genderMask = request[QuickSearchConstants.Params.GenderMask] != null ?
                    Conversion.CInt(request[QuickSearchConstants.Params.GenderMask]) : Constants.NULL_INT;

                regionID = request[QuickSearchConstants.Params.RegionId] != null ?
                    Conversion.CInt(request[QuickSearchConstants.Params.RegionId]) : Constants.NULL_INT;

                ageMin = request[QuickSearchConstants.Params.AgeMinimum] != null ?
                    Conversion.CInt(request[QuickSearchConstants.Params.AgeMinimum]) : Constants.NULL_INT;

                ageMax = request[QuickSearchConstants.Params.AgeMaximum] != null ?
                    Conversion.CInt(request[QuickSearchConstants.Params.AgeMaximum]) : Constants.NULL_INT;

                languageMask = request[QuickSearchConstants.Params.LanguageMask] != null ?
                    Conversion.CInt(request[QuickSearchConstants.Params.LanguageMask]) : Constants.NULL_INT;

                moCollection = new MOCollection(orderBy, genderMask, regionID, ageMin, ageMax, languageMask);
            }

            return moCollection;
        }

        private string GetNextProfileLink(int ordinal)
        {
            if (G.NextProfileUrlGenerated)
            {
                return G.NextProfileUrl;
            }

            if (ordinal == Constants.NULL_INT)
            {
                ordinal = 0;
            }

            int nextMemberID = RetrieveNextMemberID(ordinal);
            // Skip check Member == null here.  Allow both authenticated and anonymouse users to view (Next Profile).
            if (nextMemberID == G.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID))
            {
                nextMemberID = RetrieveNextMemberID(ordinal + 1);
                G.NextProfileUrl = MakeNextLink(nextMemberID, ordinal + 1);
            }
            else
            {
                G.NextProfileUrl = MakeNextLink(nextMemberID, ordinal);
            }

            G.NextProfileUrlGenerated = true;
            return G.NextProfileUrl;
        }

        private string GetPrevProfileLink(int ordinal)
        {
            if (!G.PreviousProfileUrlGenerated)
            {
                if (ordinal == Constants.NULL_INT)
                {
                    ordinal = 0;
                }

                var prevMemberID = RetrievePreviousMemberId(ordinal);
                // Skip check Member == null here.  Allow both authenticated and anonymouse users to view (Previous Profile).
                if (prevMemberID == G.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID))
                {
                    prevMemberID = RetrievePreviousMemberId(ordinal - 1);

                    G.PreviousProfileUrl = MakePreviousLink(prevMemberID, ordinal - 1);
                }
                else
                {
                    switch (MyEntryPoint)
                    {
                        case EntryPoint.KeywordSearchResults:
                            CreateKeywordSearchPrevious(ordinal, prevMemberID);
                            break;
                        case EntryPoint.ReverseSearchResults:
                            CreateReverseSearchPrevious(ordinal, prevMemberID);
                            break;
                        default:
                            G.PreviousProfileUrl = MakePreviousLink(prevMemberID, ordinal);
                            G.PreviousProfileUrlGenerated = true;
                            break;
                    }
                }

                return G.PreviousProfileUrl;
            }

            return G.PreviousProfileUrl;
        }

        private void CreateKeywordSearchPrevious(int ordinal, int prevMemberID)
        {
            if (ordinal > 1)
            {
                G.PreviousProfileUrl = MakePreviousLink(prevMemberID, ordinal);
                G.PreviousProfileUrlGenerated = true;
            }
            else
            {
                G.PreviousProfileUrl = String.Empty;
                G.PreviousProfileUrlGenerated = false;
            }
        }

        private void CreateReverseSearchPrevious(int ordinal, int prevMemberID)
        {
            if (ordinal > 1)
            {
                G.PreviousProfileUrl = MakePreviousLink(prevMemberID, ordinal);
                G.PreviousProfileUrlGenerated = true;
            }
            else
            {
                G.PreviousProfileUrl = String.Empty;
                G.PreviousProfileUrlGenerated = false;
            }
        }

        public int RetrieveNextMemberID(int ordinal)
        {
            switch (MyEntryPoint)
            {
                case EntryPoint.SearchResults:
                    return RetrieveNextMemberIDForSearchResults(ordinal);

                case EntryPoint.HotLists:
                    return RetrieveNextMemberIDForHotList(ordinal);

                case EntryPoint.MembersOnline:
                    return RetrieveNextMemberIDForMembersOnline(ordinal);

                case EntryPoint.HomePage:
                    return RetrieveNextMemberIDForHomePage(ordinal);

                case EntryPoint.ThumbTrail:
                    return RetrieveNextMemberIDForThumbTrail(ordinal);

                case EntryPoint.PhotoGallery:
                    return RetrieveNextMemberIDForPhotoGallery(ordinal);

                case EntryPoint.QuickSearchResults:
                    return RetrieveNextMemberIDForQuickSearchResults(ordinal);

                case EntryPoint.KeywordSearchResults:
                    return RetrieveNextMemberIDForKeywordSearchResults(ordinal);

                case EntryPoint.ReverseSearchResults:
                    return RetrieveNextMemberIDForReverseSearchResults(ordinal);

                default:
                    return 0;
            }
        }

        public int RetrievePreviousMemberId(Int32 ordinal)
        {
            switch (MyEntryPoint)
            {
                case EntryPoint.SearchResults:
                    return RetrievePreviousMemberIDForSearchResults(ordinal);

                case EntryPoint.HotLists:
                    return RetrievePreviousMemberIDForHotList(ordinal);

                case EntryPoint.MembersOnline:
                    return RetrievePreviousMemberIDForMembersOnline(ordinal);

                case EntryPoint.HomePage:
                    return RetrievePreviousMemberIDForHomePage(ordinal);

                case EntryPoint.ThumbTrail:
                    return RetrievePreviousMemberIDForThumbTrail(ordinal);

                case EntryPoint.PhotoGallery:
                    return RetrievePreviousMemberIDForPhotoGallery(ordinal);

                case EntryPoint.QuickSearchResults:
                    return RetrievePreviousMemberIDForQuickSearchResults(ordinal);

                case EntryPoint.KeywordSearchResults:
                    return RetrievePreviousMemberIDForKeywordSearchResults(ordinal);

                case EntryPoint.ReverseSearchResults:
                    return RetrievePreviousMemberIDForReverseSearchResults(ordinal);

                default:
                    return 0;
            }
        }

        public void SetBreadCrumbs(int ordinal)
        {
            if (ordinal == Constants.NULL_INT)
            {
                ordinal = 0;
            }

            string nextLink = GetNextProfileLink(ordinal);

            switch (MyEntryPoint)
            {
                case EntryPoint.SearchResults:
                    SetBreadCrumbsForSearchResults(ordinal, nextLink);
                    break;
                case EntryPoint.HotLists:
                    SetBreadCrumbsForHotLists(ordinal, nextLink);
                    break;
                case EntryPoint.MembersOnline:
                    SetBreadCrumbsForMembersOnline(ordinal, nextLink);
                    break;
                case EntryPoint.HomePage:
                    SetBreadCrumbsForHomePage(ordinal, nextLink);
                    break;
                case EntryPoint.ThumbTrail:
                    SetBreadCrumbsForThumbTrail(ordinal, nextLink);
                    break;
                case EntryPoint.Messages:
                    SetBreadCrumbsForMessages();
                    break;
                case EntryPoint.LookUpMember:
                    SetBreadCrumbsForLookUpMember();
                    break;
                case EntryPoint.AstroScope:
                    SetBreadCrumbForAstroScope();
                    break;
                case EntryPoint.NoArgs:
                    SetBreadCrumbsForNoArgs();
                    break;
                default:
                    SetBreadCrumbsForError(ordinal);
                    break;
            }
        }

        #endregion

        public string MakeNextLink(int nextMemberID, int ordinal)
        {
            //	If there is no nextMemberID then there is no next link.
            string retVal = null;
            if (nextMemberID != 0)
            {
                string profileLink = MakeViewProfileLink(MyEntryPoint, nextMemberID, ordinal + 1, MyMOCollection, HotListCategoryID, true);

                if (MyEntryPoint == EntryPoint.KeywordSearchResults)
                {
                    profileLink = BreadCrumbHelper.AppendParamToProfileLink(profileLink, "isnavrequest=true");
                }
                else if (MyEntryPoint == EntryPoint.ReverseSearchResults)
                {
                    profileLink = BreadCrumbHelper.AppendParamToProfileLink(profileLink, getReverseSearchParams(Direction.Next));
                }

                if (BreadCrumbHelper.IsJavascriptProfileLink(profileLink))
                {
                    retVal = profileLink;
                }
                else
                {
                    retVal = "javascript:window.location = '" + profileLink + "';";
                }

            }


            return (retVal);
        }

        private enum Direction
        {
            Previous,
            Next
        }

        public string MakePreviousLink(int previousMemberID, int ordinal)
        {
            //	If there is no nextMemberID then there is no next link.
            string retVal = null;
            if (previousMemberID > 0)
            {
                string profileLink = MakeViewProfileLink(MyEntryPoint, previousMemberID, ordinal - 1, MyMOCollection, HotListCategoryID, true);

                if (MyEntryPoint == EntryPoint.KeywordSearchResults)
                {
                    profileLink = BreadCrumbHelper.AppendParamToProfileLink(profileLink, "isnavrequest=true");
                }
                else if (MyEntryPoint == EntryPoint.ReverseSearchResults)
                {
                    profileLink = BreadCrumbHelper.AppendParamToProfileLink(profileLink, getReverseSearchParams(Direction.Previous));
                }

                if (BreadCrumbHelper.IsJavascriptProfileLink(profileLink))
                {
                    retVal = profileLink;
                }
                else
                {
                    retVal = "javascript:window.location = '" + profileLink + "';";
                }

            }
            return (retVal);
        }

        private String getReverseSearchParams(Direction direction)
        {
            var parameterString = new StringBuilder();

            var request = HttpContext.Current.Request;

            parameterString.Append("&isrevnavreq=true");

            if (String.IsNullOrEmpty(request.Params["pg"]) == false)
            {
                parameterString.Append("&pg=" +
                    GetLinkPageNumber(
                        Int32.Parse(request.Params["Ordinal"]),
                        Int32.Parse(request.Params["pg"]),
                        direction)
                    );
            }

            if (String.IsNullOrEmpty(request.Params["rp"]) == false)
            {
                parameterString.Append("&rp=" + request.Params["rp"]);
            }

            if (String.IsNullOrEmpty(request.Params["sl"]) == false)
            {
                parameterString.Append("&sl=" + request.Params["sl"]);
            }

            if (String.IsNullOrEmpty(request.Params["op"]) == false)
            {
                parameterString.Append("&op=" + request.Params["op"]);
            }

            return parameterString.ToString();
        }

        private Int32 GetLinkPageNumber(Int32 ordinal, Int32 page, Direction direction)
        {
            switch (direction)
            {
                case Direction.Next:
                    return ordinal % Search.Constants.Paging.DefaultPageSize == 0 ? page + 1 : page;
                case Direction.Previous:
                    return ordinal % Search.Constants.Paging.DefaultPageSize == 1 ? page - 1 : page;
                default:
                    return -1;
            }
        }

        public string MakeNextLinkNoVote(int nextMemberID, int ordinal)
        {
            //	If there is no nextMemberID then there is no next link.
            string retVal = null;
            if (nextMemberID != 0)
            {
                retVal = MakeViewProfileLink(MyEntryPoint, nextMemberID, ordinal + 1, MyMOCollection, HotListCategoryID, true);

                // Anonymous user can view without voting per PM request.
                retVal = "javascript:window.location = '" + retVal + "';";
            }

            return (retVal);
        }

        #region Set Breadcrumbs for Search Results

        private void SetBreadCrumbsForSearchResults(int ordinal, string nextLink)
        {
            int startRow = NormalizeOrdinal(ordinal);
            string memberName = RetrieveMemberName(MemberId, G.Brand);

            string backLink = "/Applications/Search/SearchResults.aspx?StartRow=" + startRow;

            G.BreadCrumbTrailHeader.SetViewProfileCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH"), "/Applications/Search/SearchResults.aspx",
                                                                          G.GetResource("TXT_MATCHES"), backLink,
                memberName,
                DefaultNextProfileText, nextLink,
                                                                          G.GetResource("TXT_BACK_TO_SEARCH_RESULTS"), backLink);
            G.BreadCrumbTrailFooter.SetViewProfileCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH"), "/Applications/Search/SearchResults.aspx",
                                                                          G.GetResource("TXT_MATCHES"), backLink,
                memberName,
                DefaultNextProfileText, nextLink,
                                                                          G.GetResource("TXT_BACK_TO_SEARCH_RESULTS"), backLink);
        }

        private int RetrieveNextMemberIDForSearchResults(int ordinal)
        {
            int retVal;

            SearchPreferenceCollection searchPreferences = G.SearchPreferences;
            MatchnetQueryResults searchResults;

            try
            {
                searchResults = MemberSearchSA.Instance.Search(searchPreferences
                    , G.Brand.Site.Community.CommunityID
                    , G.Brand.Site.SiteID
                    , ordinal	// Ordinal is 1-indexed and this search is 0-indexed, so this grabs the next one
                    , 1);

                ArrayList members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);

                if (members.Count > 0)
                {
                    var nextMember = (Member.ServiceAdapters.Member)members[0];
                    retVal = nextMember.MemberID;
                }
                else
                {
                    //	We're at the end of the list?
                    retVal = 0;
                }
            }
            catch
            {
                retVal = 0;
            }

            return (retVal);
        }

        private int RetrievePreviousMemberIDForSearchResults(int ordinal)
        {
            int retVal;

            SearchPreferenceCollection searchPreferences = G.SearchPreferences;
            MatchnetQueryResults searchResults;

            try
            {
                searchResults = MemberSearchSA.Instance.Search(searchPreferences
                     , G.Brand.Site.Community.CommunityID
                     , G.Brand.Site.SiteID
                     , ordinal - 2	// Ordinal is 1-indexed and this search is 0-indexed, subtract 2 to get previous
                     , 1);

                ArrayList members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);

                if (members.Count > 0)
                {
                    var previousMember = (Member.ServiceAdapters.Member)members[0];
                    retVal = previousMember.MemberID;
                }
                else
                {
                    //	We're at the beginning of the list?
                    retVal = 0;
                }
            }
            catch
            {
                retVal = 0;
            }

            return (retVal);
        }

        private int RetrieveNextMemberIDForPhotoGallery(int ordinal)
        {
            int retVal = 0;

            try
            {
                int totalrows = 0;
                PhotoGalleryQuery query = Applications.PhotoGallery.PhotoGalleryUtils.GetQueryFromSession(G.Brand, G.Member != null ? G.Member.MemberID : int.MinValue);
                if (query != null)
                {
                    int pageSize = G.IsSite20Enabled ? 12 : 8;
                    int page = ordinal / pageSize + 1;

                    ArrayList res = PhotoGallerySA.Instance.Search(query, G.Member == null ? Constants.NULL_INT : G.Member.MemberID,
                        G.Brand.Site.Community.CommunityID, page, pageSize, out totalrows);

                    GetPhotoGalleryResultMembers(res, G.Brand.Site.Community.CommunityID, G.Brand.Site.SiteID, G.Brand.BrandID);
                    int pageordinal = page > 1 ? ordinal - (pageSize * (page - 1)) : ordinal;
                    if (res.Count > 0)
                    {
                        PhotoResultItem nextMember = res[pageordinal] as PhotoResultItem;

                        retVal = nextMember.MemberID;
                    }
                    else
                    {
                        //	We're at the beginning of the list?
                        retVal = 0;
                    }

                    query.ReloadFlag = ReLoadFlags.load;
                    Applications.PhotoGallery.PhotoGalleryUtils.SaveQueryToSession(query, G.Brand, G.Member != null ? G.Member.MemberID : int.MinValue);

                }
            }
            catch
            {
                retVal = 0;
            }

            return (retVal);
        }

        public void GetPhotoGalleryResultMembers(ArrayList resultlist, int communityid, int siteid, int brandid)
        {
            try
            {
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);

                ArrayList memberIDs = new ArrayList();
                if (resultlist == null)
                    return;

                for (int i = 0; i < resultlist.Count; i++)
                {
                    memberIDs.Add(((PhotoResultItem)resultlist[i]).MemberID);
                }

                ArrayList members = MemberSA.Instance.GetMembers(memberIDs, MemberLoadFlags.None);
                if (members == null)
                    return;

                for (int i = 0; i < members.Count; i++)
                {
                    Member.ServiceAdapters.Member member = (Member.ServiceAdapters.Member)members[i];
                    if (member == null)
                        continue;
                    PhotoGalleryMember gmember = new PhotoGalleryMember();
                    gmember.MemberID = member.MemberID;
                    gmember.BirthDate = member.GetAttributeDate(communityid, siteid, brandid, "Birthdate", DateTime.MinValue);
                    Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandid);
                    gmember.UserName = member.GetUserName(brand);
                    var photos = MemberPhotoDisplayManager.Instance.GetAllPhotos(member, communityid);
                    if (photos != null)
                    {
                        gmember.PhotoCount = photos.Count;
                        for (int l = 0; l < photos.Count; l++)
                        {
                            if (photos[l].MemberPhotoID == ((PhotoResultItem)resultlist[i]).MemberPhotoID)
                            {

                                gmember.ThumbWebPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(G.Member, member, G.Brand, photos[l],
                                                               PhotoType.Thumbnail, PrivatePhotoImageType.Thumb,
                                                               NoPhotoImageType.Thumb, false, true);
                                gmember.FileWebPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(G.Member, member, G.Brand, photos[l],
                                                               PhotoType.Thumbnail, PrivatePhotoImageType.Thumb,
                                                               NoPhotoImageType.Thumb, false, true);

                                break;
                            }
                        }
                    }
                    ((PhotoResultItem)resultlist[i]).GalleryMember = gmember;
                }
            }
            catch (Exception ex)
            {
                G.ProcessException(ex);
            }
        }


        private int RetrievePreviousMemberIDForPhotoGallery(int ordinal)
        {
            int retVal = 0;

            try
            {
                int totalrows;
                PhotoGalleryQuery query = null;
                query = Applications.PhotoGallery.PhotoGalleryUtils.GetQueryFromSession(G.Brand, G.Member == null ? Constants.NULL_INT : G.Member.MemberID);
                if (query != null)
                {
                    int pageSize = G.IsSite20Enabled ? 12 : 8;
                    int page = (ordinal - 2) / pageSize + 1;
                    int pageordinal = page > 1 ? ordinal - (pageSize * (page - 1)) : ordinal;
                    var res = PhotoGallerySA.Instance.Search(query, G.Member == null ? Constants.NULL_INT : G.Member.MemberID,
                        G.Brand.Site.Community.CommunityID, page, pageSize, out totalrows);

                    GetPhotoGalleryResultMembers(res, G.Brand.Site.Community.CommunityID, G.Brand.Site.SiteID, G.Brand.BrandID);

                    if (res.Count > 0)
                    {
                        PhotoResultItem previousMember = res[pageordinal - 2] as PhotoResultItem;

                        retVal = previousMember.MemberID;
                    }
                    else
                    {
                        //	We're at the beginning of the list?
                        retVal = 0;
                    }

                    query.ReloadFlag = ReLoadFlags.load;
                    Applications.PhotoGallery.PhotoGalleryUtils.SaveQueryToSession(query, G.Brand, G.Member == null ? Constants.NULL_INT : G.Member.MemberID);
                }
            }
            catch
            {
                retVal = 0;
            }

            return (retVal);
        }

        private int RetrieveNextMemberIDForQuickSearchResults(int ordinal)
        {
            int retVal;

            try
            {
                G.QuickSearchPreferences["DomainID"] = G.TargetCommunityID.ToString();

                if (!PrefsHaveValue(G.QuickSearchPreferences, "SearchTypeID"))
                {
                    G.QuickSearchPreferences["SearchTypeID"] = G.Brand.Site.DefaultSearchTypeID.ToString();
                }

                if (!PrefsHaveValue(G.QuickSearchPreferences, "RegionID"))
                {
                    G.QuickSearchPreferences["RegionID"] = G.Brand.Site.DefaultRegionID.ToString();
                }

                if (!PrefsHaveValue(G.QuickSearchPreferences, "GenderMask"))
                {
                    // set the gender mask to M s M and M s F for all others
                    G.QuickSearchPreferences["GenderMask"] = G.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Glimpse ? (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString() : (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();
                }

                if (!PrefsHaveValue(G.QuickSearchPreferences, "MinAge"))
                {
                    G.QuickSearchPreferences["MinAge"] = G.Brand.DefaultAgeMin.ToString();
                }

                if (!PrefsHaveValue(G.QuickSearchPreferences, "MaxAge"))
                {
                    G.QuickSearchPreferences["MaxAge"] = G.Brand.DefaultAgeMax.ToString();
                }
                if (!PrefsHaveValue(G.QuickSearchPreferences, "Distance"))
                {
                    G.QuickSearchPreferences["Distance"] = G.Brand.DefaultSearchRadius.ToString();
                }

                if (G.QuickSearchPreferences["CountryRegionID"] == string.Empty)
                {
                    G.QuickSearchPreferences.Add("CountryRegionID", G.Brand.Site.DefaultRegionID.ToString());
                }

                MatchnetQueryResults searchResults = MemberSearchSA.Instance.Search(
                    G.QuickSearchPreferences,
                    G.Brand.Site.Community.CommunityID,
                    G.Brand.Site.SiteID,
                    ordinal, // Ordinal is 1-indexed and this search is 0-indexed, so this grabs the next one
                    1
                );

                var members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);

                if (members.Count > 0)
                {
                    var nextMember = (Member.ServiceAdapters.Member)members[0];
                    retVal = nextMember.MemberID;
                }
                else
                {
                    //	We're at the end of the list?
                    retVal = 0;
                }
            }
            catch (Exception ex)
            {
                retVal = 0;
            }

            return (retVal);
        }

        private int RetrievePreviousMemberIDForQuickSearchResults(int ordinal)
        {
            int retVal;

            try
            {
                G.QuickSearchPreferences["DomainID"] = G.TargetCommunityID.ToString();

                if (!PrefsHaveValue(G.QuickSearchPreferences, "SearchTypeID"))
                {
                    G.QuickSearchPreferences["SearchTypeID"] = G.Brand.Site.DefaultSearchTypeID.ToString();
                }

                if (!PrefsHaveValue(G.QuickSearchPreferences, "RegionID"))
                {
                    G.QuickSearchPreferences["RegionID"] = G.Brand.Site.DefaultRegionID.ToString();
                }

                if (!PrefsHaveValue(G.QuickSearchPreferences, "GenderMask"))
                {
                    // set the gender mask to M s M and M s F for all others
                    if (G.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Glimpse)
                    {
                        G.QuickSearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString();
                    }
                    else
                    {
                        G.QuickSearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();
                    }
                }

                if (!PrefsHaveValue(G.QuickSearchPreferences, "MinAge"))
                {
                    G.QuickSearchPreferences["MinAge"] = G.Brand.DefaultAgeMin.ToString();
                }

                if (!PrefsHaveValue(G.QuickSearchPreferences, "MaxAge"))
                {
                    G.QuickSearchPreferences["MaxAge"] = G.Brand.DefaultAgeMax.ToString();
                }
                if (!PrefsHaveValue(G.QuickSearchPreferences, "Distance"))
                {
                    G.QuickSearchPreferences["Distance"] = G.Brand.DefaultSearchRadius.ToString();
                }

                if (G.QuickSearchPreferences["CountryRegionID"] == string.Empty)
                {
                    G.QuickSearchPreferences.Add("CountryRegionID", G.Brand.Site.DefaultRegionID.ToString());
                }

                var searchResults = MemberSearchSA.Instance.Search(
                    G.QuickSearchPreferences,
                    G.Brand.Site.Community.CommunityID,
                    G.Brand.Site.SiteID,
                    ordinal - 2, // Ordinal is 1-indexed and this search is 0-indexed
                    1
                );

                var members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);

                if (members.Count > 0)
                {
                    var nextMember = (Member.ServiceAdapters.Member)members[0];
                    retVal = nextMember.MemberID;
                }
                else
                {
                    //	We're at the end of the list?
                    retVal = 0;
                }
            }
            catch (Exception ex)
            {
                retVal = 0;
            }

            return (retVal);
        }

        private Int32 GetPageNumber()
        {
            Int32 pageFromParameter;

            if (Int32.TryParse(HttpContext.Current.Request.Params[
                    Search.Constants.ReverseSearch.Parameter.Page],
                    out pageFromParameter))
            {
                return pageFromParameter < 1 ? Search.Constants.Paging.DefaultPage : pageFromParameter;
            }
            return Search.Constants.Paging.DefaultPage;
        }

        protected Boolean OnlyPhotos
        {
            get
            {
                return String.IsNullOrEmpty(SearchPageHelper.GetParamValue(
                    Search.Constants.ReverseSearch.Parameter.OnlyPhotos,
                    HttpContext.Current.Request)) == false;
            }
        }

        private int RetrieveNextMemberIDForReverseSearchResults(Int32 ordinal)
        {
            int retVal;

            //this method will fail if there's no valid logge din member, and reverse search should be a member only feature anyway. 
            if (G.Member == null) return 0;

            SearchPreferenceCollection searchPreferences = ResultListHandler.CreateReverseSearchPreferences(G, OnlyPhotos);
            MatchnetQueryResults searchResults = null;

            try
            {
                try
                {
                    searchResults = MemberSearchSA.Instance.Search(searchPreferences
                                                                   , G.Brand.Site.Community.CommunityID
                                                                   , G.Brand.Site.SiteID
                                                                   , ordinal // Ordinal is 1-indexed and this search is 0-indexed, so this grabs the next one
                                                                   , 1);
                }
                catch (Exception fe)
                {
                    G.ProcessException(fe);
                }

                if (null == searchResults ||
                    searchResults.MatchesFound < ResultListHandler.GetReverseSearchMinimumResults(G))
                {
                    try
                    {
                        ResultListHandler.ConvertToLaxReverseSearchPreferences(searchPreferences);

                        searchResults = MemberSearchSA.Instance.Search(searchPreferences
                                                                       , G.Brand.Site.Community.CommunityID
                                                                       , G.Brand.Site.SiteID
                                                                       , ordinal // Ordinal is 1-indexed and this search is 0-indexed, so this grabs the next one
                                                                       , 1);
                    }
                    catch (Exception se)
                    {
                        G.ProcessException(se);
                    }
                }

                ArrayList members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);

                if (members.Count > 0)
                {
                    var nextMember = (Member.ServiceAdapters.Member)members[0];
                    retVal = nextMember.MemberID;
                }
                else
                {
                    //	We're at the end of the list?
                    retVal = 0;
                }
            }
            catch
            {
                retVal = 0;
            }

            return (retVal);

        }

        private int RetrieveNextMemberIDForKeywordSearchResults(int ordinal)
        {
            return ordinal;
        }

        private int RetrievePreviousMemberIDForReverseSearchResults(int ordinal)
        {
            return ordinal;
        }

        private int RetrievePreviousMemberIDForKeywordSearchResults(int ordinal)
        {
            return ordinal;
        }

        #endregion

        #region Set Breadcrumbs for Hot Lists

        private void SetBreadCrumbsForHotLists(int ordinal, string nextLink)
        {
            int startRow = NormalizeOrdinal(ordinal);
            string memberName = RetrieveMemberName(MemberId, G.Brand);
            string categoryID = HotListCategoryID.ToString();
            string categoryName = RetrieveHotListCategoryName(Conversion.CInt(categoryID));

            string backLink = "/Applications/HotList/View.aspx?StartRow=" + startRow + "&CategoryID=" + categoryID;

            G.BreadCrumbTrailHeader.SetViewProfileCrumbs(G.GetResource("TXT_HOT_LIST"), "/Applications/HotList/View.aspx",
                categoryName, backLink,
                memberName,
                DefaultNextProfileText, nextLink,
                                                                          G.GetResource("TXT_BACK_TO") + " " + categoryName, backLink);

            G.BreadCrumbTrailFooter.SetViewProfileCrumbs(G.GetResource("TXT_HOT_LIST"), "/Applications/HotList/View.aspx",
                categoryName, backLink,
                memberName,
                DefaultNextProfileText, nextLink,
                                                                          G.GetResource("TXT_BACK_TO") + " " + categoryName, backLink);
        }

        private int RetrieveNextMemberIDForHotList(int ordinal)
        {
            int retVal;
            int rowCount;

            try
            {
                HotListCategory category = (HotListCategory)HotListCategoryID;

                var targetMemberIDs = G.List.GetListMembers(category,
                                                G.Brand.Site.Community.CommunityID,
                                                G.Brand.Site.SiteID,
                                                ordinal + 1,
                                                1,
                                                out rowCount);

                if (targetMemberIDs.Count > 0)
                {
                    retVal = (int)targetMemberIDs[0];
                }
                else
                {
                    retVal = 0;
                }
            }
            catch (Exception ex)
            {
                retVal = 0;
            }

            return retVal;
        }

        private int RetrievePreviousMemberIDForHotList(int ordinal)
        {
            int retVal;

            try
            {
                var category = (HotListCategory)HotListCategoryID;

                int rowCount;
                var targetMemberIDs = G.List.GetListMembers(
                    category,
                    G.Brand.Site.Community.CommunityID,
                    G.Brand.Site.SiteID,
                    ordinal - 1,
                    1,
                    out rowCount
                );

                if (targetMemberIDs.Count > 0)
                {
                    retVal = (int)targetMemberIDs[0];
                }
                else
                {
                    retVal = 0;
                }
            }
            catch (Exception)
            {
                retVal = 0;
            }

            return retVal;
        }

        private string RetrieveHotListCategoryName(int categoryID)
        {
            string retVal = String.Empty;

            if (categoryID > 0)
            {
                var categories =
                    List.ServiceAdapters.ListSA.Instance.GetCustomListCategories(
                        G.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID)
                        , G.Brand.Site.Community.CommunityID);

                foreach (CustomCategory thisCategory in categories)
                {
                    if (thisCategory.CategoryID == categoryID)
                    {
                        retVal = thisCategory.Description;
                    }
                }
            }
            else
            {
                if (categoryID == (int)HotListCategory.MutualYes)
                {
                    retVal = G.GetResource("TXT_YOU_EACH_SAID_YES");
                }
                else
                {
                    retVal = ListLib.GetStandardCategoryName((HotListCategory)categoryID, G);
                }
            }

            return (retVal);
        }

        #endregion

        #region Set Breadcrumbs for Members Online

        private void SetBreadCrumbsForMembersOnline(int ordinal, string nextLink)
        {
            int startRow = NormalizeOrdinal(ordinal);
            string memberName = RetrieveMemberName(MemberId, G.Brand);

            string backLink = "/Applications/MembersOnline/MembersOnline.aspx?StartRow=" + startRow;

            G.BreadCrumbTrailHeader.SetViewProfileCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH"), "/Applications/Search/SearchResults.aspx",
                                                                          G.GetResource("TXT_ONLINE_NOW"), backLink,
                memberName,
                DefaultNextProfileText, nextLink,
                                                                          G.GetResource("TXT_BACK_TO_ONLINE_NOW"), backLink);

            G.BreadCrumbTrailFooter.SetViewProfileCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH"), "/Applications/Search/SearchResults.aspx",
                                                                          G.GetResource("TXT_ONLINE_NOW"), backLink,
                memberName,
                DefaultNextProfileText, nextLink,
                                                                          G.GetResource("TXT_BACK_TO_ONLINE_NOW"), backLink);
        }

        private int RetrieveNextMemberIDForMembersOnline(int ordinal)
        {
            int retVal = 0;
            Int16 genderMask = 0;
            SortFieldType sortField = MyMOCollection.OrderBy;
            SortDirectionType sortDirection;

            if (MyMOCollection.GenderMask > 0)
            {
                genderMask = (Int16)MyMOCollection.GenderMask;
            }

            switch (sortField)
            {
                case SortFieldType.Age:
                case SortFieldType.HasPhoto:
                case SortFieldType.InsertDate:
                    sortDirection = SortDirectionType.Desc;
                    break;
                default:
                    sortDirection = SortDirectionType.Asc;
                    break;
            }

            var memberIDs = MembersOnlineSA.Instance.GetMemberIDs(G.Session.Key.ToString(),
                G.Brand.Site.Community.CommunityID,
                genderMask,
                MyMOCollection.RegionID,
                (Int16)MyMOCollection.AgeMin,
                (Int16)MyMOCollection.AgeMax,
                MyMOCollection.LanguageMask,
                sortField,
                sortDirection,
                ordinal,
                12,
                G.Member == null ? Constants.NULL_INT : G.Member.MemberID).ToArrayList();


            ArrayList members = MemberSA.Instance.GetMembers(memberIDs,
                    MemberLoadFlags.None);

            if (members.Count > 0)
            {
                var nextMember = (Member.ServiceAdapters.Member)members[0];

                retVal = nextMember.MemberID;
                if (retVal == MemberId)
                {
                    //	We're at the end of the list
                    retVal = 0;
                }
            }
            else
            {
                //	We're at the end of the list
                retVal = 0;
            }

            return retVal;
        }

        private int RetrievePreviousMemberIDForMembersOnline(int ordinal)
        {
            int retVal = 0;
            Int16 genderMask = 0;
            SortFieldType sortField = MyMOCollection.OrderBy;

            try
            {
                if (MyMOCollection.GenderMask > 0)
                {
                    genderMask = (Int16)MyMOCollection.GenderMask;
                }

                SortDirectionType sortDirection;
                switch (sortField)
                {
                    case SortFieldType.Age:
                    case SortFieldType.HasPhoto:
                    case SortFieldType.InsertDate:
                        sortDirection = SortDirectionType.Desc;
                        break;
                    default:
                        sortDirection = SortDirectionType.Asc;
                        break;
                }

                var memberIDs = MembersOnlineSA.Instance.GetMemberIDs(G.Session.Key.ToString(),
                     G.Brand.Site.Community.CommunityID,
                     genderMask,
                     MyMOCollection.RegionID,
                     (Int16)MyMOCollection.AgeMin,
                     (Int16)MyMOCollection.AgeMax,
                     MyMOCollection.LanguageMask,
                     sortField,
                     sortDirection,
                     ordinal - 2,
                     12,
                     G.Member == null ? Constants.NULL_INT : G.Member.MemberID).ToArrayList();


                ArrayList members = MemberSA.Instance.GetMembers(memberIDs,
                          MemberLoadFlags.None);

                if (members.Count > 0)
                {
                    var prevMember = (Member.ServiceAdapters.Member)members[0];

                    retVal = prevMember.MemberID;
                    if (retVal == MemberId)
                    {
                        //	We're at the beginning of the list
                        retVal = 0;
                    }
                }
                else
                {
                    //	We're at the beginning of the list
                    retVal = 0;
                }
            }
            catch (Exception ex)
            {
                retVal = 0;
            }

            return retVal;
        }

        #endregion

        #region Set Breadcrumbs for Home Page

        private void SetBreadCrumbsForHomePage(int ordinal, string nextLink)
        {
            int startRow = NormalizeOrdinal(ordinal);
            string memberName = RetrieveMemberName(MemberId, G.Brand);

            G.BreadCrumbTrailHeader.SetViewProfileCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH"), "/Applications/Search/SearchResults.aspx",
                                                                          G.GetResource("TXT_MATCHES"), "/Applications/Search/SearchResults.aspx?StartRow=" + startRow,
                memberName,
                DefaultNextProfileText, nextLink,
                                                                          G.GetResource("TXT_BACK_TO_HOME"), "/Applicatoins/Home/Default.aspx");

            G.BreadCrumbTrailFooter.SetViewProfileCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH"), "/Applications/Search/SearchResults.aspx",
                                                                          G.GetResource("TXT_MATCHES"), "/Applications/Search/SearchResults.aspx?StartRow=" + startRow,
                memberName,
                DefaultNextProfileText, nextLink,
                                                                          G.GetResource("TXT_BACK_TO_HOME"), "/Applications/Home/Default.aspx");
        }

        private int RetrieveNextMemberIDForHomePage(int ordinal)
        {
            //TL 11052008 NOTE: This will not give a consistent next memberID as displayed on homepage because the homepage
            //randomizes the results (if greater than 7 results).  But it will provide one of the memberID as shown on the homepage.

            SearchPreferenceCollection searchPreferences = G.SearchPreferences;
            MatchnetQueryResults searchResults = null;

            //get current photo preference before we modify it
            const string HASPHOTOFLAG = "HasPhotoFlag";
            string photoPreference = searchPreferences.Value(HASPHOTOFLAG);
            searchPreferences.Add(HASPHOTOFLAG, "1");

            searchResults = MemberSearchSA.Instance.Search(searchPreferences
                , G.Brand.Site.Community.CommunityID
                , G.Brand.Site.SiteID
                , ordinal	// Ordinal is 1-indexed and this search is 0-indexed, so this grabs the next one
                , 1);

            //set photo preference back to old value
            searchPreferences.Add(HASPHOTOFLAG, photoPreference);

            var members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(),
                MemberLoadFlags.None);

            return members.Count > 0 ? ((Member.ServiceAdapters.Member)members[0]).MemberID : 0;
        }

        private int RetrievePreviousMemberIDForHomePage(int ordinal)
        {
            int retVal;

            SearchPreferenceCollection searchPreferences = G.SearchPreferences;
            MatchnetQueryResults searchResults = null;

            try
            {
                //get current photo preference before we modify it
                const string HASPHOTOFLAG = "HasPhotoFlag";
                string photoPreference = searchPreferences.Value(HASPHOTOFLAG);
                searchPreferences.Add(HASPHOTOFLAG, "1");

                searchResults = MemberSearchSA.Instance.Search(searchPreferences
                     , G.Brand.Site.Community.CommunityID
                     , G.Brand.Site.SiteID
                     , ordinal - 2	// Ordinal is 1-indexed and this search is 0-indexed
                     , 1);

                //set photo preference back to old value
                searchPreferences.Add(HASPHOTOFLAG, photoPreference);

                ArrayList members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(),
                     MemberLoadFlags.None);

                retVal = members.Count > 0 ? ((Member.ServiceAdapters.Member)members[0]).MemberID : 0;
            }
            catch (Exception ex)
            {
                retVal = 0;
            }

            return (retVal);
        }

        #endregion

        #region Set Breadcrumbs for Thumb Trail

        private void SetBreadCrumbsForThumbTrail(int ordinal, string nextLink)
        {
            int startRow = NormalizeOrdinal(ordinal);
            string memberName = RetrieveMemberName(MemberId, G.Brand);

            G.BreadCrumbTrailHeader.SetViewProfileCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH"), "/Applications/Search/SearchResults.aspx",
                G.GetResource("TXT_MATCHES"), "/Applications/Search/SearchResults.aspx?StartRow=" + startRow,
                memberName,
                DefaultNextProfileText, nextLink,
                G.GetResource("TXT_BACK"), JavascriptBackLink);

            G.BreadCrumbTrailFooter.SetViewProfileCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH"), "/Applications/Search/SearchResults.aspx",
                G.GetResource("TXT_MATCHES"), "/Applications/Search/SearchResults.aspx?StartRow=" + startRow,
                memberName,
                DefaultNextProfileText, nextLink,
                G.GetResource("TXT_BACK"), JavascriptBackLink);
        }

        private int RetrieveNextMemberIDForThumbTrail(int ordinal)
        {
            //	TOFIX - Implement me!

            return Constants.NULL_INT;
        }

        private int RetrievePreviousMemberIDForThumbTrail(int ordinal)
        {
            int retVal;

            //	TOFIX - Implement me!
            retVal = Constants.NULL_INT;

            return (retVal);
        }

        public bool PrefsHaveValue(SearchPreferenceCollection prefs, string key)
        {
            bool retVal = false;
            string myValue = prefs.Value(key);
            if (!string.IsNullOrEmpty(myValue) && Conversion.CInt(myValue) != Constants.NULL_INT)
            {
                retVal = true;
            }

            return (retVal);
        }

        #endregion

        #region Set Breadcrumbs for Email/Messages

        private void SetBreadCrumbsForMessages()
        {
            string memberName = RetrieveMemberName(MemberId, G.Brand);
            G.BreadCrumbTrailHeader.SetThreeLinkCrumb(G.GetResource("NAV_MESSAGES", this)
                , "/Applications/Email/MailBox.aspx"
                , memberName
                , string.Empty);

            G.BreadCrumbTrailFooter.SetThreeLinkCrumb(G.GetResource("NAV_MESSAGES", this)
                , "/Applications/Email/MailBox.aspx"
                , memberName
                , string.Empty);
        }

        #endregion

        #region Set Breadcrumbs for Look Up Member

        private void SetBreadCrumbsForLookUpMember()
        {
            string memberName = RetrieveMemberName(MemberId, G.Brand);

            G.BreadCrumbTrailHeader.SetViewProfileCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH")
                , "/Applications/Search/SearchResults.aspx"
                , G.GetResource("NAV_SUB_LOOK_UP_PROFILE")
                , "/Applications/LookupProfile/LookupProfile.aspx"
                , memberName
                , null
                , null
                , null
                , null);

            G.BreadCrumbTrailFooter.SetViewProfileCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH")
                , "/Applications/Search/SearchResults.aspx"
                , G.GetResource("NAV_SUB_LOOK_UP_PROFILE")
                , "/Applications/LookupProfile/LookupProfile.aspx"
                , memberName
                , null
                , null
                , null
                , null);

        }
        #endregion

        #region Set Breadcrumbs for AstroScope
        private void SetBreadCrumbForAstroScope()
        {
            string memberName = RetrieveMemberName(MemberId, G.Brand);

            G.BreadCrumbTrailHeader.SetAstroScopeCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH")
                , "/Applications/Search/SearchResults.aspx"
                , G.GetResource("NAV_SUB_LOOK_UP_PROFILE")
                , "/Applications/LookupProfile/LookupProfile.aspx"
                , memberName
                , "/Applications/MemberProfile/ViewProfile.aspx?MemberID=" + MemberId
                , G.GetResource("NAV_ASTROSCOPE")
                , null
                , null
                , null
                , null);

            G.BreadCrumbTrailFooter.SetAstroScopeCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH")
                , "/Applications/Search/SearchResults.aspx"
                , G.GetResource("NAV_SUB_LOOK_UP_PROFILE")
                , "/Applications/LookupProfile/LookupProfile.aspx"
                , memberName
                , "/Applications/MemberProfile/ViewProfile.aspx?MemberID=" + MemberId
                , G.GetResource("NAV_ASTROSCOPE")
                , null
                , null
                , null
                , null);
        }
        #endregion

        private void SetBreadCrumbsForNoArgs()
        {
            string memberName = RetrieveMemberName(MemberId, G.Brand);

            G.BreadCrumbTrailHeader.SetViewProfileCrumbs(
                G.GetResource("NAV_YOUR_PROFILE"), "/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=" +
                ((int)EntryPoint.NoArgs),
                "[Edit Your Profile]", null,
                memberName,
                null, null,
                G.GetResource("TXT_BACK"), JavascriptBackLink);

            G.BreadCrumbTrailFooter.SetViewProfileCrumbs(
                G.GetResource("NAV_YOUR_PROFILE"), "/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=" +
                ((int)EntryPoint.NoArgs),
                "[Edit Your Profile]", null,
                memberName,
                null, null,
                 G.GetResource("TXT_BACK"), JavascriptBackLink);
        }

        #region Set Breadcrumbs in case of an error or undefined state

        private void SetBreadCrumbsForError(int ordinal)
        {
            string memberName = RetrieveMemberName(MemberId, G.Brand);

            if (G.Member != null && MemberId.Equals(G.Member.MemberID))
            {
                G.BreadCrumbTrailHeader.SetTwoLinkCrumb(G.GetResource("NAV_YOUR_PROFILE", this), string.Empty);
                G.BreadCrumbTrailFooter.SetTwoLinkCrumb(G.GetResource("NAV_YOUR_PROFILE", this), string.Empty);
            }
            else
            {
                G.BreadCrumbTrailHeader.SetViewProfileCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH"),
                    "/Applications/Search/SearchResults.aspx",
                    null, null,
                    memberName,
                    null, null,
                    G.GetResource("TXT_BACK"), JavascriptBackLink);

                G.BreadCrumbTrailFooter.SetViewProfileCrumbs(G.GetResource("TXT_SEARCH_AND_MATCH"),
                    "/Applications/Search/SearchResults.aspx",
                    null, null,
                    memberName,
                    null, null,
                    G.GetResource("TXT_BACK"), JavascriptBackLink);
            }
        }

        #endregion

        #region Common utility methods shared across Bread Crumb-setters

        private static int NormalizeOrdinal(int ordinal)
        {
            int retVal;
            if (ordinal <= 1)
            {
                retVal = 1;
            }
            else
            {
                if (ordinal % 15 == 0)
                {
                    ordinal = ordinal - 1;
                    retVal = ordinal - ((ordinal % 15) - 1);
                }
                else
                {
                    retVal = ordinal - ((ordinal % 15) - 1);
                }
            }

            return (retVal);
        }

        private static String RetrieveMemberName(Int32 memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            return MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None).GetUserName(brand);
        }

        #endregion
    }
}
