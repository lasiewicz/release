﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public  interface IMiniProfile
    {
         Matchnet.Web.Framework.SearchResultProfile SearchResult{get;}
         FrameworkControl ThisControl { get; }
         IMemberDTO Member { get; set; }
         #region controls
         Literal TXTGender { get; }
         Literal TXTAge { get; }
         Literal TXTRegion { get; }
         Literal TXTHeadline { get; }
         HyperLink LNKUserName{get;}
         Matchnet.Web.Framework.Image IMGIsNew{get;}
         Matchnet.Web.Framework.Image IMGIsUpdate{get;}
         Matchnet.Web.Framework.Image IMGThumb { get; }
         System.Web.UI.WebControls.Literal LITContainer{get;}
         Matchnet.Web.Framework.Image IMGPremiumAuthenticated{get;}
         Matchnet.Web.Framework.Ui.BasicElements.HighlightProfileInfoDisplay UCHighlightProfileInfoDisplay { get; }
         LastLoginDate LastLoginDate { get; }
         Panel  PNLComments{get;}
         PlaceHolder  PLCBreaks{get;}
         HtmlInputText TXTFavoriteNote{get;}
         FormElements.FrameworkButton  BTNSave{get;}
         LinkButton LBRemoveProfile { get; }
         Matchnet.Web.Framework.Txt TXTMore { get; }
         Matchnet.Web.Framework.Image IMGChat { get; }
         System.Web.UI.WebControls.HyperLink LNKOnline { get; }
         Matchnet.Web.Framework.Txt TXTOnline { get; }
         Matchnet.Web.Framework.Image IMGYes { get; }
         Matchnet.Web.Framework.Image IMGMaybe { get; }
         Matchnet.Web.Framework.Image IMGNo { get; }
         Matchnet.Web.Framework.Image IMGBothSaidYes { get; }
         Matchnet.Web.Framework.Txt TXTYes { get; }
         Matchnet.Web.Framework.Txt TXTMaybe { get; }
         Matchnet.Web.Framework.Txt TXTNo { get; }
         HtmlInputHidden YNMVOTEStatus { get; }
         HtmlTableCell YNMBackgroundAnimation { get; }


 
         #endregion


         #region Properties

         Matchnet.Web.Framework.Ui.BasicElements.ResultContextType DisplayContext { get; set; }
         string DefaultNote { get;  }
         bool IsHotListFriend { get; set; }
          bool Transparency { get; set; }
          string CategoryID { get; set; }
          int Counter { get; set; }
          int MemberID { get; set; }
          int Ordinal { get; set; }
          BreadCrumbHelper.EntryPoint MyEntryPoint { get; set; }
          MOCollection MyMOCollection { get; set; }
          HotListCategory HotListCategory { get; set; }
          bool EnableSingleSelect { get; set; }
          string Note { get; }
          bool CurrentlyOnline { get; set; }
          bool IsHighlighted { get; set; }
          bool IsSpotlight { get; set; }
          bool OverrideSelfSpotlight { get; set; }
          bool IsPromotionalMember { get; set; }
          bool IsMatchMeterEnabled { get; set; }
          string ViewProfileURL { get; set; }
          string YesFileNameSelected{ get; set; }
          string NoFileNameSelected{ get; set; }
          string MaybeFileNameSelected { get; set; }
        

         #endregion
    }
}
