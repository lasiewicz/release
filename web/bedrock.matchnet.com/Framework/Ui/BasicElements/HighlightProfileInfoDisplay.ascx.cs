using System;   
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Content.ServiceAdapters.Links;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
	/// <summary>
	///		Summary description for HighlightProfileInfoDisplay.
	/// </summary>
	public class HighlightProfileInfoDisplay : FrameworkControl
	{
		protected System.Web.UI.WebControls.HyperLink lnkHighlightProfileInfo;
		protected System.Web.UI.WebControls.Panel pnlHighlightProfileInfoForMembersWithSubscription;
		protected System.Web.UI.WebControls.Panel pnlHighlightProfileInfoForMembersWithoutSubscription;
		protected System.Web.UI.WebControls.HyperLink lnkHighlightProfileInfoSubscribe;

		protected System.Web.UI.WebControls.HyperLink lnkHighlightProfileInfoForMembersWithSubscriptionCloseText;
		protected System.Web.UI.WebControls.HyperLink lnkHighlightProfileInfoForMembersWithSubscriptionCloseImage;
		protected System.Web.UI.WebControls.HyperLink lnkHighlightProfileInfoForMembersWithoutSubscriptionCloseText;
		protected System.Web.UI.WebControls.HyperLink lnkHighlightProfileInfoForMembersWithoutSubscriptionCloseImage;
        protected Txt imgHighlightProfileInfoForMembersWithoutSubscription;
		private bool _memberHighlighted = false;
		private int _viewedMemberID = Constants.NULL_INT;
		private bool _showImageNoText = false;

        //private FrameworkControl _thisControl = null;

        //public FrameworkControl ThisControl
        //{
        //    get { return _thisControl == null? this:_thisControl; }
        //    set { _thisControl = value; }

       // }

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (this._memberHighlighted)     
			{				
				// Show Learn about Highlighted Profiles only for profiles that are highlighted
				System.Text.StringBuilder strShowPopup = new System.Text.StringBuilder();
                bool showDifferentInfoDisplayToSubscriber = false;
                if (MemberPrivilegeAttr.IsCureentSubscribedMember(g) && showDifferentInfoDisplayToSubscriber)
				{
					// Viewing member that is actively subscribed see feature information
					// but cannot upgrade to get highlighted profile
					// Renders html and does not take up space
					//strShowPopup.Append("document.getElementById('" + pnlHighlightProfileInfoForMembersWithSubscription.ClientID + "').style.display = '';");
					strShowPopup.Append("javascript: document.getElementById('" + pnlHighlightProfileInfoForMembersWithoutSubscription.ClientID + "').style.display = 'none';");
					strShowPopup.Append("TogglePopupDivDisplay('" + pnlHighlightProfileInfoForMembersWithSubscription.ClientID + "');");
					// Renders html and also takes up space
					//strShowPopup.Append("document.getElementById('" + pnlHighlightProfileInfoForMembersWithSubscription.ClientID + "').style.visibility = 'visible';");
					//strShowPopup.Append("document.getElementById('" + pnlHighlightProfileInfoForMembersWithoutSubscription.ClientID + "').style.visibility = 'hidden';");

					lnkHighlightProfileInfoForMembersWithSubscriptionCloseText.Text = g.GetResource("TXT_CLOSE", this);
                    lnkHighlightProfileInfoForMembersWithSubscriptionCloseImage.Text = g.GetResource("IMAGE_CLOSE", null, true, this);					

					lnkHighlightProfileInfoForMembersWithSubscriptionCloseText.Attributes.Add("href", strShowPopup.ToString());
					lnkHighlightProfileInfoForMembersWithSubscriptionCloseImage.Attributes.Add("href", strShowPopup.ToString());
				}
				else
				{
					// Need to set to PurchaseReasonType.ViewedHighlightProfileInfo
					lnkHighlightProfileInfoSubscribe.NavigateUrl = LinkFactory.Instance.GetLink("/Applications/Subscription/Subscribe.aspx?prtid=" + ((int) PurchaseReasonType.ViewedHighlightProfileInfo).ToString(), g.Brand, System.Web.HttpContext.Current.Request.Url.ToString()) + "&srid=" + this._viewedMemberID.ToString();
					//lnkHighlightProfileInfoSubscribe.Text = g.GetResource("HIGHLIGHTPROFILEINFO_TO_SUBSCRIPTION", this);		
			        string resKey="HIGHLIGHTPROFILEINFO_TO_SUBSCRIPTION";
                    if(g.LayoutVersion== WebConstants.LayoutVersions.versionWide)
                        resKey=resKey + "_20";

                    imgHighlightProfileInfoForMembersWithoutSubscription.ResourceConstant = resKey;
					// Viewing member that is not actively subscribed will see feature
					// explanation and link to upgrade to get highlighted profile
					strShowPopup.Append("javascript: document.getElementById('" + pnlHighlightProfileInfoForMembersWithSubscription.ClientID + "').style.display = 'none';");
					strShowPopup.Append("TogglePopupDivDisplay('" + pnlHighlightProfileInfoForMembersWithoutSubscription.ClientID + "');");
					//strShowPopup.Append("document.getElementById('" + pnlHighlightProfileInfoForMembersWithoutSubscription.ClientID + "').style.visibility = 'visible';");
					//strShowPopup.Append("document.getElementById('" + pnlHighlightProfileInfoForMembersWithSubscription.ClientID + "').style.visibility = 'hidden';");

					lnkHighlightProfileInfoForMembersWithoutSubscriptionCloseText.Text = g.GetResource("TXT_CLOSE", this);
                    lnkHighlightProfileInfoForMembersWithoutSubscriptionCloseImage.Text = g.GetResource("IMAGE_CLOSE", null, true, this);					

					lnkHighlightProfileInfoForMembersWithoutSubscriptionCloseText.Attributes.Add("href", strShowPopup.ToString());
					lnkHighlightProfileInfoForMembersWithoutSubscriptionCloseImage.Attributes.Add("href", strShowPopup.ToString());
				}
				lnkHighlightProfileInfo.Attributes.Add("href", strShowPopup.ToString());
				//lnkHighlightProfileInfo.Attributes.Add("onmouseover", strShowPopup.ToString());

				if (this._showImageNoText)
				{
					// Show image without text
                    string resourceKey = "IMAGE_LEARN_ABOUT_HIGHLIGHTED_PROFILES";
                    if (g.LayoutVersion == WebConstants.LayoutVersions.versionWide)
                        resourceKey = resourceKey + "_20";

					lnkHighlightProfileInfo.Text = g.GetResource(resourceKey, this);					
				}
				else
				{
					// Show the text without image
                    lnkHighlightProfileInfo.Text = g.GetResource("TXT_LEARN_ABOUT_HIGHLIGHTED_PROFILES", null, true, this);					
				}
				lnkHighlightProfileInfo.Visible = true;
				pnlHighlightProfileInfoForMembersWithSubscription.Style.Add("display", "none");
				pnlHighlightProfileInfoForMembersWithSubscription.Style.Remove("visibility");
				pnlHighlightProfileInfoForMembersWithoutSubscription.Style.Add("display", "none");
				pnlHighlightProfileInfoForMembersWithoutSubscription.Style.Remove("visibility");
				//				pnlHighlightProfileInfoForMembersWithSubscription.Style.Add("visibility", "hidden");
				//				pnlHighlightProfileInfoForMembersWithoutSubscription.Style.Add("visibility", "hidden");
			}
			else
			{
				lnkHighlightProfileInfo.Visible = false;
				// Do not render html
				pnlHighlightProfileInfoForMembersWithSubscription.Visible = false;
				pnlHighlightProfileInfoForMembersWithoutSubscription.Visible = false;
			}
		}

		public bool MemberHighlighted
		{
			set 
			{
				this._memberHighlighted = value;
			}
		}

		public int ViewedMemberID
		{
			set 
			{
				this._viewedMemberID = value;
			}
		}

		public bool ShowImageNoText
		{
			set 
			{
				this._showImageNoText = value;
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
