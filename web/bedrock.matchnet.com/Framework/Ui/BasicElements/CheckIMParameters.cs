#region System References
using System;
#endregion

#region Matchnet Web App References
using Matchnet.Lib;
using Matchnet.Lib.Util;
using Matchnet.Security;
#endregion

namespace Matchnet.Web.Framework.Ui.BasicElements
{
	public class CheckIMParameters 
	{
		private const string ENCRYPT_KEY = "fb50e5f3";
		private string _sessionKey;
		private int _memberID;
		private int _privateLabelID;
		private int _domainID;
		private int _version;
		private int _keepAliveCounter;
			
		public void DecodeParams(string encodedParams) 
		{
			string parameters = Crypto.Decrypt(ENCRYPT_KEY, encodedParams);
			string[] tokens = parameters.Split(new char[] { ',' });

			if (tokens.Length == 7) 
			{
				try 
				{
					_sessionKey = Matchnet.Lib.Util.Util.CString(tokens[0], "");
					_memberID = Matchnet.Conversion.CInt(tokens[1], Constants.NULL_INT);
					_privateLabelID = Matchnet.Conversion.CInt(tokens[2], Constants.NULL_INT);
					_domainID = Matchnet.Conversion.CInt(tokens[3], Constants.NULL_INT);
					_version = Matchnet.Conversion.CInt(tokens[4], Constants.NULL_INT);
					_keepAliveCounter = Matchnet.Conversion.CInt(tokens[5], 0);
				} 
				catch { /* ignore */ }
			} // else, we have an invalid CheckIM request
		}

		public string EncodeParams() 
		{
			string parameters = _sessionKey + "," +
				_memberID + "," +
				_privateLabelID + "," +
				_domainID + "," +
				_version + "," +
				_keepAliveCounter + "," +
				DateTime.Now.ToString();

			return Crypto.Encrypt(ENCRYPT_KEY, parameters);
		}
			
		public string SessionKey 
		{
			get { return _sessionKey; }
			set { _sessionKey = value; }
		}

		public int MemberID 
		{
			get { return _memberID; }
			set { _memberID = value; }
		}

		public int PrivateLabelID 
		{
			get { return _privateLabelID; }
			set { _privateLabelID = value; }
		}

		public int DomainID 
		{
			get { return _domainID; }
			set { _domainID = value; }
		}

		public int Version 
		{
			get { return _version; }
			set { _version = value; }
		}

		public int KeepAliveCounter 
		{
			get { return _keepAliveCounter; }
			set { _keepAliveCounter = value; }
		}
	}
}