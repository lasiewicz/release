﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FilmStripMicroProfile.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.FilmStripMicroProfile" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>

<%--photo--%>
<div class="picture">
    <mn:Image runat="server" ID="imgThumb" class="profileImageHover" ResourceConstant="ALT_PROFILE_PICTURE" TitleResourceConstant="TTL_VIEW_MY_PROFILE" />
    <uc1:NoPhoto runat="server" ID="noPhoto" class="no-photo" Visible="False" />
    <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
        <div class="cc-pic-tag cc-pic-tag-sm cc-pic-<%=_ColorText.ToLower() %>">
            <span><%=_ColorText %></span>
        </div>
    </asp:PlaceHolder>
</div>
<%--username--%>
<h2><asp:HyperLink runat="server" ID="lnkUserName1" /></h2>
<%--extended profile info--%>
<asp:PlaceHolder ID="phExtendedDetail" runat="server" Visible="false">
    <asp:Literal ID="literalAge" runat="server"></asp:Literal>&nbsp;
    <br />
    <asp:Literal ID="literalLocation" runat="server"></asp:Literal>
</asp:PlaceHolder>
