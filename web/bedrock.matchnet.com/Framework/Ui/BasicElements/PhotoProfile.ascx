﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhotoProfile.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.PhotoProfile" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="HighlightProfileInfoDisplay" Src="../../../Framework/Ui/BasicElements/HighlightInfoDisplay20.ascx" %>

<div class="photo-profile" runat="server">
    <div class="photo-profile-inner">
        <div class="picture">
        <mn:Image runat="server" id="imgThumb" border="0" ResourceConstant="" />
        </div>
        <h2><asp:HyperLink id="lnkUserName" runat="server" /></h2>
        <div class="details"><asp:label id="txtAge" runat="server" /></div>
        <div class="details"><asp:label id="txtLocation" runat="server" /></div>
    </div>			
</div>
