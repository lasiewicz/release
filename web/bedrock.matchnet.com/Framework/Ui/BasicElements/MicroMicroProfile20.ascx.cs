﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.PageElements;
using Matchnet.List.ValueObjects;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Applications.QuestionAnswer;
using Matchnet.Web.Applications.MemberLike;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    /// <summary>
    /// This is a profile control displaying a small version of the profile, used 
    /// on the homepage for Your Matches and Hotlists.
    /// </summary>
    public partial class MicroMicroProfile20 : FrameworkControl
    {
        #region Fields
        private int _ordinal;
        private int _memberID;
        private BreadCrumbHelper.EntryPoint _myEntryPoint;
        private MOCollection _myMOCollection;
        private int _hotListCategoryID;
        private Member.ServiceAdapters.Member _Member;
        private bool _isHighlighted = false;
        protected string viewProfileUrl;
        private DateTime _lastHotlistActionDate = DateTime.MinValue;
        private bool _displayCommunication = true;
        
        const int MAX_WORD_LENGTH = 10;
        const int MAX_STRING_LENGTH = 40;
        const int MAX_USERNAME_LENGTH = 16;
        #endregion

        #region Properties
        public Member.ServiceAdapters.Member member
        {
            get { return _Member; }
            set
            {
                _Member = value;
                _memberID = _Member.MemberID;

                if (FrameworkGlobals.memberHighlighted(this._memberID, g.Brand))
                {
                    this._isHighlighted = true;
                }
                else
                {
                    this._isHighlighted = false;
                }
            }
        }

        public Framework.Txt TxtFromLabel
        {
            get { return txtFromLabel; }
        }

        public Framework.Txt TxtToLabel
        {
            get { return txtToLabel; }
        }

        public int MemberID
        {
            //get { return (_memberID); }
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.MemberID;
                }
                return 0;
            }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }


        public BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get { return (_myEntryPoint); }
            set { _myEntryPoint = value; }
        }

        public MOCollection MyMOCollection
        {
            get { return (_myMOCollection); }
            set { _myMOCollection = value; }
        }

        public int HotListCategoryID
        {
            get { return (_hotListCategoryID); }
            set { _hotListCategoryID = value; }
        }

        public bool IsHighlighted
        {
            get { return (this._isHighlighted); }
            set { this._isHighlighted = value; }
        }

        public DateTime LastHotlistActionDate
        {
            get { return (this._lastHotlistActionDate); }
            set { this._lastHotlistActionDate = value; }
        }

        public bool DisplayCommunication
        {
            get { return this._displayCommunication; }
            set { this._displayCommunication = value; }
        }

        public string TrackingParam { get; set; }
        #endregion

        #region Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {
            ;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
        #endregion


        #region Methods

        protected string getDateDisplay(DateTime pDate)
        {
            return Matchnet.Web.Applications.MemberProfile.ProfileTabs30.ProfileUtility.GetDateDisplay(pDate, g);
        }

        public string GetResourceForJS(string resConst)
        {
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, this));
        }

        public string GetRedirectUrl(int prtId)
        {
            string redirectUrl = string.Empty;
            if (g.Member != null && g.Member.MemberID > 0)
            {
                if (!g.Member.IsPayingMember(g.Brand.Site.SiteID))
                {
                    Matchnet.Content.ServiceAdapters.Links.ParameterCollection parameterCollection = new Matchnet.Content.ServiceAdapters.Links.ParameterCollection();
                    if (_g.ImpersonateContext)
                    {
                        parameterCollection.Add(WebConstants.IMPERSONATE_MEMBERID, _g.TargetMemberID.ToString());
                        parameterCollection.Add(WebConstants.IMPERSONATE_BRANDID, _g.TargetBrandID.ToString());
                    }
                    redirectUrl = Framework.Util.Redirect.GetSubscriptionUrl(_g.Brand, prtId, g.Member.MemberID, false, HttpContext.Current.Server.UrlEncode(HttpContext.Current.Items["FullURL"].ToString()), parameterCollection);
                }
            }
            return redirectUrl;
        }

        public bool IsCommentsEnabled
        {
            get { return QuestionAnswerHelper.IsQuestionAnswerCommentEnabled(g.Brand); }
        }

        public bool IsMemberLikeEnabled
        {
            get { return MemberLikeHelper.Instance.IsMemberLikeEnabled(g.Brand); }
        }

        public bool IsLoggedIn
        {
            get { return null != g.Member && g.Member.MemberID > 0; }
        }

        public string MemberUserName
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.GetUserName(g.Brand);
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Loads Member Profile based on properties already set into the control, including the Member object
        /// </summary>
        public void LoadMemberProfile(bool showHistoryIcon)
        {
            try
            {
                SearchResultProfile searchResult = new SearchResultProfile(_Member, Ordinal);

                if (this.IsHighlighted)
                {
                    //viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.Highlight);
                    //this.phHighlightedInfoLink.Visible = true;
                    //this.ucHighlightProfileInfoDisplay.Visible = true;
                    //this.ucHighlightProfileInfoDisplay.ViewedMemberID = _memberID;
                    //this.ucHighlightProfileInfoDisplay.MemberHighlighted = true;
                    //this.ucHighlightProfileInfoDisplay.ShowImageNoText = true;
                }
                else
                {
                    //viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
                    this.panelMicroMicroProfile20.CssClass = "micro-micro-profile clearfix"; //css for container div, non-highlight
                    //this.ucHighlightProfileInfoDisplay.Visible = false;
                }

                if(!String.IsNullOrEmpty(TrackingParam)) {
                    //viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, TrackingParam);
                }
                viewProfileUrl = "";
                //set basic info
                lnkUserName1.Text = _Member.GetUserName(_g.Brand);

                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, _Member, _g.Brand);
                searchResult.SetThumbNoTextNoLink(imgThumb, noPhoto);

                searchResult.SetAge(literalAge, this);
                
                if (FrameworkGlobals.isHebrewSite(g.Brand))
                {
                    literalAge.Text = g.GetResource("TXT_YEARS_OLD", this) + " " + literalAge.Text;
                }
                else
                {
                    literalAge.Text += " " + g.GetResource("TXT_YEARS_OLD", this);
                }

                searchResult.SetRegion(g.Brand.Site.LanguageID, literalLocation, MAX_STRING_LENGTH);
                LinkParent parent = LinkParent.HomePageMicroProfile;

                if (showHistoryIcon)
                {
                    this.ShowHistoryIcon();
                }

                if (Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_OVERVIEW_ON_MESSAGE_COMPOSE_BOX", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)))
                {
                    ShowOverviewLink();
                }

                if (Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_DETAIL_ON_MESSAGE_COMPOSE_BOX", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)))
                {
                    ShowProfileDetail();
                }

                if (Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_INMYOWNWORDS_ON_MESSAGE_COMPOSE_BOX", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)))
                {
                    ShowInMyOwnWordEssay();
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public void ShowHistoryIcon()
        {
            if (contactHistory != null)
            {
                phHistoryIcon.Visible = Visible;
                //contact history
                contactHistory.LoadContactHistory(member);
            }
        }

        public void ShowOverviewLink()
        {
            if (phOverView != null)
            {
                phOverView.Visible = true;
            }

            if (basicInfo1 != null)
            {
                //Load Basic Info
                basicInfo1.LoadBasicInfo(member);
            }

            if (lastLoginDate != null)
            {
                //timestamps
                DateTime loginDate = member.GetLastLogonDate(g.Brand.Site.Community.CommunityID);
                lastLoginDate.LastLoginDateValue = loginDate;
                lastUpdateDate.Date = getDateDisplay(member.GetAttributeDate(g.Brand, "LastUpdated", loginDate));
            }

            if (nvDataGroupDealBreakerInfo != null)
            {
                //Load Dealbreaker info
                nvDataGroupDealBreakerInfo.MaxDisplayLength = ProfileUtility.MaxDisplayLengthNVDouble;
                nvDataGroupDealBreakerInfo.LoadDataGroup(member, ProfileUtility.GetProfileDataGroup(ProfileDataGroupEnum.Dealbreakers, g.Brand.Site.SiteID, g));
            }

        }


        public void ShowProfileDetail()
        {
            if (phDetail != null)
            {
                phDetail.Visible = true;
            }

            if (profileDetails1 != null)
            {
                profileDetails1.LoadProfileDetails(member);
            }
        }


        public void ShowInMyOwnWordEssay()
        {
            if (phInMyOwnWords != null)
            {
                phInMyOwnWords.Visible = true;
            }

            if (ProfileTabGroup1 != null)
            {
                //Load Profile Tabs
                ProfileTabGroup1.LoadMicroProfileTabGroup(member, ProfileTabEnum.Essays);
            }
        }

        
        #endregion
    }
}
